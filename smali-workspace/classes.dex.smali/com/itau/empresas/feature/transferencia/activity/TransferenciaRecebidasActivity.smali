.class public Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;
.super Lcom/itau/empresas/ui/activity/BaseActivity;
.source "TransferenciaRecebidasActivity.java"

# interfaces
.implements Lbr/com/itau/library/br/com/itau/library/interfaces/IAction;


# instance fields
.field campoSelecionarFimMes:Landroid/widget/TextView;

.field campoSelecionarInicioMes:Landroid/widget/TextView;

.field controller:Lcom/itau/empresas/feature/transferencia/TransferenciaController;

.field dias:[Ljava/lang/String;

.field escolhePeriodo:Lbr/com/itau/library/LinearValuePickerView;

.field private fechouHintComClique:Z

.field headerListaTransferenciasRecebidas:Landroid/widget/RelativeLayout;

.field private hintUtils:Lcom/itau/empresas/ui/util/HintUtils;

.field listaTransferenciaRecebidas:Landroid/widget/ListView;

.field llTransferenciaRecebidasDataDias:Landroid/widget/LinearLayout;

.field llTransferenciaRecebidasFiltro:Landroid/widget/LinearLayout;

.field llTransferenciaRecebidasFiltroData:Landroid/widget/LinearLayout;

.field logoutHelper:Lcom/itau/empresas/feature/logout/LogoutHelper;

.field private menu:Landroid/view/Menu;

.field rbTransferenciaRecebidasData:Landroid/widget/RadioButton;

.field rbTransferenciaRecebidasDias:Landroid/widget/RadioButton;

.field rgTransferenciaRecebidasTipoFiltro:Landroid/widget/RadioGroup;

.field rlTransferenciasRecebidasFiltroDias:Landroid/widget/RelativeLayout;

.field root:Landroid/widget/LinearLayout;

.field tabTransferencias:Landroid/support/design/widget/TabLayout;

.field textoEscolhaData:Landroid/widget/TextView;

.field textoSemTransferenciaRecebidas:Landroid/widget/TextView;

.field private tipoTransferencia:Ljava/lang/String;

.field toolBar:Landroid/support/v7/widget/Toolbar;

.field private topSnackbar:Lbr/com/itau/topsnackbar/TopSnackbar;

.field private transferenciaRecebidasAdapter:Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter;


# direct methods
.method public constructor <init>()V
    .registers 2

    .line 61
    invoke-direct {p0}, Lcom/itau/empresas/ui/activity/BaseActivity;-><init>()V

    .line 98
    const-string v0, "tef"

    iput-object v0, p0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;->tipoTransferencia:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$000(Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;)V
    .registers 1
    .param p0, "x0"    # Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;

    .line 61
    invoke-direct {p0}, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;->carregarLista()V

    return-void
.end method

.method static synthetic access$100(Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;Landroid/support/design/widget/TabLayout$Tab;)V
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;
    .param p1, "x1"    # Landroid/support/design/widget/TabLayout$Tab;

    .line 61
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;->itemTabClick(Landroid/support/design/widget/TabLayout$Tab;)V

    return-void
.end method

.method static synthetic access$200(Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;)Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter;
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;

    .line 61
    iget-object v0, p0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;->transferenciaRecebidasAdapter:Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter;

    return-object v0
.end method

.method static synthetic access$300(Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;Lcom/wdullaer/materialdatetimepicker/date/DatePickerDialog;)Ljava/lang/String;
    .registers 3
    .param p0, "x0"    # Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;
    .param p1, "x1"    # Lcom/wdullaer/materialdatetimepicker/date/DatePickerDialog;

    .line 61
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;->convertaDatePickerParaString(Lcom/wdullaer/materialdatetimepicker/date/DatePickerDialog;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$400(Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;)Z
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;

    .line 61
    iget-boolean v0, p0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;->fechouHintComClique:Z

    return v0
.end method

.method static synthetic access$402(Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;Z)Z
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;
    .param p1, "x1"    # Z

    .line 61
    iput-boolean p1, p0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;->fechouHintComClique:Z

    return p1
.end method

.method static synthetic access$500(Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;)Lcom/itau/empresas/ui/util/HintUtils;
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;

    .line 61
    iget-object v0, p0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;->hintUtils:Lcom/itau/empresas/ui/util/HintUtils;

    return-object v0
.end method

.method private adicionaHint(Landroid/view/MenuItem;)V
    .registers 6
    .param p1, "menuItem"    # Landroid/view/MenuItem;

    .line 409
    iget-object v0, p0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;->hintUtils:Lcom/itau/empresas/ui/util/HintUtils;

    const-string v1, "CHVTRF-ID"

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/HintUtils;->exibeHint(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_b

    .line 410
    return-void

    .line 414
    :cond_b
    new-instance v3, Lbr/com/itau/widgets/hintview/HintActionView;

    invoke-direct {v3, p0}, Lbr/com/itau/widgets/hintview/HintActionView;-><init>(Landroid/content/Context;)V

    .line 415
    .local v3, "hintActionView":Lbr/com/itau/widgets/hintview/HintActionView;
    invoke-interface {p1, v3}, Landroid/view/MenuItem;->setActionView(Landroid/view/View;)Landroid/view/MenuItem;

    .line 416
    new-instance v0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity$7;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity$7;-><init>(Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;)V

    invoke-virtual {v3, v0}, Lbr/com/itau/widgets/hintview/HintActionView;->setOnMenuItemClick(Landroid/view/MenuItem$OnMenuItemClickListener;)V

    .line 423
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;->fechouHintComClique:Z

    .line 425
    new-instance v0, Lbr/com/itau/widgets/hintview/Hint$Builder;

    const v1, 0x7f09011b

    invoke-direct {v0, p1, v1}, Lbr/com/itau/widgets/hintview/Hint$Builder;-><init>(Landroid/view/MenuItem;I)V

    .line 426
    const v1, 0x7f07035e

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 425
    const/16 v2, 0x50

    invoke-static {v0, v1, v2}, Lcom/itau/empresas/ui/util/HintUtils;->montaHintAlertaMenuItem(Lbr/com/itau/widgets/hintview/Hint$Builder;Ljava/lang/String;I)Lbr/com/itau/widgets/hintview/Hint$Builder;

    move-result-object v0

    new-instance v1, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity$9;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity$9;-><init>(Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;)V

    .line 428
    invoke-virtual {v0, v1}, Lbr/com/itau/widgets/hintview/Hint$Builder;->setOnClickListener(Lbr/com/itau/widgets/hintview/OnClickListener;)Lbr/com/itau/widgets/hintview/Hint$Builder;

    move-result-object v0

    new-instance v1, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity$8;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity$8;-><init>(Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;)V

    .line 436
    invoke-virtual {v0, v1}, Lbr/com/itau/widgets/hintview/Hint$Builder;->setOnDismissListener(Lbr/com/itau/widgets/hintview/OnDismissListener;)Lbr/com/itau/widgets/hintview/Hint$Builder;

    move-result-object v0

    .line 454
    invoke-virtual {v0}, Lbr/com/itau/widgets/hintview/Hint$Builder;->show()Lbr/com/itau/widgets/hintview/Hint;

    .line 456
    return-void
.end method

.method private carregarLista()V
    .registers 7

    .line 187
    iget-object v0, p0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;->rbTransferenciaRecebidasData:Landroid/widget/RadioButton;

    invoke-virtual {v0}, Landroid/widget/RadioButton;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_5f

    .line 188
    iget-object v0, p0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;->campoSelecionarInicioMes:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v4

    .line 189
    .local v4, "dtInicio":Ljava/lang/String;
    iget-object v0, p0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;->campoSelecionarFimMes:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v5

    .line 192
    .local v5, "dtFim":Ljava/lang/String;
    invoke-virtual {v4}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_28

    .line 193
    const/4 v0, 0x7

    invoke-static {v0}, Lcom/itau/empresas/ui/util/DataEHoraUtils;->subtraiDiasDeHoje(I)Ljava/lang/String;

    move-result-object v4

    goto :goto_2c

    .line 195
    :cond_28
    invoke-static {v4}, Lcom/itau/empresas/ui/util/DataEHoraUtils;->formataDataApi(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 199
    :goto_2c
    invoke-virtual {v5}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_37

    .line 200
    invoke-static {}, Lcom/itau/empresas/ui/util/DataEHoraUtils;->getDataAtual()Ljava/lang/String;

    move-result-object v5

    goto :goto_3b

    .line 202
    :cond_37
    invoke-static {v5}, Lcom/itau/empresas/ui/util/DataEHoraUtils;->formataDataApi(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 205
    :goto_3b
    invoke-static {v4}, Lcom/itau/empresas/ui/util/DataEHoraUtils;->parse(Ljava/lang/String;)Lorg/threeten/bp/LocalDate;

    move-result-object v0

    .line 206
    invoke-static {v5}, Lcom/itau/empresas/ui/util/DataEHoraUtils;->parse(Ljava/lang/String;)Lorg/threeten/bp/LocalDate;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/threeten/bp/LocalDate;->isAfter(Lorg/threeten/bp/chrono/ChronoLocalDate;)Z

    move-result v0

    if-eqz v0, :cond_59

    .line 207
    iget-object v0, p0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;->root:Landroid/widget/LinearLayout;

    const-string v1, "Data inicial deve ser menor ou igual a data final."

    .line 208
    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lbr/com/itau/topsnackbar/TopSnackbar;->make(Landroid/view/ViewGroup;Ljava/lang/CharSequence;I)Lbr/com/itau/topsnackbar/TopSnackbar;

    move-result-object v0

    .line 209
    invoke-virtual {v0}, Lbr/com/itau/topsnackbar/TopSnackbar;->show()Lbr/com/itau/topsnackbar/TopSnackbar;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;->topSnackbar:Lbr/com/itau/topsnackbar/TopSnackbar;

    goto :goto_5e

    .line 211
    :cond_59
    iget-object v0, p0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;->tipoTransferencia:Ljava/lang/String;

    invoke-direct {p0, v4, v5, v0}, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;->carregarListaTransferenciaRecebidas(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 214
    .end local v4    # "dtInicio":Ljava/lang/String;
    .end local v5    # "dtFim":Ljava/lang/String;
    :goto_5e
    goto :goto_98

    :cond_5f
    iget-object v0, p0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;->rbTransferenciaRecebidasDias:Landroid/widget/RadioButton;

    invoke-virtual {v0}, Landroid/widget/RadioButton;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_98

    .line 215
    iget-object v0, p0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;->dias:[Ljava/lang/String;

    iget-object v1, p0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;->escolhePeriodo:Lbr/com/itau/library/LinearValuePickerView;

    invoke-virtual {v1}, Lbr/com/itau/library/LinearValuePickerView;->getSelectedPosition()I

    move-result v1

    aget-object v0, v0, v1

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    .line 216
    .local v4, "periodo":I
    const v0, 0x7f0702b0

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f07031d

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    int-to-long v2, v4

    .line 217
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    .line 216
    invoke-virtual {p0, v0, v1, v2}, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;->analyticsHit(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    .line 218
    invoke-static {v4}, Lcom/itau/empresas/ui/util/DataEHoraUtils;->subtraiDiasDeHoje(I)Ljava/lang/String;

    move-result-object v0

    .line 219
    invoke-static {}, Lcom/itau/empresas/ui/util/DataEHoraUtils;->getDataAtual()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;->tipoTransferencia:Ljava/lang/String;

    .line 218
    invoke-direct {p0, v0, v1, v2}, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;->carregarListaTransferenciaRecebidas(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 221
    .end local v4    # "periodo":I
    :cond_98
    :goto_98
    return-void
.end method

.method private carregarListaTransferenciaRecebidas(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 6
    .param p1, "dataInicio"    # Ljava/lang/String;
    .param p2, "dataFim"    # Ljava/lang/String;
    .param p3, "tipo"    # Ljava/lang/String;

    .line 146
    iget-object v0, p0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;->topSnackbar:Lbr/com/itau/topsnackbar/TopSnackbar;

    if-eqz v0, :cond_9

    .line 147
    iget-object v0, p0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;->topSnackbar:Lbr/com/itau/topsnackbar/TopSnackbar;

    invoke-virtual {v0}, Lbr/com/itau/topsnackbar/TopSnackbar;->dismiss()V

    .line 149
    :cond_9
    invoke-virtual {p0}, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;->mostraDialogoDeProgresso()V

    .line 150
    iget-object v0, p0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;->listaTransferenciaRecebidas:Landroid/widget/ListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 152
    iget-object v0, p0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;->controller:Lcom/itau/empresas/feature/transferencia/TransferenciaController;

    invoke-virtual {v0, p1, p2, p3}, Lcom/itau/empresas/feature/transferencia/TransferenciaController;->buscaTransferenciaRecebidas(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 153
    return-void
.end method

.method private configurarTabs()V
    .registers 6

    .line 156
    iget-object v0, p0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;->tabTransferencias:Landroid/support/design/widget/TabLayout;

    invoke-virtual {v0}, Landroid/support/design/widget/TabLayout;->newTab()Landroid/support/design/widget/TabLayout$Tab;

    move-result-object v2

    .line 157
    .local v2, "tabItemTransferencias":Landroid/support/design/widget/TabLayout$Tab;
    iget-object v0, p0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;->tabTransferencias:Landroid/support/design/widget/TabLayout;

    invoke-virtual {v0}, Landroid/support/design/widget/TabLayout;->newTab()Landroid/support/design/widget/TabLayout$Tab;

    move-result-object v3

    .line 158
    .local v3, "tabItemTED":Landroid/support/design/widget/TabLayout$Tab;
    iget-object v0, p0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;->tabTransferencias:Landroid/support/design/widget/TabLayout;

    invoke-virtual {v0}, Landroid/support/design/widget/TabLayout;->newTab()Landroid/support/design/widget/TabLayout$Tab;

    move-result-object v4

    .line 159
    .local v4, "tabItemDOC":Landroid/support/design/widget/TabLayout$Tab;
    const v0, 0x7f070509

    invoke-virtual {v2, v0}, Landroid/support/design/widget/TabLayout$Tab;->setText(I)Landroid/support/design/widget/TabLayout$Tab;

    .line 160
    const v0, 0x7f070508

    invoke-virtual {v3, v0}, Landroid/support/design/widget/TabLayout$Tab;->setText(I)Landroid/support/design/widget/TabLayout$Tab;

    .line 161
    const v0, 0x7f070505

    invoke-virtual {v4, v0}, Landroid/support/design/widget/TabLayout$Tab;->setText(I)Landroid/support/design/widget/TabLayout$Tab;

    .line 162
    iget-object v0, p0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;->tabTransferencias:Landroid/support/design/widget/TabLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v2, v1}, Landroid/support/design/widget/TabLayout;->addTab(Landroid/support/design/widget/TabLayout$Tab;I)V

    .line 163
    iget-object v0, p0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;->tabTransferencias:Landroid/support/design/widget/TabLayout;

    const/4 v1, 0x1

    invoke-virtual {v0, v3, v1}, Landroid/support/design/widget/TabLayout;->addTab(Landroid/support/design/widget/TabLayout$Tab;I)V

    .line 164
    iget-object v0, p0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;->tabTransferencias:Landroid/support/design/widget/TabLayout;

    const/4 v1, 0x2

    invoke-virtual {v0, v4, v1}, Landroid/support/design/widget/TabLayout;->addTab(Landroid/support/design/widget/TabLayout$Tab;I)V

    .line 166
    iget-object v0, p0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;->tabTransferencias:Landroid/support/design/widget/TabLayout;

    new-instance v1, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity$2;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity$2;-><init>(Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;)V

    invoke-virtual {v0, v1}, Landroid/support/design/widget/TabLayout;->addOnTabSelectedListener(Landroid/support/design/widget/TabLayout$OnTabSelectedListener;)V

    .line 184
    return-void
.end method

.method private constroiToolbar()V
    .registers 3

    .line 475
    iget-object v0, p0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;->toolBar:Landroid/support/v7/widget/Toolbar;

    invoke-static {v0, p0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom;->Builder(Landroid/support/v7/widget/Toolbar;Landroid/support/v7/app/AppCompatActivity;)Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    move-result-object v0

    .line 477
    const v1, 0x7f0706bf

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->comTitulo(I)Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    move-result-object v0

    .line 478
    invoke-virtual {v0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->comClickListenerFinish()Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    move-result-object v0

    .line 479
    invoke-virtual {v0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->build()Lcom/itau/empresas/ui/util/listener/ToolbarCustom;

    .line 480
    return-void
.end method

.method private convertaDatePickerParaString(Lcom/wdullaer/materialdatetimepicker/date/DatePickerDialog;)Ljava/lang/String;
    .registers 8
    .param p1, "datePickerDialog"    # Lcom/wdullaer/materialdatetimepicker/date/DatePickerDialog;

    .line 383
    invoke-virtual {p1}, Lcom/wdullaer/materialdatetimepicker/date/DatePickerDialog;->getSelectedDay()Lcom/wdullaer/materialdatetimepicker/date/MonthAdapter$CalendarDay;

    move-result-object v0

    invoke-virtual {v0}, Lcom/wdullaer/materialdatetimepicker/date/MonthAdapter$CalendarDay;->getDay()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    .line 384
    .local v3, "dia":Ljava/lang/Integer;
    invoke-virtual {p1}, Lcom/wdullaer/materialdatetimepicker/date/DatePickerDialog;->getSelectedDay()Lcom/wdullaer/materialdatetimepicker/date/MonthAdapter$CalendarDay;

    move-result-object v0

    invoke-virtual {v0}, Lcom/wdullaer/materialdatetimepicker/date/MonthAdapter$CalendarDay;->getMonth()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    .line 385
    .local v4, "mes":Ljava/lang/Integer;
    invoke-virtual {p1}, Lcom/wdullaer/materialdatetimepicker/date/DatePickerDialog;->getSelectedDay()Lcom/wdullaer/materialdatetimepicker/date/MonthAdapter$CalendarDay;

    move-result-object v0

    invoke-virtual {v0}, Lcom/wdullaer/materialdatetimepicker/date/MonthAdapter$CalendarDay;->getYear()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    .line 387
    .local v5, "ano":Ljava/lang/Integer;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const/16 v2, 0xa

    if-ge v1, v2, :cond_4b

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "0"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v3}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_4f

    :cond_4b
    invoke-virtual {v3}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v1

    :goto_4f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 388
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const/16 v2, 0xa

    if-ge v1, v2, :cond_79

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "0"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v4}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_7d

    :cond_79
    invoke-virtual {v4}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v1

    :goto_7d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 389
    invoke-virtual {v5}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 387
    return-object v0
.end method

.method private hitAnalytcs(Ljava/lang/String;I)V
    .registers 5
    .param p1, "tipoTransferencia"    # Ljava/lang/String;
    .param p2, "analytcsResource"    # I

    .line 470
    iput-object p1, p0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;->tipoTransferencia:Ljava/lang/String;

    .line 471
    const v0, 0x7f0702c8

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p2}, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;->analyticsHit(Ljava/lang/String;Ljava/lang/String;)V

    .line 472
    return-void
.end method

.method private itemTabClick(Landroid/support/design/widget/TabLayout$Tab;)V
    .registers 4
    .param p1, "tab"    # Landroid/support/design/widget/TabLayout$Tab;

    .line 459
    invoke-virtual {p1}, Landroid/support/design/widget/TabLayout$Tab;->getPosition()I

    move-result v0

    if-nez v0, :cond_f

    .line 460
    const-string v0, "tef"

    const v1, 0x7f070322

    invoke-direct {p0, v0, v1}, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;->hitAnalytcs(Ljava/lang/String;I)V

    goto :goto_2e

    .line 461
    :cond_f
    invoke-virtual {p1}, Landroid/support/design/widget/TabLayout$Tab;->getPosition()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1f

    .line 462
    const-string v0, "ted"

    const v1, 0x7f070321

    invoke-direct {p0, v0, v1}, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;->hitAnalytcs(Ljava/lang/String;I)V

    goto :goto_2e

    .line 463
    :cond_1f
    invoke-virtual {p1}, Landroid/support/design/widget/TabLayout$Tab;->getPosition()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2e

    .line 464
    const-string v0, "doc"

    const v1, 0x7f070320

    invoke-direct {p0, v0, v1}, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;->hitAnalytcs(Ljava/lang/String;I)V

    .line 466
    :cond_2e
    :goto_2e
    return-void
.end method

.method private pesquisarTransferenciaRecebidas()Landroid/support/v7/widget/SearchView$OnQueryTextListener;
    .registers 2

    .line 318
    new-instance v0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity$4;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity$4;-><init>(Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;)V

    return-object v0
.end method


# virtual methods
.method public OnClickAction()V
    .registers 1

    .line 397
    invoke-virtual {p0}, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;->mostraDialogoDeProgresso()V

    .line 398
    invoke-direct {p0}, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;->carregarLista()V

    .line 399
    return-void
.end method

.method aoClicarNoBotaoSelecionarFimMes()V
    .registers 3

    .line 368
    iget-object v0, p0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;->campoSelecionarFimMes:Landroid/widget/TextView;

    .line 369
    invoke-static {p0, v0}, Lcom/itau/empresas/ui/util/DataEHoraUtils;->mostraDialogoData(Lcom/itau/empresas/ui/activity/BaseActivity;Landroid/widget/TextView;)Lcom/wdullaer/materialdatetimepicker/date/DatePickerDialog;

    move-result-object v1

    .line 370
    .local v1, "dpdPorFimMes":Lcom/wdullaer/materialdatetimepicker/date/DatePickerDialog;
    new-instance v0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity$6;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity$6;-><init>(Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;)V

    invoke-virtual {v1, v0}, Lcom/wdullaer/materialdatetimepicker/date/DatePickerDialog;->setOnDateSetListener(Lcom/wdullaer/materialdatetimepicker/date/DatePickerDialog$OnDateSetListener;)V

    .line 380
    return-void
.end method

.method aoClicarNoBotaoSelecionarInicioMes()V
    .registers 3

    .line 351
    iget-object v0, p0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;->campoSelecionarInicioMes:Landroid/widget/TextView;

    invoke-static {p0, v0}, Lcom/itau/empresas/ui/util/DataEHoraUtils;->mostraDialogoData(Lcom/itau/empresas/ui/activity/BaseActivity;Landroid/widget/TextView;)Lcom/wdullaer/materialdatetimepicker/date/DatePickerDialog;

    move-result-object v1

    .line 353
    .local v1, "dpdPorInicioMes":Lcom/wdullaer/materialdatetimepicker/date/DatePickerDialog;
    new-instance v0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity$5;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity$5;-><init>(Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;)V

    invoke-virtual {v1, v0}, Lcom/wdullaer/materialdatetimepicker/date/DatePickerDialog;->setOnDateSetListener(Lcom/wdullaer/materialdatetimepicker/date/DatePickerDialog$OnDateSetListener;)V

    .line 364
    return-void
.end method

.method carregaElementosDaTela()V
    .registers 4

    .line 107
    invoke-direct {p0}, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;->constroiToolbar()V

    .line 108
    invoke-direct {p0}, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;->configurarTabs()V

    .line 110
    iget-object v0, p0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;->rgTransferenciaRecebidasTipoFiltro:Landroid/widget/RadioGroup;

    new-instance v1, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity$1;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity$1;-><init>(Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;)V

    .line 111
    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    .line 129
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    .line 130
    .local v2, "dataInicio":Ljava/util/Calendar;
    const/4 v0, 0x5

    const/4 v1, -0x7

    invoke-virtual {v2, v0, v1}, Ljava/util/Calendar;->add(II)V

    .line 131
    iget-object v0, p0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;->campoSelecionarInicioMes:Landroid/widget/TextView;

    invoke-virtual {v2}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v1

    invoke-static {v1}, Lcom/itau/empresas/ui/util/DataEHoraUtils;->printData(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 133
    iget-object v0, p0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;->escolhePeriodo:Lbr/com/itau/library/LinearValuePickerView;

    iget-object v1, p0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;->dias:[Ljava/lang/String;

    invoke-virtual {v0, v1}, Lbr/com/itau/library/LinearValuePickerView;->setValues([Ljava/lang/String;)V

    .line 134
    iget-object v0, p0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;->escolhePeriodo:Lbr/com/itau/library/LinearValuePickerView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lbr/com/itau/library/LinearValuePickerView;->setSelectedPosition(I)V

    .line 135
    iget-object v0, p0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;->escolhePeriodo:Lbr/com/itau/library/LinearValuePickerView;

    invoke-virtual {v0, p0}, Lbr/com/itau/library/LinearValuePickerView;->setOnClickAction(Lbr/com/itau/library/br/com/itau/library/interfaces/IAction;)V

    .line 137
    invoke-direct {p0}, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;->carregarLista()V

    .line 139
    new-instance v0, Lcom/itau/empresas/ui/util/HintUtils;

    iget-object v1, p0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;->application:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v1}, Lcom/itau/empresas/CustomApplication;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/itau/empresas/ui/util/HintUtils;-><init>(Landroid/content/SharedPreferences;)V

    iput-object v0, p0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;->hintUtils:Lcom/itau/empresas/ui/util/HintUtils;

    .line 140
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .registers 6
    .param p1, "menu"    # Landroid/view/Menu;

    .line 284
    if-eqz p1, :cond_3f

    .line 285
    invoke-virtual {p0}, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f0f0005

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 286
    const v0, 0x7f0e06c5

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;->adicionaHint(Landroid/view/MenuItem;)V

    .line 288
    iput-object p1, p0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;->menu:Landroid/view/Menu;

    .line 290
    const v0, 0x7f0e06c6

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    .line 291
    .local v2, "itemSV":Landroid/view/MenuItem;
    invoke-static {v2}, Landroid/support/v4/view/MenuItemCompat;->getActionView(Landroid/view/MenuItem;)Landroid/view/View;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Landroid/support/v7/widget/SearchView;

    .line 292
    .local v3, "searchView":Landroid/support/v7/widget/SearchView;
    const v0, 0x7f070635

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/support/v7/widget/SearchView;->setQueryHint(Ljava/lang/CharSequence;)V

    .line 293
    invoke-direct {p0}, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;->pesquisarTransferenciaRecebidas()Landroid/support/v7/widget/SearchView$OnQueryTextListener;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/support/v7/widget/SearchView;->setOnQueryTextListener(Landroid/support/v7/widget/SearchView$OnQueryTextListener;)V

    .line 295
    new-instance v0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity$3;

    invoke-direct {v0, p0, p1}, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity$3;-><init>(Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;Landroid/view/Menu;)V

    .line 296
    invoke-static {v2, v0}, Landroid/support/v4/view/MenuItemCompat;->setOnActionExpandListener(Landroid/view/MenuItem;Landroid/support/v4/view/MenuItemCompat$OnActionExpandListener;)Landroid/view/MenuItem;

    .line 314
    .end local v2    # "itemSV":Landroid/view/MenuItem;
    .end local v3    # "searchView":Landroid/support/v7/widget/SearchView;
    :cond_3f
    const/4 v0, 0x1

    return v0
.end method

.method public onEventMainThread(Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent$Conversion;)V
    .registers 5
    .param p1, "e"    # Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent$Conversion;

    .line 254
    invoke-static {p1, p0}, Lcom/itau/empresas/api/ApiHelper;->devoTratarEvento(Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;Lcom/itau/empresas/api/ApiConsumidor;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 255
    return-void

    .line 258
    :cond_7
    invoke-virtual {p0}, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;->escondeDialogoDeProgresso()V

    .line 259
    iget-object v0, p0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;->root:Landroid/widget/LinearLayout;

    const v1, 0x7f0701f2

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lbr/com/itau/topsnackbar/TopSnackbar;->make(Landroid/view/ViewGroup;II)Lbr/com/itau/topsnackbar/TopSnackbar;

    move-result-object v0

    .line 260
    invoke-virtual {v0}, Lbr/com/itau/topsnackbar/TopSnackbar;->show()Lbr/com/itau/topsnackbar/TopSnackbar;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;->topSnackbar:Lbr/com/itau/topsnackbar/TopSnackbar;

    .line 261
    return-void
.end method

.method public onEventMainThread(Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;)V
    .registers 7
    .param p1, "e"    # Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;

    .line 265
    invoke-static {p1, p0}, Lcom/itau/empresas/api/ApiHelper;->devoTratarEvento(Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;Lcom/itau/empresas/api/ApiConsumidor;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 266
    return-void

    .line 269
    :cond_7
    invoke-virtual {p0}, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;->escondeDialogoDeProgresso()V

    .line 270
    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;->getException()Lbr/com/itau/sdk/android/core/exception/BackendException;

    move-result-object v3

    .line 271
    .local v3, "exception":Lbr/com/itau/sdk/android/core/exception/BackendException;
    invoke-virtual {v3}, Lbr/com/itau/sdk/android/core/exception/BackendException;->getError()Lbr/com/itau/sdk/android/core/model/ErrorDTO;

    move-result-object v0

    invoke-virtual {v0}, Lbr/com/itau/sdk/android/core/model/ErrorDTO;->getMensagem()Ljava/lang/String;

    move-result-object v4

    .line 272
    .local v4, "mensagem":Ljava/lang/String;
    if-nez v4, :cond_3c

    .line 273
    const-string v0, "TransferenciaRecebidasA"

    const-string v1, "onEventMainThread: falha ao recuperar mensagem de erro."

    invoke-static {v0, v1, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 274
    const-string v0, "TransferenciaRecebidasA"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "falha ao recuperar mensagem de erro. "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x5

    invoke-static {v2, v0, v1}, Lcom/crashlytics/android/Crashlytics;->log(ILjava/lang/String;Ljava/lang/String;)V

    .line 275
    invoke-static {v3}, Lcom/crashlytics/android/Crashlytics;->logException(Ljava/lang/Throwable;)V

    goto :goto_49

    .line 277
    :cond_3c
    iget-object v0, p0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;->root:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-static {v0, v4, v1}, Lbr/com/itau/topsnackbar/TopSnackbar;->make(Landroid/view/ViewGroup;Ljava/lang/CharSequence;I)Lbr/com/itau/topsnackbar/TopSnackbar;

    move-result-object v0

    .line 278
    invoke-virtual {v0}, Lbr/com/itau/topsnackbar/TopSnackbar;->show()Lbr/com/itau/topsnackbar/TopSnackbar;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;->topSnackbar:Lbr/com/itau/topsnackbar/TopSnackbar;

    .line 280
    :goto_49
    return-void
.end method

.method public onEventMainThread(Ljava/util/List;)V
    .registers 4
    .param p1, "retornoVO"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasVO;>;)V"
        }
    .end annotation

    .line 228
    invoke-virtual {p0}, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;->escondeDialogoDeProgresso()V

    .line 230
    iget-object v0, p0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;->listaTransferenciaRecebidas:Landroid/widget/ListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 232
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_33

    .line 233
    iget-object v0, p0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;->textoSemTransferenciaRecebidas:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 234
    iget-object v0, p0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;->headerListaTransferenciasRecebidas:Landroid/widget/RelativeLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 235
    new-instance v0, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter;

    invoke-direct {v0, p0, p1}, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter;-><init>(Landroid/content/Context;Ljava/util/List;)V

    iput-object v0, p0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;->transferenciaRecebidasAdapter:Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter;

    .line 236
    iget-object v0, p0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;->listaTransferenciaRecebidas:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;->transferenciaRecebidasAdapter:Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 237
    iget-object v0, p0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;->transferenciaRecebidasAdapter:Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter;

    sget-object v1, Lcom/daimajia/swipe/util/Attributes$Mode;->Single:Lcom/daimajia/swipe/util/Attributes$Mode;

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter;->setMode(Lcom/daimajia/swipe/util/Attributes$Mode;)V

    goto :goto_36

    .line 240
    :cond_33
    invoke-virtual {p0}, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;->semDados()V

    .line 242
    :goto_36
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .registers 4
    .param p1, "item"    # Landroid/view/MenuItem;

    .line 339
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x7f0e06c5

    if-ne v0, v1, :cond_1b

    .line 340
    iget-object v0, p0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;->llTransferenciaRecebidasFiltro:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;->llTransferenciaRecebidasFiltro:Landroid/widget/LinearLayout;

    .line 341
    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v1

    if-nez v1, :cond_16

    const/16 v1, 0x8

    goto :goto_17

    :cond_16
    const/4 v1, 0x0

    .line 340
    :goto_17
    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_29

    .line 343
    :cond_1b
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x7f0e06cf

    if-ne v0, v1, :cond_29

    .line 344
    iget-object v0, p0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;->application:Lcom/itau/empresas/CustomApplication;

    invoke-static {v0, p0}, Lcom/itau/empresas/ui/util/ViewUtils;->aoClicarBotaoSairMenu(Lcom/itau/empresas/CustomApplication;Lcom/itau/empresas/ui/activity/BaseActivity;)V

    .line 346
    :cond_29
    :goto_29
    invoke-super {p0, p1}, Lcom/itau/empresas/ui/activity/BaseActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0
.end method

.method public opkeysConsumidas()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Ljava/lang/String;>;"
        }
    .end annotation

    .line 403
    const-string v0, "transfRecebidas"

    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public semDados()V
    .registers 3

    .line 245
    iget-object v0, p0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;->textoSemTransferenciaRecebidas:Landroid/widget/TextView;

    const v1, 0x7f070472

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 246
    iget-object v0, p0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;->textoSemTransferenciaRecebidas:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 247
    iget-object v0, p0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;->headerListaTransferenciasRecebidas:Landroid/widget/RelativeLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 248
    new-instance v0, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter;-><init>(Landroid/content/Context;Ljava/util/List;)V

    iput-object v0, p0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;->transferenciaRecebidasAdapter:Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter;

    .line 250
    iget-object v0, p0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;->listaTransferenciaRecebidas:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;->transferenciaRecebidasAdapter:Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 251
    return-void
.end method

.class public final enum Lcom/google/zxing/client/android/a/h;
.super Ljava/lang/Enum;


# static fields
.field public static final enum a:Lcom/google/zxing/client/android/a/h;

.field public static final enum b:Lcom/google/zxing/client/android/a/h;

.field public static final enum c:Lcom/google/zxing/client/android/a/h;

.field private static final synthetic d:[Lcom/google/zxing/client/android/a/h;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    new-instance v0, Lcom/google/zxing/client/android/a/h;

    const-string v1, "ON"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/google/zxing/client/android/a/h;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/zxing/client/android/a/h;->a:Lcom/google/zxing/client/android/a/h;

    new-instance v0, Lcom/google/zxing/client/android/a/h;

    const-string v1, "AUTO"

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lcom/google/zxing/client/android/a/h;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/zxing/client/android/a/h;->b:Lcom/google/zxing/client/android/a/h;

    new-instance v0, Lcom/google/zxing/client/android/a/h;

    const-string v1, "OFF"

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2}, Lcom/google/zxing/client/android/a/h;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/zxing/client/android/a/h;->c:Lcom/google/zxing/client/android/a/h;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/zxing/client/android/a/h;

    sget-object v1, Lcom/google/zxing/client/android/a/h;->a:Lcom/google/zxing/client/android/a/h;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/zxing/client/android/a/h;->b:Lcom/google/zxing/client/android/a/h;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/zxing/client/android/a/h;->c:Lcom/google/zxing/client/android/a/h;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    sput-object v0, Lcom/google/zxing/client/android/a/h;->d:[Lcom/google/zxing/client/android/a/h;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static a(Landroid/content/SharedPreferences;)Lcom/google/zxing/client/android/a/h;
    .registers 3

    const-string v0, "zxing_preferences_front_light_mode"

    sget-object v1, Lcom/google/zxing/client/android/a/h;->c:Lcom/google/zxing/client/android/a/h;

    invoke-virtual {v1}, Lcom/google/zxing/client/android/a/h;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p0, v0, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/zxing/client/android/a/h;->a(Ljava/lang/String;)Lcom/google/zxing/client/android/a/h;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/lang/String;)Lcom/google/zxing/client/android/a/h;
    .registers 2

    if-nez p0, :cond_5

    sget-object v0, Lcom/google/zxing/client/android/a/h;->c:Lcom/google/zxing/client/android/a/h;

    goto :goto_9

    :cond_5
    invoke-static {p0}, Lcom/google/zxing/client/android/a/h;->valueOf(Ljava/lang/String;)Lcom/google/zxing/client/android/a/h;

    move-result-object v0

    :goto_9
    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/zxing/client/android/a/h;
    .registers 2

    const-class v0, Lcom/google/zxing/client/android/a/h;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/zxing/client/android/a/h;

    return-object v0
.end method

.method public static values()[Lcom/google/zxing/client/android/a/h;
    .registers 1

    sget-object v0, Lcom/google/zxing/client/android/a/h;->d:[Lcom/google/zxing/client/android/a/h;

    invoke-virtual {v0}, [Lcom/google/zxing/client/android/a/h;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/zxing/client/android/a/h;

    return-object v0
.end method

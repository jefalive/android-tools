.class public Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaInputVO;
.super Ljava/lang/Object;
.source "ContatoRecargaInputVO.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field agencia:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "agencia"
    .end annotation
.end field

.field apelido:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "apelido"
    .end annotation
.end field

.field conta:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "conta"
    .end annotation
.end field

.field ddd:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "ddd"
    .end annotation
.end field

.field digitoVerificadorConta:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "digito_verificador_conta"
    .end annotation
.end field

.field nomeOperadora:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "nome_operadora"
    .end annotation
.end field

.field regiao:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "regiao"
    .end annotation
.end field

.field telefone:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "telefone"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public setAgencia(Ljava/lang/String;)V
    .registers 2
    .param p1, "agencia"    # Ljava/lang/String;

    .line 32
    iput-object p1, p0, Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaInputVO;->agencia:Ljava/lang/String;

    .line 33
    return-void
.end method

.method public setApelido(Ljava/lang/String;)V
    .registers 2
    .param p1, "apelido"    # Ljava/lang/String;

    .line 40
    iput-object p1, p0, Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaInputVO;->apelido:Ljava/lang/String;

    .line 41
    return-void
.end method

.method public setConta(Ljava/lang/String;)V
    .registers 2
    .param p1, "conta"    # Ljava/lang/String;

    .line 48
    iput-object p1, p0, Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaInputVO;->conta:Ljava/lang/String;

    .line 49
    return-void
.end method

.method public setDdd(Ljava/lang/String;)V
    .registers 3
    .param p1, "ddd"    # Ljava/lang/String;

    .line 56
    invoke-static {p1}, Lcom/itau/empresas/ui/util/ViewUtils;->removaPaddingsZero(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaInputVO;->ddd:Ljava/lang/String;

    .line 57
    return-void
.end method

.method public setDigitoVerificadorConta(Ljava/lang/String;)V
    .registers 2
    .param p1, "digitoVerificadorConta"    # Ljava/lang/String;

    .line 60
    iput-object p1, p0, Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaInputVO;->digitoVerificadorConta:Ljava/lang/String;

    .line 61
    return-void
.end method

.method public setNomeOperadora(Ljava/lang/String;)V
    .registers 2
    .param p1, "nomeOperadora"    # Ljava/lang/String;

    .line 68
    iput-object p1, p0, Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaInputVO;->nomeOperadora:Ljava/lang/String;

    .line 69
    return-void
.end method

.method public setRegiao(Ljava/lang/String;)V
    .registers 2
    .param p1, "regiao"    # Ljava/lang/String;

    .line 72
    iput-object p1, p0, Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaInputVO;->regiao:Ljava/lang/String;

    .line 73
    return-void
.end method

.method public setTelefone(Ljava/lang/String;)V
    .registers 3
    .param p1, "telefone"    # Ljava/lang/String;

    .line 80
    invoke-static {p1}, Lcom/itau/empresas/ui/util/ViewUtils;->removaPaddingsZero(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaInputVO;->telefone:Ljava/lang/String;

    .line 81
    return-void
.end method

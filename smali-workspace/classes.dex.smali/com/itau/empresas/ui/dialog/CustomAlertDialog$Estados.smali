.class public final enum Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Estados;
.super Ljava/lang/Enum;
.source "CustomAlertDialog.java"

# interfaces
.implements Lcom/itau/empresas/ui/util/MaquinaDeEstado$Chave;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/itau/empresas/ui/dialog/CustomAlertDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Estados"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Estados;>;Lcom/itau/empresas/ui/util/MaquinaDeEstado$Chave;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Estados;

.field public static final enum DOIS_BOTOES:Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Estados;

.field public static final enum UM_BOTAO:Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Estados;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .line 170
    new-instance v0, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Estados;

    const-string v1, "UM_BOTAO"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Estados;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Estados;->UM_BOTAO:Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Estados;

    new-instance v0, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Estados;

    const-string v1, "DOIS_BOTOES"

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Estados;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Estados;->DOIS_BOTOES:Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Estados;

    .line 169
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Estados;

    sget-object v1, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Estados;->UM_BOTAO:Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Estados;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sget-object v1, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Estados;->DOIS_BOTOES:Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Estados;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    sput-object v0, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Estados;->$VALUES:[Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Estados;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 169
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Estados;
    .registers 2
    .param p0, "name"    # Ljava/lang/String;

    .line 169
    const-class v0, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Estados;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Estados;

    return-object v0
.end method

.method public static values()[Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Estados;
    .registers 1

    .line 169
    sget-object v0, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Estados;->$VALUES:[Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Estados;

    invoke-virtual {v0}, [Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Estados;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Estados;

    return-object v0
.end method

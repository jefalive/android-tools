.class Lcom/facebook/internal/UrlRedirectCache;
.super Ljava/lang/Object;
.source "UrlRedirectCache.java"


# static fields
.field private static final REDIRECT_CONTENT_TAG:Ljava/lang/String;

.field static final TAG:Ljava/lang/String;

.field private static volatile urlRedirectCache:Lcom/facebook/internal/FileLruCache;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .line 38
    const-class v0, Lcom/facebook/internal/UrlRedirectCache;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/internal/UrlRedirectCache;->TAG:Ljava/lang/String;

    .line 39
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/facebook/internal/UrlRedirectCache;->TAG:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_Redirect"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/internal/UrlRedirectCache;->REDIRECT_CONTENT_TAG:Ljava/lang/String;

    return-void
.end method

.method constructor <init>()V
    .registers 1

    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static cacheUriRedirect(Landroid/net/Uri;Landroid/net/Uri;)V
    .registers 7
    .param p0, "fromUri"    # Landroid/net/Uri;
    .param p1, "toUri"    # Landroid/net/Uri;

    .line 90
    if-eqz p0, :cond_4

    if-nez p1, :cond_5

    .line 91
    :cond_4
    return-void

    .line 94
    :cond_5
    const/4 v2, 0x0

    .line 96
    .local v2, "redirectStream":Ljava/io/OutputStream;
    :try_start_6
    invoke-static {}, Lcom/facebook/internal/UrlRedirectCache;->getCache()Lcom/facebook/internal/FileLruCache;

    move-result-object v3

    .line 97
    .local v3, "cache":Lcom/facebook/internal/FileLruCache;
    invoke-virtual {p0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/facebook/internal/UrlRedirectCache;->REDIRECT_CONTENT_TAG:Ljava/lang/String;

    invoke-virtual {v3, v0, v1}, Lcom/facebook/internal/FileLruCache;->openPutStream(Ljava/lang/String;Ljava/lang/String;)Ljava/io/OutputStream;

    move-result-object v0

    move-object v2, v0

    .line 98
    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/io/OutputStream;->write([B)V
    :try_end_20
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_20} :catch_24
    .catchall {:try_start_6 .. :try_end_20} :catchall_29

    .line 102
    .end local v3    # "cache":Lcom/facebook/internal/FileLruCache;
    invoke-static {v2}, Lcom/facebook/internal/Utility;->closeQuietly(Ljava/io/Closeable;)V

    .line 103
    goto :goto_2e

    .line 99
    :catch_24
    move-exception v3

    .line 102
    invoke-static {v2}, Lcom/facebook/internal/Utility;->closeQuietly(Ljava/io/Closeable;)V

    .line 103
    goto :goto_2e

    .line 102
    :catchall_29
    move-exception v4

    invoke-static {v2}, Lcom/facebook/internal/Utility;->closeQuietly(Ljava/io/Closeable;)V

    throw v4

    .line 104
    :goto_2e
    return-void
.end method

.method static declared-synchronized getCache()Lcom/facebook/internal/FileLruCache;
    .registers 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const-class v3, Lcom/facebook/internal/UrlRedirectCache;

    monitor-enter v3

    .line 44
    :try_start_3
    sget-object v0, Lcom/facebook/internal/UrlRedirectCache;->urlRedirectCache:Lcom/facebook/internal/FileLruCache;

    if-nez v0, :cond_15

    .line 45
    new-instance v0, Lcom/facebook/internal/FileLruCache;

    sget-object v1, Lcom/facebook/internal/UrlRedirectCache;->TAG:Ljava/lang/String;

    new-instance v2, Lcom/facebook/internal/FileLruCache$Limits;

    invoke-direct {v2}, Lcom/facebook/internal/FileLruCache$Limits;-><init>()V

    invoke-direct {v0, v1, v2}, Lcom/facebook/internal/FileLruCache;-><init>(Ljava/lang/String;Lcom/facebook/internal/FileLruCache$Limits;)V

    sput-object v0, Lcom/facebook/internal/UrlRedirectCache;->urlRedirectCache:Lcom/facebook/internal/FileLruCache;

    .line 47
    :cond_15
    sget-object v0, Lcom/facebook/internal/UrlRedirectCache;->urlRedirectCache:Lcom/facebook/internal/FileLruCache;
    :try_end_17
    .catchall {:try_start_3 .. :try_end_17} :catchall_19

    monitor-exit v3

    return-object v0

    :catchall_19
    move-exception v4

    monitor-exit v3

    throw v4
.end method

.method static getRedirectedUri(Landroid/net/Uri;)Landroid/net/Uri;
    .registers 12
    .param p0, "uri"    # Landroid/net/Uri;

    .line 51
    if-nez p0, :cond_4

    .line 52
    const/4 v0, 0x0

    return-object v0

    .line 55
    :cond_4
    invoke-virtual {p0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    .line 56
    .local v2, "uriString":Ljava/lang/String;
    const/4 v3, 0x0

    .line 59
    .local v3, "reader":Ljava/io/InputStreamReader;
    :try_start_9
    invoke-static {}, Lcom/facebook/internal/UrlRedirectCache;->getCache()Lcom/facebook/internal/FileLruCache;

    move-result-object v5

    .line 60
    .local v5, "cache":Lcom/facebook/internal/FileLruCache;
    const/4 v6, 0x0

    .line 61
    .local v6, "redirectExists":Z
    :goto_e
    sget-object v0, Lcom/facebook/internal/UrlRedirectCache;->REDIRECT_CONTENT_TAG:Ljava/lang/String;

    invoke-virtual {v5, v2, v0}, Lcom/facebook/internal/FileLruCache;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v4

    .local v4, "stream":Ljava/io/InputStream;
    if-eqz v4, :cond_3c

    .line 62
    const/4 v6, 0x1

    .line 65
    new-instance v0, Ljava/io/InputStreamReader;

    invoke-direct {v0, v4}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    move-object v3, v0

    .line 66
    const/16 v0, 0x80

    new-array v7, v0, [C

    .line 68
    .local v7, "buffer":[C
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    .line 69
    .local v9, "urlBuilder":Ljava/lang/StringBuilder;
    :goto_26
    array-length v0, v7

    const/4 v1, 0x0

    invoke-virtual {v3, v7, v1, v0}, Ljava/io/InputStreamReader;->read([CII)I

    move-result v0

    move v8, v0

    .local v8, "bufferLength":I
    if-lez v0, :cond_34

    .line 70
    const/4 v0, 0x0

    invoke-virtual {v9, v7, v0, v8}, Ljava/lang/StringBuilder;->append([CII)Ljava/lang/StringBuilder;

    goto :goto_26

    .line 72
    :cond_34
    invoke-static {v3}, Lcom/facebook/internal/Utility;->closeQuietly(Ljava/io/Closeable;)V

    .line 75
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 76
    .end local v7    # "buffer":[C
    .end local v8    # "bufferLength":I
    .end local v9    # "urlBuilder":Ljava/lang/StringBuilder;
    goto :goto_e

    .line 78
    :cond_3c
    if-eqz v6, :cond_46

    .line 79
    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;
    :try_end_41
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_41} :catch_4a
    .catchall {:try_start_9 .. :try_end_41} :catchall_4f

    move-result-object v7

    .line 83
    invoke-static {v3}, Lcom/facebook/internal/Utility;->closeQuietly(Ljava/io/Closeable;)V

    return-object v7

    .end local v4    # "stream":Ljava/io/InputStream;
    .end local v5    # "cache":Lcom/facebook/internal/FileLruCache;
    .end local v6    # "redirectExists":Z
    :cond_46
    invoke-static {v3}, Lcom/facebook/internal/Utility;->closeQuietly(Ljava/io/Closeable;)V

    .line 84
    goto :goto_54

    .line 81
    :catch_4a
    move-exception v4

    .line 83
    invoke-static {v3}, Lcom/facebook/internal/Utility;->closeQuietly(Ljava/io/Closeable;)V

    .line 84
    goto :goto_54

    .line 83
    :catchall_4f
    move-exception v10

    invoke-static {v3}, Lcom/facebook/internal/Utility;->closeQuietly(Ljava/io/Closeable;)V

    throw v10

    .line 86
    :goto_54
    const/4 v0, 0x0

    return-object v0
.end method

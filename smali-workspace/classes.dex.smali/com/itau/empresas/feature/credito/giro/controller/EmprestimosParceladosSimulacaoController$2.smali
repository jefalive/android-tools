.class Lcom/itau/empresas/feature/credito/giro/controller/EmprestimosParceladosSimulacaoController$2;
.super Ljava/lang/Object;
.source "EmprestimosParceladosSimulacaoController.java"

# interfaces
.implements Lbr/com/itau/sdk/android/core/type/BackendCall;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/itau/empresas/feature/credito/giro/controller/EmprestimosParceladosSimulacaoController;->contrataEmprestimoSimulado(Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoSaidaVO;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/itau/empresas/feature/credito/giro/controller/EmprestimosParceladosSimulacaoController;

.field final synthetic val$simulacaoAtualizada:Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoSaidaVO;


# direct methods
.method constructor <init>(Lcom/itau/empresas/feature/credito/giro/controller/EmprestimosParceladosSimulacaoController;Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoSaidaVO;)V
    .registers 3
    .param p1, "this$0"    # Lcom/itau/empresas/feature/credito/giro/controller/EmprestimosParceladosSimulacaoController;

    .line 49
    iput-object p1, p0, Lcom/itau/empresas/feature/credito/giro/controller/EmprestimosParceladosSimulacaoController$2;->this$0:Lcom/itau/empresas/feature/credito/giro/controller/EmprestimosParceladosSimulacaoController;

    iput-object p2, p0, Lcom/itau/empresas/feature/credito/giro/controller/EmprestimosParceladosSimulacaoController$2;->val$simulacaoAtualizada:Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoSaidaVO;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public callBackend()V
    .registers 3

    .line 52
    invoke-static {}, Lcom/itau/empresas/feature/credito/giro/controller/EmprestimosParceladosSimulacaoController;->getApi()Lcom/itau/empresas/api/credito/ApiGiro;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/giro/controller/EmprestimosParceladosSimulacaoController$2;->val$simulacaoAtualizada:Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoSaidaVO;

    invoke-interface {v0, v1}, Lcom/itau/empresas/api/credito/ApiGiro;->contrataEmprestimosParcelados(Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoSaidaVO;)Lcom/itau/empresas/feature/credito/giro/model/ResultadoEfetivacaoEmprestimosParceladosVO;

    move-result-object v0

    # invokes: Lcom/itau/empresas/feature/credito/giro/controller/EmprestimosParceladosSimulacaoController;->post(Ljava/lang/Object;)V
    invoke-static {v0}, Lcom/itau/empresas/feature/credito/giro/controller/EmprestimosParceladosSimulacaoController;->access$100(Ljava/lang/Object;)V

    .line 53
    return-void
.end method

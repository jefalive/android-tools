.class Lcom/google/android/gms/tagmanager/zzcp$3;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/tagmanager/zzcp$zza;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/gms/tagmanager/zzcp;->zza(Ljava/lang/String;Ljava/util/Set;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;Ljava/util/Set;Lcom/google/android/gms/tagmanager/zzco;)Lcom/google/android/gms/tagmanager/zzbw;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic zzbkC:Lcom/google/android/gms/tagmanager/zzcp;

.field final synthetic zzbkD:Ljava/util/Map;

.field final synthetic zzbkE:Ljava/util/Map;

.field final synthetic zzbkF:Ljava/util/Map;

.field final synthetic zzbkG:Ljava/util/Map;


# direct methods
.method constructor <init>(Lcom/google/android/gms/tagmanager/zzcp;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;)V
    .registers 6

    iput-object p1, p0, Lcom/google/android/gms/tagmanager/zzcp$3;->zzbkC:Lcom/google/android/gms/tagmanager/zzcp;

    iput-object p2, p0, Lcom/google/android/gms/tagmanager/zzcp$3;->zzbkD:Ljava/util/Map;

    iput-object p3, p0, Lcom/google/android/gms/tagmanager/zzcp$3;->zzbkE:Ljava/util/Map;

    iput-object p4, p0, Lcom/google/android/gms/tagmanager/zzcp$3;->zzbkF:Ljava/util/Map;

    iput-object p5, p0, Lcom/google/android/gms/tagmanager/zzcp$3;->zzbkG:Ljava/util/Map;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public zza(Lcom/google/android/gms/internal/zzrs$zze;Ljava/util/Set;Ljava/util/Set;Lcom/google/android/gms/tagmanager/zzck;)V
    .registers 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Lcom/google/android/gms/internal/zzrs$zze;Ljava/util/Set<Lcom/google/android/gms/internal/zzrs$zza;>;Ljava/util/Set<Lcom/google/android/gms/internal/zzrs$zza;>;Lcom/google/android/gms/tagmanager/zzck;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/gms/tagmanager/zzcp$3;->zzbkD:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Ljava/util/List;

    iget-object v0, p0, Lcom/google/android/gms/tagmanager/zzcp$3;->zzbkE:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Ljava/util/List;

    if-eqz v1, :cond_1e

    invoke-interface {p2, v1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    invoke-interface {p4}, Lcom/google/android/gms/tagmanager/zzck;->zzGJ()Lcom/google/android/gms/tagmanager/zzci;

    move-result-object v0

    invoke-interface {v0, v1, v2}, Lcom/google/android/gms/tagmanager/zzci;->zzc(Ljava/util/List;Ljava/util/List;)V

    :cond_1e
    iget-object v0, p0, Lcom/google/android/gms/tagmanager/zzcp$3;->zzbkF:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Ljava/util/List;

    iget-object v0, p0, Lcom/google/android/gms/tagmanager/zzcp$3;->zzbkG:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Ljava/util/List;

    if-eqz v3, :cond_3c

    invoke-interface {p3, v3}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    invoke-interface {p4}, Lcom/google/android/gms/tagmanager/zzck;->zzGK()Lcom/google/android/gms/tagmanager/zzci;

    move-result-object v0

    invoke-interface {v0, v3, v4}, Lcom/google/android/gms/tagmanager/zzci;->zzc(Ljava/util/List;Ljava/util/List;)V

    :cond_3c
    return-void
.end method

.class public Lcom/itau/empresas/ui/util/menu/PedagioTransferenciaCommand;
.super Ljava/lang/Object;
.source "PedagioTransferenciaCommand.java"

# interfaces
.implements Lcom/itau/empresas/ui/util/menu/Command;


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public executar(Landroid/support/v7/app/AppCompatActivity;Lcom/itau/empresas/api/model/MenuVO;)V
    .registers 11
    .param p1, "activity"    # Landroid/support/v7/app/AppCompatActivity;
    .param p2, "menuVO"    # Lcom/itau/empresas/api/model/MenuVO;

    .line 16
    invoke-virtual {p1}, Landroid/support/v7/app/AppCompatActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/itau/empresas/CustomApplication;

    .line 18
    .local v2, "app":Lcom/itau/empresas/CustomApplication;
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 19
    .local v3, "labels":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 21
    .local v4, "chaves":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    sget-object v0, Lcom/itau/empresas/CustomApplication$Exibicao;->EXIBICAO_PEDAGIO_TRANSFERENCIA_TEF:Lcom/itau/empresas/CustomApplication$Exibicao;

    invoke-virtual {v2, v0}, Lcom/itau/empresas/CustomApplication;->getExibicao(Lcom/itau/empresas/CustomApplication$Exibicao;)Lcom/itau/empresas/api/model/MenuVO;

    move-result-object v5

    .line 22
    .local v5, "tef":Lcom/itau/empresas/api/model/MenuVO;
    if-eqz v5, :cond_25

    .line 23
    const-string v0, "transfer\u00eancias entre contas Ita\u00fa"

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 24
    invoke-virtual {v5}, Lcom/itau/empresas/api/model/MenuVO;->getChaveMobile()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 27
    :cond_25
    sget-object v0, Lcom/itau/empresas/CustomApplication$Exibicao;->EXIBICAO_PEDAGIO_TRANSFERENCIA_DOC:Lcom/itau/empresas/CustomApplication$Exibicao;

    invoke-virtual {v2, v0}, Lcom/itau/empresas/CustomApplication;->getExibicao(Lcom/itau/empresas/CustomApplication$Exibicao;)Lcom/itau/empresas/api/model/MenuVO;

    move-result-object v6

    .line 28
    .local v6, "doc":Lcom/itau/empresas/api/model/MenuVO;
    if-eqz v6, :cond_39

    .line 29
    const-string v0, "DOC"

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 30
    invoke-virtual {v6}, Lcom/itau/empresas/api/model/MenuVO;->getChaveMobile()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 33
    :cond_39
    sget-object v0, Lcom/itau/empresas/CustomApplication$Exibicao;->EXIBICAO_PEDAGIO_TRANSFERENCIA_TED:Lcom/itau/empresas/CustomApplication$Exibicao;

    invoke-virtual {v2, v0}, Lcom/itau/empresas/CustomApplication;->getExibicao(Lcom/itau/empresas/CustomApplication$Exibicao;)Lcom/itau/empresas/api/model/MenuVO;

    move-result-object v7

    .line 34
    .local v7, "ted":Lcom/itau/empresas/api/model/MenuVO;
    if-eqz v7, :cond_4d

    .line 35
    const-string v0, "TED"

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 36
    invoke-virtual {v7}, Lcom/itau/empresas/api/model/MenuVO;->getChaveMobile()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 39
    :cond_4d
    invoke-static {}, Lcom/itau/empresas/ui/dialog/PedagioWebViewDialog_;->builder()Lcom/itau/empresas/ui/dialog/PedagioWebViewDialog_$FragmentBuilder_;

    move-result-object v0

    .line 41
    const v1, 0x7f0702e3

    invoke-virtual {p1, v1}, Landroid/support/v7/app/AppCompatActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 40
    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/dialog/PedagioWebViewDialog_$FragmentBuilder_;->categoriaAnalytics(Ljava/lang/String;)Lcom/itau/empresas/ui/dialog/PedagioWebViewDialog_$FragmentBuilder_;

    move-result-object v0

    .line 42
    invoke-virtual {v0, v3}, Lcom/itau/empresas/ui/dialog/PedagioWebViewDialog_$FragmentBuilder_;->labels(Ljava/util/ArrayList;)Lcom/itau/empresas/ui/dialog/PedagioWebViewDialog_$FragmentBuilder_;

    move-result-object v0

    .line 43
    invoke-virtual {v0, v4}, Lcom/itau/empresas/ui/dialog/PedagioWebViewDialog_$FragmentBuilder_;->chaves(Ljava/util/ArrayList;)Lcom/itau/empresas/ui/dialog/PedagioWebViewDialog_$FragmentBuilder_;

    move-result-object v0

    .line 44
    invoke-virtual {v0}, Lcom/itau/empresas/ui/dialog/PedagioWebViewDialog_$FragmentBuilder_;->build()Lcom/itau/empresas/ui/dialog/PedagioWebViewDialog;

    move-result-object v0

    .line 45
    invoke-virtual {v0, p1}, Lcom/itau/empresas/ui/dialog/PedagioWebViewDialog;->show(Landroid/support/v7/app/AppCompatActivity;)Lcom/itau/empresas/ui/dialog/BaseDialog;

    .line 47
    return-void
.end method

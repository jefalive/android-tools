.class Lcom/itau/empresas/adapter/areausuario/AreaUsuarioConfiguracoesAdapter$ViewHolderLista;
.super Landroid/support/v7/widget/RecyclerView$ViewHolder;
.source "AreaUsuarioConfiguracoesAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/itau/empresas/adapter/areausuario/AreaUsuarioConfiguracoesAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "ViewHolderLista"
.end annotation


# instance fields
.field icone:Lcom/itau/empresas/ui/view/TextViewIcon;

.field titulo:Landroid/widget/TextView;


# direct methods
.method private constructor <init>(Landroid/view/View;Lcom/itau/empresas/adapter/areausuario/AreaUsuarioConfiguracoesAdapter$OnConfiguracoesItemSelected;)V
    .registers 4
    .param p1, "itemView"    # Landroid/view/View;
    .param p2, "listener"    # Lcom/itau/empresas/adapter/areausuario/AreaUsuarioConfiguracoesAdapter$OnConfiguracoesItemSelected;

    .line 182
    invoke-direct {p0, p1}, Landroid/support/v7/widget/RecyclerView$ViewHolder;-><init>(Landroid/view/View;)V

    .line 184
    const v0, 0x7f0e052f

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/ui/view/TextViewIcon;

    iput-object v0, p0, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioConfiguracoesAdapter$ViewHolderLista;->icone:Lcom/itau/empresas/ui/view/TextViewIcon;

    .line 185
    const v0, 0x7f0e0530

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioConfiguracoesAdapter$ViewHolderLista;->titulo:Landroid/widget/TextView;

    .line 187
    new-instance v0, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioConfiguracoesAdapter$ViewHolderLista$1;

    invoke-direct {v0, p0, p2}, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioConfiguracoesAdapter$ViewHolderLista$1;-><init>(Lcom/itau/empresas/adapter/areausuario/AreaUsuarioConfiguracoesAdapter$ViewHolderLista;Lcom/itau/empresas/adapter/areausuario/AreaUsuarioConfiguracoesAdapter$OnConfiguracoesItemSelected;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 195
    return-void
.end method

.method synthetic constructor <init>(Landroid/view/View;Lcom/itau/empresas/adapter/areausuario/AreaUsuarioConfiguracoesAdapter$OnConfiguracoesItemSelected;Lcom/itau/empresas/adapter/areausuario/AreaUsuarioConfiguracoesAdapter$1;)V
    .registers 4
    .param p1, "x0"    # Landroid/view/View;
    .param p2, "x1"    # Lcom/itau/empresas/adapter/areausuario/AreaUsuarioConfiguracoesAdapter$OnConfiguracoesItemSelected;
    .param p3, "x2"    # Lcom/itau/empresas/adapter/areausuario/AreaUsuarioConfiguracoesAdapter$1;

    .line 177
    invoke-direct {p0, p1, p2}, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioConfiguracoesAdapter$ViewHolderLista;-><init>(Landroid/view/View;Lcom/itau/empresas/adapter/areausuario/AreaUsuarioConfiguracoesAdapter$OnConfiguracoesItemSelected;)V

    return-void
.end method

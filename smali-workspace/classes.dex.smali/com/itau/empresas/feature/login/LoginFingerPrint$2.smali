.class Lcom/itau/empresas/feature/login/LoginFingerPrint$2;
.super Ljava/lang/Object;
.source "LoginFingerPrint.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/itau/empresas/feature/login/LoginFingerPrint;->loginComFingerprint()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/itau/empresas/feature/login/LoginFingerPrint;


# direct methods
.method constructor <init>(Lcom/itau/empresas/feature/login/LoginFingerPrint;)V
    .registers 2
    .param p1, "this$0"    # Lcom/itau/empresas/feature/login/LoginFingerPrint;

    .line 94
    iput-object p1, p0, Lcom/itau/empresas/feature/login/LoginFingerPrint$2;->this$0:Lcom/itau/empresas/feature/login/LoginFingerPrint;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .registers 5

    .line 99
    :try_start_0
    iget-object v0, p0, Lcom/itau/empresas/feature/login/LoginFingerPrint$2;->this$0:Lcom/itau/empresas/feature/login/LoginFingerPrint;

    # getter for: Lcom/itau/empresas/feature/login/LoginFingerPrint;->loginActivity:Lcom/itau/empresas/feature/login/LoginActivity;
    invoke-static {v0}, Lcom/itau/empresas/feature/login/LoginFingerPrint;->access$100(Lcom/itau/empresas/feature/login/LoginFingerPrint;)Lcom/itau/empresas/feature/login/LoginActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/feature/login/LoginActivity;->chamaAPIAutenticacaoFingerPrint()V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_9} :catch_a

    .line 107
    goto :goto_32

    .line 100
    :catch_a
    move-exception v3

    .line 101
    .local v3, "e":Ljava/lang/Exception;
    const-string v0, "LoginFingerPrint"

    iget-object v1, p0, Lcom/itau/empresas/feature/login/LoginFingerPrint$2;->this$0:Lcom/itau/empresas/feature/login/LoginFingerPrint;

    .line 102
    # getter for: Lcom/itau/empresas/feature/login/LoginFingerPrint;->application:Lcom/itau/empresas/CustomApplication;
    invoke-static {v1}, Lcom/itau/empresas/feature/login/LoginFingerPrint;->access$200(Lcom/itau/empresas/feature/login/LoginFingerPrint;)Lcom/itau/empresas/CustomApplication;

    move-result-object v1

    const v2, 0x7f070447

    invoke-virtual {v1, v2}, Lcom/itau/empresas/CustomApplication;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 101
    invoke-static {v0, v1, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 104
    invoke-static {v3}, Lcom/crashlytics/android/Crashlytics;->logException(Ljava/lang/Throwable;)V

    .line 105
    iget-object v0, p0, Lcom/itau/empresas/feature/login/LoginFingerPrint$2;->this$0:Lcom/itau/empresas/feature/login/LoginFingerPrint;

    iget-object v1, p0, Lcom/itau/empresas/feature/login/LoginFingerPrint$2;->this$0:Lcom/itau/empresas/feature/login/LoginFingerPrint;

    .line 106
    # getter for: Lcom/itau/empresas/feature/login/LoginFingerPrint;->application:Lcom/itau/empresas/CustomApplication;
    invoke-static {v1}, Lcom/itau/empresas/feature/login/LoginFingerPrint;->access$200(Lcom/itau/empresas/feature/login/LoginFingerPrint;)Lcom/itau/empresas/CustomApplication;

    move-result-object v1

    const v2, 0x7f070444

    invoke-virtual {v1, v2}, Lcom/itau/empresas/CustomApplication;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 105
    # invokes: Lcom/itau/empresas/feature/login/LoginFingerPrint;->exibeMensagemErroFingerPrint(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/itau/empresas/feature/login/LoginFingerPrint;->access$300(Lcom/itau/empresas/feature/login/LoginFingerPrint;Ljava/lang/String;)V

    .line 108
    .end local v3    # "e":Ljava/lang/Exception;
    :goto_32
    return-void
.end method

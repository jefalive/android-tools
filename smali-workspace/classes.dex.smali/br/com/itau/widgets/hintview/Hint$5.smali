.class Lbr/com/itau/widgets/hintview/Hint$5;
.super Ljava/lang/Object;
.source "Hint.java"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lbr/com/itau/widgets/hintview/Hint;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lbr/com/itau/widgets/hintview/Hint;


# direct methods
.method constructor <init>(Lbr/com/itau/widgets/hintview/Hint;)V
    .registers 2
    .param p1, "this$0"    # Lbr/com/itau/widgets/hintview/Hint;

    .line 89
    iput-object p1, p0, Lbr/com/itau/widgets/hintview/Hint$5;->this$0:Lbr/com/itau/widgets/hintview/Hint;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onGlobalLayout()V
    .registers 10

    .line 92
    iget-object v0, p0, Lbr/com/itau/widgets/hintview/Hint$5;->this$0:Lbr/com/itau/widgets/hintview/Hint;

    # getter for: Lbr/com/itau/widgets/hintview/Hint;->mContentView:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lbr/com/itau/widgets/hintview/Hint;->access$400(Lbr/com/itau/widgets/hintview/Hint;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-static {v0, p0}, Lbr/com/itau/widgets/hintview/Util;->removeOnGlobalLayoutListener(Landroid/view/View;Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 94
    iget-object v0, p0, Lbr/com/itau/widgets/hintview/Hint$5;->this$0:Lbr/com/itau/widgets/hintview/Hint;

    # getter for: Lbr/com/itau/widgets/hintview/Hint;->mAnchorView:Landroid/view/View;
    invoke-static {v0}, Lbr/com/itau/widgets/hintview/Hint;->access$500(Lbr/com/itau/widgets/hintview/Hint;)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lbr/com/itau/widgets/hintview/Util;->calculateRectOnScreen(Landroid/view/View;)Landroid/graphics/RectF;

    move-result-object v3

    .line 95
    .local v3, "anchorRect":Landroid/graphics/RectF;
    iget-object v0, p0, Lbr/com/itau/widgets/hintview/Hint$5;->this$0:Lbr/com/itau/widgets/hintview/Hint;

    # getter for: Lbr/com/itau/widgets/hintview/Hint;->mContentView:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lbr/com/itau/widgets/hintview/Hint;->access$400(Lbr/com/itau/widgets/hintview/Hint;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-static {v0}, Lbr/com/itau/widgets/hintview/Util;->calculateRectOnScreen(Landroid/view/View;)Landroid/graphics/RectF;

    move-result-object v4

    .line 97
    .local v4, "contentViewRect":Landroid/graphics/RectF;
    iget-object v0, p0, Lbr/com/itau/widgets/hintview/Hint$5;->this$0:Lbr/com/itau/widgets/hintview/Hint;

    # getter for: Lbr/com/itau/widgets/hintview/Hint;->mGravity:I
    invoke-static {v0}, Lbr/com/itau/widgets/hintview/Hint;->access$600(Lbr/com/itau/widgets/hintview/Hint;)I

    move-result v0

    const/16 v1, 0x50

    if-eq v0, v1, :cond_31

    iget-object v0, p0, Lbr/com/itau/widgets/hintview/Hint$5;->this$0:Lbr/com/itau/widgets/hintview/Hint;

    # getter for: Lbr/com/itau/widgets/hintview/Hint;->mGravity:I
    invoke-static {v0}, Lbr/com/itau/widgets/hintview/Hint;->access$600(Lbr/com/itau/widgets/hintview/Hint;)I

    move-result v0

    const/16 v1, 0x30

    if-ne v0, v1, :cond_bd

    .line 98
    :cond_31
    iget-object v0, p0, Lbr/com/itau/widgets/hintview/Hint$5;->this$0:Lbr/com/itau/widgets/hintview/Hint;

    # getter for: Lbr/com/itau/widgets/hintview/Hint;->mContentView:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lbr/com/itau/widgets/hintview/Hint;->access$400(Lbr/com/itau/widgets/hintview/Hint;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getPaddingLeft()I

    move-result v0

    int-to-float v0, v0

    const/high16 v1, 0x40000000    # 2.0f

    invoke-static {v1}, Lbr/com/itau/widgets/hintview/Util;->dpToPx(F)F

    move-result v1

    add-float v5, v0, v1

    .line 99
    .local v5, "x":F
    invoke-virtual {v4}, Landroid/graphics/RectF;->width()F

    move-result v0

    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr v0, v1

    iget-object v1, p0, Lbr/com/itau/widgets/hintview/Hint$5;->this$0:Lbr/com/itau/widgets/hintview/Hint;

    # getter for: Lbr/com/itau/widgets/hintview/Hint;->mArrowView:Landroid/widget/ImageView;
    invoke-static {v1}, Lbr/com/itau/widgets/hintview/Hint;->access$700(Lbr/com/itau/widgets/hintview/Hint;)Landroid/widget/ImageView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/ImageView;->getWidth()I

    move-result v1

    int-to-float v1, v1

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    sub-float v7, v0, v1

    .line 100
    .local v7, "centerX":F
    invoke-virtual {v4}, Landroid/graphics/RectF;->centerX()F

    move-result v0

    invoke-virtual {v3}, Landroid/graphics/RectF;->centerX()F

    move-result v1

    sub-float/2addr v0, v1

    sub-float v8, v7, v0

    .line 101
    .local v8, "newX":F
    cmpl-float v0, v8, v5

    if-lez v0, :cond_a1

    .line 102
    iget-object v0, p0, Lbr/com/itau/widgets/hintview/Hint$5;->this$0:Lbr/com/itau/widgets/hintview/Hint;

    # getter for: Lbr/com/itau/widgets/hintview/Hint;->mArrowView:Landroid/widget/ImageView;
    invoke-static {v0}, Lbr/com/itau/widgets/hintview/Hint;->access$700(Lbr/com/itau/widgets/hintview/Hint;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ImageView;->getWidth()I

    move-result v0

    int-to-float v0, v0

    add-float/2addr v0, v8

    add-float/2addr v0, v5

    invoke-virtual {v4}, Landroid/graphics/RectF;->width()F

    move-result v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_99

    .line 103
    invoke-virtual {v4}, Landroid/graphics/RectF;->width()F

    move-result v0

    iget-object v1, p0, Lbr/com/itau/widgets/hintview/Hint$5;->this$0:Lbr/com/itau/widgets/hintview/Hint;

    # getter for: Lbr/com/itau/widgets/hintview/Hint;->mArrowView:Landroid/widget/ImageView;
    invoke-static {v1}, Lbr/com/itau/widgets/hintview/Hint;->access$700(Lbr/com/itau/widgets/hintview/Hint;)Landroid/widget/ImageView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/ImageView;->getWidth()I

    move-result v1

    int-to-float v1, v1

    sub-float/2addr v0, v1

    sub-float/2addr v0, v5

    iget-object v1, p0, Lbr/com/itau/widgets/hintview/Hint$5;->this$0:Lbr/com/itau/widgets/hintview/Hint;

    # getter for: Lbr/com/itau/widgets/hintview/Hint;->mArrowMargin:F
    invoke-static {v1}, Lbr/com/itau/widgets/hintview/Hint;->access$800(Lbr/com/itau/widgets/hintview/Hint;)F

    move-result v1

    sub-float v5, v0, v1

    goto :goto_a1

    .line 105
    :cond_99
    iget-object v0, p0, Lbr/com/itau/widgets/hintview/Hint$5;->this$0:Lbr/com/itau/widgets/hintview/Hint;

    # getter for: Lbr/com/itau/widgets/hintview/Hint;->mArrowMargin:F
    invoke-static {v0}, Lbr/com/itau/widgets/hintview/Hint;->access$800(Lbr/com/itau/widgets/hintview/Hint;)F

    move-result v0

    sub-float v5, v8, v0

    .line 108
    :cond_a1
    :goto_a1
    iget-object v0, p0, Lbr/com/itau/widgets/hintview/Hint$5;->this$0:Lbr/com/itau/widgets/hintview/Hint;

    # getter for: Lbr/com/itau/widgets/hintview/Hint;->mArrowView:Landroid/widget/ImageView;
    invoke-static {v0}, Lbr/com/itau/widgets/hintview/Hint;->access$700(Lbr/com/itau/widgets/hintview/Hint;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ImageView;->getTop()I

    move-result v0

    int-to-float v6, v0

    .line 109
    .local v6, "y":F
    iget-object v0, p0, Lbr/com/itau/widgets/hintview/Hint$5;->this$0:Lbr/com/itau/widgets/hintview/Hint;

    # getter for: Lbr/com/itau/widgets/hintview/Hint;->mGravity:I
    invoke-static {v0}, Lbr/com/itau/widgets/hintview/Hint;->access$600(Lbr/com/itau/widgets/hintview/Hint;)I

    move-result v0

    const/16 v1, 0x30

    if-ne v0, v1, :cond_b8

    const/4 v0, -0x1

    goto :goto_b9

    :cond_b8
    const/4 v0, 0x1

    :goto_b9
    int-to-float v0, v0

    add-float/2addr v6, v0

    .line 111
    .end local v7    # "centerX":F
    .end local v8    # "newX":F
    goto/16 :goto_16a

    .end local v5    # "x":F
    .end local v6    # "y":F
    :cond_bd
    iget-object v0, p0, Lbr/com/itau/widgets/hintview/Hint$5;->this$0:Lbr/com/itau/widgets/hintview/Hint;

    # getter for: Lbr/com/itau/widgets/hintview/Hint;->mGravity:I
    invoke-static {v0}, Lbr/com/itau/widgets/hintview/Hint;->access$600(Lbr/com/itau/widgets/hintview/Hint;)I

    move-result v0

    const/4 v1, 0x7

    if-ne v0, v1, :cond_ed

    .line 113
    iget-object v0, p0, Lbr/com/itau/widgets/hintview/Hint$5;->this$0:Lbr/com/itau/widgets/hintview/Hint;

    # getter for: Lbr/com/itau/widgets/hintview/Hint;->mContentView:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lbr/com/itau/widgets/hintview/Hint;->access$400(Lbr/com/itau/widgets/hintview/Hint;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getPaddingLeft()I

    move-result v0

    int-to-float v0, v0

    const/high16 v1, 0x40000000    # 2.0f

    invoke-static {v1}, Lbr/com/itau/widgets/hintview/Util;->dpToPx(F)F

    move-result v1

    add-float v5, v0, v1

    .line 115
    .local v5, "x":F
    const/high16 v0, 0x41400000    # 12.0f

    invoke-static {v0}, Lbr/com/itau/widgets/hintview/Util;->dpToPx(F)F

    move-result v0

    add-float/2addr v5, v0

    .line 116
    iget-object v0, p0, Lbr/com/itau/widgets/hintview/Hint$5;->this$0:Lbr/com/itau/widgets/hintview/Hint;

    # getter for: Lbr/com/itau/widgets/hintview/Hint;->mArrowView:Landroid/widget/ImageView;
    invoke-static {v0}, Lbr/com/itau/widgets/hintview/Hint;->access$700(Lbr/com/itau/widgets/hintview/Hint;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ImageView;->getTop()I

    move-result v0

    int-to-float v6, v0

    .local v6, "y":F
    goto/16 :goto_16a

    .line 119
    .end local v5    # "x":F
    .end local v6    # "y":F
    :cond_ed
    iget-object v0, p0, Lbr/com/itau/widgets/hintview/Hint$5;->this$0:Lbr/com/itau/widgets/hintview/Hint;

    # getter for: Lbr/com/itau/widgets/hintview/Hint;->mContentView:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lbr/com/itau/widgets/hintview/Hint;->access$400(Lbr/com/itau/widgets/hintview/Hint;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getPaddingTop()I

    move-result v0

    int-to-float v0, v0

    const/high16 v1, 0x40000000    # 2.0f

    invoke-static {v1}, Lbr/com/itau/widgets/hintview/Util;->dpToPx(F)F

    move-result v1

    add-float v6, v0, v1

    .line 120
    .local v6, "y":F
    invoke-virtual {v4}, Landroid/graphics/RectF;->height()F

    move-result v0

    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr v0, v1

    iget-object v1, p0, Lbr/com/itau/widgets/hintview/Hint$5;->this$0:Lbr/com/itau/widgets/hintview/Hint;

    # getter for: Lbr/com/itau/widgets/hintview/Hint;->mArrowView:Landroid/widget/ImageView;
    invoke-static {v1}, Lbr/com/itau/widgets/hintview/Hint;->access$700(Lbr/com/itau/widgets/hintview/Hint;)Landroid/widget/ImageView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/ImageView;->getHeight()I

    move-result v1

    int-to-float v1, v1

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    sub-float v7, v0, v1

    .line 121
    .local v7, "centerY":F
    invoke-virtual {v4}, Landroid/graphics/RectF;->centerY()F

    move-result v0

    invoke-virtual {v3}, Landroid/graphics/RectF;->centerY()F

    move-result v1

    sub-float/2addr v0, v1

    sub-float v8, v7, v0

    .line 122
    .local v8, "newY":F
    cmpl-float v0, v8, v6

    if-lez v0, :cond_14f

    .line 123
    iget-object v0, p0, Lbr/com/itau/widgets/hintview/Hint$5;->this$0:Lbr/com/itau/widgets/hintview/Hint;

    # getter for: Lbr/com/itau/widgets/hintview/Hint;->mArrowView:Landroid/widget/ImageView;
    invoke-static {v0}, Lbr/com/itau/widgets/hintview/Hint;->access$700(Lbr/com/itau/widgets/hintview/Hint;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ImageView;->getHeight()I

    move-result v0

    int-to-float v0, v0

    add-float/2addr v0, v8

    add-float/2addr v0, v6

    invoke-virtual {v4}, Landroid/graphics/RectF;->height()F

    move-result v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_14e

    .line 124
    invoke-virtual {v4}, Landroid/graphics/RectF;->height()F

    move-result v0

    iget-object v1, p0, Lbr/com/itau/widgets/hintview/Hint$5;->this$0:Lbr/com/itau/widgets/hintview/Hint;

    # getter for: Lbr/com/itau/widgets/hintview/Hint;->mArrowView:Landroid/widget/ImageView;
    invoke-static {v1}, Lbr/com/itau/widgets/hintview/Hint;->access$700(Lbr/com/itau/widgets/hintview/Hint;)Landroid/widget/ImageView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/ImageView;->getHeight()I

    move-result v1

    int-to-float v1, v1

    sub-float/2addr v0, v1

    sub-float v6, v0, v6

    goto :goto_14f

    .line 126
    :cond_14e
    move v6, v8

    .line 129
    :cond_14f
    :goto_14f
    iget-object v0, p0, Lbr/com/itau/widgets/hintview/Hint$5;->this$0:Lbr/com/itau/widgets/hintview/Hint;

    # getter for: Lbr/com/itau/widgets/hintview/Hint;->mArrowView:Landroid/widget/ImageView;
    invoke-static {v0}, Lbr/com/itau/widgets/hintview/Hint;->access$700(Lbr/com/itau/widgets/hintview/Hint;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ImageView;->getLeft()I

    move-result v0

    int-to-float v5, v0

    .line 130
    .local v5, "x":F
    iget-object v0, p0, Lbr/com/itau/widgets/hintview/Hint$5;->this$0:Lbr/com/itau/widgets/hintview/Hint;

    # getter for: Lbr/com/itau/widgets/hintview/Hint;->mGravity:I
    invoke-static {v0}, Lbr/com/itau/widgets/hintview/Hint;->access$600(Lbr/com/itau/widgets/hintview/Hint;)I

    move-result v0

    const v1, 0x800003

    if-ne v0, v1, :cond_167

    const/4 v0, -0x1

    goto :goto_168

    :cond_167
    const/4 v0, 0x1

    :goto_168
    int-to-float v0, v0

    add-float/2addr v5, v0

    .line 132
    .end local v7    # "centerY":F
    .end local v8    # "newY":F
    :goto_16a
    iget-object v0, p0, Lbr/com/itau/widgets/hintview/Hint$5;->this$0:Lbr/com/itau/widgets/hintview/Hint;

    # getter for: Lbr/com/itau/widgets/hintview/Hint;->mArrowView:Landroid/widget/ImageView;
    invoke-static {v0}, Lbr/com/itau/widgets/hintview/Hint;->access$700(Lbr/com/itau/widgets/hintview/Hint;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setX(F)V

    .line 133
    iget-object v0, p0, Lbr/com/itau/widgets/hintview/Hint$5;->this$0:Lbr/com/itau/widgets/hintview/Hint;

    # getter for: Lbr/com/itau/widgets/hintview/Hint;->mArrowView:Landroid/widget/ImageView;
    invoke-static {v0}, Lbr/com/itau/widgets/hintview/Hint;->access$700(Lbr/com/itau/widgets/hintview/Hint;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setY(F)V

    .line 134
    return-void
.end method

.class public Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ColorTemplate;
.super Ljava/lang/Object;
.source "ColorTemplate.java"


# static fields
.field public static final COLORFUL_COLORS:[I

.field public static final COLOR_NONE:I = -0x1

.field public static final COLOR_SKIP:I = -0x2

.field public static final JOYFUL_COLORS:[I

.field public static final LIBERTY_COLORS:[I

.field public static final PASTEL_COLORS:[I

.field public static final VORDIPLOM_COLORS:[I


# direct methods
.method static constructor <clinit>()V
    .registers 4

    .line 38
    const/4 v0, 0x5

    new-array v0, v0, [I

    .line 39
    const/16 v1, 0xcf

    const/16 v2, 0xf8

    const/16 v3, 0xf6

    invoke-static {v1, v2, v3}, Landroid/graphics/Color;->rgb(III)I

    move-result v1

    const/4 v2, 0x0

    aput v1, v0, v2

    const/16 v1, 0x94

    const/16 v2, 0xd4

    const/16 v3, 0xd4

    invoke-static {v1, v2, v3}, Landroid/graphics/Color;->rgb(III)I

    move-result v1

    const/4 v2, 0x1

    aput v1, v0, v2

    const/16 v1, 0x88

    const/16 v2, 0xb4

    const/16 v3, 0xbb

    invoke-static {v1, v2, v3}, Landroid/graphics/Color;->rgb(III)I

    move-result v1

    const/4 v2, 0x2

    aput v1, v0, v2

    .line 40
    const/16 v1, 0x76

    const/16 v2, 0xae

    const/16 v3, 0xaf

    invoke-static {v1, v2, v3}, Landroid/graphics/Color;->rgb(III)I

    move-result v1

    const/4 v2, 0x3

    aput v1, v0, v2

    const/16 v1, 0x2a

    const/16 v2, 0x6d

    const/16 v3, 0x82

    invoke-static {v1, v2, v3}, Landroid/graphics/Color;->rgb(III)I

    move-result v1

    const/4 v2, 0x4

    aput v1, v0, v2

    sput-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ColorTemplate;->LIBERTY_COLORS:[I

    .line 42
    const/4 v0, 0x5

    new-array v0, v0, [I

    .line 43
    const/16 v1, 0xd9

    const/16 v2, 0x50

    const/16 v3, 0x8a

    invoke-static {v1, v2, v3}, Landroid/graphics/Color;->rgb(III)I

    move-result v1

    const/4 v2, 0x0

    aput v1, v0, v2

    const/16 v1, 0xfe

    const/16 v2, 0x95

    const/4 v3, 0x7

    invoke-static {v1, v2, v3}, Landroid/graphics/Color;->rgb(III)I

    move-result v1

    const/4 v2, 0x1

    aput v1, v0, v2

    const/16 v1, 0xfe

    const/16 v2, 0xf7

    const/16 v3, 0x78

    invoke-static {v1, v2, v3}, Landroid/graphics/Color;->rgb(III)I

    move-result v1

    const/4 v2, 0x2

    aput v1, v0, v2

    .line 44
    const/16 v1, 0x6a

    const/16 v2, 0xa7

    const/16 v3, 0x86

    invoke-static {v1, v2, v3}, Landroid/graphics/Color;->rgb(III)I

    move-result v1

    const/4 v2, 0x3

    aput v1, v0, v2

    const/16 v1, 0x35

    const/16 v2, 0xc2

    const/16 v3, 0xd1

    invoke-static {v1, v2, v3}, Landroid/graphics/Color;->rgb(III)I

    move-result v1

    const/4 v2, 0x4

    aput v1, v0, v2

    sput-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ColorTemplate;->JOYFUL_COLORS:[I

    .line 46
    const/4 v0, 0x5

    new-array v0, v0, [I

    .line 47
    const/16 v1, 0x40

    const/16 v2, 0x59

    const/16 v3, 0x80

    invoke-static {v1, v2, v3}, Landroid/graphics/Color;->rgb(III)I

    move-result v1

    const/4 v2, 0x0

    aput v1, v0, v2

    const/16 v1, 0x95

    const/16 v2, 0xa5

    const/16 v3, 0x7c

    invoke-static {v1, v2, v3}, Landroid/graphics/Color;->rgb(III)I

    move-result v1

    const/4 v2, 0x1

    aput v1, v0, v2

    const/16 v1, 0xd9

    const/16 v2, 0xb8

    const/16 v3, 0xa2

    invoke-static {v1, v2, v3}, Landroid/graphics/Color;->rgb(III)I

    move-result v1

    const/4 v2, 0x2

    aput v1, v0, v2

    .line 48
    const/16 v1, 0xbf

    const/16 v2, 0x86

    const/16 v3, 0x86

    invoke-static {v1, v2, v3}, Landroid/graphics/Color;->rgb(III)I

    move-result v1

    const/4 v2, 0x3

    aput v1, v0, v2

    const/16 v1, 0xb3

    const/16 v2, 0x30

    const/16 v3, 0x50

    invoke-static {v1, v2, v3}, Landroid/graphics/Color;->rgb(III)I

    move-result v1

    const/4 v2, 0x4

    aput v1, v0, v2

    sput-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ColorTemplate;->PASTEL_COLORS:[I

    .line 50
    const/4 v0, 0x5

    new-array v0, v0, [I

    .line 51
    const/16 v1, 0xc1

    const/16 v2, 0x25

    const/16 v3, 0x52

    invoke-static {v1, v2, v3}, Landroid/graphics/Color;->rgb(III)I

    move-result v1

    const/4 v2, 0x0

    aput v1, v0, v2

    const/16 v1, 0xff

    const/16 v2, 0x66

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Landroid/graphics/Color;->rgb(III)I

    move-result v1

    const/4 v2, 0x1

    aput v1, v0, v2

    const/16 v1, 0xf5

    const/16 v2, 0xc7

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Landroid/graphics/Color;->rgb(III)I

    move-result v1

    const/4 v2, 0x2

    aput v1, v0, v2

    .line 52
    const/16 v1, 0x6a

    const/16 v2, 0x96

    const/16 v3, 0x1f

    invoke-static {v1, v2, v3}, Landroid/graphics/Color;->rgb(III)I

    move-result v1

    const/4 v2, 0x3

    aput v1, v0, v2

    const/16 v1, 0xb3

    const/16 v2, 0x64

    const/16 v3, 0x35

    invoke-static {v1, v2, v3}, Landroid/graphics/Color;->rgb(III)I

    move-result v1

    const/4 v2, 0x4

    aput v1, v0, v2

    sput-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ColorTemplate;->COLORFUL_COLORS:[I

    .line 54
    const/4 v0, 0x5

    new-array v0, v0, [I

    .line 55
    const/16 v1, 0xc0

    const/16 v2, 0xff

    const/16 v3, 0x8c

    invoke-static {v1, v2, v3}, Landroid/graphics/Color;->rgb(III)I

    move-result v1

    const/4 v2, 0x0

    aput v1, v0, v2

    const/16 v1, 0xff

    const/16 v2, 0xf7

    const/16 v3, 0x8c

    invoke-static {v1, v2, v3}, Landroid/graphics/Color;->rgb(III)I

    move-result v1

    const/4 v2, 0x1

    aput v1, v0, v2

    const/16 v1, 0xff

    const/16 v2, 0xd0

    const/16 v3, 0x8c

    invoke-static {v1, v2, v3}, Landroid/graphics/Color;->rgb(III)I

    move-result v1

    const/4 v2, 0x2

    aput v1, v0, v2

    .line 56
    const/16 v1, 0x8c

    const/16 v2, 0xea

    const/16 v3, 0xff

    invoke-static {v1, v2, v3}, Landroid/graphics/Color;->rgb(III)I

    move-result v1

    const/4 v2, 0x3

    aput v1, v0, v2

    const/16 v1, 0xff

    const/16 v2, 0x8c

    const/16 v3, 0x9d

    invoke-static {v1, v2, v3}, Landroid/graphics/Color;->rgb(III)I

    move-result v1

    const/4 v2, 0x4

    aput v1, v0, v2

    sput-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ColorTemplate;->VORDIPLOM_COLORS:[I

    .line 54
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static createColors(Landroid/content/res/Resources;[I)Ljava/util/List;
    .registers 8
    .param p0, "r"    # Landroid/content/res/Resources;
    .param p1, "colors"    # [I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Landroid/content/res/Resources;[I)Ljava/util/List<Ljava/lang/Integer;>;"
        }
    .end annotation

    .line 81
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 84
    .local v1, "result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    move-object v2, p1

    array-length v3, v2

    const/4 v4, 0x0

    :goto_8
    if-ge v4, v3, :cond_1a

    aget v5, v2, v4

    .line 85
    .local v5, "i":I
    invoke-virtual {p0, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 84
    .end local v5    # "i":I
    add-int/lit8 v4, v4, 0x1

    goto :goto_8

    .line 89
    :cond_1a
    return-object v1
.end method

.method public static createColors([I)Ljava/util/List;
    .registers 7
    .param p0, "colors"    # [I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([I)Ljava/util/List<Ljava/lang/Integer;>;"
        }
    .end annotation

    .line 103
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 106
    .local v1, "result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    move-object v2, p0

    array-length v3, v2

    const/4 v4, 0x0

    :goto_8
    if-ge v4, v3, :cond_16

    aget v5, v2, v4

    .line 107
    .local v5, "i":I
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 106
    .end local v5    # "i":I
    add-int/lit8 v4, v4, 0x1

    goto :goto_8

    .line 111
    :cond_16
    return-object v1
.end method

.method public static getHoloBlue()I
    .registers 3

    .line 66
    const/16 v0, 0x33

    const/16 v1, 0xb5

    const/16 v2, 0xe5

    invoke-static {v0, v1, v2}, Landroid/graphics/Color;->rgb(III)I

    move-result v0

    return v0
.end method

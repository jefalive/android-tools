.class Lcom/itau/empresas/feature/itoken/ITokenActivity$3;
.super Ljava/lang/Object;
.source "ITokenActivity.java"

# interfaces
.implements Lcom/itau/empresas/ui/util/maquinaestado/Estado$OnChangeState;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/itau/empresas/feature/itoken/ITokenActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/itau/empresas/feature/itoken/ITokenActivity;


# direct methods
.method constructor <init>(Lcom/itau/empresas/feature/itoken/ITokenActivity;)V
    .registers 2
    .param p1, "this$0"    # Lcom/itau/empresas/feature/itoken/ITokenActivity;

    .line 97
    iput-object p1, p0, Lcom/itau/empresas/feature/itoken/ITokenActivity$3;->this$0:Lcom/itau/empresas/feature/itoken/ITokenActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onChangeState()V
    .registers 4

    .line 100
    iget-object v0, p0, Lcom/itau/empresas/feature/itoken/ITokenActivity$3;->this$0:Lcom/itau/empresas/feature/itoken/ITokenActivity;

    iget-object v0, v0, Lcom/itau/empresas/feature/itoken/ITokenActivity;->textoConfirmacao:Landroid/widget/TextView;

    const v1, 0x7f07006e

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 101
    iget-object v0, p0, Lcom/itau/empresas/feature/itoken/ITokenActivity$3;->this$0:Lcom/itau/empresas/feature/itoken/ITokenActivity;

    iget-object v0, v0, Lcom/itau/empresas/feature/itoken/ITokenActivity;->llMensagemConfirmacao:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 102
    iget-object v0, p0, Lcom/itau/empresas/feature/itoken/ITokenActivity$3;->this$0:Lcom/itau/empresas/feature/itoken/ITokenActivity;

    iget-object v1, p0, Lcom/itau/empresas/feature/itoken/ITokenActivity$3;->this$0:Lcom/itau/empresas/feature/itoken/ITokenActivity;

    iget-object v1, v1, Lcom/itau/empresas/feature/itoken/ITokenActivity;->iTokenInstalacaoFragment:Lcom/itau/empresas/feature/itoken/fragment/ITokenInstalacaoFragment;

    # invokes: Lcom/itau/empresas/feature/itoken/ITokenActivity;->abreFragment(Lcom/itau/empresas/ui/fragment/BaseFragment;)V
    invoke-static {v0, v1}, Lcom/itau/empresas/feature/itoken/ITokenActivity;->access$000(Lcom/itau/empresas/feature/itoken/ITokenActivity;Lcom/itau/empresas/ui/fragment/BaseFragment;)V

    .line 103
    iget-object v0, p0, Lcom/itau/empresas/feature/itoken/ITokenActivity$3;->this$0:Lcom/itau/empresas/feature/itoken/ITokenActivity;

    iget-object v1, p0, Lcom/itau/empresas/feature/itoken/ITokenActivity$3;->this$0:Lcom/itau/empresas/feature/itoken/ITokenActivity;

    const v2, 0x7f0700f4

    invoke-virtual {v1, v2}, Lcom/itau/empresas/feature/itoken/ITokenActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    # invokes: Lcom/itau/empresas/feature/itoken/ITokenActivity;->disparaEventoAnalytics(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/itau/empresas/feature/itoken/ITokenActivity;->access$100(Lcom/itau/empresas/feature/itoken/ITokenActivity;Ljava/lang/String;)V

    .line 104
    return-void
.end method

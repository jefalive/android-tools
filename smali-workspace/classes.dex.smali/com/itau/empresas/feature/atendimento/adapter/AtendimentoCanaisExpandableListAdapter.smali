.class public Lcom/itau/empresas/feature/atendimento/adapter/AtendimentoCanaisExpandableListAdapter;
.super Landroid/widget/BaseExpandableListAdapter;
.source "AtendimentoCanaisExpandableListAdapter.java"


# instance fields
.field private context:Landroid/content/Context;

.field private listaCanais:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Lcom/itau/empresas/feature/atendimento/model/AtendimentoCanal;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/List;)V
    .registers 3
    .param p1, "mContext"    # Landroid/content/Context;
    .param p2, "listaCanais"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Landroid/content/Context;Ljava/util/List<Lcom/itau/empresas/feature/atendimento/model/AtendimentoCanal;>;)V"
        }
    .end annotation

    .line 22
    invoke-direct {p0}, Landroid/widget/BaseExpandableListAdapter;-><init>()V

    .line 23
    iput-object p2, p0, Lcom/itau/empresas/feature/atendimento/adapter/AtendimentoCanaisExpandableListAdapter;->listaCanais:Ljava/util/List;

    .line 24
    iput-object p1, p0, Lcom/itau/empresas/feature/atendimento/adapter/AtendimentoCanaisExpandableListAdapter;->context:Landroid/content/Context;

    .line 25
    return-void
.end method


# virtual methods
.method public getChild(II)Lcom/itau/empresas/feature/atendimento/model/AtendimentoCanal;
    .registers 4
    .param p1, "groupPosition"    # I
    .param p2, "childPosititon"    # I

    .line 29
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/adapter/AtendimentoCanaisExpandableListAdapter;->listaCanais:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/feature/atendimento/model/AtendimentoCanal;

    return-object v0
.end method

.method public bridge synthetic getChild(II)Ljava/lang/Object;
    .registers 4

    .line 16
    invoke-virtual {p0, p1, p2}, Lcom/itau/empresas/feature/atendimento/adapter/AtendimentoCanaisExpandableListAdapter;->getChild(II)Lcom/itau/empresas/feature/atendimento/model/AtendimentoCanal;

    move-result-object v0

    return-object v0
.end method

.method public getChildId(II)J
    .registers 5
    .param p1, "groupPosition"    # I
    .param p2, "childPosition"    # I

    .line 34
    int-to-long v0, p2

    return-wide v0
.end method

.method public getChildView(IIZLandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 11
    .param p1, "groupPosition"    # I
    .param p2, "childPosition"    # I
    .param p3, "isLastChild"    # Z
    .param p4, "view"    # Landroid/view/View;
    .param p5, "parent"    # Landroid/view/ViewGroup;

    .line 41
    move-object v2, p4

    .line 43
    .local v2, "convertView":Landroid/view/View;
    invoke-virtual {p0, p1, p2}, Lcom/itau/empresas/feature/atendimento/adapter/AtendimentoCanaisExpandableListAdapter;->getChild(II)Lcom/itau/empresas/feature/atendimento/model/AtendimentoCanal;

    move-result-object v3

    .line 44
    .local v3, "subItems":Lcom/itau/empresas/feature/atendimento/model/AtendimentoCanal;
    if-nez v2, :cond_1a

    .line 45
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/adapter/AtendimentoCanaisExpandableListAdapter;->context:Landroid/content/Context;

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Landroid/view/LayoutInflater;

    .line 47
    .local v4, "inflater":Landroid/view/LayoutInflater;
    const v0, 0x7f0300dc

    const/4 v1, 0x0

    invoke-virtual {v4, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 50
    .end local v4    # "inflater":Landroid/view/LayoutInflater;
    :cond_1a
    const v0, 0x7f0e04ed

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Landroid/widget/TextView;

    .line 52
    .local v4, "textoAtendimentoCanalDescricao":Landroid/widget/TextView;
    invoke-virtual {v3}, Lcom/itau/empresas/feature/atendimento/model/AtendimentoCanal;->getTextoExplicativo()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 54
    return-object v2
.end method

.method public getChildrenCount(I)I
    .registers 3
    .param p1, "groupPosition"    # I

    .line 59
    const/4 v0, 0x1

    return v0
.end method

.method public getGroup(I)Ljava/lang/Object;
    .registers 3
    .param p1, "groupPosition"    # I

    .line 64
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/adapter/AtendimentoCanaisExpandableListAdapter;->listaCanais:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getGroupCount()I
    .registers 2

    .line 69
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/adapter/AtendimentoCanaisExpandableListAdapter;->listaCanais:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getGroupId(I)J
    .registers 4
    .param p1, "groupPosition"    # I

    .line 74
    int-to-long v0, p1

    return-wide v0
.end method

.method public getGroupView(IZLandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 11
    .param p1, "groupPosition"    # I
    .param p2, "isExpanded"    # Z
    .param p3, "view"    # Landroid/view/View;
    .param p4, "parent"    # Landroid/view/ViewGroup;

    .line 81
    move-object v3, p3

    .line 83
    .local v3, "convertView":Landroid/view/View;
    if-nez v3, :cond_16

    .line 84
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/adapter/AtendimentoCanaisExpandableListAdapter;->context:Landroid/content/Context;

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Landroid/view/LayoutInflater;

    .line 86
    .local v4, "infalInflater":Landroid/view/LayoutInflater;
    const v0, 0x7f0300dd

    const/4 v1, 0x0

    invoke-virtual {v4, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 89
    .end local v4    # "infalInflater":Landroid/view/LayoutInflater;
    :cond_16
    const v0, 0x7f0e04ee

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Landroid/widget/TextView;

    .line 91
    .local v4, "textoAtendimentoCanal":Landroid/widget/TextView;
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/adapter/AtendimentoCanaisExpandableListAdapter;->listaCanais:Ljava/util/List;

    .line 92
    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/feature/atendimento/model/AtendimentoCanal;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/atendimento/model/AtendimentoCanal;->getCanal()Ljava/lang/String;

    move-result-object v0

    const-string v1, "_"

    const-string v2, " "

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 94
    .line 95
    const v0, 0x7f0e04ef

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/itau/empresas/ui/view/TextViewIcon;

    .line 96
    .local v5, "imgSeta":Lcom/itau/empresas/ui/view/TextViewIcon;
    if-nez p2, :cond_51

    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/adapter/AtendimentoCanaisExpandableListAdapter;->context:Landroid/content/Context;

    .line 97
    const v1, 0x7f070286

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    goto :goto_5e

    :cond_51
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/adapter/AtendimentoCanaisExpandableListAdapter;->context:Landroid/content/Context;

    .line 98
    const v1, 0x7f070288

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    .line 96
    :goto_5e
    invoke-virtual {v5, v0}, Lcom/itau/empresas/ui/view/TextViewIcon;->setText(Ljava/lang/CharSequence;)V

    .line 100
    if-nez p2, :cond_66

    const-string v0, "Fechar"

    goto :goto_68

    :cond_66
    const-string v0, "Abrir"

    :goto_68
    invoke-virtual {v5, v0}, Lcom/itau/empresas/ui/view/TextViewIcon;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 102
    return-object v3
.end method

.method public hasStableIds()Z
    .registers 2

    .line 107
    const/4 v0, 0x0

    return v0
.end method

.method public isChildSelectable(II)Z
    .registers 4
    .param p1, "groupPosition"    # I
    .param p2, "childPosition"    # I

    .line 112
    const/4 v0, 0x1

    return v0
.end method

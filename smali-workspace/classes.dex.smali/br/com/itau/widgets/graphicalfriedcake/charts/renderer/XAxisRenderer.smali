.class public Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/XAxisRenderer;
.super Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/AxisRenderer;
.source "XAxisRenderer.java"


# instance fields
.field protected mXAxis:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis;


# direct methods
.method public constructor <init>(Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis;Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Transformer;)V
    .registers 6
    .param p1, "viewPortHandler"    # Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;
    .param p2, "xAxis"    # Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis;
    .param p3, "trans"    # Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Transformer;

    .line 25
    invoke-direct {p0, p1, p3}, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/AxisRenderer;-><init>(Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Transformer;)V

    .line 28
    iput-object p2, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/XAxisRenderer;->mXAxis:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis;

    .line 31
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/XAxisRenderer;->mAxisLabelPaint:Landroid/graphics/Paint;

    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 32
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/XAxisRenderer;->mAxisLabelPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 33
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/XAxisRenderer;->mAxisLabelPaint:Landroid/graphics/Paint;

    const/high16 v1, 0x41200000    # 10.0f

    invoke-static {v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Utils;->convertDpToPixel(F)F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 34
    return-void
.end method


# virtual methods
.method public computeAxis(FLjava/util/List;)V
    .registers 9
    .param p1, "xValAverageLength"    # F
    .param p2, "xValues"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(FLjava/util/List<Ljava/lang/String;>;)V"
        }
    .end annotation

    .line 40
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/XAxisRenderer;->mAxisLabelPaint:Landroid/graphics/Paint;

    iget-object v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/XAxisRenderer;->mXAxis:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis;

    invoke-virtual {v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 41
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/XAxisRenderer;->mAxisLabelPaint:Landroid/graphics/Paint;

    iget-object v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/XAxisRenderer;->mXAxis:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis;

    invoke-virtual {v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis;->getTextSize()F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 44
    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    .line 47
    .local v3, "a":Ljava/lang/StringBuffer;
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/XAxisRenderer;->mXAxis:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis;

    .line 48
    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis;->getSpaceBetweenLabels()I

    move-result v0

    int-to-float v0, v0

    add-float/2addr v0, p1

    .line 47
    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v4

    .line 51
    .local v4, "max":I
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_28
    if-ge v5, v4, :cond_32

    .line 52
    const-string v0, "h"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 51
    add-int/lit8 v5, v5, 0x1

    goto :goto_28

    .line 56
    .end local v5    # "i":I
    :cond_32
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/XAxisRenderer;->mXAxis:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis;

    iget-object v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/XAxisRenderer;->mAxisLabelPaint:Landroid/graphics/Paint;

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Utils;->calcTextWidth(Landroid/graphics/Paint;Ljava/lang/String;)I

    move-result v1

    iput v1, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis;->mLabelWidth:I

    .line 57
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/XAxisRenderer;->mXAxis:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis;

    iget-object v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/XAxisRenderer;->mAxisLabelPaint:Landroid/graphics/Paint;

    const-string v2, "Q"

    invoke-static {v1, v2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Utils;->calcTextHeight(Landroid/graphics/Paint;Ljava/lang/String;)I

    move-result v1

    iput v1, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis;->mLabelHeight:I

    .line 58
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/XAxisRenderer;->mXAxis:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis;

    invoke-virtual {v0, p2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis;->setValues(Ljava/util/List;)V

    .line 59
    return-void
.end method

.method protected drawLabels(Landroid/graphics/Canvas;F)V
    .registers 9
    .param p1, "c"    # Landroid/graphics/Canvas;
    .param p2, "pos"    # F

    .line 151
    const/4 v0, 0x2

    new-array v2, v0, [F

    fill-array-data v2, :array_a2

    .line 156
    .local v2, "position":[F
    iget v3, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/XAxisRenderer;->mMinX:I

    .local v3, "i":I
    :goto_8
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/XAxisRenderer;->mMaxX:I

    if-gt v3, v0, :cond_a1

    .line 159
    int-to-float v0, v3

    const/4 v1, 0x0

    aput v0, v2, v1

    .line 162
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/XAxisRenderer;->mTrans:Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Transformer;

    invoke-virtual {v0, v2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Transformer;->pointValuesToPixel([F)V

    .line 165
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/XAxisRenderer;->mViewPortHandler:Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;

    const/4 v1, 0x0

    aget v1, v2, v1

    invoke-virtual {v0, v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->isInBoundsX(F)Z

    move-result v0

    if-eqz v0, :cond_9a

    .line 168
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/XAxisRenderer;->mXAxis:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis;->getValues()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Ljava/lang/String;

    .line 171
    .local v4, "label":Ljava/lang/String;
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/XAxisRenderer;->mXAxis:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis;->isAvoidFirstLastClippingEnabled()Z

    move-result v0

    if-eqz v0, :cond_92

    .line 175
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/XAxisRenderer;->mXAxis:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis;->getValues()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ne v3, v0, :cond_7e

    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/XAxisRenderer;->mXAxis:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis;->getValues()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_7e

    .line 176
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/XAxisRenderer;->mAxisLabelPaint:Landroid/graphics/Paint;

    invoke-static {v0, v4}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Utils;->calcTextWidth(Landroid/graphics/Paint;Ljava/lang/String;)I

    move-result v0

    int-to-float v5, v0

    .line 179
    .local v5, "width":F
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/XAxisRenderer;->mViewPortHandler:Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->offsetRight()F

    move-result v0

    const/high16 v1, 0x40000000    # 2.0f

    mul-float/2addr v0, v1

    cmpl-float v0, v5, v0

    if-lez v0, :cond_7d

    const/4 v0, 0x0

    aget v0, v2, v0

    add-float/2addr v0, v5

    iget-object v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/XAxisRenderer;->mViewPortHandler:Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;

    .line 180
    invoke-virtual {v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->getChartWidth()F

    move-result v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_7d

    .line 181
    const/4 v0, 0x0

    aget v0, v2, v0

    const/high16 v1, 0x40000000    # 2.0f

    div-float v1, v5, v1

    sub-float/2addr v0, v1

    const/4 v1, 0x0

    aput v0, v2, v1

    .line 185
    .end local v5    # "width":F
    :cond_7d
    goto :goto_92

    :cond_7e
    if-nez v3, :cond_92

    .line 188
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/XAxisRenderer;->mAxisLabelPaint:Landroid/graphics/Paint;

    invoke-static {v0, v4}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Utils;->calcTextWidth(Landroid/graphics/Paint;Ljava/lang/String;)I

    move-result v0

    int-to-float v5, v0

    .line 189
    .local v5, "width":F
    const/4 v0, 0x0

    aget v0, v2, v0

    const/high16 v1, 0x40000000    # 2.0f

    div-float v1, v5, v1

    add-float/2addr v0, v1

    const/4 v1, 0x0

    aput v0, v2, v1

    .line 194
    .end local v5    # "width":F
    :cond_92
    :goto_92
    const/4 v0, 0x0

    aget v0, v2, v0

    iget-object v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/XAxisRenderer;->mAxisLabelPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v4, v0, p2, v1}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 156
    .end local v4    # "label":Ljava/lang/String;
    :cond_9a
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/XAxisRenderer;->mXAxis:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis;

    iget v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis;->mAxisLabelModulus:I

    add-int/2addr v3, v0

    goto/16 :goto_8

    .line 199
    .end local v3    # "i":I
    :cond_a1
    return-void

    :array_a2
    .array-data 4
        0x0
        0x0
    .end array-data
.end method

.method public renderAxisLabels(Landroid/graphics/Canvas;)V
    .registers 5
    .param p1, "c"    # Landroid/graphics/Canvas;

    .line 66
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/XAxisRenderer;->mXAxis:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_10

    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/XAxisRenderer;->mXAxis:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis;->isDrawLabelsEnabled()Z

    move-result v0

    if-nez v0, :cond_11

    .line 67
    :cond_10
    return-void

    .line 70
    :cond_11
    const/high16 v0, 0x40800000    # 4.0f

    invoke-static {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Utils;->convertDpToPixel(F)F

    move-result v2

    .line 73
    .local v2, "yoffset":F
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/XAxisRenderer;->mAxisLabelPaint:Landroid/graphics/Paint;

    iget-object v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/XAxisRenderer;->mXAxis:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis;

    invoke-virtual {v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 74
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/XAxisRenderer;->mAxisLabelPaint:Landroid/graphics/Paint;

    iget-object v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/XAxisRenderer;->mXAxis:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis;

    invoke-virtual {v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis;->getTextSize()F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 75
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/XAxisRenderer;->mAxisLabelPaint:Landroid/graphics/Paint;

    iget-object v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/XAxisRenderer;->mXAxis:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis;

    invoke-virtual {v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis;->getTextColor()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 78
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/XAxisRenderer;->mXAxis:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis;->getPosition()Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis$XAxisPosition;

    move-result-object v0

    sget-object v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis$XAxisPosition;->TOP:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis$XAxisPosition;

    if-ne v0, v1, :cond_4e

    .line 81
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/XAxisRenderer;->mViewPortHandler:Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->offsetTop()F

    move-result v0

    sub-float/2addr v0, v2

    invoke-virtual {p0, p1, v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/XAxisRenderer;->drawLabels(Landroid/graphics/Canvas;F)V

    goto/16 :goto_ba

    .line 84
    :cond_4e
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/XAxisRenderer;->mXAxis:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis;->getPosition()Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis$XAxisPosition;

    move-result-object v0

    sget-object v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis$XAxisPosition;->BOTTOM:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis$XAxisPosition;

    if-ne v0, v1, :cond_6c

    .line 87
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/XAxisRenderer;->mViewPortHandler:Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->contentBottom()F

    move-result v0

    iget-object v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/XAxisRenderer;->mXAxis:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis;

    iget v1, v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis;->mLabelHeight:I

    int-to-float v1, v1

    add-float/2addr v0, v1

    const/high16 v1, 0x3fc00000    # 1.5f

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    invoke-virtual {p0, p1, v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/XAxisRenderer;->drawLabels(Landroid/graphics/Canvas;F)V

    goto :goto_ba

    .line 90
    :cond_6c
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/XAxisRenderer;->mXAxis:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis;->getPosition()Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis$XAxisPosition;

    move-result-object v0

    sget-object v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis$XAxisPosition;->BOTTOM_INSIDE:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis$XAxisPosition;

    if-ne v0, v1, :cond_81

    .line 93
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/XAxisRenderer;->mViewPortHandler:Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->contentBottom()F

    move-result v0

    sub-float/2addr v0, v2

    invoke-virtual {p0, p1, v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/XAxisRenderer;->drawLabels(Landroid/graphics/Canvas;F)V

    goto :goto_ba

    .line 96
    :cond_81
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/XAxisRenderer;->mXAxis:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis;->getPosition()Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis$XAxisPosition;

    move-result-object v0

    sget-object v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis$XAxisPosition;->TOP_INSIDE:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis$XAxisPosition;

    if-ne v0, v1, :cond_9c

    .line 99
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/XAxisRenderer;->mViewPortHandler:Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->offsetTop()F

    move-result v0

    add-float/2addr v0, v2

    iget-object v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/XAxisRenderer;->mXAxis:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis;

    iget v1, v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis;->mLabelHeight:I

    int-to-float v1, v1

    add-float/2addr v0, v1

    invoke-virtual {p0, p1, v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/XAxisRenderer;->drawLabels(Landroid/graphics/Canvas;F)V

    goto :goto_ba

    .line 105
    :cond_9c
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/XAxisRenderer;->mViewPortHandler:Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->offsetTop()F

    move-result v0

    sub-float/2addr v0, v2

    invoke-virtual {p0, p1, v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/XAxisRenderer;->drawLabels(Landroid/graphics/Canvas;F)V

    .line 106
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/XAxisRenderer;->mViewPortHandler:Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->contentBottom()F

    move-result v0

    iget-object v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/XAxisRenderer;->mXAxis:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis;

    iget v1, v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis;->mLabelHeight:I

    int-to-float v1, v1

    add-float/2addr v0, v1

    const v1, 0x3fcccccd    # 1.6f

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    invoke-virtual {p0, p1, v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/XAxisRenderer;->drawLabels(Landroid/graphics/Canvas;F)V

    .line 108
    :goto_ba
    return-void
.end method

.method public renderAxisLine(Landroid/graphics/Canvas;)V
    .registers 8
    .param p1, "c"    # Landroid/graphics/Canvas;

    .line 115
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/XAxisRenderer;->mXAxis:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis;->isDrawAxisLineEnabled()Z

    move-result v0

    if-eqz v0, :cond_10

    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/XAxisRenderer;->mXAxis:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_11

    .line 116
    :cond_10
    return-void

    .line 119
    :cond_11
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/XAxisRenderer;->mAxisLinePaint:Landroid/graphics/Paint;

    iget-object v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/XAxisRenderer;->mXAxis:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis;

    invoke-virtual {v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis;->getAxisLineColor()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 120
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/XAxisRenderer;->mAxisLinePaint:Landroid/graphics/Paint;

    iget-object v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/XAxisRenderer;->mXAxis:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis;

    invoke-virtual {v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis;->getAxisLineWidth()F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 123
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/XAxisRenderer;->mXAxis:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis;->getPosition()Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis$XAxisPosition;

    move-result-object v0

    sget-object v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis$XAxisPosition;->TOP:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis$XAxisPosition;

    if-eq v0, v1, :cond_45

    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/XAxisRenderer;->mXAxis:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis;

    .line 124
    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis;->getPosition()Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis$XAxisPosition;

    move-result-object v0

    sget-object v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis$XAxisPosition;->TOP_INSIDE:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis$XAxisPosition;

    if-eq v0, v1, :cond_45

    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/XAxisRenderer;->mXAxis:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis;

    .line 125
    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis;->getPosition()Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis$XAxisPosition;

    move-result-object v0

    sget-object v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis$XAxisPosition;->BOTH_SIDED:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis$XAxisPosition;

    if-ne v0, v1, :cond_63

    .line 126
    :cond_45
    move-object v0, p1

    iget-object v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/XAxisRenderer;->mViewPortHandler:Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;

    invoke-virtual {v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->contentLeft()F

    move-result v1

    iget-object v2, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/XAxisRenderer;->mViewPortHandler:Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;

    .line 127
    invoke-virtual {v2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->contentTop()F

    move-result v2

    iget-object v3, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/XAxisRenderer;->mViewPortHandler:Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;

    invoke-virtual {v3}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->contentRight()F

    move-result v3

    iget-object v4, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/XAxisRenderer;->mViewPortHandler:Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;

    .line 128
    invoke-virtual {v4}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->contentTop()F

    move-result v4

    iget-object v5, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/XAxisRenderer;->mAxisLinePaint:Landroid/graphics/Paint;

    .line 126
    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 132
    :cond_63
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/XAxisRenderer;->mXAxis:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis;->getPosition()Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis$XAxisPosition;

    move-result-object v0

    sget-object v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis$XAxisPosition;->BOTTOM:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis$XAxisPosition;

    if-eq v0, v1, :cond_81

    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/XAxisRenderer;->mXAxis:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis;

    .line 133
    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis;->getPosition()Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis$XAxisPosition;

    move-result-object v0

    sget-object v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis$XAxisPosition;->BOTTOM_INSIDE:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis$XAxisPosition;

    if-eq v0, v1, :cond_81

    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/XAxisRenderer;->mXAxis:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis;

    .line 134
    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis;->getPosition()Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis$XAxisPosition;

    move-result-object v0

    sget-object v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis$XAxisPosition;->BOTH_SIDED:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis$XAxisPosition;

    if-ne v0, v1, :cond_9f

    .line 135
    :cond_81
    move-object v0, p1

    iget-object v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/XAxisRenderer;->mViewPortHandler:Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;

    invoke-virtual {v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->contentLeft()F

    move-result v1

    iget-object v2, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/XAxisRenderer;->mViewPortHandler:Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;

    .line 136
    invoke-virtual {v2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->contentBottom()F

    move-result v2

    iget-object v3, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/XAxisRenderer;->mViewPortHandler:Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;

    invoke-virtual {v3}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->contentRight()F

    move-result v3

    iget-object v4, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/XAxisRenderer;->mViewPortHandler:Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;

    .line 137
    invoke-virtual {v4}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->contentBottom()F

    move-result v4

    iget-object v5, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/XAxisRenderer;->mAxisLinePaint:Landroid/graphics/Paint;

    .line 135
    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 139
    :cond_9f
    return-void
.end method

.method public renderGridLines(Landroid/graphics/Canvas;)V
    .registers 7
    .param p1, "c"    # Landroid/graphics/Canvas;

    .line 206
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/XAxisRenderer;->mXAxis:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis;->isDrawGridLinesEnabled()Z

    move-result v0

    if-eqz v0, :cond_10

    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/XAxisRenderer;->mXAxis:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_11

    .line 207
    :cond_10
    return-void

    .line 211
    :cond_11
    const/4 v0, 0x2

    new-array v2, v0, [F

    fill-array-data v2, :array_8e

    .line 216
    .local v2, "position":[F
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/XAxisRenderer;->mGridPaint:Landroid/graphics/Paint;

    iget-object v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/XAxisRenderer;->mXAxis:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis;

    invoke-virtual {v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis;->getGridColor()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 217
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/XAxisRenderer;->mGridPaint:Landroid/graphics/Paint;

    iget-object v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/XAxisRenderer;->mXAxis:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis;

    invoke-virtual {v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis;->getGridLineWidth()F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 218
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/XAxisRenderer;->mGridPaint:Landroid/graphics/Paint;

    iget-object v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/XAxisRenderer;->mXAxis:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis;

    invoke-virtual {v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis;->getGridDashPathEffect()Landroid/graphics/DashPathEffect;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 221
    new-instance v3, Landroid/graphics/Path;

    invoke-direct {v3}, Landroid/graphics/Path;-><init>()V

    .line 224
    .local v3, "gridLinePath":Landroid/graphics/Path;
    iget v4, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/XAxisRenderer;->mMinX:I

    .local v4, "i":I
    :goto_3f
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/XAxisRenderer;->mMaxX:I

    if-gt v4, v0, :cond_8c

    .line 227
    int-to-float v0, v4

    const/4 v1, 0x0

    aput v0, v2, v1

    .line 228
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/XAxisRenderer;->mTrans:Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Transformer;

    invoke-virtual {v0, v2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Transformer;->pointValuesToPixel([F)V

    .line 231
    const/4 v0, 0x0

    aget v0, v2, v0

    iget-object v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/XAxisRenderer;->mViewPortHandler:Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;

    invoke-virtual {v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->offsetLeft()F

    move-result v1

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_83

    const/4 v0, 0x0

    aget v0, v2, v0

    iget-object v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/XAxisRenderer;->mViewPortHandler:Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;

    .line 232
    invoke-virtual {v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->getChartWidth()F

    move-result v1

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_83

    .line 235
    const/4 v0, 0x0

    aget v0, v2, v0

    iget-object v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/XAxisRenderer;->mViewPortHandler:Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;

    invoke-virtual {v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->contentBottom()F

    move-result v1

    invoke-virtual {v3, v0, v1}, Landroid/graphics/Path;->moveTo(FF)V

    .line 236
    const/4 v0, 0x0

    aget v0, v2, v0

    iget-object v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/XAxisRenderer;->mViewPortHandler:Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;

    invoke-virtual {v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->contentTop()F

    move-result v1

    invoke-virtual {v3, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 240
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/XAxisRenderer;->mGridPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v3, v0}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 244
    :cond_83
    invoke-virtual {v3}, Landroid/graphics/Path;->reset()V

    .line 224
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/XAxisRenderer;->mXAxis:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis;

    iget v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis;->mAxisLabelModulus:I

    add-int/2addr v4, v0

    goto :goto_3f

    .line 246
    .end local v4    # "i":I
    :cond_8c
    return-void

    nop

    :array_8e
    .array-data 4
        0x0
        0x0
    .end array-data
.end method

.method public renderLimitLines(Landroid/graphics/Canvas;)V
    .registers 14
    .param p1, "c"    # Landroid/graphics/Canvas;

    .line 258
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/XAxisRenderer;->mXAxis:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis;->getLimitLines()Ljava/util/List;

    move-result-object v3

    .line 261
    .local v3, "limitLines":Ljava/util/List;, "Ljava/util/List<Lbr/com/itau/widgets/graphicalfriedcake/charts/components/LimitLine;>;"
    if-eqz v3, :cond_e

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    if-gtz v0, :cond_f

    .line 262
    :cond_e
    return-void

    .line 265
    :cond_f
    const/4 v0, 0x4

    new-array v4, v0, [F

    .line 266
    .local v4, "pts":[F
    new-instance v5, Landroid/graphics/Path;

    invoke-direct {v5}, Landroid/graphics/Path;-><init>()V

    .line 269
    .local v5, "limitLinePath":Landroid/graphics/Path;
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_18
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    if-ge v6, v0, :cond_100

    .line 272
    invoke-interface {v3, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/LimitLine;

    .line 275
    .local v7, "l":Lbr/com/itau/widgets/graphicalfriedcake/charts/components/LimitLine;
    invoke-virtual {v7}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/LimitLine;->getLimit()F

    move-result v0

    const/4 v1, 0x0

    aput v0, v4, v1

    .line 276
    invoke-virtual {v7}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/LimitLine;->getLimit()F

    move-result v0

    const/4 v1, 0x2

    aput v0, v4, v1

    .line 279
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/XAxisRenderer;->mTrans:Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Transformer;

    invoke-virtual {v0, v4}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Transformer;->pointValuesToPixel([F)V

    .line 282
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/XAxisRenderer;->mViewPortHandler:Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->contentTop()F

    move-result v0

    const/4 v1, 0x1

    aput v0, v4, v1

    .line 283
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/XAxisRenderer;->mViewPortHandler:Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->contentBottom()F

    move-result v0

    const/4 v1, 0x3

    aput v0, v4, v1

    .line 286
    const/4 v0, 0x0

    aget v0, v4, v0

    const/4 v1, 0x1

    aget v1, v4, v1

    invoke-virtual {v5, v0, v1}, Landroid/graphics/Path;->moveTo(FF)V

    .line 287
    const/4 v0, 0x2

    aget v0, v4, v0

    const/4 v1, 0x3

    aget v1, v4, v1

    invoke-virtual {v5, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 290
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/XAxisRenderer;->mLimitLinePaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 291
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/XAxisRenderer;->mLimitLinePaint:Landroid/graphics/Paint;

    invoke-virtual {v7}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/LimitLine;->getLineColor()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 292
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/XAxisRenderer;->mLimitLinePaint:Landroid/graphics/Paint;

    invoke-virtual {v7}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/LimitLine;->getLineWidth()F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 293
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/XAxisRenderer;->mLimitLinePaint:Landroid/graphics/Paint;

    invoke-virtual {v7}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/LimitLine;->getDashPathEffect()Landroid/graphics/DashPathEffect;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 296
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/XAxisRenderer;->mLimitLinePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v5, v0}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 297
    invoke-virtual {v5}, Landroid/graphics/Path;->reset()V

    .line 300
    invoke-virtual {v7}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/LimitLine;->getLabel()Ljava/lang/String;

    move-result-object v8

    .line 304
    .local v8, "label":Ljava/lang/String;
    if-eqz v8, :cond_fc

    const-string v0, ""

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_fc

    .line 307
    invoke-virtual {v7}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/LimitLine;->getLineWidth()F

    move-result v9

    .line 308
    .local v9, "xOffset":F
    const/high16 v0, 0x40800000    # 4.0f

    invoke-static {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Utils;->convertDpToPixel(F)F

    move-result v10

    .line 311
    .local v10, "add":F
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/XAxisRenderer;->mLimitLinePaint:Landroid/graphics/Paint;

    invoke-virtual {v7}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/LimitLine;->getTextStyle()Landroid/graphics/Paint$Style;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 312
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/XAxisRenderer;->mLimitLinePaint:Landroid/graphics/Paint;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 313
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/XAxisRenderer;->mLimitLinePaint:Landroid/graphics/Paint;

    invoke-virtual {v7}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/LimitLine;->getTextColor()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 314
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/XAxisRenderer;->mLimitLinePaint:Landroid/graphics/Paint;

    const/high16 v1, 0x3f000000    # 0.5f

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 315
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/XAxisRenderer;->mLimitLinePaint:Landroid/graphics/Paint;

    invoke-virtual {v7}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/LimitLine;->getTextSize()F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 318
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/XAxisRenderer;->mLimitLinePaint:Landroid/graphics/Paint;

    invoke-static {v0, v8}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Utils;->calcTextHeight(Landroid/graphics/Paint;Ljava/lang/String;)I

    move-result v0

    int-to-float v0, v0

    const/high16 v1, 0x40000000    # 2.0f

    div-float v1, v10, v1

    add-float v11, v0, v1

    .line 321
    .local v11, "yOffset":F
    invoke-virtual {v7}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/LimitLine;->getLabelPosition()Lbr/com/itau/widgets/graphicalfriedcake/charts/components/LimitLine$LimitLabelPosition;

    move-result-object v0

    sget-object v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/LimitLine$LimitLabelPosition;->POS_RIGHT:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/LimitLine$LimitLabelPosition;

    if-ne v0, v1, :cond_ec

    .line 322
    const/4 v0, 0x0

    aget v0, v4, v0

    add-float/2addr v0, v9

    iget-object v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/XAxisRenderer;->mViewPortHandler:Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;

    invoke-virtual {v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->contentBottom()F

    move-result v1

    sub-float/2addr v1, v10

    iget-object v2, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/XAxisRenderer;->mLimitLinePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v8, v0, v1, v2}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    goto :goto_fc

    .line 324
    :cond_ec
    const/4 v0, 0x0

    aget v0, v4, v0

    add-float/2addr v0, v9

    iget-object v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/XAxisRenderer;->mViewPortHandler:Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;

    invoke-virtual {v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->contentTop()F

    move-result v1

    add-float/2addr v1, v11

    iget-object v2, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/XAxisRenderer;->mLimitLinePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v8, v0, v1, v2}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 269
    .end local v7    # "l":Lbr/com/itau/widgets/graphicalfriedcake/charts/components/LimitLine;
    .end local v8    # "label":Ljava/lang/String;
    .end local v9    # "xOffset":F
    .end local v10    # "add":F
    .end local v11    # "yOffset":F
    :cond_fc
    :goto_fc
    add-int/lit8 v6, v6, 0x1

    goto/16 :goto_18

    .line 328
    .end local v6    # "i":I
    :cond_100
    return-void
.end method

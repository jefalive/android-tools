.class Lcom/itau/empresas/feature/recarga/contatos/controller/ContatoRecargaController$4;
.super Ljava/lang/Object;
.source "ContatoRecargaController.java"

# interfaces
.implements Lbr/com/itau/sdk/android/core/type/BackendCall;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/itau/empresas/feature/recarga/contatos/controller/ContatoRecargaController;->excluiContatoRecarga(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/itau/empresas/feature/recarga/contatos/controller/ContatoRecargaController;

.field final synthetic val$idContato:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/itau/empresas/feature/recarga/contatos/controller/ContatoRecargaController;Ljava/lang/String;)V
    .registers 3
    .param p1, "this$0"    # Lcom/itau/empresas/feature/recarga/contatos/controller/ContatoRecargaController;

    .line 55
    iput-object p1, p0, Lcom/itau/empresas/feature/recarga/contatos/controller/ContatoRecargaController$4;->this$0:Lcom/itau/empresas/feature/recarga/contatos/controller/ContatoRecargaController;

    iput-object p2, p0, Lcom/itau/empresas/feature/recarga/contatos/controller/ContatoRecargaController$4;->val$idContato:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public callBackend()V
    .registers 4

    .line 58
    new-instance v2, Lcom/itau/empresas/Evento$EventoContatoRecargaExcluido;

    invoke-direct {v2}, Lcom/itau/empresas/Evento$EventoContatoRecargaExcluido;-><init>()V

    .line 59
    .local v2, "eventoContatoRecargaExcluido":Lcom/itau/empresas/Evento$EventoContatoRecargaExcluido;
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/controller/ContatoRecargaController$4;->this$0:Lcom/itau/empresas/feature/recarga/contatos/controller/ContatoRecargaController;

    # invokes: Lcom/itau/empresas/feature/recarga/contatos/controller/ContatoRecargaController;->api()Ljava/lang/Object;
    invoke-static {v0}, Lcom/itau/empresas/feature/recarga/contatos/controller/ContatoRecargaController;->access$600(Lcom/itau/empresas/feature/recarga/contatos/controller/ContatoRecargaController;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/api/Api;

    iget-object v1, p0, Lcom/itau/empresas/feature/recarga/contatos/controller/ContatoRecargaController$4;->val$idContato:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/itau/empresas/api/Api;->excluiContatoRecarga(Ljava/lang/String;)Ljava/util/List;

    .line 60
    # invokes: Lcom/itau/empresas/feature/recarga/contatos/controller/ContatoRecargaController;->post(Ljava/lang/Object;)V
    invoke-static {v2}, Lcom/itau/empresas/feature/recarga/contatos/controller/ContatoRecargaController;->access$700(Ljava/lang/Object;)V

    .line 61
    return-void
.end method

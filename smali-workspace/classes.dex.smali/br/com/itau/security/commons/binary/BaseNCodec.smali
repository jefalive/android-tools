.class public abstract Lbr/com/itau/security/commons/binary/BaseNCodec;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final n:[B

.field private static o:I


# instance fields
.field protected final f:B
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field protected final g:B

.field protected final h:I

.field private final k:I

.field private final l:I

.field private final m:I


# direct methods
.method static constructor <clinit>()V
    .registers 3

    const/16 v0, 0x74

    new-array v0, v0, [B

    fill-array-data v0, :array_e

    sput-object v0, Lbr/com/itau/security/commons/binary/BaseNCodec;->n:[B

    const/16 v0, 0x80

    sput v0, Lbr/com/itau/security/commons/binary/BaseNCodec;->o:I

    return-void

    :array_e
    .array-data 1
        0x18t
        0x6et
        -0x48t
        0x27t
        0x11t
        0x11t
        -0x11t
        0xct
        -0x8t
        0xft
        -0xft
        0xdt
        -0x52t
        0x53t
        0x2t
        -0x5t
        0x0t
        -0x4t
        -0x3t
        -0x4t
        -0x1t
        -0x44t
        0x54t
        -0x5t
        -0x4ft
        0x22t
        0x1ft
        0x12t
        -0xet
        -0x38t
        0x21t
        -0x2et
        0x44t
        0x1t
        -0x2t
        0xct
        -0xbt
        0x1t
        -0x45t
        0x49t
        0xat
        -0x53t
        0x4et
        0x1t
        0x5t
        -0x54t
        0x41t
        -0x41t
        0x42t
        0x17t
        -0x5t
        -0xft
        -0xat
        0x2t
        -0x3dt
        0x4ft
        0x3t
        -0x52t
        0x41t
        -0x41t
        0x33t
        0x21t
        -0x2t
        -0x9t
        0x5t
        -0x7t
        0x11t
        0x11t
        -0x11t
        0xct
        -0x8t
        0xft
        -0xft
        0xdt
        -0x52t
        0x53t
        0x2t
        -0x5t
        0x0t
        -0x4t
        -0x3t
        -0x4t
        -0x1t
        -0x44t
        0x54t
        -0x5t
        -0x4ft
        0x22t
        0x1ft
        0x12t
        -0xet
        -0x38t
        0x21t
        -0x2et
        0x45t
        0x9t
        -0xbt
        0xct
        -0xbt
        0x1t
        -0x45t
        0x49t
        0xat
        -0x53t
        0x4et
        0x1t
        0x5t
        -0x54t
        0x41t
        -0x41t
        0x42t
        0x17t
        -0x5t
        -0xft
        -0xat
        0x2t
    .end array-data
.end method

.method protected constructor <init>(IIIIB)V
    .registers 7

    .line 214
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 162
    const/16 v0, 0x3d

    iput-byte v0, p0, Lbr/com/itau/security/commons/binary/BaseNCodec;->f:B

    .line 215
    iput p1, p0, Lbr/com/itau/security/commons/binary/BaseNCodec;->k:I

    .line 216
    iput p2, p0, Lbr/com/itau/security/commons/binary/BaseNCodec;->l:I

    .line 217
    if-lez p3, :cond_11

    if-lez p4, :cond_11

    const/4 p1, 0x1

    goto :goto_12

    :cond_11
    const/4 p1, 0x0

    .line 218
    :goto_12
    if-eqz p1, :cond_18

    div-int v0, p3, p2

    mul-int/2addr v0, p2

    goto :goto_19

    :cond_18
    const/4 v0, 0x0

    :goto_19
    iput v0, p0, Lbr/com/itau/security/commons/binary/BaseNCodec;->h:I

    .line 219
    iput p4, p0, Lbr/com/itau/security/commons/binary/BaseNCodec;->m:I

    .line 221
    iput-byte p5, p0, Lbr/com/itau/security/commons/binary/BaseNCodec;->g:B

    .line 222
    return-void
.end method

.method private static a([BILcc/b;)I
    .registers 7

    .line 298
    iget-object v0, p2, Lcc/b;->c:[B

    if-eqz v0, :cond_2b

    .line 299
    .line 1241
    move-object v3, p2

    iget-object v0, p2, Lcc/b;->c:[B

    if-eqz v0, :cond_f

    iget v0, v3, Lcc/b;->d:I

    iget v1, v3, Lcc/b;->e:I

    sub-int/2addr v0, v1

    goto :goto_10

    :cond_f
    const/4 v0, 0x0

    .line 299
    :goto_10
    invoke-static {v0, p1}, Ljava/lang/Math;->min(II)I

    move-result v3

    .line 300
    iget-object v0, p2, Lcc/b;->c:[B

    iget v1, p2, Lcc/b;->e:I

    const/4 v2, 0x0

    invoke-static {v0, v1, p0, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 301
    iget v0, p2, Lcc/b;->e:I

    add-int/2addr v0, v3

    iput v0, p2, Lcc/b;->e:I

    .line 302
    iget v0, p2, Lcc/b;->e:I

    iget v1, p2, Lcc/b;->d:I

    if-lt v0, v1, :cond_2a

    .line 303
    const/4 v0, 0x0

    iput-object v0, p2, Lcc/b;->c:[B

    .line 305
    :cond_2a
    return v3

    .line 307
    :cond_2b
    iget-boolean v0, p2, Lcc/b;->f:Z

    if-eqz v0, :cond_31

    const/4 v0, -0x1

    return v0

    :cond_31
    const/4 v0, 0x0

    return v0
.end method

.method protected static a(B)Z
    .registers 2

    .line 318
    sparse-switch p0, :sswitch_data_8

    goto :goto_6

    .line 323
    :sswitch_4
    const/4 v0, 0x1

    return v0

    .line 325
    :goto_6
    const/4 v0, 0x0

    return v0

    :sswitch_data_8
    .sparse-switch
        0x9 -> :sswitch_4
        0xa -> :sswitch_4
        0xd -> :sswitch_4
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method static a(Lcc/b;)[B
    .registers 6

    .line 259
    iget-object v0, p0, Lcc/b;->c:[B

    if-nez v0, :cond_11

    .line 260
    const/16 v0, 0x2000

    new-array v0, v0, [B

    iput-object v0, p0, Lcc/b;->c:[B

    .line 261
    const/4 v0, 0x0

    iput v0, p0, Lcc/b;->d:I

    .line 262
    const/4 v0, 0x0

    iput v0, p0, Lcc/b;->e:I

    goto :goto_24

    .line 264
    :cond_11
    iget-object v0, p0, Lcc/b;->c:[B

    array-length v0, v0

    mul-int/lit8 v0, v0, 0x2

    new-array v4, v0, [B

    .line 265
    iget-object v0, p0, Lcc/b;->c:[B

    iget-object v1, p0, Lcc/b;->c:[B

    array-length v1, v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-static {v0, v2, v4, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 266
    iput-object v4, p0, Lcc/b;->c:[B

    .line 268
    :goto_24
    iget-object v0, p0, Lcc/b;->c:[B

    return-object v0
.end method


# virtual methods
.method protected final a([B)Z
    .registers 6

    .line 508
    if-nez p1, :cond_4

    .line 509
    const/4 v0, 0x0

    return v0

    .line 511
    :cond_4
    array-length v1, p1

    const/4 v2, 0x0

    :goto_6
    if-ge v2, v1, :cond_19

    aget-byte v3, p1, v2

    .line 512
    iget-byte v0, p0, Lbr/com/itau/security/commons/binary/BaseNCodec;->g:B

    if-eq v0, v3, :cond_14

    invoke-virtual {p0, v3}, Lbr/com/itau/security/commons/binary/BaseNCodec;->isInAlphabet(B)Z

    move-result v0

    if-eqz v0, :cond_16

    .line 513
    :cond_14
    const/4 v0, 0x1

    return v0

    .line 511
    :cond_16
    add-int/lit8 v2, v2, 0x1

    goto :goto_6

    .line 516
    :cond_19
    const/4 v0, 0x0

    return v0
.end method

.method abstract b([BIILcc/b;)V
.end method

.method public encode([B)[B
    .registers 4

    .line 424
    if-eqz p1, :cond_5

    array-length v0, p1

    if-nez v0, :cond_6

    .line 425
    :cond_5
    return-object p1

    .line 427
    :cond_6
    array-length v0, p1

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v1, v0}, Lbr/com/itau/security/commons/binary/BaseNCodec;->encode([BII)[B

    move-result-object v0

    return-object v0
.end method

.method public encode([BII)[B
    .registers 7

    .line 441
    if-eqz p1, :cond_5

    array-length v0, p1

    if-nez v0, :cond_6

    .line 442
    :cond_5
    return-object p1

    .line 444
    :cond_6
    new-instance v2, Lcc/b;

    invoke-direct {v2}, Lcc/b;-><init>()V

    .line 445
    invoke-virtual {p0, p1, p2, p3, v2}, Lbr/com/itau/security/commons/binary/BaseNCodec;->b([BIILcc/b;)V

    .line 446
    const/4 v0, -0x1

    invoke-virtual {p0, p1, p2, v0, v2}, Lbr/com/itau/security/commons/binary/BaseNCodec;->b([BIILcc/b;)V

    .line 447
    iget v0, v2, Lcc/b;->d:I

    iget v1, v2, Lcc/b;->e:I

    sub-int/2addr v0, v1

    new-array p1, v0, [B

    .line 448
    array-length v0, p1

    invoke-static {p1, v0, v2}, Lbr/com/itau/security/commons/binary/BaseNCodec;->a([BILcc/b;)I

    .line 449
    return-object p1
.end method

.method protected abstract isInAlphabet(B)Z
.end method

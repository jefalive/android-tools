.class Lcom/itau/empresas/feature/comprovantes/ComprovantesAdapterListener$BotaoEnviarClickListener;
.super Ljava/lang/Object;
.source "ComprovantesAdapterListener.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/itau/empresas/feature/comprovantes/ComprovantesAdapterListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "BotaoEnviarClickListener"
.end annotation


# instance fields
.field private final comprovantesVO:Lcom/itau/empresas/feature/comprovantes/model/ComprovantesVO;

.field private final context:Landroid/content/Context;

.field private final enviarClickListener:Landroid/view/View$OnClickListener;

.field private final swipeLayout:Lcom/daimajia/swipe/SwipeLayout;


# direct methods
.method constructor <init>(Lcom/itau/empresas/feature/comprovantes/model/ComprovantesVO;Landroid/content/Context;Landroid/view/View$OnClickListener;Lcom/daimajia/swipe/SwipeLayout;)V
    .registers 5
    .param p1, "comprovantesVO"    # Lcom/itau/empresas/feature/comprovantes/model/ComprovantesVO;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "enviarClickListener"    # Landroid/view/View$OnClickListener;
    .param p4, "swipeLayout"    # Lcom/daimajia/swipe/SwipeLayout;

    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 66
    iput-object p1, p0, Lcom/itau/empresas/feature/comprovantes/ComprovantesAdapterListener$BotaoEnviarClickListener;->comprovantesVO:Lcom/itau/empresas/feature/comprovantes/model/ComprovantesVO;

    .line 67
    iput-object p2, p0, Lcom/itau/empresas/feature/comprovantes/ComprovantesAdapterListener$BotaoEnviarClickListener;->context:Landroid/content/Context;

    .line 68
    iput-object p3, p0, Lcom/itau/empresas/feature/comprovantes/ComprovantesAdapterListener$BotaoEnviarClickListener;->enviarClickListener:Landroid/view/View$OnClickListener;

    .line 69
    iput-object p4, p0, Lcom/itau/empresas/feature/comprovantes/ComprovantesAdapterListener$BotaoEnviarClickListener;->swipeLayout:Lcom/daimajia/swipe/SwipeLayout;

    .line 70
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .registers 4
    .param p1, "view"    # Landroid/view/View;

    .line 74
    iget-object v0, p0, Lcom/itau/empresas/feature/comprovantes/ComprovantesAdapterListener$BotaoEnviarClickListener;->context:Landroid/content/Context;

    .line 75
    invoke-static {v0}, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesEnviarActivity_;->intent(Landroid/content/Context;)Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesEnviarActivity_$IntentBuilder_;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/comprovantes/ComprovantesAdapterListener$BotaoEnviarClickListener;->comprovantesVO:Lcom/itau/empresas/feature/comprovantes/model/ComprovantesVO;

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesEnviarActivity_$IntentBuilder_;->comprovantesVO(Lcom/itau/empresas/feature/comprovantes/model/ComprovantesVO;)Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesEnviarActivity_$IntentBuilder_;

    move-result-object v0

    .line 76
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesEnviarActivity_$IntentBuilder_;->startForResult(I)Lorg/androidannotations/api/builder/PostActivityStarter;

    .line 77
    iget-object v0, p0, Lcom/itau/empresas/feature/comprovantes/ComprovantesAdapterListener$BotaoEnviarClickListener;->swipeLayout:Lcom/daimajia/swipe/SwipeLayout;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/daimajia/swipe/SwipeLayout;->close(Z)V

    .line 78
    iget-object v0, p0, Lcom/itau/empresas/feature/comprovantes/ComprovantesAdapterListener$BotaoEnviarClickListener;->enviarClickListener:Landroid/view/View$OnClickListener;

    if-eqz v0, :cond_1f

    .line 79
    iget-object v0, p0, Lcom/itau/empresas/feature/comprovantes/ComprovantesAdapterListener$BotaoEnviarClickListener;->enviarClickListener:Landroid/view/View$OnClickListener;

    invoke-interface {v0, p1}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    .line 81
    :cond_1f
    return-void
.end method

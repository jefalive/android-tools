.class public Lcom/itau/empresas/feature/credito/giro/model/CamposComprovanteVO;
.super Ljava/lang/Object;
.source "CamposComprovanteVO.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private mDataContratacao:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "data_contratacao"
    .end annotation
.end field

.field private mDataVencimentoOperacao:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "data_vencimento_operacao"
    .end annotation
.end field

.field private mDescricaoAmortizacao:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "descricao_amortizacao"
    .end annotation
.end field

.field private mIndicadorSeguro:Z
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "indicador_seguro"
    .end annotation
.end field

.field private mNomeCampo:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "nome_campo"
    .end annotation
.end field

.field private mNumeroControle:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "numero_controle"
    .end annotation
.end field

.field private mPercentualSeguro:F
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "percentual_seguro"
    .end annotation
.end field

.field private mPercentualTarifaContratacao:F
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "percentual_tarifa_contratacao"
    .end annotation
.end field

.field private mPercentualValorCredito:F
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "percentual_valor_credito"
    .end annotation
.end field

.field private mPercentualValorIof:F
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "percentual_valor_iof"
    .end annotation
.end field

.field private mPercentualValorTarifa:F
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "percentual_valor_tarifa"
    .end annotation
.end field

.field private mPercentualValorTotalFinanciado:F
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "percentual_valor_total_financiado"
    .end annotation
.end field

.field private mQuantidadeParcelas:I
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "quantidade_parcelas"
    .end annotation
.end field

.field private mTaxaCetAno:F
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "taxa_cet_ano"
    .end annotation
.end field

.field private mTaxaCetMes:F
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "taxa_cet_mes"
    .end annotation
.end field

.field private mTaxaJurosEfetivo:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "taxa_juros_efetivo"
    .end annotation
.end field

.field private mTaxaJurosEfetivoAno:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "taxa_juros_efetivo_ano"
    .end annotation
.end field

.field private mValorCampo:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "valor_campo"
    .end annotation
.end field

.field private mValorEmprestimo:J
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "valor_emprestimo"
    .end annotation
.end field

.field private mValorParcela:F
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "valor_parcela"
    .end annotation
.end field

.field private mValorSeguro:F
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "valor_seguro"
    .end annotation
.end field

.field private mValorTarifaContratacao:F
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "valor_tarifa_contratacao"
    .end annotation
.end field

.field private mValorTarifaIof:F
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "valor_tarifa_iof"
    .end annotation
.end field

.field private mValorTotalFinanciado:F
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "valor_total_financiado"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getDataContratacao()Ljava/lang/String;
    .registers 2

    .line 114
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/model/CamposComprovanteVO;->mDataContratacao:Ljava/lang/String;

    return-object v0
.end method

.method public getDataVencimentoOperacao()Ljava/lang/String;
    .registers 2

    .line 94
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/model/CamposComprovanteVO;->mDataVencimentoOperacao:Ljava/lang/String;

    return-object v0
.end method

.method public getDescricaoAmortizacao()Ljava/lang/String;
    .registers 2

    .line 130
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/model/CamposComprovanteVO;->mDescricaoAmortizacao:Ljava/lang/String;

    return-object v0
.end method

.method public getIndicadorSeguro()Z
    .registers 2

    .line 110
    iget-boolean v0, p0, Lcom/itau/empresas/feature/credito/giro/model/CamposComprovanteVO;->mIndicadorSeguro:Z

    return v0
.end method

.method public getNumeroControle()Ljava/lang/String;
    .registers 2

    .line 102
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/model/CamposComprovanteVO;->mNumeroControle:Ljava/lang/String;

    return-object v0
.end method

.method public getPercentualValorCredito()F
    .registers 2

    .line 118
    iget v0, p0, Lcom/itau/empresas/feature/credito/giro/model/CamposComprovanteVO;->mPercentualValorCredito:F

    return v0
.end method

.method public getPercentualValorIof()F
    .registers 2

    .line 154
    iget v0, p0, Lcom/itau/empresas/feature/credito/giro/model/CamposComprovanteVO;->mPercentualValorIof:F

    return v0
.end method

.method public getPercentualValorTarifa()F
    .registers 2

    .line 178
    iget v0, p0, Lcom/itau/empresas/feature/credito/giro/model/CamposComprovanteVO;->mPercentualValorTarifa:F

    return v0
.end method

.method public getPercentualValorTotalFinanciado()F
    .registers 2

    .line 126
    iget v0, p0, Lcom/itau/empresas/feature/credito/giro/model/CamposComprovanteVO;->mPercentualValorTotalFinanciado:F

    return v0
.end method

.method public getPertencualSeguro()F
    .registers 2

    .line 162
    iget v0, p0, Lcom/itau/empresas/feature/credito/giro/model/CamposComprovanteVO;->mPercentualSeguro:F

    return v0
.end method

.method public getQuantidadeParcelas()I
    .registers 2

    .line 98
    iget v0, p0, Lcom/itau/empresas/feature/credito/giro/model/CamposComprovanteVO;->mQuantidadeParcelas:I

    return v0
.end method

.method public getTaxaCetAno()F
    .registers 2

    .line 146
    iget v0, p0, Lcom/itau/empresas/feature/credito/giro/model/CamposComprovanteVO;->mTaxaCetAno:F

    return v0
.end method

.method public getTaxaCetMes()F
    .registers 2

    .line 142
    iget v0, p0, Lcom/itau/empresas/feature/credito/giro/model/CamposComprovanteVO;->mTaxaCetMes:F

    return v0
.end method

.method public getTaxaJurosEfetivo()Ljava/lang/String;
    .registers 2

    .line 138
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/model/CamposComprovanteVO;->mTaxaJurosEfetivo:Ljava/lang/String;

    return-object v0
.end method

.method public getTaxaJurosEfetivoAno()Ljava/lang/String;
    .registers 2

    .line 134
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/model/CamposComprovanteVO;->mTaxaJurosEfetivoAno:Ljava/lang/String;

    return-object v0
.end method

.method public getValorEmprestimo()J
    .registers 3

    .line 106
    iget-wide v0, p0, Lcom/itau/empresas/feature/credito/giro/model/CamposComprovanteVO;->mValorEmprestimo:J

    return-wide v0
.end method

.method public getValorParcela()F
    .registers 2

    .line 174
    iget v0, p0, Lcom/itau/empresas/feature/credito/giro/model/CamposComprovanteVO;->mValorParcela:F

    return v0
.end method

.method public getValorSeguro()F
    .registers 2

    .line 158
    iget v0, p0, Lcom/itau/empresas/feature/credito/giro/model/CamposComprovanteVO;->mValorSeguro:F

    return v0
.end method

.method public getValorTarifaContratacao()F
    .registers 2

    .line 166
    iget v0, p0, Lcom/itau/empresas/feature/credito/giro/model/CamposComprovanteVO;->mValorTarifaContratacao:F

    return v0
.end method

.method public getValorTarifaIof()F
    .registers 2

    .line 150
    iget v0, p0, Lcom/itau/empresas/feature/credito/giro/model/CamposComprovanteVO;->mValorTarifaIof:F

    return v0
.end method

.method public getValorTotalFinanciado()F
    .registers 2

    .line 122
    iget v0, p0, Lcom/itau/empresas/feature/credito/giro/model/CamposComprovanteVO;->mValorTotalFinanciado:F

    return v0
.end method

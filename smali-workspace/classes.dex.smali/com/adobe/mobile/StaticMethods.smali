.class final Lcom/adobe/mobile/StaticMethods;
.super Ljava/lang/Object;
.source "StaticMethods.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/adobe/mobile/StaticMethods$NullActivityException;,
        Lcom/adobe/mobile/StaticMethods$NullContextException;
    }
.end annotation


# static fields
.field private static final BYTE_TO_HEX:[C

.field private static _activity:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<Landroid/app/Activity;>;"
        }
    .end annotation
.end field

.field private static final _advertisingIdentifierMutex:Ljava/lang/Object;

.field private static _aidDone:Z

.field private static final _aidMutex:Ljava/lang/Object;

.field private static final _analyticsExecutorMutex:Ljava/lang/Object;

.field private static _appType:Lcom/adobe/mobile/Config$ApplicationType;

.field private static final _applicationIDMutex:Ljava/lang/Object;

.field private static final _applicationNameMutex:Ljava/lang/Object;

.field private static final _applicationVersionCodeMutex:Ljava/lang/Object;

.field private static final _applicationVersionMutex:Ljava/lang/Object;

.field private static final _audienceExecutorMutex:Ljava/lang/Object;

.field private static final _carrierMutex:Ljava/lang/Object;

.field static final _contextDataKeyWhiteList:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
        }
    .end annotation
.end field

.field static _contextDataKeyWhiteListCount:I

.field private static final _currentActivityMutex:Ljava/lang/Object;

.field private static _debugLogging:Z

.field private static final _defaultDataMutex:Ljava/lang/Object;

.field private static _isWearable:Z

.field private static final _mediaExecutorMutex:Ljava/lang/Object;

.field private static final _messageImageCachingExecutorMutex:Ljava/lang/Object;

.field private static final _messagesExecutorMutex:Ljava/lang/Object;

.field private static final _operatingSystemMutex:Ljava/lang/Object;

.field private static final _piiExecutorMutex:Ljava/lang/Object;

.field private static final _pushEnabledMutex:Ljava/lang/Object;

.field private static final _pushIdentifierMutex:Ljava/lang/Object;

.field private static final _resolutionMutex:Ljava/lang/Object;

.field private static final _sharedExecutorMutex:Ljava/lang/Object;

.field private static final _sharedPreferencesMutex:Ljava/lang/Object;

.field private static final _thirdPartyCallbacksExecutorMutex:Ljava/lang/Object;

.field private static final _timedActionsExecutorMutex:Ljava/lang/Object;

.field private static final _timestampMutex:Ljava/lang/Object;

.field private static final _userAgentMutex:Ljava/lang/Object;

.field private static final _userIdentifierMutex:Ljava/lang/Object;

.field private static final _visitorIDMutex:Ljava/lang/Object;

.field private static advertisingIdentifier:Ljava/lang/String;

.field private static aid:Ljava/lang/String;

.field private static analyticsExecutor:Ljava/util/concurrent/ExecutorService;

.field private static appID:Ljava/lang/String;

.field private static appName:Ljava/lang/String;

.field private static audienceExecutor:Ljava/util/concurrent/ExecutorService;

.field private static carrier:Ljava/lang/String;

.field private static final contextDataMask:[Z

.field private static defaultData:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
        }
    .end annotation
.end field

.field private static final encodedChars:[Ljava/lang/String;

.field private static mediaExecutor:Ljava/util/concurrent/ExecutorService;

.field private static messageImageCachingExecutor:Ljava/util/concurrent/ExecutorService;

.field private static messagesExecutor:Ljava/util/concurrent/ExecutorService;

.field private static operatingSystem:Ljava/lang/String;

.field private static piiExecutor:Ljava/util/concurrent/ExecutorService;

.field private static prefs:Landroid/content/SharedPreferences;

.field private static pushEnabled:Z

.field private static pushIdentifier:Ljava/lang/String;

.field private static resolution:Ljava/lang/String;

.field private static sharedContext:Landroid/content/Context;

.field private static sharedExecutor:Ljava/util/concurrent/ExecutorService;

.field private static thirdPartyCallbacksExecutor:Ljava/util/concurrent/ExecutorService;

.field private static timedActionsExecutor:Ljava/util/concurrent/ExecutorService;

.field private static timestamp:Ljava/lang/String;

.field private static userAgent:Ljava/lang/String;

.field private static final utf8Mask:[Z

.field private static versionCode:I

.field private static versionName:Ljava/lang/String;

.field private static visitorID:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .line 98
    const/16 v0, 0x100

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "%00"

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const-string v1, "%01"

    const/4 v2, 0x1

    aput-object v1, v0, v2

    const-string v1, "%02"

    const/4 v2, 0x2

    aput-object v1, v0, v2

    const-string v1, "%03"

    const/4 v2, 0x3

    aput-object v1, v0, v2

    const-string v1, "%04"

    const/4 v2, 0x4

    aput-object v1, v0, v2

    const-string v1, "%05"

    const/4 v2, 0x5

    aput-object v1, v0, v2

    const-string v1, "%06"

    const/4 v2, 0x6

    aput-object v1, v0, v2

    const-string v1, "%07"

    const/4 v2, 0x7

    aput-object v1, v0, v2

    const-string v1, "%08"

    const/16 v2, 0x8

    aput-object v1, v0, v2

    const-string v1, "%09"

    const/16 v2, 0x9

    aput-object v1, v0, v2

    const-string v1, "%0A"

    const/16 v2, 0xa

    aput-object v1, v0, v2

    const-string v1, "%0B"

    const/16 v2, 0xb

    aput-object v1, v0, v2

    const-string v1, "%0C"

    const/16 v2, 0xc

    aput-object v1, v0, v2

    const-string v1, "%0D"

    const/16 v2, 0xd

    aput-object v1, v0, v2

    const-string v1, "%0E"

    const/16 v2, 0xe

    aput-object v1, v0, v2

    const-string v1, "%0F"

    const/16 v2, 0xf

    aput-object v1, v0, v2

    const-string v1, "%10"

    const/16 v2, 0x10

    aput-object v1, v0, v2

    const-string v1, "%11"

    const/16 v2, 0x11

    aput-object v1, v0, v2

    const-string v1, "%12"

    const/16 v2, 0x12

    aput-object v1, v0, v2

    const-string v1, "%13"

    const/16 v2, 0x13

    aput-object v1, v0, v2

    const-string v1, "%14"

    const/16 v2, 0x14

    aput-object v1, v0, v2

    const-string v1, "%15"

    const/16 v2, 0x15

    aput-object v1, v0, v2

    const-string v1, "%16"

    const/16 v2, 0x16

    aput-object v1, v0, v2

    const-string v1, "%17"

    const/16 v2, 0x17

    aput-object v1, v0, v2

    const-string v1, "%18"

    const/16 v2, 0x18

    aput-object v1, v0, v2

    const-string v1, "%19"

    const/16 v2, 0x19

    aput-object v1, v0, v2

    const-string v1, "%1A"

    const/16 v2, 0x1a

    aput-object v1, v0, v2

    const-string v1, "%1B"

    const/16 v2, 0x1b

    aput-object v1, v0, v2

    const-string v1, "%1C"

    const/16 v2, 0x1c

    aput-object v1, v0, v2

    const-string v1, "%1D"

    const/16 v2, 0x1d

    aput-object v1, v0, v2

    const-string v1, "%1E"

    const/16 v2, 0x1e

    aput-object v1, v0, v2

    const-string v1, "%1F"

    const/16 v2, 0x1f

    aput-object v1, v0, v2

    const-string v1, "%20"

    const/16 v2, 0x20

    aput-object v1, v0, v2

    const-string v1, "%21"

    const/16 v2, 0x21

    aput-object v1, v0, v2

    const-string v1, "%22"

    const/16 v2, 0x22

    aput-object v1, v0, v2

    const-string v1, "%23"

    const/16 v2, 0x23

    aput-object v1, v0, v2

    const-string v1, "%24"

    const/16 v2, 0x24

    aput-object v1, v0, v2

    const-string v1, "%25"

    const/16 v2, 0x25

    aput-object v1, v0, v2

    const-string v1, "%26"

    const/16 v2, 0x26

    aput-object v1, v0, v2

    const-string v1, "%27"

    const/16 v2, 0x27

    aput-object v1, v0, v2

    const-string v1, "%28"

    const/16 v2, 0x28

    aput-object v1, v0, v2

    const-string v1, "%29"

    const/16 v2, 0x29

    aput-object v1, v0, v2

    const-string v1, "*"

    const/16 v2, 0x2a

    aput-object v1, v0, v2

    const-string v1, "%2B"

    const/16 v2, 0x2b

    aput-object v1, v0, v2

    const-string v1, "%2C"

    const/16 v2, 0x2c

    aput-object v1, v0, v2

    const-string v1, "-"

    const/16 v2, 0x2d

    aput-object v1, v0, v2

    const-string v1, "."

    const/16 v2, 0x2e

    aput-object v1, v0, v2

    const-string v1, "%2F"

    const/16 v2, 0x2f

    aput-object v1, v0, v2

    const-string v1, "0"

    const/16 v2, 0x30

    aput-object v1, v0, v2

    const-string v1, "1"

    const/16 v2, 0x31

    aput-object v1, v0, v2

    const-string v1, "2"

    const/16 v2, 0x32

    aput-object v1, v0, v2

    const-string v1, "3"

    const/16 v2, 0x33

    aput-object v1, v0, v2

    const-string v1, "4"

    const/16 v2, 0x34

    aput-object v1, v0, v2

    const-string v1, "5"

    const/16 v2, 0x35

    aput-object v1, v0, v2

    const-string v1, "6"

    const/16 v2, 0x36

    aput-object v1, v0, v2

    const-string v1, "7"

    const/16 v2, 0x37

    aput-object v1, v0, v2

    const-string v1, "8"

    const/16 v2, 0x38

    aput-object v1, v0, v2

    const-string v1, "9"

    const/16 v2, 0x39

    aput-object v1, v0, v2

    const-string v1, "%3A"

    const/16 v2, 0x3a

    aput-object v1, v0, v2

    const-string v1, "%3B"

    const/16 v2, 0x3b

    aput-object v1, v0, v2

    const-string v1, "%3C"

    const/16 v2, 0x3c

    aput-object v1, v0, v2

    const-string v1, "%3D"

    const/16 v2, 0x3d

    aput-object v1, v0, v2

    const-string v1, "%3E"

    const/16 v2, 0x3e

    aput-object v1, v0, v2

    const-string v1, "%3F"

    const/16 v2, 0x3f

    aput-object v1, v0, v2

    const-string v1, "%40"

    const/16 v2, 0x40

    aput-object v1, v0, v2

    const-string v1, "A"

    const/16 v2, 0x41

    aput-object v1, v0, v2

    const-string v1, "B"

    const/16 v2, 0x42

    aput-object v1, v0, v2

    const-string v1, "C"

    const/16 v2, 0x43

    aput-object v1, v0, v2

    const-string v1, "D"

    const/16 v2, 0x44

    aput-object v1, v0, v2

    const-string v1, "E"

    const/16 v2, 0x45

    aput-object v1, v0, v2

    const-string v1, "F"

    const/16 v2, 0x46

    aput-object v1, v0, v2

    const-string v1, "G"

    const/16 v2, 0x47

    aput-object v1, v0, v2

    const-string v1, "H"

    const/16 v2, 0x48

    aput-object v1, v0, v2

    const-string v1, "I"

    const/16 v2, 0x49

    aput-object v1, v0, v2

    const-string v1, "J"

    const/16 v2, 0x4a

    aput-object v1, v0, v2

    const-string v1, "K"

    const/16 v2, 0x4b

    aput-object v1, v0, v2

    const-string v1, "L"

    const/16 v2, 0x4c

    aput-object v1, v0, v2

    const-string v1, "M"

    const/16 v2, 0x4d

    aput-object v1, v0, v2

    const-string v1, "N"

    const/16 v2, 0x4e

    aput-object v1, v0, v2

    const-string v1, "O"

    const/16 v2, 0x4f

    aput-object v1, v0, v2

    const-string v1, "P"

    const/16 v2, 0x50

    aput-object v1, v0, v2

    const-string v1, "Q"

    const/16 v2, 0x51

    aput-object v1, v0, v2

    const-string v1, "R"

    const/16 v2, 0x52

    aput-object v1, v0, v2

    const-string v1, "S"

    const/16 v2, 0x53

    aput-object v1, v0, v2

    const-string v1, "T"

    const/16 v2, 0x54

    aput-object v1, v0, v2

    const-string v1, "U"

    const/16 v2, 0x55

    aput-object v1, v0, v2

    const-string v1, "V"

    const/16 v2, 0x56

    aput-object v1, v0, v2

    const-string v1, "W"

    const/16 v2, 0x57

    aput-object v1, v0, v2

    const-string v1, "X"

    const/16 v2, 0x58

    aput-object v1, v0, v2

    const-string v1, "Y"

    const/16 v2, 0x59

    aput-object v1, v0, v2

    const-string v1, "Z"

    const/16 v2, 0x5a

    aput-object v1, v0, v2

    const-string v1, "%5B"

    const/16 v2, 0x5b

    aput-object v1, v0, v2

    const-string v1, "%5C"

    const/16 v2, 0x5c

    aput-object v1, v0, v2

    const-string v1, "%5D"

    const/16 v2, 0x5d

    aput-object v1, v0, v2

    const-string v1, "%5E"

    const/16 v2, 0x5e

    aput-object v1, v0, v2

    const-string v1, "_"

    const/16 v2, 0x5f

    aput-object v1, v0, v2

    const-string v1, "%60"

    const/16 v2, 0x60

    aput-object v1, v0, v2

    const-string v1, "a"

    const/16 v2, 0x61

    aput-object v1, v0, v2

    const-string v1, "b"

    const/16 v2, 0x62

    aput-object v1, v0, v2

    const-string v1, "c"

    const/16 v2, 0x63

    aput-object v1, v0, v2

    const-string v1, "d"

    const/16 v2, 0x64

    aput-object v1, v0, v2

    const-string v1, "e"

    const/16 v2, 0x65

    aput-object v1, v0, v2

    const-string v1, "f"

    const/16 v2, 0x66

    aput-object v1, v0, v2

    const-string v1, "g"

    const/16 v2, 0x67

    aput-object v1, v0, v2

    const-string v1, "h"

    const/16 v2, 0x68

    aput-object v1, v0, v2

    const-string v1, "i"

    const/16 v2, 0x69

    aput-object v1, v0, v2

    const-string v1, "j"

    const/16 v2, 0x6a

    aput-object v1, v0, v2

    const-string v1, "k"

    const/16 v2, 0x6b

    aput-object v1, v0, v2

    const-string v1, "l"

    const/16 v2, 0x6c

    aput-object v1, v0, v2

    const-string v1, "m"

    const/16 v2, 0x6d

    aput-object v1, v0, v2

    const-string v1, "n"

    const/16 v2, 0x6e

    aput-object v1, v0, v2

    const-string v1, "o"

    const/16 v2, 0x6f

    aput-object v1, v0, v2

    const-string v1, "p"

    const/16 v2, 0x70

    aput-object v1, v0, v2

    const-string v1, "q"

    const/16 v2, 0x71

    aput-object v1, v0, v2

    const-string v1, "r"

    const/16 v2, 0x72

    aput-object v1, v0, v2

    const-string v1, "s"

    const/16 v2, 0x73

    aput-object v1, v0, v2

    const-string v1, "t"

    const/16 v2, 0x74

    aput-object v1, v0, v2

    const-string v1, "u"

    const/16 v2, 0x75

    aput-object v1, v0, v2

    const-string v1, "v"

    const/16 v2, 0x76

    aput-object v1, v0, v2

    const-string v1, "w"

    const/16 v2, 0x77

    aput-object v1, v0, v2

    const-string v1, "x"

    const/16 v2, 0x78

    aput-object v1, v0, v2

    const-string v1, "y"

    const/16 v2, 0x79

    aput-object v1, v0, v2

    const-string v1, "z"

    const/16 v2, 0x7a

    aput-object v1, v0, v2

    const-string v1, "%7B"

    const/16 v2, 0x7b

    aput-object v1, v0, v2

    const-string v1, "%7C"

    const/16 v2, 0x7c

    aput-object v1, v0, v2

    const-string v1, "%7D"

    const/16 v2, 0x7d

    aput-object v1, v0, v2

    const-string v1, "%7E"

    const/16 v2, 0x7e

    aput-object v1, v0, v2

    const-string v1, "%7F"

    const/16 v2, 0x7f

    aput-object v1, v0, v2

    const-string v1, "%80"

    const/16 v2, 0x80

    aput-object v1, v0, v2

    const-string v1, "%81"

    const/16 v2, 0x81

    aput-object v1, v0, v2

    const-string v1, "%82"

    const/16 v2, 0x82

    aput-object v1, v0, v2

    const-string v1, "%83"

    const/16 v2, 0x83

    aput-object v1, v0, v2

    const-string v1, "%84"

    const/16 v2, 0x84

    aput-object v1, v0, v2

    const-string v1, "%85"

    const/16 v2, 0x85

    aput-object v1, v0, v2

    const-string v1, "%86"

    const/16 v2, 0x86

    aput-object v1, v0, v2

    const-string v1, "%87"

    const/16 v2, 0x87

    aput-object v1, v0, v2

    const-string v1, "%88"

    const/16 v2, 0x88

    aput-object v1, v0, v2

    const-string v1, "%89"

    const/16 v2, 0x89

    aput-object v1, v0, v2

    const-string v1, "%8A"

    const/16 v2, 0x8a

    aput-object v1, v0, v2

    const-string v1, "%8B"

    const/16 v2, 0x8b

    aput-object v1, v0, v2

    const-string v1, "%8C"

    const/16 v2, 0x8c

    aput-object v1, v0, v2

    const-string v1, "%8D"

    const/16 v2, 0x8d

    aput-object v1, v0, v2

    const-string v1, "%8E"

    const/16 v2, 0x8e

    aput-object v1, v0, v2

    const-string v1, "%8F"

    const/16 v2, 0x8f

    aput-object v1, v0, v2

    const-string v1, "%90"

    const/16 v2, 0x90

    aput-object v1, v0, v2

    const-string v1, "%91"

    const/16 v2, 0x91

    aput-object v1, v0, v2

    const-string v1, "%92"

    const/16 v2, 0x92

    aput-object v1, v0, v2

    const-string v1, "%93"

    const/16 v2, 0x93

    aput-object v1, v0, v2

    const-string v1, "%94"

    const/16 v2, 0x94

    aput-object v1, v0, v2

    const-string v1, "%95"

    const/16 v2, 0x95

    aput-object v1, v0, v2

    const-string v1, "%96"

    const/16 v2, 0x96

    aput-object v1, v0, v2

    const-string v1, "%97"

    const/16 v2, 0x97

    aput-object v1, v0, v2

    const-string v1, "%98"

    const/16 v2, 0x98

    aput-object v1, v0, v2

    const-string v1, "%99"

    const/16 v2, 0x99

    aput-object v1, v0, v2

    const-string v1, "%9A"

    const/16 v2, 0x9a

    aput-object v1, v0, v2

    const-string v1, "%9B"

    const/16 v2, 0x9b

    aput-object v1, v0, v2

    const-string v1, "%9C"

    const/16 v2, 0x9c

    aput-object v1, v0, v2

    const-string v1, "%9D"

    const/16 v2, 0x9d

    aput-object v1, v0, v2

    const-string v1, "%9E"

    const/16 v2, 0x9e

    aput-object v1, v0, v2

    const-string v1, "%9F"

    const/16 v2, 0x9f

    aput-object v1, v0, v2

    const-string v1, "%A0"

    const/16 v2, 0xa0

    aput-object v1, v0, v2

    const-string v1, "%A1"

    const/16 v2, 0xa1

    aput-object v1, v0, v2

    const-string v1, "%A2"

    const/16 v2, 0xa2

    aput-object v1, v0, v2

    const-string v1, "%A3"

    const/16 v2, 0xa3

    aput-object v1, v0, v2

    const-string v1, "%A4"

    const/16 v2, 0xa4

    aput-object v1, v0, v2

    const-string v1, "%A5"

    const/16 v2, 0xa5

    aput-object v1, v0, v2

    const-string v1, "%A6"

    const/16 v2, 0xa6

    aput-object v1, v0, v2

    const-string v1, "%A7"

    const/16 v2, 0xa7

    aput-object v1, v0, v2

    const-string v1, "%A8"

    const/16 v2, 0xa8

    aput-object v1, v0, v2

    const-string v1, "%A9"

    const/16 v2, 0xa9

    aput-object v1, v0, v2

    const-string v1, "%AA"

    const/16 v2, 0xaa

    aput-object v1, v0, v2

    const-string v1, "%AB"

    const/16 v2, 0xab

    aput-object v1, v0, v2

    const-string v1, "%AC"

    const/16 v2, 0xac

    aput-object v1, v0, v2

    const-string v1, "%AD"

    const/16 v2, 0xad

    aput-object v1, v0, v2

    const-string v1, "%AE"

    const/16 v2, 0xae

    aput-object v1, v0, v2

    const-string v1, "%AF"

    const/16 v2, 0xaf

    aput-object v1, v0, v2

    const-string v1, "%B0"

    const/16 v2, 0xb0

    aput-object v1, v0, v2

    const-string v1, "%B1"

    const/16 v2, 0xb1

    aput-object v1, v0, v2

    const-string v1, "%B2"

    const/16 v2, 0xb2

    aput-object v1, v0, v2

    const-string v1, "%B3"

    const/16 v2, 0xb3

    aput-object v1, v0, v2

    const-string v1, "%B4"

    const/16 v2, 0xb4

    aput-object v1, v0, v2

    const-string v1, "%B5"

    const/16 v2, 0xb5

    aput-object v1, v0, v2

    const-string v1, "%B6"

    const/16 v2, 0xb6

    aput-object v1, v0, v2

    const-string v1, "%B7"

    const/16 v2, 0xb7

    aput-object v1, v0, v2

    const-string v1, "%B8"

    const/16 v2, 0xb8

    aput-object v1, v0, v2

    const-string v1, "%B9"

    const/16 v2, 0xb9

    aput-object v1, v0, v2

    const-string v1, "%BA"

    const/16 v2, 0xba

    aput-object v1, v0, v2

    const-string v1, "%BB"

    const/16 v2, 0xbb

    aput-object v1, v0, v2

    const-string v1, "%BC"

    const/16 v2, 0xbc

    aput-object v1, v0, v2

    const-string v1, "%BD"

    const/16 v2, 0xbd

    aput-object v1, v0, v2

    const-string v1, "%BE"

    const/16 v2, 0xbe

    aput-object v1, v0, v2

    const-string v1, "%BF"

    const/16 v2, 0xbf

    aput-object v1, v0, v2

    const-string v1, "%C0"

    const/16 v2, 0xc0

    aput-object v1, v0, v2

    const-string v1, "%C1"

    const/16 v2, 0xc1

    aput-object v1, v0, v2

    const-string v1, "%C2"

    const/16 v2, 0xc2

    aput-object v1, v0, v2

    const-string v1, "%C3"

    const/16 v2, 0xc3

    aput-object v1, v0, v2

    const-string v1, "%C4"

    const/16 v2, 0xc4

    aput-object v1, v0, v2

    const-string v1, "%C5"

    const/16 v2, 0xc5

    aput-object v1, v0, v2

    const-string v1, "%C6"

    const/16 v2, 0xc6

    aput-object v1, v0, v2

    const-string v1, "%C7"

    const/16 v2, 0xc7

    aput-object v1, v0, v2

    const-string v1, "%C8"

    const/16 v2, 0xc8

    aput-object v1, v0, v2

    const-string v1, "%C9"

    const/16 v2, 0xc9

    aput-object v1, v0, v2

    const-string v1, "%CA"

    const/16 v2, 0xca

    aput-object v1, v0, v2

    const-string v1, "%CB"

    const/16 v2, 0xcb

    aput-object v1, v0, v2

    const-string v1, "%CC"

    const/16 v2, 0xcc

    aput-object v1, v0, v2

    const-string v1, "%CD"

    const/16 v2, 0xcd

    aput-object v1, v0, v2

    const-string v1, "%CE"

    const/16 v2, 0xce

    aput-object v1, v0, v2

    const-string v1, "%CF"

    const/16 v2, 0xcf

    aput-object v1, v0, v2

    const-string v1, "%D0"

    const/16 v2, 0xd0

    aput-object v1, v0, v2

    const-string v1, "%D1"

    const/16 v2, 0xd1

    aput-object v1, v0, v2

    const-string v1, "%D2"

    const/16 v2, 0xd2

    aput-object v1, v0, v2

    const-string v1, "%D3"

    const/16 v2, 0xd3

    aput-object v1, v0, v2

    const-string v1, "%D4"

    const/16 v2, 0xd4

    aput-object v1, v0, v2

    const-string v1, "%D5"

    const/16 v2, 0xd5

    aput-object v1, v0, v2

    const-string v1, "%D6"

    const/16 v2, 0xd6

    aput-object v1, v0, v2

    const-string v1, "%D7"

    const/16 v2, 0xd7

    aput-object v1, v0, v2

    const-string v1, "%D8"

    const/16 v2, 0xd8

    aput-object v1, v0, v2

    const-string v1, "%D9"

    const/16 v2, 0xd9

    aput-object v1, v0, v2

    const-string v1, "%DA"

    const/16 v2, 0xda

    aput-object v1, v0, v2

    const-string v1, "%DB"

    const/16 v2, 0xdb

    aput-object v1, v0, v2

    const-string v1, "%DC"

    const/16 v2, 0xdc

    aput-object v1, v0, v2

    const-string v1, "%DD"

    const/16 v2, 0xdd

    aput-object v1, v0, v2

    const-string v1, "%DE"

    const/16 v2, 0xde

    aput-object v1, v0, v2

    const-string v1, "%DF"

    const/16 v2, 0xdf

    aput-object v1, v0, v2

    const-string v1, "%E0"

    const/16 v2, 0xe0

    aput-object v1, v0, v2

    const-string v1, "%E1"

    const/16 v2, 0xe1

    aput-object v1, v0, v2

    const-string v1, "%E2"

    const/16 v2, 0xe2

    aput-object v1, v0, v2

    const-string v1, "%E3"

    const/16 v2, 0xe3

    aput-object v1, v0, v2

    const-string v1, "%E4"

    const/16 v2, 0xe4

    aput-object v1, v0, v2

    const-string v1, "%E5"

    const/16 v2, 0xe5

    aput-object v1, v0, v2

    const-string v1, "%E6"

    const/16 v2, 0xe6

    aput-object v1, v0, v2

    const-string v1, "%E7"

    const/16 v2, 0xe7

    aput-object v1, v0, v2

    const-string v1, "%E8"

    const/16 v2, 0xe8

    aput-object v1, v0, v2

    const-string v1, "%E9"

    const/16 v2, 0xe9

    aput-object v1, v0, v2

    const-string v1, "%EA"

    const/16 v2, 0xea

    aput-object v1, v0, v2

    const-string v1, "%EB"

    const/16 v2, 0xeb

    aput-object v1, v0, v2

    const-string v1, "%EC"

    const/16 v2, 0xec

    aput-object v1, v0, v2

    const-string v1, "%ED"

    const/16 v2, 0xed

    aput-object v1, v0, v2

    const-string v1, "%EE"

    const/16 v2, 0xee

    aput-object v1, v0, v2

    const-string v1, "%EF"

    const/16 v2, 0xef

    aput-object v1, v0, v2

    const-string v1, "%F0"

    const/16 v2, 0xf0

    aput-object v1, v0, v2

    const-string v1, "%F1"

    const/16 v2, 0xf1

    aput-object v1, v0, v2

    const-string v1, "%F2"

    const/16 v2, 0xf2

    aput-object v1, v0, v2

    const-string v1, "%F3"

    const/16 v2, 0xf3

    aput-object v1, v0, v2

    const-string v1, "%F4"

    const/16 v2, 0xf4

    aput-object v1, v0, v2

    const-string v1, "%F5"

    const/16 v2, 0xf5

    aput-object v1, v0, v2

    const-string v1, "%F6"

    const/16 v2, 0xf6

    aput-object v1, v0, v2

    const-string v1, "%F7"

    const/16 v2, 0xf7

    aput-object v1, v0, v2

    const-string v1, "%F8"

    const/16 v2, 0xf8

    aput-object v1, v0, v2

    const-string v1, "%F9"

    const/16 v2, 0xf9

    aput-object v1, v0, v2

    const-string v1, "%FA"

    const/16 v2, 0xfa

    aput-object v1, v0, v2

    const-string v1, "%FB"

    const/16 v2, 0xfb

    aput-object v1, v0, v2

    const-string v1, "%FC"

    const/16 v2, 0xfc

    aput-object v1, v0, v2

    const-string v1, "%FD"

    const/16 v2, 0xfd

    aput-object v1, v0, v2

    const-string v1, "%FE"

    const/16 v2, 0xfe

    aput-object v1, v0, v2

    const-string v1, "%FF"

    const/16 v2, 0xff

    aput-object v1, v0, v2

    sput-object v0, Lcom/adobe/mobile/StaticMethods;->encodedChars:[Ljava/lang/String;

    .line 117
    const/16 v0, 0x100

    new-array v0, v0, [Z

    fill-array-data v0, :array_73e

    sput-object v0, Lcom/adobe/mobile/StaticMethods;->utf8Mask:[Z

    .line 136
    const/16 v0, 0x100

    new-array v0, v0, [Z

    fill-array-data v0, :array_7c2

    sput-object v0, Lcom/adobe/mobile/StaticMethods;->contextDataMask:[Z

    .line 157
    sget-object v0, Lcom/adobe/mobile/Config$ApplicationType;->APPLICATION_TYPE_HANDHELD:Lcom/adobe/mobile/Config$ApplicationType;

    sput-object v0, Lcom/adobe/mobile/StaticMethods;->_appType:Lcom/adobe/mobile/Config$ApplicationType;

    .line 158
    const/4 v0, 0x0

    sput-boolean v0, Lcom/adobe/mobile/StaticMethods;->_isWearable:Z

    .line 180
    const/4 v0, 0x0

    sput-object v0, Lcom/adobe/mobile/StaticMethods;->prefs:Landroid/content/SharedPreferences;

    .line 181
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/adobe/mobile/StaticMethods;->_sharedPreferencesMutex:Ljava/lang/Object;

    .line 199
    const/4 v0, 0x0

    sput-object v0, Lcom/adobe/mobile/StaticMethods;->appID:Ljava/lang/String;

    .line 200
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/adobe/mobile/StaticMethods;->_applicationIDMutex:Ljava/lang/Object;

    .line 221
    const/4 v0, 0x0

    sput-object v0, Lcom/adobe/mobile/StaticMethods;->appName:Ljava/lang/String;

    .line 222
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/adobe/mobile/StaticMethods;->_applicationNameMutex:Ljava/lang/Object;

    .line 256
    const/4 v0, 0x0

    sput-object v0, Lcom/adobe/mobile/StaticMethods;->versionName:Ljava/lang/String;

    .line 257
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/adobe/mobile/StaticMethods;->_applicationVersionMutex:Ljava/lang/Object;

    .line 290
    const/4 v0, -0x1

    sput v0, Lcom/adobe/mobile/StaticMethods;->versionCode:I

    .line 291
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/adobe/mobile/StaticMethods;->_applicationVersionCodeMutex:Ljava/lang/Object;

    .line 324
    const/4 v0, 0x0

    sput-object v0, Lcom/adobe/mobile/StaticMethods;->visitorID:Ljava/lang/String;

    .line 325
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/adobe/mobile/StaticMethods;->_visitorIDMutex:Ljava/lang/Object;

    .line 340
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/adobe/mobile/StaticMethods;->_userIdentifierMutex:Ljava/lang/Object;

    .line 355
    const/4 v0, 0x0

    sput-boolean v0, Lcom/adobe/mobile/StaticMethods;->pushEnabled:Z

    .line 356
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/adobe/mobile/StaticMethods;->_pushEnabledMutex:Ljava/lang/Object;

    .line 388
    const/4 v0, 0x0

    sput-object v0, Lcom/adobe/mobile/StaticMethods;->pushIdentifier:Ljava/lang/String;

    .line 389
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/adobe/mobile/StaticMethods;->_pushIdentifierMutex:Ljava/lang/Object;

    .line 418
    const/4 v0, 0x0

    sput-object v0, Lcom/adobe/mobile/StaticMethods;->advertisingIdentifier:Ljava/lang/String;

    .line 419
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/adobe/mobile/StaticMethods;->_advertisingIdentifierMutex:Ljava/lang/Object;

    .line 440
    const/4 v0, 0x0

    sput-object v0, Lcom/adobe/mobile/StaticMethods;->userAgent:Ljava/lang/String;

    .line 441
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/adobe/mobile/StaticMethods;->_userAgentMutex:Ljava/lang/Object;

    .line 453
    const/4 v0, 0x0

    sput-object v0, Lcom/adobe/mobile/StaticMethods;->defaultData:Ljava/util/HashMap;

    .line 454
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/adobe/mobile/StaticMethods;->_defaultDataMutex:Ljava/lang/Object;

    .line 472
    const/4 v0, 0x0

    sput-object v0, Lcom/adobe/mobile/StaticMethods;->resolution:Ljava/lang/String;

    .line 473
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/adobe/mobile/StaticMethods;->_resolutionMutex:Ljava/lang/Object;

    .line 489
    const/4 v0, 0x0

    sput-object v0, Lcom/adobe/mobile/StaticMethods;->carrier:Ljava/lang/String;

    .line 490
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/adobe/mobile/StaticMethods;->_carrierMutex:Ljava/lang/Object;

    .line 506
    const/4 v0, 0x0

    sput-object v0, Lcom/adobe/mobile/StaticMethods;->operatingSystem:Ljava/lang/String;

    .line 507
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/adobe/mobile/StaticMethods;->_operatingSystemMutex:Ljava/lang/Object;

    .line 531
    const/4 v0, 0x0

    sput-object v0, Lcom/adobe/mobile/StaticMethods;->timestamp:Ljava/lang/String;

    .line 532
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/adobe/mobile/StaticMethods;->_timestampMutex:Ljava/lang/Object;

    .line 557
    const/4 v0, 0x0

    sput-object v0, Lcom/adobe/mobile/StaticMethods;->mediaExecutor:Ljava/util/concurrent/ExecutorService;

    .line 558
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/adobe/mobile/StaticMethods;->_mediaExecutorMutex:Ljava/lang/Object;

    .line 569
    const/4 v0, 0x0

    sput-object v0, Lcom/adobe/mobile/StaticMethods;->timedActionsExecutor:Ljava/util/concurrent/ExecutorService;

    .line 570
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/adobe/mobile/StaticMethods;->_timedActionsExecutorMutex:Ljava/lang/Object;

    .line 581
    const/4 v0, 0x0

    sput-object v0, Lcom/adobe/mobile/StaticMethods;->sharedExecutor:Ljava/util/concurrent/ExecutorService;

    .line 582
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/adobe/mobile/StaticMethods;->_sharedExecutorMutex:Ljava/lang/Object;

    .line 593
    const/4 v0, 0x0

    sput-object v0, Lcom/adobe/mobile/StaticMethods;->analyticsExecutor:Ljava/util/concurrent/ExecutorService;

    .line 594
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/adobe/mobile/StaticMethods;->_analyticsExecutorMutex:Ljava/lang/Object;

    .line 605
    const/4 v0, 0x0

    sput-object v0, Lcom/adobe/mobile/StaticMethods;->messagesExecutor:Ljava/util/concurrent/ExecutorService;

    .line 606
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/adobe/mobile/StaticMethods;->_messagesExecutorMutex:Ljava/lang/Object;

    .line 617
    const/4 v0, 0x0

    sput-object v0, Lcom/adobe/mobile/StaticMethods;->thirdPartyCallbacksExecutor:Ljava/util/concurrent/ExecutorService;

    .line 618
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/adobe/mobile/StaticMethods;->_thirdPartyCallbacksExecutorMutex:Ljava/lang/Object;

    .line 628
    const/4 v0, 0x0

    sput-object v0, Lcom/adobe/mobile/StaticMethods;->messageImageCachingExecutor:Ljava/util/concurrent/ExecutorService;

    .line 629
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/adobe/mobile/StaticMethods;->_messageImageCachingExecutorMutex:Ljava/lang/Object;

    .line 640
    const/4 v0, 0x0

    sput-object v0, Lcom/adobe/mobile/StaticMethods;->audienceExecutor:Ljava/util/concurrent/ExecutorService;

    .line 641
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/adobe/mobile/StaticMethods;->_audienceExecutorMutex:Ljava/lang/Object;

    .line 652
    const/4 v0, 0x0

    sput-object v0, Lcom/adobe/mobile/StaticMethods;->piiExecutor:Ljava/util/concurrent/ExecutorService;

    .line 653
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/adobe/mobile/StaticMethods;->_piiExecutorMutex:Ljava/lang/Object;

    .line 664
    const/4 v0, 0x0

    sput-object v0, Lcom/adobe/mobile/StaticMethods;->aid:Ljava/lang/String;

    .line 665
    const/4 v0, 0x0

    sput-boolean v0, Lcom/adobe/mobile/StaticMethods;->_aidDone:Z

    .line 666
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/adobe/mobile/StaticMethods;->_aidMutex:Ljava/lang/Object;

    .line 1036
    new-instance v0, Ljava/util/HashMap;

    const/16 v1, 0x100

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    sput-object v0, Lcom/adobe/mobile/StaticMethods;->_contextDataKeyWhiteList:Ljava/util/Map;

    .line 1037
    const/4 v0, 0x0

    sput v0, Lcom/adobe/mobile/StaticMethods;->_contextDataKeyWhiteListCount:I

    .line 1208
    const/4 v0, 0x0

    sput-object v0, Lcom/adobe/mobile/StaticMethods;->sharedContext:Landroid/content/Context;

    .line 1255
    const/4 v0, 0x0

    sput-object v0, Lcom/adobe/mobile/StaticMethods;->_activity:Ljava/lang/ref/WeakReference;

    .line 1256
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/adobe/mobile/StaticMethods;->_currentActivityMutex:Ljava/lang/Object;

    .line 1296
    const-string v0, "000102030405060708090A0B0C0D0E0F101112131415161718191A1B1C1D1E1F202122232425262728292A2B2C2D2E2F303132333435363738393A3B3C3D3E3F404142434445464748494A4B4C4D4E4F505152535455565758595A5B5C5D5E5F606162636465666768696A6B6C6D6E6F707172737475767778797A7B7C7D7E7F808182838485868788898A8B8C8D8E8F909192939495969798999A9B9C9D9E9FA0A1A2A3A4A5A6A7A8A9AAABACADAEAFB0B1B2B3B4B5B6B7B8B9BABBBCBDBEBFC0C1C2C3C4C5C6C7C8C9CACBCCCDCECFD0D1D2D3D4D5D6D7D8D9DADBDCDDDEDFE0E1E2E3E4E5E6E7E8E9EAEBECEDEEEFF0F1F2F3F4F5F6F7F8F9FAFBFCFDFEFF"

    .line 1312
    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    sput-object v0, Lcom/adobe/mobile/StaticMethods;->BYTE_TO_HEX:[C

    .line 1296
    return-void

    nop

    :array_73e
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x0t
        0x0t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    :array_7c2
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data
.end method

.method constructor <init>()V
    .registers 1

    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method protected static URLEncode(Ljava/lang/String;)Ljava/lang/String;
    .registers 8
    .param p0, "unencodedString"    # Ljava/lang/String;

    .line 909
    if-nez p0, :cond_4

    .line 910
    const/4 v0, 0x0

    return-object v0

    .line 914
    :cond_4
    const-string v0, "UTF-8"

    :try_start_6
    invoke-virtual {p0, v0}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v3

    .line 916
    .local v3, "stringBytes":[B
    array-length v4, v3

    .line 917
    .local v4, "len":I
    const/4 v5, 0x0

    .line 920
    .local v5, "curIndex":I
    :goto_c
    if-ge v5, v4, :cond_1b

    sget-object v0, Lcom/adobe/mobile/StaticMethods;->utf8Mask:[Z

    aget-byte v1, v3, v5

    and-int/lit16 v1, v1, 0xff

    aget-boolean v0, v0, v1
    :try_end_16
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_6 .. :try_end_16} :catch_48

    if-eqz v0, :cond_1b

    .line 921
    add-int/lit8 v5, v5, 0x1

    goto :goto_c

    .line 926
    :cond_1b
    if-ne v5, v4, :cond_1e

    .line 927
    return-object p0

    .line 931
    :cond_1e
    :try_start_1e
    new-instance v6, Ljava/lang/StringBuilder;

    array-length v0, v3

    shl-int/lit8 v0, v0, 0x1

    invoke-direct {v6, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 934
    .local v6, "encodedString":Ljava/lang/StringBuilder;
    if-lez v5, :cond_33

    .line 935
    new-instance v0, Ljava/lang/String;

    const-string v1, "UTF-8"

    const/4 v2, 0x0

    invoke-direct {v0, v3, v2, v5, v1}, Ljava/lang/String;-><init>([BIILjava/lang/String;)V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 939
    :cond_33
    :goto_33
    if-ge v5, v4, :cond_43

    .line 940
    sget-object v0, Lcom/adobe/mobile/StaticMethods;->encodedChars:[Ljava/lang/String;

    aget-byte v1, v3, v5

    and-int/lit16 v1, v1, 0xff

    aget-object v0, v0, v1

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 939
    add-int/lit8 v5, v5, 0x1

    goto :goto_33

    .line 944
    :cond_43
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_46
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1e .. :try_end_46} :catch_48

    move-result-object v0

    return-object v0

    .line 945
    .end local v3    # "stringBytes":[B
    .end local v4    # "len":I
    .end local v5    # "curIndex":I
    .end local v6    # "encodedString":Ljava/lang/StringBuilder;
    :catch_48
    move-exception v3

    .line 946
    .local v3, "e":Ljava/io/UnsupportedEncodingException;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "UnsupportedEncodingException : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v3}, Ljava/io/UnsupportedEncodingException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logWarningFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 947
    const/4 v0, 0x0

    return-object v0
.end method

.method private static addValueToHashMap(Ljava/lang/Object;Lcom/adobe/mobile/ContextData;Ljava/util/List;I)V
    .registers 8
    .param p0, "object"    # Ljava/lang/Object;
    .param p1, "table"    # Lcom/adobe/mobile/ContextData;
    .param p2, "subKeyArray"    # Ljava/util/List;
    .param p3, "index"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/lang/Object;Lcom/adobe/mobile/ContextData;Ljava/util/List<Ljava/lang/String;>;I)V"
        }
    .end annotation

    .line 1117
    if-eqz p1, :cond_4

    if-nez p2, :cond_5

    .line 1118
    :cond_4
    return-void

    .line 1121
    :cond_5
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v1

    .line 1122
    .local v1, "arrayCount":I
    if-ge p3, v1, :cond_13

    invoke-interface {p2, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Ljava/lang/String;

    goto :goto_14

    :cond_13
    const/4 v2, 0x0

    .line 1123
    .local v2, "keyName":Ljava/lang/String;
    :goto_14
    if-nez v2, :cond_17

    .line 1124
    return-void

    .line 1127
    :cond_17
    new-instance v3, Lcom/adobe/mobile/ContextData;

    invoke-direct {v3}, Lcom/adobe/mobile/ContextData;-><init>()V

    .line 1128
    .local v3, "data":Lcom/adobe/mobile/ContextData;
    invoke-virtual {p1, v2}, Lcom/adobe/mobile/ContextData;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_26

    .line 1129
    invoke-virtual {p1, v2}, Lcom/adobe/mobile/ContextData;->get(Ljava/lang/String;)Lcom/adobe/mobile/ContextData;

    move-result-object v3

    .line 1132
    :cond_26
    add-int/lit8 v0, v1, -0x1

    if-ne v0, p3, :cond_30

    .line 1134
    iput-object p0, v3, Lcom/adobe/mobile/ContextData;->value:Ljava/lang/Object;

    .line 1135
    invoke-virtual {p1, v2, v3}, Lcom/adobe/mobile/ContextData;->put(Ljava/lang/String;Lcom/adobe/mobile/ContextData;)V

    goto :goto_38

    .line 1138
    :cond_30
    invoke-virtual {p1, v2, v3}, Lcom/adobe/mobile/ContextData;->put(Ljava/lang/String;Lcom/adobe/mobile/ContextData;)V

    .line 1139
    add-int/lit8 p3, p3, 0x1

    invoke-static {p0, v3, p2, p3}, Lcom/adobe/mobile/StaticMethods;->addValueToHashMap(Ljava/lang/Object;Lcom/adobe/mobile/ContextData;Ljava/util/List;I)V

    .line 1141
    :goto_38
    return-void
.end method

.method protected static appendContextData(Ljava/util/Map;Ljava/lang/String;)Ljava/lang/String;
    .registers 17
    .param p0, "referrerData"    # Ljava/util/Map;
    .param p1, "source"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;Ljava/lang/String;)Ljava/lang/String;"
        }
    .end annotation

    .line 1364
    if-eqz p1, :cond_a

    if-eqz p0, :cond_a

    invoke-interface {p0}, Ljava/util/Map;->size()I

    move-result v0

    if-nez v0, :cond_b

    .line 1365
    :cond_a
    return-object p1

    .line 1367
    :cond_b
    const-string v0, ".*(&c\\.(.*)&\\.c).*"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v3

    .line 1368
    .local v3, "pattern":Ljava/util/regex/Pattern;
    move-object/from16 v0, p1

    invoke-virtual {v3, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v4

    .line 1369
    .local v4, "matcher":Ljava/util/regex/Matcher;
    invoke-virtual {v4}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    if-nez v0, :cond_1e

    .line 1370
    return-object p1

    .line 1372
    :cond_1e
    const/4 v0, 0x2

    invoke-virtual {v4, v0}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v5

    .line 1373
    .local v5, "contextDataString":Ljava/lang/String;
    if-nez v5, :cond_26

    .line 1374
    return-object p1

    .line 1377
    :cond_26
    new-instance v6, Ljava/util/HashMap;

    const/16 v0, 0x40

    invoke-direct {v6, v0}, Ljava/util/HashMap;-><init>(I)V

    .line 1378
    .local v6, "contextData":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    new-instance v7, Ljava/util/ArrayList;

    const/16 v0, 0x10

    invoke-direct {v7, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 1379
    .local v7, "keyPath":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string v0, "&"

    invoke-virtual {v5, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v8

    array-length v9, v8

    const/4 v10, 0x0

    :goto_3c
    if-ge v10, v9, :cond_93

    aget-object v11, v8, v10

    .line 1380
    .local v11, "param":Ljava/lang/String;
    const-string v0, "."

    invoke-virtual {v11, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_54

    const-string v0, "="

    invoke-virtual {v11, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_54

    .line 1381
    invoke-interface {v7, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_8f

    .line 1383
    :cond_54
    const-string v0, "."

    invoke-virtual {v11, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6c

    .line 1384
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_8f

    .line 1385
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-interface {v7, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    goto :goto_8f

    .line 1388
    :cond_6c
    const-string v0, "="

    invoke-virtual {v11, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v12

    .line 1389
    .local v12, "kvpair":[Ljava/lang/String;
    array-length v0, v12

    const/4 v1, 0x2

    if-eq v0, v1, :cond_77

    .line 1390
    goto :goto_8f

    .line 1392
    :cond_77
    const/4 v0, 0x0

    aget-object v0, v12, v0

    invoke-static {v7, v0}, Lcom/adobe/mobile/StaticMethods;->contextDataStringPath(Ljava/util/List;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    .line 1394
    .local v13, "contextDataKey":Ljava/lang/String;
    const/4 v0, 0x1

    :try_start_7f
    aget-object v0, v12, v0

    const-string v1, "UTF-8"

    invoke-static {v0, v1}, Ljava/net/URLDecoder;->decode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v6, v13, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_8a
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_7f .. :try_end_8a} :catch_8b

    .line 1397
    goto :goto_8f

    .line 1395
    :catch_8b
    move-exception v14

    .line 1396
    .local v14, "e":Ljava/io/UnsupportedEncodingException;
    invoke-virtual {v14}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    .line 1379
    .end local v11    # "param":Ljava/lang/String;
    .end local v12    # "kvpair":[Ljava/lang/String;
    .end local v13    # "contextDataKey":Ljava/lang/String;
    .end local v14    # "e":Ljava/io/UnsupportedEncodingException;
    :cond_8f
    :goto_8f
    add-int/lit8 v10, v10, 0x1

    goto/16 :goto_3c

    .line 1401
    :cond_93
    invoke-interface {v6, p0}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 1403
    new-instance v8, Ljava/lang/StringBuilder;

    const/4 v0, 0x1

    invoke-virtual {v4, v0}, Ljava/util/regex/Matcher;->start(I)I

    move-result v0

    move-object/from16 v1, p1

    const/4 v2, 0x0

    invoke-virtual {v1, v2, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v8, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1404
    .local v8, "urlSb":Ljava/lang/StringBuilder;
    new-instance v9, Ljava/util/HashMap;

    invoke-direct {v9}, Ljava/util/HashMap;-><init>()V

    .line 1405
    .local v9, "contextMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    const-string v0, "c"

    invoke-static {v6}, Lcom/adobe/mobile/StaticMethods;->translateContextData(Ljava/util/Map;)Lcom/adobe/mobile/ContextData;

    move-result-object v1

    invoke-virtual {v9, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1406
    invoke-static {v9, v8}, Lcom/adobe/mobile/StaticMethods;->serializeToQueryString(Ljava/util/Map;Ljava/lang/StringBuilder;)V

    .line 1407
    const/4 v0, 0x1

    invoke-virtual {v4, v0}, Ljava/util/regex/Matcher;->end(I)I

    move-result v0

    move-object/from16 v1, p1

    invoke-virtual {v1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1408
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected static cleanContextDataDictionary(Ljava/util/Map;)Ljava/util/Map;
    .registers 6
    .param p0, "dict"    # Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;)Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
        }
    .end annotation

    .line 1015
    if-nez p0, :cond_8

    .line 1016
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    return-object v0

    .line 1019
    :cond_8
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 1021
    .local v1, "tempContextData":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    invoke-interface {p0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_15
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_36

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Ljava/util/Map$Entry;

    .line 1022
    .local v3, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Object;>;"
    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/adobe/mobile/StaticMethods;->cleanContextDataKey(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1023
    .local v4, "cleanedKey":Ljava/lang/String;
    if-eqz v4, :cond_35

    .line 1024
    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v1, v4, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1026
    .end local v3    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Object;>;"
    .end local v3
    .end local v4    # "cleanedKey":Ljava/lang/String;
    :cond_35
    goto :goto_15

    .line 1028
    :cond_36
    return-object v1
.end method

.method protected static cleanContextDataKey(Ljava/lang/String;)Ljava/lang/String;
    .registers 15
    .param p0, "key"    # Ljava/lang/String;

    .line 1039
    if-nez p0, :cond_4

    .line 1040
    const/4 v0, 0x0

    return-object v0

    .line 1044
    :cond_4
    sget-object v4, Lcom/adobe/mobile/StaticMethods;->_contextDataKeyWhiteList:Ljava/util/Map;

    monitor-enter v4

    .line 1045
    :try_start_7
    sget-object v0, Lcom/adobe/mobile/StaticMethods;->_contextDataKeyWhiteList:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Ljava/lang/String;
    :try_end_10
    .catchall {:try_start_7 .. :try_end_10} :catchall_16

    .line 1048
    .local v5, "preCleanedKey":Ljava/lang/String;
    if-eqz v5, :cond_14

    .line 1049
    monitor-exit v4

    return-object v5

    .line 1051
    .end local v5    # "preCleanedKey":Ljava/lang/String;
    :cond_14
    monitor-exit v4

    goto :goto_19

    :catchall_16
    move-exception v6

    monitor-exit v4

    throw v6

    .line 1058
    :goto_19
    const-string v0, "UTF-8"

    :try_start_1b
    invoke-virtual {p0, v0}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v5

    .line 1059
    .local v5, "utf8Key":[B
    array-length v0, v5

    new-array v6, v0, [B

    .line 1061
    .local v6, "outPut":[B
    const/4 v7, 0x0

    .line 1062
    .local v7, "lastByte":B
    const/4 v8, 0x0

    .line 1065
    .local v8, "outIndex":I
    move-object v9, v5

    array-length v10, v9

    const/4 v11, 0x0

    :goto_27
    if-ge v11, v10, :cond_45

    aget-byte v12, v9, v11

    .line 1067
    .local v12, "curByte":B
    const/16 v0, 0x2e

    if-ne v12, v0, :cond_34

    const/16 v0, 0x2e

    if-ne v7, v0, :cond_34

    goto :goto_42

    .line 1070
    :cond_34
    sget-object v0, Lcom/adobe/mobile/StaticMethods;->contextDataMask:[Z

    and-int/lit16 v1, v12, 0xff

    aget-boolean v0, v0, v1

    if-eqz v0, :cond_42

    .line 1072
    move v0, v8

    add-int/lit8 v8, v8, 0x1

    aput-byte v12, v6, v0
    :try_end_41
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1b .. :try_end_41} :catch_6e

    .line 1073
    move v7, v12

    .line 1065
    .end local v12    # "curByte":B
    :cond_42
    :goto_42
    add-int/lit8 v11, v11, 0x1

    goto :goto_27

    .line 1078
    :cond_45
    if-nez v8, :cond_49

    .line 1079
    const/4 v0, 0x0

    return-object v0

    .line 1083
    :cond_49
    const/4 v0, 0x0

    :try_start_4a
    aget-byte v0, v6, v0

    const/16 v1, 0x2e

    if-ne v0, v1, :cond_52

    const/4 v9, 0x1

    goto :goto_53

    :cond_52
    const/4 v9, 0x0

    .line 1084
    .local v9, "startIndex":I
    :goto_53
    add-int/lit8 v0, v8, -0x1

    aget-byte v0, v6, v0
    :try_end_57
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_4a .. :try_end_57} :catch_6e

    const/16 v1, 0x2e

    if-ne v0, v1, :cond_5d

    const/4 v10, 0x1

    goto :goto_5e

    :cond_5d
    const/4 v10, 0x0

    .line 1085
    .local v10, "endTrim":I
    :goto_5e
    sub-int v0, v8, v10

    sub-int v11, v0, v9

    .line 1088
    .local v11, "totalLength":I
    if-gtz v11, :cond_66

    .line 1089
    const/4 v0, 0x0

    return-object v0

    .line 1093
    :cond_66
    :try_start_66
    new-instance v4, Ljava/lang/String;

    const-string v0, "UTF-8"

    invoke-direct {v4, v6, v9, v11, v0}, Ljava/lang/String;-><init>([BIILjava/lang/String;)V
    :try_end_6d
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_66 .. :try_end_6d} :catch_6e

    .line 1097
    .local v4, "cleanKey":Ljava/lang/String;
    .end local v5    # "utf8Key":[B
    .end local v6    # "outPut":[B
    .end local v7    # "lastByte":B
    .end local v8    # "outIndex":I
    .end local v9    # "startIndex":I
    .end local v10    # "endTrim":I
    .end local v11    # "totalLength":I
    goto :goto_80

    .line 1094
    .end local v4    # "cleanKey":Ljava/lang/String;
    :catch_6e
    move-exception v5

    .line 1095
    .local v5, "ex":Ljava/io/UnsupportedEncodingException;
    const-string v0, "Analytics - Unable to clean context data key (%s)"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v5}, Ljava/io/UnsupportedEncodingException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logErrorFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1096
    const/4 v0, 0x0

    return-object v0

    .line 1100
    .local v4, "cleanKey":Ljava/lang/String;
    .end local v5    # "ex":Ljava/io/UnsupportedEncodingException;
    :goto_80
    sget-object v5, Lcom/adobe/mobile/StaticMethods;->_contextDataKeyWhiteList:Ljava/util/Map;

    monitor-enter v5

    .line 1102
    :try_start_83
    sget v0, Lcom/adobe/mobile/StaticMethods;->_contextDataKeyWhiteListCount:I

    const/16 v1, 0xfa

    if-le v0, v1, :cond_91

    .line 1104
    sget-object v0, Lcom/adobe/mobile/StaticMethods;->_contextDataKeyWhiteList:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 1105
    const/4 v0, 0x0

    sput v0, Lcom/adobe/mobile/StaticMethods;->_contextDataKeyWhiteListCount:I

    .line 1109
    :cond_91
    sget-object v0, Lcom/adobe/mobile/StaticMethods;->_contextDataKeyWhiteList:Ljava/util/Map;

    invoke-interface {v0, p0, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1110
    sget v0, Lcom/adobe/mobile/StaticMethods;->_contextDataKeyWhiteListCount:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/adobe/mobile/StaticMethods;->_contextDataKeyWhiteListCount:I
    :try_end_9c
    .catchall {:try_start_83 .. :try_end_9c} :catchall_9e

    .line 1111
    monitor-exit v5

    goto :goto_a1

    :catchall_9e
    move-exception v13

    monitor-exit v5

    throw v13

    .line 1113
    :goto_a1
    return-object v4
.end method

.method protected static contextDataStringPath(Ljava/util/List;Ljava/lang/String;)Ljava/lang/String;
    .registers 6
    .param p0, "keyPath"    # Ljava/util/List;
    .param p1, "lastComponent"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<Ljava/lang/String;>;Ljava/lang/String;)Ljava/lang/String;"
        }
    .end annotation

    .line 1413
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 1414
    .local v1, "sb":Ljava/lang/StringBuilder;
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_9
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1a

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Ljava/lang/String;

    .line 1415
    .local v3, "pathComponent":Ljava/lang/String;
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1416
    .end local v3    # "pathComponent":Ljava/lang/String;
    goto :goto_9

    .line 1417
    :cond_1a
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1418
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected static expandTokens(Ljava/lang/String;Ljava/util/Map;)Ljava/lang/String;
    .registers 9
    .param p0, "inputString"    # Ljava/lang/String;
    .param p1, "tokens"    # Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/lang/String;Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;)Ljava/lang/String;"
        }
    .end annotation

    .line 1273
    move-object v4, p0

    .line 1276
    .local v4, "returnString":Ljava/lang/String;
    :try_start_1
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_9
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_28

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Ljava/util/Map$Entry;

    .line 1277
    .local v6, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-interface {v6}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-interface {v6}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v4, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
    :try_end_25
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_25} :catch_29

    move-result-object v0

    move-object v4, v0

    .line 1278
    .end local v6    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v6
    goto :goto_9

    .line 1282
    :cond_28
    goto :goto_39

    .line 1280
    :catch_29
    move-exception v5

    .line 1281
    .local v5, "ex":Ljava/lang/Exception;
    const-string v0, "Unable to expand tokens (%s)"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v5}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logDebugFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1284
    .end local v5    # "ex":Ljava/lang/Exception;
    :goto_39
    return-object v4
.end method

.method private static generateAID()Ljava/lang/String;
    .registers 11

    .line 786
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "-"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    .line 787
    .local v3, "uuid":Ljava/lang/String;
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v3, v0}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v3

    .line 789
    const-string v0, "^[89A-F]"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v4

    .line 790
    .local v4, "firstPattern":Ljava/util/regex/Pattern;
    const-string v0, "^[4-9A-F]"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v5

    .line 791
    .local v5, "secondPattern":Ljava/util/regex/Pattern;
    const/4 v0, 0x0

    const/16 v1, 0x10

    invoke-virtual {v3, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v6

    .line 792
    .local v6, "firstMatcher":Ljava/util/regex/Matcher;
    const/16 v0, 0x10

    const/16 v1, 0x20

    invoke-virtual {v3, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v7

    .line 794
    .local v7, "secondMatcher":Ljava/util/regex/Matcher;
    new-instance v8, Ljava/security/SecureRandom;

    invoke-direct {v8}, Ljava/security/SecureRandom;-><init>()V

    .line 795
    .local v8, "r":Ljava/security/SecureRandom;
    const/4 v0, 0x7

    invoke-virtual {v8, v0}, Ljava/security/SecureRandom;->nextInt(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/util/regex/Matcher;->replaceAll(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 796
    .local v9, "vi_hi":Ljava/lang/String;
    const/4 v0, 0x3

    invoke-virtual {v8, v0}, Ljava/security/SecureRandom;->nextInt(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Ljava/util/regex/Matcher;->replaceAll(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 798
    .local v10, "vi_lo":Ljava/lang/String;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected static getAID()Ljava/lang/String;
    .registers 8

    .line 668
    invoke-static {}, Lcom/adobe/mobile/MobileConfig;->getInstance()Lcom/adobe/mobile/MobileConfig;

    move-result-object v0

    invoke-virtual {v0}, Lcom/adobe/mobile/MobileConfig;->mobileUsingAnalytics()Z

    move-result v0

    if-nez v0, :cond_c

    .line 669
    const/4 v0, 0x0

    return-object v0

    .line 672
    :cond_c
    sget-object v4, Lcom/adobe/mobile/StaticMethods;->_aidMutex:Ljava/lang/Object;

    monitor-enter v4

    .line 673
    :try_start_f
    sget-boolean v0, Lcom/adobe/mobile/StaticMethods;->_aidDone:Z

    if-nez v0, :cond_7b

    .line 674
    const/4 v0, 0x1

    sput-boolean v0, Lcom/adobe/mobile/StaticMethods;->_aidDone:Z
    :try_end_16
    .catchall {:try_start_f .. :try_end_16} :catchall_7d

    .line 676
    :try_start_16
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "ADOBEMOBILE_STOREDDEFAULTS_IGNORE_AID"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    .line 677
    .local v5, "ignoreAid":Z
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "ADOBEMOBILE_STOREDDEFAULTS_AID"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/adobe/mobile/StaticMethods;->aid:Ljava/lang/String;

    .line 682
    if-nez v5, :cond_34

    sget-object v0, Lcom/adobe/mobile/StaticMethods;->aid:Ljava/lang/String;

    if-eqz v0, :cond_40

    :cond_34
    invoke-static {}, Lcom/adobe/mobile/MobileConfig;->getInstance()Lcom/adobe/mobile/MobileConfig;

    move-result-object v0

    invoke-virtual {v0}, Lcom/adobe/mobile/MobileConfig;->getVisitorIdServiceEnabled()Z

    move-result v0

    if-nez v0, :cond_6a

    if-eqz v5, :cond_6a

    .line 683
    :cond_40
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->retrieveAIDFromVisitorIDService()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/adobe/mobile/StaticMethods;->aid:Ljava/lang/String;

    .line 684
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getSharedPreferencesEditor()Landroid/content/SharedPreferences$Editor;

    move-result-object v6

    .line 686
    .local v6, "editor":Landroid/content/SharedPreferences$Editor;
    sget-object v0, Lcom/adobe/mobile/StaticMethods;->aid:Ljava/lang/String;

    if-eqz v0, :cond_5c

    .line 688
    const-string v0, "ADOBEMOBILE_STOREDDEFAULTS_AID"

    sget-object v1, Lcom/adobe/mobile/StaticMethods;->aid:Ljava/lang/String;

    invoke-interface {v6, v0, v1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 689
    const-string v0, "ADOBEMOBILE_STOREDDEFAULTS_IGNORE_AID"

    const/4 v1, 0x0

    invoke-interface {v6, v0, v1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    goto :goto_62

    .line 691
    :cond_5c
    const-string v0, "ADOBEMOBILE_STOREDDEFAULTS_IGNORE_AID"

    const/4 v1, 0x1

    invoke-interface {v6, v0, v1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 694
    :goto_62
    invoke-interface {v6}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 696
    sget-object v0, Lcom/adobe/mobile/StaticMethods;->aid:Ljava/lang/String;

    invoke-static {v0}, Lcom/adobe/mobile/StaticMethods;->syncAIDIfNeeded(Ljava/lang/String;)V
    :try_end_6a
    .catch Lcom/adobe/mobile/StaticMethods$NullContextException; {:try_start_16 .. :try_end_6a} :catch_6b
    .catchall {:try_start_16 .. :try_end_6a} :catchall_7d

    .line 700
    .end local v5    # "ignoreAid":Z
    .end local v6    # "editor":Landroid/content/SharedPreferences$Editor;
    :cond_6a
    goto :goto_7b

    .line 698
    :catch_6b
    move-exception v5

    .line 699
    .local v5, "e":Lcom/adobe/mobile/StaticMethods$NullContextException;
    const-string v0, "Config - Error getting AID. (%s)"

    const/4 v1, 0x1

    :try_start_6f
    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v5}, Lcom/adobe/mobile/StaticMethods$NullContextException;->getMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logErrorFormat(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_7b
    .catchall {:try_start_6f .. :try_end_7b} :catchall_7d

    .line 702
    .end local v5    # "e":Lcom/adobe/mobile/StaticMethods$NullContextException;
    :cond_7b
    :goto_7b
    monitor-exit v4

    goto :goto_80

    :catchall_7d
    move-exception v7

    monitor-exit v4

    throw v7

    .line 704
    :goto_80
    sget-object v0, Lcom/adobe/mobile/StaticMethods;->aid:Ljava/lang/String;

    return-object v0
.end method

.method protected static getAdvertisingIdentifier()Ljava/lang/String;
    .registers 3

    .line 421
    sget-object v1, Lcom/adobe/mobile/StaticMethods;->_advertisingIdentifierMutex:Ljava/lang/Object;

    monitor-enter v1

    .line 422
    :try_start_3
    sget-object v0, Lcom/adobe/mobile/StaticMethods;->advertisingIdentifier:Ljava/lang/String;
    :try_end_5
    .catchall {:try_start_3 .. :try_end_5} :catchall_7

    monitor-exit v1

    return-object v0

    .line 423
    :catchall_7
    move-exception v2

    monitor-exit v1

    throw v2
.end method

.method protected static getAnalyticsExecutor()Ljava/util/concurrent/ExecutorService;
    .registers 3

    .line 596
    sget-object v1, Lcom/adobe/mobile/StaticMethods;->_analyticsExecutorMutex:Ljava/lang/Object;

    monitor-enter v1

    .line 597
    :try_start_3
    sget-object v0, Lcom/adobe/mobile/StaticMethods;->analyticsExecutor:Ljava/util/concurrent/ExecutorService;

    if-nez v0, :cond_d

    .line 598
    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    sput-object v0, Lcom/adobe/mobile/StaticMethods;->analyticsExecutor:Ljava/util/concurrent/ExecutorService;

    .line 601
    :cond_d
    sget-object v0, Lcom/adobe/mobile/StaticMethods;->analyticsExecutor:Ljava/util/concurrent/ExecutorService;
    :try_end_f
    .catchall {:try_start_3 .. :try_end_f} :catchall_11

    monitor-exit v1

    return-object v0

    .line 602
    :catchall_11
    move-exception v2

    monitor-exit v1

    throw v2
.end method

.method private static getAndroidVersion()Ljava/lang/String;
    .registers 1

    .line 1195
    sget-object v0, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    return-object v0
.end method

.method protected static getApplicationID()Ljava/lang/String;
    .registers 9

    .line 202
    sget-object v6, Lcom/adobe/mobile/StaticMethods;->_applicationIDMutex:Ljava/lang/Object;

    monitor-enter v6

    .line 203
    :try_start_3
    sget-object v0, Lcom/adobe/mobile/StaticMethods;->appID:Ljava/lang/String;

    if-nez v0, :cond_99

    .line 204
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getApplicationName()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_17

    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getApplicationName()Ljava/lang/String;

    move-result-object v1

    goto :goto_19

    :cond_17
    const-string v1, ""

    :goto_19
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 205
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getApplicationVersion()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_4b

    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getApplicationVersion()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_4b

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getApplicationVersion()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_4d

    :cond_4b
    const-string v1, ""

    :goto_4d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 206
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getApplicationVersionCode()I

    move-result v1

    if-eqz v1, :cond_6e

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "(%d)"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getApplicationVersionCode()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    const/4 v5, 0x0

    aput-object v4, v3, v5

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_70

    :cond_6e
    const-string v1, ""

    :goto_70
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/adobe/mobile/StaticMethods;->appID:Ljava/lang/String;
    :try_end_7a
    .catchall {:try_start_3 .. :try_end_7a} :catchall_9d

    .line 209
    :try_start_7a
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getSharedPreferencesEditor()Landroid/content/SharedPreferences$Editor;

    move-result-object v7

    .line 210
    .local v7, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v0, "ADOBEMOBILE_STOREDDEFAULTS_APPID"

    sget-object v1, Lcom/adobe/mobile/StaticMethods;->appID:Ljava/lang/String;

    invoke-interface {v7, v0, v1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 211
    invoke-interface {v7}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_88
    .catch Lcom/adobe/mobile/StaticMethods$NullContextException; {:try_start_7a .. :try_end_88} :catch_89
    .catchall {:try_start_7a .. :try_end_88} :catchall_9d

    .line 215
    .end local v7    # "editor":Landroid/content/SharedPreferences$Editor;
    goto :goto_99

    .line 213
    :catch_89
    move-exception v7

    .line 214
    .local v7, "ex":Lcom/adobe/mobile/StaticMethods$NullContextException;
    const-string v0, "Config - Unable to set Application ID in preferences (%s)"

    const/4 v1, 0x1

    :try_start_8d
    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v7}, Lcom/adobe/mobile/StaticMethods$NullContextException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logWarningFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 217
    .end local v7    # "ex":Lcom/adobe/mobile/StaticMethods$NullContextException;
    :cond_99
    :goto_99
    sget-object v0, Lcom/adobe/mobile/StaticMethods;->appID:Ljava/lang/String;
    :try_end_9b
    .catchall {:try_start_8d .. :try_end_9b} :catchall_9d

    monitor-exit v6

    return-object v0

    .line 218
    :catchall_9d
    move-exception v8

    monitor-exit v6

    throw v8
.end method

.method private static getApplicationName()Ljava/lang/String;
    .registers 8

    .line 224
    sget-object v4, Lcom/adobe/mobile/StaticMethods;->_applicationNameMutex:Ljava/lang/Object;

    monitor-enter v4

    .line 225
    :try_start_3
    sget-object v0, Lcom/adobe/mobile/StaticMethods;->appName:Ljava/lang/String;

    if-nez v0, :cond_79

    .line 226
    const-string v0, ""

    sput-object v0, Lcom/adobe/mobile/StaticMethods;->appName:Ljava/lang/String;
    :try_end_b
    .catchall {:try_start_3 .. :try_end_b} :catchall_7d

    .line 228
    :try_start_b
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getSharedContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    .line 229
    .local v5, "packageManager":Landroid/content/pm/PackageManager;
    if-eqz v5, :cond_43

    .line 230
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getSharedContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v5, v0, v1}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v6

    .line 231
    .local v6, "applicationInfo":Landroid/content/pm/ApplicationInfo;
    if-eqz v6, :cond_36

    .line 232
    invoke-virtual {v5, v6}, Landroid/content/pm/PackageManager;->getApplicationLabel(Landroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;

    move-result-object v0

    if-eqz v0, :cond_31

    .line 233
    invoke-virtual {v5, v6}, Landroid/content/pm/PackageManager;->getApplicationLabel(Landroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_33

    :cond_31
    const-string v0, ""

    :goto_33
    sput-object v0, Lcom/adobe/mobile/StaticMethods;->appName:Ljava/lang/String;

    goto :goto_42

    .line 236
    :cond_36
    const-string v0, "Analytics - ApplicationInfo was null"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logWarningFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 237
    const-string v0, ""

    sput-object v0, Lcom/adobe/mobile/StaticMethods;->appName:Ljava/lang/String;

    .line 239
    .end local v6    # "applicationInfo":Landroid/content/pm/ApplicationInfo;
    :goto_42
    goto :goto_4f

    .line 241
    :cond_43
    const-string v0, "Analytics - PackageManager was null"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logWarningFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 242
    const-string v0, ""

    sput-object v0, Lcom/adobe/mobile/StaticMethods;->appName:Ljava/lang/String;
    :try_end_4f
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_b .. :try_end_4f} :catch_50
    .catch Lcom/adobe/mobile/StaticMethods$NullContextException; {:try_start_b .. :try_end_4f} :catch_65
    .catchall {:try_start_b .. :try_end_4f} :catchall_7d

    .line 250
    .end local v5    # "packageManager":Landroid/content/pm/PackageManager;
    :goto_4f
    goto :goto_79

    .line 244
    :catch_50
    move-exception v5

    .line 245
    .local v5, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v0, "Analytics - PackageManager couldn\'t find application name (%s)"

    const/4 v1, 0x1

    :try_start_54
    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v5}, Landroid/content/pm/PackageManager$NameNotFoundException;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logWarningFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 246
    const-string v0, ""

    sput-object v0, Lcom/adobe/mobile/StaticMethods;->appName:Ljava/lang/String;

    .line 250
    .end local v5    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    goto :goto_79

    .line 247
    :catch_65
    move-exception v5

    .line 248
    .local v5, "e":Lcom/adobe/mobile/StaticMethods$NullContextException;
    const-string v0, "Config - Unable to get package to pull application name. (%s)"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v5}, Lcom/adobe/mobile/StaticMethods$NullContextException;->getMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logErrorFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 249
    const-string v0, ""

    sput-object v0, Lcom/adobe/mobile/StaticMethods;->appName:Ljava/lang/String;

    .line 252
    .end local v5    # "e":Lcom/adobe/mobile/StaticMethods$NullContextException;
    :cond_79
    :goto_79
    sget-object v0, Lcom/adobe/mobile/StaticMethods;->appName:Ljava/lang/String;
    :try_end_7b
    .catchall {:try_start_54 .. :try_end_7b} :catchall_7d

    monitor-exit v4

    return-object v0

    .line 253
    :catchall_7d
    move-exception v7

    monitor-exit v4

    throw v7
.end method

.method protected static getApplicationVersion()Ljava/lang/String;
    .registers 8

    .line 259
    sget-object v4, Lcom/adobe/mobile/StaticMethods;->_applicationVersionMutex:Ljava/lang/Object;

    monitor-enter v4

    .line 260
    :try_start_3
    sget-object v0, Lcom/adobe/mobile/StaticMethods;->versionName:Ljava/lang/String;

    if-nez v0, :cond_73

    .line 261
    const-string v0, ""

    sput-object v0, Lcom/adobe/mobile/StaticMethods;->versionName:Ljava/lang/String;
    :try_end_b
    .catchall {:try_start_3 .. :try_end_b} :catchall_77

    .line 263
    :try_start_b
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getSharedContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    .line 264
    .local v5, "packageManager":Landroid/content/pm/PackageManager;
    if-eqz v5, :cond_3d

    .line 265
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getSharedContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v5, v0, v1}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v6

    .line 266
    .local v6, "packageInfo":Landroid/content/pm/PackageInfo;
    if-eqz v6, :cond_30

    .line 267
    iget-object v0, v6, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    if-eqz v0, :cond_2b

    iget-object v0, v6, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    goto :goto_2d

    :cond_2b
    const-string v0, ""

    :goto_2d
    sput-object v0, Lcom/adobe/mobile/StaticMethods;->versionName:Ljava/lang/String;

    goto :goto_3c

    .line 270
    :cond_30
    const-string v0, "Analytics - PackageInfo was null"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logWarningFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 271
    const-string v0, ""

    sput-object v0, Lcom/adobe/mobile/StaticMethods;->versionName:Ljava/lang/String;

    .line 273
    .end local v6    # "packageInfo":Landroid/content/pm/PackageInfo;
    :goto_3c
    goto :goto_49

    .line 275
    :cond_3d
    const-string v0, "Analytics - PackageManager was null"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logWarningFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 276
    const-string v0, ""

    sput-object v0, Lcom/adobe/mobile/StaticMethods;->versionName:Ljava/lang/String;
    :try_end_49
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_b .. :try_end_49} :catch_4a
    .catch Lcom/adobe/mobile/StaticMethods$NullContextException; {:try_start_b .. :try_end_49} :catch_5f
    .catchall {:try_start_b .. :try_end_49} :catchall_77

    .line 284
    .end local v5    # "packageManager":Landroid/content/pm/PackageManager;
    :goto_49
    goto :goto_73

    .line 278
    :catch_4a
    move-exception v5

    .line 279
    .local v5, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v0, "Analytics - PackageManager couldn\'t find application version (%s)"

    const/4 v1, 0x1

    :try_start_4e
    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v5}, Landroid/content/pm/PackageManager$NameNotFoundException;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logWarningFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 280
    const-string v0, ""

    sput-object v0, Lcom/adobe/mobile/StaticMethods;->versionName:Ljava/lang/String;

    .line 284
    .end local v5    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    goto :goto_73

    .line 281
    :catch_5f
    move-exception v5

    .line 282
    .local v5, "e":Lcom/adobe/mobile/StaticMethods$NullContextException;
    const-string v0, "Config - Unable to get package to pull application version. (%s)"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v5}, Lcom/adobe/mobile/StaticMethods$NullContextException;->getMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logErrorFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 283
    const-string v0, ""

    sput-object v0, Lcom/adobe/mobile/StaticMethods;->versionName:Ljava/lang/String;

    .line 286
    .end local v5    # "e":Lcom/adobe/mobile/StaticMethods$NullContextException;
    :cond_73
    :goto_73
    sget-object v0, Lcom/adobe/mobile/StaticMethods;->versionName:Ljava/lang/String;
    :try_end_75
    .catchall {:try_start_4e .. :try_end_75} :catchall_77

    monitor-exit v4

    return-object v0

    .line 287
    :catchall_77
    move-exception v7

    monitor-exit v4

    throw v7
.end method

.method protected static getApplicationVersionCode()I
    .registers 8

    .line 293
    sget-object v4, Lcom/adobe/mobile/StaticMethods;->_applicationVersionCodeMutex:Ljava/lang/Object;

    monitor-enter v4

    .line 294
    :try_start_3
    sget v0, Lcom/adobe/mobile/StaticMethods;->versionCode:I
    :try_end_5
    .catchall {:try_start_3 .. :try_end_5} :catchall_6f

    const/4 v1, -0x1

    if-ne v0, v1, :cond_6b

    .line 296
    :try_start_8
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getSharedContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    .line 297
    .local v5, "packageManager":Landroid/content/pm/PackageManager;
    if-eqz v5, :cond_38

    .line 298
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getSharedContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v5, v0, v1}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v6

    .line 299
    .local v6, "packageInfo":Landroid/content/pm/PackageInfo;
    if-eqz v6, :cond_2c

    .line 300
    iget v0, v6, Landroid/content/pm/PackageInfo;->versionCode:I

    if-lez v0, :cond_28

    iget v0, v6, Landroid/content/pm/PackageInfo;->versionCode:I

    goto :goto_29

    :cond_28
    const/4 v0, 0x0

    :goto_29
    sput v0, Lcom/adobe/mobile/StaticMethods;->versionCode:I

    goto :goto_37

    .line 303
    :cond_2c
    const-string v0, "Analytics - PackageInfo was null"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logWarningFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 304
    const/4 v0, 0x0

    sput v0, Lcom/adobe/mobile/StaticMethods;->versionCode:I

    .line 306
    .end local v6    # "packageInfo":Landroid/content/pm/PackageInfo;
    :goto_37
    goto :goto_43

    .line 308
    :cond_38
    const-string v0, "Analytics - PackageManager was null"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logWarningFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 309
    const/4 v0, 0x0

    sput v0, Lcom/adobe/mobile/StaticMethods;->versionCode:I
    :try_end_43
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_8 .. :try_end_43} :catch_44
    .catch Lcom/adobe/mobile/StaticMethods$NullContextException; {:try_start_8 .. :try_end_43} :catch_58
    .catchall {:try_start_8 .. :try_end_43} :catchall_6f

    .line 318
    .end local v5    # "packageManager":Landroid/content/pm/PackageManager;
    :goto_43
    goto :goto_6b

    .line 312
    :catch_44
    move-exception v5

    .line 313
    .local v5, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v0, "Analytics - PackageManager couldn\'t find application version code (%s)"

    const/4 v1, 0x1

    :try_start_48
    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v5}, Landroid/content/pm/PackageManager$NameNotFoundException;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logWarningFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 314
    const/4 v0, 0x0

    sput v0, Lcom/adobe/mobile/StaticMethods;->versionCode:I

    .line 318
    .end local v5    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    goto :goto_6b

    .line 315
    :catch_58
    move-exception v5

    .line 316
    .local v5, "e":Lcom/adobe/mobile/StaticMethods$NullContextException;
    const-string v0, "Config - Unable to get package to pull application version code. (%s)"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v5}, Lcom/adobe/mobile/StaticMethods$NullContextException;->getMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logErrorFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 317
    const/4 v0, 0x0

    sput v0, Lcom/adobe/mobile/StaticMethods;->versionCode:I

    .line 320
    .end local v5    # "e":Lcom/adobe/mobile/StaticMethods$NullContextException;
    :cond_6b
    :goto_6b
    sget v0, Lcom/adobe/mobile/StaticMethods;->versionCode:I
    :try_end_6d
    .catchall {:try_start_48 .. :try_end_6d} :catchall_6f

    monitor-exit v4

    return v0

    .line 321
    :catchall_6f
    move-exception v7

    monitor-exit v4

    throw v7
.end method

.method protected static getAudienceExecutor()Ljava/util/concurrent/ExecutorService;
    .registers 3

    .line 643
    sget-object v1, Lcom/adobe/mobile/StaticMethods;->_audienceExecutorMutex:Ljava/lang/Object;

    monitor-enter v1

    .line 644
    :try_start_3
    sget-object v0, Lcom/adobe/mobile/StaticMethods;->audienceExecutor:Ljava/util/concurrent/ExecutorService;

    if-nez v0, :cond_d

    .line 645
    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    sput-object v0, Lcom/adobe/mobile/StaticMethods;->audienceExecutor:Ljava/util/concurrent/ExecutorService;

    .line 648
    :cond_d
    sget-object v0, Lcom/adobe/mobile/StaticMethods;->audienceExecutor:Ljava/util/concurrent/ExecutorService;
    :try_end_f
    .catchall {:try_start_3 .. :try_end_f} :catchall_11

    monitor-exit v1

    return-object v0

    .line 649
    :catchall_11
    move-exception v2

    monitor-exit v1

    throw v2
.end method

.method protected static getCacheDirectory()Ljava/io/File;
    .registers 5

    .line 549
    :try_start_0
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getSharedContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getCacheDir()Ljava/io/File;
    :try_end_7
    .catch Lcom/adobe/mobile/StaticMethods$NullContextException; {:try_start_0 .. :try_end_7} :catch_9

    move-result-object v0

    return-object v0

    .line 550
    :catch_9
    move-exception v4

    .line 551
    .local v4, "e":Lcom/adobe/mobile/StaticMethods$NullContextException;
    const-string v0, "Config - Error getting cache directory. (%s)"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v4}, Lcom/adobe/mobile/StaticMethods$NullContextException;->getMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logErrorFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 554
    .end local v4    # "e":Lcom/adobe/mobile/StaticMethods$NullContextException;
    const/4 v0, 0x0

    return-object v0
.end method

.method protected static getCarrier()Ljava/lang/String;
    .registers 7

    .line 492
    sget-object v4, Lcom/adobe/mobile/StaticMethods;->_carrierMutex:Ljava/lang/Object;

    monitor-enter v4

    .line 493
    :try_start_3
    sget-object v0, Lcom/adobe/mobile/StaticMethods;->carrier:Ljava/lang/String;
    :try_end_5
    .catchall {:try_start_3 .. :try_end_5} :catchall_2f

    if-nez v0, :cond_2b

    .line 495
    :try_start_7
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getSharedContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "phone"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Landroid/telephony/TelephonyManager;

    .line 496
    .local v5, "telephonyManager":Landroid/telephony/TelephonyManager;
    invoke-virtual {v5}, Landroid/telephony/TelephonyManager;->getNetworkOperatorName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/adobe/mobile/StaticMethods;->carrier:Ljava/lang/String;
    :try_end_1a
    .catch Lcom/adobe/mobile/StaticMethods$NullContextException; {:try_start_7 .. :try_end_1a} :catch_1b
    .catchall {:try_start_7 .. :try_end_1a} :catchall_2f

    .line 499
    .end local v5    # "telephonyManager":Landroid/telephony/TelephonyManager;
    goto :goto_2b

    .line 497
    :catch_1b
    move-exception v5

    .line 498
    .local v5, "e":Lcom/adobe/mobile/StaticMethods$NullContextException;
    const-string v0, "Config - Error getting device carrier. (%s)"

    const/4 v1, 0x1

    :try_start_1f
    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v5}, Lcom/adobe/mobile/StaticMethods$NullContextException;->getMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logErrorFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 502
    .end local v5    # "e":Lcom/adobe/mobile/StaticMethods$NullContextException;
    :cond_2b
    :goto_2b
    sget-object v0, Lcom/adobe/mobile/StaticMethods;->carrier:Ljava/lang/String;
    :try_end_2d
    .catchall {:try_start_1f .. :try_end_2d} :catchall_2f

    monitor-exit v4

    return-object v0

    .line 503
    :catchall_2f
    move-exception v6

    monitor-exit v4

    throw v6
.end method

.method protected static getCurrentActivity()Landroid/app/Activity;
    .registers 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/adobe/mobile/StaticMethods$NullActivityException;
        }
    .end annotation

    .line 1264
    sget-object v2, Lcom/adobe/mobile/StaticMethods;->_currentActivityMutex:Ljava/lang/Object;

    monitor-enter v2

    .line 1265
    :try_start_3
    sget-object v0, Lcom/adobe/mobile/StaticMethods;->_activity:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_f

    sget-object v0, Lcom/adobe/mobile/StaticMethods;->_activity:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_17

    .line 1266
    :cond_f
    new-instance v0, Lcom/adobe/mobile/StaticMethods$NullActivityException;

    const-string v1, "Message - No Current Activity (Messages must have a reference to the current activity to work properly)"

    invoke-direct {v0, v1}, Lcom/adobe/mobile/StaticMethods$NullActivityException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1268
    :cond_17
    sget-object v0, Lcom/adobe/mobile/StaticMethods;->_activity:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;
    :try_end_1f
    .catchall {:try_start_3 .. :try_end_1f} :catchall_21

    monitor-exit v2

    return-object v0

    .line 1269
    :catchall_21
    move-exception v3

    monitor-exit v2

    throw v3
.end method

.method protected static getCurrentOrientation()I
    .registers 2

    .line 1289
    :try_start_0
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getCurrentActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I
    :try_end_e
    .catch Lcom/adobe/mobile/StaticMethods$NullActivityException; {:try_start_0 .. :try_end_e} :catch_f

    return v0

    .line 1291
    :catch_f
    move-exception v1

    .line 1292
    .local v1, "ex":Lcom/adobe/mobile/StaticMethods$NullActivityException;
    const/4 v0, -0x1

    return v0
.end method

.method protected static getDebugLogging()Z
    .registers 1

    .line 809
    sget-boolean v0, Lcom/adobe/mobile/StaticMethods;->_debugLogging:Z

    return v0
.end method

.method protected static getDefaultAcceptLanguage()Ljava/lang/String;
    .registers 7

    .line 1171
    :try_start_0
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getSharedContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
    :try_end_7
    .catch Lcom/adobe/mobile/StaticMethods$NullContextException; {:try_start_0 .. :try_end_7} :catch_9

    move-result-object v4

    .line 1175
    .local v4, "resources":Landroid/content/res/Resources;
    goto :goto_1b

    .line 1172
    .end local v4    # "resources":Landroid/content/res/Resources;
    :catch_9
    move-exception v5

    .line 1173
    .local v5, "e":Lcom/adobe/mobile/StaticMethods$NullContextException;
    const-string v0, "Config - Error getting application resources for default accepted language. (%s)"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v5}, Lcom/adobe/mobile/StaticMethods$NullContextException;->getMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logErrorFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1174
    const/4 v0, 0x0

    return-object v0

    .line 1177
    .local v4, "resources":Landroid/content/res/Resources;
    .end local v5    # "e":Lcom/adobe/mobile/StaticMethods$NullContextException;
    :goto_1b
    if-nez v4, :cond_1f

    .line 1178
    const/4 v0, 0x0

    return-object v0

    .line 1181
    :cond_1f
    invoke-virtual {v4}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v5

    .line 1182
    .local v5, "configuration":Landroid/content/res/Configuration;
    if-nez v5, :cond_27

    .line 1183
    const/4 v0, 0x0

    return-object v0

    .line 1186
    :cond_27
    iget-object v6, v5, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    .line 1187
    .local v6, "locale":Ljava/util/Locale;
    if-nez v6, :cond_2d

    .line 1188
    const/4 v0, 0x0

    return-object v0

    .line 1191
    :cond_2d
    invoke-virtual {v6}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0x5f

    const/16 v2, 0x2d

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected static getDefaultData()Ljava/util/HashMap;
    .registers 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
        }
    .end annotation

    .line 456
    sget-object v3, Lcom/adobe/mobile/StaticMethods;->_defaultDataMutex:Ljava/lang/Object;

    monitor-enter v3

    .line 457
    :try_start_3
    sget-object v0, Lcom/adobe/mobile/StaticMethods;->defaultData:Ljava/util/HashMap;

    if-nez v0, :cond_55

    .line 458
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/adobe/mobile/StaticMethods;->defaultData:Ljava/util/HashMap;

    .line 459
    sget-object v0, Lcom/adobe/mobile/StaticMethods;->defaultData:Ljava/util/HashMap;

    const-string v1, "a.DeviceName"

    sget-object v2, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 460
    sget-object v0, Lcom/adobe/mobile/StaticMethods;->defaultData:Ljava/util/HashMap;

    const-string v1, "a.Resolution"

    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getResolution()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 461
    sget-object v0, Lcom/adobe/mobile/StaticMethods;->defaultData:Ljava/util/HashMap;

    const-string v1, "a.OSVersion"

    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getOperatingSystem()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 462
    sget-object v0, Lcom/adobe/mobile/StaticMethods;->defaultData:Ljava/util/HashMap;

    const-string v1, "a.CarrierName"

    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getCarrier()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 463
    sget-object v0, Lcom/adobe/mobile/StaticMethods;->defaultData:Ljava/util/HashMap;

    const-string v1, "a.AppID"

    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getApplicationID()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 464
    sget-object v0, Lcom/adobe/mobile/StaticMethods;->defaultData:Ljava/util/HashMap;

    const-string v1, "a.RunMode"

    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->isWearableApp()Z

    move-result v2

    if-eqz v2, :cond_50

    const-string v2, "Extension"

    goto :goto_52

    :cond_50
    const-string v2, "Application"

    :goto_52
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 467
    :cond_55
    sget-object v0, Lcom/adobe/mobile/StaticMethods;->defaultData:Ljava/util/HashMap;
    :try_end_57
    .catchall {:try_start_3 .. :try_end_57} :catchall_59

    monitor-exit v3

    return-object v0

    .line 468
    :catchall_59
    move-exception v4

    monitor-exit v3

    throw v4
.end method

.method protected static getDefaultUserAgent()Ljava/lang/String;
    .registers 4

    .line 443
    sget-object v2, Lcom/adobe/mobile/StaticMethods;->_userAgentMutex:Ljava/lang/Object;

    monitor-enter v2

    .line 444
    :try_start_3
    sget-object v0, Lcom/adobe/mobile/StaticMethods;->userAgent:Ljava/lang/String;

    if-nez v0, :cond_4c

    .line 445
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Mozilla/5.0 (Linux; U; Android "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getAndroidVersion()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "; "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 446
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getDefaultAcceptLanguage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "; "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " Build/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Landroid/os/Build;->ID:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/adobe/mobile/StaticMethods;->userAgent:Ljava/lang/String;

    .line 449
    :cond_4c
    sget-object v0, Lcom/adobe/mobile/StaticMethods;->userAgent:Ljava/lang/String;
    :try_end_4e
    .catchall {:try_start_3 .. :try_end_4e} :catchall_50

    monitor-exit v2

    return-object v0

    .line 450
    :catchall_50
    move-exception v3

    monitor-exit v2

    throw v3
.end method

.method private static getDeviceLocale()Ljava/util/Locale;
    .registers 6

    .line 1425
    :try_start_0
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getSharedContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
    :try_end_7
    .catch Lcom/adobe/mobile/StaticMethods$NullContextException; {:try_start_0 .. :try_end_7} :catch_9

    move-result-object v4

    .line 1429
    .local v4, "resources":Landroid/content/res/Resources;
    goto :goto_1c

    .line 1426
    .end local v4    # "resources":Landroid/content/res/Resources;
    :catch_9
    move-exception v5

    .line 1427
    .local v5, "e":Lcom/adobe/mobile/StaticMethods$NullContextException;
    const-string v0, "Config - Error getting application resources for device locale. (%s)"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v5}, Lcom/adobe/mobile/StaticMethods$NullContextException;->getMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logErrorFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1428
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    return-object v0

    .line 1431
    .local v4, "resources":Landroid/content/res/Resources;
    .end local v5    # "e":Lcom/adobe/mobile/StaticMethods$NullContextException;
    :goto_1c
    if-nez v4, :cond_21

    .line 1432
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    return-object v0

    .line 1435
    :cond_21
    invoke-virtual {v4}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v5

    .line 1436
    .local v5, "configuration":Landroid/content/res/Configuration;
    if-nez v5, :cond_2a

    .line 1437
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    return-object v0

    .line 1440
    :cond_2a
    iget-object v0, v5, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    if-eqz v0, :cond_31

    iget-object v0, v5, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    goto :goto_33

    :cond_31
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    :goto_33
    return-object v0
.end method

.method protected static getHexString(Ljava/lang/String;)Ljava/lang/String;
    .registers 11
    .param p0, "originalString"    # Ljava/lang/String;

    .line 1315
    if-eqz p0, :cond_8

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    if-gtz v0, :cond_a

    .line 1316
    :cond_8
    const/4 v0, 0x0

    return-object v0

    .line 1321
    :cond_a
    const-string v0, "UTF-8"

    :try_start_c
    invoke-virtual {p0, v0}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B
    :try_end_f
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_c .. :try_end_f} :catch_11

    move-result-object v4

    .line 1326
    .local v4, "bytes":[B
    goto :goto_23

    .line 1323
    .end local v4    # "bytes":[B
    :catch_11
    move-exception v5

    .line 1324
    .local v5, "ex":Ljava/io/UnsupportedEncodingException;
    const-string v0, "Failed to get hex from string (%s)"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v5}, Ljava/io/UnsupportedEncodingException;->getMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logDebugFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1325
    const/4 v0, 0x0

    return-object v0

    .line 1327
    .local v4, "bytes":[B
    .end local v5    # "ex":Ljava/io/UnsupportedEncodingException;
    :goto_23
    array-length v5, v4

    .line 1328
    .local v5, "bytesLength":I
    shl-int/lit8 v0, v5, 0x1

    new-array v6, v0, [C

    .line 1330
    .local v6, "chars":[C
    const/4 v8, 0x0

    .line 1331
    .local v8, "index":I
    const/4 v9, 0x0

    .line 1332
    .local v9, "offset":I
    :goto_2a
    if-ge v9, v5, :cond_4b

    .line 1333
    move v0, v9

    add-int/lit8 v9, v9, 0x1

    aget-byte v0, v4, v0

    and-int/lit16 v0, v0, 0xff

    shl-int/lit8 v7, v0, 0x1

    .line 1334
    .local v7, "hexIndex":I
    move v0, v8

    add-int/lit8 v8, v8, 0x1

    sget-object v1, Lcom/adobe/mobile/StaticMethods;->BYTE_TO_HEX:[C

    move v2, v7

    add-int/lit8 v7, v7, 0x1

    aget-char v1, v1, v2

    aput-char v1, v6, v0

    .line 1335
    move v0, v8

    add-int/lit8 v8, v8, 0x1

    sget-object v1, Lcom/adobe/mobile/StaticMethods;->BYTE_TO_HEX:[C

    aget-char v1, v1, v7

    aput-char v1, v6, v0

    goto :goto_2a

    .line 1337
    .end local v7    # "hexIndex":I
    :cond_4b
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v6}, Ljava/lang/String;-><init>([C)V

    return-object v0
.end method

.method protected static getIso8601Date()Ljava/lang/String;
    .registers 1

    .line 1444
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    invoke-static {v0}, Lcom/adobe/mobile/StaticMethods;->getIso8601Date(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected static getIso8601Date(Ljava/util/Date;)Ljava/lang/String;
    .registers 4
    .param p0, "date"    # Ljava/util/Date;

    .line 1448
    new-instance v2, Ljava/text/SimpleDateFormat;

    const-string v0, "yyyy-MM-dd\'T\'HH:mm:ssZZZ"

    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getDeviceLocale()Ljava/util/Locale;

    move-result-object v1

    invoke-direct {v2, v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 1449
    .local v2, "iso8601Format":Ljava/text/DateFormat;
    if-eqz p0, :cond_f

    move-object v0, p0

    goto :goto_14

    :cond_f
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    :goto_14
    invoke-virtual {v2, v0}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected static getMessageImageCachingExecutor()Ljava/util/concurrent/ExecutorService;
    .registers 3

    .line 631
    sget-object v1, Lcom/adobe/mobile/StaticMethods;->_messageImageCachingExecutorMutex:Ljava/lang/Object;

    monitor-enter v1

    .line 632
    :try_start_3
    sget-object v0, Lcom/adobe/mobile/StaticMethods;->messageImageCachingExecutor:Ljava/util/concurrent/ExecutorService;

    if-nez v0, :cond_d

    .line 633
    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    sput-object v0, Lcom/adobe/mobile/StaticMethods;->messageImageCachingExecutor:Ljava/util/concurrent/ExecutorService;

    .line 636
    :cond_d
    sget-object v0, Lcom/adobe/mobile/StaticMethods;->messageImageCachingExecutor:Ljava/util/concurrent/ExecutorService;
    :try_end_f
    .catchall {:try_start_3 .. :try_end_f} :catchall_11

    monitor-exit v1

    return-object v0

    .line 637
    :catchall_11
    move-exception v2

    monitor-exit v1

    throw v2
.end method

.method protected static getMessagesExecutor()Ljava/util/concurrent/ExecutorService;
    .registers 3

    .line 608
    sget-object v1, Lcom/adobe/mobile/StaticMethods;->_messagesExecutorMutex:Ljava/lang/Object;

    monitor-enter v1

    .line 609
    :try_start_3
    sget-object v0, Lcom/adobe/mobile/StaticMethods;->messagesExecutor:Ljava/util/concurrent/ExecutorService;

    if-nez v0, :cond_d

    .line 610
    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    sput-object v0, Lcom/adobe/mobile/StaticMethods;->messagesExecutor:Ljava/util/concurrent/ExecutorService;

    .line 613
    :cond_d
    sget-object v0, Lcom/adobe/mobile/StaticMethods;->messagesExecutor:Ljava/util/concurrent/ExecutorService;
    :try_end_f
    .catchall {:try_start_3 .. :try_end_f} :catchall_11

    monitor-exit v1

    return-object v0

    .line 614
    :catchall_11
    move-exception v2

    monitor-exit v1

    throw v2
.end method

.method protected static getOperatingSystem()Ljava/lang/String;
    .registers 7

    .line 509
    sget-object v4, Lcom/adobe/mobile/StaticMethods;->_operatingSystemMutex:Ljava/lang/Object;

    monitor-enter v4

    .line 510
    :try_start_3
    sget-object v0, Lcom/adobe/mobile/StaticMethods;->operatingSystem:Ljava/lang/String;

    if-nez v0, :cond_3f

    .line 511
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Android "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getAndroidVersion()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/adobe/mobile/StaticMethods;->operatingSystem:Ljava/lang/String;
    :try_end_20
    .catchall {:try_start_3 .. :try_end_20} :catchall_43

    .line 514
    :try_start_20
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getSharedPreferencesEditor()Landroid/content/SharedPreferences$Editor;

    move-result-object v5

    .line 515
    .local v5, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v0, "ADOBEMOBILE_STOREDDEFAULTS_OS"

    sget-object v1, Lcom/adobe/mobile/StaticMethods;->operatingSystem:Ljava/lang/String;

    invoke-interface {v5, v0, v1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 516
    invoke-interface {v5}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_2e
    .catch Lcom/adobe/mobile/StaticMethods$NullContextException; {:try_start_20 .. :try_end_2e} :catch_2f
    .catchall {:try_start_20 .. :try_end_2e} :catchall_43

    .line 520
    .end local v5    # "editor":Landroid/content/SharedPreferences$Editor;
    goto :goto_3f

    .line 518
    :catch_2f
    move-exception v5

    .line 519
    .local v5, "ex":Lcom/adobe/mobile/StaticMethods$NullContextException;
    const-string v0, "Config - Unable to set OS version in preferences (%s)"

    const/4 v1, 0x1

    :try_start_33
    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v5}, Lcom/adobe/mobile/StaticMethods$NullContextException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logWarningFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 523
    .end local v5    # "ex":Lcom/adobe/mobile/StaticMethods$NullContextException;
    :cond_3f
    :goto_3f
    sget-object v0, Lcom/adobe/mobile/StaticMethods;->operatingSystem:Ljava/lang/String;
    :try_end_41
    .catchall {:try_start_33 .. :try_end_41} :catchall_43

    monitor-exit v4

    return-object v0

    .line 524
    :catchall_43
    move-exception v6

    monitor-exit v4

    throw v6
.end method

.method protected static getPIIExecutor()Ljava/util/concurrent/ExecutorService;
    .registers 3

    .line 655
    sget-object v1, Lcom/adobe/mobile/StaticMethods;->_piiExecutorMutex:Ljava/lang/Object;

    monitor-enter v1

    .line 656
    :try_start_3
    sget-object v0, Lcom/adobe/mobile/StaticMethods;->piiExecutor:Ljava/util/concurrent/ExecutorService;

    if-nez v0, :cond_d

    .line 657
    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    sput-object v0, Lcom/adobe/mobile/StaticMethods;->piiExecutor:Ljava/util/concurrent/ExecutorService;

    .line 660
    :cond_d
    sget-object v0, Lcom/adobe/mobile/StaticMethods;->piiExecutor:Ljava/util/concurrent/ExecutorService;
    :try_end_f
    .catchall {:try_start_3 .. :try_end_f} :catchall_11

    monitor-exit v1

    return-object v0

    .line 661
    :catchall_11
    move-exception v2

    monitor-exit v1

    throw v2
.end method

.method protected static getPushIdentifier()Ljava/lang/String;
    .registers 3

    .line 391
    sget-object v1, Lcom/adobe/mobile/StaticMethods;->_pushIdentifierMutex:Ljava/lang/Object;

    monitor-enter v1

    .line 392
    :try_start_3
    sget-object v0, Lcom/adobe/mobile/StaticMethods;->pushIdentifier:Ljava/lang/String;
    :try_end_5
    .catchall {:try_start_3 .. :try_end_5} :catchall_7

    monitor-exit v1

    return-object v0

    .line 393
    :catchall_7
    move-exception v2

    monitor-exit v1

    throw v2
.end method

.method protected static getResolution()Ljava/lang/String;
    .registers 7

    .line 475
    sget-object v4, Lcom/adobe/mobile/StaticMethods;->_resolutionMutex:Ljava/lang/Object;

    monitor-enter v4

    .line 476
    :try_start_3
    sget-object v0, Lcom/adobe/mobile/StaticMethods;->resolution:Ljava/lang/String;
    :try_end_5
    .catchall {:try_start_3 .. :try_end_5} :catchall_45

    if-nez v0, :cond_41

    .line 478
    :try_start_7
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getSharedContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v5

    .line 479
    .local v5, "displayMetrics":Landroid/util/DisplayMetrics;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget v1, v5, Landroid/util/DisplayMetrics;->widthPixels:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "x"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, v5, Landroid/util/DisplayMetrics;->heightPixels:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/adobe/mobile/StaticMethods;->resolution:Ljava/lang/String;
    :try_end_30
    .catch Lcom/adobe/mobile/StaticMethods$NullContextException; {:try_start_7 .. :try_end_30} :catch_31
    .catchall {:try_start_7 .. :try_end_30} :catchall_45

    .line 482
    .end local v5    # "displayMetrics":Landroid/util/DisplayMetrics;
    goto :goto_41

    .line 480
    :catch_31
    move-exception v5

    .line 481
    .local v5, "e":Lcom/adobe/mobile/StaticMethods$NullContextException;
    const-string v0, "Config - Error getting device resolution. (%s)"

    const/4 v1, 0x1

    :try_start_35
    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v5}, Lcom/adobe/mobile/StaticMethods$NullContextException;->getMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logErrorFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 485
    .end local v5    # "e":Lcom/adobe/mobile/StaticMethods$NullContextException;
    :cond_41
    :goto_41
    sget-object v0, Lcom/adobe/mobile/StaticMethods;->resolution:Ljava/lang/String;
    :try_end_43
    .catchall {:try_start_35 .. :try_end_43} :catchall_45

    monitor-exit v4

    return-object v0

    .line 486
    :catchall_45
    move-exception v6

    monitor-exit v4

    throw v6
.end method

.method protected static getSharedContext()Landroid/content/Context;
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/adobe/mobile/StaticMethods$NullContextException;
        }
    .end annotation

    .line 1210
    sget-object v0, Lcom/adobe/mobile/StaticMethods;->sharedContext:Landroid/content/Context;

    if-nez v0, :cond_c

    .line 1211
    new-instance v0, Lcom/adobe/mobile/StaticMethods$NullContextException;

    const-string v1, "Config - No Application Context (Application context must be set prior to calling any library functions.)"

    invoke-direct {v0, v1}, Lcom/adobe/mobile/StaticMethods$NullContextException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1214
    :cond_c
    sget-object v0, Lcom/adobe/mobile/StaticMethods;->sharedContext:Landroid/content/Context;

    return-object v0
.end method

.method protected static getSharedPreferences()Landroid/content/SharedPreferences;
    .registers 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/adobe/mobile/StaticMethods$NullContextException;
        }
    .end annotation

    .line 183
    sget-object v3, Lcom/adobe/mobile/StaticMethods;->_sharedPreferencesMutex:Ljava/lang/Object;

    monitor-enter v3

    .line 184
    :try_start_3
    sget-object v0, Lcom/adobe/mobile/StaticMethods;->prefs:Landroid/content/SharedPreferences;

    if-nez v0, :cond_20

    .line 185
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getSharedContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "APP_MEASUREMENT_CACHE"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    sput-object v0, Lcom/adobe/mobile/StaticMethods;->prefs:Landroid/content/SharedPreferences;

    .line 186
    sget-object v0, Lcom/adobe/mobile/StaticMethods;->prefs:Landroid/content/SharedPreferences;

    if-nez v0, :cond_20

    .line 187
    const-string v0, "Config - No SharedPreferences available"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logWarningFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 191
    :cond_20
    sget-object v0, Lcom/adobe/mobile/StaticMethods;->prefs:Landroid/content/SharedPreferences;

    if-nez v0, :cond_2c

    .line 192
    new-instance v0, Lcom/adobe/mobile/StaticMethods$NullContextException;

    const-string v1, "Config - No SharedPreferences available"

    invoke-direct {v0, v1}, Lcom/adobe/mobile/StaticMethods$NullContextException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 195
    :cond_2c
    sget-object v0, Lcom/adobe/mobile/StaticMethods;->prefs:Landroid/content/SharedPreferences;
    :try_end_2e
    .catchall {:try_start_3 .. :try_end_2e} :catchall_30

    monitor-exit v3

    return-object v0

    .line 196
    :catchall_30
    move-exception v4

    monitor-exit v3

    throw v4
.end method

.method protected static getSharedPreferencesEditor()Landroid/content/SharedPreferences$Editor;
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/adobe/mobile/StaticMethods$NullContextException;
        }
    .end annotation

    .line 1199
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    .line 1200
    .local v2, "editor":Landroid/content/SharedPreferences$Editor;
    if-nez v2, :cond_12

    .line 1201
    new-instance v0, Lcom/adobe/mobile/StaticMethods$NullContextException;

    const-string v1, "Config - Unable to create an instance of a SharedPreferences Editor"

    invoke-direct {v0, v1}, Lcom/adobe/mobile/StaticMethods$NullContextException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1204
    :cond_12
    return-object v2
.end method

.method protected static getThirdPartyCallbacksExecutor()Ljava/util/concurrent/ExecutorService;
    .registers 3

    .line 620
    sget-object v1, Lcom/adobe/mobile/StaticMethods;->_thirdPartyCallbacksExecutorMutex:Ljava/lang/Object;

    monitor-enter v1

    .line 621
    :try_start_3
    sget-object v0, Lcom/adobe/mobile/StaticMethods;->thirdPartyCallbacksExecutor:Ljava/util/concurrent/ExecutorService;

    if-nez v0, :cond_d

    .line 622
    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    sput-object v0, Lcom/adobe/mobile/StaticMethods;->thirdPartyCallbacksExecutor:Ljava/util/concurrent/ExecutorService;

    .line 624
    :cond_d
    sget-object v0, Lcom/adobe/mobile/StaticMethods;->thirdPartyCallbacksExecutor:Ljava/util/concurrent/ExecutorService;
    :try_end_f
    .catchall {:try_start_3 .. :try_end_f} :catchall_11

    monitor-exit v1

    return-object v0

    .line 625
    :catchall_11
    move-exception v2

    monitor-exit v1

    throw v2
.end method

.method protected static getTimeSince1970()J
    .registers 4

    .line 1144
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    return-wide v0
.end method

.method protected static getTimeSinceLaunch()J
    .registers 6

    .line 1228
    sget-wide v2, Lcom/adobe/mobile/Lifecycle;->sessionStartTime:J

    .line 1229
    .local v2, "originalStartDate":J
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getTimeSince1970()J

    move-result-wide v0

    sub-long v4, v0, v2

    .line 1232
    .local v4, "timeSinceLaunch":J
    const-wide/32 v0, 0x93a80

    cmp-long v0, v4, v0

    if-gez v0, :cond_11

    move-wide v0, v4

    goto :goto_13

    :cond_11
    const-wide/16 v0, 0x0

    :goto_13
    return-wide v0
.end method

.method protected static getTimestampString()Ljava/lang/String;
    .registers 12

    .line 534
    sget-object v8, Lcom/adobe/mobile/StaticMethods;->_timestampMutex:Ljava/lang/Object;

    monitor-enter v8

    .line 535
    :try_start_3
    sget-object v0, Lcom/adobe/mobile/StaticMethods;->timestamp:Ljava/lang/String;

    if-nez v0, :cond_6c

    .line 536
    new-instance v9, Ljava/util/Date;

    invoke-direct {v9}, Ljava/util/Date;-><init>()V

    .line 537
    .local v9, "date":Ljava/util/Date;
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v10

    .line 538
    .local v10, "tm":Ljava/util/Calendar;
    invoke-virtual {v10, v9}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 539
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "00/00/0000 00:00:00 0 "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 540
    invoke-virtual {v10}, Ljava/util/Calendar;->getTimeZone()Ljava/util/TimeZone;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v10, v2}, Ljava/util/Calendar;->get(I)I

    move-result v3

    const/4 v2, 0x2

    invoke-virtual {v10, v2}, Ljava/util/Calendar;->get(I)I

    move-result v4

    const/4 v2, 0x5

    invoke-virtual {v10, v2}, Ljava/util/Calendar;->get(I)I

    move-result v5

    const/4 v2, 0x7

    invoke-virtual {v10, v2}, Ljava/util/Calendar;->get(I)I

    move-result v6

    const/16 v2, 0xb

    invoke-virtual {v10, v2}, Ljava/util/Calendar;->get(I)I

    move-result v2

    mul-int/lit8 v2, v2, 0x3c

    const/16 v7, 0xc

    invoke-virtual {v10, v7}, Ljava/util/Calendar;->get(I)I

    move-result v7

    add-int/2addr v2, v7

    mul-int/lit8 v2, v2, 0x3c

    const/16 v7, 0xd

    invoke-virtual {v10, v7}, Ljava/util/Calendar;->get(I)I

    move-result v7

    add-int/2addr v2, v7

    mul-int/lit16 v2, v2, 0x3e8

    const/16 v7, 0xe

    invoke-virtual {v10, v7}, Ljava/util/Calendar;->get(I)I

    move-result v7

    add-int/2addr v7, v2

    const/4 v2, 0x1

    invoke-virtual/range {v1 .. v7}, Ljava/util/TimeZone;->getOffset(IIIIII)I

    move-result v1

    const v2, 0xea60

    div-int/2addr v1, v2

    mul-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/adobe/mobile/StaticMethods;->timestamp:Ljava/lang/String;

    .line 543
    .end local v9    # "date":Ljava/util/Date;
    .end local v10    # "tm":Ljava/util/Calendar;
    :cond_6c
    sget-object v0, Lcom/adobe/mobile/StaticMethods;->timestamp:Ljava/lang/String;
    :try_end_6e
    .catchall {:try_start_3 .. :try_end_6e} :catchall_70

    monitor-exit v8

    return-object v0

    .line 544
    :catchall_70
    move-exception v11

    monitor-exit v8

    throw v11
.end method

.method protected static getVisitorID()Ljava/lang/String;
    .registers 7

    .line 327
    sget-object v4, Lcom/adobe/mobile/StaticMethods;->_visitorIDMutex:Ljava/lang/Object;

    monitor-enter v4

    .line 328
    :try_start_3
    sget-object v0, Lcom/adobe/mobile/StaticMethods;->visitorID:Ljava/lang/String;
    :try_end_5
    .catchall {:try_start_3 .. :try_end_5} :catchall_29

    if-nez v0, :cond_25

    .line 330
    :try_start_7
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "APP_MEASUREMENT_VISITOR_ID"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/adobe/mobile/StaticMethods;->visitorID:Ljava/lang/String;
    :try_end_14
    .catch Lcom/adobe/mobile/StaticMethods$NullContextException; {:try_start_7 .. :try_end_14} :catch_15
    .catchall {:try_start_7 .. :try_end_14} :catchall_29

    .line 333
    goto :goto_25

    .line 331
    :catch_15
    move-exception v5

    .line 332
    .local v5, "e":Lcom/adobe/mobile/StaticMethods$NullContextException;
    const-string v0, "Config - Unable to pull visitorID from shared preferences. (%s)"

    const/4 v1, 0x1

    :try_start_19
    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v5}, Lcom/adobe/mobile/StaticMethods$NullContextException;->getMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logErrorFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 336
    .end local v5    # "e":Lcom/adobe/mobile/StaticMethods$NullContextException;
    :cond_25
    :goto_25
    sget-object v0, Lcom/adobe/mobile/StaticMethods;->visitorID:Ljava/lang/String;
    :try_end_27
    .catchall {:try_start_19 .. :try_end_27} :catchall_29

    monitor-exit v4

    return-object v0

    .line 337
    :catchall_29
    move-exception v6

    monitor-exit v4

    throw v6
.end method

.method protected static hexToString(Ljava/lang/String;)Ljava/lang/String;
    .registers 9
    .param p0, "hexString"    # Ljava/lang/String;

    .line 1341
    if-eqz p0, :cond_10

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_10

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    rem-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_12

    .line 1342
    :cond_10
    const/4 v0, 0x0

    return-object v0

    .line 1345
    :cond_12
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v4

    .line 1346
    .local v4, "length":I
    div-int/lit8 v0, v4, 0x2

    new-array v5, v0, [B

    .line 1347
    .local v5, "data":[B
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_1b
    if-ge v6, v4, :cond_3e

    .line 1348
    div-int/lit8 v0, v6, 0x2

    invoke-virtual {p0, v6}, Ljava/lang/String;->charAt(I)C

    move-result v1

    const/16 v2, 0x10

    invoke-static {v1, v2}, Ljava/lang/Character;->digit(CI)I

    move-result v1

    shl-int/lit8 v1, v1, 0x4

    add-int/lit8 v2, v6, 0x1

    .line 1349
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v3, 0x10

    invoke-static {v2, v3}, Ljava/lang/Character;->digit(CI)I

    move-result v2

    add-int/2addr v1, v2

    int-to-byte v1, v1

    aput-byte v1, v5, v0

    .line 1347
    add-int/lit8 v6, v6, 0x2

    goto :goto_1b

    .line 1352
    .end local v6    # "i":I
    :cond_3e
    const/4 v6, 0x0

    .line 1354
    .local v6, "decodedString":Ljava/lang/String;
    :try_start_3f
    new-instance v0, Ljava/lang/String;

    const-string v1, "UTF-8"

    invoke-direct {v0, v5, v1}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_46
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_3f .. :try_end_46} :catch_48

    move-object v6, v0

    .line 1358
    goto :goto_58

    .line 1356
    :catch_48
    move-exception v7

    .line 1357
    .local v7, "ex":Ljava/io/UnsupportedEncodingException;
    const-string v0, "Failed to get string from hex (%s)"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v7}, Ljava/io/UnsupportedEncodingException;->getMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logDebugFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1360
    .end local v7    # "ex":Ljava/io/UnsupportedEncodingException;
    :goto_58
    return-object v6
.end method

.method protected static isWearableApp()Z
    .registers 1

    .line 823
    sget-boolean v0, Lcom/adobe/mobile/StaticMethods;->_isWearable:Z

    return v0
.end method

.method protected static join(Ljava/lang/Iterable;Ljava/lang/String;)Ljava/lang/String;
    .registers 5
    .param p0, "elements"    # Ljava/lang/Iterable;
    .param p1, "delimiter"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/lang/Iterable<*>;Ljava/lang/String;)Ljava/lang/String;"
        }
    .end annotation

    .line 890
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 892
    .local v1, "sb":Ljava/lang/StringBuilder;
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 894
    .local v2, "iter":Ljava/util/Iterator;
    :goto_9
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_21

    .line 895
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 896
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1d

    .line 897
    goto :goto_21

    .line 899
    :cond_1d
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_9

    .line 901
    :cond_21
    :goto_21
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected static varargs logDebugFormat(Ljava/lang/String;[Ljava/lang/Object;)V
    .registers 6
    .param p0, "format"    # Ljava/lang/String;
    .param p1, "args"    # [Ljava/lang/Object;

    .line 977
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getDebugLogging()Z

    move-result v0

    if-eqz v0, :cond_40

    .line 978
    if-eqz p1, :cond_28

    array-length v0, p1

    if-lez v0, :cond_28

    .line 979
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ADBMobile Debug : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 980
    .local v3, "formattedString":Ljava/lang/String;
    const-string v0, "ADBMobile"

    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 981
    .end local v3    # "formattedString":Ljava/lang/String;
    goto :goto_40

    .line 983
    :cond_28
    const-string v0, "ADBMobile"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ADBMobile Debug : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 986
    :cond_40
    :goto_40
    return-void
.end method

.method protected static varargs logErrorFormat(Ljava/lang/String;[Ljava/lang/Object;)V
    .registers 6
    .param p0, "format"    # Ljava/lang/String;
    .param p1, "args"    # [Ljava/lang/Object;

    .line 955
    if-eqz p1, :cond_22

    array-length v0, p1

    if-lez v0, :cond_22

    .line 956
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ADBMobile Error : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 957
    .local v3, "formattedString":Ljava/lang/String;
    const-string v0, "ADBMobile"

    invoke-static {v0, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 958
    .end local v3    # "formattedString":Ljava/lang/String;
    goto :goto_3a

    .line 960
    :cond_22
    const-string v0, "ADBMobile"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ADBMobile Error : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 962
    :goto_3a
    return-void
.end method

.method protected static varargs logWarningFormat(Ljava/lang/String;[Ljava/lang/Object;)V
    .registers 6
    .param p0, "format"    # Ljava/lang/String;
    .param p1, "args"    # [Ljava/lang/Object;

    .line 965
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getDebugLogging()Z

    move-result v0

    if-eqz v0, :cond_40

    .line 966
    if-eqz p1, :cond_28

    array-length v0, p1

    if-lez v0, :cond_28

    .line 967
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ADBMobile Warning : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 968
    .local v3, "formattedString":Ljava/lang/String;
    const-string v0, "ADBMobile"

    invoke-static {v0, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 969
    .end local v3    # "formattedString":Ljava/lang/String;
    goto :goto_40

    .line 971
    :cond_28
    const-string v0, "ADBMobile"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ADBMobile Warning : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 974
    :cond_40
    :goto_40
    return-void
.end method

.method protected static mapFromJson(Lorg/json/JSONObject;)Ljava/util/HashMap;
    .registers 9
    .param p0, "jsonData"    # Lorg/json/JSONObject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Lorg/json/JSONObject;)Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
        }
    .end annotation

    .line 1237
    if-nez p0, :cond_4

    .line 1238
    const/4 v0, 0x0

    return-object v0

    .line 1242
    :cond_4
    invoke-virtual {p0}, Lorg/json/JSONObject;->keys()Ljava/util/Iterator;

    move-result-object v4

    .line 1243
    .local v4, "keyItr":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    .line 1244
    .local v5, "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    :goto_d
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_33

    .line 1245
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Ljava/lang/String;

    .line 1247
    .local v6, "name":Ljava/lang/String;
    :try_start_1a
    invoke-virtual {p0, v6}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v6, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_21
    .catch Lorg/json/JSONException; {:try_start_1a .. :try_end_21} :catch_22

    .line 1250
    goto :goto_32

    .line 1248
    :catch_22
    move-exception v7

    .line 1249
    .local v7, "ex":Lorg/json/JSONException;
    const-string v0, "Problem parsing json data (%s)"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v7}, Lorg/json/JSONException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logWarningFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1251
    .end local v6    # "name":Ljava/lang/String;
    .end local v7    # "ex":Lorg/json/JSONException;
    :goto_32
    goto :goto_d

    .line 1252
    :cond_33
    return-object v5
.end method

.method private static retrieveAIDFromVisitorIDService()Ljava/lang/String;
    .registers 9

    .line 744
    new-instance v4, Ljava/lang/StringBuilder;

    const/16 v0, 0x40

    invoke-direct {v4, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 745
    .local v4, "urlSb":Ljava/lang/StringBuilder;
    invoke-static {}, Lcom/adobe/mobile/MobileConfig;->getInstance()Lcom/adobe/mobile/MobileConfig;

    move-result-object v0

    invoke-virtual {v0}, Lcom/adobe/mobile/MobileConfig;->getSSL()Z

    move-result v0

    if-eqz v0, :cond_14

    const-string v0, "https"

    goto :goto_16

    :cond_14
    const-string v0, "http"

    :goto_16
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 746
    const-string v0, "://"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 747
    invoke-static {}, Lcom/adobe/mobile/MobileConfig;->getInstance()Lcom/adobe/mobile/MobileConfig;

    move-result-object v0

    invoke-virtual {v0}, Lcom/adobe/mobile/MobileConfig;->getTrackingServer()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 748
    const-string v0, "/id"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 750
    invoke-static {}, Lcom/adobe/mobile/MobileConfig;->getInstance()Lcom/adobe/mobile/MobileConfig;

    move-result-object v0

    invoke-virtual {v0}, Lcom/adobe/mobile/MobileConfig;->getVisitorIdServiceEnabled()Z

    move-result v5

    .line 752
    .local v5, "useVisidService":Z
    if-eqz v5, :cond_43

    .line 753
    invoke-static {}, Lcom/adobe/mobile/VisitorIDService;->sharedInstance()Lcom/adobe/mobile/VisitorIDService;

    move-result-object v0

    invoke-virtual {v0}, Lcom/adobe/mobile/VisitorIDService;->getAnalyticsIDRequestParameterString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 756
    :cond_43
    const-string v0, "Analytics ID - Sending Analytics ID call(%s)"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object v4, v1, v2

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logDebugFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 757
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Analytics ID"

    const/4 v2, 0x0

    const/16 v3, 0x1f4

    invoke-static {v0, v2, v3, v1}, Lcom/adobe/mobile/RequestHandler;->retrieveData(Ljava/lang/String;Ljava/util/Map;ILjava/lang/String;)[B

    move-result-object v6

    .line 759
    .local v6, "serverResponse":[B
    const/4 v7, 0x0

    .line 760
    .local v7, "identifier":Ljava/lang/String;
    if-eqz v6, :cond_95

    .line 762
    :try_start_5e
    new-instance v8, Lorg/json/JSONObject;

    new-instance v0, Ljava/lang/String;

    const-string v1, "UTF-8"

    invoke-direct {v0, v6, v1}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    invoke-direct {v8, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 763
    .local v8, "jsonResponse":Lorg/json/JSONObject;
    const-string v0, "id"

    invoke-virtual {v8, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_6f
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_5e .. :try_end_6f} :catch_72
    .catch Lorg/json/JSONException; {:try_start_5e .. :try_end_6f} :catch_83

    move-result-object v0

    move-object v7, v0

    .line 770
    .end local v8    # "jsonResponse":Lorg/json/JSONObject;
    goto :goto_95

    .line 764
    :catch_72
    move-exception v8

    .line 765
    .local v8, "ex":Ljava/io/UnsupportedEncodingException;
    const-string v0, "Analytics ID - Unable to decode /id response(%s)"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v8}, Ljava/io/UnsupportedEncodingException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logErrorFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 770
    .end local v8    # "ex":Ljava/io/UnsupportedEncodingException;
    goto :goto_95

    .line 766
    :catch_83
    move-exception v8

    .line 767
    .local v8, "ex":Lorg/json/JSONException;
    if-nez v5, :cond_95

    .line 768
    const-string v0, "Analytics ID - Unable to parse /id response(%s)"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v8}, Lorg/json/JSONException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logErrorFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 774
    .end local v8    # "ex":Lorg/json/JSONException;
    :cond_95
    :goto_95
    if-eqz v7, :cond_9f

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v1, 0x21

    if-eq v0, v1, :cond_a7

    .line 775
    :cond_9f
    if-eqz v5, :cond_a3

    .line 776
    const/4 v0, 0x0

    return-object v0

    .line 779
    :cond_a3
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->generateAID()Ljava/lang/String;

    move-result-object v7

    .line 782
    :cond_a7
    return-object v7
.end method

.method private static serializeKeyValuePair(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/StringBuilder;)V
    .registers 5
    .param p0, "key"    # Ljava/lang/String;
    .param p1, "value"    # Ljava/lang/Object;
    .param p2, "builder"    # Ljava/lang/StringBuilder;

    .line 870
    if-eqz p0, :cond_e

    if-eqz p1, :cond_e

    instance-of v0, p1, Lcom/adobe/mobile/ContextData;

    if-nez v0, :cond_e

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    if-gtz v0, :cond_f

    .line 871
    :cond_e
    return-void

    .line 874
    :cond_f
    instance-of v0, p1, Ljava/lang/String;

    if-eqz v0, :cond_1d

    move-object v0, p1

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-gtz v0, :cond_1d

    .line 875
    return-void

    .line 878
    :cond_1d
    const-string v0, "&"

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 879
    invoke-virtual {p2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 880
    const-string v0, "="

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 882
    instance-of v0, p1, Ljava/util/List;

    if-eqz v0, :cond_3f

    .line 883
    move-object v0, p1

    check-cast v0, Ljava/util/List;

    const-string v1, ","

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->join(Ljava/lang/Iterable;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/adobe/mobile/StaticMethods;->URLEncode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_4a

    .line 885
    :cond_3f
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/adobe/mobile/StaticMethods;->URLEncode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 887
    :goto_4a
    return-void
.end method

.method protected static serializeToQueryString(Ljava/util/Map;Ljava/lang/StringBuilder;)V
    .registers 8
    .param p0, "parameters"    # Ljava/util/Map;
    .param p1, "builder"    # Ljava/lang/StringBuilder;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;Ljava/lang/StringBuilder;)V"
        }
    .end annotation

    .line 831
    if-nez p0, :cond_3

    .line 832
    return-void

    .line 835
    :cond_3
    invoke-interface {p0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_b
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_65

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Ljava/util/Map$Entry;

    .line 836
    .local v2, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Object;>;"
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/adobe/mobile/StaticMethods;->URLEncode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 839
    .local v3, "key":Ljava/lang/String;
    if-nez v3, :cond_25

    .line 840
    goto :goto_b

    .line 843
    :cond_25
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    .line 846
    .local v4, "obj":Ljava/lang/Object;
    instance-of v0, v4, Lcom/adobe/mobile/ContextData;

    if-eqz v0, :cond_60

    .line 847
    move-object v5, v4

    check-cast v5, Lcom/adobe/mobile/ContextData;

    .line 849
    .local v5, "data":Lcom/adobe/mobile/ContextData;
    iget-object v0, v5, Lcom/adobe/mobile/ContextData;->value:Ljava/lang/Object;

    if-eqz v0, :cond_39

    .line 850
    iget-object v0, v5, Lcom/adobe/mobile/ContextData;->value:Ljava/lang/Object;

    invoke-static {v3, v0, p1}, Lcom/adobe/mobile/StaticMethods;->serializeKeyValuePair(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/StringBuilder;)V

    .line 854
    :cond_39
    iget-object v0, v5, Lcom/adobe/mobile/ContextData;->contextData:Ljava/util/HashMap;

    if-eqz v0, :cond_5f

    iget-object v0, v5, Lcom/adobe/mobile/ContextData;->contextData:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v0

    if-lez v0, :cond_5f

    .line 855
    const-string v0, "&"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 856
    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 857
    const-string v0, "."

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 858
    iget-object v0, v5, Lcom/adobe/mobile/ContextData;->contextData:Ljava/util/HashMap;

    invoke-static {v0, p1}, Lcom/adobe/mobile/StaticMethods;->serializeToQueryString(Ljava/util/Map;Ljava/lang/StringBuilder;)V

    .line 859
    const-string v0, "&."

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 860
    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 862
    .end local v5    # "data":Lcom/adobe/mobile/ContextData;
    :cond_5f
    goto :goto_63

    .line 863
    :cond_60
    invoke-static {v3, v4, p1}, Lcom/adobe/mobile/StaticMethods;->serializeKeyValuePair(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/StringBuilder;)V

    .line 865
    .end local v2    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Object;>;"
    .end local v2
    .end local v3    # "key":Ljava/lang/String;
    .end local v4    # "obj":Ljava/lang/Object;
    :goto_63
    goto/16 :goto_b

    .line 866
    :cond_65
    return-void
.end method

.method protected static setApplicationType(Lcom/adobe/mobile/Config$ApplicationType;)V
    .registers 3
    .param p0, "apptype"    # Lcom/adobe/mobile/Config$ApplicationType;

    .line 814
    sput-object p0, Lcom/adobe/mobile/StaticMethods;->_appType:Lcom/adobe/mobile/Config$ApplicationType;

    .line 815
    sget-object v0, Lcom/adobe/mobile/StaticMethods;->_appType:Lcom/adobe/mobile/Config$ApplicationType;

    sget-object v1, Lcom/adobe/mobile/Config$ApplicationType;->APPLICATION_TYPE_WEARABLE:Lcom/adobe/mobile/Config$ApplicationType;

    if-ne v0, v1, :cond_a

    const/4 v0, 0x1

    goto :goto_b

    :cond_a
    const/4 v0, 0x0

    :goto_b
    sput-boolean v0, Lcom/adobe/mobile/StaticMethods;->_isWearable:Z

    .line 816
    return-void
.end method

.method protected static setCurrentActivity(Landroid/app/Activity;)V
    .registers 4
    .param p0, "activity"    # Landroid/app/Activity;

    .line 1258
    sget-object v1, Lcom/adobe/mobile/StaticMethods;->_currentActivityMutex:Ljava/lang/Object;

    monitor-enter v1

    .line 1259
    :try_start_3
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    sput-object v0, Lcom/adobe/mobile/StaticMethods;->_activity:Ljava/lang/ref/WeakReference;
    :try_end_a
    .catchall {:try_start_3 .. :try_end_a} :catchall_c

    .line 1260
    monitor-exit v1

    goto :goto_f

    :catchall_c
    move-exception v2

    monitor-exit v1

    throw v2

    .line 1261
    :goto_f
    return-void
.end method

.method protected static setSharedContext(Landroid/content/Context;)V
    .registers 2
    .param p0, "context"    # Landroid/content/Context;

    .line 1218
    if-eqz p0, :cond_f

    .line 1219
    instance-of v0, p0, Landroid/app/Activity;

    if-eqz v0, :cond_d

    .line 1220
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    sput-object v0, Lcom/adobe/mobile/StaticMethods;->sharedContext:Landroid/content/Context;

    goto :goto_f

    .line 1222
    :cond_d
    sput-object p0, Lcom/adobe/mobile/StaticMethods;->sharedContext:Landroid/content/Context;

    .line 1225
    :cond_f
    :goto_f
    return-void
.end method

.method private static syncAIDIfNeeded(Ljava/lang/String;)V
    .registers 8
    .param p0, "aid"    # Ljava/lang/String;

    .line 710
    if-nez p0, :cond_3

    .line 711
    return-void

    .line 715
    :cond_3
    invoke-static {}, Lcom/adobe/mobile/MobileConfig;->getInstance()Lcom/adobe/mobile/MobileConfig;

    move-result-object v0

    invoke-virtual {v0}, Lcom/adobe/mobile/MobileConfig;->getVisitorIdServiceEnabled()Z

    move-result v0

    if-nez v0, :cond_e

    .line 716
    return-void

    .line 719
    :cond_e
    const/4 v4, 0x0

    .line 721
    .local v4, "idSynced":Z
    :try_start_f
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "ADOBEMOBILE_STOREDDEFAULTS_AID_SYNCED"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z
    :try_end_19
    .catch Lcom/adobe/mobile/StaticMethods$NullContextException; {:try_start_f .. :try_end_19} :catch_1c

    move-result v0

    move v4, v0

    .line 724
    goto :goto_2c

    .line 722
    :catch_1c
    move-exception v5

    .line 723
    .local v5, "ex":Lcom/adobe/mobile/StaticMethods$NullContextException;
    const-string v0, "Visitor ID - Null context when attempting to determine visitor ID sync status (%s)"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v5}, Lcom/adobe/mobile/StaticMethods$NullContextException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logWarningFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 726
    .end local v5    # "ex":Lcom/adobe/mobile/StaticMethods$NullContextException;
    :goto_2c
    if-nez v4, :cond_5d

    .line 727
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    .line 728
    .local v5, "integrationsMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v0, "AVID"

    invoke-virtual {v5, v0, p0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 729
    invoke-static {}, Lcom/adobe/mobile/VisitorIDService;->sharedInstance()Lcom/adobe/mobile/VisitorIDService;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/adobe/mobile/VisitorIDService;->idSync(Ljava/util/Map;)V

    .line 731
    :try_start_3f
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getSharedPreferencesEditor()Landroid/content/SharedPreferences$Editor;

    move-result-object v6

    .line 732
    .local v6, "e":Landroid/content/SharedPreferences$Editor;
    const-string v0, "ADOBEMOBILE_STOREDDEFAULTS_AID_SYNCED"

    const/4 v1, 0x1

    invoke-interface {v6, v0, v1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 733
    invoke-interface {v6}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_4c
    .catch Lcom/adobe/mobile/StaticMethods$NullContextException; {:try_start_3f .. :try_end_4c} :catch_4d

    .line 736
    .end local v6    # "e":Landroid/content/SharedPreferences$Editor;
    goto :goto_5d

    .line 734
    :catch_4d
    move-exception v6

    .line 735
    .local v6, "ex":Lcom/adobe/mobile/StaticMethods$NullContextException;
    const-string v0, "Visitor ID - Null context when attempting to persist visitor ID sync status (%s)"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v6}, Lcom/adobe/mobile/StaticMethods$NullContextException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logWarningFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 738
    .end local v5    # "integrationsMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v5
    .end local v6    # "ex":Lcom/adobe/mobile/StaticMethods$NullContextException;
    :cond_5d
    :goto_5d
    return-void
.end method

.method protected static translateContextData(Ljava/util/Map;)Lcom/adobe/mobile/ContextData;
    .registers 10
    .param p0, "dict"    # Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;)Lcom/adobe/mobile/ContextData;"
        }
    .end annotation

    .line 994
    new-instance v2, Lcom/adobe/mobile/ContextData;

    invoke-direct {v2}, Lcom/adobe/mobile/ContextData;-><init>()V

    .line 996
    .local v2, "tempContextData":Lcom/adobe/mobile/ContextData;
    invoke-static {p0}, Lcom/adobe/mobile/StaticMethods;->cleanContextDataDictionary(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_11
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_52

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Ljava/util/Map$Entry;

    .line 997
    .local v4, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Object;>;"
    invoke-interface {v4}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Ljava/lang/String;

    .line 999
    .local v5, "key":Ljava/lang/String;
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 1000
    .local v6, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v7, 0x0

    .line 1001
    .local v7, "pos":I
    :goto_2b
    const/16 v0, 0x2e

    invoke-virtual {v5, v0, v7}, Ljava/lang/String;->indexOf(II)I

    move-result v0

    move v8, v0

    .local v8, "end":I
    if-ltz v0, :cond_3e

    .line 1002
    invoke-virtual {v5, v7, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1003
    add-int/lit8 v7, v8, 0x1

    goto :goto_2b

    .line 1005
    :cond_3e
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v0

    invoke-virtual {v5, v7, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1007
    invoke-interface {v4}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v2, v6, v1}, Lcom/adobe/mobile/StaticMethods;->addValueToHashMap(Ljava/lang/Object;Lcom/adobe/mobile/ContextData;Ljava/util/List;I)V

    .line 1008
    .end local v4    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Object;>;"
    .end local v4
    .end local v5    # "key":Ljava/lang/String;
    .end local v6    # "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v6
    .end local v7    # "pos":I
    .end local v8    # "end":I
    goto :goto_11

    .line 1010
    :cond_52
    return-object v2
.end method

.method protected static updateLastKnownTimestamp(Ljava/lang/Long;)V
    .registers 7
    .param p0, "timestamp"    # Ljava/lang/Long;

    .line 1149
    invoke-static {}, Lcom/adobe/mobile/MobileConfig;->getInstance()Lcom/adobe/mobile/MobileConfig;

    move-result-object v4

    .line 1150
    .local v4, "config":Lcom/adobe/mobile/MobileConfig;
    if-nez v4, :cond_f

    .line 1151
    const-string v0, "Config - Lost config instance"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logErrorFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1152
    return-void

    .line 1155
    :cond_f
    invoke-virtual {v4}, Lcom/adobe/mobile/MobileConfig;->getOfflineTrackingEnabled()Z

    move-result v0

    if-nez v0, :cond_16

    .line 1156
    return-void

    .line 1160
    :cond_16
    :try_start_16
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getSharedPreferencesEditor()Landroid/content/SharedPreferences$Editor;

    move-result-object v5

    .line 1161
    .local v5, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v0, "ADBLastKnownTimestampKey"

    invoke-virtual {p0}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-interface {v5, v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 1162
    invoke-interface {v5}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_26
    .catch Lcom/adobe/mobile/StaticMethods$NullContextException; {:try_start_16 .. :try_end_26} :catch_27

    .line 1165
    .end local v5    # "editor":Landroid/content/SharedPreferences$Editor;
    goto :goto_37

    .line 1163
    :catch_27
    move-exception v5

    .line 1164
    .local v5, "e":Lcom/adobe/mobile/StaticMethods$NullContextException;
    const-string v0, "Config - Error while updating last known timestamp. (%s)"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v5}, Lcom/adobe/mobile/StaticMethods$NullContextException;->getMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logErrorFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1166
    .end local v5    # "e":Lcom/adobe/mobile/StaticMethods$NullContextException;
    :goto_37
    return-void
.end method

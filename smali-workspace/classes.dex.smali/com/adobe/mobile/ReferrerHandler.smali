.class final Lcom/adobe/mobile/ReferrerHandler;
.super Ljava/lang/Object;
.source "ReferrerHandler.java"


# static fields
.field static final ADOBE_DATA_BLACKLISTED_NAMESPACED_KEYS_SET:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet<Ljava/lang/String;>;"
        }
    .end annotation
.end field

.field private static _referrerProcessed:Z


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .line 49
    const/4 v0, 0x1

    sput-boolean v0, Lcom/adobe/mobile/ReferrerHandler;->_referrerProcessed:Z

    .line 59
    new-instance v0, Lcom/adobe/mobile/ReferrerHandler$1;

    invoke-direct {v0}, Lcom/adobe/mobile/ReferrerHandler$1;-><init>()V

    sput-object v0, Lcom/adobe/mobile/ReferrerHandler;->ADOBE_DATA_BLACKLISTED_NAMESPACED_KEYS_SET:Ljava/util/HashSet;

    return-void
.end method

.method constructor <init>()V
    .registers 1

    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method protected static appendNamespaceToGoogleReferrerFields(Ljava/util/HashMap;)Ljava/util/HashMap;
    .registers 9
    .param p0, "referrerFields"    # Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;)Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
        }
    .end annotation

    .line 201
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 203
    .local v2, "processedFields":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    if-eqz p0, :cond_d

    invoke-virtual {p0}, Ljava/util/HashMap;->size()I

    move-result v0

    if-nez v0, :cond_e

    .line 204
    :cond_d
    return-object v2

    .line 207
    :cond_e
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3, p0}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    .line 209
    .local v3, "referrerFieldsCopy":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    const-string v0, "utm_campaign"

    invoke-virtual {v3, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_26

    .line 210
    const-string v0, "a.referrer.campaign.name"

    const-string v1, "utm_campaign"

    invoke-virtual {v3, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 212
    :cond_26
    const-string v0, "utm_source"

    invoke-virtual {v3, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_39

    .line 213
    const-string v0, "a.referrer.campaign.source"

    const-string v1, "utm_source"

    invoke-virtual {v3, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 215
    :cond_39
    const-string v0, "utm_medium"

    invoke-virtual {v3, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4c

    .line 216
    const-string v0, "a.referrer.campaign.medium"

    const-string v1, "utm_medium"

    invoke-virtual {v3, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 218
    :cond_4c
    const-string v0, "utm_term"

    invoke-virtual {v3, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5f

    .line 219
    const-string v0, "a.referrer.campaign.term"

    const-string v1, "utm_term"

    invoke-virtual {v3, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 221
    :cond_5f
    const-string v0, "utm_content"

    invoke-virtual {v3, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_72

    .line 222
    const-string v0, "a.referrer.campaign.content"

    const-string v1, "utm_content"

    invoke-virtual {v3, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 224
    :cond_72
    const-string v0, "trackingcode"

    invoke-virtual {v3, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_85

    .line 225
    const-string v0, "a.referrer.campaign.trackingcode"

    const-string v1, "trackingcode"

    invoke-virtual {v3, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 229
    :cond_85
    invoke-virtual {v3}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v4

    .line 231
    .local v4, "entrySet":Ljava/util/Set;, "Ljava/util/Set<Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Object;>;>;"
    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_8d
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_cc

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Ljava/util/Map$Entry;

    .line 232
    .local v6, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Object;>;"
    invoke-interface {v6}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Ljava/lang/String;

    .line 233
    .local v7, "key":Ljava/lang/String;
    const-string v0, "a.acquisition.custom."

    invoke-virtual {v7, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_c4

    const-string v0, "a.referrer.campaign."

    .line 234
    invoke-virtual {v7, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_c4

    .line 236
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "a.acquisition.custom."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 239
    :cond_c4
    invoke-interface {v6}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v2, v7, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 240
    .end local v6    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Object;>;"
    .end local v6
    .end local v7    # "key":Ljava/lang/String;
    goto :goto_8d

    .line 242
    :cond_cc
    return-object v2
.end method

.method protected static filterForBlacklistedKeys(Ljava/util/HashMap;)Ljava/util/HashMap;
    .registers 6
    .param p0, "adobeData"    # Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;)Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
        }
    .end annotation

    .line 321
    if-eqz p0, :cond_8

    invoke-virtual {p0}, Ljava/util/HashMap;->size()I

    move-result v0

    if-nez v0, :cond_a

    .line 322
    :cond_8
    const/4 v0, 0x0

    return-object v0

    .line 325
    :cond_a
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1, p0}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    .line 327
    .local v1, "filteredAdobeData":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    invoke-virtual {v1}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v2

    .line 329
    .local v2, "entrySet":Ljava/util/Set;, "Ljava/util/Set<Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Object;>;>;"
    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 331
    .local v3, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Object;>;>;"
    :goto_17
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_34

    .line 332
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Ljava/util/Map$Entry;

    .line 333
    .local v4, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Object;>;"
    invoke-interface {v4}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/adobe/mobile/ReferrerHandler;->isBlackListed(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_33

    .line 334
    invoke-interface {v3}, Ljava/util/Iterator;->remove()V

    .line 336
    .end local v4    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Object;>;"
    .end local v4
    :cond_33
    goto :goto_17

    .line 338
    :cond_34
    return-object v1
.end method

.method protected static generateURLForV3(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .registers 9
    .param p0, "ugid"    # Ljava/lang/String;
    .param p1, "adid"    # Ljava/lang/String;

    .line 543
    new-instance v4, Ljava/lang/StringBuilder;

    const/16 v0, 0x40

    invoke-direct {v4, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 544
    .local v4, "urlSb":Ljava/lang/StringBuilder;
    invoke-static {}, Lcom/adobe/mobile/MobileConfig;->getInstance()Lcom/adobe/mobile/MobileConfig;

    move-result-object v5

    .line 545
    .local v5, "mobileConfig":Lcom/adobe/mobile/MobileConfig;
    const-string v0, "https://%s/v3/%s/end"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v5}, Lcom/adobe/mobile/MobileConfig;->getAcquisitionServer()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-virtual {v5}, Lcom/adobe/mobile/MobileConfig;->getAcquisitionAppId()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 547
    new-instance v6, Ljava/lang/StringBuilder;

    const/16 v0, 0x40

    invoke-direct {v6, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 548
    .local v6, "querySb":Ljava/lang/StringBuilder;
    if-eqz p0, :cond_47

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_47

    .line 549
    const-string v0, "?a_ugid=%s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p0}, Lcom/adobe/mobile/StaticMethods;->URLEncode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 551
    :cond_47
    if-eqz p1, :cond_70

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_70

    .line 552
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-lez v0, :cond_58

    const-string v0, "&"

    goto :goto_5a

    :cond_58
    const-string v0, "?"

    :goto_5a
    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 553
    const-string v0, "a_cid=%s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p1}, Lcom/adobe/mobile/StaticMethods;->URLEncode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 556
    :cond_70
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected static getDataFromJSON(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/util/HashMap;
    .registers 10
    .param p0, "responseObject"    # Lorg/json/JSONObject;
    .param p1, "jsonKey"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
        }
    .end annotation

    .line 465
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 467
    .local v3, "contextDataMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    if-nez p0, :cond_8

    .line 468
    return-object v3

    .line 473
    :cond_8
    :try_start_8
    invoke-virtual {p0, p1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;
    :try_end_b
    .catch Lorg/json/JSONException; {:try_start_8 .. :try_end_b} :catch_d

    move-result-object v4

    .line 476
    .local v4, "contextData":Lorg/json/JSONObject;
    goto :goto_f

    .line 474
    .end local v4    # "contextData":Lorg/json/JSONObject;
    :catch_d
    move-exception v5

    .line 475
    .local v5, "ex":Lorg/json/JSONException;
    return-object v3

    .line 478
    .local v4, "contextData":Lorg/json/JSONObject;
    .end local v5    # "ex":Lorg/json/JSONException;
    :goto_f
    if-nez v4, :cond_12

    .line 479
    return-object v3

    .line 482
    :cond_12
    invoke-virtual {v4}, Lorg/json/JSONObject;->keys()Ljava/util/Iterator;

    move-result-object v5

    .line 484
    .local v5, "keys":Ljava/util/Iterator;
    :goto_16
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_39

    .line 485
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    .line 487
    .local v6, "key":Ljava/lang/String;
    :try_start_24
    invoke-virtual {v4, v6}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v6, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_2b
    .catch Lorg/json/JSONException; {:try_start_24 .. :try_end_2b} :catch_2c

    .line 490
    goto :goto_38

    .line 488
    :catch_2c
    move-exception v7

    .line 489
    .local v7, "ex":Lorg/json/JSONException;
    const-string v0, "Analytics - Unable to parse acquisition service response (the value for %s is not a string)"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object v6, v1, v2

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logDebugFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 491
    .end local v6    # "key":Ljava/lang/String;
    .end local v7    # "ex":Lorg/json/JSONException;
    :goto_38
    goto :goto_16

    .line 492
    :cond_39
    return-object v3
.end method

.method protected static getDeepLinkFromJSON(Lorg/json/JSONObject;)Ljava/lang/String;
    .registers 5
    .param p0, "responseObject"    # Lorg/json/JSONObject;

    .line 496
    if-nez p0, :cond_4

    .line 497
    const/4 v0, 0x0

    return-object v0

    .line 501
    :cond_4
    const-string v0, "adobeData"

    :try_start_6
    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;
    :try_end_9
    .catch Lorg/json/JSONException; {:try_start_6 .. :try_end_9} :catch_b

    move-result-object v1

    .line 504
    .local v1, "adobeData":Lorg/json/JSONObject;
    goto :goto_e

    .line 502
    .end local v1    # "adobeData":Lorg/json/JSONObject;
    :catch_b
    move-exception v2

    .line 503
    .local v2, "ex":Lorg/json/JSONException;
    const/4 v0, 0x0

    return-object v0

    .line 506
    .local v1, "adobeData":Lorg/json/JSONObject;
    .end local v2    # "ex":Lorg/json/JSONException;
    :goto_e
    if-nez v1, :cond_12

    .line 507
    const/4 v0, 0x0

    return-object v0

    .line 512
    :cond_12
    const-string v0, "a.acquisition.custom.link_deferred"

    :try_start_14
    invoke-virtual {v1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_17
    .catch Lorg/json/JSONException; {:try_start_14 .. :try_end_17} :catch_19

    move-result-object v2

    .line 515
    .local v2, "deepLink":Ljava/lang/String;
    goto :goto_1c

    .line 513
    .end local v2    # "deepLink":Ljava/lang/String;
    :catch_19
    move-exception v3

    .line 514
    .local v3, "ex":Lorg/json/JSONException;
    const/4 v0, 0x0

    return-object v0

    .line 517
    .local v2, "deepLink":Ljava/lang/String;
    .end local v3    # "ex":Lorg/json/JSONException;
    :goto_1c
    return-object v2
.end method

.method protected static getReferrerDataFromV3Server(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .registers 10
    .param p0, "ugid"    # Ljava/lang/String;
    .param p1, "adid"    # Ljava/lang/String;

    .line 521
    invoke-static {}, Lcom/adobe/mobile/MobileConfig;->getInstance()Lcom/adobe/mobile/MobileConfig;

    move-result-object v0

    invoke-virtual {v0}, Lcom/adobe/mobile/MobileConfig;->mobileReferrerConfigured()Z

    move-result v0

    if-nez v0, :cond_c

    .line 522
    const/4 v0, 0x0

    return-object v0

    .line 524
    :cond_c
    invoke-static {p0, p1}, Lcom/adobe/mobile/ReferrerHandler;->generateURLForV3(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 525
    .local v4, "url":Ljava/lang/String;
    const-string v0, "Analytics - Trying to fetch referrer data from (%s)"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object v4, v1, v2

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logDebugFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 527
    invoke-static {}, Lcom/adobe/mobile/MobileConfig;->getInstance()Lcom/adobe/mobile/MobileConfig;

    move-result-object v0

    invoke-virtual {v0}, Lcom/adobe/mobile/MobileConfig;->getReferrerTimeout()I

    move-result v0

    const-string v1, "Analytics"

    const/4 v2, 0x0

    invoke-static {v4, v2, v0, v1}, Lcom/adobe/mobile/RequestHandler;->retrieveData(Ljava/lang/String;Ljava/util/Map;ILjava/lang/String;)[B

    move-result-object v5

    .line 528
    .local v5, "responseByteArray":[B
    if-nez v5, :cond_2e

    .line 529
    const/4 v0, 0x0

    return-object v0

    .line 533
    :cond_2e
    :try_start_2e
    new-instance v6, Ljava/lang/String;

    const-string v0, "UTF-8"

    invoke-direct {v6, v5, v0}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_35
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_2e .. :try_end_35} :catch_36

    .line 537
    .local v6, "responseString":Ljava/lang/String;
    goto :goto_48

    .line 534
    .end local v6    # "responseString":Ljava/lang/String;
    :catch_36
    move-exception v7

    .line 535
    .local v7, "ex":Ljava/io/UnsupportedEncodingException;
    const-string v0, "Analytics - Unable to decode response(%s)"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v7}, Ljava/io/UnsupportedEncodingException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logErrorFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 536
    const/4 v0, 0x0

    return-object v0

    .line 538
    .local v6, "responseString":Ljava/lang/String;
    .end local v7    # "ex":Ljava/io/UnsupportedEncodingException;
    :goto_48
    return-object v6
.end method

.method protected static getReferrerProcessed()Z
    .registers 1

    .line 56
    sget-boolean v0, Lcom/adobe/mobile/ReferrerHandler;->_referrerProcessed:Z

    return v0
.end method

.method protected static getReferrerURLFromIntent(Landroid/content/Intent;)Ljava/lang/String;
    .registers 5
    .param p0, "intent"    # Landroid/content/Intent;

    .line 94
    if-nez p0, :cond_c

    .line 95
    const-string v0, "Analytics - Unable to process referrer due to an invalid intent parameter"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logWarningFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 96
    const/4 v0, 0x0

    return-object v0

    .line 99
    :cond_c
    invoke-virtual {p0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.android.vending.INSTALL_REFERRER"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_22

    .line 100
    const-string v0, "Analytics - Ignoring referrer due to the intent\'s action not being handled by analytics"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logDebugFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 101
    const/4 v0, 0x0

    return-object v0

    .line 106
    :cond_22
    :try_start_22
    invoke-virtual {p0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    .line 108
    .local v2, "extras":Landroid/os/Bundle;
    if-eqz v2, :cond_2c

    .line 109
    const/4 v0, 0x0

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z
    :try_end_2c
    .catch Ljava/lang/Exception; {:try_start_22 .. :try_end_2c} :catch_2d

    .line 114
    .end local v2    # "extras":Landroid/os/Bundle;
    :cond_2c
    goto :goto_30

    .line 112
    :catch_2d
    move-exception v2

    .line 113
    .local v2, "e":Ljava/lang/Exception;
    const/4 v0, 0x0

    return-object v0

    .line 117
    .end local v2    # "e":Ljava/lang/Exception;
    :goto_30
    const-string v0, "referrer"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 119
    .local v2, "referrerURL":Ljava/lang/String;
    if-eqz v2, :cond_3f

    const-string v0, "UTF-8"

    :try_start_3a
    invoke-static {v2, v0}, Ljava/net/URLDecoder;->decode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_3d
    .catch Ljava/lang/Exception; {:try_start_3a .. :try_end_3d} :catch_41

    move-result-object v0

    goto :goto_40

    :cond_3f
    const/4 v0, 0x0

    :goto_40
    return-object v0

    .line 120
    :catch_41
    move-exception v3

    .line 121
    .local v3, "e":Ljava/lang/Exception;
    return-object v2
.end method

.method protected static handleGooglePlayAcquisition(Ljava/util/HashMap;)V
    .registers 9
    .param p0, "referrerFields"    # Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;)V"
        }
    .end annotation

    .line 140
    const/4 v4, 0x0

    .line 143
    .local v4, "referrerInfoExists":Z
    :try_start_1
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "ADMS_Referrer_ContextData_Json_String"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z
    :try_end_a
    .catch Lcom/adobe/mobile/StaticMethods$NullContextException; {:try_start_1 .. :try_end_a} :catch_d

    move-result v0

    move v4, v0

    .line 146
    goto :goto_16

    .line 144
    :catch_d
    move-exception v5

    .line 145
    .local v5, "e":Lcom/adobe/mobile/StaticMethods$NullContextException;
    const-string v0, "Analytics - Error reading referrer info from preferences (%s)"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logDebugFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 148
    .end local v5    # "e":Lcom/adobe/mobile/StaticMethods$NullContextException;
    :goto_16
    if-eqz v4, :cond_19

    .line 149
    return-void

    .line 152
    :cond_19
    invoke-static {p0}, Lcom/adobe/mobile/ReferrerHandler;->appendNamespaceToGoogleReferrerFields(Ljava/util/HashMap;)Ljava/util/HashMap;

    move-result-object v5

    .line 155
    .local v5, "referrerLifecycleContextData":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    sget-boolean v0, Lcom/adobe/mobile/Lifecycle;->lifecycleHasRun:Z

    if-eqz v0, :cond_4d

    .line 156
    const-string v0, "a.referrer.campaign.source"

    invoke-virtual {v5, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4d

    const-string v0, "a.referrer.campaign.name"

    .line 157
    invoke-virtual {v5, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4d

    .line 160
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getAnalyticsExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    new-instance v1, Lcom/adobe/mobile/ReferrerHandler$2;

    invoke-direct {v1, v5}, Lcom/adobe/mobile/ReferrerHandler$2;-><init>(Ljava/util/HashMap;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 166
    invoke-static {}, Lcom/adobe/mobile/MobileConfig;->getInstance()Lcom/adobe/mobile/MobileConfig;

    move-result-object v0

    sget-object v1, Lcom/adobe/mobile/Config$MobileDataEvent;->MOBILE_EVENT_ACQUISITION_INSTALL:Lcom/adobe/mobile/Config$MobileDataEvent;

    invoke-virtual {v0, v1, v5}, Lcom/adobe/mobile/MobileConfig;->invokeAdobeDataCallback(Lcom/adobe/mobile/Config$MobileDataEvent;Ljava/util/Map;)V

    .line 170
    invoke-static {}, Lcom/adobe/mobile/AnalyticsWorker;->sharedInstance()Lcom/adobe/mobile/AnalyticsWorker;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/adobe/mobile/AnalyticsWorker;->kickWithReferrerData(Ljava/util/Map;)V

    .line 176
    :cond_4d
    :try_start_4d
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getSharedPreferencesEditor()Landroid/content/SharedPreferences$Editor;

    move-result-object v6

    .line 178
    .local v6, "editor":Landroid/content/SharedPreferences$Editor;
    new-instance v7, Lorg/json/JSONObject;

    invoke-direct {v7}, Lorg/json/JSONObject;-><init>()V

    .line 179
    .local v7, "googleReferrerObject":Lorg/json/JSONObject;
    const-string v0, "googleReferrerData"

    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, v5}, Lorg/json/JSONObject;-><init>(Ljava/util/Map;)V

    invoke-virtual {v7, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 180
    const-string v0, "ADMS_Referrer_ContextData_Json_String"

    invoke-virtual {v7}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v6, v0, v1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 181
    invoke-interface {v6}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_6c
    .catch Lcom/adobe/mobile/StaticMethods$NullContextException; {:try_start_4d .. :try_end_6c} :catch_6d
    .catch Lorg/json/JSONException; {:try_start_4d .. :try_end_6c} :catch_7e

    .line 186
    .end local v6    # "editor":Landroid/content/SharedPreferences$Editor;
    .end local v7    # "googleReferrerObject":Lorg/json/JSONObject;
    goto :goto_8e

    .line 182
    :catch_6d
    move-exception v6

    .line 183
    .local v6, "e":Lcom/adobe/mobile/StaticMethods$NullContextException;
    const-string v0, "Analytics - Error persisting referrer data (%s)"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v6}, Lcom/adobe/mobile/StaticMethods$NullContextException;->getMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logErrorFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 186
    .end local v6    # "e":Lcom/adobe/mobile/StaticMethods$NullContextException;
    goto :goto_8e

    .line 184
    :catch_7e
    move-exception v6

    .line 185
    .local v6, "e":Lorg/json/JSONException;
    const-string v0, "Analytics - Error persisting referrer data (%s)"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v6}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logErrorFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 189
    .end local v6    # "e":Lorg/json/JSONException;
    :goto_8e
    const/4 v0, 0x1

    sput-boolean v0, Lcom/adobe/mobile/ReferrerHandler;->_referrerProcessed:Z

    .line 190
    return-void
.end method

.method protected static handleV3Acquisition(Ljava/util/HashMap;)V
    .registers 11
    .param p0, "referrerFields"    # Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;)V"
        }
    .end annotation

    .line 344
    const-string v0, "utm_content"

    invoke-virtual {p0, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Ljava/lang/String;

    .line 345
    .local v4, "ugid":Ljava/lang/String;
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getAdvertisingIdentifier()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Lcom/adobe/mobile/ReferrerHandler;->getReferrerDataFromV3Server(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 346
    .local v5, "jsonResponse":Ljava/lang/String;
    invoke-static {v5}, Lcom/adobe/mobile/ReferrerHandler;->processReferrerDataFromV3Server(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 350
    .local v6, "processedJsonResponse":Ljava/lang/String;
    sget-boolean v0, Lcom/adobe/mobile/Lifecycle;->lifecycleHasRun:Z

    if-eqz v0, :cond_5d

    .line 352
    sget-boolean v0, Lcom/adobe/mobile/ReferrerHandler;->_referrerProcessed:Z

    if-eqz v0, :cond_26

    .line 353
    const-string v0, "Analytics - Acquisition referrer timed out"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logDebugFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 354
    return-void

    .line 357
    :cond_26
    new-instance v7, Ljava/util/HashMap;

    invoke-direct {v7}, Ljava/util/HashMap;-><init>()V

    .line 359
    .local v7, "referrerData":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    invoke-static {v6}, Lcom/adobe/mobile/ReferrerHandler;->processV3ResponseAndReturnContextData(Ljava/lang/String;)Ljava/util/HashMap;

    move-result-object v8

    .line 360
    .local v8, "contextData":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    invoke-virtual {v7, v8}, Ljava/util/HashMap;->putAll(Ljava/util/Map;)V

    .line 362
    if-eqz v8, :cond_41

    invoke-virtual {v8}, Ljava/util/HashMap;->size()I

    move-result v0

    if-lez v0, :cond_41

    .line 363
    invoke-static {v6}, Lcom/adobe/mobile/ReferrerHandler;->processV3ResponseAndReturnAdobeData(Ljava/lang/String;)Ljava/util/HashMap;

    move-result-object v9

    .line 364
    .local v9, "adobeData":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    invoke-virtual {v7, v9}, Ljava/util/HashMap;->putAll(Ljava/util/Map;)V

    .line 367
    .end local v9    # "adobeData":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    .end local v9
    :cond_41
    invoke-static {}, Lcom/adobe/mobile/MobileConfig;->getInstance()Lcom/adobe/mobile/MobileConfig;

    move-result-object v0

    sget-object v1, Lcom/adobe/mobile/Config$MobileDataEvent;->MOBILE_EVENT_ACQUISITION_INSTALL:Lcom/adobe/mobile/Config$MobileDataEvent;

    invoke-virtual {v0, v1, v7}, Lcom/adobe/mobile/MobileConfig;->invokeAdobeDataCallback(Lcom/adobe/mobile/Config$MobileDataEvent;Ljava/util/Map;)V

    .line 370
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getAnalyticsExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    new-instance v1, Lcom/adobe/mobile/ReferrerHandler$3;

    invoke-direct {v1, v7}, Lcom/adobe/mobile/ReferrerHandler$3;-><init>(Ljava/util/HashMap;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 377
    invoke-static {}, Lcom/adobe/mobile/AnalyticsWorker;->sharedInstance()Lcom/adobe/mobile/AnalyticsWorker;

    move-result-object v0

    invoke-virtual {v0, v7}, Lcom/adobe/mobile/AnalyticsWorker;->kickWithReferrerData(Ljava/util/Map;)V

    .line 381
    .end local v7    # "referrerData":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    .end local v7
    .end local v8    # "contextData":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    .end local v8
    :cond_5d
    :try_start_5d
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getSharedPreferencesEditor()Landroid/content/SharedPreferences$Editor;

    move-result-object v7

    .line 383
    .local v7, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v0, "ADMS_Referrer_ContextData_Json_String"

    invoke-interface {v7, v0, v6}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 384
    invoke-interface {v7}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_69
    .catch Lcom/adobe/mobile/StaticMethods$NullContextException; {:try_start_5d .. :try_end_69} :catch_6a

    .line 387
    .end local v7    # "editor":Landroid/content/SharedPreferences$Editor;
    goto :goto_7a

    .line 385
    :catch_6a
    move-exception v7

    .line 386
    .local v7, "e":Lcom/adobe/mobile/StaticMethods$NullContextException;
    const-string v0, "Analytics - Error persisting referrer data (%s)"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v7}, Lcom/adobe/mobile/StaticMethods$NullContextException;->getMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logErrorFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 390
    .end local v7    # "e":Lcom/adobe/mobile/StaticMethods$NullContextException;
    :goto_7a
    const/4 v0, 0x1

    sput-boolean v0, Lcom/adobe/mobile/ReferrerHandler;->_referrerProcessed:Z

    .line 391
    return-void
.end method

.method protected static isBlackListed(Ljava/lang/String;)Z
    .registers 2
    .param p0, "namespacedKey"    # Ljava/lang/String;

    .line 90
    sget-object v0, Lcom/adobe/mobile/ReferrerHandler;->ADOBE_DATA_BLACKLISTED_NAMESPACED_KEYS_SET:Ljava/util/HashSet;

    invoke-virtual {v0, p0}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method protected static isV3Response(Ljava/util/HashMap;)Z
    .registers 3
    .param p0, "referrerFields"    # Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;)Z"
        }
    .end annotation

    .line 394
    const-string v0, "adb_acq_v3"

    const-string v1, "utm_source"

    invoke-virtual {p0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1e

    const-string v0, "adb_acq_v3"

    const-string v1, "utm_campaign"

    .line 395
    invoke-virtual {p0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1e

    const/4 v0, 0x1

    goto :goto_1f

    :cond_1e
    const/4 v0, 0x0

    :goto_1f
    return v0
.end method

.method protected static parseGoogleReferrerFields(Ljava/lang/String;)Ljava/util/HashMap;
    .registers 7
    .param p0, "json"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/lang/String;)Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
        }
    .end annotation

    .line 246
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    .line 248
    .local v4, "referrerData":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    if-nez p0, :cond_8

    .line 249
    return-object v4

    .line 252
    :cond_8
    :try_start_8
    new-instance v5, Lorg/json/JSONObject;

    invoke-direct {v5, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 253
    .local v5, "jsonObject":Lorg/json/JSONObject;
    const-string v0, "googleReferrerData"

    invoke-static {v5, v0}, Lcom/adobe/mobile/ReferrerHandler;->getDataFromJSON(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/util/HashMap;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/util/HashMap;->putAll(Ljava/util/Map;)V
    :try_end_16
    .catch Lorg/json/JSONException; {:try_start_8 .. :try_end_16} :catch_17

    .line 257
    .end local v5    # "jsonObject":Lorg/json/JSONObject;
    goto :goto_2a

    .line 254
    :catch_17
    move-exception v5

    .line 255
    .local v5, "e":Lorg/json/JSONException;
    const-string v0, "Could not retrieve Google referrer data (%s)"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v5}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logDebugFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 256
    invoke-virtual {v4}, Ljava/util/HashMap;->clear()V

    .line 259
    .end local v5    # "e":Lorg/json/JSONException;
    :goto_2a
    return-object v4
.end method

.method protected static parseReferrerURLToMap(Ljava/lang/String;)Ljava/util/HashMap;
    .registers 9
    .param p0, "referrerURL"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/lang/String;)Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
        }
    .end annotation

    .line 126
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 128
    .local v2, "referrerFields":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    const-string v0, "&"

    invoke-virtual {p0, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    array-length v4, v3

    const/4 v5, 0x0

    :goto_d
    if-ge v5, v4, :cond_27

    aget-object v6, v3, v5

    .line 129
    .local v6, "item":Ljava/lang/String;
    const-string v0, "="

    invoke-virtual {v6, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    .line 131
    .local v7, "tokens":[Ljava/lang/String;
    array-length v0, v7

    const/4 v1, 0x2

    if-ne v0, v1, :cond_24

    .line 132
    const/4 v0, 0x0

    aget-object v0, v7, v0

    const/4 v1, 0x1

    aget-object v1, v7, v1

    invoke-virtual {v2, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 128
    .end local v6    # "item":Ljava/lang/String;
    .end local v7    # "tokens":[Ljava/lang/String;
    :cond_24
    add-int/lit8 v5, v5, 0x1

    goto :goto_d

    .line 135
    :cond_27
    return-object v2
.end method

.method protected static parseV3ContextDataFromResponse(Ljava/lang/String;)Ljava/util/HashMap;
    .registers 3
    .param p0, "response"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/lang/String;)Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
        }
    .end annotation

    .line 423
    invoke-static {p0}, Lcom/adobe/mobile/ReferrerHandler;->translateV3StringResponseToJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    .line 424
    .local v1, "responseJSON":Lorg/json/JSONObject;
    const-string v0, "contextData"

    invoke-static {v1, v0}, Lcom/adobe/mobile/ReferrerHandler;->getDataFromJSON(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/util/HashMap;

    move-result-object v0

    return-object v0
.end method

.method public static processIntent(Landroid/content/Intent;)V
    .registers 6
    .param p0, "intent"    # Landroid/content/Intent;

    .line 71
    invoke-static {p0}, Lcom/adobe/mobile/ReferrerHandler;->getReferrerURLFromIntent(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v3

    .line 73
    .local v3, "referrerURL":Ljava/lang/String;
    if-eqz v3, :cond_c

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_15

    .line 74
    :cond_c
    const-string v0, "Analytics - Ignoring referrer due to the intent\'s referrer string being empty"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logDebugFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 75
    return-void

    .line 78
    :cond_15
    const-string v0, "Analytics - Received referrer information(%s)"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logDebugFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 80
    invoke-static {v3}, Lcom/adobe/mobile/ReferrerHandler;->parseReferrerURLToMap(Ljava/lang/String;)Ljava/util/HashMap;

    move-result-object v4

    .line 82
    .local v4, "referrerFields":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    invoke-static {v4}, Lcom/adobe/mobile/ReferrerHandler;->isV3Response(Ljava/util/HashMap;)Z

    move-result v0

    if-eqz v0, :cond_2e

    .line 83
    invoke-static {v4}, Lcom/adobe/mobile/ReferrerHandler;->handleV3Acquisition(Ljava/util/HashMap;)V

    goto :goto_31

    .line 85
    :cond_2e
    invoke-static {v4}, Lcom/adobe/mobile/ReferrerHandler;->handleGooglePlayAcquisition(Ljava/util/HashMap;)V

    .line 87
    :goto_31
    return-void
.end method

.method protected static processReferrerDataFromV3Server(Ljava/lang/String;)Ljava/lang/String;
    .registers 13
    .param p0, "jsonResponse"    # Ljava/lang/String;

    .line 272
    if-nez p0, :cond_4

    .line 273
    const/4 v0, 0x0

    return-object v0

    .line 275
    :cond_4
    move-object v4, p0

    .line 278
    .local v4, "processedJson":Ljava/lang/String;
    :try_start_5
    new-instance v5, Lorg/json/JSONObject;

    invoke-direct {v5, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 279
    .local v5, "jsonObject":Lorg/json/JSONObject;
    const-string v0, "adobeData"

    invoke-static {v5, v0}, Lcom/adobe/mobile/ReferrerHandler;->getDataFromJSON(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/util/HashMap;

    move-result-object v6

    .line 280
    .local v6, "adobeData":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    new-instance v7, Ljava/util/HashMap;

    invoke-direct {v7}, Ljava/util/HashMap;-><init>()V

    .line 282
    .local v7, "processedAdobeData":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    if-eqz v6, :cond_70

    invoke-virtual {v6}, Ljava/util/HashMap;->size()I

    move-result v0

    if-lez v0, :cond_70

    .line 284
    invoke-virtual {v6}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v8

    .line 285
    .local v8, "entrySet":Ljava/util/Set;, "Ljava/util/Set<Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Object;>;>;"
    invoke-interface {v8}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_25
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5c

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Ljava/util/Map$Entry;

    .line 286
    .local v10, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Object;>;"
    invoke-interface {v10}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    move-object v11, v0

    check-cast v11, Ljava/lang/String;

    .line 287
    .local v11, "key":Ljava/lang/String;
    const-string v0, "a.acquisition.custom."

    invoke-virtual {v11, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_54

    .line 288
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "a.acquisition.custom."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    .line 290
    :cond_54
    invoke-interface {v10}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v7, v11, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 291
    .end local v10    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Object;>;"
    .end local v10
    .end local v11    # "key":Ljava/lang/String;
    goto :goto_25

    .line 293
    :cond_5c
    const-string v0, "adobeData"

    invoke-virtual {v5, v0}, Lorg/json/JSONObject;->remove(Ljava/lang/String;)Ljava/lang/Object;

    .line 294
    const-string v0, "adobeData"

    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, v7}, Lorg/json/JSONObject;-><init>(Ljava/util/Map;)V

    invoke-virtual {v5, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 296
    invoke-virtual {v5}, Lorg/json/JSONObject;->toString()Ljava/lang/String;
    :try_end_6e
    .catch Lorg/json/JSONException; {:try_start_5 .. :try_end_6e} :catch_71

    move-result-object v0

    move-object v4, v0

    .line 301
    .end local v5    # "jsonObject":Lorg/json/JSONObject;
    .end local v6    # "adobeData":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    .end local v6
    .end local v7    # "processedAdobeData":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    .end local v7
    .end local v8    # "entrySet":Ljava/util/Set;, "Ljava/util/Set<Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Object;>;>;"
    .end local v8
    :cond_70
    goto :goto_81

    .line 299
    :catch_71
    move-exception v5

    .line 300
    .local v5, "e":Lorg/json/JSONException;
    const-string v0, "Could not parse adobeData from the response (%s)"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v5}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logDebugFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 302
    .end local v5    # "e":Lorg/json/JSONException;
    :goto_81
    return-object v4
.end method

.method protected static processV3ResponseAndReturnAdobeData(Ljava/lang/String;)Ljava/util/HashMap;
    .registers 8
    .param p0, "jsonResponse"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/lang/String;)Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
        }
    .end annotation

    .line 306
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    .line 308
    .local v4, "adobeData":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    :try_start_5
    new-instance v5, Lorg/json/JSONObject;

    invoke-direct {v5, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 309
    .local v5, "adobeDataObject":Lorg/json/JSONObject;
    const-string v0, "adobeData"

    invoke-static {v5, v0}, Lcom/adobe/mobile/ReferrerHandler;->getDataFromJSON(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/util/HashMap;

    move-result-object v6

    .line 310
    .local v6, "unfilteredAdobeData":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    if-eqz v6, :cond_1f

    invoke-virtual {v6}, Ljava/util/HashMap;->size()I

    move-result v0

    if-lez v0, :cond_1f

    .line 311
    invoke-static {v6}, Lcom/adobe/mobile/ReferrerHandler;->filterForBlacklistedKeys(Ljava/util/HashMap;)Ljava/util/HashMap;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/util/HashMap;->putAll(Ljava/util/Map;)V
    :try_end_1f
    .catch Lorg/json/JSONException; {:try_start_5 .. :try_end_1f} :catch_20

    .line 315
    .end local v5    # "adobeDataObject":Lorg/json/JSONObject;
    .end local v6    # "unfilteredAdobeData":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    .end local v6
    :cond_1f
    goto :goto_30

    .line 313
    :catch_20
    move-exception v5

    .line 314
    .local v5, "e":Lorg/json/JSONException;
    const-string v0, "Could not parse adobeData from the response (%s)"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v5}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logDebugFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 317
    .end local v5    # "e":Lorg/json/JSONException;
    :goto_30
    return-object v4
.end method

.method protected static processV3ResponseAndReturnContextData(Ljava/lang/String;)Ljava/util/HashMap;
    .registers 10
    .param p0, "response"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/lang/String;)Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
        }
    .end annotation

    .line 400
    invoke-static {p0}, Lcom/adobe/mobile/ReferrerHandler;->translateV3StringResponseToJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v4

    .line 401
    .local v4, "responseJSON":Lorg/json/JSONObject;
    invoke-static {p0}, Lcom/adobe/mobile/ReferrerHandler;->parseV3ContextDataFromResponse(Ljava/lang/String;)Ljava/util/HashMap;

    move-result-object v5

    .line 403
    .local v5, "contextData":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    invoke-static {v4}, Lcom/adobe/mobile/ReferrerHandler;->getDeepLinkFromJSON(Lorg/json/JSONObject;)Ljava/lang/String;

    move-result-object v6

    .line 404
    .local v6, "deepLink":Ljava/lang/String;
    if-nez v6, :cond_f

    .line 405
    return-object v5

    .line 409
    :cond_f
    :try_start_f
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getCurrentActivity()Landroid/app/Activity;

    move-result-object v7

    .line 410
    .local v7, "currentActivity":Landroid/app/Activity;
    new-instance v8, Landroid/content/Intent;

    const-string v0, "android.intent.action.VIEW"

    invoke-direct {v8, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 411
    .local v8, "intent":Landroid/content/Intent;
    invoke-static {v6}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v8, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 412
    invoke-virtual {v7, v8}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V
    :try_end_24
    .catch Lcom/adobe/mobile/StaticMethods$NullActivityException; {:try_start_f .. :try_end_24} :catch_25
    .catch Ljava/lang/Exception; {:try_start_f .. :try_end_24} :catch_31

    .line 417
    .end local v7    # "currentActivity":Landroid/app/Activity;
    .end local v8    # "intent":Landroid/content/Intent;
    goto :goto_41

    .line 413
    :catch_25
    move-exception v7

    .line 414
    .local v7, "ex":Lcom/adobe/mobile/StaticMethods$NullActivityException;
    invoke-virtual {v7}, Lcom/adobe/mobile/StaticMethods$NullActivityException;->getMessage()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logWarningFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 417
    .end local v7    # "ex":Lcom/adobe/mobile/StaticMethods$NullActivityException;
    goto :goto_41

    .line 415
    :catch_31
    move-exception v7

    .line 416
    .local v7, "ex":Ljava/lang/Exception;
    const-string v0, "Acquisition - Could not load deep link intent for Acquisition (%s)"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v7}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logWarningFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 419
    .end local v7    # "ex":Ljava/lang/Exception;
    :goto_41
    return-object v5
.end method

.method protected static setReferrerProcessed(Z)V
    .registers 1
    .param p0, "processed"    # Z

    .line 52
    sput-boolean p0, Lcom/adobe/mobile/ReferrerHandler;->_referrerProcessed:Z

    .line 53
    return-void
.end method

.method protected static translateV3StringResponseToJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;
    .registers 8
    .param p0, "response"    # Ljava/lang/String;

    .line 428
    if-eqz p0, :cond_8

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_a

    .line 429
    :cond_8
    const/4 v0, 0x0

    return-object v0

    .line 435
    :cond_a
    :try_start_a
    new-instance v4, Lorg/json/JSONObject;

    invoke-direct {v4, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V
    :try_end_f
    .catch Lorg/json/JSONException; {:try_start_a .. :try_end_f} :catch_10

    .line 439
    .local v4, "responseObject":Lorg/json/JSONObject;
    goto :goto_22

    .line 436
    .end local v4    # "responseObject":Lorg/json/JSONObject;
    :catch_10
    move-exception v5

    .line 437
    .local v5, "ex":Lorg/json/JSONException;
    const-string v0, "Analytics - Unable to parse response(%s)"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v5}, Lorg/json/JSONException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logDebugFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 438
    const/4 v0, 0x0

    return-object v0

    .line 443
    .local v4, "responseObject":Lorg/json/JSONObject;
    .end local v5    # "ex":Lorg/json/JSONException;
    :goto_22
    const-string v0, "contextData"

    :try_start_24
    invoke-virtual {v4, v0}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;
    :try_end_27
    .catch Lorg/json/JSONException; {:try_start_24 .. :try_end_27} :catch_29

    move-result-object v5

    .line 447
    .local v5, "contextData":Lorg/json/JSONObject;
    goto :goto_34

    .line 444
    .end local v5    # "contextData":Lorg/json/JSONObject;
    :catch_29
    move-exception v6

    .line 445
    .local v6, "ex":Lorg/json/JSONException;
    const-string v0, "Analytics - Unable to parse acquisition service response (no contextData parameter in response)"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logDebugFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 446
    const/4 v0, 0x0

    return-object v0

    .line 449
    .local v5, "contextData":Lorg/json/JSONObject;
    .end local v6    # "ex":Lorg/json/JSONException;
    :goto_34
    if-nez v5, :cond_40

    .line 450
    const-string v0, "Analytics - Unable to parse acquisition service response (no contextData parameter in response)"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logDebugFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 451
    const/4 v0, 0x0

    return-object v0

    .line 455
    :cond_40
    const-string v0, "a.referrer.campaign.name"

    invoke-virtual {v5, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_52

    .line 456
    const-string v0, "Analytics - Acquisition referrer data was not complete (no a.referrer.campaign.name in context data), ignoring"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logDebugFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 457
    const/4 v0, 0x0

    return-object v0

    .line 460
    :cond_52
    const-string v0, "Analytics - Received Referrer Data(%s)"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logDebugFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 461
    return-object v4
.end method

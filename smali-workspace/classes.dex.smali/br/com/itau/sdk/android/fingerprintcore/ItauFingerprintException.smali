.class public Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintException;
.super Ljava/lang/Exception;
.source "ItauFingerprintException.java"


# instance fields
.field private reason:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .registers 2
    .param p1, "message"    # Ljava/lang/String;

    .line 12
    invoke-direct {p0, p1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    .line 13
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .registers 3
    .param p1, "message"    # Ljava/lang/String;
    .param p2, "reason"    # Ljava/lang/String;

    .line 16
    invoke-direct {p0, p1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    .line 17
    iput-object p2, p0, Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintException;->reason:Ljava/lang/String;

    .line 18
    return-void
.end method

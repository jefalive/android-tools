.class public Lcom/itau/empresas/ui/util/menu/FeedbackCommand;
.super Ljava/lang/Object;
.source "FeedbackCommand.java"

# interfaces
.implements Lcom/itau/empresas/ui/util/menu/Command;


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public executar(Landroid/support/v7/app/AppCompatActivity;Lcom/itau/empresas/api/model/MenuVO;)V
    .registers 5
    .param p1, "activity"    # Landroid/support/v7/app/AppCompatActivity;
    .param p2, "menuVO"    # Lcom/itau/empresas/api/model/MenuVO;

    .line 12
    invoke-static {}, Lcom/itau/empresas/feature/feedback/FeedbackDialog_;->builder()Lcom/itau/empresas/feature/feedback/FeedbackDialog_$FragmentBuilder_;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/feedback/FeedbackDialog_$FragmentBuilder_;->feedbackEspontaneo(Ljava/lang/Boolean;)Lcom/itau/empresas/feature/feedback/FeedbackDialog_$FragmentBuilder_;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/feature/feedback/FeedbackDialog_$FragmentBuilder_;->build()Lcom/itau/empresas/feature/feedback/FeedbackDialog;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/itau/empresas/feature/feedback/FeedbackDialog;->show(Landroid/support/v7/app/AppCompatActivity;)Lcom/itau/empresas/ui/dialog/BaseDialog;

    .line 13
    return-void
.end method

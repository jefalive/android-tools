.class public final Lcom/itau/empresas/feature/chaveacesso/ChaveAcessoValidador;
.super Ljava/lang/Object;
.source "ChaveAcessoValidador.java"


# static fields
.field private static final INTERNAL_ARRAY:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .line 15
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "DESABILITADO"

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const-string v1, "CADASTRAR"

    const/4 v2, 0x1

    aput-object v1, v0, v2

    const-string v1, "HABILITADO"

    const/4 v2, 0x2

    aput-object v1, v0, v2

    const-string v1, "HABILITADO_OUTRO_DISPOSITIVO"

    const/4 v2, 0x3

    aput-object v1, v0, v2

    sput-object v0, Lcom/itau/empresas/feature/chaveacesso/ChaveAcessoValidador;->INTERNAL_ARRAY:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .registers 1

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static chaveAcessoLiberadoParaLogin(I)Z
    .registers 2
    .param p0, "codigoChaveAcesso"    # I

    .line 36
    invoke-static {p0}, Lcom/itau/empresas/feature/chaveacesso/ChaveAcessoValidador;->chaveDeAcessoLiberada(I)Z

    move-result v0

    if-eqz v0, :cond_b

    const/4 v0, 0x1

    if-eq p0, v0, :cond_b

    const/4 v0, 0x1

    goto :goto_c

    :cond_b
    const/4 v0, 0x0

    :goto_c
    return v0
.end method

.method public static chaveDeAcessoLiberada(I)Z
    .registers 2
    .param p0, "codigoChaveAcesso"    # I

    .line 26
    if-eqz p0, :cond_a

    invoke-static {p0}, Lcom/itau/empresas/feature/chaveacesso/ChaveAcessoValidador;->codigoEstaEntreRange(I)Z

    move-result v0

    if-eqz v0, :cond_a

    const/4 v0, 0x1

    goto :goto_b

    :cond_a
    const/4 v0, 0x0

    :goto_b
    return v0
.end method

.method private static codigoEstaEntreRange(I)Z
    .registers 2
    .param p0, "codigoChaveAcesso"    # I

    .line 62
    if-lez p0, :cond_7

    const/4 v0, 0x3

    if-gt p0, v0, :cond_7

    const/4 v0, 0x1

    goto :goto_8

    :cond_7
    const/4 v0, 0x0

    :goto_8
    return v0
.end method

.method public static parserChaveAcesso(Ljava/lang/String;)I
    .registers 3
    .param p0, "chaveAcesso"    # Ljava/lang/String;

    .line 54
    if-nez p0, :cond_4

    .line 55
    const/4 v0, 0x0

    return v0

    .line 57
    :cond_4
    sget-object v0, Lcom/itau/empresas/feature/chaveacesso/ChaveAcessoValidador;->INTERNAL_ARRAY:[Ljava/lang/String;

    invoke-static {v0, p0}, Ljava/util/Arrays;->binarySearch([Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v1

    .line 58
    .local v1, "index":I
    if-ltz v1, :cond_e

    move v0, v1

    goto :goto_f

    :cond_e
    const/4 v0, 0x0

    :goto_f
    return v0
.end method

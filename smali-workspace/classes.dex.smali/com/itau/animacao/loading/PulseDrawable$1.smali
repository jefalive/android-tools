.class Lcom/itau/animacao/loading/PulseDrawable$1;
.super Ljava/lang/Object;
.source "PulseDrawable.java"

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/itau/animacao/loading/PulseDrawable;->prepareAnimation()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/itau/animacao/loading/PulseDrawable;


# direct methods
.method constructor <init>(Lcom/itau/animacao/loading/PulseDrawable;)V
    .registers 2
    .param p1, "this$0"    # Lcom/itau/animacao/loading/PulseDrawable;

    .line 45
    iput-object p1, p0, Lcom/itau/animacao/loading/PulseDrawable$1;->this$0:Lcom/itau/animacao/loading/PulseDrawable;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .registers 4
    .param p1, "animation"    # Landroid/animation/ValueAnimator;

    .line 48
    iget-object v0, p0, Lcom/itau/animacao/loading/PulseDrawable$1;->this$0:Lcom/itau/animacao/loading/PulseDrawable;

    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    # setter for: Lcom/itau/animacao/loading/PulseDrawable;->currentExpandAnimationValue:F
    invoke-static {v0, v1}, Lcom/itau/animacao/loading/PulseDrawable;->access$002(Lcom/itau/animacao/loading/PulseDrawable;F)F

    .line 49
    iget-object v0, p0, Lcom/itau/animacao/loading/PulseDrawable$1;->this$0:Lcom/itau/animacao/loading/PulseDrawable;

    # getter for: Lcom/itau/animacao/loading/PulseDrawable;->currentExpandAnimationValue:F
    invoke-static {v0}, Lcom/itau/animacao/loading/PulseDrawable;->access$000(Lcom/itau/animacao/loading/PulseDrawable;)F

    move-result v0

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-nez v0, :cond_21

    .line 50
    iget-object v0, p0, Lcom/itau/animacao/loading/PulseDrawable$1;->this$0:Lcom/itau/animacao/loading/PulseDrawable;

    const/16 v1, 0xff

    # setter for: Lcom/itau/animacao/loading/PulseDrawable;->currentAlphaAnimationValue:I
    invoke-static {v0, v1}, Lcom/itau/animacao/loading/PulseDrawable;->access$102(Lcom/itau/animacao/loading/PulseDrawable;I)I

    .line 52
    :cond_21
    iget-object v0, p0, Lcom/itau/animacao/loading/PulseDrawable$1;->this$0:Lcom/itau/animacao/loading/PulseDrawable;

    invoke-virtual {v0}, Lcom/itau/animacao/loading/PulseDrawable;->invalidateSelf()V

    .line 53
    return-void
.end method

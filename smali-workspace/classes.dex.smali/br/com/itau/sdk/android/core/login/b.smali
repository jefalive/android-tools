.class public final Lbr/com/itau/sdk/android/core/login/b;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lbr/com/itau/sdk/android/core/login/b$a;
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .registers 1

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    return-void
.end method

.method synthetic constructor <init>(Lbr/com/itau/sdk/android/core/login/e;)V
    .registers 2

    .line 13
    invoke-direct {p0}, Lbr/com/itau/sdk/android/core/login/b;-><init>()V

    return-void
.end method

.method public static a()Lbr/com/itau/sdk/android/core/login/b;
    .registers 1

    .line 18
    invoke-static {}, Lbr/com/itau/sdk/android/core/login/b$a;->a()Lbr/com/itau/sdk/android/core/login/b;

    move-result-object v0

    return-object v0
.end method

.method private synthetic a(Lbr/com/itau/sdk/android/core/login/EletronicPassword;Ljava/lang/String;)V
    .registers 4

    .line 42
    invoke-static {p0, p2, p1}, Lbr/com/itau/sdk/android/core/login/d;->a(Lbr/com/itau/sdk/android/core/login/b;Ljava/lang/String;Lbr/com/itau/sdk/android/core/login/EletronicPassword;)Lbr/com/itau/sdk/android/core/type/BackendCall;

    move-result-object v0

    invoke-static {v0}, Lbr/com/itau/sdk/android/core/BackendClient;->callApi(Lbr/com/itau/sdk/android/core/type/BackendCall;)V

    return-void
.end method

.method static synthetic a(Lbr/com/itau/sdk/android/core/login/b;Lbr/com/itau/sdk/android/core/login/EletronicPassword;Ljava/lang/String;)V
    .registers 3

    invoke-direct {p0, p1, p2}, Lbr/com/itau/sdk/android/core/login/b;->a(Lbr/com/itau/sdk/android/core/login/EletronicPassword;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lbr/com/itau/sdk/android/core/login/b;Ljava/lang/String;Lbr/com/itau/sdk/android/core/login/EletronicPassword;)V
    .registers 3

    invoke-direct {p0, p1, p2}, Lbr/com/itau/sdk/android/core/login/b;->a(Ljava/lang/String;Lbr/com/itau/sdk/android/core/login/EletronicPassword;)V

    return-void
.end method

.method private a(Lbr/com/itau/sdk/android/core/login/model/RouterResponseKeyAccessDTO;Lbr/com/itau/sdk/android/core/login/EletronicPassword;)V
    .registers 3

    .line 49
    invoke-direct {p0, p2}, Lbr/com/itau/sdk/android/core/login/b;->a(Ljava/lang/Object;)V

    .line 50
    invoke-direct {p0, p1}, Lbr/com/itau/sdk/android/core/login/b;->a(Ljava/lang/Object;)V

    .line 51
    return-void
.end method

.method private a(Ljava/lang/Object;)V
    .registers 3

    .line 67
    invoke-static {}, Lde/greenrobot/event/EventBus;->getDefault()Lde/greenrobot/event/EventBus;

    move-result-object v0

    invoke-virtual {v0, p1}, Lde/greenrobot/event/EventBus;->post(Ljava/lang/Object;)V

    .line 68
    return-void
.end method

.method private synthetic a(Ljava/lang/String;Lbr/com/itau/sdk/android/core/login/EletronicPassword;)V
    .registers 5

    .line 42
    .line 43
    invoke-direct {p0}, Lbr/com/itau/sdk/android/core/login/b;->f()Lbr/com/itau/sdk/android/core/login/a;

    move-result-object v0

    new-instance v1, Lbr/com/itau/sdk/android/core/login/model/b;

    invoke-direct {v1, p1}, Lbr/com/itau/sdk/android/core/login/model/b;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v1}, Lbr/com/itau/sdk/android/core/login/a;->a(Lbr/com/itau/sdk/android/core/login/model/b;)Lbr/com/itau/sdk/android/core/login/model/RouterResponseKeyAccessDTO;

    move-result-object v0

    .line 42
    invoke-direct {p0, v0, p2}, Lbr/com/itau/sdk/android/core/login/b;->a(Lbr/com/itau/sdk/android/core/login/model/RouterResponseKeyAccessDTO;Lbr/com/itau/sdk/android/core/login/EletronicPassword;)V

    return-void
.end method

.method private e()V
    .registers 3

    .line 39
    invoke-static {}, Lbr/com/itau/sdk/android/core/BackendClient;->getInstance()Lbr/com/itau/sdk/android/core/BackendClient;

    move-result-object v0

    invoke-virtual {v0}, Lbr/com/itau/sdk/android/core/BackendClient;->getSdkCustomUi()Lbr/com/itau/sdk/android/core/SDKCustomUi;

    move-result-object v0

    invoke-interface {v0}, Lbr/com/itau/sdk/android/core/SDKCustomUi;->getEletronicPasswordView()Lbr/com/itau/sdk/android/core/login/EletronicPassword;

    move-result-object v1

    .line 41
    invoke-static {p0, v1}, Lbr/com/itau/sdk/android/core/login/c;->a(Lbr/com/itau/sdk/android/core/login/b;Lbr/com/itau/sdk/android/core/login/EletronicPassword;)Lbr/com/itau/sdk/android/core/login/EletronicPassword$Callback;

    move-result-object v0

    invoke-interface {v1, v0}, Lbr/com/itau/sdk/android/core/login/EletronicPassword;->setCallback(Lbr/com/itau/sdk/android/core/login/EletronicPassword$Callback;)V

    .line 45
    invoke-interface {v1}, Lbr/com/itau/sdk/android/core/login/EletronicPassword;->show()V

    .line 46
    return-void
.end method

.method private f()Lbr/com/itau/sdk/android/core/login/a;
    .registers 2

    .line 63
    const-class v0, Lbr/com/itau/sdk/android/core/login/a;

    invoke-static {v0}, Lbr/com/itau/sdk/android/core/BackendClient;->api(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbr/com/itau/sdk/android/core/login/a;

    return-object v0
.end method


# virtual methods
.method public a(Ljava/lang/String;)Lcom/google/gson/JsonElement;
    .registers 5

    .line 22
    invoke-static {}, Lde/greenrobot/event/EventBus;->getDefault()Lde/greenrobot/event/EventBus;

    move-result-object v0

    invoke-virtual {v0, p0}, Lde/greenrobot/event/EventBus;->isRegistered(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_11

    .line 23
    invoke-static {}, Lde/greenrobot/event/EventBus;->getDefault()Lde/greenrobot/event/EventBus;

    move-result-object v0

    invoke-virtual {v0, p0}, Lde/greenrobot/event/EventBus;->register(Ljava/lang/Object;)V

    .line 25
    :cond_11
    invoke-direct {p0}, Lbr/com/itau/sdk/android/core/login/b;->f()Lbr/com/itau/sdk/android/core/login/a;

    move-result-object v0

    invoke-static {p1}, Lbr/com/itau/sdk/android/core/login/f;->a(Ljava/lang/String;)Lbr/com/itau/sdk/android/core/login/model/a;

    move-result-object v1

    invoke-interface {v0, v1}, Lbr/com/itau/sdk/android/core/login/a;->a(Lbr/com/itau/sdk/android/core/login/model/a;)Lcom/google/gson/JsonElement;

    move-result-object v2

    .line 27
    invoke-static {}, Lde/greenrobot/event/EventBus;->getDefault()Lde/greenrobot/event/EventBus;

    move-result-object v0

    invoke-virtual {v0, p0}, Lde/greenrobot/event/EventBus;->isRegistered(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2e

    .line 28
    invoke-static {}, Lde/greenrobot/event/EventBus;->getDefault()Lde/greenrobot/event/EventBus;

    move-result-object v0

    invoke-virtual {v0, p0}, Lde/greenrobot/event/EventBus;->unregister(Ljava/lang/Object;)V

    .line 30
    :cond_2e
    return-object v2
.end method

.method public b()V
    .registers 1

    .line 34
    invoke-direct {p0}, Lbr/com/itau/sdk/android/core/login/b;->e()V

    .line 35
    return-void
.end method

.method public b(Ljava/lang/String;)Z
    .registers 3

    .line 71
    invoke-static {p1}, Lbr/com/itau/sdk/android/core/login/f;->b(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public c()Lbr/com/itau/sdk/android/core/login/model/RouterResponseKeyAccessDTO;
    .registers 2

    .line 55
    invoke-direct {p0}, Lbr/com/itau/sdk/android/core/login/b;->f()Lbr/com/itau/sdk/android/core/login/a;

    move-result-object v0

    invoke-interface {v0}, Lbr/com/itau/sdk/android/core/login/a;->a()Lbr/com/itau/sdk/android/core/login/model/RouterResponseKeyAccessDTO;

    move-result-object v0

    return-object v0
.end method

.method public d()Lbr/com/itau/sdk/android/core/login/model/RouterResponseKeyAccessDTO;
    .registers 2

    .line 59
    invoke-direct {p0}, Lbr/com/itau/sdk/android/core/login/b;->f()Lbr/com/itau/sdk/android/core/login/a;

    move-result-object v0

    invoke-interface {v0}, Lbr/com/itau/sdk/android/core/login/a;->b()Lbr/com/itau/sdk/android/core/login/model/RouterResponseKeyAccessDTO;

    move-result-object v0

    return-object v0
.end method

.method public onEventBackgroundThread(Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;)V
    .registers 3

    .line 79
    invoke-static {}, Lde/greenrobot/event/EventBus;->getDefault()Lde/greenrobot/event/EventBus;

    move-result-object v0

    invoke-virtual {v0, p0}, Lde/greenrobot/event/EventBus;->isRegistered(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 80
    invoke-static {}, Lde/greenrobot/event/EventBus;->getDefault()Lde/greenrobot/event/EventBus;

    move-result-object v0

    invoke-virtual {v0, p0}, Lde/greenrobot/event/EventBus;->unregister(Ljava/lang/Object;)V

    .line 82
    :cond_11
    invoke-static {}, Lbr/com/itau/sdk/android/core/BackendClient;->getInstance()Lbr/com/itau/sdk/android/core/BackendClient;

    move-result-object v0

    invoke-virtual {v0}, Lbr/com/itau/sdk/android/core/BackendClient;->reset()V

    .line 84
    return-void
.end method

.class public Lbr/com/itau/sdk/android/core/type/SecurityAssertResponseDTO;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final $:[B

.field private static $$:I


# instance fields
.field private mensagem:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/Expose;
    .end annotation
.end field

.field private sucesso:Z
    .annotation runtime Lcom/google/gson/annotations/Expose;
    .end annotation
.end field

.field private tokenValido:Ljava/lang/Boolean;
    .annotation runtime Lcom/google/gson/annotations/Expose;
    .end annotation
.end field

.field private tokenValidoInformarNovoCodigo:Ljava/lang/Boolean;
    .annotation runtime Lcom/google/gson/annotations/Expose;
    .end annotation
.end field


# direct methods
.method private static $(III)Ljava/lang/String;
    .registers 9

    mul-int/lit8 p2, p2, 0x2

    rsub-int/lit8 p2, p2, 0x24

    rsub-int/lit8 p1, p1, 0x4f

    mul-int/lit8 p0, p0, 0x27

    rsub-int/lit8 p0, p0, 0x53

    const/4 v5, 0x0

    new-instance v0, Ljava/lang/String;

    sget-object v4, Lbr/com/itau/sdk/android/core/type/SecurityAssertResponseDTO;->$:[B

    new-array v1, p2, [B

    if-nez v4, :cond_1b

    move v2, p2

    move v3, p1

    :goto_15
    add-int/lit8 p1, p1, 0x1

    neg-int v3, v3

    add-int/2addr v2, v3

    add-int/lit8 p0, v2, 0xe

    :cond_1b
    move v2, v5

    add-int/lit8 v5, v5, 0x1

    int-to-byte v3, p0

    aput-byte v3, v1, v2

    if-ne v5, p2, :cond_28

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Ljava/lang/String;-><init>([BI)V

    return-object v0

    :cond_28
    move v2, p0

    aget-byte v3, v4, p1

    goto :goto_15
.end method

.method static constructor <clinit>()V
    .registers 3

    const/16 v0, 0x5c

    new-array v0, v0, [B

    fill-array-data v0, :array_e

    sput-object v0, Lbr/com/itau/sdk/android/core/type/SecurityAssertResponseDTO;->$:[B

    const/16 v0, 0xcf

    sput v0, Lbr/com/itau/sdk/android/core/type/SecurityAssertResponseDTO;->$$:I

    return-void

    :array_e
    .array-data 1
        0x33t
        0x1ct
        0x10t
        -0xct
        0x1at
        -0x45t
        0xct
        0x20t
        0xct
        0x0t
        0xet
        0x12t
        0x40t
        0x1at
        -0x46t
        0x13t
        0x12t
        0x14t
        0x5t
        0x26t
        0x3t
        0x3t
        0x11t
        0x13t
        0x3t
        0x34t
        -0x17t
        0x16t
        0x5t
        0xbt
        0x13t
        0x1at
        -0x3t
        0x32t
        -0x13t
        0x7t
        0x15t
        0x3at
        -0x1et
        0x19t
        0x9t
        0x10t
        0x6t
        0x40t
        -0x4t
        0x10t
        -0x4t
        0x11t
        0x17t
        0x3t
        0x9t
        0x46t
        -0x24t
        0xet
        0x1ct
        0x1t
        0xct
        0x30t
        -0x5t
        0x0t
        0x11t
        0xft
        0xft
        0x9t
        0x1ct
        0x2ft
        -0x2t
        0x13t
        -0x1et
        0x1ct
        0x16t
        0x5t
        0x9t
        0x20t
        0x8t
        0x10t
        0x6t
        0x3et
        0x24t
        0x1at
        -0x46t
        0x13t
        0x12t
        0x14t
        0x5t
        0x26t
        0x3t
        0x3t
        0x11t
        0x13t
        0x3t
        0x40t
    .end array-data
.end method

.method public constructor <init>()V
    .registers 1

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;ZZZ)V
    .registers 6

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Lbr/com/itau/sdk/android/core/type/SecurityAssertResponseDTO;->mensagem:Ljava/lang/String;

    .line 29
    iput-boolean p2, p0, Lbr/com/itau/sdk/android/core/type/SecurityAssertResponseDTO;->sucesso:Z

    .line 30
    invoke-static {p3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/sdk/android/core/type/SecurityAssertResponseDTO;->tokenValido:Ljava/lang/Boolean;

    .line 31
    invoke-static {p4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/sdk/android/core/type/SecurityAssertResponseDTO;->tokenValidoInformarNovoCodigo:Ljava/lang/Boolean;

    .line 32
    return-void
.end method


# virtual methods
.method public getMensagem()Ljava/lang/String;
    .registers 2

    .line 35
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/type/SecurityAssertResponseDTO;->mensagem:Ljava/lang/String;

    return-object v0
.end method

.method public isSucesso()Z
    .registers 2

    .line 39
    iget-boolean v0, p0, Lbr/com/itau/sdk/android/core/type/SecurityAssertResponseDTO;->sucesso:Z

    return v0
.end method

.method public isTokenValido()Ljava/lang/Boolean;
    .registers 2

    .line 43
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/type/SecurityAssertResponseDTO;->tokenValido:Ljava/lang/Boolean;

    return-object v0
.end method

.method public isTokenValidoInformarNovoCodigo()Ljava/lang/Boolean;
    .registers 2

    .line 47
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/type/SecurityAssertResponseDTO;->tokenValidoInformarNovoCodigo:Ljava/lang/Boolean;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .registers 6

    .line 52
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lbr/com/itau/sdk/android/core/type/SecurityAssertResponseDTO;->$:[B

    const/16 v2, 0x9

    aget-byte v1, v1, v2

    or-int/lit8 v2, v1, 0x23

    sget-object v3, Lbr/com/itau/sdk/android/core/type/SecurityAssertResponseDTO;->$:[B

    const/16 v4, 0x9

    aget-byte v3, v3, v4

    invoke-static {v1, v2, v3}, Lbr/com/itau/sdk/android/core/type/SecurityAssertResponseDTO;->$(III)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lbr/com/itau/sdk/android/core/type/SecurityAssertResponseDTO;->mensagem:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lbr/com/itau/sdk/android/core/type/SecurityAssertResponseDTO;->$:[B

    const/16 v2, 0x37

    aget-byte v1, v1, v2

    or-int/lit8 v2, v1, 0x4a

    sget v3, Lbr/com/itau/sdk/android/core/type/SecurityAssertResponseDTO;->$$:I

    and-int/lit8 v3, v3, 0x3d

    invoke-static {v1, v2, v3}, Lbr/com/itau/sdk/android/core/type/SecurityAssertResponseDTO;->$(III)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lbr/com/itau/sdk/android/core/type/SecurityAssertResponseDTO;->sucesso:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lbr/com/itau/sdk/android/core/type/SecurityAssertResponseDTO;->$:[B

    const/16 v2, 0x37

    aget-byte v1, v1, v2

    sget-object v2, Lbr/com/itau/sdk/android/core/type/SecurityAssertResponseDTO;->$:[B

    const/16 v3, 0x9

    aget-byte v2, v2, v3

    sget-object v3, Lbr/com/itau/sdk/android/core/type/SecurityAssertResponseDTO;->$:[B

    const/16 v4, 0x1d

    aget-byte v3, v3, v4

    invoke-static {v1, v2, v3}, Lbr/com/itau/sdk/android/core/type/SecurityAssertResponseDTO;->$(III)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lbr/com/itau/sdk/android/core/type/SecurityAssertResponseDTO;->tokenValido:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lbr/com/itau/sdk/android/core/type/SecurityAssertResponseDTO;->$:[B

    const/16 v2, 0x37

    aget-byte v1, v1, v2

    sget v2, Lbr/com/itau/sdk/android/core/type/SecurityAssertResponseDTO;->$$:I

    and-int/lit16 v2, v2, 0x172

    sget-object v3, Lbr/com/itau/sdk/android/core/type/SecurityAssertResponseDTO;->$:[B

    const/16 v4, 0x42

    aget-byte v3, v3, v4

    neg-int v3, v3

    invoke-static {v1, v2, v3}, Lbr/com/itau/sdk/android/core/type/SecurityAssertResponseDTO;->$(III)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lbr/com/itau/sdk/android/core/type/SecurityAssertResponseDTO;->tokenValidoInformarNovoCodigo:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

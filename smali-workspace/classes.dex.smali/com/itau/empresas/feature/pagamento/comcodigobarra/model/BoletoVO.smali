.class public Lcom/itau/empresas/feature/pagamento/comcodigobarra/model/BoletoVO;
.super Ljava/lang/Object;
.source "BoletoVO.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private agenciaCedente:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "agencia_cedente"
    .end annotation
.end field

.field private banco:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "banco"
    .end annotation
.end field

.field private cedente:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "cedente"
    .end annotation
.end field

.field private codigoBarras:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "codigo_barras"
    .end annotation
.end field

.field private contaCedente:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "conta_cedente"
    .end annotation
.end field

.field private dataVencimento:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "data_vencimento"
    .end annotation
.end field

.field private dataVencimentoEditavel:Z
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "data_vencimento_editavel"
    .end annotation
.end field

.field private digitoVerificadorContaCedente:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "digito_verificador_conta_cedente"
    .end annotation
.end field

.field private identificacaoComprovante:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "identificacao_comprovante"
    .end annotation
.end field

.field private indicadorAgendamento:Z
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "indicador_agendamento"
    .end annotation
.end field

.field private indicadorGuiaFgts:Z
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "indicador_guia_fgts"
    .end annotation
.end field

.field private moeda:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "moeda"
    .end annotation
.end field

.field private tipoCodigoBarras:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "tipo_codigo_barras"
    .end annotation
.end field

.field private valorDesconto:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "valor_desconto"
    .end annotation
.end field

.field private valorDescontoEditavel:Z
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "valor_desconto_editavel"
    .end annotation
.end field

.field private valorDocumento:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "valor_document"
    .end annotation
.end field

.field private valorDocumentoEditavel:Z
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "valor_documento_editavel"
    .end annotation
.end field

.field private valorJuros:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "valor_juros"
    .end annotation
.end field

.field private valorJurosEditavel:Z
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "valor_juros_editavel"
    .end annotation
.end field

.field private valorTotal:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "valor_total"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

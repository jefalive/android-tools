.class public Lcom/itau/empresas/ui/activity/EntregaBoletoIntentFilterActivity;
.super Lcom/itau/empresas/ui/activity/BaseActivity;
.source "EntregaBoletoIntentFilterActivity.java"


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 15
    invoke-direct {p0}, Lcom/itau/empresas/ui/activity/BaseActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .registers 7
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 19
    invoke-virtual {p0}, Lcom/itau/empresas/ui/activity/EntregaBoletoIntentFilterActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    .line 20
    .local v2, "intent":Landroid/content/Intent;
    if-eqz v2, :cond_40

    .line 21
    invoke-virtual {v2}, Landroid/content/Intent;->getDataString()Ljava/lang/String;

    move-result-object v3

    .line 23
    .local v3, "filePath":Ljava/lang/String;
    if-eqz v3, :cond_40

    .line 24
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "file://"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    .line 26
    .local v4, "pdfUri":Landroid/net/Uri;
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/EntregaBoletoIntentFilterActivity;->application:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getPerfilLogado()Lcom/itau/empresas/api/model/PerfilOperadorVO;

    move-result-object v0

    if-eqz v0, :cond_2c

    goto :goto_40

    .line 30
    :cond_2c
    invoke-static {p0}, Lcom/itau/empresas/feature/login/PreLoginActivity;->intent(Landroid/content/Context;)Lcom/itau/empresas/feature/login/PreLoginActivity_$IntentBuilder_;

    move-result-object v0

    const v1, 0x8000

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/login/PreLoginActivity_$IntentBuilder_;->flags(I)Lorg/androidannotations/api/builder/IntentBuilder;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/feature/login/PreLoginActivity_$IntentBuilder_;

    .line 31
    invoke-virtual {v0, v4}, Lcom/itau/empresas/feature/login/PreLoginActivity_$IntentBuilder_;->boletoPdfUri(Landroid/net/Uri;)Lcom/itau/empresas/feature/login/PreLoginActivity_$IntentBuilder_;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/feature/login/PreLoginActivity_$IntentBuilder_;->start()Lorg/androidannotations/api/builder/PostActivityStarter;

    .line 35
    .end local v3    # "filePath":Ljava/lang/String;
    .end local v4    # "pdfUri":Landroid/net/Uri;
    :cond_40
    :goto_40
    invoke-virtual {p0}, Lcom/itau/empresas/ui/activity/EntregaBoletoIntentFilterActivity;->finish()V

    .line 36
    invoke-super {p0, p1}, Lcom/itau/empresas/ui/activity/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 37
    return-void
.end method

.method public opkeysConsumidas()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Ljava/lang/String;>;"
        }
    .end annotation

    .line 41
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

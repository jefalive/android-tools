.class final Lcom/adobe/mobile/AnalyticsTrackLifetimeValueIncrease;
.super Ljava/lang/Object;
.source "AnalyticsTrackLifetimeValueIncrease.java"


# static fields
.field private static final _lifetimeValueMutex:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .line 68
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/adobe/mobile/AnalyticsTrackLifetimeValueIncrease;->_lifetimeValueMutex:Ljava/lang/Object;

    return-void
.end method

.method constructor <init>()V
    .registers 1

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method protected static getLifetimeValue()Ljava/math/BigDecimal;
    .registers 8

    .line 70
    sget-object v4, Lcom/adobe/mobile/AnalyticsTrackLifetimeValueIncrease;->_lifetimeValueMutex:Ljava/lang/Object;

    monitor-enter v4

    .line 74
    :try_start_3
    new-instance v5, Ljava/math/BigDecimal;

    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "ADB_LIFETIME_VALUE"

    const-string v2, "0"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v5, v0}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V
    :try_end_14
    .catch Ljava/lang/NumberFormatException; {:try_start_3 .. :try_end_14} :catch_15
    .catch Lcom/adobe/mobile/StaticMethods$NullContextException; {:try_start_3 .. :try_end_14} :catch_1e
    .catchall {:try_start_3 .. :try_end_14} :catchall_31

    .line 80
    .local v5, "lifetimeValue":Ljava/math/BigDecimal;
    goto :goto_2f

    .line 75
    .end local v5    # "lifetimeValue":Ljava/math/BigDecimal;
    :catch_15
    move-exception v6

    .line 76
    .local v6, "e":Ljava/lang/NumberFormatException;
    :try_start_16
    new-instance v5, Ljava/math/BigDecimal;

    const-string v0, "0"

    invoke-direct {v5, v0}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    .line 80
    .local v5, "lifetimeValue":Ljava/math/BigDecimal;
    .end local v6    # "e":Ljava/lang/NumberFormatException;
    goto :goto_2f

    .line 77
    .end local v5    # "lifetimeValue":Ljava/math/BigDecimal;
    :catch_1e
    move-exception v6

    .line 78
    .local v6, "e":Lcom/adobe/mobile/StaticMethods$NullContextException;
    const-string v0, "Analytics - Error getting current lifetime value:(%s)."

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v6}, Lcom/adobe/mobile/StaticMethods$NullContextException;->getMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logErrorFormat(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_2e
    .catchall {:try_start_16 .. :try_end_2e} :catchall_31

    .line 79
    const/4 v5, 0x0

    .line 82
    .local v5, "lifetimeValue":Ljava/math/BigDecimal;
    .end local v6    # "e":Lcom/adobe/mobile/StaticMethods$NullContextException;
    :goto_2f
    monitor-exit v4

    return-object v5

    .line 83
    .end local v5    # "lifetimeValue":Ljava/math/BigDecimal;
    :catchall_31
    move-exception v7

    monitor-exit v4

    throw v7
.end method

.class public final enum Lcom/itau/empresas/api/model/DetalheLisVO$Tipo;
.super Ljava/lang/Enum;
.source "DetalheLisVO.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/itau/empresas/api/model/DetalheLisVO;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Tipo"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<Lcom/itau/empresas/api/model/DetalheLisVO$Tipo;>;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/itau/empresas/api/model/DetalheLisVO$Tipo;

.field public static final enum DATA:Lcom/itau/empresas/api/model/DetalheLisVO$Tipo;

.field public static final enum DINHEIRO:Lcom/itau/empresas/api/model/DetalheLisVO$Tipo;

.field public static final enum STRING:Lcom/itau/empresas/api/model/DetalheLisVO$Tipo;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .line 50
    new-instance v0, Lcom/itau/empresas/api/model/DetalheLisVO$Tipo;

    const-string v1, "DATA"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/itau/empresas/api/model/DetalheLisVO$Tipo;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/itau/empresas/api/model/DetalheLisVO$Tipo;->DATA:Lcom/itau/empresas/api/model/DetalheLisVO$Tipo;

    new-instance v0, Lcom/itau/empresas/api/model/DetalheLisVO$Tipo;

    const-string v1, "DINHEIRO"

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lcom/itau/empresas/api/model/DetalheLisVO$Tipo;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/itau/empresas/api/model/DetalheLisVO$Tipo;->DINHEIRO:Lcom/itau/empresas/api/model/DetalheLisVO$Tipo;

    new-instance v0, Lcom/itau/empresas/api/model/DetalheLisVO$Tipo;

    const-string v1, "STRING"

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2}, Lcom/itau/empresas/api/model/DetalheLisVO$Tipo;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/itau/empresas/api/model/DetalheLisVO$Tipo;->STRING:Lcom/itau/empresas/api/model/DetalheLisVO$Tipo;

    .line 49
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/itau/empresas/api/model/DetalheLisVO$Tipo;

    sget-object v1, Lcom/itau/empresas/api/model/DetalheLisVO$Tipo;->DATA:Lcom/itau/empresas/api/model/DetalheLisVO$Tipo;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sget-object v1, Lcom/itau/empresas/api/model/DetalheLisVO$Tipo;->DINHEIRO:Lcom/itau/empresas/api/model/DetalheLisVO$Tipo;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    sget-object v1, Lcom/itau/empresas/api/model/DetalheLisVO$Tipo;->STRING:Lcom/itau/empresas/api/model/DetalheLisVO$Tipo;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    sput-object v0, Lcom/itau/empresas/api/model/DetalheLisVO$Tipo;->$VALUES:[Lcom/itau/empresas/api/model/DetalheLisVO$Tipo;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 49
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/itau/empresas/api/model/DetalheLisVO$Tipo;
    .registers 2
    .param p0, "name"    # Ljava/lang/String;

    .line 49
    const-class v0, Lcom/itau/empresas/api/model/DetalheLisVO$Tipo;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/api/model/DetalheLisVO$Tipo;

    return-object v0
.end method

.method public static values()[Lcom/itau/empresas/api/model/DetalheLisVO$Tipo;
    .registers 1

    .line 49
    sget-object v0, Lcom/itau/empresas/api/model/DetalheLisVO$Tipo;->$VALUES:[Lcom/itau/empresas/api/model/DetalheLisVO$Tipo;

    invoke-virtual {v0}, [Lcom/itau/empresas/api/model/DetalheLisVO$Tipo;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/itau/empresas/api/model/DetalheLisVO$Tipo;

    return-object v0
.end method

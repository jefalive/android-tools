.class public final Lbr/com/itau/sdk/android/core/endpoint/i;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final b:[B

.field private static c:I


# instance fields
.field private final a:Lcom/google/gson/Gson;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    const/16 v0, 0x79

    new-array v0, v0, [B

    fill-array-data v0, :array_e

    sput-object v0, Lbr/com/itau/sdk/android/core/endpoint/i;->b:[B

    const/16 v0, 0x1b

    sput v0, Lbr/com/itau/sdk/android/core/endpoint/i;->c:I

    return-void

    :array_e
    .array-data 1
        0x42t
        0x16t
        0x57t
        0x35t
        0x9t
        -0x7t
        -0x4t
        0x11t
        -0x2ct
        0x26t
        0x3t
        0x8t
        -0x1t
        -0xet
        -0x19t
        0xbt
        0x29t
        0x3t
        -0x1t
        -0x9t
        -0x45t
        0x4et
        0x1t
        0x5t
        -0x54t
        0x53t
        0x2t
        -0x5t
        0x0t
        -0x1t
        0x3t
        0x2t
        -0xft
        -0x1t
        0x5t
        -0x7t
        0x11t
        0x1t
        -0xet
        0xft
        -0x54t
        0x1dt
        0x0t
        -0x1dt
        0x4et
        0x7t
        -0x9t
        0x0t
        0x1t
        0x5t
        0xct
        -0x4t
        -0x1t
        -0x4et
        0x1dt
        0x0t
        -0x1dt
        0x4et
        0x7t
        -0x9t
        0x0t
        0x0t
        -0x1ct
        0x22t
        0x1ft
        0x2t
        0x8t
        -0x6t
        0x9t
        -0xat
        -0x12t
        0x13t
        0xet
        -0x3t
        -0x1t
        -0x1t
        0x5t
        -0xet
        -0x45t
        0x42t
        0xdt
        -0xbt
        0x15t
        -0x3ft
        -0x1at
        0x5t
        0x49t
        -0x49t
        0x4et
        -0xet
        0xet
        0x0t
        -0xat
        0x6t
        -0x1t
        -0x1at
        0x15t
        0x4t
        -0x8t
        0xat
        0x6t
        -0x1t
        -0x5t
        0x11t
        -0x5t
        0x8t
        -0x7t
        -0x12t
        0x12t
        0xet
        0x0t
        -0xat
        0x6t
        -0x1t
        -0x1at
        0x15t
        0x4t
        -0x8t
        0xat
        0x6t
        -0x1t
    .end array-data
.end method

.method public constructor <init>(Lcom/google/gson/Gson;)V
    .registers 6

    .line 37
    sget-object v0, Lbr/com/itau/sdk/android/core/endpoint/i;->b:[B

    const/16 v1, 0x1a

    aget-byte v0, v0, v1

    or-int/lit8 v1, v0, 0x1c

    sget-object v2, Lbr/com/itau/sdk/android/core/endpoint/i;->b:[B

    const/16 v3, 0x2d

    aget-byte v2, v2, v3

    invoke-static {v0, v1, v2}, Lbr/com/itau/sdk/android/core/endpoint/i;->a(III)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lbr/com/itau/sdk/android/core/endpoint/i;-><init>(Lcom/google/gson/Gson;Ljava/nio/charset/Charset;)V

    .line 38
    return-void
.end method

.method public constructor <init>(Lcom/google/gson/Gson;Ljava/nio/charset/Charset;)V
    .registers 8

    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    if-nez p1, :cond_1c

    new-instance v0, Ljava/lang/NullPointerException;

    sget-object v1, Lbr/com/itau/sdk/android/core/endpoint/i;->b:[B

    const/4 v2, 0x4

    aget-byte v1, v1, v2

    sget-object v2, Lbr/com/itau/sdk/android/core/endpoint/i;->b:[B

    const/16 v3, 0x32

    aget-byte v2, v2, v3

    or-int/lit8 v3, v2, 0x21

    invoke-static {v1, v2, v3}, Lbr/com/itau/sdk/android/core/endpoint/i;->a(III)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 47
    :cond_1c
    if-nez p2, :cond_36

    new-instance v0, Ljava/lang/NullPointerException;

    sget-object v1, Lbr/com/itau/sdk/android/core/endpoint/i;->b:[B

    const/16 v2, 0x32

    aget-byte v1, v1, v2

    add-int/lit8 v2, v1, 0x4

    sget-object v3, Lbr/com/itau/sdk/android/core/endpoint/i;->b:[B

    const/16 v4, 0x29

    aget-byte v3, v3, v4

    invoke-static {v1, v2, v3}, Lbr/com/itau/sdk/android/core/endpoint/i;->a(III)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 48
    :cond_36
    iput-object p1, p0, Lbr/com/itau/sdk/android/core/endpoint/i;->a:Lcom/google/gson/Gson;

    .line 49
    return-void
.end method

.method private a(Ljava/lang/String;)Ljava/lang/Integer;
    .registers 4

    .line 151
    :try_start_0
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;
    :try_end_7
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_7} :catch_9

    move-result-object v0

    return-object v0

    .line 152
    :catch_9
    move-exception v1

    .line 153
    const/4 v0, 0x0

    return-object v0
.end method

.method private static a(III)Ljava/lang/String;
    .registers 9

    const/4 v4, 0x0

    new-instance v0, Ljava/lang/String;

    sget-object v5, Lbr/com/itau/sdk/android/core/endpoint/i;->b:[B

    add-int/lit8 p0, p0, 0x3

    rsub-int/lit8 p1, p1, 0x73

    add-int/lit8 p2, p2, 0x4

    new-array v1, p0, [B

    if-nez v5, :cond_13

    move v2, p1

    move v3, p2

    :goto_11
    add-int p1, v2, v3

    :cond_13
    add-int/lit8 p2, p2, 0x1

    move v2, v4

    int-to-byte v3, p1

    add-int/lit8 v4, v4, 0x1

    aput-byte v3, v1, v2

    if-ne v4, p0, :cond_22

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Ljava/lang/String;-><init>([BI)V

    return-object v0

    :cond_22
    move v2, p1

    aget-byte v3, v5, p2

    goto :goto_11
.end method

.method private a(Lbr/com/itau/sdk/android/core/model/KeyValue;)V
    .registers 7

    .line 120
    sget-object v0, Lbr/com/itau/sdk/android/core/endpoint/i;->b:[B

    const/16 v1, 0x5d

    aget-byte v0, v0, v1

    sget v1, Lbr/com/itau/sdk/android/core/endpoint/i;->c:I

    and-int/lit8 v1, v1, 0x74

    sget-object v2, Lbr/com/itau/sdk/android/core/endpoint/i;->b:[B

    const/16 v3, 0xc

    aget-byte v2, v2, v3

    invoke-static {v0, v1, v2}, Lbr/com/itau/sdk/android/core/endpoint/i;->a(III)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/model/KeyValue;->getKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1f

    .line 121
    return-void

    .line 123
    :cond_1f
    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/model/KeyValue;->getValue()Ljava/lang/String;

    move-result-object v4

    .line 124
    invoke-static {v4}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_30

    .line 125
    invoke-static {}, Lbr/com/itau/sdk/android/core/BackendClient;->getFlowManager()Lbr/com/itau/sdk/android/core/FlowManager;

    move-result-object v0

    invoke-virtual {v0}, Lbr/com/itau/sdk/android/core/FlowManager;->c()V

    .line 127
    :cond_30
    return-void
.end method

.method private a(Ljava/util/List;)V
    .registers 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<Lbr/com/itau/sdk/android/core/model/KeyValue;>;)V"
        }
    .end annotation

    .line 106
    if-nez p1, :cond_3

    .line 107
    return-void

    .line 109
    :cond_3
    const/4 v1, 0x0

    :goto_4
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1f

    .line 110
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lbr/com/itau/sdk/android/core/model/KeyValue;

    .line 111
    if-eqz v2, :cond_1c

    .line 112
    invoke-direct {p0, v2}, Lbr/com/itau/sdk/android/core/endpoint/i;->a(Lbr/com/itau/sdk/android/core/model/KeyValue;)V

    .line 113
    invoke-direct {p0, v2}, Lbr/com/itau/sdk/android/core/endpoint/i;->b(Lbr/com/itau/sdk/android/core/model/KeyValue;)V

    .line 114
    invoke-direct {p0, v2}, Lbr/com/itau/sdk/android/core/endpoint/i;->c(Lbr/com/itau/sdk/android/core/model/KeyValue;)V

    .line 109
    :cond_1c
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 117
    :cond_1f
    return-void
.end method

.method private b(Lbr/com/itau/sdk/android/core/model/KeyValue;)V
    .registers 6

    .line 130
    sget-object v0, Lbr/com/itau/sdk/android/core/endpoint/i;->b:[B

    const/16 v1, 0x27

    aget-byte v0, v0, v1

    sget-object v1, Lbr/com/itau/sdk/android/core/endpoint/i;->b:[B

    const/16 v2, 0x63

    aget-byte v1, v1, v2

    const/16 v2, 0x63

    invoke-static {v0, v1, v2}, Lbr/com/itau/sdk/android/core/endpoint/i;->a(III)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/model/KeyValue;->getKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1d

    .line 131
    return-void

    .line 133
    :cond_1d
    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/model/KeyValue;->getValue()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lbr/com/itau/sdk/android/core/endpoint/i;->a(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v3

    .line 134
    if-eqz v3, :cond_2e

    .line 135
    invoke-static {}, Lbr/com/itau/sdk/android/core/o;->a()Lbr/com/itau/sdk/android/core/o;

    move-result-object v0

    invoke-virtual {v0, v3}, Lbr/com/itau/sdk/android/core/o;->a(Ljava/lang/Integer;)V

    .line 137
    :cond_2e
    return-void
.end method

.method private c(Lbr/com/itau/sdk/android/core/model/KeyValue;)V
    .registers 7

    .line 140
    sget-object v0, Lbr/com/itau/sdk/android/core/endpoint/i;->b:[B

    const/16 v1, 0xf

    aget-byte v0, v0, v1

    sget-object v1, Lbr/com/itau/sdk/android/core/endpoint/i;->b:[B

    const/16 v2, 0x1c

    aget-byte v1, v1, v2

    sget-object v2, Lbr/com/itau/sdk/android/core/endpoint/i;->b:[B

    const/16 v3, 0x18

    aget-byte v2, v2, v3

    neg-int v2, v2

    invoke-static {v0, v1, v2}, Lbr/com/itau/sdk/android/core/endpoint/i;->a(III)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/model/KeyValue;->getKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_22

    .line 141
    return-void

    .line 143
    :cond_22
    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/model/KeyValue;->getValue()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lbr/com/itau/sdk/android/core/endpoint/i;->a(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    .line 144
    if-eqz v4, :cond_33

    .line 145
    invoke-static {}, Lbr/com/itau/sdk/android/core/o;->a()Lbr/com/itau/sdk/android/core/o;

    move-result-object v0

    invoke-virtual {v0, v4}, Lbr/com/itau/sdk/android/core/o;->b(Ljava/lang/Integer;)V

    .line 147
    :cond_33
    return-void
.end method


# virtual methods
.method public a(Lokhttp3/ResponseBody;)Lbr/com/itau/sdk/android/core/model/ResponseDTO;
    .registers 11
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 59
    invoke-virtual {p1}, Lokhttp3/ResponseBody;->string()Ljava/lang/String;

    move-result-object v3

    .line 60
    sget-object v0, Lbr/com/itau/sdk/android/core/endpoint/i;->b:[B

    const/16 v1, 0x54

    aget-byte v0, v0, v1

    neg-int v0, v0

    const/16 v1, 0x37

    const/16 v2, 0x38

    invoke-static {v0, v1, v2}, Lbr/com/itau/sdk/android/core/endpoint/i;->a(III)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object v3, v1, v2

    invoke-static {p0, v0, v1}, Lbr/com/itau/sdk/android/core/l;->a(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 62
    new-instance v0, Lbr/com/itau/sdk/android/core/endpoint/j;

    invoke-direct {v0, p0}, Lbr/com/itau/sdk/android/core/endpoint/j;-><init>(Lbr/com/itau/sdk/android/core/endpoint/i;)V

    .line 63
    invoke-virtual {v0}, Lbr/com/itau/sdk/android/core/endpoint/j;->getType()Ljava/lang/reflect/Type;

    move-result-object v4

    .line 65
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/endpoint/i;->a:Lcom/google/gson/Gson;

    invoke-virtual {v0, v3, v4}, Lcom/google/gson/Gson;->fromJson(Ljava/lang/String;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lbr/com/itau/sdk/android/core/model/ResponseDTO;

    .line 67
    if-eqz v5, :cond_8c

    .line 70
    invoke-virtual {v5}, Lbr/com/itau/sdk/android/core/model/ResponseDTO;->getHeader()Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, v0}, Lbr/com/itau/sdk/android/core/endpoint/i;->a(Ljava/util/List;)V

    .line 72
    invoke-static {}, Lbr/com/itau/sdk/android/core/BackendClient;->getFlowManager()Lbr/com/itau/sdk/android/core/FlowManager;

    move-result-object v0

    invoke-virtual {v5}, Lbr/com/itau/sdk/android/core/model/ResponseDTO;->getFlow()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbr/com/itau/sdk/android/core/FlowManager;->feedFlows(Ljava/util/List;)V

    .line 74
    invoke-virtual {v5}, Lbr/com/itau/sdk/android/core/model/ResponseDTO;->getData()Lcom/google/gson/JsonElement;

    move-result-object v6

    .line 75
    if-eqz v6, :cond_8c

    invoke-virtual {v6}, Lcom/google/gson/JsonElement;->isJsonObject()Z

    move-result v0

    if-eqz v0, :cond_8c

    .line 76
    invoke-virtual {v6}, Lcom/google/gson/JsonElement;->getAsJsonObject()Lcom/google/gson/JsonObject;

    move-result-object v7

    .line 77
    sget-object v0, Lbr/com/itau/sdk/android/core/endpoint/i;->b:[B

    const/16 v1, 0x1c

    aget-byte v0, v0, v1

    or-int/lit8 v1, v0, 0x2a

    const/16 v2, 0x61

    invoke-static {v0, v1, v2}, Lbr/com/itau/sdk/android/core/endpoint/i;->a(III)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Lcom/google/gson/JsonObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8c

    sget-object v0, Lbr/com/itau/sdk/android/core/endpoint/i;->b:[B

    const/16 v1, 0x1c

    aget-byte v0, v0, v1

    or-int/lit8 v1, v0, 0x24

    const/16 v2, 0x2b

    invoke-static {v0, v1, v2}, Lbr/com/itau/sdk/android/core/endpoint/i;->a(III)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Lcom/google/gson/JsonObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8c

    .line 78
    sget-object v0, Lbr/com/itau/sdk/android/core/n;->a:Lcom/google/gson/Gson;

    const-class v1, Lbr/com/itau/sdk/android/core/model/LegacyMenuDTO;

    invoke-virtual {v0, v7, v1}, Lcom/google/gson/Gson;->fromJson(Lcom/google/gson/JsonElement;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lbr/com/itau/sdk/android/core/model/LegacyMenuDTO;

    .line 79
    invoke-static {}, Lbr/com/itau/sdk/android/core/BackendClient;->getFlowManager()Lbr/com/itau/sdk/android/core/FlowManager;

    move-result-object v0

    invoke-virtual {v0, v8}, Lbr/com/itau/sdk/android/core/FlowManager;->a(Lbr/com/itau/sdk/android/core/model/LegacyMenuDTO;)V

    .line 85
    :cond_8c
    return-object v5
.end method

.method public a(Lcom/google/gson/JsonElement;Lcom/google/gson/reflect/TypeToken;)Ljava/lang/Object;
    .registers 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:Ljava/lang/Object;>(Lcom/google/gson/JsonElement;Lcom/google/gson/reflect/TypeToken<TT;>;)TT;"
        }
    .end annotation

    .line 162
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/endpoint/i;->a:Lcom/google/gson/Gson;

    invoke-virtual {p2}, Lcom/google/gson/reflect/TypeToken;->getType()Ljava/lang/reflect/Type;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/Gson;->fromJson(Lcom/google/gson/JsonElement;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public a(Lokhttp3/ResponseBody;Ljava/lang/reflect/Type;)Ljava/lang/Object;
    .registers 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:Ljava/lang/Object;>(Lokhttp3/ResponseBody;Ljava/lang/reflect/Type;)TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 52
    invoke-virtual {p1}, Lokhttp3/ResponseBody;->string()Ljava/lang/String;

    move-result-object v3

    .line 53
    sget-object v0, Lbr/com/itau/sdk/android/core/endpoint/i;->b:[B

    const/16 v1, 0x54

    aget-byte v0, v0, v1

    neg-int v0, v0

    const/16 v1, 0x37

    const/16 v2, 0x38

    invoke-static {v0, v1, v2}, Lbr/com/itau/sdk/android/core/endpoint/i;->a(III)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object v3, v1, v2

    invoke-static {p0, v0, v1}, Lbr/com/itau/sdk/android/core/l;->a(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 54
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/endpoint/i;->a:Lcom/google/gson/Gson;

    invoke-virtual {v0, v3, p2}, Lcom/google/gson/Gson;->fromJson(Ljava/lang/String;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/Object;Ljava/lang/reflect/Type;Lbr/com/itau/sdk/android/core/SDKProgressListener;)Lokhttp3/RequestBody;
    .registers 7

    .line 89
    if-nez p3, :cond_f

    .line 90
    sget-object v0, Lbr/com/itau/sdk/android/core/n;->c:Lokhttp3/MediaType;

    iget-object v1, p0, Lbr/com/itau/sdk/android/core/endpoint/i;->a:Lcom/google/gson/Gson;

    .line 91
    invoke-virtual {v1, p1, p2}, Lcom/google/gson/Gson;->toJson(Ljava/lang/Object;Ljava/lang/reflect/Type;)Ljava/lang/String;

    move-result-object v1

    .line 90
    invoke-static {v0, v1}, Lokhttp3/RequestBody;->create(Lokhttp3/MediaType;Ljava/lang/String;)Lokhttp3/RequestBody;

    move-result-object v0

    return-object v0

    .line 93
    :cond_f
    new-instance v0, Lbr/com/itau/sdk/android/core/endpoint/o;

    sget-object v1, Lbr/com/itau/sdk/android/core/n;->c:Lokhttp3/MediaType;

    iget-object v2, p0, Lbr/com/itau/sdk/android/core/endpoint/i;->a:Lcom/google/gson/Gson;

    .line 94
    invoke-virtual {v2, p1, p2}, Lcom/google/gson/Gson;->toJson(Ljava/lang/Object;Ljava/lang/reflect/Type;)Ljava/lang/String;

    move-result-object v2

    .line 93
    invoke-static {v1, v2}, Lokhttp3/RequestBody;->create(Lokhttp3/MediaType;Ljava/lang/String;)Lokhttp3/RequestBody;

    move-result-object v1

    invoke-direct {v0, v1, p3}, Lbr/com/itau/sdk/android/core/endpoint/o;-><init>(Lokhttp3/RequestBody;Lbr/com/itau/sdk/android/core/SDKProgressListener;)V

    return-object v0
.end method

.method public clone()Ljava/lang/Object;
    .registers 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .line 102
    new-instance v0, Ljava/lang/CloneNotSupportedException;

    sget v1, Lbr/com/itau/sdk/android/core/endpoint/i;->c:I

    and-int/lit8 v1, v1, 0x74

    or-int/lit8 v2, v1, 0x20

    sget-object v3, Lbr/com/itau/sdk/android/core/endpoint/i;->b:[B

    const/16 v4, 0xf

    aget-byte v3, v3, v4

    invoke-static {v1, v2, v3}, Lbr/com/itau/sdk/android/core/endpoint/i;->a(III)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/CloneNotSupportedException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.class Lcom/itau/empresas/controller/ExtratoController$2;
.super Ljava/lang/Object;
.source "ExtratoController.java"

# interfaces
.implements Lbr/com/itau/sdk/android/core/type/BackendCall;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/itau/empresas/controller/ExtratoController;->consultaLis(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/itau/empresas/controller/ExtratoController;

.field final synthetic val$agencia:Ljava/lang/String;

.field final synthetic val$conta:Ljava/lang/String;

.field final synthetic val$dac:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/itau/empresas/controller/ExtratoController;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 5
    .param p1, "this$0"    # Lcom/itau/empresas/controller/ExtratoController;

    .line 33
    iput-object p1, p0, Lcom/itau/empresas/controller/ExtratoController$2;->this$0:Lcom/itau/empresas/controller/ExtratoController;

    iput-object p2, p0, Lcom/itau/empresas/controller/ExtratoController$2;->val$agencia:Ljava/lang/String;

    iput-object p3, p0, Lcom/itau/empresas/controller/ExtratoController$2;->val$conta:Ljava/lang/String;

    iput-object p4, p0, Lcom/itau/empresas/controller/ExtratoController$2;->val$dac:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public callBackend()V
    .registers 5

    .line 36
    iget-object v0, p0, Lcom/itau/empresas/controller/ExtratoController$2;->this$0:Lcom/itau/empresas/controller/ExtratoController;

    iget-object v0, v0, Lcom/itau/empresas/controller/ExtratoController;->app:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getEventoTracker()Lcom/itau/empresas/ui/util/analytics/TrackerEvento;

    move-result-object v0

    const-string v1, "lis"

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/analytics/TrackerEvento;->inicioMonitorarTempo(Ljava/lang/String;)V

    .line 37
    iget-object v0, p0, Lcom/itau/empresas/controller/ExtratoController$2;->this$0:Lcom/itau/empresas/controller/ExtratoController;

    invoke-virtual {v0}, Lcom/itau/empresas/controller/ExtratoController;->api()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/api/Api;

    iget-object v1, p0, Lcom/itau/empresas/controller/ExtratoController$2;->val$agencia:Ljava/lang/String;

    iget-object v2, p0, Lcom/itau/empresas/controller/ExtratoController$2;->val$conta:Ljava/lang/String;

    iget-object v3, p0, Lcom/itau/empresas/controller/ExtratoController$2;->val$dac:Ljava/lang/String;

    invoke-interface {v0, v1, v2, v3}, Lcom/itau/empresas/api/Api;->consultaLis(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/itau/empresas/api/model/LisVO;

    move-result-object v0

    invoke-static {v0}, Lcom/itau/empresas/controller/BaseController;->post(Ljava/lang/Object;)V

    .line 38
    iget-object v0, p0, Lcom/itau/empresas/controller/ExtratoController$2;->this$0:Lcom/itau/empresas/controller/ExtratoController;

    iget-object v0, v0, Lcom/itau/empresas/controller/ExtratoController;->app:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getEventoTracker()Lcom/itau/empresas/ui/util/analytics/TrackerEvento;

    move-result-object v0

    const-string v1, "lis"

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/analytics/TrackerEvento;->fimMonitorarTempo(Ljava/lang/String;)V

    .line 39
    return-void
.end method

.class public final Lbr/com/itau/security/commons/json/JsonHelper;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final b:[B

.field private static c:I


# instance fields
.field private final a:Lorg/json/JSONObject;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    const/16 v0, 0x1e

    new-array v0, v0, [B

    fill-array-data v0, :array_e

    sput-object v0, Lbr/com/itau/security/commons/json/JsonHelper;->b:[B

    const/16 v0, 0xc9

    sput v0, Lbr/com/itau/security/commons/json/JsonHelper;->c:I

    return-void

    :array_e
    .array-data 1
        0x71t
        -0x65t
        0x51t
        -0x1at
        -0x21t
        -0x5t
        0x42t
        -0x46t
        0x1t
        -0x15t
        0x11t
        -0x2et
        -0x8t
        0x7t
        0x6t
        0x42t
        -0x50t
        -0x3t
        -0x7t
        0x52t
        -0x52t
        0xdt
        -0x13t
        -0x3t
        0xct
        0x43t
        -0x46t
        0x1t
        -0x15t
        0x11t
    .end array-data
.end method

.method public constructor <init>()V
    .registers 2

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    iput-object v0, p0, Lbr/com/itau/security/commons/json/JsonHelper;->a:Lorg/json/JSONObject;

    .line 26
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .registers 6

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    :try_start_3
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lbr/com/itau/security/commons/json/JsonHelper;->a:Lorg/json/JSONObject;
    :try_end_a
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_a} :catch_b

    .line 21
    return-void

    .line 19
    .line 20
    :catch_b
    new-instance v0, Lbr/com/itau/security/commons/exception/CommonSecurityException;

    sget-object v1, Lbr/com/itau/security/commons/json/JsonHelper;->b:[B

    const/16 v2, 0x8

    aget-byte v1, v1, v2

    add-int/lit8 v1, v1, -0x1

    int-to-byte v1, v1

    int-to-byte v2, v1

    int-to-byte v3, v2

    invoke-static {v1, v2, v3}, Lbr/com/itau/security/commons/json/JsonHelper;->a(ISI)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lbr/com/itau/security/commons/exception/CommonSecurityException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private static a(ISI)Ljava/lang/String;
    .registers 9

    const/4 v5, 0x0

    mul-int/lit8 p1, p1, 0x7

    rsub-int/lit8 p1, p1, 0xb

    mul-int/lit8 p0, p0, 0xc

    rsub-int/lit8 p0, p0, 0x14

    rsub-int/lit8 p2, p2, 0x43

    new-instance v0, Ljava/lang/String;

    sget-object v4, Lbr/com/itau/security/commons/json/JsonHelper;->b:[B

    new-array v1, p0, [B

    add-int/lit8 p0, p0, -0x1

    if-nez v4, :cond_1d

    move v2, p1

    move v3, p0

    :goto_17
    add-int/lit8 p1, p1, 0x1

    neg-int v3, v3

    add-int/2addr v2, v3

    add-int/lit8 p2, v2, -0x2

    :cond_1d
    int-to-byte v2, p2

    aput-byte v2, v1, v5

    if-ne v5, p0, :cond_27

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Ljava/lang/String;-><init>([BI)V

    return-object v0

    :cond_27
    move v2, p2

    add-int/lit8 v5, v5, 0x1

    aget-byte v3, v4, p1

    goto :goto_17
.end method


# virtual methods
.method public final getString(Ljava/lang/String;)Ljava/lang/String;
    .registers 6

    .line 48
    iget-object v0, p0, Lbr/com/itau/security/commons/json/JsonHelper;->a:Lorg/json/JSONObject;

    invoke-virtual {v0, p1}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1f

    .line 49
    new-instance v0, Lbr/com/itau/security/commons/exception/CommonSecurityException;

    sget-object v1, Lbr/com/itau/security/commons/json/JsonHelper;->b:[B

    const/16 v2, 0x8

    aget-byte v1, v1, v2

    int-to-byte v1, v1

    int-to-byte v2, v1

    int-to-byte v3, v2

    invoke-static {v1, v2, v3}, Lbr/com/itau/security/commons/json/JsonHelper;->a(ISI)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lbr/com/itau/security/commons/exception/CommonSecurityException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 52
    :cond_1f
    :try_start_1f
    iget-object v0, p0, Lbr/com/itau/security/commons/json/JsonHelper;->a:Lorg/json/JSONObject;

    invoke-virtual {v0, p1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_24
    .catch Lorg/json/JSONException; {:try_start_1f .. :try_end_24} :catch_26

    move-result-object v0

    return-object v0

    .line 53
    .line 54
    :catch_26
    new-instance v0, Lbr/com/itau/security/commons/exception/CommonSecurityException;

    sget-object v1, Lbr/com/itau/security/commons/json/JsonHelper;->b:[B

    const/16 v2, 0x8

    aget-byte v1, v1, v2

    int-to-byte v1, v1

    int-to-byte v2, v1

    int-to-byte v3, v2

    invoke-static {v1, v2, v3}, Lbr/com/itau/security/commons/json/JsonHelper;->a(ISI)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lbr/com/itau/security/commons/exception/CommonSecurityException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final put(Ljava/lang/String;Ljava/lang/String;)Lbr/com/itau/security/commons/json/JsonHelper;
    .registers 7

    .line 30
    :try_start_0
    iget-object v0, p0, Lbr/com/itau/security/commons/json/JsonHelper;->a:Lorg/json/JSONObject;

    invoke-virtual {v0, p1, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_5
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_5} :catch_6

    .line 33
    goto :goto_1d

    .line 31
    .line 32
    :catch_6
    new-instance v0, Lbr/com/itau/security/commons/exception/CommonSecurityException;

    sget-object v1, Lbr/com/itau/security/commons/json/JsonHelper;->b:[B

    const/16 v2, 0x8

    aget-byte v1, v1, v2

    int-to-byte v1, v1

    int-to-byte v2, v1

    int-to-byte v3, v2

    invoke-static {v1, v2, v3}, Lbr/com/itau/security/commons/json/JsonHelper;->a(ISI)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lbr/com/itau/security/commons/exception/CommonSecurityException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 34
    :goto_1d
    return-object p0
.end method

.method public final toString()Ljava/lang/String;
    .registers 2

    .line 60
    iget-object v0, p0, Lbr/com/itau/security/commons/json/JsonHelper;->a:Lorg/json/JSONObject;

    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

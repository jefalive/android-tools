.class public Lcom/itau/empresas/api/model/DadosOperadorVO;
.super Ljava/lang/Object;
.source "DadosOperadorVO.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private DDD:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "DDD"
    .end annotation
.end field

.field private codigoOperador:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "codigo_operador"
    .end annotation
.end field

.field private dadosAlcadas:Lcom/itau/empresas/api/model/DadosAlcadasOperadorVO;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "DadosAlcadas"
    .end annotation
.end field

.field private dataCriacao:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "data_criacao"
    .end annotation
.end field

.field private dataUltimoAcesso:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "data_ultimo_acesso"
    .end annotation
.end field

.field private descricaoTipoOperador:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "descricao_tipo_operador"
    .end annotation
.end field

.field private email:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "email"
    .end annotation
.end field

.field private nomeEmpresa:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "nome_empresa"
    .end annotation
.end field

.field private nomeOperador:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "nome_operador"
    .end annotation
.end field

.field private nomePerfil:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "nome_perfil"
    .end annotation
.end field

.field private numeroDocumento:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "numero_documento"
    .end annotation
.end field

.field private status:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "status"
    .end annotation
.end field

.field private telefone:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "telefone"
    .end annotation
.end field

.field private tipoDocumento:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "tipo_documento"
    .end annotation
.end field

.field private tipoOperador:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "tipo_operador"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getCodigoOperador()Ljava/lang/String;
    .registers 2

    .line 42
    iget-object v0, p0, Lcom/itau/empresas/api/model/DadosOperadorVO;->codigoOperador:Ljava/lang/String;

    return-object v0
.end method

.method public getDDD()Ljava/lang/String;
    .registers 2

    .line 82
    iget-object v0, p0, Lcom/itau/empresas/api/model/DadosOperadorVO;->DDD:Ljava/lang/String;

    invoke-static {v0}, Lcom/itau/empresas/ui/util/ViewUtils;->removaPaddingsZero(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getEmail()Ljava/lang/String;
    .registers 2

    .line 58
    iget-object v0, p0, Lcom/itau/empresas/api/model/DadosOperadorVO;->email:Ljava/lang/String;

    return-object v0
.end method

.method public getNomeEmpresa()Ljava/lang/String;
    .registers 2

    .line 102
    iget-object v0, p0, Lcom/itau/empresas/api/model/DadosOperadorVO;->nomeEmpresa:Ljava/lang/String;

    return-object v0
.end method

.method public getNomeOperador()Ljava/lang/String;
    .registers 2

    .line 50
    iget-object v0, p0, Lcom/itau/empresas/api/model/DadosOperadorVO;->nomeOperador:Ljava/lang/String;

    return-object v0
.end method

.method public getNomePerfil()Ljava/lang/String;
    .registers 2

    .line 74
    iget-object v0, p0, Lcom/itau/empresas/api/model/DadosOperadorVO;->nomePerfil:Ljava/lang/String;

    return-object v0
.end method

.method public getNumeroDocumento()Ljava/lang/String;
    .registers 2

    .line 94
    iget-object v0, p0, Lcom/itau/empresas/api/model/DadosOperadorVO;->numeroDocumento:Ljava/lang/String;

    return-object v0
.end method

.method public getTipoDocumento()Ljava/lang/String;
    .registers 2

    .line 90
    iget-object v0, p0, Lcom/itau/empresas/api/model/DadosOperadorVO;->tipoDocumento:Ljava/lang/String;

    return-object v0
.end method

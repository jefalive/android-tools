.class public Lcom/itau/empresas/api/model/MenuUniversalVO;
.super Ljava/lang/Object;
.source "MenuUniversalVO.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field dados:Ljava/util/List;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "DADOS"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Lcom/itau/empresas/api/model/DadoMenuUniversalVO;>;"
        }
    .end annotation
.end field

.field links:Ljava/util/List;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "LINKS"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Lcom/itau/empresas/api/model/LinkVO;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getDado(Ljava/lang/String;)Lcom/itau/empresas/api/model/DadoMenuUniversalVO;
    .registers 5
    .param p1, "key"    # Ljava/lang/String;

    .line 28
    iget-object v0, p0, Lcom/itau/empresas/api/model/MenuUniversalVO;->dados:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1f

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/itau/empresas/api/model/DadoMenuUniversalVO;

    .line 29
    .local v2, "dado":Lcom/itau/empresas/api/model/DadoMenuUniversalVO;
    invoke-virtual {v2}, Lcom/itau/empresas/api/model/DadoMenuUniversalVO;->getKey()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1e

    .line 30
    return-object v2

    .line 32
    .end local v2    # "dado":Lcom/itau/empresas/api/model/DadoMenuUniversalVO;
    :cond_1e
    goto :goto_6

    .line 33
    :cond_1f
    const/4 v0, 0x0

    return-object v0
.end method

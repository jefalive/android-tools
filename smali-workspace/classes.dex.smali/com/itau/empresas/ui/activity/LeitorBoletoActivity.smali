.class public Lcom/itau/empresas/ui/activity/LeitorBoletoActivity;
.super Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;
.source "LeitorBoletoActivity.java"


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 15
    invoke-direct {p0}, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;-><init>()V

    return-void
.end method


# virtual methods
.method public adjustReaderState(Lbr/com/itau/sdk/android/leitorboleto/ui/utils/ReaderState;)V
    .registers 4
    .param p1, "state"    # Lbr/com/itau/sdk/android/leitorboleto/ui/utils/ReaderState;

    .line 24
    invoke-static {}, Lcom/itau/empresas/ui/util/PoyntUtils;->isPoynt()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 25
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/itau/empresas/ui/activity/LeitorBoletoActivity;->setRequestedOrientation(I)V

    goto :goto_f

    .line 27
    :cond_b
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/itau/empresas/ui/activity/LeitorBoletoActivity;->setRequestedOrientation(I)V

    .line 30
    :goto_f
    const v0, 0x7f030043

    invoke-virtual {p1, v0}, Lbr/com/itau/sdk/android/leitorboleto/ui/utils/ReaderState;->withLayoutId(I)Lbr/com/itau/sdk/android/leitorboleto/ui/utils/ReaderState;

    move-result-object v0

    .line 31
    const v1, 0x7f0e01dd

    invoke-virtual {v0, v1}, Lbr/com/itau/sdk/android/leitorboleto/ui/utils/ReaderState;->withSurfaceViewId(I)Lbr/com/itau/sdk/android/leitorboleto/ui/utils/ReaderState;

    move-result-object v0

    .line 32
    const v1, 0x7f0e01de

    invoke-virtual {v0, v1}, Lbr/com/itau/sdk/android/leitorboleto/ui/utils/ReaderState;->withViewFinderId(I)Lbr/com/itau/sdk/android/leitorboleto/ui/utils/ReaderState;

    move-result-object v0

    const-string v1, "Erro no leitor"

    .line 33
    invoke-virtual {v0, v1}, Lbr/com/itau/sdk/android/leitorboleto/ui/utils/ReaderState;->withDialogError(Ljava/lang/String;)Lbr/com/itau/sdk/android/leitorboleto/ui/utils/ReaderState;

    move-result-object v0

    const-string v1, "Erro"

    .line 34
    invoke-virtual {v0, v1}, Lbr/com/itau/sdk/android/leitorboleto/ui/utils/ReaderState;->withDialogTitle(Ljava/lang/String;)Lbr/com/itau/sdk/android/leitorboleto/ui/utils/ReaderState;

    .line 35
    return-void
.end method

.method public confirmar()V
    .registers 1

    .line 19
    invoke-virtual {p0}, Lcom/itau/empresas/ui/activity/LeitorBoletoActivity;->finish()V

    .line 20
    return-void
.end method

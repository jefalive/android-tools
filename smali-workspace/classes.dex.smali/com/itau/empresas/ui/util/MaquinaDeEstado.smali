.class public final Lcom/itau/empresas/ui/util/MaquinaDeEstado;
.super Ljava/util/HashMap;
.source "MaquinaDeEstado.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/itau/empresas/ui/util/MaquinaDeEstado$Chave;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/HashMap<Lcom/itau/empresas/ui/util/MaquinaDeEstado$Chave;Lcom/itau/empresas/ui/util/maquinaestado/Estado;>;"
    }
.end annotation


# instance fields
.field private estadoAtual:Lcom/itau/empresas/ui/util/MaquinaDeEstado$Chave;

.field private ultimoEstado:Lcom/itau/empresas/ui/util/MaquinaDeEstado$Chave;


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 10
    invoke-direct {p0}, Ljava/util/HashMap;-><init>()V

    return-void
.end method


# virtual methods
.method public getEstadoAtual()Lcom/itau/empresas/ui/util/MaquinaDeEstado$Chave;
    .registers 2

    .line 67
    iget-object v0, p0, Lcom/itau/empresas/ui/util/MaquinaDeEstado;->estadoAtual:Lcom/itau/empresas/ui/util/MaquinaDeEstado$Chave;

    return-object v0
.end method

.method public mudaParaEstado(Lcom/itau/empresas/ui/util/MaquinaDeEstado$Chave;)Lcom/itau/empresas/ui/util/MaquinaDeEstado$Chave;
    .registers 8
    .param p1, "chave"    # Lcom/itau/empresas/ui/util/MaquinaDeEstado$Chave;

    .line 28
    iget-object v0, p0, Lcom/itau/empresas/ui/util/MaquinaDeEstado;->estadoAtual:Lcom/itau/empresas/ui/util/MaquinaDeEstado$Chave;

    iput-object v0, p0, Lcom/itau/empresas/ui/util/MaquinaDeEstado;->ultimoEstado:Lcom/itau/empresas/ui/util/MaquinaDeEstado$Chave;

    .line 29
    iput-object p1, p0, Lcom/itau/empresas/ui/util/MaquinaDeEstado;->estadoAtual:Lcom/itau/empresas/ui/util/MaquinaDeEstado$Chave;

    .line 31
    invoke-virtual {p0, p1}, Lcom/itau/empresas/ui/util/MaquinaDeEstado;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/itau/empresas/ui/util/maquinaestado/Estado;

    .line 34
    .local v1, "estado":Lcom/itau/empresas/ui/util/maquinaestado/Estado;
    invoke-virtual {v1}, Lcom/itau/empresas/ui/util/maquinaestado/Estado;->getGones()[Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_25

    .line 35
    invoke-virtual {v1}, Lcom/itau/empresas/ui/util/maquinaestado/Estado;->getGones()[Landroid/view/View;

    move-result-object v2

    array-length v3, v2

    const/4 v4, 0x0

    :goto_19
    if-ge v4, v3, :cond_25

    aget-object v5, v2, v4

    .line 36
    .local v5, "view":Landroid/view/View;
    const/16 v0, 0x8

    invoke-virtual {v5, v0}, Landroid/view/View;->setVisibility(I)V

    .line 35
    .end local v5    # "view":Landroid/view/View;
    add-int/lit8 v4, v4, 0x1

    goto :goto_19

    .line 41
    :cond_25
    invoke-virtual {v1}, Lcom/itau/empresas/ui/util/maquinaestado/Estado;->getVisibles()[Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_3c

    .line 42
    invoke-virtual {v1}, Lcom/itau/empresas/ui/util/maquinaestado/Estado;->getVisibles()[Landroid/view/View;

    move-result-object v2

    array-length v3, v2

    const/4 v4, 0x0

    :goto_31
    if-ge v4, v3, :cond_3c

    aget-object v5, v2, v4

    .line 43
    .local v5, "view":Landroid/view/View;
    const/4 v0, 0x0

    invoke-virtual {v5, v0}, Landroid/view/View;->setVisibility(I)V

    .line 42
    .end local v5    # "view":Landroid/view/View;
    add-int/lit8 v4, v4, 0x1

    goto :goto_31

    .line 48
    :cond_3c
    invoke-virtual {v1}, Lcom/itau/empresas/ui/util/maquinaestado/Estado;->getOnChangeState()Lcom/itau/empresas/ui/util/maquinaestado/Estado$OnChangeState;

    move-result-object v2

    .line 49
    .local v2, "onChangeState":Lcom/itau/empresas/ui/util/maquinaestado/Estado$OnChangeState;
    if-eqz v2, :cond_45

    .line 50
    invoke-interface {v2}, Lcom/itau/empresas/ui/util/maquinaestado/Estado$OnChangeState;->onChangeState()V

    .line 53
    :cond_45
    return-object p1
.end method

.method public novoEstado(Lcom/itau/empresas/ui/util/MaquinaDeEstado$Chave;Lcom/itau/empresas/ui/util/maquinaestado/Estado;)Lcom/itau/empresas/ui/util/MaquinaDeEstado;
    .registers 4
    .param p1, "chave"    # Lcom/itau/empresas/ui/util/MaquinaDeEstado$Chave;
    .param p2, "estado"    # Lcom/itau/empresas/ui/util/maquinaestado/Estado;

    .line 22
    invoke-virtual {p0, p1, p2}, Lcom/itau/empresas/ui/util/MaquinaDeEstado;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 23
    return-object p0
.end method

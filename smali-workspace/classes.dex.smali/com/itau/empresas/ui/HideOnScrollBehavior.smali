.class public Lcom/itau/empresas/ui/HideOnScrollBehavior;
.super Landroid/support/design/widget/CoordinatorLayout$Behavior;
.source "HideOnScrollBehavior.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/support/design/widget/CoordinatorLayout$Behavior<Landroid/view/View;>;"
    }
.end annotation


# instance fields
.field private dependencyHeight:I

.field private scrollingView:Landroid/view/View;

.field private scrollingViewPaddingBottom:I


# direct methods
.method public constructor <init>()V
    .registers 2

    .line 28
    invoke-direct {p0}, Landroid/support/design/widget/CoordinatorLayout$Behavior;-><init>()V

    .line 26
    const/high16 v0, -0x80000000

    iput v0, p0, Lcom/itau/empresas/ui/HideOnScrollBehavior;->scrollingViewPaddingBottom:I

    .line 30
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .line 33
    invoke-direct {p0, p1, p2}, Landroid/support/design/widget/CoordinatorLayout$Behavior;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 26
    const/high16 v0, -0x80000000

    iput v0, p0, Lcom/itau/empresas/ui/HideOnScrollBehavior;->scrollingViewPaddingBottom:I

    .line 34
    return-void
.end method

.method private static findScrollingView(Landroid/support/design/widget/CoordinatorLayout;)Landroid/view/View;
    .registers 6
    .param p0, "parent"    # Landroid/support/design/widget/CoordinatorLayout;

    .line 45
    invoke-virtual {p0}, Landroid/support/design/widget/CoordinatorLayout;->getChildCount()I

    move-result v1

    .line 46
    .local v1, "childCount":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_5
    if-ge v2, v1, :cond_1e

    .line 47
    invoke-virtual {p0, v2}, Landroid/support/design/widget/CoordinatorLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 48
    .local v3, "view":Landroid/view/View;
    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Landroid/support/design/widget/CoordinatorLayout$LayoutParams;

    .line 49
    .local v4, "layoutParams":Landroid/support/design/widget/CoordinatorLayout$LayoutParams;
    invoke-virtual {v4}, Landroid/support/design/widget/CoordinatorLayout$LayoutParams;->getBehavior()Landroid/support/design/widget/CoordinatorLayout$Behavior;

    move-result-object v0

    instance-of v0, v0, Landroid/support/design/widget/AppBarLayout$ScrollingViewBehavior;

    if-eqz v0, :cond_1b

    .line 50
    return-object v3

    .line 46
    .end local v3    # "view":Landroid/view/View;
    .end local v4    # "layoutParams":Landroid/support/design/widget/CoordinatorLayout$LayoutParams;
    :cond_1b
    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    .line 52
    .end local v2    # "i":I
    :cond_1e
    const/4 v0, 0x0

    return-object v0
.end method

.method private static setPaddingBottom(Landroid/view/View;I)V
    .registers 6
    .param p0, "view"    # Landroid/view/View;
    .param p1, "size"    # I

    .line 77
    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    .line 78
    .local v3, "layoutParams":Landroid/view/ViewGroup$LayoutParams;
    invoke-virtual {p0, v3}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 79
    invoke-virtual {p0}, Landroid/view/View;->getPaddingLeft()I

    move-result v0

    invoke-virtual {p0}, Landroid/view/View;->getPaddingTop()I

    move-result v1

    invoke-virtual {p0}, Landroid/view/View;->getPaddingRight()I

    move-result v2

    invoke-virtual {p0, v0, v1, v2, p1}, Landroid/view/View;->setPadding(IIII)V

    .line 80
    return-void
.end method


# virtual methods
.method public layoutDependsOn(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;Landroid/view/View;)Z
    .registers 5
    .param p1, "parent"    # Landroid/support/design/widget/CoordinatorLayout;
    .param p2, "child"    # Landroid/view/View;
    .param p3, "dependency"    # Landroid/view/View;

    .line 39
    iget-object v0, p0, Lcom/itau/empresas/ui/HideOnScrollBehavior;->scrollingView:Landroid/view/View;

    if-nez v0, :cond_a

    .line 40
    invoke-static {p1}, Lcom/itau/empresas/ui/HideOnScrollBehavior;->findScrollingView(Landroid/support/design/widget/CoordinatorLayout;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/ui/HideOnScrollBehavior;->scrollingView:Landroid/view/View;

    .line 41
    :cond_a
    instance-of v0, p3, Landroid/support/design/widget/AppBarLayout;

    if-nez v0, :cond_12

    instance-of v0, p3, Landroid/support/v7/widget/Toolbar;

    if-eqz v0, :cond_14

    :cond_12
    const/4 v0, 0x1

    goto :goto_15

    :cond_14
    const/4 v0, 0x0

    :goto_15
    return v0
.end method

.method public onDependentViewChanged(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;Landroid/view/View;)Z
    .registers 10
    .param p1, "parent"    # Landroid/support/design/widget/CoordinatorLayout;
    .param p2, "child"    # Landroid/view/View;
    .param p3, "dependency"    # Landroid/view/View;

    .line 58
    iget v0, p0, Lcom/itau/empresas/ui/HideOnScrollBehavior;->dependencyHeight:I

    if-nez v0, :cond_a

    .line 59
    invoke-virtual {p3}, Landroid/view/View;->getHeight()I

    move-result v0

    iput v0, p0, Lcom/itau/empresas/ui/HideOnScrollBehavior;->dependencyHeight:I

    .line 61
    :cond_a
    invoke-virtual {p3}, Landroid/view/View;->getY()F

    move-result v0

    const/high16 v1, 0x42c80000    # 100.0f

    mul-float/2addr v0, v1

    iget v1, p0, Lcom/itau/empresas/ui/HideOnScrollBehavior;->dependencyHeight:I

    int-to-float v1, v1

    div-float v2, v0, v1

    .line 62
    .local v2, "pct":F
    invoke-virtual {p2}, Landroid/view/View;->getHeight()I

    move-result v3

    .line 63
    .local v3, "childHeight":I
    int-to-float v0, v3

    mul-float/2addr v0, v2

    const/high16 v1, 0x42c80000    # 100.0f

    div-float/2addr v0, v1

    const/high16 v1, -0x40800000    # -1.0f

    mul-float v4, v0, v1

    .line 64
    .local v4, "translationY":F
    invoke-virtual {p2, v4}, Landroid/view/View;->setTranslationY(F)V

    .line 66
    iget-object v0, p0, Lcom/itau/empresas/ui/HideOnScrollBehavior;->scrollingView:Landroid/view/View;

    if-eqz v0, :cond_43

    .line 67
    iget v0, p0, Lcom/itau/empresas/ui/HideOnScrollBehavior;->scrollingViewPaddingBottom:I

    const/high16 v1, -0x80000000

    if-ne v0, v1, :cond_38

    .line 68
    iget-object v0, p0, Lcom/itau/empresas/ui/HideOnScrollBehavior;->scrollingView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getPaddingBottom()I

    move-result v0

    iput v0, p0, Lcom/itau/empresas/ui/HideOnScrollBehavior;->scrollingViewPaddingBottom:I

    .line 69
    :cond_38
    iget v0, p0, Lcom/itau/empresas/ui/HideOnScrollBehavior;->scrollingViewPaddingBottom:I

    add-int/2addr v0, v3

    int-to-float v0, v0

    sub-float/2addr v0, v4

    float-to-int v5, v0

    .line 70
    .local v5, "size":I
    iget-object v0, p0, Lcom/itau/empresas/ui/HideOnScrollBehavior;->scrollingView:Landroid/view/View;

    invoke-static {v0, v5}, Lcom/itau/empresas/ui/HideOnScrollBehavior;->setPaddingBottom(Landroid/view/View;I)V

    .line 73
    .end local v5    # "size":I
    :cond_43
    const/4 v0, 0x1

    return v0
.end method

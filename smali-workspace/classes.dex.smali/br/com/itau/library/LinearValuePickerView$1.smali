.class Lbr/com/itau/library/LinearValuePickerView$1;
.super Ljava/lang/Object;
.source "LinearValuePickerView.java"

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lbr/com/itau/library/LinearValuePickerView;->animateTextColorChange(Landroid/widget/TextView;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lbr/com/itau/library/LinearValuePickerView;

.field final synthetic val$text:Landroid/widget/TextView;


# direct methods
.method constructor <init>(Lbr/com/itau/library/LinearValuePickerView;Landroid/widget/TextView;)V
    .registers 3
    .param p1, "this$0"    # Lbr/com/itau/library/LinearValuePickerView;

    .line 230
    iput-object p1, p0, Lbr/com/itau/library/LinearValuePickerView$1;->this$0:Lbr/com/itau/library/LinearValuePickerView;

    iput-object p2, p0, Lbr/com/itau/library/LinearValuePickerView$1;->val$text:Landroid/widget/TextView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .registers 4
    .param p1, "animator"    # Landroid/animation/ValueAnimator;

    .line 233
    iget-object v0, p0, Lbr/com/itau/library/LinearValuePickerView$1;->val$text:Landroid/widget/TextView;

    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 234
    return-void
.end method

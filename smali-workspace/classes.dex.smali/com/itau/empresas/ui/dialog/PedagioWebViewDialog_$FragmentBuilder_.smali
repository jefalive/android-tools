.class public Lcom/itau/empresas/ui/dialog/PedagioWebViewDialog_$FragmentBuilder_;
.super Lorg/androidannotations/api/builder/FragmentBuilder;
.source "PedagioWebViewDialog_.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/itau/empresas/ui/dialog/PedagioWebViewDialog_;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "FragmentBuilder_"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/androidannotations/api/builder/FragmentBuilder<Lcom/itau/empresas/ui/dialog/PedagioWebViewDialog_$FragmentBuilder_;Lcom/itau/empresas/ui/dialog/PedagioWebViewDialog;>;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 105
    invoke-direct {p0}, Lorg/androidannotations/api/builder/FragmentBuilder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/itau/empresas/ui/dialog/PedagioWebViewDialog;
    .registers 3

    .line 111
    new-instance v1, Lcom/itau/empresas/ui/dialog/PedagioWebViewDialog_;

    invoke-direct {v1}, Lcom/itau/empresas/ui/dialog/PedagioWebViewDialog_;-><init>()V

    .line 112
    .local v1, "fragment_":Lcom/itau/empresas/ui/dialog/PedagioWebViewDialog_;
    iget-object v0, p0, Lcom/itau/empresas/ui/dialog/PedagioWebViewDialog_$FragmentBuilder_;->args:Landroid/os/Bundle;

    invoke-virtual {v1, v0}, Lcom/itau/empresas/ui/dialog/PedagioWebViewDialog_;->setArguments(Landroid/os/Bundle;)V

    .line 113
    return-object v1
.end method

.method public categoriaAnalytics(Ljava/lang/String;)Lcom/itau/empresas/ui/dialog/PedagioWebViewDialog_$FragmentBuilder_;
    .registers 4
    .param p1, "categoriaAnalytics"    # Ljava/lang/String;

    .line 123
    iget-object v0, p0, Lcom/itau/empresas/ui/dialog/PedagioWebViewDialog_$FragmentBuilder_;->args:Landroid/os/Bundle;

    const-string v1, "categoriaAnalytics"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 124
    return-object p0
.end method

.method public chaves(Ljava/util/ArrayList;)Lcom/itau/empresas/ui/dialog/PedagioWebViewDialog_$FragmentBuilder_;
    .registers 4
    .param p1, "chaves"    # Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/ArrayList<Ljava/lang/String;>;)Lcom/itau/empresas/ui/dialog/PedagioWebViewDialog_$FragmentBuilder_;"
        }
    .end annotation

    .line 145
    iget-object v0, p0, Lcom/itau/empresas/ui/dialog/PedagioWebViewDialog_$FragmentBuilder_;->args:Landroid/os/Bundle;

    const-string v1, "chaves"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 146
    return-object p0
.end method

.method public labels(Ljava/util/ArrayList;)Lcom/itau/empresas/ui/dialog/PedagioWebViewDialog_$FragmentBuilder_;
    .registers 4
    .param p1, "labels"    # Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/ArrayList<Ljava/lang/String;>;)Lcom/itau/empresas/ui/dialog/PedagioWebViewDialog_$FragmentBuilder_;"
        }
    .end annotation

    .line 134
    iget-object v0, p0, Lcom/itau/empresas/ui/dialog/PedagioWebViewDialog_$FragmentBuilder_;->args:Landroid/os/Bundle;

    const-string v1, "labels"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 135
    return-object p0
.end method

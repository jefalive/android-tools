.class public Lcom/itau/empresas/api/model/PerfilOperadorVO;
.super Ljava/lang/Object;
.source "PerfilOperadorVO.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private cnpj:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "cnpj"
    .end annotation
.end field

.field private contasOperador:Ljava/util/List;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "contas_operador"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Lcom/itau/empresas/api/model/ContaOperadorVO;>;"
        }
    .end annotation
.end field

.field private diasTrocaSenha:Ljava/lang/Integer;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "dias_troca_senha"
    .end annotation
.end field

.field private finalTokenChaveiro:Ljava/lang/String;

.field private indicadorBloqueioPlus:Z
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "indicador_bloqueio_plus"
    .end annotation
.end field

.field private indicadorBloqueioSenha:Z
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "indicador_bloqueio_senha"
    .end annotation
.end field

.field private mascaraTelefoneSms:Ljava/lang/String;

.field private nomeEmpresa:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "nome_empresa"
    .end annotation
.end field

.field private primeiroAcesso:Z
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "primeiro_acesso"
    .end annotation
.end field

.field private quantidadeTentativasAcesso:Ljava/lang/Integer;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "quantidade_tentativa_acesso"
    .end annotation
.end field

.field private senhaExpirada:Z
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "senha_expirada"
    .end annotation
.end field

.field private statusSenha:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "status_senha"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 2

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/api/model/PerfilOperadorVO;->contasOperador:Ljava/util/List;

    .line 43
    const-string v0, "000"

    iput-object v0, p0, Lcom/itau/empresas/api/model/PerfilOperadorVO;->finalTokenChaveiro:Ljava/lang/String;

    .line 44
    const-string v0, "0000"

    iput-object v0, p0, Lcom/itau/empresas/api/model/PerfilOperadorVO;->mascaraTelefoneSms:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getContasOperador()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Lcom/itau/empresas/api/model/ContaOperadorVO;>;"
        }
    .end annotation

    .line 80
    iget-object v0, p0, Lcom/itau/empresas/api/model/PerfilOperadorVO;->contasOperador:Ljava/util/List;

    return-object v0
.end method

.method public getFinalTokenChaveiro()Ljava/lang/String;
    .registers 2

    .line 48
    iget-object v0, p0, Lcom/itau/empresas/api/model/PerfilOperadorVO;->finalTokenChaveiro:Ljava/lang/String;

    return-object v0
.end method

.method public getMascaraTelefoneSms()Ljava/lang/String;
    .registers 2

    .line 56
    iget-object v0, p0, Lcom/itau/empresas/api/model/PerfilOperadorVO;->mascaraTelefoneSms:Ljava/lang/String;

    return-object v0
.end method

.method public getNomeEmpresa()Ljava/lang/String;
    .registers 2

    .line 92
    iget-object v0, p0, Lcom/itau/empresas/api/model/PerfilOperadorVO;->nomeEmpresa:Ljava/lang/String;

    return-object v0
.end method

.method public isPrimeiroAcesso()Z
    .registers 2

    .line 68
    iget-boolean v0, p0, Lcom/itau/empresas/api/model/PerfilOperadorVO;->primeiroAcesso:Z

    return v0
.end method

.method public isSenhaExpirada()Z
    .registers 2

    .line 64
    iget-boolean v0, p0, Lcom/itau/empresas/api/model/PerfilOperadorVO;->senhaExpirada:Z

    return v0
.end method

.method public setContasOperador(Ljava/util/List;)V
    .registers 4
    .param p1, "contasOperador"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<Lcom/itau/empresas/api/model/ContaOperadorVO;>;)V"
        }
    .end annotation

    .line 84
    iget-object v0, p0, Lcom/itau/empresas/api/model/PerfilOperadorVO;->contasOperador:Ljava/util/List;

    if-eqz v0, :cond_13

    iget-object v0, p0, Lcom/itau/empresas/api/model/PerfilOperadorVO;->contasOperador:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_13

    .line 85
    iget-object v0, p0, Lcom/itau/empresas/api/model/PerfilOperadorVO;->contasOperador:Ljava/util/List;

    iget-object v1, p0, Lcom/itau/empresas/api/model/PerfilOperadorVO;->contasOperador:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->removeAll(Ljava/util/Collection;)Z

    .line 88
    :cond_13
    iput-object p1, p0, Lcom/itau/empresas/api/model/PerfilOperadorVO;->contasOperador:Ljava/util/List;

    .line 89
    return-void
.end method

.method public setNomeEmpresa(Ljava/lang/String;)V
    .registers 2
    .param p1, "nomeEmpresa"    # Ljava/lang/String;

    .line 96
    iput-object p1, p0, Lcom/itau/empresas/api/model/PerfilOperadorVO;->nomeEmpresa:Ljava/lang/String;

    .line 97
    return-void
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    .line 101
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "PerfilOperadorVO{senhaExpirada="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/itau/empresas/api/model/PerfilOperadorVO;->senhaExpirada:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", diasTrocaSenha="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/api/model/PerfilOperadorVO;->diasTrocaSenha:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", quantidadeTentativasAcesso="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/api/model/PerfilOperadorVO;->quantidadeTentativasAcesso:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", statusSenha=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/api/model/PerfilOperadorVO;->statusSenha:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", indicadorBloqueioSenha="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/itau/empresas/api/model/PerfilOperadorVO;->indicadorBloqueioSenha:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", indicadorBloqueioPlus="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/itau/empresas/api/model/PerfilOperadorVO;->indicadorBloqueioPlus:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", nomeEmpresa=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/api/model/PerfilOperadorVO;->nomeEmpresa:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", cnpj=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/api/model/PerfilOperadorVO;->cnpj:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", contasOperador="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/api/model/PerfilOperadorVO;->contasOperador:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", finalTokenChaveiro=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/api/model/PerfilOperadorVO;->finalTokenChaveiro:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mascaraTelefoneSms=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/api/model/PerfilOperadorVO;->mascaraTelefoneSms:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

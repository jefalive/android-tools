.class public Lcom/itau/empresas/api/model/AutorizanteBaseVO;
.super Ljava/lang/Object;
.source "AutorizanteBaseVO.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private autorizante:Z
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "autorizante"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public isAutorizante()Z
    .registers 2

    .line 14
    iget-boolean v0, p0, Lcom/itau/empresas/api/model/AutorizanteBaseVO;->autorizante:Z

    return v0
.end method

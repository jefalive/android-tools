.class public Lcom/itau/empresas/feature/recarga/contatos/model/RecargaDetalheOperadoraVO;
.super Ljava/lang/Object;
.source "RecargaDetalheOperadoraVO.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field moeda:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "moeda"
    .end annotation
.end field

.field nomeEmpresa:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "nome_empresa"
    .end annotation
.end field

.field recargas:Ljava/util/List;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "recargas"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Lcom/itau/empresas/feature/recarga/efetivacao/model/ValorRecargaVO;>;"
        }
    .end annotation
.end field

.field valorMaximo:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "valor_maximo"
    .end annotation
.end field

.field valorMinimo:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "valor_minimo"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getRecargas()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Lcom/itau/empresas/feature/recarga/efetivacao/model/ValorRecargaVO;>;"
        }
    .end annotation

    .line 31
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/model/RecargaDetalheOperadoraVO;->recargas:Ljava/util/List;

    return-object v0
.end method

.method public getValorMaximo()Ljava/lang/String;
    .registers 2

    .line 35
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/model/RecargaDetalheOperadoraVO;->valorMaximo:Ljava/lang/String;

    return-object v0
.end method

.method public getValorMinimo()Ljava/lang/String;
    .registers 2

    .line 40
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/model/RecargaDetalheOperadoraVO;->valorMinimo:Ljava/lang/String;

    return-object v0
.end method

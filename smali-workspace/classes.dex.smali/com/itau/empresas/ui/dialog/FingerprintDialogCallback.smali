.class public Lcom/itau/empresas/ui/dialog/FingerprintDialogCallback;
.super Ljava/lang/Object;
.source "FingerprintDialogCallback.java"

# interfaces
.implements Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintCallback;


# instance fields
.field private final callback:Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintCallback;

.field private final dialog:Landroid/support/v7/app/AlertDialog;

.field private final view:Lcom/itau/empresas/ui/view/FingerprintValidacaoView;


# direct methods
.method constructor <init>(Lcom/itau/empresas/ui/view/FingerprintValidacaoView;Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintCallback;Landroid/support/v7/app/AlertDialog;)V
    .registers 4
    .param p1, "view"    # Lcom/itau/empresas/ui/view/FingerprintValidacaoView;
    .param p2, "callback"    # Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintCallback;
    .param p3, "dialog"    # Landroid/support/v7/app/AlertDialog;

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object p1, p0, Lcom/itau/empresas/ui/dialog/FingerprintDialogCallback;->view:Lcom/itau/empresas/ui/view/FingerprintValidacaoView;

    .line 30
    iput-object p2, p0, Lcom/itau/empresas/ui/dialog/FingerprintDialogCallback;->callback:Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintCallback;

    .line 31
    iput-object p3, p0, Lcom/itau/empresas/ui/dialog/FingerprintDialogCallback;->dialog:Landroid/support/v7/app/AlertDialog;

    .line 32
    return-void
.end method

.method static synthetic access$000(Lcom/itau/empresas/ui/dialog/FingerprintDialogCallback;)Landroid/support/v7/app/AlertDialog;
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/ui/dialog/FingerprintDialogCallback;

    .line 17
    iget-object v0, p0, Lcom/itau/empresas/ui/dialog/FingerprintDialogCallback;->dialog:Landroid/support/v7/app/AlertDialog;

    return-object v0
.end method

.method static synthetic access$100(Lcom/itau/empresas/ui/dialog/FingerprintDialogCallback;)Lcom/itau/empresas/ui/view/FingerprintValidacaoView;
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/ui/dialog/FingerprintDialogCallback;

    .line 17
    iget-object v0, p0, Lcom/itau/empresas/ui/dialog/FingerprintDialogCallback;->view:Lcom/itau/empresas/ui/view/FingerprintValidacaoView;

    return-object v0
.end method

.method private fechaDialog()V
    .registers 5

    .line 49
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v1, Lcom/itau/empresas/ui/dialog/FingerprintDialogCallback$1;

    invoke-direct {v1, p0}, Lcom/itau/empresas/ui/dialog/FingerprintDialogCallback$1;-><init>(Lcom/itau/empresas/ui/dialog/FingerprintDialogCallback;)V

    const-wide/16 v2, 0xa

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 62
    return-void
.end method

.method private mudaTituloDialog()V
    .registers 5

    .line 65
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v1, Lcom/itau/empresas/ui/dialog/FingerprintDialogCallback$2;

    invoke-direct {v1, p0}, Lcom/itau/empresas/ui/dialog/FingerprintDialogCallback$2;-><init>(Lcom/itau/empresas/ui/dialog/FingerprintDialogCallback;)V

    const-wide/16 v2, 0x7d0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 71
    return-void
.end method

.method private tentaDispensarDialogo()V
    .registers 3

    .line 82
    :try_start_0
    iget-object v0, p0, Lcom/itau/empresas/ui/dialog/FingerprintDialogCallback;->dialog:Landroid/support/v7/app/AlertDialog;

    invoke-virtual {v0}, Landroid/support/v7/app/AlertDialog;->dismiss()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_5} :catch_6

    .line 85
    goto :goto_a

    .line 83
    :catch_6
    move-exception v1

    .line 84
    .local v1, "e":Ljava/lang/Exception;
    invoke-static {v1}, Lcom/crashlytics/android/Crashlytics;->logException(Ljava/lang/Throwable;)V

    .line 86
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_a
    return-void
.end method


# virtual methods
.method public onError(Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintException;)V
    .registers 3
    .param p1, "exception"    # Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintException;

    .line 75
    iget-object v0, p0, Lcom/itau/empresas/ui/dialog/FingerprintDialogCallback;->dialog:Landroid/support/v7/app/AlertDialog;

    invoke-virtual {v0}, Landroid/support/v7/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 76
    invoke-direct {p0}, Lcom/itau/empresas/ui/dialog/FingerprintDialogCallback;->tentaDispensarDialogo()V

    .line 77
    :cond_b
    iget-object v0, p0, Lcom/itau/empresas/ui/dialog/FingerprintDialogCallback;->callback:Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintCallback;

    invoke-interface {v0, p1}, Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintCallback;->onError(Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintException;)V

    .line 78
    return-void
.end method

.method public onResult(Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintResult;)V
    .registers 4
    .param p1, "result"    # Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintResult;

    .line 36
    sget-object v0, Lcom/itau/empresas/ui/dialog/FingerprintDialogCallback$3;->$SwitchMap$br$com$itau$sdk$android$fingerprintcore$ItauFingerprintResult:[I

    invoke-virtual {p1}, Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintResult;->ordinal()I

    move-result v1

    aget v0, v0, v1

    sparse-switch v0, :sswitch_data_1e

    goto :goto_1d

    .line 38
    :sswitch_c
    iget-object v0, p0, Lcom/itau/empresas/ui/dialog/FingerprintDialogCallback;->view:Lcom/itau/empresas/ui/view/FingerprintValidacaoView;

    invoke-virtual {v0}, Lcom/itau/empresas/ui/view/FingerprintValidacaoView;->setFalhou()V

    .line 39
    invoke-direct {p0}, Lcom/itau/empresas/ui/dialog/FingerprintDialogCallback;->mudaTituloDialog()V

    .line 40
    goto :goto_1d

    .line 42
    :sswitch_15
    iget-object v0, p0, Lcom/itau/empresas/ui/dialog/FingerprintDialogCallback;->callback:Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintCallback;

    invoke-interface {v0, p1}, Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintCallback;->onResult(Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintResult;)V

    .line 43
    invoke-direct {p0}, Lcom/itau/empresas/ui/dialog/FingerprintDialogCallback;->fechaDialog()V

    .line 46
    :goto_1d
    return-void

    :sswitch_data_1e
    .sparse-switch
        0x1 -> :sswitch_c
        0x2 -> :sswitch_15
    .end sparse-switch
.end method

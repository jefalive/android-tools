.class public Lnet/hockeyapp/android/objects/Feedback;
.super Ljava/lang/Object;
.source "Feedback.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = 0x23f22471d131d6e8L


# instance fields
.field private createdAt:Ljava/lang/String;

.field private email:Ljava/lang/String;

.field private id:I

.field private messages:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<Lnet/hockeyapp/android/objects/FeedbackMessage;>;"
        }
    .end annotation
.end field

.field private name:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getMessages()Ljava/util/ArrayList;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/ArrayList<Lnet/hockeyapp/android/objects/FeedbackMessage;>;"
        }
    .end annotation

    .line 82
    iget-object v0, p0, Lnet/hockeyapp/android/objects/Feedback;->messages:Ljava/util/ArrayList;

    return-object v0
.end method

.method public setCreatedAt(Ljava/lang/String;)V
    .registers 2
    .param p1, "createdAt"    # Ljava/lang/String;

    .line 78
    iput-object p1, p0, Lnet/hockeyapp/android/objects/Feedback;->createdAt:Ljava/lang/String;

    .line 79
    return-void
.end method

.method public setEmail(Ljava/lang/String;)V
    .registers 2
    .param p1, "email"    # Ljava/lang/String;

    .line 62
    iput-object p1, p0, Lnet/hockeyapp/android/objects/Feedback;->email:Ljava/lang/String;

    .line 63
    return-void
.end method

.method public setId(I)V
    .registers 2
    .param p1, "id"    # I

    .line 70
    iput p1, p0, Lnet/hockeyapp/android/objects/Feedback;->id:I

    .line 71
    return-void
.end method

.method public setMessages(Ljava/util/ArrayList;)V
    .registers 2
    .param p1, "messages"    # Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/ArrayList<Lnet/hockeyapp/android/objects/FeedbackMessage;>;)V"
        }
    .end annotation

    .line 86
    iput-object p1, p0, Lnet/hockeyapp/android/objects/Feedback;->messages:Ljava/util/ArrayList;

    .line 87
    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .registers 2
    .param p1, "name"    # Ljava/lang/String;

    .line 54
    iput-object p1, p0, Lnet/hockeyapp/android/objects/Feedback;->name:Ljava/lang/String;

    .line 55
    return-void
.end method

.class public Lcom/itau/empresas/ui/util/validacao/ValidaEditText;
.super Ljava/lang/Object;
.source "ValidaEditText.java"


# instance fields
.field private editTextListInvalidos:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Landroid/widget/TextView;>;"
        }
    .end annotation
.end field

.field private listener:Lcom/itau/empresas/ui/util/listener/ValidacaoEditTextListener;


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private notificaListener()V
    .registers 3

    .line 54
    iget-object v0, p0, Lcom/itau/empresas/ui/util/validacao/ValidaEditText;->editTextListInvalidos:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_e

    .line 55
    iget-object v0, p0, Lcom/itau/empresas/ui/util/validacao/ValidaEditText;->listener:Lcom/itau/empresas/ui/util/listener/ValidacaoEditTextListener;

    invoke-interface {v0}, Lcom/itau/empresas/ui/util/listener/ValidacaoEditTextListener;->campoValido()V

    goto :goto_15

    .line 57
    :cond_e
    iget-object v0, p0, Lcom/itau/empresas/ui/util/validacao/ValidaEditText;->listener:Lcom/itau/empresas/ui/util/listener/ValidacaoEditTextListener;

    iget-object v1, p0, Lcom/itau/empresas/ui/util/validacao/ValidaEditText;->editTextListInvalidos:Ljava/util/List;

    invoke-interface {v0, v1}, Lcom/itau/empresas/ui/util/listener/ValidacaoEditTextListener;->campoInvalido(Ljava/util/List;)V

    .line 59
    :goto_15
    return-void
.end method

.method private notificaListenerInvalido(Landroid/widget/EditText;)V
    .registers 3
    .param p1, "editText"    # Landroid/widget/EditText;

    .line 46
    iget-object v0, p0, Lcom/itau/empresas/ui/util/validacao/ValidaEditText;->listener:Lcom/itau/empresas/ui/util/listener/ValidacaoEditTextListener;

    invoke-interface {v0, p1}, Lcom/itau/empresas/ui/util/listener/ValidacaoEditTextListener;->campoInvalido(Landroid/widget/EditText;)V

    .line 47
    return-void
.end method

.method private notificaListenerValido(Landroid/widget/EditText;)V
    .registers 3
    .param p1, "editText"    # Landroid/widget/EditText;

    .line 50
    iget-object v0, p0, Lcom/itau/empresas/ui/util/validacao/ValidaEditText;->listener:Lcom/itau/empresas/ui/util/listener/ValidacaoEditTextListener;

    invoke-interface {v0, p1}, Lcom/itau/empresas/ui/util/listener/ValidacaoEditTextListener;->campoValido(Landroid/widget/EditText;)V

    .line 51
    return-void
.end method

.method private textoMonetarioInvalido(Ljava/lang/String;)Z
    .registers 4
    .param p1, "valor"    # Ljava/lang/String;

    .line 62
    const-string v0, "\\s+"

    const-string v1, ""

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "R$0,00"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public setListener(Lcom/itau/empresas/ui/util/listener/ValidacaoEditTextListener;)V
    .registers 2
    .param p1, "listener"    # Lcom/itau/empresas/ui/util/listener/ValidacaoEditTextListener;

    .line 66
    iput-object p1, p0, Lcom/itau/empresas/ui/util/validacao/ValidaEditText;->listener:Lcom/itau/empresas/ui/util/listener/ValidacaoEditTextListener;

    .line 67
    return-void
.end method

.method public validaTextoVazio(Landroid/widget/EditText;)V
    .registers 3
    .param p1, "editText"    # Landroid/widget/EditText;

    .line 37
    invoke-virtual {p1}, Landroid/widget/EditText;->getEditableText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1c

    .line 38
    invoke-virtual {p1}, Landroid/widget/EditText;->getEditableText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 37
    invoke-direct {p0, v0}, Lcom/itau/empresas/ui/util/validacao/ValidaEditText;->textoMonetarioInvalido(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_20

    .line 39
    :cond_1c
    invoke-direct {p0, p1}, Lcom/itau/empresas/ui/util/validacao/ValidaEditText;->notificaListenerInvalido(Landroid/widget/EditText;)V

    goto :goto_23

    .line 41
    :cond_20
    invoke-direct {p0, p1}, Lcom/itau/empresas/ui/util/validacao/ValidaEditText;->notificaListenerValido(Landroid/widget/EditText;)V

    .line 43
    :goto_23
    return-void
.end method

.method public validaTextoVazio(Ljava/util/List;)V
    .registers 5
    .param p1, "editTextList"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<Landroid/widget/TextView;>;)V"
        }
    .end annotation

    .line 22
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/ui/util/validacao/ValidaEditText;->editTextListInvalidos:Ljava/util/List;

    .line 23
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_b
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4d

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Landroid/widget/TextView;

    .line 24
    .local v2, "editText":Landroid/widget/TextView;
    invoke-virtual {v2}, Landroid/widget/TextView;->getEditableText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_34

    .line 25
    invoke-virtual {v2}, Landroid/widget/TextView;->getEditableText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 24
    invoke-direct {p0, v0}, Lcom/itau/empresas/ui/util/validacao/ValidaEditText;->textoMonetarioInvalido(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_39

    .line 26
    :cond_34
    iget-object v0, p0, Lcom/itau/empresas/ui/util/validacao/ValidaEditText;->editTextListInvalidos:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 28
    :cond_39
    invoke-virtual {v2}, Landroid/widget/TextView;->getEditableText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/itau/empresas/ui/util/validacao/ValidaEditText;->textoMonetarioInvalido(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4c

    .line 29
    iget-object v0, p0, Lcom/itau/empresas/ui/util/validacao/ValidaEditText;->editTextListInvalidos:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 31
    .end local v2    # "editText":Landroid/widget/TextView;
    :cond_4c
    goto :goto_b

    .line 32
    :cond_4d
    invoke-direct {p0}, Lcom/itau/empresas/ui/util/validacao/ValidaEditText;->notificaListener()V

    .line 33
    return-void
.end method

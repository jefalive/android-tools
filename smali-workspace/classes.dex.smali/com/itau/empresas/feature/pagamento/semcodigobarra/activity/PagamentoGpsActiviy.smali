.class public Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;
.super Lcom/itau/empresas/ui/activity/BaseActivity;
.source "PagamentoGpsActiviy.java"

# interfaces
.implements Lcom/itau/empresas/ui/view/MesEAnoPickerDialog$MesEAnoPickerDialogListener;
.implements Lcom/itau/empresas/ui/util/listener/AtualizaValorTotalGps;
.implements Lcom/itau/empresas/ui/util/listener/ValidacaoEditTextListener;


# instance fields
.field private alcadaAutorizante:Z

.field app:Lcom/itau/empresas/CustomApplication;

.field private atualizaValorPagamentoGPS:Lcom/itau/empresas/ui/util/listener/AtualizaValorPagamentoGPS;

.field private autorizar:Z

.field campoAgenciaConta:Landroid/widget/TextView;

.field campoAtualizacaoMonetaria:Landroid/widget/EditText;

.field campoCnpjPagador:Landroid/widget/TextView;

.field campoCodigoPagamento:Landroid/widget/EditText;

.field campoCompetencia:Landroid/widget/TextView;

.field campoDataPagamento:Landroid/widget/EditText;

.field campoHorarioLimiteFinal:Landroid/widget/TextView;

.field campoHorarioLimiteInicial:Landroid/widget/TextView;

.field campoIdentificacaoComprovante:Landroid/widget/EditText;

.field campoIdentificador:Landroid/widget/EditText;

.field campoNomeContribuinte:Lcom/itau/empresas/ui/view/EditTextCustomError;

.field campoNomeDaEmpresa:Landroid/widget/TextView;

.field campoReferenciaEmpresa:Landroid/widget/EditText;

.field campoValorInss:Landroid/widget/EditText;

.field campoValorOutrasEntidades:Landroid/widget/EditText;

.field campoValorTotalGps:Landroid/widget/EditText;

.field private cnpjVOList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/CnpjVO;>;"
        }
    .end annotation
.end field

.field private contaOperadorVO:Lcom/itau/empresas/api/model/ContaOperadorVO;

.field controller:Lcom/itau/empresas/controller/PagamentoGPSController;

.field private gpsRequestVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/GpsRequestVO;

.field iconeAbreListaCnpj:Landroid/widget/TextView;

.field private listaEditTextValidacao:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Landroid/widget/TextView;>;"
        }
    .end annotation
.end field

.field private onFocusCampoCompetencia:Landroid/view/View$OnFocusChangeListener;

.field rlAutorizacaoGps:Landroid/widget/RelativeLayout;

.field rlDadosCnpjPagador:Landroid/widget/RelativeLayout;

.field rlHorarioLimite:Landroid/widget/RelativeLayout;

.field rlInclusaoGps:Landroid/widget/RelativeLayout;

.field root:Landroid/widget/LinearLayout;

.field scrollGps:Landroid/widget/ScrollView;

.field toolBar:Landroid/support/v7/widget/Toolbar;


# direct methods
.method public constructor <init>()V
    .registers 2

    .line 76
    invoke-direct {p0}, Lcom/itau/empresas/ui/activity/BaseActivity;-><init>()V

    .line 122
    new-instance v0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy$1;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy$1;-><init>(Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;)V

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->onFocusCampoCompetencia:Landroid/view/View$OnFocusChangeListener;

    return-void
.end method

.method private abreDatePickerCompetencia()V
    .registers 3

    .line 411
    new-instance v1, Lcom/itau/empresas/ui/view/MesEAnoPickerDialog;

    invoke-direct {v1}, Lcom/itau/empresas/ui/view/MesEAnoPickerDialog;-><init>()V

    .line 412
    .local v1, "mesEAnoPickerDialog":Lcom/itau/empresas/ui/view/MesEAnoPickerDialog;
    invoke-virtual {v1, p0}, Lcom/itau/empresas/ui/view/MesEAnoPickerDialog;->setListener(Lcom/itau/empresas/ui/view/MesEAnoPickerDialog$MesEAnoPickerDialogListener;)V

    .line 413
    const/16 v0, 0xd

    invoke-virtual {v1, v0}, Lcom/itau/empresas/ui/view/MesEAnoPickerDialog;->setNumeroMaximoMeses(I)V

    .line 414
    invoke-virtual {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/itau/empresas/ui/view/MesEAnoPickerDialog;->show(Landroid/support/v4/app/FragmentManager;)V

    .line 415
    return-void
.end method

.method private abreDatePickerPagamento()V
    .registers 3

    .line 418
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->campoDataPagamento:Landroid/widget/EditText;

    invoke-direct {p0, v0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->constroiDatePickerDialog(Landroid/widget/TextView;)Lcom/wdullaer/materialdatetimepicker/date/DatePickerDialog;

    move-result-object v1

    .line 419
    .local v1, "datePickerDialog":Lcom/wdullaer/materialdatetimepicker/date/DatePickerDialog;
    new-instance v0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy$5;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy$5;-><init>(Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;)V

    invoke-virtual {v1, v0}, Lcom/wdullaer/materialdatetimepicker/date/DatePickerDialog;->setOnDateSetListener(Lcom/wdullaer/materialdatetimepicker/date/DatePickerDialog$OnDateSetListener;)V

    .line 426
    return-void
.end method

.method private abreTelaConfirmacao()V
    .registers 3

    .line 529
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->autorizar:Z

    .line 530
    invoke-static {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ConfirmarAutorizacaoTributosActivity;->intent(Landroid/content/Context;)Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ConfirmarAutorizacaoTributosActivity_$IntentBuilder_;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->gpsRequestVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/GpsRequestVO;

    .line 531
    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ConfirmarAutorizacaoTributosActivity_$IntentBuilder_;->gpsRequestVO(Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/GpsRequestVO;)Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ConfirmarAutorizacaoTributosActivity_$IntentBuilder_;

    move-result-object v0

    .line 532
    invoke-direct {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->buildDadosConfirmacao()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ConfirmarAutorizacaoTributosActivity_$IntentBuilder_;->dadosConfirmacaoTributos(Ljava/util/ArrayList;)Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ConfirmarAutorizacaoTributosActivity_$IntentBuilder_;

    move-result-object v0

    .line 533
    invoke-virtual {v0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ConfirmarAutorizacaoTributosActivity_$IntentBuilder_;->start()Lorg/androidannotations/api/builder/PostActivityStarter;

    .line 534
    return-void
.end method

.method private abreTelaDeInclusaoSucesso(Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/InclusaoTributoResponseVO;)V
    .registers 5
    .param p1, "inclusaoTributoResponseVO"    # Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/InclusaoTributoResponseVO;

    .line 588
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->campoIdentificacaoComprovante:Landroid/widget/EditText;

    .line 589
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 588
    invoke-virtual {p1, v0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/InclusaoTributoResponseVO;->setIdentificadorPagamento(Ljava/lang/String;)V

    .line 590
    new-instance v0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo$Builder;

    invoke-direct {v0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo$Builder;-><init>()V

    .line 592
    invoke-virtual {v0, p1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo$Builder;->comInclusaoTributos(Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/InclusaoTributoResponseVO;)Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo$Builder;

    move-result-object v0

    .line 593
    const v1, 0x7f07049f

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo$Builder;->comMensagem(Ljava/lang/String;)Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo$Builder;

    move-result-object v0

    const-string v1, "GPS"

    .line 594
    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo$Builder;->comTipoPagamento(Ljava/lang/String;)Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo$Builder;

    move-result-object v0

    .line 595
    invoke-virtual {v0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo$Builder;->build()Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo;

    move-result-object v2

    .line 596
    .local v2, "dadosPagamentoTributo":Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo;
    invoke-static {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity;->intent(Landroid/content/Context;)Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity_$IntentBuilder_;

    move-result-object v0

    .line 597
    invoke-virtual {v0, v2}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity_$IntentBuilder_;->dadosPagamentoTributo(Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo;)Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity_$IntentBuilder_;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity_$IntentBuilder_;->start()Lorg/androidannotations/api/builder/PostActivityStarter;

    .line 598
    invoke-virtual {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->finish()V

    .line 599
    return-void
.end method

.method static synthetic access$000(Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;)V
    .registers 1
    .param p0, "x0"    # Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;

    .line 76
    invoke-direct {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->abreDatePickerCompetencia()V

    return-void
.end method

.method static synthetic access$100(Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;)V
    .registers 1
    .param p0, "x0"    # Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;

    .line 76
    invoke-direct {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->abreDatePickerPagamento()V

    return-void
.end method

.method static synthetic access$200(Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;Z)V
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;
    .param p1, "x1"    # Z

    .line 76
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->incluiGps(Z)V

    return-void
.end method

.method private adicionaEditTextParaValidacao()V
    .registers 3

    .line 346
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->listaEditTextValidacao:Ljava/util/List;

    .line 347
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->listaEditTextValidacao:Ljava/util/List;

    iget-object v1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->campoNomeContribuinte:Lcom/itau/empresas/ui/view/EditTextCustomError;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 348
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->listaEditTextValidacao:Ljava/util/List;

    iget-object v1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->campoCodigoPagamento:Landroid/widget/EditText;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 349
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->listaEditTextValidacao:Ljava/util/List;

    iget-object v1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->campoIdentificador:Landroid/widget/EditText;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 350
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->listaEditTextValidacao:Ljava/util/List;

    iget-object v1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->campoIdentificador:Landroid/widget/EditText;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 351
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->listaEditTextValidacao:Ljava/util/List;

    iget-object v1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->campoValorInss:Landroid/widget/EditText;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 352
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->listaEditTextValidacao:Ljava/util/List;

    iget-object v1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->campoCompetencia:Landroid/widget/TextView;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 353
    return-void
.end method

.method private atualizaDadosPerfilLogado(Lcom/itau/empresas/api/model/PerfilOperadorVO;)V
    .registers 5
    .param p1, "perfilOperadorVO"    # Lcom/itau/empresas/api/model/PerfilOperadorVO;

    .line 373
    if-eqz p1, :cond_6a

    .line 374
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->campoNomeDaEmpresa:Landroid/widget/TextView;

    .line 375
    invoke-virtual {p1}, Lcom/itau/empresas/api/model/PerfilOperadorVO;->getNomeEmpresa()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_17

    .line 376
    invoke-virtual {p1}, Lcom/itau/empresas/api/model/PerfilOperadorVO;->getNomeEmpresa()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    goto :goto_19

    :cond_17
    const-string v1, ""

    .line 374
    :goto_19
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 377
    .line 378
    invoke-virtual {p1}, Lcom/itau/empresas/api/model/PerfilOperadorVO;->getContasOperador()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_32

    .line 379
    invoke-virtual {p1}, Lcom/itau/empresas/api/model/PerfilOperadorVO;->getContasOperador()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/api/model/ContaOperadorVO;

    goto :goto_37

    :cond_32
    new-instance v0, Lcom/itau/empresas/api/model/ContaOperadorVO;

    invoke-direct {v0}, Lcom/itau/empresas/api/model/ContaOperadorVO;-><init>()V

    :goto_37
    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->contaOperadorVO:Lcom/itau/empresas/api/model/ContaOperadorVO;

    .line 380
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->campoAgenciaConta:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->contaOperadorVO:Lcom/itau/empresas/api/model/ContaOperadorVO;

    .line 381
    invoke-virtual {v1}, Lcom/itau/empresas/api/model/ContaOperadorVO;->getAgencia()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_63

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->contaOperadorVO:Lcom/itau/empresas/api/model/ContaOperadorVO;

    .line 382
    invoke-virtual {v2}, Lcom/itau/empresas/api/model/ContaOperadorVO;->getAgencia()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->contaOperadorVO:Lcom/itau/empresas/api/model/ContaOperadorVO;

    .line 383
    invoke-virtual {v2}, Lcom/itau/empresas/api/model/ContaOperadorVO;->getConta()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    goto :goto_65

    :cond_63
    const-string v1, ""

    :goto_65
    check-cast v1, Ljava/lang/CharSequence;

    .line 380
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 385
    :cond_6a
    return-void
.end method

.method private buildDadosConfirmacao()Ljava/util/ArrayList;
    .registers 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/ArrayList<Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosConfirmacaoTributo;>;"
        }
    .end annotation

    .line 537
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 538
    .local v4, "dadosConfirmacaoTributos":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosConfirmacaoTributo;>;"
    new-instance v0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosConfirmacaoTributo;

    .line 539
    const v1, 0x7f07047f

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->gpsRequestVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/GpsRequestVO;

    .line 540
    invoke-virtual {v2}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/GpsRequestVO;->getNomeContribuinte()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosConfirmacaoTributo;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 539
    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 541
    new-instance v0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosConfirmacaoTributo;

    .line 542
    const v1, 0x7f07018c

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->gpsRequestVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/GpsRequestVO;

    .line 543
    invoke-virtual {v2}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/GpsRequestVO;->getCodigoPagamento()Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosConfirmacaoTributo;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 542
    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 544
    new-instance v0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosConfirmacaoTributo;

    const v1, 0x7f070193

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->gpsRequestVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/GpsRequestVO;

    .line 545
    invoke-virtual {v2}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/GpsRequestVO;->getMesCompetencia()Ljava/lang/String;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->gpsRequestVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/GpsRequestVO;

    .line 546
    invoke-virtual {v3}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/GpsRequestVO;->getAnoCompetencia()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosConfirmacaoTributo;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 544
    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 547
    new-instance v0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosConfirmacaoTributo;

    const v1, 0x7f070371

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->gpsRequestVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/GpsRequestVO;

    .line 548
    invoke-virtual {v2}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/GpsRequestVO;->getIdentificadorInclusao()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosConfirmacaoTributo;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 547
    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 549
    new-instance v0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosConfirmacaoTributo;

    const v1, 0x7f0706d0

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->gpsRequestVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/GpsRequestVO;

    .line 550
    invoke-virtual {v2}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/GpsRequestVO;->getValorINSS()Ljava/math/BigDecimal;

    move-result-object v2

    if-eqz v2, :cond_8e

    iget-object v2, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->gpsRequestVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/GpsRequestVO;

    .line 551
    invoke-virtual {v2}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/GpsRequestVO;->getValorINSS()Ljava/math/BigDecimal;

    move-result-object v2

    const/4 v3, 0x1

    invoke-static {v2, v3}, Lcom/itau/empresas/ui/util/formatacao/FormatadorNumerico;->decimalParaMonetario(Ljava/math/BigDecimal;Z)Ljava/lang/String;

    move-result-object v2

    goto :goto_90

    :cond_8e
    const-string v2, "R$ 0,00"

    :goto_90
    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosConfirmacaoTributo;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 549
    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 553
    new-instance v0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosConfirmacaoTributo;

    .line 554
    const v1, 0x7f0706cd

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->gpsRequestVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/GpsRequestVO;

    .line 555
    invoke-virtual {v2}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/GpsRequestVO;->getValorOutrasEntidades()Ljava/math/BigDecimal;

    move-result-object v2

    if-eqz v2, :cond_b4

    iget-object v2, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->gpsRequestVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/GpsRequestVO;

    .line 556
    invoke-virtual {v2}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/GpsRequestVO;->getValorOutrasEntidades()Ljava/math/BigDecimal;

    move-result-object v2

    const/4 v3, 0x1

    invoke-static {v2, v3}, Lcom/itau/empresas/ui/util/formatacao/FormatadorNumerico;->decimalParaMonetario(Ljava/math/BigDecimal;Z)Ljava/lang/String;

    move-result-object v2

    goto :goto_b6

    :cond_b4
    const-string v2, "R$ 0,00"

    :goto_b6
    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosConfirmacaoTributo;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 554
    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 558
    new-instance v0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosConfirmacaoTributo;

    .line 559
    const v1, 0x7f070127

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->gpsRequestVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/GpsRequestVO;

    .line 560
    invoke-virtual {v2}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/GpsRequestVO;->getValorAtualizacaoMonetaria()Ljava/math/BigDecimal;

    move-result-object v2

    if-eqz v2, :cond_da

    iget-object v2, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->gpsRequestVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/GpsRequestVO;

    .line 561
    invoke-virtual {v2}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/GpsRequestVO;->getValorAtualizacaoMonetaria()Ljava/math/BigDecimal;

    move-result-object v2

    const/4 v3, 0x1

    invoke-static {v2, v3}, Lcom/itau/empresas/ui/util/formatacao/FormatadorNumerico;->decimalParaMonetario(Ljava/math/BigDecimal;Z)Ljava/lang/String;

    move-result-object v2

    goto :goto_dc

    :cond_da
    const-string v2, "R$ 0,00"

    :goto_dc
    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosConfirmacaoTributo;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 559
    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 563
    new-instance v0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosConfirmacaoTributo;

    .line 564
    const v1, 0x7f0703f7

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->gpsRequestVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/GpsRequestVO;

    .line 565
    invoke-virtual {v2}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/GpsRequestVO;->getValorArrecadado()Ljava/math/BigDecimal;

    move-result-object v2

    if-eqz v2, :cond_100

    iget-object v2, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->gpsRequestVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/GpsRequestVO;

    .line 566
    invoke-virtual {v2}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/GpsRequestVO;->getValorArrecadado()Ljava/math/BigDecimal;

    move-result-object v2

    const/4 v3, 0x1

    invoke-static {v2, v3}, Lcom/itau/empresas/ui/util/formatacao/FormatadorNumerico;->decimalParaMonetario(Ljava/math/BigDecimal;Z)Ljava/lang/String;

    move-result-object v2

    goto :goto_102

    :cond_100
    const-string v2, "R$ 0,00"

    :goto_102
    const/4 v3, 0x1

    invoke-direct {v0, v1, v2, v3}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosConfirmacaoTributo;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 564
    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 568
    new-instance v0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosConfirmacaoTributo;

    .line 569
    const v1, 0x7f070495

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->gpsRequestVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/GpsRequestVO;

    .line 570
    invoke-virtual {v2}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/GpsRequestVO;->getDataPagamento()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_129

    iget-object v2, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->gpsRequestVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/GpsRequestVO;

    .line 571
    invoke-virtual {v2}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/GpsRequestVO;->getDataPagamento()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/itau/empresas/ui/util/DataEHoraUtils;->formataDataComBarra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    goto :goto_12b

    :cond_129
    const-string v2, ""

    :goto_12b
    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosConfirmacaoTributo;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 568
    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 573
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->gpsRequestVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/GpsRequestVO;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/GpsRequestVO;->getReferenciaEmpresa()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_154

    .line 574
    new-instance v0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosConfirmacaoTributo;

    .line 575
    const v1, 0x7f070498

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->gpsRequestVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/GpsRequestVO;

    .line 576
    invoke-virtual {v2}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/GpsRequestVO;->getReferenciaEmpresa()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosConfirmacaoTributo;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 574
    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 578
    :cond_154
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->gpsRequestVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/GpsRequestVO;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/GpsRequestVO;->getInformacoesComplementares()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_176

    .line 579
    new-instance v0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosConfirmacaoTributo;

    .line 580
    const v1, 0x7f070498

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->gpsRequestVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/GpsRequestVO;

    .line 581
    invoke-virtual {v2}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/GpsRequestVO;->getInformacoesComplementares()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosConfirmacaoTributo;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 579
    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 584
    :cond_176
    return-object v4
.end method

.method private configuraToolbar()V
    .registers 3

    .line 623
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->toolBar:Landroid/support/v7/widget/Toolbar;

    invoke-static {v0, p0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom;->Builder(Landroid/support/v7/widget/Toolbar;Landroid/support/v7/app/AppCompatActivity;)Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    move-result-object v0

    .line 624
    const v1, 0x7f0706a9

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->comTitulo(I)Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    move-result-object v0

    .line 625
    invoke-virtual {v0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->comClickListenerFinish()Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    move-result-object v0

    .line 626
    invoke-virtual {v0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->build()Lcom/itau/empresas/ui/util/listener/ToolbarCustom;

    .line 627
    return-void
.end method

.method private constroiDatePickerDialog(Landroid/widget/TextView;)Lcom/wdullaer/materialdatetimepicker/date/DatePickerDialog;
    .registers 3
    .param p1, "textView"    # Landroid/widget/TextView;

    .line 429
    invoke-static {p0, p1}, Lcom/itau/empresas/ui/util/DataEHoraUtils;->mostraDialogoDataComTodasDatas(Lcom/itau/empresas/ui/activity/BaseActivity;Landroid/widget/TextView;)Lcom/wdullaer/materialdatetimepicker/date/DatePickerDialog;

    move-result-object v0

    return-object v0
.end method

.method private consultaAlcada()V
    .registers 4

    .line 365
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->controller:Lcom/itau/empresas/controller/PagamentoGPSController;

    iget-object v1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->app:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v1}, Lcom/itau/empresas/CustomApplication;->getContaSelecionada()Lcom/itau/empresas/api/model/ContaOperadorVO;

    move-result-object v1

    invoke-virtual {v1}, Lcom/itau/empresas/api/model/ContaOperadorVO;->getConta()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->app:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v2}, Lcom/itau/empresas/CustomApplication;->getCodigoOperador()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/itau/empresas/controller/PagamentoGPSController;->consultaAlcadas(Ljava/lang/String;Ljava/lang/String;)V

    .line 366
    return-void
.end method

.method private consultaCnpjPagamentos()V
    .registers 2

    .line 369
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->controller:Lcom/itau/empresas/controller/PagamentoGPSController;

    invoke-virtual {v0}, Lcom/itau/empresas/controller/PagamentoGPSController;->consultaCnpjPagamentos()V

    .line 370
    return-void
.end method

.method private consultaDadosCliente()V
    .registers 2

    .line 356
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->controller:Lcom/itau/empresas/controller/PagamentoGPSController;

    invoke-virtual {v0}, Lcom/itau/empresas/controller/PagamentoGPSController;->consultaDadosPerfilLogado()Lcom/itau/empresas/api/model/PerfilOperadorVO;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->atualizaDadosPerfilLogado(Lcom/itau/empresas/api/model/PerfilOperadorVO;)V

    .line 357
    return-void
.end method

.method private consultaHorarioLimite()V
    .registers 3

    .line 360
    invoke-virtual {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->mostraDialogoDeProgresso()V

    .line 361
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->controller:Lcom/itau/empresas/controller/PagamentoGPSController;

    const-string v1, "impostos_tributos_concessionarias"

    invoke-virtual {v0, v1}, Lcom/itau/empresas/controller/PagamentoGPSController;->consultaLimitesHorario(Ljava/lang/String;)V

    .line 362
    return-void
.end method

.method private exibeMensagemErro(Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;)V
    .registers 6
    .param p1, "backendExceptionEvent"    # Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;

    .line 262
    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;->getException()Lbr/com/itau/sdk/android/core/exception/BackendException;

    move-result-object v0

    invoke-virtual {v0}, Lbr/com/itau/sdk/android/core/exception/BackendException;->getError()Lbr/com/itau/sdk/android/core/model/ErrorDTO;

    move-result-object v3

    .line 263
    .local v3, "error":Lbr/com/itau/sdk/android/core/model/ErrorDTO;
    invoke-virtual {v3}, Lbr/com/itau/sdk/android/core/model/ErrorDTO;->getCampos()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_21

    .line 264
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->root:Landroid/widget/LinearLayout;

    invoke-virtual {v3}, Lbr/com/itau/sdk/android/core/model/ErrorDTO;->getMensagem()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lbr/com/itau/topsnackbar/TopSnackbar;->make(Landroid/view/ViewGroup;Ljava/lang/CharSequence;I)Lbr/com/itau/topsnackbar/TopSnackbar;

    move-result-object v0

    .line 265
    invoke-virtual {v0}, Lbr/com/itau/topsnackbar/TopSnackbar;->show()Lbr/com/itau/topsnackbar/TopSnackbar;

    goto :goto_3a

    .line 267
    :cond_21
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->root:Landroid/widget/LinearLayout;

    invoke-virtual {v3}, Lbr/com/itau/sdk/android/core/model/ErrorDTO;->getCampos()Ljava/util/List;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lbr/com/itau/sdk/android/core/model/CampoErroDTO;

    invoke-virtual {v1}, Lbr/com/itau/sdk/android/core/model/CampoErroDTO;->getMensagem()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lbr/com/itau/topsnackbar/TopSnackbar;->make(Landroid/view/ViewGroup;Ljava/lang/CharSequence;I)Lbr/com/itau/topsnackbar/TopSnackbar;

    move-result-object v0

    .line 268
    invoke-virtual {v0}, Lbr/com/itau/topsnackbar/TopSnackbar;->show()Lbr/com/itau/topsnackbar/TopSnackbar;

    .line 269
    :goto_3a
    return-void
.end method

.method private habilitaSelecaoCnpj()V
    .registers 3

    .line 340
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->rlDadosCnpjPagador:Landroid/widget/RelativeLayout;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setEnabled(Z)V

    .line 341
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->campoCnpjPagador:Landroid/widget/TextView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 342
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->iconeAbreListaCnpj:Landroid/widget/TextView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 343
    return-void
.end method

.method private incluiGps(Z)V
    .registers 5
    .param p1, "duplicidade"    # Z

    .line 475
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->gpsRequestVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/GpsRequestVO;

    iget-object v1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->app:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v1}, Lcom/itau/empresas/CustomApplication;->getCodigoOperador()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/GpsRequestVO;->setCodigoOperador(Ljava/lang/Integer;)V

    .line 476
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->gpsRequestVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/GpsRequestVO;

    sget-object v1, Lbr/com/concretesolutions/canarinho/formatador/FormatadorCPFCNPJ;->CPF_CNPJ:Lbr/com/concretesolutions/canarinho/formatador/Formatador;

    iget-object v2, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->campoCnpjPagador:Landroid/widget/TextView;

    .line 477
    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lbr/com/concretesolutions/canarinho/formatador/Formatador;->desformata(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 476
    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/GpsRequestVO;->setCnpj(Ljava/lang/String;)V

    .line 478
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->gpsRequestVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/GpsRequestVO;

    iget-object v1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->app:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v1}, Lcom/itau/empresas/CustomApplication;->getPerfilLogado()Lcom/itau/empresas/api/model/PerfilOperadorVO;

    move-result-object v1

    invoke-virtual {v1}, Lcom/itau/empresas/api/model/PerfilOperadorVO;->getNomeEmpresa()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/GpsRequestVO;->setNomeEmpresa(Ljava/lang/String;)V

    .line 479
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->gpsRequestVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/GpsRequestVO;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/GpsRequestVO;->setDuplicidade(Ljava/lang/Boolean;)V

    .line 480
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->gpsRequestVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/GpsRequestVO;

    iget-object v1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->contaOperadorVO:Lcom/itau/empresas/api/model/ContaOperadorVO;

    invoke-virtual {v1}, Lcom/itau/empresas/api/model/ContaOperadorVO;->getAgencia()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/GpsRequestVO;->setAgencia(Ljava/lang/String;)V

    .line 481
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->gpsRequestVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/GpsRequestVO;

    iget-object v1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->contaOperadorVO:Lcom/itau/empresas/api/model/ContaOperadorVO;

    invoke-virtual {v1}, Lcom/itau/empresas/api/model/ContaOperadorVO;->getConta()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/GpsRequestVO;->setConta(Ljava/lang/String;)V

    .line 482
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->gpsRequestVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/GpsRequestVO;

    iget-object v1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->contaOperadorVO:Lcom/itau/empresas/api/model/ContaOperadorVO;

    invoke-virtual {v1}, Lcom/itau/empresas/api/model/ContaOperadorVO;->getDac()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/GpsRequestVO;->setDigitoVerificadorConta(Ljava/lang/String;)V

    .line 483
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->gpsRequestVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/GpsRequestVO;

    iget-object v1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->campoNomeContribuinte:Lcom/itau/empresas/ui/view/EditTextCustomError;

    invoke-virtual {v1}, Lcom/itau/empresas/ui/view/EditTextCustomError;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/GpsRequestVO;->setNomeContribuinte(Ljava/lang/String;)V

    .line 484
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->gpsRequestVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/GpsRequestVO;

    iget-object v1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->campoIdentificacaoComprovante:Landroid/widget/EditText;

    .line 485
    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/GpsRequestVO;->setInformacoesComplementares(Ljava/lang/String;)V

    .line 486
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->gpsRequestVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/GpsRequestVO;

    iget-object v1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->campoCodigoPagamento:Landroid/widget/EditText;

    .line 487
    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_a0

    iget-object v1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->campoCodigoPagamento:Landroid/widget/EditText;

    .line 488
    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    goto :goto_a1

    :cond_a0
    const/4 v1, 0x0

    .line 487
    :goto_a1
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/GpsRequestVO;->setCodigoPagamento(Ljava/lang/Integer;)V

    .line 489
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->gpsRequestVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/GpsRequestVO;

    iget-object v1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->gpsRequestVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/GpsRequestVO;

    invoke-virtual {v1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/GpsRequestVO;->getMesCompetencia()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/GpsRequestVO;->setMesCompetencia(Ljava/lang/String;)V

    .line 490
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->gpsRequestVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/GpsRequestVO;

    iget-object v1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->gpsRequestVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/GpsRequestVO;

    invoke-virtual {v1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/GpsRequestVO;->getAnoCompetencia()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/GpsRequestVO;->setAnoCompetencia(Ljava/lang/String;)V

    .line 491
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->gpsRequestVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/GpsRequestVO;

    iget-object v1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->campoIdentificador:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/GpsRequestVO;->setIdentificadorInclusao(Ljava/lang/String;)V

    .line 492
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->gpsRequestVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/GpsRequestVO;

    iget-object v1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->campoDataPagamento:Landroid/widget/EditText;

    .line 493
    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/itau/empresas/ui/util/DataEHoraUtils;->formataDataApi(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 492
    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/GpsRequestVO;->setDataPagamento(Ljava/lang/String;)V

    .line 494
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->gpsRequestVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/GpsRequestVO;

    iget-object v1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->campoValorInss:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->valorFormatado(Ljava/lang/String;)Ljava/math/BigDecimal;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/GpsRequestVO;->setValorINSS(Ljava/math/BigDecimal;)V

    .line 495
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->gpsRequestVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/GpsRequestVO;

    iget-object v1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->campoValorOutrasEntidades:Landroid/widget/EditText;

    .line 496
    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->valorFormatado(Ljava/lang/String;)Ljava/math/BigDecimal;

    move-result-object v1

    .line 495
    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/GpsRequestVO;->setValorOutrasEntidades(Ljava/math/BigDecimal;)V

    .line 497
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->gpsRequestVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/GpsRequestVO;

    iget-object v1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->campoAtualizacaoMonetaria:Landroid/widget/EditText;

    .line 498
    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->valorFormatado(Ljava/lang/String;)Ljava/math/BigDecimal;

    move-result-object v1

    .line 497
    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/GpsRequestVO;->setValorAtualizacaoMonetaria(Ljava/math/BigDecimal;)V

    .line 499
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->gpsRequestVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/GpsRequestVO;

    iget-object v1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->campoValorTotalGps:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->valorFormatado(Ljava/lang/String;)Ljava/math/BigDecimal;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/GpsRequestVO;->setValorArrecadado(Ljava/math/BigDecimal;)V

    .line 500
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->gpsRequestVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/GpsRequestVO;

    iget-object v1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->campoReferenciaEmpresa:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/GpsRequestVO;->setReferenciaEmpresa(Ljava/lang/String;)V

    .line 501
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->gpsRequestVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/GpsRequestVO;

    const-string v1, "J"

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/GpsRequestVO;->setTipoUsuario(Ljava/lang/String;)V

    .line 503
    return-void
.end method

.method private incluiOuAutorizaGps()V
    .registers 3

    .line 518
    iget-boolean v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->autorizar:Z

    if-eqz v0, :cond_c

    .line 519
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->incluiGps(Z)V

    .line 520
    invoke-direct {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->abreTelaConfirmacao()V

    goto :goto_1a

    .line 522
    :cond_c
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->incluiGps(Z)V

    .line 523
    invoke-virtual {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->mostraDialogoDeProgresso()V

    .line 524
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->controller:Lcom/itau/empresas/controller/PagamentoGPSController;

    iget-object v1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->gpsRequestVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/GpsRequestVO;

    invoke-virtual {v0, v1}, Lcom/itau/empresas/controller/PagamentoGPSController;->inclusaoGPS(Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/GpsRequestVO;)V

    .line 526
    :goto_1a
    return-void
.end method

.method private inicializaCamposValoresDefault()V
    .registers 4

    .line 331
    invoke-static {}, Lcom/itau/empresas/ui/util/formatacao/DateUtils;->getDataAtualFormatoBrasileiro()Ljava/lang/String;

    move-result-object v2

    .line 332
    .local v2, "dataAtual":Ljava/lang/String;
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->campoValorTotalGps:Landroid/widget/EditText;

    const v1, 0x7f0706cf

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(I)V

    .line 333
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->campoValorInss:Landroid/widget/EditText;

    const v1, 0x7f0706cf

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(I)V

    .line 334
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->campoValorOutrasEntidades:Landroid/widget/EditText;

    const v1, 0x7f0706cf

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(I)V

    .line 335
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->campoAtualizacaoMonetaria:Landroid/widget/EditText;

    const v1, 0x7f0706cf

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(I)V

    .line 336
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->campoDataPagamento:Landroid/widget/EditText;

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 337
    return-void
.end method

.method private irParaCamporObrigatorio(Landroid/view/View;)V
    .registers 5
    .param p1, "view"    # Landroid/view/View;

    .line 602
    invoke-static {p0, p1}, Lcom/itau/empresas/ui/util/TecladoUtils;->esconderTecladoDaView(Landroid/content/Context;Landroid/view/View;)V

    .line 603
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->scrollGps:Landroid/widget/ScrollView;

    invoke-virtual {p1}, Landroid/view/View;->getBottom()I

    move-result v1

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v2

    sub-int/2addr v1, v2

    const/4 v2, 0x0

    invoke-virtual {v0, v2, v1}, Landroid/widget/ScrollView;->scrollTo(II)V

    .line 604
    invoke-virtual {p1}, Landroid/view/View;->requestFocus()Z

    .line 605
    return-void
.end method

.method private marcaCampoObrigatorio(Ljava/util/List;)V
    .registers 7
    .param p1, "editTextList"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<Landroid/widget/TextView;>;)V"
        }
    .end annotation

    .line 608
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_4
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_40

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Landroid/widget/TextView;

    .line 609
    .local v3, "editText":Landroid/widget/TextView;
    instance-of v0, v3, Lcom/itau/empresas/ui/view/EditTextCustomError;

    if-eqz v0, :cond_28

    .line 610
    move-object v4, v3

    check-cast v4, Lcom/itau/empresas/ui/view/EditTextCustomError;

    .line 611
    .local v4, "editTextCustomError":Lcom/itau/empresas/ui/view/EditTextCustomError;
    invoke-virtual {v3}, Landroid/widget/TextView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v4, v0}, Lcom/itau/empresas/ui/view/EditTextCustomError;->setaBackgroundErro(Landroid/view/ViewGroup;)V

    .line 612
    move-object v0, v3

    check-cast v0, Lcom/itau/empresas/ui/view/EditTextCustomError;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/view/EditTextCustomError;->setErro(Z)V

    .line 614
    .end local v4    # "editTextCustomError":Lcom/itau/empresas/ui/view/EditTextCustomError;
    :cond_28
    instance-of v0, v3, Lcom/itau/empresas/ui/view/EdittextErrorMonetario;

    if-eqz v0, :cond_3f

    .line 615
    move-object v4, v3

    check-cast v4, Lcom/itau/empresas/ui/view/EdittextErrorMonetario;

    .line 616
    .local v4, "editTextCustomError":Lcom/itau/empresas/ui/view/EdittextErrorMonetario;
    invoke-virtual {v3}, Landroid/widget/TextView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v4, v0}, Lcom/itau/empresas/ui/view/EdittextErrorMonetario;->setaBackgroundErro(Landroid/view/ViewGroup;)V

    .line 617
    move-object v0, v3

    check-cast v0, Lcom/itau/empresas/ui/view/EdittextErrorMonetario;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/view/EdittextErrorMonetario;->setErro(Z)V

    .line 619
    .end local v3    # "editText":Landroid/widget/TextView;
    .end local v4    # "editTextCustomError":Lcom/itau/empresas/ui/view/EdittextErrorMonetario;
    :cond_3f
    goto :goto_4

    .line 620
    :cond_40
    return-void
.end method

.method private mostraBotaoAutorizar()V
    .registers 6

    .line 433
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->app:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getPermissaoAutorizante()Lcom/itau/empresas/api/model/MenuVO;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/api/model/MenuVO;->getFilhos()Ljava/util/List;

    move-result-object v2

    .line 434
    .local v2, "filhos":Ljava/util/List;, "Ljava/util/List<Lcom/itau/empresas/api/model/MenuVO;>;"
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_e
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_58

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/itau/empresas/api/model/MenuVO;

    .line 435
    .local v4, "menuVO":Lcom/itau/empresas/api/model/MenuVO;
    invoke-virtual {v4}, Lcom/itau/empresas/api/model/MenuVO;->getNome()Ljava/lang/String;

    move-result-object v0

    const-string v1, "exibicao_tributos_autorizar"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_39

    iget-boolean v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->alcadaAutorizante:Z

    if-eqz v0, :cond_39

    .line 437
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->rlAutorizacaoGps:Landroid/widget/RelativeLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 438
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->rlInclusaoGps:Landroid/widget/RelativeLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 439
    return-void

    .line 440
    :cond_39
    invoke-virtual {v4}, Lcom/itau/empresas/api/model/MenuVO;->getNome()Ljava/lang/String;

    move-result-object v0

    const-string v1, "exibicao_tributos_autorizar"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_57

    iget-boolean v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->alcadaAutorizante:Z

    if-nez v0, :cond_57

    .line 442
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->rlInclusaoGps:Landroid/widget/RelativeLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 443
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->rlAutorizacaoGps:Landroid/widget/RelativeLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 444
    return-void

    .line 446
    .end local v4    # "menuVO":Lcom/itau/empresas/api/model/MenuVO;
    :cond_57
    goto :goto_e

    .line 447
    :cond_58
    return-void
.end method

.method private mostraDialogDuplicidade()V
    .registers 4

    .line 450
    new-instance v0, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;

    invoke-direct {v0, p0}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget-object v1, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Estados;->DOIS_BOTOES:Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Estados;

    .line 451
    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;->estado(Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Estados;)Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;

    move-result-object v0

    .line 452
    const v1, 0x7f0701aa

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;->mensagem(I)Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;

    move-result-object v0

    .line 453
    const v1, 0x7f070176

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;->textoPositivo(I)Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;

    move-result-object v0

    .line 454
    const v1, 0x7f0701e6

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;->textoNegativo(I)Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;

    move-result-object v0

    .line 455
    invoke-virtual {v0}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;->build()Lcom/itau/empresas/ui/dialog/CustomAlertDialog;

    move-result-object v2

    .line 456
    .local v2, "dialog":Lcom/itau/empresas/ui/dialog/CustomAlertDialog;
    const/4 v0, 0x1

    invoke-virtual {v2, v0}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog;->setBoldBotaoPositivo(Z)V

    .line 457
    new-instance v0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy$6;

    invoke-direct {v0, p0, v2}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy$6;-><init>(Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;Lcom/itau/empresas/ui/dialog/CustomAlertDialog;)V

    invoke-virtual {v2, v0}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog;->setNegativoListener(Landroid/view/View$OnClickListener;)V

    .line 465
    new-instance v0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy$7;

    invoke-direct {v0, p0, v2}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy$7;-><init>(Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;Lcom/itau/empresas/ui/dialog/CustomAlertDialog;)V

    invoke-virtual {v2, v0}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog;->setPositivoListener(Landroid/view/View$OnClickListener;)V

    .line 471
    invoke-virtual {v2, p0}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog;->show(Landroid/support/v7/app/AppCompatActivity;)Lcom/itau/empresas/ui/dialog/BaseDialog;

    .line 472
    return-void
.end method

.method private mostraDialogoInformacoes()V
    .registers 4

    .line 388
    new-instance v0, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;

    invoke-direct {v0, p0}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget-object v1, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Estados;->UM_BOTAO:Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Estados;

    .line 389
    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;->estado(Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Estados;)Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;

    move-result-object v0

    .line 390
    const v1, 0x7f070377

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;->mensagem(I)Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;

    move-result-object v0

    .line 391
    const v1, 0x7f07048c

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;->textoNeutro(I)Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;

    move-result-object v0

    .line 392
    const v1, 0x800003

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;->alinhamentoTexto(I)Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;

    move-result-object v0

    .line 393
    invoke-virtual {v0}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;->build()Lcom/itau/empresas/ui/dialog/CustomAlertDialog;

    move-result-object v2

    .line 395
    .local v2, "dialog":Lcom/itau/empresas/ui/dialog/CustomAlertDialog;
    new-instance v0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy$3;

    invoke-direct {v0, p0, v2}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy$3;-><init>(Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;Lcom/itau/empresas/ui/dialog/CustomAlertDialog;)V

    invoke-virtual {v2, v0}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog;->setNeutroListener(Landroid/view/View$OnClickListener;)V

    .line 401
    new-instance v0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy$4;

    invoke-direct {v0, p0, v2}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy$4;-><init>(Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;Lcom/itau/empresas/ui/dialog/CustomAlertDialog;)V

    invoke-virtual {v2, v0}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog;->setPositivoListener(Landroid/view/View$OnClickListener;)V

    .line 407
    invoke-virtual {v2, p0}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog;->show(Landroid/support/v7/app/AppCompatActivity;)Lcom/itau/empresas/ui/dialog/BaseDialog;

    .line 408
    return-void
.end method

.method private valorFormatado(Ljava/lang/String;)Ljava/math/BigDecimal;
    .registers 5
    .param p1, "valor"    # Ljava/lang/String;

    .line 507
    :try_start_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_e

    const-string v0, "R$ 0,00"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_16

    :cond_e
    new-instance v0, Ljava/math/BigDecimal;

    const-string v1, "0.00"

    invoke-direct {v0, v1}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    goto :goto_1b

    .line 510
    :cond_16
    const/4 v0, 0x1

    invoke-static {p1, v0}, Lcom/itau/empresas/ui/util/ValorMonetarioUtils;->formataValorMonetarioParaDecimal(Ljava/lang/String;Z)Ljava/math/BigDecimal;
    :try_end_1a
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_1a} :catch_1c

    move-result-object v0

    .line 507
    :goto_1b
    return-object v0

    .line 511
    :catch_1c
    move-exception v2

    .line 512
    .local v2, "e":Ljava/text/ParseException;
    const-string v0, "ERRO"

    invoke-virtual {v2}, Ljava/text/ParseException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 514
    .end local v2    # "e":Ljava/text/ParseException;
    new-instance v0, Ljava/math/BigDecimal;

    const-string v1, "0.00"

    invoke-direct {v0, v1}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method abreActivityListaCnpj()V
    .registers 3

    .line 179
    invoke-static {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ListaCnpjPagadoresActivity;->intent(Landroid/content/Context;)Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ListaCnpjPagadoresActivity_$IntentBuilder_;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->cnpjVOList:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ListaCnpjPagadoresActivity_$IntentBuilder_;->cnpjVOList(Ljava/util/ArrayList;)Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ListaCnpjPagadoresActivity_$IntentBuilder_;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ListaCnpjPagadoresActivity_$IntentBuilder_;->startForResult(I)Lorg/androidannotations/api/builder/PostActivityStarter;

    .line 180
    return-void
.end method

.method abreDatePicker()V
    .registers 1

    .line 184
    invoke-direct {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->abreDatePickerCompetencia()V

    .line 185
    return-void
.end method

.method abreDialogoInformacoes()V
    .registers 1

    .line 158
    invoke-direct {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->mostraDialogoInformacoes()V

    .line 159
    return-void
.end method

.method public aoClicarDataPagamento()V
    .registers 1

    .line 189
    invoke-direct {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->abreDatePickerPagamento()V

    .line 190
    return-void
.end method

.method apenasIncluirGps()V
    .registers 3

    .line 174
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->controller:Lcom/itau/empresas/controller/PagamentoGPSController;

    iget-object v1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->listaEditTextValidacao:Ljava/util/List;

    invoke-virtual {v0, v1, p0}, Lcom/itau/empresas/controller/PagamentoGPSController;->validaTextoObrigatório(Ljava/util/List;Lcom/itau/empresas/ui/util/listener/ValidacaoEditTextListener;)V

    .line 175
    return-void
.end method

.method atualizaValorInss(Landroid/text/Editable;)V
    .registers 4
    .param p1, "editable"    # Landroid/text/Editable;

    .line 199
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->atualizaValorPagamentoGPS:Lcom/itau/empresas/ui/util/listener/AtualizaValorPagamentoGPS;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/listener/AtualizaValorPagamentoGPS;->setValorInss(Ljava/lang/String;)V

    .line 200
    return-void
.end method

.method public atualizaValorTotal(Lcom/itau/empresas/api/universal/model/DadosGps;)V
    .registers 5
    .param p1, "dadosGps"    # Lcom/itau/empresas/api/universal/model/DadosGps;

    .line 290
    invoke-virtual {p1}, Lcom/itau/empresas/api/universal/model/DadosGps;->getValorAPagar()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_18

    .line 291
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->campoValorTotalGps:Landroid/widget/EditText;

    .line 293
    invoke-virtual {p1}, Lcom/itau/empresas/api/universal/model/DadosGps;->getValorAPagar()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/itau/empresas/ui/util/ValorMonetarioUtils;->formataValorDecimalParaMonetario(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v1

    .line 291
    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 295
    :cond_18
    return-void
.end method

.method autorizarGps()V
    .registers 3

    .line 168
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->autorizar:Z

    .line 169
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->controller:Lcom/itau/empresas/controller/PagamentoGPSController;

    iget-object v1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->listaEditTextValidacao:Ljava/util/List;

    invoke-virtual {v0, v1, p0}, Lcom/itau/empresas/controller/PagamentoGPSController;->validaTextoObrigatório(Ljava/util/List;Lcom/itau/empresas/ui/util/listener/ValidacaoEditTextListener;)V

    .line 170
    return-void
.end method

.method public campoInvalido(Landroid/widget/EditText;)V
    .registers 2
    .param p1, "editText"    # Landroid/widget/EditText;

    .line 313
    return-void
.end method

.method public campoInvalido(Ljava/util/List;)V
    .registers 3
    .param p1, "editText"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<Landroid/widget/TextView;>;)V"
        }
    .end annotation

    .line 299
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->marcaCampoObrigatorio(Ljava/util/List;)V

    .line 300
    const/4 v0, 0x0

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-direct {p0, v0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->irParaCamporObrigatorio(Landroid/view/View;)V

    .line 301
    return-void
.end method

.method public campoValido()V
    .registers 1

    .line 305
    invoke-direct {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->incluiOuAutorizaGps()V

    .line 306
    return-void
.end method

.method public campoValido(Landroid/widget/EditText;)V
    .registers 2
    .param p1, "editText"    # Landroid/widget/EditText;

    .line 320
    return-void
.end method

.method public carregaElementosTela()V
    .registers 3

    .line 133
    new-instance v0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/GpsRequestVO;

    invoke-direct {v0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/GpsRequestVO;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->gpsRequestVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/GpsRequestVO;

    .line 134
    new-instance v0, Lcom/itau/empresas/ui/util/listener/AtualizaValorPagamentoGPS;

    invoke-direct {v0, p0}, Lcom/itau/empresas/ui/util/listener/AtualizaValorPagamentoGPS;-><init>(Lcom/itau/empresas/ui/util/listener/AtualizaValorTotalGps;)V

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->atualizaValorPagamentoGPS:Lcom/itau/empresas/ui/util/listener/AtualizaValorPagamentoGPS;

    .line 135
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->rlDadosCnpjPagador:Landroid/widget/RelativeLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setEnabled(Z)V

    .line 136
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->campoCnpjPagador:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 137
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->iconeAbreListaCnpj:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 138
    invoke-direct {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->configuraToolbar()V

    .line 139
    invoke-direct {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->consultaAlcada()V

    .line 140
    invoke-direct {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->inicializaCamposValoresDefault()V

    .line 141
    invoke-direct {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->adicionaEditTextParaValidacao()V

    .line 142
    invoke-direct {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->consultaHorarioLimite()V

    .line 143
    invoke-direct {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->consultaDadosCliente()V

    .line 144
    invoke-direct {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->consultaCnpjPagamentos()V

    .line 145
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->campoCompetencia:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->onFocusCampoCompetencia:Landroid/view/View$OnFocusChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 146
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->campoDataPagamento:Landroid/widget/EditText;

    new-instance v1, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy$2;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy$2;-><init>(Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 154
    return-void
.end method

.method changeValorAtualizacaoMonetaria(Landroid/text/Editable;)V
    .registers 4
    .param p1, "editable"    # Landroid/text/Editable;

    .line 204
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->atualizaValorPagamentoGPS:Lcom/itau/empresas/ui/util/listener/AtualizaValorPagamentoGPS;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/listener/AtualizaValorPagamentoGPS;->setValorAtualizacaoMonetaria(Ljava/lang/String;)V

    .line 205
    return-void
.end method

.method changeValorOutrasEntidades(Landroid/text/Editable;)V
    .registers 4
    .param p1, "editable"    # Landroid/text/Editable;

    .line 194
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->atualizaValorPagamentoGPS:Lcom/itau/empresas/ui/util/listener/AtualizaValorPagamentoGPS;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/listener/AtualizaValorPagamentoGPS;->setValorOutrasEntidades(Ljava/lang/String;)V

    .line 195
    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .registers 8
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .line 323
    const/4 v0, -0x1

    if-ne p2, v0, :cond_2c

    const-string v0, "cnpj"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2c

    .line 324
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "cnpj"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/CnpjVO;

    .line 325
    .local v3, "cnpjVO":Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/CnpjVO;
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->campoCnpjPagador:Landroid/widget/TextView;

    sget-object v1, Lbr/com/concretesolutions/canarinho/formatador/Formatador;->CNPJ:Lbr/com/concretesolutions/canarinho/formatador/Formatador;

    if-eqz v3, :cond_23

    .line 326
    invoke-virtual {v3}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/CnpjVO;->getCnpj()Ljava/lang/String;

    move-result-object v2

    goto :goto_25

    :cond_23
    const-string v2, ""

    :goto_25
    invoke-interface {v1, v2}, Lbr/com/concretesolutions/canarinho/formatador/Formatador;->formata(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 328
    .end local v3    # "cnpjVO":Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/CnpjVO;
    :cond_2c
    return-void
.end method

.method onClickIncluiGPS()V
    .registers 3

    .line 163
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->controller:Lcom/itau/empresas/controller/PagamentoGPSController;

    iget-object v1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->listaEditTextValidacao:Ljava/util/List;

    invoke-virtual {v0, v1, p0}, Lcom/itau/empresas/controller/PagamentoGPSController;->validaTextoObrigatório(Ljava/util/List;Lcom/itau/empresas/ui/util/listener/ValidacaoEditTextListener;)V

    .line 164
    return-void
.end method

.method public onEventMainThread(Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;)V
    .registers 5
    .param p1, "backEndException"    # Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;

    .line 248
    invoke-static {p1, p0}, Lcom/itau/empresas/api/ApiHelper;->devoTratarEvento(Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;Lcom/itau/empresas/api/ApiConsumidor;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 249
    return-void

    .line 251
    :cond_7
    invoke-virtual {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->escondeDialogoDeProgresso()V

    .line 252
    const-string v0, "102cnpj n\u00e3o cadastrado com a forma de pagamento"

    .line 253
    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;->getException()Lbr/com/itau/sdk/android/core/exception/BackendException;

    move-result-object v1

    invoke-virtual {v1}, Lbr/com/itau/sdk/android/core/exception/BackendException;->getError()Lbr/com/itau/sdk/android/core/model/ErrorDTO;

    move-result-object v1

    invoke-virtual {v1}, Lbr/com/itau/sdk/android/core/model/ErrorDTO;->getMensagem()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2c

    .line 254
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->root:Landroid/widget/LinearLayout;

    const v1, 0x7f070494

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lbr/com/itau/topsnackbar/TopSnackbar;->make(Landroid/view/ViewGroup;II)Lbr/com/itau/topsnackbar/TopSnackbar;

    move-result-object v0

    .line 255
    invoke-virtual {v0}, Lbr/com/itau/topsnackbar/TopSnackbar;->show()Lbr/com/itau/topsnackbar/TopSnackbar;

    goto :goto_2f

    .line 257
    :cond_2c
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->exibeMensagemErro(Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;)V

    .line 259
    :goto_2f
    return-void
.end method

.method public onEventMainThread(Lcom/itau/empresas/api/model/AlcadaVO;)V
    .registers 3
    .param p1, "alcadaVO"    # Lcom/itau/empresas/api/model/AlcadaVO;

    .line 208
    invoke-virtual {p1}, Lcom/itau/empresas/api/model/AlcadaVO;->getContasAPagarVO()Lcom/itau/empresas/api/model/ContasAPagarVO;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/api/model/ContasAPagarVO;->getTributosAlcadaVO()Lcom/itau/empresas/api/model/TributosAlcadaVO;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/api/model/TributosAlcadaVO;->isAutorizante()Z

    move-result v0

    if-eqz v0, :cond_1e

    .line 209
    invoke-virtual {p1}, Lcom/itau/empresas/api/model/AlcadaVO;->getContasAPagarVO()Lcom/itau/empresas/api/model/ContasAPagarVO;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/api/model/ContasAPagarVO;->getObrigatorioVistadorVO()Lcom/itau/empresas/api/model/ObrigatorioVistadorVO;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/api/model/ObrigatorioVistadorVO;->isAutorizante()Z

    move-result v0

    if-nez v0, :cond_1e

    const/4 v0, 0x1

    goto :goto_1f

    :cond_1e
    const/4 v0, 0x0

    :goto_1f
    iput-boolean v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->alcadaAutorizante:Z

    .line 210
    invoke-direct {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->mostraBotaoAutorizar()V

    .line 211
    return-void
.end method

.method public onEventMainThread(Lcom/itau/empresas/api/model/HorarioLimiteTransacoesVO;)V
    .registers 4
    .param p1, "horarioLimiteTransacoesVO"    # Lcom/itau/empresas/api/model/HorarioLimiteTransacoesVO;

    .line 214
    invoke-virtual {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->escondeDialogoDeProgresso()V

    .line 215
    if-eqz p1, :cond_25

    .line 216
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->rlHorarioLimite:Landroid/widget/RelativeLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 217
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->campoHorarioLimiteInicial:Landroid/widget/TextView;

    .line 219
    invoke-virtual {p1}, Lcom/itau/empresas/api/model/HorarioLimiteTransacoesVO;->getHorarioInclusao()Ljava/lang/String;

    move-result-object v1

    .line 218
    invoke-static {v1}, Lcom/itau/empresas/ui/util/formatacao/FormataHorario;->HoraMinutoSegundoParaHoraMinuto(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 217
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 220
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->campoHorarioLimiteFinal:Landroid/widget/TextView;

    .line 222
    invoke-virtual {p1}, Lcom/itau/empresas/api/model/HorarioLimiteTransacoesVO;->getHorarioAutorizacao()Ljava/lang/String;

    move-result-object v1

    .line 221
    invoke-static {v1}, Lcom/itau/empresas/ui/util/formatacao/FormataHorario;->HoraMinutoSegundoParaHoraMinuto(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 220
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 224
    :cond_25
    return-void
.end method

.method public onEventMainThread(Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/InclusaoTributoResponseVO;)V
    .registers 3
    .param p1, "inclusaoTributoResponseVO"    # Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/InclusaoTributoResponseVO;

    .line 237
    invoke-virtual {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->escondeDialogoDeProgresso()V

    .line 238
    invoke-virtual {p1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/InclusaoTributoResponseVO;->isDuplicado()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 239
    invoke-direct {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->mostraDialogDuplicidade()V

    goto :goto_10

    .line 241
    :cond_d
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->abreTelaDeInclusaoSucesso(Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/InclusaoTributoResponseVO;)V

    .line 244
    :goto_10
    return-void
.end method

.method public onEventMainThread(Ljava/util/List;)V
    .registers 5
    .param p1, "cnpjVOList"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/CnpjVO;>;)V"
        }
    .end annotation

    .line 227
    invoke-virtual {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->escondeDialogoDeProgresso()V

    .line 228
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->cnpjVOList:Ljava/util/ArrayList;

    .line 229
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2e

    .line 230
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->cnpjVOList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 231
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->campoCnpjPagador:Landroid/widget/TextView;

    sget-object v1, Lbr/com/concretesolutions/canarinho/formatador/Formatador;->CNPJ:Lbr/com/concretesolutions/canarinho/formatador/Formatador;

    const/4 v2, 0x0

    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/CnpjVO;

    invoke-virtual {v2}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/CnpjVO;->getCnpj()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lbr/com/concretesolutions/canarinho/formatador/Formatador;->formata(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 232
    invoke-direct {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->habilitaSelecaoCnpj()V

    .line 234
    :cond_2e
    return-void
.end method

.method public onMesAnoFormatado(Ljava/lang/String;)V
    .registers 3
    .param p1, "mesAno"    # Ljava/lang/String;

    .line 279
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->campoCompetencia:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 280
    return-void
.end method

.method public onMesEAno(Ljava/lang/String;Ljava/lang/String;)V
    .registers 4
    .param p1, "mes"    # Ljava/lang/String;
    .param p2, "ano"    # Ljava/lang/String;

    .line 284
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->gpsRequestVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/GpsRequestVO;

    invoke-virtual {v0, p1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/GpsRequestVO;->setMesCompetencia(Ljava/lang/String;)V

    .line 285
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->gpsRequestVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/GpsRequestVO;

    invoke-virtual {v0, p2}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/GpsRequestVO;->setAnoCompetencia(Ljava/lang/String;)V

    .line 286
    return-void
.end method

.method public opkeysConsumidas()Ljava/util/List;
    .registers 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Ljava/lang/String;>;"
        }
    .end annotation

    .line 273
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "pagamentoTributoGpsPJIncluirEfetivar"

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const-string v1, "pagamentoTributoGpsPJIncluirSimular"

    const/4 v2, 0x1

    aput-object v1, v0, v2

    const-string v1, "pagamentosCNPJPagadores"

    const/4 v2, 0x2

    aput-object v1, v0, v2

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.class public Lcom/itau/empresas/api/model/TokenAppVO;
.super Ljava/lang/Object;
.source "TokenAppVO.java"


# instance fields
.field private action:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "action"
    .end annotation
.end field

.field private message:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "message"
    .end annotation
.end field

.field private status:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "status"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getAction()Ljava/lang/String;
    .registers 2

    .line 15
    iget-object v0, p0, Lcom/itau/empresas/api/model/TokenAppVO;->action:Ljava/lang/String;

    return-object v0
.end method

.method public getStatus()Ljava/lang/String;
    .registers 2

    .line 19
    iget-object v0, p0, Lcom/itau/empresas/api/model/TokenAppVO;->status:Ljava/lang/String;

    return-object v0
.end method

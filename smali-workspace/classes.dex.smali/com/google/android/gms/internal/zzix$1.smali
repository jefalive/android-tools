.class Lcom/google/android/gms/internal/zzix$1;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/gms/internal/zzix;->zzhD()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic zzMI:Lcom/google/android/gms/internal/zzix;


# direct methods
.method constructor <init>(Lcom/google/android/gms/internal/zzix;)V
    .registers 2

    iput-object p1, p0, Lcom/google/android/gms/internal/zzix$1;->zzMI:Lcom/google/android/gms/internal/zzix;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .registers 5

    iget-object v0, p0, Lcom/google/android/gms/internal/zzix$1;->zzMI:Lcom/google/android/gms/internal/zzix;

    invoke-static {v0}, Lcom/google/android/gms/internal/zzix;->zza(Lcom/google/android/gms/internal/zzix;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    const-string v0, "Suspending the looper thread"

    :try_start_9
    invoke-static {v0}, Lcom/google/android/gms/internal/zzin;->v(Ljava/lang/String;)V

    :goto_c
    iget-object v0, p0, Lcom/google/android/gms/internal/zzix$1;->zzMI:Lcom/google/android/gms/internal/zzix;

    invoke-static {v0}, Lcom/google/android/gms/internal/zzix;->zzb(Lcom/google/android/gms/internal/zzix;)I
    :try_end_11
    .catchall {:try_start_9 .. :try_end_11} :catchall_2c

    move-result v0

    if-nez v0, :cond_2a

    :try_start_14
    iget-object v0, p0, Lcom/google/android/gms/internal/zzix$1;->zzMI:Lcom/google/android/gms/internal/zzix;

    invoke-static {v0}, Lcom/google/android/gms/internal/zzix;->zza(Lcom/google/android/gms/internal/zzix;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->wait()V

    const-string v0, "Looper thread resumed"

    invoke-static {v0}, Lcom/google/android/gms/internal/zzin;->v(Ljava/lang/String;)V
    :try_end_22
    .catch Ljava/lang/InterruptedException; {:try_start_14 .. :try_end_22} :catch_23
    .catchall {:try_start_14 .. :try_end_22} :catchall_2c

    goto :goto_c

    :catch_23
    move-exception v2

    const-string v0, "Looper thread interrupted."

    :try_start_26
    invoke-static {v0}, Lcom/google/android/gms/internal/zzin;->v(Ljava/lang/String;)V
    :try_end_29
    .catchall {:try_start_26 .. :try_end_29} :catchall_2c

    goto :goto_c

    :cond_2a
    monitor-exit v1

    goto :goto_2f

    :catchall_2c
    move-exception v3

    monitor-exit v1

    throw v3

    :goto_2f
    return-void
.end method

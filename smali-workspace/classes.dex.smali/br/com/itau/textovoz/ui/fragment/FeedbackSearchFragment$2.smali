.class Lbr/com/itau/textovoz/ui/fragment/FeedbackSearchFragment$2;
.super Ljava/lang/Object;
.source "FeedbackSearchFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lbr/com/itau/textovoz/ui/fragment/FeedbackSearchFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lbr/com/itau/textovoz/ui/fragment/FeedbackSearchFragment;


# direct methods
.method constructor <init>(Lbr/com/itau/textovoz/ui/fragment/FeedbackSearchFragment;)V
    .registers 2
    .param p1, "this$0"    # Lbr/com/itau/textovoz/ui/fragment/FeedbackSearchFragment;

    .line 84
    iput-object p1, p0, Lbr/com/itau/textovoz/ui/fragment/FeedbackSearchFragment$2;->this$0:Lbr/com/itau/textovoz/ui/fragment/FeedbackSearchFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .registers 4
    .param p1, "v"    # Landroid/view/View;

    .line 87
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/fragment/FeedbackSearchFragment$2;->this$0:Lbr/com/itau/textovoz/ui/fragment/FeedbackSearchFragment;

    # getter for: Lbr/com/itau/textovoz/ui/fragment/FeedbackSearchFragment;->linearFeedbackYesNoContainer:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lbr/com/itau/textovoz/ui/fragment/FeedbackSearchFragment;->access$000(Lbr/com/itau/textovoz/ui/fragment/FeedbackSearchFragment;)Landroid/widget/LinearLayout;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 88
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/fragment/FeedbackSearchFragment$2;->this$0:Lbr/com/itau/textovoz/ui/fragment/FeedbackSearchFragment;

    # getter for: Lbr/com/itau/textovoz/ui/fragment/FeedbackSearchFragment;->linearFeedbackCollaboration:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lbr/com/itau/textovoz/ui/fragment/FeedbackSearchFragment;->access$100(Lbr/com/itau/textovoz/ui/fragment/FeedbackSearchFragment;)Landroid/widget/LinearLayout;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 89
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/fragment/FeedbackSearchFragment$2;->this$0:Lbr/com/itau/textovoz/ui/fragment/FeedbackSearchFragment;

    # getter for: Lbr/com/itau/textovoz/ui/fragment/FeedbackSearchFragment;->listener:Lbr/com/itau/textovoz/ui/fragment/FeedbackSearchFragment$FeedbackOptionListener;
    invoke-static {v0}, Lbr/com/itau/textovoz/ui/fragment/FeedbackSearchFragment;->access$200(Lbr/com/itau/textovoz/ui/fragment/FeedbackSearchFragment;)Lbr/com/itau/textovoz/ui/fragment/FeedbackSearchFragment$FeedbackOptionListener;

    move-result-object v0

    if-eqz v0, :cond_26

    .line 90
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/fragment/FeedbackSearchFragment$2;->this$0:Lbr/com/itau/textovoz/ui/fragment/FeedbackSearchFragment;

    # getter for: Lbr/com/itau/textovoz/ui/fragment/FeedbackSearchFragment;->listener:Lbr/com/itau/textovoz/ui/fragment/FeedbackSearchFragment$FeedbackOptionListener;
    invoke-static {v0}, Lbr/com/itau/textovoz/ui/fragment/FeedbackSearchFragment;->access$200(Lbr/com/itau/textovoz/ui/fragment/FeedbackSearchFragment;)Lbr/com/itau/textovoz/ui/fragment/FeedbackSearchFragment$FeedbackOptionListener;

    move-result-object v0

    invoke-interface {v0}, Lbr/com/itau/textovoz/ui/fragment/FeedbackSearchFragment$FeedbackOptionListener;->onFeedbackNoClicked()V

    .line 92
    :cond_26
    return-void
.end method

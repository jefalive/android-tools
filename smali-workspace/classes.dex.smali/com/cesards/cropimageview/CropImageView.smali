.class public Lcom/cesards/cropimageview/CropImageView;
.super Landroid/widget/ImageView;
.source "CropImageView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/cesards/cropimageview/CropImageView$CropType;
    }
.end annotation


# instance fields
.field private cropType:Lcom/cesards/cropimageview/CropImageView$CropType;

.field private imageMaths:Lcom/cesards/cropimageview/ImageMaths;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .param p1, "context"    # Landroid/content/Context;

    .line 35
    invoke-direct {p0, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 32
    sget-object v0, Lcom/cesards/cropimageview/CropImageView$CropType;->NONE:Lcom/cesards/cropimageview/CropImageView$CropType;

    iput-object v0, p0, Lcom/cesards/cropimageview/CropImageView;->cropType:Lcom/cesards/cropimageview/CropImageView$CropType;

    .line 37
    invoke-direct {p0}, Lcom/cesards/cropimageview/CropImageView;->initImageView()V

    .line 38
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .line 41
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/cesards/cropimageview/CropImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 42
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .line 45
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 32
    sget-object v0, Lcom/cesards/cropimageview/CropImageView$CropType;->NONE:Lcom/cesards/cropimageview/CropImageView$CropType;

    iput-object v0, p0, Lcom/cesards/cropimageview/CropImageView;->cropType:Lcom/cesards/cropimageview/CropImageView$CropType;

    .line 47
    invoke-direct {p0, p2}, Lcom/cesards/cropimageview/CropImageView;->parseAttributes(Landroid/util/AttributeSet;)V

    .line 48
    invoke-direct {p0}, Lcom/cesards/cropimageview/CropImageView;->initImageView()V

    .line 49
    return-void
.end method

.method private computeImageMatrix()V
    .registers 16

    .line 113
    invoke-virtual {p0}, Lcom/cesards/cropimageview/CropImageView;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Lcom/cesards/cropimageview/CropImageView;->getPaddingLeft()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {p0}, Lcom/cesards/cropimageview/CropImageView;->getPaddingRight()I

    move-result v1

    sub-int v2, v0, v1

    .line 114
    .local v2, "viewWidth":I
    invoke-virtual {p0}, Lcom/cesards/cropimageview/CropImageView;->getHeight()I

    move-result v0

    invoke-virtual {p0}, Lcom/cesards/cropimageview/CropImageView;->getPaddingTop()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {p0}, Lcom/cesards/cropimageview/CropImageView;->getPaddingBottom()I

    move-result v1

    sub-int v3, v0, v1

    .line 116
    .local v3, "viewHeight":I
    iget-object v0, p0, Lcom/cesards/cropimageview/CropImageView;->cropType:Lcom/cesards/cropimageview/CropImageView$CropType;

    sget-object v1, Lcom/cesards/cropimageview/CropImageView$CropType;->NONE:Lcom/cesards/cropimageview/CropImageView$CropType;

    if-eq v0, v1, :cond_6f

    if-lez v3, :cond_6f

    if-lez v2, :cond_6f

    .line 117
    iget-object v0, p0, Lcom/cesards/cropimageview/CropImageView;->imageMaths:Lcom/cesards/cropimageview/ImageMaths;

    invoke-interface {v0}, Lcom/cesards/cropimageview/ImageMaths;->getMatrix()Landroid/graphics/Matrix;

    move-result-object v4

    .line 119
    .local v4, "matrix":Landroid/graphics/Matrix;
    invoke-virtual {p0}, Lcom/cesards/cropimageview/CropImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v5

    .line 120
    .local v5, "drawableWidth":I
    invoke-virtual {p0}, Lcom/cesards/cropimageview/CropImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v6

    .line 122
    .local v6, "drawableHeight":I
    int-to-float v0, v3

    int-to-float v1, v6

    div-float v7, v0, v1

    .line 123
    .local v7, "scaleY":F
    int-to-float v0, v2

    int-to-float v1, v5

    div-float v8, v0, v1

    .line 124
    .local v8, "scaleX":F
    cmpl-float v0, v8, v7

    if-lez v0, :cond_4c

    move v9, v8

    goto :goto_4d

    :cond_4c
    move v9, v7

    .line 125
    .local v9, "scale":F
    :goto_4d
    invoke-virtual {v4, v9, v9}, Landroid/graphics/Matrix;->setScale(FF)V

    .line 127
    cmpl-float v0, v8, v7

    if-lez v0, :cond_56

    const/4 v10, 0x1

    goto :goto_57

    :cond_56
    const/4 v10, 0x0

    .line 129
    .local v10, "verticalImageMode":Z
    :goto_57
    int-to-float v0, v5

    mul-float v11, v0, v9

    .line 130
    .local v11, "postDrawableWidth":F
    iget-object v0, p0, Lcom/cesards/cropimageview/CropImageView;->cropType:Lcom/cesards/cropimageview/CropImageView$CropType;

    invoke-direct {p0, v0, v2, v11, v10}, Lcom/cesards/cropimageview/CropImageView;->getXTranslation(Lcom/cesards/cropimageview/CropImageView$CropType;IFZ)F

    move-result v12

    .line 131
    .local v12, "xTranslation":F
    int-to-float v0, v6

    mul-float v13, v0, v9

    .line 132
    .local v13, "postDrawabeHeigth":F
    iget-object v0, p0, Lcom/cesards/cropimageview/CropImageView;->cropType:Lcom/cesards/cropimageview/CropImageView$CropType;

    invoke-direct {p0, v0, v3, v13, v10}, Lcom/cesards/cropimageview/CropImageView;->getYTranslation(Lcom/cesards/cropimageview/CropImageView$CropType;IFZ)F

    move-result v14

    .line 134
    .local v14, "yTranslation":F
    invoke-virtual {v4, v12, v14}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 135
    invoke-virtual {p0, v4}, Lcom/cesards/cropimageview/CropImageView;->setImageMatrix(Landroid/graphics/Matrix;)V

    .line 137
    .end local v4    # "matrix":Landroid/graphics/Matrix;
    .end local v5    # "drawableWidth":I
    .end local v6    # "drawableHeight":I
    .end local v7    # "scaleY":F
    .end local v8    # "scaleX":F
    .end local v9    # "scale":F
    .end local v10    # "verticalImageMode":Z
    .end local v11    # "postDrawableWidth":F
    .end local v12    # "xTranslation":F
    .end local v13    # "postDrawabeHeigth":F
    .end local v14    # "yTranslation":F
    :cond_6f
    return-void
.end method

.method private getXTranslation(Lcom/cesards/cropimageview/CropImageView$CropType;IFZ)F
    .registers 7
    .param p1, "cropType"    # Lcom/cesards/cropimageview/CropImageView$CropType;
    .param p2, "viewWidth"    # I
    .param p3, "postDrawableWidth"    # F
    .param p4, "verticalImageMode"    # Z

    .line 158
    if-nez p4, :cond_17

    .line 159
    sget-object v0, Lcom/cesards/cropimageview/CropImageView$1;->$SwitchMap$com$cesards$cropimageview$CropImageView$CropType:[I

    invoke-virtual {p1}, Lcom/cesards/cropimageview/CropImageView$CropType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_1a

    goto :goto_17

    .line 163
    :pswitch_e
    int-to-float v0, p2

    sub-float/2addr v0, p3

    return v0

    .line 167
    :pswitch_11
    int-to-float v0, p2

    sub-float/2addr v0, p3

    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr v0, v1

    return v0

    .line 171
    :cond_17
    :goto_17
    :pswitch_17
    const/4 v0, 0x0

    return v0

    nop

    :pswitch_data_1a
    .packed-switch 0x1
        :pswitch_11
        :pswitch_17
        :pswitch_e
        :pswitch_17
        :pswitch_e
        :pswitch_e
        :pswitch_11
    .end packed-switch
.end method

.method private getYTranslation(Lcom/cesards/cropimageview/CropImageView$CropType;IFZ)F
    .registers 7
    .param p1, "cropType"    # Lcom/cesards/cropimageview/CropImageView$CropType;
    .param p2, "viewHeight"    # I
    .param p3, "postDrawabeHeigth"    # F
    .param p4, "verticalImageMode"    # Z

    .line 140
    if-eqz p4, :cond_17

    .line 141
    sget-object v0, Lcom/cesards/cropimageview/CropImageView$1;->$SwitchMap$com$cesards$cropimageview$CropImageView$CropType:[I

    invoke-virtual {p1}, Lcom/cesards/cropimageview/CropImageView$CropType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_1a

    goto :goto_17

    .line 145
    :pswitch_e
    int-to-float v0, p2

    sub-float/2addr v0, p3

    return v0

    .line 149
    :pswitch_11
    int-to-float v0, p2

    sub-float/2addr v0, p3

    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr v0, v1

    return v0

    .line 154
    :cond_17
    :goto_17
    const/4 v0, 0x0

    return v0

    nop

    :pswitch_data_1a
    .packed-switch 0x1
        :pswitch_e
        :pswitch_e
        :pswitch_e
        :pswitch_11
        :pswitch_11
    .end packed-switch
.end method

.method private initImageView()V
    .registers 2

    .line 89
    new-instance v0, Lcom/cesards/cropimageview/ImageMathsFactory;

    invoke-direct {v0}, Lcom/cesards/cropimageview/ImageMathsFactory;-><init>()V

    invoke-virtual {v0, p0}, Lcom/cesards/cropimageview/ImageMathsFactory;->getImageMaths(Lcom/cesards/cropimageview/CropImageView;)Lcom/cesards/cropimageview/ImageMaths;

    move-result-object v0

    iput-object v0, p0, Lcom/cesards/cropimageview/CropImageView;->imageMaths:Lcom/cesards/cropimageview/ImageMaths;

    .line 90
    return-void
.end method

.method private parseAttributes(Landroid/util/AttributeSet;)V
    .registers 6
    .param p1, "attrs"    # Landroid/util/AttributeSet;

    .line 93
    invoke-virtual {p0}, Lcom/cesards/cropimageview/CropImageView;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, Lcom/cesards/cropimageview/R$styleable;->CropImageView:[I

    invoke-virtual {v0, p1, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v2

    .line 95
    .local v2, "a":Landroid/content/res/TypedArray;
    sget v0, Lcom/cesards/cropimageview/R$styleable;->CropImageView_crop:I

    sget-object v1, Lcom/cesards/cropimageview/CropImageView$CropType;->NONE:Lcom/cesards/cropimageview/CropImageView$CropType;

    invoke-virtual {v1}, Lcom/cesards/cropimageview/CropImageView$CropType;->getCrop()I

    move-result v1

    invoke-virtual {v2, v0, v1}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v3

    .line 96
    .local v3, "crop":I
    if-ltz v3, :cond_23

    .line 97
    sget-object v0, Landroid/widget/ImageView$ScaleType;->MATRIX:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {p0, v0}, Lcom/cesards/cropimageview/CropImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 98
    invoke-static {v3}, Lcom/cesards/cropimageview/CropImageView$CropType;->get(I)Lcom/cesards/cropimageview/CropImageView$CropType;

    move-result-object v0

    iput-object v0, p0, Lcom/cesards/cropimageview/CropImageView;->cropType:Lcom/cesards/cropimageview/CropImageView$CropType;

    .line 100
    :cond_23
    invoke-virtual {v2}, Landroid/content/res/TypedArray;->recycle()V

    .line 101
    return-void
.end method


# virtual methods
.method public getCropType()Lcom/cesards/cropimageview/CropImageView$CropType;
    .registers 2

    .line 85
    iget-object v0, p0, Lcom/cesards/cropimageview/CropImageView;->cropType:Lcom/cesards/cropimageview/CropImageView$CropType;

    return-object v0
.end method

.method public setCropType(Lcom/cesards/cropimageview/CropImageView$CropType;)V
    .registers 3
    .param p1, "cropType"    # Lcom/cesards/cropimageview/CropImageView$CropType;

    .line 65
    if-nez p1, :cond_8

    .line 66
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 69
    :cond_8
    iget-object v0, p0, Lcom/cesards/cropimageview/CropImageView;->cropType:Lcom/cesards/cropimageview/CropImageView$CropType;

    if-eq v0, p1, :cond_18

    .line 70
    iput-object p1, p0, Lcom/cesards/cropimageview/CropImageView;->cropType:Lcom/cesards/cropimageview/CropImageView$CropType;

    .line 72
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/cesards/cropimageview/CropImageView;->setWillNotCacheDrawing(Z)V

    .line 74
    invoke-virtual {p0}, Lcom/cesards/cropimageview/CropImageView;->requestLayout()V

    .line 75
    invoke-virtual {p0}, Lcom/cesards/cropimageview/CropImageView;->invalidate()V

    .line 77
    :cond_18
    return-void
.end method

.method protected setFrame(IIII)Z
    .registers 7
    .param p1, "l"    # I
    .param p2, "t"    # I
    .param p3, "r"    # I
    .param p4, "b"    # I

    .line 105
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/ImageView;->setFrame(IIII)Z

    move-result v1

    .line 106
    .local v1, "changed":Z
    invoke-virtual {p0}, Lcom/cesards/cropimageview/CropImageView;->isInEditMode()Z

    move-result v0

    if-nez v0, :cond_d

    .line 107
    invoke-direct {p0}, Lcom/cesards/cropimageview/CropImageView;->computeImageMatrix()V

    .line 109
    :cond_d
    return v1
.end method

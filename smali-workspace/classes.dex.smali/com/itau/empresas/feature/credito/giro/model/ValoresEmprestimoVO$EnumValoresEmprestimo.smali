.class public final enum Lcom/itau/empresas/feature/credito/giro/model/ValoresEmprestimoVO$EnumValoresEmprestimo;
.super Ljava/lang/Enum;
.source "ValoresEmprestimoVO.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/itau/empresas/feature/credito/giro/model/ValoresEmprestimoVO;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "EnumValoresEmprestimo"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<Lcom/itau/empresas/feature/credito/giro/model/ValoresEmprestimoVO$EnumValoresEmprestimo;>;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/itau/empresas/feature/credito/giro/model/ValoresEmprestimoVO$EnumValoresEmprestimo;

.field public static final enum CARREGADO_DA_API:Lcom/itau/empresas/feature/credito/giro/model/ValoresEmprestimoVO$EnumValoresEmprestimo;

.field public static final enum CARREGADO_MANUALMENTE:Lcom/itau/empresas/feature/credito/giro/model/ValoresEmprestimoVO$EnumValoresEmprestimo;

.field public static final enum OUTRO_VALOR:Lcom/itau/empresas/feature/credito/giro/model/ValoresEmprestimoVO$EnumValoresEmprestimo;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .line 84
    new-instance v0, Lcom/itau/empresas/feature/credito/giro/model/ValoresEmprestimoVO$EnumValoresEmprestimo;

    const-string v1, "CARREGADO_DA_API"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/itau/empresas/feature/credito/giro/model/ValoresEmprestimoVO$EnumValoresEmprestimo;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/itau/empresas/feature/credito/giro/model/ValoresEmprestimoVO$EnumValoresEmprestimo;->CARREGADO_DA_API:Lcom/itau/empresas/feature/credito/giro/model/ValoresEmprestimoVO$EnumValoresEmprestimo;

    .line 85
    new-instance v0, Lcom/itau/empresas/feature/credito/giro/model/ValoresEmprestimoVO$EnumValoresEmprestimo;

    const-string v1, "CARREGADO_MANUALMENTE"

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lcom/itau/empresas/feature/credito/giro/model/ValoresEmprestimoVO$EnumValoresEmprestimo;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/itau/empresas/feature/credito/giro/model/ValoresEmprestimoVO$EnumValoresEmprestimo;->CARREGADO_MANUALMENTE:Lcom/itau/empresas/feature/credito/giro/model/ValoresEmprestimoVO$EnumValoresEmprestimo;

    .line 86
    new-instance v0, Lcom/itau/empresas/feature/credito/giro/model/ValoresEmprestimoVO$EnumValoresEmprestimo;

    const-string v1, "OUTRO_VALOR"

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2}, Lcom/itau/empresas/feature/credito/giro/model/ValoresEmprestimoVO$EnumValoresEmprestimo;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/itau/empresas/feature/credito/giro/model/ValoresEmprestimoVO$EnumValoresEmprestimo;->OUTRO_VALOR:Lcom/itau/empresas/feature/credito/giro/model/ValoresEmprestimoVO$EnumValoresEmprestimo;

    .line 83
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/itau/empresas/feature/credito/giro/model/ValoresEmprestimoVO$EnumValoresEmprestimo;

    sget-object v1, Lcom/itau/empresas/feature/credito/giro/model/ValoresEmprestimoVO$EnumValoresEmprestimo;->CARREGADO_DA_API:Lcom/itau/empresas/feature/credito/giro/model/ValoresEmprestimoVO$EnumValoresEmprestimo;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sget-object v1, Lcom/itau/empresas/feature/credito/giro/model/ValoresEmprestimoVO$EnumValoresEmprestimo;->CARREGADO_MANUALMENTE:Lcom/itau/empresas/feature/credito/giro/model/ValoresEmprestimoVO$EnumValoresEmprestimo;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    sget-object v1, Lcom/itau/empresas/feature/credito/giro/model/ValoresEmprestimoVO$EnumValoresEmprestimo;->OUTRO_VALOR:Lcom/itau/empresas/feature/credito/giro/model/ValoresEmprestimoVO$EnumValoresEmprestimo;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    sput-object v0, Lcom/itau/empresas/feature/credito/giro/model/ValoresEmprestimoVO$EnumValoresEmprestimo;->$VALUES:[Lcom/itau/empresas/feature/credito/giro/model/ValoresEmprestimoVO$EnumValoresEmprestimo;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 83
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/itau/empresas/feature/credito/giro/model/ValoresEmprestimoVO$EnumValoresEmprestimo;
    .registers 2
    .param p0, "name"    # Ljava/lang/String;

    .line 83
    const-class v0, Lcom/itau/empresas/feature/credito/giro/model/ValoresEmprestimoVO$EnumValoresEmprestimo;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/feature/credito/giro/model/ValoresEmprestimoVO$EnumValoresEmprestimo;

    return-object v0
.end method

.method public static values()[Lcom/itau/empresas/feature/credito/giro/model/ValoresEmprestimoVO$EnumValoresEmprestimo;
    .registers 1

    .line 83
    sget-object v0, Lcom/itau/empresas/feature/credito/giro/model/ValoresEmprestimoVO$EnumValoresEmprestimo;->$VALUES:[Lcom/itau/empresas/feature/credito/giro/model/ValoresEmprestimoVO$EnumValoresEmprestimo;

    invoke-virtual {v0}, [Lcom/itau/empresas/feature/credito/giro/model/ValoresEmprestimoVO$EnumValoresEmprestimo;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/itau/empresas/feature/credito/giro/model/ValoresEmprestimoVO$EnumValoresEmprestimo;

    return-object v0
.end method

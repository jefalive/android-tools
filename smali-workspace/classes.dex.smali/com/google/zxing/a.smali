.class public final enum Lcom/google/zxing/a;
.super Ljava/lang/Enum;


# static fields
.field public static final enum a:Lcom/google/zxing/a;

.field private static final synthetic b:[Lcom/google/zxing/a;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    new-instance v0, Lcom/google/zxing/a;

    const-string v1, "ITF"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/google/zxing/a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/zxing/a;->a:Lcom/google/zxing/a;

    const/4 v0, 0x1

    new-array v0, v0, [Lcom/google/zxing/a;

    sget-object v1, Lcom/google/zxing/a;->a:Lcom/google/zxing/a;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sput-object v0, Lcom/google/zxing/a;->b:[Lcom/google/zxing/a;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/zxing/a;
    .registers 2

    const-class v0, Lcom/google/zxing/a;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/zxing/a;

    return-object v0
.end method

.method public static values()[Lcom/google/zxing/a;
    .registers 1

    sget-object v0, Lcom/google/zxing/a;->b:[Lcom/google/zxing/a;

    invoke-virtual {v0}, [Lcom/google/zxing/a;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/zxing/a;

    return-object v0
.end method

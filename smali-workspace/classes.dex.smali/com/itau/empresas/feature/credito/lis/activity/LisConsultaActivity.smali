.class public Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaActivity;
.super Lcom/itau/empresas/ui/activity/BaseActivity;
.source "LisConsultaActivity.java"


# instance fields
.field cardView:Landroid/view/View;

.field controller:Lcom/itau/empresas/feature/credito/lis/LisController;

.field creditoProdutosController:Lcom/itau/empresas/controller/CreditoProdutosController;

.field emptyStateView:Landroid/view/ViewGroup;

.field private idTitle:I

.field mainLayout:Landroid/widget/RelativeLayout;

.field private respostaConsultaVO:Lcom/itau/empresas/feature/credito/lis/model/LisConsultaDetalheRespostaVO;

.field textoDiaPagamento:Landroid/widget/TextView;

.field textoValorLimite:Landroid/widget/TextView;

.field tituloComprovante:Landroid/widget/TextView;

.field toolBar:Landroid/support/v7/widget/Toolbar;


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 39
    invoke-direct {p0}, Lcom/itau/empresas/ui/activity/BaseActivity;-><init>()V

    return-void
.end method

.method private constroiToolbar()V
    .registers 3

    .line 149
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaActivity;->toolBar:Landroid/support/v7/widget/Toolbar;

    invoke-static {v0, p0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom;->Builder(Landroid/support/v7/widget/Toolbar;Landroid/support/v7/app/AppCompatActivity;)Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    move-result-object v0

    iget v1, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaActivity;->idTitle:I

    .line 150
    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->comTitulo(I)Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    move-result-object v0

    .line 151
    invoke-virtual {v0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->comClickListenerFinish()Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    move-result-object v0

    .line 152
    invoke-virtual {v0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->build()Lcom/itau/empresas/ui/util/listener/ToolbarCustom;

    .line 153
    return-void
.end method

.method private fluxoConsultaLis()V
    .registers 7

    .line 176
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaActivity;->creditoProdutosController:Lcom/itau/empresas/controller/CreditoProdutosController;

    const-string v1, "J"

    iget-object v2, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaActivity;->application:Lcom/itau/empresas/CustomApplication;

    .line 177
    invoke-virtual {v2}, Lcom/itau/empresas/CustomApplication;->getDadosOperador()Lcom/itau/empresas/api/model/DadosOperadorVO;

    move-result-object v2

    invoke-virtual {v2}, Lcom/itau/empresas/api/model/DadosOperadorVO;->getNumeroDocumento()Ljava/lang/String;

    move-result-object v2

    const-string v3, "rotativos"

    const-string v4, "cheque_especial"

    const-string v5, "consulta"

    .line 176
    invoke-virtual/range {v0 .. v5}, Lcom/itau/empresas/controller/CreditoProdutosController;->buscaLimiteProduto(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 179
    return-void
.end method

.method private fluxoContratacaoLIS()V
    .registers 5

    .line 170
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaActivity;->controller:Lcom/itau/empresas/feature/credito/lis/LisController;

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaActivity;->application:Lcom/itau/empresas/CustomApplication;

    .line 171
    invoke-virtual {v1}, Lcom/itau/empresas/CustomApplication;->getDadosOperador()Lcom/itau/empresas/api/model/DadosOperadorVO;

    move-result-object v1

    invoke-virtual {v1}, Lcom/itau/empresas/api/model/DadosOperadorVO;->getNumeroDocumento()Ljava/lang/String;

    move-result-object v1

    const-string v2, "L01"

    const-string v3, "contratacao"

    invoke-virtual {v0, v1, v2, v3}, Lcom/itau/empresas/feature/credito/lis/LisController;->buscaOfertaChequeEspecial(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 173
    return-void
.end method

.method private iniciaLayout()V
    .registers 4

    .line 156
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaActivity;->tituloComprovante:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaActivity;->respostaConsultaVO:Lcom/itau/empresas/feature/credito/lis/model/LisConsultaDetalheRespostaVO;

    invoke-virtual {v1}, Lcom/itau/empresas/feature/credito/lis/model/LisConsultaDetalheRespostaVO;->getNomeProduto()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 157
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaActivity;->textoValorLimite:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaActivity;->respostaConsultaVO:Lcom/itau/empresas/feature/credito/lis/model/LisConsultaDetalheRespostaVO;

    .line 158
    invoke-virtual {v1}, Lcom/itau/empresas/feature/credito/lis/model/LisConsultaDetalheRespostaVO;->getLimiteContratado()F

    move-result v1

    sget-object v2, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    .line 159
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    .line 158
    invoke-static {v1, v2}, Lcom/itau/empresas/ui/util/ValorMonetarioUtils;->formataValorDecimalParaMonetario(FZ)Ljava/lang/String;

    move-result-object v1

    .line 157
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 160
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaActivity;->textoDiaPagamento:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaActivity;->respostaConsultaVO:Lcom/itau/empresas/feature/credito/lis/model/LisConsultaDetalheRespostaVO;

    .line 161
    invoke-virtual {v1}, Lcom/itau/empresas/feature/credito/lis/model/LisConsultaDetalheRespostaVO;->getDataVencimentoOperacao()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/itau/empresas/ui/util/DataEHoraUtils;->formataDataComBarra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 160
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 162
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaActivity;->toolBar:Landroid/support/v7/widget/Toolbar;

    invoke-static {v0, p0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom;->Builder(Landroid/support/v7/widget/Toolbar;Landroid/support/v7/app/AppCompatActivity;)Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaActivity;->respostaConsultaVO:Lcom/itau/empresas/feature/credito/lis/model/LisConsultaDetalheRespostaVO;

    .line 163
    invoke-virtual {v1}, Lcom/itau/empresas/feature/credito/lis/model/LisConsultaDetalheRespostaVO;->getNomeProduto()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->comTitulo(Ljava/lang/String;)Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    move-result-object v0

    .line 164
    invoke-virtual {v0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->comClickListenerFinish()Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    move-result-object v0

    .line 165
    invoke-virtual {v0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->build()Lcom/itau/empresas/ui/util/listener/ToolbarCustom;

    .line 166
    return-void
.end method


# virtual methods
.method aoCarregarTela()V
    .registers 3

    .line 131
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaActivity;->mainLayout:Landroid/widget/RelativeLayout;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 132
    const v0, 0x7f070404

    iput v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaActivity;->idTitle:I

    .line 133
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaActivity;->constroiToolbar()V

    .line 134
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaActivity;->mostraDialogoDeProgresso()V

    .line 135
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaActivity;->controller:Lcom/itau/empresas/feature/credito/lis/LisController;

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaActivity;->application:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v1}, Lcom/itau/empresas/CustomApplication;->getDadosOperador()Lcom/itau/empresas/api/model/DadosOperadorVO;

    move-result-object v1

    invoke-virtual {v1}, Lcom/itau/empresas/api/model/DadosOperadorVO;->getNumeroDocumento()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/credito/lis/LisController;->buscaOfertaResumo(Ljava/lang/String;)V

    .line 136
    return-void
.end method

.method clickDetalhe()V
    .registers 4

    .line 141
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaActivity;->respostaConsultaVO:Lcom/itau/empresas/feature/credito/lis/model/LisConsultaDetalheRespostaVO;

    if-eqz v0, :cond_1c

    .line 142
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaActivity;->mostraDialogoDeProgresso()V

    .line 143
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaActivity;->controller:Lcom/itau/empresas/feature/credito/lis/LisController;

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaActivity;->respostaConsultaVO:Lcom/itau/empresas/feature/credito/lis/model/LisConsultaDetalheRespostaVO;

    invoke-virtual {v1}, Lcom/itau/empresas/feature/credito/lis/model/LisConsultaDetalheRespostaVO;->getCodigoProduto()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaActivity;->application:Lcom/itau/empresas/CustomApplication;

    .line 144
    invoke-virtual {v2}, Lcom/itau/empresas/CustomApplication;->getDadosOperador()Lcom/itau/empresas/api/model/DadosOperadorVO;

    move-result-object v2

    invoke-virtual {v2}, Lcom/itau/empresas/api/model/DadosOperadorVO;->getNumeroDocumento()Ljava/lang/String;

    move-result-object v2

    .line 143
    invoke-virtual {v0, v1, v2}, Lcom/itau/empresas/feature/credito/lis/LisController;->buscaConsultaDetalha(Ljava/lang/String;Ljava/lang/String;)V

    .line 146
    :cond_1c
    return-void
.end method

.method public hitAnalytics()Ljava/lang/String;
    .registers 2

    .line 126
    const v0, 0x7f070334

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public onEventMainThread(Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;)V
    .registers 6
    .param p1, "e"    # Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;

    .line 111
    invoke-static {p1, p0}, Lcom/itau/empresas/api/ApiHelper;->devoTratarEvento(Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;Lcom/itau/empresas/api/ApiConsumidor;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 112
    return-void

    .line 115
    :cond_7
    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;->getException()Lbr/com/itau/sdk/android/core/exception/BackendException;

    move-result-object v0

    invoke-virtual {v0}, Lbr/com/itau/sdk/android/core/exception/BackendException;->getError()Lbr/com/itau/sdk/android/core/model/ErrorDTO;

    move-result-object v2

    .line 116
    .local v2, "errorDTO":Lbr/com/itau/sdk/android/core/model/ErrorDTO;
    if-eqz v2, :cond_16

    invoke-virtual {v2}, Lbr/com/itau/sdk/android/core/model/ErrorDTO;->getMensagem()Ljava/lang/String;

    move-result-object v3

    goto :goto_1d

    .line 117
    :cond_16
    const v0, 0x7f0701f2

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 118
    .local v3, "mensagem":Ljava/lang/String;
    :goto_1d
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaActivity;->emptyStateView:Landroid/view/ViewGroup;

    const v1, 0x7f07025b

    invoke-static {p0, v0, v3, v1}, Lcom/itau/empresas/ui/util/EmptyStateUtil;->mostraEmptyState(Landroid/content/Context;Landroid/view/ViewGroup;Ljava/lang/String;I)V

    .line 119
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaActivity;->mainLayout:Landroid/widget/RelativeLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 120
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaActivity;->cardView:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 121
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaActivity;->escondeDialogoDeProgresso()V

    .line 122
    return-void
.end method

.method public onEventMainThread(Lcom/itau/empresas/Evento$EventoLisConsulta;)V
    .registers 4
    .param p1, "eventoLisConsulta"    # Lcom/itau/empresas/Evento$EventoLisConsulta;

    .line 78
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaActivity;->mainLayout:Landroid/widget/RelativeLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 79
    invoke-virtual {p1}, Lcom/itau/empresas/Evento$EventoLisConsulta;->getProdutoLimite()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/feature/credito/lis/model/LisConsultaDetalheRespostaVO;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaActivity;->respostaConsultaVO:Lcom/itau/empresas/feature/credito/lis/model/LisConsultaDetalheRespostaVO;

    .line 80
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaActivity;->iniciaLayout()V

    .line 81
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaActivity;->escondeDialogoDeProgresso()V

    .line 82
    return-void
.end method

.method public onEventMainThread(Lcom/itau/empresas/feature/credito/lis/model/LisConsultaDetalhadaRespostaVO;)V
    .registers 4
    .param p1, "lisConsultaDetalhadaRespostaVO"    # Lcom/itau/empresas/feature/credito/lis/model/LisConsultaDetalhadaRespostaVO;

    .line 85
    .line 86
    invoke-static {p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity;->intent(Landroid/content/Context;)Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity_$IntentBuilder_;

    move-result-object v0

    .line 87
    invoke-virtual {v0, p1}, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity_$IntentBuilder_;->lisConsultaDetalhadaRespostaVO(Lcom/itau/empresas/feature/credito/lis/model/LisConsultaDetalhadaRespostaVO;)Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity_$IntentBuilder_;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaActivity;->respostaConsultaVO:Lcom/itau/empresas/feature/credito/lis/model/LisConsultaDetalheRespostaVO;

    .line 88
    invoke-virtual {v1}, Lcom/itau/empresas/feature/credito/lis/model/LisConsultaDetalheRespostaVO;->getCodigoProduto()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity_$IntentBuilder_;->tipoComprovante(Ljava/lang/String;)Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity_$IntentBuilder_;

    move-result-object v0

    .line 89
    invoke-virtual {v0}, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity_$IntentBuilder_;->start()Lorg/androidannotations/api/builder/PostActivityStarter;

    .line 90
    return-void
.end method

.method public onEventMainThread(Lcom/itau/empresas/feature/credito/lis/model/OfertaContratacaoLisVo;)V
    .registers 4
    .param p1, "ofertaContratacaoLisVo"    # Lcom/itau/empresas/feature/credito/lis/model/OfertaContratacaoLisVo;

    .line 102
    .line 103
    invoke-static {p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisIntroducaoActivity;->intent(Landroid/content/Context;)Lcom/itau/empresas/feature/credito/lis/activity/LisIntroducaoActivity_$IntentBuilder_;

    move-result-object v0

    .line 104
    invoke-virtual {v0, p1}, Lcom/itau/empresas/feature/credito/lis/activity/LisIntroducaoActivity_$IntentBuilder_;->ofertaContratacaoLisVo(Lcom/itau/empresas/feature/credito/lis/model/OfertaContratacaoLisVo;)Lcom/itau/empresas/feature/credito/lis/activity/LisIntroducaoActivity_$IntentBuilder_;

    move-result-object v0

    .line 105
    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/credito/lis/activity/LisIntroducaoActivity_$IntentBuilder_;->startForResult(I)Lorg/androidannotations/api/builder/PostActivityStarter;

    .line 106
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaActivity;->finish()V

    .line 107
    return-void
.end method

.method public onEventMainThread(Lcom/itau/empresas/feature/credito/lis/model/OfertaResumoLisVO;)V
    .registers 3
    .param p1, "ofertaResumo"    # Lcom/itau/empresas/feature/credito/lis/model/OfertaResumoLisVO;

    .line 94
    invoke-virtual {p1}, Lcom/itau/empresas/feature/credito/lis/model/OfertaResumoLisVO;->isIndicadorContratacao()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 95
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaActivity;->fluxoContratacaoLIS()V

    goto :goto_d

    .line 97
    :cond_a
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaActivity;->fluxoConsultaLis()V

    .line 99
    :goto_d
    return-void
.end method

.method public opkeysConsumidas()Ljava/util/List;
    .registers 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Ljava/lang/String;>;"
        }
    .end annotation

    .line 70
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "creditoRotativoChequeEspecialOfertasResumo"

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const-string v1, "creditoRotativoChequeEspecialOfertas"

    const/4 v2, 0x1

    aput-object v1, v0, v2

    const-string v1, "limitesProduto"

    const/4 v2, 0x2

    aput-object v1, v0, v2

    const-string v1, "creditoRotativoChequeEspecialConsultar"

    const/4 v2, 0x3

    aput-object v1, v0, v2

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.class public final Lcom/itau/empresas/feature/itoken/fragment/ITokenInstaladoFragment_;
.super Lcom/itau/empresas/feature/itoken/fragment/ITokenInstaladoFragment;
.source "ITokenInstaladoFragment_.java"

# interfaces
.implements Lorg/androidannotations/api/view/HasViews;
.implements Lorg/androidannotations/api/view/OnViewChangedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/itau/empresas/feature/itoken/fragment/ITokenInstaladoFragment_$FragmentBuilder_;
    }
.end annotation


# instance fields
.field private contentView_:Landroid/view/View;

.field private final onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;


# direct methods
.method public constructor <init>()V
    .registers 2

    .line 25
    invoke-direct {p0}, Lcom/itau/empresas/feature/itoken/fragment/ITokenInstaladoFragment;-><init>()V

    .line 29
    new-instance v0, Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-direct {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/feature/itoken/fragment/ITokenInstaladoFragment_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    return-void
.end method

.method public static builder()Lcom/itau/empresas/feature/itoken/fragment/ITokenInstaladoFragment_$FragmentBuilder_;
    .registers 1

    .line 80
    new-instance v0, Lcom/itau/empresas/feature/itoken/fragment/ITokenInstaladoFragment_$FragmentBuilder_;

    invoke-direct {v0}, Lcom/itau/empresas/feature/itoken/fragment/ITokenInstaladoFragment_$FragmentBuilder_;-><init>()V

    return-object v0
.end method

.method private init_(Landroid/os/Bundle;)V
    .registers 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 68
    invoke-static {p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->registerOnViewChangedListener(Lorg/androidannotations/api/view/OnViewChangedListener;)V

    .line 69
    invoke-static {}, Lcom/itau/empresas/CustomApplication_;->getInstance()Lcom/itau/empresas/CustomApplication;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/itoken/fragment/ITokenInstaladoFragment_;->application:Lcom/itau/empresas/CustomApplication;

    .line 70
    invoke-virtual {p0}, Lcom/itau/empresas/feature/itoken/fragment/ITokenInstaladoFragment_;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/itau/empresas/controller/ITokenController_;->getInstance_(Landroid/content/Context;)Lcom/itau/empresas/controller/ITokenController_;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/itoken/fragment/ITokenInstaladoFragment_;->controller:Lcom/itau/empresas/controller/ITokenController;

    .line 71
    return-void
.end method


# virtual methods
.method public findViewById(I)Landroid/view/View;
    .registers 3
    .param p1, "id"    # I

    .line 42
    iget-object v0, p0, Lcom/itau/empresas/feature/itoken/fragment/ITokenInstaladoFragment_;->contentView_:Landroid/view/View;

    if-nez v0, :cond_6

    .line 43
    const/4 v0, 0x0

    return-object v0

    .line 45
    :cond_6
    iget-object v0, p0, Lcom/itau/empresas/feature/itoken/fragment/ITokenInstaladoFragment_;->contentView_:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 34
    iget-object v0, p0, Lcom/itau/empresas/feature/itoken/fragment/ITokenInstaladoFragment_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-static {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    move-result-object v1

    .line 35
    .local v1, "previousNotifier":Lorg/androidannotations/api/view/OnViewChangedNotifier;
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/itoken/fragment/ITokenInstaladoFragment_;->init_(Landroid/os/Bundle;)V

    .line 36
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/itoken/fragment/ITokenInstaladoFragment;->onCreate(Landroid/os/Bundle;)V

    .line 37
    invoke-static {v1}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 38
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .registers 6
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .line 50
    invoke-super {p0, p1, p2, p3}, Lcom/itau/empresas/feature/itoken/fragment/ITokenInstaladoFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/itoken/fragment/ITokenInstaladoFragment_;->contentView_:Landroid/view/View;

    .line 51
    iget-object v0, p0, Lcom/itau/empresas/feature/itoken/fragment/ITokenInstaladoFragment_;->contentView_:Landroid/view/View;

    if-nez v0, :cond_14

    .line 52
    const v0, 0x7f0300ad

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/itoken/fragment/ITokenInstaladoFragment_;->contentView_:Landroid/view/View;

    .line 54
    :cond_14
    iget-object v0, p0, Lcom/itau/empresas/feature/itoken/fragment/ITokenInstaladoFragment_;->contentView_:Landroid/view/View;

    return-object v0
.end method

.method public onDestroyView()V
    .registers 2

    .line 59
    invoke-super {p0}, Lcom/itau/empresas/feature/itoken/fragment/ITokenInstaladoFragment;->onDestroyView()V

    .line 60
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/itau/empresas/feature/itoken/fragment/ITokenInstaladoFragment_;->contentView_:Landroid/view/View;

    .line 61
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/itau/empresas/feature/itoken/fragment/ITokenInstaladoFragment_;->progressTempoToken:Landroid/widget/ProgressBar;

    .line 62
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/itau/empresas/feature/itoken/fragment/ITokenInstaladoFragment_;->textoNumeroToken:Landroid/widget/TextView;

    .line 63
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/itau/empresas/feature/itoken/fragment/ITokenInstaladoFragment_;->textoNumeroSerie:Landroid/widget/TextView;

    .line 64
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/itau/empresas/feature/itoken/fragment/ITokenInstaladoFragment_;->textoNumero:Landroid/widget/TextView;

    .line 65
    return-void
.end method

.method public onViewChanged(Lorg/androidannotations/api/view/HasViews;)V
    .registers 6
    .param p1, "hasViews"    # Lorg/androidannotations/api/view/HasViews;

    .line 85
    const v0, 0x7f0e0427

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/itau/empresas/feature/itoken/fragment/ITokenInstaladoFragment_;->progressTempoToken:Landroid/widget/ProgressBar;

    .line 86
    const v0, 0x7f0e027a

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/itoken/fragment/ITokenInstaladoFragment_;->textoNumeroToken:Landroid/widget/TextView;

    .line 87
    const v0, 0x7f0e0428

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/itoken/fragment/ITokenInstaladoFragment_;->textoNumeroSerie:Landroid/widget/TextView;

    .line 88
    const v0, 0x7f0e0279

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/itoken/fragment/ITokenInstaladoFragment_;->textoNumero:Landroid/widget/TextView;

    .line 89
    const v0, 0x7f0e0422

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 90
    .local v1, "view_botao_orientacoes_seguranca":Landroid/view/View;
    const v0, 0x7f0e0424

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 91
    .local v2, "view_botao_inicio":Landroid/view/View;
    const v0, 0x7f0e0425

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 93
    .local v3, "view_botao_desinstalar_itoken":Landroid/view/View;
    if-eqz v1, :cond_4b

    .line 94
    new-instance v0, Lcom/itau/empresas/feature/itoken/fragment/ITokenInstaladoFragment_$1;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/itoken/fragment/ITokenInstaladoFragment_$1;-><init>(Lcom/itau/empresas/feature/itoken/fragment/ITokenInstaladoFragment_;)V

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 103
    :cond_4b
    if-eqz v2, :cond_55

    .line 104
    new-instance v0, Lcom/itau/empresas/feature/itoken/fragment/ITokenInstaladoFragment_$2;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/itoken/fragment/ITokenInstaladoFragment_$2;-><init>(Lcom/itau/empresas/feature/itoken/fragment/ITokenInstaladoFragment_;)V

    invoke-virtual {v2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 113
    :cond_55
    if-eqz v3, :cond_5f

    .line 114
    new-instance v0, Lcom/itau/empresas/feature/itoken/fragment/ITokenInstaladoFragment_$3;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/itoken/fragment/ITokenInstaladoFragment_$3;-><init>(Lcom/itau/empresas/feature/itoken/fragment/ITokenInstaladoFragment_;)V

    invoke-virtual {v3, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 123
    :cond_5f
    invoke-virtual {p0}, Lcom/itau/empresas/feature/itoken/fragment/ITokenInstaladoFragment_;->aoCarregar()V

    .line 124
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .registers 4
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;

    .line 75
    invoke-super {p0, p1, p2}, Lcom/itau/empresas/feature/itoken/fragment/ITokenInstaladoFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 76
    iget-object v0, p0, Lcom/itau/empresas/feature/itoken/fragment/ITokenInstaladoFragment_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 77
    return-void
.end method

.class public Lcom/itau/empresas/api/model/TaxasLimiteVO;
.super Ljava/lang/Object;
.source "TaxasLimiteVO.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private taxaCetAnual:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "taxa_cet_anual"
    .end annotation
.end field

.field private taxaCetMensal:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "taxa_cet_mensal"
    .end annotation
.end field

.field private taxaContratoAnual:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "taxa_contrato_anual"
    .end annotation
.end field

.field private taxaContratoMensal:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "taxa_contrato_mensal"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getTaxaCetAnual()Ljava/lang/String;
    .registers 2

    .line 34
    iget-object v0, p0, Lcom/itau/empresas/api/model/TaxasLimiteVO;->taxaCetAnual:Ljava/lang/String;

    return-object v0
.end method

.method public getTaxaCetMensal()Ljava/lang/String;
    .registers 2

    .line 30
    iget-object v0, p0, Lcom/itau/empresas/api/model/TaxasLimiteVO;->taxaCetMensal:Ljava/lang/String;

    return-object v0
.end method

.method public getTaxaContratoAnual()Ljava/lang/String;
    .registers 2

    .line 26
    iget-object v0, p0, Lcom/itau/empresas/api/model/TaxasLimiteVO;->taxaContratoAnual:Ljava/lang/String;

    return-object v0
.end method

.method public getTaxaContratoMensal()Ljava/lang/String;
    .registers 2

    .line 22
    iget-object v0, p0, Lcom/itau/empresas/api/model/TaxasLimiteVO;->taxaContratoMensal:Ljava/lang/String;

    return-object v0
.end method

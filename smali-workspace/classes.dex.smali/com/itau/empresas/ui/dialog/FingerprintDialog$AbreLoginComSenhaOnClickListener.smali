.class final Lcom/itau/empresas/ui/dialog/FingerprintDialog$AbreLoginComSenhaOnClickListener;
.super Ljava/lang/Object;
.source "FingerprintDialog.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;
.implements Landroid/content/DialogInterface$OnCancelListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/itau/empresas/ui/dialog/FingerprintDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "AbreLoginComSenhaOnClickListener"
.end annotation


# instance fields
.field final signal:Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintCanceller;


# direct methods
.method constructor <init>(Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintCanceller;)V
    .registers 2
    .param p1, "signal"    # Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintCanceller;

    .line 100
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 101
    iput-object p1, p0, Lcom/itau/empresas/ui/dialog/FingerprintDialog$AbreLoginComSenhaOnClickListener;->signal:Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintCanceller;

    .line 102
    return-void
.end method

.method private abreLogin(Landroid/content/DialogInterface;)V
    .registers 4
    .param p1, "dialog"    # Landroid/content/DialogInterface;

    .line 115
    invoke-static {}, Lde/greenrobot/event/EventBus;->getDefault()Lde/greenrobot/event/EventBus;

    move-result-object v0

    new-instance v1, Lcom/itau/empresas/Evento$EventoLoginComSenha;

    invoke-direct {v1}, Lcom/itau/empresas/Evento$EventoLoginComSenha;-><init>()V

    invoke-virtual {v0, v1}, Lde/greenrobot/event/EventBus;->post(Ljava/lang/Object;)V

    .line 116
    iget-object v0, p0, Lcom/itau/empresas/ui/dialog/FingerprintDialog$AbreLoginComSenhaOnClickListener;->signal:Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintCanceller;

    invoke-interface {v0}, Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintCanceller;->cancel()V

    .line 117
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 118
    return-void
.end method


# virtual methods
.method public onCancel(Landroid/content/DialogInterface;)V
    .registers 2
    .param p1, "dialog"    # Landroid/content/DialogInterface;

    .line 111
    invoke-direct {p0, p1}, Lcom/itau/empresas/ui/dialog/FingerprintDialog$AbreLoginComSenhaOnClickListener;->abreLogin(Landroid/content/DialogInterface;)V

    .line 112
    return-void
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .registers 3
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .line 106
    invoke-direct {p0, p1}, Lcom/itau/empresas/ui/dialog/FingerprintDialog$AbreLoginComSenhaOnClickListener;->abreLogin(Landroid/content/DialogInterface;)V

    .line 107
    return-void
.end method

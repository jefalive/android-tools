.class public Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity;
.super Lcom/itau/empresas/ui/activity/BaseActivity;
.source "PagamentoTributoSucessoActivity.java"


# instance fields
.field campoDataPagamento:Landroid/widget/TextView;

.field campoNomeRecebido:Landroid/widget/TextView;

.field campoValorTotalPagar:Landroid/widget/TextView;

.field dadosPagamentoTributo:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo;

.field detalhesAutorizacaoVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;

.field detalhesPagamento:Landroid/widget/TextView;

.field iconeAlerta:Landroid/widget/TextView;

.field labelIdentificacaoComprovante:Landroid/widget/TextView;

.field labelTituloSucesso:Landroid/widget/TextView;

.field linhaSeparacao:Landroid/view/View;

.field motivoPendenciaAutorizacao:Landroid/widget/TextView;

.field rlAutorizarPagamento:Landroid/widget/RelativeLayout;

.field rlExtrato:Landroid/widget/RelativeLayout;

.field rlNavegacaoHome:Landroid/widget/RelativeLayout;

.field rlNovoPagamento:Landroid/widget/RelativeLayout;

.field root:Landroid/widget/LinearLayout;

.field shareComprovante:Landroid/widget/TextView;

.field showComprovante:Landroid/widget/TextView;

.field statusPagamento:Landroid/widget/TextView;

.field textoLimiteHorarioUltrapassado:Landroid/widget/TextView;

.field tituloTipoPagamento:Landroid/widget/TextView;

.field toolBar:Landroid/support/v7/widget/Toolbar;


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 37
    invoke-direct {p0}, Lcom/itau/empresas/ui/activity/BaseActivity;-><init>()V

    return-void
.end method

.method private configuraToolbar()V
    .registers 4

    .line 260
    invoke-virtual {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    if-eqz v0, :cond_e

    .line 261
    invoke-virtual {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 263
    :cond_e
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity;->toolBar:Landroid/support/v7/widget/Toolbar;

    const v1, 0x7f0e05c2

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Landroid/widget/TextView;

    .line 264
    .local v2, "tituloToolbar":Landroid/widget/TextView;
    const v0, 0x7f0706a4

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(I)V

    .line 265
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity;->toolBar:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity;->setSupportActionBar(Landroid/support/v7/widget/Toolbar;)V

    .line 266
    return-void
.end method

.method public static intent(Landroid/content/Context;)Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity_$IntentBuilder_;
    .registers 2
    .param p0, "context"    # Landroid/content/Context;

    .line 269
    new-instance v0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity_$IntentBuilder_;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity_$IntentBuilder_;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method private mostraCardNavegacao()V
    .registers 3

    .line 216
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity;->dadosPagamentoTributo:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo;->isMostraDetalhesAutorizacao()Z

    move-result v0

    if-eqz v0, :cond_41

    .line 217
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity;->rlAutorizarPagamento:Landroid/widget/RelativeLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 218
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity;->rlNovoPagamento:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity;->dadosPagamentoTributo:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo;

    .line 219
    invoke-virtual {v1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo;->isMostraDetalhesAutorizacao()Z

    move-result v1

    if-eqz v1, :cond_1a

    const/4 v1, 0x0

    goto :goto_1c

    :cond_1a
    const/16 v1, 0x8

    .line 218
    :goto_1c
    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 220
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity;->rlNavegacaoHome:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity;->dadosPagamentoTributo:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo;

    .line 221
    invoke-virtual {v1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo;->isMostraDetalhesAutorizacao()Z

    move-result v1

    if-eqz v1, :cond_2c

    const/16 v1, 0x8

    goto :goto_2d

    :cond_2c
    const/4 v1, 0x0

    .line 220
    :goto_2d
    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 222
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity;->rlExtrato:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity;->dadosPagamentoTributo:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo;

    .line 223
    invoke-virtual {v1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo;->isMostraDetalhesAutorizacao()Z

    move-result v1

    if-eqz v1, :cond_3d

    const/16 v1, 0x8

    goto :goto_3e

    :cond_3d
    const/4 v1, 0x0

    .line 222
    :goto_3e
    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 225
    :cond_41
    return-void
.end method

.method private mostraComprovante()V
    .registers 3

    .line 175
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iget-object v1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity;->dadosPagamentoTributo:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo;

    invoke-virtual {v1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo;->isMostraComprovantes()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2b

    .line 176
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity;->shareComprovante:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 177
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity;->showComprovante:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 178
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity;->linhaSeparacao:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 179
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity;->detalhesPagamento:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 181
    :cond_2b
    return-void
.end method

.method private mostraDetalhes()V
    .registers 3

    .line 184
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iget-object v1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity;->dadosPagamentoTributo:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo;

    invoke-virtual {v1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo;->isMostraDetalhesAutorizacao()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2c

    .line 185
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity;->detalhesPagamento:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 186
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity;->linhaSeparacao:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 187
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity;->shareComprovante:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 188
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity;->showComprovante:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 190
    :cond_2c
    return-void
.end method

.method private mostraMensagemLimiteUltrapassado()V
    .registers 5

    .line 193
    invoke-direct {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity;->notificaAnalitycsLDP()V

    .line 194
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity;->dadosPagamentoTributo:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo;->getInclusaoTributoResponseVO()Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/InclusaoTributoResponseVO;

    move-result-object v0

    if-eqz v0, :cond_60

    .line 195
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity;->textoLimiteHorarioUltrapassado:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity;->dadosPagamentoTributo:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo;

    .line 196
    invoke-virtual {v1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo;->getInclusaoTributoResponseVO()Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/InclusaoTributoResponseVO;

    move-result-object v1

    invoke-virtual {v1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/InclusaoTributoResponseVO;->isIndicadorEstouroLdp()Z

    move-result v1

    if-eqz v1, :cond_1b

    const/4 v1, 0x0

    goto :goto_1d

    :cond_1b
    const/16 v1, 0x8

    .line 195
    :goto_1d
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 200
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity;->textoLimiteHorarioUltrapassado:Landroid/widget/TextView;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    .line 202
    invoke-static {}, Lcom/itau/empresas/ui/util/formatacao/DateUtils;->getDataAtualFormatoBrasileiro()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    .line 201
    const v2, 0x7f0704a5

    invoke-virtual {p0, v2, v1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 203
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity;->iconeAlerta:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity;->dadosPagamentoTributo:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo;

    .line 204
    invoke-virtual {v1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo;->getInclusaoTributoResponseVO()Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/InclusaoTributoResponseVO;

    move-result-object v1

    invoke-virtual {v1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/InclusaoTributoResponseVO;->isIndicadorEstouroLdp()Z

    move-result v1

    if-eqz v1, :cond_46

    const/4 v1, 0x0

    goto :goto_48

    :cond_46
    const/16 v1, 0x8

    .line 203
    :goto_48
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 207
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity;->linhaSeparacao:Landroid/view/View;

    iget-object v1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity;->dadosPagamentoTributo:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo;

    .line 208
    invoke-virtual {v1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo;->getInclusaoTributoResponseVO()Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/InclusaoTributoResponseVO;

    move-result-object v1

    invoke-virtual {v1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/InclusaoTributoResponseVO;->isIndicadorEstouroLdp()Z

    move-result v1

    if-eqz v1, :cond_5b

    const/4 v1, 0x0

    goto :goto_5d

    :cond_5b
    const/16 v1, 0x8

    .line 207
    :goto_5d
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 212
    :cond_60
    return-void
.end method

.method private mostraStatusPagamento()V
    .registers 6

    .line 142
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity;->dadosPagamentoTributo:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo;->isMostraComprovantes()Z

    move-result v0

    if-nez v0, :cond_10

    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity;->dadosPagamentoTributo:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo;

    .line 143
    invoke-virtual {v0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo;->isMostraDetalhesAutorizacao()Z

    move-result v0

    if-eqz v0, :cond_61

    .line 144
    :cond_10
    invoke-direct {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity;->notificaAnalitycsAutorizado()V

    .line 145
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity;->dadosPagamentoTributo:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo;

    .line 146
    invoke-virtual {v0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo;->getDetalhesAutorizacaoVO()Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;

    move-result-object v2

    .line 147
    .local v2, "detalhesAutorizacaoVO":Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;
    new-instance v3, Lcom/itau/empresas/api/model/DadosAlertaAutorizacaoTributos;

    .line 148
    invoke-virtual {v2}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;->getSituacao()Ljava/lang/String;

    move-result-object v0

    .line 149
    invoke-virtual {v2}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;->getCodigoMotivo()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v3, v0, v1}, Lcom/itau/empresas/api/model/DadosAlertaAutorizacaoTributos;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 151
    .line 152
    .local v3, "dadosAlertaAutorizacaoTributos":Lcom/itau/empresas/api/model/DadosAlertaAutorizacaoTributos;
    invoke-virtual {v3}, Lcom/itau/empresas/api/model/DadosAlertaAutorizacaoTributos;->getDadodStatusAutorizacaoTributos()Lcom/itau/empresas/api/model/StatusPagamentoTributos;

    move-result-object v4

    .line 154
    .local v4, "statusPagamentoTributos":Lcom/itau/empresas/api/model/StatusPagamentoTributos;
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity;->labelTituloSucesso:Landroid/widget/TextView;

    invoke-virtual {v4}, Lcom/itau/empresas/api/model/StatusPagamentoTributos;->getTituloPagamento()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 155
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity;->statusPagamento:Landroid/widget/TextView;

    invoke-virtual {v4}, Lcom/itau/empresas/api/model/StatusPagamentoTributos;->getMensagemStatus()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 156
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity;->statusPagamento:Landroid/widget/TextView;

    invoke-virtual {v4}, Lcom/itau/empresas/api/model/StatusPagamentoTributos;->getColorMessage()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 157
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity;->tituloTipoPagamento:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity;->dadosPagamentoTributo:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo;

    invoke-virtual {v1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo;->getTipoPagamento()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 158
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity;->motivoPendenciaAutorizacao:Landroid/widget/TextView;

    invoke-virtual {v2}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;->getDescricaoMotivo()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 159
    invoke-virtual {v2}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;->getDescricaoMotivo()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity;->validaSaldoInsufucienteParaAnaliticys(Ljava/lang/String;)V

    .line 160
    .end local v2    # "detalhesAutorizacaoVO":Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;
    .end local v3    # "dadosAlertaAutorizacaoTributos":Lcom/itau/empresas/api/model/DadosAlertaAutorizacaoTributos;
    .end local v4    # "statusPagamentoTributos":Lcom/itau/empresas/api/model/StatusPagamentoTributos;
    goto :goto_74

    .line 161
    :cond_61
    invoke-direct {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity;->notificaAnalitycsIncluido()V

    .line 162
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity;->labelTituloSucesso:Landroid/widget/TextView;

    const v1, 0x7f070490

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 163
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity;->statusPagamento:Landroid/widget/TextView;

    const v1, 0x7f070191

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 165
    :goto_74
    return-void
.end method

.method private notificaAnalitycsAutorizado()V
    .registers 3

    .line 234
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity;->dadosPagamentoTributo:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo;->getTipoPagamento()Ljava/lang/String;

    move-result-object v0

    const-string v1, "DARF"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_18

    .line 235
    const v0, 0x7f0702ee

    const v1, 0x7f070293

    invoke-direct {p0, v0, v1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity;->trackerAnalitycs(II)V

    goto :goto_21

    .line 237
    :cond_18
    const v0, 0x7f0702ef

    const v1, 0x7f070293

    invoke-direct {p0, v0, v1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity;->trackerAnalitycs(II)V

    .line 239
    :goto_21
    return-void
.end method

.method private notificaAnalitycsIncluido()V
    .registers 3

    .line 242
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity;->dadosPagamentoTributo:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo;->getTipoPagamento()Ljava/lang/String;

    move-result-object v0

    const-string v1, "DARF"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_21

    .line 243
    const v0, 0x7f0702f8

    const v1, 0x7f070293

    invoke-direct {p0, v0, v1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity;->trackerAnalitycs(II)V

    .line 244
    const v0, 0x7f070325

    const v1, 0x7f070293

    invoke-direct {p0, v0, v1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity;->trackerAnalitycs(II)V

    goto :goto_33

    .line 246
    :cond_21
    const v0, 0x7f0702f9

    const v1, 0x7f070293

    invoke-direct {p0, v0, v1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity;->trackerAnalitycs(II)V

    .line 247
    const v0, 0x7f070326

    const v1, 0x7f070293

    invoke-direct {p0, v0, v1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity;->trackerAnalitycs(II)V

    .line 249
    :goto_33
    return-void
.end method

.method private notificaAnalitycsLDP()V
    .registers 3

    .line 252
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity;->dadosPagamentoTributo:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo;->getTipoPagamento()Ljava/lang/String;

    move-result-object v0

    const-string v1, "DARF"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_18

    .line 253
    const v0, 0x7f07032b

    const v1, 0x7f070293

    invoke-direct {p0, v0, v1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity;->trackerAnalitycs(II)V

    goto :goto_21

    .line 255
    :cond_18
    const v0, 0x7f07032c

    const v1, 0x7f070293

    invoke-direct {p0, v0, v1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity;->trackerAnalitycs(II)V

    .line 257
    :goto_21
    return-void
.end method

.method private trackerAnalitycs(II)V
    .registers 7
    .param p1, "categoriaResId"    # I
    .param p2, "acaoResId"    # I

    .line 228
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity;->application:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getEventoTracker()Lcom/itau/empresas/ui/util/analytics/TrackerEvento;

    move-result-object v0

    new-instance v1, Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;

    .line 229
    invoke-virtual {p0, p1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 230
    invoke-virtual {p0, p2}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 228
    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/analytics/TrackerEvento;->enviarHitEvento(Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;)V

    .line 231
    return-void
.end method

.method private validaSaldoInsufucienteParaAnaliticys(Ljava/lang/String;)V
    .registers 3
    .param p1, "motivo"    # Ljava/lang/String;

    .line 168
    const-string v0, "insuficiente"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 169
    invoke-direct {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity;->notificaAnalitycsIncluido()V

    .line 172
    :cond_b
    return-void
.end method


# virtual methods
.method public abreTelaPagamento()V
    .registers 2

    .line 90
    invoke-static {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/SelecaoTributoPagamentoActivity;->intent(Landroid/content/Context;)Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/SelecaoTributoPagamentoActivity_$IntentBuilder_;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/SelecaoTributoPagamentoActivity_$IntentBuilder_;->start()Lorg/androidannotations/api/builder/PostActivityStarter;

    .line 91
    invoke-virtual {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity;->finish()V

    .line 92
    return-void
.end method

.method public atualizaDados()V
    .registers 3

    .line 71
    invoke-direct {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity;->mostraStatusPagamento()V

    .line 72
    invoke-direct {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity;->mostraCardNavegacao()V

    .line 73
    invoke-direct {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity;->mostraMensagemLimiteUltrapassado()V

    .line 74
    invoke-direct {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity;->mostraComprovante()V

    .line 75
    invoke-direct {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity;->mostraDetalhes()V

    .line 76
    invoke-direct {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity;->configuraToolbar()V

    .line 78
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity;->tituloTipoPagamento:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity;->dadosPagamentoTributo:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo;

    invoke-virtual {v1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo;->getTipoPagamento()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 79
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity;->campoDataPagamento:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity;->dadosPagamentoTributo:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo;

    invoke-virtual {v1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo;->getDataPagamento()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 80
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity;->campoValorTotalPagar:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity;->dadosPagamentoTributo:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo;

    invoke-virtual {v1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo;->getValorPagamento()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 81
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity;->campoNomeRecebido:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity;->dadosPagamentoTributo:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo;

    .line 82
    invoke-virtual {v1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo;->isIdentificadorComprovante()Z

    move-result v1

    if-eqz v1, :cond_3f

    const/4 v1, 0x0

    goto :goto_41

    :cond_3f
    const/16 v1, 0x8

    .line 81
    :goto_41
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 83
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity;->labelIdentificacaoComprovante:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity;->dadosPagamentoTributo:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo;

    .line 84
    invoke-virtual {v1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo;->isIdentificadorComprovante()Z

    move-result v1

    if-eqz v1, :cond_50

    const/4 v1, 0x0

    goto :goto_52

    :cond_50
    const/16 v1, 0x8

    .line 83
    :goto_52
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 85
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity;->campoNomeRecebido:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity;->dadosPagamentoTributo:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo;

    invoke-virtual {v1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo;->getIdentificacaoComprovante()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 86
    return-void
.end method

.method public compartilhaComprovante()V
    .registers 3

    .line 114
    invoke-static {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ComprovantePagamentoTributosActivity;->intent(Landroid/content/Context;)Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ComprovantePagamentoTributosActivity_$IntentBuilder_;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity;->dadosPagamentoTributo:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo;

    .line 115
    invoke-virtual {v1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo;->getDetalhesAutorizacaoVO()Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ComprovantePagamentoTributosActivity_$IntentBuilder_;->detalhesAutorizacaoVO(Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;)Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ComprovantePagamentoTributosActivity_$IntentBuilder_;

    move-result-object v0

    .line 116
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ComprovantePagamentoTributosActivity_$IntentBuilder_;->shareComprovante(Z)Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ComprovantePagamentoTributosActivity_$IntentBuilder_;

    move-result-object v0

    .line 117
    invoke-virtual {v0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ComprovantePagamentoTributosActivity_$IntentBuilder_;->start()Lorg/androidannotations/api/builder/PostActivityStarter;

    .line 118
    invoke-virtual {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity;->finish()V

    .line 119
    return-void
.end method

.method public mostraComprovantePagamento()V
    .registers 4

    .line 103
    invoke-static {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ComprovantePagamentoTributosActivity;->intent(Landroid/content/Context;)Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ComprovantePagamentoTributosActivity_$IntentBuilder_;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity;->dadosPagamentoTributo:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo;

    .line 104
    invoke-virtual {v1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo;->getDetalhesAutorizacaoVO()Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ComprovantePagamentoTributosActivity_$IntentBuilder_;->detalhesAutorizacaoVO(Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;)Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ComprovantePagamentoTributosActivity_$IntentBuilder_;

    move-result-object v0

    new-instance v1, Lcom/itau/empresas/ui/util/BuildDadosComprovante;

    iget-object v2, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity;->dadosPagamentoTributo:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo;

    .line 106
    invoke-virtual {v2}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo;->getDetalhesAutorizacaoVO()Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;

    move-result-object v2

    invoke-direct {v1, v2, p0}, Lcom/itau/empresas/ui/util/BuildDadosComprovante;-><init>(Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;Landroid/content/Context;)V

    .line 107
    invoke-virtual {v1}, Lcom/itau/empresas/ui/util/BuildDadosComprovante;->getDadosComprovante()Ljava/util/ArrayList;

    move-result-object v1

    .line 105
    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ComprovantePagamentoTributosActivity_$IntentBuilder_;->dadosComprovanteList(Ljava/util/ArrayList;)Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ComprovantePagamentoTributosActivity_$IntentBuilder_;

    move-result-object v0

    .line 108
    invoke-virtual {v0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ComprovantePagamentoTributosActivity_$IntentBuilder_;->start()Lorg/androidannotations/api/builder/PostActivityStarter;

    .line 109
    invoke-virtual {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity;->finish()V

    .line 110
    return-void
.end method

.method public mostraDetalhesPagamento()V
    .registers 3

    .line 96
    invoke-static {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/DetalhesAutorizacaoActivity;->intent(Landroid/content/Context;)Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/DetalhesAutorizacaoActivity_$IntentBuilder_;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity;->dadosPagamentoTributo:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo;

    .line 97
    invoke-virtual {v1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo;->getDetalhesAutorizacaoVO()Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/DetalhesAutorizacaoActivity_$IntentBuilder_;->detalhesAutorizacaoVO(Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;)Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/DetalhesAutorizacaoActivity_$IntentBuilder_;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/DetalhesAutorizacaoActivity_$IntentBuilder_;->start()Lorg/androidannotations/api/builder/PostActivityStarter;

    .line 98
    invoke-virtual {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity;->finish()V

    .line 99
    return-void
.end method

.method public onEventMainThread(Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;)V
    .registers 5
    .param p1, "e"    # Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;

    .line 126
    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;->getException()Lbr/com/itau/sdk/android/core/exception/BackendException;

    move-result-object v0

    invoke-virtual {v0}, Lbr/com/itau/sdk/android/core/exception/BackendException;->getError()Lbr/com/itau/sdk/android/core/model/ErrorDTO;

    move-result-object v0

    invoke-virtual {v0}, Lbr/com/itau/sdk/android/core/model/ErrorDTO;->getCampos()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_42

    .line 127
    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;->getException()Lbr/com/itau/sdk/android/core/exception/BackendException;

    move-result-object v0

    invoke-virtual {v0}, Lbr/com/itau/sdk/android/core/exception/BackendException;->getError()Lbr/com/itau/sdk/android/core/model/ErrorDTO;

    move-result-object v0

    invoke-virtual {v0}, Lbr/com/itau/sdk/android/core/model/ErrorDTO;->getCampos()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_42

    .line 128
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity;->root:Landroid/widget/LinearLayout;

    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;->getException()Lbr/com/itau/sdk/android/core/exception/BackendException;

    move-result-object v1

    invoke-virtual {v1}, Lbr/com/itau/sdk/android/core/exception/BackendException;->getError()Lbr/com/itau/sdk/android/core/model/ErrorDTO;

    move-result-object v1

    invoke-virtual {v1}, Lbr/com/itau/sdk/android/core/model/ErrorDTO;->getCampos()Ljava/util/List;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lbr/com/itau/sdk/android/core/model/CampoErroDTO;

    invoke-virtual {v1}, Lbr/com/itau/sdk/android/core/model/CampoErroDTO;->getMensagem()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lbr/com/itau/topsnackbar/TopSnackbar;->make(Landroid/view/ViewGroup;Ljava/lang/CharSequence;I)Lbr/com/itau/topsnackbar/TopSnackbar;

    move-result-object v0

    .line 129
    invoke-virtual {v0}, Lbr/com/itau/topsnackbar/TopSnackbar;->show()Lbr/com/itau/topsnackbar/TopSnackbar;

    goto :goto_6a

    .line 130
    :cond_42
    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;->getException()Lbr/com/itau/sdk/android/core/exception/BackendException;

    move-result-object v0

    invoke-virtual {v0}, Lbr/com/itau/sdk/android/core/exception/BackendException;->getError()Lbr/com/itau/sdk/android/core/model/ErrorDTO;

    move-result-object v0

    invoke-virtual {v0}, Lbr/com/itau/sdk/android/core/model/ErrorDTO;->getMensagem()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_6a

    .line 131
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity;->root:Landroid/widget/LinearLayout;

    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;->getException()Lbr/com/itau/sdk/android/core/exception/BackendException;

    move-result-object v1

    invoke-virtual {v1}, Lbr/com/itau/sdk/android/core/exception/BackendException;->getError()Lbr/com/itau/sdk/android/core/model/ErrorDTO;

    move-result-object v1

    invoke-virtual {v1}, Lbr/com/itau/sdk/android/core/model/ErrorDTO;->getMensagem()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lbr/com/itau/topsnackbar/TopSnackbar;->make(Landroid/view/ViewGroup;Ljava/lang/CharSequence;I)Lbr/com/itau/topsnackbar/TopSnackbar;

    move-result-object v0

    .line 132
    invoke-virtual {v0}, Lbr/com/itau/topsnackbar/TopSnackbar;->show()Lbr/com/itau/topsnackbar/TopSnackbar;

    .line 134
    :cond_6a
    :goto_6a
    return-void
.end method

.method public onEventMainThread(Lcom/itau/empresas/api/model/ResponseComprovantePagamentoVO;)V
    .registers 4
    .param p1, "responseComprovantePagamentoVO"    # Lcom/itau/empresas/api/model/ResponseComprovantePagamentoVO;

    .line 122
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity;->tituloTipoPagamento:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/itau/empresas/api/model/ResponseComprovantePagamentoVO;->getAutenticacaoComprovante()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 123
    return-void
.end method

.method public opkeysConsumidas()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Ljava/lang/String;>;"
        }
    .end annotation

    .line 138
    const-string v0, "detalhePagamentoAutorizado"

    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

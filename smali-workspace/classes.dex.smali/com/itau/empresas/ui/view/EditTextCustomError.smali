.class public Lcom/itau/empresas/ui/view/EditTextCustomError;
.super Lcom/itau/empresas/ui/view/TextoEditavel;
.source "EditTextCustomError.java"


# instance fields
.field private hint:Lbr/com/itau/widgets/hintview/Hint$Builder;

.field private hintView:Lbr/com/itau/widgets/hintview/Hint;

.field private isErro:Z

.field private mensagemErro:Ljava/lang/String;

.field private mostraHint:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 2
    .param p1, "context"    # Landroid/content/Context;

    .line 32
    invoke-direct {p0, p1}, Lcom/itau/empresas/ui/view/TextoEditavel;-><init>(Landroid/content/Context;)V

    .line 33
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .line 36
    invoke-direct {p0, p1, p2}, Lcom/itau/empresas/ui/view/TextoEditavel;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 37
    .line 38
    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    sget-object v1, Lcom/itau/empresas/R$styleable;->EditTextCustomError:[I

    .line 39
    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, p2, v1, v2, v3}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v4

    .line 40
    .local v4, "typedArray":Landroid/content/res/TypedArray;
    const/4 v0, 0x1

    invoke-virtual {v4, v0}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/ui/view/EditTextCustomError;->mensagemErro:Ljava/lang/String;

    .line 41
    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-virtual {v4, v0, v1}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    iput-boolean v0, p0, Lcom/itau/empresas/ui/view/EditTextCustomError;->mostraHint:Z

    .line 43
    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/EditTextCustomError;->getRootView()Landroid/view/View;

    move-result-object v5

    .line 44
    .local v5, "activityRootView":Landroid/view/View;
    invoke-virtual {v5}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    new-instance v1, Lcom/itau/empresas/ui/view/EditTextCustomError$1;

    invoke-direct {v1, p0, v5}, Lcom/itau/empresas/ui/view/EditTextCustomError$1;-><init>(Lcom/itau/empresas/ui/view/EditTextCustomError;Landroid/view/View;)V

    .line 45
    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 57
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .line 60
    invoke-direct {p0, p1, p2, p3}, Lcom/itau/empresas/ui/view/TextoEditavel;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 61
    return-void
.end method

.method static synthetic access$000(Lcom/itau/empresas/ui/view/EditTextCustomError;)V
    .registers 1
    .param p0, "x0"    # Lcom/itau/empresas/ui/view/EditTextCustomError;

    .line 22
    invoke-direct {p0}, Lcom/itau/empresas/ui/view/EditTextCustomError;->removePopPupErro()V

    return-void
.end method

.method public static dpToPx(Landroid/content/Context;F)F
    .registers 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "valueInDp"    # F

    .line 68
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    .line 69
    .local v1, "metrics":Landroid/util/DisplayMetrics;
    const/4 v0, 0x1

    invoke-static {v0, p1, v1}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    return v0
.end method

.method private mostraCampoRequerido()V
    .registers 3

    .line 99
    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/EditTextCustomError;->getParent()Landroid/view/ViewParent;

    .line 100
    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/EditTextCustomError;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {p0, v0}, Lcom/itau/empresas/ui/view/EditTextCustomError;->setaBackgroundErro(Landroid/view/ViewGroup;)V

    .line 101
    new-instance v0, Lbr/com/itau/widgets/hintview/Hint$Builder;

    const v1, 0x7f09011d

    invoke-direct {v0, p0, v1}, Lbr/com/itau/widgets/hintview/Hint$Builder;-><init>(Landroid/view/View;I)V

    iput-object v0, p0, Lcom/itau/empresas/ui/view/EditTextCustomError;->hint:Lbr/com/itau/widgets/hintview/Hint$Builder;

    .line 103
    iget-object v0, p0, Lcom/itau/empresas/ui/view/EditTextCustomError;->hint:Lbr/com/itau/widgets/hintview/Hint$Builder;

    iget-object v1, p0, Lcom/itau/empresas/ui/view/EditTextCustomError;->mensagemErro:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/itau/empresas/ui/util/HintUtils;->montaHintErro(Lbr/com/itau/widgets/hintview/Hint$Builder;Ljava/lang/String;)Lbr/com/itau/widgets/hintview/Hint$Builder;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/ui/view/EditTextCustomError;->hint:Lbr/com/itau/widgets/hintview/Hint$Builder;

    .line 104
    iget-object v0, p0, Lcom/itau/empresas/ui/view/EditTextCustomError;->hint:Lbr/com/itau/widgets/hintview/Hint$Builder;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lbr/com/itau/widgets/hintview/Hint$Builder;->setCancelable(Z)Lbr/com/itau/widgets/hintview/Hint$Builder;

    .line 105
    iget-object v0, p0, Lcom/itau/empresas/ui/view/EditTextCustomError;->hint:Lbr/com/itau/widgets/hintview/Hint$Builder;

    new-instance v1, Lcom/itau/empresas/ui/view/EditTextCustomError$2;

    invoke-direct {v1, p0}, Lcom/itau/empresas/ui/view/EditTextCustomError$2;-><init>(Lcom/itau/empresas/ui/view/EditTextCustomError;)V

    invoke-virtual {v0, v1}, Lbr/com/itau/widgets/hintview/Hint$Builder;->setOnClickListener(Lbr/com/itau/widgets/hintview/OnClickListener;)Lbr/com/itau/widgets/hintview/Hint$Builder;

    .line 111
    iget-object v0, p0, Lcom/itau/empresas/ui/view/EditTextCustomError;->hint:Lbr/com/itau/widgets/hintview/Hint$Builder;

    invoke-virtual {v0}, Lbr/com/itau/widgets/hintview/Hint$Builder;->show()Lbr/com/itau/widgets/hintview/Hint;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/ui/view/EditTextCustomError;->hintView:Lbr/com/itau/widgets/hintview/Hint;

    .line 113
    return-void
.end method

.method private removePopPupErro()V
    .registers 2

    .line 148
    iget-object v0, p0, Lcom/itau/empresas/ui/view/EditTextCustomError;->hintView:Lbr/com/itau/widgets/hintview/Hint;

    if-eqz v0, :cond_9

    .line 149
    iget-object v0, p0, Lcom/itau/empresas/ui/view/EditTextCustomError;->hintView:Lbr/com/itau/widgets/hintview/Hint;

    invoke-virtual {v0}, Lbr/com/itau/widgets/hintview/Hint;->dismiss()V

    .line 151
    :cond_9
    return-void
.end method

.method private setaBackgroundDefault(Landroid/view/ViewGroup;)V
    .registers 4
    .param p1, "parent"    # Landroid/view/ViewGroup;

    .line 137
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-ge v0, v1, :cond_15

    .line 138
    .line 139
    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/EditTextCustomError;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f020064

    invoke-static {v0, v1}, Landroid/support/v4/content/ContextCompat;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 138
    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_23

    .line 141
    .line 142
    :cond_15
    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/EditTextCustomError;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f020064

    invoke-static {v0, v1}, Landroid/support/v4/content/ContextCompat;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 141
    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 144
    :goto_23
    return-void
.end method


# virtual methods
.method protected onConfigurationChanged(Landroid/content/res/Configuration;)V
    .registers 4
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .line 117
    invoke-super {p0, p1}, Lcom/itau/empresas/ui/view/TextoEditavel;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 118
    iget v0, p1, Landroid/content/res/Configuration;->hardKeyboardHidden:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_c

    .line 119
    invoke-direct {p0}, Lcom/itau/empresas/ui/view/EditTextCustomError;->removePopPupErro()V

    goto :goto_14

    .line 120
    :cond_c
    iget v0, p1, Landroid/content/res/Configuration;->hardKeyboardHidden:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_14

    .line 121
    invoke-direct {p0}, Lcom/itau/empresas/ui/view/EditTextCustomError;->removePopPupErro()V

    .line 123
    :cond_14
    :goto_14
    return-void
.end method

.method protected onFocusChanged(ZILandroid/graphics/Rect;)V
    .registers 5
    .param p1, "focused"    # Z
    .param p2, "direction"    # I
    .param p3, "previouslyFocusedRect"    # Landroid/graphics/Rect;

    .line 78
    invoke-super {p0, p1, p2, p3}, Lcom/itau/empresas/ui/view/TextoEditavel;->onFocusChanged(ZILandroid/graphics/Rect;)V

    .line 80
    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/EditTextCustomError;->hasFocus()Z

    move-result v0

    if-eqz v0, :cond_1f

    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/EditTextCustomError;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1f

    iget-boolean v0, p0, Lcom/itau/empresas/ui/view/EditTextCustomError;->isErro:Z

    if-eqz v0, :cond_1f

    .line 81
    invoke-direct {p0}, Lcom/itau/empresas/ui/view/EditTextCustomError;->mostraCampoRequerido()V

    goto :goto_4c

    .line 82
    :cond_1f
    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/EditTextCustomError;->hasFocus()Z

    move-result v0

    if-nez v0, :cond_40

    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/EditTextCustomError;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_40

    .line 83
    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/EditTextCustomError;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {p0, v0}, Lcom/itau/empresas/ui/view/EditTextCustomError;->setaBackgroundErro(Landroid/view/ViewGroup;)V

    .line 84
    invoke-direct {p0}, Lcom/itau/empresas/ui/view/EditTextCustomError;->removePopPupErro()V

    goto :goto_4c

    .line 86
    :cond_40
    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/EditTextCustomError;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-direct {p0, v0}, Lcom/itau/empresas/ui/view/EditTextCustomError;->setaBackgroundDefault(Landroid/view/ViewGroup;)V

    .line 87
    invoke-direct {p0}, Lcom/itau/empresas/ui/view/EditTextCustomError;->removePopPupErro()V

    .line 89
    :goto_4c
    return-void
.end method

.method protected onTextChanged(Ljava/lang/CharSequence;III)V
    .registers 5
    .param p1, "text"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "lengthBefore"    # I
    .param p4, "lengthAfter"    # I

    .line 93
    invoke-super {p0, p1, p2, p3, p4}, Lcom/itau/empresas/ui/view/TextoEditavel;->onTextChanged(Ljava/lang/CharSequence;III)V

    .line 94
    invoke-direct {p0}, Lcom/itau/empresas/ui/view/EditTextCustomError;->removePopPupErro()V

    .line 95
    return-void
.end method

.method public setErro(Z)V
    .registers 2
    .param p1, "erro"    # Z

    .line 64
    iput-boolean p1, p0, Lcom/itau/empresas/ui/view/EditTextCustomError;->isErro:Z

    .line 65
    return-void
.end method

.method public setMostraHint(Z)V
    .registers 2
    .param p1, "mostraHint"    # Z

    .line 73
    iput-boolean p1, p0, Lcom/itau/empresas/ui/view/EditTextCustomError;->mostraHint:Z

    .line 74
    return-void
.end method

.method public setaBackgroundErro(Landroid/view/ViewGroup;)V
    .registers 4
    .param p1, "viewGroup"    # Landroid/view/ViewGroup;

    .line 127
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-ge v0, v1, :cond_15

    .line 128
    .line 129
    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/EditTextCustomError;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0200d5

    invoke-static {v0, v1}, Landroid/support/v4/content/ContextCompat;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 128
    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_23

    .line 131
    .line 132
    :cond_15
    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/EditTextCustomError;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0200d5

    invoke-static {v0, v1}, Landroid/support/v4/content/ContextCompat;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 131
    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 134
    :goto_23
    return-void
.end method

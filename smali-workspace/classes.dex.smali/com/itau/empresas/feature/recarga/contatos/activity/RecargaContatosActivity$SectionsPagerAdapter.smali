.class public Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaContatosActivity$SectionsPagerAdapter;
.super Landroid/support/v4/app/FragmentPagerAdapter;
.source "RecargaContatosActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaContatosActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "SectionsPagerAdapter"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaContatosActivity;


# direct methods
.method constructor <init>(Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaContatosActivity;Landroid/support/v4/app/FragmentManager;)V
    .registers 3
    .param p1, "this$0"    # Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaContatosActivity;
    .param p2, "fragmentManager"    # Landroid/support/v4/app/FragmentManager;

    .line 152
    iput-object p1, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaContatosActivity$SectionsPagerAdapter;->this$0:Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaContatosActivity;

    .line 153
    invoke-direct {p0, p2}, Landroid/support/v4/app/FragmentPagerAdapter;-><init>(Landroid/support/v4/app/FragmentManager;)V

    .line 154
    return-void
.end method


# virtual methods
.method public getCount()I
    .registers 3

    .line 158
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaContatosActivity$SectionsPagerAdapter;->this$0:Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaContatosActivity;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaContatosActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d0004

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    array-length v0, v0

    return v0
.end method

.method public getItem(I)Landroid/support/v4/app/Fragment;
    .registers 4
    .param p1, "position"    # I

    .line 163
    if-nez p1, :cond_2e

    .line 164
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaContatosActivity$SectionsPagerAdapter;->this$0:Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaContatosActivity;

    invoke-static {}, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment_;->builder()Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment_$FragmentBuilder_;

    move-result-object v1

    invoke-virtual {v1}, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment_$FragmentBuilder_;->build()Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment;

    move-result-object v1

    # setter for: Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaContatosActivity;->recargaListaContatoFragment:Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment;
    invoke-static {v0, v1}, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaContatosActivity;->access$102(Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaContatosActivity;Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment;)Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment;

    .line 165
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaContatosActivity$SectionsPagerAdapter;->this$0:Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaContatosActivity;

    # getter for: Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaContatosActivity;->recargaListaContatoFragment:Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment;
    invoke-static {v0}, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaContatosActivity;->access$100(Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaContatosActivity;)Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment;->setPosition(I)V

    .line 166
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaContatosActivity$SectionsPagerAdapter;->this$0:Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaContatosActivity;

    # getter for: Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaContatosActivity;->recargaListaContatoFragment:Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment;
    invoke-static {v0}, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaContatosActivity;->access$100(Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaContatosActivity;)Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaContatosActivity$SectionsPagerAdapter;->this$0:Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaContatosActivity;

    # getter for: Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaContatosActivity;->application:Lcom/itau/empresas/CustomApplication;
    invoke-static {v1}, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaContatosActivity;->access$200(Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaContatosActivity;)Lcom/itau/empresas/CustomApplication;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment;->setCustomApplication(Lcom/itau/empresas/CustomApplication;)V

    .line 167
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaContatosActivity$SectionsPagerAdapter;->this$0:Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaContatosActivity;

    # getter for: Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaContatosActivity;->recargaListaContatoFragment:Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment;
    invoke-static {v0}, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaContatosActivity;->access$100(Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaContatosActivity;)Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment;

    move-result-object v0

    return-object v0

    .line 169
    :cond_2e
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaContatosActivity$SectionsPagerAdapter;->this$0:Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaContatosActivity;

    invoke-static {}, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment_;->builder()Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment_$FragmentBuilder_;

    move-result-object v1

    invoke-virtual {v1}, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment_$FragmentBuilder_;->build()Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;

    move-result-object v1

    # setter for: Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaContatosActivity;->recargaCadastroContatoFragment:Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;
    invoke-static {v0, v1}, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaContatosActivity;->access$302(Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaContatosActivity;Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;)Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;

    .line 170
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaContatosActivity$SectionsPagerAdapter;->this$0:Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaContatosActivity;

    # getter for: Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaContatosActivity;->recargaCadastroContatoFragment:Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;
    invoke-static {v0}, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaContatosActivity;->access$300(Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaContatosActivity;)Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;

    move-result-object v0

    return-object v0
.end method

.method public getPageTitle(I)Ljava/lang/CharSequence;
    .registers 4
    .param p1, "position"    # I

    .line 176
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaContatosActivity$SectionsPagerAdapter;->this$0:Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaContatosActivity;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaContatosActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d0004

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    aget-object v0, v0, p1

    return-object v0
.end method

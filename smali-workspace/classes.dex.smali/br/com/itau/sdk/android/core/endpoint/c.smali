.class public Lbr/com/itau/sdk/android/core/endpoint/c;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lokhttp3/Call;


# static fields
.field private static final e:[B

.field private static f:I


# instance fields
.field a:Lokhttp3/Request;

.field private final b:Lokhttp3/OkHttpClient;

.field private final c:Ljava/lang/String;

.field private d:Z


# direct methods
.method static constructor <clinit>()V
    .registers 3

    const/16 v0, 0x1a

    new-array v0, v0, [B

    fill-array-data v0, :array_e

    sput-object v0, Lbr/com/itau/sdk/android/core/endpoint/c;->e:[B

    const/16 v0, 0x7d

    sput v0, Lbr/com/itau/sdk/android/core/endpoint/c;->f:I

    return-void

    :array_e
    .array-data 1
        0x44t
        0x77t
        -0x13t
        0xct
        -0x3et
        -0x19t
        -0x6t
        -0xft
        -0x16t
        -0x28t
        0x46t
        -0x38t
        -0x46t
        0x0t
        -0x11t
        -0x25t
        -0x12t
        -0x4t
        -0x12t
        -0x31t
        -0x20t
        -0x8t
        -0x15t
        -0x1at
        -0xct
        -0x12t
    .end array-data
.end method

.method protected constructor <init>(Lokhttp3/OkHttpClient;Lokhttp3/Request;Ljava/lang/String;)V
    .registers 4

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object p1, p0, Lbr/com/itau/sdk/android/core/endpoint/c;->b:Lokhttp3/OkHttpClient;

    .line 34
    iput-object p2, p0, Lbr/com/itau/sdk/android/core/endpoint/c;->a:Lokhttp3/Request;

    .line 35
    iput-object p3, p0, Lbr/com/itau/sdk/android/core/endpoint/c;->c:Ljava/lang/String;

    .line 36
    return-void
.end method

.method private static a(III)Ljava/lang/String;
    .registers 9

    mul-int/lit8 p0, p0, 0x8

    rsub-int/lit8 p0, p0, 0x10

    mul-int/lit8 p1, p1, 0xf

    rsub-int/lit8 p1, p1, 0x12

    new-instance v0, Ljava/lang/String;

    const/4 v5, 0x0

    sget-object v4, Lbr/com/itau/sdk/android/core/endpoint/c;->e:[B

    mul-int/lit8 p2, p2, 0x2

    add-int/lit8 p2, p2, 0x41

    new-array v1, p0, [B

    add-int/lit8 p0, p0, -0x1

    if-nez v4, :cond_1d

    move v2, p0

    move v3, p2

    :goto_19
    neg-int v3, v3

    add-int/2addr v2, v3

    add-int/lit8 p2, v2, -0x13

    :cond_1d
    int-to-byte v2, p2

    aput-byte v2, v1, v5

    move v2, v5

    add-int/lit8 v5, v5, 0x1

    if-ne v2, p0, :cond_2a

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Ljava/lang/String;-><init>([BI)V

    return-object v0

    :cond_2a
    move v2, p2

    add-int/lit8 p1, p1, 0x1

    aget-byte v3, v4, p1

    goto :goto_19
.end method

.method private a()Lokhttp3/Response;
    .registers 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 77
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 79
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/endpoint/c;->b:Lokhttp3/OkHttpClient;

    invoke-virtual {v0}, Lokhttp3/OkHttpClient;->interceptors()Ljava/util/List;

    move-result-object v0

    invoke-interface {v7, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 80
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/endpoint/c;->b:Lokhttp3/OkHttpClient;

    invoke-virtual {v0}, Lokhttp3/OkHttpClient;->networkInterceptors()Ljava/util/List;

    move-result-object v0

    invoke-interface {v7, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 81
    new-instance v0, Lbr/com/itau/sdk/android/core/endpoint/a/b;

    iget-object v1, p0, Lbr/com/itau/sdk/android/core/endpoint/c;->c:Ljava/lang/String;

    invoke-direct {v0, v1}, Lbr/com/itau/sdk/android/core/endpoint/a/b;-><init>(Ljava/lang/String;)V

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 83
    new-instance v0, Lokhttp3/internal/http/RealInterceptorChain;

    move-object v1, v7

    iget-object v6, p0, Lbr/com/itau/sdk/android/core/endpoint/c;->a:Lokhttp3/Request;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v6}, Lokhttp3/internal/http/RealInterceptorChain;-><init>(Ljava/util/List;Lokhttp3/internal/connection/StreamAllocation;Lokhttp3/internal/http/HttpStream;Lokhttp3/Connection;ILokhttp3/Request;)V

    move-object v8, v0

    .line 86
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/endpoint/c;->a:Lokhttp3/Request;

    invoke-interface {v8, v0}, Lokhttp3/Interceptor$Chain;->proceed(Lokhttp3/Request;)Lokhttp3/Response;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public execute()Lokhttp3/Response;
    .registers 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 45
    move-object v5, p0

    monitor-enter v5

    .line 46
    :try_start_2
    iget-boolean v0, p0, Lbr/com/itau/sdk/android/core/endpoint/c;->d:Z

    if-eqz v0, :cond_1e

    new-instance v0, Ljava/lang/IllegalStateException;

    sget-object v1, Lbr/com/itau/sdk/android/core/endpoint/c;->e:[B

    const/16 v2, 0xd

    aget-byte v1, v1, v2

    add-int/lit8 v2, v1, 0x1

    sget-object v3, Lbr/com/itau/sdk/android/core/endpoint/c;->e:[B

    const/16 v4, 0xd

    aget-byte v3, v3, v4

    invoke-static {v1, v2, v3}, Lbr/com/itau/sdk/android/core/endpoint/c;->a(III)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 47
    :cond_1e
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbr/com/itau/sdk/android/core/endpoint/c;->d:Z
    :try_end_21
    .catchall {:try_start_2 .. :try_end_21} :catchall_23

    .line 48
    monitor-exit v5

    goto :goto_26

    :catchall_23
    move-exception v6

    monitor-exit v5

    throw v6

    .line 50
    :goto_26
    invoke-direct {p0}, Lbr/com/itau/sdk/android/core/endpoint/c;->a()Lokhttp3/Response;

    move-result-object v5

    .line 51
    if-nez v5, :cond_42

    new-instance v0, Ljava/io/IOException;

    sget v1, Lbr/com/itau/sdk/android/core/endpoint/c;->f:I

    and-int/lit8 v1, v1, 0x3

    sget-object v2, Lbr/com/itau/sdk/android/core/endpoint/c;->e:[B

    const/16 v3, 0xd

    aget-byte v2, v2, v3

    add-int/lit8 v3, v2, 0x1

    invoke-static {v1, v2, v3}, Lbr/com/itau/sdk/android/core/endpoint/c;->a(III)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 52
    :cond_42
    return-object v5
.end method

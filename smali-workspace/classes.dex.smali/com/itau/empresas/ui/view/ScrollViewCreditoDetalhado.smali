.class public Lcom/itau/empresas/ui/view/ScrollViewCreditoDetalhado;
.super Landroid/widget/ScrollView;
.source "ScrollViewCreditoDetalhado.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/itau/empresas/ui/view/ScrollViewCreditoDetalhado$AoAtingirFinalScroll;
    }
.end annotation


# static fields
.field static aoAtingirFinalScroll:Lcom/itau/empresas/ui/view/ScrollViewCreditoDetalhado$AoAtingirFinalScroll;


# instance fields
.field private isDectarFinalScroll:Z

.field tamanhoViewInferior:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 2
    .param p1, "context"    # Landroid/content/Context;

    .line 36
    invoke-direct {p0, p1}, Landroid/widget/ScrollView;-><init>(Landroid/content/Context;)V

    .line 37
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .line 40
    invoke-direct {p0, p1, p2}, Landroid/widget/ScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 41
    invoke-direct {p0, p2}, Lcom/itau/empresas/ui/view/ScrollViewCreditoDetalhado;->init(Landroid/util/AttributeSet;)V

    .line 42
    return-void
.end method

.method private init(Landroid/util/AttributeSet;)V
    .registers 6
    .param p1, "attrs"    # Landroid/util/AttributeSet;

    .line 45
    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/ScrollViewCreditoDetalhado;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, Lcom/itau/empresas/R$styleable;->ScrollViewCreditoDetalhado:[I

    .line 46
    invoke-virtual {v0, p1, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v2

    .line 48
    .line 49
    .local v2, "typedArray":Landroid/content/res/TypedArray;
    const/4 v0, 0x0

    const/high16 v1, 0x42200000    # 40.0f

    invoke-virtual {v2, v0, v1}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v3

    .line 51
    .local v3, "tamanho":F
    invoke-virtual {p0, v3}, Lcom/itau/empresas/ui/view/ScrollViewCreditoDetalhado;->setTamanhoViewInferior(F)V

    .line 52
    invoke-virtual {v2}, Landroid/content/res/TypedArray;->recycle()V

    .line 53
    return-void
.end method

.method public static setScrollViewListener(Lcom/itau/empresas/ui/view/ScrollViewCreditoDetalhado$AoAtingirFinalScroll;)V
    .registers 1
    .param p0, "finalScroll"    # Lcom/itau/empresas/ui/view/ScrollViewCreditoDetalhado$AoAtingirFinalScroll;

    .line 84
    sput-object p0, Lcom/itau/empresas/ui/view/ScrollViewCreditoDetalhado;->aoAtingirFinalScroll:Lcom/itau/empresas/ui/view/ScrollViewCreditoDetalhado$AoAtingirFinalScroll;

    .line 85
    return-void
.end method

.method private transformaDpParaPixels(F)F
    .registers 5
    .param p1, "valor"    # F

    .line 71
    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/ScrollViewCreditoDetalhado;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 72
    .local v2, "r":Landroid/content/res/Resources;
    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v1, p1, v0}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    return v0
.end method


# virtual methods
.method public getTamanhoViewInferior()F
    .registers 2

    .line 76
    iget v0, p0, Lcom/itau/empresas/ui/view/ScrollViewCreditoDetalhado;->tamanhoViewInferior:F

    return v0
.end method

.method public isDectarFinalScroll()Z
    .registers 2

    .line 23
    iget-boolean v0, p0, Lcom/itau/empresas/ui/view/ScrollViewCreditoDetalhado;->isDectarFinalScroll:Z

    return v0
.end method

.method protected onScrollChanged(IIII)V
    .registers 12
    .param p1, "l"    # I
    .param p2, "t"    # I
    .param p3, "oldl"    # I
    .param p4, "oldt"    # I

    .line 57
    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/ScrollViewCreditoDetalhado;->getChildCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0}, Lcom/itau/empresas/ui/view/ScrollViewCreditoDetalhado;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 59
    .local v3, "view":Landroid/view/View;
    invoke-virtual {v3}, Landroid/view/View;->getBottom()I

    move-result v0

    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/ScrollViewCreditoDetalhado;->getHeight()I

    move-result v1

    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/ScrollViewCreditoDetalhado;->getScrollY()I

    move-result v2

    add-int/2addr v1, v2

    sub-int/2addr v0, v1

    int-to-float v4, v0

    .line 60
    .local v4, "diferenca":F
    invoke-direct {p0, v4}, Lcom/itau/empresas/ui/view/ScrollViewCreditoDetalhado;->transformaDpParaPixels(F)F

    move-result v5

    .line 62
    .local v5, "diferencaPixels":F
    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/ScrollViewCreditoDetalhado;->getTamanhoViewInferior()F

    move-result v0

    invoke-direct {p0, v0}, Lcom/itau/empresas/ui/view/ScrollViewCreditoDetalhado;->transformaDpParaPixels(F)F

    move-result v6

    .line 64
    .local v6, "pixels":F
    cmpg-float v0, v5, v6

    if-gez v0, :cond_34

    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/ScrollViewCreditoDetalhado;->isDectarFinalScroll()Z

    move-result v0

    if-eqz v0, :cond_34

    .line 65
    sget-object v0, Lcom/itau/empresas/ui/view/ScrollViewCreditoDetalhado;->aoAtingirFinalScroll:Lcom/itau/empresas/ui/view/ScrollViewCreditoDetalhado$AoAtingirFinalScroll;

    invoke-interface {v0}, Lcom/itau/empresas/ui/view/ScrollViewCreditoDetalhado$AoAtingirFinalScroll;->aoFinalScroll()V

    .line 67
    :cond_34
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/ScrollView;->onScrollChanged(IIII)V

    .line 68
    return-void
.end method

.method public setDectarFinalScroll(Z)V
    .registers 2
    .param p1, "dectarFinalScroll"    # Z

    .line 27
    iput-boolean p1, p0, Lcom/itau/empresas/ui/view/ScrollViewCreditoDetalhado;->isDectarFinalScroll:Z

    .line 28
    return-void
.end method

.method public setTamanhoViewInferior(F)V
    .registers 2
    .param p1, "tamanhoViewInferior"    # F

    .line 80
    iput p1, p0, Lcom/itau/empresas/ui/view/ScrollViewCreditoDetalhado;->tamanhoViewInferior:F

    .line 81
    return-void
.end method

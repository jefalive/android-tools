.class final Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/AnimationsUtils$6;
.super Landroid/view/animation/Animation;
.source "AnimationsUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/AnimationsUtils;->animateProgressBar(Landroid/widget/ProgressBar;FF)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$from:F

.field final synthetic val$progressBar:Landroid/widget/ProgressBar;

.field final synthetic val$to:F


# direct methods
.method constructor <init>(Landroid/widget/ProgressBar;FF)V
    .registers 4

    .line 202
    iput-object p1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/AnimationsUtils$6;->val$progressBar:Landroid/widget/ProgressBar;

    iput p2, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/AnimationsUtils$6;->val$from:F

    iput p3, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/AnimationsUtils$6;->val$to:F

    invoke-direct {p0}, Landroid/view/animation/Animation;-><init>()V

    return-void
.end method


# virtual methods
.method protected applyTransformation(FLandroid/view/animation/Transformation;)V
    .registers 7
    .param p1, "interpolatedTime"    # F
    .param p2, "t"    # Landroid/view/animation/Transformation;

    .line 205
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/AnimationsUtils$6;->val$progressBar:Landroid/widget/ProgressBar;

    iget v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/AnimationsUtils$6;->val$from:F

    iget v2, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/AnimationsUtils$6;->val$to:F

    iget v3, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/AnimationsUtils$6;->val$from:F

    sub-float/2addr v2, v3

    mul-float/2addr v2, p1

    add-float/2addr v1, v2

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 206
    return-void
.end method

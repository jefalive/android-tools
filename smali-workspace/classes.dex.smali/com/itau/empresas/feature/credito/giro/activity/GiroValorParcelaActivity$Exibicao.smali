.class public final enum Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity$Exibicao;
.super Ljava/lang/Enum;
.source "GiroValorParcelaActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Exibicao"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity$Exibicao;>;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity$Exibicao;

.field public static final enum PARCELA:Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity$Exibicao;

.field public static final enum VALOR:Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity$Exibicao;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .line 235
    new-instance v0, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity$Exibicao;

    const-string v1, "VALOR"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity$Exibicao;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity$Exibicao;->VALOR:Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity$Exibicao;

    .line 236
    new-instance v0, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity$Exibicao;

    const-string v1, "PARCELA"

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity$Exibicao;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity$Exibicao;->PARCELA:Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity$Exibicao;

    .line 234
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity$Exibicao;

    sget-object v1, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity$Exibicao;->VALOR:Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity$Exibicao;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sget-object v1, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity$Exibicao;->PARCELA:Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity$Exibicao;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    sput-object v0, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity$Exibicao;->$VALUES:[Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity$Exibicao;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 234
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity$Exibicao;
    .registers 2
    .param p0, "name"    # Ljava/lang/String;

    .line 234
    const-class v0, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity$Exibicao;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity$Exibicao;

    return-object v0
.end method

.method public static values()[Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity$Exibicao;
    .registers 1

    .line 234
    sget-object v0, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity$Exibicao;->$VALUES:[Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity$Exibicao;

    invoke-virtual {v0}, [Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity$Exibicao;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity$Exibicao;

    return-object v0
.end method

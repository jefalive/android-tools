.class public final Lcom/google/zxing/client/android/ViewfinderView;
.super Landroid/view/View;


# static fields
.field private static final a:[I


# instance fields
.field private b:Lcom/google/zxing/client/android/a/g;

.field private final c:Landroid/graphics/Paint;

.field private d:Landroid/graphics/Bitmap;

.field private final e:I

.field private final f:I

.field private g:I

.field private h:I

.field private i:I

.field private j:I

.field private k:Ljava/util/List;

.field private l:Ljava/util/List;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    const/16 v0, 0x8

    new-array v0, v0, [I

    fill-array-data v0, :array_a

    sput-object v0, Lcom/google/zxing/client/android/ViewfinderView;->a:[I

    return-void

    :array_a
    .array-data 4
        0x0
        0x40
        0x80
        0xc0
        0xff
        0xc0
        0x80
        0x40
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 8

    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    sget-object v0, Lbr/com/itau/sdk/android/leitorboleto/R$styleable;->ViewfinderView:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v3

    :try_start_9
    sget v0, Lbr/com/itau/sdk/android/leitorboleto/R$styleable;->ViewfinderView_laserColor:I

    invoke-virtual {p0}, Lcom/google/zxing/client/android/ViewfinderView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lbr/com/itau/sdk/android/leitorboleto/R$color;->zxing_viewfinder_laser:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v3, v0, v1}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    iput v0, p0, Lcom/google/zxing/client/android/ViewfinderView;->g:I

    sget v0, Lbr/com/itau/sdk/android/leitorboleto/R$styleable;->ViewfinderView_maskColor:I

    invoke-virtual {p0}, Lcom/google/zxing/client/android/ViewfinderView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lbr/com/itau/sdk/android/leitorboleto/R$color;->zxing_viewfinder_mask:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v3, v0, v1}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    iput v0, p0, Lcom/google/zxing/client/android/ViewfinderView;->h:I

    sget v0, Lbr/com/itau/sdk/android/leitorboleto/R$styleable;->ViewfinderView_previewHeight:I

    const/16 v1, 0x96

    invoke-virtual {v3, v0, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    iput v0, p0, Lcom/google/zxing/client/android/ViewfinderView;->i:I
    :try_end_37
    .catchall {:try_start_9 .. :try_end_37} :catchall_3b

    invoke-virtual {v3}, Landroid/content/res/TypedArray;->recycle()V

    goto :goto_40

    :catchall_3b
    move-exception v4

    invoke-virtual {v3}, Landroid/content/res/TypedArray;->recycle()V

    throw v4

    :goto_40
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/google/zxing/client/android/ViewfinderView;->c:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/google/zxing/client/android/ViewfinderView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v0, Lbr/com/itau/sdk/android/leitorboleto/R$color;->zxing_result_view:I

    invoke-virtual {v4, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/google/zxing/client/android/ViewfinderView;->e:I

    sget v0, Lbr/com/itau/sdk/android/leitorboleto/R$color;->zxing_possible_result_points:I

    invoke-virtual {v4, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/google/zxing/client/android/ViewfinderView;->f:I

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/zxing/client/android/ViewfinderView;->j:I

    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x5

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/google/zxing/client/android/ViewfinderView;->k:Ljava/util/List;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/zxing/client/android/ViewfinderView;->l:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public a()V
    .registers 3

    iget-object v1, p0, Lcom/google/zxing/client/android/ViewfinderView;->d:Landroid/graphics/Bitmap;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/zxing/client/android/ViewfinderView;->d:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_a

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    :cond_a
    invoke-virtual {p0}, Lcom/google/zxing/client/android/ViewfinderView;->invalidate()V

    return-void
.end method

.method public a(Landroid/graphics/Bitmap;)V
    .registers 2

    iput-object p1, p0, Lcom/google/zxing/client/android/ViewfinderView;->d:Landroid/graphics/Bitmap;

    invoke-virtual {p0}, Lcom/google/zxing/client/android/ViewfinderView;->invalidate()V

    return-void
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
    .registers 27
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "DrawAllocation"
        }
    .end annotation

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/zxing/client/android/ViewfinderView;->b:Lcom/google/zxing/client/android/a/g;

    if-nez v0, :cond_7

    return-void

    :cond_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/zxing/client/android/ViewfinderView;->b:Lcom/google/zxing/client/android/a/g;

    invoke-virtual {v0}, Lcom/google/zxing/client/android/a/g;->g()Landroid/graphics/Rect;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/zxing/client/android/ViewfinderView;->b:Lcom/google/zxing/client/android/a/g;

    invoke-virtual {v0}, Lcom/google/zxing/client/android/a/g;->h()Landroid/graphics/Rect;

    move-result-object v8

    if-eqz v7, :cond_1b

    if-nez v8, :cond_1c

    :cond_1b
    return-void

    :cond_1c
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v9

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v10

    div-int/lit8 v11, v10, 0x2

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/zxing/client/android/ViewfinderView;->i:I

    div-int/lit8 v12, v0, 0x2

    sub-int v0, v11, v12

    iput v0, v7, Landroid/graphics/Rect;->top:I

    add-int v0, v11, v12

    iput v0, v7, Landroid/graphics/Rect;->bottom:I

    const/4 v0, 0x0

    iput v0, v7, Landroid/graphics/Rect;->left:I

    iput v9, v7, Landroid/graphics/Rect;->right:I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/zxing/client/android/ViewfinderView;->c:Landroid/graphics/Paint;

    move-object/from16 v1, p0

    iget-object v1, v1, Lcom/google/zxing/client/android/ViewfinderView;->d:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_48

    move-object/from16 v1, p0

    iget v1, v1, Lcom/google/zxing/client/android/ViewfinderView;->e:I

    goto :goto_4c

    :cond_48
    move-object/from16 v1, p0

    iget v1, v1, Lcom/google/zxing/client/android/ViewfinderView;->h:I

    :goto_4c
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    move-object/from16 v0, p1

    int-to-float v3, v9

    iget v1, v7, Landroid/graphics/Rect;->top:I

    int-to-float v4, v1

    move-object/from16 v1, p0

    iget-object v5, v1, Lcom/google/zxing/client/android/ViewfinderView;->c:Landroid/graphics/Paint;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    move-object/from16 v0, p1

    iget v1, v7, Landroid/graphics/Rect;->top:I

    int-to-float v2, v1

    iget v1, v7, Landroid/graphics/Rect;->left:I

    int-to-float v3, v1

    iget v1, v7, Landroid/graphics/Rect;->bottom:I

    add-int/lit8 v1, v1, 0x1

    int-to-float v4, v1

    move-object/from16 v1, p0

    iget-object v5, v1, Lcom/google/zxing/client/android/ViewfinderView;->c:Landroid/graphics/Paint;

    const/4 v1, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    move-object/from16 v0, p1

    iget v1, v7, Landroid/graphics/Rect;->right:I

    add-int/lit8 v1, v1, 0x1

    int-to-float v1, v1

    iget v2, v7, Landroid/graphics/Rect;->top:I

    int-to-float v2, v2

    int-to-float v3, v9

    iget v4, v7, Landroid/graphics/Rect;->bottom:I

    add-int/lit8 v4, v4, 0x1

    int-to-float v4, v4

    move-object/from16 v5, p0

    iget-object v5, v5, Lcom/google/zxing/client/android/ViewfinderView;->c:Landroid/graphics/Paint;

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    move-object/from16 v0, p1

    iget v1, v7, Landroid/graphics/Rect;->bottom:I

    add-int/lit8 v1, v1, 0x1

    int-to-float v2, v1

    int-to-float v3, v9

    int-to-float v4, v10

    move-object/from16 v1, p0

    iget-object v5, v1, Lcom/google/zxing/client/android/ViewfinderView;->c:Landroid/graphics/Paint;

    const/4 v1, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/zxing/client/android/ViewfinderView;->d:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_ba

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/zxing/client/android/ViewfinderView;->c:Landroid/graphics/Paint;

    const/16 v1, 0xa0

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/zxing/client/android/ViewfinderView;->d:Landroid/graphics/Bitmap;

    move-object/from16 v1, p0

    iget-object v1, v1, Lcom/google/zxing/client/android/ViewfinderView;->c:Landroid/graphics/Paint;

    move-object/from16 v2, p1

    const/4 v3, 0x0

    invoke-virtual {v2, v0, v3, v7, v1}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    goto/16 :goto_201

    :cond_ba
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/zxing/client/android/ViewfinderView;->c:Landroid/graphics/Paint;

    move-object/from16 v1, p0

    iget v1, v1, Lcom/google/zxing/client/android/ViewfinderView;->g:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/zxing/client/android/ViewfinderView;->c:Landroid/graphics/Paint;

    sget-object v1, Lcom/google/zxing/client/android/ViewfinderView;->a:[I

    move-object/from16 v2, p0

    iget v2, v2, Lcom/google/zxing/client/android/ViewfinderView;->j:I

    aget v1, v1, v2

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/zxing/client/android/ViewfinderView;->j:I

    add-int/lit8 v0, v0, 0x1

    sget-object v1, Lcom/google/zxing/client/android/ViewfinderView;->a:[I

    array-length v1, v1

    rem-int/2addr v0, v1

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/zxing/client/android/ViewfinderView;->j:I

    invoke-virtual {v7}, Landroid/graphics/Rect;->height()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    iget v1, v7, Landroid/graphics/Rect;->top:I

    add-int v13, v0, v1

    move-object/from16 v0, p1

    iget v1, v7, Landroid/graphics/Rect;->left:I

    add-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    add-int/lit8 v2, v13, -0x1

    int-to-float v2, v2

    iget v3, v7, Landroid/graphics/Rect;->right:I

    add-int/lit8 v3, v3, -0x1

    int-to-float v3, v3

    add-int/lit8 v4, v13, 0x2

    int-to-float v4, v4

    move-object/from16 v5, p0

    iget-object v5, v5, Lcom/google/zxing/client/android/ViewfinderView;->c:Landroid/graphics/Paint;

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    invoke-virtual {v7}, Landroid/graphics/Rect;->width()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {v8}, Landroid/graphics/Rect;->width()I

    move-result v1

    int-to-float v1, v1

    div-float v14, v0, v1

    invoke-virtual {v7}, Landroid/graphics/Rect;->height()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {v8}, Landroid/graphics/Rect;->height()I

    move-result v1

    int-to-float v1, v1

    div-float v15, v0, v1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/zxing/client/android/ViewfinderView;->k:Ljava/util/List;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/zxing/client/android/ViewfinderView;->l:Ljava/util/List;

    move-object/from16 v17, v0

    iget v0, v7, Landroid/graphics/Rect;->left:I

    move/from16 v18, v0

    iget v0, v7, Landroid/graphics/Rect;->top:I

    move/from16 v19, v0

    invoke-interface/range {v16 .. v16}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_13e

    const/4 v0, 0x0

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/zxing/client/android/ViewfinderView;->l:Ljava/util/List;

    goto/16 :goto_19a

    :cond_13e
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x5

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/zxing/client/android/ViewfinderView;->k:Ljava/util/List;

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/zxing/client/android/ViewfinderView;->l:Ljava/util/List;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/zxing/client/android/ViewfinderView;->c:Landroid/graphics/Paint;

    const/16 v1, 0xa0

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/zxing/client/android/ViewfinderView;->c:Landroid/graphics/Paint;

    move-object/from16 v1, p0

    iget v1, v1, Lcom/google/zxing/client/android/ViewfinderView;->f:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    move-object/from16 v20, v16

    monitor-enter v20

    :try_start_165
    invoke-interface/range {v16 .. v16}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v21

    :goto_169
    invoke-interface/range {v21 .. v21}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_195

    invoke-interface/range {v21 .. v21}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object/from16 v22, v0

    check-cast v22, Lcom/google/zxing/m;

    invoke-virtual/range {v22 .. v22}, Lcom/google/zxing/m;->a()F

    move-result v0

    mul-float/2addr v0, v14

    float-to-int v0, v0

    add-int v0, v0, v18

    int-to-float v0, v0

    invoke-virtual/range {v22 .. v22}, Lcom/google/zxing/m;->b()F

    move-result v1

    mul-float/2addr v1, v15

    float-to-int v1, v1

    add-int v1, v1, v19

    int-to-float v1, v1

    move-object/from16 v2, p0

    iget-object v2, v2, Lcom/google/zxing/client/android/ViewfinderView;->c:Landroid/graphics/Paint;

    move-object/from16 v3, p1

    const/high16 v4, 0x40c00000    # 6.0f

    invoke-virtual {v3, v0, v1, v4, v2}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V
    :try_end_194
    .catchall {:try_start_165 .. :try_end_194} :catchall_197

    goto :goto_169

    :cond_195
    monitor-exit v20

    goto :goto_19a

    :catchall_197
    move-exception v23

    monitor-exit v20

    throw v23

    :goto_19a
    if-eqz v17, :cond_1ea

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/zxing/client/android/ViewfinderView;->c:Landroid/graphics/Paint;

    const/16 v1, 0x50

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/zxing/client/android/ViewfinderView;->c:Landroid/graphics/Paint;

    move-object/from16 v1, p0

    iget v1, v1, Lcom/google/zxing/client/android/ViewfinderView;->f:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    move-object/from16 v20, v17

    monitor-enter v20

    const/high16 v21, 0x40400000    # 3.0f

    :try_start_1b5
    invoke-interface/range {v17 .. v17}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v22

    :goto_1b9
    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1e5

    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object/from16 v23, v0

    check-cast v23, Lcom/google/zxing/m;

    invoke-virtual/range {v23 .. v23}, Lcom/google/zxing/m;->a()F

    move-result v0

    mul-float/2addr v0, v14

    float-to-int v0, v0

    add-int v0, v0, v18

    int-to-float v0, v0

    invoke-virtual/range {v23 .. v23}, Lcom/google/zxing/m;->b()F

    move-result v1

    mul-float/2addr v1, v15

    float-to-int v1, v1

    add-int v1, v1, v19

    int-to-float v1, v1

    move-object/from16 v2, p0

    iget-object v2, v2, Lcom/google/zxing/client/android/ViewfinderView;->c:Landroid/graphics/Paint;

    move-object/from16 v3, p1

    move/from16 v4, v21

    invoke-virtual {v3, v0, v1, v4, v2}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V
    :try_end_1e4
    .catchall {:try_start_1b5 .. :try_end_1e4} :catchall_1e7

    goto :goto_1b9

    :cond_1e5
    monitor-exit v20

    goto :goto_1ea

    :catchall_1e7
    move-exception v24

    monitor-exit v20

    throw v24

    :cond_1ea
    :goto_1ea
    move-object/from16 v0, p0

    iget v1, v7, Landroid/graphics/Rect;->left:I

    add-int/lit8 v3, v1, -0x6

    iget v1, v7, Landroid/graphics/Rect;->top:I

    add-int/lit8 v4, v1, -0x6

    iget v1, v7, Landroid/graphics/Rect;->right:I

    add-int/lit8 v5, v1, 0x6

    iget v1, v7, Landroid/graphics/Rect;->bottom:I

    add-int/lit8 v6, v1, 0x6

    const-wide/16 v1, 0x50

    invoke-virtual/range {v0 .. v6}, Lcom/google/zxing/client/android/ViewfinderView;->postInvalidateDelayed(JIIII)V

    :goto_201
    return-void
.end method

.method public setCameraManager(Lcom/google/zxing/client/android/a/g;)V
    .registers 2

    iput-object p1, p0, Lcom/google/zxing/client/android/ViewfinderView;->b:Lcom/google/zxing/client/android/a/g;

    return-void
.end method

.method public setLaserColor(I)V
    .registers 2

    iput p1, p0, Lcom/google/zxing/client/android/ViewfinderView;->g:I

    invoke-virtual {p0}, Lcom/google/zxing/client/android/ViewfinderView;->invalidate()V

    return-void
.end method

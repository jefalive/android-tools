.class public Lcom/itau/empresas/feature/home/menu/ServicosFragment;
.super Landroid/support/v4/app/Fragment;
.source "ServicosFragment.java"

# interfaces
.implements Lcom/itau/empresas/feature/home/menu/adapter/CardMenuListener;


# instance fields
.field menu:Lcom/itau/empresas/api/model/MenuVO;

.field recyclerViewMenu:Landroid/support/v7/widget/RecyclerView;


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 23
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    return-void
.end method

.method private configuraEmptyState(Landroid/view/View;)V
    .registers 6
    .param p1, "view"    # Landroid/view/View;

    .line 91
    invoke-virtual {p0}, Lcom/itau/empresas/feature/home/menu/ServicosFragment;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 92
    const v1, 0x7f0e0474

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    .line 93
    const v2, 0x7f070473

    invoke-virtual {p0, v2}, Lcom/itau/empresas/feature/home/menu/ServicosFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 91
    const v3, 0x7f070284

    invoke-static {v0, v1, v2, v3}, Lcom/itau/empresas/ui/util/EmptyStateUtil;->mostraEmptyState(Landroid/content/Context;Landroid/view/ViewGroup;Ljava/lang/String;I)V

    .line 95
    return-void
.end method

.method private criaRecyclerView(Landroid/view/View;)V
    .registers 4
    .param p1, "view"    # Landroid/view/View;

    .line 86
    const v0, 0x7f0e0477

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    iput-object v0, p0, Lcom/itau/empresas/feature/home/menu/ServicosFragment;->recyclerViewMenu:Landroid/support/v7/widget/RecyclerView;

    .line 87
    iget-object v0, p0, Lcom/itau/empresas/feature/home/menu/ServicosFragment;->recyclerViewMenu:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, Lcom/itau/empresas/feature/home/menu/ServicosFragment;->menu:Lcom/itau/empresas/api/model/MenuVO;

    invoke-static {v0, v1, p0}, Lcom/itau/empresas/feature/home/menu/ConfiguradorRecyclerViewMenu;->configura(Landroid/support/v7/widget/RecyclerView;Lcom/itau/empresas/api/model/MenuVO;Lcom/itau/empresas/feature/home/menu/adapter/CardMenuListener;)V

    .line 88
    return-void
.end method

.method private disparaEventoAnalytics()V
    .registers 5

    .line 106
    invoke-static {}, Lcom/itau/empresas/feature/analytics/AnalyticsHelper;->getInstance()Lcom/itau/empresas/feature/analytics/AnalyticsHelper;

    move-result-object v3

    .line 108
    .local v3, "analyticsHelper":Lcom/itau/empresas/feature/analytics/AnalyticsHelper;
    iget-object v0, p0, Lcom/itau/empresas/feature/home/menu/ServicosFragment;->menu:Lcom/itau/empresas/api/model/MenuVO;

    if-eqz v0, :cond_14

    iget-object v0, p0, Lcom/itau/empresas/feature/home/menu/ServicosFragment;->menu:Lcom/itau/empresas/api/model/MenuVO;

    invoke-virtual {v0}, Lcom/itau/empresas/api/model/MenuVO;->getFilhos()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2d

    .line 109
    .line 110
    :cond_14
    const v0, 0x7f0700f8

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/home/menu/ServicosFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 111
    const v1, 0x7f0700d0

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/home/menu/ServicosFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 112
    const v2, 0x7f0700c3

    invoke-virtual {p0, v2}, Lcom/itau/empresas/feature/home/menu/ServicosFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 109
    invoke-virtual {v3, v0, v1, v2}, Lcom/itau/empresas/feature/analytics/AnalyticsHelper;->eventoAdobeState(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 113
    return-void

    .line 116
    .line 117
    :cond_2d
    const v0, 0x7f0700f7

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/home/menu/ServicosFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 118
    const v1, 0x7f0700d0

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/home/menu/ServicosFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 119
    const v2, 0x7f0700c3

    invoke-virtual {p0, v2}, Lcom/itau/empresas/feature/home/menu/ServicosFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 116
    invoke-virtual {v3, v0, v1, v2}, Lcom/itau/empresas/feature/analytics/AnalyticsHelper;->eventoAdobeState(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 120
    return-void
.end method

.method private iniciaLayout(Landroid/view/View;)V
    .registers 3
    .param p1, "view"    # Landroid/view/View;

    .line 78
    iget-object v0, p0, Lcom/itau/empresas/feature/home/menu/ServicosFragment;->menu:Lcom/itau/empresas/api/model/MenuVO;

    if-eqz v0, :cond_10

    iget-object v0, p0, Lcom/itau/empresas/feature/home/menu/ServicosFragment;->menu:Lcom/itau/empresas/api/model/MenuVO;

    invoke-virtual {v0}, Lcom/itau/empresas/api/model/MenuVO;->getFilhos()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_14

    .line 79
    :cond_10
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/home/menu/ServicosFragment;->configuraEmptyState(Landroid/view/View;)V

    goto :goto_17

    .line 81
    :cond_14
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/home/menu/ServicosFragment;->criaRecyclerView(Landroid/view/View;)V

    .line 83
    :goto_17
    return-void
.end method

.method public static newInstance(Lcom/itau/empresas/api/model/MenuVO;)Lcom/itau/empresas/feature/home/menu/ServicosFragment;
    .registers 4
    .param p0, "menuVO"    # Lcom/itau/empresas/api/model/MenuVO;

    .line 98
    new-instance v1, Lcom/itau/empresas/feature/home/menu/ServicosFragment;

    invoke-direct {v1}, Lcom/itau/empresas/feature/home/menu/ServicosFragment;-><init>()V

    .line 99
    .local v1, "fragment":Lcom/itau/empresas/feature/home/menu/ServicosFragment;
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 100
    .local v2, "args":Landroid/os/Bundle;
    const-string v0, "menuItem"

    invoke-virtual {v2, v0, p0}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 101
    invoke-virtual {v1, v2}, Lcom/itau/empresas/feature/home/menu/ServicosFragment;->setArguments(Landroid/os/Bundle;)V

    .line 102
    return-object v1
.end method


# virtual methods
.method abreFragment(Landroid/support/v4/app/Fragment;)V
    .registers 5
    .param p1, "fragment"    # Landroid/support/v4/app/Fragment;

    .line 70
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    .line 72
    .local v2, "fragmentName":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/itau/empresas/feature/home/menu/ServicosFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    .line 73
    const v1, 0x7f0e01b9

    invoke-virtual {v0, v1, p1, v2}, Landroid/support/v4/app/FragmentTransaction;->replace(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    .line 74
    invoke-virtual {v0, v2}, Landroid/support/v4/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 75
    return-void
.end method

.method public menuSelecionado(Lcom/itau/empresas/api/model/MenuVO;Z)V
    .registers 6
    .param p1, "menu"    # Lcom/itau/empresas/api/model/MenuVO;
    .param p2, "temSubMenu"    # Z

    .line 52
    if-nez p1, :cond_3

    .line 53
    return-void

    .line 54
    :cond_3
    if-eqz p2, :cond_d

    .line 55
    invoke-static {p1}, Lcom/itau/empresas/feature/home/menu/SubMenuFragment;->newInstance(Lcom/itau/empresas/api/model/MenuVO;)Lcom/itau/empresas/feature/home/menu/SubMenuFragment;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/home/menu/ServicosFragment;->abreFragment(Landroid/support/v4/app/Fragment;)V

    goto :goto_35

    .line 57
    :cond_d
    invoke-virtual {p1}, Lcom/itau/empresas/api/model/MenuVO;->getChaveMobile()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/itau/empresas/ui/util/menu/InformacaoMenu;->getInstance(Ljava/lang/String;)Lcom/itau/empresas/ui/util/menu/InformacaoMenu;

    move-result-object v0

    .line 58
    invoke-virtual {v0}, Lcom/itau/empresas/ui/util/menu/InformacaoMenu;->getCommand()Lcom/itau/empresas/ui/util/menu/Command;

    move-result-object v2

    .line 59
    .local v2, "command":Lcom/itau/empresas/ui/util/menu/Command;
    if-eqz v2, :cond_35

    .line 60
    invoke-virtual {p0}, Lcom/itau/empresas/feature/home/menu/ServicosFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Landroid/support/v7/app/AppCompatActivity;

    invoke-interface {v2, v0, p1}, Lcom/itau/empresas/ui/util/menu/Command;->executar(Landroid/support/v7/app/AppCompatActivity;Lcom/itau/empresas/api/model/MenuVO;)V

    .line 62
    .line 63
    invoke-virtual {p0}, Lcom/itau/empresas/feature/home/menu/ServicosFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/CustomApplication;

    .line 64
    invoke-virtual {p1}, Lcom/itau/empresas/api/model/MenuVO;->getChaveMobile()Ljava/lang/String;

    move-result-object v1

    .line 62
    invoke-static {v0, v1}, Lcom/itau/empresas/ui/util/AtendimentoUtil;->registraContextualizacaoFiltroAtendimento(Lcom/itau/empresas/CustomApplication;Ljava/lang/String;)V

    .line 67
    .end local v2    # "command":Lcom/itau/empresas/ui/util/menu/Command;
    :cond_35
    :goto_35
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 43
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 45
    invoke-virtual {p0}, Lcom/itau/empresas/feature/home/menu/ServicosFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_17

    .line 46
    invoke-virtual {p0}, Lcom/itau/empresas/feature/home/menu/ServicosFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "menuItem"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/api/model/MenuVO;

    iput-object v0, p0, Lcom/itau/empresas/feature/home/menu/ServicosFragment;->menu:Lcom/itau/empresas/api/model/MenuVO;

    .line 48
    :cond_17
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .registers 7
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .line 35
    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    const v1, 0x7f0300bf

    invoke-virtual {p1, v1, p2, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 36
    .local v2, "view":Landroid/view/View;
    invoke-direct {p0, v2}, Lcom/itau/empresas/feature/home/menu/ServicosFragment;->iniciaLayout(Landroid/view/View;)V

    .line 37
    invoke-direct {p0}, Lcom/itau/empresas/feature/home/menu/ServicosFragment;->disparaEventoAnalytics()V

    .line 38
    return-object v2
.end method

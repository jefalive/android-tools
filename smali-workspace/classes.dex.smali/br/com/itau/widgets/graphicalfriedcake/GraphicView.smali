.class public Lbr/com/itau/widgets/graphicalfriedcake/GraphicView;
.super Landroid/widget/LinearLayout;
.source "GraphicView.java"

# interfaces
.implements Lbr/com/itau/widgets/graphicalfriedcake/charts/listener/OnChartValueSelectedListener;


# instance fields
.field private angles:[F

.field private currentPosition:I

.field private indicatorDetailsList:Lbr/com/itau/widgets/graphicalfriedcake/PageIndicator;

.field private items:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Lbr/com/itau/widgets/graphicalfriedcake/model/ItemVO;>;"
        }
    .end annotation
.end field

.field protected mParties:[Ljava/lang/String;

.field private pagerDetailsList:Landroid/support/v4/view/ViewPager;

.field private pieChart:Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 5
    .param p1, "context"    # Landroid/content/Context;

    .line 41
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 28
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/GraphicView;->items:Ljava/util/List;

    .line 35
    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "Party A"

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const-string v1, "Party B"

    const/4 v2, 0x1

    aput-object v1, v0, v2

    const-string v1, "Party C"

    const/4 v2, 0x2

    aput-object v1, v0, v2

    const-string v1, "Party D"

    const/4 v2, 0x3

    aput-object v1, v0, v2

    const-string v1, "Party E"

    const/4 v2, 0x4

    aput-object v1, v0, v2

    const-string v1, "Party F"

    const/4 v2, 0x5

    aput-object v1, v0, v2

    const-string v1, "Party G"

    const/4 v2, 0x6

    aput-object v1, v0, v2

    const-string v1, "Party H"

    const/4 v2, 0x7

    aput-object v1, v0, v2

    const-string v1, "Party I"

    const/16 v2, 0x8

    aput-object v1, v0, v2

    const-string v1, "Party J"

    const/16 v2, 0x9

    aput-object v1, v0, v2

    iput-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/GraphicView;->mParties:[Ljava/lang/String;

    .line 42
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .line 45
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 28
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/GraphicView;->items:Ljava/util/List;

    .line 35
    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "Party A"

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const-string v1, "Party B"

    const/4 v2, 0x1

    aput-object v1, v0, v2

    const-string v1, "Party C"

    const/4 v2, 0x2

    aput-object v1, v0, v2

    const-string v1, "Party D"

    const/4 v2, 0x3

    aput-object v1, v0, v2

    const-string v1, "Party E"

    const/4 v2, 0x4

    aput-object v1, v0, v2

    const-string v1, "Party F"

    const/4 v2, 0x5

    aput-object v1, v0, v2

    const-string v1, "Party G"

    const/4 v2, 0x6

    aput-object v1, v0, v2

    const-string v1, "Party H"

    const/4 v2, 0x7

    aput-object v1, v0, v2

    const-string v1, "Party I"

    const/16 v2, 0x8

    aput-object v1, v0, v2

    const-string v1, "Party J"

    const/16 v2, 0x9

    aput-object v1, v0, v2

    iput-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/GraphicView;->mParties:[Ljava/lang/String;

    .line 46
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .line 49
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 28
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/GraphicView;->items:Ljava/util/List;

    .line 35
    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "Party A"

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const-string v1, "Party B"

    const/4 v2, 0x1

    aput-object v1, v0, v2

    const-string v1, "Party C"

    const/4 v2, 0x2

    aput-object v1, v0, v2

    const-string v1, "Party D"

    const/4 v2, 0x3

    aput-object v1, v0, v2

    const-string v1, "Party E"

    const/4 v2, 0x4

    aput-object v1, v0, v2

    const-string v1, "Party F"

    const/4 v2, 0x5

    aput-object v1, v0, v2

    const-string v1, "Party G"

    const/4 v2, 0x6

    aput-object v1, v0, v2

    const-string v1, "Party H"

    const/4 v2, 0x7

    aput-object v1, v0, v2

    const-string v1, "Party I"

    const/16 v2, 0x8

    aput-object v1, v0, v2

    const-string v1, "Party J"

    const/16 v2, 0x9

    aput-object v1, v0, v2

    iput-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/GraphicView;->mParties:[Ljava/lang/String;

    .line 50
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .registers 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I
    .param p4, "defStyleRes"    # I
    .annotation build Landroid/annotation/TargetApi;
        value = 0x15
    .end annotation

    .line 54
    invoke-direct {p0, p1, p2, p3, p4}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    .line 28
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/GraphicView;->items:Ljava/util/List;

    .line 35
    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "Party A"

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const-string v1, "Party B"

    const/4 v2, 0x1

    aput-object v1, v0, v2

    const-string v1, "Party C"

    const/4 v2, 0x2

    aput-object v1, v0, v2

    const-string v1, "Party D"

    const/4 v2, 0x3

    aput-object v1, v0, v2

    const-string v1, "Party E"

    const/4 v2, 0x4

    aput-object v1, v0, v2

    const-string v1, "Party F"

    const/4 v2, 0x5

    aput-object v1, v0, v2

    const-string v1, "Party G"

    const/4 v2, 0x6

    aput-object v1, v0, v2

    const-string v1, "Party H"

    const/4 v2, 0x7

    aput-object v1, v0, v2

    const-string v1, "Party I"

    const/16 v2, 0x8

    aput-object v1, v0, v2

    const-string v1, "Party J"

    const/16 v2, 0x9

    aput-object v1, v0, v2

    iput-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/GraphicView;->mParties:[Ljava/lang/String;

    .line 55
    return-void
.end method

.method static synthetic access$000(Lbr/com/itau/widgets/graphicalfriedcake/GraphicView;)Landroid/support/v4/view/ViewPager;
    .registers 2
    .param p0, "x0"    # Lbr/com/itau/widgets/graphicalfriedcake/GraphicView;

    .line 26
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/GraphicView;->pagerDetailsList:Landroid/support/v4/view/ViewPager;

    return-object v0
.end method

.method static synthetic access$100(Lbr/com/itau/widgets/graphicalfriedcake/GraphicView;)Ljava/util/List;
    .registers 2
    .param p0, "x0"    # Lbr/com/itau/widgets/graphicalfriedcake/GraphicView;

    .line 26
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/GraphicView;->items:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$200(Lbr/com/itau/widgets/graphicalfriedcake/GraphicView;I)V
    .registers 2
    .param p0, "x0"    # Lbr/com/itau/widgets/graphicalfriedcake/GraphicView;
    .param p1, "x1"    # I

    .line 26
    invoke-direct {p0, p1}, Lbr/com/itau/widgets/graphicalfriedcake/GraphicView;->rotateChartToPosition(I)V

    return-void
.end method

.method static synthetic access$300(Lbr/com/itau/widgets/graphicalfriedcake/GraphicView;)Lbr/com/itau/widgets/graphicalfriedcake/PageIndicator;
    .registers 2
    .param p0, "x0"    # Lbr/com/itau/widgets/graphicalfriedcake/GraphicView;

    .line 26
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/GraphicView;->indicatorDetailsList:Lbr/com/itau/widgets/graphicalfriedcake/PageIndicator;

    return-object v0
.end method

.method static synthetic access$402(Lbr/com/itau/widgets/graphicalfriedcake/GraphicView;I)I
    .registers 2
    .param p0, "x0"    # Lbr/com/itau/widgets/graphicalfriedcake/GraphicView;
    .param p1, "x1"    # I

    .line 26
    iput p1, p0, Lbr/com/itau/widgets/graphicalfriedcake/GraphicView;->currentPosition:I

    return p1
.end method

.method private initPiechart()V
    .registers 4

    .line 66
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/GraphicView;->pieChart:Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->setDescription(Ljava/lang/String;)V

    .line 67
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/GraphicView;->pieChart:Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;

    const v1, 0x3f733333    # 0.95f

    invoke-virtual {v0, v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->setDragDecelerationFrictionCoef(F)V

    .line 68
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/GraphicView;->pieChart:Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->setDrawMarkerViews(Z)V

    .line 69
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/GraphicView;->pieChart:Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->setDrawHoleEnabled(Z)V

    .line 70
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/GraphicView;->pieChart:Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->setHoleColorTransparent(Z)V

    .line 71
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/GraphicView;->pieChart:Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->setTransparentCircleColor(I)V

    .line 72
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/GraphicView;->pieChart:Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;

    const/high16 v1, 0x423c0000    # 47.0f

    invoke-virtual {v0, v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->setHoleRadius(F)V

    .line 73
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/GraphicView;->pieChart:Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;

    const/high16 v1, 0x42200000    # 40.0f

    invoke-virtual {v0, v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->setTransparentCircleRadius(F)V

    .line 74
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/GraphicView;->pieChart:Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;

    invoke-virtual {v0, p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->setOnChartValueSelectedListener(Lbr/com/itau/widgets/graphicalfriedcake/charts/listener/OnChartValueSelectedListener;)V

    .line 75
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/GraphicView;->pieChart:Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->setRotationAngle(F)V

    .line 76
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/GraphicView;->pieChart:Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->setRotationEnabled(Z)V

    .line 77
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/GraphicView;->pieChart:Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->setTouchEnabled(Z)V

    .line 78
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/GraphicView;->pieChart:Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->getLegend()Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->setEnabled(Z)V

    .line 79
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/GraphicView;->pieChart:Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->setDrawSliceText(Z)V

    .line 80
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/GraphicView;->pieChart:Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->setRotationEnabled(Z)V

    .line 82
    new-instance v2, Lbr/com/itau/widgets/graphicalfriedcake/DetailsPageAdapter;

    invoke-virtual {p0}, Lbr/com/itau/widgets/graphicalfriedcake/GraphicView;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/GraphicView;->items:Ljava/util/List;

    invoke-direct {v2, v0, v1}, Lbr/com/itau/widgets/graphicalfriedcake/DetailsPageAdapter;-><init>(Landroid/content/Context;Ljava/util/List;)V

    .line 84
    .local v2, "pa":Lbr/com/itau/widgets/graphicalfriedcake/DetailsPageAdapter;
    invoke-virtual {p0, v2}, Lbr/com/itau/widgets/graphicalfriedcake/GraphicView;->setDetailsPagerAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 86
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/GraphicView;->indicatorDetailsList:Lbr/com/itau/widgets/graphicalfriedcake/PageIndicator;

    new-instance v1, Lbr/com/itau/widgets/graphicalfriedcake/GraphicView$1;

    invoke-direct {v1, p0, v2}, Lbr/com/itau/widgets/graphicalfriedcake/GraphicView$1;-><init>(Lbr/com/itau/widgets/graphicalfriedcake/GraphicView;Lbr/com/itau/widgets/graphicalfriedcake/DetailsPageAdapter;)V

    invoke-virtual {v0, v1}, Lbr/com/itau/widgets/graphicalfriedcake/PageIndicator;->setConfiguration(Lbr/com/itau/widgets/graphicalfriedcake/PageIndicator$Configuration;)V

    .line 98
    return-void
.end method

.method private rotateChartToPosition(I)V
    .registers 10
    .param p1, "posSelected"    # I

    .line 163
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/GraphicView;->angles:[F

    array-length v0, v0

    if-ge p1, v0, :cond_7

    .line 164
    const/4 v5, 0x0

    .local v5, "angleControl":I
    goto :goto_a

    .line 167
    .end local v5    # "angleControl":I
    :cond_7
    const/16 v5, -0x168

    .line 168
    .local v5, "angleControl":I
    const/4 p1, 0x0

    .line 170
    :goto_a
    const/4 v6, 0x0

    .line 171
    .local v6, "totalAngle":F
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_c
    if-gt v7, p1, :cond_16

    .line 172
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/GraphicView;->angles:[F

    aget v0, v0, v7

    add-float/2addr v6, v0

    .line 171
    add-int/lit8 v7, v7, 0x1

    goto :goto_c

    .line 174
    .end local v7    # "i":I
    :cond_16
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/GraphicView;->angles:[F

    aget v0, v0, p1

    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr v0, v1

    sub-float v0, v6, v0

    const/high16 v1, 0x43b40000    # 360.0f

    sub-float v6, v1, v0

    .line 175
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/GraphicView;->pieChart:Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;

    iget-object v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/GraphicView;->pieChart:Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;

    invoke-virtual {v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->getRotationAngle()F

    move-result v1

    int-to-float v2, v5

    add-float/2addr v2, v6

    sget-object v3, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;->EaseInOutQuad:Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;

    const/16 v4, 0x190

    invoke-virtual {v0, v4, v1, v2, v3}, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->spin(IFFLbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;)V

    .line 177
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/GraphicView;->pieChart:Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->highlightValue(II)V

    .line 178
    return-void
.end method

.method private setData(Ljava/util/List;)V
    .registers 10
    .param p1, "list"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<Lbr/com/itau/widgets/graphicalfriedcake/model/ItemVO;>;)V"
        }
    .end annotation

    .line 108
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 109
    .local v3, "yVals1":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;>;"
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 110
    .local v4, "xVals":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_b
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v5, v0, :cond_32

    .line 111
    new-instance v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;

    invoke-interface {p1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lbr/com/itau/widgets/graphicalfriedcake/model/ItemVO;

    invoke-virtual {v1}, Lbr/com/itau/widgets/graphicalfriedcake/model/ItemVO;->getPercentual()F

    move-result v1

    invoke-direct {v0, v1, v5}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;-><init>(FI)V

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 112
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/GraphicView;->mParties:[Ljava/lang/String;

    iget-object v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/GraphicView;->mParties:[Ljava/lang/String;

    array-length v1, v1

    rem-int v1, v5, v1

    aget-object v0, v0, v1

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 110
    add-int/lit8 v5, v5, 0x1

    goto :goto_b

    .line 115
    .end local v5    # "i":I
    :cond_32
    new-instance v5, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/PieDataSet;

    const-string v0, ""

    invoke-direct {v5, v3, v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/PieDataSet;-><init>(Ljava/util/List;Ljava/lang/String;)V

    .line 117
    .local v5, "dataSet":Lbr/com/itau/widgets/graphicalfriedcake/charts/data/PieDataSet;
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 118
    .local v6, "colors":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_3f
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v7, v0, :cond_5f

    .line 119
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/GraphicView;->items:Ljava/util/List;

    invoke-interface {v0, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbr/com/itau/widgets/graphicalfriedcake/model/ItemVO;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/model/ItemVO;->getColor()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 118
    add-int/lit8 v7, v7, 0x1

    goto :goto_3f

    .line 121
    .end local v7    # "i":I
    :cond_5f
    invoke-virtual {v5, v6}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/PieDataSet;->setColors(Ljava/util/List;)V

    .line 123
    const/4 v0, 0x0

    invoke-virtual {v5, v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/PieDataSet;->setSliceSpace(F)V

    .line 124
    const/high16 v0, 0x41000000    # 8.0f

    invoke-virtual {v5, v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/PieDataSet;->setSelectionShift(F)V

    .line 125
    const/4 v0, 0x0

    invoke-virtual {v5, v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/PieDataSet;->setDrawValues(Z)V

    .line 127
    new-instance v7, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/PieData;

    invoke-direct {v7, v4, v5}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/PieData;-><init>(Ljava/util/List;Lbr/com/itau/widgets/graphicalfriedcake/charts/data/PieDataSet;)V

    .line 128
    .local v7, "data":Lbr/com/itau/widgets/graphicalfriedcake/charts/data/PieData;
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/GraphicView;->pieChart:Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;

    invoke-virtual {v0, v7}, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->setData(Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;)V

    .line 129
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/GraphicView;->pieChart:Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->invalidate()V

    .line 130
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/GraphicView;->pieChart:Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->getDrawAngles()[F

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/GraphicView;->angles:[F

    .line 131
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/GraphicView;->pieChart:Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->highlightValue(II)V

    .line 132
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lbr/com/itau/widgets/graphicalfriedcake/GraphicView;->rotateChartToPosition(I)V

    .line 133
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/GraphicView;->indicatorDetailsList:Lbr/com/itau/widgets/graphicalfriedcake/PageIndicator;

    iget-object v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/GraphicView;->items:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lbr/com/itau/widgets/graphicalfriedcake/model/ItemVO;

    invoke-virtual {v1}, Lbr/com/itau/widgets/graphicalfriedcake/model/ItemVO;->getColor()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbr/com/itau/widgets/graphicalfriedcake/PageIndicator;->refresh(Ljava/lang/String;)V

    .line 135
    return-void
.end method


# virtual methods
.method public init(Ljava/util/List;)V
    .registers 4
    .param p1, "items"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<Lbr/com/itau/widgets/graphicalfriedcake/model/ItemVO;>;)V"
        }
    .end annotation

    .line 58
    invoke-virtual {p0}, Lbr/com/itau/widgets/graphicalfriedcake/GraphicView;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lbr/com/itau/widgets/graphicalfriedcake/R$layout;->view_graphic:I

    invoke-static {v0, v1, p0}, Lbr/com/itau/widgets/graphicalfriedcake/GraphicView;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 59
    sget v0, Lbr/com/itau/widgets/graphicalfriedcake/R$id;->piechart_chart:I

    invoke-virtual {p0, v0}, Lbr/com/itau/widgets/graphicalfriedcake/GraphicView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;

    iput-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/GraphicView;->pieChart:Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;

    .line 60
    sget v0, Lbr/com/itau/widgets/graphicalfriedcake/R$id;->pager_details_list:I

    invoke-virtual {p0, v0}, Lbr/com/itau/widgets/graphicalfriedcake/GraphicView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    iput-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/GraphicView;->pagerDetailsList:Landroid/support/v4/view/ViewPager;

    .line 61
    sget v0, Lbr/com/itau/widgets/graphicalfriedcake/R$id;->indicator_details_list:I

    invoke-virtual {p0, v0}, Lbr/com/itau/widgets/graphicalfriedcake/GraphicView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lbr/com/itau/widgets/graphicalfriedcake/PageIndicator;

    iput-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/GraphicView;->indicatorDetailsList:Lbr/com/itau/widgets/graphicalfriedcake/PageIndicator;

    .line 62
    invoke-virtual {p0, p1}, Lbr/com/itau/widgets/graphicalfriedcake/GraphicView;->setItems(Ljava/util/List;)V

    .line 63
    return-void
.end method

.method public onNothingSelected()V
    .registers 1

    .line 150
    return-void
.end method

.method public onValueSelected(Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;ILbr/com/itau/widgets/graphicalfriedcake/charts/highlight/Highlight;)V
    .registers 6
    .param p1, "e"    # Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;
    .param p2, "dataSetIndex"    # I
    .param p3, "h"    # Lbr/com/itau/widgets/graphicalfriedcake/charts/highlight/Highlight;

    .line 139
    if-nez p1, :cond_3

    .line 140
    return-void

    .line 142
    :cond_3
    invoke-virtual {p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;->getXIndex()I

    move-result v1

    .line 143
    .local v1, "posSelected":I
    invoke-direct {p0, v1}, Lbr/com/itau/widgets/graphicalfriedcake/GraphicView;->rotateChartToPosition(I)V

    .line 144
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/GraphicView;->pagerDetailsList:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 145
    return-void
.end method

.method public setDetailsPagerAdapter(Landroid/support/v4/view/PagerAdapter;)V
    .registers 4
    .param p1, "adapter"    # Landroid/support/v4/view/PagerAdapter;

    .line 182
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/GraphicView;->pagerDetailsList:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, p1}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 183
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/GraphicView;->pagerDetailsList:Landroid/support/v4/view/ViewPager;

    new-instance v1, Lbr/com/itau/widgets/graphicalfriedcake/GraphicView$2;

    invoke-direct {v1, p0}, Lbr/com/itau/widgets/graphicalfriedcake/GraphicView$2;-><init>(Lbr/com/itau/widgets/graphicalfriedcake/GraphicView;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->addOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    .line 206
    return-void
.end method

.method public setItems(Ljava/util/List;)V
    .registers 2
    .param p1, "items"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<Lbr/com/itau/widgets/graphicalfriedcake/model/ItemVO;>;)V"
        }
    .end annotation

    .line 101
    iput-object p1, p0, Lbr/com/itau/widgets/graphicalfriedcake/GraphicView;->items:Ljava/util/List;

    .line 102
    invoke-direct {p0}, Lbr/com/itau/widgets/graphicalfriedcake/GraphicView;->initPiechart()V

    .line 103
    invoke-direct {p0, p1}, Lbr/com/itau/widgets/graphicalfriedcake/GraphicView;->setData(Ljava/util/List;)V

    .line 104
    return-void
.end method

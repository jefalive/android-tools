.class Lcom/itau/empresas/feature/credito/giro/controller/EmprestimosParceladosSimulacaoController$1;
.super Ljava/lang/Object;
.source "EmprestimosParceladosSimulacaoController.java"

# interfaces
.implements Lbr/com/itau/sdk/android/core/type/BackendCall;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/itau/empresas/feature/credito/giro/controller/EmprestimosParceladosSimulacaoController;->consultaSimulacao(Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/itau/empresas/feature/credito/giro/controller/EmprestimosParceladosSimulacaoController;

.field final synthetic val$ofertaAceita:Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;


# direct methods
.method constructor <init>(Lcom/itau/empresas/feature/credito/giro/controller/EmprestimosParceladosSimulacaoController;Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;)V
    .registers 3
    .param p1, "this$0"    # Lcom/itau/empresas/feature/credito/giro/controller/EmprestimosParceladosSimulacaoController;

    .line 27
    iput-object p1, p0, Lcom/itau/empresas/feature/credito/giro/controller/EmprestimosParceladosSimulacaoController$1;->this$0:Lcom/itau/empresas/feature/credito/giro/controller/EmprestimosParceladosSimulacaoController;

    iput-object p2, p0, Lcom/itau/empresas/feature/credito/giro/controller/EmprestimosParceladosSimulacaoController$1;->val$ofertaAceita:Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public callBackend()V
    .registers 3

    .line 30
    invoke-static {}, Lcom/itau/empresas/feature/credito/giro/controller/EmprestimosParceladosSimulacaoController;->getApi()Lcom/itau/empresas/api/credito/ApiGiro;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/giro/controller/EmprestimosParceladosSimulacaoController$1;->val$ofertaAceita:Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;

    invoke-interface {v0, v1}, Lcom/itau/empresas/api/credito/ApiGiro;->buscaSimulacaoEmprestimosParcelados(Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;)Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoSaidaVO;

    move-result-object v0

    # invokes: Lcom/itau/empresas/feature/credito/giro/controller/EmprestimosParceladosSimulacaoController;->post(Ljava/lang/Object;)V
    invoke-static {v0}, Lcom/itau/empresas/feature/credito/giro/controller/EmprestimosParceladosSimulacaoController;->access$000(Ljava/lang/Object;)V

    .line 32
    return-void
.end method

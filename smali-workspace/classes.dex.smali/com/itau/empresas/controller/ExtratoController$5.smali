.class Lcom/itau/empresas/controller/ExtratoController$5;
.super Ljava/lang/Object;
.source "ExtratoController.java"

# interfaces
.implements Lbr/com/itau/sdk/android/core/type/BackendCall;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/itau/empresas/controller/ExtratoController;->consultaSaldo()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/itau/empresas/controller/ExtratoController;


# direct methods
.method constructor <init>(Lcom/itau/empresas/controller/ExtratoController;)V
    .registers 2
    .param p1, "this$0"    # Lcom/itau/empresas/controller/ExtratoController;

    .line 74
    iput-object p1, p0, Lcom/itau/empresas/controller/ExtratoController$5;->this$0:Lcom/itau/empresas/controller/ExtratoController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public callBackend()V
    .registers 3

    .line 77
    iget-object v0, p0, Lcom/itau/empresas/controller/ExtratoController$5;->this$0:Lcom/itau/empresas/controller/ExtratoController;

    iget-object v0, v0, Lcom/itau/empresas/controller/ExtratoController;->app:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getEventoTracker()Lcom/itau/empresas/ui/util/analytics/TrackerEvento;

    move-result-object v0

    const-string v1, "saldo"

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/analytics/TrackerEvento;->inicioMonitorarTempo(Ljava/lang/String;)V

    .line 78
    iget-object v0, p0, Lcom/itau/empresas/controller/ExtratoController$5;->this$0:Lcom/itau/empresas/controller/ExtratoController;

    invoke-virtual {v0}, Lcom/itau/empresas/controller/ExtratoController;->api()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/api/Api;

    iget-object v1, p0, Lcom/itau/empresas/controller/ExtratoController$5;->this$0:Lcom/itau/empresas/controller/ExtratoController;

    iget-object v1, v1, Lcom/itau/empresas/controller/ExtratoController;->app:Lcom/itau/empresas/CustomApplication;

    invoke-static {v1}, Lcom/itau/empresas/ui/util/ViewUtils;->getContaCompleta(Lcom/itau/empresas/CustomApplication;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/itau/empresas/api/Api;->consultaSaldo(Ljava/lang/String;)Lcom/itau/empresas/api/model/SaldoVO;

    move-result-object v0

    invoke-static {v0}, Lcom/itau/empresas/controller/BaseController;->post(Ljava/lang/Object;)V

    .line 79
    iget-object v0, p0, Lcom/itau/empresas/controller/ExtratoController$5;->this$0:Lcom/itau/empresas/controller/ExtratoController;

    iget-object v0, v0, Lcom/itau/empresas/controller/ExtratoController;->app:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getEventoTracker()Lcom/itau/empresas/ui/util/analytics/TrackerEvento;

    move-result-object v0

    const-string v1, "saldo"

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/analytics/TrackerEvento;->fimMonitorarTempo(Ljava/lang/String;)V

    .line 80
    return-void
.end method

.class public Lcc/a;
.super Ljava/security/Provider;
.source "SourceFile"


# static fields
.field private static final a:[B

.field private static b:I


# direct methods
.method static constructor <clinit>()V
    .registers 3

    const/16 v0, 0x86

    new-array v0, v0, [B

    fill-array-data v0, :array_e

    sput-object v0, Lcc/a;->a:[B

    const/16 v0, 0xe9

    sput v0, Lcc/a;->b:I

    return-void

    :array_e
    .array-data 1
        0x1ct
        0x10t
        0x3t
        -0x10t
        0x12t
        -0x2t
        0x12t
        -0x3t
        -0xdt
        -0x13t
        0xft
        0xdt
        -0xat
        0xbt
        -0x2t
        -0x3ft
        0x25t
        -0xbt
        -0x7t
        -0x10t
        0x1ft
        0x2t
        -0x4t
        -0x7t
        0x12t
        -0x2t
        0x12t
        -0x3t
        -0xdt
        -0x13t
        0xft
        0xdt
        -0xat
        0xbt
        -0x2t
        -0x3ft
        0x25t
        -0xbt
        -0x7t
        -0x10t
        0x1ft
        0x2t
        -0x4t
        -0x7t
        -0x27t
        0x29t
        0x24t
        0x3t
        -0x4t
        -0x7t
        0x8t
        -0x8t
        0x9t
        0x6t
        -0xft
        -0x1t
        -0x1bt
        0x25t
        0x1dt
        0x5t
        0x7t
        0x3t
        -0x28t
        0x2t
        -0x4t
        -0x7t
        -0x21t
        0x2ct
        0x1dt
        0x5t
        0x7t
        0x3t
        -0x4bt
        0x46t
        -0x3t
        -0xbt
        -0x2t
        0x6t
        -0x3t
        0x3t
        -0x6t
        -0x43t
        0x52t
        -0x11t
        0xdt
        -0xat
        0xbt
        -0x2t
        -0x4dt
        0x4et
        0x7t
        -0x8t
        -0xbt
        0x3t
        0xdt
        -0x52t
        0x50t
        0x2t
        -0x3t
        0x7t
        -0xdt
        -0x5t
        0x1t
        0xdt
        -0x52t
        0x54t
        -0xct
        -0x7t
        0x13t
        -0x54t
        0x55t
        -0x2t
        -0xet
        0xet
        -0x53t
        0xft
        0x35t
        0x1t
        0x11t
        -0x47t
        0x46t
        -0x3t
        -0x11t
        0xdt
        -0xat
        0xbt
        -0x2t
        0x1ct
        -0x9t
        0xet
        0x3t
        -0x16t
        0x11t
        -0xdt
    .end array-data
.end method

.method public constructor <init>()V
    .registers 5

    .line 153
    sget-object v0, Lcc/a;->a:[B

    const/16 v1, 0x3c

    aget-byte v0, v0, v1

    int-to-byte v0, v0

    sget-object v1, Lcc/a;->a:[B

    const/16 v2, 0x49

    aget-byte v1, v1, v2

    add-int/lit8 v1, v1, -0x1

    int-to-byte v1, v1

    sget-object v2, Lcc/a;->a:[B

    const/16 v3, 0x66

    aget-byte v2, v2, v3

    int-to-byte v2, v2

    invoke-static {v0, v1, v2}, Lcc/a;->a(BSB)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcc/a;->a:[B

    const/4 v2, 0x4

    aget-byte v1, v1, v2

    int-to-byte v1, v1

    sget-object v2, Lcc/a;->a:[B

    const/16 v3, 0x74

    aget-byte v2, v2, v3

    add-int/lit8 v2, v2, 0x1

    int-to-byte v2, v2

    const/16 v3, 0x3d

    invoke-static {v1, v3, v2}, Lcc/a;->a(BSB)Ljava/lang/String;

    move-result-object v1

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    invoke-direct {p0, v0, v2, v3, v1}, Ljava/security/Provider;-><init>(Ljava/lang/String;DLjava/lang/String;)V

    .line 161
    sget-object v0, Lcc/a;->a:[B

    const/16 v1, 0x66

    aget-byte v0, v0, v1

    add-int/lit8 v0, v0, -0x1

    int-to-byte v0, v0

    or-int/lit8 v1, v0, 0x7b

    int-to-byte v1, v1

    sget-object v2, Lcc/a;->a:[B

    const/16 v3, 0xb

    aget-byte v2, v2, v3

    int-to-byte v2, v2

    invoke-static {v0, v1, v2}, Lcc/a;->a(BSB)Ljava/lang/String;

    move-result-object v0

    const-class v1, Lbr/com/itau/security/commons/PRNGFixes$LinuxPRNGSecureRandom;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcc/a;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 162
    sget-object v0, Lcc/a;->a:[B

    const/16 v1, 0x66

    aget-byte v0, v0, v1

    add-int/lit8 v0, v0, -0x1

    int-to-byte v0, v0

    or-int/lit8 v1, v0, 0x67

    int-to-byte v1, v1

    sget-object v2, Lcc/a;->a:[B

    const/16 v3, 0x38

    aget-byte v2, v2, v3

    neg-int v2, v2

    int-to-byte v2, v2

    invoke-static {v0, v1, v2}, Lcc/a;->a(BSB)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcc/a;->a:[B

    const/16 v2, 0x66

    aget-byte v1, v1, v2

    add-int/lit8 v1, v1, -0x1

    int-to-byte v1, v1

    int-to-byte v2, v1

    int-to-byte v3, v2

    invoke-static {v1, v2, v3}, Lcc/a;->a(BSB)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcc/a;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 163
    return-void
.end method

.method private static a(BSB)Ljava/lang/String;
    .registers 9

    new-instance v0, Ljava/lang/String;

    rsub-int/lit8 p1, p1, 0x7e

    add-int/lit8 p2, p2, 0x8

    rsub-int/lit8 p0, p0, 0x53

    const/4 v5, 0x0

    sget-object v4, Lcc/a;->a:[B

    new-array v1, p2, [B

    if-nez v4, :cond_13

    move v2, p1

    move v3, p0

    :goto_11
    add-int p0, v2, v3

    :cond_13
    int-to-byte v2, p0

    aput-byte v2, v1, v5

    add-int/lit8 v5, v5, 0x1

    if-ne v5, p2, :cond_23

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Ljava/lang/String;-><init>([BI)V

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_23
    move v2, p0

    add-int/lit8 p1, p1, 0x1

    aget-byte v3, v4, p1

    goto :goto_11
.end method

.class public final Lcom/jakewharton/threetenabp/AndroidThreeTen;
.super Ljava/lang/Object;
.source "AndroidThreeTen.java"


# static fields
.field private static final initialized:Ljava/util/concurrent/atomic/AtomicBoolean;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .line 13
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    sput-object v0, Lcom/jakewharton/threetenabp/AndroidThreeTen;->initialized:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-void
.end method

.method public static init(Landroid/app/Application;)V
    .registers 1
    .param p0, "application"    # Landroid/app/Application;

    .line 16
    invoke-static {p0}, Lcom/jakewharton/threetenabp/AndroidThreeTen;->init(Landroid/content/Context;)V

    .line 17
    return-void
.end method

.method public static init(Landroid/content/Context;)V
    .registers 8
    .param p0, "context"    # Landroid/content/Context;

    .line 20
    sget-object v0, Lcom/jakewharton/threetenabp/AndroidThreeTen;->initialized:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 21
    return-void

    .line 25
    :cond_a
    const/4 v3, 0x0

    .line 27
    .local v3, "is":Ljava/io/InputStream;
    :try_start_b
    invoke-virtual {p0}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    const-string v1, "org/threeten/bp/TZDB.dat"

    invoke-virtual {v0, v1}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v0

    move-object v3, v0

    .line 28
    new-instance v2, Lorg/threeten/bp/zone/TzdbZoneRulesProvider;

    invoke-direct {v2, v3}, Lorg/threeten/bp/zone/TzdbZoneRulesProvider;-><init>(Ljava/io/InputStream;)V
    :try_end_1b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_1b} :catch_23
    .catchall {:try_start_b .. :try_end_1b} :catchall_2c

    .line 32
    .local v2, "provider":Lorg/threeten/bp/zone/TzdbZoneRulesProvider;
    if-eqz v3, :cond_35

    .line 34
    :try_start_1d
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_20
    .catch Ljava/io/IOException; {:try_start_1d .. :try_end_20} :catch_21

    .line 36
    goto :goto_35

    .line 35
    :catch_21
    move-exception v4

    .line 36
    goto :goto_35

    .line 29
    .end local v2    # "provider":Lorg/threeten/bp/zone/TzdbZoneRulesProvider;
    :catch_23
    move-exception v4

    .line 30
    .local v4, "e":Ljava/io/IOException;
    :try_start_24
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "TZDB.dat missing from assets."

    invoke-direct {v0, v1, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0
    :try_end_2c
    .catchall {:try_start_24 .. :try_end_2c} :catchall_2c

    .line 32
    .end local v4    # "e":Ljava/io/IOException;
    :catchall_2c
    move-exception v5

    if-eqz v3, :cond_34

    .line 34
    :try_start_2f
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_32
    .catch Ljava/io/IOException; {:try_start_2f .. :try_end_32} :catch_33

    .line 36
    goto :goto_34

    .line 35
    :catch_33
    move-exception v6

    .line 36
    :cond_34
    :goto_34
    throw v5

    .line 40
    .local v2, "provider":Lorg/threeten/bp/zone/TzdbZoneRulesProvider;
    :cond_35
    :goto_35
    invoke-static {v2}, Lorg/threeten/bp/zone/ZoneRulesProvider;->registerProvider(Lorg/threeten/bp/zone/ZoneRulesProvider;)V

    .line 41
    return-void
.end method

.class public Lcom/itau/empresas/ui/dialog/ConfirmarSairDialog;
.super Lcom/itau/empresas/ui/dialog/BaseDialog;
.source "ConfirmarSairDialog.java"


# instance fields
.field private isPoynt:Z

.field llViewConteudo:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 25
    invoke-direct {p0}, Lcom/itau/empresas/ui/dialog/BaseDialog;-><init>()V

    return-void
.end method


# virtual methods
.method public afterViews()V
    .registers 6

    .line 34
    invoke-virtual {p0}, Lcom/itau/empresas/ui/dialog/ConfirmarSairDialog;->getDialog()Landroid/app/Dialog;

    move-result-object v2

    .line 36
    .local v2, "dlg":Landroid/app/Dialog;
    invoke-virtual {v2}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v3

    .line 37
    .local v3, "window":Landroid/view/Window;
    invoke-virtual {v3}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v4

    .line 39
    .local v4, "wlp":Landroid/view/WindowManager$LayoutParams;
    const/16 v0, 0x50

    iput v0, v4, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 41
    invoke-virtual {p0}, Lcom/itau/empresas/ui/dialog/ConfirmarSairDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "isPoynt"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/itau/empresas/ui/dialog/ConfirmarSairDialog;->isPoynt:Z

    .line 42
    return-void
.end method

.method public clicouNao()V
    .registers 1

    .line 80
    invoke-virtual {p0}, Lcom/itau/empresas/ui/dialog/ConfirmarSairDialog;->dismiss()V

    .line 81
    return-void
.end method

.method public clicouSair()V
    .registers 3

    .line 69
    invoke-virtual {p0}, Lcom/itau/empresas/ui/dialog/ConfirmarSairDialog;->dismiss()V

    .line 70
    iget-boolean v0, p0, Lcom/itau/empresas/ui/dialog/ConfirmarSairDialog;->isPoynt:Z

    if-eqz v0, :cond_14

    .line 71
    invoke-static {}, Lde/greenrobot/event/EventBus;->getDefault()Lde/greenrobot/event/EventBus;

    move-result-object v0

    new-instance v1, Lcom/itau/empresas/Evento$EventoSairPoynt;

    invoke-direct {v1}, Lcom/itau/empresas/Evento$EventoSairPoynt;-><init>()V

    invoke-virtual {v0, v1}, Lde/greenrobot/event/EventBus;->post(Ljava/lang/Object;)V

    goto :goto_20

    .line 73
    :cond_14
    invoke-static {}, Lde/greenrobot/event/EventBus;->getDefault()Lde/greenrobot/event/EventBus;

    move-result-object v0

    new-instance v1, Lcom/itau/empresas/Evento$EventoSair;

    invoke-direct {v1}, Lcom/itau/empresas/Evento$EventoSair;-><init>()V

    invoke-virtual {v0, v1}, Lde/greenrobot/event/EventBus;->post(Ljava/lang/Object;)V

    .line 76
    :goto_20
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .registers 8
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 47
    new-instance v4, Landroid/widget/RelativeLayout;

    invoke-virtual {p0}, Lcom/itau/empresas/ui/dialog/ConfirmarSairDialog;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-direct {v4, v0}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 48
    .local v4, "root":Landroid/widget/RelativeLayout;
    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    const/4 v1, -0x1

    const/4 v2, -0x1

    invoke-direct {v0, v1, v2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v4, v0}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 51
    invoke-super {p0, p1}, Lcom/itau/empresas/ui/dialog/BaseDialog;->onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;

    move-result-object v5

    .line 52
    .local v5, "dialog":Landroid/app/Dialog;
    if-eqz v5, :cond_40

    .line 53
    const/4 v0, 0x1

    invoke-virtual {v5, v0}, Landroid/app/Dialog;->requestWindowFeature(I)Z

    .line 54
    invoke-virtual {v5, v4}, Landroid/app/Dialog;->setContentView(Landroid/view/View;)V

    .line 55
    invoke-virtual {v5}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    .line 57
    invoke-virtual {p0}, Lcom/itau/empresas/ui/dialog/ConfirmarSairDialog;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const v3, 0x106000d

    invoke-static {v2, v3}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v2

    invoke-direct {v1, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    .line 55
    invoke-virtual {v0, v1}, Landroid/view/Window;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 58
    invoke-virtual {v5}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    .line 59
    const/4 v1, -0x1

    const/4 v2, -0x2

    invoke-virtual {v0, v1, v2}, Landroid/view/Window;->setLayout(II)V

    .line 62
    :cond_40
    return-object v5
.end method

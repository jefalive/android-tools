.class public Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/ChartAnimator;
.super Ljava/lang/Object;
.source "ChartAnimator.java"


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "NewApi"
    }
.end annotation


# instance fields
.field private mListener:Landroid/animation/ValueAnimator$AnimatorUpdateListener;

.field protected mPhaseX:F

.field protected mPhaseY:F


# direct methods
.method public constructor <init>()V
    .registers 2

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/ChartAnimator;->mPhaseY:F

    .line 44
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/ChartAnimator;->mPhaseX:F

    .line 25
    return-void
.end method

.method public constructor <init>(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V
    .registers 3
    .param p1, "listener"    # Landroid/animation/ValueAnimator$AnimatorUpdateListener;

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/ChartAnimator;->mPhaseY:F

    .line 44
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/ChartAnimator;->mPhaseX:F

    .line 29
    iput-object p1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/ChartAnimator;->mListener:Landroid/animation/ValueAnimator$AnimatorUpdateListener;

    .line 30
    return-void
.end method


# virtual methods
.method public animateX(I)V
    .registers 7
    .param p1, "durationMillis"    # I

    .line 288
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-ge v0, v1, :cond_7

    .line 289
    return-void

    .line 292
    :cond_7
    const-string v0, "phaseX"

    const/4 v1, 0x2

    new-array v1, v1, [F

    fill-array-data v1, :array_20

    invoke-static {p0, v0, v1}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v4

    .line 293
    .local v4, "animatorX":Landroid/animation/ObjectAnimator;
    int-to-long v0, p1

    invoke-virtual {v4, v0, v1}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 294
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/ChartAnimator;->mListener:Landroid/animation/ValueAnimator$AnimatorUpdateListener;

    invoke-virtual {v4, v0}, Landroid/animation/ObjectAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 295
    invoke-virtual {v4}, Landroid/animation/ObjectAnimator;->start()V

    .line 296
    return-void

    :array_20
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method public animateX(ILbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;)V
    .registers 8
    .param p1, "durationMillis"    # I
    .param p2, "easing"    # Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;

    .line 200
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-ge v0, v1, :cond_7

    .line 201
    return-void

    .line 204
    :cond_7
    const-string v0, "phaseX"

    const/4 v1, 0x2

    new-array v1, v1, [F

    fill-array-data v1, :array_28

    invoke-static {p0, v0, v1}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v4

    .line 205
    .local v4, "animatorX":Landroid/animation/ObjectAnimator;
    invoke-static {p2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing;->getEasingFunctionFromOption(Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;)Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/EasingFunction;

    move-result-object v0

    invoke-virtual {v4, v0}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 206
    int-to-long v0, p1

    invoke-virtual {v4, v0, v1}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 207
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/ChartAnimator;->mListener:Landroid/animation/ValueAnimator$AnimatorUpdateListener;

    invoke-virtual {v4, v0}, Landroid/animation/ObjectAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 208
    invoke-virtual {v4}, Landroid/animation/ObjectAnimator;->start()V

    .line 209
    return-void

    nop

    :array_28
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method public animateX(ILbr/com/itau/widgets/graphicalfriedcake/charts/animation/EasingFunction;)V
    .registers 8
    .param p1, "durationMillis"    # I
    .param p2, "easing"    # Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/EasingFunction;

    .line 106
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-ge v0, v1, :cond_7

    .line 107
    return-void

    .line 110
    :cond_7
    const-string v0, "phaseX"

    const/4 v1, 0x2

    new-array v1, v1, [F

    fill-array-data v1, :array_24

    invoke-static {p0, v0, v1}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v4

    .line 111
    .local v4, "animatorX":Landroid/animation/ObjectAnimator;
    invoke-virtual {v4, p2}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 112
    int-to-long v0, p1

    invoke-virtual {v4, v0, v1}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 113
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/ChartAnimator;->mListener:Landroid/animation/ValueAnimator$AnimatorUpdateListener;

    invoke-virtual {v4, v0}, Landroid/animation/ObjectAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 114
    invoke-virtual {v4}, Landroid/animation/ObjectAnimator;->start()V

    .line 115
    return-void

    nop

    :array_24
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method public animateXY(II)V
    .registers 9
    .param p1, "durationMillisX"    # I
    .param p2, "durationMillisY"    # I

    .line 252
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-ge v0, v1, :cond_7

    .line 253
    return-void

    .line 256
    :cond_7
    const-string v0, "phaseY"

    const/4 v1, 0x2

    new-array v1, v1, [F

    fill-array-data v1, :array_3c

    invoke-static {p0, v0, v1}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v4

    .line 257
    .local v4, "animatorY":Landroid/animation/ObjectAnimator;
    int-to-long v0, p2

    invoke-virtual {v4, v0, v1}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 259
    const-string v0, "phaseX"

    const/4 v1, 0x2

    new-array v1, v1, [F

    fill-array-data v1, :array_44

    invoke-static {p0, v0, v1}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v5

    .line 260
    .local v5, "animatorX":Landroid/animation/ObjectAnimator;
    int-to-long v0, p1

    invoke-virtual {v5, v0, v1}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 266
    if-le p1, p2, :cond_2f

    .line 267
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/ChartAnimator;->mListener:Landroid/animation/ValueAnimator$AnimatorUpdateListener;

    invoke-virtual {v5, v0}, Landroid/animation/ObjectAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    goto :goto_34

    .line 269
    :cond_2f
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/ChartAnimator;->mListener:Landroid/animation/ValueAnimator$AnimatorUpdateListener;

    invoke-virtual {v4, v0}, Landroid/animation/ObjectAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 273
    :goto_34
    invoke-virtual {v5}, Landroid/animation/ObjectAnimator;->start()V

    .line 274
    invoke-virtual {v4}, Landroid/animation/ObjectAnimator;->start()V

    .line 275
    return-void

    nop

    :array_3c
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data

    :array_44
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method public animateXY(IILbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;)V
    .registers 11
    .param p1, "durationMillisX"    # I
    .param p2, "durationMillisY"    # I
    .param p3, "easingX"    # Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;
    .param p4, "easingY"    # Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;

    .line 161
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-ge v0, v1, :cond_7

    .line 162
    return-void

    .line 165
    :cond_7
    const-string v0, "phaseY"

    const/4 v1, 0x2

    new-array v1, v1, [F

    fill-array-data v1, :array_4a

    invoke-static {p0, v0, v1}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v4

    .line 166
    .local v4, "animatorY":Landroid/animation/ObjectAnimator;
    invoke-static {p4}, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing;->getEasingFunctionFromOption(Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;)Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/EasingFunction;

    move-result-object v0

    invoke-virtual {v4, v0}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 167
    int-to-long v0, p2

    invoke-virtual {v4, v0, v1}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 169
    const-string v0, "phaseX"

    const/4 v1, 0x2

    new-array v1, v1, [F

    fill-array-data v1, :array_52

    invoke-static {p0, v0, v1}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v5

    .line 170
    .local v5, "animatorX":Landroid/animation/ObjectAnimator;
    invoke-static {p3}, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing;->getEasingFunctionFromOption(Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;)Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/EasingFunction;

    move-result-object v0

    invoke-virtual {v5, v0}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 171
    int-to-long v0, p1

    invoke-virtual {v5, v0, v1}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 177
    if-le p1, p2, :cond_3d

    .line 178
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/ChartAnimator;->mListener:Landroid/animation/ValueAnimator$AnimatorUpdateListener;

    invoke-virtual {v5, v0}, Landroid/animation/ObjectAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    goto :goto_42

    .line 180
    :cond_3d
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/ChartAnimator;->mListener:Landroid/animation/ValueAnimator$AnimatorUpdateListener;

    invoke-virtual {v4, v0}, Landroid/animation/ObjectAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 184
    :goto_42
    invoke-virtual {v5}, Landroid/animation/ObjectAnimator;->start()V

    .line 185
    invoke-virtual {v4}, Landroid/animation/ObjectAnimator;->start()V

    .line 186
    return-void

    nop

    :array_4a
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data

    :array_52
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method public animateXY(IILbr/com/itau/widgets/graphicalfriedcake/charts/animation/EasingFunction;Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/EasingFunction;)V
    .registers 11
    .param p1, "durationMillisX"    # I
    .param p2, "durationMillisY"    # I
    .param p3, "easingX"    # Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/EasingFunction;
    .param p4, "easingY"    # Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/EasingFunction;

    .line 67
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-ge v0, v1, :cond_7

    .line 68
    return-void

    .line 71
    :cond_7
    const-string v0, "phaseY"

    const/4 v1, 0x2

    new-array v1, v1, [F

    fill-array-data v1, :array_42

    invoke-static {p0, v0, v1}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v4

    .line 72
    .local v4, "animatorY":Landroid/animation/ObjectAnimator;
    invoke-virtual {v4, p4}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 73
    int-to-long v0, p2

    invoke-virtual {v4, v0, v1}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 75
    const-string v0, "phaseX"

    const/4 v1, 0x2

    new-array v1, v1, [F

    fill-array-data v1, :array_4a

    invoke-static {p0, v0, v1}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v5

    .line 76
    .local v5, "animatorX":Landroid/animation/ObjectAnimator;
    invoke-virtual {v5, p3}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 77
    int-to-long v0, p1

    invoke-virtual {v5, v0, v1}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 83
    if-le p1, p2, :cond_35

    .line 84
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/ChartAnimator;->mListener:Landroid/animation/ValueAnimator$AnimatorUpdateListener;

    invoke-virtual {v5, v0}, Landroid/animation/ObjectAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    goto :goto_3a

    .line 86
    :cond_35
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/ChartAnimator;->mListener:Landroid/animation/ValueAnimator$AnimatorUpdateListener;

    invoke-virtual {v4, v0}, Landroid/animation/ObjectAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 90
    :goto_3a
    invoke-virtual {v5}, Landroid/animation/ObjectAnimator;->start()V

    .line 91
    invoke-virtual {v4}, Landroid/animation/ObjectAnimator;->start()V

    .line 92
    return-void

    nop

    :array_42
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data

    :array_4a
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method public animateY(I)V
    .registers 7
    .param p1, "durationMillis"    # I

    .line 309
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-ge v0, v1, :cond_7

    .line 310
    return-void

    .line 313
    :cond_7
    const-string v0, "phaseY"

    const/4 v1, 0x2

    new-array v1, v1, [F

    fill-array-data v1, :array_20

    invoke-static {p0, v0, v1}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v4

    .line 314
    .local v4, "animatorY":Landroid/animation/ObjectAnimator;
    int-to-long v0, p1

    invoke-virtual {v4, v0, v1}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 315
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/ChartAnimator;->mListener:Landroid/animation/ValueAnimator$AnimatorUpdateListener;

    invoke-virtual {v4, v0}, Landroid/animation/ObjectAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 316
    invoke-virtual {v4}, Landroid/animation/ObjectAnimator;->start()V

    .line 317
    return-void

    :array_20
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method public animateY(ILbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;)V
    .registers 8
    .param p1, "durationMillis"    # I
    .param p2, "easing"    # Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;

    .line 223
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-ge v0, v1, :cond_7

    .line 224
    return-void

    .line 227
    :cond_7
    const-string v0, "phaseY"

    const/4 v1, 0x2

    new-array v1, v1, [F

    fill-array-data v1, :array_28

    invoke-static {p0, v0, v1}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v4

    .line 228
    .local v4, "animatorY":Landroid/animation/ObjectAnimator;
    invoke-static {p2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing;->getEasingFunctionFromOption(Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;)Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/EasingFunction;

    move-result-object v0

    invoke-virtual {v4, v0}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 229
    int-to-long v0, p1

    invoke-virtual {v4, v0, v1}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 230
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/ChartAnimator;->mListener:Landroid/animation/ValueAnimator$AnimatorUpdateListener;

    invoke-virtual {v4, v0}, Landroid/animation/ObjectAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 231
    invoke-virtual {v4}, Landroid/animation/ObjectAnimator;->start()V

    .line 232
    return-void

    nop

    :array_28
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method public animateY(ILbr/com/itau/widgets/graphicalfriedcake/charts/animation/EasingFunction;)V
    .registers 8
    .param p1, "durationMillis"    # I
    .param p2, "easing"    # Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/EasingFunction;

    .line 129
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-ge v0, v1, :cond_7

    .line 130
    return-void

    .line 133
    :cond_7
    const-string v0, "phaseY"

    const/4 v1, 0x2

    new-array v1, v1, [F

    fill-array-data v1, :array_24

    invoke-static {p0, v0, v1}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v4

    .line 134
    .local v4, "animatorY":Landroid/animation/ObjectAnimator;
    invoke-virtual {v4, p2}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 135
    int-to-long v0, p1

    invoke-virtual {v4, v0, v1}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 136
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/ChartAnimator;->mListener:Landroid/animation/ValueAnimator$AnimatorUpdateListener;

    invoke-virtual {v4, v0}, Landroid/animation/ObjectAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 137
    invoke-virtual {v4}, Landroid/animation/ObjectAnimator;->start()V

    .line 138
    return-void

    nop

    :array_24
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method public getPhaseX()F
    .registers 2

    .line 346
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/ChartAnimator;->mPhaseX:F

    return v0
.end method

.method public getPhaseY()F
    .registers 2

    .line 326
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/ChartAnimator;->mPhaseY:F

    return v0
.end method

.method public setPhaseX(F)V
    .registers 2
    .param p1, "phase"    # F

    .line 356
    iput p1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/ChartAnimator;->mPhaseX:F

    .line 357
    return-void
.end method

.method public setPhaseY(F)V
    .registers 2
    .param p1, "phase"    # F

    .line 336
    iput p1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/ChartAnimator;->mPhaseY:F

    .line 337
    return-void
.end method

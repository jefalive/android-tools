.class public final Lcom/itau/empresas/BoletoUtils;
.super Ljava/lang/Object;
.source "BoletoUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/itau/empresas/BoletoUtils$Boleto;,
        Lcom/itau/empresas/BoletoUtils$TipoBoleto;
    }
.end annotation


# static fields
.field private static final patternsBoleto:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Ljava/util/regex/Pattern;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .line 57
    const-string v0, "pdf2text"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 63
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/util/regex/Pattern;

    sget-object v1, Lcom/itau/empresas/BoletoUtils$TipoBoleto;->NORMAL:Lcom/itau/empresas/BoletoUtils$TipoBoleto;

    .line 64
    invoke-virtual {v1}, Lcom/itau/empresas/BoletoUtils$TipoBoleto;->getPattern()Ljava/util/regex/Pattern;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sget-object v1, Lcom/itau/empresas/BoletoUtils$TipoBoleto;->CONCESSIONARIA:Lcom/itau/empresas/BoletoUtils$TipoBoleto;

    .line 65
    invoke-virtual {v1}, Lcom/itau/empresas/BoletoUtils$TipoBoleto;->getPattern()Ljava/util/regex/Pattern;

    move-result-object v1

    const/4 v2, 0x1

    aput-object v1, v0, v2

    const-string v1, "(\\d{11}\\-\\d{1})\\s(\\d{11}\\-\\d{1})\\s(\\d{11}\\-\\d{1})\\s(\\d{11}\\-\\d{1})"

    .line 68
    invoke-static {v1}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v1

    const/4 v2, 0x2

    aput-object v1, v0, v2

    .line 63
    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/itau/empresas/BoletoUtils;->patternsBoleto:Ljava/util/List;

    return-void
.end method

.method public static native converterPdfParaTxt(Ljava/lang/String;)V
.end method

.method private static copy(Ljava/io/InputStream;Ljava/io/OutputStream;)V
    .registers 5
    .param p0, "in"    # Ljava/io/InputStream;
    .param p1, "out"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 139
    const/16 v0, 0x400

    new-array v1, v0, [B

    .line 141
    .local v1, "buf":[B
    :goto_4
    invoke-virtual {p0, v1}, Ljava/io/InputStream;->read([B)I

    move-result v0

    move v2, v0

    .local v2, "len":I
    if-lez v0, :cond_10

    .line 142
    const/4 v0, 0x0

    invoke-virtual {p1, v1, v0, v2}, Ljava/io/OutputStream;->write([BII)V

    goto :goto_4

    .line 144
    :cond_10
    return-void
.end method

.method public static lerBoletoPdf(Landroid/content/Context;Landroid/net/Uri;)Lcom/itau/empresas/BoletoUtils$Boleto;
    .registers 19
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "uri"    # Landroid/net/Uri;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 87
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    .line 88
    .local v4, "resolver":Landroid/content/ContentResolver;
    move-object/from16 v0, p1

    invoke-virtual {v4, v0}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v5

    .line 90
    .local v5, "in":Ljava/io/InputStream;
    const-string v0, "boleto-tmp"

    const-string v1, ".pdf"

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getExternalCacheDir()Ljava/io/File;

    move-result-object v2

    invoke-static {v0, v1, v2}, Ljava/io/File;->createTempFile(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)Ljava/io/File;

    move-result-object v6

    .line 91
    .local v6, "file":Ljava/io/File;
    invoke-virtual {v6}, Ljava/io/File;->deleteOnExit()V

    .line 92
    new-instance v7, Ljava/io/FileOutputStream;

    invoke-direct {v7, v6}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 94
    .local v7, "out":Ljava/io/OutputStream;
    new-instance v8, Lcom/itau/empresas/BoletoUtils$Boleto;

    invoke-direct {v8}, Lcom/itau/empresas/BoletoUtils$Boleto;-><init>()V

    .line 95
    .local v8, "result":Lcom/itau/empresas/BoletoUtils$Boleto;
    sget-object v0, Lcom/itau/empresas/BoletoUtils$Boleto$Estado;->NAO_ENCONTRADO:Lcom/itau/empresas/BoletoUtils$Boleto$Estado;

    invoke-virtual {v8, v0}, Lcom/itau/empresas/BoletoUtils$Boleto;->setEstado(Lcom/itau/empresas/BoletoUtils$Boleto$Estado;)V

    .line 98
    :try_start_28
    invoke-static {v5, v7}, Lcom/itau/empresas/BoletoUtils;->copy(Ljava/io/InputStream;Ljava/io/OutputStream;)V
    :try_end_2b
    .catchall {:try_start_28 .. :try_end_2b} :catchall_32

    .line 100
    invoke-virtual {v5}, Ljava/io/InputStream;->close()V

    .line 101
    invoke-virtual {v7}, Ljava/io/OutputStream;->close()V

    .line 102
    goto :goto_3a

    .line 100
    :catchall_32
    move-exception v9

    invoke-virtual {v5}, Ljava/io/InputStream;->close()V

    .line 101
    invoke-virtual {v7}, Ljava/io/OutputStream;->close()V

    throw v9

    .line 103
    :goto_3a
    const/4 v9, 0x0

    .line 104
    .local v9, "inputStream":Ljava/io/InputStream;
    const/4 v10, 0x0

    .line 107
    .local v10, "reader":Ljava/io/BufferedReader;
    :try_start_3c
    invoke-virtual {v6}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/itau/empresas/BoletoUtils;->converterPdfParaTxt(Ljava/lang/String;)V

    .line 108
    new-instance v0, Ljava/io/FileInputStream;

    .line 109
    invoke-virtual {v6}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    const-string v2, "(pdf|PDF)$"

    const-string v3, "txt"

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    move-object v9, v0

    .line 110
    new-instance v0, Ljava/io/BufferedReader;

    new-instance v1, Ljava/io/InputStreamReader;

    invoke-direct {v1, v9}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v0, v1}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    move-object v10, v0

    .line 113
    :goto_60
    invoke-virtual {v10}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v11

    .local v11, "line":Ljava/lang/String;
    if-eqz v11, :cond_9d

    .line 114
    sget-object v0, Lcom/itau/empresas/BoletoUtils;->patternsBoleto:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :goto_6c
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9c

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v13, v0

    check-cast v13, Ljava/util/regex/Pattern;

    .line 115
    .local v13, "p":Ljava/util/regex/Pattern;
    invoke-virtual {v13, v11}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v14

    .line 117
    .local v14, "matcher":Ljava/util/regex/Matcher;
    invoke-virtual {v14}, Ljava/util/regex/Matcher;->find()Z

    move-result v0

    if-eqz v0, :cond_9b

    .line 118
    sget-object v0, Lcom/itau/empresas/BoletoUtils$Boleto$Estado;->SUCESSO:Lcom/itau/empresas/BoletoUtils$Boleto$Estado;

    invoke-virtual {v8, v0}, Lcom/itau/empresas/BoletoUtils$Boleto;->setEstado(Lcom/itau/empresas/BoletoUtils$Boleto$Estado;)V

    .line 119
    invoke-virtual {v14}, Ljava/util/regex/Matcher;->group()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v8, v0}, Lcom/itau/empresas/BoletoUtils$Boleto;->setCodigo(Ljava/lang/String;)V
    :try_end_8f
    .catch Ljava/lang/Exception; {:try_start_3c .. :try_end_8f} :catch_a8
    .catchall {:try_start_3c .. :try_end_8f} :catchall_bd

    .line 120
    move-object v15, v8

    .line 127
    if-eqz v10, :cond_95

    .line 128
    invoke-virtual {v10}, Ljava/io/BufferedReader;->close()V

    .line 130
    :cond_95
    if-eqz v9, :cond_9a

    .line 131
    invoke-virtual {v9}, Ljava/io/InputStream;->close()V

    .line 120
    :cond_9a
    return-object v15

    .line 122
    .end local v13    # "p":Ljava/util/regex/Pattern;
    .end local v14    # "matcher":Ljava/util/regex/Matcher;
    :cond_9b
    goto :goto_6c

    :cond_9c
    goto :goto_60

    .line 127
    .end local v11    # "line":Ljava/lang/String;
    :cond_9d
    if-eqz v10, :cond_a2

    .line 128
    invoke-virtual {v10}, Ljava/io/BufferedReader;->close()V

    .line 130
    :cond_a2
    if-eqz v9, :cond_c9

    .line 131
    invoke-virtual {v9}, Ljava/io/InputStream;->close()V

    goto :goto_c9

    .line 124
    :catch_a8
    move-exception v11

    .line 125
    .local v11, "e":Ljava/lang/Exception;
    const-string v0, "erro boleto pdf"

    :try_start_ab
    invoke-virtual {v11}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_b2
    .catchall {:try_start_ab .. :try_end_b2} :catchall_bd

    .line 127
    .end local v11    # "e":Ljava/lang/Exception;
    if-eqz v10, :cond_b7

    .line 128
    invoke-virtual {v10}, Ljava/io/BufferedReader;->close()V

    .line 130
    :cond_b7
    if-eqz v9, :cond_c9

    .line 131
    invoke-virtual {v9}, Ljava/io/InputStream;->close()V

    goto :goto_c9

    .line 127
    :catchall_bd
    move-exception v16

    if-eqz v10, :cond_c3

    .line 128
    invoke-virtual {v10}, Ljava/io/BufferedReader;->close()V

    .line 130
    :cond_c3
    if-eqz v9, :cond_c8

    .line 131
    invoke-virtual {v9}, Ljava/io/InputStream;->close()V

    :cond_c8
    throw v16

    .line 135
    :cond_c9
    :goto_c9
    return-object v8
.end method

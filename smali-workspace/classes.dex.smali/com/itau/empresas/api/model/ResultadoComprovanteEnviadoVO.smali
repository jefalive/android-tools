.class public Lcom/itau/empresas/api/model/ResultadoComprovanteEnviadoVO;
.super Ljava/lang/Object;
.source "ResultadoComprovanteEnviadoVO.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private agenciaContaDebito:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "agencia_conta_debito"
    .end annotation
.end field

.field private controleEmail:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "controle_email"
    .end annotation
.end field

.field private dataSolicitacao:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "data_solicitacao"
    .end annotation
.end field

.field private emailDestinatario:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "email_destinatario"
    .end annotation
.end field

.field private horaSolicitacao:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "hora_solicitacao"
    .end annotation
.end field

.field private valorTotalOperacao:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "valor_total_operacao"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

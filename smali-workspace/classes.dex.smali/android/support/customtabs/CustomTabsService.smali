.class public abstract Landroid/support/customtabs/CustomTabsService;
.super Landroid/app/Service;
.source "CustomTabsService.java"


# instance fields
.field private mBinder:Landroid/support/customtabs/ICustomTabsService$Stub;

.field private final mDeathRecipientMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<Landroid/os/IBinder;Landroid/os/IBinder$DeathRecipient;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 2

    .line 38
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 53
    new-instance v0, Landroid/support/v4/util/ArrayMap;

    invoke-direct {v0}, Landroid/support/v4/util/ArrayMap;-><init>()V

    iput-object v0, p0, Landroid/support/customtabs/CustomTabsService;->mDeathRecipientMap:Ljava/util/Map;

    .line 55
    new-instance v0, Landroid/support/customtabs/CustomTabsService$1;

    invoke-direct {v0, p0}, Landroid/support/customtabs/CustomTabsService$1;-><init>(Landroid/support/customtabs/CustomTabsService;)V

    iput-object v0, p0, Landroid/support/customtabs/CustomTabsService;->mBinder:Landroid/support/customtabs/ICustomTabsService$Stub;

    return-void
.end method

.method static synthetic access$000(Landroid/support/customtabs/CustomTabsService;)Ljava/util/Map;
    .registers 2
    .param p0, "x0"    # Landroid/support/customtabs/CustomTabsService;

    .line 38
    iget-object v0, p0, Landroid/support/customtabs/CustomTabsService;->mDeathRecipientMap:Ljava/util/Map;

    return-object v0
.end method


# virtual methods
.method protected cleanUpSession(Landroid/support/customtabs/CustomTabsSessionToken;)Z
    .registers 7
    .param p1, "sessionToken"    # Landroid/support/customtabs/CustomTabsSessionToken;

    .line 116
    :try_start_0
    iget-object v1, p0, Landroid/support/customtabs/CustomTabsService;->mDeathRecipientMap:Ljava/util/Map;

    monitor-enter v1
    :try_end_3
    .catch Ljava/util/NoSuchElementException; {:try_start_0 .. :try_end_3} :catch_1f

    .line 117
    :try_start_3
    invoke-virtual {p1}, Landroid/support/customtabs/CustomTabsSessionToken;->getCallbackBinder()Landroid/os/IBinder;

    move-result-object v2

    .line 118
    .local v2, "binder":Landroid/os/IBinder;
    iget-object v0, p0, Landroid/support/customtabs/CustomTabsService;->mDeathRecipientMap:Ljava/util/Map;

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Landroid/os/IBinder$DeathRecipient;

    .line 120
    .local v3, "deathRecipient":Landroid/os/IBinder$DeathRecipient;
    const/4 v0, 0x0

    invoke-interface {v2, v3, v0}, Landroid/os/IBinder;->unlinkToDeath(Landroid/os/IBinder$DeathRecipient;I)Z

    .line 121
    iget-object v0, p0, Landroid/support/customtabs/CustomTabsService;->mDeathRecipientMap:Ljava/util/Map;

    invoke-interface {v0, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_19
    .catchall {:try_start_3 .. :try_end_19} :catchall_1b

    .line 122
    .end local v2    # "binder":Landroid/os/IBinder;
    .end local v3    # "deathRecipient":Landroid/os/IBinder$DeathRecipient;
    monitor-exit v1

    goto :goto_1e

    :catchall_1b
    move-exception v4

    monitor-exit v1

    :try_start_1d
    throw v4
    :try_end_1e
    .catch Ljava/util/NoSuchElementException; {:try_start_1d .. :try_end_1e} :catch_1f

    .line 125
    :goto_1e
    goto :goto_22

    .line 123
    :catch_1f
    move-exception v1

    .line 124
    .local v1, "e":Ljava/util/NoSuchElementException;
    const/4 v0, 0x0

    return v0

    .line 126
    .end local v1    # "e":Ljava/util/NoSuchElementException;
    :goto_22
    const/4 v0, 0x1

    return v0
.end method

.method protected abstract extraCommand(Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;
.end method

.method protected abstract mayLaunchUrl(Landroid/support/customtabs/CustomTabsSessionToken;Landroid/net/Uri;Landroid/os/Bundle;Ljava/util/List;)Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Landroid/support/customtabs/CustomTabsSessionToken;Landroid/net/Uri;Landroid/os/Bundle;Ljava/util/List<Landroid/os/Bundle;>;)Z"
        }
    .end annotation
.end method

.method protected abstract newSession(Landroid/support/customtabs/CustomTabsSessionToken;)Z
.end method

.method protected abstract updateVisuals(Landroid/support/customtabs/CustomTabsSessionToken;Landroid/os/Bundle;)Z
.end method

.method protected abstract warmup(J)Z
.end method

.class final Lcom/adobe/mobile/AnalyticsWorker;
.super Lcom/adobe/mobile/AbstractHitDatabase;
.source "AnalyticsWorker.java"


# static fields
.field private static _instance:Lcom/adobe/mobile/AnalyticsWorker;

.field private static final _instanceMutex:Ljava/lang/Object;

.field private static volatile analyticsGetBaseURL_pred:Z

.field private static baseURL:Ljava/lang/String;

.field private static final randomGen:Ljava/security/SecureRandom;


# instance fields
.field protected _preparedInsertStatement:Landroid/database/sqlite/SQLiteStatement;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .line 51
    new-instance v0, Ljava/security/SecureRandom;

    invoke-direct {v0}, Ljava/security/SecureRandom;-><init>()V

    sput-object v0, Lcom/adobe/mobile/AnalyticsWorker;->randomGen:Ljava/security/SecureRandom;

    .line 58
    const/4 v0, 0x0

    sput-object v0, Lcom/adobe/mobile/AnalyticsWorker;->_instance:Lcom/adobe/mobile/AnalyticsWorker;

    .line 59
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/adobe/mobile/AnalyticsWorker;->_instanceMutex:Ljava/lang/Object;

    .line 137
    const/4 v0, 0x1

    sput-boolean v0, Lcom/adobe/mobile/AnalyticsWorker;->analyticsGetBaseURL_pred:Z

    return-void
.end method

.method protected constructor <init>()V
    .registers 4

    .line 71
    invoke-direct {p0}, Lcom/adobe/mobile/AbstractHitDatabase;-><init>()V

    .line 53
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/adobe/mobile/AnalyticsWorker;->_preparedInsertStatement:Landroid/database/sqlite/SQLiteStatement;

    .line 72
    const-string v0, "ADBMobileDataCache.sqlite"

    iput-object v0, p0, Lcom/adobe/mobile/AnalyticsWorker;->fileName:Ljava/lang/String;

    .line 73
    const-string v0, "Analytics"

    iput-object v0, p0, Lcom/adobe/mobile/AnalyticsWorker;->logPrefix:Ljava/lang/String;

    .line 74
    const-string v0, "CREATE TABLE IF NOT EXISTS HITS (ID INTEGER PRIMARY KEY AUTOINCREMENT, URL TEXT, TIMESTAMP INTEGER)"

    iput-object v0, p0, Lcom/adobe/mobile/AnalyticsWorker;->dbCreateStatement:Ljava/lang/String;

    .line 75
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/adobe/mobile/AnalyticsWorker;->lastHitTimestamp:J

    .line 77
    new-instance v0, Ljava/io/File;

    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getCacheDirectory()Ljava/io/File;

    move-result-object v1

    iget-object v2, p0, Lcom/adobe/mobile/AnalyticsWorker;->fileName:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/adobe/mobile/AnalyticsWorker;->initDatabaseBacking(Ljava/io/File;)V

    .line 78
    invoke-virtual {p0}, Lcom/adobe/mobile/AnalyticsWorker;->getTrackingQueueSize()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/adobe/mobile/AnalyticsWorker;->numberOfUnsentHits:J

    .line 79
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .registers 1

    .line 38
    invoke-static {}, Lcom/adobe/mobile/AnalyticsWorker;->getBaseURL()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100()Ljava/security/SecureRandom;
    .registers 1

    .line 38
    sget-object v0, Lcom/adobe/mobile/AnalyticsWorker;->randomGen:Ljava/security/SecureRandom;

    return-object v0
.end method

.method private static getBaseURL()Ljava/lang/String;
    .registers 4

    .line 139
    sget-boolean v0, Lcom/adobe/mobile/AnalyticsWorker;->analyticsGetBaseURL_pred:Z

    if-eqz v0, :cond_78

    .line 140
    const/4 v0, 0x0

    sput-boolean v0, Lcom/adobe/mobile/AnalyticsWorker;->analyticsGetBaseURL_pred:Z

    .line 142
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/adobe/mobile/MobileConfig;->getInstance()Lcom/adobe/mobile/MobileConfig;

    move-result-object v1

    invoke-virtual {v1}, Lcom/adobe/mobile/MobileConfig;->getSSL()Z

    move-result v1

    if-eqz v1, :cond_19

    const-string v1, "https://"

    goto :goto_1b

    :cond_19
    const-string v1, "http://"

    :goto_1b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 143
    invoke-static {}, Lcom/adobe/mobile/MobileConfig;->getInstance()Lcom/adobe/mobile/MobileConfig;

    move-result-object v1

    invoke-virtual {v1}, Lcom/adobe/mobile/MobileConfig;->getTrackingServer()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/b/ss/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 144
    invoke-static {}, Lcom/adobe/mobile/MobileConfig;->getInstance()Lcom/adobe/mobile/MobileConfig;

    move-result-object v1

    invoke-virtual {v1}, Lcom/adobe/mobile/MobileConfig;->getReportSuiteIds()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/adobe/mobile/StaticMethods;->URLEncode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 145
    invoke-static {}, Lcom/adobe/mobile/MobileConfig;->getInstance()Lcom/adobe/mobile/MobileConfig;

    move-result-object v1

    invoke-virtual {v1}, Lcom/adobe/mobile/MobileConfig;->getAnalyticsResponseType()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/JAVA-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "4.13.1-AN"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/s"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/adobe/mobile/AnalyticsWorker;->baseURL:Ljava/lang/String;

    .line 148
    const-string v0, "Analytics - Setting base request URL(%s)"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    sget-object v2, Lcom/adobe/mobile/AnalyticsWorker;->baseURL:Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logDebugFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 151
    :cond_78
    sget-object v0, Lcom/adobe/mobile/AnalyticsWorker;->baseURL:Ljava/lang/String;

    return-object v0
.end method

.method public static sharedInstance()Lcom/adobe/mobile/AnalyticsWorker;
    .registers 3

    .line 62
    sget-object v1, Lcom/adobe/mobile/AnalyticsWorker;->_instanceMutex:Ljava/lang/Object;

    monitor-enter v1

    .line 63
    :try_start_3
    sget-object v0, Lcom/adobe/mobile/AnalyticsWorker;->_instance:Lcom/adobe/mobile/AnalyticsWorker;

    if-nez v0, :cond_e

    .line 64
    new-instance v0, Lcom/adobe/mobile/AnalyticsWorker;

    invoke-direct {v0}, Lcom/adobe/mobile/AnalyticsWorker;-><init>()V

    sput-object v0, Lcom/adobe/mobile/AnalyticsWorker;->_instance:Lcom/adobe/mobile/AnalyticsWorker;

    .line 67
    :cond_e
    sget-object v0, Lcom/adobe/mobile/AnalyticsWorker;->_instance:Lcom/adobe/mobile/AnalyticsWorker;
    :try_end_10
    .catchall {:try_start_3 .. :try_end_10} :catchall_12

    monitor-exit v1

    return-object v0

    .line 68
    :catchall_12
    move-exception v2

    monitor-exit v1

    throw v2
.end method


# virtual methods
.method protected kickWithReferrerData(Ljava/util/Map;)V
    .registers 4
    .param p1, "referrerData"    # Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;)V"
        }
    .end annotation

    .line 358
    if-eqz p1, :cond_8

    invoke-interface {p1}, Ljava/util/Map;->size()I

    move-result v0

    if-gtz v0, :cond_11

    .line 360
    :cond_8
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/adobe/mobile/ReferrerHandler;->setReferrerProcessed(Z)V

    .line 361
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/adobe/mobile/AnalyticsWorker;->kick(Z)V

    .line 362
    return-void

    .line 365
    :cond_11
    invoke-virtual {p0}, Lcom/adobe/mobile/AnalyticsWorker;->selectOldestHit()Lcom/adobe/mobile/AbstractHitDatabase$Hit;

    move-result-object v1

    .line 367
    .local v1, "hit":Lcom/adobe/mobile/AbstractHitDatabase$Hit;
    if-eqz v1, :cond_2a

    iget-object v0, v1, Lcom/adobe/mobile/AbstractHitDatabase$Hit;->urlFragment:Ljava/lang/String;

    if-eqz v0, :cond_2a

    .line 368
    iget-object v0, v1, Lcom/adobe/mobile/AbstractHitDatabase$Hit;->urlFragment:Ljava/lang/String;

    invoke-static {p1, v0}, Lcom/adobe/mobile/StaticMethods;->appendContextData(Ljava/util/Map;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/adobe/mobile/AbstractHitDatabase$Hit;->urlFragment:Ljava/lang/String;

    .line 370
    invoke-virtual {p0, v1}, Lcom/adobe/mobile/AnalyticsWorker;->updateHitInDatabase(Lcom/adobe/mobile/AbstractHitDatabase$Hit;)V

    .line 372
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/adobe/mobile/ReferrerHandler;->setReferrerProcessed(Z)V

    .line 376
    :cond_2a
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/adobe/mobile/AnalyticsWorker;->kick(Z)V

    .line 377
    return-void
.end method

.method protected preMigrate()V
    .registers 8

    .line 160
    new-instance v4, Ljava/io/File;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getCacheDirectory()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/adobe/mobile/AnalyticsWorker;->fileName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 161
    .local v4, "oldFile":Ljava/io/File;
    new-instance v5, Ljava/io/File;

    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getCacheDirectory()Ljava/io/File;

    move-result-object v0

    iget-object v1, p0, Lcom/adobe/mobile/AnalyticsWorker;->fileName:Ljava/lang/String;

    invoke-direct {v5, v0, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 164
    .local v5, "newFile":Ljava/io/File;
    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_52

    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_52

    .line 166
    :try_start_33
    invoke-virtual {v4, v5}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    move-result v0

    if-nez v0, :cond_41

    .line 167
    const-string v0, "Analytics - Unable to migrate old hits db, creating new hits db (move file returned false)"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logWarningFormat(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_41
    .catch Ljava/lang/Exception; {:try_start_33 .. :try_end_41} :catch_42

    .line 171
    :cond_41
    goto :goto_52

    .line 169
    :catch_42
    move-exception v6

    .line 170
    .local v6, "e":Ljava/lang/Exception;
    const-string v0, "Analytics - Unable to migrate old hits db, creating new hits db (%s)"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v6}, Ljava/lang/Exception;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logWarningFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 173
    .end local v6    # "e":Ljava/lang/Exception;
    :cond_52
    :goto_52
    return-void
.end method

.method protected prepareStatements()V
    .registers 6

    .line 178
    :try_start_0
    iget-object v0, p0, Lcom/adobe/mobile/AnalyticsWorker;->database:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "INSERT INTO HITS (URL, TIMESTAMP) VALUES (?, ?)"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v0

    iput-object v0, p0, Lcom/adobe/mobile/AnalyticsWorker;->_preparedInsertStatement:Landroid/database/sqlite/SQLiteStatement;
    :try_end_a
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_a} :catch_b
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_a} :catch_1c
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_a} :catch_2d

    .line 188
    goto :goto_3d

    .line 180
    :catch_b
    move-exception v4

    .line 181
    .local v4, "x":Ljava/lang/NullPointerException;
    const-string v0, "Analytics - Unable to create database due to an invalid path (%s)"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v4}, Ljava/lang/NullPointerException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logErrorFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 188
    .end local v4    # "x":Ljava/lang/NullPointerException;
    goto :goto_3d

    .line 183
    :catch_1c
    move-exception v4

    .line 184
    .local v4, "x":Landroid/database/SQLException;
    const-string v0, "Analytics - Unable to create database due to a sql error (%s)"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v4}, Landroid/database/SQLException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logErrorFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 188
    .end local v4    # "x":Landroid/database/SQLException;
    goto :goto_3d

    .line 186
    :catch_2d
    move-exception v4

    .line 187
    .local v4, "x":Ljava/lang/Exception;
    const-string v0, "Analytics - Unable to create database due to an unexpected error (%s)"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v4}, Ljava/lang/Exception;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logErrorFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 189
    .end local v4    # "x":Ljava/lang/Exception;
    :goto_3d
    return-void
.end method

.method protected queue(Ljava/lang/String;J)V
    .registers 12
    .param p1, "url"    # Ljava/lang/String;
    .param p2, "timeStamp"    # J

    .line 86
    invoke-static {}, Lcom/adobe/mobile/MobileConfig;->getInstance()Lcom/adobe/mobile/MobileConfig;

    move-result-object v4

    .line 87
    .local v4, "mobileConfigInstance":Lcom/adobe/mobile/MobileConfig;
    if-nez v4, :cond_f

    .line 88
    const-string v0, "Analytics - Cannot send hit, MobileConfig is null (this really shouldn\'t happen)"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logErrorFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 89
    return-void

    .line 93
    :cond_f
    invoke-static {}, Lcom/adobe/mobile/MobileConfig;->getInstance()Lcom/adobe/mobile/MobileConfig;

    move-result-object v0

    invoke-virtual {v0}, Lcom/adobe/mobile/MobileConfig;->mobileUsingAnalytics()Z

    move-result v0

    if-nez v0, :cond_1a

    .line 94
    return-void

    .line 98
    :cond_1a
    invoke-virtual {v4}, Lcom/adobe/mobile/MobileConfig;->getPrivacyStatus()Lcom/adobe/mobile/MobilePrivacyStatus;

    move-result-object v0

    sget-object v1, Lcom/adobe/mobile/MobilePrivacyStatus;->MOBILE_PRIVACY_STATUS_OPT_OUT:Lcom/adobe/mobile/MobilePrivacyStatus;

    if-ne v0, v1, :cond_2b

    .line 99
    const-string v0, "Analytics - Ignoring hit due to privacy status being opted out"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logDebugFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 100
    return-void

    .line 104
    :cond_2b
    iget-object v5, p0, Lcom/adobe/mobile/AnalyticsWorker;->dbMutex:Ljava/lang/Object;

    monitor-enter v5

    .line 107
    :try_start_2e
    iget-object v0, p0, Lcom/adobe/mobile/AnalyticsWorker;->_preparedInsertStatement:Landroid/database/sqlite/SQLiteStatement;

    const/4 v1, 0x1

    invoke-virtual {v0, v1, p1}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    .line 108
    iget-object v0, p0, Lcom/adobe/mobile/AnalyticsWorker;->_preparedInsertStatement:Landroid/database/sqlite/SQLiteStatement;

    const/4 v1, 0x2

    invoke-virtual {v0, v1, p2, p3}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 111
    iget-object v0, p0, Lcom/adobe/mobile/AnalyticsWorker;->_preparedInsertStatement:Landroid/database/sqlite/SQLiteStatement;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteStatement;->execute()V

    .line 113
    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {v0}, Lcom/adobe/mobile/StaticMethods;->updateLastKnownTimestamp(Ljava/lang/Long;)V

    .line 115
    iget-wide v0, p0, Lcom/adobe/mobile/AnalyticsWorker;->numberOfUnsentHits:J

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/adobe/mobile/AnalyticsWorker;->numberOfUnsentHits:J

    .line 118
    iget-object v0, p0, Lcom/adobe/mobile/AnalyticsWorker;->_preparedInsertStatement:Landroid/database/sqlite/SQLiteStatement;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteStatement;->clearBindings()V
    :try_end_52
    .catch Landroid/database/SQLException; {:try_start_2e .. :try_end_52} :catch_53
    .catch Ljava/lang/Exception; {:try_start_2e .. :try_end_52} :catch_63
    .catchall {:try_start_2e .. :try_end_52} :catchall_74

    .line 127
    goto :goto_72

    .line 120
    :catch_53
    move-exception v6

    .line 121
    .local v6, "e":Landroid/database/SQLException;
    const-string v0, "Analytics - Unable to insert url (%s)"

    const/4 v1, 0x1

    :try_start_57
    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logErrorFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 122
    invoke-virtual {p0, v6}, Lcom/adobe/mobile/AnalyticsWorker;->resetDatabase(Ljava/lang/Exception;)V

    .line 127
    .end local v6    # "e":Landroid/database/SQLException;
    goto :goto_72

    .line 124
    :catch_63
    move-exception v6

    .line 125
    .local v6, "e":Ljava/lang/Exception;
    const-string v0, "Analytics - Unknown error while inserting url (%s)"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logErrorFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 126
    invoke-virtual {p0, v6}, Lcom/adobe/mobile/AnalyticsWorker;->resetDatabase(Ljava/lang/Exception;)V
    :try_end_72
    .catchall {:try_start_57 .. :try_end_72} :catchall_74

    .line 128
    .end local v6    # "e":Ljava/lang/Exception;
    :goto_72
    monitor-exit v5

    goto :goto_77

    :catchall_74
    move-exception v7

    monitor-exit v5

    throw v7

    .line 130
    :goto_77
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/adobe/mobile/AnalyticsWorker;->kick(Z)V

    .line 131
    return-void
.end method

.method protected selectOldestHit()Lcom/adobe/mobile/AbstractHitDatabase$Hit;
    .registers 16

    .line 193
    const/4 v9, 0x0

    .line 195
    .local v9, "hit":Lcom/adobe/mobile/AbstractHitDatabase$Hit;
    iget-object v10, p0, Lcom/adobe/mobile/AnalyticsWorker;->dbMutex:Ljava/lang/Object;

    monitor-enter v10

    .line 196
    const/4 v11, 0x0

    .line 200
    .local v11, "cursor":Landroid/database/Cursor;
    :try_start_5
    iget-object v0, p0, Lcom/adobe/mobile/AnalyticsWorker;->database:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "HITS"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "ID"

    const/4 v4, 0x0

    aput-object v3, v2, v4

    const-string v3, "URL"

    const/4 v4, 0x1

    aput-object v3, v2, v4

    const-string v3, "TIMESTAMP"

    const/4 v4, 0x2

    aput-object v3, v2, v4

    const-string v7, "ID ASC"

    const-string v8, "1"

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    move-object v11, v0

    .line 202
    invoke-interface {v11}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_49

    .line 203
    new-instance v0, Lcom/adobe/mobile/AbstractHitDatabase$Hit;

    invoke-direct {v0}, Lcom/adobe/mobile/AbstractHitDatabase$Hit;-><init>()V

    move-object v9, v0

    .line 206
    const/4 v0, 0x0

    invoke-interface {v11, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v9, Lcom/adobe/mobile/AbstractHitDatabase$Hit;->identifier:Ljava/lang/String;

    .line 207
    const/4 v0, 0x1

    invoke-interface {v11, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v9, Lcom/adobe/mobile/AbstractHitDatabase$Hit;->urlFragment:Ljava/lang/String;

    .line 208
    const/4 v0, 0x2

    invoke-interface {v11, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    iput-wide v0, v9, Lcom/adobe/mobile/AbstractHitDatabase$Hit;->timestamp:J
    :try_end_49
    .catch Landroid/database/SQLException; {:try_start_5 .. :try_end_49} :catch_4f
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_49} :catch_65
    .catchall {:try_start_5 .. :try_end_49} :catchall_7b

    .line 220
    :cond_49
    if-eqz v11, :cond_82

    .line 221
    :try_start_4b
    invoke-interface {v11}, Landroid/database/Cursor;->close()V
    :try_end_4e
    .catchall {:try_start_4b .. :try_end_4e} :catchall_84

    goto :goto_82

    .line 211
    :catch_4f
    move-exception v12

    .line 213
    .local v12, "e":Landroid/database/SQLException;
    const-string v0, "Analytics - Unable to read from database (%s)"

    const/4 v1, 0x1

    :try_start_53
    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v12}, Landroid/database/SQLException;->getMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logErrorFormat(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_5f
    .catchall {:try_start_53 .. :try_end_5f} :catchall_7b

    .line 220
    .end local v12    # "e":Landroid/database/SQLException;
    if-eqz v11, :cond_82

    .line 221
    :try_start_61
    invoke-interface {v11}, Landroid/database/Cursor;->close()V
    :try_end_64
    .catchall {:try_start_61 .. :try_end_64} :catchall_84

    goto :goto_82

    .line 215
    :catch_65
    move-exception v12

    .line 217
    .local v12, "e":Ljava/lang/Exception;
    const-string v0, "Analytics - Unknown error reading from database (%s)"

    const/4 v1, 0x1

    :try_start_69
    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v12}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logErrorFormat(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_75
    .catchall {:try_start_69 .. :try_end_75} :catchall_7b

    .line 220
    .end local v12    # "e":Ljava/lang/Exception;
    if-eqz v11, :cond_82

    .line 221
    :try_start_77
    invoke-interface {v11}, Landroid/database/Cursor;->close()V
    :try_end_7a
    .catchall {:try_start_77 .. :try_end_7a} :catchall_84

    goto :goto_82

    .line 220
    :catchall_7b
    move-exception v13

    if-eqz v11, :cond_81

    .line 221
    :try_start_7e
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    :cond_81
    throw v13
    :try_end_82
    .catchall {:try_start_7e .. :try_end_82} :catchall_84

    .line 224
    .end local v11    # "cursor":Landroid/database/Cursor;
    :cond_82
    :goto_82
    monitor-exit v10

    goto :goto_87

    :catchall_84
    move-exception v14

    monitor-exit v10

    throw v14

    .line 225
    :goto_87
    return-object v9
.end method

.method protected updateHitInDatabase(Lcom/adobe/mobile/AbstractHitDatabase$Hit;)V
    .registers 9
    .param p1, "hit"    # Lcom/adobe/mobile/AbstractHitDatabase$Hit;

    .line 380
    iget-object v4, p0, Lcom/adobe/mobile/AnalyticsWorker;->dbMutex:Ljava/lang/Object;

    monitor-enter v4

    .line 382
    :try_start_3
    new-instance v5, Landroid/content/ContentValues;

    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    .line 384
    .local v5, "updatedURLFragment":Landroid/content/ContentValues;
    const-string v0, "URL"

    iget-object v1, p1, Lcom/adobe/mobile/AbstractHitDatabase$Hit;->urlFragment:Ljava/lang/String;

    invoke-virtual {v5, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 386
    iget-object v0, p0, Lcom/adobe/mobile/AnalyticsWorker;->database:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "HITS"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "id="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p1, Lcom/adobe/mobile/AbstractHitDatabase$Hit;->identifier:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v5, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_2c
    .catch Landroid/database/SQLException; {:try_start_3 .. :try_end_2c} :catch_2d
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_2c} :catch_3e
    .catchall {:try_start_3 .. :try_end_2c} :catchall_50

    .line 394
    .end local v5    # "updatedURLFragment":Landroid/content/ContentValues;
    goto :goto_4e

    .line 388
    :catch_2d
    move-exception v5

    .line 390
    .local v5, "e":Landroid/database/SQLException;
    const-string v0, "Analytics - Unable to update url in database (%s)"

    const/4 v1, 0x1

    :try_start_31
    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v5}, Landroid/database/SQLException;->getMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logErrorFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 394
    .end local v5    # "e":Landroid/database/SQLException;
    goto :goto_4e

    .line 392
    :catch_3e
    move-exception v5

    .line 393
    .local v5, "e":Ljava/lang/Exception;
    const-string v0, "Analytics - Unknown error updating url in database (%s)"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v5}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logErrorFormat(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_4e
    .catchall {:try_start_31 .. :try_end_4e} :catchall_50

    .line 395
    .end local v5    # "e":Ljava/lang/Exception;
    :goto_4e
    monitor-exit v4

    goto :goto_53

    :catchall_50
    move-exception v6

    monitor-exit v4

    throw v6

    .line 396
    :goto_53
    return-void
.end method

.method protected final workerThread()Ljava/lang/Runnable;
    .registers 2

    .line 230
    new-instance v0, Lcom/adobe/mobile/AnalyticsWorker$1;

    invoke-direct {v0, p0}, Lcom/adobe/mobile/AnalyticsWorker$1;-><init>(Lcom/adobe/mobile/AnalyticsWorker;)V

    return-object v0
.end method

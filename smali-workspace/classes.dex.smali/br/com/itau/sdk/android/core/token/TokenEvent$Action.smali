.class public final enum Lbr/com/itau/sdk/android/core/token/TokenEvent$Action;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lbr/com/itau/sdk/android/core/token/TokenEvent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Action"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<Lbr/com/itau/sdk/android/core/token/TokenEvent$Action;>;"
    }
.end annotation


# static fields
.field private static final $:[B

.field private static $$:I

.field private static final synthetic $VALUES:[Lbr/com/itau/sdk/android/core/token/TokenEvent$Action;

.field public static final enum APP_CONFIRMED:Lbr/com/itau/sdk/android/core/token/TokenEvent$Action;

.field public static final enum APP_SELECTED:Lbr/com/itau/sdk/android/core/token/TokenEvent$Action;

.field public static final enum BACK:Lbr/com/itau/sdk/android/core/token/TokenEvent$Action;

.field public static final enum CHAVEIRO_CONFIRMED:Lbr/com/itau/sdk/android/core/token/TokenEvent$Action;

.field public static final enum CHAVEIRO_SELECTED:Lbr/com/itau/sdk/android/core/token/TokenEvent$Action;

.field public static final enum CONFIRMATION:Lbr/com/itau/sdk/android/core/token/TokenEvent$Action;

.field public static final enum RESEND:Lbr/com/itau/sdk/android/core/token/TokenEvent$Action;

.field public static final enum SMS_CONFIRMED:Lbr/com/itau/sdk/android/core/token/TokenEvent$Action;

.field public static final enum SMS_SELECTED:Lbr/com/itau/sdk/android/core/token/TokenEvent$Action;


# direct methods
.method private static $(III)Ljava/lang/String;
    .registers 9

    rsub-int/lit8 p1, p1, 0x63

    rsub-int/lit8 p2, p2, 0x12

    add-int/lit8 p0, p0, 0x41

    const/4 v5, -0x1

    sget-object v4, Lbr/com/itau/sdk/android/core/token/TokenEvent$Action;->$:[B

    new-instance v0, Ljava/lang/String;

    new-array v1, p2, [B

    add-int/lit8 p2, p2, -0x1

    if-nez v4, :cond_16

    move v2, p0

    move v3, p2

    :goto_13
    add-int/2addr v2, v3

    add-int/lit8 p0, v2, -0x3

    :cond_16
    add-int/lit8 v5, v5, 0x1

    int-to-byte v2, p0

    aput-byte v2, v1, v5

    add-int/lit8 p1, p1, 0x1

    if-ne v5, p2, :cond_24

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Ljava/lang/String;-><init>([BI)V

    return-object v0

    :cond_24
    move v2, p0

    aget-byte v3, v4, p1

    goto :goto_13
.end method

.method static constructor <clinit>()V
    .registers 5

    .line 21
    const/16 v0, 0x67

    new-array v0, v0, [B

    fill-array-data v0, :array_134

    sput-object v0, Lbr/com/itau/sdk/android/core/token/TokenEvent$Action;->$:[B

    const/16 v0, 0xcd

    sput v0, Lbr/com/itau/sdk/android/core/token/TokenEvent$Action;->$$:I

    new-instance v0, Lbr/com/itau/sdk/android/core/token/TokenEvent$Action;

    sget-object v1, Lbr/com/itau/sdk/android/core/token/TokenEvent$Action;->$:[B

    const/16 v2, 0x34

    aget-byte v1, v1, v2

    sget-object v2, Lbr/com/itau/sdk/android/core/token/TokenEvent$Action;->$:[B

    const/4 v3, 0x0

    aget-byte v2, v2, v3

    sget-object v3, Lbr/com/itau/sdk/android/core/token/TokenEvent$Action;->$:[B

    const/16 v4, 0x14

    aget-byte v3, v3, v4

    neg-int v3, v3

    invoke-static {v1, v2, v3}, Lbr/com/itau/sdk/android/core/token/TokenEvent$Action;->$(III)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lbr/com/itau/sdk/android/core/token/TokenEvent$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbr/com/itau/sdk/android/core/token/TokenEvent$Action;->BACK:Lbr/com/itau/sdk/android/core/token/TokenEvent$Action;

    .line 26
    new-instance v0, Lbr/com/itau/sdk/android/core/token/TokenEvent$Action;

    sget-object v1, Lbr/com/itau/sdk/android/core/token/TokenEvent$Action;->$:[B

    const/16 v2, 0xa

    aget-byte v1, v1, v2

    or-int/lit8 v2, v1, 0x40

    sget-object v3, Lbr/com/itau/sdk/android/core/token/TokenEvent$Action;->$:[B

    const/16 v4, 0xc

    aget-byte v3, v3, v4

    invoke-static {v1, v2, v3}, Lbr/com/itau/sdk/android/core/token/TokenEvent$Action;->$(III)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lbr/com/itau/sdk/android/core/token/TokenEvent$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbr/com/itau/sdk/android/core/token/TokenEvent$Action;->CONFIRMATION:Lbr/com/itau/sdk/android/core/token/TokenEvent$Action;

    .line 31
    new-instance v0, Lbr/com/itau/sdk/android/core/token/TokenEvent$Action;

    sget-object v1, Lbr/com/itau/sdk/android/core/token/TokenEvent$Action;->$:[B

    const/16 v2, 0x39

    aget-byte v1, v1, v2

    sget-object v2, Lbr/com/itau/sdk/android/core/token/TokenEvent$Action;->$:[B

    const/16 v3, 0xd

    aget-byte v2, v2, v3

    const/16 v3, 0x2c

    invoke-static {v1, v3, v2}, Lbr/com/itau/sdk/android/core/token/TokenEvent$Action;->$(III)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2}, Lbr/com/itau/sdk/android/core/token/TokenEvent$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbr/com/itau/sdk/android/core/token/TokenEvent$Action;->RESEND:Lbr/com/itau/sdk/android/core/token/TokenEvent$Action;

    .line 36
    new-instance v0, Lbr/com/itau/sdk/android/core/token/TokenEvent$Action;

    sget-object v1, Lbr/com/itau/sdk/android/core/token/TokenEvent$Action;->$:[B

    const/16 v2, 0x2d

    aget-byte v1, v1, v2

    sget-object v2, Lbr/com/itau/sdk/android/core/token/TokenEvent$Action;->$:[B

    const/16 v3, 0xc

    aget-byte v2, v2, v3

    const/16 v3, 0x27

    invoke-static {v1, v3, v2}, Lbr/com/itau/sdk/android/core/token/TokenEvent$Action;->$(III)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x3

    invoke-direct {v0, v1, v2}, Lbr/com/itau/sdk/android/core/token/TokenEvent$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbr/com/itau/sdk/android/core/token/TokenEvent$Action;->SMS_SELECTED:Lbr/com/itau/sdk/android/core/token/TokenEvent$Action;

    .line 41
    new-instance v0, Lbr/com/itau/sdk/android/core/token/TokenEvent$Action;

    sget-object v1, Lbr/com/itau/sdk/android/core/token/TokenEvent$Action;->$:[B

    const/4 v2, 0x0

    aget-byte v1, v1, v2

    or-int/lit8 v2, v1, 0x37

    sget-object v3, Lbr/com/itau/sdk/android/core/token/TokenEvent$Action;->$:[B

    const/16 v4, 0xc

    aget-byte v3, v3, v4

    invoke-static {v1, v2, v3}, Lbr/com/itau/sdk/android/core/token/TokenEvent$Action;->$(III)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x4

    invoke-direct {v0, v1, v2}, Lbr/com/itau/sdk/android/core/token/TokenEvent$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbr/com/itau/sdk/android/core/token/TokenEvent$Action;->APP_SELECTED:Lbr/com/itau/sdk/android/core/token/TokenEvent$Action;

    .line 46
    new-instance v0, Lbr/com/itau/sdk/android/core/token/TokenEvent$Action;

    sget-object v1, Lbr/com/itau/sdk/android/core/token/TokenEvent$Action;->$:[B

    const/16 v2, 0xa

    aget-byte v1, v1, v2

    sget-object v2, Lbr/com/itau/sdk/android/core/token/TokenEvent$Action;->$:[B

    const/16 v3, 0x39

    aget-byte v2, v2, v3

    add-int/lit8 v2, v2, -0x1

    sget-object v3, Lbr/com/itau/sdk/android/core/token/TokenEvent$Action;->$:[B

    const/16 v4, 0x34

    aget-byte v3, v3, v4

    invoke-static {v1, v2, v3}, Lbr/com/itau/sdk/android/core/token/TokenEvent$Action;->$(III)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lbr/com/itau/sdk/android/core/token/TokenEvent$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbr/com/itau/sdk/android/core/token/TokenEvent$Action;->CHAVEIRO_SELECTED:Lbr/com/itau/sdk/android/core/token/TokenEvent$Action;

    .line 51
    new-instance v0, Lbr/com/itau/sdk/android/core/token/TokenEvent$Action;

    sget-object v1, Lbr/com/itau/sdk/android/core/token/TokenEvent$Action;->$:[B

    const/16 v2, 0x2d

    aget-byte v1, v1, v2

    or-int/lit8 v2, v1, 0x4d

    sget-object v3, Lbr/com/itau/sdk/android/core/token/TokenEvent$Action;->$:[B

    const/16 v4, 0x65

    aget-byte v3, v3, v4

    invoke-static {v1, v2, v3}, Lbr/com/itau/sdk/android/core/token/TokenEvent$Action;->$(III)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lbr/com/itau/sdk/android/core/token/TokenEvent$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbr/com/itau/sdk/android/core/token/TokenEvent$Action;->SMS_CONFIRMED:Lbr/com/itau/sdk/android/core/token/TokenEvent$Action;

    .line 56
    new-instance v0, Lbr/com/itau/sdk/android/core/token/TokenEvent$Action;

    sget-object v1, Lbr/com/itau/sdk/android/core/token/TokenEvent$Action;->$:[B

    const/4 v2, 0x0

    aget-byte v1, v1, v2

    or-int/lit8 v2, v1, 0x1c

    sget-object v3, Lbr/com/itau/sdk/android/core/token/TokenEvent$Action;->$:[B

    const/16 v4, 0x65

    aget-byte v3, v3, v4

    invoke-static {v1, v2, v3}, Lbr/com/itau/sdk/android/core/token/TokenEvent$Action;->$(III)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lbr/com/itau/sdk/android/core/token/TokenEvent$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbr/com/itau/sdk/android/core/token/TokenEvent$Action;->APP_CONFIRMED:Lbr/com/itau/sdk/android/core/token/TokenEvent$Action;

    .line 61
    new-instance v0, Lbr/com/itau/sdk/android/core/token/TokenEvent$Action;

    sget-object v1, Lbr/com/itau/sdk/android/core/token/TokenEvent$Action;->$:[B

    const/16 v2, 0xa

    aget-byte v1, v1, v2

    or-int/lit8 v2, v1, 0x51

    sget-object v3, Lbr/com/itau/sdk/android/core/token/TokenEvent$Action;->$:[B

    const/4 v4, 0x0

    aget-byte v3, v3, v4

    invoke-static {v1, v2, v3}, Lbr/com/itau/sdk/android/core/token/TokenEvent$Action;->$(III)Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lbr/com/itau/sdk/android/core/token/TokenEvent$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbr/com/itau/sdk/android/core/token/TokenEvent$Action;->CHAVEIRO_CONFIRMED:Lbr/com/itau/sdk/android/core/token/TokenEvent$Action;

    .line 17
    const/16 v0, 0x9

    new-array v0, v0, [Lbr/com/itau/sdk/android/core/token/TokenEvent$Action;

    sget-object v1, Lbr/com/itau/sdk/android/core/token/TokenEvent$Action;->BACK:Lbr/com/itau/sdk/android/core/token/TokenEvent$Action;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sget-object v1, Lbr/com/itau/sdk/android/core/token/TokenEvent$Action;->CONFIRMATION:Lbr/com/itau/sdk/android/core/token/TokenEvent$Action;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    sget-object v1, Lbr/com/itau/sdk/android/core/token/TokenEvent$Action;->RESEND:Lbr/com/itau/sdk/android/core/token/TokenEvent$Action;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    sget-object v1, Lbr/com/itau/sdk/android/core/token/TokenEvent$Action;->SMS_SELECTED:Lbr/com/itau/sdk/android/core/token/TokenEvent$Action;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    sget-object v1, Lbr/com/itau/sdk/android/core/token/TokenEvent$Action;->APP_SELECTED:Lbr/com/itau/sdk/android/core/token/TokenEvent$Action;

    const/4 v2, 0x4

    aput-object v1, v0, v2

    sget-object v1, Lbr/com/itau/sdk/android/core/token/TokenEvent$Action;->CHAVEIRO_SELECTED:Lbr/com/itau/sdk/android/core/token/TokenEvent$Action;

    const/4 v2, 0x5

    aput-object v1, v0, v2

    sget-object v1, Lbr/com/itau/sdk/android/core/token/TokenEvent$Action;->SMS_CONFIRMED:Lbr/com/itau/sdk/android/core/token/TokenEvent$Action;

    const/4 v2, 0x6

    aput-object v1, v0, v2

    sget-object v1, Lbr/com/itau/sdk/android/core/token/TokenEvent$Action;->APP_CONFIRMED:Lbr/com/itau/sdk/android/core/token/TokenEvent$Action;

    const/4 v2, 0x7

    aput-object v1, v0, v2

    sget-object v1, Lbr/com/itau/sdk/android/core/token/TokenEvent$Action;->CHAVEIRO_CONFIRMED:Lbr/com/itau/sdk/android/core/token/TokenEvent$Action;

    const/16 v2, 0x8

    aput-object v1, v0, v2

    sput-object v0, Lbr/com/itau/sdk/android/core/token/TokenEvent$Action;->$VALUES:[Lbr/com/itau/sdk/android/core/token/TokenEvent$Action;

    return-void

    :array_134
    .array-data 1
        0x0t
        -0x80t
        -0x31t
        0x29t
        -0x40t
        -0x3t
        0x9t
        0xft
        -0x19t
        0xft
        0x2t
        -0x5t
        0x6t
        0xct
        -0x2t
        -0x5t
        0x2t
        0x8t
        -0x4t
        0x18t
        -0xet
        0x7t
        0xct
        0x0t
        0x13t
        -0x19t
        0xft
        0x2t
        -0x5t
        0x6t
        0xct
        -0x2t
        -0x5t
        0x2t
        0xft
        0x2t
        -0x5t
        0x6t
        0xct
        -0x2t
        -0x9t
        0x16t
        -0x8t
        0x9t
        0x2t
        0x12t
        0x3t
        0x12t
        -0x9t
        -0xbt
        0xat
        -0x4t
        0x1t
        0x14t
        -0xct
        0x2t
        -0xat
        0x11t
        -0xbt
        0xct
        -0x7t
        -0x3t
        0x9t
        0xft
        -0x9t
        -0xbt
        0xat
        -0x4t
        0x1t
        0x14t
        -0xct
        0x2t
        0x12t
        0x3t
        0x12t
        -0x19t
        0xft
        0x2t
        -0x5t
        0x6t
        0xct
        -0x2t
        -0x5t
        0x2t
        0x8t
        -0x4t
        0x18t
        -0xet
        0x7t
        0xct
        0x0t
        0x13t
        -0x9t
        -0xbt
        0xat
        -0x4t
        0x1t
        0x14t
        -0xct
        0x2t
        0x2t
        0x5t
        0xbt
    .end array-data
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 17
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lbr/com/itau/sdk/android/core/token/TokenEvent$Action;
    .registers 2

    .line 17
    const-class v0, Lbr/com/itau/sdk/android/core/token/TokenEvent$Action;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lbr/com/itau/sdk/android/core/token/TokenEvent$Action;

    return-object v0
.end method

.method public static values()[Lbr/com/itau/sdk/android/core/token/TokenEvent$Action;
    .registers 1

    .line 17
    sget-object v0, Lbr/com/itau/sdk/android/core/token/TokenEvent$Action;->$VALUES:[Lbr/com/itau/sdk/android/core/token/TokenEvent$Action;

    invoke-virtual {v0}, [Lbr/com/itau/sdk/android/core/token/TokenEvent$Action;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lbr/com/itau/sdk/android/core/token/TokenEvent$Action;

    return-object v0
.end method

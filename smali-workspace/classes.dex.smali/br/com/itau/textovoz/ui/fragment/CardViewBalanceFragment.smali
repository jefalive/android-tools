.class public Lbr/com/itau/textovoz/ui/fragment/CardViewBalanceFragment;
.super Landroid/support/v4/app/Fragment;
.source "CardViewBalanceFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lbr/com/itau/textovoz/ui/fragment/CardViewBalanceFragment$OnBalanceAccountHeaderContainer;
    }
.end annotation


# static fields
.field public static final TAG:Ljava/lang/String;


# instance fields
.field balanceIsLoaded:Z

.field imageViewExpanded:Landroid/widget/ImageView;

.field linearLayoutExpanded:Landroid/widget/LinearLayout;

.field linearLayoutFeedback:Landroid/widget/LinearLayout;

.field linearbalanceDescription:Landroid/widget/LinearLayout;

.field mListener:Lbr/com/itau/textovoz/ui/fragment/CardViewBalanceFragment$OnBalanceAccountHeaderContainer;

.field private final onClickListener:Landroid/view/View$OnClickListener;

.field progressBalanceAccountLoading:Landroid/widget/ProgressBar;

.field relativeBalanceAccountBalanceContainer:Landroid/widget/RelativeLayout;

.field textBalanceAccountError:Landroid/widget/TextView;

.field textViewExpanded:Landroid/widget/TextView;

.field textViewSeeExtract:Landroid/widget/TextView;

.field viewBalanceAccountCurrency:Lbr/com/itau/textovoz/ui/view/custom/CurrencyValueView;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .line 25
    const-class v0, Lbr/com/itau/textovoz/ui/fragment/CardViewBalanceFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lbr/com/itau/textovoz/ui/fragment/CardViewBalanceFragment;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .registers 2

    .line 23
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 155
    new-instance v0, Lbr/com/itau/textovoz/ui/fragment/CardViewBalanceFragment$3;

    invoke-direct {v0, p0}, Lbr/com/itau/textovoz/ui/fragment/CardViewBalanceFragment$3;-><init>(Lbr/com/itau/textovoz/ui/fragment/CardViewBalanceFragment;)V

    iput-object v0, p0, Lbr/com/itau/textovoz/ui/fragment/CardViewBalanceFragment;->onClickListener:Landroid/view/View$OnClickListener;

    return-void
.end method

.method static synthetic access$000(Lbr/com/itau/textovoz/ui/fragment/CardViewBalanceFragment;)V
    .registers 1
    .param p0, "x0"    # Lbr/com/itau/textovoz/ui/fragment/CardViewBalanceFragment;

    .line 23
    invoke-direct {p0}, Lbr/com/itau/textovoz/ui/fragment/CardViewBalanceFragment;->hideLoading()V

    return-void
.end method

.method static synthetic access$100(Lbr/com/itau/textovoz/ui/fragment/CardViewBalanceFragment;Ljava/lang/Double;)V
    .registers 2
    .param p0, "x0"    # Lbr/com/itau/textovoz/ui/fragment/CardViewBalanceFragment;
    .param p1, "x1"    # Ljava/lang/Double;

    .line 23
    invoke-direct {p0, p1}, Lbr/com/itau/textovoz/ui/fragment/CardViewBalanceFragment;->setValueBalance(Ljava/lang/Double;)V

    return-void
.end method

.method private changeBalanceVisibility(Z)V
    .registers 5
    .param p1, "visible"    # Z

    .line 90
    if-eqz p1, :cond_33

    .line 91
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/fragment/CardViewBalanceFragment;->imageViewExpanded:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lbr/com/itau/textovoz/ui/fragment/CardViewBalanceFragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lbr/com/itau/textovoz/R$drawable;->ic_expand_less:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 93
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/fragment/CardViewBalanceFragment;->textViewExpanded:Landroid/widget/TextView;

    invoke-virtual {p0}, Lbr/com/itau/textovoz/ui/fragment/CardViewBalanceFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lbr/com/itau/textovoz/R$string;->expand_card_feedback:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 94
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/fragment/CardViewBalanceFragment;->linearLayoutExpanded:Landroid/widget/LinearLayout;

    const/16 v1, 0x12c

    invoke-static {v0, v1}, Lbr/com/itau/textovoz/util/AnimationUtils;->expandAnimation(Landroid/view/View;I)V

    .line 95
    iget-boolean v0, p0, Lbr/com/itau/textovoz/ui/fragment/CardViewBalanceFragment;->balanceIsLoaded:Z

    if-nez v0, :cond_5c

    .line 96
    invoke-direct {p0}, Lbr/com/itau/textovoz/ui/fragment/CardViewBalanceFragment;->loadBalance()V

    goto :goto_5c

    .line 99
    :cond_33
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/fragment/CardViewBalanceFragment;->imageViewExpanded:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lbr/com/itau/textovoz/ui/fragment/CardViewBalanceFragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lbr/com/itau/textovoz/R$drawable;->ic_expand_more:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 101
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/fragment/CardViewBalanceFragment;->textViewExpanded:Landroid/widget/TextView;

    invoke-virtual {p0}, Lbr/com/itau/textovoz/ui/fragment/CardViewBalanceFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lbr/com/itau/textovoz/R$string;->expand_card_feedback_show:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 103
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/fragment/CardViewBalanceFragment;->linearLayoutExpanded:Landroid/widget/LinearLayout;

    const/16 v1, 0x12c

    invoke-static {v0, v1}, Lbr/com/itau/textovoz/util/AnimationUtils;->collapseAnimation(Landroid/view/View;I)V

    .line 105
    :cond_5c
    :goto_5c
    return-void
.end method

.method private hideLoading()V
    .registers 3

    .line 136
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/fragment/CardViewBalanceFragment;->progressBalanceAccountLoading:Landroid/widget/ProgressBar;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 137
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/fragment/CardViewBalanceFragment;->viewBalanceAccountCurrency:Lbr/com/itau/textovoz/ui/view/custom/CurrencyValueView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lbr/com/itau/textovoz/ui/view/custom/CurrencyValueView;->setVisibility(I)V

    .line 138
    return-void
.end method

.method private loadBalance()V
    .registers 3

    .line 122
    invoke-direct {p0}, Lbr/com/itau/textovoz/ui/fragment/CardViewBalanceFragment;->showLoading()V

    .line 123
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/fragment/CardViewBalanceFragment;->progressBalanceAccountLoading:Landroid/widget/ProgressBar;

    sget v1, Lbr/com/itau/textovoz/R$string;->progressbar_description_wait:I

    invoke-virtual {p0, v1}, Lbr/com/itau/textovoz/ui/fragment/CardViewBalanceFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbr/com/itau/textovoz/util/AccessibilityUtils;->setRequestFocus(Landroid/view/View;Ljava/lang/String;)V

    .line 125
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/fragment/CardViewBalanceFragment;->mListener:Lbr/com/itau/textovoz/ui/fragment/CardViewBalanceFragment$OnBalanceAccountHeaderContainer;

    invoke-interface {v0}, Lbr/com/itau/textovoz/ui/fragment/CardViewBalanceFragment$OnBalanceAccountHeaderContainer;->onBalanceAccountViewBalance()V

    .line 126
    return-void
.end method

.method private setContentDescription(Ljava/lang/Double;)V
    .registers 7
    .param p1, "valueNumber"    # Ljava/lang/Double;

    .line 184
    const-string v4, ""

    .line 186
    .local v4, "contentDescription":Ljava/lang/String;
    invoke-virtual {p1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmpl-double v0, v0, v2

    if-eqz v0, :cond_55

    .line 187
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lbr/com/itau/textovoz/ui/fragment/CardViewBalanceFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    sget v2, Lbr/com/itau/textovoz/R$string;->title_card_simple_balance:I

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 188
    invoke-virtual {p0}, Lbr/com/itau/textovoz/ui/fragment/CardViewBalanceFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    sget v2, Lbr/com/itau/textovoz/R$string;->available_balance:I

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 189
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lbr/com/itau/textovoz/util/CommonUtils;->getMoneyFormatter()Ljava/text/DecimalFormat;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/text/DecimalFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto :goto_5f

    .line 191
    :cond_55
    invoke-virtual {p0}, Lbr/com/itau/textovoz/ui/fragment/CardViewBalanceFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    sget v1, Lbr/com/itau/textovoz/R$string;->error_system_unavailable:I

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 194
    :goto_5f
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/fragment/CardViewBalanceFragment;->linearbalanceDescription:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 195
    return-void
.end method

.method private setValueBalance(Ljava/lang/Double;)V
    .registers 4
    .param p1, "value"    # Ljava/lang/Double;

    .line 168
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/fragment/CardViewBalanceFragment;->viewBalanceAccountCurrency:Lbr/com/itau/textovoz/ui/view/custom/CurrencyValueView;

    invoke-static {}, Lbr/com/itau/textovoz/util/CommonUtils;->getMoneyFormatter()Ljava/text/DecimalFormat;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/text/DecimalFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbr/com/itau/textovoz/ui/view/custom/CurrencyValueView;->setValueText(Ljava/lang/String;)V

    .line 169
    return-void
.end method

.method private showLoading()V
    .registers 3

    .line 129
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/fragment/CardViewBalanceFragment;->progressBalanceAccountLoading:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 130
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/fragment/CardViewBalanceFragment;->viewBalanceAccountCurrency:Lbr/com/itau/textovoz/ui/view/custom/CurrencyValueView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lbr/com/itau/textovoz/ui/view/custom/CurrencyValueView;->setVisibility(I)V

    .line 131
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/fragment/CardViewBalanceFragment;->relativeBalanceAccountBalanceContainer:Landroid/widget/RelativeLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 132
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/fragment/CardViewBalanceFragment;->textBalanceAccountError:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 133
    return-void
.end method


# virtual methods
.method feedbackToggle()V
    .registers 3

    .line 85
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/fragment/CardViewBalanceFragment;->linearLayoutExpanded:Landroid/widget/LinearLayout;

    invoke-static {v0}, Lbr/com/itau/textovoz/util/ViewUtils;->isVisible(Landroid/view/View;)Z

    move-result v0

    if-nez v0, :cond_a

    const/4 v1, 0x1

    goto :goto_b

    :cond_a
    const/4 v1, 0x0

    .line 86
    .local v1, "show":Z
    :goto_b
    invoke-direct {p0, v1}, Lbr/com/itau/textovoz/ui/fragment/CardViewBalanceFragment;->changeBalanceVisibility(Z)V

    .line 87
    return-void
.end method

.method public onAttach(Landroid/app/Activity;)V
    .registers 6
    .param p1, "activity"    # Landroid/app/Activity;

    .line 173
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 175
    move-object v0, p1

    :try_start_4
    check-cast v0, Lbr/com/itau/textovoz/ui/fragment/CardViewBalanceFragment$OnBalanceAccountHeaderContainer;

    iput-object v0, p0, Lbr/com/itau/textovoz/ui/fragment/CardViewBalanceFragment;->mListener:Lbr/com/itau/textovoz/ui/fragment/CardViewBalanceFragment$OnBalanceAccountHeaderContainer;
    :try_end_8
    .catch Ljava/lang/ClassCastException; {:try_start_4 .. :try_end_8} :catch_9

    .line 179
    goto :goto_2d

    .line 176
    :catch_9
    move-exception v3

    .line 177
    .local v3, "e":Ljava/lang/ClassCastException;
    new-instance v0, Ljava/lang/ClassCastException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " must implement "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "OnBalanceAccountHeaderContainer"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ClassCastException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 180
    .end local v3    # "e":Ljava/lang/ClassCastException;
    :goto_2d
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .registers 6
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .line 52
    sget v0, Lbr/com/itau/textovoz/R$layout;->card_view_simple_balance_library:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .registers 5
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;

    .line 57
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/Fragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 59
    sget v0, Lbr/com/itau/textovoz/R$id;->linear_expanded:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lbr/com/itau/textovoz/ui/fragment/CardViewBalanceFragment;->linearLayoutExpanded:Landroid/widget/LinearLayout;

    .line 60
    sget v0, Lbr/com/itau/textovoz/R$id;->linear_extract:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lbr/com/itau/textovoz/ui/fragment/CardViewBalanceFragment;->linearLayoutFeedback:Landroid/widget/LinearLayout;

    .line 61
    sget v0, Lbr/com/itau/textovoz/R$id;->image_view_expanded:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lbr/com/itau/textovoz/ui/fragment/CardViewBalanceFragment;->imageViewExpanded:Landroid/widget/ImageView;

    .line 62
    sget v0, Lbr/com/itau/textovoz/R$id;->text_view_expanded:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lbr/com/itau/textovoz/ui/fragment/CardViewBalanceFragment;->textViewExpanded:Landroid/widget/TextView;

    .line 63
    sget v0, Lbr/com/itau/textovoz/R$id;->progress_account_loading:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lbr/com/itau/textovoz/ui/fragment/CardViewBalanceFragment;->progressBalanceAccountLoading:Landroid/widget/ProgressBar;

    .line 65
    sget v0, Lbr/com/itau/textovoz/R$id;->view_balance_account_currency:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lbr/com/itau/textovoz/ui/view/custom/CurrencyValueView;

    iput-object v0, p0, Lbr/com/itau/textovoz/ui/fragment/CardViewBalanceFragment;->viewBalanceAccountCurrency:Lbr/com/itau/textovoz/ui/view/custom/CurrencyValueView;

    .line 67
    sget v0, Lbr/com/itau/textovoz/R$id;->relative_balance_account_balance_container:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lbr/com/itau/textovoz/ui/fragment/CardViewBalanceFragment;->relativeBalanceAccountBalanceContainer:Landroid/widget/RelativeLayout;

    .line 69
    sget v0, Lbr/com/itau/textovoz/R$id;->text_balance_account_error:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lbr/com/itau/textovoz/ui/fragment/CardViewBalanceFragment;->textBalanceAccountError:Landroid/widget/TextView;

    .line 70
    sget v0, Lbr/com/itau/textovoz/R$id;->textView_see_extract:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lbr/com/itau/textovoz/ui/fragment/CardViewBalanceFragment;->textViewSeeExtract:Landroid/widget/TextView;

    .line 71
    sget v0, Lbr/com/itau/textovoz/R$id;->linear_balance_description:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lbr/com/itau/textovoz/ui/fragment/CardViewBalanceFragment;->linearbalanceDescription:Landroid/widget/LinearLayout;

    .line 74
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/fragment/CardViewBalanceFragment;->linearLayoutFeedback:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lbr/com/itau/textovoz/ui/fragment/CardViewBalanceFragment;->onClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 75
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/fragment/CardViewBalanceFragment;->textViewSeeExtract:Landroid/widget/TextView;

    iget-object v1, p0, Lbr/com/itau/textovoz/ui/fragment/CardViewBalanceFragment;->onClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 77
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lbr/com/itau/textovoz/ui/fragment/CardViewBalanceFragment;->changeBalanceVisibility(Z)V

    .line 78
    return-void
.end method

.method relativeBalanceAccountHeaderContainer()V
    .registers 2

    .line 81
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/fragment/CardViewBalanceFragment;->mListener:Lbr/com/itau/textovoz/ui/fragment/CardViewBalanceFragment$OnBalanceAccountHeaderContainer;

    invoke-interface {v0}, Lbr/com/itau/textovoz/ui/fragment/CardViewBalanceFragment$OnBalanceAccountHeaderContainer;->onBalanceAccountHeaderSelected()V

    .line 82
    return-void
.end method

.method public showContent(Ljava/lang/Double;)V
    .registers 4
    .param p1, "value"    # Ljava/lang/Double;

    .line 141
    invoke-direct {p0, p1}, Lbr/com/itau/textovoz/ui/fragment/CardViewBalanceFragment;->setContentDescription(Ljava/lang/Double;)V

    .line 142
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbr/com/itau/textovoz/ui/fragment/CardViewBalanceFragment;->balanceIsLoaded:Z

    .line 143
    invoke-virtual {p0}, Lbr/com/itau/textovoz/ui/fragment/CardViewBalanceFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    new-instance v1, Lbr/com/itau/textovoz/ui/fragment/CardViewBalanceFragment$2;

    invoke-direct {v1, p0, p1}, Lbr/com/itau/textovoz/ui/fragment/CardViewBalanceFragment$2;-><init>(Lbr/com/itau/textovoz/ui/fragment/CardViewBalanceFragment;Ljava/lang/Double;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 153
    return-void
.end method

.method public showError()V
    .registers 3

    .line 108
    const-wide/16 v0, 0x0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    invoke-direct {p0, v0}, Lbr/com/itau/textovoz/ui/fragment/CardViewBalanceFragment;->setContentDescription(Ljava/lang/Double;)V

    .line 109
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbr/com/itau/textovoz/ui/fragment/CardViewBalanceFragment;->balanceIsLoaded:Z

    .line 110
    invoke-virtual {p0}, Lbr/com/itau/textovoz/ui/fragment/CardViewBalanceFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    new-instance v1, Lbr/com/itau/textovoz/ui/fragment/CardViewBalanceFragment$1;

    invoke-direct {v1, p0}, Lbr/com/itau/textovoz/ui/fragment/CardViewBalanceFragment$1;-><init>(Lbr/com/itau/textovoz/ui/fragment/CardViewBalanceFragment;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 119
    return-void
.end method

.class public Lcom/itau/empresas/api/model/PagamentoVO;
.super Ljava/lang/Object;
.source "PagamentoVO.java"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/itau/empresas/api/model/PagamentoVO$ComIDApenas;
    }
.end annotation


# instance fields
.field private codigoBarras:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "codigo_barras"
    .end annotation
.end field

.field private codigoOperador:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "codigo_operador"
    .end annotation
.end field

.field private concessionaria:Z
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "concessionaria"
    .end annotation
.end field

.field private dataInclusao:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "data_inclusao"
    .end annotation
.end field

.field private dataVencimento:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "data_vencimento"
    .end annotation
.end field

.field private favorecido:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "favorecido"
    .end annotation
.end field

.field private horaInclusao:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "hora_inclusao"
    .end annotation
.end field

.field private id:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "id"
    .end annotation
.end field

.field private identificacaoComprovante:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "identificacao_comprovante"
    .end annotation
.end field

.field private nomeEmpresa:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "nome_empresa"
    .end annotation
.end field

.field private nomeOperador:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "nome_operador"
    .end annotation
.end field

.field private pendenteAutorizacao:Z
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "pendente_autorizacao"
    .end annotation
.end field

.field private tipoInclusao:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "tipo_inclusao"
    .end annotation
.end field

.field private valorDesconto:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "valor_desconto"
    .end annotation
.end field

.field private valorJuros:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "valor_juros"
    .end annotation
.end field

.field private valorPagamento:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "valor_documento"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .registers 5
    .param p1, "o"    # Ljava/lang/Object;

    .line 105
    if-ne p0, p1, :cond_4

    .line 106
    const/4 v0, 0x1

    return v0

    .line 108
    :cond_4
    if-eqz p1, :cond_10

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    if-eq v0, v1, :cond_12

    .line 109
    :cond_10
    const/4 v0, 0x0

    return v0

    .line 112
    :cond_12
    move-object v2, p1

    check-cast v2, Lcom/itau/empresas/api/model/PagamentoVO;

    .line 114
    .local v2, "that":Lcom/itau/empresas/api/model/PagamentoVO;
    iget-boolean v0, p0, Lcom/itau/empresas/api/model/PagamentoVO;->concessionaria:Z

    iget-boolean v1, v2, Lcom/itau/empresas/api/model/PagamentoVO;->concessionaria:Z

    if-eq v0, v1, :cond_1d

    .line 115
    const/4 v0, 0x0

    return v0

    .line 117
    :cond_1d
    iget-boolean v0, p0, Lcom/itau/empresas/api/model/PagamentoVO;->pendenteAutorizacao:Z

    iget-boolean v1, v2, Lcom/itau/empresas/api/model/PagamentoVO;->pendenteAutorizacao:Z

    if-eq v0, v1, :cond_25

    .line 118
    const/4 v0, 0x0

    return v0

    .line 120
    :cond_25
    iget-object v0, p0, Lcom/itau/empresas/api/model/PagamentoVO;->codigoBarras:Ljava/lang/String;

    if-eqz v0, :cond_34

    iget-object v0, p0, Lcom/itau/empresas/api/model/PagamentoVO;->codigoBarras:Ljava/lang/String;

    iget-object v1, v2, Lcom/itau/empresas/api/model/PagamentoVO;->codigoBarras:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3a

    goto :goto_38

    :cond_34
    iget-object v0, v2, Lcom/itau/empresas/api/model/PagamentoVO;->codigoBarras:Ljava/lang/String;

    if-eqz v0, :cond_3a

    .line 122
    :goto_38
    const/4 v0, 0x0

    return v0

    .line 124
    :cond_3a
    iget-object v0, p0, Lcom/itau/empresas/api/model/PagamentoVO;->codigoOperador:Ljava/lang/String;

    if-eqz v0, :cond_49

    iget-object v0, p0, Lcom/itau/empresas/api/model/PagamentoVO;->codigoOperador:Ljava/lang/String;

    iget-object v1, v2, Lcom/itau/empresas/api/model/PagamentoVO;->codigoOperador:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4f

    goto :goto_4d

    :cond_49
    iget-object v0, v2, Lcom/itau/empresas/api/model/PagamentoVO;->codigoOperador:Ljava/lang/String;

    if-eqz v0, :cond_4f

    .line 126
    :goto_4d
    const/4 v0, 0x0

    return v0

    .line 128
    :cond_4f
    iget-object v0, p0, Lcom/itau/empresas/api/model/PagamentoVO;->dataInclusao:Ljava/lang/String;

    if-eqz v0, :cond_5e

    iget-object v0, p0, Lcom/itau/empresas/api/model/PagamentoVO;->dataInclusao:Ljava/lang/String;

    iget-object v1, v2, Lcom/itau/empresas/api/model/PagamentoVO;->dataInclusao:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_64

    goto :goto_62

    :cond_5e
    iget-object v0, v2, Lcom/itau/empresas/api/model/PagamentoVO;->dataInclusao:Ljava/lang/String;

    if-eqz v0, :cond_64

    .line 130
    :goto_62
    const/4 v0, 0x0

    return v0

    .line 132
    :cond_64
    iget-object v0, p0, Lcom/itau/empresas/api/model/PagamentoVO;->dataVencimento:Ljava/lang/String;

    if-eqz v0, :cond_73

    iget-object v0, p0, Lcom/itau/empresas/api/model/PagamentoVO;->dataVencimento:Ljava/lang/String;

    iget-object v1, v2, Lcom/itau/empresas/api/model/PagamentoVO;->dataVencimento:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_79

    goto :goto_77

    :cond_73
    iget-object v0, v2, Lcom/itau/empresas/api/model/PagamentoVO;->dataVencimento:Ljava/lang/String;

    if-eqz v0, :cond_79

    .line 134
    :goto_77
    const/4 v0, 0x0

    return v0

    .line 136
    :cond_79
    iget-object v0, p0, Lcom/itau/empresas/api/model/PagamentoVO;->favorecido:Ljava/lang/String;

    if-eqz v0, :cond_88

    iget-object v0, p0, Lcom/itau/empresas/api/model/PagamentoVO;->favorecido:Ljava/lang/String;

    iget-object v1, v2, Lcom/itau/empresas/api/model/PagamentoVO;->favorecido:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_8e

    goto :goto_8c

    :cond_88
    iget-object v0, v2, Lcom/itau/empresas/api/model/PagamentoVO;->favorecido:Ljava/lang/String;

    if-eqz v0, :cond_8e

    .line 137
    :goto_8c
    const/4 v0, 0x0

    return v0

    .line 139
    :cond_8e
    iget-object v0, p0, Lcom/itau/empresas/api/model/PagamentoVO;->horaInclusao:Ljava/lang/String;

    if-eqz v0, :cond_9d

    iget-object v0, p0, Lcom/itau/empresas/api/model/PagamentoVO;->horaInclusao:Ljava/lang/String;

    iget-object v1, v2, Lcom/itau/empresas/api/model/PagamentoVO;->horaInclusao:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_a3

    goto :goto_a1

    :cond_9d
    iget-object v0, v2, Lcom/itau/empresas/api/model/PagamentoVO;->horaInclusao:Ljava/lang/String;

    if-eqz v0, :cond_a3

    .line 141
    :goto_a1
    const/4 v0, 0x0

    return v0

    .line 143
    :cond_a3
    iget-object v0, p0, Lcom/itau/empresas/api/model/PagamentoVO;->id:Ljava/lang/String;

    if-eqz v0, :cond_b2

    iget-object v0, p0, Lcom/itau/empresas/api/model/PagamentoVO;->id:Ljava/lang/String;

    iget-object v1, v2, Lcom/itau/empresas/api/model/PagamentoVO;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_b8

    goto :goto_b6

    :cond_b2
    iget-object v0, v2, Lcom/itau/empresas/api/model/PagamentoVO;->id:Ljava/lang/String;

    if-eqz v0, :cond_b8

    .line 144
    :goto_b6
    const/4 v0, 0x0

    return v0

    .line 146
    :cond_b8
    iget-object v0, p0, Lcom/itau/empresas/api/model/PagamentoVO;->identificacaoComprovante:Ljava/lang/String;

    if-eqz v0, :cond_c7

    iget-object v0, p0, Lcom/itau/empresas/api/model/PagamentoVO;->identificacaoComprovante:Ljava/lang/String;

    iget-object v1, v2, Lcom/itau/empresas/api/model/PagamentoVO;->identificacaoComprovante:Ljava/lang/String;

    .line 147
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_cd

    goto :goto_cb

    :cond_c7
    iget-object v0, v2, Lcom/itau/empresas/api/model/PagamentoVO;->identificacaoComprovante:Ljava/lang/String;

    if-eqz v0, :cond_cd

    .line 148
    :goto_cb
    const/4 v0, 0x0

    return v0

    .line 150
    :cond_cd
    iget-object v0, p0, Lcom/itau/empresas/api/model/PagamentoVO;->nomeEmpresa:Ljava/lang/String;

    if-eqz v0, :cond_dc

    iget-object v0, p0, Lcom/itau/empresas/api/model/PagamentoVO;->nomeEmpresa:Ljava/lang/String;

    iget-object v1, v2, Lcom/itau/empresas/api/model/PagamentoVO;->nomeEmpresa:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_e2

    goto :goto_e0

    :cond_dc
    iget-object v0, v2, Lcom/itau/empresas/api/model/PagamentoVO;->nomeEmpresa:Ljava/lang/String;

    if-eqz v0, :cond_e2

    .line 152
    :goto_e0
    const/4 v0, 0x0

    return v0

    .line 154
    :cond_e2
    iget-object v0, p0, Lcom/itau/empresas/api/model/PagamentoVO;->nomeOperador:Ljava/lang/String;

    if-eqz v0, :cond_f1

    iget-object v0, p0, Lcom/itau/empresas/api/model/PagamentoVO;->nomeOperador:Ljava/lang/String;

    iget-object v1, v2, Lcom/itau/empresas/api/model/PagamentoVO;->nomeOperador:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_f7

    goto :goto_f5

    :cond_f1
    iget-object v0, v2, Lcom/itau/empresas/api/model/PagamentoVO;->nomeOperador:Ljava/lang/String;

    if-eqz v0, :cond_f7

    .line 156
    :goto_f5
    const/4 v0, 0x0

    return v0

    .line 158
    :cond_f7
    iget-object v0, p0, Lcom/itau/empresas/api/model/PagamentoVO;->tipoInclusao:Ljava/lang/String;

    if-eqz v0, :cond_106

    iget-object v0, p0, Lcom/itau/empresas/api/model/PagamentoVO;->tipoInclusao:Ljava/lang/String;

    iget-object v1, v2, Lcom/itau/empresas/api/model/PagamentoVO;->tipoInclusao:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_10c

    goto :goto_10a

    :cond_106
    iget-object v0, v2, Lcom/itau/empresas/api/model/PagamentoVO;->tipoInclusao:Ljava/lang/String;

    if-eqz v0, :cond_10c

    .line 160
    :goto_10a
    const/4 v0, 0x0

    return v0

    .line 162
    :cond_10c
    iget-object v0, p0, Lcom/itau/empresas/api/model/PagamentoVO;->valorDesconto:Ljava/lang/String;

    if-eqz v0, :cond_11b

    iget-object v0, p0, Lcom/itau/empresas/api/model/PagamentoVO;->valorDesconto:Ljava/lang/String;

    iget-object v1, v2, Lcom/itau/empresas/api/model/PagamentoVO;->valorDesconto:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_121

    goto :goto_11f

    :cond_11b
    iget-object v0, v2, Lcom/itau/empresas/api/model/PagamentoVO;->valorDesconto:Ljava/lang/String;

    if-eqz v0, :cond_121

    .line 164
    :goto_11f
    const/4 v0, 0x0

    return v0

    .line 166
    :cond_121
    iget-object v0, p0, Lcom/itau/empresas/api/model/PagamentoVO;->valorJuros:Ljava/lang/String;

    if-eqz v0, :cond_130

    iget-object v0, p0, Lcom/itau/empresas/api/model/PagamentoVO;->valorJuros:Ljava/lang/String;

    iget-object v1, v2, Lcom/itau/empresas/api/model/PagamentoVO;->valorJuros:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_136

    goto :goto_134

    :cond_130
    iget-object v0, v2, Lcom/itau/empresas/api/model/PagamentoVO;->valorJuros:Ljava/lang/String;

    if-eqz v0, :cond_136

    .line 167
    :goto_134
    const/4 v0, 0x0

    return v0

    .line 169
    :cond_136
    iget-object v0, p0, Lcom/itau/empresas/api/model/PagamentoVO;->valorPagamento:Ljava/lang/String;

    if-eqz v0, :cond_145

    iget-object v0, p0, Lcom/itau/empresas/api/model/PagamentoVO;->valorPagamento:Ljava/lang/String;

    iget-object v1, v2, Lcom/itau/empresas/api/model/PagamentoVO;->valorPagamento:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_14b

    goto :goto_149

    :cond_145
    iget-object v0, v2, Lcom/itau/empresas/api/model/PagamentoVO;->valorPagamento:Ljava/lang/String;

    if-eqz v0, :cond_14b

    .line 171
    :goto_149
    const/4 v0, 0x0

    return v0

    .line 174
    :cond_14b
    const/4 v0, 0x1

    return v0
.end method

.method public hashCode()I
    .registers 4

    .line 179
    iget-object v0, p0, Lcom/itau/empresas/api/model/PagamentoVO;->id:Ljava/lang/String;

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/itau/empresas/api/model/PagamentoVO;->id:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_c

    :cond_b
    const/4 v2, 0x0

    .line 180
    .local v2, "result":I
    :goto_c
    mul-int/lit8 v0, v2, 0x1f

    iget-object v1, p0, Lcom/itau/empresas/api/model/PagamentoVO;->dataInclusao:Ljava/lang/String;

    if-eqz v1, :cond_19

    iget-object v1, p0, Lcom/itau/empresas/api/model/PagamentoVO;->dataInclusao:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1a

    :cond_19
    const/4 v1, 0x0

    :goto_1a
    add-int v2, v0, v1

    .line 181
    mul-int/lit8 v0, v2, 0x1f

    iget-object v1, p0, Lcom/itau/empresas/api/model/PagamentoVO;->horaInclusao:Ljava/lang/String;

    if-eqz v1, :cond_29

    iget-object v1, p0, Lcom/itau/empresas/api/model/PagamentoVO;->horaInclusao:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2a

    :cond_29
    const/4 v1, 0x0

    :goto_2a
    add-int v2, v0, v1

    .line 182
    mul-int/lit8 v0, v2, 0x1f

    iget-object v1, p0, Lcom/itau/empresas/api/model/PagamentoVO;->nomeEmpresa:Ljava/lang/String;

    if-eqz v1, :cond_39

    iget-object v1, p0, Lcom/itau/empresas/api/model/PagamentoVO;->nomeEmpresa:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_3a

    :cond_39
    const/4 v1, 0x0

    :goto_3a
    add-int v2, v0, v1

    .line 183
    mul-int/lit8 v0, v2, 0x1f

    iget-object v1, p0, Lcom/itau/empresas/api/model/PagamentoVO;->nomeOperador:Ljava/lang/String;

    if-eqz v1, :cond_49

    iget-object v1, p0, Lcom/itau/empresas/api/model/PagamentoVO;->nomeOperador:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_4a

    :cond_49
    const/4 v1, 0x0

    :goto_4a
    add-int v2, v0, v1

    .line 184
    mul-int/lit8 v0, v2, 0x1f

    iget-object v1, p0, Lcom/itau/empresas/api/model/PagamentoVO;->codigoOperador:Ljava/lang/String;

    if-eqz v1, :cond_59

    iget-object v1, p0, Lcom/itau/empresas/api/model/PagamentoVO;->codigoOperador:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_5a

    :cond_59
    const/4 v1, 0x0

    :goto_5a
    add-int v2, v0, v1

    .line 185
    mul-int/lit8 v0, v2, 0x1f

    iget-object v1, p0, Lcom/itau/empresas/api/model/PagamentoVO;->dataVencimento:Ljava/lang/String;

    if-eqz v1, :cond_69

    iget-object v1, p0, Lcom/itau/empresas/api/model/PagamentoVO;->dataVencimento:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_6a

    :cond_69
    const/4 v1, 0x0

    :goto_6a
    add-int v2, v0, v1

    .line 186
    mul-int/lit8 v0, v2, 0x1f

    iget-object v1, p0, Lcom/itau/empresas/api/model/PagamentoVO;->codigoBarras:Ljava/lang/String;

    if-eqz v1, :cond_79

    iget-object v1, p0, Lcom/itau/empresas/api/model/PagamentoVO;->codigoBarras:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_7a

    :cond_79
    const/4 v1, 0x0

    :goto_7a
    add-int v2, v0, v1

    .line 187
    mul-int/lit8 v0, v2, 0x1f

    iget-object v1, p0, Lcom/itau/empresas/api/model/PagamentoVO;->valorPagamento:Ljava/lang/String;

    if-eqz v1, :cond_89

    iget-object v1, p0, Lcom/itau/empresas/api/model/PagamentoVO;->valorPagamento:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_8a

    :cond_89
    const/4 v1, 0x0

    :goto_8a
    add-int v2, v0, v1

    .line 188
    mul-int/lit8 v0, v2, 0x1f

    iget-object v1, p0, Lcom/itau/empresas/api/model/PagamentoVO;->valorJuros:Ljava/lang/String;

    if-eqz v1, :cond_99

    iget-object v1, p0, Lcom/itau/empresas/api/model/PagamentoVO;->valorJuros:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_9a

    :cond_99
    const/4 v1, 0x0

    :goto_9a
    add-int v2, v0, v1

    .line 189
    mul-int/lit8 v0, v2, 0x1f

    iget-object v1, p0, Lcom/itau/empresas/api/model/PagamentoVO;->valorDesconto:Ljava/lang/String;

    if-eqz v1, :cond_a9

    iget-object v1, p0, Lcom/itau/empresas/api/model/PagamentoVO;->valorDesconto:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_aa

    :cond_a9
    const/4 v1, 0x0

    :goto_aa
    add-int v2, v0, v1

    .line 190
    mul-int/lit8 v0, v2, 0x1f

    iget-object v1, p0, Lcom/itau/empresas/api/model/PagamentoVO;->identificacaoComprovante:Ljava/lang/String;

    if-eqz v1, :cond_b9

    iget-object v1, p0, Lcom/itau/empresas/api/model/PagamentoVO;->identificacaoComprovante:Ljava/lang/String;

    .line 192
    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_ba

    :cond_b9
    const/4 v1, 0x0

    :goto_ba
    add-int v2, v0, v1

    .line 193
    mul-int/lit8 v0, v2, 0x1f

    iget-object v1, p0, Lcom/itau/empresas/api/model/PagamentoVO;->favorecido:Ljava/lang/String;

    if-eqz v1, :cond_c9

    iget-object v1, p0, Lcom/itau/empresas/api/model/PagamentoVO;->favorecido:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_ca

    :cond_c9
    const/4 v1, 0x0

    :goto_ca
    add-int v2, v0, v1

    .line 194
    mul-int/lit8 v0, v2, 0x1f

    iget-object v1, p0, Lcom/itau/empresas/api/model/PagamentoVO;->tipoInclusao:Ljava/lang/String;

    if-eqz v1, :cond_d9

    iget-object v1, p0, Lcom/itau/empresas/api/model/PagamentoVO;->tipoInclusao:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_da

    :cond_d9
    const/4 v1, 0x0

    :goto_da
    add-int v2, v0, v1

    .line 195
    mul-int/lit8 v0, v2, 0x1f

    iget-boolean v1, p0, Lcom/itau/empresas/api/model/PagamentoVO;->pendenteAutorizacao:Z

    if-eqz v1, :cond_e4

    const/4 v1, 0x1

    goto :goto_e5

    :cond_e4
    const/4 v1, 0x0

    :goto_e5
    add-int v2, v0, v1

    .line 196
    mul-int/lit8 v0, v2, 0x1f

    iget-boolean v1, p0, Lcom/itau/empresas/api/model/PagamentoVO;->concessionaria:Z

    if-eqz v1, :cond_ef

    const/4 v1, 0x1

    goto :goto_f0

    :cond_ef
    const/4 v1, 0x0

    :goto_f0
    add-int v2, v0, v1

    .line 197
    return v2
.end method

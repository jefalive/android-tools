.class final Lcom/itau/empresas/ui/util/MascaraCelular$1;
.super Lcom/itau/empresas/ui/util/SimpleTextWatcher;
.source "MascaraCelular.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/itau/empresas/ui/util/MascaraCelular;->insere(Landroid/widget/EditText;)Landroid/text/TextWatcher;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field private final TAMANHO_MASCARA_MAIOR:I

.field private mask:Ljava/lang/String;

.field oldCount:I

.field final synthetic val$ediTxt:Landroid/widget/EditText;


# direct methods
.method constructor <init>(Landroid/widget/EditText;)V
    .registers 3

    .line 17
    iput-object p1, p0, Lcom/itau/empresas/ui/util/MascaraCelular$1;->val$ediTxt:Landroid/widget/EditText;

    invoke-direct {p0}, Lcom/itau/empresas/ui/util/SimpleTextWatcher;-><init>()V

    .line 19
    const-string v0, "(##) ####-####"

    iput-object v0, p0, Lcom/itau/empresas/ui/util/MascaraCelular$1;->mask:Ljava/lang/String;

    .line 20
    const/4 v0, 0x0

    iput v0, p0, Lcom/itau/empresas/ui/util/MascaraCelular$1;->oldCount:I

    .line 21
    const-string v0, "(##) #####-####"

    invoke-direct {p0, v0}, Lcom/itau/empresas/ui/util/MascaraCelular$1;->tamanhoMascara(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/itau/empresas/ui/util/MascaraCelular$1;->TAMANHO_MASCARA_MAIOR:I

    return-void
.end method

.method private tamanhoMascara(Ljava/lang/String;)I
    .registers 9
    .param p1, "s"    # Ljava/lang/String;

    .line 24
    const/4 v2, 0x0

    .line 25
    .local v2, "tamanho":I
    invoke-virtual {p1}, Ljava/lang/String;->toCharArray()[C

    move-result-object v3

    array-length v4, v3

    const/4 v5, 0x0

    :goto_7
    if-ge v5, v4, :cond_1c

    aget-char v0, v3, v5

    invoke-static {v0}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v6

    .line 26
    .local v6, "c":Ljava/lang/Character;
    invoke-virtual {v6}, Ljava/lang/Character;->charValue()C

    move-result v0

    const/16 v1, 0x23

    if-ne v0, v1, :cond_19

    .line 27
    add-int/lit8 v2, v2, 0x1

    .line 25
    .end local v6    # "c":Ljava/lang/Character;
    :cond_19
    add-int/lit8 v5, v5, 0x1

    goto :goto_7

    .line 30
    :cond_1c
    return v2
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .registers 13
    .param p1, "s"    # Landroid/text/Editable;

    .line 42
    iget-object v0, p0, Lcom/itau/empresas/ui/util/MascaraCelular$1;->val$ediTxt:Landroid/widget/EditText;

    invoke-virtual {v0, p0}, Landroid/widget/EditText;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 44
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/itau/empresas/ui/util/MascaraCelular;->removeMascara(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 46
    .local v3, "str":Ljava/lang/String;
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v0

    iget v1, p0, Lcom/itau/empresas/ui/util/MascaraCelular$1;->TAMANHO_MASCARA_MAIOR:I

    if-lt v0, v1, :cond_1a

    .line 47
    const-string v0, "(##) #####-####"

    iput-object v0, p0, Lcom/itau/empresas/ui/util/MascaraCelular$1;->mask:Ljava/lang/String;

    goto :goto_1e

    .line 49
    :cond_1a
    const-string v0, "(##) ####-####"

    iput-object v0, p0, Lcom/itau/empresas/ui/util/MascaraCelular$1;->mask:Ljava/lang/String;

    .line 52
    :goto_1e
    iget v0, p0, Lcom/itau/empresas/ui/util/MascaraCelular$1;->oldCount:I

    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v1

    if-le v0, v1, :cond_28

    const/4 v4, 0x1

    goto :goto_29

    :cond_28
    const/4 v4, 0x0

    .line 53
    .local v4, "apagou":Z
    :goto_29
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v0, ""

    invoke-direct {v5, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 55
    .local v5, "resultado":Ljava/lang/StringBuilder;
    const/4 v6, 0x0

    .line 57
    .local v6, "i":I
    iget-object v0, p0, Lcom/itau/empresas/ui/util/MascaraCelular$1;->mask:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v7

    array-length v8, v7

    const/4 v9, 0x0

    :goto_39
    if-ge v9, v8, :cond_58

    aget-char v10, v7, v9

    .line 58
    .local v10, "m":C
    const/16 v0, 0x23

    if-eq v10, v0, :cond_45

    .line 59
    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 60
    goto :goto_55

    .line 63
    :cond_45
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v0

    if-lt v6, v0, :cond_4c

    .line 64
    goto :goto_58

    .line 67
    :cond_4c
    invoke-virtual {v3, v6}, Ljava/lang/String;->charAt(I)C

    move-result v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 68
    add-int/lit8 v6, v6, 0x1

    .line 57
    .end local v10    # "m":C
    :goto_55
    add-int/lit8 v9, v9, 0x1

    goto :goto_39

    .line 71
    :cond_58
    :goto_58
    if-eqz v4, :cond_7a

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-lez v0, :cond_7a

    iget-object v0, p0, Lcom/itau/empresas/ui/util/MascaraCelular$1;->mask:Ljava/lang/String;

    .line 72
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    const/16 v1, 0x23

    if-eq v0, v1, :cond_7a

    .line 73
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->deleteCharAt(I)Ljava/lang/StringBuilder;

    goto :goto_58

    .line 76
    :cond_7a
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    iput v0, p0, Lcom/itau/empresas/ui/util/MascaraCelular$1;->oldCount:I

    .line 77
    iget-object v0, p0, Lcom/itau/empresas/ui/util/MascaraCelular$1;->val$ediTxt:Landroid/widget/EditText;

    invoke-virtual {v0, v5}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 78
    iget-object v0, p0, Lcom/itau/empresas/ui/util/MascaraCelular$1;->val$ediTxt:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/itau/empresas/ui/util/MascaraCelular$1;->val$ediTxt:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setSelection(I)V

    .line 79
    iget-object v0, p0, Lcom/itau/empresas/ui/util/MascaraCelular$1;->val$ediTxt:Landroid/widget/EditText;

    invoke-virtual {v0, p0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 80
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .registers 5
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "count"    # I
    .param p4, "after"    # I

    .line 36
    invoke-super {p0, p1, p2, p3, p4}, Lcom/itau/empresas/ui/util/SimpleTextWatcher;->beforeTextChanged(Ljava/lang/CharSequence;III)V

    .line 37
    return-void
.end method

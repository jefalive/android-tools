.class public Lcom/google/android/gms/ads/internal/purchase/zze;
.super Lcom/google/android/gms/internal/zzge$zza;

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation runtime Lcom/google/android/gms/internal/zzhb;
.end annotation


# instance fields
.field private final mActivity:Landroid/app/Activity;

.field private zzFC:Lcom/google/android/gms/ads/internal/purchase/zzb;

.field zzFD:Lcom/google/android/gms/ads/internal/purchase/zzh;

.field private zzFF:Lcom/google/android/gms/ads/internal/purchase/zzk;

.field private zzFK:Landroid/content/Context;

.field private zzFL:Lcom/google/android/gms/internal/zzgc;

.field private zzFM:Lcom/google/android/gms/ads/internal/purchase/zzf;

.field private zzFN:Lcom/google/android/gms/ads/internal/purchase/zzj;

.field private zzFO:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/app/Activity;)V
    .registers 3

    invoke-direct {p0}, Lcom/google/android/gms/internal/zzge$zza;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/ads/internal/purchase/zze;->zzFO:Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/gms/ads/internal/purchase/zze;->mActivity:Landroid/app/Activity;

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/purchase/zze;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/purchase/zzh;->zzy(Landroid/content/Context;)Lcom/google/android/gms/ads/internal/purchase/zzh;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/ads/internal/purchase/zze;->zzFD:Lcom/google/android/gms/ads/internal/purchase/zzh;

    return-void
.end method


# virtual methods
.method public onActivityResult(IILandroid/content/Intent;)V
    .registers 9
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    const/16 v0, 0x3e9

    if-ne p1, v0, :cond_54

    const/4 v2, 0x0

    :try_start_5
    invoke-static {}, Lcom/google/android/gms/ads/internal/zzr;->zzbM()Lcom/google/android/gms/ads/internal/purchase/zzi;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/google/android/gms/ads/internal/purchase/zzi;->zzd(Landroid/content/Intent;)I

    move-result v3

    const/4 v0, -0x1

    if-ne p2, v0, :cond_22

    invoke-static {}, Lcom/google/android/gms/ads/internal/zzr;->zzbM()Lcom/google/android/gms/ads/internal/purchase/zzi;

    const/4 v0, 0x0

    if-ne v3, v0, :cond_22

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/purchase/zze;->zzFF:Lcom/google/android/gms/ads/internal/purchase/zzk;

    iget-object v1, p0, Lcom/google/android/gms/ads/internal/purchase/zze;->zzFO:Ljava/lang/String;

    invoke-virtual {v0, v1, p2, p3}, Lcom/google/android/gms/ads/internal/purchase/zzk;->zza(Ljava/lang/String;ILandroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_29

    const/4 v2, 0x1

    goto :goto_29

    :cond_22
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/purchase/zze;->zzFD:Lcom/google/android/gms/ads/internal/purchase/zzh;

    iget-object v1, p0, Lcom/google/android/gms/ads/internal/purchase/zze;->zzFM:Lcom/google/android/gms/ads/internal/purchase/zzf;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/ads/internal/purchase/zzh;->zza(Lcom/google/android/gms/ads/internal/purchase/zzf;)V

    :cond_29
    :goto_29
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/purchase/zze;->zzFL:Lcom/google/android/gms/internal/zzgc;

    invoke-interface {v0, v3}, Lcom/google/android/gms/internal/zzgc;->recordPlayBillingResolution(I)V

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/purchase/zze;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/purchase/zze;->zzFL:Lcom/google/android/gms/internal/zzgc;

    invoke-interface {v0}, Lcom/google/android/gms/internal/zzgc;->getProductId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, v2, p2, p3}, Lcom/google/android/gms/ads/internal/purchase/zze;->zza(Ljava/lang/String;ZILandroid/content/Intent;)V
    :try_end_3c
    .catch Landroid/os/RemoteException; {:try_start_5 .. :try_end_3c} :catch_40
    .catchall {:try_start_5 .. :try_end_3c} :catchall_4f

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/ads/internal/purchase/zze;->zzFO:Ljava/lang/String;

    goto :goto_54

    :catch_40
    move-exception v3

    const-string v0, "Fail to process purchase result."

    :try_start_43
    invoke-static {v0}, Lcom/google/android/gms/internal/zzin;->zzaK(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/purchase/zze;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V
    :try_end_4b
    .catchall {:try_start_43 .. :try_end_4b} :catchall_4f

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/ads/internal/purchase/zze;->zzFO:Ljava/lang/String;

    goto :goto_54

    :catchall_4f
    move-exception v4

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/ads/internal/purchase/zze;->zzFO:Ljava/lang/String;

    throw v4

    :cond_54
    :goto_54
    return-void
.end method

.method public onCreate()V
    .registers 6

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/purchase/zze;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/purchase/GInAppPurchaseManagerInfoParcel;->zzc(Landroid/content/Intent;)Lcom/google/android/gms/ads/internal/purchase/GInAppPurchaseManagerInfoParcel;

    move-result-object v2

    iget-object v0, v2, Lcom/google/android/gms/ads/internal/purchase/GInAppPurchaseManagerInfoParcel;->zzFy:Lcom/google/android/gms/ads/internal/purchase/zzj;

    iput-object v0, p0, Lcom/google/android/gms/ads/internal/purchase/zze;->zzFN:Lcom/google/android/gms/ads/internal/purchase/zzj;

    iget-object v0, v2, Lcom/google/android/gms/ads/internal/purchase/GInAppPurchaseManagerInfoParcel;->zzrI:Lcom/google/android/gms/ads/internal/purchase/zzk;

    iput-object v0, p0, Lcom/google/android/gms/ads/internal/purchase/zze;->zzFF:Lcom/google/android/gms/ads/internal/purchase/zzk;

    iget-object v0, v2, Lcom/google/android/gms/ads/internal/purchase/GInAppPurchaseManagerInfoParcel;->zzFw:Lcom/google/android/gms/internal/zzgc;

    iput-object v0, p0, Lcom/google/android/gms/ads/internal/purchase/zze;->zzFL:Lcom/google/android/gms/internal/zzgc;

    new-instance v0, Lcom/google/android/gms/ads/internal/purchase/zzb;

    iget-object v1, p0, Lcom/google/android/gms/ads/internal/purchase/zze;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/ads/internal/purchase/zzb;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/ads/internal/purchase/zze;->zzFC:Lcom/google/android/gms/ads/internal/purchase/zzb;

    iget-object v0, v2, Lcom/google/android/gms/ads/internal/purchase/GInAppPurchaseManagerInfoParcel;->zzFx:Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/gms/ads/internal/purchase/zze;->zzFK:Landroid/content/Context;

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/purchase/zze;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v3, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v0, 0x2

    if-ne v3, v0, :cond_44

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/purchase/zze;->mActivity:Landroid/app/Activity;

    invoke-static {}, Lcom/google/android/gms/ads/internal/zzr;->zzbE()Lcom/google/android/gms/internal/zzis;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/internal/zzis;->zzhv()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setRequestedOrientation(I)V

    goto :goto_51

    :cond_44
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/purchase/zze;->mActivity:Landroid/app/Activity;

    invoke-static {}, Lcom/google/android/gms/ads/internal/zzr;->zzbE()Lcom/google/android/gms/internal/zzis;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/internal/zzis;->zzhw()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setRequestedOrientation(I)V

    :goto_51
    new-instance v4, Landroid/content/Intent;

    const-string v0, "com.android.vending.billing.InAppBillingService.BIND"

    invoke-direct {v4, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v0, "com.android.vending"

    invoke-virtual {v4, v0}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/purchase/zze;->mActivity:Landroid/app/Activity;

    const/4 v1, 0x1

    invoke-virtual {v0, v4, p0, v1}, Landroid/app/Activity;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    return-void
.end method

.method public onDestroy()V
    .registers 2

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/purchase/zze;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0, p0}, Landroid/app/Activity;->unbindService(Landroid/content/ServiceConnection;)V

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/purchase/zze;->zzFC:Lcom/google/android/gms/ads/internal/purchase/zzb;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/internal/purchase/zzb;->destroy()V

    return-void
.end method

.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .registers 13
    .param p1, "name"    # Landroid/content/ComponentName;
    .param p2, "service"    # Landroid/os/IBinder;

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/purchase/zze;->zzFC:Lcom/google/android/gms/ads/internal/purchase/zzb;

    invoke-virtual {v0, p2}, Lcom/google/android/gms/ads/internal/purchase/zzb;->zzN(Landroid/os/IBinder;)V

    :try_start_5
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/purchase/zze;->zzFF:Lcom/google/android/gms/ads/internal/purchase/zzk;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/internal/purchase/zzk;->zzfZ()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/ads/internal/purchase/zze;->zzFO:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/purchase/zze;->zzFC:Lcom/google/android/gms/ads/internal/purchase/zzb;

    iget-object v1, p0, Lcom/google/android/gms/ads/internal/purchase/zze;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getPackageName()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/ads/internal/purchase/zze;->zzFL:Lcom/google/android/gms/internal/zzgc;

    invoke-interface {v2}, Lcom/google/android/gms/internal/zzgc;->getProductId()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/ads/internal/purchase/zze;->zzFO:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/ads/internal/purchase/zzb;->zzb(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v7

    const-string v0, "BUY_INTENT"

    invoke-virtual {v7, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Landroid/app/PendingIntent;

    if-nez v8, :cond_4a

    invoke-static {}, Lcom/google/android/gms/ads/internal/zzr;->zzbM()Lcom/google/android/gms/ads/internal/purchase/zzi;

    move-result-object v0

    invoke-virtual {v0, v7}, Lcom/google/android/gms/ads/internal/purchase/zzi;->zzd(Landroid/os/Bundle;)I

    move-result v9

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/purchase/zze;->zzFL:Lcom/google/android/gms/internal/zzgc;

    invoke-interface {v0, v9}, Lcom/google/android/gms/internal/zzgc;->recordPlayBillingResolution(I)V

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/purchase/zze;->zzFL:Lcom/google/android/gms/internal/zzgc;

    invoke-interface {v0}, Lcom/google/android/gms/internal/zzgc;->getProductId()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, v9, v2}, Lcom/google/android/gms/ads/internal/purchase/zze;->zza(Ljava/lang/String;ZILandroid/content/Intent;)V

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/purchase/zze;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto :goto_8b

    :cond_4a
    new-instance v0, Lcom/google/android/gms/ads/internal/purchase/zzf;

    iget-object v1, p0, Lcom/google/android/gms/ads/internal/purchase/zze;->zzFL:Lcom/google/android/gms/internal/zzgc;

    invoke-interface {v1}, Lcom/google/android/gms/internal/zzgc;->getProductId()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/ads/internal/purchase/zze;->zzFO:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/ads/internal/purchase/zzf;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/ads/internal/purchase/zze;->zzFM:Lcom/google/android/gms/ads/internal/purchase/zzf;

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/purchase/zze;->zzFD:Lcom/google/android/gms/ads/internal/purchase/zzh;

    iget-object v1, p0, Lcom/google/android/gms/ads/internal/purchase/zze;->zzFM:Lcom/google/android/gms/ads/internal/purchase/zzf;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/ads/internal/purchase/zzh;->zzb(Lcom/google/android/gms/ads/internal/purchase/zzf;)V

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/purchase/zze;->mActivity:Landroid/app/Activity;

    invoke-virtual {v8}, Landroid/app/PendingIntent;->getIntentSender()Landroid/content/IntentSender;

    move-result-object v1

    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v4

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v5

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v6

    const/16 v2, 0x3e9

    invoke-virtual/range {v0 .. v6}, Landroid/app/Activity;->startIntentSenderForResult(Landroid/content/IntentSender;ILandroid/content/Intent;III)V
    :try_end_8b
    .catch Landroid/os/RemoteException; {:try_start_5 .. :try_end_8b} :catch_8c
    .catch Landroid/content/IntentSender$SendIntentException; {:try_start_5 .. :try_end_8b} :catch_8c

    :goto_8b
    goto :goto_97

    :catch_8c
    move-exception v7

    const-string v0, "Error when connecting in-app billing service"

    invoke-static {v0, v7}, Lcom/google/android/gms/internal/zzin;->zzd(Ljava/lang/String;Ljava/lang/Throwable;)V

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/purchase/zze;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    :goto_97
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .registers 3
    .param p1, "name"    # Landroid/content/ComponentName;

    const-string v0, "In-app billing service disconnected."

    invoke-static {v0}, Lcom/google/android/gms/internal/zzin;->zzaJ(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/purchase/zze;->zzFC:Lcom/google/android/gms/ads/internal/purchase/zzb;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/internal/purchase/zzb;->destroy()V

    return-void
.end method

.method protected zza(Ljava/lang/String;ZILandroid/content/Intent;)V
    .registers 11

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/purchase/zze;->zzFN:Lcom/google/android/gms/ads/internal/purchase/zzj;

    if-eqz v0, :cond_f

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/purchase/zze;->zzFN:Lcom/google/android/gms/ads/internal/purchase/zzj;

    move-object v1, p1

    move v2, p2

    move v3, p3

    move-object v4, p4

    iget-object v5, p0, Lcom/google/android/gms/ads/internal/purchase/zze;->zzFM:Lcom/google/android/gms/ads/internal/purchase/zzf;

    invoke-interface/range {v0 .. v5}, Lcom/google/android/gms/ads/internal/purchase/zzj;->zza(Ljava/lang/String;ZILandroid/content/Intent;Lcom/google/android/gms/ads/internal/purchase/zzf;)V

    :cond_f
    return-void
.end method

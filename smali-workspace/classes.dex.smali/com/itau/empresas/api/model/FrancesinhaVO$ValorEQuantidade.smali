.class public Lcom/itau/empresas/api/model/FrancesinhaVO$ValorEQuantidade;
.super Ljava/lang/Object;
.source "FrancesinhaVO.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/itau/empresas/api/model/FrancesinhaVO;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "ValorEQuantidade"
.end annotation


# instance fields
.field private quantidadeTitulos:I
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "quantidade_titulos"
    .end annotation
.end field

.field private valorTitulos:Ljava/lang/Float;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "valor_titulos"
    .end annotation
.end field


# virtual methods
.method public getQuantidadeTitulos()I
    .registers 2

    .line 79
    iget v0, p0, Lcom/itau/empresas/api/model/FrancesinhaVO$ValorEQuantidade;->quantidadeTitulos:I

    return v0
.end method

.method public getValorTitulos()Ljava/lang/Float;
    .registers 2

    .line 75
    iget-object v0, p0, Lcom/itau/empresas/api/model/FrancesinhaVO$ValorEQuantidade;->valorTitulos:Ljava/lang/Float;

    return-object v0
.end method

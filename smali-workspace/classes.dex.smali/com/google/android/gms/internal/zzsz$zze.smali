.class public final Lcom/google/android/gms/internal/zzsz$zze;
.super Lcom/google/android/gms/internal/zzso;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/internal/zzsz;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "zze"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/gms/internal/zzso<Lcom/google/android/gms/internal/zzsz$zze;>;"
    }
.end annotation


# static fields
.field private static volatile zzbvj:[Lcom/google/android/gms/internal/zzsz$zze;


# instance fields
.field public key:Ljava/lang/String;

.field public value:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .registers 2

    invoke-direct {p0}, Lcom/google/android/gms/internal/zzso;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/gms/internal/zzsz$zze;->zzJH()Lcom/google/android/gms/internal/zzsz$zze;

    return-void
.end method

.method public static zzJG()[Lcom/google/android/gms/internal/zzsz$zze;
    .registers 3

    sget-object v0, Lcom/google/android/gms/internal/zzsz$zze;->zzbvj:[Lcom/google/android/gms/internal/zzsz$zze;

    if-nez v0, :cond_15

    sget-object v1, Lcom/google/android/gms/internal/zzss;->zzbut:Ljava/lang/Object;

    monitor-enter v1

    :try_start_7
    sget-object v0, Lcom/google/android/gms/internal/zzsz$zze;->zzbvj:[Lcom/google/android/gms/internal/zzsz$zze;

    if-nez v0, :cond_10

    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/gms/internal/zzsz$zze;

    sput-object v0, Lcom/google/android/gms/internal/zzsz$zze;->zzbvj:[Lcom/google/android/gms/internal/zzsz$zze;
    :try_end_10
    .catchall {:try_start_7 .. :try_end_10} :catchall_12

    :cond_10
    monitor-exit v1

    goto :goto_15

    :catchall_12
    move-exception v2

    monitor-exit v1

    throw v2

    :cond_15
    :goto_15
    sget-object v0, Lcom/google/android/gms/internal/zzsz$zze;->zzbvj:[Lcom/google/android/gms/internal/zzsz$zze;

    return-object v0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .registers 5
    .param p1, "o"    # Ljava/lang/Object;

    if-ne p1, p0, :cond_4

    const/4 v0, 0x1

    return v0

    :cond_4
    instance-of v0, p1, Lcom/google/android/gms/internal/zzsz$zze;

    if-nez v0, :cond_a

    const/4 v0, 0x0

    return v0

    :cond_a
    move-object v2, p1

    check-cast v2, Lcom/google/android/gms/internal/zzsz$zze;

    iget-object v0, p0, Lcom/google/android/gms/internal/zzsz$zze;->key:Ljava/lang/String;

    if-nez v0, :cond_17

    iget-object v0, v2, Lcom/google/android/gms/internal/zzsz$zze;->key:Ljava/lang/String;

    if-eqz v0, :cond_23

    const/4 v0, 0x0

    return v0

    :cond_17
    iget-object v0, p0, Lcom/google/android/gms/internal/zzsz$zze;->key:Ljava/lang/String;

    iget-object v1, v2, Lcom/google/android/gms/internal/zzsz$zze;->key:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_23

    const/4 v0, 0x0

    return v0

    :cond_23
    iget-object v0, p0, Lcom/google/android/gms/internal/zzsz$zze;->value:Ljava/lang/String;

    if-nez v0, :cond_2d

    iget-object v0, v2, Lcom/google/android/gms/internal/zzsz$zze;->value:Ljava/lang/String;

    if-eqz v0, :cond_39

    const/4 v0, 0x0

    return v0

    :cond_2d
    iget-object v0, p0, Lcom/google/android/gms/internal/zzsz$zze;->value:Ljava/lang/String;

    iget-object v1, v2, Lcom/google/android/gms/internal/zzsz$zze;->value:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_39

    const/4 v0, 0x0

    return v0

    :cond_39
    iget-object v0, p0, Lcom/google/android/gms/internal/zzsz$zze;->zzbuj:Lcom/google/android/gms/internal/zzsq;

    if-eqz v0, :cond_45

    iget-object v0, p0, Lcom/google/android/gms/internal/zzsz$zze;->zzbuj:Lcom/google/android/gms/internal/zzsq;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/zzsq;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_55

    :cond_45
    iget-object v0, v2, Lcom/google/android/gms/internal/zzsz$zze;->zzbuj:Lcom/google/android/gms/internal/zzsq;

    if-eqz v0, :cond_51

    iget-object v0, v2, Lcom/google/android/gms/internal/zzsz$zze;->zzbuj:Lcom/google/android/gms/internal/zzsq;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/zzsq;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_53

    :cond_51
    const/4 v0, 0x1

    goto :goto_54

    :cond_53
    const/4 v0, 0x0

    :goto_54
    return v0

    :cond_55
    iget-object v0, p0, Lcom/google/android/gms/internal/zzsz$zze;->zzbuj:Lcom/google/android/gms/internal/zzsq;

    iget-object v1, v2, Lcom/google/android/gms/internal/zzsz$zze;->zzbuj:Lcom/google/android/gms/internal/zzsq;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/internal/zzsq;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public hashCode()I
    .registers 4

    const/16 v2, 0x11

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v2, v0, 0x20f

    mul-int/lit8 v0, v2, 0x1f

    iget-object v1, p0, Lcom/google/android/gms/internal/zzsz$zze;->key:Ljava/lang/String;

    if-nez v1, :cond_18

    const/4 v1, 0x0

    goto :goto_1e

    :cond_18
    iget-object v1, p0, Lcom/google/android/gms/internal/zzsz$zze;->key:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    :goto_1e
    add-int v2, v0, v1

    mul-int/lit8 v0, v2, 0x1f

    iget-object v1, p0, Lcom/google/android/gms/internal/zzsz$zze;->value:Ljava/lang/String;

    if-nez v1, :cond_28

    const/4 v1, 0x0

    goto :goto_2e

    :cond_28
    iget-object v1, p0, Lcom/google/android/gms/internal/zzsz$zze;->value:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    :goto_2e
    add-int v2, v0, v1

    mul-int/lit8 v0, v2, 0x1f

    iget-object v1, p0, Lcom/google/android/gms/internal/zzsz$zze;->zzbuj:Lcom/google/android/gms/internal/zzsq;

    if-eqz v1, :cond_3e

    iget-object v1, p0, Lcom/google/android/gms/internal/zzsz$zze;->zzbuj:Lcom/google/android/gms/internal/zzsq;

    invoke-virtual {v1}, Lcom/google/android/gms/internal/zzsq;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_40

    :cond_3e
    const/4 v1, 0x0

    goto :goto_46

    :cond_40
    iget-object v1, p0, Lcom/google/android/gms/internal/zzsz$zze;->zzbuj:Lcom/google/android/gms/internal/zzsq;

    invoke-virtual {v1}, Lcom/google/android/gms/internal/zzsq;->hashCode()I

    move-result v1

    :goto_46
    add-int v2, v0, v1

    return v2
.end method

.method public synthetic mergeFrom(Lcom/google/android/gms/internal/zzsm;)Lcom/google/android/gms/internal/zzsu;
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/android/gms/internal/zzsz$zze;->zzW(Lcom/google/android/gms/internal/zzsm;)Lcom/google/android/gms/internal/zzsz$zze;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/android/gms/internal/zzsn;)V
    .registers 4
    .param p1, "output"    # Lcom/google/android/gms/internal/zzsn;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/gms/internal/zzsz$zze;->key:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_10

    iget-object v0, p0, Lcom/google/android/gms/internal/zzsz$zze;->key:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-virtual {p1, v1, v0}, Lcom/google/android/gms/internal/zzsn;->zzn(ILjava/lang/String;)V

    :cond_10
    iget-object v0, p0, Lcom/google/android/gms/internal/zzsz$zze;->value:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_20

    iget-object v0, p0, Lcom/google/android/gms/internal/zzsz$zze;->value:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-virtual {p1, v1, v0}, Lcom/google/android/gms/internal/zzsn;->zzn(ILjava/lang/String;)V

    :cond_20
    invoke-super {p0, p1}, Lcom/google/android/gms/internal/zzso;->writeTo(Lcom/google/android/gms/internal/zzsn;)V

    return-void
.end method

.method public zzJH()Lcom/google/android/gms/internal/zzsz$zze;
    .registers 2

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/internal/zzsz$zze;->key:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/internal/zzsz$zze;->value:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/internal/zzsz$zze;->zzbuj:Lcom/google/android/gms/internal/zzsq;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/internal/zzsz$zze;->zzbuu:I

    return-object p0
.end method

.method public zzW(Lcom/google/android/gms/internal/zzsm;)Lcom/google/android/gms/internal/zzsz$zze;
    .registers 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :goto_0
    invoke-virtual {p1}, Lcom/google/android/gms/internal/zzsm;->zzIX()I

    move-result v1

    sparse-switch v1, :sswitch_data_1e

    goto :goto_9

    :sswitch_8
    return-object p0

    :goto_9
    invoke-virtual {p0, p1, v1}, Lcom/google/android/gms/internal/zzsz$zze;->zza(Lcom/google/android/gms/internal/zzsm;I)Z

    move-result v0

    if-nez v0, :cond_1d

    return-object p0

    :sswitch_10
    invoke-virtual {p1}, Lcom/google/android/gms/internal/zzsm;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/internal/zzsz$zze;->key:Ljava/lang/String;

    goto :goto_1d

    :sswitch_17
    invoke-virtual {p1}, Lcom/google/android/gms/internal/zzsm;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/internal/zzsz$zze;->value:Ljava/lang/String;

    :cond_1d
    :goto_1d
    goto :goto_0

    :sswitch_data_1e
    .sparse-switch
        0x0 -> :sswitch_8
        0xa -> :sswitch_10
        0x12 -> :sswitch_17
    .end sparse-switch
.end method

.method protected zzz()I
    .registers 4

    invoke-super {p0}, Lcom/google/android/gms/internal/zzso;->zzz()I

    move-result v2

    iget-object v0, p0, Lcom/google/android/gms/internal/zzsz$zze;->key:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_16

    iget-object v0, p0, Lcom/google/android/gms/internal/zzsz$zze;->key:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-static {v1, v0}, Lcom/google/android/gms/internal/zzsn;->zzo(ILjava/lang/String;)I

    move-result v0

    add-int/2addr v2, v0

    :cond_16
    iget-object v0, p0, Lcom/google/android/gms/internal/zzsz$zze;->value:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_28

    iget-object v0, p0, Lcom/google/android/gms/internal/zzsz$zze;->value:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-static {v1, v0}, Lcom/google/android/gms/internal/zzsn;->zzo(ILjava/lang/String;)I

    move-result v0

    add-int/2addr v2, v0

    :cond_28
    return v2
.end method

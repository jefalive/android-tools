.class Lokio/AsyncTimeout$1;
.super Ljava/lang/Object;
.source "AsyncTimeout.java"

# interfaces
.implements Lokio/Sink;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lokio/AsyncTimeout;->sink(Lokio/Sink;)Lokio/Sink;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lokio/AsyncTimeout;

.field final synthetic val$sink:Lokio/Sink;


# direct methods
.method constructor <init>(Lokio/AsyncTimeout;Lokio/Sink;)V
    .registers 3
    .param p1, "this$0"    # Lokio/AsyncTimeout;

    .line 160
    iput-object p1, p0, Lokio/AsyncTimeout$1;->this$0:Lokio/AsyncTimeout;

    iput-object p2, p0, Lokio/AsyncTimeout$1;->val$sink:Lokio/Sink;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public close()V
    .registers 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 205
    const/4 v1, 0x0

    .line 206
    .local v1, "throwOnTimeout":Z
    iget-object v0, p0, Lokio/AsyncTimeout$1;->this$0:Lokio/AsyncTimeout;

    invoke-virtual {v0}, Lokio/AsyncTimeout;->enter()V

    .line 208
    :try_start_6
    iget-object v0, p0, Lokio/AsyncTimeout$1;->val$sink:Lokio/Sink;

    invoke-interface {v0}, Lokio/Sink;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_b} :catch_12
    .catchall {:try_start_6 .. :try_end_b} :catchall_1a

    .line 209
    const/4 v1, 0x1

    .line 213
    iget-object v0, p0, Lokio/AsyncTimeout$1;->this$0:Lokio/AsyncTimeout;

    invoke-virtual {v0, v1}, Lokio/AsyncTimeout;->exit(Z)V

    .line 214
    goto :goto_21

    .line 210
    :catch_12
    move-exception v2

    .line 211
    .local v2, "e":Ljava/io/IOException;
    :try_start_13
    iget-object v0, p0, Lokio/AsyncTimeout$1;->this$0:Lokio/AsyncTimeout;

    invoke-virtual {v0, v2}, Lokio/AsyncTimeout;->exit(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    throw v0
    :try_end_1a
    .catchall {:try_start_13 .. :try_end_1a} :catchall_1a

    .line 213
    .end local v2    # "e":Ljava/io/IOException;
    :catchall_1a
    move-exception v3

    iget-object v0, p0, Lokio/AsyncTimeout$1;->this$0:Lokio/AsyncTimeout;

    invoke-virtual {v0, v1}, Lokio/AsyncTimeout;->exit(Z)V

    throw v3

    .line 215
    :goto_21
    return-void
.end method

.method public flush()V
    .registers 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 192
    const/4 v1, 0x0

    .line 193
    .local v1, "throwOnTimeout":Z
    iget-object v0, p0, Lokio/AsyncTimeout$1;->this$0:Lokio/AsyncTimeout;

    invoke-virtual {v0}, Lokio/AsyncTimeout;->enter()V

    .line 195
    :try_start_6
    iget-object v0, p0, Lokio/AsyncTimeout$1;->val$sink:Lokio/Sink;

    invoke-interface {v0}, Lokio/Sink;->flush()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_b} :catch_12
    .catchall {:try_start_6 .. :try_end_b} :catchall_1a

    .line 196
    const/4 v1, 0x1

    .line 200
    iget-object v0, p0, Lokio/AsyncTimeout$1;->this$0:Lokio/AsyncTimeout;

    invoke-virtual {v0, v1}, Lokio/AsyncTimeout;->exit(Z)V

    .line 201
    goto :goto_21

    .line 197
    :catch_12
    move-exception v2

    .line 198
    .local v2, "e":Ljava/io/IOException;
    :try_start_13
    iget-object v0, p0, Lokio/AsyncTimeout$1;->this$0:Lokio/AsyncTimeout;

    invoke-virtual {v0, v2}, Lokio/AsyncTimeout;->exit(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    throw v0
    :try_end_1a
    .catchall {:try_start_13 .. :try_end_1a} :catchall_1a

    .line 200
    .end local v2    # "e":Ljava/io/IOException;
    :catchall_1a
    move-exception v3

    iget-object v0, p0, Lokio/AsyncTimeout$1;->this$0:Lokio/AsyncTimeout;

    invoke-virtual {v0, v1}, Lokio/AsyncTimeout;->exit(Z)V

    throw v3

    .line 202
    :goto_21
    return-void
.end method

.method public timeout()Lokio/Timeout;
    .registers 2

    .line 218
    iget-object v0, p0, Lokio/AsyncTimeout$1;->this$0:Lokio/AsyncTimeout;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    .line 222
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "AsyncTimeout.sink("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lokio/AsyncTimeout$1;->val$sink:Lokio/Sink;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public write(Lokio/Buffer;J)V
    .registers 15
    .param p1, "source"    # Lokio/Buffer;
    .param p2, "byteCount"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 162
    iget-wide v0, p1, Lokio/Buffer;->size:J

    move-wide v4, p2

    const-wide/16 v2, 0x0

    invoke-static/range {v0 .. v5}, Lokio/Util;->checkOffsetAndCount(JJJ)V

    .line 164
    :goto_8
    const-wide/16 v0, 0x0

    cmp-long v0, p2, v0

    if-lez v0, :cond_52

    .line 166
    const-wide/16 v6, 0x0

    .line 167
    .local v6, "toWrite":J
    iget-object v8, p1, Lokio/Buffer;->head:Lokio/Segment;

    .local v8, "s":Lokio/Segment;
    :goto_12
    const-wide/32 v0, 0x10000

    cmp-long v0, v6, v0

    if-gez v0, :cond_2e

    .line 168
    iget-object v0, p1, Lokio/Buffer;->head:Lokio/Segment;

    iget v0, v0, Lokio/Segment;->limit:I

    iget-object v1, p1, Lokio/Buffer;->head:Lokio/Segment;

    iget v1, v1, Lokio/Segment;->pos:I

    sub-int v9, v0, v1

    .line 169
    .local v9, "segmentSize":I
    int-to-long v0, v9

    add-long/2addr v6, v0

    .line 170
    cmp-long v0, v6, p2

    if-ltz v0, :cond_2b

    .line 171
    move-wide v6, p2

    .line 172
    goto :goto_2e

    .line 167
    .end local v9    # "segmentSize":I
    :cond_2b
    iget-object v8, v8, Lokio/Segment;->next:Lokio/Segment;

    goto :goto_12

    .line 177
    .end local v8    # "s":Lokio/Segment;
    :cond_2e
    :goto_2e
    const/4 v8, 0x0

    .line 178
    .local v8, "throwOnTimeout":Z
    iget-object v0, p0, Lokio/AsyncTimeout$1;->this$0:Lokio/AsyncTimeout;

    invoke-virtual {v0}, Lokio/AsyncTimeout;->enter()V

    .line 180
    :try_start_34
    iget-object v0, p0, Lokio/AsyncTimeout$1;->val$sink:Lokio/Sink;

    invoke-interface {v0, p1, v6, v7}, Lokio/Sink;->write(Lokio/Buffer;J)V
    :try_end_39
    .catch Ljava/io/IOException; {:try_start_34 .. :try_end_39} :catch_41
    .catchall {:try_start_34 .. :try_end_39} :catchall_49

    .line 181
    sub-long/2addr p2, v6

    .line 182
    const/4 v8, 0x1

    .line 186
    iget-object v0, p0, Lokio/AsyncTimeout$1;->this$0:Lokio/AsyncTimeout;

    invoke-virtual {v0, v8}, Lokio/AsyncTimeout;->exit(Z)V

    .line 187
    goto :goto_50

    .line 183
    :catch_41
    move-exception v9

    .line 184
    .local v9, "e":Ljava/io/IOException;
    :try_start_42
    iget-object v0, p0, Lokio/AsyncTimeout$1;->this$0:Lokio/AsyncTimeout;

    invoke-virtual {v0, v9}, Lokio/AsyncTimeout;->exit(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    throw v0
    :try_end_49
    .catchall {:try_start_42 .. :try_end_49} :catchall_49

    .line 186
    .end local v9    # "e":Ljava/io/IOException;
    :catchall_49
    move-exception v10

    iget-object v0, p0, Lokio/AsyncTimeout$1;->this$0:Lokio/AsyncTimeout;

    invoke-virtual {v0, v8}, Lokio/AsyncTimeout;->exit(Z)V

    throw v10

    .line 188
    .end local v6    # "toWrite":J
    .end local v8    # "throwOnTimeout":Z
    :goto_50
    goto/16 :goto_8

    .line 189
    :cond_52
    return-void
.end method

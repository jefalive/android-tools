.class public Lbr/com/itau/textovoz/model/Link;
.super Ljava/lang/Object;
.source "Link.java"

# interfaces
.implements Landroid/os/Parcelable;
.implements Ljava/io/Serializable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<Lbr/com/itau/textovoz/model/Link;>;"
        }
    .end annotation
.end field


# instance fields
.field _ref:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "_ref"
    .end annotation
.end field

.field text:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "texto"
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .line 42
    new-instance v0, Lbr/com/itau/textovoz/model/Link$1;

    invoke-direct {v0}, Lbr/com/itau/textovoz/model/Link$1;-><init>()V

    sput-object v0, Lbr/com/itau/textovoz/model/Link;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method protected constructor <init>(Landroid/os/Parcel;)V
    .registers 3
    .param p1, "in"    # Landroid/os/Parcel;

    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/textovoz/model/Link;->text:Ljava/lang/String;

    .line 39
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/textovoz/model/Link;->_ref:Ljava/lang/String;

    .line 40
    return-void
.end method


# virtual methods
.method public describeContents()I
    .registers 2

    .line 34
    const/4 v0, 0x0

    return v0
.end method

.method public getText()Ljava/lang/String;
    .registers 2

    .line 19
    iget-object v0, p0, Lbr/com/itau/textovoz/model/Link;->text:Ljava/lang/String;

    return-object v0
.end method

.method public get_ref()Ljava/lang/String;
    .registers 2

    .line 23
    iget-object v0, p0, Lbr/com/itau/textovoz/model/Link;->_ref:Ljava/lang/String;

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 4
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .line 28
    iget-object v0, p0, Lbr/com/itau/textovoz/model/Link;->text:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 29
    iget-object v0, p0, Lbr/com/itau/textovoz/model/Link;->_ref:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 30
    return-void
.end method

.class public final Lrc/c;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static a:Ljava/lang/String;

.field private static final b:Ljava/security/SecureRandom;

.field private static c:Ljava/lang/String;

.field private static d:Ljava/lang/String;

.field private static e:Ljava/lang/String;

.field private static f:Ljava/lang/String;

.field private static g:Ljava/lang/String;

.field private static m:B

.field private static o:Ljava/lang/String;

.field private static p:Ljava/lang/String;

.field private static q:Ljava/lang/String;

.field private static r:Ljava/lang/String;

.field private static s:Ljava/lang/String;

.field private static t:Ljava/util/Queue;

.field private static final u:[B

.field private static v:I


# direct methods
.method static constructor <clinit>()V
    .registers 4

    .line 30
    const/16 v0, 0x8a

    new-array v0, v0, [B

    fill-array-data v0, :array_128

    sput-object v0, Lrc/c;->u:[B

    const/16 v0, 0xff

    sput v0, Lrc/c;->v:I

    sget-object v0, Lrc/c;->u:[B

    const/16 v1, 0x29

    aget-byte v0, v0, v1

    int-to-byte v0, v0

    sget v1, Lrc/c;->v:I

    and-int/lit16 v1, v1, 0x154

    int-to-byte v1, v1

    sget-object v2, Lrc/c;->u:[B

    const/16 v3, 0x47

    aget-byte v2, v2, v3

    neg-int v2, v2

    int-to-short v2, v2

    invoke-static {v0, v1, v2}, Lrc/c;->a(BBI)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lrc/c;->a:Ljava/lang/String;

    .line 31
    new-instance v0, Ljava/security/SecureRandom;

    invoke-direct {v0}, Ljava/security/SecureRandom;-><init>()V

    sput-object v0, Lrc/c;->b:Ljava/security/SecureRandom;

    .line 33
    sget-object v0, Lrc/c;->u:[B

    const/16 v1, 0x19

    aget-byte v0, v0, v1

    int-to-byte v0, v0

    sget v1, Lrc/c;->v:I

    and-int/lit8 v1, v1, 0x11

    int-to-byte v1, v1

    sget v2, Lrc/c;->v:I

    and-int/lit16 v2, v2, 0x160

    int-to-short v2, v2

    invoke-static {v0, v1, v2}, Lrc/c;->a(BBI)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lrc/c;->c:Ljava/lang/String;

    .line 34
    sget-object v0, Lrc/c;->u:[B

    const/16 v1, 0xa

    aget-byte v0, v0, v1

    int-to-byte v0, v0

    sget-object v1, Lrc/c;->u:[B

    const/16 v2, 0x55

    aget-byte v1, v1, v2

    int-to-byte v1, v1

    sget-object v2, Lrc/c;->u:[B

    const/16 v3, 0x46

    aget-byte v2, v2, v3

    int-to-short v2, v2

    invoke-static {v0, v1, v2}, Lrc/c;->a(BBI)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lrc/c;->d:Ljava/lang/String;

    .line 35
    sget-object v0, Lrc/c;->u:[B

    const/16 v1, 0x10

    aget-byte v0, v0, v1

    int-to-byte v0, v0

    sget-object v1, Lrc/c;->u:[B

    const/16 v2, 0x49

    aget-byte v1, v1, v2

    int-to-byte v1, v1

    sget v2, Lrc/c;->v:I

    and-int/lit16 v2, v2, 0x156

    int-to-short v2, v2

    invoke-static {v0, v1, v2}, Lrc/c;->a(BBI)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lrc/c;->e:Ljava/lang/String;

    .line 37
    sget-object v0, Lrc/c;->u:[B

    const/16 v1, 0x10

    aget-byte v0, v0, v1

    int-to-byte v0, v0

    or-int/lit8 v1, v0, 0x10

    int-to-byte v1, v1

    or-int/lit8 v2, v1, 0x46

    int-to-short v2, v2

    invoke-static {v0, v1, v2}, Lrc/c;->a(BBI)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lrc/c;->f:Ljava/lang/String;

    .line 38
    sget-object v0, Lrc/c;->u:[B

    const/16 v1, 0xa

    aget-byte v0, v0, v1

    int-to-byte v0, v0

    sget v1, Lrc/c;->v:I

    and-int/lit8 v1, v1, 0x10

    int-to-byte v1, v1

    sget-object v2, Lrc/c;->u:[B

    const/16 v3, 0x49

    aget-byte v2, v2, v3

    int-to-short v2, v2

    invoke-static {v0, v1, v2}, Lrc/c;->a(BBI)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lrc/c;->g:Ljava/lang/String;

    .line 49
    sget-object v0, Lrc/c;->u:[B

    const/16 v1, 0x10

    aget-byte v0, v0, v1

    int-to-byte v0, v0

    sget-object v1, Lrc/c;->u:[B

    const/16 v2, 0x48

    aget-byte v1, v1, v2

    int-to-byte v1, v1

    sget-object v2, Lrc/c;->u:[B

    const/4 v3, 0x4

    aget-byte v2, v2, v3

    int-to-short v2, v2

    invoke-static {v0, v1, v2}, Lrc/c;->a(BBI)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lrc/c;->o:Ljava/lang/String;

    .line 50
    sget-object v0, Lrc/c;->u:[B

    const/16 v1, 0x10

    aget-byte v0, v0, v1

    int-to-byte v0, v0

    or-int/lit8 v1, v0, 0x10

    int-to-byte v1, v1

    sget v2, Lrc/c;->v:I

    and-int/lit16 v2, v2, 0x158

    int-to-short v2, v2

    invoke-static {v0, v1, v2}, Lrc/c;->a(BBI)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lrc/c;->p:Ljava/lang/String;

    .line 51
    sget-object v0, Lrc/c;->u:[B

    const/16 v1, 0xa

    aget-byte v0, v0, v1

    int-to-byte v0, v0

    sget v1, Lrc/c;->v:I

    and-int/lit8 v1, v1, 0x10

    int-to-byte v1, v1

    sget-object v2, Lrc/c;->u:[B

    const/4 v3, 0x1

    aget-byte v2, v2, v3

    neg-int v2, v2

    int-to-short v2, v2

    invoke-static {v0, v1, v2}, Lrc/c;->a(BBI)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lrc/c;->q:Ljava/lang/String;

    .line 52
    sget-object v0, Lrc/c;->u:[B

    const/4 v1, 0x5

    aget-byte v0, v0, v1

    int-to-byte v0, v0

    sget-object v1, Lrc/c;->u:[B

    const/16 v2, 0x48

    aget-byte v1, v1, v2

    int-to-byte v1, v1

    sget v2, Lrc/c;->v:I

    and-int/lit16 v2, v2, 0x170

    int-to-short v2, v2

    invoke-static {v0, v1, v2}, Lrc/c;->a(BBI)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lrc/c;->r:Ljava/lang/String;

    .line 53
    sget-object v0, Lrc/c;->u:[B

    const/16 v1, 0x10

    aget-byte v0, v0, v1

    int-to-byte v0, v0

    sget-object v1, Lrc/c;->u:[B

    const/16 v2, 0x48

    aget-byte v1, v1, v2

    int-to-byte v1, v1

    sget v2, Lrc/c;->v:I

    and-int/lit16 v2, v2, 0x385

    int-to-short v2, v2

    invoke-static {v0, v1, v2}, Lrc/c;->a(BBI)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lrc/c;->s:Ljava/lang/String;

    .line 55
    new-instance v0, Ljava/util/concurrent/LinkedBlockingQueue;

    const/16 v1, 0x14

    invoke-direct {v0, v1}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>(I)V

    sput-object v0, Lrc/c;->t:Ljava/util/Queue;

    return-void

    :array_128
    .array-data 1
        0x51t
        -0x17t
        0x48t
        0x1dt
        0xat
        0x0t
        0x0t
        -0xat
        -0x33t
        0xbt
        0x2t
        0x26t
        -0x18t
        -0x19t
        0x4t
        0x0t
        0x1t
        -0x1t
        0x4et
        -0x4et
        0x4et
        0x1t
        -0x1t
        0x1t
        0x2t
        0x3t
        0x1t
        0x1t
        0x5t
        0x1bt
        -0x3t
        -0x1t
        -0x7t
        -0x47t
        0x4ct
        -0x3t
        -0x6t
        0x2t
        0x9t
        0x5t
        -0xet
        0x1ft
        0x3t
        -0x44t
        0x2at
        0x9t
        -0x4t
        -0x1t
        0x1ft
        0x3t
        -0x44t
        0x2at
        0x9t
        0x1ct
        -0x1t
        0x0t
        -0x9t
        -0x1at
        -0x26t
        0x1at
        0x4t
        0x4t
        -0x1ct
        -0x3t
        0x1ct
        0x6t
        -0x2ct
        -0x17t
        0x6t
        0x1dt
        0x15t
        -0x36t
        0x14t
        0x13t
        0xdt
        -0x1et
        0x27t
        -0x2dt
        0x26t
        -0x1ft
        0x2t
        0x21t
        -0x22t
        -0x9t
        0x23t
        0x12t
        0x4t
        -0x48t
        0x3at
        -0x1t
        -0x1t
        0x2t
        0x5t
        0x33t
        -0xft
        0x0t
        -0x1t
        0xdt
        -0x38t
        -0x1at
        -0x1t
        0x7t
        0x0t
        -0xet
        -0x2t
        0x12t
        -0x3t
        -0x9t
        0xbt
        0x5t
        -0x35t
        0x1dt
        0x13t
        -0x13t
        0xdt
        -0xdt
        0x0t
        0x0t
        0x1t
        -0x1t
        0x2ct
        -0x1t
        0x6t
        -0xft
        0x9t
        0x6t
        -0x47t
        0x16t
        0x1et
        0x2t
        0x5t
        -0x3t
        0x0t
        0x0t
        0x0t
        0x1t
        -0x1t
        -0x3t
    .end array-data
.end method

.method constructor <init>()V
    .registers 1

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(J)J
    .registers 9

    .line 308
    sget-object v0, Lrc/c;->u:[B

    const/16 v1, 0xa

    aget-byte v0, v0, v1

    int-to-byte v0, v0

    sget v1, Lrc/c;->v:I

    and-int/lit8 v1, v1, 0x10

    int-to-byte v1, v1

    add-int/lit8 v2, v1, 0x1

    int-to-short v2, v2

    invoke-static {v0, v1, v2}, Lrc/c;->a(BBI)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    int-to-long v0, v0

    rem-long v0, p0, v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    .line 309
    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    sub-long v0, p0, v0

    sget-object v2, Lrc/c;->u:[B

    const/16 v3, 0x19

    aget-byte v2, v2, v3

    int-to-byte v2, v2

    sget v3, Lrc/c;->v:I

    and-int/lit8 v3, v3, 0x10

    int-to-byte v3, v3

    sget-object v4, Lrc/c;->u:[B

    const/16 v5, 0x9

    aget-byte v4, v4, v5

    int-to-short v4, v4

    invoke-static {v2, v3, v4}, Lrc/c;->a(BBI)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    int-to-long v2, v2

    div-long/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p0

    .line 311
    invoke-virtual {p0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_54

    .line 312
    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    return-wide v0

    .line 315
    :cond_54
    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v2, v3}, Lrc/c;->a(J)J

    move-result-wide v2

    add-long/2addr v0, v2

    return-wide v0
.end method

.method private static a(BBI)Ljava/lang/String;
    .registers 9

    new-instance v0, Ljava/lang/String;

    add-int/lit8 p0, p0, 0x1

    add-int/lit8 p1, p1, 0x20

    add-int/lit8 p2, p2, 0x4

    sget-object v4, Lrc/c;->u:[B

    const/4 v5, 0x0

    new-array v1, p0, [B

    add-int/lit8 p0, p0, -0x1

    if-nez v4, :cond_17

    move v2, p2

    move v3, p1

    :goto_13
    add-int p1, v2, v3

    add-int/lit8 p2, p2, 0x1

    :cond_17
    int-to-byte v2, p1

    aput-byte v2, v1, v5

    if-ne v5, p0, :cond_25

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Ljava/lang/String;-><init>([BI)V

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_25
    move v2, p1

    add-int/lit8 v5, v5, 0x1

    aget-byte v3, v4, p2

    goto :goto_13
.end method

.method private static a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;
    .registers 6

    .line 330
    sget-object v0, Lrc/c;->u:[B

    const/16 v1, 0xa

    aget-byte v0, v0, v1

    int-to-byte v0, v0

    sget v1, Lrc/c;->v:I

    and-int/lit8 v1, v1, 0x3c

    int-to-byte v1, v1

    and-int/lit8 v2, v1, 0xa

    int-to-short v2, v2

    invoke-static {v0, v1, v2}, Lrc/c;->a(BBI)Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 332
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v1, 0x12

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    const/4 v1, 0x0

    invoke-virtual {v3, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 334
    const/16 v1, 0xa

    invoke-static {v0, v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;I)J

    move-result-wide v0

    .line 335
    invoke-static {v0, v1}, Lrc/c;->b(J)I

    move-result v3

    .line 336
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lrc/c;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    aget-object v1, p1, v3

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a([BLrc/j;B)Ljava/lang/String;
    .registers 18

    .line 58
    sput-byte p2, Lrc/c;->m:B

    .line 60
    sget-object v0, Lrc/c;->p:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result p2

    .line 61
    sget-object v0, Lrc/c;->c:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    .line 62
    sget-object v0, Lrc/c;->d:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    .line 63
    sget-object v0, Lrc/c;->f:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    .line 64
    sget-object v0, Lrc/c;->g:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    .line 1344
    new-instance v0, Ljava/math/BigInteger;

    sget-object v1, Lrc/c;->b:Ljava/security/SecureRandom;

    const/16 v2, 0x82

    invoke-direct {v0, v2, v1}, Ljava/math/BigInteger;-><init>(ILjava/util/Random;)V

    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Ljava/math/BigInteger;->toString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    move/from16 v2, p2

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v8

    .line 66
    .line 67
    sget-object v0, Lrc/c;->q:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v9

    .line 2344
    new-instance v0, Ljava/math/BigInteger;

    sget-object v1, Lrc/c;->b:Ljava/security/SecureRandom;

    const/16 v2, 0x82

    invoke-direct {v0, v2, v1}, Ljava/math/BigInteger;-><init>(ILjava/util/Random;)V

    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Ljava/math/BigInteger;->toString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v9

    .line 67
    .line 3340
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    .line 69
    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v10

    .line 70
    sget-object v0, Lrc/c;->u:[B

    const/16 v1, 0xa

    aget-byte v0, v0, v1

    int-to-byte v0, v0

    sget v1, Lrc/c;->v:I

    and-int/lit8 v1, v1, 0x3c

    int-to-byte v1, v1

    and-int/lit8 v2, v1, 0xa

    int-to-short v2, v2

    invoke-static {v0, v1, v2}, Lrc/c;->a(BBI)Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v10, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 71
    sget-object v0, Lrc/c;->u:[B

    const/4 v1, 0x5

    aget-byte v0, v0, v1

    int-to-byte v0, v0

    or-int/lit8 v1, v0, 0x10

    int-to-byte v1, v1

    sget v2, Lrc/c;->v:I

    and-int/lit16 v2, v2, 0x16e

    int-to-short v2, v2

    invoke-static {v0, v1, v2}, Lrc/c;->a(BBI)Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v11, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 73
    sget-object v0, Lrc/c;->u:[B

    const/16 v1, 0x10

    aget-byte v0, v0, v1

    int-to-byte v0, v0

    or-int/lit8 v1, v0, 0x10

    int-to-byte v1, v1

    or-int/lit8 v2, v1, 0x44

    int-to-short v2, v2

    invoke-static {v0, v1, v2}, Lrc/c;->a(BBI)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v13

    .line 74
    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v0

    rem-int v11, v0, v13

    .line 75
    invoke-virtual {v12}, Ljava/lang/String;->length()I

    move-result v0

    rem-int v12, v0, v13

    .line 77
    invoke-interface/range {p1 .. p1}, Lrc/e;->a()[Ljava/lang/String;

    move-result-object v13

    .line 78
    invoke-interface/range {p1 .. p1}, Lrc/e;->b()I

    move-result p1

    .line 80
    aget-object v0, v13, v11

    invoke-static {v0}, Lbr/com/itau/security/commons/Crypto;->sha256(Ljava/lang/String;)[B

    move-result-object v14

    .line 81
    aget-object v0, v13, v12

    invoke-static {v0}, Lbr/com/itau/security/commons/Crypto;->sha256(Ljava/lang/String;)[B

    move-result-object v0

    const/4 v1, 0x0

    move/from16 v2, p2

    invoke-static {v0, v1, v2}, Ljava/util/Arrays;->copyOfRange([BII)[B

    move-result-object p2

    .line 83
    invoke-virtual {v10}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    move-object/from16 v1, p2

    invoke-static {v0, v14, v1}, Lbr/com/itau/security/commons/Crypto;->pkcs5PaddingEncrypt([B[B[B)[B

    move-result-object p2

    .line 84
    invoke-static {v10, v13}, Lrc/c;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 86
    sget-object v0, Lbr/com/itau/security/routercryptor/MobileCryptor;->a:Lrc/b;

    instance-of v0, v0, Lrc/h;

    if-eqz v0, :cond_e9

    .line 87
    invoke-static {p0}, Lrc/c;->b([B)[B

    move-result-object p0

    .line 89
    :cond_e9
    invoke-static {v10, v9, v4, v5}, Lbr/com/itau/security/commons/Crypto;->pbkdf2Encrypt(Ljava/lang/String;[BII)[B

    move-result-object v4

    .line 90
    invoke-static {p0, v4, v8}, Lbr/com/itau/security/commons/Crypto;->pkcs5PaddingEncrypt([B[B[B)[B

    move-result-object p0

    .line 91
    move/from16 v5, p1

    move-object v4, v8

    move-object/from16 p1, v9

    .line 4297
    const/4 v0, 0x5

    new-array v0, v0, [B

    const/4 v1, 0x1

    const/4 v2, 0x0

    aput-byte v1, v0, v2

    const/4 v1, 0x0

    const/4 v2, 0x1

    aput-byte v1, v0, v2

    const/4 v1, 0x0

    const/4 v2, 0x2

    aput-byte v1, v0, v2

    const/4 v1, 0x0

    const/4 v2, 0x3

    aput-byte v1, v0, v2

    sget-byte v1, Lrc/c;->m:B

    const/4 v2, 0x4

    aput-byte v1, v0, v2

    .line 4298
    move-object/from16 v1, p1

    invoke-static {v0, v1}, Lbr/com/itau/security/commons/binary/ByteUtils;->join([B[B)[B

    move-result-object p1

    .line 4299
    const/4 v0, 0x4

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-virtual {v0, v5}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v5

    .line 4300
    move-object/from16 v0, p1

    invoke-static {v0, v4}, Lbr/com/itau/security/commons/binary/ByteUtils;->join([B[B)[B

    move-result-object v0

    .line 4302
    invoke-static {v0, v5}, Lbr/com/itau/security/commons/binary/ByteUtils;->join([B[B)[B

    move-result-object v0

    .line 92
    invoke-static {v0, p0}, Lbr/com/itau/security/commons/binary/ByteUtils;->join([B[B)[B

    move-result-object v0

    .line 93
    move-object/from16 v1, p2

    invoke-static {v0, v1}, Lbr/com/itau/security/commons/binary/ByteUtils;->join([B[B)[B

    move-result-object v0

    .line 95
    invoke-static {v0}, Lbr/com/itau/security/commons/binary/ByteUtils;->b64Encode([B)Ljava/lang/String;

    move-result-object p0

    .line 96
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v1, 0x0

    invoke-virtual {p0, v1, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v11}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {p0, v6, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 97
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    sub-int/2addr v1, v7

    const/4 v2, 0x0

    invoke-virtual {p0, v2, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v12}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    sub-int/2addr v1, v7

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {p0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 99
    return-object v0
.end method

.method private static declared-synchronized a()Lrc/f;
    .registers 8

    const-class v7, Lrc/c;

    monitor-enter v7

    .line 278
    :try_start_3
    sget-object v0, Lrc/c;->t:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->size()I

    move-result v0

    .line 279
    move v6, v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_45

    .line 280
    new-instance v0, Lrc/f;

    sget-object v1, Lrc/c;->t:Ljava/util/Queue;

    invoke-interface {v1}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    sget-object v2, Lrc/c;->u:[B

    const/16 v3, 0xe

    aget-byte v2, v2, v3

    int-to-byte v2, v2

    sget-object v3, Lrc/c;->u:[B

    const/16 v4, 0x62

    aget-byte v3, v3, v4

    neg-int v3, v3

    int-to-byte v3, v3

    sget-object v4, Lrc/c;->u:[B

    const/16 v5, 0xe

    aget-byte v4, v4, v5

    int-to-short v4, v4

    invoke-static {v2, v3, v4}, Lrc/c;->a(BBI)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    const/4 v5, 0x0

    aput-object v4, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lrc/f;-><init>(Ljava/lang/String;Ljava/lang/String;B)V

    monitor-exit v7

    return-object v0

    .line 282
    :cond_45
    new-instance v0, Lrc/f;

    sget-object v1, Lrc/c;->t:Ljava/util/Queue;

    invoke-interface {v1}, Ljava/util/Queue;->peek()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    sget-object v2, Lrc/c;->u:[B

    const/16 v3, 0xe

    aget-byte v2, v2, v3

    int-to-byte v2, v2

    sget-object v3, Lrc/c;->u:[B

    const/16 v4, 0x62

    aget-byte v3, v3, v4

    neg-int v3, v3

    int-to-byte v3, v3

    sget-object v4, Lrc/c;->u:[B

    const/16 v5, 0xe

    aget-byte v4, v4, v5

    int-to-short v4, v4

    invoke-static {v2, v3, v4}, Lrc/c;->a(BBI)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    const/4 v5, 0x0

    aput-object v4, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lrc/f;-><init>(Ljava/lang/String;Ljava/lang/String;B)V
    :try_end_7b
    .catchall {:try_start_3 .. :try_end_7b} :catchall_7d

    monitor-exit v7

    return-object v0

    :catchall_7d
    move-exception v6

    monitor-exit v7

    throw v6
.end method

.method public static a(Ljava/lang/String;Lrc/j;)[B
    .registers 18

    .line 104
    sget-object v0, Lrc/c;->f:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    .line 105
    sget-object v0, Lrc/c;->g:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    .line 106
    sget-object v0, Lrc/c;->s:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    .line 107
    sget-object v0, Lrc/c;->e:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v8

    .line 108
    sget-object v0, Lrc/c;->d:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v9

    .line 109
    sget-object v0, Lrc/c;->c:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v10

    .line 110
    sget-object v0, Lrc/c;->r:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v11

    .line 111
    sget-object v0, Lrc/c;->o:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v12

    .line 113
    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Ljava/lang/String;->charAt(I)C

    move-result v0

    invoke-static {v0}, Ljava/lang/Character;->getNumericValue(C)I

    move-result v13

    .line 114
    invoke-virtual/range {p0 .. p0}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v1, v6, 0x1

    sub-int/2addr v0, v1

    move-object/from16 v1, p0

    invoke-virtual {v1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v0

    invoke-static {v0}, Ljava/lang/Character;->getNumericValue(C)I

    move-result v14

    .line 116
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v1, p0

    const/4 v2, 0x0

    invoke-virtual {v1, v2, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    add-int/lit8 v1, v5, 0x1

    invoke-virtual/range {p0 .. p0}, Ljava/lang/String;->length()I

    move-result v2

    move-object/from16 v3, p0

    invoke-virtual {v3, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 117
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p0 .. p0}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v2, v6, 0x1

    sub-int/2addr v1, v2

    move-object/from16 v2, p0

    const/4 v3, 0x0

    invoke-virtual {v2, v3, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual/range {p0 .. p0}, Ljava/lang/String;->length()I

    move-result v1

    sub-int/2addr v1, v6

    invoke-virtual/range {p0 .. p0}, Ljava/lang/String;->length()I

    move-result v2

    move-object/from16 v3, p0

    invoke-virtual {v3, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 119
    invoke-static {v0}, Lbr/com/itau/security/commons/binary/ByteUtils;->b64Decode(Ljava/lang/String;)[B

    move-result-object p0

    .line 121
    move-object/from16 v0, p0

    array-length v0, v0

    add-int v1, v7, v8

    if-le v0, v1, :cond_170

    .line 123
    new-array v5, v7, [B

    .line 124
    move-object/from16 v0, p0

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-static {v0, v1, v5, v2, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 126
    const/4 v0, 0x1

    new-array v6, v0, [B

    .line 127
    const/4 v0, 0x0

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-static {v5, v0, v6, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 129
    const/4 v0, 0x4

    new-array v6, v0, [B

    .line 130
    const/4 v0, 0x1

    const/4 v1, 0x0

    const/4 v2, 0x4

    invoke-static {v5, v0, v6, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 132
    sget-object v0, Lrc/c;->q:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    new-array v6, v0, [B

    .line 133
    array-length v0, v6

    const/4 v1, 0x5

    const/4 v2, 0x0

    invoke-static {v5, v1, v6, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 135
    sget-object v0, Lrc/c;->p:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    new-array v8, v0, [B

    .line 136
    array-length v0, v8

    sub-int v0, v7, v0

    sub-int/2addr v0, v11

    array-length v1, v8

    const/4 v2, 0x0

    invoke-static {v5, v0, v8, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 138
    new-array v15, v11, [B

    .line 139
    sub-int v0, v7, v11

    const/4 v1, 0x0

    invoke-static {v5, v0, v15, v1, v11}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 141
    invoke-static {v15}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 142
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v5

    .line 144
    new-array v11, v12, [B

    .line 145
    move-object/from16 v0, p0

    array-length v0, v0

    sub-int/2addr v0, v12

    move-object/from16 v1, p0

    const/4 v2, 0x0

    invoke-static {v1, v0, v11, v2, v12}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 147
    invoke-interface/range {p1 .. p1}, Lrc/e;->a()[Ljava/lang/String;

    move-result-object v15

    .line 148
    invoke-interface/range {p1 .. p1}, Lrc/e;->b()I

    move-result v0

    .line 150
    if-eq v0, v5, :cond_124

    .line 151
    new-instance v0, Lbr/com/itau/security/commons/exception/BadCryptoDataException;

    sget v1, Lrc/c;->v:I

    and-int/lit8 v1, v1, 0xc

    int-to-byte v1, v1

    sget v2, Lrc/c;->v:I

    and-int/lit8 v2, v2, 0x37

    int-to-byte v2, v2

    sget-object v3, Lrc/c;->u:[B

    const/16 v4, 0xd

    aget-byte v3, v3, v4

    neg-int v3, v3

    int-to-short v3, v3

    invoke-static {v1, v2, v3}, Lrc/c;->a(BBI)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lbr/com/itau/security/commons/exception/BadCryptoDataException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 153
    :cond_124
    aget-object v0, v15, v13

    invoke-static {v0}, Lbr/com/itau/security/commons/Crypto;->sha256(Ljava/lang/String;)[B

    move-result-object p1

    .line 154
    aget-object v0, v15, v14

    invoke-static {v0}, Lbr/com/itau/security/commons/Crypto;->sha256(Ljava/lang/String;)[B

    move-result-object v0

    sget-object v1, Lrc/c;->p:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    const/4 v2, 0x0

    invoke-static {v0, v2, v1}, Ljava/util/Arrays;->copyOfRange([BII)[B

    move-result-object v5

    .line 156
    move-object/from16 v0, p1

    invoke-static {v11, v0, v5}, Lbr/com/itau/security/commons/Crypto;->pkcs5PaddingDecrypt([B[B[B)[B

    move-result-object p1

    .line 157
    new-instance v0, Ljava/lang/String;

    move-object/from16 v1, p1

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>([B)V

    .line 159
    invoke-static {v0, v15}, Lrc/c;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 160
    invoke-static {v0, v6, v10, v9}, Lbr/com/itau/security/commons/Crypto;->pbkdf2Encrypt(Ljava/lang/String;[BII)[B

    move-result-object p1

    .line 162
    move-object/from16 v0, p0

    array-length v0, v0

    sub-int/2addr v0, v7

    sub-int/2addr v0, v12

    .line 164
    move v5, v0

    new-array v6, v0, [B

    .line 165
    move-object/from16 v0, p0

    const/4 v1, 0x0

    invoke-static {v0, v7, v6, v1, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 166
    move-object/from16 v0, p1

    invoke-static {v6, v0, v8}, Lbr/com/itau/security/commons/Crypto;->pkcs5PaddingDecrypt([B[B[B)[B

    move-result-object p0

    .line 168
    sget-object v0, Lbr/com/itau/security/routercryptor/MobileCryptor;->a:Lrc/b;

    instance-of v0, v0, Lrc/h;

    if-eqz v0, :cond_16f

    .line 169
    invoke-static/range {p0 .. p0}, Lrc/c;->a([B)[B

    move-result-object v0

    return-object v0

    .line 171
    :cond_16f
    return-object p0

    .line 174
    :cond_170
    const/4 v0, 0x0

    return-object v0
.end method

.method private static a([B)[B
    .registers 10

    .line 181
    :try_start_0
    new-instance v4, Lorg/json/JSONObject;

    invoke-static {p0}, Lbr/com/itau/security/commons/binary/ByteUtils;->to([B)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_9} :catch_a

    .line 186
    goto :goto_4c

    .line 182
    :catch_a
    move-exception v5

    .line 183
    sget v0, Lrc/c;->v:I

    and-int/lit8 v0, v0, 0xc

    int-to-byte v0, v0

    sget-object v1, Lrc/c;->u:[B

    const/16 v2, 0x54

    aget-byte v1, v1, v2

    int-to-byte v1, v1

    sget v2, Lrc/c;->v:I

    and-int/lit16 v2, v2, 0x174

    int-to-short v2, v2

    invoke-static {v0, v1, v2}, Lrc/c;->a(BBI)Ljava/lang/String;

    sget-object v0, Lrc/c;->u:[B

    const/16 v1, 0x65

    aget-byte v0, v0, v1

    int-to-byte v0, v0

    sget-object v1, Lrc/c;->u:[B

    const/16 v2, 0x52

    aget-byte v1, v1, v2

    neg-int v1, v1

    int-to-byte v1, v1

    add-int/lit8 v2, v1, 0x3

    int-to-short v2, v2

    invoke-static {v0, v1, v2}, Lrc/c;->a(BBI)Ljava/lang/String;

    .line 184
    sget v0, Lrc/c;->v:I

    and-int/lit8 v0, v0, 0xc

    int-to-byte v0, v0

    sget-object v1, Lrc/c;->u:[B

    const/16 v2, 0x54

    aget-byte v1, v1, v2

    int-to-byte v1, v1

    sget v2, Lrc/c;->v:I

    and-int/lit16 v2, v2, 0x174

    int-to-short v2, v2

    invoke-static {v0, v1, v2}, Lrc/c;->a(BBI)Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    .line 185
    return-object p0

    .line 188
    :goto_4c
    sget-object v0, Lrc/c;->u:[B

    const/16 v1, 0x9

    aget-byte v0, v0, v1

    int-to-byte v0, v0

    sget v1, Lrc/c;->v:I

    and-int/lit16 v1, v1, 0x153

    int-to-byte v1, v1

    sget v2, Lrc/c;->v:I

    and-int/lit16 v2, v2, 0x163

    int-to-short v2, v2

    invoke-static {v0, v1, v2}, Lrc/c;->a(BBI)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v5

    .line 190
    if-nez v5, :cond_68

    .line 191
    return-object p0

    .line 194
    :cond_68
    sget v0, Lrc/c;->v:I

    and-int/lit8 v0, v0, 0xc

    int-to-byte v0, v0

    sget-object v1, Lrc/c;->u:[B

    const/16 v2, 0x54

    aget-byte v1, v1, v2

    int-to-byte v1, v1

    sget v2, Lrc/c;->v:I

    and-int/lit16 v2, v2, 0x174

    int-to-short v2, v2

    invoke-static {v0, v1, v2}, Lrc/c;->a(BBI)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Lorg/json/JSONObject;->opt(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    .line 196
    sget v0, Lrc/c;->v:I

    and-int/lit8 v0, v0, 0xc

    int-to-byte v0, v0

    sget-object v1, Lrc/c;->u:[B

    const/16 v2, 0x54

    aget-byte v1, v1, v2

    int-to-byte v1, v1

    sget v2, Lrc/c;->v:I

    and-int/lit16 v2, v2, 0x174

    int-to-short v2, v2

    invoke-static {v0, v1, v2}, Lrc/c;->a(BBI)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Lorg/json/JSONObject;->remove(Ljava/lang/String;)Ljava/lang/Object;

    .line 199
    :try_start_99
    sget-object v0, Lrc/c;->u:[B

    const/16 v1, 0x9

    aget-byte v0, v0, v1

    int-to-byte v0, v0

    sget v1, Lrc/c;->v:I

    and-int/lit16 v1, v1, 0x153

    int-to-byte v1, v1

    sget v2, Lrc/c;->v:I

    and-int/lit16 v2, v2, 0x163

    int-to-short v2, v2

    invoke-static {v0, v1, v2}, Lrc/c;->a(BBI)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0, v5}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_b1
    .catch Lorg/json/JSONException; {:try_start_99 .. :try_end_b1} :catch_b2

    .line 201
    nop

    .line 200
    .line 203
    :catch_b2
    invoke-virtual {v5}, Lorg/json/JSONObject;->length()I

    move-result v0

    if-gtz v0, :cond_d0

    .line 204
    sget-object v0, Lrc/c;->u:[B

    const/16 v1, 0x9

    aget-byte v0, v0, v1

    int-to-byte v0, v0

    sget v1, Lrc/c;->v:I

    and-int/lit16 v1, v1, 0x153

    int-to-byte v1, v1

    sget v2, Lrc/c;->v:I

    and-int/lit16 v2, v2, 0x163

    int-to-short v2, v2

    invoke-static {v0, v1, v2}, Lrc/c;->a(BBI)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Lorg/json/JSONObject;->remove(Ljava/lang/String;)Ljava/lang/Object;

    .line 206
    :cond_d0
    if-eqz p0, :cond_14d

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Lorg/json/JSONArray;

    if-ne v0, v1, :cond_14d

    .line 207
    sget-object v0, Lrc/c;->t:Ljava/util/Queue;

    sget-object v1, Lrc/c;->t:Ljava/util/Queue;

    invoke-interface {v0, v1}, Ljava/util/Queue;->removeAll(Ljava/util/Collection;)Z

    .line 208
    move-object v5, p0

    check-cast v5, Lorg/json/JSONArray;

    .line 209
    const/4 v6, 0x0

    :goto_e5
    invoke-virtual {v5}, Lorg/json/JSONArray;->length()I

    move-result v0

    if-ge v6, v0, :cond_14d

    .line 210
    invoke-virtual {v5, v6}, Lorg/json/JSONArray;->opt(I)Ljava/lang/Object;

    move-result-object p0

    .line 211
    sget-object v7, Lrc/c;->t:Ljava/util/Queue;

    .line 5218
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Ljava/lang/String;

    if-ne v0, v1, :cond_149

    .line 5219
    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_149

    .line 5223
    sget-object v0, Lbr/com/itau/security/routercryptor/MobileCryptor;->a:Lrc/b;

    move-object v1, p0

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0, v1}, Lrc/b;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 5224
    if-eqz v8, :cond_114

    invoke-virtual {v8}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_115

    .line 5225
    :cond_114
    goto :goto_149

    .line 5228
    :cond_115
    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v7, v0}, Ljava/util/Queue;->offer(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_129

    .line 5229
    invoke-interface {v7}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    .line 5230
    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v7, v0}, Ljava/util/Queue;->offer(Ljava/lang/Object;)Z

    .line 5232
    :cond_129
    sget v0, Lrc/c;->v:I

    and-int/lit8 v0, v0, 0xc

    int-to-byte v0, v0

    sget-object v1, Lrc/c;->u:[B

    const/16 v2, 0x54

    aget-byte v1, v1, v2

    int-to-byte v1, v1

    sget v2, Lrc/c;->v:I

    and-int/lit16 v2, v2, 0x174

    int-to-short v2, v2

    invoke-static {v0, v1, v2}, Lrc/c;->a(BBI)Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {v7}, Ljava/util/Queue;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 209
    :cond_149
    :goto_149
    add-int/lit8 v6, v6, 0x1

    goto/16 :goto_e5

    .line 214
    :cond_14d
    invoke-virtual {v4}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    return-object v0
.end method

.method private static b(J)I
    .registers 6

    .line 319
    :goto_0
    sget-object v0, Lrc/c;->u:[B

    const/16 v1, 0xe

    aget-byte v0, v0, v1

    int-to-byte v0, v0

    shl-int/lit8 v1, v0, 0x2

    int-to-byte v1, v1

    or-int/lit8 v2, v1, 0x60

    int-to-short v2, v2

    invoke-static {v0, v1, v2}, Lrc/c;->a(BBI)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    int-to-long v0, v0

    cmp-long v0, p0, v0

    if-lez v0, :cond_1f

    .line 320
    invoke-static {p0, p1}, Lrc/c;->a(J)J

    move-result-wide p0

    goto :goto_0

    .line 322
    :cond_1f
    sget-object v0, Lrc/c;->u:[B

    const/16 v1, 0x1c

    aget-byte v0, v0, v1

    int-to-byte v0, v0

    sget v1, Lrc/c;->v:I

    and-int/lit8 v1, v1, 0x10

    int-to-byte v1, v1

    sget v2, Lrc/c;->v:I

    and-int/lit16 v2, v2, 0x380

    int-to-short v2, v2

    invoke-static {v0, v1, v2}, Lrc/c;->a(BBI)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    int-to-long v0, v0

    cmp-long v0, p0, v0

    if-nez v0, :cond_3f

    .line 323
    const/4 v0, 0x0

    return v0

    .line 325
    :cond_3f
    long-to-int v0, p0

    return v0
.end method

.method private static b([B)[B
    .registers 10

    .line 238
    sget-object v0, Lrc/c;->t:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 239
    return-object p0

    .line 243
    :cond_9
    :try_start_9
    new-instance v5, Lorg/json/JSONObject;

    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, p0}, Ljava/lang/String;-><init>([B)V

    invoke-direct {v5, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 245
    sget v0, Lrc/c;->v:I

    and-int/lit8 v0, v0, 0xc

    int-to-byte v0, v0

    sget-object v1, Lrc/c;->u:[B

    const/16 v2, 0x54

    aget-byte v1, v1, v2

    int-to-byte v1, v1

    sget v2, Lrc/c;->v:I

    and-int/lit16 v2, v2, 0x174

    int-to-short v2, v2

    invoke-static {v0, v1, v2}, Lrc/c;->a(BBI)Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lrc/c;->u:[B

    const/16 v2, 0xe

    aget-byte v1, v1, v2

    int-to-byte v1, v1

    sget-object v2, Lrc/c;->u:[B

    const/4 v3, 0x5

    aget-byte v2, v2, v3

    int-to-byte v2, v2

    int-to-short v3, v2

    invoke-static {v1, v2, v3}, Lrc/c;->a(BBI)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lrc/c;->u:[B

    const/16 v2, 0xa

    aget-byte v1, v1, v2

    int-to-byte v1, v1

    sget-object v2, Lrc/c;->u:[B

    const/4 v3, 0x5

    aget-byte v2, v2, v3

    int-to-byte v2, v2

    or-int/lit8 v3, v2, 0x6e

    int-to-short v3, v3

    invoke-static {v1, v2, v3}, Lrc/c;->a(BBI)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lrc/c;->t:Ljava/util/Queue;

    invoke-interface {v1}, Ljava/util/Queue;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 247
    invoke-static {}, Lrc/c;->a()Lrc/f;

    move-result-object v6

    .line 248
    sget-object v0, Lrc/c;->u:[B

    const/16 v1, 0x19

    aget-byte v0, v0, v1

    int-to-byte v0, v0

    sget-object v1, Lrc/c;->u:[B

    const/16 v2, 0x1c

    aget-byte v1, v1, v2

    int-to-byte v1, v1

    sget-object v2, Lrc/c;->u:[B

    const/16 v3, 0x28

    aget-byte v2, v2, v3

    neg-int v2, v2

    int-to-short v2, v2

    invoke-static {v0, v1, v2}, Lrc/c;->a(BBI)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, v6, Lrc/f;->b:Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    sget-object v2, Lbr/com/itau/security/routercryptor/MobileCryptor;->a:Lrc/b;

    iget-object v3, v6, Lrc/f;->a:Ljava/lang/String;

    invoke-interface {v2, v3}, Lrc/b;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    .line 250
    sget-object v0, Lrc/c;->u:[B

    const/16 v1, 0x9

    aget-byte v0, v0, v1

    int-to-byte v0, v0

    sget v1, Lrc/c;->v:I

    and-int/lit16 v1, v1, 0x153

    int-to-byte v1, v1

    sget v2, Lrc/c;->v:I

    and-int/lit16 v2, v2, 0x163

    int-to-short v2, v2

    invoke-static {v0, v1, v2}, Lrc/c;->a(BBI)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v8

    .line 252
    if-nez v8, :cond_c4

    .line 253
    new-instance v8, Lorg/json/JSONObject;

    invoke-direct {v8}, Lorg/json/JSONObject;-><init>()V

    .line 255
    :cond_c4
    sget v0, Lrc/c;->v:I

    and-int/lit8 v0, v0, 0xc

    int-to-byte v0, v0

    sget-object v1, Lrc/c;->u:[B

    const/16 v2, 0x54

    aget-byte v1, v1, v2

    int-to-byte v1, v1

    sget v2, Lrc/c;->v:I

    and-int/lit16 v2, v2, 0x174

    int-to-short v2, v2

    invoke-static {v0, v1, v2}, Lrc/c;->a(BBI)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v8, v0, v7}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 256
    sget-object v0, Lrc/c;->u:[B

    const/16 v1, 0x9

    aget-byte v0, v0, v1

    int-to-byte v0, v0

    sget v1, Lrc/c;->v:I

    and-int/lit16 v1, v1, 0x153

    int-to-byte v1, v1

    sget v2, Lrc/c;->v:I

    and-int/lit16 v2, v2, 0x163

    int-to-short v2, v2

    invoke-static {v0, v1, v2}, Lrc/c;->a(BBI)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0, v8}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 258
    invoke-virtual {v5}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    move-object p0, v0

    .line 260
    iget-object v0, v6, Lrc/f;->b:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_187

    .line 261
    sget v0, Lrc/c;->v:I

    and-int/lit8 v0, v0, 0xc

    int-to-byte v0, v0

    sget-object v1, Lrc/c;->u:[B

    const/16 v2, 0x54

    aget-byte v1, v1, v2

    int-to-byte v1, v1

    sget v2, Lrc/c;->v:I

    and-int/lit16 v2, v2, 0x174

    int-to-short v2, v2

    invoke-static {v0, v1, v2}, Lrc/c;->a(BBI)Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lrc/c;->u:[B

    const/16 v2, 0x65

    aget-byte v1, v1, v2

    int-to-byte v1, v1

    sget-object v2, Lrc/c;->u:[B

    const/16 v3, 0x52

    aget-byte v2, v2, v3

    neg-int v2, v2

    int-to-byte v2, v2

    sget v3, Lrc/c;->v:I

    and-int/lit16 v3, v3, 0x159

    int-to-short v3, v3

    invoke-static {v1, v2, v3}, Lrc/c;->a(BBI)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lrc/c;->t:Ljava/util/Queue;

    invoke-interface {v1}, Ljava/util/Queue;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lrc/c;->u:[B

    const/16 v2, 0xa

    aget-byte v1, v1, v2

    int-to-byte v1, v1

    sget-object v2, Lrc/c;->u:[B

    const/4 v3, 0x5

    aget-byte v2, v2, v3

    int-to-byte v2, v2

    or-int/lit8 v3, v2, 0x6e

    int-to-short v3, v3

    invoke-static {v1, v2, v3}, Lrc/c;->a(BBI)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lrc/c;->u:[B

    const/16 v2, 0xa

    aget-byte v1, v1, v2

    int-to-byte v1, v1

    sget-object v2, Lrc/c;->u:[B

    const/4 v3, 0x5

    aget-byte v2, v2, v3

    int-to-byte v2, v2

    or-int/lit8 v3, v2, 0x6e

    int-to-short v3, v3

    invoke-static {v1, v2, v3}, Lrc/c;->a(BBI)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_206

    .line 263
    :cond_187
    sget v0, Lrc/c;->v:I

    and-int/lit8 v0, v0, 0xc

    int-to-byte v0, v0

    sget-object v1, Lrc/c;->u:[B

    const/16 v2, 0x54

    aget-byte v1, v1, v2

    int-to-byte v1, v1

    sget v2, Lrc/c;->v:I

    and-int/lit16 v2, v2, 0x174

    int-to-short v2, v2

    invoke-static {v0, v1, v2}, Lrc/c;->a(BBI)Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lrc/c;->u:[B

    const/16 v2, 0x19

    aget-byte v1, v1, v2

    int-to-byte v1, v1

    sget-object v2, Lrc/c;->u:[B

    const/16 v3, 0x54

    aget-byte v2, v2, v3

    int-to-byte v2, v2

    sget-object v3, Lrc/c;->u:[B

    const/16 v4, 0x5d

    aget-byte v3, v3, v4

    int-to-short v3, v3

    invoke-static {v1, v2, v3}, Lrc/c;->a(BBI)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lrc/c;->t:Ljava/util/Queue;

    invoke-interface {v1}, Ljava/util/Queue;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lrc/c;->u:[B

    const/16 v2, 0xa

    aget-byte v1, v1, v2

    int-to-byte v1, v1

    sget-object v2, Lrc/c;->u:[B

    const/4 v3, 0x5

    aget-byte v2, v2, v3

    int-to-byte v2, v2

    or-int/lit8 v3, v2, 0x6e

    int-to-short v3, v3

    invoke-static {v1, v2, v3}, Lrc/c;->a(BBI)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lrc/c;->u:[B

    const/16 v2, 0xa

    aget-byte v1, v1, v2

    int-to-byte v1, v1

    sget-object v2, Lrc/c;->u:[B

    const/4 v3, 0x5

    aget-byte v2, v2, v3

    int-to-byte v2, v2

    or-int/lit8 v3, v2, 0x6e

    int-to-short v3, v3

    invoke-static {v1, v2, v3}, Lrc/c;->a(BBI)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_206
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_206} :catch_207

    .line 266
    :goto_206
    return-object p0

    .line 268
    :catch_207
    move-exception v6

    .line 269
    sget v0, Lrc/c;->v:I

    and-int/lit8 v0, v0, 0xc

    int-to-byte v0, v0

    sget-object v1, Lrc/c;->u:[B

    const/16 v2, 0x54

    aget-byte v1, v1, v2

    int-to-byte v1, v1

    sget v2, Lrc/c;->v:I

    and-int/lit16 v2, v2, 0x174

    int-to-short v2, v2

    invoke-static {v0, v1, v2}, Lrc/c;->a(BBI)Ljava/lang/String;

    sget-object v0, Lrc/c;->u:[B

    const/16 v1, 0x65

    aget-byte v0, v0, v1

    int-to-byte v0, v0

    sget-object v1, Lrc/c;->u:[B

    const/16 v2, 0x52

    aget-byte v1, v1, v2

    neg-int v1, v1

    int-to-byte v1, v1

    sget-object v2, Lrc/c;->u:[B

    const/16 v3, 0x78

    aget-byte v2, v2, v3

    int-to-short v2, v2

    invoke-static {v0, v1, v2}, Lrc/c;->a(BBI)Ljava/lang/String;

    .line 270
    sget v0, Lrc/c;->v:I

    and-int/lit8 v0, v0, 0xc

    int-to-byte v0, v0

    sget-object v1, Lrc/c;->u:[B

    const/16 v2, 0x54

    aget-byte v1, v1, v2

    int-to-byte v1, v1

    sget v2, Lrc/c;->v:I

    and-int/lit16 v2, v2, 0x174

    int-to-short v2, v2

    invoke-static {v0, v1, v2}, Lrc/c;->a(BBI)Ljava/lang/String;

    invoke-virtual {v6}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    .line 273
    return-object p0
.end method

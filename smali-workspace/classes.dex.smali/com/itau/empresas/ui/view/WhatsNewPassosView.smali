.class public Lcom/itau/empresas/ui/view/WhatsNewPassosView;
.super Landroid/widget/RelativeLayout;
.source "WhatsNewPassosView.java"

# interfaces
.implements Lcom/itau/empresas/ui/view/PageIndicator$Configuration;
.implements Lcom/itau/empresas/ui/util/SwipeTouchEvent$Listener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/itau/empresas/ui/view/WhatsNewPassosView$WhatsNewInfo;,
        Lcom/itau/empresas/ui/view/WhatsNewPassosView$Listener;
    }
.end annotation


# instance fields
.field botaoAnterior:Landroid/widget/TextView;

.field botaoProximo:Landroid/widget/TextView;

.field imgPrincipal:Lcom/cesards/cropimageview/CropImageView;

.field private index:I

.field private infos:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Lcom/itau/empresas/ui/view/WhatsNewPassosView$WhatsNewInfo;>;"
        }
    .end annotation
.end field

.field private listener:Lcom/itau/empresas/ui/view/WhatsNewPassosView$Listener;

.field llPagerIndicator:Lcom/itau/empresas/ui/view/PageIndicator;

.field private final margemBottom:I

.field private final margemTop:I

.field textoConteudoInfo:Landroid/widget/TextView;

.field textoTituloInfo:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 4
    .param p1, "context"    # Landroid/content/Context;

    .line 45
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 28
    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/WhatsNewPassosView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 29
    const v1, 0x7f080230

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/itau/empresas/ui/view/WhatsNewPassosView;->margemTop:I

    .line 30
    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/WhatsNewPassosView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 31
    const v1, 0x7f08022f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/itau/empresas/ui/view/WhatsNewPassosView;->margemBottom:I

    .line 39
    const/4 v0, 0x0

    iput v0, p0, Lcom/itau/empresas/ui/view/WhatsNewPassosView;->index:I

    .line 42
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/ui/view/WhatsNewPassosView;->infos:Ljava/util/List;

    .line 46
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .line 49
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 28
    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/WhatsNewPassosView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 29
    const v1, 0x7f080230

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/itau/empresas/ui/view/WhatsNewPassosView;->margemTop:I

    .line 30
    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/WhatsNewPassosView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 31
    const v1, 0x7f08022f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/itau/empresas/ui/view/WhatsNewPassosView;->margemBottom:I

    .line 39
    const/4 v0, 0x0

    iput v0, p0, Lcom/itau/empresas/ui/view/WhatsNewPassosView;->index:I

    .line 42
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/ui/view/WhatsNewPassosView;->infos:Ljava/util/List;

    .line 50
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .line 53
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 28
    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/WhatsNewPassosView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 29
    const v1, 0x7f080230

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/itau/empresas/ui/view/WhatsNewPassosView;->margemTop:I

    .line 30
    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/WhatsNewPassosView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 31
    const v1, 0x7f08022f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/itau/empresas/ui/view/WhatsNewPassosView;->margemBottom:I

    .line 39
    const/4 v0, 0x0

    iput v0, p0, Lcom/itau/empresas/ui/view/WhatsNewPassosView;->index:I

    .line 42
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/ui/view/WhatsNewPassosView;->infos:Ljava/util/List;

    .line 54
    return-void
.end method

.method private atualizarInfo(I)V
    .registers 9
    .param p1, "index"    # I

    .line 133
    if-ltz p1, :cond_6d

    iget-object v0, p0, Lcom/itau/empresas/ui/view/WhatsNewPassosView;->infos:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge p1, v0, :cond_6d

    .line 134
    iget-object v0, p0, Lcom/itau/empresas/ui/view/WhatsNewPassosView;->infos:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/itau/empresas/ui/view/WhatsNewPassosView$WhatsNewInfo;

    .line 136
    .local v5, "info":Lcom/itau/empresas/ui/view/WhatsNewPassosView$WhatsNewInfo;
    iget-object v0, p0, Lcom/itau/empresas/ui/view/WhatsNewPassosView;->textoTituloInfo:Landroid/widget/TextView;

    iget v1, v5, Lcom/itau/empresas/ui/view/WhatsNewPassosView$WhatsNewInfo;->drawableTitulo:I

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    .line 137
    iget-object v0, p0, Lcom/itau/empresas/ui/view/WhatsNewPassosView;->textoTituloInfo:Landroid/widget/TextView;

    iget v1, v5, Lcom/itau/empresas/ui/view/WhatsNewPassosView$WhatsNewInfo;->stringTitulo:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 138
    iget-object v0, p0, Lcom/itau/empresas/ui/view/WhatsNewPassosView;->imgPrincipal:Lcom/cesards/cropimageview/CropImageView;

    iget-boolean v1, v5, Lcom/itau/empresas/ui/view/WhatsNewPassosView$WhatsNewInfo;->margemTopo:Z

    if-eqz v1, :cond_2d

    sget-object v1, Lcom/cesards/cropimageview/CropImageView$CropType;->CENTER_TOP:Lcom/cesards/cropimageview/CropImageView$CropType;

    goto :goto_2f

    :cond_2d
    sget-object v1, Lcom/cesards/cropimageview/CropImageView$CropType;->CENTER_BOTTOM:Lcom/cesards/cropimageview/CropImageView$CropType;

    :goto_2f
    invoke-virtual {v0, v1}, Lcom/cesards/cropimageview/CropImageView;->setCropType(Lcom/cesards/cropimageview/CropImageView$CropType;)V

    .line 140
    iget-object v0, p0, Lcom/itau/empresas/ui/view/WhatsNewPassosView;->textoConteudoInfo:Landroid/widget/TextView;

    iget v1, v5, Lcom/itau/empresas/ui/view/WhatsNewPassosView$WhatsNewInfo;->stringConteudo:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 141
    iget-object v0, p0, Lcom/itau/empresas/ui/view/WhatsNewPassosView;->imgPrincipal:Lcom/cesards/cropimageview/CropImageView;

    iget-object v1, p0, Lcom/itau/empresas/ui/view/WhatsNewPassosView;->textoConteudoInfo:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/cesards/cropimageview/CropImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 142
    iget-object v0, p0, Lcom/itau/empresas/ui/view/WhatsNewPassosView;->imgPrincipal:Lcom/cesards/cropimageview/CropImageView;

    iget v1, v5, Lcom/itau/empresas/ui/view/WhatsNewPassosView$WhatsNewInfo;->drawablePrincipal:I

    invoke-virtual {v0, v1}, Lcom/cesards/cropimageview/CropImageView;->setImageResource(I)V

    .line 144
    iget-object v0, p0, Lcom/itau/empresas/ui/view/WhatsNewPassosView;->imgPrincipal:Lcom/cesards/cropimageview/CropImageView;

    .line 145
    invoke-virtual {v0}, Lcom/cesards/cropimageview/CropImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Landroid/widget/RelativeLayout$LayoutParams;

    .line 146
    .local v6, "params":Landroid/widget/RelativeLayout$LayoutParams;
    iget-boolean v0, v5, Lcom/itau/empresas/ui/view/WhatsNewPassosView$WhatsNewInfo;->margemTopo:Z

    if-eqz v0, :cond_5b

    iget v0, p0, Lcom/itau/empresas/ui/view/WhatsNewPassosView;->margemTop:I

    goto :goto_5c

    :cond_5b
    const/4 v0, 0x0

    :goto_5c
    iput v0, v6, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 147
    iget-boolean v0, v5, Lcom/itau/empresas/ui/view/WhatsNewPassosView$WhatsNewInfo;->margemTopo:Z

    if-eqz v0, :cond_64

    const/4 v0, 0x0

    goto :goto_66

    :cond_64
    iget v0, p0, Lcom/itau/empresas/ui/view/WhatsNewPassosView;->margemBottom:I

    :goto_66
    iput v0, v6, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    .line 149
    iget-object v0, p0, Lcom/itau/empresas/ui/view/WhatsNewPassosView;->llPagerIndicator:Lcom/itau/empresas/ui/view/PageIndicator;

    invoke-virtual {v0}, Lcom/itau/empresas/ui/view/PageIndicator;->refresh()V

    .line 151
    .end local v5    # "info":Lcom/itau/empresas/ui/view/WhatsNewPassosView$WhatsNewInfo;
    .end local v6    # "params":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_6d
    return-void
.end method


# virtual methods
.method anterior()V
    .registers 5

    .line 89
    move-object v2, p0

    monitor-enter v2

    .line 90
    :try_start_2
    iget v0, p0, Lcom/itau/empresas/ui/view/WhatsNewPassosView;->index:I

    if-lez v0, :cond_1c

    .line 91
    iget v0, p0, Lcom/itau/empresas/ui/view/WhatsNewPassosView;->index:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/itau/empresas/ui/view/WhatsNewPassosView;->index:I

    .line 93
    iget v0, p0, Lcom/itau/empresas/ui/view/WhatsNewPassosView;->index:I

    invoke-direct {p0, v0}, Lcom/itau/empresas/ui/view/WhatsNewPassosView;->atualizarInfo(I)V

    .line 95
    iget-object v0, p0, Lcom/itau/empresas/ui/view/WhatsNewPassosView;->listener:Lcom/itau/empresas/ui/view/WhatsNewPassosView$Listener;

    if-eqz v0, :cond_26

    .line 96
    iget-object v0, p0, Lcom/itau/empresas/ui/view/WhatsNewPassosView;->listener:Lcom/itau/empresas/ui/view/WhatsNewPassosView$Listener;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/itau/empresas/ui/view/WhatsNewPassosView$Listener;->anterior(Z)V

    goto :goto_26

    .line 99
    :cond_1c
    iget-object v0, p0, Lcom/itau/empresas/ui/view/WhatsNewPassosView;->listener:Lcom/itau/empresas/ui/view/WhatsNewPassosView$Listener;

    if-eqz v0, :cond_26

    .line 100
    iget-object v0, p0, Lcom/itau/empresas/ui/view/WhatsNewPassosView;->listener:Lcom/itau/empresas/ui/view/WhatsNewPassosView$Listener;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/itau/empresas/ui/view/WhatsNewPassosView$Listener;->anterior(Z)V

    .line 104
    :cond_26
    :goto_26
    iget-object v0, p0, Lcom/itau/empresas/ui/view/WhatsNewPassosView;->botaoProximo:Landroid/widget/TextView;

    const v1, 0x7f0706dd

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V
    :try_end_2e
    .catchall {:try_start_2 .. :try_end_2e} :catchall_30

    .line 105
    monitor-exit v2

    goto :goto_33

    :catchall_30
    move-exception v3

    monitor-exit v2

    throw v3

    .line 106
    :goto_33
    return-void
.end method

.method public count()I
    .registers 2

    .line 79
    iget-object v0, p0, Lcom/itau/empresas/ui/view/WhatsNewPassosView;->infos:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public inicializarViews()V
    .registers 3

    .line 65
    iget v0, p0, Lcom/itau/empresas/ui/view/WhatsNewPassosView;->index:I

    invoke-direct {p0, v0}, Lcom/itau/empresas/ui/view/WhatsNewPassosView;->atualizarInfo(I)V

    .line 67
    iget-object v0, p0, Lcom/itau/empresas/ui/view/WhatsNewPassosView;->llPagerIndicator:Lcom/itau/empresas/ui/view/PageIndicator;

    invoke-virtual {v0, p0}, Lcom/itau/empresas/ui/view/PageIndicator;->setConfiguration(Lcom/itau/empresas/ui/view/PageIndicator$Configuration;)V

    .line 68
    iget-object v0, p0, Lcom/itau/empresas/ui/view/WhatsNewPassosView;->llPagerIndicator:Lcom/itau/empresas/ui/view/PageIndicator;

    invoke-virtual {v0}, Lcom/itau/empresas/ui/view/PageIndicator;->refresh()V

    .line 70
    new-instance v1, Lcom/itau/empresas/ui/util/SwipeTouchEvent;

    invoke-direct {v1}, Lcom/itau/empresas/ui/util/SwipeTouchEvent;-><init>()V

    .line 71
    .local v1, "swipe":Lcom/itau/empresas/ui/util/SwipeTouchEvent;
    invoke-virtual {v1, p0}, Lcom/itau/empresas/ui/util/SwipeTouchEvent;->setListener(Lcom/itau/empresas/ui/util/SwipeTouchEvent$Listener;)V

    .line 73
    invoke-virtual {p0, v1}, Lcom/itau/empresas/ui/view/WhatsNewPassosView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 74
    return-void
.end method

.method public onSwipeLeft()V
    .registers 1

    .line 159
    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/WhatsNewPassosView;->proximo()V

    .line 160
    return-void
.end method

.method public onSwipeRight()V
    .registers 1

    .line 164
    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/WhatsNewPassosView;->anterior()V

    .line 165
    return-void
.end method

.method proximo()V
    .registers 6

    .line 110
    move-object v2, p0

    monitor-enter v2

    .line 111
    :try_start_2
    iget-object v0, p0, Lcom/itau/empresas/ui/view/WhatsNewPassosView;->infos:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v3, v0, -0x1

    .line 113
    .local v3, "indexUltimo":I
    iget v0, p0, Lcom/itau/empresas/ui/view/WhatsNewPassosView;->index:I

    if-ge v0, v3, :cond_30

    .line 114
    iget v0, p0, Lcom/itau/empresas/ui/view/WhatsNewPassosView;->index:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/itau/empresas/ui/view/WhatsNewPassosView;->index:I

    .line 116
    iget v0, p0, Lcom/itau/empresas/ui/view/WhatsNewPassosView;->index:I

    invoke-direct {p0, v0}, Lcom/itau/empresas/ui/view/WhatsNewPassosView;->atualizarInfo(I)V

    .line 118
    iget v0, p0, Lcom/itau/empresas/ui/view/WhatsNewPassosView;->index:I

    if-ne v0, v3, :cond_25

    .line 119
    iget-object v0, p0, Lcom/itau/empresas/ui/view/WhatsNewPassosView;->botaoProximo:Landroid/widget/TextView;

    const v1, 0x7f07048c

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 121
    :cond_25
    iget-object v0, p0, Lcom/itau/empresas/ui/view/WhatsNewPassosView;->listener:Lcom/itau/empresas/ui/view/WhatsNewPassosView$Listener;

    if-eqz v0, :cond_3a

    .line 122
    iget-object v0, p0, Lcom/itau/empresas/ui/view/WhatsNewPassosView;->listener:Lcom/itau/empresas/ui/view/WhatsNewPassosView$Listener;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/itau/empresas/ui/view/WhatsNewPassosView$Listener;->proximo(Z)V

    goto :goto_3a

    .line 125
    :cond_30
    iget-object v0, p0, Lcom/itau/empresas/ui/view/WhatsNewPassosView;->listener:Lcom/itau/empresas/ui/view/WhatsNewPassosView$Listener;

    if-eqz v0, :cond_3a

    .line 126
    iget-object v0, p0, Lcom/itau/empresas/ui/view/WhatsNewPassosView;->listener:Lcom/itau/empresas/ui/view/WhatsNewPassosView$Listener;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/itau/empresas/ui/view/WhatsNewPassosView$Listener;->proximo(Z)V
    :try_end_3a
    .catchall {:try_start_2 .. :try_end_3a} :catchall_3c

    .line 129
    .end local v3    # "indexUltimo":I
    :cond_3a
    :goto_3a
    monitor-exit v2

    goto :goto_3f

    :catchall_3c
    move-exception v4

    monitor-exit v2

    throw v4

    .line 130
    :goto_3f
    return-void
.end method

.method public selected(I)Z
    .registers 3
    .param p1, "i"    # I

    .line 84
    iget v0, p0, Lcom/itau/empresas/ui/view/WhatsNewPassosView;->index:I

    if-ne v0, p1, :cond_6

    const/4 v0, 0x1

    goto :goto_7

    :cond_6
    const/4 v0, 0x0

    :goto_7
    return v0
.end method

.method public setListener(Lcom/itau/empresas/ui/view/WhatsNewPassosView$Listener;)V
    .registers 2
    .param p1, "listener"    # Lcom/itau/empresas/ui/view/WhatsNewPassosView$Listener;

    .line 154
    iput-object p1, p0, Lcom/itau/empresas/ui/view/WhatsNewPassosView;->listener:Lcom/itau/empresas/ui/view/WhatsNewPassosView$Listener;

    .line 155
    return-void
.end method

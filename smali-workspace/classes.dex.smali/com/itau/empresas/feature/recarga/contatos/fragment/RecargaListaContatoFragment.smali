.class public Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment;
.super Lcom/itau/empresas/ui/fragment/BaseFragment;
.source "RecargaListaContatoFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment$Estados;
    }
.end annotation


# instance fields
.field botaoAutorizarContato:Landroid/widget/Button;

.field botaoIncluirContato:Landroid/widget/Button;

.field private contato:Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;

.field contatoRecargaController:Lcom/itau/empresas/feature/recarga/contatos/controller/ContatoRecargaController;

.field contatosRecargaAdapter:Lcom/itau/empresas/feature/recarga/contatos/adapter/ContatosRecargaAdapter;

.field itemContatoRecargaEscolhido:Lcom/itau/empresas/feature/recarga/contatos/customview/ItemContatoRecargaView;

.field listViewContatos:Landroid/widget/ListView;

.field private listaContatos:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;>;"
        }
    .end annotation
.end field

.field llRecargaContatoExistente:Landroid/widget/LinearLayout;

.field private maquina:Lcom/itau/empresas/ui/util/MaquinaDeEstado;

.field private position:I

.field recargaController:Lcom/itau/empresas/feature/recarga/efetivacao/controller/RecargaController;

.field private recargaInputVO:Lcom/itau/empresas/feature/recarga/contatos/model/RecargaInputVO;

.field rlAdicionarContato:Landroid/widget/RelativeLayout;

.field rlSemContato:Landroid/widget/RelativeLayout;

.field textoSemContato:Landroid/widget/TextView;

.field valorRecargaContato:Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView;


# direct methods
.method public constructor <init>()V
    .registers 2

    .line 57
    invoke-direct {p0}, Lcom/itau/empresas/ui/fragment/BaseFragment;-><init>()V

    .line 75
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment;->listaContatos:Ljava/util/List;

    return-void
.end method

.method static synthetic access$000(Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment;)V
    .registers 1
    .param p0, "x0"    # Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment;

    .line 57
    invoke-direct {p0}, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment;->carregaContatosRecarga()V

    return-void
.end method

.method private atualizarListaContatos(Ljava/util/List;)V
    .registers 9
    .param p1, "contatos"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;>;)V"
        }
    .end annotation

    .line 207
    const/4 v6, 0x0

    .line 208
    .local v6, "quantidadeContatos":I
    if-eqz p1, :cond_24

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_24

    .line 209
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v6

    .line 211
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment;->listaContatos:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 212
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment;->listaContatos:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 213
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment;->contatosRecargaAdapter:Lcom/itau/empresas/feature/recarga/contatos/adapter/ContatosRecargaAdapter;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/recarga/contatos/adapter/ContatosRecargaAdapter;->notifyDataSetChanged()V

    .line 215
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment;->maquina:Lcom/itau/empresas/ui/util/MaquinaDeEstado;

    sget-object v1, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment$Estados;->LISTA:Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment$Estados;

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/MaquinaDeEstado;->mudaParaEstado(Lcom/itau/empresas/ui/util/MaquinaDeEstado$Chave;)Lcom/itau/empresas/ui/util/MaquinaDeEstado$Chave;

    goto :goto_35

    .line 218
    :cond_24
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment;->listaContatos:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 219
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment;->contatosRecargaAdapter:Lcom/itau/empresas/feature/recarga/contatos/adapter/ContatosRecargaAdapter;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/recarga/contatos/adapter/ContatosRecargaAdapter;->notifyDataSetChanged()V

    .line 220
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment;->maquina:Lcom/itau/empresas/ui/util/MaquinaDeEstado;

    sget-object v1, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment$Estados;->SEM_LISTA:Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment$Estados;

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/MaquinaDeEstado;->mudaParaEstado(Lcom/itau/empresas/ui/util/MaquinaDeEstado$Chave;)Lcom/itau/empresas/ui/util/MaquinaDeEstado$Chave;

    .line 223
    :goto_35
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment;->application:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getEventoTracker()Lcom/itau/empresas/ui/util/analytics/TrackerEvento;

    move-result-object v0

    new-instance v1, Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;

    .line 224
    const v2, 0x7f0702e7

    invoke-virtual {p0, v2}, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/Long;

    int-to-long v4, v6

    invoke-direct {v3, v4, v5}, Ljava/lang/Long;-><init>(J)V

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-direct {v1, v2, v4, v5, v3}, Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    .line 223
    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/analytics/TrackerEvento;->enviarHitEvento(Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;)V

    .line 227
    return-void
.end method

.method private carregaContatosRecarga()V
    .registers 5

    .line 285
    invoke-virtual {p0}, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/itau/empresas/ui/util/SnackbarUtils;->tentaDispensarSnackbarNo(Ljava/lang/Object;)V

    .line 287
    invoke-virtual {p0}, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/ui/activity/BaseActivity;

    invoke-virtual {v0}, Lcom/itau/empresas/ui/activity/BaseActivity;->mostraDialogoDeProgresso()V

    .line 289
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment;->contatoRecargaController:Lcom/itau/empresas/feature/recarga/contatos/controller/ContatoRecargaController;

    iget-object v1, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment;->application:Lcom/itau/empresas/CustomApplication;

    .line 290
    invoke-virtual {v1}, Lcom/itau/empresas/CustomApplication;->getContaSelecionada()Lcom/itau/empresas/api/model/ContaOperadorVO;

    move-result-object v1

    invoke-virtual {v1}, Lcom/itau/empresas/api/model/ContaOperadorVO;->getAgencia()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment;->application:Lcom/itau/empresas/CustomApplication;

    .line 291
    invoke-virtual {v2}, Lcom/itau/empresas/CustomApplication;->getContaSelecionada()Lcom/itau/empresas/api/model/ContaOperadorVO;

    move-result-object v2

    invoke-virtual {v2}, Lcom/itau/empresas/api/model/ContaOperadorVO;->getConta()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment;->application:Lcom/itau/empresas/CustomApplication;

    .line 292
    invoke-virtual {v3}, Lcom/itau/empresas/CustomApplication;->getContaSelecionada()Lcom/itau/empresas/api/model/ContaOperadorVO;

    move-result-object v3

    invoke-virtual {v3}, Lcom/itau/empresas/api/model/ContaOperadorVO;->getDac()Ljava/lang/String;

    move-result-object v3

    .line 289
    invoke-virtual {v0, v1, v2, v3}, Lcom/itau/empresas/feature/recarga/contatos/controller/ContatoRecargaController;->buscaContatos(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 293
    return-void
.end method

.method private confirmarSimulacao(Z)V
    .registers 9
    .param p1, "autorizante"    # Z

    .line 122
    invoke-direct {p0}, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment;->isValido()Z

    move-result v0

    if-nez v0, :cond_7

    .line 123
    return-void

    .line 126
    :cond_7
    invoke-virtual {p0}, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/itau/empresas/ui/util/SnackbarUtils;->tentaDispensarSnackbarNo(Ljava/lang/Object;)V

    .line 127
    invoke-virtual {p0}, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment;->baseActivity()Lcom/itau/empresas/ui/activity/BaseActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/ui/activity/BaseActivity;->mostraDialogoDeProgresso()V

    .line 129
    invoke-direct {p0}, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment;->criarRecargaInputVO()Lcom/itau/empresas/feature/recarga/contatos/model/RecargaInputVO;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment;->recargaInputVO:Lcom/itau/empresas/feature/recarga/contatos/model/RecargaInputVO;

    .line 130
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment;->recargaInputVO:Lcom/itau/empresas/feature/recarga/contatos/model/RecargaInputVO;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/recarga/contatos/model/RecargaInputVO;->getPessoaJuridica()Lcom/itau/empresas/api/model/DadosPJVO;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/itau/empresas/api/model/DadosPJVO;->setAutorizacao(Z)V

    .line 131
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment;->application:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getEventoTracker()Lcom/itau/empresas/ui/util/analytics/TrackerEvento;

    move-result-object v0

    new-instance v1, Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;

    .line 132
    const v2, 0x7f0702e7

    invoke-virtual {p0, v2}, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 133
    const v3, 0x7f0702bc

    invoke-virtual {p0, v3}, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment;->recargaInputVO:Lcom/itau/empresas/feature/recarga/contatos/model/RecargaInputVO;

    .line 135
    invoke-virtual {v5}, Lcom/itau/empresas/feature/recarga/contatos/model/RecargaInputVO;->getRecarga()Lcom/itau/empresas/feature/recarga/contatos/model/RecargaVO;

    move-result-object v5

    invoke-virtual {v5}, Lcom/itau/empresas/feature/recarga/contatos/model/RecargaVO;->getNomeOperadora()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    aput-object v5, v4, v6

    .line 134
    const v5, 0x7f070324

    invoke-virtual {p0, v5, v4}, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment;->recargaInputVO:Lcom/itau/empresas/feature/recarga/contatos/model/RecargaInputVO;

    .line 136
    invoke-virtual {v5}, Lcom/itau/empresas/feature/recarga/contatos/model/RecargaInputVO;->getRecarga()Lcom/itau/empresas/feature/recarga/contatos/model/RecargaVO;

    move-result-object v5

    invoke-virtual {v5}, Lcom/itau/empresas/feature/recarga/contatos/model/RecargaVO;->getValorRecarga()Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Integer;->longValue()J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    .line 131
    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/analytics/TrackerEvento;->enviarHitEvento(Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;)V

    .line 138
    if-eqz p1, :cond_90

    .line 139
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment;->recargaController:Lcom/itau/empresas/feature/recarga/efetivacao/controller/RecargaController;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment;->contato:Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;

    .line 140
    invoke-virtual {v2}, Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;->getDdd()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment;->contato:Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;

    invoke-virtual {v2}, Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;->getTelefone()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment;->recargaInputVO:Lcom/itau/empresas/feature/recarga/contatos/model/RecargaInputVO;

    invoke-virtual {v0, v1, v2}, Lcom/itau/empresas/feature/recarga/efetivacao/controller/RecargaController;->simulaRecargaAutorizar(Ljava/lang/String;Lcom/itau/empresas/feature/recarga/contatos/model/RecargaInputVO;)V

    goto :goto_b4

    .line 143
    :cond_90
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment;->recargaController:Lcom/itau/empresas/feature/recarga/efetivacao/controller/RecargaController;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment;->contato:Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;

    .line 144
    invoke-virtual {v2}, Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;->getDdd()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment;->contato:Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;

    invoke-virtual {v2}, Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;->getTelefone()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment;->recargaInputVO:Lcom/itau/empresas/feature/recarga/contatos/model/RecargaInputVO;

    invoke-virtual {v0, v1, v2}, Lcom/itau/empresas/feature/recarga/efetivacao/controller/RecargaController;->simulaRecarga(Ljava/lang/String;Lcom/itau/empresas/feature/recarga/contatos/model/RecargaInputVO;)V

    .line 146
    :goto_b4
    return-void
.end method

.method private criarRecargaInputVO()Lcom/itau/empresas/feature/recarga/contatos/model/RecargaInputVO;
    .registers 6

    .line 261
    new-instance v1, Lcom/itau/empresas/api/model/DadosClienteVO;

    invoke-direct {v1}, Lcom/itau/empresas/api/model/DadosClienteVO;-><init>()V

    .line 262
    .local v1, "dadosClienteVO":Lcom/itau/empresas/api/model/DadosClienteVO;
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment;->application:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getContaSelecionada()Lcom/itau/empresas/api/model/ContaOperadorVO;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/api/model/ContaOperadorVO;->getAgencia()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/itau/empresas/api/model/DadosClienteVO;->setAgencia(Ljava/lang/String;)V

    .line 263
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment;->application:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getContaSelecionada()Lcom/itau/empresas/api/model/ContaOperadorVO;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/api/model/ContaOperadorVO;->getConta()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/itau/empresas/api/model/DadosClienteVO;->setConta(Ljava/lang/String;)V

    .line 264
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment;->application:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getContaSelecionada()Lcom/itau/empresas/api/model/ContaOperadorVO;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/api/model/ContaOperadorVO;->getDac()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/itau/empresas/api/model/DadosClienteVO;->setDigitoVerificadorDaConta(Ljava/lang/String;)V

    .line 266
    new-instance v2, Lcom/itau/empresas/feature/recarga/contatos/model/RecargaInputVO;

    invoke-direct {v2}, Lcom/itau/empresas/feature/recarga/contatos/model/RecargaInputVO;-><init>()V

    .line 267
    .local v2, "recargaInputVO":Lcom/itau/empresas/feature/recarga/contatos/model/RecargaInputVO;
    new-instance v3, Lcom/itau/empresas/feature/recarga/contatos/model/RecargaVO;

    invoke-direct {v3}, Lcom/itau/empresas/feature/recarga/contatos/model/RecargaVO;-><init>()V

    .line 268
    .local v3, "recargaVO":Lcom/itau/empresas/feature/recarga/contatos/model/RecargaVO;
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment;->contato:Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;->getNomeOperadora()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/itau/empresas/feature/recarga/contatos/model/RecargaVO;->setNomeOperadora(Ljava/lang/String;)V

    .line 269
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment;->contato:Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;->getEstadoOperadora()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/itau/empresas/feature/recarga/contatos/model/RecargaVO;->setNomeRegiao(Ljava/lang/String;)V

    .line 270
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment;->valorRecargaContato:Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView;->getValor()Lorg/joda/money/Money;

    move-result-object v0

    invoke-virtual {v0}, Lorg/joda/money/Money;->getAmount()Ljava/math/BigDecimal;

    move-result-object v0

    invoke-virtual {v0}, Ljava/math/BigDecimal;->intValue()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/itau/empresas/feature/recarga/contatos/model/RecargaVO;->setValorRecarga(Ljava/lang/Integer;)V

    .line 272
    new-instance v4, Lcom/itau/empresas/api/model/DadosPJVO;

    invoke-direct {v4}, Lcom/itau/empresas/api/model/DadosPJVO;-><init>()V

    .line 273
    .local v4, "dadosPJVO":Lcom/itau/empresas/api/model/DadosPJVO;
    const-string v0, "PEX"

    invoke-virtual {v4, v0}, Lcom/itau/empresas/api/model/DadosPJVO;->setProduto(Ljava/lang/String;)V

    .line 274
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment;->application:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getCodigoOperador()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Lcom/itau/empresas/api/model/DadosPJVO;->setCodigoOperador(Ljava/lang/String;)V

    .line 276
    invoke-virtual {v2, v1}, Lcom/itau/empresas/feature/recarga/contatos/model/RecargaInputVO;->setCliente(Lcom/itau/empresas/api/model/DadosClienteVO;)V

    .line 277
    invoke-virtual {v2, v3}, Lcom/itau/empresas/feature/recarga/contatos/model/RecargaInputVO;->setRecarga(Lcom/itau/empresas/feature/recarga/contatos/model/RecargaVO;)V

    .line 278
    invoke-virtual {v2, v4}, Lcom/itau/empresas/feature/recarga/contatos/model/RecargaInputVO;->setPessoaJuridica(Lcom/itau/empresas/api/model/DadosPJVO;)V

    .line 280
    return-object v2
.end method

.method private inicializaMaquinaDeEstado()Lcom/itau/empresas/ui/util/MaquinaDeEstado;
    .registers 7

    .line 297
    new-instance v0, Lcom/itau/empresas/ui/util/MaquinaDeEstado;

    invoke-direct {v0}, Lcom/itau/empresas/ui/util/MaquinaDeEstado;-><init>()V

    sget-object v1, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment$Estados;->SEM_LISTA:Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment$Estados;

    new-instance v2, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;

    invoke-direct {v2}, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;-><init>()V

    const/4 v3, 0x2

    new-array v3, v3, [Landroid/view/View;

    iget-object v4, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment;->llRecargaContatoExistente:Landroid/widget/LinearLayout;

    const/4 v5, 0x0

    aput-object v4, v3, v5

    iget-object v4, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment;->listViewContatos:Landroid/widget/ListView;

    const/4 v5, 0x1

    aput-object v4, v3, v5

    .line 300
    invoke-virtual {v2, v3}, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;->gone([Landroid/view/View;)Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;

    move-result-object v2

    const/4 v3, 0x2

    new-array v3, v3, [Landroid/view/View;

    iget-object v4, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment;->rlSemContato:Landroid/widget/RelativeLayout;

    const/4 v5, 0x0

    aput-object v4, v3, v5

    iget-object v4, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment;->rlAdicionarContato:Landroid/widget/RelativeLayout;

    const/4 v5, 0x1

    aput-object v4, v3, v5

    .line 301
    invoke-virtual {v2, v3}, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;->visible([Landroid/view/View;)Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;

    move-result-object v2

    .line 302
    invoke-virtual {v2}, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;->build()Lcom/itau/empresas/ui/util/maquinaestado/Estado;

    move-result-object v2

    .line 298
    invoke-virtual {v0, v1, v2}, Lcom/itau/empresas/ui/util/MaquinaDeEstado;->novoEstado(Lcom/itau/empresas/ui/util/MaquinaDeEstado$Chave;Lcom/itau/empresas/ui/util/maquinaestado/Estado;)Lcom/itau/empresas/ui/util/MaquinaDeEstado;

    move-result-object v0

    sget-object v1, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment$Estados;->LISTA:Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment$Estados;

    new-instance v2, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;

    invoke-direct {v2}, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;-><init>()V

    const/4 v3, 0x2

    new-array v3, v3, [Landroid/view/View;

    iget-object v4, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment;->llRecargaContatoExistente:Landroid/widget/LinearLayout;

    const/4 v5, 0x0

    aput-object v4, v3, v5

    iget-object v4, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment;->rlSemContato:Landroid/widget/RelativeLayout;

    const/4 v5, 0x1

    aput-object v4, v3, v5

    .line 305
    invoke-virtual {v2, v3}, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;->gone([Landroid/view/View;)Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;

    move-result-object v2

    const/4 v3, 0x2

    new-array v3, v3, [Landroid/view/View;

    iget-object v4, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment;->listViewContatos:Landroid/widget/ListView;

    const/4 v5, 0x0

    aput-object v4, v3, v5

    iget-object v4, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment;->rlAdicionarContato:Landroid/widget/RelativeLayout;

    const/4 v5, 0x1

    aput-object v4, v3, v5

    .line 306
    invoke-virtual {v2, v3}, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;->visible([Landroid/view/View;)Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;

    move-result-object v2

    .line 307
    invoke-virtual {v2}, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;->build()Lcom/itau/empresas/ui/util/maquinaestado/Estado;

    move-result-object v2

    .line 303
    invoke-virtual {v0, v1, v2}, Lcom/itau/empresas/ui/util/MaquinaDeEstado;->novoEstado(Lcom/itau/empresas/ui/util/MaquinaDeEstado$Chave;Lcom/itau/empresas/ui/util/maquinaestado/Estado;)Lcom/itau/empresas/ui/util/MaquinaDeEstado;

    move-result-object v0

    sget-object v1, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment$Estados;->RECARGA:Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment$Estados;

    new-instance v2, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;

    invoke-direct {v2}, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;-><init>()V

    const/4 v3, 0x3

    new-array v3, v3, [Landroid/view/View;

    iget-object v4, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment;->listViewContatos:Landroid/widget/ListView;

    const/4 v5, 0x0

    aput-object v4, v3, v5

    iget-object v4, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment;->rlSemContato:Landroid/widget/RelativeLayout;

    const/4 v5, 0x1

    aput-object v4, v3, v5

    iget-object v4, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment;->rlAdicionarContato:Landroid/widget/RelativeLayout;

    const/4 v5, 0x2

    aput-object v4, v3, v5

    .line 310
    invoke-virtual {v2, v3}, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;->gone([Landroid/view/View;)Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Landroid/view/View;

    iget-object v4, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment;->llRecargaContatoExistente:Landroid/widget/LinearLayout;

    const/4 v5, 0x0

    aput-object v4, v3, v5

    .line 311
    invoke-virtual {v2, v3}, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;->visible([Landroid/view/View;)Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;

    move-result-object v2

    new-instance v3, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment$2;

    invoke-direct {v3, p0}, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment$2;-><init>(Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment;)V

    .line 312
    invoke-virtual {v2, v3}, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;->onChangeState(Lcom/itau/empresas/ui/util/maquinaestado/Estado$OnChangeState;)Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;

    move-result-object v2

    .line 319
    invoke-virtual {v2}, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;->build()Lcom/itau/empresas/ui/util/maquinaestado/Estado;

    move-result-object v2

    .line 308
    invoke-virtual {v0, v1, v2}, Lcom/itau/empresas/ui/util/MaquinaDeEstado;->novoEstado(Lcom/itau/empresas/ui/util/MaquinaDeEstado$Chave;Lcom/itau/empresas/ui/util/maquinaestado/Estado;)Lcom/itau/empresas/ui/util/MaquinaDeEstado;

    move-result-object v0

    .line 297
    return-object v0
.end method

.method private isValido()Z
    .registers 4

    .line 111
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment;->valorRecargaContato:Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView;->validaValor()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 112
    const/4 v0, 0x1

    return v0

    .line 113
    :cond_a
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment;->valorRecargaContato:Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView;->isViewCarregada()Z

    move-result v0

    if-nez v0, :cond_21

    .line 114
    invoke-virtual {p0}, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 115
    const v1, 0x7f070687

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 114
    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/itau/empresas/ui/util/SnackbarUtils;->tentaMostrarSnackbarNo(Ljava/lang/Object;Ljava/lang/CharSequence;I)V

    .line 118
    :cond_21
    const/4 v0, 0x0

    return v0
.end method


# virtual methods
.method aoClicarAdicionarContato()V
    .registers 5

    .line 102
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment;->application:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getEventoTracker()Lcom/itau/empresas/ui/util/analytics/TrackerEvento;

    move-result-object v0

    new-instance v1, Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;

    .line 103
    const v2, 0x7f0702e7

    invoke-virtual {p0, v2}, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 104
    const v3, 0x7f070295

    invoke-virtual {p0, v3}, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 102
    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/analytics/TrackerEvento;->enviarHitEvento(Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;)V

    .line 105
    invoke-virtual {p0}, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity;->intent(Landroid/content/Context;)Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity_$IntentBuilder_;

    move-result-object v0

    sget-object v1, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity$TIPOS;->NOVO_CONTATO:Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity$TIPOS;

    .line 106
    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity_$IntentBuilder_;->tipo(Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity$TIPOS;)Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity_$IntentBuilder_;

    move-result-object v0

    .line 107
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity_$IntentBuilder_;->startForResult(I)Lorg/androidannotations/api/builder/PostActivityStarter;

    .line 108
    return-void
.end method

.method protected aoClicarAutorizar()V
    .registers 6

    .line 150
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment;->application:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getEventoTracker()Lcom/itau/empresas/ui/util/analytics/TrackerEvento;

    move-result-object v0

    new-instance v1, Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;

    .line 151
    const v2, 0x7f0702e7

    invoke-virtual {p0, v2}, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 152
    const v3, 0x7f0702bc

    invoke-virtual {p0, v3}, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 153
    const v4, 0x7f0702fe

    invoke-virtual {p0, v4}, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 150
    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/analytics/TrackerEvento;->enviarHitEvento(Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;)V

    .line 154
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment;->confirmarSimulacao(Z)V

    .line 155
    return-void
.end method

.method protected aoClicarIncluir()V
    .registers 6

    .line 159
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment;->application:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getEventoTracker()Lcom/itau/empresas/ui/util/analytics/TrackerEvento;

    move-result-object v0

    new-instance v1, Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;

    .line 160
    const v2, 0x7f0702e7

    invoke-virtual {p0, v2}, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 161
    const v3, 0x7f0702bc

    invoke-virtual {p0, v3}, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    const v4, 0x7f070308

    invoke-virtual {p0, v4}, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 159
    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/analytics/TrackerEvento;->enviarHitEvento(Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;)V

    .line 162
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment;->confirmarSimulacao(Z)V

    .line 163
    return-void
.end method

.method carregandoElementosDeTela()V
    .registers 3

    .line 83
    invoke-direct {p0}, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment;->inicializaMaquinaDeEstado()Lcom/itau/empresas/ui/util/MaquinaDeEstado;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment;->maquina:Lcom/itau/empresas/ui/util/MaquinaDeEstado;

    .line 84
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment;->maquina:Lcom/itau/empresas/ui/util/MaquinaDeEstado;

    sget-object v1, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment$Estados;->SEM_LISTA:Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment$Estados;

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/MaquinaDeEstado;->mudaParaEstado(Lcom/itau/empresas/ui/util/MaquinaDeEstado$Chave;)Lcom/itau/empresas/ui/util/MaquinaDeEstado$Chave;

    .line 86
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment;->contatosRecargaAdapter:Lcom/itau/empresas/feature/recarga/contatos/adapter/ContatosRecargaAdapter;

    iget-object v1, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment;->listaContatos:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/recarga/contatos/adapter/ContatosRecargaAdapter;->setItems(Ljava/util/List;)V

    .line 87
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment;->listViewContatos:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment;->contatosRecargaAdapter:Lcom/itau/empresas/feature/recarga/contatos/adapter/ContatosRecargaAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 88
    invoke-direct {p0}, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment;->carregaContatosRecarga()V

    .line 90
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment;->botaoIncluirContato:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 91
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment;->botaoAutorizarContato:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 93
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment;->application:Lcom/itau/empresas/CustomApplication;

    sget-object v1, Lcom/itau/empresas/CustomApplication$Exibicao;->EXIBICAO_RECARGA_AUTORIZAR:Lcom/itau/empresas/CustomApplication$Exibicao;

    invoke-virtual {v0, v1}, Lcom/itau/empresas/CustomApplication;->verificarPermissao(Lcom/itau/empresas/CustomApplication$Exibicao;)Z

    move-result v0

    if-eqz v0, :cond_3b

    .line 94
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment;->botaoAutorizarContato:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_42

    .line 96
    :cond_3b
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment;->botaoAutorizarContato:Landroid/widget/Button;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 98
    :goto_42
    return-void
.end method

.method public getContatosRecargaAdapter()Lcom/itau/empresas/feature/recarga/contatos/adapter/ContatosRecargaAdapter;
    .registers 2

    .line 327
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment;->contatosRecargaAdapter:Lcom/itau/empresas/feature/recarga/contatos/adapter/ContatosRecargaAdapter;

    return-object v0
.end method

.method public onEventMainThread(Lbr/com/itau/sdk/android/core/endpoint/http/RouterResponse;)V
    .registers 8
    .param p1, "resultadoSimulacaoRecargaVO"    # Lbr/com/itau/sdk/android/core/endpoint/http/RouterResponse;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Lbr/com/itau/sdk/android/core/endpoint/http/RouterResponse<Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoSimulacaoRecargaVO;>;)V"
        }
    .end annotation

    .line 178
    invoke-virtual {p0}, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment;->baseActivity()Lcom/itau/empresas/ui/activity/BaseActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/ui/activity/BaseActivity;->escondeDialogoDeProgresso()V

    .line 180
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment;->recargaInputVO:Lcom/itau/empresas/feature/recarga/contatos/model/RecargaInputVO;

    if-eqz v0, :cond_68

    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/endpoint/http/RouterResponse;->getHeaders()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_68

    .line 181
    const/4 v2, 0x0

    .line 182
    .local v2, "warning":Ljava/lang/String;
    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/endpoint/http/RouterResponse;->getHeaders()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1a
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3a

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lbr/com/itau/sdk/android/core/model/KeyValue;

    .line 183
    .local v4, "kv":Lbr/com/itau/sdk/android/core/model/KeyValue;
    invoke-virtual {v4}, Lbr/com/itau/sdk/android/core/model/KeyValue;->getKey()Ljava/lang/String;

    move-result-object v5

    .line 184
    .local v5, "key":Ljava/lang/String;
    if-eqz v5, :cond_39

    const-string v0, "Warning"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_39

    .line 185
    invoke-virtual {v4}, Lbr/com/itau/sdk/android/core/model/KeyValue;->getValue()Ljava/lang/String;

    move-result-object v2

    .line 187
    .end local v4    # "kv":Lbr/com/itau/sdk/android/core/model/KeyValue;
    .end local v5    # "key":Ljava/lang/String;
    :cond_39
    goto :goto_1a

    .line 189
    :cond_3a
    invoke-virtual {p0}, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity;->intent(Landroid/content/Context;)Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity_$IntentBuilder_;

    move-result-object v0

    .line 190
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity_$IntentBuilder_;->novo(Z)Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity_$IntentBuilder_;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment;->contato:Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;

    .line 191
    invoke-virtual {v1}, Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;->getApelido()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity_$IntentBuilder_;->nome(Ljava/lang/String;)Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity_$IntentBuilder_;

    move-result-object v0

    .line 192
    invoke-virtual {v0, v2}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity_$IntentBuilder_;->aviso(Ljava/lang/String;)Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity_$IntentBuilder_;

    move-result-object v0

    .line 193
    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/endpoint/http/RouterResponse;->getData()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoSimulacaoRecargaVO;

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity_$IntentBuilder_;->resultadoSimulacaoRecargaVO(Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoSimulacaoRecargaVO;)Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity_$IntentBuilder_;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment;->recargaInputVO:Lcom/itau/empresas/feature/recarga/contatos/model/RecargaInputVO;

    .line 194
    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity_$IntentBuilder_;->recargaInputVO(Lcom/itau/empresas/feature/recarga/contatos/model/RecargaInputVO;)Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity_$IntentBuilder_;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity_$IntentBuilder_;->start()Lorg/androidannotations/api/builder/PostActivityStarter;

    .line 197
    .end local v2    # "warning":Ljava/lang/String;
    :cond_68
    return-void
.end method

.method public onEventMainThread(Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;)V
    .registers 5
    .param p1, "ex"    # Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;

    .line 230
    invoke-static {p1, p0}, Lcom/itau/empresas/api/ApiHelper;->devoTratarEvento(Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;Lcom/itau/empresas/api/ApiConsumidor;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 231
    return-void

    .line 234
    :cond_7
    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;->getException()Lbr/com/itau/sdk/android/core/exception/BackendException;

    move-result-object v0

    invoke-virtual {v0}, Lbr/com/itau/sdk/android/core/exception/BackendException;->getError()Lbr/com/itau/sdk/android/core/model/ErrorDTO;

    move-result-object v2

    .line 235
    .local v2, "dto":Lbr/com/itau/sdk/android/core/model/ErrorDTO;
    if-eqz v2, :cond_21

    invoke-virtual {v2}, Lbr/com/itau/sdk/android/core/model/ErrorDTO;->getCodigo()Ljava/lang/String;

    move-result-object v0

    const-string v1, "codigo_erro_03"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_21

    .line 236
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment;->atualizarListaContatos(Ljava/util/List;)V

    .line 238
    :cond_21
    invoke-virtual {p0}, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/ui/activity/BaseActivity;

    invoke-virtual {v0}, Lcom/itau/empresas/ui/activity/BaseActivity;->escondeDialogoDeProgresso()V

    .line 239
    return-void
.end method

.method public onEventMainThread(Lcom/itau/empresas/Evento$EventoBuscaContatosRecarga;)V
    .registers 3
    .param p1, "eventoBuscaContatosRecarga"    # Lcom/itau/empresas/Evento$EventoBuscaContatosRecarga;

    .line 202
    invoke-virtual {p1}, Lcom/itau/empresas/Evento$EventoBuscaContatosRecarga;->getContatosRecarga()Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment;->atualizarListaContatos(Ljava/util/List;)V

    .line 203
    invoke-virtual {p0}, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/ui/activity/BaseActivity;

    invoke-virtual {v0}, Lcom/itau/empresas/ui/activity/BaseActivity;->escondeDialogoDeProgresso()V

    .line 204
    return-void
.end method

.method public onEventMainThread(Lcom/itau/empresas/Evento$EventoContatoRecargaExcluido;)V
    .registers 5
    .param p1, "eventoContatoRecargaExcluido"    # Lcom/itau/empresas/Evento$EventoContatoRecargaExcluido;

    .line 243
    invoke-virtual {p0}, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/ui/activity/BaseActivity;

    invoke-virtual {v0}, Lcom/itau/empresas/ui/activity/BaseActivity;->escondeDialogoDeProgresso()V

    .line 244
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const-string v1, "Contato exclu\u00eddo."

    .line 245
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const-string v1, "Ok"

    new-instance v2, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment$1;

    invoke-direct {v2, p0}, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment$1;-><init>(Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment;)V

    .line 246
    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNeutralButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 252
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 253
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 254
    return-void
.end method

.method public onEventMainThread(Lcom/itau/empresas/Evento$ValorSelecionado;)V
    .registers 4
    .param p1, "valorSelecionado"    # Lcom/itau/empresas/Evento$ValorSelecionado;

    .line 356
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment;->botaoIncluirContato:Landroid/widget/Button;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 357
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment;->botaoAutorizarContato:Landroid/widget/Button;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 358
    return-void
.end method

.method public onEventMainThread(Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;)V
    .registers 5
    .param p1, "contatoRecargaVO"    # Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;

    .line 166
    iput-object p1, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment;->contato:Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;

    .line 167
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment;->itemContatoRecargaEscolhido:Lcom/itau/empresas/feature/recarga/contatos/customview/ItemContatoRecargaView;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Lcom/itau/empresas/feature/recarga/contatos/customview/ItemContatoRecargaView;->ajustaDados(Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;Z)Lcom/itau/empresas/feature/recarga/contatos/customview/ItemContatoRecargaView;

    .line 168
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment;->valorRecargaContato:Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView;

    invoke-virtual {p1}, Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;->getNomeOperadora()Ljava/lang/String;

    move-result-object v1

    .line 169
    invoke-virtual {p1}, Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;->getEstadoOperadora()Ljava/lang/String;

    move-result-object v2

    .line 168
    invoke-virtual {v0, v1, v2}, Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView;->carregarValores(Ljava/lang/String;Ljava/lang/String;)V

    .line 170
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment;->maquina:Lcom/itau/empresas/ui/util/MaquinaDeEstado;

    sget-object v1, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment$Estados;->RECARGA:Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment$Estados;

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/MaquinaDeEstado;->mudaParaEstado(Lcom/itau/empresas/ui/util/MaquinaDeEstado$Chave;)Lcom/itau/empresas/ui/util/MaquinaDeEstado$Chave;

    .line 171
    return-void
.end method

.method public onResume()V
    .registers 5

    .line 349
    invoke-super {p0}, Lcom/itau/empresas/ui/fragment/BaseFragment;->onResume()V

    .line 350
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment;->contatoRecargaController:Lcom/itau/empresas/feature/recarga/contatos/controller/ContatoRecargaController;

    iget-object v1, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment;->application:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v1}, Lcom/itau/empresas/CustomApplication;->getContaSelecionada()Lcom/itau/empresas/api/model/ContaOperadorVO;

    move-result-object v1

    invoke-virtual {v1}, Lcom/itau/empresas/api/model/ContaOperadorVO;->getAgencia()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment;->application:Lcom/itau/empresas/CustomApplication;

    .line 351
    invoke-virtual {v2}, Lcom/itau/empresas/CustomApplication;->getContaSelecionada()Lcom/itau/empresas/api/model/ContaOperadorVO;

    move-result-object v2

    invoke-virtual {v2}, Lcom/itau/empresas/api/model/ContaOperadorVO;->getConta()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment;->application:Lcom/itau/empresas/CustomApplication;

    .line 352
    invoke-virtual {v3}, Lcom/itau/empresas/CustomApplication;->getContaSelecionada()Lcom/itau/empresas/api/model/ContaOperadorVO;

    move-result-object v3

    invoke-virtual {v3}, Lcom/itau/empresas/api/model/ContaOperadorVO;->getDac()Ljava/lang/String;

    move-result-object v3

    .line 350
    invoke-virtual {v0, v1, v2, v3}, Lcom/itau/empresas/feature/recarga/contatos/controller/ContatoRecargaController;->buscaContatos(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 353
    return-void
.end method

.method public opkeysConsumidas()Ljava/util/List;
    .registers 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Ljava/lang/String;>;"
        }
    .end annotation

    .line 332
    const/16 v0, 0x9

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "listaOperadoras"

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const-string v1, "contatosRecarga"

    const/4 v2, 0x1

    aput-object v1, v0, v2

    const-string v1, "cadastrarContatoRecarga"

    const/4 v2, 0x2

    aput-object v1, v0, v2

    const-string v1, "alterarContatoRecarga"

    const/4 v2, 0x3

    aput-object v1, v0, v2

    const-string v1, "deletarContatoRecarga"

    const/4 v2, 0x4

    aput-object v1, v0, v2

    const-string v1, "valoresOperadora"

    const/4 v2, 0x5

    aput-object v1, v0, v2

    const-string v1, "simularRecarga"

    const/4 v2, 0x6

    aput-object v1, v0, v2

    const-string v1, "exibicao_recarga_autorizar"

    const/4 v2, 0x7

    aput-object v1, v0, v2

    const-string v1, "efetivarRecarga"

    const/16 v2, 0x8

    aput-object v1, v0, v2

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public setCustomApplication(Lcom/itau/empresas/CustomApplication;)V
    .registers 2
    .param p1, "customApplication"    # Lcom/itau/empresas/CustomApplication;

    .line 323
    iput-object p1, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment;->application:Lcom/itau/empresas/CustomApplication;

    .line 324
    return-void
.end method

.method public setPosition(I)V
    .registers 2
    .param p1, "pos"    # I

    .line 257
    iput p1, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment;->position:I

    .line 258
    return-void
.end method

.class public final Lcom/itau/empresas/controller/TrocaSenhaController_;
.super Lcom/itau/empresas/controller/TrocaSenhaController;
.source "TrocaSenhaController_.java"


# instance fields
.field private context_:Landroid/content/Context;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .registers 2
    .param p1, "context"    # Landroid/content/Context;

    .line 19
    invoke-direct {p0}, Lcom/itau/empresas/controller/TrocaSenhaController;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/itau/empresas/controller/TrocaSenhaController_;->context_:Landroid/content/Context;

    .line 21
    invoke-direct {p0}, Lcom/itau/empresas/controller/TrocaSenhaController_;->init_()V

    .line 22
    return-void
.end method

.method public static getInstance_(Landroid/content/Context;)Lcom/itau/empresas/controller/TrocaSenhaController_;
    .registers 2
    .param p0, "context"    # Landroid/content/Context;

    .line 25
    new-instance v0, Lcom/itau/empresas/controller/TrocaSenhaController_;

    invoke-direct {v0, p0}, Lcom/itau/empresas/controller/TrocaSenhaController_;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method private init_()V
    .registers 2

    .line 29
    iget-object v0, p0, Lcom/itau/empresas/controller/TrocaSenhaController_;->context_:Landroid/content/Context;

    invoke-static {v0}, Lcom/itau/empresas/feature/login/controller/MenuController_;->getInstance_(Landroid/content/Context;)Lcom/itau/empresas/feature/login/controller/MenuController_;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/controller/TrocaSenhaController_;->menuController:Lcom/itau/empresas/feature/login/controller/MenuController;

    .line 30
    iget-object v0, p0, Lcom/itau/empresas/controller/TrocaSenhaController_;->context_:Landroid/content/Context;

    invoke-static {v0}, Lcom/itau/empresas/feature/login/controller/OperadorController_;->getInstance_(Landroid/content/Context;)Lcom/itau/empresas/feature/login/controller/OperadorController_;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/controller/TrocaSenhaController_;->operadorController:Lcom/itau/empresas/feature/login/controller/OperadorController;

    .line 31
    return-void
.end method

.class public final Lcom/itau/empresas/ui/view/ScrollViewCreditoDetalhado_;
.super Lcom/itau/empresas/ui/view/ScrollViewCreditoDetalhado;
.source "ScrollViewCreditoDetalhado_.java"

# interfaces
.implements Lorg/androidannotations/api/view/HasViews;


# instance fields
.field private alreadyInflated_:Z

.field private final onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .param p1, "context"    # Landroid/content/Context;

    .line 31
    invoke-direct {p0, p1}, Lcom/itau/empresas/ui/view/ScrollViewCreditoDetalhado;-><init>(Landroid/content/Context;)V

    .line 27
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/itau/empresas/ui/view/ScrollViewCreditoDetalhado_;->alreadyInflated_:Z

    .line 28
    new-instance v0, Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-direct {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/ui/view/ScrollViewCreditoDetalhado_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 32
    invoke-direct {p0}, Lcom/itau/empresas/ui/view/ScrollViewCreditoDetalhado_;->init_()V

    .line 33
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .line 36
    invoke-direct {p0, p1, p2}, Lcom/itau/empresas/ui/view/ScrollViewCreditoDetalhado;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 27
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/itau/empresas/ui/view/ScrollViewCreditoDetalhado_;->alreadyInflated_:Z

    .line 28
    new-instance v0, Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-direct {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/ui/view/ScrollViewCreditoDetalhado_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 37
    invoke-direct {p0}, Lcom/itau/empresas/ui/view/ScrollViewCreditoDetalhado_;->init_()V

    .line 38
    return-void
.end method

.method private init_()V
    .registers 3

    .line 62
    iget-object v0, p0, Lcom/itau/empresas/ui/view/ScrollViewCreditoDetalhado_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-static {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    move-result-object v1

    .line 63
    .local v1, "previousNotifier":Lorg/androidannotations/api/view/OnViewChangedNotifier;
    invoke-static {v1}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 64
    return-void
.end method


# virtual methods
.method public onFinishInflate()V
    .registers 2

    .line 54
    iget-boolean v0, p0, Lcom/itau/empresas/ui/view/ScrollViewCreditoDetalhado_;->alreadyInflated_:Z

    if-nez v0, :cond_c

    .line 55
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/itau/empresas/ui/view/ScrollViewCreditoDetalhado_;->alreadyInflated_:Z

    .line 56
    iget-object v0, p0, Lcom/itau/empresas/ui/view/ScrollViewCreditoDetalhado_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 58
    :cond_c
    invoke-super {p0}, Lcom/itau/empresas/ui/view/ScrollViewCreditoDetalhado;->onFinishInflate()V

    .line 59
    return-void
.end method

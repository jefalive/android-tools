.class public Lcom/itau/empresas/feature/credito/lis/activity/LisSelecaoValoresActivity;
.super Lcom/itau/empresas/ui/activity/BaseActivity;
.source "LisSelecaoValoresActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field ofertaContratacaoLisVo:Lcom/itau/empresas/feature/credito/lis/model/OfertaContratacaoLisVo;

.field possuiProdutoLisAdiciona:Z

.field recyclerView:Landroid/support/v7/widget/RecyclerView;

.field rlLisSelecaoValores:Landroid/view/View;

.field toolbar:Landroid/support/v7/widget/Toolbar;

.field valorPreAprovado:Landroid/widget/TextView;

.field valorTaxa:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 26
    invoke-direct {p0}, Lcom/itau/empresas/ui/activity/BaseActivity;-><init>()V

    return-void
.end method

.method private configuraAcessibilidade()V
    .registers 5

    .line 97
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisSelecaoValoresActivity;->rlLisSelecaoValores:Landroid/view/View;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisSelecaoValoresActivity;->ofertaContratacaoLisVo:Lcom/itau/empresas/feature/credito/lis/model/OfertaContratacaoLisVo;

    .line 100
    invoke-virtual {v2}, Lcom/itau/empresas/feature/credito/lis/model/OfertaContratacaoLisVo;->getValorNovoLimite()F

    move-result v2

    .line 99
    const/4 v3, 0x1

    invoke-static {v2, v3}, Lcom/itau/empresas/ui/util/ValorMonetarioUtils;->formataValorDecimalParaMonetario(FZ)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    iget-object v2, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisSelecaoValoresActivity;->ofertaContratacaoLisVo:Lcom/itau/empresas/feature/credito/lis/model/OfertaContratacaoLisVo;

    .line 103
    invoke-virtual {v2}, Lcom/itau/empresas/feature/credito/lis/model/OfertaContratacaoLisVo;->getTaxaJurosMes()Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    .line 102
    invoke-static {v2}, Lcom/itau/empresas/ui/util/PorcentagemUtils;->formataPorcentagemSimples(F)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    aput-object v2, v1, v3

    .line 98
    const v2, 0x7f070426

    invoke-virtual {p0, v2, v1}, Lcom/itau/empresas/feature/credito/lis/activity/LisSelecaoValoresActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 97
    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 104
    return-void
.end method

.method private configuraLayout()V
    .registers 6

    .line 65
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisSelecaoValoresActivity;->valorPreAprovado:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisSelecaoValoresActivity;->valorPreAprovado:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisSelecaoValoresActivity;->ofertaContratacaoLisVo:Lcom/itau/empresas/feature/credito/lis/model/OfertaContratacaoLisVo;

    .line 67
    invoke-virtual {v3}, Lcom/itau/empresas/feature/credito/lis/model/OfertaContratacaoLisVo;->getValorNovoLimite()F

    move-result v3

    .line 66
    const/4 v4, 0x1

    invoke-static {v3, v4}, Lcom/itau/empresas/ui/util/ValorMonetarioUtils;->formataValorDecimalParaMonetario(FZ)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    aput-object v3, v2, v4

    .line 65
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 68
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisSelecaoValoresActivity;->valorTaxa:Landroid/widget/TextView;

    const v1, 0x7f070427

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/credito/lis/activity/LisSelecaoValoresActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisSelecaoValoresActivity;->ofertaContratacaoLisVo:Lcom/itau/empresas/feature/credito/lis/model/OfertaContratacaoLisVo;

    .line 70
    invoke-virtual {v3}, Lcom/itau/empresas/feature/credito/lis/model/OfertaContratacaoLisVo;->getTaxaJurosMes()Ljava/lang/Float;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v3

    invoke-static {v3}, Lcom/itau/empresas/ui/util/PorcentagemUtils;->formataPorcentagemSimples(F)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    aput-object v3, v2, v4

    .line 68
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 72
    return-void
.end method

.method private configuraToolbar()V
    .registers 5

    .line 76
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisSelecaoValoresActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f07040a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 78
    .local v2, "titulo":Ljava/lang/String;
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisSelecaoValoresActivity;->toolbar:Landroid/support/v7/widget/Toolbar;

    const v1, 0x7f0e05c2

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Landroid/widget/TextView;

    .line 79
    .local v3, "title":Landroid/widget/TextView;
    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 80
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisSelecaoValoresActivity;->toolbar:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/credito/lis/activity/LisSelecaoValoresActivity;->setSupportActionBar(Landroid/support/v7/widget/Toolbar;)V

    .line 81
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisSelecaoValoresActivity;->toolbar:Landroid/support/v7/widget/Toolbar;

    const v1, 0x7f020165

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->setNavigationIcon(I)V

    .line 82
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisSelecaoValoresActivity;->toolbar:Landroid/support/v7/widget/Toolbar;

    const v1, 0x7f0700b8

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->setNavigationContentDescription(I)V

    .line 83
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisSelecaoValoresActivity;->toolbar:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v0, p0}, Landroid/support/v7/widget/Toolbar;->setNavigationOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 84
    return-void
.end method

.method public static intent(Landroid/content/Context;)Lcom/itau/empresas/feature/credito/lis/activity/LisSelecaoValoresActivity_$IntentBuilder_;
    .registers 2
    .param p0, "context"    # Landroid/content/Context;

    .line 107
    new-instance v0, Lcom/itau/empresas/feature/credito/lis/activity/LisSelecaoValoresActivity_$IntentBuilder_;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisSelecaoValoresActivity_$IntentBuilder_;-><init>(Landroid/content/Context;)V

    return-object v0
.end method


# virtual methods
.method aoCarregarTela()V
    .registers 5

    .line 54
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisSelecaoValoresActivity;->recyclerView:Landroid/support/v7/widget/RecyclerView;

    new-instance v1, Lcom/itau/empresas/feature/credito/lis/adapter/LisSelecaoValoresAdapter;

    iget-object v2, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisSelecaoValoresActivity;->ofertaContratacaoLisVo:Lcom/itau/empresas/feature/credito/lis/model/OfertaContratacaoLisVo;

    iget-boolean v3, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisSelecaoValoresActivity;->possuiProdutoLisAdiciona:Z

    invoke-direct {v1, v2, v3, p0}, Lcom/itau/empresas/feature/credito/lis/adapter/LisSelecaoValoresAdapter;-><init>(Lcom/itau/empresas/feature/credito/lis/model/OfertaContratacaoLisVo;ZLandroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 57
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisSelecaoValoresActivity;->recyclerView:Landroid/support/v7/widget/RecyclerView;

    new-instance v1, Landroid/support/v7/widget/LinearLayoutManager;

    invoke-direct {v1, p0}, Landroid/support/v7/widget/LinearLayoutManager;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(Landroid/support/v7/widget/RecyclerView$LayoutManager;)V

    .line 59
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisSelecaoValoresActivity;->configuraToolbar()V

    .line 60
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisSelecaoValoresActivity;->configuraLayout()V

    .line 61
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisSelecaoValoresActivity;->configuraAcessibilidade()V

    .line 62
    return-void
.end method

.method public hitAnalytics()Ljava/lang/String;
    .registers 2

    .line 49
    const v0, 0x7f070336

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/credito/lis/activity/LisSelecaoValoresActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public onClick(Landroid/view/View;)V
    .registers 2
    .param p1, "v"    # Landroid/view/View;

    .line 93
    invoke-super {p0}, Lcom/itau/empresas/ui/activity/BaseActivity;->onBackPressed()V

    .line 94
    return-void
.end method

.method public opkeysConsumidas()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Ljava/lang/String;>;"
        }
    .end annotation

    .line 88
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

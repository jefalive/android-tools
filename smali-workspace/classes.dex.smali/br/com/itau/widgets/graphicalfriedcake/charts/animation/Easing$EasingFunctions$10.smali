.class final Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingFunctions$10;
.super Ljava/lang/Object;
.source "Easing.java"

# interfaces
.implements Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/EasingFunction;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingFunctions;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .registers 1

    .line 246
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getInterpolation(F)F
    .registers 5
    .param p1, "input"    # F

    .line 249
    const/high16 v0, 0x3f000000    # 0.5f

    div-float v2, p1, v0

    .line 250
    .local v2, "position":F
    const/high16 v0, 0x3f800000    # 1.0f

    cmpg-float v0, v2, v0

    if-gez v0, :cond_11

    .line 251
    const/high16 v0, 0x3f000000    # 0.5f

    mul-float/2addr v0, v2

    mul-float/2addr v0, v2

    mul-float/2addr v0, v2

    mul-float/2addr v0, v2

    return v0

    .line 253
    :cond_11
    const/high16 v0, 0x40000000    # 2.0f

    sub-float/2addr v2, v0

    .line 254
    mul-float v0, v2, v2

    mul-float/2addr v0, v2

    mul-float/2addr v0, v2

    const/high16 v1, 0x40000000    # 2.0f

    sub-float/2addr v0, v1

    const/high16 v1, -0x41000000    # -0.5f

    mul-float/2addr v0, v1

    return v0
.end method

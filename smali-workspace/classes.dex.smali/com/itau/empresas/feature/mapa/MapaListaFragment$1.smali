.class Lcom/itau/empresas/feature/mapa/MapaListaFragment$1;
.super Ljava/lang/Object;
.source "MapaListaFragment.java"

# interfaces
.implements Landroid/support/v4/view/MenuItemCompat$OnActionExpandListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/itau/empresas/feature/mapa/MapaListaFragment;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/itau/empresas/feature/mapa/MapaListaFragment;

.field final synthetic val$menu:Landroid/view/Menu;


# direct methods
.method constructor <init>(Lcom/itau/empresas/feature/mapa/MapaListaFragment;Landroid/view/Menu;)V
    .registers 3
    .param p1, "this$0"    # Lcom/itau/empresas/feature/mapa/MapaListaFragment;

    .line 71
    iput-object p1, p0, Lcom/itau/empresas/feature/mapa/MapaListaFragment$1;->this$0:Lcom/itau/empresas/feature/mapa/MapaListaFragment;

    iput-object p2, p0, Lcom/itau/empresas/feature/mapa/MapaListaFragment$1;->val$menu:Landroid/view/Menu;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onMenuItemActionCollapse(Landroid/view/MenuItem;)Z
    .registers 4
    .param p1, "item"    # Landroid/view/MenuItem;

    .line 74
    iget-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaListaFragment$1;->val$menu:Landroid/view/Menu;

    const v1, 0x7f0e06c5

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 75
    const/4 v0, 0x1

    return v0
.end method

.method public onMenuItemActionExpand(Landroid/view/MenuItem;)Z
    .registers 6
    .param p1, "item"    # Landroid/view/MenuItem;

    .line 80
    iget-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaListaFragment$1;->val$menu:Landroid/view/Menu;

    const v1, 0x7f0e06c5

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 81
    iget-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaListaFragment$1;->this$0:Lcom/itau/empresas/feature/mapa/MapaListaFragment;

    iget-object v1, p0, Lcom/itau/empresas/feature/mapa/MapaListaFragment$1;->this$0:Lcom/itau/empresas/feature/mapa/MapaListaFragment;

    const v2, 0x7f0702b0

    invoke-virtual {v1, v2}, Lcom/itau/empresas/feature/mapa/MapaListaFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/itau/empresas/feature/mapa/MapaListaFragment$1;->this$0:Lcom/itau/empresas/feature/mapa/MapaListaFragment;

    .line 82
    const v3, 0x7f0702ff

    invoke-virtual {v2, v3}, Lcom/itau/empresas/feature/mapa/MapaListaFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 81
    invoke-virtual {v0, v1, v2}, Lcom/itau/empresas/feature/mapa/MapaListaFragment;->analyticsHit(Ljava/lang/String;Ljava/lang/String;)V

    .line 83
    const/4 v0, 0x1

    return v0
.end method

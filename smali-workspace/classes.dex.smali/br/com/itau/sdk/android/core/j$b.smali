.class public abstract Lbr/com/itau/sdk/android/core/j$b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lbr/com/itau/sdk/android/core/j$d;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lbr/com/itau/sdk/android/core/j;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "b"
.end annotation


# instance fields
.field private final builder:Lokhttp3/FormBody$Builder;

.field private final idServ:Ljava/lang/String;

.field private final url:Ljava/lang/String;


# direct methods
.method protected constructor <init>(Ljava/lang/String;Ljava/lang/String;Lokhttp3/FormBody$Builder;)V
    .registers 4

    .line 189
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 190
    iput-object p1, p0, Lbr/com/itau/sdk/android/core/j$b;->url:Ljava/lang/String;

    .line 191
    iput-object p2, p0, Lbr/com/itau/sdk/android/core/j$b;->idServ:Ljava/lang/String;

    .line 192
    iput-object p3, p0, Lbr/com/itau/sdk/android/core/j$b;->builder:Lokhttp3/FormBody$Builder;

    .line 193
    return-void
.end method

.method static synthetic access$000(Lbr/com/itau/sdk/android/core/j$b;)Ljava/lang/String;
    .registers 2

    .line 184
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/j$b;->idServ:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lbr/com/itau/sdk/android/core/j$b;)Lokhttp3/FormBody$Builder;
    .registers 2

    .line 184
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/j$b;->builder:Lokhttp3/FormBody$Builder;

    return-object v0
.end method

.method static synthetic access$200(Lbr/com/itau/sdk/android/core/j$b;)Ljava/lang/String;
    .registers 2

    .line 184
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/j$b;->url:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method protected abstract onPageLoaded(Ljava/lang/String;)V
.end method

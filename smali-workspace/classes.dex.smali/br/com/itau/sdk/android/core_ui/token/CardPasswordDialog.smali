.class public final Lbr/com/itau/sdk/android/core_ui/token/CardPasswordDialog;
.super Lbr/com/itau/sdk/android/core/token/ui/BaseDialog;
.source "SourceFile"


# instance fields
.field private a:I


# direct methods
.method public constructor <init>()V
    .registers 2

    .line 32
    invoke-direct {p0}, Lbr/com/itau/sdk/android/core/token/ui/BaseDialog;-><init>()V

    .line 33
    const/4 v0, 0x6

    iput v0, p0, Lbr/com/itau/sdk/android/core_ui/token/CardPasswordDialog;->a:I

    return-void
.end method

.method static synthetic a(Lbr/com/itau/sdk/android/core_ui/token/CardPasswordDialog;)I
    .registers 2

    .line 32
    iget v0, p0, Lbr/com/itau/sdk/android/core_ui/token/CardPasswordDialog;->a:I

    return v0
.end method

.method private a()V
    .registers 2

    .line 157
    invoke-static {}, Lde/greenrobot/event/EventBus;->getDefault()Lde/greenrobot/event/EventBus;

    move-result-object v0

    invoke-virtual {v0, p0}, Lde/greenrobot/event/EventBus;->isRegistered(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_11

    .line 158
    invoke-static {}, Lde/greenrobot/event/EventBus;->getDefault()Lde/greenrobot/event/EventBus;

    move-result-object v0

    invoke-virtual {v0, p0}, Lde/greenrobot/event/EventBus;->register(Ljava/lang/Object;)V

    .line 159
    :cond_11
    return-void
.end method

.method private a(Landroid/view/View;)V
    .registers 4

    .line 94
    sget v0, Lbr/com/itau/sdk/android/core_ui/R$id;->card_dialog:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 95
    if-eqz v1, :cond_b

    .line 96
    invoke-static {v1}, Lbr/com/itau/sdk/android/core_ui/theme/b;->a(Landroid/view/View;)V

    .line 98
    :cond_b
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/CardPasswordDialog;->loading:Landroid/view/View;

    invoke-static {v0}, Lbr/com/itau/sdk/android/core_ui/theme/b;->b(Landroid/view/View;)V

    .line 99
    return-void
.end method

.method private synthetic a(Landroid/widget/EditText;Landroid/view/View;)V
    .registers 4

    .line 58
    invoke-virtual {p1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lbr/com/itau/sdk/android/core_ui/token/CardPasswordDialog;->a(Ljava/lang/String;)V

    return-void
.end method

.method private synthetic a(Landroid/widget/ImageView;)V
    .registers 3

    .line 88
    sget v0, Lbr/com/itau/sdk/android/core_ui/R$string;->itausdkcore_dialog_card_password_close:I

    invoke-virtual {p0, v0}, Lbr/com/itau/sdk/android/core_ui/token/CardPasswordDialog;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    return-void
.end method

.method static synthetic a(Lbr/com/itau/sdk/android/core_ui/token/CardPasswordDialog;Landroid/view/View;)V
    .registers 2

    invoke-direct {p0, p1}, Lbr/com/itau/sdk/android/core_ui/token/CardPasswordDialog;->b(Landroid/view/View;)V

    return-void
.end method

.method static synthetic a(Lbr/com/itau/sdk/android/core_ui/token/CardPasswordDialog;Landroid/widget/EditText;Landroid/view/View;)V
    .registers 3

    invoke-direct {p0, p1, p2}, Lbr/com/itau/sdk/android/core_ui/token/CardPasswordDialog;->a(Landroid/widget/EditText;Landroid/view/View;)V

    return-void
.end method

.method static synthetic a(Lbr/com/itau/sdk/android/core_ui/token/CardPasswordDialog;Landroid/widget/ImageView;)V
    .registers 2

    invoke-direct {p0, p1}, Lbr/com/itau/sdk/android/core_ui/token/CardPasswordDialog;->a(Landroid/widget/ImageView;)V

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .registers 5

    .line 122
    invoke-static {}, Lbr/com/itau/sdk/android/core/BackendClient;->getInstance()Lbr/com/itau/sdk/android/core/BackendClient;

    move-result-object v0

    sget-object v1, Lbr/com/itau/sdk/android/core/token/TokenEvent$Category;->PASSWORD:Lbr/com/itau/sdk/android/core/token/TokenEvent$Category;

    sget-object v2, Lbr/com/itau/sdk/android/core/token/TokenEvent$Action;->CONFIRMATION:Lbr/com/itau/sdk/android/core/token/TokenEvent$Action;

    invoke-virtual {v0, v1, v2}, Lbr/com/itau/sdk/android/core/BackendClient;->track(Lbr/com/itau/sdk/android/core/token/TokenEvent$Category;Lbr/com/itau/sdk/android/core/token/TokenEvent$Action;)V

    .line 124
    invoke-virtual {p0}, Lbr/com/itau/sdk/android/core_ui/token/CardPasswordDialog;->hideKeyboard()V

    .line 125
    invoke-virtual {p0}, Lbr/com/itau/sdk/android/core_ui/token/CardPasswordDialog;->showLoading()V

    .line 126
    invoke-static {p1}, Lbr/com/itau/sdk/android/core/SDKCustomUiController;->validatePassword(Ljava/lang/String;)V

    .line 127
    return-void
.end method

.method private b()V
    .registers 2

    .line 162
    invoke-static {}, Lde/greenrobot/event/EventBus;->getDefault()Lde/greenrobot/event/EventBus;

    move-result-object v0

    invoke-virtual {v0, p0}, Lde/greenrobot/event/EventBus;->isRegistered(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 163
    invoke-static {}, Lde/greenrobot/event/EventBus;->getDefault()Lde/greenrobot/event/EventBus;

    move-result-object v0

    invoke-virtual {v0, p0}, Lde/greenrobot/event/EventBus;->unregister(Ljava/lang/Object;)V

    .line 164
    :cond_11
    return-void
.end method

.method private synthetic b(Landroid/view/View;)V
    .registers 2

    .line 78
    invoke-virtual {p0}, Lbr/com/itau/sdk/android/core_ui/token/CardPasswordDialog;->onBackPressed()V

    return-void
.end method


# virtual methods
.method protected onBackPressed()V
    .registers 4

    .line 103
    invoke-static {}, Lbr/com/itau/sdk/android/core/BackendClient;->getInstance()Lbr/com/itau/sdk/android/core/BackendClient;

    move-result-object v0

    sget-object v1, Lbr/com/itau/sdk/android/core/token/TokenEvent$Category;->PASSWORD:Lbr/com/itau/sdk/android/core/token/TokenEvent$Category;

    sget-object v2, Lbr/com/itau/sdk/android/core/token/TokenEvent$Action;->BACK:Lbr/com/itau/sdk/android/core/token/TokenEvent$Action;

    invoke-virtual {v0, v1, v2}, Lbr/com/itau/sdk/android/core/BackendClient;->track(Lbr/com/itau/sdk/android/core/token/TokenEvent$Category;Lbr/com/itau/sdk/android/core/token/TokenEvent$Action;)V

    .line 105
    invoke-direct {p0}, Lbr/com/itau/sdk/android/core_ui/token/CardPasswordDialog;->b()V

    .line 106
    invoke-super {p0}, Lbr/com/itau/sdk/android/core/token/ui/BaseDialog;->onBackPressed()V

    .line 107
    return-void
.end method

.method public onCancel(Landroid/content/DialogInterface;)V
    .registers 2

    .line 111
    invoke-direct {p0}, Lbr/com/itau/sdk/android/core_ui/token/CardPasswordDialog;->b()V

    .line 112
    invoke-super {p0, p1}, Lbr/com/itau/sdk/android/core/token/ui/BaseDialog;->onCancel(Landroid/content/DialogInterface;)V

    .line 113
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .registers 5

    .line 38
    invoke-static {}, Lbr/com/itau/sdk/android/core_ui/theme/SDKThemeManager;->getInstance()Lbr/com/itau/sdk/android/core_ui/theme/SDKThemeManager;

    move-result-object v1

    .line 39
    invoke-virtual {v1}, Lbr/com/itau/sdk/android/core/theme/BaseSDKThemeManager;->getDialogTheme()I

    move-result v0

    invoke-super {p0, v0}, Lbr/com/itau/sdk/android/core/token/ui/BaseDialog;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v2

    .line 40
    invoke-virtual {v1, v2}, Lbr/com/itau/sdk/android/core/theme/BaseSDKThemeManager;->checkDialogTheme(Landroid/app/Dialog;)Landroid/app/Dialog;

    move-result-object v0

    return-object v0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .registers 13

    .line 45
    sget v0, Lbr/com/itau/sdk/android/core_ui/R$layout;->itausdkcore_dialog_password:I

    invoke-virtual {p1, v0, p2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 47
    sget v0, Lbr/com/itau/sdk/android/core_ui/R$id;->title:I

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Landroid/widget/TextView;

    .line 48
    invoke-static {}, Lbr/com/itau/sdk/android/core_ui/theme/SDKThemeManager;->getInstance()Lbr/com/itau/sdk/android/core_ui/theme/SDKThemeManager;

    move-result-object v0

    invoke-virtual {p0}, Lbr/com/itau/sdk/android/core_ui/token/CardPasswordDialog;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v1

    invoke-static {v0, v1}, Lbr/com/itau/sdk/android/core/util/TypefaceUtils;->getFontBold(Lbr/com/itau/sdk/android/core/theme/BaseSDKThemeManager;Landroid/content/res/AssetManager;)Landroid/graphics/Typeface;

    move-result-object v0

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 50
    sget v0, Lbr/com/itau/sdk/android/core_ui/R$id;->message:I

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Landroid/widget/TextView;

    .line 51
    invoke-static {}, Lbr/com/itau/sdk/android/core_ui/theme/SDKThemeManager;->getInstance()Lbr/com/itau/sdk/android/core_ui/theme/SDKThemeManager;

    move-result-object v0

    invoke-virtual {p0}, Lbr/com/itau/sdk/android/core_ui/token/CardPasswordDialog;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v1

    invoke-static {v0, v1}, Lbr/com/itau/sdk/android/core/util/TypefaceUtils;->getFontRegular(Lbr/com/itau/sdk/android/core/theme/BaseSDKThemeManager;Landroid/content/res/AssetManager;)Landroid/graphics/Typeface;

    move-result-object v0

    invoke-virtual {v5, v0}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 53
    sget v0, Lbr/com/itau/sdk/android/core_ui/R$id;->token_password_edit:I

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Landroid/widget/EditText;

    .line 54
    const/4 v0, 0x1

    new-array v0, v0, [Landroid/text/InputFilter;

    new-instance v1, Landroid/text/InputFilter$LengthFilter;

    iget v2, p0, Lbr/com/itau/sdk/android/core_ui/token/CardPasswordDialog;->a:I

    invoke-direct {v1, v2}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    const/4 v2, 0x0

    aput-object v1, v0, v2

    invoke-virtual {v6, v0}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 55
    invoke-static {}, Lbr/com/itau/sdk/android/core_ui/theme/SDKThemeManager;->getInstance()Lbr/com/itau/sdk/android/core_ui/theme/SDKThemeManager;

    move-result-object v0

    invoke-virtual {p0}, Lbr/com/itau/sdk/android/core_ui/token/CardPasswordDialog;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v1

    invoke-static {v0, v1}, Lbr/com/itau/sdk/android/core/util/TypefaceUtils;->getFontRegular(Lbr/com/itau/sdk/android/core/theme/BaseSDKThemeManager;Landroid/content/res/AssetManager;)Landroid/graphics/Typeface;

    move-result-object v0

    invoke-virtual {v6, v0}, Landroid/widget/EditText;->setTypeface(Landroid/graphics/Typeface;)V

    .line 57
    sget v0, Lbr/com/itau/sdk/android/core_ui/R$id;->token_password_button:I

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Landroid/widget/Button;

    .line 58
    invoke-static {p0, v6}, Lbr/com/itau/sdk/android/core_ui/token/a;->a(Lbr/com/itau/sdk/android/core_ui/token/CardPasswordDialog;Landroid/widget/EditText;)Landroid/view/View$OnClickListener;

    move-result-object v0

    invoke-virtual {v7, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 59
    invoke-static {}, Lbr/com/itau/sdk/android/core_ui/theme/SDKThemeManager;->getInstance()Lbr/com/itau/sdk/android/core_ui/theme/SDKThemeManager;

    move-result-object v0

    invoke-virtual {p0}, Lbr/com/itau/sdk/android/core_ui/token/CardPasswordDialog;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v1

    invoke-static {v0, v1}, Lbr/com/itau/sdk/android/core/util/TypefaceUtils;->getFontBold(Lbr/com/itau/sdk/android/core/theme/BaseSDKThemeManager;Landroid/content/res/AssetManager;)Landroid/graphics/Typeface;

    move-result-object v0

    invoke-virtual {v7, v0}, Landroid/widget/Button;->setTypeface(Landroid/graphics/Typeface;)V

    .line 61
    new-instance v0, Lbr/com/itau/sdk/android/core_ui/token/d;

    invoke-direct {v0, p0, v7}, Lbr/com/itau/sdk/android/core_ui/token/d;-><init>(Lbr/com/itau/sdk/android/core_ui/token/CardPasswordDialog;Landroid/widget/Button;)V

    invoke-virtual {v6, v0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 76
    sget v0, Lbr/com/itau/sdk/android/core_ui/R$id;->token_back:I

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Landroid/widget/ImageView;

    .line 77
    invoke-static {}, Lbr/com/itau/sdk/android/core_ui/theme/b;->f()I

    move-result v0

    invoke-virtual {v8, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 78
    invoke-static {p0}, Lbr/com/itau/sdk/android/core_ui/token/b;->a(Lbr/com/itau/sdk/android/core_ui/token/CardPasswordDialog;)Landroid/view/View$OnClickListener;

    move-result-object v0

    invoke-virtual {v8, v0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 80
    sget v0, Lbr/com/itau/sdk/android/core_ui/R$id;->global_loading:I

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/CardPasswordDialog;->loading:Landroid/view/View;

    .line 82
    invoke-direct {p0}, Lbr/com/itau/sdk/android/core_ui/token/CardPasswordDialog;->a()V

    .line 83
    invoke-direct {p0, v3}, Lbr/com/itau/sdk/android/core_ui/token/CardPasswordDialog;->a(Landroid/view/View;)V

    .line 85
    invoke-virtual {p0, v6}, Lbr/com/itau/sdk/android/core_ui/token/CardPasswordDialog;->showKeyboard(Landroid/widget/EditText;)V

    .line 87
    invoke-static {p0, v8}, Lbr/com/itau/sdk/android/core_ui/token/c;->a(Lbr/com/itau/sdk/android/core_ui/token/CardPasswordDialog;Landroid/widget/ImageView;)Ljava/lang/Runnable;

    move-result-object v0

    invoke-virtual {v8, v0}, Landroid/widget/ImageView;->post(Ljava/lang/Runnable;)Z

    .line 90
    return-object v3
.end method

.method public onDestroy()V
    .registers 1

    .line 117
    invoke-direct {p0}, Lbr/com/itau/sdk/android/core_ui/token/CardPasswordDialog;->b()V

    .line 118
    invoke-super {p0}, Lbr/com/itau/sdk/android/core/token/ui/BaseDialog;->onDestroy()V

    .line 119
    return-void
.end method

.method public onEventMainThread(Lbr/com/itau/sdk/android/core/SDKCustomUiController$ValidatePasswordResponseDTO;)V
    .registers 3

    .line 131
    invoke-virtual {p0}, Lbr/com/itau/sdk/android/core_ui/token/CardPasswordDialog;->hideKeyboard()V

    .line 132
    invoke-virtual {p0}, Lbr/com/itau/sdk/android/core_ui/token/CardPasswordDialog;->hideLoading()V

    .line 133
    invoke-direct {p0}, Lbr/com/itau/sdk/android/core_ui/token/CardPasswordDialog;->b()V

    .line 135
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lbr/com/itau/sdk/android/core_ui/token/CardPasswordDialog;->onAction(I)V

    .line 136
    invoke-virtual {p0}, Lbr/com/itau/sdk/android/core_ui/token/CardPasswordDialog;->dismissAllowingStateLoss()V

    .line 137
    return-void
.end method

.method public onStart()V
    .registers 3

    .line 141
    invoke-super {p0}, Lbr/com/itau/sdk/android/core/token/ui/BaseDialog;->onStart()V

    .line 142
    invoke-static {}, Lbr/com/itau/sdk/android/core/BackendClient;->getInstance()Lbr/com/itau/sdk/android/core/BackendClient;

    move-result-object v0

    sget-object v1, Lbr/com/itau/sdk/android/core/token/TokenEvent$Screen;->PASSWORD:Lbr/com/itau/sdk/android/core/token/TokenEvent$Screen;

    invoke-virtual {v0, v1}, Lbr/com/itau/sdk/android/core/BackendClient;->trackEnterScreen(Lbr/com/itau/sdk/android/core/token/TokenEvent$Screen;)V

    .line 143
    return-void
.end method

.method public onStop()V
    .registers 3

    .line 147
    invoke-super {p0}, Lbr/com/itau/sdk/android/core/token/ui/BaseDialog;->onStop()V

    .line 148
    invoke-static {}, Lbr/com/itau/sdk/android/core/BackendClient;->getInstance()Lbr/com/itau/sdk/android/core/BackendClient;

    move-result-object v0

    sget-object v1, Lbr/com/itau/sdk/android/core/token/TokenEvent$Screen;->PASSWORD:Lbr/com/itau/sdk/android/core/token/TokenEvent$Screen;

    invoke-virtual {v0, v1}, Lbr/com/itau/sdk/android/core/BackendClient;->trackLeaveScreen(Lbr/com/itau/sdk/android/core/token/TokenEvent$Screen;)V

    .line 149
    return-void
.end method

.method public setPasswordLength(I)V
    .registers 2

    .line 152
    if-lez p1, :cond_4

    .line 153
    iput p1, p0, Lbr/com/itau/sdk/android/core_ui/token/CardPasswordDialog;->a:I

    .line 154
    :cond_4
    return-void
.end method

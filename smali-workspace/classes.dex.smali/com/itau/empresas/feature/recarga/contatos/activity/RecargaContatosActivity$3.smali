.class Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaContatosActivity$3;
.super Ljava/lang/Object;
.source "RecargaContatosActivity.java"

# interfaces
.implements Landroid/support/v7/widget/SearchView$OnQueryTextListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaContatosActivity;->pesquisarContatos()Landroid/support/v7/widget/SearchView$OnQueryTextListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaContatosActivity;


# direct methods
.method constructor <init>(Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaContatosActivity;)V
    .registers 2
    .param p1, "this$0"    # Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaContatosActivity;

    .line 230
    iput-object p1, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaContatosActivity$3;->this$0:Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaContatosActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onQueryTextChange(Ljava/lang/String;)Z
    .registers 3
    .param p1, "query"    # Ljava/lang/String;

    .line 239
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaContatosActivity$3;->this$0:Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaContatosActivity;

    # getter for: Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaContatosActivity;->recargaListaContatoFragment:Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment;
    invoke-static {v0}, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaContatosActivity;->access$100(Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaContatosActivity;)Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment;->getContatosRecargaAdapter()Lcom/itau/empresas/feature/recarga/contatos/adapter/ContatosRecargaAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/feature/recarga/contatos/adapter/ContatosRecargaAdapter;->getFilter()Landroid/widget/Filter;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/Filter;->filter(Ljava/lang/CharSequence;)V

    .line 240
    const/4 v0, 0x0

    return v0
.end method

.method public onQueryTextSubmit(Ljava/lang/String;)Z
    .registers 3
    .param p1, "query"    # Ljava/lang/String;

    .line 233
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaContatosActivity$3;->this$0:Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaContatosActivity;

    # getter for: Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaContatosActivity;->recargaListaContatoFragment:Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment;
    invoke-static {v0}, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaContatosActivity;->access$100(Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaContatosActivity;)Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment;->getContatosRecargaAdapter()Lcom/itau/empresas/feature/recarga/contatos/adapter/ContatosRecargaAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/feature/recarga/contatos/adapter/ContatosRecargaAdapter;->getFilter()Landroid/widget/Filter;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/Filter;->filter(Ljava/lang/CharSequence;)V

    .line 234
    const/4 v0, 0x0

    return v0
.end method

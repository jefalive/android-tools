.class public Lcom/itau/empresas/feature/credito/giro/controller/EmprestimosParceladosController;
.super Lcom/itau/empresas/controller/BaseController;
.source "EmprestimosParceladosController.java"


# instance fields
.field app:Lcom/itau/empresas/CustomApplication;


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 16
    invoke-direct {p0}, Lcom/itau/empresas/controller/BaseController;-><init>()V

    return-void
.end method

.method static synthetic access$000(Ljava/lang/Object;)V
    .registers 1
    .param p0, "x0"    # Ljava/lang/Object;

    .line 16
    invoke-static {p0}, Lcom/itau/empresas/feature/credito/giro/controller/EmprestimosParceladosController;->post(Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic access$100(Ljava/lang/Object;)V
    .registers 1
    .param p0, "x0"    # Ljava/lang/Object;

    .line 16
    invoke-static {p0}, Lcom/itau/empresas/feature/credito/giro/controller/EmprestimosParceladosController;->post(Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic access$200(Ljava/lang/Object;)V
    .registers 1
    .param p0, "x0"    # Ljava/lang/Object;

    .line 16
    invoke-static {p0}, Lcom/itau/empresas/feature/credito/giro/controller/EmprestimosParceladosController;->post(Ljava/lang/Object;)V

    return-void
.end method

.method public static getApi()Lcom/itau/empresas/api/credito/ApiGiro;
    .registers 1

    .line 21
    const-class v0, Lcom/itau/empresas/api/credito/ApiGiro;

    invoke-static {v0}, Lbr/com/itau/sdk/android/core/BackendClient;->api(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/api/credito/ApiGiro;

    return-object v0
.end method


# virtual methods
.method public consultaContratadaComprovante(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 11
    .param p1, "idComprovante"    # Ljava/lang/String;
    .param p2, "agencia"    # Ljava/lang/String;
    .param p3, "conta"    # Ljava/lang/String;
    .param p4, "digitoConta"    # Ljava/lang/String;

    .line 54
    new-instance v0, Lcom/itau/empresas/feature/credito/giro/controller/EmprestimosParceladosController$3;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/itau/empresas/feature/credito/giro/controller/EmprestimosParceladosController$3;-><init>(Lcom/itau/empresas/feature/credito/giro/controller/EmprestimosParceladosController;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Lbr/com/itau/sdk/android/core/BackendClient;->callApi(Lbr/com/itau/sdk/android/core/type/BackendCall;)V

    .line 62
    return-void
.end method

.method public consultaContratadas(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 11
    .param p1, "agencia"    # Ljava/lang/String;
    .param p2, "conta"    # Ljava/lang/String;
    .param p3, "digitoVerificadorConta"    # Ljava/lang/String;
    .param p4, "codigoComprovante"    # Ljava/lang/String;

    .line 40
    new-instance v0, Lcom/itau/empresas/feature/credito/giro/controller/EmprestimosParceladosController$2;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/itau/empresas/feature/credito/giro/controller/EmprestimosParceladosController$2;-><init>(Lcom/itau/empresas/feature/credito/giro/controller/EmprestimosParceladosController;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Lbr/com/itau/sdk/android/core/BackendClient;->callApi(Lbr/com/itau/sdk/android/core/type/BackendCall;)V

    .line 49
    return-void
.end method

.method public consultaOfertas(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 11
    .param p1, "agencia"    # Ljava/lang/String;
    .param p2, "conta"    # Ljava/lang/String;
    .param p3, "digitoConta"    # Ljava/lang/String;
    .param p4, "cpfOperador"    # Ljava/lang/String;

    .line 26
    new-instance v0, Lcom/itau/empresas/feature/credito/giro/controller/EmprestimosParceladosController$1;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/itau/empresas/feature/credito/giro/controller/EmprestimosParceladosController$1;-><init>(Lcom/itau/empresas/feature/credito/giro/controller/EmprestimosParceladosController;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Lbr/com/itau/sdk/android/core/BackendClient;->callApi(Lbr/com/itau/sdk/android/core/type/BackendCall;)V

    .line 35
    return-void
.end method

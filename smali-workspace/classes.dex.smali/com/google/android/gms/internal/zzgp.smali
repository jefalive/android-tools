.class public Lcom/google/android/gms/internal/zzgp;
.super Lcom/google/android/gms/internal/zzgn;


# annotations
.annotation runtime Lcom/google/android/gms/internal/zzhb;
.end annotation


# instance fields
.field private zzGs:Lcom/google/android/gms/internal/zzgo;


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/google/android/gms/internal/zzif$zza;Lcom/google/android/gms/internal/zzjp;Lcom/google/android/gms/internal/zzgr$zza;)V
    .registers 5

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/gms/internal/zzgn;-><init>(Landroid/content/Context;Lcom/google/android/gms/internal/zzif$zza;Lcom/google/android/gms/internal/zzjp;Lcom/google/android/gms/internal/zzgr$zza;)V

    return-void
.end method


# virtual methods
.method protected zzgb()V
    .registers 7

    iget-object v0, p0, Lcom/google/android/gms/internal/zzgp;->zzpD:Lcom/google/android/gms/internal/zzjp;

    invoke-interface {v0}, Lcom/google/android/gms/internal/zzjp;->zzaN()Lcom/google/android/gms/ads/internal/client/AdSizeParcel;

    move-result-object v4

    iget-boolean v0, v4, Lcom/google/android/gms/ads/internal/client/AdSizeParcel;->zzui:Z

    if-eqz v0, :cond_19

    iget-object v0, p0, Lcom/google/android/gms/internal/zzgp;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v5

    iget v2, v5, Landroid/util/DisplayMetrics;->widthPixels:I

    iget v3, v5, Landroid/util/DisplayMetrics;->heightPixels:I

    goto :goto_1d

    :cond_19
    iget v2, v4, Lcom/google/android/gms/ads/internal/client/AdSizeParcel;->widthPixels:I

    iget v3, v4, Lcom/google/android/gms/ads/internal/client/AdSizeParcel;->heightPixels:I

    :goto_1d
    new-instance v0, Lcom/google/android/gms/internal/zzgo;

    iget-object v1, p0, Lcom/google/android/gms/internal/zzgp;->zzpD:Lcom/google/android/gms/internal/zzjp;

    invoke-direct {v0, p0, v1, v2, v3}, Lcom/google/android/gms/internal/zzgo;-><init>(Lcom/google/android/gms/internal/zzjq$zza;Lcom/google/android/gms/internal/zzjp;II)V

    iput-object v0, p0, Lcom/google/android/gms/internal/zzgp;->zzGs:Lcom/google/android/gms/internal/zzgo;

    iget-object v0, p0, Lcom/google/android/gms/internal/zzgp;->zzpD:Lcom/google/android/gms/internal/zzjp;

    invoke-interface {v0}, Lcom/google/android/gms/internal/zzjp;->zzhU()Lcom/google/android/gms/internal/zzjq;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/gms/internal/zzjq;->zza(Lcom/google/android/gms/internal/zzjq$zza;)V

    iget-object v0, p0, Lcom/google/android/gms/internal/zzgp;->zzGs:Lcom/google/android/gms/internal/zzgo;

    iget-object v1, p0, Lcom/google/android/gms/internal/zzgp;->zzGe:Lcom/google/android/gms/ads/internal/request/AdResponseParcel;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/internal/zzgo;->zza(Lcom/google/android/gms/ads/internal/request/AdResponseParcel;)V

    return-void
.end method

.method protected zzgc()I
    .registers 2

    iget-object v0, p0, Lcom/google/android/gms/internal/zzgp;->zzGs:Lcom/google/android/gms/internal/zzgo;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/zzgo;->zzgg()Z

    move-result v0

    if-eqz v0, :cond_f

    const-string v0, "Ad-Network indicated no fill with passback URL."

    invoke-static {v0}, Lcom/google/android/gms/internal/zzin;->zzaI(Ljava/lang/String;)V

    const/4 v0, 0x3

    return v0

    :cond_f
    iget-object v0, p0, Lcom/google/android/gms/internal/zzgp;->zzGs:Lcom/google/android/gms/internal/zzgo;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/zzgo;->zzgh()Z

    move-result v0

    if-nez v0, :cond_19

    const/4 v0, 0x2

    return v0

    :cond_19
    const/4 v0, -0x2

    return v0
.end method

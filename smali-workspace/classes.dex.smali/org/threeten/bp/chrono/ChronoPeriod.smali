.class public abstract Lorg/threeten/bp/chrono/ChronoPeriod;
.super Ljava/lang/Object;
.source "ChronoPeriod.java"

# interfaces
.implements Lorg/threeten/bp/temporal/TemporalAmount;


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract get(Lorg/threeten/bp/temporal/TemporalUnit;)J
.end method

.method public abstract getUnits()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Lorg/threeten/bp/temporal/TemporalUnit;>;"
        }
    .end annotation
.end method

.method public isZero()Z
    .registers 7

    .line 145
    invoke-virtual {p0}, Lorg/threeten/bp/chrono/ChronoPeriod;->getUnits()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :goto_8
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_22

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lorg/threeten/bp/temporal/TemporalUnit;

    .line 146
    .local v5, "unit":Lorg/threeten/bp/temporal/TemporalUnit;
    invoke-virtual {p0, v5}, Lorg/threeten/bp/chrono/ChronoPeriod;->get(Lorg/threeten/bp/temporal/TemporalUnit;)J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_21

    .line 147
    const/4 v0, 0x0

    return v0

    .end local v5    # "unit":Lorg/threeten/bp/temporal/TemporalUnit;
    :cond_21
    goto :goto_8

    .line 150
    .end local v4    # "i$":Ljava/util/Iterator;
    :cond_22
    const/4 v0, 0x1

    return v0
.end method

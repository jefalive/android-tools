.class Lnet/hockeyapp/android/utils/Base64$Encoder;
.super Lnet/hockeyapp/android/utils/Base64$Coder;
.source "Base64.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lnet/hockeyapp/android/utils/Base64;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Encoder"
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final ENCODE:[B

.field private static final ENCODE_WEBSAFE:[B


# instance fields
.field private final alphabet:[B

.field private count:I

.field public final do_cr:Z

.field public final do_newline:Z

.field public final do_padding:Z

.field private final tail:[B

.field tailLen:I


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .line 545
    const-class v0, Lnet/hockeyapp/android/utils/Base64;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_a

    const/4 v0, 0x1

    goto :goto_b

    :cond_a
    const/4 v0, 0x0

    :goto_b
    sput-boolean v0, Lnet/hockeyapp/android/utils/Base64$Encoder;->$assertionsDisabled:Z

    .line 557
    const/16 v0, 0x40

    new-array v0, v0, [B

    fill-array-data v0, :array_20

    sput-object v0, Lnet/hockeyapp/android/utils/Base64$Encoder;->ENCODE:[B

    .line 568
    const/16 v0, 0x40

    new-array v0, v0, [B

    fill-array-data v0, :array_44

    sput-object v0, Lnet/hockeyapp/android/utils/Base64$Encoder;->ENCODE_WEBSAFE:[B

    return-void

    :array_20
    .array-data 1
        0x41t
        0x42t
        0x43t
        0x44t
        0x45t
        0x46t
        0x47t
        0x48t
        0x49t
        0x4at
        0x4bt
        0x4ct
        0x4dt
        0x4et
        0x4ft
        0x50t
        0x51t
        0x52t
        0x53t
        0x54t
        0x55t
        0x56t
        0x57t
        0x58t
        0x59t
        0x5at
        0x61t
        0x62t
        0x63t
        0x64t
        0x65t
        0x66t
        0x67t
        0x68t
        0x69t
        0x6at
        0x6bt
        0x6ct
        0x6dt
        0x6et
        0x6ft
        0x70t
        0x71t
        0x72t
        0x73t
        0x74t
        0x75t
        0x76t
        0x77t
        0x78t
        0x79t
        0x7at
        0x30t
        0x31t
        0x32t
        0x33t
        0x34t
        0x35t
        0x36t
        0x37t
        0x38t
        0x39t
        0x2bt
        0x2ft
    .end array-data

    :array_44
    .array-data 1
        0x41t
        0x42t
        0x43t
        0x44t
        0x45t
        0x46t
        0x47t
        0x48t
        0x49t
        0x4at
        0x4bt
        0x4ct
        0x4dt
        0x4et
        0x4ft
        0x50t
        0x51t
        0x52t
        0x53t
        0x54t
        0x55t
        0x56t
        0x57t
        0x58t
        0x59t
        0x5at
        0x61t
        0x62t
        0x63t
        0x64t
        0x65t
        0x66t
        0x67t
        0x68t
        0x69t
        0x6at
        0x6bt
        0x6ct
        0x6dt
        0x6et
        0x6ft
        0x70t
        0x71t
        0x72t
        0x73t
        0x74t
        0x75t
        0x76t
        0x77t
        0x78t
        0x79t
        0x7at
        0x30t
        0x31t
        0x32t
        0x33t
        0x34t
        0x35t
        0x36t
        0x37t
        0x38t
        0x39t
        0x2dt
        0x5ft
    .end array-data
.end method

.method public constructor <init>(I[B)V
    .registers 4
    .param p1, "flags"    # I
    .param p2, "output"    # [B

    .line 584
    invoke-direct {p0}, Lnet/hockeyapp/android/utils/Base64$Coder;-><init>()V

    .line 585
    iput-object p2, p0, Lnet/hockeyapp/android/utils/Base64$Encoder;->output:[B

    .line 587
    and-int/lit8 v0, p1, 0x1

    if-nez v0, :cond_b

    const/4 v0, 0x1

    goto :goto_c

    :cond_b
    const/4 v0, 0x0

    :goto_c
    iput-boolean v0, p0, Lnet/hockeyapp/android/utils/Base64$Encoder;->do_padding:Z

    .line 588
    and-int/lit8 v0, p1, 0x2

    if-nez v0, :cond_14

    const/4 v0, 0x1

    goto :goto_15

    :cond_14
    const/4 v0, 0x0

    :goto_15
    iput-boolean v0, p0, Lnet/hockeyapp/android/utils/Base64$Encoder;->do_newline:Z

    .line 589
    and-int/lit8 v0, p1, 0x4

    if-eqz v0, :cond_1d

    const/4 v0, 0x1

    goto :goto_1e

    :cond_1d
    const/4 v0, 0x0

    :goto_1e
    iput-boolean v0, p0, Lnet/hockeyapp/android/utils/Base64$Encoder;->do_cr:Z

    .line 590
    and-int/lit8 v0, p1, 0x8

    if-nez v0, :cond_27

    sget-object v0, Lnet/hockeyapp/android/utils/Base64$Encoder;->ENCODE:[B

    goto :goto_29

    :cond_27
    sget-object v0, Lnet/hockeyapp/android/utils/Base64$Encoder;->ENCODE_WEBSAFE:[B

    :goto_29
    iput-object v0, p0, Lnet/hockeyapp/android/utils/Base64$Encoder;->alphabet:[B

    .line 592
    const/4 v0, 0x2

    new-array v0, v0, [B

    iput-object v0, p0, Lnet/hockeyapp/android/utils/Base64$Encoder;->tail:[B

    .line 593
    const/4 v0, 0x0

    iput v0, p0, Lnet/hockeyapp/android/utils/Base64$Encoder;->tailLen:I

    .line 595
    iget-boolean v0, p0, Lnet/hockeyapp/android/utils/Base64$Encoder;->do_newline:Z

    if-eqz v0, :cond_3a

    const/16 v0, 0x13

    goto :goto_3b

    :cond_3a
    const/4 v0, -0x1

    :goto_3b
    iput v0, p0, Lnet/hockeyapp/android/utils/Base64$Encoder;->count:I

    .line 596
    return-void
.end method


# virtual methods
.method public process([BIIZ)Z
    .registers 15
    .param p1, "input"    # [B
    .param p2, "offset"    # I
    .param p3, "len"    # I
    .param p4, "finish"    # Z

    .line 608
    iget-object v3, p0, Lnet/hockeyapp/android/utils/Base64$Encoder;->alphabet:[B

    .line 609
    .local v3, "alphabet":[B
    iget-object v4, p0, Lnet/hockeyapp/android/utils/Base64$Encoder;->output:[B

    .line 610
    .local v4, "output":[B
    const/4 v5, 0x0

    .line 611
    .local v5, "op":I
    iget v6, p0, Lnet/hockeyapp/android/utils/Base64$Encoder;->count:I

    .line 613
    .local v6, "count":I
    move v7, p2

    .line 614
    .local v7, "p":I
    add-int/2addr p3, p2

    .line 615
    const/4 v8, -0x1

    .line 621
    .local v8, "v":I
    iget v0, p0, Lnet/hockeyapp/android/utils/Base64$Encoder;->tailLen:I

    packed-switch v0, :pswitch_data_24c

    goto/16 :goto_59

    .line 624
    :pswitch_11
    goto :goto_59

    .line 627
    :pswitch_12
    add-int/lit8 v0, v7, 0x2

    if-gt v0, p3, :cond_59

    .line 630
    iget-object v0, p0, Lnet/hockeyapp/android/utils/Base64$Encoder;->tail:[B

    const/4 v1, 0x0

    aget-byte v0, v0, v1

    and-int/lit16 v0, v0, 0xff

    shl-int/lit8 v0, v0, 0x10

    move v1, v7

    add-int/lit8 v7, v7, 0x1

    aget-byte v1, p1, v1

    and-int/lit16 v1, v1, 0xff

    shl-int/lit8 v1, v1, 0x8

    or-int/2addr v0, v1

    move v1, v7

    add-int/lit8 v7, v7, 0x1

    aget-byte v1, p1, v1

    and-int/lit16 v1, v1, 0xff

    or-int v8, v0, v1

    .line 633
    const/4 v0, 0x0

    iput v0, p0, Lnet/hockeyapp/android/utils/Base64$Encoder;->tailLen:I

    goto :goto_59

    .line 638
    :pswitch_36
    add-int/lit8 v0, v7, 0x1

    if-gt v0, p3, :cond_59

    .line 640
    iget-object v0, p0, Lnet/hockeyapp/android/utils/Base64$Encoder;->tail:[B

    const/4 v1, 0x0

    aget-byte v0, v0, v1

    and-int/lit16 v0, v0, 0xff

    shl-int/lit8 v0, v0, 0x10

    iget-object v1, p0, Lnet/hockeyapp/android/utils/Base64$Encoder;->tail:[B

    const/4 v2, 0x1

    aget-byte v1, v1, v2

    and-int/lit16 v1, v1, 0xff

    shl-int/lit8 v1, v1, 0x8

    or-int/2addr v0, v1

    move v1, v7

    add-int/lit8 v7, v7, 0x1

    aget-byte v1, p1, v1

    and-int/lit16 v1, v1, 0xff

    or-int v8, v0, v1

    .line 643
    const/4 v0, 0x0

    iput v0, p0, Lnet/hockeyapp/android/utils/Base64$Encoder;->tailLen:I

    .line 648
    :cond_59
    :goto_59
    const/4 v0, -0x1

    if-eq v8, v0, :cond_a3

    .line 649
    move v0, v5

    add-int/lit8 v5, v5, 0x1

    shr-int/lit8 v1, v8, 0x12

    and-int/lit8 v1, v1, 0x3f

    aget-byte v1, v3, v1

    const/4 v0, 0x0

    aput-byte v1, v4, v0

    .line 650
    move v0, v5

    add-int/lit8 v5, v5, 0x1

    shr-int/lit8 v1, v8, 0xc

    and-int/lit8 v1, v1, 0x3f

    aget-byte v1, v3, v1

    const/4 v0, 0x1

    aput-byte v1, v4, v0

    .line 651
    move v0, v5

    add-int/lit8 v5, v5, 0x1

    shr-int/lit8 v1, v8, 0x6

    and-int/lit8 v1, v1, 0x3f

    aget-byte v1, v3, v1

    const/4 v0, 0x2

    aput-byte v1, v4, v0

    .line 652
    move v0, v5

    add-int/lit8 v5, v5, 0x1

    and-int/lit8 v1, v8, 0x3f

    aget-byte v1, v3, v1

    const/4 v0, 0x3

    aput-byte v1, v4, v0

    .line 653
    add-int/lit8 v6, v6, -0x1

    if-nez v6, :cond_a3

    .line 654
    iget-boolean v0, p0, Lnet/hockeyapp/android/utils/Base64$Encoder;->do_cr:Z

    if-eqz v0, :cond_9a

    move v0, v5

    add-int/lit8 v5, v5, 0x1

    const/16 v1, 0xd

    const/4 v0, 0x4

    aput-byte v1, v4, v0

    .line 655
    :cond_9a
    move v0, v5

    add-int/lit8 v5, v5, 0x1

    const/16 v1, 0xa

    aput-byte v1, v4, v0

    .line 656
    const/16 v6, 0x13

    .line 665
    :cond_a3
    :goto_a3
    add-int/lit8 v0, v7, 0x3

    if-gt v0, p3, :cond_100

    .line 666
    aget-byte v0, p1, v7

    and-int/lit16 v0, v0, 0xff

    shl-int/lit8 v0, v0, 0x10

    add-int/lit8 v1, v7, 0x1

    aget-byte v1, p1, v1

    and-int/lit16 v1, v1, 0xff

    shl-int/lit8 v1, v1, 0x8

    or-int/2addr v0, v1

    add-int/lit8 v1, v7, 0x2

    aget-byte v1, p1, v1

    and-int/lit16 v1, v1, 0xff

    or-int v8, v0, v1

    .line 669
    shr-int/lit8 v0, v8, 0x12

    and-int/lit8 v0, v0, 0x3f

    aget-byte v0, v3, v0

    aput-byte v0, v4, v5

    .line 670
    add-int/lit8 v0, v5, 0x1

    shr-int/lit8 v1, v8, 0xc

    and-int/lit8 v1, v1, 0x3f

    aget-byte v1, v3, v1

    aput-byte v1, v4, v0

    .line 671
    add-int/lit8 v0, v5, 0x2

    shr-int/lit8 v1, v8, 0x6

    and-int/lit8 v1, v1, 0x3f

    aget-byte v1, v3, v1

    aput-byte v1, v4, v0

    .line 672
    add-int/lit8 v0, v5, 0x3

    and-int/lit8 v1, v8, 0x3f

    aget-byte v1, v3, v1

    aput-byte v1, v4, v0

    .line 673
    add-int/lit8 v7, v7, 0x3

    .line 674
    add-int/lit8 v5, v5, 0x4

    .line 675
    add-int/lit8 v6, v6, -0x1

    if-nez v6, :cond_a3

    .line 676
    iget-boolean v0, p0, Lnet/hockeyapp/android/utils/Base64$Encoder;->do_cr:Z

    if-eqz v0, :cond_f5

    move v0, v5

    add-int/lit8 v5, v5, 0x1

    const/16 v1, 0xd

    aput-byte v1, v4, v0

    .line 677
    :cond_f5
    move v0, v5

    add-int/lit8 v5, v5, 0x1

    const/16 v1, 0xa

    aput-byte v1, v4, v0

    .line 678
    const/16 v6, 0x13

    goto/16 :goto_a3

    .line 682
    :cond_100
    if-eqz p4, :cond_217

    .line 688
    iget v0, p0, Lnet/hockeyapp/android/utils/Base64$Encoder;->tailLen:I

    sub-int v0, v7, v0

    add-int/lit8 v1, p3, -0x1

    if-ne v0, v1, :cond_164

    .line 689
    const/4 v9, 0x0

    .line 690
    .local v9, "t":I
    iget v0, p0, Lnet/hockeyapp/android/utils/Base64$Encoder;->tailLen:I

    if-lez v0, :cond_118

    iget-object v0, p0, Lnet/hockeyapp/android/utils/Base64$Encoder;->tail:[B

    move v1, v9

    add-int/lit8 v9, v9, 0x1

    const/4 v1, 0x0

    aget-byte v0, v0, v1

    goto :goto_11d

    :cond_118
    move v0, v7

    add-int/lit8 v7, v7, 0x1

    aget-byte v0, p1, v0

    :goto_11d
    and-int/lit16 v0, v0, 0xff

    shl-int/lit8 v8, v0, 0x4

    .line 691
    iget v0, p0, Lnet/hockeyapp/android/utils/Base64$Encoder;->tailLen:I

    sub-int/2addr v0, v9

    iput v0, p0, Lnet/hockeyapp/android/utils/Base64$Encoder;->tailLen:I

    .line 692
    move v0, v5

    add-int/lit8 v5, v5, 0x1

    shr-int/lit8 v1, v8, 0x6

    and-int/lit8 v1, v1, 0x3f

    aget-byte v1, v3, v1

    aput-byte v1, v4, v0

    .line 693
    move v0, v5

    add-int/lit8 v5, v5, 0x1

    and-int/lit8 v1, v8, 0x3f

    aget-byte v1, v3, v1

    aput-byte v1, v4, v0

    .line 694
    iget-boolean v0, p0, Lnet/hockeyapp/android/utils/Base64$Encoder;->do_padding:Z

    if-eqz v0, :cond_14c

    .line 695
    move v0, v5

    add-int/lit8 v5, v5, 0x1

    const/16 v1, 0x3d

    aput-byte v1, v4, v0

    .line 696
    move v0, v5

    add-int/lit8 v5, v5, 0x1

    const/16 v1, 0x3d

    aput-byte v1, v4, v0

    .line 698
    :cond_14c
    iget-boolean v0, p0, Lnet/hockeyapp/android/utils/Base64$Encoder;->do_newline:Z

    if-eqz v0, :cond_162

    .line 699
    iget-boolean v0, p0, Lnet/hockeyapp/android/utils/Base64$Encoder;->do_cr:Z

    if-eqz v0, :cond_15b

    move v0, v5

    add-int/lit8 v5, v5, 0x1

    const/16 v1, 0xd

    aput-byte v1, v4, v0

    .line 700
    :cond_15b
    move v0, v5

    add-int/lit8 v5, v5, 0x1

    const/16 v1, 0xa

    aput-byte v1, v4, v0

    .line 702
    .end local v9    # "t":I
    :cond_162
    goto/16 :goto_1fd

    :cond_164
    iget v0, p0, Lnet/hockeyapp/android/utils/Base64$Encoder;->tailLen:I

    sub-int v0, v7, v0

    add-int/lit8 v1, p3, -0x2

    if-ne v0, v1, :cond_1e1

    .line 703
    const/4 v9, 0x0

    .line 704
    .local v9, "t":I
    iget v0, p0, Lnet/hockeyapp/android/utils/Base64$Encoder;->tailLen:I

    const/4 v1, 0x1

    if-le v0, v1, :cond_17b

    iget-object v0, p0, Lnet/hockeyapp/android/utils/Base64$Encoder;->tail:[B

    move v1, v9

    add-int/lit8 v9, v9, 0x1

    const/4 v1, 0x0

    aget-byte v0, v0, v1

    goto :goto_180

    :cond_17b
    move v0, v7

    add-int/lit8 v7, v7, 0x1

    aget-byte v0, p1, v0

    :goto_180
    and-int/lit16 v0, v0, 0xff

    shl-int/lit8 v0, v0, 0xa

    iget v1, p0, Lnet/hockeyapp/android/utils/Base64$Encoder;->tailLen:I

    if-lez v1, :cond_190

    iget-object v1, p0, Lnet/hockeyapp/android/utils/Base64$Encoder;->tail:[B

    move v2, v9

    add-int/lit8 v9, v9, 0x1

    aget-byte v1, v1, v2

    goto :goto_195

    :cond_190
    move v1, v7

    add-int/lit8 v7, v7, 0x1

    aget-byte v1, p1, v1

    :goto_195
    and-int/lit16 v1, v1, 0xff

    shl-int/lit8 v1, v1, 0x2

    or-int v8, v0, v1

    .line 706
    iget v0, p0, Lnet/hockeyapp/android/utils/Base64$Encoder;->tailLen:I

    sub-int/2addr v0, v9

    iput v0, p0, Lnet/hockeyapp/android/utils/Base64$Encoder;->tailLen:I

    .line 707
    move v0, v5

    add-int/lit8 v5, v5, 0x1

    shr-int/lit8 v1, v8, 0xc

    and-int/lit8 v1, v1, 0x3f

    aget-byte v1, v3, v1

    aput-byte v1, v4, v0

    .line 708
    move v0, v5

    add-int/lit8 v5, v5, 0x1

    shr-int/lit8 v1, v8, 0x6

    and-int/lit8 v1, v1, 0x3f

    aget-byte v1, v3, v1

    aput-byte v1, v4, v0

    .line 709
    move v0, v5

    add-int/lit8 v5, v5, 0x1

    and-int/lit8 v1, v8, 0x3f

    aget-byte v1, v3, v1

    aput-byte v1, v4, v0

    .line 710
    iget-boolean v0, p0, Lnet/hockeyapp/android/utils/Base64$Encoder;->do_padding:Z

    if-eqz v0, :cond_1ca

    .line 711
    move v0, v5

    add-int/lit8 v5, v5, 0x1

    const/16 v1, 0x3d

    aput-byte v1, v4, v0

    .line 713
    :cond_1ca
    iget-boolean v0, p0, Lnet/hockeyapp/android/utils/Base64$Encoder;->do_newline:Z

    if-eqz v0, :cond_1e0

    .line 714
    iget-boolean v0, p0, Lnet/hockeyapp/android/utils/Base64$Encoder;->do_cr:Z

    if-eqz v0, :cond_1d9

    move v0, v5

    add-int/lit8 v5, v5, 0x1

    const/16 v1, 0xd

    aput-byte v1, v4, v0

    .line 715
    :cond_1d9
    move v0, v5

    add-int/lit8 v5, v5, 0x1

    const/16 v1, 0xa

    aput-byte v1, v4, v0

    .line 717
    .end local v9    # "t":I
    :cond_1e0
    goto :goto_1fd

    :cond_1e1
    iget-boolean v0, p0, Lnet/hockeyapp/android/utils/Base64$Encoder;->do_newline:Z

    if-eqz v0, :cond_1fd

    if-lez v5, :cond_1fd

    const/16 v0, 0x13

    if-eq v6, v0, :cond_1fd

    .line 718
    iget-boolean v0, p0, Lnet/hockeyapp/android/utils/Base64$Encoder;->do_cr:Z

    if-eqz v0, :cond_1f6

    move v0, v5

    add-int/lit8 v5, v5, 0x1

    const/16 v1, 0xd

    aput-byte v1, v4, v0

    .line 719
    :cond_1f6
    move v0, v5

    add-int/lit8 v5, v5, 0x1

    const/16 v1, 0xa

    aput-byte v1, v4, v0

    .line 722
    :cond_1fd
    :goto_1fd
    sget-boolean v0, Lnet/hockeyapp/android/utils/Base64$Encoder;->$assertionsDisabled:Z

    if-nez v0, :cond_20b

    iget v0, p0, Lnet/hockeyapp/android/utils/Base64$Encoder;->tailLen:I

    if-eqz v0, :cond_20b

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 723
    :cond_20b
    sget-boolean v0, Lnet/hockeyapp/android/utils/Base64$Encoder;->$assertionsDisabled:Z

    if-nez v0, :cond_246

    if-eq v7, p3, :cond_246

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 728
    :cond_217
    add-int/lit8 v0, p3, -0x1

    if-ne v7, v0, :cond_228

    .line 729
    iget-object v0, p0, Lnet/hockeyapp/android/utils/Base64$Encoder;->tail:[B

    iget v1, p0, Lnet/hockeyapp/android/utils/Base64$Encoder;->tailLen:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lnet/hockeyapp/android/utils/Base64$Encoder;->tailLen:I

    aget-byte v2, p1, v7

    aput-byte v2, v0, v1

    goto :goto_246

    .line 730
    :cond_228
    add-int/lit8 v0, p3, -0x2

    if-ne v7, v0, :cond_246

    .line 731
    iget-object v0, p0, Lnet/hockeyapp/android/utils/Base64$Encoder;->tail:[B

    iget v1, p0, Lnet/hockeyapp/android/utils/Base64$Encoder;->tailLen:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lnet/hockeyapp/android/utils/Base64$Encoder;->tailLen:I

    aget-byte v2, p1, v7

    aput-byte v2, v0, v1

    .line 732
    iget-object v0, p0, Lnet/hockeyapp/android/utils/Base64$Encoder;->tail:[B

    iget v1, p0, Lnet/hockeyapp/android/utils/Base64$Encoder;->tailLen:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lnet/hockeyapp/android/utils/Base64$Encoder;->tailLen:I

    add-int/lit8 v2, v7, 0x1

    aget-byte v2, p1, v2

    aput-byte v2, v0, v1

    .line 736
    :cond_246
    :goto_246
    iput v5, p0, Lnet/hockeyapp/android/utils/Base64$Encoder;->op:I

    .line 737
    iput v6, p0, Lnet/hockeyapp/android/utils/Base64$Encoder;->count:I

    .line 739
    const/4 v0, 0x1

    return v0

    :pswitch_data_24c
    .packed-switch 0x0
        :pswitch_11
        :pswitch_12
        :pswitch_36
    .end packed-switch
.end method

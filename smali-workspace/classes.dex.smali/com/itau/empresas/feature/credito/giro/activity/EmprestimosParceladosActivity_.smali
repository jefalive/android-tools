.class public final Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity_;
.super Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity;
.source "EmprestimosParceladosActivity_.java"

# interfaces
.implements Lorg/androidannotations/api/view/HasViews;
.implements Lorg/androidannotations/api/view/OnViewChangedListener;


# instance fields
.field private final onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;


# direct methods
.method public constructor <init>()V
    .registers 2

    .line 35
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity;-><init>()V

    .line 39
    new-instance v0, Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-direct {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    return-void
.end method

.method static synthetic access$001(Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity_;)V
    .registers 1
    .param p0, "x0"    # Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity_;

    .line 35
    invoke-super {p0}, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity;->mostraDialogoDeProgresso()V

    return-void
.end method

.method static synthetic access$101(Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity_;)V
    .registers 1
    .param p0, "x0"    # Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity_;

    .line 35
    invoke-super {p0}, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity;->escondeDialogoDeProgresso()V

    return-void
.end method

.method private init_(Landroid/os/Bundle;)V
    .registers 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 51
    new-instance v0, Lcom/itau/empresas/Preferences_;

    invoke-direct {v0, p0}, Lcom/itau/empresas/Preferences_;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity_;->prefs:Lcom/itau/empresas/Preferences_;

    .line 52
    invoke-static {p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->registerOnViewChangedListener(Lorg/androidannotations/api/view/OnViewChangedListener;)V

    .line 53
    invoke-static {}, Lcom/itau/empresas/CustomApplication_;->getInstance()Lcom/itau/empresas/CustomApplication;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity_;->application:Lcom/itau/empresas/CustomApplication;

    .line 54
    invoke-static {p0}, Lcom/itau/empresas/feature/credito/giro/controller/EmprestimosParceladosSimulacaoPropostaController_;->getInstance_(Landroid/content/Context;)Lcom/itau/empresas/feature/credito/giro/controller/EmprestimosParceladosSimulacaoPropostaController_;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity_;->simulacaoPropostaController:Lcom/itau/empresas/feature/credito/giro/controller/EmprestimosParceladosSimulacaoPropostaController;

    .line 55
    invoke-static {p0}, Lcom/itau/empresas/feature/credito/giro/controller/EmprestimosParceladosSimulacaoController_;->getInstance_(Landroid/content/Context;)Lcom/itau/empresas/feature/credito/giro/controller/EmprestimosParceladosSimulacaoController_;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity_;->simulacaoController:Lcom/itau/empresas/feature/credito/giro/controller/EmprestimosParceladosSimulacaoController;

    .line 56
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity_;->afterInject()V

    .line 57
    return-void
.end method


# virtual methods
.method public escondeDialogoDeProgresso()V
    .registers 5

    .line 115
    const-string v0, ""

    new-instance v1, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity_$2;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity_$2;-><init>(Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity_;)V

    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3}, Lorg/androidannotations/api/UiThreadExecutor;->runTask(Ljava/lang/String;Ljava/lang/Runnable;J)V

    .line 123
    return-void
.end method

.method public mostraDialogoDeProgresso()V
    .registers 5
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "CommitTransaction"
        }
    .end annotation

    .line 103
    const-string v0, ""

    new-instance v1, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity_$1;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity_$1;-><init>(Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity_;)V

    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3}, Lorg/androidannotations/api/UiThreadExecutor;->runTask(Ljava/lang/String;Ljava/lang/Runnable;J)V

    .line 111
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 43
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-static {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    move-result-object v1

    .line 44
    .local v1, "previousNotifier":Lorg/androidannotations/api/view/OnViewChangedNotifier;
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity_;->init_(Landroid/os/Bundle;)V

    .line 45
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity;->onCreate(Landroid/os/Bundle;)V

    .line 46
    invoke-static {v1}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 47
    const v0, 0x7f03002c

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity_;->setContentView(I)V

    .line 48
    return-void
.end method

.method public onViewChanged(Lorg/androidannotations/api/view/HasViews;)V
    .registers 3
    .param p1, "hasViews"    # Lorg/androidannotations/api/view/HasViews;

    .line 91
    const v0, 0x7f0e0131

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity_;->root:Landroid/widget/LinearLayout;

    .line 92
    const v0, 0x7f0e00b6

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/Toolbar;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity_;->toolBar:Landroid/support/v7/widget/Toolbar;

    .line 93
    const v0, 0x7f0e0133

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/ui/util/CustomViewPager;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity_;->creditoSimplificadoViewpager:Lcom/itau/empresas/ui/util/CustomViewPager;

    .line 94
    const v0, 0x7f0e0132

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/ui/util/acessibilidade/TabLayout;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity_;->creditoSimplificadoTab:Lcom/itau/empresas/ui/util/acessibilidade/TabLayout;

    .line 95
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity_;->aoCarregarElementosDeTela()V

    .line 96
    return-void
.end method

.method public setContentView(I)V
    .registers 3
    .param p1, "layoutResID"    # I

    .line 61
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity;->setContentView(I)V

    .line 62
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 63
    return-void
.end method

.method public setContentView(Landroid/view/View;)V
    .registers 3
    .param p1, "view"    # Landroid/view/View;

    .line 73
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity;->setContentView(Landroid/view/View;)V

    .line 74
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 75
    return-void
.end method

.method public setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .registers 4
    .param p1, "view"    # Landroid/view/View;
    .param p2, "params"    # Landroid/view/ViewGroup$LayoutParams;

    .line 67
    invoke-super {p0, p1, p2}, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity;->setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 68
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 69
    return-void
.end method

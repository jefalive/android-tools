.class public Lcom/itau/empresas/ui/util/PoyntUtils;
.super Ljava/lang/Object;
.source "PoyntUtils.java"


# direct methods
.method public static isPoynt()Z
    .registers 2

    .line 17
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "Poynt"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    return v0
.end method

.method public static onCreateOptionsMenu(Landroid/app/Activity;Landroid/view/Menu;)Z
    .registers 6
    .param p0, "activity"    # Landroid/app/Activity;
    .param p1, "menu"    # Landroid/view/Menu;

    .line 21
    invoke-static {}, Lcom/itau/empresas/ui/util/PoyntUtils;->isPoynt()Z

    move-result v0

    if-eqz v0, :cond_28

    .line 22
    invoke-virtual {p0}, Landroid/app/Activity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f0f0005

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 24
    const v0, 0x7f0e06c6

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    .line 25
    .local v2, "itemMenuPesquisa":Landroid/view/MenuItem;
    const/4 v0, 0x0

    invoke-interface {v2, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 27
    const v0, 0x7f0e06c5

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v3

    .line 28
    .local v3, "itemMenuFiltro":Landroid/view/MenuItem;
    const/4 v0, 0x0

    invoke-interface {v3, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 29
    const/4 v0, 0x1

    return v0

    .line 31
    .end local v2    # "itemMenuPesquisa":Landroid/view/MenuItem;
    .end local v3    # "itemMenuFiltro":Landroid/view/MenuItem;
    :cond_28
    const/4 v0, 0x0

    return v0
.end method

.method public static onOptionsItemSelected(Lcom/itau/empresas/ui/activity/BaseActivity;Landroid/view/MenuItem;)Z
    .registers 5
    .param p0, "activity"    # Lcom/itau/empresas/ui/activity/BaseActivity;
    .param p1, "item"    # Landroid/view/MenuItem;

    .line 35
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x7f0e06cf

    if-ne v0, v1, :cond_15

    .line 36
    invoke-virtual {p0}, Lcom/itau/empresas/ui/activity/BaseActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/itau/empresas/CustomApplication;

    .line 37
    .local v2, "application":Lcom/itau/empresas/CustomApplication;
    invoke-static {v2, p0}, Lcom/itau/empresas/ui/util/ViewUtils;->aoClicarBotaoSairMenu(Lcom/itau/empresas/CustomApplication;Lcom/itau/empresas/ui/activity/BaseActivity;)V

    .line 38
    const/4 v0, 0x1

    return v0

    .line 40
    .end local v2    # "application":Lcom/itau/empresas/CustomApplication;
    :cond_15
    const/4 v0, 0x0

    return v0
.end method

.class public final Lti/j;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Lorg/xml/sax/ErrorHandler;

.field private static final c:[B

.field private static d:I


# instance fields
.field private final b:Ljavax/xml/parsers/DocumentBuilder;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .line 24
    const/16 v0, 0x75

    new-array v0, v0, [B

    fill-array-data v0, :array_16

    sput-object v0, Lti/j;->c:[B

    const/16 v0, 0xd9

    sput v0, Lti/j;->d:I

    new-instance v0, Lti/k;

    invoke-direct {v0}, Lti/k;-><init>()V

    sput-object v0, Lti/j;->a:Lorg/xml/sax/ErrorHandler;

    return-void

    nop

    :array_16
    .array-data 1
        0x22t
        -0x6t
        0x11t
        0xat
        0x21t
        0x11t
        -0xdt
        -0x6t
        0x2t
        -0x1ct
        0x1bt
        0x13t
        0xct
        -0x8t
        0x2ct
        0x6t
        -0x9t
        -0x8t
        -0x44t
        0x4et
        0x1t
        0x5t
        -0x54t
        0x50t
        -0xft
        0x11t
        0x1t
        -0xet
        0x13t
        0xet
        -0x3t
        -0x1t
        -0x1t
        0x5t
        -0xet
        -0x1ft
        0x29t
        0x3t
        -0x5t
        -0xct
        0x13t
        0x12t
        0xdt
        -0x9t
        -0x8t
        0xbt
        -0x1et
        0x21t
        0x1et
        -0x12t
        0x6t
        -0x2t
        0x21t
        0x11t
        -0xdt
        -0x6t
        0x2t
        -0x7t
        0x9t
        -0x7t
        0xdt
        -0xct
        0x1dt
        0x13t
        -0x13t
        0x12t
        -0x2t
        0xft
        -0xdt
        0xft
        -0xbt
        0x4t
        -0x8t
        0x1at
        -0x16t
        0x5t
        0x6t
        -0xft
        0xdt
        0x4t
        -0x15t
        0xbt
        -0x1t
        -0xet
        -0x39t
        0xbt
        0x21t
        -0x4ft
        0x53t
        0x2t
        -0x5t
        0x0t
        -0x1t
        0x3t
        0x2t
        -0xft
        -0x1t
        -0x44t
        0x24t
        0xbt
        -0x2t
        -0x2dt
        0x50t
        -0xft
        0x11t
        0x1t
        -0xet
        0xdt
        0xbt
        0xbt
        0x9t
        -0x10t
        -0xet
        -0x2t
        0xft
        -0xdt
        0xft
    .end array-data
.end method

.method public constructor <init>()V
    .registers 5

    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    :try_start_3
    invoke-static {}, Ljavax/xml/parsers/DocumentBuilderFactory;->newInstance()Ljavax/xml/parsers/DocumentBuilderFactory;

    move-result-object v0

    invoke-virtual {v0}, Ljavax/xml/parsers/DocumentBuilderFactory;->newDocumentBuilder()Ljavax/xml/parsers/DocumentBuilder;

    move-result-object v0

    iput-object v0, p0, Lti/j;->b:Ljavax/xml/parsers/DocumentBuilder;

    .line 45
    iget-object v0, p0, Lti/j;->b:Ljavax/xml/parsers/DocumentBuilder;

    sget-object v1, Lti/j;->a:Lorg/xml/sax/ErrorHandler;

    invoke-virtual {v0, v1}, Ljavax/xml/parsers/DocumentBuilder;->setErrorHandler(Lorg/xml/sax/ErrorHandler;)V
    :try_end_14
    .catch Ljavax/xml/parsers/ParserConfigurationException; {:try_start_3 .. :try_end_14} :catch_15

    .line 48
    return-void

    .line 46
    .line 47
    :catch_15
    new-instance v0, Lbr/com/itau/security/commons/exception/CommonSecurityException;

    sget-object v1, Lti/j;->c:[B

    const/16 v2, 0x49

    aget-byte v1, v1, v2

    int-to-byte v1, v1

    sget-object v2, Lti/j;->c:[B

    const/16 v3, 0x5b

    aget-byte v2, v2, v3

    int-to-byte v2, v2

    const/16 v3, 0x27

    invoke-static {v1, v3, v2}, Lti/j;->a(BBS)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lbr/com/itau/security/commons/exception/CommonSecurityException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private static a(BBS)Ljava/lang/String;
    .registers 9

    rsub-int/lit8 p2, p2, 0x17

    rsub-int/lit8 p0, p0, 0x6f

    rsub-int/lit8 p1, p1, 0x75

    const/4 v4, 0x0

    sget-object v5, Lti/j;->c:[B

    new-instance v0, Ljava/lang/String;

    new-array v1, p2, [B

    add-int/lit8 p2, p2, -0x1

    if-nez v5, :cond_15

    move v2, p0

    move v3, p2

    :goto_13
    add-int p1, v2, v3

    :cond_15
    int-to-byte v2, p1

    aput-byte v2, v1, v4

    if-ne v4, p2, :cond_23

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Ljava/lang/String;-><init>([BI)V

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_23
    add-int/lit8 v4, v4, 0x1

    move v2, p1

    add-int/lit8 p0, p0, 0x1

    aget-byte v3, v5, p0

    goto :goto_13
.end method

.method private static a(Lorg/w3c/dom/Element;Ljava/lang/String;)Ljava/lang/String;
    .registers 5

    .line 134
    invoke-interface {p0}, Lorg/w3c/dom/Element;->getChildNodes()Lorg/w3c/dom/NodeList;

    move-result-object p0

    .line 136
    const/4 v1, 0x0

    :goto_5
    invoke-interface {p0}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v0

    if-ge v1, v0, :cond_27

    .line 138
    invoke-interface {p0, v1}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v2

    .line 140
    instance-of v0, v2, Lorg/w3c/dom/Element;

    if-eqz v0, :cond_24

    .line 141
    check-cast v2, Lorg/w3c/dom/Element;

    .line 143
    invoke-interface {v2}, Lorg/w3c/dom/Element;->getTagName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_24

    .line 144
    invoke-interface {v2}, Lorg/w3c/dom/Element;->getTextContent()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 136
    :cond_24
    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    .line 147
    :cond_27
    const/4 v0, 0x0

    return-object v0
.end method

.method private static a(Lorg/w3c/dom/Element;Lti/b;)V
    .registers 11

    .line 87
    invoke-interface {p0}, Lorg/w3c/dom/Element;->getChildNodes()Lorg/w3c/dom/NodeList;

    move-result-object p0

    .line 89
    const/4 v4, 0x0

    :goto_5
    invoke-interface {p0}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v0

    if-ge v4, v0, :cond_137

    .line 91
    invoke-interface {p0, v4}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v5

    .line 93
    instance-of v0, v5, Lorg/w3c/dom/Element;

    if-eqz v0, :cond_133

    .line 94
    check-cast v5, Lorg/w3c/dom/Element;

    .line 96
    invoke-interface {v5}, Lorg/w3c/dom/Element;->getTagName()Ljava/lang/String;

    move-result-object v6

    .line 97
    sget v0, Lti/j;->d:I

    and-int/lit16 v0, v0, 0x160

    int-to-byte v0, v0

    ushr-int/lit8 v1, v0, 0x1

    int-to-byte v1, v1

    sget-object v2, Lti/j;->c:[B

    const/16 v3, 0x29

    aget-byte v2, v2, v3

    int-to-byte v2, v2

    invoke-static {v0, v1, v2}, Lti/j;->a(BBS)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8e

    .line 98
    move-object v6, p1

    .line 1116
    invoke-interface {v5}, Lorg/w3c/dom/Element;->getChildNodes()Lorg/w3c/dom/NodeList;

    move-result-object v5

    .line 1118
    const/4 v7, 0x0

    :goto_38
    invoke-interface {v5}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v0

    if-ge v7, v0, :cond_8c

    .line 1119
    invoke-interface {v5, v7}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v8

    .line 1121
    instance-of v0, v8, Lorg/w3c/dom/Element;

    if-eqz v0, :cond_89

    .line 1122
    check-cast v8, Lorg/w3c/dom/Element;

    .line 1124
    sget-object v0, Lti/j;->c:[B

    const/16 v1, 0x16

    aget-byte v0, v0, v1

    neg-int v0, v0

    int-to-byte v0, v0

    sget-object v1, Lti/j;->c:[B

    const/16 v2, 0x62

    aget-byte v1, v1, v2

    add-int/lit8 v1, v1, -0x1

    int-to-byte v1, v1

    sget-object v2, Lti/j;->c:[B

    const/16 v3, 0x3a

    aget-byte v2, v2, v3

    int-to-byte v2, v2

    invoke-static {v0, v1, v2}, Lti/j;->a(BBS)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v8}, Lorg/w3c/dom/Element;->getTagName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_89

    .line 1125
    sget-object v0, Lti/j;->c:[B

    const/16 v1, 0x3a

    aget-byte v0, v0, v1

    int-to-byte v0, v0

    sget-object v1, Lti/j;->c:[B

    const/4 v2, 0x2

    aget-byte v1, v1, v2

    int-to-byte v1, v1

    const/16 v2, 0x37

    invoke-static {v2, v0, v1}, Lti/j;->a(BBS)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v8, v0}, Lorg/w3c/dom/Element;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2039
    iput-object v0, v6, Lti/b;->b:Ljava/lang/String;

    .line 1126
    goto/16 :goto_133

    .line 1118
    :cond_89
    add-int/lit8 v7, v7, 0x1

    goto :goto_38

    .line 98
    :cond_8c
    goto/16 :goto_133

    .line 100
    :cond_8e
    sget-object v0, Lti/j;->c:[B

    const/16 v1, 0xb

    aget-byte v0, v0, v1

    int-to-byte v0, v0

    const/16 v1, 0x32

    const/16 v2, 0x31

    invoke-static {v1, v2, v0}, Lti/j;->a(BBS)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_133

    .line 102
    sget-object v0, Lti/j;->c:[B

    const/16 v1, 0xb

    aget-byte v0, v0, v1

    int-to-byte v0, v0

    const/16 v1, 0x65

    const/16 v2, 0x27

    invoke-static {v1, v2, v0}, Lti/j;->a(BBS)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v5, v0}, Lorg/w3c/dom/Element;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 104
    sget-object v0, Lti/j;->c:[B

    const/16 v1, 0x5b

    aget-byte v0, v0, v1

    int-to-byte v0, v0

    sget-object v1, Lti/j;->c:[B

    const/4 v2, 0x0

    aget-byte v1, v1, v2

    int-to-byte v1, v1

    sget-object v2, Lti/j;->c:[B

    const/4 v3, 0x2

    aget-byte v2, v2, v3

    int-to-byte v2, v2

    invoke-static {v0, v1, v2}, Lti/j;->a(BBS)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f4

    .line 105
    sget-object v0, Lti/j;->c:[B

    const/16 v1, 0x47

    aget-byte v0, v0, v1

    int-to-byte v0, v0

    sget-object v1, Lti/j;->c:[B

    const/16 v2, 0x23

    aget-byte v1, v1, v2

    neg-int v1, v1

    int-to-byte v1, v1

    sget-object v2, Lti/j;->c:[B

    const/16 v3, 0x29

    aget-byte v2, v2, v3

    int-to-byte v2, v2

    invoke-static {v0, v1, v2}, Lti/j;->a(BBS)Ljava/lang/String;

    move-result-object v0

    invoke-static {v5, v0}, Lti/j;->a(Lorg/w3c/dom/Element;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 3023
    iput-object v0, p1, Lti/b;->c:Ljava/lang/String;

    .line 105
    goto :goto_133

    .line 107
    :cond_f4
    sget-object v0, Lti/j;->c:[B

    const/16 v1, 0x24

    aget-byte v0, v0, v1

    add-int/lit8 v0, v0, 0x1

    int-to-byte v0, v0

    sget-object v1, Lti/j;->c:[B

    const/4 v2, 0x4

    aget-byte v1, v1, v2

    int-to-byte v1, v1

    sget-object v2, Lti/j;->c:[B

    const/4 v3, 0x3

    aget-byte v2, v2, v3

    int-to-byte v2, v2

    invoke-static {v0, v1, v2}, Lti/j;->a(BBS)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_133

    .line 108
    sget-object v0, Lti/j;->c:[B

    const/16 v1, 0x47

    aget-byte v0, v0, v1

    int-to-byte v0, v0

    sget-object v1, Lti/j;->c:[B

    const/16 v2, 0x23

    aget-byte v1, v1, v2

    neg-int v1, v1

    int-to-byte v1, v1

    sget-object v2, Lti/j;->c:[B

    const/16 v3, 0x29

    aget-byte v2, v2, v3

    int-to-byte v2, v2

    invoke-static {v0, v1, v2}, Lti/j;->a(BBS)Ljava/lang/String;

    move-result-object v0

    invoke-static {v5, v0}, Lti/j;->a(Lorg/w3c/dom/Element;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 3031
    iput-object v0, p1, Lti/b;->d:Ljava/lang/String;

    .line 89
    :cond_133
    :goto_133
    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_5

    .line 112
    :cond_137
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Lti/b;
    .registers 11

    .line 55
    :try_start_0
    iget-object v0, p0, Lti/j;->b:Ljavax/xml/parsers/DocumentBuilder;

    new-instance v1, Ljava/io/ByteArrayInputStream;

    sget-object v2, Lti/j;->c:[B

    const/16 v3, 0x30

    aget-byte v2, v2, v3

    int-to-byte v2, v2

    sget-object v3, Lti/j;->c:[B

    const/16 v4, 0x5b

    aget-byte v3, v3, v4

    int-to-byte v3, v3

    sget-object v4, Lti/j;->c:[B

    const/16 v5, 0x29

    aget-byte v4, v4, v5

    int-to-byte v4, v4

    invoke-static {v2, v3, v4}, Lti/j;->a(BBS)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    sget-object v2, Lti/j;->c:[B

    const/16 v3, 0x30

    aget-byte v2, v2, v3

    int-to-byte v2, v2

    sget-object v3, Lti/j;->c:[B

    const/16 v4, 0x5b

    aget-byte v3, v3, v4

    int-to-byte v3, v3

    sget-object v4, Lti/j;->c:[B

    const/16 v5, 0x29

    aget-byte v4, v4, v5

    int-to-byte v4, v4

    invoke-static {v2, v3, v4}, Lti/j;->a(BBS)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljavax/xml/parsers/DocumentBuilder;->parse(Ljava/io/InputStream;Ljava/lang/String;)Lorg/w3c/dom/Document;
    :try_end_40
    .catch Lorg/xml/sax/SAXException; {:try_start_0 .. :try_end_40} :catch_42
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_40} :catch_42

    move-result-object p1

    .line 58
    goto :goto_58

    .line 56
    .line 57
    :catch_42
    new-instance v0, Lbr/com/itau/security/commons/exception/UnsupportedEncodingException;

    sget-object v1, Lti/j;->c:[B

    const/16 v2, 0xd

    aget-byte v1, v1, v2

    neg-int v1, v1

    int-to-byte v1, v1

    const/16 v2, 0x62

    const/16 v3, 0x32

    invoke-static {v2, v3, v1}, Lti/j;->a(BBS)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lbr/com/itau/security/commons/exception/UnsupportedEncodingException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 60
    :goto_58
    sget-object v0, Lti/j;->c:[B

    const/4 v1, 0x2

    aget-byte v0, v0, v1

    int-to-byte v0, v0

    const/16 v1, 0x3c

    const/16 v2, 0x31

    invoke-static {v1, v2, v0}, Lti/j;->a(BBS)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lorg/w3c/dom/Document;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object p1

    .line 62
    if-nez p1, :cond_6e

    .line 63
    const/4 v0, 0x0

    return-object v0

    .line 65
    :cond_6e
    new-instance v6, Lti/b;

    invoke-direct {v6}, Lti/b;-><init>()V

    .line 67
    const/4 v0, 0x0

    invoke-interface {p1, v0}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v0

    invoke-interface {v0}, Lorg/w3c/dom/Node;->getChildNodes()Lorg/w3c/dom/NodeList;

    move-result-object p1

    .line 68
    const/4 v7, 0x0

    :goto_7d
    invoke-interface {p1}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v0

    if-ge v7, v0, :cond_e6

    .line 69
    invoke-interface {p1, v7}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v8

    .line 71
    instance-of v0, v8, Lorg/w3c/dom/Element;

    if-eqz v0, :cond_e2

    .line 73
    check-cast v8, Lorg/w3c/dom/Element;

    .line 74
    sget v0, Lti/j;->d:I

    ushr-int/lit8 v0, v0, 0x1

    int-to-byte v0, v0

    sget-object v1, Lti/j;->c:[B

    const/16 v2, 0x43

    aget-byte v1, v1, v2

    int-to-byte v1, v1

    const/16 v2, 0x31

    invoke-static {v0, v2, v1}, Lti/j;->a(BBS)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v8}, Lorg/w3c/dom/Element;->getTagName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c3

    .line 75
    sget-object v0, Lti/j;->c:[B

    const/4 v1, 0x0

    aget-byte v0, v0, v1

    int-to-byte v0, v0

    sget-object v1, Lti/j;->c:[B

    const/16 v2, 0x43

    aget-byte v1, v1, v2

    int-to-byte v1, v1

    const/16 v2, 0x47

    invoke-static {v2, v0, v1}, Lti/j;->a(BBS)Ljava/lang/String;

    move-result-object v0

    invoke-static {v8, v0}, Lti/j;->a(Lorg/w3c/dom/Element;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1015
    iput-object v0, v6, Lti/b;->a:Ljava/lang/String;

    .line 75
    goto :goto_e2

    .line 77
    :cond_c3
    sget-object v0, Lti/j;->c:[B

    const/4 v1, 0x0

    aget-byte v0, v0, v1

    int-to-byte v0, v0

    sget-object v1, Lti/j;->c:[B

    const/4 v2, 0x2

    aget-byte v1, v1, v2

    int-to-byte v1, v1

    const/16 v2, 0x2f

    invoke-static {v2, v0, v1}, Lti/j;->a(BBS)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v8}, Lorg/w3c/dom/Element;->getTagName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e2

    .line 78
    invoke-static {v8, v6}, Lti/j;->a(Lorg/w3c/dom/Element;Lti/b;)V

    .line 68
    :cond_e2
    :goto_e2
    add-int/lit8 v7, v7, 0x1

    goto/16 :goto_7d

    .line 82
    :cond_e6
    return-object v6
.end method

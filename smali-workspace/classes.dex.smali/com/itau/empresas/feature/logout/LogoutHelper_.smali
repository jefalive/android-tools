.class public final Lcom/itau/empresas/feature/logout/LogoutHelper_;
.super Lcom/itau/empresas/feature/logout/LogoutHelper;
.source "LogoutHelper_.java"


# instance fields
.field private context_:Landroid/content/Context;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .registers 2
    .param p1, "context"    # Landroid/content/Context;

    .line 21
    invoke-direct {p0}, Lcom/itau/empresas/feature/logout/LogoutHelper;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/itau/empresas/feature/logout/LogoutHelper_;->context_:Landroid/content/Context;

    .line 23
    invoke-direct {p0}, Lcom/itau/empresas/feature/logout/LogoutHelper_;->init_()V

    .line 24
    return-void
.end method

.method public static getInstance_(Landroid/content/Context;)Lcom/itau/empresas/feature/logout/LogoutHelper_;
    .registers 2
    .param p0, "context"    # Landroid/content/Context;

    .line 27
    new-instance v0, Lcom/itau/empresas/feature/logout/LogoutHelper_;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/logout/LogoutHelper_;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method private init_()V
    .registers 2

    .line 31
    invoke-static {}, Lcom/itau/empresas/CustomApplication_;->getInstance()Lcom/itau/empresas/CustomApplication;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/logout/LogoutHelper_;->application:Lcom/itau/empresas/CustomApplication;

    .line 32
    return-void
.end method

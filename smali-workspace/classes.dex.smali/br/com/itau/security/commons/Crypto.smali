.class public final Lbr/com/itau/security/commons/Crypto;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Ljava/security/SecureRandom;

.field private static final b:[B

.field private static c:I


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .line 23
    const/16 v0, 0xa4

    new-array v0, v0, [B

    fill-array-data v0, :array_16

    sput-object v0, Lbr/com/itau/security/commons/Crypto;->b:[B

    const/16 v0, 0x7d

    sput v0, Lbr/com/itau/security/commons/Crypto;->c:I

    new-instance v0, Ljava/security/SecureRandom;

    invoke-direct {v0}, Ljava/security/SecureRandom;-><init>()V

    sput-object v0, Lbr/com/itau/security/commons/Crypto;->a:Ljava/security/SecureRandom;

    return-void

    nop

    :array_16
    .array-data 1
        0x75t
        0x7et
        -0x2t
        0x71t
        -0xbt
        -0x7t
        -0x14t
        0x4t
        -0x11t
        0x16t
        0x1et
        0xdt
        0x0t
        0x1t
        0x5t
        -0x54t
        0x48t
        0x5t
        -0xct
        0x2t
        -0x4t
        0x14t
        -0xbt
        -0x7t
        0x4t
        0xet
        0x25t
        -0xct
        0x2t
        -0x16t
        -0x9t
        -0xft
        0x1t
        -0x4ft
        0x33t
        -0xbt
        -0x7t
        -0x14t
        0x4t
        0x25t
        -0xct
        0x2t
        -0x10t
        -0xbt
        -0x7t
        -0xft
        0x3t
        0x1t
        -0xet
        0x9t
        -0x7t
        0x2t
        -0x14t
        0x25t
        0x12t
        0xbt
        -0xct
        -0x20t
        0x25t
        -0xct
        0x2t
        -0x10t
        -0xbt
        -0x7t
        -0x10t
        0x2ct
        0x6t
        -0x9t
        -0x8t
        0xat
        -0x47t
        0x4dt
        -0x54t
        0x44t
        0xbt
        -0x4ft
        0x43t
        0xft
        0x7t
        -0x9t
        0x4t
        0x25t
        -0xct
        0x2t
        -0x10t
        -0xbt
        -0x7t
        -0x10t
        0x1t
        -0x4ft
        0x33t
        -0xbt
        -0x7t
        -0x14t
        0x5t
        0x3t
        0x1t
        -0xbt
        -0x7t
        -0x14t
        0x5t
        0x3t
        0x1t
        0x4t
        0xet
        -0x24t
        0x14t
        -0x1t
        0x1t
        -0x14t
        0x21t
        -0x5t
        -0x8t
        0x10t
        -0x1et
        0x1bt
        0x11t
        0x3t
        0x0t
        0x5t
        0x5t
        -0x7t
        0x4t
        0xet
        -0x24t
        0x14t
        -0x1t
        0x1t
        -0x14t
        0x21t
        -0x5t
        -0x8t
        0x10t
        -0x1ct
        0x19t
        0x11t
        0x3t
        0x0t
        0x5t
        0x5t
        -0x7t
        0x1bt
        0x8t
        0x3t
        -0x7t
        -0x1t
        -0x44t
        0x54t
        -0x5t
        -0x4ft
        0x49t
        0x5t
        -0x5t
        0xbt
        -0xbt
        -0x8t
        0xbt
        -0x3t
        0x11t
        -0x15t
        -0x45t
        0x4bt
        -0x6t
        0x14t
    .end array-data
.end method

.method public constructor <init>()V
    .registers 1

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(SSS)Ljava/lang/String;
    .registers 9

    add-int/lit8 p2, p2, 0x4

    const/4 v5, 0x0

    new-instance v0, Ljava/lang/String;

    add-int/lit8 p1, p1, 0x41

    sget-object v4, Lbr/com/itau/security/commons/Crypto;->b:[B

    rsub-int/lit8 v1, p0, 0x18

    move p0, v1

    new-array v1, v1, [B

    if-nez v4, :cond_16

    move v2, p2

    move v3, p1

    :goto_12
    add-int/lit8 p2, p2, 0x1

    add-int p1, v2, v3

    :cond_16
    move v2, v5

    int-to-byte v3, p1

    add-int/lit8 v5, v5, 0x1

    aput-byte v3, v1, v2

    if-ne v5, p0, :cond_27

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Ljava/lang/String;-><init>([BI)V

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_27
    move v2, p1

    aget-byte v3, v4, p2

    goto :goto_12
.end method

.method private static a(Ljava/lang/String;[B[B)[B
    .registers 8

    .line 56
    :try_start_0
    invoke-static {p0}, Ljavax/crypto/Mac;->getInstance(Ljava/lang/String;)Ljavax/crypto/Mac;

    move-result-object p0

    .line 57
    new-instance v0, Ljavax/crypto/spec/SecretKeySpec;

    sget-object v1, Lbr/com/itau/security/commons/Crypto;->b:[B

    const/16 v2, 0x9f

    aget-byte v1, v1, v2

    neg-int v1, v1

    int-to-byte v1, v1

    sget-object v2, Lbr/com/itau/security/commons/Crypto;->b:[B

    const/16 v3, 0x74

    aget-byte v2, v2, v3

    int-to-byte v2, v2

    sget-object v3, Lbr/com/itau/security/commons/Crypto;->b:[B

    const/4 v4, 0x7

    aget-byte v3, v3, v4

    int-to-short v3, v3

    invoke-static {v1, v2, v3}, Lbr/com/itau/security/commons/Crypto;->a(SSS)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p1, v1}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    move-object p1, v0

    .line 58
    invoke-virtual {p0, p1}, Ljavax/crypto/Mac;->init(Ljava/security/Key;)V

    .line 59
    invoke-virtual {p0, p2}, Ljavax/crypto/Mac;->doFinal([B)[B
    :try_end_29
    .catch Ljava/security/GeneralSecurityException; {:try_start_0 .. :try_end_29} :catch_2b

    move-result-object v0

    return-object v0

    .line 60
    :catch_2b
    move-exception p0

    .line 61
    new-instance v0, Lbr/com/itau/security/commons/exception/BadCryptoDataException;

    sget-object v1, Lbr/com/itau/security/commons/Crypto;->b:[B

    const/16 v2, 0x31

    aget-byte v1, v1, v2

    int-to-byte v1, v1

    sget-object v2, Lbr/com/itau/security/commons/Crypto;->b:[B

    const/16 v3, 0x13

    aget-byte v2, v2, v3

    int-to-byte v2, v2

    sget-object v3, Lbr/com/itau/security/commons/Crypto;->b:[B

    const/16 v4, 0x42

    aget-byte v3, v3, v4

    int-to-short v3, v3

    invoke-static {v1, v2, v3}, Lbr/com/itau/security/commons/Crypto;->a(SSS)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Lbr/com/itau/security/commons/exception/BadCryptoDataException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v0
.end method

.method private static a([B[B[BILjava/lang/String;Ljava/lang/String;)[B
    .registers 11

    .line 103
    new-instance v0, Ljavax/crypto/spec/SecretKeySpec;

    invoke-direct {v0, p1, p5}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    move-object p1, v0

    .line 104
    new-instance v0, Ljavax/crypto/spec/IvParameterSpec;

    invoke-direct {v0, p2}, Ljavax/crypto/spec/IvParameterSpec;-><init>([B)V

    move-object p2, v0

    .line 107
    :try_start_c
    invoke-static {p4}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;

    move-result-object p4

    .line 108
    invoke-virtual {p4, p3, p1, p2}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;Ljava/security/spec/AlgorithmParameterSpec;)V

    .line 109
    invoke-virtual {p4, p0}, Ljavax/crypto/Cipher;->doFinal([B)[B
    :try_end_16
    .catch Ljava/security/GeneralSecurityException; {:try_start_c .. :try_end_16} :catch_18

    move-result-object v0

    return-object v0

    .line 110
    :catch_18
    move-exception p4

    .line 111
    new-instance v0, Lbr/com/itau/security/commons/exception/BadCryptoDataException;

    sget-object v1, Lbr/com/itau/security/commons/Crypto;->b:[B

    const/16 v2, 0x4e

    aget-byte v1, v1, v2

    int-to-byte v1, v1

    sget-object v2, Lbr/com/itau/security/commons/Crypto;->b:[B

    const/16 v3, 0x13

    aget-byte v2, v2, v3

    int-to-byte v2, v2

    sget v3, Lbr/com/itau/security/commons/Crypto;->c:I

    and-int/lit16 v3, v3, 0xbf

    int-to-short v3, v3

    invoke-static {v1, v2, v3}, Lbr/com/itau/security/commons/Crypto;->a(SSS)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, p4}, Lbr/com/itau/security/commons/exception/BadCryptoDataException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v0
.end method

.method public static hmac_sha1([B[B)[B
    .registers 6

    .line 43
    sget-object v0, Lbr/com/itau/security/commons/Crypto;->b:[B

    const/16 v1, 0x71

    aget-byte v0, v0, v1

    int-to-byte v0, v0

    sget-object v1, Lbr/com/itau/security/commons/Crypto;->b:[B

    const/16 v2, 0x4e

    aget-byte v1, v1, v2

    int-to-byte v1, v1

    sget-object v2, Lbr/com/itau/security/commons/Crypto;->b:[B

    const/16 v3, 0x47

    aget-byte v2, v2, v3

    int-to-short v2, v2

    invoke-static {v0, v1, v2}, Lbr/com/itau/security/commons/Crypto;->a(SSS)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p0, p1}, Lbr/com/itau/security/commons/Crypto;->a(Ljava/lang/String;[B[B)[B

    move-result-object v0

    return-object v0
.end method

.method public static hmac_sha256([B[B)[B
    .registers 5

    .line 51
    sget-object v0, Lbr/com/itau/security/commons/Crypto;->b:[B

    const/16 v1, 0x19

    aget-byte v0, v0, v1

    int-to-byte v0, v0

    sget-object v1, Lbr/com/itau/security/commons/Crypto;->b:[B

    const/16 v2, 0x4e

    aget-byte v1, v1, v2

    int-to-byte v1, v1

    const/16 v2, 0x23

    invoke-static {v0, v1, v2}, Lbr/com/itau/security/commons/Crypto;->a(SSS)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p0, p1}, Lbr/com/itau/security/commons/Crypto;->a(Ljava/lang/String;[B[B)[B

    move-result-object v0

    return-object v0
.end method

.method public static pbkdf2Encrypt(Ljava/lang/String;[BII)[B
    .registers 8

    .line 32
    invoke-virtual {p0}, Ljava/lang/String;->toCharArray()[C

    move-result-object p0

    .line 34
    :try_start_4
    new-instance v0, Ljavax/crypto/spec/PBEKeySpec;

    invoke-direct {v0, p0, p1, p2, p3}, Ljavax/crypto/spec/PBEKeySpec;-><init>([C[BII)V

    move-object p0, v0

    .line 35
    sget-object v0, Lbr/com/itau/security/commons/Crypto;->b:[B

    const/16 v1, 0x42

    aget-byte v0, v0, v1

    int-to-byte v0, v0

    sget-object v1, Lbr/com/itau/security/commons/Crypto;->b:[B

    const/16 v2, 0x4d

    aget-byte v1, v1, v2

    int-to-byte v1, v1

    sget-object v2, Lbr/com/itau/security/commons/Crypto;->b:[B

    const/16 v3, 0x41

    aget-byte v2, v2, v3

    int-to-short v2, v2

    invoke-static {v0, v1, v2}, Lbr/com/itau/security/commons/Crypto;->a(SSS)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljavax/crypto/SecretKeyFactory;->getInstance(Ljava/lang/String;)Ljavax/crypto/SecretKeyFactory;

    move-result-object v0

    .line 36
    invoke-virtual {v0, p0}, Ljavax/crypto/SecretKeyFactory;->generateSecret(Ljava/security/spec/KeySpec;)Ljavax/crypto/SecretKey;

    move-result-object v0

    invoke-interface {v0}, Ljavax/crypto/SecretKey;->getEncoded()[B
    :try_end_2e
    .catch Ljava/security/GeneralSecurityException; {:try_start_4 .. :try_end_2e} :catch_30

    move-result-object v0

    return-object v0

    .line 37
    :catch_30
    move-exception p0

    .line 38
    new-instance v0, Lbr/com/itau/security/commons/exception/BadCryptoDataException;

    sget-object v1, Lbr/com/itau/security/commons/Crypto;->b:[B

    const/16 v2, 0xc

    aget-byte v1, v1, v2

    int-to-byte v1, v1

    sget-object v2, Lbr/com/itau/security/commons/Crypto;->b:[B

    const/16 v3, 0xe

    aget-byte v2, v2, v3

    int-to-byte v2, v2

    const/16 v3, 0x89

    invoke-static {v1, v2, v3}, Lbr/com/itau/security/commons/Crypto;->a(SSS)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Lbr/com/itau/security/commons/exception/BadCryptoDataException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v0
.end method

.method public static pkcs5PaddingDecrypt([B[B[B)[B
    .registers 11

    .line 70
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    sget-object v3, Lbr/com/itau/security/commons/Crypto;->b:[B

    const/4 v4, 0x7

    aget-byte v3, v3, v4

    int-to-byte v3, v3

    sget-object v4, Lbr/com/itau/security/commons/Crypto;->b:[B

    const/16 v5, 0xc

    aget-byte v4, v4, v5

    int-to-byte v4, v4

    or-int/lit8 v5, v4, 0x63

    int-to-short v5, v5

    invoke-static {v3, v4, v5}, Lbr/com/itau/security/commons/Crypto;->a(SSS)Ljava/lang/String;

    move-result-object v4

    sget-object v3, Lbr/com/itau/security/commons/Crypto;->b:[B

    const/16 v5, 0x9f

    aget-byte v3, v3, v5

    neg-int v3, v3

    int-to-byte v3, v3

    sget-object v5, Lbr/com/itau/security/commons/Crypto;->b:[B

    const/16 v6, 0xc

    aget-byte v5, v5, v6

    int-to-byte v5, v5

    sget-object v6, Lbr/com/itau/security/commons/Crypto;->b:[B

    const/16 v7, 0x15

    aget-byte v6, v6, v7

    int-to-short v6, v6

    invoke-static {v3, v5, v6}, Lbr/com/itau/security/commons/Crypto;->a(SSS)Ljava/lang/String;

    move-result-object v5

    const/4 v3, 0x2

    invoke-static/range {v0 .. v5}, Lbr/com/itau/security/commons/Crypto;->a([B[B[BILjava/lang/String;Ljava/lang/String;)[B

    move-result-object v0

    return-object v0
.end method

.method public static pkcs5PaddingEncrypt([B[B[B)[B
    .registers 11

    .line 66
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    sget-object v3, Lbr/com/itau/security/commons/Crypto;->b:[B

    const/4 v4, 0x7

    aget-byte v3, v3, v4

    int-to-byte v3, v3

    sget-object v4, Lbr/com/itau/security/commons/Crypto;->b:[B

    const/16 v5, 0xc

    aget-byte v4, v4, v5

    int-to-byte v4, v4

    or-int/lit8 v5, v4, 0x63

    int-to-short v5, v5

    invoke-static {v3, v4, v5}, Lbr/com/itau/security/commons/Crypto;->a(SSS)Ljava/lang/String;

    move-result-object v4

    sget-object v3, Lbr/com/itau/security/commons/Crypto;->b:[B

    const/16 v5, 0x9f

    aget-byte v3, v3, v5

    neg-int v3, v3

    int-to-byte v3, v3

    sget-object v5, Lbr/com/itau/security/commons/Crypto;->b:[B

    const/16 v6, 0xc

    aget-byte v5, v5, v6

    int-to-byte v5, v5

    sget-object v6, Lbr/com/itau/security/commons/Crypto;->b:[B

    const/16 v7, 0x15

    aget-byte v6, v6, v7

    int-to-short v6, v6

    invoke-static {v3, v5, v6}, Lbr/com/itau/security/commons/Crypto;->a(SSS)Ljava/lang/String;

    move-result-object v5

    const/4 v3, 0x1

    invoke-static/range {v0 .. v5}, Lbr/com/itau/security/commons/Crypto;->a([B[B[BILjava/lang/String;Ljava/lang/String;)[B

    move-result-object v0

    return-object v0
.end method

.method public static pkcs7PaddingDecrypt([B[B[B)[B
    .registers 11

    .line 78
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    sget-object v3, Lbr/com/itau/security/commons/Crypto;->b:[B

    const/4 v4, 0x7

    aget-byte v3, v3, v4

    int-to-byte v3, v3

    sget-object v4, Lbr/com/itau/security/commons/Crypto;->b:[B

    const/16 v5, 0xc

    aget-byte v4, v4, v5

    int-to-byte v4, v4

    or-int/lit8 v5, v4, 0x76

    int-to-short v5, v5

    invoke-static {v3, v4, v5}, Lbr/com/itau/security/commons/Crypto;->a(SSS)Ljava/lang/String;

    move-result-object v4

    sget-object v3, Lbr/com/itau/security/commons/Crypto;->b:[B

    const/16 v5, 0x9f

    aget-byte v3, v3, v5

    neg-int v3, v3

    int-to-byte v3, v3

    sget-object v5, Lbr/com/itau/security/commons/Crypto;->b:[B

    const/16 v6, 0xc

    aget-byte v5, v5, v6

    int-to-byte v5, v5

    sget-object v6, Lbr/com/itau/security/commons/Crypto;->b:[B

    const/16 v7, 0x15

    aget-byte v6, v6, v7

    int-to-short v6, v6

    invoke-static {v3, v5, v6}, Lbr/com/itau/security/commons/Crypto;->a(SSS)Ljava/lang/String;

    move-result-object v5

    const/4 v3, 0x2

    invoke-static/range {v0 .. v5}, Lbr/com/itau/security/commons/Crypto;->a([B[B[BILjava/lang/String;Ljava/lang/String;)[B

    move-result-object v0

    return-object v0
.end method

.method public static pkcs7PaddingEncrypt([B[B[B)[B
    .registers 11

    .line 74
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    sget-object v3, Lbr/com/itau/security/commons/Crypto;->b:[B

    const/4 v4, 0x7

    aget-byte v3, v3, v4

    int-to-byte v3, v3

    sget-object v4, Lbr/com/itau/security/commons/Crypto;->b:[B

    const/16 v5, 0xc

    aget-byte v4, v4, v5

    int-to-byte v4, v4

    or-int/lit8 v5, v4, 0x76

    int-to-short v5, v5

    invoke-static {v3, v4, v5}, Lbr/com/itau/security/commons/Crypto;->a(SSS)Ljava/lang/String;

    move-result-object v4

    sget-object v3, Lbr/com/itau/security/commons/Crypto;->b:[B

    const/16 v5, 0x9f

    aget-byte v3, v3, v5

    neg-int v3, v3

    int-to-byte v3, v3

    sget-object v5, Lbr/com/itau/security/commons/Crypto;->b:[B

    const/16 v6, 0xc

    aget-byte v5, v5, v6

    int-to-byte v5, v5

    sget-object v6, Lbr/com/itau/security/commons/Crypto;->b:[B

    const/16 v7, 0x15

    aget-byte v6, v6, v7

    int-to-short v6, v6

    invoke-static {v3, v5, v6}, Lbr/com/itau/security/commons/Crypto;->a(SSS)Ljava/lang/String;

    move-result-object v5

    const/4 v3, 0x1

    invoke-static/range {v0 .. v5}, Lbr/com/itau/security/commons/Crypto;->a([B[B[BILjava/lang/String;Ljava/lang/String;)[B

    move-result-object v0

    return-object v0
.end method

.method public static randomString(I)Ljava/lang/String;
    .registers 4

    .line 26
    new-instance v0, Ljava/math/BigInteger;

    mul-int/lit8 v1, p0, 0x8

    sget-object v2, Lbr/com/itau/security/commons/Crypto;->a:Ljava/security/SecureRandom;

    invoke-direct {v0, v1, v2}, Ljava/math/BigInteger;-><init>(ILjava/util/Random;)V

    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Ljava/math/BigInteger;->toString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1, p0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static sha1([B)[B
    .registers 6

    .line 83
    :try_start_0
    sget-object v0, Lbr/com/itau/security/commons/Crypto;->b:[B

    const/16 v1, 0x15

    aget-byte v0, v0, v1

    add-int/lit8 v0, v0, -0x1

    int-to-byte v0, v0

    sget-object v1, Lbr/com/itau/security/commons/Crypto;->b:[B

    const/16 v2, 0x36

    aget-byte v1, v1, v2

    int-to-byte v1, v1

    sget-object v2, Lbr/com/itau/security/commons/Crypto;->b:[B

    const/16 v3, 0xc

    aget-byte v2, v2, v3

    int-to-short v2, v2

    invoke-static {v0, v1, v2}, Lbr/com/itau/security/commons/Crypto;->a(SSS)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/security/MessageDigest;->digest([B)[B
    :try_end_22
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_22} :catch_24

    move-result-object v0

    return-object v0

    .line 84
    .line 85
    :catch_24
    new-instance v0, Lbr/com/itau/security/commons/exception/CommonSecurityException;

    sget-object v1, Lbr/com/itau/security/commons/Crypto;->b:[B

    const/16 v2, 0x71

    aget-byte v1, v1, v2

    int-to-byte v1, v1

    sget v2, Lbr/com/itau/security/commons/Crypto;->c:I

    and-int/lit16 v2, v2, 0xaf

    int-to-byte v2, v2

    sget-object v3, Lbr/com/itau/security/commons/Crypto;->b:[B

    const/16 v4, 0x85

    aget-byte v3, v3, v4

    neg-int v3, v3

    int-to-short v3, v3

    invoke-static {v1, v2, v3}, Lbr/com/itau/security/commons/Crypto;->a(SSS)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lbr/com/itau/security/commons/exception/CommonSecurityException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static sha256(Ljava/lang/String;)[B
    .registers 2

    .line 90
    invoke-static {p0}, Lbr/com/itau/security/commons/binary/ByteUtils;->from(Ljava/lang/String;)[B

    move-result-object v0

    invoke-static {v0}, Lbr/com/itau/security/commons/Crypto;->sha256([B)[B

    move-result-object v0

    return-object v0
.end method

.method public static sha256([B)[B
    .registers 6

    .line 95
    :try_start_0
    sget-object v0, Lbr/com/itau/security/commons/Crypto;->b:[B

    const/16 v1, 0x74

    aget-byte v0, v0, v1

    int-to-byte v0, v0

    sget-object v1, Lbr/com/itau/security/commons/Crypto;->b:[B

    const/16 v2, 0x36

    aget-byte v1, v1, v2

    int-to-byte v1, v1

    sget v2, Lbr/com/itau/security/commons/Crypto;->c:I

    and-int/lit16 v2, v2, 0x1df

    int-to-short v2, v2

    invoke-static {v0, v1, v2}, Lbr/com/itau/security/commons/Crypto;->a(SSS)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/security/MessageDigest;->digest([B)[B
    :try_end_1e
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_1e} :catch_20

    move-result-object v0

    return-object v0

    .line 96
    .line 97
    :catch_20
    new-instance v0, Lbr/com/itau/security/commons/exception/CommonSecurityException;

    sget-object v1, Lbr/com/itau/security/commons/Crypto;->b:[B

    const/16 v2, 0x19

    aget-byte v1, v1, v2

    int-to-byte v1, v1

    sget v2, Lbr/com/itau/security/commons/Crypto;->c:I

    and-int/lit16 v2, v2, 0xaf

    int-to-byte v2, v2

    sget-object v3, Lbr/com/itau/security/commons/Crypto;->b:[B

    const/16 v4, 0x93

    aget-byte v3, v3, v4

    int-to-short v3, v3

    invoke-static {v1, v2, v3}, Lbr/com/itau/security/commons/Crypto;->a(SSS)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lbr/com/itau/security/commons/exception/CommonSecurityException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.class public Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesActivity;
.super Lcom/itau/empresas/ui/activity/BaseActivity;
.source "ComprovantesActivity.java"


# instance fields
.field comprovantesAdapter:Lcom/itau/empresas/feature/comprovantes/ComprovantesAdapter;

.field controller:Lcom/itau/empresas/feature/comprovantes/controller/ComprovantesController;

.field private diaSelecionadoIndex:I

.field dialogFiltro:Lcom/itau/empresas/ui/dialog/FiltroComprovantesDialog;

.field listaComprovantes:Landroid/widget/ListView;

.field root:Landroid/widget/LinearLayout;

.field textoSemComprovantes:Landroid/widget/TextView;

.field toolBar:Landroid/support/v7/widget/Toolbar;

.field topSnackbar:Lbr/com/itau/topsnackbar/TopSnackbar;


# direct methods
.method public constructor <init>()V
    .registers 2

    .line 42
    invoke-direct {p0}, Lcom/itau/empresas/ui/activity/BaseActivity;-><init>()V

    .line 46
    const/4 v0, 0x0

    iput v0, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesActivity;->diaSelecionadoIndex:I

    return-void
.end method

.method static synthetic access$000(Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesActivity;)Lcom/itau/empresas/CustomApplication;
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesActivity;

    .line 42
    iget-object v0, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesActivity;->application:Lcom/itau/empresas/CustomApplication;

    return-object v0
.end method

.method static synthetic access$100(Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesActivity;)I
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesActivity;

    .line 42
    iget v0, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesActivity;->diaSelecionadoIndex:I

    return v0
.end method

.method static synthetic access$102(Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesActivity;I)I
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesActivity;
    .param p1, "x1"    # I

    .line 42
    iput p1, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesActivity;->diaSelecionadoIndex:I

    return p1
.end method

.method static synthetic access$200(Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesActivity;Ljava/lang/String;)V
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesActivity;
    .param p1, "x1"    # Ljava/lang/String;

    .line 42
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesActivity;->carregarListaComprovantes(Ljava/lang/String;)V

    return-void
.end method

.method private carregarListaComprovantes(Ljava/lang/String;)V
    .registers 6
    .param p1, "dataInicio"    # Ljava/lang/String;

    .line 228
    invoke-virtual {p0}, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesActivity;->mostraDialogoDeProgresso()V

    .line 230
    iget-object v0, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesActivity;->controller:Lcom/itau/empresas/feature/comprovantes/controller/ComprovantesController;

    invoke-static {}, Lcom/itau/empresas/ui/util/DataEHoraUtils;->getDataAtual()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, p1, v1, v2, v3}, Lcom/itau/empresas/feature/comprovantes/controller/ComprovantesController;->buscacomprovantes(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 231
    return-void
.end method

.method private constroiToolbar()V
    .registers 3

    .line 273
    iget-object v0, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesActivity;->toolBar:Landroid/support/v7/widget/Toolbar;

    invoke-static {v0, p0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom;->Builder(Landroid/support/v7/widget/Toolbar;Landroid/support/v7/app/AppCompatActivity;)Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    move-result-object v0

    .line 274
    const v1, 0x7f07071e

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->comTitulo(I)Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    move-result-object v0

    .line 275
    invoke-virtual {v0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->comClickListenerFinish()Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    move-result-object v0

    .line 276
    invoke-virtual {v0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->build()Lcom/itau/empresas/ui/util/listener/ToolbarCustom;

    .line 277
    return-void
.end method

.method private disparaEventoAnalytics()V
    .registers 5

    .line 72
    invoke-static {}, Lcom/itau/empresas/feature/analytics/AnalyticsHelper;->getInstance()Lcom/itau/empresas/feature/analytics/AnalyticsHelper;

    move-result-object v3

    .line 73
    .line 74
    .local v3, "analyticsHelper":Lcom/itau/empresas/feature/analytics/AnalyticsHelper;
    const v0, 0x7f0700da

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 75
    const v1, 0x7f0700c9

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 76
    const v2, 0x7f0700c3

    invoke-virtual {p0, v2}, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 73
    invoke-virtual {v3, v0, v1, v2}, Lcom/itau/empresas/feature/analytics/AnalyticsHelper;->eventoAdobeState(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 77
    return-void
.end method

.method private pesquisarSaldoExtrato()Landroid/support/v7/widget/SearchView$OnQueryTextListener;
    .registers 2

    .line 253
    new-instance v0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesActivity$7;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesActivity$7;-><init>(Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesActivity;)V

    return-object v0
.end method

.method private preparaDialogDeFiltro()V
    .registers 3

    .line 234
    new-instance v0, Lcom/itau/empresas/ui/dialog/FiltroComprovantesDialog_$FragmentBuilder_;

    invoke-direct {v0}, Lcom/itau/empresas/ui/dialog/FiltroComprovantesDialog_$FragmentBuilder_;-><init>()V

    iget v1, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesActivity;->diaSelecionadoIndex:I

    .line 235
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/dialog/FiltroComprovantesDialog_$FragmentBuilder_;->selecionado(Ljava/lang/Integer;)Lcom/itau/empresas/ui/dialog/FiltroComprovantesDialog_$FragmentBuilder_;

    move-result-object v0

    .line 236
    invoke-virtual {v0}, Lcom/itau/empresas/ui/dialog/FiltroComprovantesDialog_$FragmentBuilder_;->build()Lcom/itau/empresas/ui/dialog/FiltroComprovantesDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesActivity;->dialogFiltro:Lcom/itau/empresas/ui/dialog/FiltroComprovantesDialog;

    .line 238
    iget-object v0, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesActivity;->dialogFiltro:Lcom/itau/empresas/ui/dialog/FiltroComprovantesDialog;

    new-instance v1, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesActivity$6;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesActivity$6;-><init>(Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesActivity;)V

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/dialog/FiltroComprovantesDialog;->setConfirmarListener(Landroid/view/View$OnClickListener;)V

    .line 250
    return-void
.end method


# virtual methods
.method carregandoElementosDeTela()V
    .registers 2

    .line 64
    const/4 v0, 0x7

    invoke-static {v0}, Lcom/itau/empresas/ui/util/DataEHoraUtils;->subtraiDiasDeHoje(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesActivity;->carregarListaComprovantes(Ljava/lang/String;)V

    .line 66
    invoke-direct {p0}, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesActivity;->constroiToolbar()V

    .line 68
    invoke-direct {p0}, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesActivity;->disparaEventoAnalytics()V

    .line 69
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .registers 6
    .param p1, "menu"    # Landroid/view/Menu;

    .line 179
    invoke-virtual {p0}, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f0f0005

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 181
    const v0, 0x7f0e06c6

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    .line 182
    .local v2, "itemSVSaldoExtrato":Landroid/view/MenuItem;
    invoke-static {v2}, Landroid/support/v4/view/MenuItemCompat;->getActionView(Landroid/view/MenuItem;)Landroid/view/View;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Landroid/support/v7/widget/SearchView;

    .line 183
    .local v3, "searchView":Landroid/support/v7/widget/SearchView;
    const v0, 0x7f070635

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/support/v7/widget/SearchView;->setQueryHint(Ljava/lang/CharSequence;)V

    .line 184
    invoke-direct {p0}, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesActivity;->pesquisarSaldoExtrato()Landroid/support/v7/widget/SearchView$OnQueryTextListener;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/support/v7/widget/SearchView;->setOnQueryTextListener(Landroid/support/v7/widget/SearchView$OnQueryTextListener;)V

    .line 186
    new-instance v0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesActivity$5;

    invoke-direct {v0, p0, p1}, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesActivity$5;-><init>(Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesActivity;Landroid/view/Menu;)V

    .line 187
    invoke-static {v2, v0}, Landroid/support/v4/view/MenuItemCompat;->setOnActionExpandListener(Landroid/view/MenuItem;Landroid/support/v4/view/MenuItemCompat$OnActionExpandListener;)Landroid/view/MenuItem;

    .line 204
    const/4 v0, 0x1

    return v0
.end method

.method public onEventMainThread(Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent$Conversion;)V
    .registers 5
    .param p1, "e"    # Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent$Conversion;

    .line 139
    invoke-static {p1, p0}, Lcom/itau/empresas/api/ApiHelper;->devoTratarEvento(Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;Lcom/itau/empresas/api/ApiConsumidor;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 140
    return-void

    .line 143
    :cond_7
    const-string v0, "ItauEmpresas"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Erro na Chamanda: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent$Conversion;->getException()Lbr/com/itau/sdk/android/core/exception/BackendException;

    move-result-object v2

    invoke-virtual {v2}, Lbr/com/itau/sdk/android/core/exception/BackendException;->getOpKey()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent$Conversion;->getException()Lbr/com/itau/sdk/android/core/exception/BackendException;

    move-result-object v2

    invoke-static {v0, v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 144
    iget-object v0, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesActivity;->listaComprovantes:Landroid/widget/ListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 146
    invoke-virtual {p0}, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesActivity;->escondeDialogoDeProgresso()V

    .line 147
    iget-object v0, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesActivity;->root:Landroid/widget/LinearLayout;

    const v1, 0x7f0701f2

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lbr/com/itau/topsnackbar/TopSnackbar;->make(Landroid/view/ViewGroup;II)Lbr/com/itau/topsnackbar/TopSnackbar;

    move-result-object v0

    .line 148
    invoke-virtual {v0}, Lbr/com/itau/topsnackbar/TopSnackbar;->show()Lbr/com/itau/topsnackbar/TopSnackbar;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesActivity;->topSnackbar:Lbr/com/itau/topsnackbar/TopSnackbar;

    .line 149
    return-void
.end method

.method public onEventMainThread(Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent$Unexpected;)V
    .registers 5
    .param p1, "e"    # Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent$Unexpected;

    .line 152
    invoke-static {p1, p0}, Lcom/itau/empresas/api/ApiHelper;->devoTratarEvento(Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;Lcom/itau/empresas/api/ApiConsumidor;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 153
    return-void

    .line 156
    :cond_7
    const-string v0, "ItauEmpresas"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Erro na Chamada:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent$Unexpected;->getException()Lbr/com/itau/sdk/android/core/exception/BackendException;

    move-result-object v2

    invoke-virtual {v2}, Lbr/com/itau/sdk/android/core/exception/BackendException;->getOpKey()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent$Unexpected;->getException()Lbr/com/itau/sdk/android/core/exception/BackendException;

    move-result-object v2

    invoke-static {v0, v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 157
    iget-object v0, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesActivity;->listaComprovantes:Landroid/widget/ListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 158
    invoke-virtual {p0}, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesActivity;->escondeDialogoDeProgresso()V

    .line 159
    iget-object v0, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesActivity;->root:Landroid/widget/LinearLayout;

    const v1, 0x7f0701ef

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lbr/com/itau/topsnackbar/TopSnackbar;->make(Landroid/view/ViewGroup;II)Lbr/com/itau/topsnackbar/TopSnackbar;

    move-result-object v0

    .line 160
    invoke-virtual {v0}, Lbr/com/itau/topsnackbar/TopSnackbar;->show()Lbr/com/itau/topsnackbar/TopSnackbar;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesActivity;->topSnackbar:Lbr/com/itau/topsnackbar/TopSnackbar;

    .line 161
    return-void
.end method

.method public onEventMainThread(Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;)V
    .registers 5
    .param p1, "e"    # Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;

    .line 165
    invoke-static {p1, p0}, Lcom/itau/empresas/api/ApiHelper;->devoTratarEvento(Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;Lcom/itau/empresas/api/ApiConsumidor;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 166
    return-void

    .line 169
    :cond_7
    const-string v0, "ItauEmpresas"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Erro na Chamada:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;->getException()Lbr/com/itau/sdk/android/core/exception/BackendException;

    move-result-object v2

    invoke-virtual {v2}, Lbr/com/itau/sdk/android/core/exception/BackendException;->getOpKey()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;->getException()Lbr/com/itau/sdk/android/core/exception/BackendException;

    move-result-object v2

    invoke-static {v0, v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 170
    iget-object v0, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesActivity;->listaComprovantes:Landroid/widget/ListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 171
    invoke-virtual {p0}, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesActivity;->escondeDialogoDeProgresso()V

    .line 172
    iget-object v0, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesActivity;->root:Landroid/widget/LinearLayout;

    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;->getException()Lbr/com/itau/sdk/android/core/exception/BackendException;

    move-result-object v1

    invoke-virtual {v1}, Lbr/com/itau/sdk/android/core/exception/BackendException;->getError()Lbr/com/itau/sdk/android/core/model/ErrorDTO;

    move-result-object v1

    invoke-virtual {v1}, Lbr/com/itau/sdk/android/core/model/ErrorDTO;->getMensagem()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lbr/com/itau/topsnackbar/TopSnackbar;->make(Landroid/view/ViewGroup;Ljava/lang/CharSequence;I)Lbr/com/itau/topsnackbar/TopSnackbar;

    move-result-object v0

    .line 173
    invoke-virtual {v0}, Lbr/com/itau/topsnackbar/TopSnackbar;->show()Lbr/com/itau/topsnackbar/TopSnackbar;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesActivity;->topSnackbar:Lbr/com/itau/topsnackbar/TopSnackbar;

    .line 174
    return-void
.end method

.method public onEventMainThread(Ljava/util/List;)V
    .registers 4
    .param p1, "comprovantesVO"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<Lcom/itau/empresas/feature/comprovantes/model/ComprovantesVO;>;)V"
        }
    .end annotation

    .line 92
    iget-object v0, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesActivity;->listaComprovantes:Landroid/widget/ListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 93
    iget-object v0, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesActivity;->topSnackbar:Lbr/com/itau/topsnackbar/TopSnackbar;

    if-eqz v0, :cond_f

    .line 94
    iget-object v0, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesActivity;->topSnackbar:Lbr/com/itau/topsnackbar/TopSnackbar;

    invoke-virtual {v0}, Lbr/com/itau/topsnackbar/TopSnackbar;->dismiss()V

    .line 95
    :cond_f
    invoke-virtual {p0}, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesActivity;->escondeDialogoDeProgresso()V

    .line 97
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_35

    .line 98
    iget-object v0, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesActivity;->textoSemComprovantes:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 99
    new-instance v0, Lcom/itau/empresas/feature/comprovantes/ComprovantesAdapter;

    invoke-direct {v0, p0, p1}, Lcom/itau/empresas/feature/comprovantes/ComprovantesAdapter;-><init>(Landroid/content/Context;Ljava/util/List;)V

    iput-object v0, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesActivity;->comprovantesAdapter:Lcom/itau/empresas/feature/comprovantes/ComprovantesAdapter;

    .line 100
    iget-object v0, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesActivity;->listaComprovantes:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesActivity;->comprovantesAdapter:Lcom/itau/empresas/feature/comprovantes/ComprovantesAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 101
    iget-object v0, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesActivity;->comprovantesAdapter:Lcom/itau/empresas/feature/comprovantes/ComprovantesAdapter;

    sget-object v1, Lcom/daimajia/swipe/util/Attributes$Mode;->Single:Lcom/daimajia/swipe/util/Attributes$Mode;

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/comprovantes/ComprovantesAdapter;->setMode(Lcom/daimajia/swipe/util/Attributes$Mode;)V

    goto :goto_56

    .line 104
    :cond_35
    iget-object v0, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesActivity;->textoSemComprovantes:Landroid/widget/TextView;

    const v1, 0x7f070471

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 105
    iget-object v0, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesActivity;->textoSemComprovantes:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 106
    new-instance v0, Lcom/itau/empresas/feature/comprovantes/ComprovantesAdapter;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/itau/empresas/feature/comprovantes/ComprovantesAdapter;-><init>(Landroid/content/Context;Ljava/util/List;)V

    iput-object v0, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesActivity;->comprovantesAdapter:Lcom/itau/empresas/feature/comprovantes/ComprovantesAdapter;

    .line 107
    iget-object v0, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesActivity;->listaComprovantes:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesActivity;->comprovantesAdapter:Lcom/itau/empresas/feature/comprovantes/ComprovantesAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 109
    :goto_56
    iget-object v0, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesActivity;->comprovantesAdapter:Lcom/itau/empresas/feature/comprovantes/ComprovantesAdapter;

    new-instance v1, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesActivity$1;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesActivity$1;-><init>(Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesActivity;)V

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/comprovantes/ComprovantesAdapter;->setSalvarClickListener(Landroid/view/View$OnClickListener;)V

    .line 115
    iget-object v0, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesActivity;->comprovantesAdapter:Lcom/itau/empresas/feature/comprovantes/ComprovantesAdapter;

    new-instance v1, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesActivity$2;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesActivity$2;-><init>(Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesActivity;)V

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/comprovantes/ComprovantesAdapter;->setEnviarClickListener(Landroid/view/View$OnClickListener;)V

    .line 121
    iget-object v0, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesActivity;->comprovantesAdapter:Lcom/itau/empresas/feature/comprovantes/ComprovantesAdapter;

    new-instance v1, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesActivity$3;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesActivity$3;-><init>(Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesActivity;)V

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/comprovantes/ComprovantesAdapter;->setCompartilharClickListener(Landroid/view/View$OnClickListener;)V

    .line 127
    iget-object v0, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesActivity;->comprovantesAdapter:Lcom/itau/empresas/feature/comprovantes/ComprovantesAdapter;

    new-instance v1, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesActivity$4;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesActivity$4;-><init>(Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesActivity;)V

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/comprovantes/ComprovantesAdapter;->setchooseDetailClickListner(Landroid/view/View$OnClickListener;)V

    .line 136
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .registers 4
    .param p1, "item"    # Landroid/view/MenuItem;

    .line 209
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x7f0e06c5

    if-ne v0, v1, :cond_13

    .line 210
    invoke-direct {p0}, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesActivity;->preparaDialogDeFiltro()V

    .line 211
    iget-object v0, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesActivity;->dialogFiltro:Lcom/itau/empresas/ui/dialog/FiltroComprovantesDialog;

    invoke-virtual {v0, p0}, Lcom/itau/empresas/ui/dialog/FiltroComprovantesDialog;->show(Landroid/support/v7/app/AppCompatActivity;)Lcom/itau/empresas/ui/dialog/BaseDialog;

    .line 212
    const/4 v0, 0x1

    return v0

    .line 214
    :cond_13
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x7f0e06cf

    if-ne v0, v1, :cond_21

    .line 215
    iget-object v0, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesActivity;->application:Lcom/itau/empresas/CustomApplication;

    invoke-static {v0, p0}, Lcom/itau/empresas/ui/util/ViewUtils;->aoClicarBotaoSairMenu(Lcom/itau/empresas/CustomApplication;Lcom/itau/empresas/ui/activity/BaseActivity;)V

    .line 217
    :cond_21
    invoke-super {p0, p1}, Lcom/itau/empresas/ui/activity/BaseActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0
.end method

.method public opkeysConsumidas()Ljava/util/List;
    .registers 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Ljava/lang/String;>;"
        }
    .end annotation

    .line 222
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "enviarComprovanteEmail"

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const-string v1, "comprovantes"

    const/4 v2, 0x1

    aput-object v1, v0, v2

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

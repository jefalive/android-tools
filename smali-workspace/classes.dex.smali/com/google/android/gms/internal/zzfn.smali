.class public Lcom/google/android/gms/internal/zzfn;
.super Lcom/google/android/gms/internal/zzfs;


# annotations
.annotation runtime Lcom/google/android/gms/internal/zzhb;
.end annotation


# static fields
.field static final zzDa:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<Ljava/lang/String;>;"
        }
    .end annotation
.end field


# instance fields
.field private zzCh:Lcom/google/android/gms/ads/internal/client/AdSizeParcel;

.field private zzDb:Ljava/lang/String;

.field private zzDc:Z

.field private zzDd:I

.field private zzDe:I

.field private zzDf:I

.field private zzDg:I

.field private final zzDh:Landroid/app/Activity;

.field private zzDi:Landroid/widget/ImageView;

.field private zzDj:Landroid/widget/LinearLayout;

.field private zzDk:Lcom/google/android/gms/internal/zzft;

.field private zzDl:Landroid/widget/PopupWindow;

.field private zzDm:Landroid/widget/RelativeLayout;

.field private zzDn:Landroid/view/ViewGroup;

.field private zzoG:I

.field private zzoH:I

.field private final zzpD:Lcom/google/android/gms/internal/zzjp;

.field private final zzpV:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "top-left"

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const-string v1, "top-right"

    const/4 v2, 0x1

    aput-object v1, v0, v2

    const-string v1, "top-center"

    const/4 v2, 0x2

    aput-object v1, v0, v2

    const-string v1, "center"

    const/4 v2, 0x3

    aput-object v1, v0, v2

    const-string v1, "bottom-left"

    const/4 v2, 0x4

    aput-object v1, v0, v2

    const-string v1, "bottom-right"

    const/4 v2, 0x5

    aput-object v1, v0, v2

    const-string v1, "bottom-center"

    const/4 v2, 0x6

    aput-object v1, v0, v2

    invoke-static {v0}, Lcom/google/android/gms/internal/zzmr;->zzc([Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/internal/zzfn;->zzDa:Ljava/util/Set;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/internal/zzjp;Lcom/google/android/gms/internal/zzft;)V
    .registers 4

    const-string v0, "resize"

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/internal/zzfs;-><init>(Lcom/google/android/gms/internal/zzjp;Ljava/lang/String;)V

    const-string v0, "top-right"

    iput-object v0, p0, Lcom/google/android/gms/internal/zzfn;->zzDb:Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/internal/zzfn;->zzDc:Z

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/internal/zzfn;->zzDd:I

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/internal/zzfn;->zzDe:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/internal/zzfn;->zzoH:I

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/internal/zzfn;->zzDf:I

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/internal/zzfn;->zzDg:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/internal/zzfn;->zzoG:I

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/internal/zzfn;->zzpV:Ljava/lang/Object;

    iput-object p1, p0, Lcom/google/android/gms/internal/zzfn;->zzpD:Lcom/google/android/gms/internal/zzjp;

    invoke-interface {p1}, Lcom/google/android/gms/internal/zzjp;->zzhP()Landroid/app/Activity;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/internal/zzfn;->zzDh:Landroid/app/Activity;

    iput-object p2, p0, Lcom/google/android/gms/internal/zzfn;->zzDk:Lcom/google/android/gms/internal/zzft;

    return-void
.end method

.method private zzeM()[I
    .registers 9

    invoke-virtual {p0}, Lcom/google/android/gms/internal/zzfn;->zzeO()Z

    move-result v0

    if-nez v0, :cond_8

    const/4 v0, 0x0

    return-object v0

    :cond_8
    iget-boolean v0, p0, Lcom/google/android/gms/internal/zzfn;->zzDc:Z

    if-eqz v0, :cond_20

    const/4 v0, 0x2

    new-array v0, v0, [I

    iget v1, p0, Lcom/google/android/gms/internal/zzfn;->zzDd:I

    iget v2, p0, Lcom/google/android/gms/internal/zzfn;->zzDf:I

    add-int/2addr v1, v2

    const/4 v2, 0x0

    aput v1, v0, v2

    iget v1, p0, Lcom/google/android/gms/internal/zzfn;->zzDe:I

    iget v2, p0, Lcom/google/android/gms/internal/zzfn;->zzDg:I

    add-int/2addr v1, v2

    const/4 v2, 0x1

    aput v1, v0, v2

    return-object v0

    :cond_20
    invoke-static {}, Lcom/google/android/gms/ads/internal/zzr;->zzbC()Lcom/google/android/gms/internal/zzir;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/internal/zzfn;->zzDh:Landroid/app/Activity;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/internal/zzir;->zzf(Landroid/app/Activity;)[I

    move-result-object v3

    invoke-static {}, Lcom/google/android/gms/ads/internal/zzr;->zzbC()Lcom/google/android/gms/internal/zzir;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/internal/zzfn;->zzDh:Landroid/app/Activity;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/internal/zzir;->zzh(Landroid/app/Activity;)[I

    move-result-object v4

    const/4 v0, 0x0

    aget v5, v3, v0

    iget v0, p0, Lcom/google/android/gms/internal/zzfn;->zzDd:I

    iget v1, p0, Lcom/google/android/gms/internal/zzfn;->zzDf:I

    add-int v6, v0, v1

    iget v0, p0, Lcom/google/android/gms/internal/zzfn;->zzDe:I

    iget v1, p0, Lcom/google/android/gms/internal/zzfn;->zzDg:I

    add-int v7, v0, v1

    if-gez v6, :cond_47

    const/4 v6, 0x0

    goto :goto_50

    :cond_47
    iget v0, p0, Lcom/google/android/gms/internal/zzfn;->zzoG:I

    add-int/2addr v0, v6

    if-le v0, v5, :cond_50

    iget v0, p0, Lcom/google/android/gms/internal/zzfn;->zzoG:I

    sub-int v6, v5, v0

    :cond_50
    :goto_50
    const/4 v0, 0x0

    aget v0, v4, v0

    if-ge v7, v0, :cond_59

    const/4 v0, 0x0

    aget v7, v4, v0

    goto :goto_68

    :cond_59
    iget v0, p0, Lcom/google/android/gms/internal/zzfn;->zzoH:I

    add-int/2addr v0, v7

    const/4 v1, 0x1

    aget v1, v4, v1

    if-le v0, v1, :cond_68

    const/4 v0, 0x1

    aget v0, v4, v0

    iget v1, p0, Lcom/google/android/gms/internal/zzfn;->zzoH:I

    sub-int v7, v0, v1

    :cond_68
    :goto_68
    const/4 v0, 0x2

    new-array v0, v0, [I

    const/4 v1, 0x0

    aput v6, v0, v1

    const/4 v1, 0x1

    aput v7, v0, v1

    return-object v0
.end method

.method private zzh(Ljava/util/Map;)V
    .registers 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;)V"
        }
    .end annotation

    const-string v0, "width"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_20

    invoke-static {}, Lcom/google/android/gms/ads/internal/zzr;->zzbC()Lcom/google/android/gms/internal/zzir;

    move-result-object v0

    const-string v1, "width"

    invoke-interface {p1, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/internal/zzir;->zzaD(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/internal/zzfn;->zzoG:I

    :cond_20
    const-string v0, "height"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_40

    invoke-static {}, Lcom/google/android/gms/ads/internal/zzr;->zzbC()Lcom/google/android/gms/internal/zzir;

    move-result-object v0

    const-string v1, "height"

    invoke-interface {p1, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/internal/zzir;->zzaD(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/internal/zzfn;->zzoH:I

    :cond_40
    const-string v0, "offsetX"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_60

    invoke-static {}, Lcom/google/android/gms/ads/internal/zzr;->zzbC()Lcom/google/android/gms/internal/zzir;

    move-result-object v0

    const-string v1, "offsetX"

    invoke-interface {p1, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/internal/zzir;->zzaD(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/internal/zzfn;->zzDf:I

    :cond_60
    const-string v0, "offsetY"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_80

    invoke-static {}, Lcom/google/android/gms/ads/internal/zzr;->zzbC()Lcom/google/android/gms/internal/zzir;

    move-result-object v0

    const-string v1, "offsetY"

    invoke-interface {p1, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/internal/zzir;->zzaD(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/internal/zzfn;->zzDg:I

    :cond_80
    const-string v0, "allowOffscreen"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_9c

    const-string v0, "allowOffscreen"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/internal/zzfn;->zzDc:Z

    :cond_9c
    const-string v0, "customClosePosition"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_ad

    iput-object v2, p0, Lcom/google/android/gms/internal/zzfn;->zzDb:Ljava/lang/String;

    :cond_ad
    return-void
.end method


# virtual methods
.method public zza(IIZ)V
    .registers 12

    iget-object v5, p0, Lcom/google/android/gms/internal/zzfn;->zzpV:Ljava/lang/Object;

    monitor-enter v5

    :try_start_3
    iput p1, p0, Lcom/google/android/gms/internal/zzfn;->zzDd:I

    iput p2, p0, Lcom/google/android/gms/internal/zzfn;->zzDe:I

    iget-object v0, p0, Lcom/google/android/gms/internal/zzfn;->zzDl:Landroid/widget/PopupWindow;

    if-eqz v0, :cond_4c

    if-eqz p3, :cond_4c

    invoke-direct {p0}, Lcom/google/android/gms/internal/zzfn;->zzeM()[I

    move-result-object v6

    if-eqz v6, :cond_48

    iget-object v0, p0, Lcom/google/android/gms/internal/zzfn;->zzDl:Landroid/widget/PopupWindow;

    invoke-static {}, Lcom/google/android/gms/ads/internal/client/zzn;->zzcS()Lcom/google/android/gms/ads/internal/util/client/zza;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/internal/zzfn;->zzDh:Landroid/app/Activity;

    const/4 v3, 0x0

    aget v3, v6, v3

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gms/ads/internal/util/client/zza;->zzb(Landroid/content/Context;I)I

    move-result v1

    invoke-static {}, Lcom/google/android/gms/ads/internal/client/zzn;->zzcS()Lcom/google/android/gms/ads/internal/util/client/zza;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/internal/zzfn;->zzDh:Landroid/app/Activity;

    const/4 v4, 0x1

    aget v4, v6, v4

    invoke-virtual {v2, v3, v4}, Lcom/google/android/gms/ads/internal/util/client/zza;->zzb(Landroid/content/Context;I)I

    move-result v2

    iget-object v3, p0, Lcom/google/android/gms/internal/zzfn;->zzDl:Landroid/widget/PopupWindow;

    invoke-virtual {v3}, Landroid/widget/PopupWindow;->getWidth()I

    move-result v3

    iget-object v4, p0, Lcom/google/android/gms/internal/zzfn;->zzDl:Landroid/widget/PopupWindow;

    invoke-virtual {v4}, Landroid/widget/PopupWindow;->getHeight()I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/PopupWindow;->update(IIII)V

    const/4 v0, 0x0

    aget v0, v6, v0

    const/4 v1, 0x1

    aget v1, v6, v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/internal/zzfn;->zzd(II)V

    goto :goto_4c

    :cond_48
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/internal/zzfn;->zzp(Z)V
    :try_end_4c
    .catchall {:try_start_3 .. :try_end_4c} :catchall_4e

    :cond_4c
    :goto_4c
    monitor-exit v5

    goto :goto_51

    :catchall_4e
    move-exception v7

    monitor-exit v5

    throw v7

    :goto_51
    return-void
.end method

.method zzc(II)V
    .registers 6

    iget-object v0, p0, Lcom/google/android/gms/internal/zzfn;->zzDk:Lcom/google/android/gms/internal/zzft;

    if-eqz v0, :cond_d

    iget-object v0, p0, Lcom/google/android/gms/internal/zzfn;->zzDk:Lcom/google/android/gms/internal/zzft;

    iget v1, p0, Lcom/google/android/gms/internal/zzfn;->zzoG:I

    iget v2, p0, Lcom/google/android/gms/internal/zzfn;->zzoH:I

    invoke-interface {v0, p1, p2, v1, v2}, Lcom/google/android/gms/internal/zzft;->zza(IIII)V

    :cond_d
    return-void
.end method

.method zzd(II)V
    .registers 7

    invoke-static {}, Lcom/google/android/gms/ads/internal/zzr;->zzbC()Lcom/google/android/gms/internal/zzir;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/internal/zzfn;->zzDh:Landroid/app/Activity;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/internal/zzir;->zzh(Landroid/app/Activity;)[I

    move-result-object v0

    const/4 v1, 0x0

    aget v3, v0, v1

    sub-int v0, p2, v3

    iget v1, p0, Lcom/google/android/gms/internal/zzfn;->zzoG:I

    iget v2, p0, Lcom/google/android/gms/internal/zzfn;->zzoH:I

    invoke-virtual {p0, p1, v0, v1, v2}, Lcom/google/android/gms/internal/zzfn;->zzb(IIII)V

    return-void
.end method

.method public zze(II)V
    .registers 3

    iput p1, p0, Lcom/google/android/gms/internal/zzfn;->zzDd:I

    iput p2, p0, Lcom/google/android/gms/internal/zzfn;->zzDe:I

    return-void
.end method

.method zzeL()Z
    .registers 3

    iget v0, p0, Lcom/google/android/gms/internal/zzfn;->zzoG:I

    const/4 v1, -0x1

    if-le v0, v1, :cond_c

    iget v0, p0, Lcom/google/android/gms/internal/zzfn;->zzoH:I

    const/4 v1, -0x1

    if-le v0, v1, :cond_c

    const/4 v0, 0x1

    goto :goto_d

    :cond_c
    const/4 v0, 0x0

    :goto_d
    return v0
.end method

.method public zzeN()Z
    .registers 4

    iget-object v1, p0, Lcom/google/android/gms/internal/zzfn;->zzpV:Ljava/lang/Object;

    monitor-enter v1

    :try_start_3
    iget-object v0, p0, Lcom/google/android/gms/internal/zzfn;->zzDl:Landroid/widget/PopupWindow;
    :try_end_5
    .catchall {:try_start_3 .. :try_end_5} :catchall_c

    if-eqz v0, :cond_9

    const/4 v0, 0x1

    goto :goto_a

    :cond_9
    const/4 v0, 0x0

    :goto_a
    monitor-exit v1

    return v0

    :catchall_c
    move-exception v2

    monitor-exit v1

    throw v2
.end method

.method zzeO()Z
    .registers 12

    invoke-static {}, Lcom/google/android/gms/ads/internal/zzr;->zzbC()Lcom/google/android/gms/internal/zzir;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/internal/zzfn;->zzDh:Landroid/app/Activity;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/internal/zzir;->zzf(Landroid/app/Activity;)[I

    move-result-object v3

    invoke-static {}, Lcom/google/android/gms/ads/internal/zzr;->zzbC()Lcom/google/android/gms/internal/zzir;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/internal/zzfn;->zzDh:Landroid/app/Activity;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/internal/zzir;->zzh(Landroid/app/Activity;)[I

    move-result-object v4

    const/4 v0, 0x0

    aget v5, v3, v0

    const/4 v0, 0x1

    aget v6, v3, v0

    iget v0, p0, Lcom/google/android/gms/internal/zzfn;->zzoG:I

    const/16 v1, 0x32

    if-lt v0, v1, :cond_24

    iget v0, p0, Lcom/google/android/gms/internal/zzfn;->zzoG:I

    if-le v0, v5, :cond_2b

    :cond_24
    const-string v0, "Width is too small or too large."

    invoke-static {v0}, Lcom/google/android/gms/internal/zzin;->zzaK(Ljava/lang/String;)V

    const/4 v0, 0x0

    return v0

    :cond_2b
    iget v0, p0, Lcom/google/android/gms/internal/zzfn;->zzoH:I

    const/16 v1, 0x32

    if-lt v0, v1, :cond_35

    iget v0, p0, Lcom/google/android/gms/internal/zzfn;->zzoH:I

    if-le v0, v6, :cond_3c

    :cond_35
    const-string v0, "Height is too small or too large."

    invoke-static {v0}, Lcom/google/android/gms/internal/zzin;->zzaK(Ljava/lang/String;)V

    const/4 v0, 0x0

    return v0

    :cond_3c
    iget v0, p0, Lcom/google/android/gms/internal/zzfn;->zzoH:I

    if-ne v0, v6, :cond_4b

    iget v0, p0, Lcom/google/android/gms/internal/zzfn;->zzoG:I

    if-ne v0, v5, :cond_4b

    const-string v0, "Cannot resize to a full-screen ad."

    invoke-static {v0}, Lcom/google/android/gms/internal/zzin;->zzaK(Ljava/lang/String;)V

    const/4 v0, 0x0

    return v0

    :cond_4b
    iget-boolean v0, p0, Lcom/google/android/gms/internal/zzfn;->zzDc:Z

    if-eqz v0, :cond_13a

    const/4 v7, -0x1

    const/4 v8, -0x2

    iget-object v9, p0, Lcom/google/android/gms/internal/zzfn;->zzDb:Ljava/lang/String;

    const/4 v10, -0x1

    invoke-virtual {v9}, Ljava/lang/String;->hashCode()I

    move-result v0

    sparse-switch v0, :sswitch_data_13c

    goto/16 :goto_98

    :sswitch_5d
    const-string v0, "top-left"

    invoke-virtual {v9, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_98

    const/4 v10, 0x0

    goto :goto_98

    :sswitch_67
    const-string v0, "top-center"

    invoke-virtual {v9, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_98

    const/4 v10, 0x1

    goto :goto_98

    :sswitch_71
    const-string v0, "center"

    invoke-virtual {v9, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_98

    const/4 v10, 0x2

    goto :goto_98

    :sswitch_7b
    const-string v0, "bottom-left"

    invoke-virtual {v9, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_98

    const/4 v10, 0x3

    goto :goto_98

    :sswitch_85
    const-string v0, "bottom-center"

    invoke-virtual {v9, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_98

    const/4 v10, 0x4

    goto :goto_98

    :sswitch_8f
    const-string v0, "bottom-right"

    invoke-virtual {v9, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_98

    const/4 v10, 0x5

    :cond_98
    :goto_98
    packed-switch v10, :pswitch_data_156

    goto/16 :goto_116

    :pswitch_9d
    iget v0, p0, Lcom/google/android/gms/internal/zzfn;->zzDd:I

    iget v1, p0, Lcom/google/android/gms/internal/zzfn;->zzDf:I

    add-int v7, v0, v1

    iget v0, p0, Lcom/google/android/gms/internal/zzfn;->zzDe:I

    iget v1, p0, Lcom/google/android/gms/internal/zzfn;->zzDg:I

    add-int v8, v0, v1

    goto/16 :goto_126

    :pswitch_ab
    iget v0, p0, Lcom/google/android/gms/internal/zzfn;->zzDd:I

    iget v1, p0, Lcom/google/android/gms/internal/zzfn;->zzDf:I

    add-int/2addr v0, v1

    iget v1, p0, Lcom/google/android/gms/internal/zzfn;->zzoG:I

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    add-int/lit8 v7, v0, -0x19

    iget v0, p0, Lcom/google/android/gms/internal/zzfn;->zzDe:I

    iget v1, p0, Lcom/google/android/gms/internal/zzfn;->zzDg:I

    add-int v8, v0, v1

    goto/16 :goto_126

    :pswitch_bf
    iget v0, p0, Lcom/google/android/gms/internal/zzfn;->zzDd:I

    iget v1, p0, Lcom/google/android/gms/internal/zzfn;->zzDf:I

    add-int/2addr v0, v1

    iget v1, p0, Lcom/google/android/gms/internal/zzfn;->zzoG:I

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    add-int/lit8 v7, v0, -0x19

    iget v0, p0, Lcom/google/android/gms/internal/zzfn;->zzDe:I

    iget v1, p0, Lcom/google/android/gms/internal/zzfn;->zzDg:I

    add-int/2addr v0, v1

    iget v1, p0, Lcom/google/android/gms/internal/zzfn;->zzoH:I

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    add-int/lit8 v8, v0, -0x19

    goto/16 :goto_126

    :pswitch_d9
    iget v0, p0, Lcom/google/android/gms/internal/zzfn;->zzDd:I

    iget v1, p0, Lcom/google/android/gms/internal/zzfn;->zzDf:I

    add-int v7, v0, v1

    iget v0, p0, Lcom/google/android/gms/internal/zzfn;->zzDe:I

    iget v1, p0, Lcom/google/android/gms/internal/zzfn;->zzDg:I

    add-int/2addr v0, v1

    iget v1, p0, Lcom/google/android/gms/internal/zzfn;->zzoH:I

    add-int/2addr v0, v1

    add-int/lit8 v8, v0, -0x32

    goto :goto_126

    :pswitch_ea
    iget v0, p0, Lcom/google/android/gms/internal/zzfn;->zzDd:I

    iget v1, p0, Lcom/google/android/gms/internal/zzfn;->zzDf:I

    add-int/2addr v0, v1

    iget v1, p0, Lcom/google/android/gms/internal/zzfn;->zzoG:I

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    add-int/lit8 v7, v0, -0x19

    iget v0, p0, Lcom/google/android/gms/internal/zzfn;->zzDe:I

    iget v1, p0, Lcom/google/android/gms/internal/zzfn;->zzDg:I

    add-int/2addr v0, v1

    iget v1, p0, Lcom/google/android/gms/internal/zzfn;->zzoH:I

    add-int/2addr v0, v1

    add-int/lit8 v8, v0, -0x32

    goto :goto_126

    :pswitch_101
    iget v0, p0, Lcom/google/android/gms/internal/zzfn;->zzDd:I

    iget v1, p0, Lcom/google/android/gms/internal/zzfn;->zzDf:I

    add-int/2addr v0, v1

    iget v1, p0, Lcom/google/android/gms/internal/zzfn;->zzoG:I

    add-int/2addr v0, v1

    add-int/lit8 v7, v0, -0x32

    iget v0, p0, Lcom/google/android/gms/internal/zzfn;->zzDe:I

    iget v1, p0, Lcom/google/android/gms/internal/zzfn;->zzDg:I

    add-int/2addr v0, v1

    iget v1, p0, Lcom/google/android/gms/internal/zzfn;->zzoH:I

    add-int/2addr v0, v1

    add-int/lit8 v8, v0, -0x32

    goto :goto_126

    :goto_116
    iget v0, p0, Lcom/google/android/gms/internal/zzfn;->zzDd:I

    iget v1, p0, Lcom/google/android/gms/internal/zzfn;->zzDf:I

    add-int/2addr v0, v1

    iget v1, p0, Lcom/google/android/gms/internal/zzfn;->zzoG:I

    add-int/2addr v0, v1

    add-int/lit8 v7, v0, -0x32

    iget v0, p0, Lcom/google/android/gms/internal/zzfn;->zzDe:I

    iget v1, p0, Lcom/google/android/gms/internal/zzfn;->zzDg:I

    add-int v8, v0, v1

    :goto_126
    if-ltz v7, :cond_138

    add-int/lit8 v0, v7, 0x32

    if-gt v0, v5, :cond_138

    const/4 v0, 0x0

    aget v0, v4, v0

    if-lt v8, v0, :cond_138

    add-int/lit8 v0, v8, 0x32

    const/4 v1, 0x1

    aget v1, v4, v1

    if-le v0, v1, :cond_13a

    :cond_138
    const/4 v0, 0x0

    return v0

    :cond_13a
    const/4 v0, 0x1

    return v0

    :sswitch_data_13c
    .sparse-switch
        -0x514d33ab -> :sswitch_71
        -0x3c587281 -> :sswitch_5d
        -0x27103597 -> :sswitch_7b
        0x455fe3fa -> :sswitch_8f
        0x4ccee637 -> :sswitch_85
        0x68a23bcd -> :sswitch_67
    .end sparse-switch

    :pswitch_data_156
    .packed-switch 0x0
        :pswitch_9d
        :pswitch_ab
        :pswitch_bf
        :pswitch_d9
        :pswitch_ea
        :pswitch_101
    .end packed-switch
.end method

.method public zzi(Ljava/util/Map;)V
    .registers 18
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;)V"
        }
    .end annotation

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/gms/internal/zzfn;->zzpV:Ljava/lang/Object;

    monitor-enter v6

    move-object/from16 v0, p0

    :try_start_7
    iget-object v0, v0, Lcom/google/android/gms/internal/zzfn;->zzDh:Landroid/app/Activity;

    if-nez v0, :cond_14

    const-string v0, "Not an activity context. Cannot resize."

    move-object/from16 v1, p0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/internal/zzfn;->zzam(Ljava/lang/String;)V
    :try_end_12
    .catchall {:try_start_7 .. :try_end_12} :catchall_32f

    monitor-exit v6

    return-void

    :cond_14
    move-object/from16 v0, p0

    :try_start_16
    iget-object v0, v0, Lcom/google/android/gms/internal/zzfn;->zzpD:Lcom/google/android/gms/internal/zzjp;

    invoke-interface {v0}, Lcom/google/android/gms/internal/zzjp;->zzaN()Lcom/google/android/gms/ads/internal/client/AdSizeParcel;

    move-result-object v0

    if-nez v0, :cond_27

    const-string v0, "Webview is not yet available, size is not set."

    move-object/from16 v1, p0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/internal/zzfn;->zzam(Ljava/lang/String;)V
    :try_end_25
    .catchall {:try_start_16 .. :try_end_25} :catchall_32f

    monitor-exit v6

    return-void

    :cond_27
    move-object/from16 v0, p0

    :try_start_29
    iget-object v0, v0, Lcom/google/android/gms/internal/zzfn;->zzpD:Lcom/google/android/gms/internal/zzjp;

    invoke-interface {v0}, Lcom/google/android/gms/internal/zzjp;->zzaN()Lcom/google/android/gms/ads/internal/client/AdSizeParcel;

    move-result-object v0

    iget-boolean v0, v0, Lcom/google/android/gms/ads/internal/client/AdSizeParcel;->zzui:Z

    if-eqz v0, :cond_3c

    const-string v0, "Is interstitial. Cannot resize an interstitial."

    move-object/from16 v1, p0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/internal/zzfn;->zzam(Ljava/lang/String;)V
    :try_end_3a
    .catchall {:try_start_29 .. :try_end_3a} :catchall_32f

    monitor-exit v6

    return-void

    :cond_3c
    move-object/from16 v0, p0

    :try_start_3e
    iget-object v0, v0, Lcom/google/android/gms/internal/zzfn;->zzpD:Lcom/google/android/gms/internal/zzjp;

    invoke-interface {v0}, Lcom/google/android/gms/internal/zzjp;->zzhY()Z

    move-result v0

    if-eqz v0, :cond_4f

    const-string v0, "Cannot resize an expanded banner."

    move-object/from16 v1, p0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/internal/zzfn;->zzam(Ljava/lang/String;)V
    :try_end_4d
    .catchall {:try_start_3e .. :try_end_4d} :catchall_32f

    monitor-exit v6

    return-void

    :cond_4f
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    :try_start_53
    invoke-direct {v0, v1}, Lcom/google/android/gms/internal/zzfn;->zzh(Ljava/util/Map;)V

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/internal/zzfn;->zzeL()Z

    move-result v0

    if-nez v0, :cond_65

    const-string v0, "Invalid width and height options. Cannot resize."

    move-object/from16 v1, p0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/internal/zzfn;->zzam(Ljava/lang/String;)V
    :try_end_63
    .catchall {:try_start_53 .. :try_end_63} :catchall_32f

    monitor-exit v6

    return-void

    :cond_65
    move-object/from16 v0, p0

    :try_start_67
    iget-object v0, v0, Lcom/google/android/gms/internal/zzfn;->zzDh:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v7

    if-eqz v7, :cond_75

    invoke-virtual {v7}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_7e

    :cond_75
    const-string v0, "Activity context is not ready, cannot get window or decor view."

    move-object/from16 v1, p0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/internal/zzfn;->zzam(Ljava/lang/String;)V
    :try_end_7c
    .catchall {:try_start_67 .. :try_end_7c} :catchall_32f

    monitor-exit v6

    return-void

    :cond_7e
    :try_start_7e
    invoke-direct/range {p0 .. p0}, Lcom/google/android/gms/internal/zzfn;->zzeM()[I

    move-result-object v8

    if-nez v8, :cond_8d

    const-string v0, "Resize location out of screen or close button is not visible."

    move-object/from16 v1, p0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/internal/zzfn;->zzam(Ljava/lang/String;)V
    :try_end_8b
    .catchall {:try_start_7e .. :try_end_8b} :catchall_32f

    monitor-exit v6

    return-void

    :cond_8d
    :try_start_8d
    invoke-static {}, Lcom/google/android/gms/ads/internal/client/zzn;->zzcS()Lcom/google/android/gms/ads/internal/util/client/zza;

    move-result-object v0

    move-object/from16 v1, p0

    iget-object v1, v1, Lcom/google/android/gms/internal/zzfn;->zzDh:Landroid/app/Activity;

    move-object/from16 v2, p0

    iget v2, v2, Lcom/google/android/gms/internal/zzfn;->zzoG:I

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/ads/internal/util/client/zza;->zzb(Landroid/content/Context;I)I

    move-result v9

    invoke-static {}, Lcom/google/android/gms/ads/internal/client/zzn;->zzcS()Lcom/google/android/gms/ads/internal/util/client/zza;

    move-result-object v0

    move-object/from16 v1, p0

    iget-object v1, v1, Lcom/google/android/gms/internal/zzfn;->zzDh:Landroid/app/Activity;

    move-object/from16 v2, p0

    iget v2, v2, Lcom/google/android/gms/internal/zzfn;->zzoH:I

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/ads/internal/util/client/zza;->zzb(Landroid/content/Context;I)I

    move-result v10

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/internal/zzfn;->zzpD:Lcom/google/android/gms/internal/zzjp;

    invoke-interface {v0}, Lcom/google/android/gms/internal/zzjp;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v11

    if-eqz v11, :cond_11e

    instance-of v0, v11, Landroid/view/ViewGroup;

    if-eqz v0, :cond_11e

    move-object v0, v11

    check-cast v0, Landroid/view/ViewGroup;

    move-object/from16 v1, p0

    iget-object v1, v1, Lcom/google/android/gms/internal/zzfn;->zzpD:Lcom/google/android/gms/internal/zzjp;

    invoke-interface {v1}, Lcom/google/android/gms/internal/zzjp;->getView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/internal/zzfn;->zzDl:Landroid/widget/PopupWindow;

    if-nez v0, :cond_116

    move-object v0, v11

    check-cast v0, Landroid/view/ViewGroup;

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/gms/internal/zzfn;->zzDn:Landroid/view/ViewGroup;

    invoke-static {}, Lcom/google/android/gms/ads/internal/zzr;->zzbC()Lcom/google/android/gms/internal/zzir;

    move-result-object v0

    move-object/from16 v1, p0

    iget-object v1, v1, Lcom/google/android/gms/internal/zzfn;->zzpD:Lcom/google/android/gms/internal/zzjp;

    invoke-interface {v1}, Lcom/google/android/gms/internal/zzjp;->getView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/internal/zzir;->zzk(Landroid/view/View;)Landroid/graphics/Bitmap;

    move-result-object v12

    new-instance v0, Landroid/widget/ImageView;

    move-object/from16 v1, p0

    iget-object v1, v1, Lcom/google/android/gms/internal/zzfn;->zzDh:Landroid/app/Activity;

    invoke-direct {v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/gms/internal/zzfn;->zzDi:Landroid/widget/ImageView;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/internal/zzfn;->zzDi:Landroid/widget/ImageView;

    invoke-virtual {v0, v12}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/internal/zzfn;->zzpD:Lcom/google/android/gms/internal/zzjp;

    invoke-interface {v0}, Lcom/google/android/gms/internal/zzjp;->zzaN()Lcom/google/android/gms/ads/internal/client/AdSizeParcel;

    move-result-object v0

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/gms/internal/zzfn;->zzCh:Lcom/google/android/gms/ads/internal/client/AdSizeParcel;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/internal/zzfn;->zzDn:Landroid/view/ViewGroup;

    move-object/from16 v1, p0

    iget-object v1, v1, Lcom/google/android/gms/internal/zzfn;->zzDi:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    goto :goto_127

    :cond_116
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/internal/zzfn;->zzDl:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V

    goto :goto_127

    :cond_11e
    const-string v0, "Webview is detached, probably in the middle of a resize or expand."

    move-object/from16 v1, p0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/internal/zzfn;->zzam(Ljava/lang/String;)V
    :try_end_125
    .catchall {:try_start_8d .. :try_end_125} :catchall_32f

    monitor-exit v6

    return-void

    :goto_127
    :try_start_127
    new-instance v0, Landroid/widget/RelativeLayout;

    move-object/from16 v1, p0

    iget-object v1, v1, Lcom/google/android/gms/internal/zzfn;->zzDh:Landroid/app/Activity;

    invoke-direct {v0, v1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/gms/internal/zzfn;->zzDm:Landroid/widget/RelativeLayout;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/internal/zzfn;->zzDm:Landroid/widget/RelativeLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setBackgroundColor(I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/internal/zzfn;->zzDm:Landroid/widget/RelativeLayout;

    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v1, v9, v10}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-static {}, Lcom/google/android/gms/ads/internal/zzr;->zzbC()Lcom/google/android/gms/internal/zzir;

    move-result-object v0

    move-object/from16 v1, p0

    iget-object v1, v1, Lcom/google/android/gms/internal/zzfn;->zzDm:Landroid/widget/RelativeLayout;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v9, v10, v2}, Lcom/google/android/gms/internal/zzir;->zza(Landroid/view/View;IIZ)Landroid/widget/PopupWindow;

    move-result-object v0

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/gms/internal/zzfn;->zzDl:Landroid/widget/PopupWindow;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/internal/zzfn;->zzDl:Landroid/widget/PopupWindow;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setOutsideTouchable(Z)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/internal/zzfn;->zzDl:Landroid/widget/PopupWindow;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setTouchable(Z)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/internal/zzfn;->zzDl:Landroid/widget/PopupWindow;

    move-object/from16 v1, p0

    iget-boolean v1, v1, Lcom/google/android/gms/internal/zzfn;->zzDc:Z

    if-nez v1, :cond_175

    const/4 v1, 0x1

    goto :goto_176

    :cond_175
    const/4 v1, 0x0

    :goto_176
    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setClippingEnabled(Z)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/internal/zzfn;->zzDm:Landroid/widget/RelativeLayout;

    move-object/from16 v1, p0

    iget-object v1, v1, Lcom/google/android/gms/internal/zzfn;->zzpD:Lcom/google/android/gms/internal/zzjp;

    invoke-interface {v1}, Lcom/google/android/gms/internal/zzjp;->getView()Landroid/view/View;

    move-result-object v1

    const/4 v2, -0x1

    const/4 v3, -0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;II)V

    new-instance v0, Landroid/widget/LinearLayout;

    move-object/from16 v1, p0

    iget-object v1, v1, Lcom/google/android/gms/internal/zzfn;->zzDh:Landroid/app/Activity;

    invoke-direct {v0, v1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/gms/internal/zzfn;->zzDj:Landroid/widget/LinearLayout;

    new-instance v12, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-static {}, Lcom/google/android/gms/ads/internal/client/zzn;->zzcS()Lcom/google/android/gms/ads/internal/util/client/zza;

    move-result-object v0

    move-object/from16 v1, p0

    iget-object v1, v1, Lcom/google/android/gms/internal/zzfn;->zzDh:Landroid/app/Activity;

    const/16 v2, 0x32

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/ads/internal/util/client/zza;->zzb(Landroid/content/Context;I)I

    move-result v0

    invoke-static {}, Lcom/google/android/gms/ads/internal/client/zzn;->zzcS()Lcom/google/android/gms/ads/internal/util/client/zza;

    move-result-object v1

    move-object/from16 v2, p0

    iget-object v2, v2, Lcom/google/android/gms/internal/zzfn;->zzDh:Landroid/app/Activity;

    const/16 v3, 0x32

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gms/ads/internal/util/client/zza;->zzb(Landroid/content/Context;I)I

    move-result v1

    invoke-direct {v12, v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/gms/internal/zzfn;->zzDb:Ljava/lang/String;

    const/4 v14, -0x1

    invoke-virtual {v13}, Ljava/lang/String;->hashCode()I

    move-result v0

    sparse-switch v0, :sswitch_data_334

    goto/16 :goto_201

    :sswitch_1c6
    const-string v0, "top-left"

    invoke-virtual {v13, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_201

    const/4 v14, 0x0

    goto :goto_201

    :sswitch_1d0
    const-string v0, "top-center"

    invoke-virtual {v13, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_201

    const/4 v14, 0x1

    goto :goto_201

    :sswitch_1da
    const-string v0, "center"

    invoke-virtual {v13, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_201

    const/4 v14, 0x2

    goto :goto_201

    :sswitch_1e4
    const-string v0, "bottom-left"

    invoke-virtual {v13, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_201

    const/4 v14, 0x3

    goto :goto_201

    :sswitch_1ee
    const-string v0, "bottom-center"

    invoke-virtual {v13, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_201

    const/4 v14, 0x4

    goto :goto_201

    :sswitch_1f8
    const-string v0, "bottom-right"

    invoke-virtual {v13, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_201

    const/4 v14, 0x5

    :cond_201
    :goto_201
    packed-switch v14, :pswitch_data_34e

    goto/16 :goto_243

    :pswitch_206
    const/16 v0, 0xa

    invoke-virtual {v12, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    const/16 v0, 0x9

    invoke-virtual {v12, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    goto :goto_24d

    :pswitch_211
    const/16 v0, 0xa

    invoke-virtual {v12, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    const/16 v0, 0xe

    invoke-virtual {v12, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    goto :goto_24d

    :pswitch_21c
    const/16 v0, 0xd

    invoke-virtual {v12, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    goto :goto_24d

    :pswitch_222
    const/16 v0, 0xc

    invoke-virtual {v12, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    const/16 v0, 0x9

    invoke-virtual {v12, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    goto :goto_24d

    :pswitch_22d
    const/16 v0, 0xc

    invoke-virtual {v12, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    const/16 v0, 0xe

    invoke-virtual {v12, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    goto :goto_24d

    :pswitch_238
    const/16 v0, 0xc

    invoke-virtual {v12, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    const/16 v0, 0xb

    invoke-virtual {v12, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    goto :goto_24d

    :goto_243
    const/16 v0, 0xa

    invoke-virtual {v12, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    const/16 v0, 0xb

    invoke-virtual {v12, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    :goto_24d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/internal/zzfn;->zzDj:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/google/android/gms/internal/zzfn$1;

    move-object/from16 v2, p0

    invoke-direct {v1, v2}, Lcom/google/android/gms/internal/zzfn$1;-><init>(Lcom/google/android/gms/internal/zzfn;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/internal/zzfn;->zzDj:Landroid/widget/LinearLayout;

    const-string v1, "Close button"

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/internal/zzfn;->zzDm:Landroid/widget/RelativeLayout;

    move-object/from16 v1, p0

    iget-object v1, v1, Lcom/google/android/gms/internal/zzfn;->zzDj:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1, v12}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    :try_end_26f
    .catchall {:try_start_127 .. :try_end_26f} :catchall_32f

    move-object/from16 v0, p0

    :try_start_271
    iget-object v0, v0, Lcom/google/android/gms/internal/zzfn;->zzDl:Landroid/widget/PopupWindow;

    invoke-virtual {v7}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    invoke-static {}, Lcom/google/android/gms/ads/internal/client/zzn;->zzcS()Lcom/google/android/gms/ads/internal/util/client/zza;

    move-result-object v2

    move-object/from16 v3, p0

    iget-object v3, v3, Lcom/google/android/gms/internal/zzfn;->zzDh:Landroid/app/Activity;

    const/4 v4, 0x0

    aget v4, v8, v4

    invoke-virtual {v2, v3, v4}, Lcom/google/android/gms/ads/internal/util/client/zza;->zzb(Landroid/content/Context;I)I

    move-result v2

    invoke-static {}, Lcom/google/android/gms/ads/internal/client/zzn;->zzcS()Lcom/google/android/gms/ads/internal/util/client/zza;

    move-result-object v3

    move-object/from16 v4, p0

    iget-object v4, v4, Lcom/google/android/gms/internal/zzfn;->zzDh:Landroid/app/Activity;

    const/4 v5, 0x1

    aget v5, v8, v5

    invoke-virtual {v3, v4, v5}, Lcom/google/android/gms/ads/internal/util/client/zza;->zzb(Landroid/content/Context;I)I

    move-result v3

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v4, v2, v3}, Landroid/widget/PopupWindow;->showAtLocation(Landroid/view/View;III)V
    :try_end_299
    .catch Ljava/lang/RuntimeException; {:try_start_271 .. :try_end_299} :catch_29a
    .catchall {:try_start_271 .. :try_end_299} :catchall_32f

    goto :goto_2f3

    :catch_29a
    move-exception v13

    :try_start_29b
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Cannot show popup window: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v13}, Ljava/lang/RuntimeException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    move-object/from16 v1, p0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/internal/zzfn;->zzam(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/internal/zzfn;->zzDm:Landroid/widget/RelativeLayout;

    move-object/from16 v1, p0

    iget-object v1, v1, Lcom/google/android/gms/internal/zzfn;->zzpD:Lcom/google/android/gms/internal/zzjp;

    invoke-interface {v1}, Lcom/google/android/gms/internal/zzjp;->getView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->removeView(Landroid/view/View;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/internal/zzfn;->zzDn:Landroid/view/ViewGroup;

    if-eqz v0, :cond_2f1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/internal/zzfn;->zzDn:Landroid/view/ViewGroup;

    move-object/from16 v1, p0

    iget-object v1, v1, Lcom/google/android/gms/internal/zzfn;->zzDi:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/internal/zzfn;->zzDn:Landroid/view/ViewGroup;

    move-object/from16 v1, p0

    iget-object v1, v1, Lcom/google/android/gms/internal/zzfn;->zzpD:Lcom/google/android/gms/internal/zzjp;

    invoke-interface {v1}, Lcom/google/android/gms/internal/zzjp;->getView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/internal/zzfn;->zzpD:Lcom/google/android/gms/internal/zzjp;

    move-object/from16 v1, p0

    iget-object v1, v1, Lcom/google/android/gms/internal/zzfn;->zzCh:Lcom/google/android/gms/ads/internal/client/AdSizeParcel;

    invoke-interface {v0, v1}, Lcom/google/android/gms/internal/zzjp;->zza(Lcom/google/android/gms/ads/internal/client/AdSizeParcel;)V
    :try_end_2f1
    .catchall {:try_start_29b .. :try_end_2f1} :catchall_32f

    :cond_2f1
    monitor-exit v6

    return-void

    :goto_2f3
    const/4 v0, 0x0

    :try_start_2f4
    aget v0, v8, v0

    const/4 v1, 0x1

    aget v1, v8, v1

    move-object/from16 v2, p0

    invoke-virtual {v2, v0, v1}, Lcom/google/android/gms/internal/zzfn;->zzc(II)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/internal/zzfn;->zzpD:Lcom/google/android/gms/internal/zzjp;

    new-instance v1, Lcom/google/android/gms/ads/internal/client/AdSizeParcel;

    move-object/from16 v2, p0

    iget-object v2, v2, Lcom/google/android/gms/internal/zzfn;->zzDh:Landroid/app/Activity;

    new-instance v3, Lcom/google/android/gms/ads/AdSize;

    move-object/from16 v4, p0

    iget v4, v4, Lcom/google/android/gms/internal/zzfn;->zzoG:I

    move-object/from16 v5, p0

    iget v5, v5, Lcom/google/android/gms/internal/zzfn;->zzoH:I

    invoke-direct {v3, v4, v5}, Lcom/google/android/gms/ads/AdSize;-><init>(II)V

    invoke-direct {v1, v2, v3}, Lcom/google/android/gms/ads/internal/client/AdSizeParcel;-><init>(Landroid/content/Context;Lcom/google/android/gms/ads/AdSize;)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/internal/zzjp;->zza(Lcom/google/android/gms/ads/internal/client/AdSizeParcel;)V

    const/4 v0, 0x0

    aget v0, v8, v0

    const/4 v1, 0x1

    aget v1, v8, v1

    move-object/from16 v2, p0

    invoke-virtual {v2, v0, v1}, Lcom/google/android/gms/internal/zzfn;->zzd(II)V

    const-string v0, "resized"

    move-object/from16 v1, p0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/internal/zzfn;->zzao(Ljava/lang/String;)V
    :try_end_32d
    .catchall {:try_start_2f4 .. :try_end_32d} :catchall_32f

    monitor-exit v6

    goto :goto_332

    :catchall_32f
    move-exception v15

    monitor-exit v6

    throw v15

    :goto_332
    return-void

    nop

    :sswitch_data_334
    .sparse-switch
        -0x514d33ab -> :sswitch_1da
        -0x3c587281 -> :sswitch_1c6
        -0x27103597 -> :sswitch_1e4
        0x455fe3fa -> :sswitch_1f8
        0x4ccee637 -> :sswitch_1ee
        0x68a23bcd -> :sswitch_1d0
    .end sparse-switch

    :pswitch_data_34e
    .packed-switch 0x0
        :pswitch_206
        :pswitch_211
        :pswitch_21c
        :pswitch_222
        :pswitch_22d
        :pswitch_238
    .end packed-switch
.end method

.method public zzp(Z)V
    .registers 6

    iget-object v2, p0, Lcom/google/android/gms/internal/zzfn;->zzpV:Ljava/lang/Object;

    monitor-enter v2

    :try_start_3
    iget-object v0, p0, Lcom/google/android/gms/internal/zzfn;->zzDl:Landroid/widget/PopupWindow;

    if-eqz v0, :cond_50

    iget-object v0, p0, Lcom/google/android/gms/internal/zzfn;->zzDl:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V

    iget-object v0, p0, Lcom/google/android/gms/internal/zzfn;->zzDm:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/google/android/gms/internal/zzfn;->zzpD:Lcom/google/android/gms/internal/zzjp;

    invoke-interface {v1}, Lcom/google/android/gms/internal/zzjp;->getView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->removeView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/gms/internal/zzfn;->zzDn:Landroid/view/ViewGroup;

    if-eqz v0, :cond_34

    iget-object v0, p0, Lcom/google/android/gms/internal/zzfn;->zzDn:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/google/android/gms/internal/zzfn;->zzDi:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/gms/internal/zzfn;->zzDn:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/google/android/gms/internal/zzfn;->zzpD:Lcom/google/android/gms/internal/zzjp;

    invoke-interface {v1}, Lcom/google/android/gms/internal/zzjp;->getView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/gms/internal/zzfn;->zzpD:Lcom/google/android/gms/internal/zzjp;

    iget-object v1, p0, Lcom/google/android/gms/internal/zzfn;->zzCh:Lcom/google/android/gms/ads/internal/client/AdSizeParcel;

    invoke-interface {v0, v1}, Lcom/google/android/gms/internal/zzjp;->zza(Lcom/google/android/gms/ads/internal/client/AdSizeParcel;)V

    :cond_34
    if-eqz p1, :cond_44

    const-string v0, "default"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/internal/zzfn;->zzao(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/internal/zzfn;->zzDk:Lcom/google/android/gms/internal/zzft;

    if-eqz v0, :cond_44

    iget-object v0, p0, Lcom/google/android/gms/internal/zzfn;->zzDk:Lcom/google/android/gms/internal/zzft;

    invoke-interface {v0}, Lcom/google/android/gms/internal/zzft;->zzbf()V

    :cond_44
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/internal/zzfn;->zzDl:Landroid/widget/PopupWindow;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/internal/zzfn;->zzDm:Landroid/widget/RelativeLayout;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/internal/zzfn;->zzDn:Landroid/view/ViewGroup;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/internal/zzfn;->zzDj:Landroid/widget/LinearLayout;
    :try_end_50
    .catchall {:try_start_3 .. :try_end_50} :catchall_52

    :cond_50
    monitor-exit v2

    goto :goto_55

    :catchall_52
    move-exception v3

    monitor-exit v2

    throw v3

    :goto_55
    return-void
.end method

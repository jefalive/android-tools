.class public final Lbr/com/itau/sdk/android/core/endpoint/p;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final m:[B

.field private static n:I


# instance fields
.field private final a:[Ljava/lang/annotation/Annotation;

.field private final b:Lbr/com/itau/sdk/android/core/endpoint/http/CallMethod;

.field private final c:Lbr/com/itau/sdk/android/core/model/RequestDTO;

.field private final d:Z

.field private final e:Ljava/lang/String;

.field private final f:Ljava/lang/String;

.field private final g:Lokhttp3/Headers$Builder;

.field private final h:Z

.field private final i:Z

.field private j:Lokhttp3/RequestBody;

.field private k:Ljava/lang/String;

.field private l:Ljava/lang/StringBuilder;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    const/16 v0, 0x21d

    new-array v0, v0, [B

    fill-array-data v0, :array_e

    sput-object v0, Lbr/com/itau/sdk/android/core/endpoint/p;->m:[B

    const/16 v0, 0xad

    sput v0, Lbr/com/itau/sdk/android/core/endpoint/p;->n:I

    return-void

    :array_e
    .array-data 1
        0x0t
        -0x80t
        -0x64t
        -0xet
        -0x69t
        -0x11t
        -0x23t
        0x2t
        -0x6t
        0x4t
        0xbt
        -0xdt
        0x2t
        0x4bt
        -0x51t
        0xet
        -0x12t
        0x10t
        -0xdt
        0x7t
        -0x10t
        0xet
        -0xet
        0x51t
        -0x57t
        0x14t
        -0xct
        -0xat
        0xft
        0x44t
        -0x4et
        -0x9t
        0x1t
        -0x2t
        0x53t
        -0x43t
        -0x4t
        0x44t
        -0x42t
        0x40t
        -0x33t
        -0x14t
        -0xdt
        -0x5t
        0xft
        -0xft
        -0x2t
        0x2ft
        -0x11t
        0x4t
        0x20t
        -0x12t
        -0x14t
        0xbt
        0x47t
        -0x53t
        0xct
        -0xct
        0x3t
        0xat
        -0x3t
        -0x3t
        -0x9t
        0x7t
        -0xat
        -0x7t
        0x53t
        -0x3t
        -0x2at
        -0x4t
        0x0t
        0x8t
        0x44t
        -0x4ft
        -0x2t
        -0x6t
        0x53t
        -0x54t
        -0x3t
        0x4t
        -0x1t
        0x0t
        -0x4t
        -0x3t
        0xet
        0x0t
        -0x25t
        0xft
        -0xet
        -0x8t
        0x58t
        -0x51t
        0xet
        -0x12t
        0x10t
        -0xdt
        0x4ct
        -0x3t
        -0x12t
        -0x14t
        0xbt
        0x47t
        -0x51t
        0xet
        -0x12t
        0x10t
        -0xdt
        0x7t
        -0x10t
        0xet
        -0xet
        0x51t
        -0x3t
        -0x13t
        -0x14t
        -0xdt
        0x20t
        -0x23t
        0x2t
        0x7t
        -0xct
        0xct
        -0xft
        -0x1t
        0x52t
        -0x51t
        0xet
        -0x12t
        0x10t
        -0xdt
        0x7t
        -0x10t
        0xet
        -0xet
        0x51t
        -0x57t
        0x14t
        -0xct
        -0xat
        0xft
        0x44t
        -0x4et
        -0x9t
        0x1t
        -0x2t
        0x53t
        -0x43t
        -0x4t
        0x44t
        -0x42t
        0x40t
        -0x34t
        0xet
        -0x8t
        -0x6t
        -0x23t
        0x2t
        0x7t
        -0xct
        0xct
        -0xft
        -0x1t
        0x26t
        -0x1et
        -0xbt
        -0x2t
        0xet
        -0xat
        0x8t
        -0xet
        0x43t
        0x6t
        0x1ct
        -0x22t
        0x1bt
        0x4t
        -0x22t
        0x4et
        -0x50t
        -0x2t
        0x4ft
        -0x47t
        -0xat
        -0x4t
        0x51t
        -0x55t
        0xbt
        0x2t
        0x44t
        -0x48t
        -0x3t
        -0xet
        0x10t
        -0xat
        0x4dt
        -0x50t
        -0x2t
        0xat
        -0xet
        0x10t
        -0x14t
        0xat
        -0x7t
        0x0t
        -0x1at
        0x2t
        -0x4t
        -0x2t
        -0x9t
        0x8t
        0x4dt
        -0x42t
        -0xet
        -0x1t
        -0x2t
        -0x6t
        0x12t
        -0x14t
        0xat
        -0x7t
        0x0t
        0x33t
        0x19t
        -0x14t
        0xct
        -0x9t
        -0x3t
        -0x6t
        0xet
        0x22t
        -0x34t
        0x6t
        0x9t
        -0x9t
        0x6t
        0x1ct
        -0x1at
        -0x13t
        0xat
        -0x2dt
        0x0t
        -0x7t
        0xet
        -0xat
        -0x7t
        0x46t
        -0x28t
        -0x26t
        0x8t
        0xat
        -0x2t
        0x1t
        -0x57t
        0x14t
        -0xct
        -0xat
        0xft
        0x44t
        -0x4et
        -0x9t
        0x1t
        -0x2t
        0x53t
        -0x4ft
        -0x2t
        -0x6t
        0x53t
        -0x43t
        -0x4t
        0x44t
        -0x4ft
        -0x8t
        0x8t
        -0x1t
        0x3dt
        -0xet
        0x1t
        -0x25t
        0xft
        -0xet
        -0x8t
        0x58t
        -0x51t
        0xet
        -0x12t
        0x10t
        -0xdt
        0x4ct
        -0x4ft
        0xct
        -0xdt
        0x7t
        0x44t
        -0x4et
        -0x9t
        0x1t
        -0x2t
        0x53t
        -0x4ft
        -0x2t
        -0x6t
        0x53t
        -0x43t
        -0x4t
        0x44t
        -0x4ft
        -0x8t
        0x8t
        -0x1t
        0x3dt
        0x0t
        0xdt
        0x18t
        -0xct
        -0x3t
        -0x2et
        0xat
        -0x16t
        0x58t
        -0x51t
        0xet
        -0x12t
        0x10t
        -0xdt
        0x7t
        -0x10t
        0xet
        -0xet
        0x51t
        -0x44t
        0x1t
        -0xet
        -0x1t
        -0x2t
        -0x6t
        0x53t
        -0x43t
        -0x4t
        0x44t
        -0x42t
        0x40t
        -0x33t
        -0x14t
        -0xdt
        -0x5t
        0xft
        -0xft
        -0x2t
        0x31t
        -0x2et
        0xat
        -0x16t
        -0x12t
        -0x14t
        0xbt
        0x47t
        -0x53t
        0xct
        -0xct
        0x3t
        0xat
        -0x3t
        -0x3t
        -0x9t
        0x7t
        -0xat
        -0x7t
        0x53t
        -0x4ft
        0xct
        -0xdt
        0x7t
        0x44t
        -0x4et
        -0x9t
        0x1t
        -0x2t
        0x53t
        -0x4ft
        -0x2t
        -0x6t
        0x53t
        -0x43t
        -0x4t
        0x44t
        -0x4ft
        -0x8t
        0x8t
        -0x1t
        0x3dt
        0x1t
        -0x57t
        0x14t
        -0xct
        -0xat
        0xft
        0x44t
        -0x55t
        0x4t
        0x4et
        -0x36t
        0x0t
        0xdt
        0x18t
        -0xct
        -0x3t
        -0x3t
        -0x2et
        0xat
        -0x16t
        0x58t
        -0x51t
        0xet
        -0x12t
        0x10t
        -0xdt
        0x7t
        -0x10t
        0xet
        -0xet
        0x51t
        -0x57t
        0x14t
        -0xct
        -0xat
        0xft
        0x44t
        -0x4et
        -0x9t
        0x1t
        -0x2t
        0x53t
        -0x4ft
        -0x2t
        -0x6t
        0x53t
        -0x43t
        -0x4t
        0x44t
        -0x4ft
        -0x8t
        0x8t
        -0x1t
        0x3dt
        -0x11t
        0x43t
        -0x36t
        -0xdt
        0x1t
        0x3et
        -0x3ct
        -0xct
        0x12t
        -0x15t
        0x46t
        -0x46t
        0xet
        -0x8t
        0x3ct
        -0x34t
        -0xet
        0x9t
        -0xft
        0x2t
        0x5t
        0x4t
        0x35t
        -0x36t
        -0xdt
        -0x4t
        0xct
        0x36t
        -0x1bt
        0x6t
        -0x13t
        0xat
        -0x18t
        0x13t
        0x5t
        -0x15t
        -0x1at
        0xct
        -0x2t
        -0xbt
        0x6t
        0x44t
        -0x55t
        0x4t
        0x4et
        -0x44t
        -0xdt
        0x0t
        -0x9t
        0x10t
        -0xet
        -0x3t
        0x53t
        -0x51t
        0xet
        -0x14t
        0xbt
        0x47t
        -0x51t
        0xet
        -0x12t
        0x10t
        -0xdt
        0x7t
        -0x10t
        0xet
        -0xet
        0x51t
        -0x3t
        -0x1et
        0x3t
        -0x4t
        -0x2t
        -0xet
        0x51t
        -0x4ft
        0xct
        -0xdt
        0x7t
        0x44t
        -0x4et
        -0x9t
        0x1t
        -0x2t
        0x53t
        -0x4ft
        -0x2t
        -0x6t
        0x53t
        -0x43t
        -0x4t
        0x44t
        -0x4ft
        -0x8t
        0x8t
        -0x1t
        0x3dt
    .end array-data
.end method

.method constructor <init>(Ljava/lang/String;Lbr/com/itau/sdk/android/core/endpoint/k;Z)V
    .registers 7

    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    new-instance v0, Lokhttp3/Headers$Builder;

    invoke-direct {v0}, Lokhttp3/Headers$Builder;-><init>()V

    iput-object v0, p0, Lbr/com/itau/sdk/android/core/endpoint/p;->g:Lokhttp3/Headers$Builder;

    .line 50
    new-instance v0, Lbr/com/itau/sdk/android/core/model/RequestDTO;

    iget-object v1, p2, Lbr/com/itau/sdk/android/core/endpoint/k;->e:Lbr/com/itau/sdk/android/core/endpoint/http/CallMethod;

    invoke-direct {v0, v1}, Lbr/com/itau/sdk/android/core/model/RequestDTO;-><init>(Lbr/com/itau/sdk/android/core/endpoint/http/CallMethod;)V

    iput-object v0, p0, Lbr/com/itau/sdk/android/core/endpoint/p;->c:Lbr/com/itau/sdk/android/core/model/RequestDTO;

    .line 51
    iget-boolean v0, p2, Lbr/com/itau/sdk/android/core/endpoint/k;->g:Z

    iput-boolean v0, p0, Lbr/com/itau/sdk/android/core/endpoint/p;->d:Z

    .line 52
    iput-object p1, p0, Lbr/com/itau/sdk/android/core/endpoint/p;->e:Ljava/lang/String;

    .line 53
    iget-object v0, p2, Lbr/com/itau/sdk/android/core/endpoint/k;->p:Ljava/lang/String;

    iput-object v0, p0, Lbr/com/itau/sdk/android/core/endpoint/p;->f:Ljava/lang/String;

    .line 54
    iput-boolean p3, p0, Lbr/com/itau/sdk/android/core/endpoint/p;->h:Z

    .line 55
    iget-boolean v0, p2, Lbr/com/itau/sdk/android/core/endpoint/k;->q:Z

    iput-boolean v0, p0, Lbr/com/itau/sdk/android/core/endpoint/p;->i:Z

    .line 57
    iget-object v0, p2, Lbr/com/itau/sdk/android/core/endpoint/k;->t:[Ljava/lang/annotation/Annotation;

    iput-object v0, p0, Lbr/com/itau/sdk/android/core/endpoint/p;->a:[Ljava/lang/annotation/Annotation;

    .line 58
    sget-object v0, Lbr/com/itau/sdk/android/core/endpoint/http/CallMethod;->POST:Lbr/com/itau/sdk/android/core/endpoint/http/CallMethod;

    iput-object v0, p0, Lbr/com/itau/sdk/android/core/endpoint/p;->b:Lbr/com/itau/sdk/android/core/endpoint/http/CallMethod;

    .line 60
    iget-object v0, p2, Lbr/com/itau/sdk/android/core/endpoint/k;->c:Lokhttp3/Headers$Builder;

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lbr/com/itau/sdk/android/core/endpoint/p;->a(Lokhttp3/Headers$Builder;Z)V

    .line 61
    iget-object v0, p2, Lbr/com/itau/sdk/android/core/endpoint/k;->b:Lokhttp3/Headers$Builder;

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lbr/com/itau/sdk/android/core/endpoint/p;->a(Lokhttp3/Headers$Builder;Z)V

    .line 62
    invoke-direct {p0}, Lbr/com/itau/sdk/android/core/endpoint/p;->c()V

    .line 64
    iget-object v0, p2, Lbr/com/itau/sdk/android/core/endpoint/k;->l:Ljava/lang/String;

    iput-object v0, p0, Lbr/com/itau/sdk/android/core/endpoint/p;->k:Ljava/lang/String;

    .line 66
    iget-object v2, p2, Lbr/com/itau/sdk/android/core/endpoint/k;->n:Ljava/lang/String;

    .line 68
    if-eqz v2, :cond_53

    .line 69
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v1, 0x3f

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/sdk/android/core/endpoint/p;->l:Ljava/lang/StringBuilder;

    .line 70
    :cond_53
    return-void
.end method

.method private static a(III)Ljava/lang/String;
    .registers 9

    add-int/lit8 p1, p1, 0x1

    rsub-int/lit8 p2, p2, 0x7d

    new-instance v0, Ljava/lang/String;

    sget-object v5, Lbr/com/itau/sdk/android/core/endpoint/p;->m:[B

    const/4 v4, 0x0

    rsub-int p0, p0, 0x201

    new-array v1, p1, [B

    add-int/lit8 p1, p1, -0x1

    if-nez v5, :cond_19

    move v2, p2

    move v3, p0

    :goto_13
    neg-int v3, v3

    add-int/2addr v2, v3

    add-int/lit8 p2, v2, -0x1

    add-int/lit8 p0, p0, 0x1

    :cond_19
    int-to-byte v2, p2

    aput-byte v2, v1, v4

    if-ne v4, p1, :cond_23

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Ljava/lang/String;-><init>([BI)V

    return-object v0

    :cond_23
    move v2, p2

    add-int/lit8 v4, v4, 0x1

    aget-byte v3, v5, p0

    goto :goto_13
.end method

.method private a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .registers 8

    .line 188
    instance-of v0, p2, Ljava/lang/Iterable;

    if-eqz v0, :cond_20

    .line 190
    move-object v0, p2

    check-cast v0, Ljava/lang/Iterable;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_b
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1f

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 192
    if-eqz v2, :cond_1e

    .line 193
    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0, p3}, Lbr/com/itau/sdk/android/core/endpoint/p;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 194
    :cond_1e
    goto :goto_b

    :cond_1f
    goto :goto_49

    .line 196
    :cond_20
    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->isArray()Z

    move-result v0

    if-eqz v0, :cond_42

    .line 197
    const/4 v1, 0x0

    invoke-static {p2}, Ljava/lang/reflect/Array;->getLength(Ljava/lang/Object;)I

    move-result v2

    :goto_2f
    if-ge v1, v2, :cond_41

    .line 198
    invoke-static {p2, v1}, Ljava/lang/reflect/Array;->get(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v3

    .line 199
    if-eqz v3, :cond_3e

    .line 200
    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0, p3}, Lbr/com/itau/sdk/android/core/endpoint/p;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 197
    :cond_3e
    add-int/lit8 v1, v1, 0x1

    goto :goto_2f

    :cond_41
    goto :goto_49

    .line 203
    :cond_42
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0, p3}, Lbr/com/itau/sdk/android/core/endpoint/p;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 205
    :goto_49
    return-void
.end method

.method private a(Lokhttp3/Headers$Builder;Z)V
    .registers 7

    .line 81
    invoke-virtual {p1}, Lokhttp3/Headers$Builder;->build()Lokhttp3/Headers;

    move-result-object v1

    .line 82
    invoke-virtual {v1}, Lokhttp3/Headers;->names()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_c
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_21

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Ljava/lang/String;

    .line 83
    invoke-virtual {v1, v3}, Lokhttp3/Headers;->values(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, v3, v0, p2}, Lbr/com/itau/sdk/android/core/endpoint/p;->a(Ljava/lang/String;Ljava/lang/Object;Z)V

    goto :goto_c

    .line 84
    :cond_21
    return-void
.end method

.method private b(Ljava/lang/String;Ljava/lang/Object;Z)V
    .registers 8

    .line 276
    instance-of v0, p2, Ljava/lang/Iterable;

    if-eqz v0, :cond_20

    .line 277
    move-object v0, p2

    check-cast v0, Ljava/lang/Iterable;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_b
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1f

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 278
    if-eqz v2, :cond_1e

    .line 279
    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0, p3}, Lbr/com/itau/sdk/android/core/endpoint/p;->c(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 280
    :cond_1e
    goto :goto_b

    :cond_1f
    goto :goto_49

    .line 281
    :cond_20
    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->isArray()Z

    move-result v0

    if-eqz v0, :cond_42

    .line 282
    const/4 v1, 0x0

    invoke-static {p2}, Ljava/lang/reflect/Array;->getLength(Ljava/lang/Object;)I

    move-result v2

    :goto_2f
    if-ge v1, v2, :cond_41

    .line 283
    invoke-static {p2, v1}, Ljava/lang/reflect/Array;->get(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v3

    .line 284
    if-eqz v3, :cond_3e

    .line 285
    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0, p3}, Lbr/com/itau/sdk/android/core/endpoint/p;->c(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 282
    :cond_3e
    add-int/lit8 v1, v1, 0x1

    goto :goto_2f

    :cond_41
    goto :goto_49

    .line 288
    :cond_42
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0, p3}, Lbr/com/itau/sdk/android/core/endpoint/p;->c(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 289
    :goto_49
    return-void
.end method

.method private b(Ljava/lang/String;Ljava/lang/String;Z)V
    .registers 11

    .line 247
    if-nez p1, :cond_1d

    .line 248
    new-instance v0, Ljava/lang/IllegalArgumentException;

    sget v1, Lbr/com/itau/sdk/android/core/endpoint/p;->n:I

    and-int/lit16 v1, v1, 0x3f3

    sget-object v2, Lbr/com/itau/sdk/android/core/endpoint/p;->m:[B

    const/16 v3, 0xa2

    aget-byte v2, v2, v3

    sget-object v3, Lbr/com/itau/sdk/android/core/endpoint/p;->m:[B

    const/16 v4, 0xef

    aget-byte v3, v3, v4

    neg-int v3, v3

    invoke-static {v1, v2, v3}, Lbr/com/itau/sdk/android/core/endpoint/p;->a(III)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 250
    :cond_1d
    if-nez p2, :cond_5b

    .line 251
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lbr/com/itau/sdk/android/core/endpoint/p;->m:[B

    const/4 v3, 0x5

    aget-byte v2, v2, v3

    neg-int v2, v2

    sget-object v3, Lbr/com/itau/sdk/android/core/endpoint/p;->m:[B

    const/16 v4, 0xef

    aget-byte v3, v3, v4

    neg-int v3, v3

    const/16 v4, 0x1ce

    invoke-static {v4, v2, v3}, Lbr/com/itau/sdk/android/core/endpoint/p;->a(III)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lbr/com/itau/sdk/android/core/endpoint/p;->m:[B

    const/16 v3, 0x138

    aget-byte v2, v2, v3

    or-int/lit8 v3, v2, 0x43

    const/16 v4, 0x106

    invoke-static {v4, v2, v3}, Lbr/com/itau/sdk/android/core/endpoint/p;->a(III)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 255
    :cond_5b
    if-nez p3, :cond_db

    .line 257
    :try_start_5d
    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lbr/com/itau/sdk/android/core/endpoint/p;->m:[B

    const/16 v2, 0x9

    aget-byte v1, v1, v2

    sget-object v2, Lbr/com/itau/sdk/android/core/endpoint/p;->m:[B

    const/16 v3, 0xf6

    aget-byte v2, v2, v3

    neg-int v2, v2

    const/16 v3, 0xcb

    invoke-static {v3, v1, v2}, Lbr/com/itau/sdk/android/core/endpoint/p;->a(III)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 262
    sget-object v0, Lbr/com/itau/sdk/android/core/endpoint/p;->m:[B

    const/4 v1, 0x0

    aget-byte v0, v0, v1

    sget-object v1, Lbr/com/itau/sdk/android/core/endpoint/p;->m:[B

    const/16 v2, 0x7c

    aget-byte v1, v1, v2

    const/16 v2, 0x7b

    invoke-static {v2, v0, v1}, Lbr/com/itau/sdk/android/core/endpoint/p;->a(III)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lbr/com/itau/sdk/android/core/endpoint/p;->m:[B

    const/4 v2, 0x7

    aget-byte v1, v1, v2

    sget-object v2, Lbr/com/itau/sdk/android/core/endpoint/p;->m:[B

    const/16 v3, 0x5a

    aget-byte v2, v2, v3

    const/16 v3, 0xee

    invoke-static {v3, v1, v2}, Lbr/com/itau/sdk/android/core/endpoint/p;->a(III)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v6

    .line 263
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/endpoint/p;->k:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget v2, Lbr/com/itau/sdk/android/core/endpoint/p;->n:I

    and-int/lit8 v2, v2, 0x73

    sget-object v3, Lbr/com/itau/sdk/android/core/endpoint/p;->m:[B

    const/4 v4, 0x0

    aget-byte v3, v3, v4

    sget-object v4, Lbr/com/itau/sdk/android/core/endpoint/p;->m:[B

    const/4 v5, 0x7

    aget-byte v4, v4, v5

    invoke-static {v2, v3, v4}, Lbr/com/itau/sdk/android/core/endpoint/p;->a(III)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lbr/com/itau/sdk/android/core/endpoint/p;->n:I

    and-int/lit8 v2, v2, 0x73

    sget-object v3, Lbr/com/itau/sdk/android/core/endpoint/p;->m:[B

    const/4 v4, 0x0

    aget-byte v3, v3, v4

    invoke-static {v2, v3, v3}, Lbr/com/itau/sdk/android/core/endpoint/p;->a(III)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v6}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/sdk/android/core/endpoint/p;->k:Ljava/lang/String;

    .line 265
    goto :goto_11b

    .line 266
    :cond_db
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/endpoint/p;->k:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget v2, Lbr/com/itau/sdk/android/core/endpoint/p;->n:I

    and-int/lit8 v2, v2, 0x73

    sget-object v3, Lbr/com/itau/sdk/android/core/endpoint/p;->m:[B

    const/4 v4, 0x0

    aget-byte v3, v3, v4

    sget-object v4, Lbr/com/itau/sdk/android/core/endpoint/p;->m:[B

    const/4 v5, 0x7

    aget-byte v4, v4, v5

    invoke-static {v2, v3, v4}, Lbr/com/itau/sdk/android/core/endpoint/p;->a(III)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lbr/com/itau/sdk/android/core/endpoint/p;->n:I

    and-int/lit8 v2, v2, 0x73

    sget-object v3, Lbr/com/itau/sdk/android/core/endpoint/p;->m:[B

    const/4 v4, 0x0

    aget-byte v3, v3, v4

    invoke-static {v2, v3, v3}, Lbr/com/itau/sdk/android/core/endpoint/p;->a(III)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/sdk/android/core/endpoint/p;->k:Ljava/lang/String;
    :try_end_11b
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_5d .. :try_end_11b} :catch_11c

    .line 271
    :goto_11b
    goto :goto_159

    .line 268
    :catch_11c
    move-exception v6

    .line 269
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget v2, Lbr/com/itau/sdk/android/core/endpoint/p;->n:I

    and-int/lit8 v2, v2, 0x73

    sget-object v3, Lbr/com/itau/sdk/android/core/endpoint/p;->m:[B

    const/16 v4, 0xf6

    aget-byte v3, v3, v4

    neg-int v3, v3

    invoke-static {v2, v2, v3}, Lbr/com/itau/sdk/android/core/endpoint/p;->a(III)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lbr/com/itau/sdk/android/core/endpoint/p;->m:[B

    const/16 v3, 0x11

    aget-byte v2, v2, v3

    or-int/lit8 v3, v2, 0x4b

    const/16 v4, 0x7b

    invoke-static {v4, v2, v3}, Lbr/com/itau/sdk/android/core/endpoint/p;->a(III)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, v6}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0

    .line 272
    :goto_159
    return-void
.end method

.method private c()V
    .registers 7

    .line 73
    new-instance v5, Lbr/com/itau/security/securestorage/SecureStorage;

    invoke-static {}, Lbr/com/itau/sdk/android/core/BackendClient;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v5, v0}, Lbr/com/itau/security/securestorage/SecureStorage;-><init>(Landroid/content/Context;)V

    .line 74
    sget-object v0, Lbr/com/itau/sdk/android/core/endpoint/p;->m:[B

    const/16 v1, 0xf5

    aget-byte v0, v0, v1

    add-int/lit8 v0, v0, -0x1

    sget v1, Lbr/com/itau/sdk/android/core/endpoint/p;->n:I

    and-int/lit8 v1, v1, 0x76

    sget-object v2, Lbr/com/itau/sdk/android/core/endpoint/p;->m:[B

    const/16 v3, 0xae

    aget-byte v2, v2, v3

    invoke-static {v0, v1, v2}, Lbr/com/itau/sdk/android/core/endpoint/p;->a(III)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v5, v0}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_58

    .line 75
    sget-object v0, Lbr/com/itau/sdk/android/core/endpoint/p;->m:[B

    const/16 v1, 0x11

    aget-byte v0, v0, v1

    sget-object v1, Lbr/com/itau/sdk/android/core/endpoint/p;->m:[B

    const/16 v2, 0xa3

    aget-byte v1, v1, v2

    neg-int v1, v1

    const/16 v2, 0x122

    invoke-static {v2, v0, v1}, Lbr/com/itau/sdk/android/core/endpoint/p;->a(III)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lbr/com/itau/sdk/android/core/endpoint/p;->m:[B

    const/16 v2, 0xf5

    aget-byte v1, v1, v2

    add-int/lit8 v1, v1, -0x1

    sget v2, Lbr/com/itau/sdk/android/core/endpoint/p;->n:I

    and-int/lit8 v2, v2, 0x76

    sget-object v3, Lbr/com/itau/sdk/android/core/endpoint/p;->m:[B

    const/16 v4, 0xae

    aget-byte v3, v3, v4

    invoke-static {v1, v2, v3}, Lbr/com/itau/sdk/android/core/endpoint/p;->a(III)Ljava/lang/String;

    move-result-object v1

    const-string v2, ""

    invoke-interface {v5, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {p0, v0, v1, v2}, Lbr/com/itau/sdk/android/core/endpoint/p;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 77
    :cond_58
    return-void
.end method

.method private c(Ljava/lang/String;Ljava/lang/String;Z)V
    .registers 9

    .line 293
    if-nez p1, :cond_16

    .line 294
    new-instance v0, Ljava/lang/IllegalArgumentException;

    sget v1, Lbr/com/itau/sdk/android/core/endpoint/p;->n:I

    and-int/lit8 v1, v1, 0x73

    sget v2, Lbr/com/itau/sdk/android/core/endpoint/p;->n:I

    and-int/lit8 v2, v2, 0x7e

    const/16 v3, 0xec

    invoke-static {v3, v1, v2}, Lbr/com/itau/sdk/android/core/endpoint/p;->a(III)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 296
    :cond_16
    if-nez p2, :cond_4f

    .line 297
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lbr/com/itau/sdk/android/core/endpoint/p;->m:[B

    const/16 v3, 0x38

    aget-byte v2, v2, v3

    or-int/lit8 v3, v2, 0x20

    const/16 v4, 0x1ab

    invoke-static {v4, v2, v3}, Lbr/com/itau/sdk/android/core/endpoint/p;->a(III)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lbr/com/itau/sdk/android/core/endpoint/p;->m:[B

    const/16 v3, 0x138

    aget-byte v2, v2, v3

    or-int/lit8 v3, v2, 0x43

    const/16 v4, 0x106

    invoke-static {v4, v2, v3}, Lbr/com/itau/sdk/android/core/endpoint/p;->a(III)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 299
    :cond_4f
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/endpoint/p;->c:Lbr/com/itau/sdk/android/core/model/RequestDTO;

    invoke-virtual {v0, p1, p2}, Lbr/com/itau/sdk/android/core/model/RequestDTO;->addQuery(Ljava/lang/String;Ljava/lang/String;)Lbr/com/itau/sdk/android/core/model/RequestDTO;

    .line 300
    return-void
.end method


# virtual methods
.method a([Ljava/lang/Object;)Lbr/com/itau/sdk/android/core/endpoint/p;
    .registers 15

    .line 110
    if-nez p1, :cond_5

    .line 111
    const/4 v0, 0x0

    new-array p1, v0, [Ljava/lang/Object;

    .line 113
    :cond_5
    const/4 v5, 0x0

    .line 115
    const/4 v6, 0x0

    .line 117
    const/4 v7, 0x0

    :goto_8
    array-length v0, p1

    if-ge v7, v0, :cond_167

    .line 118
    aget-object v8, p1, v7

    .line 119
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/endpoint/p;->a:[Ljava/lang/annotation/Annotation;

    aget-object v9, v0, v7

    .line 120
    invoke-interface {v9}, Ljava/lang/annotation/Annotation;->annotationType()Ljava/lang/Class;

    move-result-object v10

    .line 122
    const-class v0, Lbr/com/itau/sdk/android/core/endpoint/http/Path;

    if-ne v10, v0, :cond_79

    .line 123
    move-object v11, v9

    check-cast v11, Lbr/com/itau/sdk/android/core/endpoint/http/Path;

    .line 124
    invoke-interface {v11}, Lbr/com/itau/sdk/android/core/endpoint/http/Path;->value()Ljava/lang/String;

    move-result-object v12

    .line 126
    if-nez v8, :cond_5e

    .line 127
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lbr/com/itau/sdk/android/core/endpoint/p;->m:[B

    const/16 v3, 0x1c

    aget-byte v2, v2, v3

    sget-object v3, Lbr/com/itau/sdk/android/core/endpoint/p;->m:[B

    const/16 v4, 0xef

    aget-byte v3, v3, v4

    neg-int v3, v3

    const/16 v4, 0x19f

    invoke-static {v4, v2, v3}, Lbr/com/itau/sdk/android/core/endpoint/p;->a(III)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lbr/com/itau/sdk/android/core/endpoint/p;->m:[B

    const/16 v3, 0x138

    aget-byte v2, v2, v3

    or-int/lit8 v3, v2, 0x43

    const/16 v4, 0x106

    invoke-static {v4, v2, v3}, Lbr/com/itau/sdk/android/core/endpoint/p;->a(III)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 131
    :cond_5e
    iget-boolean v0, p0, Lbr/com/itau/sdk/android/core/endpoint/p;->d:Z

    if-eqz v0, :cond_6e

    .line 132
    invoke-virtual {v8}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v11}, Lbr/com/itau/sdk/android/core/endpoint/http/Path;->encoded()Z

    move-result v1

    invoke-direct {p0, v12, v0, v1}, Lbr/com/itau/sdk/android/core/endpoint/p;->b(Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_77

    .line 134
    :cond_6e
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/endpoint/p;->c:Lbr/com/itau/sdk/android/core/model/RequestDTO;

    invoke-virtual {v8}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v12, v1}, Lbr/com/itau/sdk/android/core/model/RequestDTO;->addPath(Ljava/lang/String;Ljava/lang/String;)Lbr/com/itau/sdk/android/core/model/RequestDTO;

    .line 136
    :goto_77
    goto/16 :goto_163

    :cond_79
    const-class v0, Lbr/com/itau/sdk/android/core/endpoint/http/Query;

    if-ne v10, v0, :cond_91

    .line 137
    if-nez v8, :cond_81

    .line 138
    goto/16 :goto_163

    .line 141
    :cond_81
    move-object v11, v9

    check-cast v11, Lbr/com/itau/sdk/android/core/endpoint/http/Query;

    .line 142
    invoke-interface {v11}, Lbr/com/itau/sdk/android/core/endpoint/http/Query;->value()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v11}, Lbr/com/itau/sdk/android/core/endpoint/http/Query;->encoded()Z

    move-result v1

    invoke-direct {p0, v0, v8, v1}, Lbr/com/itau/sdk/android/core/endpoint/p;->b(Ljava/lang/String;Ljava/lang/Object;Z)V

    .line 143
    goto/16 :goto_163

    :cond_91
    const-class v0, Lbr/com/itau/sdk/android/core/endpoint/http/Header;

    if-ne v10, v0, :cond_a6

    .line 144
    if-nez v8, :cond_99

    .line 145
    goto/16 :goto_163

    .line 148
    :cond_99
    move-object v0, v9

    check-cast v0, Lbr/com/itau/sdk/android/core/endpoint/http/Header;

    invoke-interface {v0}, Lbr/com/itau/sdk/android/core/endpoint/http/Header;->value()Ljava/lang/String;

    move-result-object v11

    .line 149
    const/4 v0, 0x0

    invoke-direct {p0, v11, v8, v0}, Lbr/com/itau/sdk/android/core/endpoint/p;->a(Ljava/lang/String;Ljava/lang/Object;Z)V

    .line 150
    goto/16 :goto_163

    :cond_a6
    const-class v0, Lbr/com/itau/sdk/android/core/endpoint/http/Body;

    if-ne v10, v0, :cond_e9

    .line 151
    if-nez v8, :cond_c4

    .line 152
    new-instance v0, Ljava/lang/IllegalArgumentException;

    sget-object v1, Lbr/com/itau/sdk/android/core/endpoint/p;->m:[B

    const/16 v2, 0xa2

    aget-byte v1, v1, v2

    sget-object v2, Lbr/com/itau/sdk/android/core/endpoint/p;->m:[B

    const/16 v3, 0x112

    aget-byte v2, v2, v3

    const/16 v3, 0x6b

    invoke-static {v3, v1, v2}, Lbr/com/itau/sdk/android/core/endpoint/p;->a(III)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 155
    :cond_c4
    instance-of v0, v8, Lokhttp3/RequestBody;

    if-eqz v0, :cond_e0

    .line 156
    new-instance v0, Ljava/lang/IllegalArgumentException;

    sget-object v1, Lbr/com/itau/sdk/android/core/endpoint/p;->m:[B

    const/16 v2, 0xa2

    aget-byte v1, v1, v2

    sget-object v2, Lbr/com/itau/sdk/android/core/endpoint/p;->m:[B

    const/16 v3, 0x112

    aget-byte v2, v2, v3

    const/16 v3, 0xc7

    invoke-static {v3, v1, v2}, Lbr/com/itau/sdk/android/core/endpoint/p;->a(III)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 159
    :cond_e0
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/endpoint/p;->c:Lbr/com/itau/sdk/android/core/model/RequestDTO;

    invoke-virtual {v0, v8}, Lbr/com/itau/sdk/android/core/model/RequestDTO;->setBody(Ljava/lang/Object;)V

    .line 160
    iget-object v5, p0, Lbr/com/itau/sdk/android/core/endpoint/p;->c:Lbr/com/itau/sdk/android/core/model/RequestDTO;

    goto/16 :goto_163

    .line 161
    :cond_e9
    const-class v0, Lbr/com/itau/sdk/android/core/endpoint/http/ReqProgress;

    if-ne v10, v0, :cond_109

    .line 162
    instance-of v0, v8, Lbr/com/itau/sdk/android/core/SDKProgressListener;

    if-nez v0, :cond_105

    .line 163
    new-instance v0, Ljava/lang/IllegalArgumentException;

    sget-object v1, Lbr/com/itau/sdk/android/core/endpoint/p;->m:[B

    const/16 v2, 0x112

    aget-byte v1, v1, v2

    const/16 v2, 0x190

    const/16 v3, 0x3a

    invoke-static {v2, v3, v1}, Lbr/com/itau/sdk/android/core/endpoint/p;->a(III)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 165
    :cond_105
    move-object v6, v8

    check-cast v6, Lbr/com/itau/sdk/android/core/SDKProgressListener;

    goto :goto_163

    .line 166
    :cond_109
    const-class v0, Lbr/com/itau/sdk/android/core/endpoint/http/a/a;

    if-ne v10, v0, :cond_135

    .line 167
    instance-of v0, v8, Lbr/com/itau/sdk/android/core/model/WebViewRequestDTO;

    if-nez v0, :cond_12a

    .line 168
    new-instance v0, Ljava/lang/IllegalArgumentException;

    sget-object v1, Lbr/com/itau/sdk/android/core/endpoint/p;->m:[B

    const/16 v2, 0x13b

    aget-byte v1, v1, v2

    neg-int v1, v1

    sget-object v2, Lbr/com/itau/sdk/android/core/endpoint/p;->m:[B

    const/16 v3, 0x112

    aget-byte v2, v2, v3

    const/16 v3, 0x1fc

    invoke-static {v3, v1, v2}, Lbr/com/itau/sdk/android/core/endpoint/p;->a(III)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 170
    :cond_12a
    move-object v11, v8

    check-cast v11, Lbr/com/itau/sdk/android/core/model/WebViewRequestDTO;

    .line 171
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/endpoint/p;->c:Lbr/com/itau/sdk/android/core/model/RequestDTO;

    invoke-virtual {v11, v0}, Lbr/com/itau/sdk/android/core/model/WebViewRequestDTO;->copy(Lbr/com/itau/sdk/android/core/model/RequestDTO;)V

    .line 172
    iget-object v5, p0, Lbr/com/itau/sdk/android/core/endpoint/p;->c:Lbr/com/itau/sdk/android/core/model/RequestDTO;

    .line 173
    goto :goto_163

    .line 174
    :cond_135
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lbr/com/itau/sdk/android/core/endpoint/p;->m:[B

    const/16 v3, 0x1dd

    aget-byte v2, v2, v3

    sget-object v3, Lbr/com/itau/sdk/android/core/endpoint/p;->m:[B

    const/16 v4, 0xf6

    aget-byte v3, v3, v4

    neg-int v3, v3

    const/16 v4, 0x135

    invoke-static {v4, v2, v3}, Lbr/com/itau/sdk/android/core/endpoint/p;->a(III)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v10}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 117
    :goto_163
    add-int/lit8 v7, v7, 0x1

    goto/16 :goto_8

    .line 177
    :cond_167
    if-eqz v5, :cond_176

    .line 178
    sget-object v0, Lbr/com/itau/sdk/android/core/n;->b:Lbr/com/itau/sdk/android/core/endpoint/i;

    invoke-virtual {v5}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v0, v5, v1, v6}, Lbr/com/itau/sdk/android/core/endpoint/i;->a(Ljava/lang/Object;Ljava/lang/reflect/Type;Lbr/com/itau/sdk/android/core/SDKProgressListener;)Lokhttp3/RequestBody;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/sdk/android/core/endpoint/p;->j:Lokhttp3/RequestBody;

    goto :goto_190

    .line 180
    :cond_176
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/endpoint/p;->c:Lbr/com/itau/sdk/android/core/model/RequestDTO;

    new-instance v1, Ljava/lang/Object;

    invoke-direct {v1}, Ljava/lang/Object;-><init>()V

    invoke-virtual {v0, v1}, Lbr/com/itau/sdk/android/core/model/RequestDTO;->setBody(Ljava/lang/Object;)V

    .line 181
    sget-object v0, Lbr/com/itau/sdk/android/core/n;->b:Lbr/com/itau/sdk/android/core/endpoint/i;

    iget-object v1, p0, Lbr/com/itau/sdk/android/core/endpoint/p;->c:Lbr/com/itau/sdk/android/core/model/RequestDTO;

    iget-object v2, p0, Lbr/com/itau/sdk/android/core/endpoint/p;->c:Lbr/com/itau/sdk/android/core/model/RequestDTO;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v0, v1, v2, v6}, Lbr/com/itau/sdk/android/core/endpoint/i;->a(Ljava/lang/Object;Ljava/lang/reflect/Type;Lbr/com/itau/sdk/android/core/SDKProgressListener;)Lokhttp3/RequestBody;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/sdk/android/core/endpoint/p;->j:Lokhttp3/RequestBody;

    .line 183
    :goto_190
    return-object p0
.end method

.method a()Lokhttp3/Request;
    .registers 8

    .line 208
    iget-object v4, p0, Lbr/com/itau/sdk/android/core/endpoint/p;->e:Ljava/lang/String;

    .line 209
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 211
    sget-object v0, Lbr/com/itau/sdk/android/core/endpoint/p;->m:[B

    const/4 v1, 0x0

    aget-byte v0, v0, v1

    sget-object v1, Lbr/com/itau/sdk/android/core/endpoint/p;->m:[B

    const/16 v2, 0xb1

    aget-byte v1, v1, v2

    const/16 v2, 0x7b

    invoke-static {v2, v0, v1}, Lbr/com/itau/sdk/android/core/endpoint/p;->a(III)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_27

    .line 213
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->deleteCharAt(I)Ljava/lang/StringBuilder;

    .line 215
    :cond_27
    iget-boolean v0, p0, Lbr/com/itau/sdk/android/core/endpoint/p;->d:Z

    if-eqz v0, :cond_39

    .line 216
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/endpoint/p;->k:Ljava/lang/String;

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 217
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/endpoint/p;->l:Ljava/lang/StringBuilder;

    if-eqz v0, :cond_39

    .line 218
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/endpoint/p;->l:Ljava/lang/StringBuilder;

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 221
    :cond_39
    new-instance v0, Lokhttp3/Request$Builder;

    invoke-direct {v0}, Lokhttp3/Request$Builder;-><init>()V

    .line 222
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lokhttp3/Request$Builder;->url(Ljava/lang/String;)Lokhttp3/Request$Builder;

    move-result-object v0

    iget-object v1, p0, Lbr/com/itau/sdk/android/core/endpoint/p;->b:Lbr/com/itau/sdk/android/core/endpoint/http/CallMethod;

    .line 223
    invoke-virtual {v1}, Lbr/com/itau/sdk/android/core/endpoint/http/CallMethod;->name()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lbr/com/itau/sdk/android/core/endpoint/p;->j:Lokhttp3/RequestBody;

    invoke-virtual {v0, v1, v2}, Lokhttp3/Request$Builder;->method(Ljava/lang/String;Lokhttp3/RequestBody;)Lokhttp3/Request$Builder;

    move-result-object v6

    .line 225
    iget-boolean v0, p0, Lbr/com/itau/sdk/android/core/endpoint/p;->d:Z

    if-nez v0, :cond_e6

    const-string v0, ""

    iget-object v1, p0, Lbr/com/itau/sdk/android/core/endpoint/p;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_e6

    .line 227
    invoke-static {}, Lbr/com/itau/sdk/android/core/BackendClient;->getFlowManager()Lbr/com/itau/sdk/android/core/FlowManager;

    move-result-object v0

    iget-object v1, p0, Lbr/com/itau/sdk/android/core/endpoint/p;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lbr/com/itau/sdk/android/core/FlowManager;->d(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_85

    .line 228
    new-instance v0, Lbr/com/itau/sdk/android/core/exception/e;

    sget-object v1, Lbr/com/itau/sdk/android/core/endpoint/p;->m:[B

    const/16 v2, 0xac

    aget-byte v1, v1, v2

    sget-object v2, Lbr/com/itau/sdk/android/core/endpoint/p;->m:[B

    const/16 v3, 0x2f

    aget-byte v2, v2, v3

    const/16 v3, 0x151

    invoke-static {v3, v1, v2}, Lbr/com/itau/sdk/android/core/endpoint/p;->a(III)Ljava/lang/String;

    move-result-object v1

    const/4 v2, -0x1

    invoke-direct {v0, v1, v2}, Lbr/com/itau/sdk/android/core/exception/e;-><init>(Ljava/lang/String;I)V

    throw v0

    .line 230
    :cond_85
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/endpoint/p;->f:Ljava/lang/String;

    sget-object v1, Lbr/com/itau/sdk/android/core/endpoint/p;->m:[B

    const/4 v2, 0x0

    aget-byte v1, v1, v2

    sget-object v2, Lbr/com/itau/sdk/android/core/endpoint/p;->m:[B

    const/16 v3, 0xa3

    aget-byte v2, v2, v3

    neg-int v2, v2

    const/16 v3, 0x107

    invoke-static {v3, v1, v2}, Lbr/com/itau/sdk/android/core/endpoint/p;->a(III)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c5

    iget-boolean v0, p0, Lbr/com/itau/sdk/android/core/endpoint/p;->h:Z

    if-eqz v0, :cond_c5

    .line 231
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/endpoint/p;->g:Lokhttp3/Headers$Builder;

    sget-object v1, Lbr/com/itau/sdk/android/core/endpoint/p;->m:[B

    const/16 v2, 0x20

    aget-byte v1, v1, v2

    sget-object v2, Lbr/com/itau/sdk/android/core/endpoint/p;->m:[B

    const/16 v3, 0xf

    aget-byte v2, v2, v3

    const/16 v3, 0x107

    invoke-static {v3, v1, v2}, Lbr/com/itau/sdk/android/core/endpoint/p;->a(III)Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lbr/com/itau/sdk/android/core/BackendClient;->getFlowManager()Lbr/com/itau/sdk/android/core/FlowManager;

    move-result-object v2

    iget-object v3, p0, Lbr/com/itau/sdk/android/core/endpoint/p;->f:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lbr/com/itau/sdk/android/core/FlowManager;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lokhttp3/Headers$Builder;->add(Ljava/lang/String;Ljava/lang/String;)Lokhttp3/Headers$Builder;

    goto :goto_e6

    .line 234
    :cond_c5
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/endpoint/p;->g:Lokhttp3/Headers$Builder;

    sget-object v1, Lbr/com/itau/sdk/android/core/endpoint/p;->m:[B

    const/16 v2, 0x20

    aget-byte v1, v1, v2

    sget-object v2, Lbr/com/itau/sdk/android/core/endpoint/p;->m:[B

    const/16 v3, 0xf

    aget-byte v2, v2, v3

    const/16 v3, 0x107

    invoke-static {v3, v1, v2}, Lbr/com/itau/sdk/android/core/endpoint/p;->a(III)Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lbr/com/itau/sdk/android/core/BackendClient;->getFlowManager()Lbr/com/itau/sdk/android/core/FlowManager;

    move-result-object v2

    iget-object v3, p0, Lbr/com/itau/sdk/android/core/endpoint/p;->f:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lbr/com/itau/sdk/android/core/FlowManager;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lokhttp3/Headers$Builder;->add(Ljava/lang/String;Ljava/lang/String;)Lokhttp3/Headers$Builder;

    .line 237
    :cond_e6
    :goto_e6
    iget-boolean v0, p0, Lbr/com/itau/sdk/android/core/endpoint/p;->i:Z

    if-eqz v0, :cond_10a

    .line 238
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/endpoint/p;->g:Lokhttp3/Headers$Builder;

    sget-object v1, Lbr/com/itau/sdk/android/core/endpoint/p;->m:[B

    const/16 v2, 0x1d0

    aget-byte v1, v1, v2

    sget-object v2, Lbr/com/itau/sdk/android/core/endpoint/p;->m:[B

    const/16 v3, 0x1c5

    aget-byte v2, v2, v3

    neg-int v2, v2

    const/16 v3, 0x156

    invoke-static {v3, v1, v2}, Lbr/com/itau/sdk/android/core/endpoint/p;->a(III)Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lbr/com/itau/sdk/android/core/BackendClient;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lbr/com/itau/security/did/DID;->getHDID(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lokhttp3/Headers$Builder;->add(Ljava/lang/String;Ljava/lang/String;)Lokhttp3/Headers$Builder;

    .line 240
    :cond_10a
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/endpoint/p;->g:Lokhttp3/Headers$Builder;

    .line 241
    invoke-virtual {v0}, Lokhttp3/Headers$Builder;->build()Lokhttp3/Headers;

    move-result-object v0

    invoke-virtual {v6, v0}, Lokhttp3/Request$Builder;->headers(Lokhttp3/Headers;)Lokhttp3/Request$Builder;

    move-result-object v0

    .line 242
    invoke-virtual {v0}, Lokhttp3/Request$Builder;->build()Lokhttp3/Request;

    move-result-object v0

    .line 240
    return-object v0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Z)V
    .registers 9

    .line 94
    if-nez p1, :cond_1d

    .line 95
    new-instance v0, Ljava/lang/IllegalArgumentException;

    sget-object v1, Lbr/com/itau/sdk/android/core/endpoint/p;->m:[B

    const/4 v2, 0x0

    aget-byte v1, v1, v2

    sget-object v2, Lbr/com/itau/sdk/android/core/endpoint/p;->m:[B

    const/16 v3, 0xac

    aget-byte v2, v2, v3

    sget-object v3, Lbr/com/itau/sdk/android/core/endpoint/p;->m:[B

    const/16 v4, 0x1d2

    aget-byte v3, v3, v4

    invoke-static {v1, v2, v3}, Lbr/com/itau/sdk/android/core/endpoint/p;->a(III)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 97
    :cond_1d
    sget-object v0, Lbr/com/itau/sdk/android/core/endpoint/p;->m:[B

    const/16 v1, 0xa

    aget-byte v0, v0, v1

    const/16 v1, 0x112

    const/16 v2, 0x3a

    invoke-static {v1, v0, v2}, Lbr/com/itau/sdk/android/core/endpoint/p;->a(III)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_32

    .line 99
    return-void

    .line 101
    :cond_32
    if-nez p3, :cond_3a

    .line 102
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/endpoint/p;->c:Lbr/com/itau/sdk/android/core/model/RequestDTO;

    invoke-virtual {v0, p1, p2}, Lbr/com/itau/sdk/android/core/model/RequestDTO;->addHeader(Ljava/lang/String;Ljava/lang/String;)Lbr/com/itau/sdk/android/core/model/RequestDTO;

    .line 103
    return-void

    .line 106
    :cond_3a
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/endpoint/p;->g:Lokhttp3/Headers$Builder;

    invoke-virtual {v0, p1, p2}, Lokhttp3/Headers$Builder;->add(Ljava/lang/String;Ljava/lang/String;)Lokhttp3/Headers$Builder;

    .line 107
    return-void
.end method

.method public b()Lbr/com/itau/sdk/android/core/model/RequestDTO;
    .registers 2

    .line 305
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/endpoint/p;->c:Lbr/com/itau/sdk/android/core/model/RequestDTO;

    return-object v0
.end method

.method public clone()Ljava/lang/Object;
    .registers 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .line 89
    new-instance v0, Ljava/lang/CloneNotSupportedException;

    sget v1, Lbr/com/itau/sdk/android/core/endpoint/p;->n:I

    or-int/lit16 v1, v1, 0x110

    sget-object v2, Lbr/com/itau/sdk/android/core/endpoint/p;->m:[B

    const/16 v3, 0xd8

    aget-byte v2, v2, v3

    or-int/lit8 v3, v2, 0x28

    invoke-static {v1, v2, v3}, Lbr/com/itau/sdk/android/core/endpoint/p;->a(III)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/CloneNotSupportedException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

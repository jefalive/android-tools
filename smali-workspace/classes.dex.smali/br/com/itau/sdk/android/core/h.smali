.class public final Lbr/com/itau/sdk/android/core/h;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lokhttp3/Interceptor;


# static fields
.field private static final a:[B

.field private static b:I


# direct methods
.method static constructor <clinit>()V
    .registers 3

    const/16 v0, 0x37

    new-array v0, v0, [B

    fill-array-data v0, :array_e

    sput-object v0, Lbr/com/itau/sdk/android/core/h;->a:[B

    const/16 v0, 0xd5

    sput v0, Lbr/com/itau/sdk/android/core/h;->b:I

    return-void

    :array_e
    .array-data 1
        0x42t
        0x5et
        0x76t
        -0x2t
        -0x2bt
        0x14t
        0x34t
        -0x1t
        -0xct
        -0x3bt
        0x27t
        0x1bt
        -0x4t
        -0x6t
        0x9t
        0x1et
        -0xet
        0xdt
        -0x45t
        0x14t
        0x26t
        -0x2t
        0x9t
        0x6t
        -0x2t
        0x3t
        -0x10t
        0x29t
        0x3t
        -0x1t
        -0x9t
        -0x45t
        0x4et
        0x1t
        0x5t
        -0x54t
        0x53t
        0x2t
        -0x5t
        0x0t
        -0x1t
        0x3t
        0x2t
        -0xft
        -0x1t
        0x9t
        -0x9t
        0x17t
        -0x26t
        0x13t
        0xct
        0x4t
        -0x10t
        0xet
        0x1t
    .end array-data
.end method

.method constructor <init>()V
    .registers 1

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    return-void
.end method

.method private static a(III)Ljava/lang/String;
    .registers 9

    rsub-int/lit8 p0, p0, 0x13

    new-instance v0, Ljava/lang/String;

    const/4 v5, 0x0

    add-int/lit8 p2, p2, 0x43

    rsub-int/lit8 p1, p1, 0x2c

    sget-object v4, Lbr/com/itau/sdk/android/core/h;->a:[B

    new-array v1, p0, [B

    add-int/lit8 p0, p0, -0x1

    if-nez v4, :cond_15

    move v2, p1

    move v3, p2

    :goto_13
    add-int p2, v2, v3

    :cond_15
    int-to-byte v2, p2

    add-int/lit8 p1, p1, 0x1

    aput-byte v2, v1, v5

    if-ne v5, p0, :cond_21

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Ljava/lang/String;-><init>([BI)V

    return-object v0

    :cond_21
    move v2, p2

    add-int/lit8 v5, v5, 0x1

    aget-byte v3, v4, p1

    goto :goto_13
.end method


# virtual methods
.method public clone()Ljava/lang/Object;
    .registers 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .line 42
    new-instance v0, Ljava/lang/CloneNotSupportedException;

    sget-object v1, Lbr/com/itau/sdk/android/core/h;->a:[B

    const/16 v2, 0x27

    aget-byte v1, v1, v2

    or-int/lit8 v2, v1, 0x12

    sget-object v3, Lbr/com/itau/sdk/android/core/h;->a:[B

    const/16 v4, 0x27

    aget-byte v3, v3, v4

    invoke-static {v1, v2, v3}, Lbr/com/itau/sdk/android/core/h;->a(III)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/CloneNotSupportedException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public intercept(Lokhttp3/Interceptor$Chain;)Lokhttp3/Response;
    .registers 11
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 19
    invoke-interface {p1}, Lokhttp3/Interceptor$Chain;->request()Lokhttp3/Request;

    move-result-object v0

    .line 20
    invoke-virtual {v0}, Lokhttp3/Request;->newBuilder()Lokhttp3/Request$Builder;

    move-result-object v0

    sget-object v1, Lbr/com/itau/sdk/android/core/h;->a:[B

    const/16 v2, 0xe

    aget-byte v1, v1, v2

    sget-object v2, Lbr/com/itau/sdk/android/core/h;->a:[B

    const/16 v3, 0xf

    aget-byte v2, v2, v3

    and-int/lit8 v3, v2, 0x73

    invoke-static {v1, v2, v3}, Lbr/com/itau/sdk/android/core/h;->a(III)Ljava/lang/String;

    move-result-object v1

    .line 21
    invoke-virtual {v0, v1}, Lokhttp3/Request$Builder;->removeHeader(Ljava/lang/String;)Lokhttp3/Request$Builder;

    move-result-object v0

    sget-object v1, Lbr/com/itau/sdk/android/core/h;->a:[B

    const/16 v2, 0x17

    aget-byte v1, v1, v2

    add-int/lit8 v1, v1, 0x1

    sget-object v2, Lbr/com/itau/sdk/android/core/h;->a:[B

    const/16 v3, 0x1b

    aget-byte v2, v2, v3

    sget v3, Lbr/com/itau/sdk/android/core/h;->b:I

    and-int/lit8 v3, v3, 0x3f

    invoke-static {v1, v2, v3}, Lbr/com/itau/sdk/android/core/h;->a(III)Ljava/lang/String;

    move-result-object v1

    .line 22
    invoke-virtual {v0, v1}, Lokhttp3/Request$Builder;->removeHeader(Ljava/lang/String;)Lokhttp3/Request$Builder;

    move-result-object v0

    sget-object v1, Lbr/com/itau/sdk/android/core/h;->a:[B

    const/16 v2, 0xe

    aget-byte v1, v1, v2

    sget-object v2, Lbr/com/itau/sdk/android/core/h;->a:[B

    const/16 v3, 0xf

    aget-byte v2, v2, v3

    and-int/lit8 v3, v2, 0x73

    invoke-static {v1, v2, v3}, Lbr/com/itau/sdk/android/core/h;->a(III)Ljava/lang/String;

    move-result-object v1

    .line 23
    invoke-static {}, Lbr/com/itau/sdk/android/core/BackendClient;->getUserAgent()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lokhttp3/Request$Builder;->addHeader(Ljava/lang/String;Ljava/lang/String;)Lokhttp3/Request$Builder;

    move-result-object v0

    sget-object v1, Lbr/com/itau/sdk/android/core/h;->a:[B

    const/16 v2, 0xe

    aget-byte v1, v1, v2

    add-int/lit8 v1, v1, -0x1

    sget-object v2, Lbr/com/itau/sdk/android/core/h;->a:[B

    const/16 v3, 0x27

    aget-byte v2, v2, v3

    sget-object v3, Lbr/com/itau/sdk/android/core/h;->a:[B

    const/16 v4, 0xf

    aget-byte v3, v3, v4

    invoke-static {v1, v2, v3}, Lbr/com/itau/sdk/android/core/h;->a(III)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lbr/com/itau/sdk/android/core/h;->a:[B

    const/16 v3, 0x2b

    aget-byte v2, v2, v3

    neg-int v2, v2

    sget v3, Lbr/com/itau/sdk/android/core/h;->b:I

    and-int/lit8 v3, v3, 0x3f

    const/16 v4, 0x31

    invoke-static {v2, v3, v4}, Lbr/com/itau/sdk/android/core/h;->a(III)Ljava/lang/String;

    move-result-object v2

    .line 24
    invoke-virtual {v0, v1, v2}, Lokhttp3/Request$Builder;->addHeader(Ljava/lang/String;Ljava/lang/String;)Lokhttp3/Request$Builder;

    move-result-object v5

    .line 26
    invoke-static {}, Lbr/com/itau/sdk/android/core/o;->a()Lbr/com/itau/sdk/android/core/o;

    move-result-object v0

    invoke-virtual {v0}, Lbr/com/itau/sdk/android/core/o;->g()Ljava/lang/String;

    move-result-object v6

    .line 28
    invoke-static {}, Lbr/com/itau/sdk/android/core/o;->a()Lbr/com/itau/sdk/android/core/o;

    move-result-object v0

    invoke-virtual {v0}, Lbr/com/itau/sdk/android/core/o;->c()Z

    move-result v0

    if-eqz v0, :cond_aa

    .line 29
    sget-object v0, Lbr/com/itau/sdk/android/core/h;->a:[B

    const/16 v1, 0x17

    aget-byte v0, v0, v1

    add-int/lit8 v0, v0, 0x1

    sget-object v1, Lbr/com/itau/sdk/android/core/h;->a:[B

    const/16 v2, 0x1b

    aget-byte v1, v1, v2

    sget v2, Lbr/com/itau/sdk/android/core/h;->b:I

    and-int/lit8 v2, v2, 0x3f

    invoke-static {v0, v1, v2}, Lbr/com/itau/sdk/android/core/h;->a(III)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0, v6}, Lokhttp3/Request$Builder;->addHeader(Ljava/lang/String;Ljava/lang/String;)Lokhttp3/Request$Builder;

    .line 31
    :cond_aa
    invoke-virtual {v5}, Lokhttp3/Request$Builder;->build()Lokhttp3/Request;

    move-result-object v7

    .line 33
    invoke-interface {p1, v7}, Lokhttp3/Interceptor$Chain;->proceed(Lokhttp3/Request;)Lokhttp3/Response;

    move-result-object v8

    .line 35
    invoke-static {}, Lbr/com/itau/sdk/android/core/o;->a()Lbr/com/itau/sdk/android/core/o;

    move-result-object v0

    sget-object v1, Lbr/com/itau/sdk/android/core/h;->a:[B

    const/16 v2, 0x17

    aget-byte v1, v1, v2

    add-int/lit8 v1, v1, 0x1

    sget-object v2, Lbr/com/itau/sdk/android/core/h;->a:[B

    const/16 v3, 0x1b

    aget-byte v2, v2, v3

    sget v3, Lbr/com/itau/sdk/android/core/h;->b:I

    and-int/lit8 v3, v3, 0x3f

    invoke-static {v1, v2, v3}, Lbr/com/itau/sdk/android/core/h;->a(III)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v8, v1, v6}, Lokhttp3/Response;->header(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbr/com/itau/sdk/android/core/o;->a(Ljava/lang/String;)V

    .line 37
    return-object v8
.end method

.class public final Lbr/com/itau/sdk/android/core/j;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lbr/com/itau/sdk/android/core/j$e;,
        Lbr/com/itau/sdk/android/core/j$a;,
        Lbr/com/itau/sdk/android/core/j$b;,
        Lbr/com/itau/sdk/android/core/j$c;,
        Lbr/com/itau/sdk/android/core/j$d;
    }
.end annotation


# static fields
.field private static b:I

.field private static final e:[B

.field private static f:I


# instance fields
.field private final c:Lde/greenrobot/event/EventBus;

.field private final d:Lokhttp3/OkHttpClient;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .line 20
    const/16 v0, 0x82

    new-array v0, v0, [B

    fill-array-data v0, :array_12

    sput-object v0, Lbr/com/itau/sdk/android/core/j;->e:[B

    const/16 v0, 0x91

    sput v0, Lbr/com/itau/sdk/android/core/j;->f:I

    const/4 v0, 0x0

    sput v0, Lbr/com/itau/sdk/android/core/j;->b:I

    return-void

    nop

    :array_12
    .array-data 1
        0x6t
        -0x3ft
        -0x1et
        -0x3t
        0x5t
        0x23t
        -0x2ft
        0x0t
        -0x1t
        0x2ft
        -0x2ft
        0x0t
        0x5t
        0x11t
        -0x12t
        -0xdt
        -0x4t
        -0x2dt
        0x0t
        0x3t
        0x4ft
        -0x41t
        -0xet
        0x4ft
        -0x43t
        0x2t
        -0x11t
        0x0t
        0xdt
        -0x2t
        0x6t
        -0x11t
        0x52t
        -0x41t
        0x41t
        -0x55t
        0x2t
        0x12t
        -0x1t
        -0x7t
        -0x3t
        0x3t
        0x5t
        0x3t
        -0x3t
        -0x1t
        0x23t
        0x14t
        0x2bt
        -0x14t
        -0x34t
        0x1t
        0xct
        0x3bt
        -0x27t
        -0x1bt
        0x4t
        0x6t
        -0x9t
        -0x1et
        0x12t
        -0x1t
        -0x7t
        -0x3t
        0x3t
        0x5t
        0x3t
        -0x3t
        -0x1t
        0x2bt
        0x1at
        -0x21t
        0x4ft
        -0x49t
        0x5t
        0x11t
        -0x12t
        -0xdt
        -0x4t
        0x56t
        -0x46t
        -0x9t
        -0x6t
        0x7t
        0xat
        -0x2dt
        0x0t
        0x3t
        0x4ft
        -0x41t
        -0xet
        0x4ft
        -0x43t
        0x2t
        -0x11t
        0x0t
        0xdt
        -0x2t
        0x6t
        -0x11t
        0x52t
        -0x41t
        0x41t
        -0x57t
        0x12t
        0x3t
        0xct
        -0x13t
        0x4t
        -0x12t
        -0x2dt
        0x0t
        0x3t
        0x4ft
        -0x41t
        -0xet
        0x4ft
        -0x43t
        0x2t
        -0x11t
        0x0t
        0xdt
        -0x2t
        0x6t
        -0x11t
        0x52t
        -0x55t
        0x3t
        0x6t
        0x4ct
    .end array-data
.end method

.method private constructor <init>()V
    .registers 3

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    new-instance v0, Lde/greenrobot/event/EventBus;

    invoke-direct {v0}, Lde/greenrobot/event/EventBus;-><init>()V

    iput-object v0, p0, Lbr/com/itau/sdk/android/core/j;->c:Lde/greenrobot/event/EventBus;

    .line 28
    new-instance v1, Lokhttp3/OkHttpClient$Builder;

    invoke-direct {v1}, Lokhttp3/OkHttpClient$Builder;-><init>()V

    .line 29
    invoke-static {v1}, Lbr/com/itau/sdk/android/core/i;->b(Lokhttp3/OkHttpClient$Builder;)V

    .line 30
    invoke-virtual {v1}, Lokhttp3/OkHttpClient$Builder;->build()Lokhttp3/OkHttpClient;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/sdk/android/core/j;->d:Lokhttp3/OkHttpClient;

    .line 31
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/j;->c:Lde/greenrobot/event/EventBus;

    invoke-virtual {v0, p0}, Lde/greenrobot/event/EventBus;->register(Ljava/lang/Object;)V

    .line 32
    return-void
.end method

.method synthetic constructor <init>(Lbr/com/itau/sdk/android/core/k;)V
    .registers 2

    .line 17
    invoke-direct {p0}, Lbr/com/itau/sdk/android/core/j;-><init>()V

    return-void
.end method

.method private static a(III)Ljava/lang/String;
    .registers 9

    sget-object v5, Lbr/com/itau/sdk/android/core/j;->e:[B

    rsub-int/lit8 p0, p0, 0x1e

    add-int/lit8 p1, p1, 0x45

    new-instance v0, Ljava/lang/String;

    rsub-int/lit8 p2, p2, 0x6e

    const/4 v4, -0x1

    new-array v1, p0, [B

    add-int/lit8 p0, p0, -0x1

    if-nez v5, :cond_18

    move v2, p2

    move v3, p0

    :goto_13
    neg-int v3, v3

    add-int p1, v2, v3

    add-int/lit8 p2, p2, 0x1

    :cond_18
    add-int/lit8 v4, v4, 0x1

    int-to-byte v2, p1

    aput-byte v2, v1, v4

    if-ne v4, p0, :cond_24

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Ljava/lang/String;-><init>([BI)V

    return-object v0

    :cond_24
    move v2, p1

    aget-byte v3, v5, p2

    goto :goto_13
.end method

.method public static a(Lbr/com/itau/sdk/android/core/j$b;)V
    .registers 2

    .line 162
    invoke-static {}, Lbr/com/itau/sdk/android/core/j$e;->a()Lbr/com/itau/sdk/android/core/j;

    move-result-object v0

    iget-object v0, v0, Lbr/com/itau/sdk/android/core/j;->c:Lde/greenrobot/event/EventBus;

    invoke-virtual {v0, p0}, Lde/greenrobot/event/EventBus;->post(Ljava/lang/Object;)V

    .line 163
    return-void
.end method

.method public static a(Lbr/com/itau/sdk/android/core/j$c;)V
    .registers 2

    .line 166
    invoke-static {}, Lbr/com/itau/sdk/android/core/j$e;->a()Lbr/com/itau/sdk/android/core/j;

    move-result-object v0

    iget-object v0, v0, Lbr/com/itau/sdk/android/core/j;->c:Lde/greenrobot/event/EventBus;

    invoke-virtual {v0, p0}, Lde/greenrobot/event/EventBus;->post(Ljava/lang/Object;)V

    .line 167
    return-void
.end method

.method private a(Lbr/com/itau/sdk/android/core/j$d;Ljava/io/IOException;)V
    .registers 8

    .line 120
    invoke-static {p2}, Lbr/com/itau/sdk/android/core/exception/BackendException;->networkError(Ljava/io/IOException;)Lbr/com/itau/sdk/android/core/exception/BackendException;

    move-result-object v4

    .line 121
    new-instance v0, Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent$Network;

    invoke-direct {v0, v4}, Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent$Network;-><init>(Lbr/com/itau/sdk/android/core/exception/BackendException;)V

    invoke-direct {p0, v0, p1}, Lbr/com/itau/sdk/android/core/j;->a(Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;Lbr/com/itau/sdk/android/core/j$d;)V

    .line 123
    instance-of v0, p1, Lbr/com/itau/sdk/android/core/j$c;

    if-eqz v0, :cond_3c

    .line 124
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lbr/com/itau/sdk/android/core/j;->e:[B

    const/16 v2, 0x3a

    aget-byte v1, v1, v2

    neg-int v1, v1

    sget-object v2, Lbr/com/itau/sdk/android/core/j;->e:[B

    const/4 v3, 0x7

    aget-byte v2, v2, v3

    invoke-static {v1, v2, v2}, Lbr/com/itau/sdk/android/core/j;->a(III)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    move-object v1, p1

    check-cast v1, Lbr/com/itau/sdk/android/core/j$c;

    # getter for: Lbr/com/itau/sdk/android/core/j$c;->url:Ljava/lang/String;
    invoke-static {v1}, Lbr/com/itau/sdk/android/core/j$c;->access$400(Lbr/com/itau/sdk/android/core/j$c;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, p2, v0}, Lbr/com/itau/sdk/android/core/l;->a(Ljava/lang/Object;Ljava/lang/Exception;Ljava/lang/String;)V

    goto :goto_4a

    .line 126
    :cond_3c
    sget-object v0, Lbr/com/itau/sdk/android/core/j;->e:[B

    const/4 v1, 0x7

    aget-byte v0, v0, v1

    or-int/lit8 v1, v0, 0x5d

    invoke-static {v0, v0, v1}, Lbr/com/itau/sdk/android/core/j;->a(III)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, p2, v0}, Lbr/com/itau/sdk/android/core/l;->a(Ljava/lang/Object;Ljava/lang/Exception;Ljava/lang/String;)V

    .line 128
    :goto_4a
    return-void
.end method

.method private a(Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;Lbr/com/itau/sdk/android/core/j$d;)V
    .registers 8

    .line 131
    instance-of v0, p2, Lbr/com/itau/sdk/android/core/j$b;

    if-eqz v0, :cond_31

    .line 132
    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;->getException()Lbr/com/itau/sdk/android/core/exception/BackendException;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lbr/com/itau/sdk/android/core/j;->e:[B

    const/16 v3, 0xd

    aget-byte v2, v2, v3

    add-int/lit8 v3, v2, -0x1

    or-int/lit8 v4, v3, 0x23

    invoke-static {v2, v3, v4}, Lbr/com/itau/sdk/android/core/j;->a(III)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object v2, p2

    check-cast v2, Lbr/com/itau/sdk/android/core/j$b;

    # getter for: Lbr/com/itau/sdk/android/core/j$b;->idServ:Ljava/lang/String;
    invoke-static {v2}, Lbr/com/itau/sdk/android/core/j$b;->access$000(Lbr/com/itau/sdk/android/core/j$b;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbr/com/itau/sdk/android/core/exception/BackendException;->setOpKey(Ljava/lang/String;)V

    .line 135
    :cond_31
    invoke-static {}, Lde/greenrobot/event/EventBus;->getDefault()Lde/greenrobot/event/EventBus;

    move-result-object v0

    invoke-virtual {v0, p1}, Lde/greenrobot/event/EventBus;->post(Ljava/lang/Object;)V

    .line 136
    return-void
.end method

.method private a(Lokhttp3/Request;Lbr/com/itau/sdk/android/core/j$d;)V
    .registers 9

    .line 92
    :try_start_0
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/j;->d:Lokhttp3/OkHttpClient;

    invoke-virtual {v0, p1}, Lokhttp3/OkHttpClient;->newCall(Lokhttp3/Request;)Lokhttp3/Call;

    move-result-object v0

    invoke-interface {v0}, Lokhttp3/Call;->execute()Lokhttp3/Response;

    move-result-object v3

    .line 94
    invoke-virtual {v3}, Lokhttp3/Response;->body()Lokhttp3/ResponseBody;

    move-result-object v0

    invoke-virtual {v0}, Lokhttp3/ResponseBody;->string()Ljava/lang/String;

    move-result-object v4

    .line 96
    invoke-virtual {v3}, Lokhttp3/Response;->code()I

    move-result v0

    const/16 v1, 0xc8

    if-eq v0, v1, :cond_3b

    .line 97
    invoke-static {v3, v4}, Lbr/com/itau/sdk/android/core/exception/BackendException;->httpError(Lokhttp3/Response;Ljava/lang/String;)Lbr/com/itau/sdk/android/core/exception/BackendException;

    move-result-object v5

    .line 98
    sget-object v0, Lbr/com/itau/sdk/android/core/j;->e:[B

    const/16 v1, 0x38

    aget-byte v0, v0, v1

    sget-object v1, Lbr/com/itau/sdk/android/core/j;->e:[B

    const/4 v2, 0x7

    aget-byte v1, v1, v2

    or-int/lit8 v2, v1, 0x19

    invoke-static {v0, v1, v2}, Lbr/com/itau/sdk/android/core/j;->a(III)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v5, v0}, Lbr/com/itau/sdk/android/core/l;->a(Ljava/lang/Object;Ljava/lang/Exception;Ljava/lang/String;)V

    .line 99
    new-instance v0, Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent$HTTP;

    invoke-direct {v0, v5}, Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent$HTTP;-><init>(Lbr/com/itau/sdk/android/core/exception/BackendException;)V

    invoke-direct {p0, v0, p2}, Lbr/com/itau/sdk/android/core/j;->a(Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;Lbr/com/itau/sdk/android/core/j$d;)V
    :try_end_3a
    .catch Ljavax/net/ssl/SSLHandshakeException; {:try_start_0 .. :try_end_3a} :catch_47
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_3a} :catch_59

    .line 100
    return-void

    .line 103
    :cond_3b
    move-object v5, v4

    .line 104
    :try_start_3c
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/j;->c:Lde/greenrobot/event/EventBus;

    new-instance v1, Lbr/com/itau/sdk/android/core/j$a;

    invoke-direct {v1, v5, p2}, Lbr/com/itau/sdk/android/core/j$a;-><init>(Ljava/lang/String;Lbr/com/itau/sdk/android/core/j$d;)V

    invoke-virtual {v0, v1}, Lde/greenrobot/event/EventBus;->post(Ljava/lang/Object;)V
    :try_end_46
    .catch Ljavax/net/ssl/SSLHandshakeException; {:try_start_3c .. :try_end_46} :catch_47
    .catch Ljava/io/IOException; {:try_start_3c .. :try_end_46} :catch_59

    .line 114
    goto :goto_5d

    .line 105
    :catch_47
    move-exception v3

    .line 106
    sget v0, Lbr/com/itau/sdk/android/core/j;->b:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lbr/com/itau/sdk/android/core/j;->b:I

    const/4 v1, 0x2

    if-ge v0, v1, :cond_55

    .line 107
    invoke-direct {p0, p1, p2}, Lbr/com/itau/sdk/android/core/j;->a(Lokhttp3/Request;Lbr/com/itau/sdk/android/core/j$d;)V

    .line 108
    return-void

    .line 110
    :cond_55
    invoke-direct {p0, p2, v3}, Lbr/com/itau/sdk/android/core/j;->a(Lbr/com/itau/sdk/android/core/j$d;Ljava/io/IOException;)V

    .line 114
    goto :goto_5d

    .line 112
    :catch_59
    move-exception v3

    .line 113
    invoke-direct {p0, p2, v3}, Lbr/com/itau/sdk/android/core/j;->a(Lbr/com/itau/sdk/android/core/j$d;Ljava/io/IOException;)V

    .line 116
    :goto_5d
    const/4 v0, 0x0

    sput v0, Lbr/com/itau/sdk/android/core/j;->b:I

    .line 117
    return-void
.end method


# virtual methods
.method public onEventBackgroundThread(Lbr/com/itau/sdk/android/core/j$b;)V
    .registers 10

    .line 38
    invoke-static {}, Lbr/com/itau/sdk/android/core/BackendClient;->getFlowManager()Lbr/com/itau/sdk/android/core/FlowManager;

    move-result-object v0

    .line 39
    invoke-virtual {v0}, Lbr/com/itau/sdk/android/core/FlowManager;->a()Landroid/support/v4/util/Pair;

    move-result-object v4

    .line 41
    if-nez v4, :cond_34

    .line 43
    new-instance v5, Ljava/lang/IllegalArgumentException;

    sget-object v0, Lbr/com/itau/sdk/android/core/j;->e:[B

    const/16 v1, 0x3a

    aget-byte v0, v0, v1

    neg-int v0, v0

    sget-object v1, Lbr/com/itau/sdk/android/core/j;->e:[B

    const/16 v2, 0x36

    aget-byte v1, v1, v2

    neg-int v1, v1

    const/16 v2, 0xf

    invoke-static {v2, v0, v1}, Lbr/com/itau/sdk/android/core/j;->a(III)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v5, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    .line 44
    invoke-static {v5}, Lbr/com/itau/sdk/android/core/exception/BackendException;->unexpectedError(Ljava/lang/Throwable;)Lbr/com/itau/sdk/android/core/exception/BackendException;

    move-result-object v6

    .line 45
    invoke-static {}, Lde/greenrobot/event/EventBus;->getDefault()Lde/greenrobot/event/EventBus;

    move-result-object v0

    new-instance v1, Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent$Unexpected;

    invoke-direct {v1, v6}, Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent$Unexpected;-><init>(Lbr/com/itau/sdk/android/core/exception/BackendException;)V

    invoke-virtual {v0, v1}, Lde/greenrobot/event/EventBus;->post(Ljava/lang/Object;)V

    .line 46
    throw v5

    .line 49
    :cond_34
    invoke-static {}, Lbr/com/itau/sdk/android/core/BackendClient;->getUserAgent()Ljava/lang/String;

    move-result-object v5

    .line 51
    # getter for: Lbr/com/itau/sdk/android/core/j$b;->builder:Lokhttp3/FormBody$Builder;
    invoke-static {p1}, Lbr/com/itau/sdk/android/core/j$b;->access$100(Lbr/com/itau/sdk/android/core/j$b;)Lokhttp3/FormBody$Builder;

    move-result-object v0

    sget-object v1, Lbr/com/itau/sdk/android/core/j;->e:[B

    const/16 v2, 0x46

    aget-byte v1, v1, v2

    add-int/lit8 v1, v1, -0x1

    sget v2, Lbr/com/itau/sdk/android/core/j;->f:I

    ushr-int/lit8 v2, v2, 0x2

    const/16 v3, 0x6a

    invoke-static {v1, v2, v3}, Lbr/com/itau/sdk/android/core/j;->a(III)Ljava/lang/String;

    move-result-object v1

    iget-object v2, v4, Landroid/support/v4/util/Pair;->first:Ljava/lang/Object;

    check-cast v2, Ljava/lang/String;

    .line 52
    invoke-virtual {v0, v1, v2}, Lokhttp3/FormBody$Builder;->add(Ljava/lang/String;Ljava/lang/String;)Lokhttp3/FormBody$Builder;

    move-result-object v0

    sget-object v1, Lbr/com/itau/sdk/android/core/j;->e:[B

    const/16 v2, 0x46

    aget-byte v1, v1, v2

    add-int/lit8 v1, v1, -0x1

    sget-object v2, Lbr/com/itau/sdk/android/core/j;->e:[B

    const/16 v3, 0x30

    aget-byte v2, v2, v3

    add-int/lit8 v2, v2, -0x1

    const/16 v3, 0x66

    invoke-static {v1, v2, v3}, Lbr/com/itau/sdk/android/core/j;->a(III)Ljava/lang/String;

    move-result-object v1

    iget-object v2, v4, Landroid/support/v4/util/Pair;->second:Ljava/lang/Object;

    check-cast v2, Ljava/lang/String;

    .line 53
    invoke-virtual {v0, v1, v2}, Lokhttp3/FormBody$Builder;->add(Ljava/lang/String;Ljava/lang/String;)Lokhttp3/FormBody$Builder;

    move-result-object v0

    sget v1, Lbr/com/itau/sdk/android/core/j;->f:I

    ushr-int/lit8 v1, v1, 0x2

    const/16 v2, 0x18

    const/16 v3, 0x62

    invoke-static {v2, v1, v3}, Lbr/com/itau/sdk/android/core/j;->a(III)Ljava/lang/String;

    move-result-object v1

    .line 54
    # getter for: Lbr/com/itau/sdk/android/core/j$b;->idServ:Ljava/lang/String;
    invoke-static {p1}, Lbr/com/itau/sdk/android/core/j$b;->access$000(Lbr/com/itau/sdk/android/core/j$b;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lokhttp3/FormBody$Builder;->add(Ljava/lang/String;Ljava/lang/String;)Lokhttp3/FormBody$Builder;

    move-result-object v0

    sget-object v1, Lbr/com/itau/sdk/android/core/j;->e:[B

    const/16 v2, 0x37

    aget-byte v1, v1, v2

    neg-int v1, v1

    sget-object v2, Lbr/com/itau/sdk/android/core/j;->e:[B

    const/16 v3, 0x22

    aget-byte v2, v2, v3

    add-int/lit8 v2, v2, -0x1

    const/16 v3, 0x33

    invoke-static {v1, v3, v2}, Lbr/com/itau/sdk/android/core/j;->a(III)Ljava/lang/String;

    move-result-object v1

    .line 55
    invoke-virtual {v0, v1, v5}, Lokhttp3/FormBody$Builder;->add(Ljava/lang/String;Ljava/lang/String;)Lokhttp3/FormBody$Builder;

    .line 57
    invoke-static {}, Lbr/com/itau/sdk/android/core/o;->a()Lbr/com/itau/sdk/android/core/o;

    move-result-object v0

    invoke-virtual {v0}, Lbr/com/itau/sdk/android/core/o;->c()Z

    move-result v0

    if-eqz v0, :cond_d0

    .line 58
    # getter for: Lbr/com/itau/sdk/android/core/j$b;->builder:Lokhttp3/FormBody$Builder;
    invoke-static {p1}, Lbr/com/itau/sdk/android/core/j$b;->access$100(Lbr/com/itau/sdk/android/core/j$b;)Lokhttp3/FormBody$Builder;

    move-result-object v0

    sget-object v1, Lbr/com/itau/sdk/android/core/j;->e:[B

    const/16 v2, 0x25

    aget-byte v1, v1, v2

    sget-object v2, Lbr/com/itau/sdk/android/core/j;->e:[B

    const/16 v3, 0x6b

    aget-byte v2, v2, v3

    neg-int v2, v2

    const/16 v3, 0x3e

    invoke-static {v1, v2, v3}, Lbr/com/itau/sdk/android/core/j;->a(III)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lbr/com/itau/sdk/android/core/o;->a()Lbr/com/itau/sdk/android/core/o;

    move-result-object v2

    invoke-virtual {v2}, Lbr/com/itau/sdk/android/core/o;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lokhttp3/FormBody$Builder;->add(Ljava/lang/String;Ljava/lang/String;)Lokhttp3/FormBody$Builder;

    .line 61
    :cond_d0
    new-instance v0, Lokhttp3/Request$Builder;

    invoke-direct {v0}, Lokhttp3/Request$Builder;-><init>()V

    .line 62
    # getter for: Lbr/com/itau/sdk/android/core/j$b;->url:Ljava/lang/String;
    invoke-static {p1}, Lbr/com/itau/sdk/android/core/j$b;->access$200(Lbr/com/itau/sdk/android/core/j$b;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lokhttp3/Request$Builder;->url(Ljava/lang/String;)Lokhttp3/Request$Builder;

    move-result-object v0

    .line 63
    # getter for: Lbr/com/itau/sdk/android/core/j$b;->builder:Lokhttp3/FormBody$Builder;
    invoke-static {p1}, Lbr/com/itau/sdk/android/core/j$b;->access$100(Lbr/com/itau/sdk/android/core/j$b;)Lokhttp3/FormBody$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lokhttp3/FormBody$Builder;->build()Lokhttp3/FormBody;

    move-result-object v1

    invoke-virtual {v0, v1}, Lokhttp3/Request$Builder;->post(Lokhttp3/RequestBody;)Lokhttp3/Request$Builder;

    move-result-object v6

    .line 66
    invoke-virtual {v6}, Lokhttp3/Request$Builder;->build()Lokhttp3/Request;

    move-result-object v7

    .line 67
    invoke-direct {p0, v7, p1}, Lbr/com/itau/sdk/android/core/j;->a(Lokhttp3/Request;Lbr/com/itau/sdk/android/core/j$d;)V

    .line 68
    return-void
.end method

.method public onEventBackgroundThread(Lbr/com/itau/sdk/android/core/j$c;)V
    .registers 9

    .line 73
    invoke-static {}, Lbr/com/itau/sdk/android/core/BackendClient;->getUserAgent()Ljava/lang/String;

    move-result-object v4

    .line 75
    # getter for: Lbr/com/itau/sdk/android/core/j$c;->builder:Lokhttp3/FormBody$Builder;
    invoke-static {p1}, Lbr/com/itau/sdk/android/core/j$c;->access$300(Lbr/com/itau/sdk/android/core/j$c;)Lokhttp3/FormBody$Builder;

    move-result-object v0

    sget-object v1, Lbr/com/itau/sdk/android/core/j;->e:[B

    const/16 v2, 0x37

    aget-byte v1, v1, v2

    neg-int v1, v1

    sget-object v2, Lbr/com/itau/sdk/android/core/j;->e:[B

    const/16 v3, 0x22

    aget-byte v2, v2, v3

    add-int/lit8 v2, v2, -0x1

    const/16 v3, 0x33

    invoke-static {v1, v3, v2}, Lbr/com/itau/sdk/android/core/j;->a(III)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v4}, Lokhttp3/FormBody$Builder;->add(Ljava/lang/String;Ljava/lang/String;)Lokhttp3/FormBody$Builder;

    .line 77
    new-instance v0, Lokhttp3/Request$Builder;

    invoke-direct {v0}, Lokhttp3/Request$Builder;-><init>()V

    .line 78
    # getter for: Lbr/com/itau/sdk/android/core/j$c;->url:Ljava/lang/String;
    invoke-static {p1}, Lbr/com/itau/sdk/android/core/j$c;->access$400(Lbr/com/itau/sdk/android/core/j$c;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lokhttp3/Request$Builder;->url(Ljava/lang/String;)Lokhttp3/Request$Builder;

    move-result-object v0

    .line 79
    # getter for: Lbr/com/itau/sdk/android/core/j$c;->builder:Lokhttp3/FormBody$Builder;
    invoke-static {p1}, Lbr/com/itau/sdk/android/core/j$c;->access$300(Lbr/com/itau/sdk/android/core/j$c;)Lokhttp3/FormBody$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lokhttp3/FormBody$Builder;->build()Lokhttp3/FormBody;

    move-result-object v1

    invoke-virtual {v0, v1}, Lokhttp3/Request$Builder;->post(Lokhttp3/RequestBody;)Lokhttp3/Request$Builder;

    move-result-object v5

    .line 81
    invoke-static {}, Lbr/com/itau/sdk/android/core/o;->a()Lbr/com/itau/sdk/android/core/o;

    move-result-object v0

    invoke-virtual {v0}, Lbr/com/itau/sdk/android/core/o;->c()Z

    move-result v0

    if-eqz v0, :cond_65

    .line 82
    sget-object v0, Lbr/com/itau/sdk/android/core/j;->e:[B

    const/16 v1, 0x25

    aget-byte v0, v0, v1

    sget-object v1, Lbr/com/itau/sdk/android/core/j;->e:[B

    const/16 v2, 0x6b

    aget-byte v1, v1, v2

    neg-int v1, v1

    const/16 v2, 0x3e

    invoke-static {v0, v1, v2}, Lbr/com/itau/sdk/android/core/j;->a(III)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    .line 83
    invoke-static {}, Lbr/com/itau/sdk/android/core/o;->a()Lbr/com/itau/sdk/android/core/o;

    move-result-object v1

    invoke-virtual {v1}, Lbr/com/itau/sdk/android/core/o;->g()Ljava/lang/String;

    move-result-object v1

    .line 82
    invoke-virtual {v5, v0, v1}, Lokhttp3/Request$Builder;->addHeader(Ljava/lang/String;Ljava/lang/String;)Lokhttp3/Request$Builder;

    .line 86
    :cond_65
    invoke-virtual {v5}, Lokhttp3/Request$Builder;->build()Lokhttp3/Request;

    move-result-object v6

    .line 87
    invoke-direct {p0, v6, p1}, Lbr/com/itau/sdk/android/core/j;->a(Lokhttp3/Request;Lbr/com/itau/sdk/android/core/j$d;)V

    .line 88
    return-void
.end method

.method public onEventMainThread(Lbr/com/itau/sdk/android/core/j$a;)V
    .registers 4

    .line 140
    invoke-static {p1}, Lbr/com/itau/sdk/android/core/j$a;->a(Lbr/com/itau/sdk/android/core/j$a;)Lbr/com/itau/sdk/android/core/j$b;

    move-result-object v0

    if-eqz v0, :cond_11

    .line 141
    invoke-static {p1}, Lbr/com/itau/sdk/android/core/j$a;->a(Lbr/com/itau/sdk/android/core/j$a;)Lbr/com/itau/sdk/android/core/j$b;

    move-result-object v0

    invoke-static {p1}, Lbr/com/itau/sdk/android/core/j$a;->b(Lbr/com/itau/sdk/android/core/j$a;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbr/com/itau/sdk/android/core/j$b;->onPageLoaded(Ljava/lang/String;)V

    .line 144
    :cond_11
    invoke-static {p1}, Lbr/com/itau/sdk/android/core/j$a;->c(Lbr/com/itau/sdk/android/core/j$a;)Lbr/com/itau/sdk/android/core/j$c;

    move-result-object v0

    if-eqz v0, :cond_22

    .line 145
    invoke-static {p1}, Lbr/com/itau/sdk/android/core/j$a;->c(Lbr/com/itau/sdk/android/core/j$a;)Lbr/com/itau/sdk/android/core/j$c;

    move-result-object v0

    invoke-static {p1}, Lbr/com/itau/sdk/android/core/j$a;->b(Lbr/com/itau/sdk/android/core/j$a;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbr/com/itau/sdk/android/core/j$c;->onPageLoaded(Ljava/lang/String;)V

    .line 147
    :cond_22
    return-void
.end method

.class public Lcom/itau/empresas/api/model/DadosPagamentoCodigoBarraComCartaoVO;
.super Ljava/lang/Object;
.source "DadosPagamentoCodigoBarraComCartaoVO.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private custoEfetivoTotalAnual:Ljava/lang/Float;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "custo_efetivo_total_anual"
    .end annotation
.end field

.field private custoEfetivoTotalMensal:Ljava/lang/Float;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "custo_efetivo_total_mensal"
    .end annotation
.end field

.field private dataVencimentoFatura:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "data_vencimento_fatura"
    .end annotation
.end field

.field private moeda:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "moeda"
    .end annotation
.end field

.field private nomeCartao:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "nome_cartao"
    .end annotation
.end field

.field private numeroCartao:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "numero_cartao"
    .end annotation
.end field

.field private periodoJuros:Ljava/lang/Float;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "periodo_juros"
    .end annotation
.end field

.field private taxaACobrar:Ljava/lang/Float;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "taxa_a_cobrar"
    .end annotation
.end field

.field private taxaJuros:Ljava/lang/Float;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "taxa_juros"
    .end annotation
.end field

.field private valorIof:Ljava/lang/Float;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "valor_iof"
    .end annotation
.end field

.field private valorJuros:Ljava/lang/Float;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "valor_juros"
    .end annotation
.end field

.field private valorTotalCobrado:Ljava/lang/Float;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "valor_total_cobrado"
    .end annotation
.end field

.field private valorTotalFinanciado:Ljava/lang/Float;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "valor_total_financiado"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

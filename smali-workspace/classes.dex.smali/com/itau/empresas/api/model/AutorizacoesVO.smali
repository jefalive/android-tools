.class public Lcom/itau/empresas/api/model/AutorizacoesVO;
.super Ljava/lang/Object;
.source "AutorizacoesVO.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field public numeroLote:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "numero_lote"
    .end annotation
.end field

.field public numeroPagamento:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "numero_pagamento"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getNumeroLote()Ljava/lang/String;
    .registers 2

    .line 19
    iget-object v0, p0, Lcom/itau/empresas/api/model/AutorizacoesVO;->numeroLote:Ljava/lang/String;

    return-object v0
.end method

.method public getNumeroPagamento()Ljava/lang/String;
    .registers 2

    .line 27
    iget-object v0, p0, Lcom/itau/empresas/api/model/AutorizacoesVO;->numeroPagamento:Ljava/lang/String;

    return-object v0
.end method

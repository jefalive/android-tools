.class public Lcom/viewpagerindicator/TitlePageIndicator;
.super Landroid/view/View;
.source "TitlePageIndicator.java"

# interfaces
.implements Lcom/viewpagerindicator/PageIndicator;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/viewpagerindicator/TitlePageIndicator$1;,
        Lcom/viewpagerindicator/TitlePageIndicator$SavedState;,
        Lcom/viewpagerindicator/TitlePageIndicator$LinePosition;,
        Lcom/viewpagerindicator/TitlePageIndicator$IndicatorStyle;,
        Lcom/viewpagerindicator/TitlePageIndicator$OnCenterItemClickListener;
    }
.end annotation


# instance fields
.field private mActivePointerId:I

.field private mBoldText:Z

.field private final mBounds:Landroid/graphics/Rect;

.field private mCenterItemClickListener:Lcom/viewpagerindicator/TitlePageIndicator$OnCenterItemClickListener;

.field private mClipPadding:F

.field private mColorSelected:I

.field private mColorText:I

.field private mCurrentPage:I

.field private mFooterIndicatorHeight:F

.field private mFooterIndicatorStyle:Lcom/viewpagerindicator/TitlePageIndicator$IndicatorStyle;

.field private mFooterIndicatorUnderlinePadding:F

.field private mFooterLineHeight:F

.field private mFooterPadding:F

.field private mIsDragging:Z

.field private mLastMotionX:F

.field private mLinePosition:Lcom/viewpagerindicator/TitlePageIndicator$LinePosition;

.field private mListener:Landroid/support/v4/view/ViewPager$OnPageChangeListener;

.field private mPageOffset:F

.field private final mPaintFooterIndicator:Landroid/graphics/Paint;

.field private final mPaintFooterLine:Landroid/graphics/Paint;

.field private final mPaintText:Landroid/graphics/Paint;

.field private mPath:Landroid/graphics/Path;

.field private mScrollState:I

.field private mTitlePadding:F

.field private mTopPadding:F

.field private mTouchSlop:I

.field private mViewPager:Landroid/support/v4/view/ViewPager;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .param p1, "context"    # Landroid/content/Context;

    .line 152
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/viewpagerindicator/TitlePageIndicator;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 153
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .line 156
    sget v0, Lcom/viewpagerindicator/R$attr;->vpiTitlePageIndicatorStyle:I

    invoke-direct {p0, p1, p2, v0}, Lcom/viewpagerindicator/TitlePageIndicator;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 157
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 29
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .line 160
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move/from16 v3, p3

    invoke-direct {v0, v1, v2, v3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 119
    const/4 v0, -0x1

    move-object/from16 v1, p0

    iput v0, v1, Lcom/viewpagerindicator/TitlePageIndicator;->mCurrentPage:I

    .line 122
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/viewpagerindicator/TitlePageIndicator;->mPaintText:Landroid/graphics/Paint;

    .line 126
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/viewpagerindicator/TitlePageIndicator;->mPath:Landroid/graphics/Path;

    .line 127
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/viewpagerindicator/TitlePageIndicator;->mBounds:Landroid/graphics/Rect;

    .line 128
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/viewpagerindicator/TitlePageIndicator;->mPaintFooterLine:Landroid/graphics/Paint;

    .line 131
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/viewpagerindicator/TitlePageIndicator;->mPaintFooterIndicator:Landroid/graphics/Paint;

    .line 144
    const/high16 v0, -0x40800000    # -1.0f

    move-object/from16 v1, p0

    iput v0, v1, Lcom/viewpagerindicator/TitlePageIndicator;->mLastMotionX:F

    .line 145
    const/4 v0, -0x1

    move-object/from16 v1, p0

    iput v0, v1, Lcom/viewpagerindicator/TitlePageIndicator;->mActivePointerId:I

    .line 161
    invoke-virtual/range {p0 .. p0}, Lcom/viewpagerindicator/TitlePageIndicator;->isInEditMode()Z

    move-result v0

    if-eqz v0, :cond_4f

    return-void

    .line 164
    :cond_4f
    invoke-virtual/range {p0 .. p0}, Lcom/viewpagerindicator/TitlePageIndicator;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    .line 165
    .local v5, "res":Landroid/content/res/Resources;
    sget v0, Lcom/viewpagerindicator/R$color;->default_title_indicator_footer_color:I

    invoke-virtual {v5, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v6

    .line 166
    .local v6, "defaultFooterColor":I
    sget v0, Lcom/viewpagerindicator/R$dimen;->default_title_indicator_footer_line_height:I

    invoke-virtual {v5, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v7

    .line 167
    .local v7, "defaultFooterLineHeight":F
    sget v0, Lcom/viewpagerindicator/R$integer;->default_title_indicator_footer_indicator_style:I

    invoke-virtual {v5, v0}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v8

    .line 168
    .local v8, "defaultFooterIndicatorStyle":I
    sget v0, Lcom/viewpagerindicator/R$dimen;->default_title_indicator_footer_indicator_height:I

    invoke-virtual {v5, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v9

    .line 169
    .local v9, "defaultFooterIndicatorHeight":F
    sget v0, Lcom/viewpagerindicator/R$dimen;->default_title_indicator_footer_indicator_underline_padding:I

    invoke-virtual {v5, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v10

    .line 170
    .local v10, "defaultFooterIndicatorUnderlinePadding":F
    sget v0, Lcom/viewpagerindicator/R$dimen;->default_title_indicator_footer_padding:I

    invoke-virtual {v5, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v11

    .line 171
    .local v11, "defaultFooterPadding":F
    sget v0, Lcom/viewpagerindicator/R$integer;->default_title_indicator_line_position:I

    invoke-virtual {v5, v0}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v12

    .line 172
    .local v12, "defaultLinePosition":I
    sget v0, Lcom/viewpagerindicator/R$color;->default_title_indicator_selected_color:I

    invoke-virtual {v5, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v13

    .line 173
    .local v13, "defaultSelectedColor":I
    sget v0, Lcom/viewpagerindicator/R$bool;->default_title_indicator_selected_bold:I

    invoke-virtual {v5, v0}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v14

    .line 174
    .local v14, "defaultSelectedBold":Z
    sget v0, Lcom/viewpagerindicator/R$color;->default_title_indicator_text_color:I

    invoke-virtual {v5, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v15

    .line 175
    .local v15, "defaultTextColor":I
    sget v0, Lcom/viewpagerindicator/R$dimen;->default_title_indicator_text_size:I

    invoke-virtual {v5, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v16

    .line 176
    .local v16, "defaultTextSize":F
    sget v0, Lcom/viewpagerindicator/R$dimen;->default_title_indicator_title_padding:I

    invoke-virtual {v5, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v17

    .line 177
    .local v17, "defaultTitlePadding":F
    sget v0, Lcom/viewpagerindicator/R$dimen;->default_title_indicator_clip_padding:I

    invoke-virtual {v5, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v18

    .line 178
    .local v18, "defaultClipPadding":F
    sget v0, Lcom/viewpagerindicator/R$dimen;->default_title_indicator_top_padding:I

    invoke-virtual {v5, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v19

    .line 181
    .local v19, "defaultTopPadding":F
    sget-object v0, Lcom/viewpagerindicator/R$styleable;->TitlePageIndicator:[I

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move/from16 v3, p3

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v0, v3, v4}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v20

    .line 184
    .local v20, "a":Landroid/content/res/TypedArray;
    move-object/from16 v0, v20

    const/4 v1, 0x6

    invoke-virtual {v0, v1, v7}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v0

    move-object/from16 v1, p0

    iput v0, v1, Lcom/viewpagerindicator/TitlePageIndicator;->mFooterLineHeight:F

    .line 185
    move-object/from16 v0, v20

    const/4 v1, 0x7

    invoke-virtual {v0, v1, v8}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v0

    invoke-static {v0}, Lcom/viewpagerindicator/TitlePageIndicator$IndicatorStyle;->fromValue(I)Lcom/viewpagerindicator/TitlePageIndicator$IndicatorStyle;

    move-result-object v0

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/viewpagerindicator/TitlePageIndicator;->mFooterIndicatorStyle:Lcom/viewpagerindicator/TitlePageIndicator$IndicatorStyle;

    .line 186
    move-object/from16 v0, v20

    const/16 v1, 0x8

    invoke-virtual {v0, v1, v9}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v0

    move-object/from16 v1, p0

    iput v0, v1, Lcom/viewpagerindicator/TitlePageIndicator;->mFooterIndicatorHeight:F

    .line 187
    move-object/from16 v0, v20

    const/16 v1, 0x9

    invoke-virtual {v0, v1, v10}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v0

    move-object/from16 v1, p0

    iput v0, v1, Lcom/viewpagerindicator/TitlePageIndicator;->mFooterIndicatorUnderlinePadding:F

    .line 188
    move-object/from16 v0, v20

    const/16 v1, 0xa

    invoke-virtual {v0, v1, v11}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v0

    move-object/from16 v1, p0

    iput v0, v1, Lcom/viewpagerindicator/TitlePageIndicator;->mFooterPadding:F

    .line 189
    move-object/from16 v0, v20

    const/16 v1, 0xb

    invoke-virtual {v0, v1, v12}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v0

    invoke-static {v0}, Lcom/viewpagerindicator/TitlePageIndicator$LinePosition;->fromValue(I)Lcom/viewpagerindicator/TitlePageIndicator$LinePosition;

    move-result-object v0

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/viewpagerindicator/TitlePageIndicator;->mLinePosition:Lcom/viewpagerindicator/TitlePageIndicator$LinePosition;

    .line 190
    move-object/from16 v0, v20

    const/16 v1, 0xe

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v0

    move-object/from16 v1, p0

    iput v0, v1, Lcom/viewpagerindicator/TitlePageIndicator;->mTopPadding:F

    .line 191
    move-object/from16 v0, v20

    const/16 v1, 0xd

    move/from16 v2, v17

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v0

    move-object/from16 v1, p0

    iput v0, v1, Lcom/viewpagerindicator/TitlePageIndicator;->mTitlePadding:F

    .line 192
    move-object/from16 v0, v20

    const/4 v1, 0x4

    move/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v0

    move-object/from16 v1, p0

    iput v0, v1, Lcom/viewpagerindicator/TitlePageIndicator;->mClipPadding:F

    .line 193
    move-object/from16 v0, v20

    const/4 v1, 0x3

    invoke-virtual {v0, v1, v13}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    move-object/from16 v1, p0

    iput v0, v1, Lcom/viewpagerindicator/TitlePageIndicator;->mColorSelected:I

    .line 194
    move-object/from16 v0, v20

    const/4 v1, 0x1

    invoke-virtual {v0, v1, v15}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    move-object/from16 v1, p0

    iput v0, v1, Lcom/viewpagerindicator/TitlePageIndicator;->mColorText:I

    .line 195
    move-object/from16 v0, v20

    const/16 v1, 0xc

    invoke-virtual {v0, v1, v14}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/viewpagerindicator/TitlePageIndicator;->mBoldText:Z

    .line 197
    move-object/from16 v0, v20

    const/4 v1, 0x0

    move/from16 v2, v16

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v21

    .line 198
    .local v21, "textSize":F
    move-object/from16 v0, v20

    const/4 v1, 0x5

    invoke-virtual {v0, v1, v6}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v22

    .line 199
    .local v22, "footerColor":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/viewpagerindicator/TitlePageIndicator;->mPaintText:Landroid/graphics/Paint;

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 200
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/viewpagerindicator/TitlePageIndicator;->mPaintText:Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 201
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/viewpagerindicator/TitlePageIndicator;->mPaintFooterLine:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL_AND_STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 202
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/viewpagerindicator/TitlePageIndicator;->mPaintFooterLine:Landroid/graphics/Paint;

    move-object/from16 v1, p0

    iget v1, v1, Lcom/viewpagerindicator/TitlePageIndicator;->mFooterLineHeight:F

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 203
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/viewpagerindicator/TitlePageIndicator;->mPaintFooterLine:Landroid/graphics/Paint;

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 204
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/viewpagerindicator/TitlePageIndicator;->mPaintFooterIndicator:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL_AND_STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 205
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/viewpagerindicator/TitlePageIndicator;->mPaintFooterIndicator:Landroid/graphics/Paint;

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 207
    move-object/from16 v0, v20

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v23

    .line 208
    .local v23, "background":Landroid/graphics/drawable/Drawable;
    if-eqz v23, :cond_1ad

    .line 209
    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/viewpagerindicator/TitlePageIndicator;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 212
    :cond_1ad
    invoke-virtual/range {v20 .. v20}, Landroid/content/res/TypedArray;->recycle()V

    .line 214
    invoke-static/range {p1 .. p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v24

    .line 215
    .local v24, "configuration":Landroid/view/ViewConfiguration;
    invoke-static/range {v24 .. v24}, Landroid/support/v4/view/ViewConfigurationCompat;->getScaledPagingTouchSlop(Landroid/view/ViewConfiguration;)I

    move-result v0

    move-object/from16 v1, p0

    iput v0, v1, Lcom/viewpagerindicator/TitlePageIndicator;->mTouchSlop:I

    .line 216
    return-void
.end method

.method private calcBounds(ILandroid/graphics/Paint;)Landroid/graphics/Rect;
    .registers 7
    .param p1, "index"    # I
    .param p2, "paint"    # Landroid/graphics/Paint;

    .line 700
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    .line 701
    .local v2, "bounds":Landroid/graphics/Rect;
    invoke-direct {p0, p1}, Lcom/viewpagerindicator/TitlePageIndicator;->getTitle(I)Ljava/lang/CharSequence;

    move-result-object v3

    .line 702
    .local v3, "title":Ljava/lang/CharSequence;
    invoke-interface {v3}, Ljava/lang/CharSequence;->length()I

    move-result v0

    const/4 v1, 0x0

    invoke-virtual {p2, v3, v1, v0}, Landroid/graphics/Paint;->measureText(Ljava/lang/CharSequence;II)F

    move-result v0

    float-to-int v0, v0

    iput v0, v2, Landroid/graphics/Rect;->right:I

    .line 703
    invoke-virtual {p2}, Landroid/graphics/Paint;->descent()F

    move-result v0

    invoke-virtual {p2}, Landroid/graphics/Paint;->ascent()F

    move-result v1

    sub-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, v2, Landroid/graphics/Rect;->bottom:I

    .line 704
    return-object v2
.end method

.method private calculateAllBounds(Landroid/graphics/Paint;)Ljava/util/ArrayList;
    .registers 13
    .param p1, "paint"    # Landroid/graphics/Paint;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Landroid/graphics/Paint;)Ljava/util/ArrayList<Landroid/graphics/Rect;>;"
        }
    .end annotation

    .line 672
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 674
    .local v3, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/graphics/Rect;>;"
    iget-object v0, p0, Lcom/viewpagerindicator/TitlePageIndicator;->mViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getAdapter()Landroid/support/v4/view/PagerAdapter;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/view/PagerAdapter;->getCount()I

    move-result v4

    .line 675
    .local v4, "count":I
    invoke-virtual {p0}, Lcom/viewpagerindicator/TitlePageIndicator;->getWidth()I

    move-result v5

    .line 676
    .local v5, "width":I
    div-int/lit8 v6, v5, 0x2

    .line 677
    .local v6, "halfWidth":I
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_16
    if-ge v7, v4, :cond_4c

    .line 678
    invoke-direct {p0, v7, p1}, Lcom/viewpagerindicator/TitlePageIndicator;->calcBounds(ILandroid/graphics/Paint;)Landroid/graphics/Rect;

    move-result-object v8

    .line 679
    .local v8, "bounds":Landroid/graphics/Rect;
    iget v0, v8, Landroid/graphics/Rect;->right:I

    iget v1, v8, Landroid/graphics/Rect;->left:I

    sub-int v9, v0, v1

    .line 680
    .local v9, "w":I
    iget v0, v8, Landroid/graphics/Rect;->bottom:I

    iget v1, v8, Landroid/graphics/Rect;->top:I

    sub-int v10, v0, v1

    .line 681
    .local v10, "h":I
    int-to-float v0, v6

    int-to-float v1, v9

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    sub-float/2addr v0, v1

    iget v1, p0, Lcom/viewpagerindicator/TitlePageIndicator;->mCurrentPage:I

    sub-int v1, v7, v1

    int-to-float v1, v1

    iget v2, p0, Lcom/viewpagerindicator/TitlePageIndicator;->mPageOffset:F

    sub-float/2addr v1, v2

    int-to-float v2, v5

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, v8, Landroid/graphics/Rect;->left:I

    .line 682
    iget v0, v8, Landroid/graphics/Rect;->left:I

    add-int/2addr v0, v9

    iput v0, v8, Landroid/graphics/Rect;->right:I

    .line 683
    const/4 v0, 0x0

    iput v0, v8, Landroid/graphics/Rect;->top:I

    .line 684
    iput v10, v8, Landroid/graphics/Rect;->bottom:I

    .line 685
    invoke-virtual {v3, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 677
    .end local v8    # "bounds":Landroid/graphics/Rect;
    .end local v9    # "w":I
    .end local v10    # "h":I
    add-int/lit8 v7, v7, 0x1

    goto :goto_16

    .line 688
    .end local v7    # "i":I
    :cond_4c
    return-object v3
.end method

.method private clipViewOnTheLeft(Landroid/graphics/Rect;FI)V
    .registers 6
    .param p1, "curViewBound"    # Landroid/graphics/Rect;
    .param p2, "curViewWidth"    # F
    .param p3, "left"    # I

    .line 661
    int-to-float v0, p3

    iget v1, p0, Lcom/viewpagerindicator/TitlePageIndicator;->mClipPadding:F

    add-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p1, Landroid/graphics/Rect;->left:I

    .line 662
    iget v0, p0, Lcom/viewpagerindicator/TitlePageIndicator;->mClipPadding:F

    add-float/2addr v0, p2

    float-to-int v0, v0

    iput v0, p1, Landroid/graphics/Rect;->right:I

    .line 663
    return-void
.end method

.method private clipViewOnTheRight(Landroid/graphics/Rect;FI)V
    .registers 6
    .param p1, "curViewBound"    # Landroid/graphics/Rect;
    .param p2, "curViewWidth"    # F
    .param p3, "right"    # I

    .line 648
    int-to-float v0, p3

    iget v1, p0, Lcom/viewpagerindicator/TitlePageIndicator;->mClipPadding:F

    sub-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p1, Landroid/graphics/Rect;->right:I

    .line 649
    iget v0, p1, Landroid/graphics/Rect;->right:I

    int-to-float v0, v0

    sub-float/2addr v0, p2

    float-to-int v0, v0

    iput v0, p1, Landroid/graphics/Rect;->left:I

    .line 650
    return-void
.end method

.method private getTitle(I)Ljava/lang/CharSequence;
    .registers 4
    .param p1, "i"    # I

    .line 864
    iget-object v0, p0, Lcom/viewpagerindicator/TitlePageIndicator;->mViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getAdapter()Landroid/support/v4/view/PagerAdapter;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/support/v4/view/PagerAdapter;->getPageTitle(I)Ljava/lang/CharSequence;

    move-result-object v1

    .line 865
    .local v1, "title":Ljava/lang/CharSequence;
    if-nez v1, :cond_e

    .line 866
    const-string v1, ""

    .line 868
    :cond_e
    return-object v1
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .registers 35
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .line 355
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-super {v0, v1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 357
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/viewpagerindicator/TitlePageIndicator;->mViewPager:Landroid/support/v4/view/ViewPager;

    if-nez v0, :cond_e

    .line 358
    return-void

    .line 360
    :cond_e
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/viewpagerindicator/TitlePageIndicator;->mViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getAdapter()Landroid/support/v4/view/PagerAdapter;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/view/PagerAdapter;->getCount()I

    move-result v7

    .line 361
    .local v7, "count":I
    if-nez v7, :cond_1d

    .line 362
    return-void

    .line 366
    :cond_1d
    move-object/from16 v0, p0

    iget v0, v0, Lcom/viewpagerindicator/TitlePageIndicator;->mCurrentPage:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_36

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/viewpagerindicator/TitlePageIndicator;->mViewPager:Landroid/support/v4/view/ViewPager;

    if-eqz v0, :cond_36

    .line 367
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/viewpagerindicator/TitlePageIndicator;->mViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v0

    move-object/from16 v1, p0

    iput v0, v1, Lcom/viewpagerindicator/TitlePageIndicator;->mCurrentPage:I

    .line 371
    :cond_36
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/viewpagerindicator/TitlePageIndicator;->mPaintText:Landroid/graphics/Paint;

    move-object/from16 v1, p0

    invoke-direct {v1, v0}, Lcom/viewpagerindicator/TitlePageIndicator;->calculateAllBounds(Landroid/graphics/Paint;)Ljava/util/ArrayList;

    move-result-object v8

    .line 372
    .local v8, "bounds":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/graphics/Rect;>;"
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v9

    .line 375
    .local v9, "boundsSize":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/viewpagerindicator/TitlePageIndicator;->mCurrentPage:I

    if-lt v0, v9, :cond_52

    .line 376
    add-int/lit8 v0, v9, -0x1

    move-object/from16 v1, p0

    invoke-virtual {v1, v0}, Lcom/viewpagerindicator/TitlePageIndicator;->setCurrentItem(I)V

    .line 377
    return-void

    .line 380
    :cond_52
    add-int/lit8 v10, v7, -0x1

    .line 381
    .local v10, "countMinusOne":I
    invoke-virtual/range {p0 .. p0}, Lcom/viewpagerindicator/TitlePageIndicator;->getWidth()I

    move-result v0

    int-to-float v0, v0

    const/high16 v1, 0x40000000    # 2.0f

    div-float v11, v0, v1

    .line 382
    .local v11, "halfWidth":F
    invoke-virtual/range {p0 .. p0}, Lcom/viewpagerindicator/TitlePageIndicator;->getLeft()I

    move-result v12

    .line 383
    .local v12, "left":I
    int-to-float v0, v12

    move-object/from16 v1, p0

    iget v1, v1, Lcom/viewpagerindicator/TitlePageIndicator;->mClipPadding:F

    add-float v13, v0, v1

    .line 384
    .local v13, "leftClip":F
    invoke-virtual/range {p0 .. p0}, Lcom/viewpagerindicator/TitlePageIndicator;->getWidth()I

    move-result v14

    .line 385
    .local v14, "width":I
    invoke-virtual/range {p0 .. p0}, Lcom/viewpagerindicator/TitlePageIndicator;->getHeight()I

    move-result v15

    .line 386
    .local v15, "height":I
    add-int v16, v12, v14

    .line 387
    .local v16, "right":I
    move/from16 v0, v16

    int-to-float v0, v0

    move-object/from16 v1, p0

    iget v1, v1, Lcom/viewpagerindicator/TitlePageIndicator;->mClipPadding:F

    sub-float v17, v0, v1

    .line 389
    .local v17, "rightClip":F
    move-object/from16 v0, p0

    iget v0, v0, Lcom/viewpagerindicator/TitlePageIndicator;->mCurrentPage:I

    move/from16 v18, v0

    .line 391
    .local v18, "page":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/viewpagerindicator/TitlePageIndicator;->mPageOffset:F

    float-to-double v0, v0

    const-wide/high16 v2, 0x3fe0000000000000L    # 0.5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_93

    .line 392
    move-object/from16 v0, p0

    iget v0, v0, Lcom/viewpagerindicator/TitlePageIndicator;->mPageOffset:F

    move/from16 v19, v0

    .local v19, "offsetPercent":F
    goto :goto_9d

    .line 394
    .end local v19    # "offsetPercent":F
    :cond_93
    add-int/lit8 v18, v18, 0x1

    .line 395
    move-object/from16 v0, p0

    iget v0, v0, Lcom/viewpagerindicator/TitlePageIndicator;->mPageOffset:F

    const/high16 v1, 0x3f800000    # 1.0f

    sub-float v19, v1, v0

    .line 397
    .local v19, "offsetPercent":F
    :goto_9d
    const/high16 v0, 0x3e800000    # 0.25f

    cmpg-float v0, v19, v0

    if-gtz v0, :cond_a6

    const/16 v20, 0x1

    goto :goto_a8

    :cond_a6
    const/16 v20, 0x0

    .line 398
    .local v20, "currentSelected":Z
    :goto_a8
    const v0, 0x3d4ccccd    # 0.05f

    cmpg-float v0, v19, v0

    if-gtz v0, :cond_b2

    const/16 v21, 0x1

    goto :goto_b4

    :cond_b2
    const/16 v21, 0x0

    .line 399
    .local v21, "currentBold":Z
    :goto_b4
    const/high16 v0, 0x3e800000    # 0.25f

    sub-float v0, v0, v19

    const/high16 v1, 0x3e800000    # 0.25f

    div-float v22, v0, v1

    .line 402
    .local v22, "selectedPercent":F
    move-object/from16 v0, p0

    iget v0, v0, Lcom/viewpagerindicator/TitlePageIndicator;->mCurrentPage:I

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object/from16 v23, v0

    check-cast v23, Landroid/graphics/Rect;

    .line 403
    .local v23, "curPageBound":Landroid/graphics/Rect;
    move-object/from16 v0, v23

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move-object/from16 v1, v23

    iget v1, v1, Landroid/graphics/Rect;->left:I

    sub-int/2addr v0, v1

    int-to-float v1, v0

    move/from16 v24, v1

    .line 404
    .local v24, "curPageWidth":F
    move-object/from16 v0, v23

    iget v0, v0, Landroid/graphics/Rect;->left:I

    int-to-float v0, v0

    cmpg-float v0, v0, v13

    if-gez v0, :cond_e6

    .line 406
    move-object/from16 v0, p0

    move-object/from16 v1, v23

    move/from16 v2, v24

    invoke-direct {v0, v1, v2, v12}, Lcom/viewpagerindicator/TitlePageIndicator;->clipViewOnTheLeft(Landroid/graphics/Rect;FI)V

    .line 408
    :cond_e6
    move-object/from16 v0, v23

    iget v0, v0, Landroid/graphics/Rect;->right:I

    int-to-float v0, v0

    cmpl-float v0, v0, v17

    if-lez v0, :cond_fa

    .line 410
    move-object/from16 v0, p0

    move-object/from16 v1, v23

    move/from16 v2, v24

    move/from16 v3, v16

    invoke-direct {v0, v1, v2, v3}, Lcom/viewpagerindicator/TitlePageIndicator;->clipViewOnTheRight(Landroid/graphics/Rect;FI)V

    .line 414
    :cond_fa
    move-object/from16 v0, p0

    iget v0, v0, Lcom/viewpagerindicator/TitlePageIndicator;->mCurrentPage:I

    if-lez v0, :cond_16a

    .line 415
    move-object/from16 v0, p0

    iget v0, v0, Lcom/viewpagerindicator/TitlePageIndicator;->mCurrentPage:I

    add-int/lit8 v25, v0, -0x1

    .local v25, "i":I
    :goto_106
    if-ltz v25, :cond_16a

    .line 416
    move/from16 v0, v25

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object/from16 v26, v0

    check-cast v26, Landroid/graphics/Rect;

    .line 418
    .local v26, "bound":Landroid/graphics/Rect;
    move-object/from16 v0, v26

    iget v0, v0, Landroid/graphics/Rect;->left:I

    int-to-float v0, v0

    cmpg-float v0, v0, v13

    if-gez v0, :cond_167

    .line 419
    move-object/from16 v0, v26

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move-object/from16 v1, v26

    iget v1, v1, Landroid/graphics/Rect;->left:I

    sub-int v27, v0, v1

    .line 421
    .local v27, "w":I
    move/from16 v0, v27

    int-to-float v0, v0

    move-object/from16 v1, p0

    move-object/from16 v2, v26

    invoke-direct {v1, v2, v0, v12}, Lcom/viewpagerindicator/TitlePageIndicator;->clipViewOnTheLeft(Landroid/graphics/Rect;FI)V

    .line 423
    add-int/lit8 v0, v25, 0x1

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object/from16 v28, v0

    check-cast v28, Landroid/graphics/Rect;

    .line 425
    .local v28, "rightBound":Landroid/graphics/Rect;
    move-object/from16 v0, v26

    iget v0, v0, Landroid/graphics/Rect;->right:I

    int-to-float v0, v0

    move-object/from16 v1, p0

    iget v1, v1, Lcom/viewpagerindicator/TitlePageIndicator;->mTitlePadding:F

    add-float/2addr v0, v1

    move-object/from16 v1, v28

    iget v1, v1, Landroid/graphics/Rect;->left:I

    int-to-float v1, v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_167

    .line 426
    move-object/from16 v0, v28

    iget v0, v0, Landroid/graphics/Rect;->left:I

    sub-int v0, v0, v27

    int-to-float v0, v0

    move-object/from16 v1, p0

    iget v1, v1, Lcom/viewpagerindicator/TitlePageIndicator;->mTitlePadding:F

    sub-float/2addr v0, v1

    float-to-int v0, v0

    move-object/from16 v1, v26

    iput v0, v1, Landroid/graphics/Rect;->left:I

    .line 427
    move-object/from16 v0, v26

    iget v0, v0, Landroid/graphics/Rect;->left:I

    add-int v0, v0, v27

    move-object/from16 v1, v26

    iput v0, v1, Landroid/graphics/Rect;->right:I

    .line 415
    .end local v26    # "bound":Landroid/graphics/Rect;
    .end local v27    # "w":I
    .end local v28    # "rightBound":Landroid/graphics/Rect;
    :cond_167
    add-int/lit8 v25, v25, -0x1

    goto :goto_106

    .line 433
    .end local v25    # "i":I
    :cond_16a
    move-object/from16 v0, p0

    iget v0, v0, Lcom/viewpagerindicator/TitlePageIndicator;->mCurrentPage:I

    if-ge v0, v10, :cond_1dc

    .line 434
    move-object/from16 v0, p0

    iget v0, v0, Lcom/viewpagerindicator/TitlePageIndicator;->mCurrentPage:I

    add-int/lit8 v25, v0, 0x1

    .local v25, "i":I
    :goto_176
    move/from16 v0, v25

    if-ge v0, v7, :cond_1dc

    .line 435
    move/from16 v0, v25

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object/from16 v26, v0

    check-cast v26, Landroid/graphics/Rect;

    .line 437
    .local v26, "bound":Landroid/graphics/Rect;
    move-object/from16 v0, v26

    iget v0, v0, Landroid/graphics/Rect;->right:I

    int-to-float v0, v0

    cmpl-float v0, v0, v17

    if-lez v0, :cond_1d9

    .line 438
    move-object/from16 v0, v26

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move-object/from16 v1, v26

    iget v1, v1, Landroid/graphics/Rect;->left:I

    sub-int v27, v0, v1

    .line 440
    .local v27, "w":I
    move/from16 v0, v27

    int-to-float v0, v0

    move-object/from16 v1, p0

    move-object/from16 v2, v26

    move/from16 v3, v16

    invoke-direct {v1, v2, v0, v3}, Lcom/viewpagerindicator/TitlePageIndicator;->clipViewOnTheRight(Landroid/graphics/Rect;FI)V

    .line 442
    add-int/lit8 v0, v25, -0x1

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object/from16 v28, v0

    check-cast v28, Landroid/graphics/Rect;

    .line 444
    .local v28, "leftBound":Landroid/graphics/Rect;
    move-object/from16 v0, v26

    iget v0, v0, Landroid/graphics/Rect;->left:I

    int-to-float v0, v0

    move-object/from16 v1, p0

    iget v1, v1, Lcom/viewpagerindicator/TitlePageIndicator;->mTitlePadding:F

    sub-float/2addr v0, v1

    move-object/from16 v1, v28

    iget v1, v1, Landroid/graphics/Rect;->right:I

    int-to-float v1, v1

    cmpg-float v0, v0, v1

    if-gez v0, :cond_1d9

    .line 445
    move-object/from16 v0, v28

    iget v0, v0, Landroid/graphics/Rect;->right:I

    int-to-float v0, v0

    move-object/from16 v1, p0

    iget v1, v1, Lcom/viewpagerindicator/TitlePageIndicator;->mTitlePadding:F

    add-float/2addr v0, v1

    float-to-int v0, v0

    move-object/from16 v1, v26

    iput v0, v1, Landroid/graphics/Rect;->left:I

    .line 446
    move-object/from16 v0, v26

    iget v0, v0, Landroid/graphics/Rect;->left:I

    add-int v0, v0, v27

    move-object/from16 v1, v26

    iput v0, v1, Landroid/graphics/Rect;->right:I

    .line 434
    .end local v26    # "bound":Landroid/graphics/Rect;
    .end local v27    # "w":I
    .end local v28    # "leftBound":Landroid/graphics/Rect;
    :cond_1d9
    add-int/lit8 v25, v25, 0x1

    goto :goto_176

    .line 453
    .end local v25    # "i":I
    :cond_1dc
    move-object/from16 v0, p0

    iget v0, v0, Lcom/viewpagerindicator/TitlePageIndicator;->mColorText:I

    ushr-int/lit8 v25, v0, 0x18

    .line 454
    .local v25, "colorTextAlpha":I
    const/16 v26, 0x0

    .local v26, "i":I
    :goto_1e4
    move/from16 v0, v26

    if-ge v0, v7, :cond_2fd

    .line 456
    move/from16 v0, v26

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object/from16 v27, v0

    check-cast v27, Landroid/graphics/Rect;

    .line 458
    .local v27, "bound":Landroid/graphics/Rect;
    move-object/from16 v0, v27

    iget v0, v0, Landroid/graphics/Rect;->left:I

    if-le v0, v12, :cond_200

    move-object/from16 v0, v27

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v1, v16

    if-lt v0, v1, :cond_20e

    :cond_200
    move-object/from16 v0, v27

    iget v0, v0, Landroid/graphics/Rect;->right:I

    if-le v0, v12, :cond_2f9

    move-object/from16 v0, v27

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move/from16 v1, v16

    if-ge v0, v1, :cond_2f9

    .line 459
    :cond_20e
    move/from16 v0, v26

    move/from16 v1, v18

    if-ne v0, v1, :cond_217

    const/16 v28, 0x1

    goto :goto_219

    :cond_217
    const/16 v28, 0x0

    .line 460
    .local v28, "currentPage":Z
    :goto_219
    move-object/from16 v0, p0

    move/from16 v1, v26

    invoke-direct {v0, v1}, Lcom/viewpagerindicator/TitlePageIndicator;->getTitle(I)Ljava/lang/CharSequence;

    move-result-object v29

    .line 463
    .local v29, "pageTitle":Ljava/lang/CharSequence;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/viewpagerindicator/TitlePageIndicator;->mPaintText:Landroid/graphics/Paint;

    if-eqz v28, :cond_231

    if-eqz v21, :cond_231

    move-object/from16 v1, p0

    iget-boolean v1, v1, Lcom/viewpagerindicator/TitlePageIndicator;->mBoldText:Z

    if-eqz v1, :cond_231

    const/4 v1, 0x1

    goto :goto_232

    :cond_231
    const/4 v1, 0x0

    :goto_232
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setFakeBoldText(Z)V

    .line 466
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/viewpagerindicator/TitlePageIndicator;->mPaintText:Landroid/graphics/Paint;

    move-object/from16 v1, p0

    iget v1, v1, Lcom/viewpagerindicator/TitlePageIndicator;->mColorText:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 467
    if-eqz v28, :cond_253

    if-eqz v20, :cond_253

    .line 469
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/viewpagerindicator/TitlePageIndicator;->mPaintText:Landroid/graphics/Paint;

    move/from16 v1, v25

    int-to-float v1, v1

    mul-float v1, v1, v22

    float-to-int v1, v1

    sub-int v1, v25, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 473
    :cond_253
    add-int/lit8 v0, v9, -0x1

    move/from16 v1, v26

    if-ge v1, v0, :cond_29b

    .line 474
    add-int/lit8 v0, v26, 0x1

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object/from16 v30, v0

    check-cast v30, Landroid/graphics/Rect;

    .line 476
    .local v30, "rightBound":Landroid/graphics/Rect;
    move-object/from16 v0, v27

    iget v0, v0, Landroid/graphics/Rect;->right:I

    int-to-float v0, v0

    move-object/from16 v1, p0

    iget v1, v1, Lcom/viewpagerindicator/TitlePageIndicator;->mTitlePadding:F

    add-float/2addr v0, v1

    move-object/from16 v1, v30

    iget v1, v1, Landroid/graphics/Rect;->left:I

    int-to-float v1, v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_29b

    .line 477
    move-object/from16 v0, v27

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move-object/from16 v1, v27

    iget v1, v1, Landroid/graphics/Rect;->left:I

    sub-int v31, v0, v1

    .line 478
    .local v31, "w":I
    move-object/from16 v0, v30

    iget v0, v0, Landroid/graphics/Rect;->left:I

    sub-int v0, v0, v31

    int-to-float v0, v0

    move-object/from16 v1, p0

    iget v1, v1, Lcom/viewpagerindicator/TitlePageIndicator;->mTitlePadding:F

    sub-float/2addr v0, v1

    float-to-int v0, v0

    move-object/from16 v1, v27

    iput v0, v1, Landroid/graphics/Rect;->left:I

    .line 479
    move-object/from16 v0, v27

    iget v0, v0, Landroid/graphics/Rect;->left:I

    add-int v0, v0, v31

    move-object/from16 v1, v27

    iput v0, v1, Landroid/graphics/Rect;->right:I

    .line 482
    .end local v30    # "rightBound":Landroid/graphics/Rect;
    .end local v31    # "w":I
    :cond_29b
    move-object/from16 v0, p1

    move-object/from16 v1, v29

    invoke-interface/range {v29 .. v29}, Ljava/lang/CharSequence;->length()I

    move-result v3

    move-object/from16 v2, v27

    iget v2, v2, Landroid/graphics/Rect;->left:I

    int-to-float v4, v2

    move-object/from16 v2, v27

    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    int-to-float v2, v2

    move-object/from16 v5, p0

    iget v5, v5, Lcom/viewpagerindicator/TitlePageIndicator;->mTopPadding:F

    add-float/2addr v5, v2

    move-object/from16 v2, p0

    iget-object v6, v2, Lcom/viewpagerindicator/TitlePageIndicator;->mPaintText:Landroid/graphics/Paint;

    const/4 v2, 0x0

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Canvas;->drawText(Ljava/lang/CharSequence;IIFFLandroid/graphics/Paint;)V

    .line 485
    if-eqz v28, :cond_2f9

    if-eqz v20, :cond_2f9

    .line 486
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/viewpagerindicator/TitlePageIndicator;->mPaintText:Landroid/graphics/Paint;

    move-object/from16 v1, p0

    iget v1, v1, Lcom/viewpagerindicator/TitlePageIndicator;->mColorSelected:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 487
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/viewpagerindicator/TitlePageIndicator;->mPaintText:Landroid/graphics/Paint;

    move-object/from16 v1, p0

    iget v1, v1, Lcom/viewpagerindicator/TitlePageIndicator;->mColorSelected:I

    ushr-int/lit8 v1, v1, 0x18

    int-to-float v1, v1

    mul-float v1, v1, v22

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 488
    move-object/from16 v0, p1

    move-object/from16 v1, v29

    invoke-interface/range {v29 .. v29}, Ljava/lang/CharSequence;->length()I

    move-result v3

    move-object/from16 v2, v27

    iget v2, v2, Landroid/graphics/Rect;->left:I

    int-to-float v4, v2

    move-object/from16 v2, v27

    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    int-to-float v2, v2

    move-object/from16 v5, p0

    iget v5, v5, Lcom/viewpagerindicator/TitlePageIndicator;->mTopPadding:F

    add-float/2addr v5, v2

    move-object/from16 v2, p0

    iget-object v6, v2, Lcom/viewpagerindicator/TitlePageIndicator;->mPaintText:Landroid/graphics/Paint;

    const/4 v2, 0x0

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Canvas;->drawText(Ljava/lang/CharSequence;IIFFLandroid/graphics/Paint;)V

    .line 454
    .end local v27    # "bound":Landroid/graphics/Rect;
    .end local v28    # "currentPage":Z
    .end local v29    # "pageTitle":Ljava/lang/CharSequence;
    :cond_2f9
    add-int/lit8 v26, v26, 0x1

    goto/16 :goto_1e4

    .line 494
    .end local v26    # "i":I
    :cond_2fd
    move-object/from16 v0, p0

    iget v0, v0, Lcom/viewpagerindicator/TitlePageIndicator;->mFooterLineHeight:F

    move/from16 v26, v0

    .line 495
    .local v26, "footerLineHeight":F
    move-object/from16 v0, p0

    iget v0, v0, Lcom/viewpagerindicator/TitlePageIndicator;->mFooterIndicatorHeight:F

    move/from16 v27, v0

    .line 496
    .local v27, "footerIndicatorLineHeight":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/viewpagerindicator/TitlePageIndicator;->mLinePosition:Lcom/viewpagerindicator/TitlePageIndicator$LinePosition;

    sget-object v1, Lcom/viewpagerindicator/TitlePageIndicator$LinePosition;->Top:Lcom/viewpagerindicator/TitlePageIndicator$LinePosition;

    if-ne v0, v1, :cond_31c

    .line 497
    const/4 v15, 0x0

    .line 498
    move/from16 v0, v26

    neg-float v0, v0

    move/from16 v26, v0

    .line 499
    move/from16 v0, v27

    neg-float v0, v0

    move/from16 v27, v0

    .line 503
    :cond_31c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/viewpagerindicator/TitlePageIndicator;->mPath:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    .line 504
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/viewpagerindicator/TitlePageIndicator;->mPath:Landroid/graphics/Path;

    int-to-float v1, v15

    const/high16 v2, 0x40000000    # 2.0f

    div-float v2, v26, v2

    sub-float/2addr v1, v2

    const/4 v2, 0x0

    invoke-virtual {v0, v2, v1}, Landroid/graphics/Path;->moveTo(FF)V

    .line 505
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/viewpagerindicator/TitlePageIndicator;->mPath:Landroid/graphics/Path;

    int-to-float v1, v14

    int-to-float v2, v15

    const/high16 v3, 0x40000000    # 2.0f

    div-float v3, v26, v3

    sub-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 506
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/viewpagerindicator/TitlePageIndicator;->mPath:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->close()V

    .line 507
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/viewpagerindicator/TitlePageIndicator;->mPath:Landroid/graphics/Path;

    move-object/from16 v1, p0

    iget-object v1, v1, Lcom/viewpagerindicator/TitlePageIndicator;->mPaintFooterLine:Landroid/graphics/Paint;

    move-object/from16 v2, p1

    invoke-virtual {v2, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 509
    int-to-float v0, v15

    sub-float v28, v0, v26

    .line 510
    .local v28, "heightMinusLine":F
    sget-object v0, Lcom/viewpagerindicator/TitlePageIndicator$1;->$SwitchMap$com$viewpagerindicator$TitlePageIndicator$IndicatorStyle:[I

    move-object/from16 v1, p0

    iget-object v1, v1, Lcom/viewpagerindicator/TitlePageIndicator;->mFooterIndicatorStyle:Lcom/viewpagerindicator/TitlePageIndicator$IndicatorStyle;

    invoke-virtual {v1}, Lcom/viewpagerindicator/TitlePageIndicator$IndicatorStyle;->ordinal()I

    move-result v1

    aget v0, v0, v1

    sparse-switch v0, :sswitch_data_42a

    goto/16 :goto_429

    .line 512
    :sswitch_367
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/viewpagerindicator/TitlePageIndicator;->mPath:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    .line 513
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/viewpagerindicator/TitlePageIndicator;->mPath:Landroid/graphics/Path;

    sub-float v1, v28, v27

    invoke-virtual {v0, v11, v1}, Landroid/graphics/Path;->moveTo(FF)V

    .line 514
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/viewpagerindicator/TitlePageIndicator;->mPath:Landroid/graphics/Path;

    add-float v1, v11, v27

    move/from16 v2, v28

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 515
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/viewpagerindicator/TitlePageIndicator;->mPath:Landroid/graphics/Path;

    sub-float v1, v11, v27

    move/from16 v2, v28

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 516
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/viewpagerindicator/TitlePageIndicator;->mPath:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->close()V

    .line 517
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/viewpagerindicator/TitlePageIndicator;->mPath:Landroid/graphics/Path;

    move-object/from16 v1, p0

    iget-object v1, v1, Lcom/viewpagerindicator/TitlePageIndicator;->mPaintFooterIndicator:Landroid/graphics/Paint;

    move-object/from16 v2, p1

    invoke-virtual {v2, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 518
    goto/16 :goto_429

    .line 521
    :sswitch_3a3
    if-eqz v20, :cond_429

    move/from16 v0, v18

    if-lt v0, v9, :cond_3ab

    .line 522
    goto/16 :goto_429

    .line 525
    :cond_3ab
    move/from16 v0, v18

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object/from16 v29, v0

    check-cast v29, Landroid/graphics/Rect;

    .line 526
    .local v29, "underlineBounds":Landroid/graphics/Rect;
    move-object/from16 v0, v29

    iget v0, v0, Landroid/graphics/Rect;->right:I

    int-to-float v0, v0

    move-object/from16 v1, p0

    iget v1, v1, Lcom/viewpagerindicator/TitlePageIndicator;->mFooterIndicatorUnderlinePadding:F

    add-float v30, v0, v1

    .line 527
    .local v30, "rightPlusPadding":F
    move-object/from16 v0, v29

    iget v0, v0, Landroid/graphics/Rect;->left:I

    int-to-float v0, v0

    move-object/from16 v1, p0

    iget v1, v1, Lcom/viewpagerindicator/TitlePageIndicator;->mFooterIndicatorUnderlinePadding:F

    sub-float v31, v0, v1

    .line 528
    .local v31, "leftMinusPadding":F
    sub-float v32, v28, v27

    .line 530
    .local v32, "heightMinusLineMinusIndicator":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/viewpagerindicator/TitlePageIndicator;->mPath:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    .line 531
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/viewpagerindicator/TitlePageIndicator;->mPath:Landroid/graphics/Path;

    move/from16 v1, v31

    move/from16 v2, v28

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 532
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/viewpagerindicator/TitlePageIndicator;->mPath:Landroid/graphics/Path;

    move/from16 v1, v30

    move/from16 v2, v28

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 533
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/viewpagerindicator/TitlePageIndicator;->mPath:Landroid/graphics/Path;

    move/from16 v1, v30

    move/from16 v2, v32

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 534
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/viewpagerindicator/TitlePageIndicator;->mPath:Landroid/graphics/Path;

    move/from16 v1, v31

    move/from16 v2, v32

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 535
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/viewpagerindicator/TitlePageIndicator;->mPath:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->close()V

    .line 537
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/viewpagerindicator/TitlePageIndicator;->mPaintFooterIndicator:Landroid/graphics/Paint;

    const/high16 v1, 0x437f0000    # 255.0f

    mul-float v1, v1, v22

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 538
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/viewpagerindicator/TitlePageIndicator;->mPath:Landroid/graphics/Path;

    move-object/from16 v1, p0

    iget-object v1, v1, Lcom/viewpagerindicator/TitlePageIndicator;->mPaintFooterIndicator:Landroid/graphics/Paint;

    move-object/from16 v2, p1

    invoke-virtual {v2, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 539
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/viewpagerindicator/TitlePageIndicator;->mPaintFooterIndicator:Landroid/graphics/Paint;

    const/16 v1, 0xff

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 542
    .end local v29    # "underlineBounds":Landroid/graphics/Rect;
    .end local v30    # "rightPlusPadding":F
    .end local v31    # "leftMinusPadding":F
    .end local v32    # "heightMinusLineMinusIndicator":F
    :cond_429
    :goto_429
    return-void

    :sswitch_data_42a
    .sparse-switch
        0x1 -> :sswitch_367
        0x2 -> :sswitch_3a3
    .end sparse-switch
.end method

.method protected onMeasure(II)V
    .registers 10
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .line 793
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v3

    .line 797
    .local v3, "measuredWidth":I
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v5

    .line 798
    .local v5, "heightSpecMode":I
    const/high16 v0, 0x40000000    # 2.0f

    if-ne v5, v0, :cond_12

    .line 800
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    int-to-float v4, v0

    .local v4, "height":F
    goto :goto_46

    .line 803
    .end local v4    # "height":F
    :cond_12
    iget-object v0, p0, Lcom/viewpagerindicator/TitlePageIndicator;->mBounds:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->setEmpty()V

    .line 804
    iget-object v0, p0, Lcom/viewpagerindicator/TitlePageIndicator;->mBounds:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/viewpagerindicator/TitlePageIndicator;->mPaintText:Landroid/graphics/Paint;

    invoke-virtual {v1}, Landroid/graphics/Paint;->descent()F

    move-result v1

    iget-object v2, p0, Lcom/viewpagerindicator/TitlePageIndicator;->mPaintText:Landroid/graphics/Paint;

    invoke-virtual {v2}, Landroid/graphics/Paint;->ascent()F

    move-result v2

    sub-float/2addr v1, v2

    float-to-int v1, v1

    iput v1, v0, Landroid/graphics/Rect;->bottom:I

    .line 805
    iget-object v0, p0, Lcom/viewpagerindicator/TitlePageIndicator;->mBounds:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    iget-object v1, p0, Lcom/viewpagerindicator/TitlePageIndicator;->mBounds:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    sub-int/2addr v0, v1

    int-to-float v0, v0

    iget v1, p0, Lcom/viewpagerindicator/TitlePageIndicator;->mFooterLineHeight:F

    add-float/2addr v0, v1

    iget v1, p0, Lcom/viewpagerindicator/TitlePageIndicator;->mFooterPadding:F

    add-float/2addr v0, v1

    iget v1, p0, Lcom/viewpagerindicator/TitlePageIndicator;->mTopPadding:F

    add-float v4, v0, v1

    .line 806
    .local v4, "height":F
    iget-object v0, p0, Lcom/viewpagerindicator/TitlePageIndicator;->mFooterIndicatorStyle:Lcom/viewpagerindicator/TitlePageIndicator$IndicatorStyle;

    sget-object v1, Lcom/viewpagerindicator/TitlePageIndicator$IndicatorStyle;->None:Lcom/viewpagerindicator/TitlePageIndicator$IndicatorStyle;

    if-eq v0, v1, :cond_46

    .line 807
    iget v0, p0, Lcom/viewpagerindicator/TitlePageIndicator;->mFooterIndicatorHeight:F

    add-float/2addr v4, v0

    .line 810
    :cond_46
    :goto_46
    float-to-int v6, v4

    .line 812
    .local v6, "measuredHeight":I
    invoke-virtual {p0, v3, v6}, Lcom/viewpagerindicator/TitlePageIndicator;->setMeasuredDimension(II)V

    .line 813
    return-void
.end method

.method public onPageScrollStateChanged(I)V
    .registers 3
    .param p1, "state"    # I

    .line 755
    iput p1, p0, Lcom/viewpagerindicator/TitlePageIndicator;->mScrollState:I

    .line 757
    iget-object v0, p0, Lcom/viewpagerindicator/TitlePageIndicator;->mListener:Landroid/support/v4/view/ViewPager$OnPageChangeListener;

    if-eqz v0, :cond_b

    .line 758
    iget-object v0, p0, Lcom/viewpagerindicator/TitlePageIndicator;->mListener:Landroid/support/v4/view/ViewPager$OnPageChangeListener;

    invoke-interface {v0, p1}, Landroid/support/v4/view/ViewPager$OnPageChangeListener;->onPageScrollStateChanged(I)V

    .line 760
    :cond_b
    return-void
.end method

.method public onPageScrolled(IFI)V
    .registers 5
    .param p1, "position"    # I
    .param p2, "positionOffset"    # F
    .param p3, "positionOffsetPixels"    # I

    .line 764
    iput p1, p0, Lcom/viewpagerindicator/TitlePageIndicator;->mCurrentPage:I

    .line 765
    iput p2, p0, Lcom/viewpagerindicator/TitlePageIndicator;->mPageOffset:F

    .line 766
    invoke-virtual {p0}, Lcom/viewpagerindicator/TitlePageIndicator;->invalidate()V

    .line 768
    iget-object v0, p0, Lcom/viewpagerindicator/TitlePageIndicator;->mListener:Landroid/support/v4/view/ViewPager$OnPageChangeListener;

    if-eqz v0, :cond_10

    .line 769
    iget-object v0, p0, Lcom/viewpagerindicator/TitlePageIndicator;->mListener:Landroid/support/v4/view/ViewPager$OnPageChangeListener;

    invoke-interface {v0, p1, p2, p3}, Landroid/support/v4/view/ViewPager$OnPageChangeListener;->onPageScrolled(IFI)V

    .line 771
    :cond_10
    return-void
.end method

.method public onPageSelected(I)V
    .registers 3
    .param p1, "position"    # I

    .line 775
    iget v0, p0, Lcom/viewpagerindicator/TitlePageIndicator;->mScrollState:I

    if-nez v0, :cond_9

    .line 776
    iput p1, p0, Lcom/viewpagerindicator/TitlePageIndicator;->mCurrentPage:I

    .line 777
    invoke-virtual {p0}, Lcom/viewpagerindicator/TitlePageIndicator;->invalidate()V

    .line 780
    :cond_9
    iget-object v0, p0, Lcom/viewpagerindicator/TitlePageIndicator;->mListener:Landroid/support/v4/view/ViewPager$OnPageChangeListener;

    if-eqz v0, :cond_12

    .line 781
    iget-object v0, p0, Lcom/viewpagerindicator/TitlePageIndicator;->mListener:Landroid/support/v4/view/ViewPager$OnPageChangeListener;

    invoke-interface {v0, p1}, Landroid/support/v4/view/ViewPager$OnPageChangeListener;->onPageSelected(I)V

    .line 783
    :cond_12
    return-void
.end method

.method public onRestoreInstanceState(Landroid/os/Parcelable;)V
    .registers 4
    .param p1, "state"    # Landroid/os/Parcelable;

    .line 817
    move-object v1, p1

    check-cast v1, Lcom/viewpagerindicator/TitlePageIndicator$SavedState;

    .line 818
    .local v1, "savedState":Lcom/viewpagerindicator/TitlePageIndicator$SavedState;
    invoke-virtual {v1}, Lcom/viewpagerindicator/TitlePageIndicator$SavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/view/View;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 819
    iget v0, v1, Lcom/viewpagerindicator/TitlePageIndicator$SavedState;->currentPage:I

    iput v0, p0, Lcom/viewpagerindicator/TitlePageIndicator;->mCurrentPage:I

    .line 820
    invoke-virtual {p0}, Lcom/viewpagerindicator/TitlePageIndicator;->requestLayout()V

    .line 821
    return-void
.end method

.method public onSaveInstanceState()Landroid/os/Parcelable;
    .registers 4

    .line 825
    invoke-super {p0}, Landroid/view/View;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v1

    .line 826
    .local v1, "superState":Landroid/os/Parcelable;
    new-instance v2, Lcom/viewpagerindicator/TitlePageIndicator$SavedState;

    invoke-direct {v2, v1}, Lcom/viewpagerindicator/TitlePageIndicator$SavedState;-><init>(Landroid/os/Parcelable;)V

    .line 827
    .local v2, "savedState":Lcom/viewpagerindicator/TitlePageIndicator$SavedState;
    iget v0, p0, Lcom/viewpagerindicator/TitlePageIndicator;->mCurrentPage:I

    iput v0, v2, Lcom/viewpagerindicator/TitlePageIndicator$SavedState;->currentPage:I

    .line 828
    return-object v2
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 13
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .line 545
    invoke-super {p0, p1}, Landroid/view/View;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 546
    const/4 v0, 0x1

    return v0

    .line 548
    :cond_8
    iget-object v0, p0, Lcom/viewpagerindicator/TitlePageIndicator;->mViewPager:Landroid/support/v4/view/ViewPager;

    if-eqz v0, :cond_18

    iget-object v0, p0, Lcom/viewpagerindicator/TitlePageIndicator;->mViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getAdapter()Landroid/support/v4/view/PagerAdapter;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/view/PagerAdapter;->getCount()I

    move-result v0

    if-nez v0, :cond_1a

    .line 549
    :cond_18
    const/4 v0, 0x0

    return v0

    .line 552
    :cond_1a
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    and-int/lit16 v3, v0, 0xff

    .line 553
    .local v3, "action":I
    packed-switch v3, :pswitch_data_11c

    goto/16 :goto_119

    .line 555
    :pswitch_25
    const/4 v0, 0x0

    invoke-static {p1, v0}, Landroid/support/v4/view/MotionEventCompat;->getPointerId(Landroid/view/MotionEvent;I)I

    move-result v0

    iput v0, p0, Lcom/viewpagerindicator/TitlePageIndicator;->mActivePointerId:I

    .line 556
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iput v0, p0, Lcom/viewpagerindicator/TitlePageIndicator;->mLastMotionX:F

    .line 557
    goto/16 :goto_119

    .line 560
    :pswitch_34
    iget v0, p0, Lcom/viewpagerindicator/TitlePageIndicator;->mActivePointerId:I

    invoke-static {p1, v0}, Landroid/support/v4/view/MotionEventCompat;->findPointerIndex(Landroid/view/MotionEvent;I)I

    move-result v4

    .line 561
    .local v4, "activePointerIndex":I
    invoke-static {p1, v4}, Landroid/support/v4/view/MotionEventCompat;->getX(Landroid/view/MotionEvent;I)F

    move-result v5

    .line 562
    .local v5, "x":F
    iget v0, p0, Lcom/viewpagerindicator/TitlePageIndicator;->mLastMotionX:F

    sub-float v6, v5, v0

    .line 564
    .local v6, "deltaX":F
    iget-boolean v0, p0, Lcom/viewpagerindicator/TitlePageIndicator;->mIsDragging:Z

    if-nez v0, :cond_54

    .line 565
    invoke-static {v6}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iget v1, p0, Lcom/viewpagerindicator/TitlePageIndicator;->mTouchSlop:I

    int-to-float v1, v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_54

    .line 566
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/viewpagerindicator/TitlePageIndicator;->mIsDragging:Z

    .line 570
    :cond_54
    iget-boolean v0, p0, Lcom/viewpagerindicator/TitlePageIndicator;->mIsDragging:Z

    if-eqz v0, :cond_119

    .line 571
    iput v5, p0, Lcom/viewpagerindicator/TitlePageIndicator;->mLastMotionX:F

    .line 572
    iget-object v0, p0, Lcom/viewpagerindicator/TitlePageIndicator;->mViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->isFakeDragging()Z

    move-result v0

    if-nez v0, :cond_6a

    iget-object v0, p0, Lcom/viewpagerindicator/TitlePageIndicator;->mViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->beginFakeDrag()Z

    move-result v0

    if-eqz v0, :cond_119

    .line 573
    :cond_6a
    iget-object v0, p0, Lcom/viewpagerindicator/TitlePageIndicator;->mViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, v6}, Landroid/support/v4/view/ViewPager;->fakeDragBy(F)V

    goto/16 :goto_119

    .line 582
    .end local v4    # "activePointerIndex":I
    .end local v5    # "x":F
    .end local v6    # "deltaX":F
    :pswitch_71
    iget-boolean v0, p0, Lcom/viewpagerindicator/TitlePageIndicator;->mIsDragging:Z

    if-nez v0, :cond_d1

    .line 583
    iget-object v0, p0, Lcom/viewpagerindicator/TitlePageIndicator;->mViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getAdapter()Landroid/support/v4/view/PagerAdapter;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/view/PagerAdapter;->getCount()I

    move-result v4

    .line 584
    .local v4, "count":I
    invoke-virtual {p0}, Lcom/viewpagerindicator/TitlePageIndicator;->getWidth()I

    move-result v5

    .line 585
    .local v5, "width":I
    int-to-float v0, v5

    const/high16 v1, 0x40000000    # 2.0f

    div-float v6, v0, v1

    .line 586
    .local v6, "halfWidth":F
    int-to-float v0, v5

    const/high16 v1, 0x40c00000    # 6.0f

    div-float v7, v0, v1

    .line 587
    .local v7, "sixthWidth":F
    sub-float v8, v6, v7

    .line 588
    .local v8, "leftThird":F
    add-float v9, v6, v7

    .line 589
    .local v9, "rightThird":F
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v10

    .line 591
    .local v10, "eventX":F
    cmpg-float v0, v10, v8

    if-gez v0, :cond_ab

    .line 592
    iget v0, p0, Lcom/viewpagerindicator/TitlePageIndicator;->mCurrentPage:I

    if-lez v0, :cond_d1

    .line 593
    const/4 v0, 0x3

    if-eq v3, v0, :cond_a9

    .line 594
    iget-object v0, p0, Lcom/viewpagerindicator/TitlePageIndicator;->mViewPager:Landroid/support/v4/view/ViewPager;

    iget v1, p0, Lcom/viewpagerindicator/TitlePageIndicator;->mCurrentPage:I

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 596
    :cond_a9
    const/4 v0, 0x1

    return v0

    .line 598
    :cond_ab
    cmpl-float v0, v10, v9

    if-lez v0, :cond_c3

    .line 599
    iget v0, p0, Lcom/viewpagerindicator/TitlePageIndicator;->mCurrentPage:I

    add-int/lit8 v1, v4, -0x1

    if-ge v0, v1, :cond_d1

    .line 600
    const/4 v0, 0x3

    if-eq v3, v0, :cond_c1

    .line 601
    iget-object v0, p0, Lcom/viewpagerindicator/TitlePageIndicator;->mViewPager:Landroid/support/v4/view/ViewPager;

    iget v1, p0, Lcom/viewpagerindicator/TitlePageIndicator;->mCurrentPage:I

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 603
    :cond_c1
    const/4 v0, 0x1

    return v0

    .line 607
    :cond_c3
    iget-object v0, p0, Lcom/viewpagerindicator/TitlePageIndicator;->mCenterItemClickListener:Lcom/viewpagerindicator/TitlePageIndicator$OnCenterItemClickListener;

    if-eqz v0, :cond_d1

    const/4 v0, 0x3

    if-eq v3, v0, :cond_d1

    .line 608
    iget-object v0, p0, Lcom/viewpagerindicator/TitlePageIndicator;->mCenterItemClickListener:Lcom/viewpagerindicator/TitlePageIndicator$OnCenterItemClickListener;

    iget v1, p0, Lcom/viewpagerindicator/TitlePageIndicator;->mCurrentPage:I

    invoke-interface {v0, v1}, Lcom/viewpagerindicator/TitlePageIndicator$OnCenterItemClickListener;->onCenterItemClick(I)V

    .line 613
    .end local v4    # "count":I
    .end local v5    # "width":I
    .end local v6    # "halfWidth":F
    .end local v7    # "sixthWidth":F
    .end local v8    # "leftThird":F
    .end local v9    # "rightThird":F
    .end local v10    # "eventX":F
    :cond_d1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/viewpagerindicator/TitlePageIndicator;->mIsDragging:Z

    .line 614
    const/4 v0, -0x1

    iput v0, p0, Lcom/viewpagerindicator/TitlePageIndicator;->mActivePointerId:I

    .line 615
    iget-object v0, p0, Lcom/viewpagerindicator/TitlePageIndicator;->mViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->isFakeDragging()Z

    move-result v0

    if-eqz v0, :cond_119

    iget-object v0, p0, Lcom/viewpagerindicator/TitlePageIndicator;->mViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->endFakeDrag()V

    goto :goto_119

    .line 619
    :pswitch_e5
    invoke-static {p1}, Landroid/support/v4/view/MotionEventCompat;->getActionIndex(Landroid/view/MotionEvent;)I

    move-result v4

    .line 620
    .local v4, "index":I
    invoke-static {p1, v4}, Landroid/support/v4/view/MotionEventCompat;->getX(Landroid/view/MotionEvent;I)F

    move-result v0

    iput v0, p0, Lcom/viewpagerindicator/TitlePageIndicator;->mLastMotionX:F

    .line 621
    invoke-static {p1, v4}, Landroid/support/v4/view/MotionEventCompat;->getPointerId(Landroid/view/MotionEvent;I)I

    move-result v0

    iput v0, p0, Lcom/viewpagerindicator/TitlePageIndicator;->mActivePointerId:I

    .line 622
    goto :goto_119

    .line 626
    .end local v4    # "index":I
    :pswitch_f6
    invoke-static {p1}, Landroid/support/v4/view/MotionEventCompat;->getActionIndex(Landroid/view/MotionEvent;)I

    move-result v4

    .line 627
    .local v4, "pointerIndex":I
    invoke-static {p1, v4}, Landroid/support/v4/view/MotionEventCompat;->getPointerId(Landroid/view/MotionEvent;I)I

    move-result v5

    .line 628
    .local v5, "pointerId":I
    iget v0, p0, Lcom/viewpagerindicator/TitlePageIndicator;->mActivePointerId:I

    if-ne v5, v0, :cond_10d

    .line 629
    if-nez v4, :cond_106

    const/4 v6, 0x1

    goto :goto_107

    :cond_106
    const/4 v6, 0x0

    .line 630
    .local v6, "newPointerIndex":I
    :goto_107
    invoke-static {p1, v6}, Landroid/support/v4/view/MotionEventCompat;->getPointerId(Landroid/view/MotionEvent;I)I

    move-result v0

    iput v0, p0, Lcom/viewpagerindicator/TitlePageIndicator;->mActivePointerId:I

    .line 632
    .end local v6    # "newPointerIndex":I
    :cond_10d
    iget v0, p0, Lcom/viewpagerindicator/TitlePageIndicator;->mActivePointerId:I

    invoke-static {p1, v0}, Landroid/support/v4/view/MotionEventCompat;->findPointerIndex(Landroid/view/MotionEvent;I)I

    move-result v0

    invoke-static {p1, v0}, Landroid/support/v4/view/MotionEventCompat;->getX(Landroid/view/MotionEvent;I)F

    move-result v0

    iput v0, p0, Lcom/viewpagerindicator/TitlePageIndicator;->mLastMotionX:F

    .line 636
    .end local v4    # "pointerIndex":I
    .end local v5    # "pointerId":I
    :cond_119
    :goto_119
    :pswitch_119
    const/4 v0, 0x1

    return v0

    nop

    :pswitch_data_11c
    .packed-switch 0x0
        :pswitch_25
        :pswitch_71
        :pswitch_34
        :pswitch_71
        :pswitch_119
        :pswitch_e5
        :pswitch_f6
    .end packed-switch
.end method

.method public setClipPadding(F)V
    .registers 2
    .param p1, "clipPadding"    # F

    .line 335
    iput p1, p0, Lcom/viewpagerindicator/TitlePageIndicator;->mClipPadding:F

    .line 336
    invoke-virtual {p0}, Lcom/viewpagerindicator/TitlePageIndicator;->invalidate()V

    .line 337
    return-void
.end method

.method public setCurrentItem(I)V
    .registers 4
    .param p1, "item"    # I

    .line 745
    iget-object v0, p0, Lcom/viewpagerindicator/TitlePageIndicator;->mViewPager:Landroid/support/v4/view/ViewPager;

    if-nez v0, :cond_c

    .line 746
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "ViewPager has not been bound."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 748
    :cond_c
    iget-object v0, p0, Lcom/viewpagerindicator/TitlePageIndicator;->mViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, p1}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 749
    iput p1, p0, Lcom/viewpagerindicator/TitlePageIndicator;->mCurrentPage:I

    .line 750
    invoke-virtual {p0}, Lcom/viewpagerindicator/TitlePageIndicator;->invalidate()V

    .line 751
    return-void
.end method

.method public setFooterColor(I)V
    .registers 3
    .param p1, "footerColor"    # I

    .line 224
    iget-object v0, p0, Lcom/viewpagerindicator/TitlePageIndicator;->mPaintFooterLine:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 225
    iget-object v0, p0, Lcom/viewpagerindicator/TitlePageIndicator;->mPaintFooterIndicator:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 226
    invoke-virtual {p0}, Lcom/viewpagerindicator/TitlePageIndicator;->invalidate()V

    .line 227
    return-void
.end method

.method public setFooterIndicatorHeight(F)V
    .registers 2
    .param p1, "footerTriangleHeight"    # F

    .line 244
    iput p1, p0, Lcom/viewpagerindicator/TitlePageIndicator;->mFooterIndicatorHeight:F

    .line 245
    invoke-virtual {p0}, Lcom/viewpagerindicator/TitlePageIndicator;->invalidate()V

    .line 246
    return-void
.end method

.method public setFooterIndicatorPadding(F)V
    .registers 2
    .param p1, "footerIndicatorPadding"    # F

    .line 253
    iput p1, p0, Lcom/viewpagerindicator/TitlePageIndicator;->mFooterPadding:F

    .line 254
    invoke-virtual {p0}, Lcom/viewpagerindicator/TitlePageIndicator;->invalidate()V

    .line 255
    return-void
.end method

.method public setFooterIndicatorStyle(Lcom/viewpagerindicator/TitlePageIndicator$IndicatorStyle;)V
    .registers 2
    .param p1, "indicatorStyle"    # Lcom/viewpagerindicator/TitlePageIndicator$IndicatorStyle;

    .line 262
    iput-object p1, p0, Lcom/viewpagerindicator/TitlePageIndicator;->mFooterIndicatorStyle:Lcom/viewpagerindicator/TitlePageIndicator$IndicatorStyle;

    .line 263
    invoke-virtual {p0}, Lcom/viewpagerindicator/TitlePageIndicator;->invalidate()V

    .line 264
    return-void
.end method

.method public setFooterLineHeight(F)V
    .registers 4
    .param p1, "footerLineHeight"    # F

    .line 234
    iput p1, p0, Lcom/viewpagerindicator/TitlePageIndicator;->mFooterLineHeight:F

    .line 235
    iget-object v0, p0, Lcom/viewpagerindicator/TitlePageIndicator;->mPaintFooterLine:Landroid/graphics/Paint;

    iget v1, p0, Lcom/viewpagerindicator/TitlePageIndicator;->mFooterLineHeight:F

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 236
    invoke-virtual {p0}, Lcom/viewpagerindicator/TitlePageIndicator;->invalidate()V

    .line 237
    return-void
.end method

.method public setLinePosition(Lcom/viewpagerindicator/TitlePageIndicator$LinePosition;)V
    .registers 2
    .param p1, "linePosition"    # Lcom/viewpagerindicator/TitlePageIndicator$LinePosition;

    .line 271
    iput-object p1, p0, Lcom/viewpagerindicator/TitlePageIndicator;->mLinePosition:Lcom/viewpagerindicator/TitlePageIndicator$LinePosition;

    .line 272
    invoke-virtual {p0}, Lcom/viewpagerindicator/TitlePageIndicator;->invalidate()V

    .line 273
    return-void
.end method

.method public setOnCenterItemClickListener(Lcom/viewpagerindicator/TitlePageIndicator$OnCenterItemClickListener;)V
    .registers 2
    .param p1, "listener"    # Lcom/viewpagerindicator/TitlePageIndicator$OnCenterItemClickListener;

    .line 740
    iput-object p1, p0, Lcom/viewpagerindicator/TitlePageIndicator;->mCenterItemClickListener:Lcom/viewpagerindicator/TitlePageIndicator$OnCenterItemClickListener;

    .line 741
    return-void
.end method

.method public setOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V
    .registers 2
    .param p1, "listener"    # Landroid/support/v4/view/ViewPager$OnPageChangeListener;

    .line 787
    iput-object p1, p0, Lcom/viewpagerindicator/TitlePageIndicator;->mListener:Landroid/support/v4/view/ViewPager$OnPageChangeListener;

    .line 788
    return-void
.end method

.method public setSelectedBold(Z)V
    .registers 2
    .param p1, "selectedBold"    # Z

    .line 289
    iput-boolean p1, p0, Lcom/viewpagerindicator/TitlePageIndicator;->mBoldText:Z

    .line 290
    invoke-virtual {p0}, Lcom/viewpagerindicator/TitlePageIndicator;->invalidate()V

    .line 291
    return-void
.end method

.method public setSelectedColor(I)V
    .registers 2
    .param p1, "selectedColor"    # I

    .line 280
    iput p1, p0, Lcom/viewpagerindicator/TitlePageIndicator;->mColorSelected:I

    .line 281
    invoke-virtual {p0}, Lcom/viewpagerindicator/TitlePageIndicator;->invalidate()V

    .line 282
    return-void
.end method

.method public setTextColor(I)V
    .registers 3
    .param p1, "textColor"    # I

    .line 298
    iget-object v0, p0, Lcom/viewpagerindicator/TitlePageIndicator;->mPaintText:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 299
    iput p1, p0, Lcom/viewpagerindicator/TitlePageIndicator;->mColorText:I

    .line 300
    invoke-virtual {p0}, Lcom/viewpagerindicator/TitlePageIndicator;->invalidate()V

    .line 301
    return-void
.end method

.method public setTextSize(F)V
    .registers 3
    .param p1, "textSize"    # F

    .line 308
    iget-object v0, p0, Lcom/viewpagerindicator/TitlePageIndicator;->mPaintText:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 309
    invoke-virtual {p0}, Lcom/viewpagerindicator/TitlePageIndicator;->invalidate()V

    .line 310
    return-void
.end method

.method public setTitlePadding(F)V
    .registers 2
    .param p1, "titlePadding"    # F

    .line 317
    iput p1, p0, Lcom/viewpagerindicator/TitlePageIndicator;->mTitlePadding:F

    .line 318
    invoke-virtual {p0}, Lcom/viewpagerindicator/TitlePageIndicator;->invalidate()V

    .line 319
    return-void
.end method

.method public setTopPadding(F)V
    .registers 2
    .param p1, "topPadding"    # F

    .line 326
    iput p1, p0, Lcom/viewpagerindicator/TitlePageIndicator;->mTopPadding:F

    .line 327
    invoke-virtual {p0}, Lcom/viewpagerindicator/TitlePageIndicator;->invalidate()V

    .line 328
    return-void
.end method

.method public setTypeface(Landroid/graphics/Typeface;)V
    .registers 3
    .param p1, "typeface"    # Landroid/graphics/Typeface;

    .line 340
    iget-object v0, p0, Lcom/viewpagerindicator/TitlePageIndicator;->mPaintText:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 341
    invoke-virtual {p0}, Lcom/viewpagerindicator/TitlePageIndicator;->invalidate()V

    .line 342
    return-void
.end method

.method public setViewPager(Landroid/support/v4/view/ViewPager;)V
    .registers 4
    .param p1, "view"    # Landroid/support/v4/view/ViewPager;

    .line 709
    iget-object v0, p0, Lcom/viewpagerindicator/TitlePageIndicator;->mViewPager:Landroid/support/v4/view/ViewPager;

    if-ne v0, p1, :cond_5

    .line 710
    return-void

    .line 712
    :cond_5
    iget-object v0, p0, Lcom/viewpagerindicator/TitlePageIndicator;->mViewPager:Landroid/support/v4/view/ViewPager;

    if-eqz v0, :cond_f

    .line 713
    iget-object v0, p0, Lcom/viewpagerindicator/TitlePageIndicator;->mViewPager:Landroid/support/v4/view/ViewPager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    .line 715
    :cond_f
    invoke-virtual {p1}, Landroid/support/v4/view/ViewPager;->getAdapter()Landroid/support/v4/view/PagerAdapter;

    move-result-object v0

    if-nez v0, :cond_1d

    .line 716
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "ViewPager does not have adapter instance."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 718
    :cond_1d
    iput-object p1, p0, Lcom/viewpagerindicator/TitlePageIndicator;->mViewPager:Landroid/support/v4/view/ViewPager;

    .line 719
    iget-object v0, p0, Lcom/viewpagerindicator/TitlePageIndicator;->mViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, p0}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    .line 720
    invoke-virtual {p0}, Lcom/viewpagerindicator/TitlePageIndicator;->invalidate()V

    .line 721
    return-void
.end method

.method public setViewPager(Landroid/support/v4/view/ViewPager;I)V
    .registers 3
    .param p1, "view"    # Landroid/support/v4/view/ViewPager;
    .param p2, "initialPosition"    # I

    .line 725
    invoke-virtual {p0, p1}, Lcom/viewpagerindicator/TitlePageIndicator;->setViewPager(Landroid/support/v4/view/ViewPager;)V

    .line 726
    invoke-virtual {p0, p2}, Lcom/viewpagerindicator/TitlePageIndicator;->setCurrentItem(I)V

    .line 727
    return-void
.end method

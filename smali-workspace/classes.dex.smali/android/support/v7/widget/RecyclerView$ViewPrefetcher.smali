.class Landroid/support/v7/widget/RecyclerView$ViewPrefetcher;
.super Ljava/lang/Object;
.source "RecyclerView.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/support/v7/widget/RecyclerView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ViewPrefetcher"
.end annotation


# instance fields
.field private mDx:I

.field private mDy:I

.field mItemPrefetchArray:[I

.field mPostTimeNanos:J

.field final synthetic this$0:Landroid/support/v7/widget/RecyclerView;


# direct methods
.method constructor <init>(Landroid/support/v7/widget/RecyclerView;)V
    .registers 2
    .param p1, "this$0"    # Landroid/support/v7/widget/RecyclerView;

    .line 4448
    iput-object p1, p0, Landroid/support/v7/widget/RecyclerView$ViewPrefetcher;->this$0:Landroid/support/v7/widget/RecyclerView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public clearPrefetchPositions()V
    .registers 3

    .line 4484
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView$ViewPrefetcher;->mItemPrefetchArray:[I

    if-eqz v0, :cond_a

    .line 4485
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView$ViewPrefetcher;->mItemPrefetchArray:[I

    const/4 v1, -0x1

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([II)V

    .line 4487
    :cond_a
    return-void
.end method

.method public lastPrefetchIncludedPosition(I)Z
    .registers 4
    .param p1, "position"    # I

    .line 4472
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView$ViewPrefetcher;->mItemPrefetchArray:[I

    if-eqz v0, :cond_15

    .line 4473
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_5
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView$ViewPrefetcher;->mItemPrefetchArray:[I

    array-length v0, v0

    if-ge v1, v0, :cond_15

    .line 4474
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView$ViewPrefetcher;->mItemPrefetchArray:[I

    aget v0, v0, v1

    if-ne v0, p1, :cond_12

    const/4 v0, 0x1

    return v0

    .line 4473
    :cond_12
    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    .line 4477
    .end local v1    # "i":I
    :cond_15
    const/4 v0, 0x0

    return v0
.end method

.method public postFromTraversal(II)V
    .registers 5
    .param p1, "dx"    # I
    .param p2, "dy"    # I

    .line 4460
    # getter for: Landroid/support/v7/widget/RecyclerView;->ALLOW_PREFETCHING:Z
    invoke-static {}, Landroid/support/v7/widget/RecyclerView;->access$400()Z

    move-result v0

    if-eqz v0, :cond_2b

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView$ViewPrefetcher;->this$0:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->mAdapter:Landroid/support/v7/widget/RecyclerView$Adapter;

    if-eqz v0, :cond_2b

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView$ViewPrefetcher;->this$0:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->mLayout:Landroid/support/v7/widget/RecyclerView$LayoutManager;

    if-eqz v0, :cond_2b

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView$ViewPrefetcher;->this$0:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->mLayout:Landroid/support/v7/widget/RecyclerView$LayoutManager;

    .line 4463
    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView$LayoutManager;->getItemPrefetchCount()I

    move-result v0

    if-lez v0, :cond_2b

    .line 4464
    iput p1, p0, Landroid/support/v7/widget/RecyclerView$ViewPrefetcher;->mDx:I

    .line 4465
    iput p2, p0, Landroid/support/v7/widget/RecyclerView$ViewPrefetcher;->mDy:I

    .line 4466
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    iput-wide v0, p0, Landroid/support/v7/widget/RecyclerView$ViewPrefetcher;->mPostTimeNanos:J

    .line 4467
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView$ViewPrefetcher;->this$0:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, p0}, Landroid/support/v7/widget/RecyclerView;->post(Ljava/lang/Runnable;)Z

    .line 4469
    :cond_2b
    return-void
.end method

.method public run()V
    .registers 15

    .line 4492
    const-string v0, "RV Prefetch"

    :try_start_2
    invoke-static {v0}, Landroid/support/v4/os/TraceCompat;->beginSection(Ljava/lang/String;)V

    .line 4493
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView$ViewPrefetcher;->this$0:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->mLayout:Landroid/support/v7/widget/RecyclerView$LayoutManager;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView$LayoutManager;->getItemPrefetchCount()I

    move-result v5

    .line 4494
    .local v5, "prefetchCount":I
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView$ViewPrefetcher;->this$0:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->mAdapter:Landroid/support/v7/widget/RecyclerView$Adapter;

    if-eqz v0, :cond_2e

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView$ViewPrefetcher;->this$0:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->mLayout:Landroid/support/v7/widget/RecyclerView$LayoutManager;

    if-eqz v0, :cond_2e

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView$ViewPrefetcher;->this$0:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->mLayout:Landroid/support/v7/widget/RecyclerView$LayoutManager;

    .line 4496
    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView$LayoutManager;->isItemPrefetchEnabled()Z

    move-result v0

    if-eqz v0, :cond_2e

    const/4 v0, 0x1

    if-lt v5, v0, :cond_2e

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView$ViewPrefetcher;->this$0:Landroid/support/v7/widget/RecyclerView;

    .line 4498
    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->hasPendingAdapterUpdates()Z
    :try_end_2b
    .catchall {:try_start_2 .. :try_end_2b} :catchall_a2

    move-result v0

    if-eqz v0, :cond_32

    .line 4526
    :cond_2e
    invoke-static {}, Landroid/support/v4/os/TraceCompat;->endSection()V

    .line 4500
    return-void

    .line 4505
    :cond_32
    :try_start_32
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView$ViewPrefetcher;->this$0:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1}, Landroid/support/v7/widget/RecyclerView;->getDrawingTime()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/TimeUnit;->toNanos(J)J

    move-result-wide v6

    .line 4506
    .local v6, "lastFrameVsyncNanos":J
    const-wide/16 v0, 0x0

    cmp-long v0, v6, v0

    if-eqz v0, :cond_4c

    sget-wide v0, Landroid/support/v7/widget/RecyclerView;->sFrameIntervalNanos:J
    :try_end_46
    .catchall {:try_start_32 .. :try_end_46} :catchall_a2

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_50

    .line 4526
    :cond_4c
    invoke-static {}, Landroid/support/v4/os/TraceCompat;->endSection()V

    .line 4508
    return-void

    .line 4511
    :cond_50
    :try_start_50
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v8

    .line 4512
    .local v8, "nowNanos":J
    sget-wide v0, Landroid/support/v7/widget/RecyclerView;->sFrameIntervalNanos:J

    add-long v10, v6, v0

    .line 4513
    .local v10, "nextFrameNanos":J
    iget-wide v0, p0, Landroid/support/v7/widget/RecyclerView$ViewPrefetcher;->mPostTimeNanos:J

    sub-long v0, v8, v0

    sget-wide v2, Landroid/support/v7/widget/RecyclerView;->sFrameIntervalNanos:J

    cmp-long v0, v0, v2

    if-gtz v0, :cond_6c

    sub-long v0, v10, v8

    .line 4514
    # getter for: Landroid/support/v7/widget/RecyclerView;->MIN_PREFETCH_TIME_NANOS:J
    invoke-static {}, Landroid/support/v7/widget/RecyclerView;->access$500()J
    :try_end_67
    .catchall {:try_start_50 .. :try_end_67} :catchall_a2

    move-result-wide v2

    cmp-long v0, v0, v2

    if-gez v0, :cond_70

    .line 4526
    :cond_6c
    invoke-static {}, Landroid/support/v4/os/TraceCompat;->endSection()V

    .line 4516
    return-void

    .line 4519
    :cond_70
    :try_start_70
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView$ViewPrefetcher;->mItemPrefetchArray:[I

    if-eqz v0, :cond_79

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView$ViewPrefetcher;->mItemPrefetchArray:[I

    array-length v0, v0

    if-ge v0, v5, :cond_7d

    .line 4520
    :cond_79
    new-array v0, v5, [I

    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView$ViewPrefetcher;->mItemPrefetchArray:[I

    .line 4522
    :cond_7d
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView$ViewPrefetcher;->mItemPrefetchArray:[I

    const/4 v1, -0x1

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([II)V

    .line 4523
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView$ViewPrefetcher;->this$0:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->mLayout:Landroid/support/v7/widget/RecyclerView$LayoutManager;

    iget v1, p0, Landroid/support/v7/widget/RecyclerView$ViewPrefetcher;->mDx:I

    iget v2, p0, Landroid/support/v7/widget/RecyclerView$ViewPrefetcher;->mDy:I

    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView$ViewPrefetcher;->this$0:Landroid/support/v7/widget/RecyclerView;

    iget-object v3, v3, Landroid/support/v7/widget/RecyclerView;->mState:Landroid/support/v7/widget/RecyclerView$State;

    iget-object v4, p0, Landroid/support/v7/widget/RecyclerView$ViewPrefetcher;->mItemPrefetchArray:[I

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/support/v7/widget/RecyclerView$LayoutManager;->gatherPrefetchIndices(IILandroid/support/v7/widget/RecyclerView$State;[I)I

    move-result v12

    .line 4524
    .local v12, "viewCount":I
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView$ViewPrefetcher;->this$0:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->mRecycler:Landroid/support/v7/widget/RecyclerView$Recycler;

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView$ViewPrefetcher;->mItemPrefetchArray:[I

    invoke-virtual {v0, v1, v12}, Landroid/support/v7/widget/RecyclerView$Recycler;->prefetch([II)V
    :try_end_9e
    .catchall {:try_start_70 .. :try_end_9e} :catchall_a2

    .line 4526
    .end local v5    # "prefetchCount":I
    .end local v6    # "lastFrameVsyncNanos":J
    .end local v8    # "nowNanos":J
    .end local v10    # "nextFrameNanos":J
    .end local v12    # "viewCount":I
    invoke-static {}, Landroid/support/v4/os/TraceCompat;->endSection()V

    .line 4527
    goto :goto_a7

    .line 4526
    :catchall_a2
    move-exception v13

    invoke-static {}, Landroid/support/v4/os/TraceCompat;->endSection()V

    throw v13

    .line 4528
    :goto_a7
    return-void
.end method

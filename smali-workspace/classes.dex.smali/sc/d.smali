.class public final Lsc/d;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field static a:Ljava/lang/String;

.field private static b:[Ljava/lang/String;

.field private static final c:[B

.field private static d:I


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .line 24
    const/16 v0, 0x43

    new-array v0, v0, [B

    fill-array-data v0, :array_14

    sput-object v0, Lsc/d;->c:[B

    const/16 v0, 0xdf

    sput v0, Lsc/d;->d:I

    const/4 v0, 0x0

    sput-object v0, Lsc/d;->a:Ljava/lang/String;

    .line 25
    const/4 v0, 0x0

    sput-object v0, Lsc/d;->b:[Ljava/lang/String;

    return-void

    :array_14
    .array-data 1
        0x51t
        0x4bt
        -0x2dt
        0x32t
        0x3t
        0x3t
        0x3t
        -0x29t
        0x3t
        0x1at
        0x3t
        -0x19t
        0x3t
        0x28t
        0x3t
        0x9t
        0x3t
        0x11t
        0xet
        0xbt
        0x2t
        -0x50t
        0x4bt
        -0x4t
        0x18t
        -0xet
        -0x42t
        0x51t
        0x4t
        -0x4ct
        0x2ct
        0x10t
        0x2bt
        0x1t
        0x4t
        -0x51t
        0x46t
        0x1t
        0xet
        0x3t
        -0x49t
        0x44t
        0x5t
        0x14t
        -0x8t
        0x10t
        -0x12t
        0x16t
        -0xct
        -0x42t
        0x4ct
        0x8t
        -0x4bt
        0x46t
        0xft
        0x2t
        -0x5t
        0x6t
        0x1t
        0x11t
        0x0t
        -0xet
        0x16t
        -0x8t
        0x9t
        0x2t
        -0x4at
    .end array-data
.end method

.method constructor <init>()V
    .registers 1

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/String;)I
    .registers 8

    .line 84
    .line 1117
    const/4 v0, 0x6

    invoke-virtual {p0, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result p0

    .line 1121
    :goto_9
    const/16 v0, 0xa

    if-le p0, v0, :cond_62

    .line 1122
    move v0, p0

    .line 1131
    move p0, v0

    rem-int/lit8 v6, v0, 0xa

    .line 1132
    sub-int v0, p0, v6

    div-int/lit8 v0, v0, 0xa

    .line 1133
    move p0, v0

    if-nez v0, :cond_1a

    .line 1134
    move p0, v6

    goto :goto_60

    .line 1136
    :cond_1a
    move v0, v6

    move v1, p0

    .line 2131
    move p0, v1

    rem-int/lit8 v1, v1, 0xa

    move v6, v1

    .line 2132
    sub-int v1, p0, v6

    div-int/lit8 v1, v1, 0xa

    .line 2133
    move p0, v1

    if-nez v1, :cond_29

    .line 2134
    move v1, v6

    goto :goto_5e

    .line 2136
    :cond_29
    move v1, v6

    move v2, p0

    .line 3131
    move p0, v2

    rem-int/lit8 v2, v2, 0xa

    move v6, v2

    .line 3132
    sub-int v2, p0, v6

    div-int/lit8 v2, v2, 0xa

    .line 3133
    move p0, v2

    if-nez v2, :cond_38

    .line 3134
    move v2, v6

    goto :goto_5d

    .line 3136
    :cond_38
    move v2, v6

    move v3, p0

    .line 4131
    move p0, v3

    rem-int/lit8 v3, v3, 0xa

    move v6, v3

    .line 4132
    sub-int v3, p0, v6

    div-int/lit8 v3, v3, 0xa

    .line 4133
    move p0, v3

    if-nez v3, :cond_47

    .line 4134
    move v3, v6

    goto :goto_5c

    .line 4136
    :cond_47
    move v3, v6

    move v4, p0

    .line 5131
    move p0, v4

    rem-int/lit8 v4, v4, 0xa

    move v6, v4

    .line 5132
    sub-int v4, p0, v6

    div-int/lit8 v4, v4, 0xa

    .line 5133
    move p0, v4

    if-nez v4, :cond_56

    .line 5134
    move v4, v6

    goto :goto_5b

    .line 5136
    :cond_56
    invoke-static {p0}, Lsc/d;->b(I)I

    move-result v4

    add-int/2addr v4, v6

    .line 4136
    :goto_5b
    add-int/2addr v3, v4

    .line 3136
    :goto_5c
    add-int/2addr v2, v3

    .line 2136
    :goto_5d
    add-int/2addr v1, v2

    .line 1136
    :goto_5e
    add-int p0, v0, v1

    .line 1122
    :goto_60
    goto/16 :goto_9

    .line 1123
    :cond_62
    const/16 v0, 0xa

    if-ne p0, v0, :cond_68

    .line 1124
    const/4 v0, 0x0

    return v0

    .line 84
    .line 1126
    :cond_68
    return p0
.end method

.method public static a()Ljava/lang/String;
    .registers 4

    .line 73
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    .line 74
    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lsc/d;->c:[B

    const/16 v2, 0x36

    aget-byte v1, v1, v2

    int-to-byte v1, v1

    sget-object v2, Lsc/d;->c:[B

    const/16 v3, 0x3c

    aget-byte v2, v2, v3

    int-to-byte v2, v2

    const/16 v3, 0x23

    invoke-static {v1, v3, v2}, Lsc/d;->a(BSS)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    const-string v2, ""

    .line 75
    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 76
    const/4 v1, 0x0

    const/16 v2, 0x10

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 72
    return-object v0
.end method

.method private static a(BSS)Ljava/lang/String;
    .registers 9

    sget-object v5, Lsc/d;->c:[B

    const/4 v4, 0x0

    rsub-int/lit8 p1, p1, 0x24

    mul-int/lit8 p2, p2, 0x2

    add-int/lit8 p2, p2, 0x2d

    new-instance v0, Ljava/lang/String;

    rsub-int/lit8 p0, p0, 0x1f

    new-array v1, p1, [B

    if-nez v5, :cond_16

    move v2, p2

    move v3, p1

    :goto_13
    add-int/2addr v2, v3

    add-int/lit8 p2, v2, -0x3

    :cond_16
    move v2, v4

    add-int/lit8 v4, v4, 0x1

    int-to-byte v3, p2

    aput-byte v3, v1, v2

    if-ne v4, p1, :cond_23

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Ljava/lang/String;-><init>([BI)V

    return-object v0

    :cond_23
    move v2, p2

    add-int/lit8 p0, p0, 0x1

    aget-byte v3, v5, p0

    goto :goto_13
.end method

.method public static a(Ljava/lang/String;I)Ljava/lang/String;
    .registers 4

    .line 92
    invoke-static {}, Lsc/d;->d()V

    .line 93
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lsc/d;->b:[Ljava/lang/String;

    aget-object v1, v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lbr/com/itau/security/commons/binary/ByteUtils;->from(Ljava/lang/String;)[B

    move-result-object v0

    .line 94
    invoke-static {v0}, Lbr/com/itau/security/commons/Crypto;->sha1([B)[B

    move-result-object v0

    invoke-static {v0}, Lbr/com/itau/security/commons/binary/ByteUtils;->bytes2hexStr([B)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static declared-synchronized a(Ljava/lang/String;[B)V
    .registers 4

    const-class v1, Lsc/d;

    monitor-enter v1

    .line 29
    :try_start_3
    invoke-static {}, Lsc/d;->c()Z

    move-result v0

    if-nez v0, :cond_b

    .line 30
    monitor-exit v1

    return-void

    .line 32
    :cond_b
    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/String;

    .line 33
    sput-object v0, Lsc/d;->b:[Ljava/lang/String;

    invoke-static {v0, p0, p1}, Lbr/com/itau/security/license/LGR;->fillSalts([Ljava/lang/String;Ljava/lang/String;[B)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lsc/d;->a:Ljava/lang/String;
    :try_end_17
    .catchall {:try_start_3 .. :try_end_17} :catchall_19

    .line 34
    monitor-exit v1

    return-void

    :catchall_19
    move-exception p0

    monitor-exit v1

    throw p0
.end method

.method public static a(I[B[BI)[B
    .registers 8

    .line 50
    invoke-static {}, Lsc/d;->d()V

    .line 52
    sget-object v0, Lsc/d;->b:[Ljava/lang/String;

    if-eqz v0, :cond_d

    sget-object v0, Lsc/d;->b:[Ljava/lang/String;

    aget-object v0, v0, p3

    if-nez v0, :cond_2c

    .line 53
    :cond_d
    new-instance v0, Lbr/com/itau/security/commons/exception/CommonSecurityException;

    sget-object v1, Lsc/d;->c:[B

    const/16 v2, 0x36

    aget-byte v1, v1, v2

    int-to-byte v1, v1

    sget-object v2, Lsc/d;->c:[B

    const/16 v3, 0x2b

    aget-byte v2, v2, v3

    int-to-byte v2, v2

    add-int/lit8 v3, v2, -0x1

    int-to-byte v3, v3

    invoke-static {v1, v2, v3}, Lsc/d;->a(BSS)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lbr/com/itau/security/commons/exception/CommonSecurityException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 55
    :cond_2c
    sget-object v0, Lsc/d;->b:[Ljava/lang/String;

    aget-object v0, v0, p3

    invoke-static {v0}, Lbr/com/itau/security/commons/binary/ByteUtils;->from(Ljava/lang/String;)[B

    move-result-object p3

    .line 57
    const/4 v0, 0x2

    if-ne p0, v0, :cond_3c

    .line 58
    invoke-static {p1, p2, p3}, Lbr/com/itau/security/commons/Crypto;->pkcs7PaddingDecrypt([B[B[B)[B

    move-result-object v0

    return-object v0

    .line 60
    :cond_3c
    const/4 v0, 0x1

    if-ne p0, v0, :cond_44

    .line 61
    invoke-static {p1, p2, p3}, Lbr/com/itau/security/commons/Crypto;->pkcs7PaddingEncrypt([B[B[B)[B

    move-result-object v0

    return-object v0

    .line 63
    :cond_44
    const/4 v0, 0x0

    return-object v0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;I)[B
    .registers 6

    .line 37
    invoke-static {p0}, Lbr/com/itau/security/commons/binary/ByteUtils;->from(Ljava/lang/String;)[B

    move-result-object v0

    invoke-static {p1}, Lbr/com/itau/security/commons/binary/ByteUtils;->from(Ljava/lang/String;)[B

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v2, v0, v1, p2}, Lsc/d;->a(I[B[BI)[B

    move-result-object v0

    return-object v0
.end method

.method public static a([BLjava/lang/String;I)[B
    .registers 5

    .line 45
    invoke-static {p1}, Lbr/com/itau/security/commons/binary/ByteUtils;->from(Ljava/lang/String;)[B

    move-result-object v0

    const/4 v1, 0x2

    invoke-static {v1, p0, v0, p2}, Lsc/d;->a(I[B[BI)[B

    move-result-object v0

    return-object v0
.end method

.method private static b(I)I
    .registers 4

    .line 131
    rem-int/lit8 v2, p0, 0xa

    .line 132
    sub-int v0, p0, v2

    div-int/lit8 v0, v0, 0xa

    .line 133
    move p0, v0

    if-nez v0, :cond_a

    .line 134
    return v2

    .line 136
    :cond_a
    invoke-static {p0}, Lsc/d;->b(I)I

    move-result v0

    add-int/2addr v0, v2

    return v0
.end method

.method public static b(Ljava/lang/String;)I
    .registers 8

    .line 88
    .line 6117
    const/16 v0, 0xa

    invoke-virtual {p0, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result p0

    .line 6121
    :goto_a
    const/16 v0, 0xa

    if-le p0, v0, :cond_63

    .line 6122
    move v0, p0

    .line 6131
    move p0, v0

    rem-int/lit8 v6, v0, 0xa

    .line 6132
    sub-int v0, p0, v6

    div-int/lit8 v0, v0, 0xa

    .line 6133
    move p0, v0

    if-nez v0, :cond_1b

    .line 6134
    move p0, v6

    goto :goto_61

    .line 6136
    :cond_1b
    move v0, v6

    move v1, p0

    .line 7131
    move p0, v1

    rem-int/lit8 v1, v1, 0xa

    move v6, v1

    .line 7132
    sub-int v1, p0, v6

    div-int/lit8 v1, v1, 0xa

    .line 7133
    move p0, v1

    if-nez v1, :cond_2a

    .line 7134
    move v1, v6

    goto :goto_5f

    .line 7136
    :cond_2a
    move v1, v6

    move v2, p0

    .line 8131
    move p0, v2

    rem-int/lit8 v2, v2, 0xa

    move v6, v2

    .line 8132
    sub-int v2, p0, v6

    div-int/lit8 v2, v2, 0xa

    .line 8133
    move p0, v2

    if-nez v2, :cond_39

    .line 8134
    move v2, v6

    goto :goto_5e

    .line 8136
    :cond_39
    move v2, v6

    move v3, p0

    .line 9131
    move p0, v3

    rem-int/lit8 v3, v3, 0xa

    move v6, v3

    .line 9132
    sub-int v3, p0, v6

    div-int/lit8 v3, v3, 0xa

    .line 9133
    move p0, v3

    if-nez v3, :cond_48

    .line 9134
    move v3, v6

    goto :goto_5d

    .line 9136
    :cond_48
    move v3, v6

    move v4, p0

    .line 10131
    move p0, v4

    rem-int/lit8 v4, v4, 0xa

    move v6, v4

    .line 10132
    sub-int v4, p0, v6

    div-int/lit8 v4, v4, 0xa

    .line 10133
    move p0, v4

    if-nez v4, :cond_57

    .line 10134
    move v4, v6

    goto :goto_5c

    .line 10136
    :cond_57
    invoke-static {p0}, Lsc/d;->b(I)I

    move-result v4

    add-int/2addr v4, v6

    .line 9136
    :goto_5c
    add-int/2addr v3, v4

    .line 8136
    :goto_5d
    add-int/2addr v2, v3

    .line 7136
    :goto_5e
    add-int/2addr v1, v2

    .line 6136
    :goto_5f
    add-int p0, v0, v1

    .line 6122
    :goto_61
    goto/16 :goto_a

    .line 6123
    :cond_63
    const/16 v0, 0xa

    if-ne p0, v0, :cond_69

    .line 6124
    const/4 v0, 0x0

    return v0

    .line 88
    .line 6126
    :cond_69
    return p0
.end method

.method public static b()Ljava/lang/String;
    .registers 4

    .line 80
    new-instance v0, Ljava/text/SimpleDateFormat;

    sget v1, Lsc/d;->d:I

    and-int/lit8 v1, v1, 0x3c

    int-to-byte v1, v1

    sget-object v2, Lsc/d;->c:[B

    const/16 v3, 0x2f

    aget-byte v2, v2, v3

    int-to-byte v2, v2

    const/16 v3, 0x26

    invoke-static {v1, v2, v3}, Lsc/d;->a(BSS)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    new-instance v1, Ljava/util/Date;

    invoke-direct {v1}, Ljava/util/Date;-><init>()V

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static c()Z
    .registers 2

    .line 99
    sget-object v0, Lsc/d;->b:[Ljava/lang/String;

    if-nez v0, :cond_6

    .line 100
    const/4 v0, 0x1

    return v0

    .line 102
    :cond_6
    const/4 v1, 0x0

    :goto_7
    sget-object v0, Lsc/d;->b:[Ljava/lang/String;

    array-length v0, v0

    if-ge v1, v0, :cond_17

    .line 103
    sget-object v0, Lsc/d;->b:[Ljava/lang/String;

    aget-object v0, v0, v1

    if-nez v0, :cond_14

    .line 104
    const/4 v0, 0x1

    return v0

    .line 102
    :cond_14
    add-int/lit8 v1, v1, 0x1

    goto :goto_7

    .line 107
    :cond_17
    const/4 v0, 0x0

    return v0
.end method

.method private static d()V
    .registers 5

    .line 111
    invoke-static {}, Lsc/d;->c()Z

    move-result v0

    if-nez v0, :cond_a

    sget-object v0, Lsc/d;->a:Ljava/lang/String;

    if-nez v0, :cond_27

    .line 112
    :cond_a
    new-instance v0, Lbr/com/itau/security/commons/exception/CommonSecurityException;

    sget-object v1, Lsc/d;->c:[B

    const/16 v2, 0x3c

    aget-byte v1, v1, v2

    int-to-byte v1, v1

    int-to-byte v2, v1

    sget-object v3, Lsc/d;->c:[B

    const/16 v4, 0x1f

    aget-byte v3, v3, v4

    int-to-byte v3, v3

    invoke-static {v1, v2, v3}, Lsc/d;->a(BSS)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lbr/com/itau/security/commons/exception/CommonSecurityException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 113
    :cond_27
    return-void
.end method

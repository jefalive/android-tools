.class public final Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ComprovantePagamentoTributosActivity_;
.super Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ComprovantePagamentoTributosActivity;
.source "ComprovantePagamentoTributosActivity_.java"

# interfaces
.implements Lorg/androidannotations/api/view/HasViews;
.implements Lorg/androidannotations/api/view/OnViewChangedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ComprovantePagamentoTributosActivity_$IntentBuilder_;
    }
.end annotation


# instance fields
.field private final onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;


# direct methods
.method public constructor <init>()V
    .registers 2

    .line 39
    invoke-direct {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ComprovantePagamentoTributosActivity;-><init>()V

    .line 43
    new-instance v0, Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-direct {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ComprovantePagamentoTributosActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    return-void
.end method

.method static synthetic access$001(Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ComprovantePagamentoTributosActivity_;)V
    .registers 1
    .param p0, "x0"    # Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ComprovantePagamentoTributosActivity_;

    .line 39
    invoke-super {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ComprovantePagamentoTributosActivity;->mostraDialogoDeProgresso()V

    return-void
.end method

.method static synthetic access$101(Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ComprovantePagamentoTributosActivity_;)V
    .registers 1
    .param p0, "x0"    # Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ComprovantePagamentoTributosActivity_;

    .line 39
    invoke-super {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ComprovantePagamentoTributosActivity;->escondeDialogoDeProgresso()V

    return-void
.end method

.method private init_(Landroid/os/Bundle;)V
    .registers 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 58
    new-instance v0, Lcom/itau/empresas/Preferences_;

    invoke-direct {v0, p0}, Lcom/itau/empresas/Preferences_;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ComprovantePagamentoTributosActivity_;->prefs:Lcom/itau/empresas/Preferences_;

    .line 59
    invoke-static {p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->registerOnViewChangedListener(Lorg/androidannotations/api/view/OnViewChangedListener;)V

    .line 60
    invoke-static {}, Lcom/itau/empresas/CustomApplication_;->getInstance()Lcom/itau/empresas/CustomApplication;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ComprovantePagamentoTributosActivity_;->application:Lcom/itau/empresas/CustomApplication;

    .line 61
    invoke-direct {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ComprovantePagamentoTributosActivity_;->injectExtras_()V

    .line 62
    invoke-virtual {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ComprovantePagamentoTributosActivity_;->afterInject()V

    .line 63
    return-void
.end method

.method private injectExtras_()V
    .registers 3

    .line 121
    invoke-virtual {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ComprovantePagamentoTributosActivity_;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 122
    .local v1, "extras_":Landroid/os/Bundle;
    if-eqz v1, :cond_3e

    .line 123
    const-string v0, "shareComprovante"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 124
    const-string v0, "shareComprovante"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ComprovantePagamentoTributosActivity_;->shareComprovante:Z

    .line 126
    :cond_1a
    const-string v0, "dadosComprovanteList"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2c

    .line 127
    const-string v0, "dadosComprovanteList"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ComprovantePagamentoTributosActivity_;->dadosComprovanteList:Ljava/util/ArrayList;

    .line 129
    :cond_2c
    const-string v0, "detalhesAutorizacaoVO"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3e

    .line 130
    const-string v0, "detalhesAutorizacaoVO"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ComprovantePagamentoTributosActivity_;->detalhesAutorizacaoVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;

    .line 133
    :cond_3e
    return-void
.end method


# virtual methods
.method public escondeDialogoDeProgresso()V
    .registers 5

    .line 158
    const-string v0, ""

    new-instance v1, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ComprovantePagamentoTributosActivity_$3;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ComprovantePagamentoTributosActivity_$3;-><init>(Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ComprovantePagamentoTributosActivity_;)V

    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3}, Lorg/androidannotations/api/UiThreadExecutor;->runTask(Ljava/lang/String;Ljava/lang/Runnable;J)V

    .line 166
    return-void
.end method

.method public mostraDialogoDeProgresso()V
    .registers 5
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "CommitTransaction"
        }
    .end annotation

    .line 146
    const-string v0, ""

    new-instance v1, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ComprovantePagamentoTributosActivity_$2;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ComprovantePagamentoTributosActivity_$2;-><init>(Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ComprovantePagamentoTributosActivity_;)V

    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3}, Lorg/androidannotations/api/UiThreadExecutor;->runTask(Ljava/lang/String;Ljava/lang/Runnable;J)V

    .line 154
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 50
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ComprovantePagamentoTributosActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-static {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    move-result-object v1

    .line 51
    .local v1, "previousNotifier":Lorg/androidannotations/api/view/OnViewChangedNotifier;
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ComprovantePagamentoTributosActivity_;->init_(Landroid/os/Bundle;)V

    .line 52
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ComprovantePagamentoTributosActivity;->onCreate(Landroid/os/Bundle;)V

    .line 53
    invoke-static {v1}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 54
    const v0, 0x7f030024

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ComprovantePagamentoTributosActivity_;->setContentView(I)V

    .line 55
    return-void
.end method

.method public onViewChanged(Lorg/androidannotations/api/view/HasViews;)V
    .registers 4
    .param p1, "hasViews"    # Lorg/androidannotations/api/view/HasViews;

    .line 97
    const v0, 0x7f0e00e8

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ComprovantePagamentoTributosActivity_;->campoValorPagamento:Landroid/widget/TextView;

    .line 98
    const v0, 0x7f0e00f1

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ComprovantePagamentoTributosActivity_;->campoAutenticacao:Landroid/widget/TextView;

    .line 99
    const v0, 0x7f0e00ef

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ComprovantePagamentoTributosActivity_;->campoAlteracao:Landroid/widget/TextView;

    .line 100
    const v0, 0x7f0e00e7

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ComprovantePagamentoTributosActivity_;->labelTipoPagamento:Landroid/widget/TextView;

    .line 101
    const v0, 0x7f0e05c2

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ComprovantePagamentoTributosActivity_;->toolbarTitle:Landroid/widget/TextView;

    .line 102
    const v0, 0x7f0e00f2

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ComprovantePagamentoTributosActivity_;->botaoShareComprovante:Landroid/widget/Button;

    .line 103
    const v0, 0x7f0e00e5

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ComprovantePagamentoTributosActivity_;->llComprovante:Landroid/widget/LinearLayout;

    .line 104
    const v0, 0x7f0e00eb

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ComprovantePagamentoTributosActivity_;->llDadosComprovante:Landroid/widget/LinearLayout;

    .line 105
    const v0, 0x7f0e025a

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/Toolbar;

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ComprovantePagamentoTributosActivity_;->toolbar:Landroid/support/v7/widget/Toolbar;

    .line 106
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ComprovantePagamentoTributosActivity_;->botaoShareComprovante:Landroid/widget/Button;

    if-eqz v0, :cond_71

    .line 107
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ComprovantePagamentoTributosActivity_;->botaoShareComprovante:Landroid/widget/Button;

    new-instance v1, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ComprovantePagamentoTributosActivity_$1;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ComprovantePagamentoTributosActivity_$1;-><init>(Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ComprovantePagamentoTributosActivity_;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 116
    :cond_71
    invoke-virtual {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ComprovantePagamentoTributosActivity_;->updateView()V

    .line 117
    return-void
.end method

.method public setContentView(I)V
    .registers 3
    .param p1, "layoutResID"    # I

    .line 67
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ComprovantePagamentoTributosActivity;->setContentView(I)V

    .line 68
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ComprovantePagamentoTributosActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 69
    return-void
.end method

.method public setContentView(Landroid/view/View;)V
    .registers 3
    .param p1, "view"    # Landroid/view/View;

    .line 79
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ComprovantePagamentoTributosActivity;->setContentView(Landroid/view/View;)V

    .line 80
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ComprovantePagamentoTributosActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 81
    return-void
.end method

.method public setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .registers 4
    .param p1, "view"    # Landroid/view/View;
    .param p2, "params"    # Landroid/view/ViewGroup$LayoutParams;

    .line 73
    invoke-super {p0, p1, p2}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ComprovantePagamentoTributosActivity;->setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 74
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ComprovantePagamentoTributosActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 75
    return-void
.end method

.method public setIntent(Landroid/content/Intent;)V
    .registers 2
    .param p1, "newIntent"    # Landroid/content/Intent;

    .line 137
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ComprovantePagamentoTributosActivity;->setIntent(Landroid/content/Intent;)V

    .line 138
    invoke-direct {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ComprovantePagamentoTributosActivity_;->injectExtras_()V

    .line 139
    return-void
.end method

.class Lcom/itau/empresas/feature/credito/giro/controller/EmprestimosParceladosController$3;
.super Ljava/lang/Object;
.source "EmprestimosParceladosController.java"

# interfaces
.implements Lbr/com/itau/sdk/android/core/type/BackendCall;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/itau/empresas/feature/credito/giro/controller/EmprestimosParceladosController;->consultaContratadaComprovante(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/itau/empresas/feature/credito/giro/controller/EmprestimosParceladosController;

.field final synthetic val$agencia:Ljava/lang/String;

.field final synthetic val$conta:Ljava/lang/String;

.field final synthetic val$digitoConta:Ljava/lang/String;

.field final synthetic val$idComprovante:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/itau/empresas/feature/credito/giro/controller/EmprestimosParceladosController;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 6
    .param p1, "this$0"    # Lcom/itau/empresas/feature/credito/giro/controller/EmprestimosParceladosController;

    .line 54
    iput-object p1, p0, Lcom/itau/empresas/feature/credito/giro/controller/EmprestimosParceladosController$3;->this$0:Lcom/itau/empresas/feature/credito/giro/controller/EmprestimosParceladosController;

    iput-object p2, p0, Lcom/itau/empresas/feature/credito/giro/controller/EmprestimosParceladosController$3;->val$idComprovante:Ljava/lang/String;

    iput-object p3, p0, Lcom/itau/empresas/feature/credito/giro/controller/EmprestimosParceladosController$3;->val$agencia:Ljava/lang/String;

    iput-object p4, p0, Lcom/itau/empresas/feature/credito/giro/controller/EmprestimosParceladosController$3;->val$conta:Ljava/lang/String;

    iput-object p5, p0, Lcom/itau/empresas/feature/credito/giro/controller/EmprestimosParceladosController$3;->val$digitoConta:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public callBackend()V
    .registers 6

    .line 57
    invoke-static {}, Lcom/itau/empresas/feature/credito/giro/controller/EmprestimosParceladosController;->getApi()Lcom/itau/empresas/api/credito/ApiGiro;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/giro/controller/EmprestimosParceladosController$3;->val$idComprovante:Ljava/lang/String;

    iget-object v2, p0, Lcom/itau/empresas/feature/credito/giro/controller/EmprestimosParceladosController$3;->val$agencia:Ljava/lang/String;

    iget-object v3, p0, Lcom/itau/empresas/feature/credito/giro/controller/EmprestimosParceladosController$3;->val$conta:Ljava/lang/String;

    iget-object v4, p0, Lcom/itau/empresas/feature/credito/giro/controller/EmprestimosParceladosController$3;->val$digitoConta:Ljava/lang/String;

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/itau/empresas/api/credito/ApiGiro;->buscaEmprestimosParceladosContratadoComprovante(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosContratadosVO;

    move-result-object v0

    # invokes: Lcom/itau/empresas/feature/credito/giro/controller/EmprestimosParceladosController;->post(Ljava/lang/Object;)V
    invoke-static {v0}, Lcom/itau/empresas/feature/credito/giro/controller/EmprestimosParceladosController;->access$200(Ljava/lang/Object;)V

    .line 60
    return-void
.end method

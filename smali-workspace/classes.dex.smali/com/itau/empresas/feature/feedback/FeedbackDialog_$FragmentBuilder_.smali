.class public Lcom/itau/empresas/feature/feedback/FeedbackDialog_$FragmentBuilder_;
.super Lorg/androidannotations/api/builder/FragmentBuilder;
.source "FeedbackDialog_.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/itau/empresas/feature/feedback/FeedbackDialog_;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "FragmentBuilder_"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/androidannotations/api/builder/FragmentBuilder<Lcom/itau/empresas/feature/feedback/FeedbackDialog_$FragmentBuilder_;Lcom/itau/empresas/feature/feedback/FeedbackDialog;>;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 186
    invoke-direct {p0}, Lorg/androidannotations/api/builder/FragmentBuilder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/itau/empresas/feature/feedback/FeedbackDialog;
    .registers 3

    .line 192
    new-instance v1, Lcom/itau/empresas/feature/feedback/FeedbackDialog_;

    invoke-direct {v1}, Lcom/itau/empresas/feature/feedback/FeedbackDialog_;-><init>()V

    .line 193
    .local v1, "fragment_":Lcom/itau/empresas/feature/feedback/FeedbackDialog_;
    iget-object v0, p0, Lcom/itau/empresas/feature/feedback/FeedbackDialog_$FragmentBuilder_;->args:Landroid/os/Bundle;

    invoke-virtual {v1, v0}, Lcom/itau/empresas/feature/feedback/FeedbackDialog_;->setArguments(Landroid/os/Bundle;)V

    .line 194
    return-object v1
.end method

.method public feedbackEspontaneo(Ljava/lang/Boolean;)Lcom/itau/empresas/feature/feedback/FeedbackDialog_$FragmentBuilder_;
    .registers 4
    .param p1, "feedbackEspontaneo"    # Ljava/lang/Boolean;

    .line 204
    iget-object v0, p0, Lcom/itau/empresas/feature/feedback/FeedbackDialog_$FragmentBuilder_;->args:Landroid/os/Bundle;

    const-string v1, "feedbackEspontaneo"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 205
    return-object p0
.end method

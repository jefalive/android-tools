.class public Lbr/com/itau/sdk/android/core/endpoint/a/e;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:[B

.field private static b:I


# direct methods
.method static constructor <clinit>()V
    .registers 3

    const/16 v0, 0xc

    new-array v0, v0, [B

    fill-array-data v0, :array_e

    sput-object v0, Lbr/com/itau/sdk/android/core/endpoint/a/e;->a:[B

    const/16 v0, 0xa3

    sput v0, Lbr/com/itau/sdk/android/core/endpoint/a/e;->b:I

    return-void

    :array_e
    .array-data 1
        0x5t
        -0x7t
        0x3et
        -0x1at
        -0x4t
        0x0t
        -0xdt
        0xbt
        0x6t
        -0x7t
        -0x12t
        0x12t
    .end array-data
.end method

.method public constructor <init>()V
    .registers 1

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(III)Ljava/lang/String;
    .registers 9

    mul-int/lit8 p2, p2, 0x2

    rsub-int/lit8 p2, p2, 0x55

    new-instance v0, Ljava/lang/String;

    mul-int/lit8 p0, p0, 0x2

    add-int/lit8 p0, p0, 0x5

    sget-object v5, Lbr/com/itau/sdk/android/core/endpoint/a/e;->a:[B

    const/4 v4, 0x0

    mul-int/lit8 p1, p1, 0x4

    rsub-int/lit8 p1, p1, 0x7

    new-array v1, p0, [B

    add-int/lit8 p0, p0, -0x1

    if-nez v5, :cond_1c

    move v2, p0

    move v3, p1

    :goto_19
    add-int/2addr v2, v3

    add-int/lit8 p2, v2, -0x7

    :cond_1c
    int-to-byte v2, p2

    aput-byte v2, v1, v4

    add-int/lit8 p1, p1, 0x1

    if-ne v4, p0, :cond_28

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Ljava/lang/String;-><init>([BI)V

    return-object v0

    :cond_28
    move v2, p2

    add-int/lit8 v4, v4, 0x1

    aget-byte v3, v5, p1

    goto :goto_19
.end method

.method public static a(Ljava/lang/String;)Ljava/lang/String;
    .registers 6

    .line 43
    :try_start_0
    sget-object v0, Lbr/com/itau/sdk/android/core/endpoint/a/e;->a:[B

    const/4 v1, 0x5

    aget-byte v0, v0, v1

    add-int/lit8 v1, v0, 0x1

    invoke-static {v0, v1, v1}, Lbr/com/itau/sdk/android/core/endpoint/a/e;->a(III)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v3

    .line 44
    sget-object v0, Lbr/com/itau/sdk/android/core/endpoint/a/e;->a:[B

    const/4 v1, 0x5

    aget-byte v0, v0, v1

    invoke-static {v0, v0, v0}, Lbr/com/itau/sdk/android/core/endpoint/a/e;->a(III)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v2, 0x0

    invoke-virtual {v3, v0, v2, v1}, Ljava/security/MessageDigest;->update([BII)V

    .line 45
    invoke-virtual {v3}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v4

    .line 46
    invoke-static {v4}, Lbr/com/itau/sdk/android/core/endpoint/a/e;->a([B)Ljava/lang/String;
    :try_end_2b
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_2b} :catch_2d
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_2b} :catch_32

    move-result-object v0

    return-object v0

    .line 47
    :catch_2d
    move-exception v3

    .line 48
    invoke-virtual {v3}, Ljava/security/NoSuchAlgorithmException;->printStackTrace()V

    .line 51
    goto :goto_36

    .line 49
    :catch_32
    move-exception v3

    .line 50
    invoke-virtual {v3}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    .line 53
    :goto_36
    const/4 v0, 0x0

    return-object v0
.end method

.method private static a([B)Ljava/lang/String;
    .registers 10

    .line 21
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 22
    move-object v3, p0

    array-length v4, v3

    const/4 v5, 0x0

    :goto_8
    if-ge v5, v4, :cond_2f

    aget-byte v6, v3, v5

    .line 23
    ushr-int/lit8 v0, v6, 0x4

    and-int/lit8 v7, v0, 0xf

    .line 24
    const/4 v8, 0x0

    .line 26
    :cond_11
    const/4 v0, 0x0

    if-gt v0, v7, :cond_1c

    const/16 v0, 0x9

    if-gt v7, v0, :cond_1c

    add-int/lit8 v0, v7, 0x30

    int-to-char v0, v0

    goto :goto_21

    :cond_1c
    add-int/lit8 v0, v7, -0xa

    add-int/lit8 v0, v0, 0x61

    int-to-char v0, v0

    :goto_21
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 27
    and-int/lit8 v7, v6, 0xf

    .line 28
    move v0, v8

    add-int/lit8 v8, v8, 0x1

    const/4 v1, 0x1

    if-lt v0, v1, :cond_11

    .line 22
    add-int/lit8 v5, v5, 0x1

    goto :goto_8

    .line 30
    :cond_2f
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

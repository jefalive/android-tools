.class public final Lbr/com/concretesolutions/canarinho/validator/ValidadorTelefone;
.super Ljava/lang/Object;
.source "ValidadorTelefone.java"

# interfaces
.implements Lbr/com/concretesolutions/canarinho/validator/Validador;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lbr/com/concretesolutions/canarinho/validator/ValidadorTelefone$SingletonHolder;
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .registers 1

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    return-void
.end method

.method synthetic constructor <init>(Lbr/com/concretesolutions/canarinho/validator/ValidadorTelefone$1;)V
    .registers 2
    .param p1, "x0"    # Lbr/com/concretesolutions/canarinho/validator/ValidadorTelefone$1;

    .line 10
    invoke-direct {p0}, Lbr/com/concretesolutions/canarinho/validator/ValidadorTelefone;-><init>()V

    return-void
.end method

.method public static getInstance()Lbr/com/concretesolutions/canarinho/validator/ValidadorTelefone;
    .registers 1

    .line 17
    # getter for: Lbr/com/concretesolutions/canarinho/validator/ValidadorTelefone$SingletonHolder;->INSTANCE:Lbr/com/concretesolutions/canarinho/validator/ValidadorTelefone;
    invoke-static {}, Lbr/com/concretesolutions/canarinho/validator/ValidadorTelefone$SingletonHolder;->access$000()Lbr/com/concretesolutions/canarinho/validator/ValidadorTelefone;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public ehValido(Landroid/text/Editable;Lbr/com/concretesolutions/canarinho/validator/Validador$ResultadoParcial;)Lbr/com/concretesolutions/canarinho/validator/Validador$ResultadoParcial;
    .registers 6
    .param p1, "valor"    # Landroid/text/Editable;
    .param p2, "resultadoParcial"    # Lbr/com/concretesolutions/canarinho/validator/Validador$ResultadoParcial;

    .line 33
    if-eqz p2, :cond_4

    if-nez p1, :cond_c

    .line 34
    :cond_4
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Valores n\u00e3o podem ser nulos"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 37
    :cond_c
    sget-object v0, Lbr/com/concretesolutions/canarinho/formatador/Formatador$Padroes;->PADRAO_SOMENTE_NUMEROS:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/util/regex/Matcher;->replaceAll(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 39
    .local v2, "desformatado":Ljava/lang/String;
    invoke-virtual {p0, v2}, Lbr/com/concretesolutions/canarinho/validator/ValidadorTelefone;->ehValido(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_39

    .line 40
    .line 41
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v1, 0xb

    if-ge v0, v1, :cond_28

    const/4 v0, 0x1

    goto :goto_29

    :cond_28
    const/4 v0, 0x0

    :goto_29
    invoke-virtual {p2, v0}, Lbr/com/concretesolutions/canarinho/validator/Validador$ResultadoParcial;->parcialmenteValido(Z)Lbr/com/concretesolutions/canarinho/validator/Validador$ResultadoParcial;

    move-result-object v0

    const-string v1, "Telefone inv\u00e1lido"

    .line 42
    invoke-virtual {v0, v1}, Lbr/com/concretesolutions/canarinho/validator/Validador$ResultadoParcial;->mensagem(Ljava/lang/String;)Lbr/com/concretesolutions/canarinho/validator/Validador$ResultadoParcial;

    move-result-object v0

    .line 43
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lbr/com/concretesolutions/canarinho/validator/Validador$ResultadoParcial;->totalmenteValido(Z)Lbr/com/concretesolutions/canarinho/validator/Validador$ResultadoParcial;

    move-result-object v0

    return-object v0

    .line 46
    .line 47
    :cond_39
    const/4 v0, 0x1

    invoke-virtual {p2, v0}, Lbr/com/concretesolutions/canarinho/validator/Validador$ResultadoParcial;->parcialmenteValido(Z)Lbr/com/concretesolutions/canarinho/validator/Validador$ResultadoParcial;

    move-result-object v0

    .line 48
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lbr/com/concretesolutions/canarinho/validator/Validador$ResultadoParcial;->totalmenteValido(Z)Lbr/com/concretesolutions/canarinho/validator/Validador$ResultadoParcial;

    move-result-object v0

    return-object v0
.end method

.method public ehValido(Ljava/lang/String;)Z
    .registers 5
    .param p1, "valor"    # Ljava/lang/String;

    .line 22
    if-eqz p1, :cond_a

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v1, 0xa

    if-ge v0, v1, :cond_c

    .line 23
    :cond_a
    const/4 v0, 0x0

    return v0

    .line 26
    :cond_c
    sget-object v0, Lbr/com/concretesolutions/canarinho/formatador/Formatador$Padroes;->PADRAO_SOMENTE_NUMEROS:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/util/regex/Matcher;->replaceAll(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 28
    .local v2, "desformatado":Ljava/lang/String;
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v1, 0xa

    if-eq v0, v1, :cond_28

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v1, 0xb

    if-ne v0, v1, :cond_2a

    :cond_28
    const/4 v0, 0x1

    goto :goto_2b

    :cond_2a
    const/4 v0, 0x0

    :goto_2b
    return v0
.end method

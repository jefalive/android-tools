.class public final Lbr/com/itau/textovoz/R$layout;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lbr/com/itau/textovoz/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "layout"
.end annotation


# static fields
.field public static final abc_action_bar_title_item:I = 0x7f030000

.field public static final abc_action_bar_up_container:I = 0x7f030001

.field public static final abc_action_bar_view_list_nav_layout:I = 0x7f030002

.field public static final abc_action_menu_item_layout:I = 0x7f030003

.field public static final abc_action_menu_layout:I = 0x7f030004

.field public static final abc_action_mode_bar:I = 0x7f030005

.field public static final abc_action_mode_close_item_material:I = 0x7f030006

.field public static final abc_activity_chooser_view:I = 0x7f030007

.field public static final abc_activity_chooser_view_list_item:I = 0x7f030008

.field public static final abc_alert_dialog_button_bar_material:I = 0x7f030009

.field public static final abc_alert_dialog_material:I = 0x7f03000a

.field public static final abc_dialog_title_material:I = 0x7f03000b

.field public static final abc_expanded_menu_layout:I = 0x7f03000c

.field public static final abc_list_menu_item_checkbox:I = 0x7f03000d

.field public static final abc_list_menu_item_icon:I = 0x7f03000e

.field public static final abc_list_menu_item_layout:I = 0x7f03000f

.field public static final abc_list_menu_item_radio:I = 0x7f030010

.field public static final abc_popup_menu_header_item_layout:I = 0x7f030011

.field public static final abc_popup_menu_item_layout:I = 0x7f030012

.field public static final abc_screen_content_include:I = 0x7f030013

.field public static final abc_screen_simple:I = 0x7f030014

.field public static final abc_screen_simple_overlay_action_mode:I = 0x7f030015

.field public static final abc_screen_toolbar:I = 0x7f030016

.field public static final abc_search_dropdown_item_icons_2line:I = 0x7f030017

.field public static final abc_search_view:I = 0x7f030018

.field public static final abc_select_dialog_material:I = 0x7f030019

.field public static final activity_error_system:I = 0x7f030034

.field public static final activity_faq:I = 0x7f030036

.field public static final activity_home_search:I = 0x7f03003e

.field public static final activity_home_search_:I = 0x7f03003f

.field public static final activity_search_voice:I = 0x7f030063

.field public static final card_view_faq:I = 0x7f03006f

.field public static final card_view_simple_balance_library:I = 0x7f030070

.field public static final design_bottom_sheet_dialog:I = 0x7f030078

.field public static final design_layout_snackbar:I = 0x7f030079

.field public static final design_layout_snackbar_include:I = 0x7f03007a

.field public static final design_layout_tab_icon:I = 0x7f03007b

.field public static final design_layout_tab_text:I = 0x7f03007c

.field public static final design_menu_item_action_area:I = 0x7f03007d

.field public static final design_navigation_item:I = 0x7f03007e

.field public static final design_navigation_item_header:I = 0x7f03007f

.field public static final design_navigation_item_separator:I = 0x7f030080

.field public static final design_navigation_item_subheader:I = 0x7f030081

.field public static final design_navigation_menu:I = 0x7f030082

.field public static final design_navigation_menu_item:I = 0x7f030083

.field public static final design_text_input_password_icon:I = 0x7f030084

.field public static final fragment_empty_list:I = 0x7f03009f

.field public static final fragment_faq_list:I = 0x7f0300a4

.field public static final fragment_home_search:I = 0x7f0300a7

.field public static final fragment_menu_list:I = 0x7f0300b8

.field public static final fragment_receipts_list:I = 0x7f0300bc

.field public static final fragment_search_attendance:I = 0x7f0300bd

.field public static final fragment_search_feedback:I = 0x7f0300be

.field public static final generic_fragment_container:I = 0x7f0300c9

.field public static final itausdkcore_dialog_password:I = 0x7f0300cf

.field public static final itausdkcore_dialog_token:I = 0x7f0300d0

.field public static final itausdkcore_dialog_webview:I = 0x7f0300d1

.field public static final itausdkcore_simple_relative_layout:I = 0x7f0300d4

.field public static final itausdkcore_view_locked_token:I = 0x7f0300d5

.field public static final itausdkcore_view_sms_token_type:I = 0x7f0300d6

.field public static final itausdkcore_view_token_type:I = 0x7f0300d7

.field public static final itausdkcore_view_unlocked_token:I = 0x7f0300d8

.field public static final notification_media_action:I = 0x7f03012f

.field public static final notification_media_cancel_action:I = 0x7f030130

.field public static final notification_template_big_media:I = 0x7f030131

.field public static final notification_template_big_media_narrow:I = 0x7f030133

.field public static final notification_template_media:I = 0x7f030138

.field public static final notification_template_part_chronometer:I = 0x7f03013a

.field public static final notification_template_part_time:I = 0x7f03013b

.field public static final search_details_header:I = 0x7f030140

.field public static final search_details_row:I = 0x7f030141

.field public static final select_dialog_item_material:I = 0x7f030142

.field public static final select_dialog_multichoice_material:I = 0x7f030143

.field public static final select_dialog_singlechoice_material:I = 0x7f030144

.field public static final support_simple_spinner_dropdown_item:I = 0x7f030147

.field public static final toolbar_library:I = 0x7f03014a

.field public static final view_attendance_channel:I = 0x7f03014f

.field public static final view_attendance_channel_group:I = 0x7f030150

.field public static final view_content_overflow_message:I = 0x7f030164

.field public static final view_currency_value:I = 0x7f030165

.field public static final view_feedback_faq_library:I = 0x7f03016a

.field public static final view_item_query_common:I = 0x7f030176

.field public static final view_item_receipt:I = 0x7f030177

.field public static final view_link_itens:I = 0x7f03017a

.field public static final view_more_items:I = 0x7f030180


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 1370
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

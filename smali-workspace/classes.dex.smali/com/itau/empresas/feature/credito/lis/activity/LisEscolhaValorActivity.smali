.class public Lcom/itau/empresas/feature/credito/lis/activity/LisEscolhaValorActivity;
.super Lcom/itau/empresas/ui/activity/BaseActivity;
.source "LisEscolhaValorActivity.java"

# interfaces
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;


# instance fields
.field cifrao:Landroid/widget/TextView;

.field private contratacaoMaxima:Ljava/math/BigDecimal;

.field private contratacaoMinima:Ljava/math/BigDecimal;

.field corCinza:I

.field corLaranja:I

.field marginLisEscolhaValorSeekbar:F

.field ofertaContratacaoLisVo:Lcom/itau/empresas/feature/credito/lis/model/OfertaContratacaoLisVo;

.field possuiProdutoLisAdiciona:Z

.field rlLisEscolhaValorMinMax:Landroid/view/View;

.field seekBar:Landroid/widget/SeekBar;

.field selecionaValor:Lcom/itau/empresas/ui/view/EditTextMonetario;

.field separadorEditTex:Landroid/view/View;

.field valorDefault:Ljava/lang/Float;

.field private valorMaxReal:Ljava/math/BigDecimal;

.field valorMaximoSelecionar:Landroid/widget/TextView;

.field valorMinimoSelecionar:Landroid/widget/TextView;

.field valorTaxaJuros:Landroid/widget/TextView;

.field private valorUsadoSeekBar:Ljava/math/BigDecimal;


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 37
    invoke-direct {p0}, Lcom/itau/empresas/ui/activity/BaseActivity;-><init>()V

    return-void
.end method

.method private configuraAcessibilidade()V
    .registers 5

    .line 243
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisEscolhaValorActivity;->rlLisEscolhaValorMinMax:Landroid/view/View;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisEscolhaValorActivity;->contratacaoMinima:Ljava/math/BigDecimal;

    sget-object v3, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    .line 245
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    invoke-static {v2, v3}, Lcom/itau/empresas/ui/util/ValorMonetarioUtils;->formataValorDecimalParaMonetario(Ljava/math/BigDecimal;Z)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    iget-object v2, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisEscolhaValorActivity;->contratacaoMaxima:Ljava/math/BigDecimal;

    sget-object v3, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    .line 248
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    .line 247
    invoke-static {v2, v3}, Lcom/itau/empresas/ui/util/ValorMonetarioUtils;->formataValorDecimalParaMonetario(Ljava/math/BigDecimal;Z)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    aput-object v2, v1, v3

    .line 244
    const v2, 0x7f07041c

    invoke-virtual {p0, v2, v1}, Lcom/itau/empresas/feature/credito/lis/activity/LisEscolhaValorActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 243
    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 249
    return-void
.end method

.method private configuraLayout()V
    .registers 4

    .line 105
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisEscolhaValorActivity;->valorMinimoSelecionar:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisEscolhaValorActivity;->contratacaoMinima:Ljava/math/BigDecimal;

    sget-object v2, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    .line 107
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-static {v1, v2}, Lcom/itau/empresas/ui/util/ValorMonetarioUtils;->formataValorDecimalParaMonetario(Ljava/math/BigDecimal;Z)Ljava/lang/String;

    move-result-object v1

    .line 105
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 108
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisEscolhaValorActivity;->valorMaximoSelecionar:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisEscolhaValorActivity;->contratacaoMaxima:Ljava/math/BigDecimal;

    sget-object v2, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    .line 109
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-static {v1, v2}, Lcom/itau/empresas/ui/util/ValorMonetarioUtils;->formataValorDecimalParaMonetario(Ljava/math/BigDecimal;Z)Ljava/lang/String;

    move-result-object v1

    .line 108
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 110
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisEscolhaValorActivity;->valorTaxaJuros:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisEscolhaValorActivity;->ofertaContratacaoLisVo:Lcom/itau/empresas/feature/credito/lis/model/OfertaContratacaoLisVo;

    .line 112
    invoke-virtual {v1}, Lcom/itau/empresas/feature/credito/lis/model/OfertaContratacaoLisVo;->getTaxaJurosMes()Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-static {p0, v1}, Lcom/itau/empresas/ui/util/PorcentagemUtils;->formataPorcentagemTaxaMes(Landroid/content/Context;F)Ljava/lang/String;

    move-result-object v1

    .line 110
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 114
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisEscolhaValorActivity;->valorDefault:Ljava/lang/Float;

    if-eqz v0, :cond_4f

    .line 115
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisEscolhaValorActivity;->selecionaValor:Lcom/itau/empresas/ui/view/EditTextMonetario;

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisEscolhaValorActivity;->valorDefault:Ljava/lang/Float;

    .line 116
    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    sget-object v2, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-static {v1, v2}, Lcom/itau/empresas/ui/util/ValorMonetarioUtils;->formataValorDecimalParaMonetario(FZ)Ljava/lang/String;

    move-result-object v1

    .line 115
    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/view/EditTextMonetario;->setText(Ljava/lang/CharSequence;)V

    goto :goto_54

    .line 118
    :cond_4f
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisEscolhaValorActivity;->selecionaValor:Lcom/itau/empresas/ui/view/EditTextMonetario;

    invoke-static {p0, v0}, Lcom/itau/empresas/ui/util/TecladoUtils;->mostrarTeclado(Landroid/content/Context;Landroid/view/View;)V

    .line 120
    :goto_54
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisEscolhaValorActivity;->selecionaValor:Lcom/itau/empresas/ui/view/EditTextMonetario;

    iget v1, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisEscolhaValorActivity;->corLaranja:I

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/view/EditTextMonetario;->setTextColor(I)V

    .line 121
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisEscolhaValorActivity;->configuraAcessibilidade()V

    .line 122
    return-void
.end method

.method private configuraSeekBar()V
    .registers 6

    .line 131
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisEscolhaValorActivity;->contratacaoMaxima:Ljava/math/BigDecimal;

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisEscolhaValorActivity;->contratacaoMinima:Ljava/math/BigDecimal;

    invoke-virtual {v0, v1}, Ljava/math/BigDecimal;->subtract(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v0

    sget-object v1, Ljava/math/RoundingMode;->UP:Ljava/math/RoundingMode;

    .line 132
    const/4 v2, 0x0

    invoke-virtual {v0, v2, v1}, Ljava/math/BigDecimal;->setScale(ILjava/math/RoundingMode;)Ljava/math/BigDecimal;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisEscolhaValorActivity;->valorMaxReal:Ljava/math/BigDecimal;

    .line 133
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisEscolhaValorActivity;->valorMaxReal:Ljava/math/BigDecimal;

    const-wide v1, 0x3f847ae147ae147bL    # 0.01

    invoke-static {v1, v2}, Ljava/math/BigDecimal;->valueOf(D)Ljava/math/BigDecimal;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/math/BigDecimal;->multiply(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisEscolhaValorActivity;->valorUsadoSeekBar:Ljava/math/BigDecimal;

    .line 134
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisEscolhaValorActivity;->seekBar:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisEscolhaValorActivity;->valorUsadoSeekBar:Ljava/math/BigDecimal;

    invoke-virtual {v1}, Ljava/math/BigDecimal;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setMax(I)V

    .line 135
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisEscolhaValorActivity;->seekBar:Landroid/widget/SeekBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 137
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisEscolhaValorActivity;->seekBar:Landroid/widget/SeekBar;

    iget v1, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisEscolhaValorActivity;->marginLisEscolhaValorSeekbar:F

    float-to-int v1, v1

    iget v2, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisEscolhaValorActivity;->marginLisEscolhaValorSeekbar:F

    float-to-int v2, v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v3, v2, v4}, Landroid/widget/SeekBar;->setPadding(IIII)V

    .line 140
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisEscolhaValorActivity;->seekBar:Landroid/widget/SeekBar;

    invoke-virtual {v0, p0}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 141
    return-void
.end method

.method private configuraVariaveis()V
    .registers 4

    .line 98
    new-instance v0, Ljava/math/BigDecimal;

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisEscolhaValorActivity;->ofertaContratacaoLisVo:Lcom/itau/empresas/feature/credito/lis/model/OfertaContratacaoLisVo;

    invoke-virtual {v1}, Lcom/itau/empresas/feature/credito/lis/model/OfertaContratacaoLisVo;->getValorLimiteMinimo()F

    move-result v1

    float-to-double v1, v1

    invoke-direct {v0, v1, v2}, Ljava/math/BigDecimal;-><init>(D)V

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisEscolhaValorActivity;->contratacaoMinima:Ljava/math/BigDecimal;

    .line 99
    new-instance v0, Ljava/math/BigDecimal;

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisEscolhaValorActivity;->ofertaContratacaoLisVo:Lcom/itau/empresas/feature/credito/lis/model/OfertaContratacaoLisVo;

    .line 100
    invoke-virtual {v1}, Lcom/itau/empresas/feature/credito/lis/model/OfertaContratacaoLisVo;->getLimitesOfertados()Ljava/util/List;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/itau/empresas/feature/credito/lis/model/LisLimiteOfertadoVO;

    invoke-virtual {v1}, Lcom/itau/empresas/feature/credito/lis/model/LisLimiteOfertadoVO;->getLimiteOfertado()F

    move-result v1

    float-to-double v1, v1

    invoke-direct {v0, v1, v2}, Ljava/math/BigDecimal;-><init>(D)V

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisEscolhaValorActivity;->contratacaoMaxima:Ljava/math/BigDecimal;

    .line 101
    return-void
.end method

.method private getValorSelecionado()Ljava/math/BigDecimal;
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/text/ParseException;
        }
    .end annotation

    .line 125
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisEscolhaValorActivity;->selecionaValor:Lcom/itau/empresas/ui/view/EditTextMonetario;

    .line 126
    invoke-virtual {v0}, Lcom/itau/empresas/ui/view/EditTextMonetario;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/itau/empresas/ui/util/ValorMonetarioUtils;->formataValorMonetarioParaDecimal(Ljava/lang/String;Z)Ljava/math/BigDecimal;

    move-result-object v0

    .line 125
    return-object v0
.end method

.method public static intent(Landroid/content/Context;)Lcom/itau/empresas/feature/credito/lis/activity/LisEscolhaValorActivity_$IntentBuilder_;
    .registers 2
    .param p0, "context"    # Landroid/content/Context;

    .line 252
    new-instance v0, Lcom/itau/empresas/feature/credito/lis/activity/LisEscolhaValorActivity_$IntentBuilder_;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisEscolhaValorActivity_$IntentBuilder_;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method private mostraHintErro(Ljava/lang/String;)V
    .registers 6
    .param p1, "mensagem"    # Ljava/lang/String;

    .line 224
    const v0, 0x7f0c012e

    invoke-static {p0, v0}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v2

    .line 225
    .local v2, "color":I
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisEscolhaValorActivity;->cifrao:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 226
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisEscolhaValorActivity;->selecionaValor:Lcom/itau/empresas/ui/view/EditTextMonetario;

    invoke-virtual {v0, v2}, Lcom/itau/empresas/ui/view/EditTextMonetario;->setTextColor(I)V

    .line 227
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisEscolhaValorActivity;->separadorEditTex:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setBackgroundColor(I)V

    .line 228
    new-instance v3, Lbr/com/itau/widgets/hintview/Hint$Builder;

    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisEscolhaValorActivity;->selecionaValor:Lcom/itau/empresas/ui/view/EditTextMonetario;

    const v1, 0x7f09011d

    invoke-direct {v3, v0, v1}, Lbr/com/itau/widgets/hintview/Hint$Builder;-><init>(Landroid/view/View;I)V

    .line 229
    .local v3, "builder":Lbr/com/itau/widgets/hintview/Hint$Builder;
    const/4 v0, 0x1

    invoke-virtual {v3, v0}, Lbr/com/itau/widgets/hintview/Hint$Builder;->setCancelable(Z)Lbr/com/itau/widgets/hintview/Hint$Builder;

    .line 230
    invoke-virtual {v3, p1}, Lbr/com/itau/widgets/hintview/Hint$Builder;->setText(Ljava/lang/CharSequence;)Lbr/com/itau/widgets/hintview/Hint$Builder;

    .line 231
    new-instance v0, Lcom/itau/empresas/feature/credito/lis/activity/LisEscolhaValorActivity$1;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisEscolhaValorActivity$1;-><init>(Lcom/itau/empresas/feature/credito/lis/activity/LisEscolhaValorActivity;)V

    invoke-virtual {v3, v0}, Lbr/com/itau/widgets/hintview/Hint$Builder;->setOnDismissListener(Lbr/com/itau/widgets/hintview/OnDismissListener;)Lbr/com/itau/widgets/hintview/Hint$Builder;

    .line 239
    invoke-virtual {v3}, Lbr/com/itau/widgets/hintview/Hint$Builder;->show()Lbr/com/itau/widgets/hintview/Hint;

    .line 240
    return-void
.end method

.method private validarValorSelecionado(Ljava/math/BigDecimal;)Z
    .registers 4
    .param p1, "valorSelecionado"    # Ljava/math/BigDecimal;

    .line 180
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisEscolhaValorActivity;->contratacaoMinima:Ljava/math/BigDecimal;

    invoke-virtual {p1, v0}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result v0

    sget-object v1, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    invoke-virtual {v1}, Ljava/math/BigDecimal;->intValue()I

    move-result v1

    if-ge v0, v1, :cond_1f

    .line 181
    const v0, 0x7f07041b

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/credito/lis/activity/LisEscolhaValorActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/itau/empresas/feature/credito/lis/activity/LisEscolhaValorActivity;->mostraHintErro(Ljava/lang/String;)V

    .line 182
    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0

    .line 183
    :cond_1f
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisEscolhaValorActivity;->contratacaoMaxima:Ljava/math/BigDecimal;

    invoke-virtual {p1, v0}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result v0

    sget-object v1, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    invoke-virtual {v1}, Ljava/math/BigDecimal;->intValue()I

    move-result v1

    if-le v0, v1, :cond_3e

    .line 184
    const v0, 0x7f07041a

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/credito/lis/activity/LisEscolhaValorActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/itau/empresas/feature/credito/lis/activity/LisEscolhaValorActivity;->mostraHintErro(Ljava/lang/String;)V

    .line 185
    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0

    .line 187
    :cond_3e
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method


# virtual methods
.method aoCarregarTela()V
    .registers 1

    .line 92
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisEscolhaValorActivity;->configuraVariaveis()V

    .line 93
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisEscolhaValorActivity;->configuraSeekBar()V

    .line 94
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisEscolhaValorActivity;->configuraLayout()V

    .line 95
    return-void
.end method

.method atualizaSeekbarAfterTextChange()V
    .registers 5

    .line 147
    :try_start_0
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisEscolhaValorActivity;->selecionaValor:Lcom/itau/empresas/ui/view/EditTextMonetario;

    .line 148
    invoke-virtual {v0}, Lcom/itau/empresas/ui/view/EditTextMonetario;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/itau/empresas/ui/util/ValorMonetarioUtils;->formataValorMonetarioParaDecimal(Ljava/lang/String;Z)Ljava/math/BigDecimal;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisEscolhaValorActivity;->contratacaoMinima:Ljava/math/BigDecimal;

    .line 149
    invoke-virtual {v0, v1}, Ljava/math/BigDecimal;->subtract(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v0

    sget-object v1, Ljava/math/RoundingMode;->UP:Ljava/math/RoundingMode;

    .line 150
    const/4 v2, 0x0

    invoke-virtual {v0, v2, v1}, Ljava/math/BigDecimal;->setScale(ILjava/math/RoundingMode;)Ljava/math/BigDecimal;

    move-result-object v0

    .line 151
    const-wide v1, 0x3f847ae147ae147bL    # 0.01

    invoke-static {v1, v2}, Ljava/math/BigDecimal;->valueOf(D)Ljava/math/BigDecimal;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/math/BigDecimal;->multiply(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v3

    .line 153
    .local v3, "multiply":Ljava/math/BigDecimal;
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisEscolhaValorActivity;->seekBar:Landroid/widget/SeekBar;

    invoke-virtual {v3}, Ljava/math/BigDecimal;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setProgress(I)V
    :try_end_32
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_32} :catch_33

    .line 156
    .end local v3    # "multiply":Ljava/math/BigDecimal;
    goto :goto_3b

    .line 154
    :catch_33
    move-exception v3

    .line 155
    .local v3, "e":Ljava/text/ParseException;
    const-string v0, "ERRO PARSER"

    const-string v1, "ERRO AO PARSEAR VALOR PARA EDITTEXT"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 158
    .end local v3    # "e":Ljava/text/ParseException;
    :goto_3b
    return-void
.end method

.method public botaoFechar()V
    .registers 1

    .line 220
    invoke-super {p0}, Lcom/itau/empresas/ui/activity/BaseActivity;->onBackPressed()V

    .line 221
    return-void
.end method

.method public onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .registers 9
    .param p1, "seekBar"    # Landroid/widget/SeekBar;
    .param p2, "progress"    # I
    .param p3, "fromUser"    # Z

    .line 199
    if-eqz p3, :cond_25

    .line 200
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisEscolhaValorActivity;->valorMaxReal:Ljava/math/BigDecimal;

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisEscolhaValorActivity;->valorUsadoSeekBar:Ljava/math/BigDecimal;

    sget-object v2, Ljava/math/RoundingMode;->HALF_UP:Ljava/math/RoundingMode;

    const/4 v3, 0x2

    invoke-virtual {v0, v1, v3, v2}, Ljava/math/BigDecimal;->divide(Ljava/math/BigDecimal;ILjava/math/RoundingMode;)Ljava/math/BigDecimal;

    move-result-object v0

    new-instance v1, Ljava/math/BigDecimal;

    invoke-direct {v1, p2}, Ljava/math/BigDecimal;-><init>(I)V

    .line 201
    invoke-virtual {v0, v1}, Ljava/math/BigDecimal;->multiply(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisEscolhaValorActivity;->contratacaoMinima:Ljava/math/BigDecimal;

    invoke-virtual {v0, v1}, Ljava/math/BigDecimal;->add(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v4

    .line 203
    .local v4, "add":Ljava/math/BigDecimal;
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisEscolhaValorActivity;->selecionaValor:Lcom/itau/empresas/ui/view/EditTextMonetario;

    invoke-virtual {v4}, Ljava/math/BigDecimal;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/view/EditTextMonetario;->setText(Ljava/lang/CharSequence;)V

    .line 206
    .end local v4    # "add":Ljava/math/BigDecimal;
    :cond_25
    return-void
.end method

.method public onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .registers 2
    .param p1, "seekBar"    # Landroid/widget/SeekBar;

    .line 211
    return-void
.end method

.method public onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .registers 2
    .param p1, "seekBar"    # Landroid/widget/SeekBar;

    .line 216
    return-void
.end method

.method public opkeysConsumidas()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Ljava/lang/String;>;"
        }
    .end annotation

    .line 193
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public simular()V
    .registers 4

    .line 164
    :try_start_0
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisEscolhaValorActivity;->getValorSelecionado()Ljava/math/BigDecimal;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/itau/empresas/feature/credito/lis/activity/LisEscolhaValorActivity;->validarValorSelecionado(Ljava/math/BigDecimal;)Z

    move-result v0

    if-eqz v0, :cond_25

    .line 165
    .line 166
    invoke-static {p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisDataSimulacaoActivity_;->intent(Landroid/content/Context;)Lcom/itau/empresas/feature/credito/lis/activity/LisDataSimulacaoActivity_$IntentBuilder_;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisEscolhaValorActivity;->ofertaContratacaoLisVo:Lcom/itau/empresas/feature/credito/lis/model/OfertaContratacaoLisVo;

    .line 167
    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/credito/lis/activity/LisDataSimulacaoActivity_$IntentBuilder_;->ofertaContratacaoLisVo(Lcom/itau/empresas/feature/credito/lis/model/OfertaContratacaoLisVo;)Lcom/itau/empresas/feature/credito/lis/activity/LisDataSimulacaoActivity_$IntentBuilder_;

    move-result-object v0

    .line 168
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisEscolhaValorActivity;->getValorSelecionado()Ljava/math/BigDecimal;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/credito/lis/activity/LisDataSimulacaoActivity_$IntentBuilder_;->valorSelecionado(Ljava/math/BigDecimal;)Lcom/itau/empresas/feature/credito/lis/activity/LisDataSimulacaoActivity_$IntentBuilder_;

    move-result-object v0

    iget-boolean v1, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisEscolhaValorActivity;->possuiProdutoLisAdiciona:Z

    .line 169
    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/credito/lis/activity/LisDataSimulacaoActivity_$IntentBuilder_;->possuiProdutoLisAdicional(Z)Lcom/itau/empresas/feature/credito/lis/activity/LisDataSimulacaoActivity_$IntentBuilder_;

    move-result-object v0

    .line 170
    invoke-virtual {v0}, Lcom/itau/empresas/feature/credito/lis/activity/LisDataSimulacaoActivity_$IntentBuilder_;->start()Lorg/androidannotations/api/builder/PostActivityStarter;
    :try_end_25
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_25} :catch_26

    .line 175
    :cond_25
    goto :goto_38

    .line 172
    :catch_26
    move-exception v2

    .line 173
    .local v2, "e":Ljava/text/ParseException;
    const-string v0, "LIS ERRO"

    const-string v1, "Erro ao converter valor monetario"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 174
    const v0, 0x7f0705f2

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/credito/lis/activity/LisEscolhaValorActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/itau/empresas/feature/credito/lis/activity/LisEscolhaValorActivity;->mostraHintErro(Ljava/lang/String;)V

    .line 176
    .end local v2    # "e":Ljava/text/ParseException;
    :goto_38
    return-void
.end method

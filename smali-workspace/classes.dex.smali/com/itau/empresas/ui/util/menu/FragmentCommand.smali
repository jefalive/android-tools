.class public Lcom/itau/empresas/ui/util/menu/FragmentCommand;
.super Ljava/lang/Object;
.source "FragmentCommand.java"

# interfaces
.implements Lcom/itau/empresas/ui/util/menu/Command;


# instance fields
.field private classFragment:Ljava/lang/Class;


# direct methods
.method public constructor <init>(Ljava/lang/Class;)V
    .registers 2
    .param p1, "classFragment"    # Ljava/lang/Class;

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    iput-object p1, p0, Lcom/itau/empresas/ui/util/menu/FragmentCommand;->classFragment:Ljava/lang/Class;

    .line 14
    return-void
.end method


# virtual methods
.method public executar(Landroid/support/v7/app/AppCompatActivity;Lcom/itau/empresas/api/model/MenuVO;)V
    .registers 5
    .param p1, "activity"    # Landroid/support/v7/app/AppCompatActivity;
    .param p2, "menuVO"    # Lcom/itau/empresas/api/model/MenuVO;

    .line 18
    invoke-static {p1}, Lcom/itau/empresas/ui/activity/GenericaActivity;->intent(Landroid/content/Context;)Lcom/itau/empresas/ui/activity/GenericaActivity_$IntentBuilder_;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/ui/util/menu/FragmentCommand;->classFragment:Ljava/lang/Class;

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/activity/GenericaActivity_$IntentBuilder_;->fragmentClass(Ljava/lang/Class;)Lcom/itau/empresas/ui/activity/GenericaActivity_$IntentBuilder_;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/ui/activity/GenericaActivity_$IntentBuilder_;->start()Lorg/androidannotations/api/builder/PostActivityStarter;

    .line 19
    return-void
.end method

.class public final Lbr/com/itau/security/middlewarechannel/Configuration;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static a:Ljava/lang/String;

.field private static b:Ljava/lang/String;

.field private static final c:[B

.field private static d:I


# direct methods
.method static constructor <clinit>()V
    .registers 9

    .line 7
    const/16 v0, 0xc

    new-array v0, v0, [B

    fill-array-data v0, :array_4c

    sput-object v0, Lbr/com/itau/security/middlewarechannel/Configuration;->c:[B

    const/16 v0, 0xd3

    sput v0, Lbr/com/itau/security/middlewarechannel/Configuration;->d:I

    const/4 v0, 0x0

    sput-object v0, Lbr/com/itau/security/middlewarechannel/Configuration;->a:Ljava/lang/String;

    .line 8
    sget-object v0, Lbr/com/itau/security/middlewarechannel/Configuration;->c:[B

    const/4 v1, 0x0

    aget-byte v0, v0, v1

    int-to-byte v4, v0

    int-to-byte v5, v4

    int-to-byte v6, v5

    .line 1000
    mul-int/lit8 v6, v6, 0x4

    add-int/lit8 v6, v6, 0x6d

    const/4 v8, 0x0

    mul-int/lit8 v4, v4, 0x3

    rsub-int/lit8 v4, v4, 0x4

    sget-object v7, Lbr/com/itau/security/middlewarechannel/Configuration;->c:[B

    new-instance v0, Ljava/lang/String;

    mul-int/lit8 v5, v5, 0x3

    add-int/lit8 v5, v5, 0x8

    new-array v1, v5, [B

    if-nez v7, :cond_32

    move v2, v6

    move v3, v5

    :goto_2f
    add-int/2addr v2, v3

    add-int/lit8 v6, v2, 0x3

    :cond_32
    move v2, v8

    add-int/lit8 v8, v8, 0x1

    int-to-byte v3, v6

    aput-byte v3, v1, v2

    if-ne v8, v5, :cond_3f

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Ljava/lang/String;-><init>([BI)V

    goto :goto_45

    :cond_3f
    move v2, v6

    add-int/lit8 v4, v4, 0x1

    aget-byte v3, v7, v4

    goto :goto_2f

    .line 8
    :goto_45
    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lbr/com/itau/security/middlewarechannel/Configuration;->b:Ljava/lang/String;

    return-void

    :array_4c
    .array-data 1
        0x0t
        -0x80t
        0x18t
        -0x34t
        0x45t
        -0x1t
        -0x10t
        -0x37t
        0x38t
        0x0t
        -0x6t
        -0x9t
    .end array-data
.end method

.method public constructor <init>()V
    .registers 1

    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static activate(Ljava/lang/String;[B)V
    .registers 2

    .line 24
    invoke-static {p0, p1}, Lsc/d;->a(Ljava/lang/String;[B)V

    .line 25
    return-void
.end method

.method public static getServer()Ljava/lang/String;
    .registers 1

    .line 15
    sget-object v0, Lbr/com/itau/security/middlewarechannel/Configuration;->a:Ljava/lang/String;

    return-object v0
.end method

.method public static setServer(Ljava/lang/String;)V
    .registers 1

    .line 11
    sput-object p0, Lbr/com/itau/security/middlewarechannel/Configuration;->a:Ljava/lang/String;

    .line 12
    return-void
.end method

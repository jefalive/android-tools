.class Lcom/itau/empresas/feature/recarga/contatos/controller/ContatoRecargaController$3;
.super Ljava/lang/Object;
.source "ContatoRecargaController.java"

# interfaces
.implements Lbr/com/itau/sdk/android/core/type/BackendCall;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/itau/empresas/feature/recarga/contatos/controller/ContatoRecargaController;->alteraApelidoContatoRecarga(Ljava/lang/String;Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaApelidoInputVO;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/itau/empresas/feature/recarga/contatos/controller/ContatoRecargaController;

.field final synthetic val$contato:Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaApelidoInputVO;

.field final synthetic val$idContato:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/itau/empresas/feature/recarga/contatos/controller/ContatoRecargaController;Ljava/lang/String;Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaApelidoInputVO;)V
    .registers 4
    .param p1, "this$0"    # Lcom/itau/empresas/feature/recarga/contatos/controller/ContatoRecargaController;

    .line 43
    iput-object p1, p0, Lcom/itau/empresas/feature/recarga/contatos/controller/ContatoRecargaController$3;->this$0:Lcom/itau/empresas/feature/recarga/contatos/controller/ContatoRecargaController;

    iput-object p2, p0, Lcom/itau/empresas/feature/recarga/contatos/controller/ContatoRecargaController$3;->val$idContato:Ljava/lang/String;

    iput-object p3, p0, Lcom/itau/empresas/feature/recarga/contatos/controller/ContatoRecargaController$3;->val$contato:Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaApelidoInputVO;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public callBackend()V
    .registers 5

    .line 46
    new-instance v3, Lcom/itau/empresas/Evento$EventoApelidoContatoRecargaAlterado;

    invoke-direct {v3}, Lcom/itau/empresas/Evento$EventoApelidoContatoRecargaAlterado;-><init>()V

    .line 47
    .local v3, "eventoApelidoContatoRecargaAlterado":Lcom/itau/empresas/Evento$EventoApelidoContatoRecargaAlterado;
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/controller/ContatoRecargaController$3;->this$0:Lcom/itau/empresas/feature/recarga/contatos/controller/ContatoRecargaController;

    # invokes: Lcom/itau/empresas/feature/recarga/contatos/controller/ContatoRecargaController;->api()Ljava/lang/Object;
    invoke-static {v0}, Lcom/itau/empresas/feature/recarga/contatos/controller/ContatoRecargaController;->access$400(Lcom/itau/empresas/feature/recarga/contatos/controller/ContatoRecargaController;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/api/Api;

    iget-object v1, p0, Lcom/itau/empresas/feature/recarga/contatos/controller/ContatoRecargaController$3;->val$idContato:Ljava/lang/String;

    iget-object v2, p0, Lcom/itau/empresas/feature/recarga/contatos/controller/ContatoRecargaController$3;->val$contato:Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaApelidoInputVO;

    .line 48
    invoke-interface {v0, v1, v2}, Lcom/itau/empresas/api/Api;->alteraApelidoContatoRecarga(Ljava/lang/String;Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaApelidoInputVO;)Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;

    move-result-object v0

    .line 47
    invoke-virtual {v3, v0}, Lcom/itau/empresas/Evento$EventoApelidoContatoRecargaAlterado;->setContatoRecargaVO(Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;)V

    .line 49
    # invokes: Lcom/itau/empresas/feature/recarga/contatos/controller/ContatoRecargaController;->post(Ljava/lang/Object;)V
    invoke-static {v3}, Lcom/itau/empresas/feature/recarga/contatos/controller/ContatoRecargaController;->access$500(Ljava/lang/Object;)V

    .line 50
    return-void
.end method

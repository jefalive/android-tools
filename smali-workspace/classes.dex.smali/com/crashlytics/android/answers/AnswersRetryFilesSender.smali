.class Lcom/crashlytics/android/answers/AnswersRetryFilesSender;
.super Ljava/lang/Object;
.source "AnswersRetryFilesSender.java"

# interfaces
.implements Lio/fabric/sdk/android/services/events/FilesSender;


# static fields
.field private static final BACKOFF_MS:I = 0x3e8

.field private static final BACKOFF_POWER:I = 0x8

.field private static final JITTER_PERCENT:D = 0.1

.field private static final MAX_RETRIES:I = 0x5


# instance fields
.field private final filesSender:Lcom/crashlytics/android/answers/SessionAnalyticsFilesSender;

.field private final retryManager:Lcom/crashlytics/android/answers/RetryManager;


# direct methods
.method constructor <init>(Lcom/crashlytics/android/answers/SessionAnalyticsFilesSender;Lcom/crashlytics/android/answers/RetryManager;)V
    .registers 3
    .param p1, "filesSender"    # Lcom/crashlytics/android/answers/SessionAnalyticsFilesSender;
    .param p2, "retryManager"    # Lcom/crashlytics/android/answers/RetryManager;

    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    iput-object p1, p0, Lcom/crashlytics/android/answers/AnswersRetryFilesSender;->filesSender:Lcom/crashlytics/android/answers/SessionAnalyticsFilesSender;

    .line 42
    iput-object p2, p0, Lcom/crashlytics/android/answers/AnswersRetryFilesSender;->retryManager:Lcom/crashlytics/android/answers/RetryManager;

    .line 43
    return-void
.end method

.method public static build(Lcom/crashlytics/android/answers/SessionAnalyticsFilesSender;)Lcom/crashlytics/android/answers/AnswersRetryFilesSender;
    .registers 9
    .param p0, "filesSender"    # Lcom/crashlytics/android/answers/SessionAnalyticsFilesSender;

    .line 31
    new-instance v4, Lcom/crashlytics/android/answers/RandomBackoff;

    new-instance v0, Lio/fabric/sdk/android/services/concurrency/internal/ExponentialBackoff;

    const-wide/16 v1, 0x3e8

    const/16 v3, 0x8

    invoke-direct {v0, v1, v2, v3}, Lio/fabric/sdk/android/services/concurrency/internal/ExponentialBackoff;-><init>(JI)V

    const-wide v1, 0x3fb999999999999aL    # 0.1

    invoke-direct {v4, v0, v1, v2}, Lcom/crashlytics/android/answers/RandomBackoff;-><init>(Lio/fabric/sdk/android/services/concurrency/internal/Backoff;D)V

    .line 33
    .local v4, "backoff":Lio/fabric/sdk/android/services/concurrency/internal/Backoff;
    new-instance v5, Lio/fabric/sdk/android/services/concurrency/internal/DefaultRetryPolicy;

    const/4 v0, 0x5

    invoke-direct {v5, v0}, Lio/fabric/sdk/android/services/concurrency/internal/DefaultRetryPolicy;-><init>(I)V

    .line 34
    .local v5, "retryPolicy":Lio/fabric/sdk/android/services/concurrency/internal/RetryPolicy;
    new-instance v6, Lio/fabric/sdk/android/services/concurrency/internal/RetryState;

    invoke-direct {v6, v4, v5}, Lio/fabric/sdk/android/services/concurrency/internal/RetryState;-><init>(Lio/fabric/sdk/android/services/concurrency/internal/Backoff;Lio/fabric/sdk/android/services/concurrency/internal/RetryPolicy;)V

    .line 35
    .local v6, "retryState":Lio/fabric/sdk/android/services/concurrency/internal/RetryState;
    new-instance v7, Lcom/crashlytics/android/answers/RetryManager;

    invoke-direct {v7, v6}, Lcom/crashlytics/android/answers/RetryManager;-><init>(Lio/fabric/sdk/android/services/concurrency/internal/RetryState;)V

    .line 36
    .local v7, "retryManager":Lcom/crashlytics/android/answers/RetryManager;
    new-instance v0, Lcom/crashlytics/android/answers/AnswersRetryFilesSender;

    invoke-direct {v0, p0, v7}, Lcom/crashlytics/android/answers/AnswersRetryFilesSender;-><init>(Lcom/crashlytics/android/answers/SessionAnalyticsFilesSender;Lcom/crashlytics/android/answers/RetryManager;)V

    return-object v0
.end method


# virtual methods
.method public send(Ljava/util/List;)Z
    .registers 6
    .param p1, "files"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<Ljava/io/File;>;)Z"
        }
    .end annotation

    .line 47
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v1

    .line 48
    .local v1, "currentNanoTime":J
    iget-object v0, p0, Lcom/crashlytics/android/answers/AnswersRetryFilesSender;->retryManager:Lcom/crashlytics/android/answers/RetryManager;

    invoke-virtual {v0, v1, v2}, Lcom/crashlytics/android/answers/RetryManager;->canRetry(J)Z

    move-result v0

    if-eqz v0, :cond_22

    .line 49
    iget-object v0, p0, Lcom/crashlytics/android/answers/AnswersRetryFilesSender;->filesSender:Lcom/crashlytics/android/answers/SessionAnalyticsFilesSender;

    invoke-virtual {v0, p1}, Lcom/crashlytics/android/answers/SessionAnalyticsFilesSender;->send(Ljava/util/List;)Z

    move-result v3

    .line 50
    .local v3, "cleanup":Z
    if-eqz v3, :cond_1b

    .line 51
    iget-object v0, p0, Lcom/crashlytics/android/answers/AnswersRetryFilesSender;->retryManager:Lcom/crashlytics/android/answers/RetryManager;

    invoke-virtual {v0}, Lcom/crashlytics/android/answers/RetryManager;->reset()V

    .line 52
    const/4 v0, 0x1

    return v0

    .line 54
    :cond_1b
    iget-object v0, p0, Lcom/crashlytics/android/answers/AnswersRetryFilesSender;->retryManager:Lcom/crashlytics/android/answers/RetryManager;

    invoke-virtual {v0, v1, v2}, Lcom/crashlytics/android/answers/RetryManager;->recordRetry(J)V

    .line 55
    const/4 v0, 0x0

    return v0

    .line 58
    .end local v3    # "cleanup":Z
    :cond_22
    const/4 v0, 0x0

    return v0
.end method

.class public final Lcom/itau/empresas/feature/home/customview/FormalizacaoRemotaWarningView_;
.super Lcom/itau/empresas/feature/home/customview/FormalizacaoRemotaWarningView;
.source "FormalizacaoRemotaWarningView_.java"

# interfaces
.implements Lorg/androidannotations/api/view/HasViews;
.implements Lorg/androidannotations/api/view/OnViewChangedListener;


# instance fields
.field private alreadyInflated_:Z

.field private final onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .param p1, "context"    # Landroid/content/Context;

    .line 39
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/home/customview/FormalizacaoRemotaWarningView;-><init>(Landroid/content/Context;)V

    .line 35
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/itau/empresas/feature/home/customview/FormalizacaoRemotaWarningView_;->alreadyInflated_:Z

    .line 36
    new-instance v0, Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-direct {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/feature/home/customview/FormalizacaoRemotaWarningView_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 40
    invoke-direct {p0}, Lcom/itau/empresas/feature/home/customview/FormalizacaoRemotaWarningView_;->init_()V

    .line 41
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .line 44
    invoke-direct {p0, p1, p2}, Lcom/itau/empresas/feature/home/customview/FormalizacaoRemotaWarningView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 35
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/itau/empresas/feature/home/customview/FormalizacaoRemotaWarningView_;->alreadyInflated_:Z

    .line 36
    new-instance v0, Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-direct {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/feature/home/customview/FormalizacaoRemotaWarningView_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 45
    invoke-direct {p0}, Lcom/itau/empresas/feature/home/customview/FormalizacaoRemotaWarningView_;->init_()V

    .line 46
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .line 49
    invoke-direct {p0, p1, p2, p3}, Lcom/itau/empresas/feature/home/customview/FormalizacaoRemotaWarningView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 35
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/itau/empresas/feature/home/customview/FormalizacaoRemotaWarningView_;->alreadyInflated_:Z

    .line 36
    new-instance v0, Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-direct {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/feature/home/customview/FormalizacaoRemotaWarningView_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 50
    invoke-direct {p0}, Lcom/itau/empresas/feature/home/customview/FormalizacaoRemotaWarningView_;->init_()V

    .line 51
    return-void
.end method

.method private init_()V
    .registers 4

    .line 81
    iget-object v0, p0, Lcom/itau/empresas/feature/home/customview/FormalizacaoRemotaWarningView_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-static {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    move-result-object v2

    .line 82
    .local v2, "previousNotifier":Lorg/androidannotations/api/view/OnViewChangedNotifier;
    invoke-static {p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->registerOnViewChangedListener(Lorg/androidannotations/api/view/OnViewChangedListener;)V

    .line 83
    invoke-virtual {p0}, Lcom/itau/empresas/feature/home/customview/FormalizacaoRemotaWarningView_;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0c014e

    invoke-static {v0, v1}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v0

    iput v0, p0, Lcom/itau/empresas/feature/home/customview/FormalizacaoRemotaWarningView_;->corVerde:I

    .line 84
    invoke-virtual {p0}, Lcom/itau/empresas/feature/home/customview/FormalizacaoRemotaWarningView_;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0c0044

    invoke-static {v0, v1}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v0

    iput v0, p0, Lcom/itau/empresas/feature/home/customview/FormalizacaoRemotaWarningView_;->corCinza:I

    .line 85
    invoke-virtual {p0}, Lcom/itau/empresas/feature/home/customview/FormalizacaoRemotaWarningView_;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0c00cd

    invoke-static {v0, v1}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v0

    iput v0, p0, Lcom/itau/empresas/feature/home/customview/FormalizacaoRemotaWarningView_;->corLaranja:I

    .line 86
    invoke-static {}, Lcom/itau/empresas/CustomApplication_;->getInstance()Lcom/itau/empresas/CustomApplication;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/home/customview/FormalizacaoRemotaWarningView_;->application:Lcom/itau/empresas/CustomApplication;

    .line 87
    invoke-virtual {p0}, Lcom/itau/empresas/feature/home/customview/FormalizacaoRemotaWarningView_;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/itau/empresas/controller/MenuUniversalController_;->getInstance_(Landroid/content/Context;)Lcom/itau/empresas/controller/MenuUniversalController_;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/home/customview/FormalizacaoRemotaWarningView_;->controller:Lcom/itau/empresas/controller/MenuUniversalController;

    .line 88
    invoke-static {v2}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 89
    return-void
.end method


# virtual methods
.method public onFinishInflate()V
    .registers 3

    .line 72
    iget-boolean v0, p0, Lcom/itau/empresas/feature/home/customview/FormalizacaoRemotaWarningView_;->alreadyInflated_:Z

    if-nez v0, :cond_16

    .line 73
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/itau/empresas/feature/home/customview/FormalizacaoRemotaWarningView_;->alreadyInflated_:Z

    .line 74
    invoke-virtual {p0}, Lcom/itau/empresas/feature/home/customview/FormalizacaoRemotaWarningView_;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f030189

    invoke-static {v0, v1, p0}, Lcom/itau/empresas/feature/home/customview/FormalizacaoRemotaWarningView_;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 75
    iget-object v0, p0, Lcom/itau/empresas/feature/home/customview/FormalizacaoRemotaWarningView_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 77
    :cond_16
    invoke-super {p0}, Lcom/itau/empresas/feature/home/customview/FormalizacaoRemotaWarningView;->onFinishInflate()V

    .line 78
    return-void
.end method

.method public onViewChanged(Lorg/androidannotations/api/view/HasViews;)V
    .registers 3
    .param p1, "hasViews"    # Lorg/androidannotations/api/view/HasViews;

    .line 111
    const v0, 0x7f0e069d

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/itau/empresas/feature/home/customview/FormalizacaoRemotaWarningView_;->llViewWarning:Landroid/widget/LinearLayout;

    .line 112
    const v0, 0x7f0e05c7

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/itau/empresas/feature/home/customview/FormalizacaoRemotaWarningView_;->imgWarning:Landroid/widget/ImageView;

    .line 113
    const v0, 0x7f0e05c8

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/home/customview/FormalizacaoRemotaWarningView_;->textoWarning:Landroid/widget/TextView;

    .line 114
    invoke-virtual {p0}, Lcom/itau/empresas/feature/home/customview/FormalizacaoRemotaWarningView_;->afterViews()V

    .line 115
    return-void
.end method

.class public final Lcom/itau/empresas/feature/atendimento/fragment/AtendimentoFragment_;
.super Lcom/itau/empresas/feature/atendimento/fragment/AtendimentoFragment;
.source "AtendimentoFragment_.java"

# interfaces
.implements Lorg/androidannotations/api/view/HasViews;
.implements Lorg/androidannotations/api/view/OnViewChangedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/itau/empresas/feature/atendimento/fragment/AtendimentoFragment_$FragmentBuilder_;
    }
.end annotation


# instance fields
.field private contentView_:Landroid/view/View;

.field private final onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;


# direct methods
.method public constructor <init>()V
    .registers 2

    .line 28
    invoke-direct {p0}, Lcom/itau/empresas/feature/atendimento/fragment/AtendimentoFragment;-><init>()V

    .line 32
    new-instance v0, Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-direct {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/feature/atendimento/fragment/AtendimentoFragment_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    return-void
.end method

.method public static builder()Lcom/itau/empresas/feature/atendimento/fragment/AtendimentoFragment_$FragmentBuilder_;
    .registers 1

    .line 85
    new-instance v0, Lcom/itau/empresas/feature/atendimento/fragment/AtendimentoFragment_$FragmentBuilder_;

    invoke-direct {v0}, Lcom/itau/empresas/feature/atendimento/fragment/AtendimentoFragment_$FragmentBuilder_;-><init>()V

    return-object v0
.end method

.method private init_(Landroid/os/Bundle;)V
    .registers 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 71
    invoke-static {p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->registerOnViewChangedListener(Lorg/androidannotations/api/view/OnViewChangedListener;)V

    .line 72
    invoke-static {}, Lcom/itau/empresas/CustomApplication_;->getInstance()Lcom/itau/empresas/CustomApplication;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/atendimento/fragment/AtendimentoFragment_;->application:Lcom/itau/empresas/CustomApplication;

    .line 73
    invoke-static {}, Lcom/itau/empresas/CustomApplication_;->getInstance()Lcom/itau/empresas/CustomApplication;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/atendimento/fragment/AtendimentoFragment_;->application:Lcom/itau/empresas/CustomApplication;

    .line 74
    invoke-virtual {p0}, Lcom/itau/empresas/feature/atendimento/fragment/AtendimentoFragment_;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/itau/empresas/feature/atendimento/controller/AtendimentoController_;->getInstance_(Landroid/content/Context;)Lcom/itau/empresas/feature/atendimento/controller/AtendimentoController_;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/atendimento/fragment/AtendimentoFragment_;->controller:Lcom/itau/empresas/feature/atendimento/controller/AtendimentoController;

    .line 75
    invoke-virtual {p0}, Lcom/itau/empresas/feature/atendimento/fragment/AtendimentoFragment_;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/itau/empresas/feature/logout/LogoutHelper_;->getInstance_(Landroid/content/Context;)Lcom/itau/empresas/feature/logout/LogoutHelper_;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/atendimento/fragment/AtendimentoFragment_;->logoutHelper:Lcom/itau/empresas/feature/logout/LogoutHelper;

    .line 76
    return-void
.end method


# virtual methods
.method public findViewById(I)Landroid/view/View;
    .registers 3
    .param p1, "id"    # I

    .line 45
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/fragment/AtendimentoFragment_;->contentView_:Landroid/view/View;

    if-nez v0, :cond_6

    .line 46
    const/4 v0, 0x0

    return-object v0

    .line 48
    :cond_6
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/fragment/AtendimentoFragment_;->contentView_:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 37
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/fragment/AtendimentoFragment_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-static {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    move-result-object v1

    .line 38
    .local v1, "previousNotifier":Lorg/androidannotations/api/view/OnViewChangedNotifier;
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/atendimento/fragment/AtendimentoFragment_;->init_(Landroid/os/Bundle;)V

    .line 39
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/atendimento/fragment/AtendimentoFragment;->onCreate(Landroid/os/Bundle;)V

    .line 40
    invoke-static {v1}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 41
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .registers 6
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .line 53
    invoke-super {p0, p1, p2, p3}, Lcom/itau/empresas/feature/atendimento/fragment/AtendimentoFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/atendimento/fragment/AtendimentoFragment_;->contentView_:Landroid/view/View;

    .line 54
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/fragment/AtendimentoFragment_;->contentView_:Landroid/view/View;

    if-nez v0, :cond_14

    .line 55
    const v0, 0x7f030097

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/atendimento/fragment/AtendimentoFragment_;->contentView_:Landroid/view/View;

    .line 57
    :cond_14
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/fragment/AtendimentoFragment_;->contentView_:Landroid/view/View;

    return-object v0
.end method

.method public onDestroyView()V
    .registers 2

    .line 62
    invoke-super {p0}, Lcom/itau/empresas/feature/atendimento/fragment/AtendimentoFragment;->onDestroyView()V

    .line 63
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/itau/empresas/feature/atendimento/fragment/AtendimentoFragment_;->contentView_:Landroid/view/View;

    .line 64
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/itau/empresas/feature/atendimento/fragment/AtendimentoFragment_;->svAtendimento:Landroid/widget/ScrollView;

    .line 65
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/itau/empresas/feature/atendimento/fragment/AtendimentoFragment_;->listaFaqs:Lcom/itau/empresas/ui/widgets/NonScrollListView;

    .line 66
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/itau/empresas/feature/atendimento/fragment/AtendimentoFragment_;->mensagemAlertaView:Lcom/itau/empresas/ui/view/MensagemAlertaView;

    .line 67
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/itau/empresas/feature/atendimento/fragment/AtendimentoFragment_;->linearCartaoAlerta:Landroid/widget/LinearLayout;

    .line 68
    return-void
.end method

.method public onViewChanged(Lorg/androidannotations/api/view/HasViews;)V
    .registers 5
    .param p1, "hasViews"    # Lorg/androidannotations/api/view/HasViews;

    .line 90
    const v0, 0x7f0e00bf

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ScrollView;

    iput-object v0, p0, Lcom/itau/empresas/feature/atendimento/fragment/AtendimentoFragment_;->svAtendimento:Landroid/widget/ScrollView;

    .line 91
    const v0, 0x7f0e00c5

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/ui/widgets/NonScrollListView;

    iput-object v0, p0, Lcom/itau/empresas/feature/atendimento/fragment/AtendimentoFragment_;->listaFaqs:Lcom/itau/empresas/ui/widgets/NonScrollListView;

    .line 92
    const v0, 0x7f0e00c2

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/ui/view/MensagemAlertaView;

    iput-object v0, p0, Lcom/itau/empresas/feature/atendimento/fragment/AtendimentoFragment_;->mensagemAlertaView:Lcom/itau/empresas/ui/view/MensagemAlertaView;

    .line 93
    const v0, 0x7f0e05c6

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/itau/empresas/feature/atendimento/fragment/AtendimentoFragment_;->linearCartaoAlerta:Landroid/widget/LinearLayout;

    .line 94
    const v0, 0x7f0e00c6

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 95
    .local v1, "view_text_principais_duvidas":Landroid/view/View;
    const v0, 0x7f0e05e3

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 97
    .local v2, "view_linear_cartao_busca":Landroid/view/View;
    if-eqz v1, :cond_44

    .line 98
    new-instance v0, Lcom/itau/empresas/feature/atendimento/fragment/AtendimentoFragment_$1;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/atendimento/fragment/AtendimentoFragment_$1;-><init>(Lcom/itau/empresas/feature/atendimento/fragment/AtendimentoFragment_;)V

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 107
    :cond_44
    if-eqz v2, :cond_4e

    .line 108
    new-instance v0, Lcom/itau/empresas/feature/atendimento/fragment/AtendimentoFragment_$2;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/atendimento/fragment/AtendimentoFragment_$2;-><init>(Lcom/itau/empresas/feature/atendimento/fragment/AtendimentoFragment_;)V

    invoke-virtual {v2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 117
    :cond_4e
    invoke-virtual {p0}, Lcom/itau/empresas/feature/atendimento/fragment/AtendimentoFragment_;->carregandoElementosDeTela()V

    .line 118
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .registers 4
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;

    .line 80
    invoke-super {p0, p1, p2}, Lcom/itau/empresas/feature/atendimento/fragment/AtendimentoFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 81
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/fragment/AtendimentoFragment_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 82
    return-void
.end method

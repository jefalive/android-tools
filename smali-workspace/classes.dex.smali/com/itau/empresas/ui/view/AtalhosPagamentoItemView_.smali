.class public final Lcom/itau/empresas/ui/view/AtalhosPagamentoItemView_;
.super Lcom/itau/empresas/ui/view/AtalhosPagamentoItemView;
.source "AtalhosPagamentoItemView_.java"

# interfaces
.implements Lorg/androidannotations/api/view/HasViews;
.implements Lorg/androidannotations/api/view/OnViewChangedListener;


# instance fields
.field private alreadyInflated_:Z

.field private final onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;


# virtual methods
.method public onFinishInflate()V
    .registers 3

    .line 53
    iget-boolean v0, p0, Lcom/itau/empresas/ui/view/AtalhosPagamentoItemView_;->alreadyInflated_:Z

    if-nez v0, :cond_16

    .line 54
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/itau/empresas/ui/view/AtalhosPagamentoItemView_;->alreadyInflated_:Z

    .line 55
    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/AtalhosPagamentoItemView_;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0300da

    invoke-static {v0, v1, p0}, Lcom/itau/empresas/ui/view/AtalhosPagamentoItemView_;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 56
    iget-object v0, p0, Lcom/itau/empresas/ui/view/AtalhosPagamentoItemView_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 58
    :cond_16
    invoke-super {p0}, Lcom/itau/empresas/ui/view/AtalhosPagamentoItemView;->onFinishInflate()V

    .line 59
    return-void
.end method

.method public onViewChanged(Lorg/androidannotations/api/view/HasViews;)V
    .registers 4
    .param p1, "hasViews"    # Lorg/androidannotations/api/view/HasViews;

    .line 69
    const v0, 0x7f0e04ea

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/ui/view/AtalhosPagamentoItemView_;->textoItemAtalhosNome:Landroid/widget/TextView;

    .line 70
    const v0, 0x7f0e04e8

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/ui/view/AtalhosPagamentoItemView_;->itemAtalhosDivisor:Landroid/view/View;

    .line 71
    const v0, 0x7f0e04e7

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 73
    .local v1, "view_ll_item_atalhos_pagamento":Landroid/view/View;
    if-eqz v1, :cond_25

    .line 74
    new-instance v0, Lcom/itau/empresas/ui/view/AtalhosPagamentoItemView_$1;

    invoke-direct {v0, p0}, Lcom/itau/empresas/ui/view/AtalhosPagamentoItemView_$1;-><init>(Lcom/itau/empresas/ui/view/AtalhosPagamentoItemView_;)V

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 83
    :cond_25
    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/AtalhosPagamentoItemView_;->inicializarViews()V

    .line 84
    return-void
.end method

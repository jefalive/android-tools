.class public Lbr/com/itau/sdk/android/core/endpoint/a/a;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:J

.field private b:J

.field private c:Ljava/lang/String;

.field private d:I

.field private e:Lokhttp3/Headers;


# direct methods
.method public constructor <init>(JJLjava/lang/String;ILokhttp3/Headers;)V
    .registers 8

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-wide p1, p0, Lbr/com/itau/sdk/android/core/endpoint/a/a;->a:J

    .line 21
    iput-wide p3, p0, Lbr/com/itau/sdk/android/core/endpoint/a/a;->b:J

    .line 22
    iput-object p5, p0, Lbr/com/itau/sdk/android/core/endpoint/a/a;->c:Ljava/lang/String;

    .line 23
    iput p6, p0, Lbr/com/itau/sdk/android/core/endpoint/a/a;->d:I

    .line 24
    iput-object p7, p0, Lbr/com/itau/sdk/android/core/endpoint/a/a;->e:Lokhttp3/Headers;

    .line 25
    invoke-virtual {p0, p1, p2}, Lbr/com/itau/sdk/android/core/endpoint/a/a;->a(J)V

    .line 26
    return-void
.end method


# virtual methods
.method public a()J
    .registers 3

    .line 29
    iget-wide v0, p0, Lbr/com/itau/sdk/android/core/endpoint/a/a;->a:J

    return-wide v0
.end method

.method public a(J)V
    .registers 5

    .line 49
    const-wide/32 v0, 0x493e0

    cmp-long v0, v0, p1

    if-gez v0, :cond_c

    .line 50
    const-wide/32 v0, 0x493e0

    iput-wide v0, p0, Lbr/com/itau/sdk/android/core/endpoint/a/a;->a:J

    .line 51
    :cond_c
    return-void
.end method

.method public b()Ljava/lang/String;
    .registers 2

    .line 33
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/endpoint/a/a;->c:Ljava/lang/String;

    return-object v0
.end method

.method public c()J
    .registers 3

    .line 37
    iget-wide v0, p0, Lbr/com/itau/sdk/android/core/endpoint/a/a;->b:J

    return-wide v0
.end method

.method public d()I
    .registers 2

    .line 41
    iget v0, p0, Lbr/com/itau/sdk/android/core/endpoint/a/a;->d:I

    return v0
.end method

.method public e()Lokhttp3/Headers;
    .registers 2

    .line 45
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/endpoint/a/a;->e:Lokhttp3/Headers;

    return-object v0
.end method

.class public Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;
.super Lbr/com/itau/widgets/graphicalfriedcake/charts/components/ComponentBase;
.source "Legend.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendDirection;,
        Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendForm;,
        Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;
    }
.end annotation


# instance fields
.field private mCalculatedLabelBreakPoints:[Ljava/lang/Boolean;

.field private mCalculatedLabelSizes:[Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/FSize;

.field private mCalculatedLineSizes:[Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/FSize;

.field private mColors:[I

.field private mDirection:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendDirection;

.field private mExtraColors:[I

.field private mExtraLabels:[Ljava/lang/String;

.field private mFormSize:F

.field private mFormToTextSpace:F

.field private mIsLegendCustom:Z

.field private mLabels:[Ljava/lang/String;

.field private mMaxSizePercent:F

.field public mNeededHeight:F

.field public mNeededWidth:F

.field private mPosition:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;

.field private mShape:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendForm;

.field private mStackSpace:F

.field public mTextHeightMax:F

.field public mTextWidthMax:F

.field private mWordWrapEnabled:Z

.field private mXEntrySpace:F

.field private mYEntrySpace:F


# direct methods
.method public constructor <init>()V
    .registers 2

    .line 100
    invoke-direct {p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/ComponentBase;-><init>()V

    .line 62
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->mIsLegendCustom:Z

    .line 65
    sget-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;->BELOW_CHART_LEFT:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;

    iput-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->mPosition:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;

    .line 68
    sget-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendDirection;->LEFT_TO_RIGHT:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendDirection;

    iput-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->mDirection:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendDirection;

    .line 71
    sget-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendForm;->SQUARE:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendForm;

    iput-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->mShape:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendForm;

    .line 74
    const/high16 v0, 0x41000000    # 8.0f

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->mFormSize:F

    .line 79
    const/high16 v0, 0x40c00000    # 6.0f

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->mXEntrySpace:F

    .line 84
    const/4 v0, 0x0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->mYEntrySpace:F

    .line 91
    const/high16 v0, 0x40a00000    # 5.0f

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->mFormToTextSpace:F

    .line 94
    const/high16 v0, 0x40400000    # 3.0f

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->mStackSpace:F

    .line 97
    const v0, 0x3f733333    # 0.95f

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->mMaxSizePercent:F

    .line 553
    const/4 v0, 0x0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->mNeededWidth:F

    .line 556
    const/4 v0, 0x0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->mNeededHeight:F

    .line 558
    const/4 v0, 0x0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->mTextHeightMax:F

    .line 560
    const/4 v0, 0x0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->mTextWidthMax:F

    .line 563
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->mWordWrapEnabled:Z

    .line 616
    const/4 v0, 0x0

    new-array v0, v0, [Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/FSize;

    iput-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->mCalculatedLabelSizes:[Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/FSize;

    .line 617
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Boolean;

    iput-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->mCalculatedLabelBreakPoints:[Ljava/lang/Boolean;

    .line 618
    const/4 v0, 0x0

    new-array v0, v0, [Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/FSize;

    iput-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->mCalculatedLineSizes:[Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/FSize;

    .line 102
    const/high16 v0, 0x41000000    # 8.0f

    invoke-static {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Utils;->convertDpToPixel(F)F

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->mFormSize:F

    .line 103
    const/high16 v0, 0x40c00000    # 6.0f

    invoke-static {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Utils;->convertDpToPixel(F)F

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->mXEntrySpace:F

    .line 104
    const/4 v0, 0x0

    invoke-static {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Utils;->convertDpToPixel(F)F

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->mYEntrySpace:F

    .line 105
    const/high16 v0, 0x40a00000    # 5.0f

    invoke-static {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Utils;->convertDpToPixel(F)F

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->mFormToTextSpace:F

    .line 106
    const/high16 v0, 0x41200000    # 10.0f

    invoke-static {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Utils;->convertDpToPixel(F)F

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->mTextSize:F

    .line 107
    const/high16 v0, 0x40400000    # 3.0f

    invoke-static {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Utils;->convertDpToPixel(F)F

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->mStackSpace:F

    .line 108
    const/high16 v0, 0x40a00000    # 5.0f

    invoke-static {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Utils;->convertDpToPixel(F)F

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->mXOffset:F

    .line 109
    const/high16 v0, 0x40e00000    # 7.0f

    invoke-static {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Utils;->convertDpToPixel(F)F

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->mYOffset:F

    .line 110
    return-void
.end method

.method public constructor <init>(Ljava/util/List;Ljava/util/List;)V
    .registers 5
    .param p1, "colors"    # Ljava/util/List;
    .param p2, "labels"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<Ljava/lang/Integer;>;Ljava/util/List<Ljava/lang/String;>;)V"
        }
    .end annotation

    .line 141
    invoke-direct {p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;-><init>()V

    .line 143
    if-eqz p1, :cond_7

    if-nez p2, :cond_f

    .line 144
    :cond_7
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "colors array or labels array is NULL"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 147
    :cond_f
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v1

    if-eq v0, v1, :cond_21

    .line 148
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "colors array and labels array need to be of same size"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 152
    :cond_21
    invoke-static {p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Utils;->convertIntegers(Ljava/util/List;)[I

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->mColors:[I

    .line 153
    invoke-static {p2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Utils;->convertStrings(Ljava/util/List;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->mLabels:[Ljava/lang/String;

    .line 154
    return-void
.end method

.method public constructor <init>([I[Ljava/lang/String;)V
    .registers 5
    .param p1, "colors"    # [I
    .param p2, "labels"    # [Ljava/lang/String;

    .line 119
    invoke-direct {p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;-><init>()V

    .line 121
    if-eqz p1, :cond_7

    if-nez p2, :cond_f

    .line 122
    :cond_7
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "colors array or labels array is NULL"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 125
    :cond_f
    array-length v0, p1

    array-length v1, p2

    if-eq v0, v1, :cond_1b

    .line 126
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "colors array and labels array need to be of same size"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 130
    :cond_1b
    iput-object p1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->mColors:[I

    .line 131
    iput-object p2, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->mLabels:[Ljava/lang/String;

    .line 132
    return-void
.end method


# virtual methods
.method public calculateDimensions(Landroid/graphics/Paint;Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;)V
    .registers 20
    .param p1, "labelpaint"    # Landroid/graphics/Paint;
    .param p2, "viewPortHandler"    # Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;

    .line 641
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->mPosition:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;

    sget-object v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;->RIGHT_OF_CHART:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;

    if-eq v0, v1, :cond_28

    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->mPosition:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;

    sget-object v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;->RIGHT_OF_CHART_CENTER:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;

    if-eq v0, v1, :cond_28

    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->mPosition:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;

    sget-object v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;->LEFT_OF_CHART:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;

    if-eq v0, v1, :cond_28

    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->mPosition:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;

    sget-object v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;->LEFT_OF_CHART_CENTER:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;

    if-eq v0, v1, :cond_28

    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->mPosition:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;

    sget-object v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;->PIECHART_CENTER:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;

    if-ne v0, v1, :cond_56

    .line 646
    :cond_28
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->getMaximumEntryWidth(Landroid/graphics/Paint;)F

    move-result v0

    move-object/from16 v1, p0

    iput v0, v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->mNeededWidth:F

    .line 647
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->getFullHeight(Landroid/graphics/Paint;)F

    move-result v0

    move-object/from16 v1, p0

    iput v0, v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->mNeededHeight:F

    .line 648
    move-object/from16 v0, p0

    iget v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->mNeededWidth:F

    move-object/from16 v1, p0

    iput v0, v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->mTextWidthMax:F

    .line 649
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->getMaximumEntryHeight(Landroid/graphics/Paint;)F

    move-result v0

    move-object/from16 v1, p0

    iput v0, v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->mTextHeightMax:F

    goto/16 :goto_202

    .line 651
    :cond_56
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->mPosition:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;

    sget-object v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;->BELOW_CHART_LEFT:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;

    if-eq v0, v1, :cond_6e

    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->mPosition:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;

    sget-object v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;->BELOW_CHART_RIGHT:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;

    if-eq v0, v1, :cond_6e

    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->mPosition:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;

    sget-object v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;->BELOW_CHART_CENTER:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;

    if-ne v0, v1, :cond_1d6

    .line 655
    :cond_6e
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->mLabels:[Ljava/lang/String;

    array-length v3, v0

    .line 656
    .local v3, "labelCount":I
    invoke-static/range {p1 .. p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Utils;->getLineHeight(Landroid/graphics/Paint;)F

    move-result v4

    .line 657
    .local v4, "labelLineHeight":F
    invoke-static/range {p1 .. p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Utils;->getLineSpacing(Landroid/graphics/Paint;)F

    move-result v0

    move-object/from16 v1, p0

    iget v1, v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->mYEntrySpace:F

    add-float v5, v0, v1

    .line 658
    .local v5, "labelLineSpacing":F
    invoke-virtual/range {p2 .. p2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->contentWidth()F

    move-result v6

    .line 661
    .local v6, "contentWidth":F
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 662
    .local v7, "calculatedLabelSizes":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/FSize;>;"
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 663
    .local v8, "calculatedLabelBreakPoints":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Boolean;>;"
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 666
    .local v9, "calculatedLineSizes":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/FSize;>;"
    const/4 v10, 0x0

    .line 667
    .local v10, "maxLineWidth":F
    const/4 v11, 0x0

    .line 668
    .local v11, "currentLineWidth":F
    const/4 v12, 0x0

    .line 669
    .local v12, "requiredWidth":F
    const/4 v13, -0x1

    .line 671
    .local v13, "stackedStartIndex":I
    const/4 v14, 0x0

    .local v14, "i":I
    :goto_99
    if-ge v14, v3, :cond_16b

    .line 673
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->mColors:[I

    aget v0, v0, v14

    const/4 v1, -0x2

    if-eq v0, v1, :cond_a6

    const/4 v15, 0x1

    goto :goto_a7

    :cond_a6
    const/4 v15, 0x0

    .line 675
    .local v15, "drawingForm":Z
    :goto_a7
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 677
    const/4 v0, -0x1

    if-ne v13, v0, :cond_b4

    .line 681
    const/4 v12, 0x0

    goto :goto_b9

    .line 684
    :cond_b4
    move-object/from16 v0, p0

    iget v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->mStackSpace:F

    add-float/2addr v12, v0

    .line 688
    :goto_b9
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->mLabels:[Ljava/lang/String;

    aget-object v0, v0, v14

    if-eqz v0, :cond_e8

    .line 690
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->mLabels:[Ljava/lang/String;

    aget-object v0, v0, v14

    move-object/from16 v1, p1

    invoke-static {v1, v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Utils;->calcTextSize(Landroid/graphics/Paint;Ljava/lang/String;)Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/FSize;

    move-result-object v0

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 691
    if-eqz v15, :cond_dc

    move-object/from16 v0, p0

    iget v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->mFormToTextSpace:F

    move-object/from16 v1, p0

    iget v1, v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->mFormSize:F

    add-float/2addr v0, v1

    goto :goto_dd

    :cond_dc
    const/4 v0, 0x0

    :goto_dd
    add-float/2addr v12, v0

    .line 692
    invoke-virtual {v7, v14}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/FSize;

    iget v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/FSize;->width:F

    add-float/2addr v12, v0

    goto :goto_ff

    .line 695
    :cond_e8
    new-instance v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/FSize;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/FSize;-><init>(FF)V

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 696
    if-eqz v15, :cond_f9

    move-object/from16 v0, p0

    iget v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->mFormSize:F

    goto :goto_fa

    :cond_f9
    const/4 v0, 0x0

    :goto_fa
    add-float/2addr v12, v0

    .line 698
    const/4 v0, -0x1

    if-ne v13, v0, :cond_ff

    .line 700
    move v13, v14

    .line 704
    :cond_ff
    :goto_ff
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->mLabels:[Ljava/lang/String;

    aget-object v0, v0, v14

    if-nez v0, :cond_10b

    add-int/lit8 v0, v3, -0x1

    if-ne v14, v0, :cond_15b

    .line 706
    :cond_10b
    const/4 v0, 0x0

    cmpl-float v0, v11, v0

    if-nez v0, :cond_113

    const/16 v16, 0x0

    goto :goto_119

    :cond_113
    move-object/from16 v0, p0

    iget v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->mXEntrySpace:F

    move/from16 v16, v0

    .line 708
    .local v16, "requiredSpacing":F
    :goto_119
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->mWordWrapEnabled:Z

    if-eqz v0, :cond_12c

    const/4 v0, 0x0

    cmpl-float v0, v11, v0

    if-eqz v0, :cond_12c

    sub-float v0, v6, v11

    add-float v1, v16, v12

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_130

    .line 716
    :cond_12c
    add-float v0, v16, v12

    add-float/2addr v11, v0

    goto :goto_14b

    .line 721
    :cond_130
    new-instance v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/FSize;

    invoke-direct {v0, v11, v4}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/FSize;-><init>(FF)V

    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 722
    invoke-static {v10, v11}, Ljava/lang/Math;->max(FF)F

    move-result v10

    .line 725
    const/4 v0, -0x1

    if-le v13, v0, :cond_141

    move v0, v13

    goto :goto_142

    :cond_141
    move v0, v14

    .line 726
    :goto_142
    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    .line 725
    invoke-virtual {v8, v0, v1}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 727
    move v11, v12

    .line 730
    :goto_14b
    add-int/lit8 v0, v3, -0x1

    if-ne v14, v0, :cond_15b

    .line 732
    new-instance v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/FSize;

    invoke-direct {v0, v11, v4}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/FSize;-><init>(FF)V

    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 733
    invoke-static {v10, v11}, Ljava/lang/Math;->max(FF)F

    move-result v10

    .line 737
    .end local v16    # "requiredSpacing":F
    :cond_15b
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->mLabels:[Ljava/lang/String;

    aget-object v0, v0, v14

    if-eqz v0, :cond_165

    const/4 v0, -0x1

    goto :goto_166

    :cond_165
    move v0, v13

    :goto_166
    move v13, v0

    .line 671
    .end local v15    # "drawingForm":Z
    add-int/lit8 v14, v14, 0x1

    goto/16 :goto_99

    .line 740
    .line 741
    .end local v14    # "i":I
    :cond_16b
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/FSize;

    .line 740
    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/FSize;

    move-object/from16 v1, p0

    iput-object v0, v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->mCalculatedLabelSizes:[Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/FSize;

    .line 742
    .line 743
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/Boolean;

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Boolean;

    move-object/from16 v1, p0

    iput-object v0, v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->mCalculatedLabelBreakPoints:[Ljava/lang/Boolean;

    .line 744
    .line 745
    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/FSize;

    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/FSize;

    move-object/from16 v1, p0

    iput-object v0, v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->mCalculatedLineSizes:[Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/FSize;

    .line 747
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->getMaximumEntryWidth(Landroid/graphics/Paint;)F

    move-result v0

    move-object/from16 v1, p0

    iput v0, v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->mTextWidthMax:F

    .line 748
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->getMaximumEntryHeight(Landroid/graphics/Paint;)F

    move-result v0

    move-object/from16 v1, p0

    iput v0, v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->mTextHeightMax:F

    .line 749
    move-object/from16 v0, p0

    iput v10, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->mNeededWidth:F

    .line 750
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->mCalculatedLineSizes:[Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/FSize;

    array-length v0, v0

    int-to-float v0, v0

    mul-float/2addr v0, v4

    move-object/from16 v1, p0

    iget-object v1, v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->mCalculatedLineSizes:[Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/FSize;

    array-length v1, v1

    if-nez v1, :cond_1c7

    const/4 v1, 0x0

    goto :goto_1ce

    :cond_1c7
    move-object/from16 v1, p0

    iget-object v1, v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->mCalculatedLineSizes:[Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/FSize;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    :goto_1ce
    int-to-float v1, v1

    mul-float/2addr v1, v5

    add-float/2addr v0, v1

    move-object/from16 v1, p0

    iput v0, v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->mNeededHeight:F

    .line 757
    .end local v3    # "labelCount":I
    .end local v4    # "labelLineHeight":F
    .end local v5    # "labelLineSpacing":F
    .end local v6    # "contentWidth":F
    .end local v7    # "calculatedLabelSizes":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/FSize;>;"
    .end local v7
    .end local v8    # "calculatedLabelBreakPoints":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Boolean;>;"
    .end local v8
    .end local v9    # "calculatedLineSizes":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/FSize;>;"
    .end local v9
    .end local v10    # "maxLineWidth":F
    .end local v11    # "currentLineWidth":F
    .end local v12    # "requiredWidth":F
    .end local v13    # "stackedStartIndex":I
    goto :goto_202

    .line 760
    :cond_1d6
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->getFullWidth(Landroid/graphics/Paint;)F

    move-result v0

    move-object/from16 v1, p0

    iput v0, v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->mNeededWidth:F

    .line 761
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->getMaximumEntryHeight(Landroid/graphics/Paint;)F

    move-result v0

    move-object/from16 v1, p0

    iput v0, v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->mNeededHeight:F

    .line 762
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->getMaximumEntryWidth(Landroid/graphics/Paint;)F

    move-result v0

    move-object/from16 v1, p0

    iput v0, v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->mTextWidthMax:F

    .line 763
    move-object/from16 v0, p0

    iget v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->mNeededHeight:F

    move-object/from16 v1, p0

    iput v0, v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->mTextHeightMax:F

    .line 765
    :goto_202
    return-void
.end method

.method public getCalculatedLabelBreakPoints()[Ljava/lang/Boolean;
    .registers 2

    .line 625
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->mCalculatedLabelBreakPoints:[Ljava/lang/Boolean;

    return-object v0
.end method

.method public getCalculatedLabelSizes()[Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/FSize;
    .registers 2

    .line 621
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->mCalculatedLabelSizes:[Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/FSize;

    return-object v0
.end method

.method public getCalculatedLineSizes()[Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/FSize;
    .registers 2

    .line 629
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->mCalculatedLineSizes:[Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/FSize;

    return-object v0
.end method

.method public getColors()[I
    .registers 2

    .line 227
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->mColors:[I

    return-object v0
.end method

.method public getDirection()Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendDirection;
    .registers 2

    .line 370
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->mDirection:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendDirection;

    return-object v0
.end method

.method public getExtraColors()[I
    .registers 2

    .line 254
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->mExtraColors:[I

    return-object v0
.end method

.method public getExtraLabels()[Ljava/lang/String;
    .registers 2

    .line 262
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->mExtraLabels:[Ljava/lang/String;

    return-object v0
.end method

.method public getForm()Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendForm;
    .registers 2

    .line 388
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->mShape:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendForm;

    return-object v0
.end method

.method public getFormSize()F
    .registers 2

    .line 416
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->mFormSize:F

    return v0
.end method

.method public getFormToTextSpace()F
    .registers 2

    .line 464
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->mFormToTextSpace:F

    return v0
.end method

.method public getFullHeight(Landroid/graphics/Paint;)F
    .registers 6
    .param p1, "labelpaint"    # Landroid/graphics/Paint;

    .line 535
    const/4 v2, 0x0

    .line 537
    .local v2, "height":F
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_2
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->mLabels:[Ljava/lang/String;

    array-length v0, v0

    if-ge v3, v0, :cond_24

    .line 540
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->mLabels:[Ljava/lang/String;

    aget-object v0, v0, v3

    if-eqz v0, :cond_21

    .line 542
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->mLabels:[Ljava/lang/String;

    aget-object v0, v0, v3

    invoke-static {p1, v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Utils;->calcTextHeight(Landroid/graphics/Paint;Ljava/lang/String;)I

    move-result v0

    int-to-float v0, v0

    add-float/2addr v2, v0

    .line 544
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->mLabels:[Ljava/lang/String;

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    if-ge v3, v0, :cond_21

    .line 545
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->mYEntrySpace:F

    add-float/2addr v2, v0

    .line 537
    :cond_21
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 549
    .end local v3    # "i":I
    :cond_24
    return v2
.end method

.method public getFullWidth(Landroid/graphics/Paint;)F
    .registers 6
    .param p1, "labelpaint"    # Landroid/graphics/Paint;

    .line 502
    const/4 v2, 0x0

    .line 504
    .local v2, "width":F
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_2
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->mLabels:[Ljava/lang/String;

    array-length v0, v0

    if-ge v3, v0, :cond_3f

    .line 507
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->mLabels:[Ljava/lang/String;

    aget-object v0, v0, v3

    if-eqz v0, :cond_2f

    .line 510
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->mColors:[I

    aget v0, v0, v3

    const/4 v1, -0x2

    if-eq v0, v1, :cond_1a

    .line 511
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->mFormSize:F

    iget v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->mFormToTextSpace:F

    add-float/2addr v0, v1

    add-float/2addr v2, v0

    .line 513
    :cond_1a
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->mLabels:[Ljava/lang/String;

    aget-object v0, v0, v3

    invoke-static {p1, v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Utils;->calcTextWidth(Landroid/graphics/Paint;Ljava/lang/String;)I

    move-result v0

    int-to-float v0, v0

    add-float/2addr v2, v0

    .line 515
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->mLabels:[Ljava/lang/String;

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    if-ge v3, v0, :cond_3c

    .line 516
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->mXEntrySpace:F

    add-float/2addr v2, v0

    goto :goto_3c

    .line 518
    :cond_2f
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->mFormSize:F

    add-float/2addr v2, v0

    .line 519
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->mLabels:[Ljava/lang/String;

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    if-ge v3, v0, :cond_3c

    .line 520
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->mStackSpace:F

    add-float/2addr v2, v0

    .line 504
    :cond_3c
    :goto_3c
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 524
    .end local v3    # "i":I
    :cond_3f
    return v2
.end method

.method public getLabel(I)Ljava/lang/String;
    .registers 3
    .param p1, "index"    # I

    .line 246
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->mLabels:[Ljava/lang/String;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public getLabels()[Ljava/lang/String;
    .registers 2

    .line 236
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->mLabels:[Ljava/lang/String;

    return-object v0
.end method

.method public getMaxSizePercent()F
    .registers 2

    .line 599
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->mMaxSizePercent:F

    return v0
.end method

.method public getMaximumEntryHeight(Landroid/graphics/Paint;)F
    .registers 6
    .param p1, "p"    # Landroid/graphics/Paint;

    .line 205
    const/4 v1, 0x0

    .line 207
    .local v1, "max":F
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_2
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->mLabels:[Ljava/lang/String;

    array-length v0, v0

    if-ge v2, v0, :cond_1e

    .line 209
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->mLabels:[Ljava/lang/String;

    aget-object v0, v0, v2

    if-eqz v0, :cond_1b

    .line 211
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->mLabels:[Ljava/lang/String;

    aget-object v0, v0, v2

    invoke-static {p1, v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Utils;->calcTextHeight(Landroid/graphics/Paint;Ljava/lang/String;)I

    move-result v0

    int-to-float v3, v0

    .line 213
    .local v3, "length":F
    cmpl-float v0, v3, v1

    if-lez v0, :cond_1b

    .line 214
    move v1, v3

    .line 207
    .end local v3    # "length":F
    :cond_1b
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 218
    .end local v2    # "i":I
    :cond_1e
    return v1
.end method

.method public getMaximumEntryWidth(Landroid/graphics/Paint;)F
    .registers 7
    .param p1, "p"    # Landroid/graphics/Paint;

    .line 181
    const/4 v2, 0x0

    .line 183
    .local v2, "max":F
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_2
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->mLabels:[Ljava/lang/String;

    array-length v0, v0

    if-ge v3, v0, :cond_1e

    .line 185
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->mLabels:[Ljava/lang/String;

    aget-object v0, v0, v3

    if-eqz v0, :cond_1b

    .line 187
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->mLabels:[Ljava/lang/String;

    aget-object v0, v0, v3

    invoke-static {p1, v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Utils;->calcTextWidth(Landroid/graphics/Paint;Ljava/lang/String;)I

    move-result v0

    int-to-float v4, v0

    .line 189
    .local v4, "length":F
    cmpl-float v0, v4, v2

    if-lez v0, :cond_1b

    .line 190
    move v2, v4

    .line 183
    .end local v4    # "length":F
    :cond_1b
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 194
    .end local v3    # "i":I
    :cond_1e
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->mFormSize:F

    add-float/2addr v0, v2

    iget v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->mFormToTextSpace:F

    add-float/2addr v0, v1

    return v0
.end method

.method public getPosition()Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;
    .registers 2

    .line 352
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->mPosition:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;

    return-object v0
.end method

.method public getStackSpace()F
    .registers 2

    .line 483
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->mStackSpace:F

    return v0
.end method

.method public getXEntrySpace()F
    .registers 2

    .line 426
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->mXEntrySpace:F

    return v0
.end method

.method public getYEntrySpace()F
    .registers 2

    .line 445
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->mYEntrySpace:F

    return v0
.end method

.method public isLegendCustom()Z
    .registers 2

    .line 343
    iget-boolean v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->mIsLegendCustom:Z

    return v0
.end method

.method public isWordWrapEnabled()Z
    .registers 2

    .line 585
    iget-boolean v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->mWordWrapEnabled:Z

    return v0
.end method

.method public resetCustom()V
    .registers 2

    .line 335
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->mIsLegendCustom:Z

    .line 336
    return-void
.end method

.method public setComputedColors(Ljava/util/List;)V
    .registers 3
    .param p1, "colors"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<Ljava/lang/Integer;>;)V"
        }
    .end annotation

    .line 161
    invoke-static {p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Utils;->convertIntegers(Ljava/util/List;)[I

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->mColors:[I

    .line 162
    return-void
.end method

.method public setComputedLabels(Ljava/util/List;)V
    .registers 3
    .param p1, "labels"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<Ljava/lang/String;>;)V"
        }
    .end annotation

    .line 169
    invoke-static {p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Utils;->convertStrings(Ljava/util/List;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->mLabels:[Ljava/lang/String;

    .line 170
    return-void
.end method

.method public setCustom(Ljava/util/List;Ljava/util/List;)V
    .registers 5
    .param p1, "colors"    # Ljava/util/List;
    .param p2, "labels"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<Ljava/lang/Integer;>;Ljava/util/List<Ljava/lang/String;>;)V"
        }
    .end annotation

    .line 319
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v1

    if-eq v0, v1, :cond_12

    .line 320
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "colors array and labels array need to be of same size"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 324
    :cond_12
    invoke-static {p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Utils;->convertIntegers(Ljava/util/List;)[I

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->mColors:[I

    .line 325
    invoke-static {p2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Utils;->convertStrings(Ljava/util/List;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->mLabels:[Ljava/lang/String;

    .line 326
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->mIsLegendCustom:Z

    .line 327
    return-void
.end method

.method public setCustom([I[Ljava/lang/String;)V
    .registers 5
    .param p1, "colors"    # [I
    .param p2, "labels"    # [Ljava/lang/String;

    .line 298
    array-length v0, p1

    array-length v1, p2

    if-eq v0, v1, :cond_c

    .line 299
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "colors array and labels array need to be of same size"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 303
    :cond_c
    iput-object p2, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->mLabels:[Ljava/lang/String;

    .line 304
    iput-object p1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->mColors:[I

    .line 305
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->mIsLegendCustom:Z

    .line 306
    return-void
.end method

.method public setDirection(Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendDirection;)V
    .registers 2
    .param p1, "pos"    # Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendDirection;

    .line 379
    iput-object p1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->mDirection:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendDirection;

    .line 380
    return-void
.end method

.method public setExtra(Ljava/util/List;Ljava/util/List;)V
    .registers 4
    .param p1, "colors"    # Ljava/util/List;
    .param p2, "labels"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<Ljava/lang/Integer;>;Ljava/util/List<Ljava/lang/String;>;)V"
        }
    .end annotation

    .line 272
    invoke-static {p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Utils;->convertIntegers(Ljava/util/List;)[I

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->mExtraColors:[I

    .line 273
    invoke-static {p2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Utils;->convertStrings(Ljava/util/List;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->mExtraLabels:[Ljava/lang/String;

    .line 274
    return-void
.end method

.method public setExtra([I[Ljava/lang/String;)V
    .registers 3
    .param p1, "colors"    # [I
    .param p2, "labels"    # [Ljava/lang/String;

    .line 283
    iput-object p1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->mExtraColors:[I

    .line 284
    iput-object p2, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->mExtraLabels:[Ljava/lang/String;

    .line 285
    return-void
.end method

.method public setForm(Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendForm;)V
    .registers 2
    .param p1, "shape"    # Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendForm;

    .line 397
    iput-object p1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->mShape:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendForm;

    .line 398
    return-void
.end method

.method public setFormSize(F)V
    .registers 3
    .param p1, "size"    # F

    .line 407
    invoke-static {p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Utils;->convertDpToPixel(F)F

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->mFormSize:F

    .line 408
    return-void
.end method

.method public setFormToTextSpace(F)V
    .registers 3
    .param p1, "space"    # F

    .line 474
    invoke-static {p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Utils;->convertDpToPixel(F)F

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->mFormToTextSpace:F

    .line 475
    return-void
.end method

.method public setMaxSizePercent(F)V
    .registers 2
    .param p1, "maxSize"    # F

    .line 613
    iput p1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->mMaxSizePercent:F

    .line 614
    return-void
.end method

.method public setPosition(Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;)V
    .registers 2
    .param p1, "pos"    # Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;

    .line 361
    iput-object p1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->mPosition:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;

    .line 362
    return-void
.end method

.method public setStackSpace(F)V
    .registers 2
    .param p1, "space"    # F

    .line 492
    iput p1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->mStackSpace:F

    .line 493
    return-void
.end method

.method public setWordWrapEnabled(Z)V
    .registers 2
    .param p1, "enabled"    # Z

    .line 575
    iput-boolean p1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->mWordWrapEnabled:Z

    .line 576
    return-void
.end method

.method public setXEntrySpace(F)V
    .registers 3
    .param p1, "space"    # F

    .line 436
    invoke-static {p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Utils;->convertDpToPixel(F)F

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->mXEntrySpace:F

    .line 437
    return-void
.end method

.method public setYEntrySpace(F)V
    .registers 3
    .param p1, "space"    # F

    .line 455
    invoke-static {p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Utils;->convertDpToPixel(F)F

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->mYEntrySpace:F

    .line 456
    return-void
.end method

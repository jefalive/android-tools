.class public Lcom/itau/empresas/feature/pagamento/comcodigobarra/model/RequestPagamentoBoletoVO;
.super Ljava/lang/Object;
.source "RequestPagamentoBoletoVO.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private cliente:Lcom/itau/empresas/api/model/DadosClienteVO;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "cliente"
    .end annotation
.end field

.field private codigoBarras:Lcom/itau/empresas/feature/pagamento/comcodigobarra/model/DadosBoletoVO;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "codigo_barras"
    .end annotation
.end field

.field private pagamento:Lcom/itau/empresas/api/model/DadosPagamentoVO;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "pagamento"
    .end annotation
.end field

.field private pessoaJuridica:Lcom/itau/empresas/api/model/PessoaJuridicaVO;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "pessoa_juridica"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.class Lcom/itau/empresas/LogoutActivityListener;
.super Ljava/lang/Object;
.source "LogoutActivityListener.java"

# interfaces
.implements Landroid/app/Application$ActivityLifecycleCallbacks;


# static fields
.field private static final ACTIVITIES_IGNORADAS:[Ljava/lang/Class;


# instance fields
.field controller:Lcom/itau/empresas/feature/login/controller/LoginController;

.field customApplication:Lcom/itau/empresas/CustomApplication;

.field logoutHelper:Lcom/itau/empresas/feature/logout/LogoutHelper;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .line 35
    const/16 v0, 0xb

    new-array v0, v0, [Ljava/lang/Class;

    const-class v1, Lcom/itau/empresas/feature/login/LoginActivity;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const-class v1, Lcom/itau/empresas/feature/itoken/MostraItokenActivity;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    const-class v1, Lcom/itau/empresas/feature/login/PreLoginActivity;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    const-class v1, Lcom/itau/empresas/feature/mapa/MapaActivity;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    const-class v1, Lcom/itau/empresas/ui/activity/GenericaActivity;

    const/4 v2, 0x4

    aput-object v1, v0, v2

    const-class v1, Lcom/itau/empresas/ui/activity/ContatosActivity;

    const/4 v2, 0x5

    aput-object v1, v0, v2

    const-class v1, Lcom/itau/empresas/ui/activity/BetaActivity;

    const/4 v2, 0x6

    aput-object v1, v0, v2

    const-class v1, Lcom/itau/empresas/ui/activity/TermosDeUsoActivity;

    const/4 v2, 0x7

    aput-object v1, v0, v2

    const-class v1, Lcom/itau/empresas/ui/activity/SegurancaActivity;

    const/16 v2, 0x8

    aput-object v1, v0, v2

    const-class v1, Lcom/itau/empresas/ui/activity/AplicativosActivity;

    const/16 v2, 0x9

    aput-object v1, v0, v2

    const-class v1, Lcom/itau/empresas/feature/whatsnew/WhatsNewIntroducaoActivity;

    const/16 v2, 0xa

    aput-object v1, v0, v2

    sput-object v0, Lcom/itau/empresas/LogoutActivityListener;->ACTIVITIES_IGNORADAS:[Ljava/lang/Class;

    return-void
.end method

.method constructor <init>()V
    .registers 1

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static deveIgnorarTratamento(Landroid/app/Activity;)Z
    .registers 2
    .param p0, "activity"    # Landroid/app/Activity;

    .line 95
    if-nez p0, :cond_4

    .line 96
    const/4 v0, 0x1

    return v0

    .line 97
    :cond_4
    sget-object v0, Lcom/itau/empresas/LogoutActivityListener;->ACTIVITIES_IGNORADAS:[Ljava/lang/Class;

    invoke-static {p0, v0}, Lcom/itau/empresas/LogoutActivityListener;->isInstanceOf(Ljava/lang/Object;[Ljava/lang/Class;)Z

    move-result v0

    return v0
.end method

.method private static varargs isInstanceOf(Ljava/lang/Object;[Ljava/lang/Class;)Z
    .registers 7
    .param p0, "value"    # Ljava/lang/Object;
    .param p1, "list"    # [Ljava/lang/Class;

    .line 101
    move-object v1, p1

    array-length v2, v1

    const/4 v3, 0x0

    :goto_3
    if-ge v3, v2, :cond_12

    aget-object v4, v1, v3

    .line 102
    .local v4, "parent":Ljava/lang/Class;
    invoke-virtual {v4, p0}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 103
    const/4 v0, 0x1

    return v0

    .line 101
    .end local v4    # "parent":Ljava/lang/Class;
    :cond_f
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    .line 104
    :cond_12
    const/4 v0, 0x0

    return v0
.end method


# virtual methods
.method public onActivityCreated(Landroid/app/Activity;Landroid/os/Bundle;)V
    .registers 3
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;

    .line 52
    return-void
.end method

.method public onActivityDestroyed(Landroid/app/Activity;)V
    .registers 2
    .param p1, "activity"    # Landroid/app/Activity;

    .line 92
    return-void
.end method

.method public onActivityPaused(Landroid/app/Activity;)V
    .registers 2
    .param p1, "activity"    # Landroid/app/Activity;

    .line 77
    return-void
.end method

.method public onActivityResumed(Landroid/app/Activity;)V
    .registers 3
    .param p1, "activity"    # Landroid/app/Activity;

    .line 66
    invoke-static {p1}, Lcom/itau/empresas/LogoutActivityListener;->deveIgnorarTratamento(Landroid/app/Activity;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 67
    return-void

    .line 69
    :cond_7
    iget-object v0, p0, Lcom/itau/empresas/LogoutActivityListener;->customApplication:Lcom/itau/empresas/CustomApplication;

    if-eqz v0, :cond_18

    iget-object v0, p0, Lcom/itau/empresas/LogoutActivityListener;->customApplication:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getContaSelecionada()Lcom/itau/empresas/api/model/ContaOperadorVO;

    move-result-object v0

    if-nez v0, :cond_18

    .line 70
    iget-object v0, p0, Lcom/itau/empresas/LogoutActivityListener;->logoutHelper:Lcom/itau/empresas/feature/logout/LogoutHelper;

    invoke-virtual {v0, p1}, Lcom/itau/empresas/feature/logout/LogoutHelper;->trataSessaoExpirada(Landroid/app/Activity;)V

    .line 72
    :cond_18
    return-void
.end method

.method public onActivitySaveInstanceState(Landroid/app/Activity;Landroid/os/Bundle;)V
    .registers 3
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "outState"    # Landroid/os/Bundle;

    .line 87
    return-void
.end method

.method public onActivityStarted(Landroid/app/Activity;)V
    .registers 3
    .param p1, "activity"    # Landroid/app/Activity;

    .line 56
    invoke-static {p1}, Lcom/itau/empresas/LogoutActivityListener;->deveIgnorarTratamento(Landroid/app/Activity;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 57
    return-void

    .line 59
    :cond_7
    invoke-static {}, Lbr/com/itau/sdk/android/core/BackendClient;->isValidSession()Z

    move-result v0

    if-nez v0, :cond_12

    .line 60
    iget-object v0, p0, Lcom/itau/empresas/LogoutActivityListener;->logoutHelper:Lcom/itau/empresas/feature/logout/LogoutHelper;

    invoke-virtual {v0, p1}, Lcom/itau/empresas/feature/logout/LogoutHelper;->trataSessaoExpirada(Landroid/app/Activity;)V

    .line 62
    :cond_12
    return-void
.end method

.method public onActivityStopped(Landroid/app/Activity;)V
    .registers 2
    .param p1, "activity"    # Landroid/app/Activity;

    .line 82
    return-void
.end method

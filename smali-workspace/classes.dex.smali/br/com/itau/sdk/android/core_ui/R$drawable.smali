.class public final Lbr/com/itau/sdk/android/core_ui/R$drawable;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lbr/com/itau/sdk/android/core_ui/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "drawable"
.end annotation


# static fields
.field public static final abc_ab_share_pack_mtrl_alpha:I = 0x7f020000

.field public static final abc_action_bar_item_background_material:I = 0x7f020001

.field public static final abc_btn_borderless_material:I = 0x7f020002

.field public static final abc_btn_check_material:I = 0x7f020003

.field public static final abc_btn_check_to_on_mtrl_000:I = 0x7f020004

.field public static final abc_btn_check_to_on_mtrl_015:I = 0x7f020005

.field public static final abc_btn_colored_material:I = 0x7f020006

.field public static final abc_btn_default_mtrl_shape:I = 0x7f020007

.field public static final abc_btn_radio_material:I = 0x7f020008

.field public static final abc_btn_radio_to_on_mtrl_000:I = 0x7f020009

.field public static final abc_btn_radio_to_on_mtrl_015:I = 0x7f02000a

.field public static final abc_btn_switch_to_on_mtrl_00001:I = 0x7f02000b

.field public static final abc_btn_switch_to_on_mtrl_00012:I = 0x7f02000c

.field public static final abc_cab_background_internal_bg:I = 0x7f02000d

.field public static final abc_cab_background_top_material:I = 0x7f02000e

.field public static final abc_cab_background_top_mtrl_alpha:I = 0x7f02000f

.field public static final abc_control_background_material:I = 0x7f020010

.field public static final abc_edit_text_material:I = 0x7f020012

.field public static final abc_ic_commit_search_api_mtrl_alpha:I = 0x7f020016

.field public static final abc_ic_menu_copy_mtrl_am_alpha:I = 0x7f020018

.field public static final abc_ic_menu_cut_mtrl_alpha:I = 0x7f020019

.field public static final abc_ic_menu_paste_mtrl_am_alpha:I = 0x7f02001b

.field public static final abc_ic_menu_selectall_mtrl_alpha:I = 0x7f02001c

.field public static final abc_ic_menu_share_mtrl_alpha:I = 0x7f02001d

.field public static final abc_item_background_holo_dark:I = 0x7f020026

.field public static final abc_item_background_holo_light:I = 0x7f020027

.field public static final abc_list_divider_mtrl_alpha:I = 0x7f020028

.field public static final abc_list_focused_holo:I = 0x7f020029

.field public static final abc_list_longpressed_holo:I = 0x7f02002a

.field public static final abc_list_pressed_holo_dark:I = 0x7f02002b

.field public static final abc_list_pressed_holo_light:I = 0x7f02002c

.field public static final abc_list_selector_background_transition_holo_dark:I = 0x7f02002d

.field public static final abc_list_selector_background_transition_holo_light:I = 0x7f02002e

.field public static final abc_list_selector_disabled_holo_dark:I = 0x7f02002f

.field public static final abc_list_selector_disabled_holo_light:I = 0x7f020030

.field public static final abc_list_selector_holo_dark:I = 0x7f020031

.field public static final abc_list_selector_holo_light:I = 0x7f020032

.field public static final abc_menu_hardkey_panel_mtrl_mult:I = 0x7f020033

.field public static final abc_popup_background_mtrl_mult:I = 0x7f020034

.field public static final abc_scrubber_control_off_mtrl_alpha:I = 0x7f020038

.field public static final abc_scrubber_control_to_pressed_mtrl_000:I = 0x7f020039

.field public static final abc_scrubber_control_to_pressed_mtrl_005:I = 0x7f02003a

.field public static final abc_scrubber_primary_mtrl_alpha:I = 0x7f02003b

.field public static final abc_scrubber_track_mtrl_alpha:I = 0x7f02003c

.field public static final abc_seekbar_thumb_material:I = 0x7f02003d

.field public static final abc_seekbar_track_material:I = 0x7f02003f

.field public static final abc_spinner_mtrl_am_alpha:I = 0x7f020040

.field public static final abc_spinner_textfield_background_material:I = 0x7f020041

.field public static final abc_switch_thumb_material:I = 0x7f020042

.field public static final abc_switch_track_mtrl_alpha:I = 0x7f020043

.field public static final abc_tab_indicator_material:I = 0x7f020044

.field public static final abc_tab_indicator_mtrl_alpha:I = 0x7f020045

.field public static final abc_text_cursor_material:I = 0x7f020046

.field public static final abc_textfield_activated_mtrl_alpha:I = 0x7f02004d

.field public static final abc_textfield_default_mtrl_alpha:I = 0x7f02004e

.field public static final abc_textfield_search_activated_mtrl_alpha:I = 0x7f02004f

.field public static final abc_textfield_search_default_mtrl_alpha:I = 0x7f020050

.field public static final abc_textfield_search_material:I = 0x7f020051

.field public static final ic_busca:I = 0x7f020114

.field public static final ic_cadeado_itau:I = 0x7f020117

.field public static final ic_cartao_senha:I = 0x7f02011a

.field public static final ic_check_itau:I = 0x7f02011d

.field public static final ic_check_personnalite:I = 0x7f02011e

.field public static final ic_editar_itau:I = 0x7f020125

.field public static final ic_editar_personnalite:I = 0x7f020126

.field public static final ic_favorito:I = 0x7f020130

.field public static final ic_favorito_itau:I = 0x7f020132

.field public static final ic_favorito_personnalite:I = 0x7f020133

.field public static final ic_interrogacao:I = 0x7f020141

.field public static final ic_menu_hamburguer:I = 0x7f02014d

.field public static final ic_menu_hamburguer_personnalite:I = 0x7f02014e

.field public static final ic_picker_seta_nao_selecionado:I = 0x7f020156

.field public static final ic_picker_seta_selecionado:I = 0x7f020157

.field public static final ic_seta_baixo:I = 0x7f02015f

.field public static final ic_seta_cima:I = 0x7f020161

.field public static final ic_seta_esquerda_itau:I = 0x7f020164

.field public static final ic_seta_esquerda_personnalite:I = 0x7f020166

.field public static final ic_token_aplicativo_itau:I = 0x7f02016a

.field public static final ic_token_aplicativo_personnalite:I = 0x7f02016b

.field public static final ic_token_cartoes:I = 0x7f02016c

.field public static final ic_token_chaveiro_itau:I = 0x7f02016e

.field public static final ic_token_chaveiro_personnalite:I = 0x7f02016f

.field public static final ic_token_ok:I = 0x7f020170

.field public static final ic_token_reenviar:I = 0x7f020171

.field public static final ic_token_sms_itau:I = 0x7f020175

.field public static final ic_token_sms_personnalite:I = 0x7f020176

.field public static final ic_transferencias_itau:I = 0x7f020178

.field public static final ic_transferencias_personnalite:I = 0x7f020179

.field public static final img_avatar_confirmacao:I = 0x7f020180

.field public static final img_avatar_favoritos:I = 0x7f020181

.field public static final img_avatar_iniciais:I = 0x7f020182

.field public static final img_avatar_iniciais_itau:I = 0x7f020183

.field public static final img_avatar_iniciais_personnalite:I = 0x7f020184

.field public static final img_avatar_itau:I = 0x7f020185

.field public static final img_avatar_lista:I = 0x7f020186

.field public static final itausdkcore_dialog_new_password_defaulbackground:I = 0x7f020191

.field public static final itausdkcore_ic_fechar:I = 0x7f020192

.field public static final itausdkcore_ic_finger_print:I = 0x7f020193

.field public static final itausdkcore_ic_finger_print_branco:I = 0x7f020194

.field public static final itausdkcore_ic_pass_empty:I = 0x7f020197

.field public static final itausdkcore_ic_pass_filled:I = 0x7f020198

.field public static final itausdkcore_ic_senha_branca:I = 0x7f020199

.field public static final itausdkcore_ic_seta:I = 0x7f02019a

.field public static final itausdkcore_ic_seta_branca:I = 0x7f02019b

.field public static final itausdkcore_ic_seta_cima:I = 0x7f02019c

.field public static final itausdkcore_token_button_background:I = 0x7f02019d

.field public static final itausdkcore_token_button_background_light:I = 0x7f02019e

.field public static final itausdkcore_token_number_background:I = 0x7f02019f

.field public static final itausdkcore_token_secondary_action_background_selector:I = 0x7f0201a0

.field public static final notification_template_icon_bg:I = 0x7f020204


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 416
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

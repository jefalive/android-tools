.class public Lcom/itau/empresas/ui/activity/PDFActivity;
.super Lcom/itau/empresas/ui/activity/BaseActivity;
.source "PDFActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/itau/empresas/ui/activity/PDFActivity$PdfActivityClient;
    }
.end annotation


# instance fields
.field url:Ljava/lang/String;

.field webView:Landroid/webkit/WebView;


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 22
    invoke-direct {p0}, Lcom/itau/empresas/ui/activity/BaseActivity;-><init>()V

    return-void
.end method

.method private configuraWebView()V
    .registers 4

    .line 43
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/PDFActivity;->webView:Landroid/webkit/WebView;

    new-instance v1, Lcom/itau/empresas/ui/activity/PDFActivity$PdfActivityClient;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/itau/empresas/ui/activity/PDFActivity$PdfActivityClient;-><init>(Lcom/itau/empresas/ui/activity/PDFActivity;Lcom/itau/empresas/ui/activity/PDFActivity$1;)V

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 44
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/PDFActivity;->webView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 45
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/PDFActivity;->webView:Landroid/webkit/WebView;

    invoke-direct {p0}, Lcom/itau/empresas/ui/activity/PDFActivity;->montarURL()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 46
    return-void
.end method

.method public static intent(Landroid/content/Context;)Lcom/itau/empresas/ui/activity/PDFActivity_$IntentBuilder_;
    .registers 2
    .param p0, "context"    # Landroid/content/Context;

    .line 63
    new-instance v0, Lcom/itau/empresas/ui/activity/PDFActivity_$IntentBuilder_;

    invoke-direct {v0, p0}, Lcom/itau/empresas/ui/activity/PDFActivity_$IntentBuilder_;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method private montarURL()Ljava/lang/String;
    .registers 3

    .line 49
    invoke-virtual {p0}, Lcom/itau/empresas/ui/activity/PDFActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0704ac

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/ui/activity/PDFActivity;->url:Ljava/lang/String;

    .line 50
    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 49
    return-object v0
.end method


# virtual methods
.method aoCarregarTela()V
    .registers 1

    .line 38
    invoke-virtual {p0}, Lcom/itau/empresas/ui/activity/PDFActivity;->mostraDialogoDeProgresso()V

    .line 39
    invoke-direct {p0}, Lcom/itau/empresas/ui/activity/PDFActivity;->configuraWebView()V

    .line 40
    return-void
.end method

.method public opkeysConsumidas()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Ljava/lang/String;>;"
        }
    .end annotation

    .line 33
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

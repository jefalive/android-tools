.class public Lcom/itau/empresas/ui/view/EditTextMonetario;
.super Lcom/itau/empresas/ui/view/TextoEditavel;
.source "EditTextMonetario.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/itau/empresas/ui/view/EditTextMonetario$OnAfterTextChanged;
    }
.end annotation


# instance fields
.field private mostraCifrao:Z

.field private onAfterTextChanged:Lcom/itau/empresas/ui/view/EditTextMonetario$OnAfterTextChanged;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .param p1, "context"    # Landroid/content/Context;

    .line 57
    invoke-direct {p0, p1}, Lcom/itau/empresas/ui/view/TextoEditavel;-><init>(Landroid/content/Context;)V

    .line 28
    new-instance v0, Lcom/itau/empresas/ui/view/EditTextMonetario$1;

    invoke-direct {v0, p0}, Lcom/itau/empresas/ui/view/EditTextMonetario$1;-><init>(Lcom/itau/empresas/ui/view/EditTextMonetario;)V

    invoke-virtual {p0, v0}, Lcom/itau/empresas/ui/view/EditTextMonetario;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 58
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .line 61
    invoke-direct {p0, p1, p2}, Lcom/itau/empresas/ui/view/TextoEditavel;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 28
    new-instance v0, Lcom/itau/empresas/ui/view/EditTextMonetario$1;

    invoke-direct {v0, p0}, Lcom/itau/empresas/ui/view/EditTextMonetario$1;-><init>(Lcom/itau/empresas/ui/view/EditTextMonetario;)V

    invoke-virtual {p0, v0}, Lcom/itau/empresas/ui/view/EditTextMonetario;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 63
    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    sget-object v1, Lcom/itau/empresas/R$styleable;->EditTextMonetario:[I

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, p2, v1, v2, v3}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v4

    .line 66
    .local v4, "typedArray":Landroid/content/res/TypedArray;
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {v4, v0, v1}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    iput-boolean v0, p0, Lcom/itau/empresas/ui/view/EditTextMonetario;->mostraCifrao:Z

    .line 67
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .line 70
    invoke-direct {p0, p1, p2, p3}, Lcom/itau/empresas/ui/view/TextoEditavel;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 28
    new-instance v0, Lcom/itau/empresas/ui/view/EditTextMonetario$1;

    invoke-direct {v0, p0}, Lcom/itau/empresas/ui/view/EditTextMonetario$1;-><init>(Lcom/itau/empresas/ui/view/EditTextMonetario;)V

    invoke-virtual {p0, v0}, Lcom/itau/empresas/ui/view/EditTextMonetario;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 71
    return-void
.end method

.method static synthetic access$000(Lcom/itau/empresas/ui/view/EditTextMonetario;)Z
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/ui/view/EditTextMonetario;

    .line 19
    iget-boolean v0, p0, Lcom/itau/empresas/ui/view/EditTextMonetario;->mostraCifrao:Z

    return v0
.end method

.method static synthetic access$100(Lcom/itau/empresas/ui/view/EditTextMonetario;)Lcom/itau/empresas/ui/view/EditTextMonetario$OnAfterTextChanged;
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/ui/view/EditTextMonetario;

    .line 19
    iget-object v0, p0, Lcom/itau/empresas/ui/view/EditTextMonetario;->onAfterTextChanged:Lcom/itau/empresas/ui/view/EditTextMonetario$OnAfterTextChanged;

    return-object v0
.end method


# virtual methods
.method public getValorMonetario()Lorg/joda/money/Money;
    .registers 4

    .line 82
    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/EditTextMonetario;->getValorMonetarioDesformatado()Ljava/lang/String;

    move-result-object v2

    .line 84
    .local v2, "valor":Ljava/lang/String;
    if-eqz v2, :cond_e

    const-string v0, ""

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_15

    .line 85
    :cond_e
    sget-object v0, Lcom/itau/empresas/ui/util/ViewUtils;->CURRENCY_UNIT:Lorg/joda/money/CurrencyUnit;

    invoke-static {v0}, Lorg/joda/money/Money;->zero(Lorg/joda/money/CurrencyUnit;)Lorg/joda/money/Money;

    move-result-object v0

    return-object v0

    .line 88
    :cond_15
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "BRL "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lorg/joda/money/Money;->parse(Ljava/lang/String;)Lorg/joda/money/Money;

    move-result-object v0

    return-object v0
.end method

.method public getValorMonetarioDesformatado()Ljava/lang/String;
    .registers 5

    .line 97
    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/EditTextMonetario;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    .line 99
    .local v2, "valorFormatado":Ljava/lang/String;
    const-string v0, ""

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 100
    const-string v0, ""

    return-object v0

    .line 104
    :cond_13
    :try_start_13
    iget-boolean v0, p0, Lcom/itau/empresas/ui/view/EditTextMonetario;->mostraCifrao:Z

    invoke-static {v2, v0}, Lcom/itau/empresas/ui/util/ValorMonetarioUtils;->formataValorMonetarioParaDecimal(Ljava/lang/String;Z)Ljava/math/BigDecimal;

    move-result-object v0

    invoke-virtual {v0}, Ljava/math/BigDecimal;->toString()Ljava/lang/String;
    :try_end_1c
    .catch Ljava/text/ParseException; {:try_start_13 .. :try_end_1c} :catch_1e

    move-result-object v0

    return-object v0

    .line 105
    :catch_1e
    move-exception v3

    .line 106
    .local v3, "e":Ljava/text/ParseException;
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "N\u00e3o foi poss\u00edvel recuperar o valor desformatado"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public setMostraCifrao(Z)V
    .registers 2
    .param p1, "mostraCifrao"    # Z

    .line 124
    iput-boolean p1, p0, Lcom/itau/empresas/ui/view/EditTextMonetario;->mostraCifrao:Z

    .line 125
    return-void
.end method

.method public setOnAfterTextChanged(Lcom/itau/empresas/ui/view/EditTextMonetario$OnAfterTextChanged;)V
    .registers 2
    .param p1, "onAfterTextChanged"    # Lcom/itau/empresas/ui/view/EditTextMonetario$OnAfterTextChanged;

    .line 111
    iput-object p1, p0, Lcom/itau/empresas/ui/view/EditTextMonetario;->onAfterTextChanged:Lcom/itau/empresas/ui/view/EditTextMonetario$OnAfterTextChanged;

    .line 112
    return-void
.end method

.method public setValorMonetario(Ljava/lang/String;)V
    .registers 3
    .param p1, "valor"    # Ljava/lang/String;

    .line 92
    iget-boolean v0, p0, Lcom/itau/empresas/ui/view/EditTextMonetario;->mostraCifrao:Z

    invoke-static {p1, v0}, Lcom/itau/empresas/ui/util/ValorMonetarioUtils;->formataValorDecimalParaMonetario(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/itau/empresas/ui/view/EditTextMonetario;->setText(Ljava/lang/CharSequence;)V

    .line 93
    return-void
.end method

.method public setValorMonetario(Lorg/joda/money/Money;)V
    .registers 4
    .param p1, "valor"    # Lorg/joda/money/Money;

    .line 74
    invoke-virtual {p1}, Lorg/joda/money/Money;->getAmount()Ljava/math/BigDecimal;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/itau/empresas/ui/util/ValorMonetarioUtils;->formataValorDecimalParaMonetario(Ljava/math/BigDecimal;Z)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/itau/empresas/ui/view/EditTextMonetario;->setText(Ljava/lang/CharSequence;)V

    .line 75
    return-void
.end method

.method public setValorMonetarioSemCifrao(Ljava/lang/String;)V
    .registers 3
    .param p1, "valor"    # Ljava/lang/String;

    .line 78
    const/4 v0, 0x0

    invoke-static {p1, v0}, Lcom/itau/empresas/ui/util/ValorMonetarioUtils;->formataValorDecimalParaMonetario(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/itau/empresas/ui/view/EditTextMonetario;->setText(Ljava/lang/CharSequence;)V

    .line 79
    return-void
.end method

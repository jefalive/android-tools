.class public final Lbr/com/concretesolutions/canarinho/validator/ValidadorCEP;
.super Ljava/lang/Object;
.source "ValidadorCEP.java"

# interfaces
.implements Lbr/com/concretesolutions/canarinho/validator/Validador;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lbr/com/concretesolutions/canarinho/validator/ValidadorCEP$SingletonHolder;
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .registers 1

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    return-void
.end method

.method synthetic constructor <init>(Lbr/com/concretesolutions/canarinho/validator/ValidadorCEP$1;)V
    .registers 2
    .param p1, "x0"    # Lbr/com/concretesolutions/canarinho/validator/ValidadorCEP$1;

    .line 12
    invoke-direct {p0}, Lbr/com/concretesolutions/canarinho/validator/ValidadorCEP;-><init>()V

    return-void
.end method

.method public static getInstance()Lbr/com/concretesolutions/canarinho/validator/ValidadorCEP;
    .registers 1

    .line 19
    # getter for: Lbr/com/concretesolutions/canarinho/validator/ValidadorCEP$SingletonHolder;->INSTANCE:Lbr/com/concretesolutions/canarinho/validator/ValidadorCEP;
    invoke-static {}, Lbr/com/concretesolutions/canarinho/validator/ValidadorCEP$SingletonHolder;->access$000()Lbr/com/concretesolutions/canarinho/validator/ValidadorCEP;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public ehValido(Landroid/text/Editable;Lbr/com/concretesolutions/canarinho/validator/Validador$ResultadoParcial;)Lbr/com/concretesolutions/canarinho/validator/Validador$ResultadoParcial;
    .registers 6
    .param p1, "valor"    # Landroid/text/Editable;
    .param p2, "resultadoParcial"    # Lbr/com/concretesolutions/canarinho/validator/Validador$ResultadoParcial;

    .line 37
    if-eqz p2, :cond_4

    if-nez p1, :cond_c

    .line 38
    :cond_4
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Valores n\u00e3o podem ser nulos"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 41
    :cond_c
    sget-object v0, Lbr/com/concretesolutions/canarinho/formatador/Formatador$Padroes;->PADRAO_SOMENTE_NUMEROS:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/util/regex/Matcher;->replaceAll(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 43
    .local v2, "desformatado":Ljava/lang/String;
    invoke-virtual {p0, v2}, Lbr/com/concretesolutions/canarinho/validator/ValidadorCEP;->ehValido(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_39

    .line 44
    .line 45
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v1, 0x8

    if-ge v0, v1, :cond_28

    const/4 v0, 0x1

    goto :goto_29

    :cond_28
    const/4 v0, 0x0

    :goto_29
    invoke-virtual {p2, v0}, Lbr/com/concretesolutions/canarinho/validator/Validador$ResultadoParcial;->parcialmenteValido(Z)Lbr/com/concretesolutions/canarinho/validator/Validador$ResultadoParcial;

    move-result-object v0

    const-string v1, "CEP inv\u00e1lido"

    .line 46
    invoke-virtual {v0, v1}, Lbr/com/concretesolutions/canarinho/validator/Validador$ResultadoParcial;->mensagem(Ljava/lang/String;)Lbr/com/concretesolutions/canarinho/validator/Validador$ResultadoParcial;

    move-result-object v0

    .line 47
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lbr/com/concretesolutions/canarinho/validator/Validador$ResultadoParcial;->totalmenteValido(Z)Lbr/com/concretesolutions/canarinho/validator/Validador$ResultadoParcial;

    move-result-object v0

    return-object v0

    .line 50
    .line 51
    :cond_39
    const/4 v0, 0x1

    invoke-virtual {p2, v0}, Lbr/com/concretesolutions/canarinho/validator/Validador$ResultadoParcial;->parcialmenteValido(Z)Lbr/com/concretesolutions/canarinho/validator/Validador$ResultadoParcial;

    move-result-object v0

    .line 52
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lbr/com/concretesolutions/canarinho/validator/Validador$ResultadoParcial;->totalmenteValido(Z)Lbr/com/concretesolutions/canarinho/validator/Validador$ResultadoParcial;

    move-result-object v0

    return-object v0
.end method

.method public ehValido(Ljava/lang/String;)Z
    .registers 5
    .param p1, "valor"    # Ljava/lang/String;

    .line 25
    if-eqz p1, :cond_a

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v1, 0x8

    if-ge v0, v1, :cond_c

    .line 26
    :cond_a
    const/4 v0, 0x0

    return v0

    .line 29
    :cond_c
    sget-object v0, Lbr/com/concretesolutions/canarinho/formatador/Formatador$Padroes;->PADRAO_SOMENTE_NUMEROS:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/util/regex/Matcher;->replaceAll(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 31
    .local v2, "desformatado":Ljava/lang/String;
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_22

    const/4 v0, 0x1

    goto :goto_23

    :cond_22
    const/4 v0, 0x0

    :goto_23
    return v0
.end method

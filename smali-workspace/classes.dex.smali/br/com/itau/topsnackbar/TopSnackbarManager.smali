.class Lbr/com/itau/topsnackbar/TopSnackbarManager;
.super Ljava/lang/Object;
.source "TopSnackbarManager.java"


# static fields
.field static instance:Lbr/com/itau/topsnackbar/TopSnackbarManager;


# instance fields
.field private final handler:Landroid/os/Handler;

.field private final lock:Ljava/lang/Object;

.field private snackbarWeak:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<Lbr/com/itau/topsnackbar/TopSnackbar;>;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .registers 4

    .line 36
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    new-instance v1, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    invoke-direct {p0, v0, v1}, Lbr/com/itau/topsnackbar/TopSnackbarManager;-><init>(Ljava/lang/Object;Landroid/os/Handler;)V

    .line 37
    return-void
.end method

.method constructor <init>(Ljava/lang/Object;Landroid/os/Handler;)V
    .registers 3
    .param p1, "lock"    # Ljava/lang/Object;
    .param p2, "handler"    # Landroid/os/Handler;

    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    iput-object p1, p0, Lbr/com/itau/topsnackbar/TopSnackbarManager;->lock:Ljava/lang/Object;

    .line 41
    iput-object p2, p0, Lbr/com/itau/topsnackbar/TopSnackbarManager;->handler:Landroid/os/Handler;

    .line 42
    return-void
.end method

.method private static beginDelayedTrasition(Landroid/view/ViewGroup;)V
    .registers 3
    .param p0, "parent"    # Landroid/view/ViewGroup;

    .line 98
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-lt v0, v1, :cond_9

    .line 99
    invoke-static {p0}, Landroid/transition/TransitionManager;->beginDelayedTransition(Landroid/view/ViewGroup;)V

    .line 100
    :cond_9
    return-void
.end method

.method private dismissPreviousIfExist()V
    .registers 4

    .line 72
    iget-object v0, p0, Lbr/com/itau/topsnackbar/TopSnackbarManager;->snackbarWeak:Ljava/lang/ref/WeakReference;

    if-nez v0, :cond_5

    return-void

    .line 73
    :cond_5
    iget-object v0, p0, Lbr/com/itau/topsnackbar/TopSnackbarManager;->snackbarWeak:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lbr/com/itau/topsnackbar/TopSnackbar;

    .line 74
    .local v2, "snackbar":Lbr/com/itau/topsnackbar/TopSnackbar;
    if-nez v2, :cond_11

    return-void

    .line 75
    :cond_11
    iget-object v0, v2, Lbr/com/itau/topsnackbar/TopSnackbar;->viewManager:Lbr/com/itau/topsnackbar/ViewManager;

    iget-object v1, v2, Lbr/com/itau/topsnackbar/TopSnackbar;->content:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Lbr/com/itau/topsnackbar/ViewManager;->removeContent(Landroid/view/View;)V

    .line 76
    return-void
.end method

.method private static durationInMilis(I)J
    .registers 3
    .param p0, "duration"    # I

    .line 45
    packed-switch p0, :pswitch_data_10

    goto :goto_d

    .line 47
    :pswitch_4
    const-wide/16 v0, 0x5dc

    return-wide v0

    .line 49
    :pswitch_7
    const-wide/16 v0, 0xabe

    return-wide v0

    .line 51
    :pswitch_a
    const-wide/16 v0, 0x0

    return-wide v0

    .line 53
    :goto_d
    int-to-long v0, p0

    return-wide v0

    nop

    :pswitch_data_10
    .packed-switch -0x2
        :pswitch_7
        :pswitch_4
        :pswitch_a
    .end packed-switch
.end method

.method private static endTransitions(Landroid/view/ViewGroup;)V
    .registers 3
    .param p0, "parent"    # Landroid/view/ViewGroup;

    .line 103
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x17

    if-lt v0, v1, :cond_9

    .line 104
    invoke-static {p0}, Landroid/transition/TransitionManager;->endTransitions(Landroid/view/ViewGroup;)V

    .line 105
    :cond_9
    return-void
.end method

.method static getInstance()Lbr/com/itau/topsnackbar/TopSnackbarManager;
    .registers 1

    .line 30
    sget-object v0, Lbr/com/itau/topsnackbar/TopSnackbarManager;->instance:Lbr/com/itau/topsnackbar/TopSnackbarManager;

    if-nez v0, :cond_b

    .line 31
    new-instance v0, Lbr/com/itau/topsnackbar/TopSnackbarManager;

    invoke-direct {v0}, Lbr/com/itau/topsnackbar/TopSnackbarManager;-><init>()V

    sput-object v0, Lbr/com/itau/topsnackbar/TopSnackbarManager;->instance:Lbr/com/itau/topsnackbar/TopSnackbarManager;

    .line 32
    :cond_b
    sget-object v0, Lbr/com/itau/topsnackbar/TopSnackbarManager;->instance:Lbr/com/itau/topsnackbar/TopSnackbarManager;

    return-object v0
.end method

.method private scheduleDismissIfNeeded(Lbr/com/itau/topsnackbar/TopSnackbar;)V
    .registers 6
    .param p1, "snackbar"    # Lbr/com/itau/topsnackbar/TopSnackbar;

    .line 79
    iget v0, p1, Lbr/com/itau/topsnackbar/TopSnackbar;->duration:I

    invoke-static {v0}, Lbr/com/itau/topsnackbar/TopSnackbarManager;->durationInMilis(I)J

    move-result-wide v2

    .line 80
    .local v2, "duration":J
    const-wide/16 v0, 0x0

    cmp-long v0, v2, v0

    if-lez v0, :cond_16

    .line 81
    iget-object v0, p0, Lbr/com/itau/topsnackbar/TopSnackbarManager;->handler:Landroid/os/Handler;

    new-instance v1, Lbr/com/itau/topsnackbar/TopSnackbarDimisser;

    invoke-direct {v1, p1}, Lbr/com/itau/topsnackbar/TopSnackbarDimisser;-><init>(Lbr/com/itau/topsnackbar/TopSnackbar;)V

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 82
    :cond_16
    return-void
.end method


# virtual methods
.method dismiss(Lbr/com/itau/topsnackbar/TopSnackbar;)V
    .registers 6
    .param p1, "snackbar"    # Lbr/com/itau/topsnackbar/TopSnackbar;

    .line 86
    iget-object v2, p0, Lbr/com/itau/topsnackbar/TopSnackbarManager;->lock:Ljava/lang/Object;

    monitor-enter v2

    .line 87
    if-nez p1, :cond_a

    .line 88
    :try_start_5
    invoke-direct {p0}, Lbr/com/itau/topsnackbar/TopSnackbarManager;->dismissPreviousIfExist()V
    :try_end_8
    .catchall {:try_start_5 .. :try_end_8} :catchall_1d

    .line 89
    monitor-exit v2

    return-void

    .line 91
    :cond_a
    :try_start_a
    iget-object v0, p1, Lbr/com/itau/topsnackbar/TopSnackbar;->parent:Landroid/view/ViewGroup;

    invoke-static {v0}, Lbr/com/itau/topsnackbar/TopSnackbarManager;->beginDelayedTrasition(Landroid/view/ViewGroup;)V

    .line 92
    iget-object v0, p1, Lbr/com/itau/topsnackbar/TopSnackbar;->viewManager:Lbr/com/itau/topsnackbar/ViewManager;

    iget-object v1, p1, Lbr/com/itau/topsnackbar/TopSnackbar;->content:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Lbr/com/itau/topsnackbar/ViewManager;->removeContent(Landroid/view/View;)V

    .line 93
    iget-object v0, p1, Lbr/com/itau/topsnackbar/TopSnackbar;->parent:Landroid/view/ViewGroup;

    invoke-static {v0}, Lbr/com/itau/topsnackbar/TopSnackbarManager;->endTransitions(Landroid/view/ViewGroup;)V
    :try_end_1b
    .catchall {:try_start_a .. :try_end_1b} :catchall_1d

    .line 94
    monitor-exit v2

    goto :goto_20

    :catchall_1d
    move-exception v3

    monitor-exit v2

    throw v3

    .line 95
    :goto_20
    return-void
.end method

.method show(Lbr/com/itau/topsnackbar/TopSnackbar;)V
    .registers 6
    .param p1, "snackbar"    # Lbr/com/itau/topsnackbar/TopSnackbar;

    .line 58
    iget-object v2, p0, Lbr/com/itau/topsnackbar/TopSnackbarManager;->lock:Ljava/lang/Object;

    monitor-enter v2

    .line 59
    :try_start_3
    iget-object v0, p0, Lbr/com/itau/topsnackbar/TopSnackbarManager;->handler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 60
    invoke-direct {p0}, Lbr/com/itau/topsnackbar/TopSnackbarManager;->dismissPreviousIfExist()V

    .line 61
    iget-object v0, p1, Lbr/com/itau/topsnackbar/TopSnackbar;->parent:Landroid/view/ViewGroup;

    invoke-static {v0}, Lbr/com/itau/topsnackbar/TopSnackbarManager;->beginDelayedTrasition(Landroid/view/ViewGroup;)V

    .line 62
    iget-object v0, p1, Lbr/com/itau/topsnackbar/TopSnackbar;->viewManager:Lbr/com/itau/topsnackbar/ViewManager;

    iget-object v1, p1, Lbr/com/itau/topsnackbar/TopSnackbar;->content:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Lbr/com/itau/topsnackbar/ViewManager;->addContent(Landroid/view/View;)V

    .line 63
    iget-object v0, p1, Lbr/com/itau/topsnackbar/TopSnackbar;->content:Landroid/widget/TextView;

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/support/v4/view/ViewCompat;->setAccessibilityLiveRegion(Landroid/view/View;I)V

    .line 65
    invoke-direct {p0, p1}, Lbr/com/itau/topsnackbar/TopSnackbarManager;->scheduleDismissIfNeeded(Lbr/com/itau/topsnackbar/TopSnackbar;)V

    .line 66
    iget-object v0, p1, Lbr/com/itau/topsnackbar/TopSnackbar;->parent:Landroid/view/ViewGroup;

    invoke-static {v0}, Lbr/com/itau/topsnackbar/TopSnackbarManager;->endTransitions(Landroid/view/ViewGroup;)V

    .line 67
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lbr/com/itau/topsnackbar/TopSnackbarManager;->snackbarWeak:Ljava/lang/ref/WeakReference;
    :try_end_31
    .catchall {:try_start_3 .. :try_end_31} :catchall_33

    .line 68
    monitor-exit v2

    goto :goto_36

    :catchall_33
    move-exception v3

    monitor-exit v2

    throw v3

    .line 69
    :goto_36
    return-void
.end method

.class public Lcom/google/android/gms/analytics/internal/zzad;
.super Ljava/lang/Object;


# instance fields
.field private final zzSP:J

.field private final zzSQ:I

.field private zzSR:D

.field private zzSS:J

.field private final zzST:Ljava/lang/Object;

.field private final zzSU:Ljava/lang/String;

.field private final zzqW:Lcom/google/android/gms/internal/zzmq;


# direct methods
.method public constructor <init>(IJLjava/lang/String;Lcom/google/android/gms/internal/zzmq;)V
    .registers 8

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/analytics/internal/zzad;->zzST:Ljava/lang/Object;

    iput p1, p0, Lcom/google/android/gms/analytics/internal/zzad;->zzSQ:I

    iget v0, p0, Lcom/google/android/gms/analytics/internal/zzad;->zzSQ:I

    int-to-double v0, v0

    iput-wide v0, p0, Lcom/google/android/gms/analytics/internal/zzad;->zzSR:D

    iput-wide p2, p0, Lcom/google/android/gms/analytics/internal/zzad;->zzSP:J

    iput-object p4, p0, Lcom/google/android/gms/analytics/internal/zzad;->zzSU:Ljava/lang/String;

    iput-object p5, p0, Lcom/google/android/gms/analytics/internal/zzad;->zzqW:Lcom/google/android/gms/internal/zzmq;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/google/android/gms/internal/zzmq;)V
    .registers 9

    move-object v0, p0

    move-object v4, p1

    move-object v5, p2

    const/16 v1, 0x3c

    const-wide/16 v2, 0x7d0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/analytics/internal/zzad;-><init>(IJLjava/lang/String;Lcom/google/android/gms/internal/zzmq;)V

    return-void
.end method


# virtual methods
.method public zzlw()Z
    .registers 13

    iget-object v4, p0, Lcom/google/android/gms/analytics/internal/zzad;->zzST:Ljava/lang/Object;

    monitor-enter v4

    :try_start_3
    iget-object v0, p0, Lcom/google/android/gms/analytics/internal/zzad;->zzqW:Lcom/google/android/gms/internal/zzmq;

    invoke-interface {v0}, Lcom/google/android/gms/internal/zzmq;->currentTimeMillis()J

    move-result-wide v5

    iget-wide v0, p0, Lcom/google/android/gms/analytics/internal/zzad;->zzSR:D

    iget v2, p0, Lcom/google/android/gms/analytics/internal/zzad;->zzSQ:I

    int-to-double v2, v2

    cmpg-double v0, v0, v2

    if-gez v0, :cond_2e

    iget-wide v0, p0, Lcom/google/android/gms/analytics/internal/zzad;->zzSS:J

    sub-long v7, v5, v0

    long-to-double v0, v7

    iget-wide v2, p0, Lcom/google/android/gms/analytics/internal/zzad;->zzSP:J

    long-to-double v2, v2

    div-double v9, v0, v2

    const-wide/16 v0, 0x0

    cmpl-double v0, v9, v0

    if-lez v0, :cond_2e

    iget v0, p0, Lcom/google/android/gms/analytics/internal/zzad;->zzSQ:I

    int-to-double v0, v0

    iget-wide v2, p0, Lcom/google/android/gms/analytics/internal/zzad;->zzSR:D

    add-double/2addr v2, v9

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->min(DD)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/analytics/internal/zzad;->zzSR:D

    :cond_2e
    iput-wide v5, p0, Lcom/google/android/gms/analytics/internal/zzad;->zzSS:J

    iget-wide v0, p0, Lcom/google/android/gms/analytics/internal/zzad;->zzSR:D

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    cmpl-double v0, v0, v2

    if-ltz v0, :cond_42

    iget-wide v0, p0, Lcom/google/android/gms/analytics/internal/zzad;->zzSR:D

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    sub-double/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/gms/analytics/internal/zzad;->zzSR:D
    :try_end_3f
    .catchall {:try_start_3 .. :try_end_3f} :catchall_63

    monitor-exit v4

    const/4 v0, 0x1

    return v0

    :cond_42
    :try_start_42
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Excessive "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/analytics/internal/zzad;->zzSU:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " detected; call ignored."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/analytics/internal/zzae;->zzaK(Ljava/lang/String;)V
    :try_end_60
    .catchall {:try_start_42 .. :try_end_60} :catchall_63

    monitor-exit v4

    const/4 v0, 0x0

    return v0

    :catchall_63
    move-exception v11

    monitor-exit v4

    throw v11
.end method

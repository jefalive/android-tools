.class public final Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity_;
.super Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity;
.source "RecargaCadastroContatoActivity_.java"

# interfaces
.implements Lorg/androidannotations/api/view/HasViews;
.implements Lorg/androidannotations/api/view/OnViewChangedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity_$IntentBuilder_;
    }
.end annotation


# instance fields
.field private final onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;


# direct methods
.method public constructor <init>()V
    .registers 2

    .line 47
    invoke-direct {p0}, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity;-><init>()V

    .line 51
    new-instance v0, Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-direct {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    return-void
.end method

.method static synthetic access$001(Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity_;)V
    .registers 1
    .param p0, "x0"    # Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity_;

    .line 47
    invoke-super {p0}, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity;->mostraDialogoDeProgresso()V

    return-void
.end method

.method static synthetic access$101(Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity_;)V
    .registers 1
    .param p0, "x0"    # Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity_;

    .line 47
    invoke-super {p0}, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity;->escondeDialogoDeProgresso()V

    return-void
.end method

.method private init_(Landroid/os/Bundle;)V
    .registers 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 65
    new-instance v0, Lcom/itau/empresas/Preferences_;

    invoke-direct {v0, p0}, Lcom/itau/empresas/Preferences_;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity_;->prefs:Lcom/itau/empresas/Preferences_;

    .line 66
    invoke-static {p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->registerOnViewChangedListener(Lorg/androidannotations/api/view/OnViewChangedListener;)V

    .line 67
    invoke-static {}, Lcom/itau/empresas/CustomApplication_;->getInstance()Lcom/itau/empresas/CustomApplication;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity_;->application:Lcom/itau/empresas/CustomApplication;

    .line 68
    invoke-static {p0}, Lcom/itau/empresas/feature/recarga/efetivacao/controller/RecargaController_;->getInstance_(Landroid/content/Context;)Lcom/itau/empresas/feature/recarga/efetivacao/controller/RecargaController_;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity_;->recargaController:Lcom/itau/empresas/feature/recarga/efetivacao/controller/RecargaController;

    .line 69
    invoke-static {p0}, Lcom/itau/empresas/feature/recarga/contatos/controller/ContatoRecargaController_;->getInstance_(Landroid/content/Context;)Lcom/itau/empresas/feature/recarga/contatos/controller/ContatoRecargaController_;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity_;->contatoRecargaController:Lcom/itau/empresas/feature/recarga/contatos/controller/ContatoRecargaController;

    .line 70
    invoke-direct {p0}, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity_;->injectExtras_()V

    .line 71
    invoke-virtual {p0}, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity_;->afterInject()V

    .line 72
    return-void
.end method

.method private injectExtras_()V
    .registers 3

    .line 213
    invoke-virtual {p0}, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity_;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 214
    .local v1, "extras_":Landroid/os/Bundle;
    if-eqz v1, :cond_2e

    .line 215
    const-string v0, "contatoRecarga"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 216
    const-string v0, "contatoRecarga"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity_;->contatoRecarga:Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;

    .line 218
    :cond_1c
    const-string v0, "tipo"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2e

    .line 219
    const-string v0, "tipo"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity$TIPOS;

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity_;->tipo:Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity$TIPOS;

    .line 222
    :cond_2e
    return-void
.end method


# virtual methods
.method public escondeDialogoDeProgresso()V
    .registers 5

    .line 247
    const-string v0, ""

    new-instance v1, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity_$9;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity_$9;-><init>(Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity_;)V

    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3}, Lorg/androidannotations/api/UiThreadExecutor;->runTask(Ljava/lang/String;Ljava/lang/Runnable;J)V

    .line 255
    return-void
.end method

.method public mostraDialogoDeProgresso()V
    .registers 5
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "CommitTransaction"
        }
    .end annotation

    .line 235
    const-string v0, ""

    new-instance v1, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity_$8;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity_$8;-><init>(Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity_;)V

    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3}, Lorg/androidannotations/api/UiThreadExecutor;->runTask(Ljava/lang/String;Ljava/lang/Runnable;J)V

    .line 243
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 57
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-static {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    move-result-object v1

    .line 58
    .local v1, "previousNotifier":Lorg/androidannotations/api/view/OnViewChangedNotifier;
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity_;->init_(Landroid/os/Bundle;)V

    .line 59
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity;->onCreate(Landroid/os/Bundle;)V

    .line 60
    invoke-static {v1}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 61
    const v0, 0x7f03005b

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity_;->setContentView(I)V

    .line 62
    return-void
.end method

.method public onViewChanged(Lorg/androidannotations/api/view/HasViews;)V
    .registers 5
    .param p1, "hasViews"    # Lorg/androidannotations/api/view/HasViews;

    .line 106
    const v0, 0x7f0e02a2

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity_;->root:Landroid/widget/LinearLayout;

    .line 107
    const v0, 0x7f0e02a5

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lbr/com/itau/widgets/material/MaterialEditText;

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity_;->campoCelular:Lbr/com/itau/widgets/material/MaterialEditText;

    .line 108
    const v0, 0x7f0e02a8

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lbr/com/itau/widgets/material/MaterialEditText;

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity_;->campoApelido:Lbr/com/itau/widgets/material/MaterialEditText;

    .line 109
    const v0, 0x7f0e02a9

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lbr/com/itau/widgets/material/MaterialSpinner;

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity_;->spinnerOperadora:Lbr/com/itau/widgets/material/MaterialSpinner;

    .line 110
    const v0, 0x7f0e02aa

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lbr/com/itau/widgets/material/MaterialSpinner;

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity_;->spinnerRegiao:Lbr/com/itau/widgets/material/MaterialSpinner;

    .line 111
    const v0, 0x7f0e02a7

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity_;->botaoAgenda:Landroid/widget/ImageView;

    .line 112
    const v0, 0x7f0e02ab

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity_;->botaoFazerRecarga:Landroid/widget/Button;

    .line 113
    const v0, 0x7f0e02ae

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity_;->botaoSalvar:Landroid/widget/Button;

    .line 114
    const v0, 0x7f0e02ad

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity_;->botaoExcluir:Landroid/widget/Button;

    .line 115
    const v0, 0x7f0e02a4

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity_;->textoMensagem:Landroid/widget/TextView;

    .line 116
    const v0, 0x7f0e02ac

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity_;->llBotoesSalvarExcluir:Landroid/widget/LinearLayout;

    .line 117
    const v0, 0x7f0e00b6

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/Toolbar;

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity_;->toolBar:Landroid/support/v7/widget/Toolbar;

    .line 118
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity_;->botaoAgenda:Landroid/widget/ImageView;

    if-eqz v0, :cond_92

    .line 119
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity_;->botaoAgenda:Landroid/widget/ImageView;

    new-instance v1, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity_$1;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity_$1;-><init>(Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity_;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 128
    :cond_92
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity_;->botaoExcluir:Landroid/widget/Button;

    if-eqz v0, :cond_a0

    .line 129
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity_;->botaoExcluir:Landroid/widget/Button;

    new-instance v1, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity_$2;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity_$2;-><init>(Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity_;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 138
    :cond_a0
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity_;->botaoSalvar:Landroid/widget/Button;

    if-eqz v0, :cond_ae

    .line 139
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity_;->botaoSalvar:Landroid/widget/Button;

    new-instance v1, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity_$3;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity_$3;-><init>(Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity_;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 148
    :cond_ae
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity_;->botaoFazerRecarga:Landroid/widget/Button;

    if-eqz v0, :cond_bc

    .line 149
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity_;->botaoFazerRecarga:Landroid/widget/Button;

    new-instance v1, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity_$4;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity_$4;-><init>(Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity_;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 158
    :cond_bc
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity_;->spinnerOperadora:Lbr/com/itau/widgets/material/MaterialSpinner;

    if-eqz v0, :cond_ca

    .line 159
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity_;->spinnerOperadora:Lbr/com/itau/widgets/material/MaterialSpinner;

    new-instance v1, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity_$5;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity_$5;-><init>(Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity_;)V

    invoke-virtual {v0, v1}, Landroid/widget/AdapterView;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 173
    :cond_ca
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity_;->spinnerRegiao:Lbr/com/itau/widgets/material/MaterialSpinner;

    if-eqz v0, :cond_d8

    .line 174
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity_;->spinnerRegiao:Lbr/com/itau/widgets/material/MaterialSpinner;

    new-instance v1, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity_$6;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity_$6;-><init>(Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity_;)V

    invoke-virtual {v0, v1}, Landroid/widget/AdapterView;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 189
    :cond_d8
    const v0, 0x7f0e02a8

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Landroid/widget/TextView;

    .line 190
    .local v2, "view":Landroid/widget/TextView;
    if-eqz v2, :cond_ec

    .line 191
    new-instance v0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity_$7;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity_$7;-><init>(Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity_;)V

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 209
    .end local v2    # "view":Landroid/widget/TextView;
    :cond_ec
    invoke-virtual {p0}, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity_;->carregaElementosDaTela()V

    .line 210
    return-void
.end method

.method public setContentView(I)V
    .registers 3
    .param p1, "layoutResID"    # I

    .line 76
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity;->setContentView(I)V

    .line 77
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 78
    return-void
.end method

.method public setContentView(Landroid/view/View;)V
    .registers 3
    .param p1, "view"    # Landroid/view/View;

    .line 88
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity;->setContentView(Landroid/view/View;)V

    .line 89
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 90
    return-void
.end method

.method public setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .registers 4
    .param p1, "view"    # Landroid/view/View;
    .param p2, "params"    # Landroid/view/ViewGroup$LayoutParams;

    .line 82
    invoke-super {p0, p1, p2}, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity;->setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 83
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 84
    return-void
.end method

.method public setIntent(Landroid/content/Intent;)V
    .registers 2
    .param p1, "newIntent"    # Landroid/content/Intent;

    .line 226
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity;->setIntent(Landroid/content/Intent;)V

    .line 227
    invoke-direct {p0}, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity_;->injectExtras_()V

    .line 228
    return-void
.end method

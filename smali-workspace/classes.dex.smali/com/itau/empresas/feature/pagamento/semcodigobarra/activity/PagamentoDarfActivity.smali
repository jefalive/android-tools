.class public Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;
.super Lcom/itau/empresas/ui/activity/BaseActivity;
.source "PagamentoDarfActivity.java"

# interfaces
.implements Lcom/itau/empresas/ui/util/listener/AtualizaDadosActivityListener;
.implements Lcom/itau/empresas/ui/util/listener/ValidacaoEditTextListener;
.implements Lbr/com/concretesolutions/canarinho/watcher/evento/EventoDeValidacao;


# instance fields
.field private alcadaAutorizante:Z

.field app:Lcom/itau/empresas/CustomApplication;

.field private autorizar:Z

.field campoAgenciaConta:Landroid/widget/TextView;

.field campoCnpjPagador:Landroid/widget/TextView;

.field campoCodigoReceita:Landroid/widget/EditText;

.field campoCpfCnpjContribuinte:Landroid/widget/EditText;

.field campoDataApuracao:Landroid/widget/EditText;

.field campoDataPagamento:Landroid/widget/TextView;

.field campoDataVencimento:Landroid/widget/EditText;

.field campoHorarioLimiteFinal:Landroid/widget/TextView;

.field campoHorarioLimiteInicial:Landroid/widget/TextView;

.field campoIdentificacaoComprovante:Landroid/widget/EditText;

.field campoNomeContribuinte:Landroid/widget/EditText;

.field campoNomeDaEmpresa:Landroid/widget/TextView;

.field campoNumeroReferencia:Landroid/widget/EditText;

.field campoReferenciaEmpresa:Landroid/widget/EditText;

.field campoValorJuros:Lcom/itau/empresas/ui/view/EditTextMonetario;

.field campoValorMulta:Lcom/itau/empresas/ui/view/EditTextMonetario;

.field campoValorPrincipal:Lcom/itau/empresas/ui/view/EditTextMonetario;

.field campoValorTotalPagarDarf:Landroid/widget/TextView;

.field private cnpjVOList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/CnpjVO;>;"
        }
    .end annotation
.end field

.field private contaOperadorVO:Lcom/itau/empresas/api/model/ContaOperadorVO;

.field private dadosDarf:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosDarf;

.field private darfInclusaoRequestVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;

.field private dataVencimentoSelecionada:Ljava/lang/String;

.field private datePickerDialog:Lcom/wdullaer/materialdatetimepicker/date/DatePickerDialog;

.field private editTextFocus:Landroid/widget/EditText;

.field private hint:Lbr/com/itau/widgets/hintview/Hint$Builder;

.field private hintView:Lbr/com/itau/widgets/hintview/Hint;

.field private isTooltipWarning:Z

.field private listaEditTextInvalido:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Landroid/widget/TextView;>;"
        }
    .end annotation
.end field

.field private listaEditTextValidacao:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Landroid/widget/TextView;>;"
        }
    .end annotation
.end field

.field private mapaEditTextMensagemErrro:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<Landroid/widget/EditText;Ljava/lang/String;>;"
        }
    .end annotation
.end field

.field private mostraImagemFechar:Z

.field private onFocusCampoNomeContibuinte:Landroid/view/View$OnFocusChangeListener;

.field pagamentoDarfController:Lcom/itau/empresas/controller/PagamentoDarfController;

.field rlAutorizacaoDarf:Landroid/widget/RelativeLayout;

.field rlHorarioLimite:Landroid/widget/RelativeLayout;

.field rlInclusaoDarf:Landroid/widget/RelativeLayout;

.field root:Landroid/widget/LinearLayout;

.field scroolDarf:Landroid/widget/ScrollView;

.field toolBar:Landroid/support/v7/widget/Toolbar;

.field private valorPagamentoDarf:Lcom/itau/empresas/ui/util/listener/AtualizaValorPagamento;


# direct methods
.method public constructor <init>()V
    .registers 2

    .line 89
    invoke-direct {p0}, Lcom/itau/empresas/ui/activity/BaseActivity;-><init>()V

    .line 146
    new-instance v0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity$1;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity$1;-><init>(Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;)V

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->onFocusCampoNomeContibuinte:Landroid/view/View$OnFocusChangeListener;

    return-void
.end method

.method private abreTelaConfirmacao()V
    .registers 3

    .line 445
    invoke-static {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ConfirmarAutorizacaoTributosActivity;->intent(Landroid/content/Context;)Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ConfirmarAutorizacaoTributosActivity_$IntentBuilder_;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->darfInclusaoRequestVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;

    .line 446
    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ConfirmarAutorizacaoTributosActivity_$IntentBuilder_;->darfInclusaoRequestVO(Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;)Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ConfirmarAutorizacaoTributosActivity_$IntentBuilder_;

    move-result-object v0

    .line 447
    invoke-direct {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->buildDadosConfirmacao()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ConfirmarAutorizacaoTributosActivity_$IntentBuilder_;->dadosConfirmacaoTributos(Ljava/util/ArrayList;)Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ConfirmarAutorizacaoTributosActivity_$IntentBuilder_;

    move-result-object v0

    .line 448
    invoke-virtual {v0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ConfirmarAutorizacaoTributosActivity_$IntentBuilder_;->start()Lorg/androidannotations/api/builder/PostActivityStarter;

    .line 449
    return-void
.end method

.method private abreTelaDeInclusaoSucesso(Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/InclusaoTributoResponseVO;)V
    .registers 4
    .param p1, "inclusaoTributoResponseVO"    # Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/InclusaoTributoResponseVO;

    .line 565
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->campoIdentificacaoComprovante:Landroid/widget/EditText;

    .line 566
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 565
    invoke-virtual {p1, v0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/InclusaoTributoResponseVO;->setIdentificadorPagamento(Ljava/lang/String;)V

    .line 567
    invoke-static {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity;->intent(Landroid/content/Context;)Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity_$IntentBuilder_;

    move-result-object v0

    .line 568
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->buildDadosPagamento(Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/InclusaoTributoResponseVO;)Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity_$IntentBuilder_;->dadosPagamentoTributo(Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo;)Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity_$IntentBuilder_;

    move-result-object v0

    .line 569
    invoke-virtual {v0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity_$IntentBuilder_;->start()Lorg/androidannotations/api/builder/PostActivityStarter;

    .line 570
    invoke-virtual {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->finish()V

    .line 571
    return-void
.end method

.method static synthetic access$000(Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;II)V
    .registers 3
    .param p0, "x0"    # Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .line 89
    invoke-direct {p0, p1, p2}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->trackerAnalitycs(II)V

    return-void
.end method

.method static synthetic access$100(Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;Landroid/widget/EditText;)V
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;
    .param p1, "x1"    # Landroid/widget/EditText;

    .line 89
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->validaCampoObrigatorioUnico(Landroid/widget/EditText;)V

    return-void
.end method

.method static synthetic access$1000(Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;Landroid/widget/TextView;)V
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;
    .param p1, "x1"    # Landroid/widget/TextView;

    .line 89
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->removeEditText(Landroid/widget/TextView;)V

    return-void
.end method

.method static synthetic access$1100(Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;Ljava/lang/String;)V
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;
    .param p1, "x1"    # Ljava/lang/String;

    .line 89
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->setaDadosDatePicker(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$1300(Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;Landroid/view/View;Ljava/lang/String;)V
    .registers 3
    .param p0, "x0"    # Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;
    .param p1, "x1"    # Landroid/view/View;
    .param p2, "x2"    # Ljava/lang/String;

    .line 89
    invoke-direct {p0, p1, p2}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->mostraAvisoDeCampoObrigatorio(Landroid/view/View;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$1400(Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;)Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosDarf;
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;

    .line 89
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->dadosDarf:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosDarf;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;Z)V
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;
    .param p1, "x1"    # Z

    .line 89
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->incluiPagamentoDARF(Z)V

    return-void
.end method

.method static synthetic access$200(Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;Ljava/lang/String;)V
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;
    .param p1, "x1"    # Ljava/lang/String;

    .line 89
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->cpfCnpjInvalido(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$300(Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;)V
    .registers 1
    .param p0, "x0"    # Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;

    .line 89
    invoke-direct {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->removeHint()V

    return-void
.end method

.method static synthetic access$400(Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;)V
    .registers 1
    .param p0, "x0"    # Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;

    .line 89
    invoke-direct {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->openDatePickerApuracao()V

    return-void
.end method

.method static synthetic access$500(Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;)V
    .registers 1
    .param p0, "x0"    # Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;

    .line 89
    invoke-direct {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->openDatePickerPagamento()V

    return-void
.end method

.method static synthetic access$600(Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;)V
    .registers 1
    .param p0, "x0"    # Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;

    .line 89
    invoke-direct {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->openDatePickerVencimento()V

    return-void
.end method

.method static synthetic access$700(Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;)Ljava/lang/String;
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;

    .line 89
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->dataVencimentoSelecionada:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$702(Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;Ljava/lang/String;)Ljava/lang/String;
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;
    .param p1, "x1"    # Ljava/lang/String;

    .line 89
    iput-object p1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->dataVencimentoSelecionada:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$800(Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;Landroid/view/ViewGroup;Z)V
    .registers 3
    .param p0, "x0"    # Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;
    .param p1, "x1"    # Landroid/view/ViewGroup;
    .param p2, "x2"    # Z

    .line 89
    invoke-direct {p0, p1, p2}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->trocaCorBackground(Landroid/view/ViewGroup;Z)V

    return-void
.end method

.method static synthetic access$900(Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;Landroid/widget/TextView;Z)V
    .registers 3
    .param p0, "x0"    # Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;
    .param p1, "x1"    # Landroid/widget/TextView;
    .param p2, "x2"    # Z

    .line 89
    invoke-direct {p0, p1, p2}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->trocaCorLabel(Landroid/widget/TextView;Z)V

    return-void
.end method

.method private adicionaEditTextParaValidacao()V
    .registers 3

    .line 811
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->listaEditTextValidacao:Ljava/util/List;

    .line 812
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->listaEditTextValidacao:Ljava/util/List;

    iget-object v1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->campoNomeContribuinte:Landroid/widget/EditText;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 813
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->listaEditTextValidacao:Ljava/util/List;

    iget-object v1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->campoDataApuracao:Landroid/widget/EditText;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 814
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->listaEditTextValidacao:Ljava/util/List;

    iget-object v1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->campoCpfCnpjContribuinte:Landroid/widget/EditText;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 815
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->listaEditTextValidacao:Ljava/util/List;

    iget-object v1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->campoCodigoReceita:Landroid/widget/EditText;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 816
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->listaEditTextValidacao:Ljava/util/List;

    iget-object v1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->campoValorPrincipal:Lcom/itau/empresas/ui/view/EditTextMonetario;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 817
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->listaEditTextValidacao:Ljava/util/List;

    iget-object v1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->campoDataVencimento:Landroid/widget/EditText;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 818
    return-void
.end method

.method private adicionaMensagemErroValidacao()V
    .registers 4

    .line 821
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->mapaEditTextMensagemErrro:Ljava/util/Map;

    .line 822
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->mapaEditTextMensagemErrro:Ljava/util/Map;

    iget-object v1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->campoCpfCnpjContribuinte:Landroid/widget/EditText;

    .line 823
    const v2, 0x7f0701ee

    invoke-virtual {p0, v2}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 822
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 824
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->mapaEditTextMensagemErrro:Ljava/util/Map;

    iget-object v1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->campoDataApuracao:Landroid/widget/EditText;

    const-string v2, "Preenchimento obrigat\u00f3rio"

    .line 825
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 826
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->mapaEditTextMensagemErrro:Ljava/util/Map;

    iget-object v1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->campoDataVencimento:Landroid/widget/EditText;

    const-string v2, "Preenchimento obrigat\u00f3rio"

    .line 827
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 828
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->mapaEditTextMensagemErrro:Ljava/util/Map;

    iget-object v1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->campoCodigoReceita:Landroid/widget/EditText;

    const-string v2, "Preenchimento obrigat\u00f3rio"

    .line 829
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 830
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->mapaEditTextMensagemErrro:Ljava/util/Map;

    iget-object v1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->campoNomeContribuinte:Landroid/widget/EditText;

    const-string v2, "Preenchimento obrigat\u00f3rio"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 832
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->mapaEditTextMensagemErrro:Ljava/util/Map;

    iget-object v1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->campoValorMulta:Lcom/itau/empresas/ui/view/EditTextMonetario;

    const-string v2, "Preenchimento obrigat\u00f3rio"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 833
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->mapaEditTextMensagemErrro:Ljava/util/Map;

    iget-object v1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->campoValorPrincipal:Lcom/itau/empresas/ui/view/EditTextMonetario;

    const-string v2, "Preenchimento obrigat\u00f3rio"

    .line 834
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 835
    return-void
.end method

.method private buildDadosConfirmacao()Ljava/util/ArrayList;
    .registers 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/ArrayList<Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosConfirmacaoTributo;>;"
        }
    .end annotation

    .line 452
    invoke-direct {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->preencheDadosConfirmacaoTributos()Ljava/util/ArrayList;

    move-result-object v4

    .line 453
    .local v4, "dadosConfirmacaoTributos":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosConfirmacaoTributo;>;"
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->darfInclusaoRequestVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;->getReferenciaEmpresa()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_26

    .line 454
    new-instance v0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosConfirmacaoTributo;

    .line 455
    const v1, 0x7f070498

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->darfInclusaoRequestVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;

    .line 456
    invoke-virtual {v2}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;->getReferenciaEmpresa()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosConfirmacaoTributo;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 454
    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 458
    :cond_26
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->darfInclusaoRequestVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;->getInformativoPagador()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_48

    .line 459
    new-instance v0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosConfirmacaoTributo;

    .line 460
    const v1, 0x7f070498

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->darfInclusaoRequestVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;

    .line 461
    invoke-virtual {v2}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;->getInformativoPagador()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosConfirmacaoTributo;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 459
    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 463
    :cond_48
    return-object v4
.end method

.method private buildDadosPagamento(Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/InclusaoTributoResponseVO;)Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo;
    .registers 4
    .param p1, "inclusaoTributos"    # Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/InclusaoTributoResponseVO;

    .line 574
    new-instance v0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo$Builder;

    invoke-direct {v0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo$Builder;-><init>()V

    .line 575
    invoke-virtual {v0, p1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo$Builder;->comInclusaoTributos(Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/InclusaoTributoResponseVO;)Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo$Builder;

    move-result-object v0

    .line 576
    const v1, 0x7f07049f

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo$Builder;->comMensagem(Ljava/lang/String;)Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo$Builder;

    move-result-object v0

    const-string v1, "DARF"

    .line 577
    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo$Builder;->comTipoPagamento(Ljava/lang/String;)Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo$Builder;

    move-result-object v0

    .line 578
    invoke-virtual {v0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo$Builder;->build()Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo;

    move-result-object v0

    .line 574
    return-object v0
.end method

.method private constroiDatePickerDialog(Landroid/widget/TextView;)Lcom/wdullaer/materialdatetimepicker/date/DatePickerDialog;
    .registers 3
    .param p1, "textView"    # Landroid/widget/TextView;

    .line 743
    invoke-static {p0, p1}, Lcom/itau/empresas/ui/util/DataEHoraUtils;->mostraDialogoDataComTodasDatas(Lcom/itau/empresas/ui/activity/BaseActivity;Landroid/widget/TextView;)Lcom/wdullaer/materialdatetimepicker/date/DatePickerDialog;

    move-result-object v0

    return-object v0
.end method

.method private constroiToolbar()V
    .registers 3

    .line 732
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->toolBar:Landroid/support/v7/widget/Toolbar;

    invoke-static {v0, p0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom;->Builder(Landroid/support/v7/widget/Toolbar;Landroid/support/v7/app/AppCompatActivity;)Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    move-result-object v0

    .line 733
    const v1, 0x7f0706a2

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->comTitulo(I)Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    move-result-object v0

    .line 734
    invoke-virtual {v0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->comClickListenerFinish()Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    move-result-object v0

    .line 735
    invoke-virtual {v0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->build()Lcom/itau/empresas/ui/util/listener/ToolbarCustom;

    .line 736
    return-void
.end method

.method private consultaAlcada()V
    .registers 4

    .line 596
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->pagamentoDarfController:Lcom/itau/empresas/controller/PagamentoDarfController;

    iget-object v1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->app:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v1}, Lcom/itau/empresas/CustomApplication;->getContaSelecionada()Lcom/itau/empresas/api/model/ContaOperadorVO;

    move-result-object v1

    invoke-virtual {v1}, Lcom/itau/empresas/api/model/ContaOperadorVO;->getConta()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->app:Lcom/itau/empresas/CustomApplication;

    .line 597
    invoke-virtual {v2}, Lcom/itau/empresas/CustomApplication;->getCodigoOperador()Ljava/lang/String;

    move-result-object v2

    .line 596
    invoke-virtual {v0, v1, v2}, Lcom/itau/empresas/controller/PagamentoDarfController;->consultaAlcadas(Ljava/lang/String;Ljava/lang/String;)V

    .line 598
    return-void
.end method

.method private consultaCnpjPagamentos()V
    .registers 2

    .line 838
    invoke-virtual {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->mostraDialogoDeProgresso()V

    .line 839
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->pagamentoDarfController:Lcom/itau/empresas/controller/PagamentoDarfController;

    invoke-virtual {v0}, Lcom/itau/empresas/controller/PagamentoDarfController;->consultaCnpjPagamentos()V

    .line 840
    return-void
.end method

.method private consultaDadosCliente()V
    .registers 2

    .line 843
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->pagamentoDarfController:Lcom/itau/empresas/controller/PagamentoDarfController;

    invoke-virtual {v0}, Lcom/itau/empresas/controller/PagamentoDarfController;->consultaDadosPerfilLogado()Lcom/itau/empresas/api/model/PerfilOperadorVO;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->atualizaDadosPerfilLogado(Lcom/itau/empresas/api/model/PerfilOperadorVO;)V

    .line 844
    return-void
.end method

.method private consultaHorarioLimite()V
    .registers 3

    .line 847
    invoke-virtual {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->mostraDialogoDeProgresso()V

    .line 848
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->pagamentoDarfController:Lcom/itau/empresas/controller/PagamentoDarfController;

    const-string v1, "impostos_tributos_concessionarias"

    invoke-virtual {v0, v1}, Lcom/itau/empresas/controller/PagamentoDarfController;->consultaLimitesHorario(Ljava/lang/String;)V

    .line 849
    return-void
.end method

.method private cpfCnpjInvalido(Ljava/lang/String;)V
    .registers 4
    .param p1, "textoValidar"    # Ljava/lang/String;

    .line 719
    invoke-static {}, Lbr/com/concretesolutions/canarinho/validator/ValidadorCPFCNPJ;->getInstance()Lbr/com/concretesolutions/canarinho/validator/ValidadorCPFCNPJ;

    move-result-object v0

    sget-object v1, Lbr/com/concretesolutions/canarinho/formatador/FormatadorCPFCNPJ;->CPF_CNPJ:Lbr/com/concretesolutions/canarinho/formatador/Formatador;

    .line 720
    invoke-interface {v1, p1}, Lbr/com/concretesolutions/canarinho/formatador/Formatador;->estaFormatado(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_13

    sget-object v1, Lbr/com/concretesolutions/canarinho/formatador/FormatadorCPFCNPJ;->CPF_CNPJ:Lbr/com/concretesolutions/canarinho/formatador/Formatador;

    .line 721
    invoke-interface {v1, p1}, Lbr/com/concretesolutions/canarinho/formatador/Formatador;->desformata(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto :goto_14

    :cond_13
    move-object v1, p1

    .line 720
    :goto_14
    invoke-virtual {v0, v1}, Lbr/com/concretesolutions/canarinho/validator/ValidadorCPFCNPJ;->ehValido(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2f

    .line 722
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->campoCpfCnpjContribuinte:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->hasFocus()Z

    move-result v0

    if-eqz v0, :cond_32

    .line 723
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->campoCpfCnpjContribuinte:Landroid/widget/EditText;

    .line 724
    const v1, 0x7f0701ba

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 723
    invoke-direct {p0, v0, v1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->mostraAvisoDeCampoObrigatorio(Landroid/view/View;Ljava/lang/String;)V

    goto :goto_32

    .line 727
    :cond_2f
    invoke-direct {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->removeHint()V

    .line 729
    :cond_32
    :goto_32
    return-void
.end method

.method private incluiDarf()V
    .registers 3

    .line 784
    iget-boolean v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->autorizar:Z

    if-eqz v0, :cond_11

    .line 785
    const v0, 0x7f070325

    const v1, 0x7f0702a0

    invoke-direct {p0, v0, v1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->trackerAnalitycs(II)V

    .line 786
    invoke-direct {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->abreTelaConfirmacao()V

    goto :goto_21

    .line 788
    :cond_11
    const v0, 0x7f0702f8

    const v1, 0x7f0702a0

    invoke-direct {p0, v0, v1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->trackerAnalitycs(II)V

    .line 789
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->pagamentoDarfController:Lcom/itau/empresas/controller/PagamentoDarfController;

    iget-object v1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->darfInclusaoRequestVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;

    invoke-virtual {v0, v1}, Lcom/itau/empresas/controller/PagamentoDarfController;->incluiDarf(Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;)V

    .line 791
    :goto_21
    return-void
.end method

.method private incluiPagamentoDARF(Z)V
    .registers 6
    .param p1, "duplicidade"    # Z

    .line 748
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->contaOperadorVO:Lcom/itau/empresas/api/model/ContaOperadorVO;

    invoke-virtual {v1}, Lcom/itau/empresas/api/model/ContaOperadorVO;->getAgencia()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->contaOperadorVO:Lcom/itau/empresas/api/model/ContaOperadorVO;

    .line 749
    invoke-virtual {v1}, Lcom/itau/empresas/api/model/ContaOperadorVO;->getConta()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->contaOperadorVO:Lcom/itau/empresas/api/model/ContaOperadorVO;

    .line 750
    invoke-virtual {v1}, Lcom/itau/empresas/api/model/ContaOperadorVO;->getDac()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 751
    .local v3, "idContaOperador":Ljava/lang/String;
    new-instance v0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;

    invoke-direct {v0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->darfInclusaoRequestVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;

    .line 752
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->darfInclusaoRequestVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;

    invoke-virtual {v0, v3}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;->setIdConta(Ljava/lang/String;)V

    .line 753
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->darfInclusaoRequestVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;

    iget-object v1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->campoNumeroReferencia:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;->setNumeroReferencia(Ljava/lang/String;)V

    .line 754
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->darfInclusaoRequestVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;

    iget-object v1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->campoIdentificacaoComprovante:Landroid/widget/EditText;

    .line 755
    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;->setInformativoPagador(Ljava/lang/String;)V

    .line 756
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->darfInclusaoRequestVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;

    iget-object v1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->campoCnpjPagador:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;->setCnpjEmpresa(Ljava/lang/String;)V

    .line 757
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->darfInclusaoRequestVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;

    iget-object v1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->campoCpfCnpjContribuinte:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;->setCnpjContribuinte(Ljava/lang/String;)V

    .line 758
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->darfInclusaoRequestVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;

    iget-object v1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->campoDataApuracao:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getEditableText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;->setDataApuracao(Ljava/lang/String;)V

    .line 759
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->darfInclusaoRequestVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;

    iget-object v1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->campoNomeDaEmpresa:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;->setNomeEmpresa(Ljava/lang/String;)V

    .line 760
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->darfInclusaoRequestVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;

    iget-object v1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->campoNomeContribuinte:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;->setNomeContribuinte(Ljava/lang/String;)V

    .line 761
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->darfInclusaoRequestVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;

    iget-object v1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->campoNomeDaEmpresa:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;->setNomeEmpresa(Ljava/lang/String;)V

    .line 762
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->darfInclusaoRequestVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;

    iget-object v1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->campoDataApuracao:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;->setDataApuracao(Ljava/lang/String;)V

    .line 763
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->darfInclusaoRequestVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;

    iget-object v1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->campoDataPagamento:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;->setDataPagamento(Ljava/lang/String;)V

    .line 764
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->darfInclusaoRequestVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;

    iget-object v1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->campoDataVencimento:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;->setDataVencimento(Ljava/lang/String;)V

    .line 765
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->darfInclusaoRequestVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;

    invoke-virtual {v0, p1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;->setDuplicidade(Z)V

    .line 766
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->darfInclusaoRequestVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;->setSimples(Z)V

    .line 767
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->darfInclusaoRequestVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;

    iget-object v1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->campoCodigoReceita:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;->setCodigoReceita(Ljava/lang/String;)V

    .line 768
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->darfInclusaoRequestVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;

    iget-object v1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->campoValorJuros:Lcom/itau/empresas/ui/view/EditTextMonetario;

    invoke-virtual {v1}, Lcom/itau/empresas/ui/view/EditTextMonetario;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->valorFormatado(Ljava/lang/String;)Ljava/math/BigDecimal;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;->setValorJuros(Ljava/math/BigDecimal;)V

    .line 769
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->darfInclusaoRequestVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;

    iget-object v1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->campoValorMulta:Lcom/itau/empresas/ui/view/EditTextMonetario;

    invoke-virtual {v1}, Lcom/itau/empresas/ui/view/EditTextMonetario;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->valorFormatado(Ljava/lang/String;)Ljava/math/BigDecimal;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;->setValorMultas(Ljava/math/BigDecimal;)V

    .line 770
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->darfInclusaoRequestVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;

    iget-object v1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->campoValorPrincipal:Lcom/itau/empresas/ui/view/EditTextMonetario;

    .line 771
    invoke-virtual {v1}, Lcom/itau/empresas/ui/view/EditTextMonetario;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->valorFormatado(Ljava/lang/String;)Ljava/math/BigDecimal;

    move-result-object v1

    .line 770
    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;->setValorPrincipal(Ljava/math/BigDecimal;)V

    .line 772
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->darfInclusaoRequestVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;

    iget-object v1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->campoValorTotalPagarDarf:Landroid/widget/TextView;

    .line 773
    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->valorFormatado(Ljava/lang/String;)Ljava/math/BigDecimal;

    move-result-object v1

    .line 772
    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;->setValorTotal(Ljava/math/BigDecimal;)V

    .line 774
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->darfInclusaoRequestVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;

    new-instance v1, Ljava/math/BigDecimal;

    const-string v2, "0.00"

    invoke-direct {v1, v2}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;->setValorReceita(Ljava/math/BigDecimal;)V

    .line 775
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->darfInclusaoRequestVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;

    new-instance v1, Ljava/math/BigDecimal;

    const-string v2, "0.00"

    invoke-direct {v1, v2}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;->setPercentual(Ljava/math/BigDecimal;)V

    .line 776
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->darfInclusaoRequestVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;

    iget-object v1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->campoReferenciaEmpresa:Landroid/widget/EditText;

    .line 777
    invoke-virtual {v1}, Landroid/widget/EditText;->getEditableText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;->setReferenciaEmpresa(Ljava/lang/String;)V

    .line 778
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->darfInclusaoRequestVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;

    iget-object v1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->app:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v1}, Lcom/itau/empresas/CustomApplication;->getCodigoOperador()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;->setCodigoOperador(Ljava/lang/String;)V

    .line 779
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->darfInclusaoRequestVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;

    const-string v1, "J"

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;->setTipoUsuario(Ljava/lang/String;)V

    .line 780
    invoke-direct {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->incluiDarf()V

    .line 781
    return-void
.end method

.method private inicializaCamposValoresDefault()V
    .registers 4

    .line 803
    invoke-static {}, Lcom/itau/empresas/ui/util/formatacao/DateUtils;->getDataAtualFormatoBrasileiro()Ljava/lang/String;

    move-result-object v2

    .line 804
    .local v2, "dataAtual":Ljava/lang/String;
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->campoValorPrincipal:Lcom/itau/empresas/ui/view/EditTextMonetario;

    const-string v1, "000"

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/view/EditTextMonetario;->setText(Ljava/lang/CharSequence;)V

    .line 805
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->campoValorJuros:Lcom/itau/empresas/ui/view/EditTextMonetario;

    const-string v1, "000"

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/view/EditTextMonetario;->setText(Ljava/lang/CharSequence;)V

    .line 806
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->campoValorMulta:Lcom/itau/empresas/ui/view/EditTextMonetario;

    const-string v1, "000"

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/view/EditTextMonetario;->setText(Ljava/lang/CharSequence;)V

    .line 807
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->campoDataPagamento:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 808
    return-void
.end method

.method public static intent(Landroid/content/Context;)Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity_$IntentBuilder_;
    .registers 2
    .param p0, "context"    # Landroid/content/Context;

    .line 899
    new-instance v0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity_$IntentBuilder_;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity_$IntentBuilder_;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method private irParaCamporObrigatorio(Landroid/view/View;)V
    .registers 5
    .param p1, "view"    # Landroid/view/View;

    .line 883
    invoke-static {p0, p1}, Lcom/itau/empresas/ui/util/TecladoUtils;->esconderTecladoDaView(Landroid/content/Context;Landroid/view/View;)V

    .line 884
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->scroolDarf:Landroid/widget/ScrollView;

    invoke-virtual {p1}, Landroid/view/View;->getBottom()I

    move-result v1

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v2

    sub-int/2addr v1, v2

    const/4 v2, 0x0

    invoke-virtual {v0, v2, v1}, Landroid/widget/ScrollView;->scrollTo(II)V

    .line 885
    invoke-virtual {p1}, Landroid/view/View;->requestFocus()Z

    .line 886
    return-void
.end method

.method private listenerChange(Landroid/view/ViewGroup;Landroid/widget/TextView;Landroid/widget/TextView;)V
    .registers 5
    .param p1, "viewGroup"    # Landroid/view/ViewGroup;
    .param p2, "editText"    # Landroid/widget/TextView;
    .param p3, "label"    # Landroid/widget/TextView;

    .line 646
    new-instance v0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity$9;

    invoke-direct {v0, p0, p3, p1, p2}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity$9;-><init>(Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;Landroid/widget/TextView;Landroid/view/ViewGroup;Landroid/widget/TextView;)V

    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 658
    return-void
.end method

.method private marcaCampoObrigatorio(Ljava/util/List;)V
    .registers 7
    .param p1, "editTextList"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<Landroid/widget/TextView;>;)V"
        }
    .end annotation

    .line 889
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_4
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_29

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Landroid/widget/TextView;

    .line 890
    .local v3, "editText":Landroid/widget/TextView;
    instance-of v0, v3, Lcom/itau/empresas/ui/view/EditTextCustomError;

    if-eqz v0, :cond_28

    .line 891
    move-object v4, v3

    check-cast v4, Lcom/itau/empresas/ui/view/EditTextCustomError;

    .line 892
    .local v4, "editTextCustomError":Lcom/itau/empresas/ui/view/EditTextCustomError;
    invoke-virtual {v3}, Landroid/widget/TextView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v4, v0}, Lcom/itau/empresas/ui/view/EditTextCustomError;->setaBackgroundErro(Landroid/view/ViewGroup;)V

    .line 893
    move-object v0, v3

    check-cast v0, Lcom/itau/empresas/ui/view/EditTextCustomError;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/view/EditTextCustomError;->setErro(Z)V

    .line 895
    .end local v3    # "editText":Landroid/widget/TextView;
    .end local v4    # "editTextCustomError":Lcom/itau/empresas/ui/view/EditTextCustomError;
    :cond_28
    goto :goto_4

    .line 896
    :cond_29
    return-void
.end method

.method private mostraAvisoDeCampoObrigatorio(Landroid/view/View;Ljava/lang/String;)V
    .registers 7
    .param p1, "view"    # Landroid/view/View;
    .param p2, "mensagem"    # Ljava/lang/String;

    .line 602
    invoke-direct {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->removeHint()V

    .line 603
    move-object v0, p1

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->editTextFocus:Landroid/widget/EditText;

    .line 604
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Landroid/view/ViewGroup;

    .line 605
    .local v2, "viewGroup":Landroid/view/ViewGroup;
    const/4 v0, 0x0

    invoke-virtual {v2, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 606
    .local v3, "viewAncora":Landroid/view/View;
    iget-boolean v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->mostraImagemFechar:Z

    if-eqz v0, :cond_39

    invoke-virtual {p1}, Landroid/view/View;->hasFocus()Z

    move-result v0

    if-eqz v0, :cond_39

    .line 607
    new-instance v0, Lbr/com/itau/widgets/hintview/Hint$Builder;

    const v1, 0x7f09011b

    invoke-direct {v0, v3, v1}, Lbr/com/itau/widgets/hintview/Hint$Builder;-><init>(Landroid/view/View;I)V

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->hint:Lbr/com/itau/widgets/hintview/Hint$Builder;

    .line 608
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->hint:Lbr/com/itau/widgets/hintview/Hint$Builder;

    const/16 v1, 0x30

    invoke-static {v0, p2, v1}, Lcom/itau/empresas/ui/util/HintUtils;->montaHintAlerta(Lbr/com/itau/widgets/hintview/Hint$Builder;Ljava/lang/String;I)Lbr/com/itau/widgets/hintview/Hint$Builder;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->hint:Lbr/com/itau/widgets/hintview/Hint$Builder;

    .line 609
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->hint:Lbr/com/itau/widgets/hintview/Hint$Builder;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lbr/com/itau/widgets/hintview/Hint$Builder;->setCancelable(Z)Lbr/com/itau/widgets/hintview/Hint$Builder;

    goto :goto_57

    .line 611
    :cond_39
    new-instance v0, Lbr/com/itau/widgets/hintview/Hint$Builder;

    const v1, 0x7f09011d

    invoke-direct {v0, v3, v1}, Lbr/com/itau/widgets/hintview/Hint$Builder;-><init>(Landroid/view/View;I)V

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->hint:Lbr/com/itau/widgets/hintview/Hint$Builder;

    .line 612
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->hint:Lbr/com/itau/widgets/hintview/Hint$Builder;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lbr/com/itau/widgets/hintview/Hint$Builder;->setCancelable(Z)Lbr/com/itau/widgets/hintview/Hint$Builder;

    .line 613
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->hint:Lbr/com/itau/widgets/hintview/Hint$Builder;

    invoke-static {v0, p2}, Lcom/itau/empresas/ui/util/HintUtils;->montaHintErro(Lbr/com/itau/widgets/hintview/Hint$Builder;Ljava/lang/String;)Lbr/com/itau/widgets/hintview/Hint$Builder;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->hint:Lbr/com/itau/widgets/hintview/Hint$Builder;

    .line 614
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->hint:Lbr/com/itau/widgets/hintview/Hint$Builder;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lbr/com/itau/widgets/hintview/Hint$Builder;->setCancelable(Z)Lbr/com/itau/widgets/hintview/Hint$Builder;

    .line 617
    :goto_57
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->hint:Lbr/com/itau/widgets/hintview/Hint$Builder;

    new-instance v1, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity$8;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity$8;-><init>(Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;)V

    invoke-virtual {v0, v1}, Lbr/com/itau/widgets/hintview/Hint$Builder;->setOnClickListener(Lbr/com/itau/widgets/hintview/OnClickListener;)Lbr/com/itau/widgets/hintview/Hint$Builder;

    .line 623
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->hint:Lbr/com/itau/widgets/hintview/Hint$Builder;

    invoke-virtual {v0}, Lbr/com/itau/widgets/hintview/Hint$Builder;->show()Lbr/com/itau/widgets/hintview/Hint;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->hintView:Lbr/com/itau/widgets/hintview/Hint;

    .line 624
    return-void
.end method

.method private mostraBotaoAutorizar()V
    .registers 5

    .line 582
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->app:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getPermissaoAutorizante()Lcom/itau/empresas/api/model/MenuVO;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/api/model/MenuVO;->getFilhos()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_e
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_46

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/itau/empresas/api/model/MenuVO;

    .line 583
    .local v3, "menuVO":Lcom/itau/empresas/api/model/MenuVO;
    invoke-virtual {v3}, Lcom/itau/empresas/api/model/MenuVO;->getNome()Ljava/lang/String;

    move-result-object v0

    const-string v1, "exibicao_tributos_autorizar"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_38

    iget-boolean v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->alcadaAutorizante:Z

    if-eqz v0, :cond_38

    .line 585
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->rlAutorizacaoDarf:Landroid/widget/RelativeLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 586
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->rlInclusaoDarf:Landroid/widget/RelativeLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 587
    return-void

    .line 589
    :cond_38
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->rlInclusaoDarf:Landroid/widget/RelativeLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 590
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->rlAutorizacaoDarf:Landroid/widget/RelativeLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 592
    .end local v3    # "menuVO":Lcom/itau/empresas/api/model/MenuVO;
    goto :goto_e

    .line 593
    :cond_46
    return-void
.end method

.method private mostraDialogDuplicidade()V
    .registers 4

    .line 852
    new-instance v0, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;

    invoke-direct {v0, p0}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget-object v1, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Estados;->DOIS_BOTOES:Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Estados;

    .line 853
    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;->estado(Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Estados;)Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;

    move-result-object v0

    .line 854
    const v1, 0x7f0701aa

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;->mensagem(I)Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;

    move-result-object v0

    .line 855
    const v1, 0x7f070176

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;->textoPositivo(I)Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;

    move-result-object v0

    .line 856
    const v1, 0x7f0701e6

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;->textoNegativo(I)Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;

    move-result-object v0

    .line 857
    invoke-virtual {v0}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;->build()Lcom/itau/empresas/ui/dialog/CustomAlertDialog;

    move-result-object v2

    .line 858
    .local v2, "dialog":Lcom/itau/empresas/ui/dialog/CustomAlertDialog;
    const/4 v0, 0x1

    invoke-virtual {v2, v0}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog;->setBoldBotaoPositivo(Z)V

    .line 859
    new-instance v0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity$12;

    invoke-direct {v0, p0, v2}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity$12;-><init>(Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;Lcom/itau/empresas/ui/dialog/CustomAlertDialog;)V

    invoke-virtual {v2, v0}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog;->setNegativoListener(Landroid/view/View$OnClickListener;)V

    .line 867
    new-instance v0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity$13;

    invoke-direct {v0, p0, v2}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity$13;-><init>(Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;Lcom/itau/empresas/ui/dialog/CustomAlertDialog;)V

    invoke-virtual {v2, v0}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog;->setPositivoListener(Landroid/view/View$OnClickListener;)V

    .line 873
    invoke-virtual {v2, p0}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog;->show(Landroid/support/v7/app/AppCompatActivity;)Lcom/itau/empresas/ui/dialog/BaseDialog;

    .line 874
    return-void
.end method

.method private openDatePickerApuracao()V
    .registers 3

    .line 522
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->campoDataApuracao:Landroid/widget/EditText;

    invoke-direct {p0, v0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->constroiDatePickerDialog(Landroid/widget/TextView;)Lcom/wdullaer/materialdatetimepicker/date/DatePickerDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->datePickerDialog:Lcom/wdullaer/materialdatetimepicker/date/DatePickerDialog;

    .line 523
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->datePickerDialog:Lcom/wdullaer/materialdatetimepicker/date/DatePickerDialog;

    new-instance v1, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity$6;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity$6;-><init>(Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;)V

    invoke-virtual {v0, v1}, Lcom/wdullaer/materialdatetimepicker/date/DatePickerDialog;->setOnDateSetListener(Lcom/wdullaer/materialdatetimepicker/date/DatePickerDialog$OnDateSetListener;)V

    .line 530
    return-void
.end method

.method private openDatePickerPagamento()V
    .registers 3

    .line 694
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->campoDataPagamento:Landroid/widget/TextView;

    .line 695
    invoke-static {p0, v0}, Lcom/itau/empresas/ui/util/DataEHoraUtils;->mostraDialogoDataComAnterioresDesabilitadasEDataMaximaMesAtual(Lcom/itau/empresas/ui/activity/BaseActivity;Landroid/widget/TextView;)Lcom/wdullaer/materialdatetimepicker/date/DatePickerDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->datePickerDialog:Lcom/wdullaer/materialdatetimepicker/date/DatePickerDialog;

    .line 697
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->datePickerDialog:Lcom/wdullaer/materialdatetimepicker/date/DatePickerDialog;

    new-instance v1, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity$11;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity$11;-><init>(Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;)V

    invoke-virtual {v0, v1}, Lcom/wdullaer/materialdatetimepicker/date/DatePickerDialog;->setOnDateSetListener(Lcom/wdullaer/materialdatetimepicker/date/DatePickerDialog$OnDateSetListener;)V

    .line 712
    return-void
.end method

.method private openDatePickerVencimento()V
    .registers 3

    .line 533
    invoke-direct {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->removeHint()V

    .line 534
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->campoDataVencimento:Landroid/widget/EditText;

    invoke-direct {p0, v0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->constroiDatePickerDialog(Landroid/widget/TextView;)Lcom/wdullaer/materialdatetimepicker/date/DatePickerDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->datePickerDialog:Lcom/wdullaer/materialdatetimepicker/date/DatePickerDialog;

    .line 535
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->datePickerDialog:Lcom/wdullaer/materialdatetimepicker/date/DatePickerDialog;

    new-instance v1, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity$7;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity$7;-><init>(Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;)V

    invoke-virtual {v0, v1}, Lcom/wdullaer/materialdatetimepicker/date/DatePickerDialog;->setOnDateSetListener(Lcom/wdullaer/materialdatetimepicker/date/DatePickerDialog$OnDateSetListener;)V

    .line 547
    return-void
.end method

.method private preencheDadosConfirmacaoTributos()Ljava/util/ArrayList;
    .registers 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/ArrayList<Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosConfirmacaoTributo;>;"
        }
    .end annotation

    .line 467
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 468
    .local v4, "dadosConfirmacaoTributos":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosConfirmacaoTributo;>;"
    new-instance v0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosConfirmacaoTributo;

    .line 469
    const v1, 0x7f07047f

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->darfInclusaoRequestVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;

    .line 470
    invoke-virtual {v2}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;->getNomeContribuinte()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosConfirmacaoTributo;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 469
    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 471
    new-instance v0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosConfirmacaoTributo;

    .line 472
    const v1, 0x7f0704af

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->darfInclusaoRequestVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;

    .line 473
    invoke-virtual {v2}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;->getDataApuracao()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3b

    iget-object v2, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->darfInclusaoRequestVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;

    .line 474
    invoke-virtual {v2}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;->getDataApuracao()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/itau/empresas/ui/util/DataEHoraUtils;->formataDataComBarra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    goto :goto_3d

    :cond_3b
    const-string v2, ""

    :goto_3d
    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosConfirmacaoTributo;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 472
    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 476
    new-instance v0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosConfirmacaoTributo;

    .line 477
    const v1, 0x7f070188

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lbr/com/concretesolutions/canarinho/formatador/FormatadorCPFCNPJ;->CPF_CNPJ:Lbr/com/concretesolutions/canarinho/formatador/Formatador;

    iget-object v3, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->darfInclusaoRequestVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;

    .line 479
    invoke-virtual {v3}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;->getCnpjContribuinte()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_62

    iget-object v3, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->darfInclusaoRequestVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;

    .line 480
    invoke-virtual {v3}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;->getCnpjContribuinte()Ljava/lang/String;

    move-result-object v3

    goto :goto_68

    :cond_62
    iget-object v3, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->darfInclusaoRequestVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;

    .line 481
    invoke-virtual {v3}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;->getCpfContribuinte()Ljava/lang/String;

    move-result-object v3

    .line 478
    :goto_68
    invoke-interface {v2, v3}, Lbr/com/concretesolutions/canarinho/formatador/Formatador;->formata(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosConfirmacaoTributo;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 477
    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 482
    new-instance v0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosConfirmacaoTributo;

    const v1, 0x7f07018e

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->darfInclusaoRequestVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;

    .line 483
    invoke-virtual {v2}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;->getCodigoReceita()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosConfirmacaoTributo;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 482
    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 484
    new-instance v0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosConfirmacaoTributo;

    .line 485
    const v1, 0x7f070485

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->darfInclusaoRequestVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;

    .line 486
    invoke-virtual {v2}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;->getNumeroReferencia()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosConfirmacaoTributo;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 485
    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 487
    new-instance v0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosConfirmacaoTributo;

    .line 488
    const v1, 0x7f0703f6

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->darfInclusaoRequestVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;

    .line 489
    invoke-virtual {v2}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;->getDataPagamento()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_bf

    iget-object v2, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->darfInclusaoRequestVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;

    .line 491
    invoke-virtual {v2}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;->getDataPagamento()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/itau/empresas/ui/util/DataEHoraUtils;->formataDataComBarra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    goto :goto_c1

    :cond_bf
    const-string v2, ""

    :goto_c1
    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosConfirmacaoTributo;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 488
    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 493
    new-instance v0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosConfirmacaoTributo;

    .line 494
    const v1, 0x7f07049d

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->darfInclusaoRequestVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;

    .line 495
    invoke-virtual {v2}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;->getValorPrincipal()Ljava/math/BigDecimal;

    move-result-object v2

    if-eqz v2, :cond_e5

    iget-object v2, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->darfInclusaoRequestVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;

    .line 496
    invoke-virtual {v2}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;->getValorPrincipal()Ljava/math/BigDecimal;

    move-result-object v2

    const/4 v3, 0x1

    invoke-static {v2, v3}, Lcom/itau/empresas/ui/util/formatacao/FormatadorNumerico;->decimalParaMonetario(Ljava/math/BigDecimal;Z)Ljava/lang/String;

    move-result-object v2

    goto :goto_e7

    :cond_e5
    const-string v2, "R$ 0,00"

    :goto_e7
    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosConfirmacaoTributo;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 494
    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 498
    new-instance v0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosConfirmacaoTributo;

    .line 499
    const v1, 0x7f070499

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->darfInclusaoRequestVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;

    .line 500
    invoke-virtual {v2}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;->getValorMultas()Ljava/math/BigDecimal;

    move-result-object v2

    if-eqz v2, :cond_10b

    iget-object v2, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->darfInclusaoRequestVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;

    .line 501
    invoke-virtual {v2}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;->getValorMultas()Ljava/math/BigDecimal;

    move-result-object v2

    const/4 v3, 0x1

    invoke-static {v2, v3}, Lcom/itau/empresas/ui/util/formatacao/FormatadorNumerico;->decimalParaMonetario(Ljava/math/BigDecimal;Z)Ljava/lang/String;

    move-result-object v2

    goto :goto_10d

    :cond_10b
    const-string v2, "R$ 0,00"

    :goto_10d
    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosConfirmacaoTributo;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 499
    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 503
    new-instance v0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosConfirmacaoTributo;

    .line 504
    const v1, 0x7f07049b

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->darfInclusaoRequestVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;

    .line 505
    invoke-virtual {v2}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;->getValorJuros()Ljava/math/BigDecimal;

    move-result-object v2

    if-eqz v2, :cond_131

    iget-object v2, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->darfInclusaoRequestVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;

    .line 506
    invoke-virtual {v2}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;->getValorJuros()Ljava/math/BigDecimal;

    move-result-object v2

    const/4 v3, 0x1

    invoke-static {v2, v3}, Lcom/itau/empresas/ui/util/formatacao/FormatadorNumerico;->decimalParaMonetario(Ljava/math/BigDecimal;Z)Ljava/lang/String;

    move-result-object v2

    goto :goto_133

    :cond_131
    const-string v2, "R$ 0,00"

    :goto_133
    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosConfirmacaoTributo;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 503
    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 508
    new-instance v0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosConfirmacaoTributo;

    .line 509
    const v1, 0x7f070680

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->darfInclusaoRequestVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;

    .line 510
    invoke-virtual {v2}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;->getValorTotal()Ljava/math/BigDecimal;

    move-result-object v2

    if-eqz v2, :cond_157

    iget-object v2, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->darfInclusaoRequestVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;

    .line 511
    invoke-virtual {v2}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;->getValorTotal()Ljava/math/BigDecimal;

    move-result-object v2

    const/4 v3, 0x1

    invoke-static {v2, v3}, Lcom/itau/empresas/ui/util/formatacao/FormatadorNumerico;->decimalParaMonetario(Ljava/math/BigDecimal;Z)Ljava/lang/String;

    move-result-object v2

    goto :goto_159

    :cond_157
    const-string v2, "R$ 0,00"

    :goto_159
    const/4 v3, 0x1

    invoke-direct {v0, v1, v2, v3}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosConfirmacaoTributo;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 509
    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 513
    new-instance v0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosConfirmacaoTributo;

    .line 514
    const v1, 0x7f070495

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->darfInclusaoRequestVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;

    .line 515
    invoke-virtual {v2}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;->getDataPagamento()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_180

    iget-object v2, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->darfInclusaoRequestVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;

    .line 516
    invoke-virtual {v2}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;->getDataPagamento()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/itau/empresas/ui/util/DataEHoraUtils;->formataDataComBarra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    goto :goto_182

    :cond_180
    const-string v2, ""

    :goto_182
    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosConfirmacaoTributo;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 513
    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 518
    return-object v4
.end method

.method private removeEditText(Landroid/widget/TextView;)V
    .registers 6
    .param p1, "editText"    # Landroid/widget/TextView;

    .line 794
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->listaEditTextInvalido:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 795
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->listaEditTextInvalido:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3f

    .line 796
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->listaEditTextInvalido:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Landroid/view/ViewGroup;

    .line 797
    .local v3, "viewGroup":Landroid/view/ViewGroup;
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->listaEditTextInvalido:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->listaEditTextInvalido:Ljava/util/List;

    .line 798
    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const/4 v2, 0x0

    invoke-virtual {v3, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 797
    invoke-direct {p0, v0, v1, v2}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->listenerChange(Landroid/view/ViewGroup;Landroid/widget/TextView;Landroid/widget/TextView;)V

    .line 800
    .end local v3    # "viewGroup":Landroid/view/ViewGroup;
    :cond_3f
    return-void
.end method

.method private removeHint()V
    .registers 2

    .line 627
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->hintView:Lbr/com/itau/widgets/hintview/Hint;

    if-eqz v0, :cond_9

    .line 628
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->hintView:Lbr/com/itau/widgets/hintview/Hint;

    invoke-virtual {v0}, Lbr/com/itau/widgets/hintview/Hint;->dismiss()V

    .line 630
    :cond_9
    return-void
.end method

.method private setaDadosDatePicker(Ljava/lang/String;)V
    .registers 4
    .param p1, "data"    # Ljava/lang/String;

    .line 550
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->pagamentoDarfController:Lcom/itau/empresas/controller/PagamentoDarfController;

    invoke-virtual {v0, p1}, Lcom/itau/empresas/controller/PagamentoDarfController;->dataSelecionadaEhMenorQueAtual(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_23

    .line 551
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->isTooltipWarning:Z

    .line 552
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->mostraImagemFechar:Z

    .line 553
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->campoDataVencimento:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 554
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->campoDataVencimento:Landroid/widget/EditText;

    .line 555
    const v1, 0x7f0706db

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 554
    invoke-direct {p0, v0, v1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->mostraAvisoDeCampoObrigatorio(Landroid/view/View;Ljava/lang/String;)V

    .line 556
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->mostraImagemFechar:Z

    goto :goto_32

    .line 558
    :cond_23
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->campoDataVencimento:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 559
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->campoDataPagamento:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 560
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->dadosDarf:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosDarf;

    invoke-virtual {v0, p1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosDarf;->setDataPagamento(Ljava/lang/String;)V

    .line 562
    :goto_32
    return-void
.end method

.method private trackerAnalitycs(II)V
    .registers 7
    .param p1, "categoriaResId"    # I
    .param p2, "acaoResId"    # I

    .line 877
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->application:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getEventoTracker()Lcom/itau/empresas/ui/util/analytics/TrackerEvento;

    move-result-object v0

    new-instance v1, Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;

    .line 878
    invoke-virtual {p0, p1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 879
    invoke-virtual {p0, p2}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 877
    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/analytics/TrackerEvento;->enviarHitEvento(Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;)V

    .line 880
    return-void
.end method

.method private trocaCorBackground(Landroid/view/ViewGroup;Z)V
    .registers 5
    .param p1, "view"    # Landroid/view/ViewGroup;
    .param p2, "erro"    # Z

    .line 661
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-ge v0, v1, :cond_13

    .line 662
    if-eqz p2, :cond_c

    const v0, 0x7f020065

    goto :goto_f

    :cond_c
    const v0, 0x7f020063

    :goto_f
    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->setBackgroundResource(I)V

    goto :goto_23

    .line 665
    :cond_13
    if-eqz p2, :cond_19

    const v0, 0x7f020065

    goto :goto_1c

    :cond_19
    const v0, 0x7f020063

    :goto_1c
    invoke-static {p0, v0}, Landroid/support/v4/content/ContextCompat;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 668
    :goto_23
    return-void
.end method

.method private trocaCorLabel(Landroid/widget/TextView;Z)V
    .registers 4
    .param p1, "textView"    # Landroid/widget/TextView;
    .param p2, "erro"    # Z

    .line 671
    if-eqz p2, :cond_6

    const v0, 0x7f0c012c

    goto :goto_9

    :cond_6
    const v0, 0x7f0c0041

    .line 672
    :goto_9
    invoke-static {p0, v0}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v0

    .line 671
    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 674
    return-void
.end method

.method private validaCampoObrigatorioUnico(Landroid/widget/EditText;)V
    .registers 3
    .param p1, "editText"    # Landroid/widget/EditText;

    .line 715
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->pagamentoDarfController:Lcom/itau/empresas/controller/PagamentoDarfController;

    invoke-virtual {v0, p1, p0}, Lcom/itau/empresas/controller/PagamentoDarfController;->validaCampoObrigatorio(Landroid/widget/EditText;Lcom/itau/empresas/ui/util/listener/ValidacaoEditTextListener;)V

    .line 716
    return-void
.end method

.method private validaCamposObrigatorios()V
    .registers 3

    .line 739
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->pagamentoDarfController:Lcom/itau/empresas/controller/PagamentoDarfController;

    iget-object v1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->listaEditTextValidacao:Ljava/util/List;

    invoke-virtual {v0, v1, p0}, Lcom/itau/empresas/controller/PagamentoDarfController;->validaTextoObrigatório(Ljava/util/List;Lcom/itau/empresas/ui/util/listener/ValidacaoEditTextListener;)V

    .line 740
    return-void
.end method

.method private validaDataVencimento()V
    .registers 3

    .line 213
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->pagamentoDarfController:Lcom/itau/empresas/controller/PagamentoDarfController;

    iget-object v1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->dataVencimentoSelecionada:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/itau/empresas/controller/PagamentoDarfController;->dataSelecionadaEhMenorQueAtual(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_16

    .line 214
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->campoDataPagamento:Landroid/widget/TextView;

    .line 215
    const v1, 0x7f0701ce

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 214
    invoke-direct {p0, v0, v1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->mostraAvisoDeCampoObrigatorio(Landroid/view/View;Ljava/lang/String;)V

    .line 217
    :cond_16
    return-void
.end method

.method private valorFormatado(Ljava/lang/String;)Ljava/math/BigDecimal;
    .registers 5
    .param p1, "valor"    # Ljava/lang/String;

    .line 634
    :try_start_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_e

    const-string v0, "R$ 0,00"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_16

    :cond_e
    new-instance v0, Ljava/math/BigDecimal;

    const-string v1, "0.00"

    invoke-direct {v0, v1}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    goto :goto_1b

    .line 637
    :cond_16
    const/4 v0, 0x1

    invoke-static {p1, v0}, Lcom/itau/empresas/ui/util/ValorMonetarioUtils;->formataValorMonetarioParaDecimal(Ljava/lang/String;Z)Ljava/math/BigDecimal;
    :try_end_1a
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_1a} :catch_1c

    move-result-object v0

    .line 634
    :goto_1b
    return-object v0

    .line 638
    :catch_1c
    move-exception v2

    .line 639
    .local v2, "e":Ljava/text/ParseException;
    const-string v0, "ERRO"

    invoke-virtual {v2}, Ljava/text/ParseException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 641
    .end local v2    # "e":Ljava/text/ParseException;
    new-instance v0, Ljava/math/BigDecimal;

    const-string v1, "0.00"

    invoke-direct {v0, v1}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public abreActivityListaCnpj()V
    .registers 3

    .line 279
    invoke-static {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ListaCnpjPagadoresActivity;->intent(Landroid/content/Context;)Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ListaCnpjPagadoresActivity_$IntentBuilder_;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->cnpjVOList:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ListaCnpjPagadoresActivity_$IntentBuilder_;->cnpjVOList(Ljava/util/ArrayList;)Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ListaCnpjPagadoresActivity_$IntentBuilder_;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ListaCnpjPagadoresActivity_$IntentBuilder_;->startForResult(I)Lorg/androidannotations/api/builder/PostActivityStarter;

    .line 280
    return-void
.end method

.method public aoClicarAutorizacao()V
    .registers 2

    .line 294
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->autorizar:Z

    .line 295
    invoke-direct {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->validaCamposObrigatorios()V

    .line 296
    return-void
.end method

.method public aoClicarDataApuracao()V
    .registers 1

    .line 289
    invoke-direct {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->openDatePickerApuracao()V

    .line 290
    return-void
.end method

.method public aoClicarDataPagamento()V
    .registers 1

    .line 284
    invoke-direct {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->openDatePickerPagamento()V

    .line 285
    return-void
.end method

.method public aoClicarDataVencimento()V
    .registers 1

    .line 306
    invoke-direct {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->openDatePickerVencimento()V

    .line 307
    return-void
.end method

.method public atualizaDadosPerfilLogado(Lcom/itau/empresas/api/model/PerfilOperadorVO;)V
    .registers 5
    .param p1, "perfilOperadorVO"    # Lcom/itau/empresas/api/model/PerfilOperadorVO;

    .line 220
    if-eqz p1, :cond_6a

    .line 221
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->campoNomeDaEmpresa:Landroid/widget/TextView;

    .line 222
    invoke-virtual {p1}, Lcom/itau/empresas/api/model/PerfilOperadorVO;->getNomeEmpresa()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_17

    .line 223
    invoke-virtual {p1}, Lcom/itau/empresas/api/model/PerfilOperadorVO;->getNomeEmpresa()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    goto :goto_19

    :cond_17
    const-string v1, ""

    .line 221
    :goto_19
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 224
    .line 225
    invoke-virtual {p1}, Lcom/itau/empresas/api/model/PerfilOperadorVO;->getContasOperador()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_32

    .line 226
    invoke-virtual {p1}, Lcom/itau/empresas/api/model/PerfilOperadorVO;->getContasOperador()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/api/model/ContaOperadorVO;

    goto :goto_37

    :cond_32
    new-instance v0, Lcom/itau/empresas/api/model/ContaOperadorVO;

    invoke-direct {v0}, Lcom/itau/empresas/api/model/ContaOperadorVO;-><init>()V

    :goto_37
    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->contaOperadorVO:Lcom/itau/empresas/api/model/ContaOperadorVO;

    .line 227
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->campoAgenciaConta:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->contaOperadorVO:Lcom/itau/empresas/api/model/ContaOperadorVO;

    .line 228
    invoke-virtual {v1}, Lcom/itau/empresas/api/model/ContaOperadorVO;->getAgencia()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_63

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->contaOperadorVO:Lcom/itau/empresas/api/model/ContaOperadorVO;

    .line 229
    invoke-virtual {v2}, Lcom/itau/empresas/api/model/ContaOperadorVO;->getAgencia()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->contaOperadorVO:Lcom/itau/empresas/api/model/ContaOperadorVO;

    .line 230
    invoke-virtual {v2}, Lcom/itau/empresas/api/model/ContaOperadorVO;->getConta()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    goto :goto_65

    :cond_63
    const-string v1, ""

    :goto_65
    check-cast v1, Ljava/lang/CharSequence;

    .line 227
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 232
    :cond_6a
    return-void
.end method

.method public atualizaValorPagamento(Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosDarf;)V
    .registers 5
    .param p1, "dadosDarf"    # Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosDarf;

    .line 340
    iput-object p1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->dadosDarf:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosDarf;

    .line 341
    invoke-virtual {p1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosDarf;->getValorAPagar()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1a

    .line 342
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->campoValorTotalPagarDarf:Landroid/widget/TextView;

    .line 344
    invoke-virtual {p1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosDarf;->getValorAPagar()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/itau/empresas/ui/util/ValorMonetarioUtils;->formataValorDecimalParaMonetario(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v1

    .line 342
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 346
    :cond_1a
    return-void
.end method

.method public atualizaValorTotal(Landroid/text/Editable;)V
    .registers 4
    .param p1, "text"    # Landroid/text/Editable;

    .line 243
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->valorPagamentoDarf:Lcom/itau/empresas/ui/util/listener/AtualizaValorPagamento;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/listener/AtualizaValorPagamento;->setValorPrincipal(Ljava/lang/String;)V

    .line 244
    return-void
.end method

.method public atualizaValorTotalComJuros(Landroid/text/Editable;)V
    .registers 4
    .param p1, "text"    # Landroid/text/Editable;

    .line 248
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->valorPagamentoDarf:Lcom/itau/empresas/ui/util/listener/AtualizaValorPagamento;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/listener/AtualizaValorPagamento;->setValorJuros(Ljava/lang/String;)V

    .line 249
    return-void
.end method

.method public atualizaValorTotalComMulta(Landroid/text/Editable;)V
    .registers 4
    .param p1, "text"    # Landroid/text/Editable;

    .line 253
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->valorPagamentoDarf:Lcom/itau/empresas/ui/util/listener/AtualizaValorPagamento;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/listener/AtualizaValorPagamento;->setValorMulta(Ljava/lang/String;)V

    .line 254
    return-void
.end method

.method public campoInvalido(Landroid/widget/EditText;)V
    .registers 2
    .param p1, "editText"    # Landroid/widget/EditText;

    .line 331
    return-void
.end method

.method public campoInvalido(Ljava/util/List;)V
    .registers 3
    .param p1, "editTextList"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<Landroid/widget/TextView;>;)V"
        }
    .end annotation

    .line 317
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->marcaCampoObrigatorio(Ljava/util/List;)V

    .line 318
    const/4 v0, 0x0

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-direct {p0, v0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->irParaCamporObrigatorio(Landroid/view/View;)V

    .line 319
    return-void
.end method

.method public campoValido()V
    .registers 2

    .line 323
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->incluiPagamentoDARF(Z)V

    .line 324
    return-void
.end method

.method public campoValido(Landroid/widget/EditText;)V
    .registers 4
    .param p1, "editText"    # Landroid/widget/EditText;

    .line 335
    invoke-virtual {p1}, Landroid/widget/EditText;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->trocaCorBackground(Landroid/view/ViewGroup;Z)V

    .line 336
    return-void
.end method

.method public carregaElementosDeTela()V
    .registers 3

    .line 159
    invoke-direct {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->consultaHorarioLimite()V

    .line 160
    invoke-direct {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->constroiToolbar()V

    .line 161
    invoke-direct {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->consultaAlcada()V

    .line 162
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->rlInclusaoDarf:Landroid/widget/RelativeLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 163
    new-instance v0, Lcom/itau/empresas/ui/util/listener/AtualizaValorPagamento;

    invoke-direct {v0, p0}, Lcom/itau/empresas/ui/util/listener/AtualizaValorPagamento;-><init>(Lcom/itau/empresas/ui/util/listener/AtualizaDadosActivityListener;)V

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->valorPagamentoDarf:Lcom/itau/empresas/ui/util/listener/AtualizaValorPagamento;

    .line 164
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->listaEditTextInvalido:Ljava/util/List;

    .line 165
    invoke-direct {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->inicializaCamposValoresDefault()V

    .line 166
    invoke-direct {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->adicionaEditTextParaValidacao()V

    .line 167
    invoke-direct {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->adicionaMensagemErroValidacao()V

    .line 168
    invoke-direct {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->consultaDadosCliente()V

    .line 169
    invoke-direct {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->consultaCnpjPagamentos()V

    .line 170
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->campoNomeContribuinte:Landroid/widget/EditText;

    const v1, 0x7f0e03b4

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setNextFocusDownId(I)V

    .line 171
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->campoCpfCnpjContribuinte:Landroid/widget/EditText;

    new-instance v1, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity$2;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity$2;-><init>(Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 181
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->campoCpfCnpjContribuinte:Landroid/widget/EditText;

    new-instance v1, Lbr/com/concretesolutions/canarinho/watcher/CPFCNPJTextWatcher;

    invoke-direct {v1}, Lbr/com/concretesolutions/canarinho/watcher/CPFCNPJTextWatcher;-><init>()V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 182
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->campoDataApuracao:Landroid/widget/EditText;

    new-instance v1, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity$3;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity$3;-><init>(Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 191
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->campoDataPagamento:Landroid/widget/TextView;

    new-instance v1, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity$4;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity$4;-><init>(Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 200
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->campoDataVencimento:Landroid/widget/EditText;

    new-instance v1, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity$5;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity$5;-><init>(Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 209
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->campoNomeContribuinte:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->onFocusCampoNomeContibuinte:Landroid/view/View$OnFocusChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 210
    return-void
.end method

.method public fechaTooltip()V
    .registers 2

    .line 258
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->hint:Lbr/com/itau/widgets/hintview/Hint$Builder;

    if-eqz v0, :cond_14

    iget-boolean v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->isTooltipWarning:Z

    if-eqz v0, :cond_14

    .line 259
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->isTooltipWarning:Z

    .line 260
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->hint:Lbr/com/itau/widgets/hintview/Hint$Builder;

    invoke-virtual {v0}, Lbr/com/itau/widgets/hintview/Hint$Builder;->build()Lbr/com/itau/widgets/hintview/Hint;

    move-result-object v0

    invoke-virtual {v0}, Lbr/com/itau/widgets/hintview/Hint;->dismiss()V

    .line 262
    :cond_14
    return-void
.end method

.method public inclui()V
    .registers 2

    .line 300
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->autorizar:Z

    .line 301
    invoke-direct {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->validaCamposObrigatorios()V

    .line 302
    return-void
.end method

.method public incluiPagamento()V
    .registers 2

    .line 271
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->editTextFocus:Landroid/widget/EditText;

    if-eqz v0, :cond_9

    .line 272
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->editTextFocus:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->clearFocus()V

    .line 274
    :cond_9
    invoke-direct {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->validaCamposObrigatorios()V

    .line 275
    return-void
.end method

.method public invalido(Ljava/lang/String;Ljava/lang/String;)V
    .registers 4
    .param p1, "valorAtual"    # Ljava/lang/String;
    .param p2, "mensagem"    # Ljava/lang/String;

    .line 350
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->campoCpfCnpjContribuinte:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->hasFocus()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 351
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->campoCpfCnpjContribuinte:Landroid/widget/EditText;

    invoke-direct {p0, v0, p2}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->mostraAvisoDeCampoObrigatorio(Landroid/view/View;Ljava/lang/String;)V

    .line 354
    :cond_d
    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .registers 8
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .line 437
    const/4 v0, -0x1

    if-ne p2, v0, :cond_2c

    const-string v0, "cnpj"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2c

    .line 438
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "cnpj"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/CnpjVO;

    .line 439
    .local v3, "cnpjVO":Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/CnpjVO;
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->campoCnpjPagador:Landroid/widget/TextView;

    sget-object v1, Lbr/com/concretesolutions/canarinho/formatador/Formatador;->CNPJ:Lbr/com/concretesolutions/canarinho/formatador/Formatador;

    if-eqz v3, :cond_23

    .line 440
    invoke-virtual {v3}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/CnpjVO;->getCnpj()Ljava/lang/String;

    move-result-object v2

    goto :goto_25

    :cond_23
    const-string v2, ""

    :goto_25
    invoke-interface {v1, v2}, Lbr/com/concretesolutions/canarinho/formatador/Formatador;->formata(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 442
    .end local v3    # "cnpjVO":Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/CnpjVO;
    :cond_2c
    return-void
.end method

.method public onEventMainThread(Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent$Conversion;)V
    .registers 5
    .param p1, "e"    # Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent$Conversion;

    .line 385
    invoke-static {p1, p0}, Lcom/itau/empresas/api/ApiHelper;->devoTratarEvento(Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;Lcom/itau/empresas/api/ApiConsumidor;)Z

    move-result v0

    if-nez v0, :cond_1c

    .line 386
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->root:Landroid/widget/LinearLayout;

    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent$Conversion;->getException()Lbr/com/itau/sdk/android/core/exception/BackendException;

    move-result-object v1

    invoke-virtual {v1}, Lbr/com/itau/sdk/android/core/exception/BackendException;->getError()Lbr/com/itau/sdk/android/core/model/ErrorDTO;

    move-result-object v1

    invoke-virtual {v1}, Lbr/com/itau/sdk/android/core/model/ErrorDTO;->getMensagem()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lbr/com/itau/topsnackbar/TopSnackbar;->make(Landroid/view/ViewGroup;Ljava/lang/CharSequence;I)Lbr/com/itau/topsnackbar/TopSnackbar;

    move-result-object v0

    .line 387
    invoke-virtual {v0}, Lbr/com/itau/topsnackbar/TopSnackbar;->show()Lbr/com/itau/topsnackbar/TopSnackbar;

    .line 389
    :cond_1c
    return-void
.end method

.method public onEventMainThread(Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent$HTTP;)V
    .registers 5
    .param p1, "exceptionEvent"    # Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent$HTTP;

    .line 371
    invoke-static {p1, p0}, Lcom/itau/empresas/api/ApiHelper;->devoTratarEvento(Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;Lcom/itau/empresas/api/ApiConsumidor;)Z

    move-result v0

    if-nez v0, :cond_1c

    .line 372
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->root:Landroid/widget/LinearLayout;

    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent$HTTP;->getException()Lbr/com/itau/sdk/android/core/exception/BackendException;

    move-result-object v1

    invoke-virtual {v1}, Lbr/com/itau/sdk/android/core/exception/BackendException;->getError()Lbr/com/itau/sdk/android/core/model/ErrorDTO;

    move-result-object v1

    invoke-virtual {v1}, Lbr/com/itau/sdk/android/core/model/ErrorDTO;->getMensagem()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lbr/com/itau/topsnackbar/TopSnackbar;->make(Landroid/view/ViewGroup;Ljava/lang/CharSequence;I)Lbr/com/itau/topsnackbar/TopSnackbar;

    move-result-object v0

    .line 373
    invoke-virtual {v0}, Lbr/com/itau/topsnackbar/TopSnackbar;->show()Lbr/com/itau/topsnackbar/TopSnackbar;

    .line 375
    :cond_1c
    return-void
.end method

.method public onEventMainThread(Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent$Network;)V
    .registers 5
    .param p1, "exceptionEvent"    # Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent$Network;

    .line 378
    invoke-static {p1, p0}, Lcom/itau/empresas/api/ApiHelper;->devoTratarEvento(Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;Lcom/itau/empresas/api/ApiConsumidor;)Z

    move-result v0

    if-nez v0, :cond_1c

    .line 379
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->root:Landroid/widget/LinearLayout;

    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent$Network;->getException()Lbr/com/itau/sdk/android/core/exception/BackendException;

    move-result-object v1

    invoke-virtual {v1}, Lbr/com/itau/sdk/android/core/exception/BackendException;->getError()Lbr/com/itau/sdk/android/core/model/ErrorDTO;

    move-result-object v1

    invoke-virtual {v1}, Lbr/com/itau/sdk/android/core/model/ErrorDTO;->getMensagem()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lbr/com/itau/topsnackbar/TopSnackbar;->make(Landroid/view/ViewGroup;Ljava/lang/CharSequence;I)Lbr/com/itau/topsnackbar/TopSnackbar;

    move-result-object v0

    .line 380
    invoke-virtual {v0}, Lbr/com/itau/topsnackbar/TopSnackbar;->show()Lbr/com/itau/topsnackbar/TopSnackbar;

    .line 382
    :cond_1c
    return-void
.end method

.method public onEventMainThread(Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;)V
    .registers 5
    .param p1, "e"    # Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;

    .line 409
    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;->getException()Lbr/com/itau/sdk/android/core/exception/BackendException;

    move-result-object v0

    invoke-virtual {v0}, Lbr/com/itau/sdk/android/core/exception/BackendException;->getError()Lbr/com/itau/sdk/android/core/model/ErrorDTO;

    move-result-object v0

    invoke-virtual {v0}, Lbr/com/itau/sdk/android/core/model/ErrorDTO;->getCampos()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_6b

    .line 410
    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;->getException()Lbr/com/itau/sdk/android/core/exception/BackendException;

    move-result-object v0

    invoke-virtual {v0}, Lbr/com/itau/sdk/android/core/exception/BackendException;->getError()Lbr/com/itau/sdk/android/core/model/ErrorDTO;

    move-result-object v0

    invoke-virtual {v0}, Lbr/com/itau/sdk/android/core/model/ErrorDTO;->getCampos()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_42

    .line 411
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->root:Landroid/widget/LinearLayout;

    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;->getException()Lbr/com/itau/sdk/android/core/exception/BackendException;

    move-result-object v1

    invoke-virtual {v1}, Lbr/com/itau/sdk/android/core/exception/BackendException;->getError()Lbr/com/itau/sdk/android/core/model/ErrorDTO;

    move-result-object v1

    invoke-virtual {v1}, Lbr/com/itau/sdk/android/core/model/ErrorDTO;->getCampos()Ljava/util/List;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lbr/com/itau/sdk/android/core/model/CampoErroDTO;

    invoke-virtual {v1}, Lbr/com/itau/sdk/android/core/model/CampoErroDTO;->getMensagem()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lbr/com/itau/topsnackbar/TopSnackbar;->make(Landroid/view/ViewGroup;Ljava/lang/CharSequence;I)Lbr/com/itau/topsnackbar/TopSnackbar;

    move-result-object v0

    .line 412
    invoke-virtual {v0}, Lbr/com/itau/topsnackbar/TopSnackbar;->show()Lbr/com/itau/topsnackbar/TopSnackbar;

    goto :goto_93

    .line 414
    :cond_42
    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;->getException()Lbr/com/itau/sdk/android/core/exception/BackendException;

    move-result-object v0

    invoke-virtual {v0}, Lbr/com/itau/sdk/android/core/exception/BackendException;->getError()Lbr/com/itau/sdk/android/core/model/ErrorDTO;

    move-result-object v0

    invoke-virtual {v0}, Lbr/com/itau/sdk/android/core/model/ErrorDTO;->getMensagem()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_93

    .line 415
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->root:Landroid/widget/LinearLayout;

    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;->getException()Lbr/com/itau/sdk/android/core/exception/BackendException;

    move-result-object v1

    invoke-virtual {v1}, Lbr/com/itau/sdk/android/core/exception/BackendException;->getError()Lbr/com/itau/sdk/android/core/model/ErrorDTO;

    move-result-object v1

    invoke-virtual {v1}, Lbr/com/itau/sdk/android/core/model/ErrorDTO;->getMensagem()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lbr/com/itau/topsnackbar/TopSnackbar;->make(Landroid/view/ViewGroup;Ljava/lang/CharSequence;I)Lbr/com/itau/topsnackbar/TopSnackbar;

    move-result-object v0

    .line 416
    invoke-virtual {v0}, Lbr/com/itau/topsnackbar/TopSnackbar;->show()Lbr/com/itau/topsnackbar/TopSnackbar;

    goto :goto_93

    .line 419
    :cond_6b
    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;->getException()Lbr/com/itau/sdk/android/core/exception/BackendException;

    move-result-object v0

    invoke-virtual {v0}, Lbr/com/itau/sdk/android/core/exception/BackendException;->getError()Lbr/com/itau/sdk/android/core/model/ErrorDTO;

    move-result-object v0

    invoke-virtual {v0}, Lbr/com/itau/sdk/android/core/model/ErrorDTO;->getMensagem()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_93

    .line 420
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->root:Landroid/widget/LinearLayout;

    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;->getException()Lbr/com/itau/sdk/android/core/exception/BackendException;

    move-result-object v1

    invoke-virtual {v1}, Lbr/com/itau/sdk/android/core/exception/BackendException;->getError()Lbr/com/itau/sdk/android/core/model/ErrorDTO;

    move-result-object v1

    invoke-virtual {v1}, Lbr/com/itau/sdk/android/core/model/ErrorDTO;->getMensagem()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lbr/com/itau/topsnackbar/TopSnackbar;->make(Landroid/view/ViewGroup;Ljava/lang/CharSequence;I)Lbr/com/itau/topsnackbar/TopSnackbar;

    move-result-object v0

    .line 421
    invoke-virtual {v0}, Lbr/com/itau/topsnackbar/TopSnackbar;->show()Lbr/com/itau/topsnackbar/TopSnackbar;

    .line 423
    :cond_93
    :goto_93
    return-void
.end method

.method public onEventMainThread(Lcom/itau/empresas/api/model/AlcadaVO;)V
    .registers 3
    .param p1, "alcadaVO"    # Lcom/itau/empresas/api/model/AlcadaVO;

    .line 310
    invoke-virtual {p1}, Lcom/itau/empresas/api/model/AlcadaVO;->getContasAPagarVO()Lcom/itau/empresas/api/model/ContasAPagarVO;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/api/model/ContasAPagarVO;->getTributosAlcadaVO()Lcom/itau/empresas/api/model/TributosAlcadaVO;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/api/model/TributosAlcadaVO;->isAutorizante()Z

    move-result v0

    if-eqz v0, :cond_1e

    .line 311
    invoke-virtual {p1}, Lcom/itau/empresas/api/model/AlcadaVO;->getContasAPagarVO()Lcom/itau/empresas/api/model/ContasAPagarVO;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/api/model/ContasAPagarVO;->getObrigatorioVistadorVO()Lcom/itau/empresas/api/model/ObrigatorioVistadorVO;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/api/model/ObrigatorioVistadorVO;->isAutorizante()Z

    move-result v0

    if-nez v0, :cond_1e

    const/4 v0, 0x1

    goto :goto_1f

    :cond_1e
    const/4 v0, 0x0

    :goto_1f
    iput-boolean v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->alcadaAutorizante:Z

    .line 312
    invoke-direct {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->mostraBotaoAutorizar()V

    .line 313
    return-void
.end method

.method public onEventMainThread(Lcom/itau/empresas/api/model/HorarioLimiteTransacoesVO;)V
    .registers 4
    .param p1, "horarioLimiteTransacoesVO"    # Lcom/itau/empresas/api/model/HorarioLimiteTransacoesVO;

    .line 426
    invoke-virtual {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->escondeDialogoDeProgresso()V

    .line 427
    if-eqz p1, :cond_25

    .line 428
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->rlHorarioLimite:Landroid/widget/RelativeLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 429
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->campoHorarioLimiteInicial:Landroid/widget/TextView;

    .line 430
    invoke-virtual {p1}, Lcom/itau/empresas/api/model/HorarioLimiteTransacoesVO;->getHorarioInclusao()Ljava/lang/String;

    move-result-object v1

    .line 429
    invoke-static {v1}, Lcom/itau/empresas/ui/util/formatacao/FormataHorario;->HoraMinutoSegundoParaHoraMinuto(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 431
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->campoHorarioLimiteFinal:Landroid/widget/TextView;

    .line 432
    invoke-virtual {p1}, Lcom/itau/empresas/api/model/HorarioLimiteTransacoesVO;->getHorarioAutorizacao()Ljava/lang/String;

    move-result-object v1

    .line 431
    invoke-static {v1}, Lcom/itau/empresas/ui/util/formatacao/FormataHorario;->HoraMinutoSegundoParaHoraMinuto(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 434
    :cond_25
    return-void
.end method

.method public onEventMainThread(Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/InclusaoTributoResponseVO;)V
    .registers 3
    .param p1, "inclusaoTributoResponseVO"    # Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/InclusaoTributoResponseVO;

    .line 392
    invoke-virtual {p1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/InclusaoTributoResponseVO;->isDuplicado()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 393
    invoke-direct {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->mostraDialogDuplicidade()V

    goto :goto_d

    .line 395
    :cond_a
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->abreTelaDeInclusaoSucesso(Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/InclusaoTributoResponseVO;)V

    .line 397
    :goto_d
    return-void
.end method

.method public onEventMainThread(Ljava/util/List;)V
    .registers 5
    .param p1, "cnpjVOList"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/CnpjVO;>;)V"
        }
    .end annotation

    .line 400
    invoke-virtual {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->escondeDialogoDeProgresso()V

    .line 401
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->cnpjVOList:Ljava/util/ArrayList;

    .line 402
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2b

    .line 403
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->cnpjVOList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 404
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->campoCnpjPagador:Landroid/widget/TextView;

    sget-object v1, Lbr/com/concretesolutions/canarinho/formatador/Formatador;->CNPJ:Lbr/com/concretesolutions/canarinho/formatador/Formatador;

    const/4 v2, 0x0

    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/CnpjVO;

    invoke-virtual {v2}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/CnpjVO;->getCnpj()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lbr/com/concretesolutions/canarinho/formatador/Formatador;->formata(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 406
    :cond_2b
    return-void
.end method

.method public opkeysConsumidas()Ljava/util/List;
    .registers 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Ljava/lang/String;>;"
        }
    .end annotation

    .line 236
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "pagamentosCNPJPagadores"

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const-string v1, "horariosLimiteSISPAG"

    const/4 v2, 0x1

    aput-object v1, v0, v2

    const-string v1, "pagamentoTributoDarfPJIncluirSimular"

    const/4 v2, 0x2

    aput-object v1, v0, v2

    const-string v1, "pagamentoTributoDarfPJIncluirEfetivar"

    const/4 v2, 0x3

    aput-object v1, v0, v2

    const-string v1, "alcadas"

    const/4 v2, 0x4

    aput-object v1, v0, v2

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public parcialmenteValido(Ljava/lang/String;)V
    .registers 2
    .param p1, "valorAtual"    # Ljava/lang/String;

    .line 361
    return-void
.end method

.method public totalmenteValido(Ljava/lang/String;)V
    .registers 2
    .param p1, "valorAtual"    # Ljava/lang/String;

    .line 368
    return-void
.end method

.method public validaData()V
    .registers 1

    .line 266
    invoke-direct {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->validaDataVencimento()V

    .line 267
    return-void
.end method

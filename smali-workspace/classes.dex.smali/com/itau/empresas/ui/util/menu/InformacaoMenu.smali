.class public Lcom/itau/empresas/ui/util/menu/InformacaoMenu;
.super Ljava/lang/Object;
.source "InformacaoMenu.java"


# static fields
.field private static final PREFIXOS_WEB_VIEW:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Ljava/lang/String;>;"
        }
    .end annotation
.end field

.field private static final menuFixo:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<Ljava/lang/String;Lcom/itau/empresas/ui/util/menu/InformacaoMenu;>;"
        }
    .end annotation
.end field


# instance fields
.field private chave:Ljava/lang/String;

.field private command:Lcom/itau/empresas/ui/util/menu/Command;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .line 50
    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "NET_"

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const-string v1, "OLAS_"

    const/4 v2, 0x1

    aput-object v1, v0, v2

    const-string v1, "PAG_"

    const/4 v2, 0x2

    aput-object v1, v0, v2

    const-string v1, "IL25_"

    const/4 v2, 0x3

    aput-object v1, v0, v2

    const-string v1, "DESC_"

    const/4 v2, 0x4

    aput-object v1, v0, v2

    const-string v1, "APLIC_"

    const/4 v2, 0x5

    aput-object v1, v0, v2

    const-string v1, "R_"

    const/4 v2, 0x6

    aput-object v1, v0, v2

    const-string v1, "ORDEM_"

    const/4 v2, 0x7

    aput-object v1, v0, v2

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/itau/empresas/ui/util/menu/InformacaoMenu;->PREFIXOS_WEB_VIEW:Ljava/util/List;

    .line 54
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/itau/empresas/ui/util/menu/InformacaoMenu;->menuFixo:Ljava/util/Map;

    .line 57
    const-string v0, "inicio"

    new-instance v1, Lcom/itau/empresas/ui/util/menu/ActivityCommand;

    const-class v2, Lcom/itau/empresas/feature/home/HomeLogadaActivity_;

    invoke-direct {v1, v2}, Lcom/itau/empresas/ui/util/menu/ActivityCommand;-><init>(Ljava/lang/Class;)V

    invoke-static {v0, v1}, Lcom/itau/empresas/ui/util/menu/InformacaoMenu;->itemMenuFixo(Ljava/lang/String;Lcom/itau/empresas/ui/util/menu/Command;)Lcom/itau/empresas/ui/util/menu/InformacaoMenu;

    .line 59
    const-string v0, "tabbar_extrato"

    new-instance v1, Lcom/itau/empresas/ui/util/menu/ExtratoCommand;

    const-class v2, Lcom/itau/empresas/feature/home/HomeLogadaActivity_;

    invoke-direct {v1, v2}, Lcom/itau/empresas/ui/util/menu/ExtratoCommand;-><init>(Ljava/lang/Class;)V

    invoke-static {v0, v1}, Lcom/itau/empresas/ui/util/menu/InformacaoMenu;->itemMenuFixo(Ljava/lang/String;Lcom/itau/empresas/ui/util/menu/Command;)Lcom/itau/empresas/ui/util/menu/InformacaoMenu;

    .line 60
    const-string v0, "transacoes_transferencias_recebidas"

    new-instance v1, Lcom/itau/empresas/ui/util/menu/ActivityCommand;

    const-class v2, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity_;

    invoke-direct {v1, v2}, Lcom/itau/empresas/ui/util/menu/ActivityCommand;-><init>(Ljava/lang/Class;)V

    invoke-static {v0, v1}, Lcom/itau/empresas/ui/util/menu/InformacaoMenu;->itemMenuFixo(Ljava/lang/String;Lcom/itau/empresas/ui/util/menu/Command;)Lcom/itau/empresas/ui/util/menu/InformacaoMenu;

    .line 64
    const-string v0, "transacoes_autorizacao_de_transacoes"

    new-instance v1, Lcom/itau/empresas/ui/util/menu/PedagioAutorizarCommand;

    invoke-direct {v1}, Lcom/itau/empresas/ui/util/menu/PedagioAutorizarCommand;-><init>()V

    invoke-static {v0, v1}, Lcom/itau/empresas/ui/util/menu/InformacaoMenu;->itemMenuFixo(Ljava/lang/String;Lcom/itau/empresas/ui/util/menu/Command;)Lcom/itau/empresas/ui/util/menu/InformacaoMenu;

    .line 65
    const-string v0, "transacoes_com_codigo_de_barras"

    new-instance v1, Lcom/itau/empresas/ui/util/menu/ActivityCommand;

    const-class v2, Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity_;

    invoke-direct {v1, v2}, Lcom/itau/empresas/ui/util/menu/ActivityCommand;-><init>(Ljava/lang/Class;)V

    invoke-static {v0, v1}, Lcom/itau/empresas/ui/util/menu/InformacaoMenu;->itemMenuFixo(Ljava/lang/String;Lcom/itau/empresas/ui/util/menu/Command;)Lcom/itau/empresas/ui/util/menu/InformacaoMenu;

    .line 67
    const-string v0, "pedagio_transferencias_doc_ted_tef"

    new-instance v1, Lcom/itau/empresas/ui/util/menu/PedagioTransferenciaCommand;

    invoke-direct {v1}, Lcom/itau/empresas/ui/util/menu/PedagioTransferenciaCommand;-><init>()V

    invoke-static {v0, v1}, Lcom/itau/empresas/ui/util/menu/InformacaoMenu;->itemMenuFixo(Ljava/lang/String;Lcom/itau/empresas/ui/util/menu/Command;)Lcom/itau/empresas/ui/util/menu/InformacaoMenu;

    .line 68
    const-string v0, "transacoes_impostos_e_tributos"

    new-instance v1, Lcom/itau/empresas/ui/util/menu/ActivityCommand;

    const-class v2, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/SelecaoTributoPagamentoActivity_;

    invoke-direct {v1, v2}, Lcom/itau/empresas/ui/util/menu/ActivityCommand;-><init>(Ljava/lang/Class;)V

    invoke-static {v0, v1}, Lcom/itau/empresas/ui/util/menu/InformacaoMenu;->itemMenuFixo(Ljava/lang/String;Lcom/itau/empresas/ui/util/menu/Command;)Lcom/itau/empresas/ui/util/menu/InformacaoMenu;

    .line 71
    const-string v0, "transacoes_nova_recarga"

    new-instance v1, Lcom/itau/empresas/ui/util/menu/ActivityCommand;

    const-class v2, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaContatosActivity_;

    invoke-direct {v1, v2}, Lcom/itau/empresas/ui/util/menu/ActivityCommand;-><init>(Ljava/lang/Class;)V

    invoke-static {v0, v1}, Lcom/itau/empresas/ui/util/menu/InformacaoMenu;->itemMenuFixo(Ljava/lang/String;Lcom/itau/empresas/ui/util/menu/Command;)Lcom/itau/empresas/ui/util/menu/InformacaoMenu;

    .line 74
    const-string v0, "NET_APP_EPLUS_FORMALIZACAOREMOTA"

    new-instance v1, Lcom/itau/empresas/ui/util/menu/WebViewActivityCommand;

    invoke-direct {v1}, Lcom/itau/empresas/ui/util/menu/WebViewActivityCommand;-><init>()V

    invoke-static {v0, v1}, Lcom/itau/empresas/ui/util/menu/InformacaoMenu;->itemMenuFixo(Ljava/lang/String;Lcom/itau/empresas/ui/util/menu/Command;)Lcom/itau/empresas/ui/util/menu/InformacaoMenu;

    .line 77
    const-string v0, "transacoes_comprovantes"

    new-instance v1, Lcom/itau/empresas/ui/util/menu/ActivityCommand;

    const-class v2, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesActivity_;

    invoke-direct {v1, v2}, Lcom/itau/empresas/ui/util/menu/ActivityCommand;-><init>(Ljava/lang/Class;)V

    invoke-static {v0, v1}, Lcom/itau/empresas/ui/util/menu/InformacaoMenu;->itemMenuFixo(Ljava/lang/String;Lcom/itau/empresas/ui/util/menu/Command;)Lcom/itau/empresas/ui/util/menu/InformacaoMenu;

    .line 80
    const-string v0, "troca_de_senha"

    new-instance v1, Lcom/itau/empresas/ui/util/menu/FragmentCommand;

    const-class v2, Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment_;

    invoke-direct {v1, v2}, Lcom/itau/empresas/ui/util/menu/FragmentCommand;-><init>(Ljava/lang/Class;)V

    invoke-static {v0, v1}, Lcom/itau/empresas/ui/util/menu/InformacaoMenu;->itemMenuFixo(Ljava/lang/String;Lcom/itau/empresas/ui/util/menu/Command;)Lcom/itau/empresas/ui/util/menu/InformacaoMenu;

    .line 81
    const-string v0, "token_app"

    new-instance v1, Lcom/itau/empresas/ui/util/menu/ActivityCommand;

    const-class v2, Lcom/itau/empresas/feature/itoken/ITokenActivity_;

    invoke-direct {v1, v2}, Lcom/itau/empresas/ui/util/menu/ActivityCommand;-><init>(Ljava/lang/Class;)V

    invoke-static {v0, v1}, Lcom/itau/empresas/ui/util/menu/InformacaoMenu;->itemMenuFixo(Ljava/lang/String;Lcom/itau/empresas/ui/util/menu/Command;)Lcom/itau/empresas/ui/util/menu/InformacaoMenu;

    .line 84
    const-string v0, "servico_cheque_especial"

    new-instance v1, Lcom/itau/empresas/ui/util/menu/ActivityCommand;

    const-class v2, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaActivity_;

    invoke-direct {v1, v2}, Lcom/itau/empresas/ui/util/menu/ActivityCommand;-><init>(Ljava/lang/Class;)V

    invoke-static {v0, v1}, Lcom/itau/empresas/ui/util/menu/InformacaoMenu;->itemMenuFixo(Ljava/lang/String;Lcom/itau/empresas/ui/util/menu/Command;)Lcom/itau/empresas/ui/util/menu/InformacaoMenu;

    .line 85
    const-string v0, "servico_emprestimo_parcelado"

    new-instance v1, Lcom/itau/empresas/ui/util/menu/ActivityCommand;

    const-class v2, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity_;

    invoke-direct {v1, v2}, Lcom/itau/empresas/ui/util/menu/ActivityCommand;-><init>(Ljava/lang/Class;)V

    invoke-static {v0, v1}, Lcom/itau/empresas/ui/util/menu/InformacaoMenu;->itemMenuFixo(Ljava/lang/String;Lcom/itau/empresas/ui/util/menu/Command;)Lcom/itau/empresas/ui/util/menu/InformacaoMenu;

    .line 89
    const-string v0, "itau_empresas_beta"

    new-instance v1, Lcom/itau/empresas/ui/util/menu/ActivityCommand;

    const-class v2, Lcom/itau/empresas/ui/activity/BetaActivity_;

    invoke-direct {v1, v2}, Lcom/itau/empresas/ui/util/menu/ActivityCommand;-><init>(Ljava/lang/Class;)V

    invoke-static {v0, v1}, Lcom/itau/empresas/ui/util/menu/InformacaoMenu;->itemMenuFixo(Ljava/lang/String;Lcom/itau/empresas/ui/util/menu/Command;)Lcom/itau/empresas/ui/util/menu/InformacaoMenu;

    .line 92
    const-string v0, "tabbar_ajuda"

    new-instance v1, Lcom/itau/empresas/ui/util/menu/AtendimentoCommand;

    const-class v2, Lcom/itau/empresas/feature/home/HomeLogadaActivity_;

    invoke-direct {v1, v2}, Lcom/itau/empresas/ui/util/menu/AtendimentoCommand;-><init>(Ljava/lang/Class;)V

    invoke-static {v0, v1}, Lcom/itau/empresas/ui/util/menu/InformacaoMenu;->itemMenuFixo(Ljava/lang/String;Lcom/itau/empresas/ui/util/menu/Command;)Lcom/itau/empresas/ui/util/menu/InformacaoMenu;

    .line 94
    const-string v0, "fale_sobre_o_app"

    new-instance v1, Lcom/itau/empresas/ui/util/menu/FeedbackCommand;

    invoke-direct {v1}, Lcom/itau/empresas/ui/util/menu/FeedbackCommand;-><init>()V

    invoke-static {v0, v1}, Lcom/itau/empresas/ui/util/menu/InformacaoMenu;->itemMenuFixo(Ljava/lang/String;Lcom/itau/empresas/ui/util/menu/Command;)Lcom/itau/empresas/ui/util/menu/InformacaoMenu;

    .line 95
    const-string v0, "ajuda"

    new-instance v1, Lcom/itau/empresas/ui/util/menu/FragmentCommand;

    const-class v2, Lcom/itau/empresas/ui/fragment/OutrosFragment_;

    invoke-direct {v1, v2}, Lcom/itau/empresas/ui/util/menu/FragmentCommand;-><init>(Ljava/lang/Class;)V

    invoke-static {v0, v1}, Lcom/itau/empresas/ui/util/menu/InformacaoMenu;->itemMenuFixo(Ljava/lang/String;Lcom/itau/empresas/ui/util/menu/Command;)Lcom/itau/empresas/ui/util/menu/InformacaoMenu;

    .line 96
    const-string v0, "sair"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/itau/empresas/ui/util/menu/InformacaoMenu;->itemMenuFixo(Ljava/lang/String;Lcom/itau/empresas/ui/util/menu/Command;)Lcom/itau/empresas/ui/util/menu/InformacaoMenu;

    .line 98
    const-string v0, ""

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/itau/empresas/ui/util/menu/InformacaoMenu;->itemMenuFixo(Ljava/lang/String;Lcom/itau/empresas/ui/util/menu/Command;)Lcom/itau/empresas/ui/util/menu/InformacaoMenu;

    .line 99
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Lcom/itau/empresas/ui/util/menu/Command;)V
    .registers 3
    .param p1, "chave"    # Ljava/lang/String;
    .param p2, "command"    # Lcom/itau/empresas/ui/util/menu/Command;

    .line 104
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 105
    iput-object p1, p0, Lcom/itau/empresas/ui/util/menu/InformacaoMenu;->chave:Ljava/lang/String;

    .line 106
    iput-object p2, p0, Lcom/itau/empresas/ui/util/menu/InformacaoMenu;->command:Lcom/itau/empresas/ui/util/menu/Command;

    .line 107
    return-void
.end method

.method public static getInstance(Ljava/lang/String;)Lcom/itau/empresas/ui/util/menu/InformacaoMenu;
    .registers 5
    .param p0, "chave"    # Ljava/lang/String;

    .line 121
    if-nez p0, :cond_d

    .line 122
    sget-object v0, Lcom/itau/empresas/ui/util/menu/InformacaoMenu;->menuFixo:Ljava/util/Map;

    const-string v1, ""

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/ui/util/menu/InformacaoMenu;

    return-object v0

    .line 125
    :cond_d
    sget-object v0, Lcom/itau/empresas/ui/util/menu/InformacaoMenu;->menuFixo:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1e

    .line 126
    sget-object v0, Lcom/itau/empresas/ui/util/menu/InformacaoMenu;->menuFixo:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/ui/util/menu/InformacaoMenu;

    return-object v0

    .line 129
    :cond_1e
    sget-object v0, Lcom/itau/empresas/ui/util/menu/InformacaoMenu;->PREFIXOS_WEB_VIEW:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_24
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_43

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Ljava/lang/String;

    .line 130
    .local v3, "prefixo":Ljava/lang/String;
    invoke-virtual {p0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_42

    .line 131
    new-instance v0, Lcom/itau/empresas/ui/util/menu/InformacaoMenu;

    new-instance v1, Lcom/itau/empresas/ui/util/menu/WebViewActivityCommand;

    invoke-direct {v1}, Lcom/itau/empresas/ui/util/menu/WebViewActivityCommand;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/itau/empresas/ui/util/menu/InformacaoMenu;-><init>(Ljava/lang/String;Lcom/itau/empresas/ui/util/menu/Command;)V

    return-object v0

    .line 133
    .end local v3    # "prefixo":Ljava/lang/String;
    :cond_42
    goto :goto_24

    .line 135
    :cond_43
    sget-object v0, Lcom/itau/empresas/ui/util/menu/InformacaoMenu;->menuFixo:Ljava/util/Map;

    const-string v1, ""

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/ui/util/menu/InformacaoMenu;

    return-object v0
.end method

.method private static itemMenuFixo(Ljava/lang/String;Lcom/itau/empresas/ui/util/menu/Command;)Lcom/itau/empresas/ui/util/menu/InformacaoMenu;
    .registers 4
    .param p0, "chave"    # Ljava/lang/String;
    .param p1, "command"    # Lcom/itau/empresas/ui/util/menu/Command;

    .line 110
    sget-object v0, Lcom/itau/empresas/ui/util/menu/InformacaoMenu;->menuFixo:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_13

    .line 111
    new-instance v1, Lcom/itau/empresas/ui/util/menu/InformacaoMenu;

    invoke-direct {v1, p0, p1}, Lcom/itau/empresas/ui/util/menu/InformacaoMenu;-><init>(Ljava/lang/String;Lcom/itau/empresas/ui/util/menu/Command;)V

    .line 112
    .local v1, "infoMenu":Lcom/itau/empresas/ui/util/menu/InformacaoMenu;
    sget-object v0, Lcom/itau/empresas/ui/util/menu/InformacaoMenu;->menuFixo:Ljava/util/Map;

    invoke-interface {v0, p0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 113
    return-object v1

    .line 115
    .end local v1    # "infoMenu":Lcom/itau/empresas/ui/util/menu/InformacaoMenu;
    :cond_13
    sget-object v0, Lcom/itau/empresas/ui/util/menu/InformacaoMenu;->menuFixo:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/ui/util/menu/InformacaoMenu;

    return-object v0
.end method


# virtual methods
.method public getCommand()Lcom/itau/empresas/ui/util/menu/Command;
    .registers 2

    .line 143
    iget-object v0, p0, Lcom/itau/empresas/ui/util/menu/InformacaoMenu;->command:Lcom/itau/empresas/ui/util/menu/Command;

    return-object v0
.end method

.class public Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ListaCnpjPagadoresActivity;
.super Lcom/itau/empresas/ui/activity/BaseActivity;
.source "ListaCnpjPagadoresActivity.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field cnpjPagamentosAdapter:Lcom/itau/empresas/feature/pagamento/semcodigobarra/adapter/CnpjPagamentosAdapter;

.field cnpjVOList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/CnpjVO;>;"
        }
    .end annotation
.end field

.field listaCnpj:Landroid/widget/ListView;

.field search:Landroid/widget/EditText;

.field toolbar:Landroid/support/v7/widget/Toolbar;

.field toolbarTitle:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .line 31
    const-class v0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ListaCnpjPagadoresActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_a

    const/4 v0, 0x1

    goto :goto_b

    :cond_a
    const/4 v0, 0x0

    :goto_b
    sput-boolean v0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ListaCnpjPagadoresActivity;->$assertionsDisabled:Z

    return-void
.end method

.method public constructor <init>()V
    .registers 1

    .line 32
    invoke-direct {p0}, Lcom/itau/empresas/ui/activity/BaseActivity;-><init>()V

    return-void
.end method

.method private buildAdapter()V
    .registers 3

    .line 86
    new-instance v0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/adapter/CnpjPagamentosAdapter;

    invoke-direct {v0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/adapter/CnpjPagamentosAdapter;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ListaCnpjPagadoresActivity;->cnpjPagamentosAdapter:Lcom/itau/empresas/feature/pagamento/semcodigobarra/adapter/CnpjPagamentosAdapter;

    .line 87
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ListaCnpjPagadoresActivity;->cnpjVOList:Ljava/util/ArrayList;

    if-eqz v0, :cond_19

    .line 88
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ListaCnpjPagadoresActivity;->cnpjPagamentosAdapter:Lcom/itau/empresas/feature/pagamento/semcodigobarra/adapter/CnpjPagamentosAdapter;

    iget-object v1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ListaCnpjPagadoresActivity;->cnpjVOList:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/adapter/CnpjPagamentosAdapter;->setItems(Ljava/util/List;)V

    .line 89
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ListaCnpjPagadoresActivity;->listaCnpj:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ListaCnpjPagadoresActivity;->cnpjPagamentosAdapter:Lcom/itau/empresas/feature/pagamento/semcodigobarra/adapter/CnpjPagamentosAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 91
    :cond_19
    return-void
.end method

.method public static intent(Landroid/content/Context;)Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ListaCnpjPagadoresActivity_$IntentBuilder_;
    .registers 2
    .param p0, "context"    # Landroid/content/Context;

    .line 106
    new-instance v0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ListaCnpjPagadoresActivity_$IntentBuilder_;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ListaCnpjPagadoresActivity_$IntentBuilder_;-><init>(Landroid/content/Context;)V

    return-object v0
.end method


# virtual methods
.method public carregaElementosDaTela()V
    .registers 3

    .line 45
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ListaCnpjPagadoresActivity;->toolbar:Landroid/support/v7/widget/Toolbar;

    const v1, 0x7f020136

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->setNavigationIcon(I)V

    .line 46
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ListaCnpjPagadoresActivity;->toolbar:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ListaCnpjPagadoresActivity;->setSupportActionBar(Landroid/support/v7/widget/Toolbar;)V

    .line 47
    sget-boolean v0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ListaCnpjPagadoresActivity;->$assertionsDisabled:Z

    if-nez v0, :cond_1d

    invoke-virtual {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ListaCnpjPagadoresActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    if-nez v0, :cond_1d

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 48
    :cond_1d
    invoke-virtual {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ListaCnpjPagadoresActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 49
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ListaCnpjPagadoresActivity;->toolbarTitle:Landroid/widget/TextView;

    const v1, 0x7f07018a

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ListaCnpjPagadoresActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 50
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ListaCnpjPagadoresActivity;->toolbar:Landroid/support/v7/widget/Toolbar;

    new-instance v1, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ListaCnpjPagadoresActivity$1;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ListaCnpjPagadoresActivity$1;-><init>(Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ListaCnpjPagadoresActivity;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->setNavigationOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 56
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ListaCnpjPagadoresActivity;->listaCnpj:Landroid/widget/ListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setDividerHeight(I)V

    .line 58
    invoke-direct {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ListaCnpjPagadoresActivity;->buildAdapter()V

    .line 60
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ListaCnpjPagadoresActivity;->listaCnpj:Landroid/widget/ListView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setTextFilterEnabled(Z)V

    .line 61
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ListaCnpjPagadoresActivity;->search:Landroid/widget/EditText;

    new-instance v1, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ListaCnpjPagadoresActivity$2;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ListaCnpjPagadoresActivity$2;-><init>(Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ListaCnpjPagadoresActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 75
    return-void
.end method

.method public itemSelecionado(Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/CnpjVO;)V
    .registers 4
    .param p1, "cnpjVO"    # Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/CnpjVO;

    .line 94
    new-instance v1, Landroid/content/Intent;

    const-class v0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity_;

    invoke-direct {v1, p0, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 95
    .local v1, "intent":Landroid/content/Intent;
    const-string v0, "cnpj"

    invoke-virtual {v1, v0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 96
    const/4 v0, -0x1

    invoke-virtual {p0, v0, v1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ListaCnpjPagadoresActivity;->setResult(ILandroid/content/Intent;)V

    .line 97
    invoke-virtual {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ListaCnpjPagadoresActivity;->finish()V

    .line 98
    return-void
.end method

.method public opkeysConsumidas()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Ljava/lang/String;>;"
        }
    .end annotation

    .line 78
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

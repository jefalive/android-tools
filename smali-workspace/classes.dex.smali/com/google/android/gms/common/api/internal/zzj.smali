.class public final Lcom/google/android/gms/common/api/internal/zzj;
.super Lcom/google/android/gms/common/api/GoogleApiClient;

# interfaces
.implements Lcom/google/android/gms/common/api/internal/zzp$zza;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/common/api/internal/zzj$zza;,
        Lcom/google/android/gms/common/api/internal/zzj$zzc;,
        Lcom/google/android/gms/common/api/internal/zzj$zzb;,
        Lcom/google/android/gms/common/api/internal/zzj$zze;,
        Lcom/google/android/gms/common/api/internal/zzj$zzd;
    }
.end annotation


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final zzXG:Ljava/util/concurrent/locks/Lock;

.field private final zzagp:I

.field private final zzagr:Landroid/os/Looper;

.field private final zzags:Lcom/google/android/gms/common/zzc;

.field final zzagt:Lcom/google/android/gms/common/api/Api$zza;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/common/api/Api$zza<+Lcom/google/android/gms/internal/zzrn;Lcom/google/android/gms/internal/zzro;>;"
        }
    .end annotation
.end field

.field final zzahA:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<Lcom/google/android/gms/common/api/Api<*>;Ljava/lang/Integer;>;"
        }
    .end annotation
.end field

.field private final zzahL:Lcom/google/android/gms/common/internal/zzk;

.field private zzahM:Lcom/google/android/gms/common/api/internal/zzp;

.field final zzahN:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue<Lcom/google/android/gms/common/api/internal/zza$zza<**>;>;"
        }
    .end annotation
.end field

.field private volatile zzahO:Z

.field private zzahP:J

.field private zzahQ:J

.field private final zzahR:Lcom/google/android/gms/common/api/internal/zzj$zza;

.field zzahS:Lcom/google/android/gms/common/api/internal/zzj$zzc;

.field final zzahT:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<Lcom/google/android/gms/common/api/Api$zzc<*>;Lcom/google/android/gms/common/api/Api$zzb;>;"
        }
    .end annotation
.end field

.field zzahU:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<Lcom/google/android/gms/common/api/Scope;>;"
        }
    .end annotation
.end field

.field private final zzahV:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<Lcom/google/android/gms/common/api/internal/zzq<*>;>;"
        }
    .end annotation
.end field

.field final zzahW:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<Lcom/google/android/gms/common/api/internal/zzj$zze<*>;>;"
        }
    .end annotation
.end field

.field private zzahX:Lcom/google/android/gms/common/api/zza;

.field private final zzahY:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<Lcom/google/android/gms/common/api/internal/zzc;>;"
        }
    .end annotation
.end field

.field private zzahZ:Ljava/lang/Integer;

.field final zzahz:Lcom/google/android/gms/common/internal/zzf;

.field zzaia:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<Lcom/google/android/gms/common/api/internal/zzx;>;"
        }
    .end annotation
.end field

.field private final zzaib:Lcom/google/android/gms/common/api/internal/zzj$zzd;

.field private final zzaic:Lcom/google/android/gms/common/internal/zzk$zza;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/concurrent/locks/Lock;Landroid/os/Looper;Lcom/google/android/gms/common/internal/zzf;Lcom/google/android/gms/common/zzc;Lcom/google/android/gms/common/api/Api$zza;Ljava/util/Map;Ljava/util/List;Ljava/util/List;Ljava/util/Map;IILjava/util/ArrayList;)V
    .registers 20
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Landroid/content/Context;Ljava/util/concurrent/locks/Lock;Landroid/os/Looper;Lcom/google/android/gms/common/internal/zzf;Lcom/google/android/gms/common/zzc;Lcom/google/android/gms/common/api/Api$zza<+Lcom/google/android/gms/internal/zzrn;Lcom/google/android/gms/internal/zzro;>;Ljava/util/Map<Lcom/google/android/gms/common/api/Api<*>;Ljava/lang/Integer;>;Ljava/util/List<Lcom/google/android/gms/common/api/GoogleApiClient$ConnectionCallbacks;>;Ljava/util/List<Lcom/google/android/gms/common/api/GoogleApiClient$OnConnectionFailedListener;>;Ljava/util/Map<Lcom/google/android/gms/common/api/Api$zzc<*>;Lcom/google/android/gms/common/api/Api$zzb;>;IILjava/util/ArrayList<Lcom/google/android/gms/common/api/internal/zzc;>;)V"
        }
    .end annotation

    invoke-direct {p0}, Lcom/google/android/gms/common/api/GoogleApiClient;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/common/api/internal/zzj;->zzahM:Lcom/google/android/gms/common/api/internal/zzp;

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/common/api/internal/zzj;->zzahN:Ljava/util/Queue;

    const-wide/32 v0, 0x1d4c0

    iput-wide v0, p0, Lcom/google/android/gms/common/api/internal/zzj;->zzahP:J

    const-wide/16 v0, 0x1388

    iput-wide v0, p0, Lcom/google/android/gms/common/api/internal/zzj;->zzahQ:J

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/common/api/internal/zzj;->zzahU:Ljava/util/Set;

    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->newSetFromMap(Ljava/util/Map;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/common/api/internal/zzj;->zzahV:Ljava/util/Set;

    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    const/16 v1, 0x10

    const/high16 v2, 0x3f400000    # 0.75f

    const/4 v3, 0x2

    invoke-direct {v0, v1, v2, v3}, Ljava/util/concurrent/ConcurrentHashMap;-><init>(IFI)V

    invoke-static {v0}, Ljava/util/Collections;->newSetFromMap(Ljava/util/Map;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/common/api/internal/zzj;->zzahW:Ljava/util/Set;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/common/api/internal/zzj;->zzahZ:Ljava/lang/Integer;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/common/api/internal/zzj;->zzaia:Ljava/util/Set;

    new-instance v0, Lcom/google/android/gms/common/api/internal/zzj$1;

    invoke-direct {v0, p0}, Lcom/google/android/gms/common/api/internal/zzj$1;-><init>(Lcom/google/android/gms/common/api/internal/zzj;)V

    iput-object v0, p0, Lcom/google/android/gms/common/api/internal/zzj;->zzaib:Lcom/google/android/gms/common/api/internal/zzj$zzd;

    new-instance v0, Lcom/google/android/gms/common/api/internal/zzj$2;

    invoke-direct {v0, p0}, Lcom/google/android/gms/common/api/internal/zzj$2;-><init>(Lcom/google/android/gms/common/api/internal/zzj;)V

    iput-object v0, p0, Lcom/google/android/gms/common/api/internal/zzj;->zzaic:Lcom/google/android/gms/common/internal/zzk$zza;

    iput-object p1, p0, Lcom/google/android/gms/common/api/internal/zzj;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/android/gms/common/api/internal/zzj;->zzXG:Ljava/util/concurrent/locks/Lock;

    new-instance v0, Lcom/google/android/gms/common/internal/zzk;

    iget-object v1, p0, Lcom/google/android/gms/common/api/internal/zzj;->zzaic:Lcom/google/android/gms/common/internal/zzk$zza;

    invoke-direct {v0, p3, v1}, Lcom/google/android/gms/common/internal/zzk;-><init>(Landroid/os/Looper;Lcom/google/android/gms/common/internal/zzk$zza;)V

    iput-object v0, p0, Lcom/google/android/gms/common/api/internal/zzj;->zzahL:Lcom/google/android/gms/common/internal/zzk;

    iput-object p3, p0, Lcom/google/android/gms/common/api/internal/zzj;->zzagr:Landroid/os/Looper;

    new-instance v0, Lcom/google/android/gms/common/api/internal/zzj$zza;

    invoke-direct {v0, p0, p3}, Lcom/google/android/gms/common/api/internal/zzj$zza;-><init>(Lcom/google/android/gms/common/api/internal/zzj;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/gms/common/api/internal/zzj;->zzahR:Lcom/google/android/gms/common/api/internal/zzj$zza;

    iput-object p5, p0, Lcom/google/android/gms/common/api/internal/zzj;->zzags:Lcom/google/android/gms/common/zzc;

    move/from16 v0, p11

    iput v0, p0, Lcom/google/android/gms/common/api/internal/zzj;->zzagp:I

    iget v0, p0, Lcom/google/android/gms/common/api/internal/zzj;->zzagp:I

    if-ltz v0, :cond_72

    invoke-static/range {p12 .. p12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/common/api/internal/zzj;->zzahZ:Ljava/lang/Integer;

    :cond_72
    iput-object p7, p0, Lcom/google/android/gms/common/api/internal/zzj;->zzahA:Ljava/util/Map;

    move-object/from16 v0, p10

    iput-object v0, p0, Lcom/google/android/gms/common/api/internal/zzj;->zzahT:Ljava/util/Map;

    move-object/from16 v0, p13

    iput-object v0, p0, Lcom/google/android/gms/common/api/internal/zzj;->zzahY:Ljava/util/ArrayList;

    invoke-interface {p8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_80
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_93

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/google/android/gms/common/api/GoogleApiClient$ConnectionCallbacks;

    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzj;->zzahL:Lcom/google/android/gms/common/internal/zzk;

    invoke-virtual {v0, v5}, Lcom/google/android/gms/common/internal/zzk;->registerConnectionCallbacks(Lcom/google/android/gms/common/api/GoogleApiClient$ConnectionCallbacks;)V

    goto :goto_80

    :cond_93
    invoke-interface {p9}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_97
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_aa

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/google/android/gms/common/api/GoogleApiClient$OnConnectionFailedListener;

    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzj;->zzahL:Lcom/google/android/gms/common/internal/zzk;

    invoke-virtual {v0, v5}, Lcom/google/android/gms/common/internal/zzk;->registerConnectionFailedListener(Lcom/google/android/gms/common/api/GoogleApiClient$OnConnectionFailedListener;)V

    goto :goto_97

    :cond_aa
    iput-object p4, p0, Lcom/google/android/gms/common/api/internal/zzj;->zzahz:Lcom/google/android/gms/common/internal/zzf;

    iput-object p6, p0, Lcom/google/android/gms/common/api/internal/zzj;->zzagt:Lcom/google/android/gms/common/api/Api$zza;

    return-void
.end method

.method private resume()V
    .registers 3

    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzj;->zzXG:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    :try_start_5
    invoke-virtual {p0}, Lcom/google/android/gms/common/api/internal/zzj;->zzpB()Z

    move-result v0

    if-eqz v0, :cond_e

    invoke-direct {p0}, Lcom/google/android/gms/common/api/internal/zzj;->zzpC()V
    :try_end_e
    .catchall {:try_start_5 .. :try_end_e} :catchall_14

    :cond_e
    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzj;->zzXG:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto :goto_1b

    :catchall_14
    move-exception v1

    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzj;->zzXG:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v1

    :goto_1b
    return-void
.end method

.method public static zza(Ljava/lang/Iterable;Z)I
    .registers 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/lang/Iterable<Lcom/google/android/gms/common/api/Api$zzb;>;Z)I"
        }
    .end annotation

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_6
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_22

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/google/android/gms/common/api/Api$zzb;

    invoke-interface {v4}, Lcom/google/android/gms/common/api/Api$zzb;->zzmE()Z

    move-result v0

    if-eqz v0, :cond_1a

    const/4 v1, 0x1

    :cond_1a
    invoke-interface {v4}, Lcom/google/android/gms/common/api/Api$zzb;->zznb()Z

    move-result v0

    if-eqz v0, :cond_21

    const/4 v2, 0x1

    :cond_21
    goto :goto_6

    :cond_22
    if-eqz v1, :cond_2c

    if-eqz v2, :cond_2a

    if-eqz p1, :cond_2a

    const/4 v0, 0x2

    return v0

    :cond_2a
    const/4 v0, 0x1

    return v0

    :cond_2c
    const/4 v0, 0x3

    return v0
.end method

.method static synthetic zza(Lcom/google/android/gms/common/api/internal/zzj;)Lcom/google/android/gms/common/api/zza;
    .registers 2

    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzj;->zzahX:Lcom/google/android/gms/common/api/zza;

    return-object v0
.end method

.method private static zza(Lcom/google/android/gms/common/api/internal/zzj$zze;Lcom/google/android/gms/common/api/zza;Landroid/os/IBinder;)V
    .registers 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Lcom/google/android/gms/common/api/internal/zzj$zze<*>;Lcom/google/android/gms/common/api/zza;Landroid/os/IBinder;)V"
        }
    .end annotation

    invoke-interface {p0}, Lcom/google/android/gms/common/api/internal/zzj$zze;->isReady()Z

    move-result v0

    if-eqz v0, :cond_10

    new-instance v1, Lcom/google/android/gms/common/api/internal/zzj$zzb;

    const/4 v0, 0x0

    invoke-direct {v1, p0, p1, p2, v0}, Lcom/google/android/gms/common/api/internal/zzj$zzb;-><init>(Lcom/google/android/gms/common/api/internal/zzj$zze;Lcom/google/android/gms/common/api/zza;Landroid/os/IBinder;Lcom/google/android/gms/common/api/internal/zzj$1;)V

    invoke-interface {p0, v1}, Lcom/google/android/gms/common/api/internal/zzj$zze;->zza(Lcom/google/android/gms/common/api/internal/zzj$zzd;)V

    goto :goto_48

    :cond_10
    if-eqz p2, :cond_36

    invoke-interface {p2}, Landroid/os/IBinder;->isBinderAlive()Z

    move-result v0

    if-eqz v0, :cond_36

    new-instance v1, Lcom/google/android/gms/common/api/internal/zzj$zzb;

    const/4 v0, 0x0

    invoke-direct {v1, p0, p1, p2, v0}, Lcom/google/android/gms/common/api/internal/zzj$zzb;-><init>(Lcom/google/android/gms/common/api/internal/zzj$zze;Lcom/google/android/gms/common/api/zza;Landroid/os/IBinder;Lcom/google/android/gms/common/api/internal/zzj$1;)V

    invoke-interface {p0, v1}, Lcom/google/android/gms/common/api/internal/zzj$zze;->zza(Lcom/google/android/gms/common/api/internal/zzj$zzd;)V

    const/4 v0, 0x0

    :try_start_22
    invoke-interface {p2, v1, v0}, Landroid/os/IBinder;->linkToDeath(Landroid/os/IBinder$DeathRecipient;I)V
    :try_end_25
    .catch Landroid/os/RemoteException; {:try_start_22 .. :try_end_25} :catch_26

    goto :goto_35

    :catch_26
    move-exception v2

    invoke-interface {p0}, Lcom/google/android/gms/common/api/internal/zzj$zze;->cancel()V

    invoke-interface {p0}, Lcom/google/android/gms/common/api/internal/zzj$zze;->zzpa()Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/android/gms/common/api/zza;->remove(I)V

    :goto_35
    goto :goto_48

    :cond_36
    const/4 v0, 0x0

    invoke-interface {p0, v0}, Lcom/google/android/gms/common/api/internal/zzj$zze;->zza(Lcom/google/android/gms/common/api/internal/zzj$zzd;)V

    invoke-interface {p0}, Lcom/google/android/gms/common/api/internal/zzj$zze;->cancel()V

    invoke-interface {p0}, Lcom/google/android/gms/common/api/internal/zzj$zze;->zzpa()Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/android/gms/common/api/zza;->remove(I)V

    :goto_48
    return-void
.end method

.method static synthetic zzb(Lcom/google/android/gms/common/api/internal/zzj;)V
    .registers 1

    invoke-direct {p0}, Lcom/google/android/gms/common/api/internal/zzj;->resume()V

    return-void
.end method

.method private zzbB(I)V
    .registers 18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/common/api/internal/zzj;->zzahZ:Ljava/lang/Integer;

    if-nez v0, :cond_f

    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/gms/common/api/internal/zzj;->zzahZ:Ljava/lang/Integer;

    goto :goto_4e

    :cond_f
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/common/api/internal/zzj;->zzahZ:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    move/from16 v1, p1

    if-eq v0, v1, :cond_4e

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Cannot use sign-in mode: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static/range {p1 .. p1}, Lcom/google/android/gms/common/api/internal/zzj;->zzbC(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ". Mode was already set to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v2, p0

    iget-object v2, v2, Lcom/google/android/gms/common/api/internal/zzj;->zzahZ:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v2}, Lcom/google/android/gms/common/api/internal/zzj;->zzbC(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4e
    :goto_4e
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/common/api/internal/zzj;->zzahM:Lcom/google/android/gms/common/api/internal/zzp;

    if-eqz v0, :cond_55

    return-void

    :cond_55
    const/4 v12, 0x0

    const/4 v13, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/common/api/internal/zzj;->zzahT:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v14

    :goto_63
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7f

    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v15, v0

    check-cast v15, Lcom/google/android/gms/common/api/Api$zzb;

    invoke-interface {v15}, Lcom/google/android/gms/common/api/Api$zzb;->zzmE()Z

    move-result v0

    if-eqz v0, :cond_77

    const/4 v12, 0x1

    :cond_77
    invoke-interface {v15}, Lcom/google/android/gms/common/api/Api$zzb;->zznb()Z

    move-result v0

    if-eqz v0, :cond_7e

    const/4 v13, 0x1

    :cond_7e
    goto :goto_63

    :cond_7f
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/common/api/internal/zzj;->zzahZ:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    packed-switch v0, :pswitch_data_104

    goto :goto_d2

    :pswitch_8b
    goto :goto_d2

    :pswitch_8c
    if-nez v12, :cond_96

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "SIGN_IN_MODE_REQUIRED cannot be used on a GoogleApiClient that does not contain any authenticated APIs. Use connect() instead."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_96
    if-eqz v13, :cond_d2

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot use SIGN_IN_MODE_REQUIRED with GOOGLE_SIGN_IN_API. Use connect(SIGN_IN_MODE_OPTIONAL) instead."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_a0
    if-eqz v12, :cond_d2

    new-instance v0, Lcom/google/android/gms/common/api/internal/zzd;

    move-object/from16 v1, p0

    iget-object v1, v1, Lcom/google/android/gms/common/api/internal/zzj;->mContext:Landroid/content/Context;

    move-object/from16 v2, p0

    iget-object v3, v2, Lcom/google/android/gms/common/api/internal/zzj;->zzXG:Ljava/util/concurrent/locks/Lock;

    move-object/from16 v2, p0

    iget-object v4, v2, Lcom/google/android/gms/common/api/internal/zzj;->zzagr:Landroid/os/Looper;

    move-object/from16 v2, p0

    iget-object v5, v2, Lcom/google/android/gms/common/api/internal/zzj;->zzags:Lcom/google/android/gms/common/zzc;

    move-object/from16 v2, p0

    iget-object v6, v2, Lcom/google/android/gms/common/api/internal/zzj;->zzahT:Ljava/util/Map;

    move-object/from16 v2, p0

    iget-object v7, v2, Lcom/google/android/gms/common/api/internal/zzj;->zzahz:Lcom/google/android/gms/common/internal/zzf;

    move-object/from16 v2, p0

    iget-object v8, v2, Lcom/google/android/gms/common/api/internal/zzj;->zzahA:Ljava/util/Map;

    move-object/from16 v2, p0

    iget-object v9, v2, Lcom/google/android/gms/common/api/internal/zzj;->zzagt:Lcom/google/android/gms/common/api/Api$zza;

    move-object/from16 v2, p0

    iget-object v10, v2, Lcom/google/android/gms/common/api/internal/zzj;->zzahY:Ljava/util/ArrayList;

    move-object/from16 v2, p0

    invoke-direct/range {v0 .. v10}, Lcom/google/android/gms/common/api/internal/zzd;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/api/internal/zzj;Ljava/util/concurrent/locks/Lock;Landroid/os/Looper;Lcom/google/android/gms/common/zzc;Ljava/util/Map;Lcom/google/android/gms/common/internal/zzf;Ljava/util/Map;Lcom/google/android/gms/common/api/Api$zza;Ljava/util/ArrayList;)V

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/gms/common/api/internal/zzj;->zzahM:Lcom/google/android/gms/common/api/internal/zzp;

    return-void

    :cond_d2
    :goto_d2
    new-instance v0, Lcom/google/android/gms/common/api/internal/zzl;

    move-object/from16 v1, p0

    iget-object v1, v1, Lcom/google/android/gms/common/api/internal/zzj;->mContext:Landroid/content/Context;

    move-object/from16 v2, p0

    iget-object v3, v2, Lcom/google/android/gms/common/api/internal/zzj;->zzXG:Ljava/util/concurrent/locks/Lock;

    move-object/from16 v2, p0

    iget-object v4, v2, Lcom/google/android/gms/common/api/internal/zzj;->zzagr:Landroid/os/Looper;

    move-object/from16 v2, p0

    iget-object v5, v2, Lcom/google/android/gms/common/api/internal/zzj;->zzags:Lcom/google/android/gms/common/zzc;

    move-object/from16 v2, p0

    iget-object v6, v2, Lcom/google/android/gms/common/api/internal/zzj;->zzahT:Ljava/util/Map;

    move-object/from16 v2, p0

    iget-object v7, v2, Lcom/google/android/gms/common/api/internal/zzj;->zzahz:Lcom/google/android/gms/common/internal/zzf;

    move-object/from16 v2, p0

    iget-object v8, v2, Lcom/google/android/gms/common/api/internal/zzj;->zzahA:Ljava/util/Map;

    move-object/from16 v2, p0

    iget-object v9, v2, Lcom/google/android/gms/common/api/internal/zzj;->zzagt:Lcom/google/android/gms/common/api/Api$zza;

    move-object/from16 v2, p0

    iget-object v10, v2, Lcom/google/android/gms/common/api/internal/zzj;->zzahY:Ljava/util/ArrayList;

    move-object/from16 v2, p0

    move-object/from16 v11, p0

    invoke-direct/range {v0 .. v11}, Lcom/google/android/gms/common/api/internal/zzl;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/api/internal/zzj;Ljava/util/concurrent/locks/Lock;Landroid/os/Looper;Lcom/google/android/gms/common/zzc;Ljava/util/Map;Lcom/google/android/gms/common/internal/zzf;Ljava/util/Map;Lcom/google/android/gms/common/api/Api$zza;Ljava/util/ArrayList;Lcom/google/android/gms/common/api/internal/zzp$zza;)V

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/gms/common/api/internal/zzj;->zzahM:Lcom/google/android/gms/common/api/internal/zzp;

    return-void

    :pswitch_data_104
    .packed-switch 0x1
        :pswitch_8c
        :pswitch_a0
        :pswitch_8b
    .end packed-switch
.end method

.method static zzbC(I)Ljava/lang/String;
    .registers 2

    packed-switch p0, :pswitch_data_10

    goto :goto_d

    :pswitch_4
    const-string v0, "SIGN_IN_MODE_NONE"

    return-object v0

    :pswitch_7
    const-string v0, "SIGN_IN_MODE_REQUIRED"

    return-object v0

    :pswitch_a
    const-string v0, "SIGN_IN_MODE_OPTIONAL"

    return-object v0

    :goto_d
    const-string v0, "UNKNOWN"

    return-object v0

    :pswitch_data_10
    .packed-switch 0x1
        :pswitch_7
        :pswitch_a
        :pswitch_4
    .end packed-switch
.end method

.method static synthetic zzc(Lcom/google/android/gms/common/api/internal/zzj;)V
    .registers 1

    invoke-direct {p0}, Lcom/google/android/gms/common/api/internal/zzj;->zzpD()V

    return-void
.end method

.method private zzpC()V
    .registers 2

    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzj;->zzahL:Lcom/google/android/gms/common/internal/zzk;

    invoke-virtual {v0}, Lcom/google/android/gms/common/internal/zzk;->zzqR()V

    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzj;->zzahM:Lcom/google/android/gms/common/api/internal/zzp;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/internal/zzp;->connect()V

    return-void
.end method

.method private zzpD()V
    .registers 4

    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzj;->zzXG:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    :try_start_5
    invoke-virtual {p0}, Lcom/google/android/gms/common/api/internal/zzj;->zzpF()Z

    move-result v1

    if-eqz v1, :cond_e

    invoke-direct {p0}, Lcom/google/android/gms/common/api/internal/zzj;->zzpC()V
    :try_end_e
    .catchall {:try_start_5 .. :try_end_e} :catchall_14

    :cond_e
    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzj;->zzXG:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto :goto_1b

    :catchall_14
    move-exception v2

    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzj;->zzXG:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v2

    :goto_1b
    return-void
.end method


# virtual methods
.method public connect()V
    .registers 4

    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzj;->zzXG:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    :try_start_5
    iget v0, p0, Lcom/google/android/gms/common/api/internal/zzj;->zzagp:I

    if-ltz v0, :cond_16

    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzj;->zzahZ:Ljava/lang/Integer;

    if-eqz v0, :cond_f

    const/4 v0, 0x1

    goto :goto_10

    :cond_f
    const/4 v0, 0x0

    :goto_10
    const-string v1, "Sign-in mode should have been set explicitly by auto-manage."

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/zzx;->zza(ZLjava/lang/Object;)V

    goto :goto_3d

    :cond_16
    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzj;->zzahZ:Ljava/lang/Integer;

    if-nez v0, :cond_2c

    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzj;->zzahT:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/gms/common/api/internal/zzj;->zza(Ljava/lang/Iterable;Z)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/common/api/internal/zzj;->zzahZ:Ljava/lang/Integer;

    goto :goto_3d

    :cond_2c
    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzj;->zzahZ:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_3d

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot call connect() when SignInMode is set to SIGN_IN_MODE_OPTIONAL. Call connect(SIGN_IN_MODE_OPTIONAL) instead."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3d
    :goto_3d
    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzj;->zzahZ:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/common/api/internal/zzj;->connect(I)V
    :try_end_46
    .catchall {:try_start_5 .. :try_end_46} :catchall_4c

    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzj;->zzXG:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto :goto_53

    :catchall_4c
    move-exception v2

    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzj;->zzXG:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v2

    :goto_53
    return-void
.end method

.method public connect(I)V
    .registers 6
    .param p1, "signInMode"    # I

    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzj;->zzXG:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    const/4 v0, 0x3

    if-eq p1, v0, :cond_e

    const/4 v0, 0x1

    if-eq p1, v0, :cond_e

    const/4 v0, 0x2

    if-ne p1, v0, :cond_10

    :cond_e
    const/4 v0, 0x1

    goto :goto_11

    :cond_10
    const/4 v0, 0x0

    :goto_11
    :try_start_11
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Illegal sign-in mode: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/zzx;->zzb(ZLjava/lang/Object;)V

    invoke-direct {p0, p1}, Lcom/google/android/gms/common/api/internal/zzj;->zzbB(I)V

    invoke-direct {p0}, Lcom/google/android/gms/common/api/internal/zzj;->zzpC()V
    :try_end_2d
    .catchall {:try_start_11 .. :try_end_2d} :catchall_33

    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzj;->zzXG:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto :goto_3a

    :catchall_33
    move-exception v3

    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzj;->zzXG:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v3

    :goto_3a
    return-void
.end method

.method public disconnect()V
    .registers 6

    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzj;->zzXG:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    :try_start_5
    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzj;->zzahM:Lcom/google/android/gms/common/api/internal/zzp;

    if-eqz v0, :cond_13

    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzj;->zzahM:Lcom/google/android/gms/common/api/internal/zzp;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/internal/zzp;->disconnect()Z

    move-result v0

    if-nez v0, :cond_13

    const/4 v1, 0x1

    goto :goto_14

    :cond_13
    const/4 v1, 0x0

    :goto_14
    invoke-virtual {p0, v1}, Lcom/google/android/gms/common/api/internal/zzj;->zzaa(Z)V

    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzj;->zzahV:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1d
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2e

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/google/android/gms/common/api/internal/zzq;

    invoke-virtual {v3}, Lcom/google/android/gms/common/api/internal/zzq;->clear()V

    goto :goto_1d

    :cond_2e
    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzj;->zzahV:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzj;->zzahN:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_39
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4e

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/google/android/gms/common/api/internal/zzj$zze;

    const/4 v0, 0x0

    invoke-interface {v3, v0}, Lcom/google/android/gms/common/api/internal/zzj$zze;->zza(Lcom/google/android/gms/common/api/internal/zzj$zzd;)V

    invoke-interface {v3}, Lcom/google/android/gms/common/api/internal/zzj$zze;->cancel()V

    goto :goto_39

    :cond_4e
    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzj;->zzahN:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->clear()V

    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzj;->zzahM:Lcom/google/android/gms/common/api/internal/zzp;
    :try_end_55
    .catchall {:try_start_5 .. :try_end_55} :catchall_6b

    if-nez v0, :cond_5d

    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzj;->zzXG:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    return-void

    :cond_5d
    :try_start_5d
    invoke-virtual {p0}, Lcom/google/android/gms/common/api/internal/zzj;->zzpF()Z

    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzj;->zzahL:Lcom/google/android/gms/common/internal/zzk;

    invoke-virtual {v0}, Lcom/google/android/gms/common/internal/zzk;->zzqQ()V
    :try_end_65
    .catchall {:try_start_5d .. :try_end_65} :catchall_6b

    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzj;->zzXG:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto :goto_72

    :catchall_6b
    move-exception v4

    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzj;->zzXG:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v4

    :goto_72
    return-void
.end method

.method public dump(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .registers 7
    .param p1, "prefix"    # Ljava/lang/String;
    .param p2, "fd"    # Ljava/io/FileDescriptor;
    .param p3, "writer"    # Ljava/io/PrintWriter;
    .param p4, "args"    # [Ljava/lang/String;

    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    move-result-object v0

    const-string v1, "mContext="

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/common/api/internal/zzj;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    move-result-object v0

    const-string v1, "mResuming="

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/gms/common/api/internal/zzj;->zzahO:Z

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Z)V

    const-string v0, " mWorkQueue.size()="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/common/api/internal/zzj;->zzahN:Ljava/util/Queue;

    invoke-interface {v1}, Ljava/util/Queue;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(I)V

    const-string v0, " mUnconsumedRunners.size()="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/common/api/internal/zzj;->zzahW:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(I)V

    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzj;->zzahM:Lcom/google/android/gms/common/api/internal/zzp;

    if-eqz v0, :cond_45

    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzj;->zzahM:Lcom/google/android/gms/common/api/internal/zzp;

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/google/android/gms/common/api/internal/zzp;->dump(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    :cond_45
    return-void
.end method

.method public getLooper()Landroid/os/Looper;
    .registers 2

    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzj;->zzagr:Landroid/os/Looper;

    return-object v0
.end method

.method public getSessionId()I
    .registers 2

    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public isConnected()Z
    .registers 2

    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzj;->zzahM:Lcom/google/android/gms/common/api/internal/zzp;

    if-eqz v0, :cond_e

    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzj;->zzahM:Lcom/google/android/gms/common/api/internal/zzp;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/internal/zzp;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_e

    const/4 v0, 0x1

    goto :goto_f

    :cond_e
    const/4 v0, 0x0

    :goto_f
    return v0
.end method

.method public registerConnectionFailedListener(Lcom/google/android/gms/common/api/GoogleApiClient$OnConnectionFailedListener;)V
    .registers 3
    .param p1, "listener"    # Lcom/google/android/gms/common/api/GoogleApiClient$OnConnectionFailedListener;

    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzj;->zzahL:Lcom/google/android/gms/common/internal/zzk;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/common/internal/zzk;->registerConnectionFailedListener(Lcom/google/android/gms/common/api/GoogleApiClient$OnConnectionFailedListener;)V

    return-void
.end method

.method public unregisterConnectionFailedListener(Lcom/google/android/gms/common/api/GoogleApiClient$OnConnectionFailedListener;)V
    .registers 3
    .param p1, "listener"    # Lcom/google/android/gms/common/api/GoogleApiClient$OnConnectionFailedListener;

    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzj;->zzahL:Lcom/google/android/gms/common/internal/zzk;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/common/internal/zzk;->unregisterConnectionFailedListener(Lcom/google/android/gms/common/api/GoogleApiClient$OnConnectionFailedListener;)V

    return-void
.end method

.method public zza(Lcom/google/android/gms/common/api/Api$zzc;)Lcom/google/android/gms/common/api/Api$zzb;
    .registers 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<C::Lcom/google/android/gms/common/api/Api$zzb;>(Lcom/google/android/gms/common/api/Api$zzc<TC;>;)TC;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzj;->zzahT:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/google/android/gms/common/api/Api$zzb;

    const-string v0, "Appropriate Api was not requested."

    invoke-static {v1, v0}, Lcom/google/android/gms/common/internal/zzx;->zzb(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-object v1
.end method

.method public zza(Lcom/google/android/gms/common/api/internal/zza$zza;)Lcom/google/android/gms/common/api/internal/zza$zza;
    .registers 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<A::Lcom/google/android/gms/common/api/Api$zzb;R::Lcom/google/android/gms/common/api/Result;T:Lcom/google/android/gms/common/api/internal/zza$zza<TR;TA;>;>(TT;)TT;"
        }
    .end annotation

    invoke-virtual {p1}, Lcom/google/android/gms/common/api/internal/zza$zza;->zzoR()Lcom/google/android/gms/common/api/Api$zzc;

    move-result-object v0

    if-eqz v0, :cond_8

    const/4 v0, 0x1

    goto :goto_9

    :cond_8
    const/4 v0, 0x0

    :goto_9
    const-string v1, "This task can not be enqueued (it\'s probably a Batch or malformed)"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/zzx;->zzb(ZLjava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzj;->zzahT:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/google/android/gms/common/api/internal/zza$zza;->zzoR()Lcom/google/android/gms/common/api/Api$zzc;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    const-string v1, "GoogleApiClient is not configured to use the API required for this call."

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/zzx;->zzb(ZLjava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzj;->zzXG:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    :try_start_22
    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzj;->zzahM:Lcom/google/android/gms/common/api/internal/zzp;

    if-nez v0, :cond_32

    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzj;->zzahN:Ljava/util/Queue;

    invoke-interface {v0, p1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z
    :try_end_2b
    .catchall {:try_start_22 .. :try_end_2b} :catchall_3e

    move-object v2, p1

    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzj;->zzXG:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    return-object v2

    :cond_32
    :try_start_32
    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzj;->zzahM:Lcom/google/android/gms/common/api/internal/zzp;

    invoke-interface {v0, p1}, Lcom/google/android/gms/common/api/internal/zzp;->zza(Lcom/google/android/gms/common/api/internal/zza$zza;)Lcom/google/android/gms/common/api/internal/zza$zza;
    :try_end_37
    .catchall {:try_start_32 .. :try_end_37} :catchall_3e

    move-result-object v2

    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzj;->zzXG:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    return-object v2

    :catchall_3e
    move-exception v3

    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzj;->zzXG:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v3
.end method

.method public zza(Lcom/google/android/gms/common/api/internal/zzx;)V
    .registers 4

    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzj;->zzXG:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    :try_start_5
    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzj;->zzaia:Ljava/util/Set;

    if-nez v0, :cond_10

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/common/api/internal/zzj;->zzaia:Ljava/util/Set;

    :cond_10
    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzj;->zzaia:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_15
    .catchall {:try_start_5 .. :try_end_15} :catchall_1b

    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzj;->zzXG:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto :goto_22

    :catchall_1b
    move-exception v1

    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzj;->zzXG:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v1

    :goto_22
    return-void
.end method

.method zzaa(Z)V
    .registers 6

    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzj;->zzahW:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_42

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/google/android/gms/common/api/internal/zzj$zze;

    invoke-interface {v2}, Lcom/google/android/gms/common/api/internal/zzj$zze;->zzpa()Ljava/lang/Integer;

    move-result-object v0

    if-nez v0, :cond_28

    if-eqz p1, :cond_1f

    invoke-interface {v2}, Lcom/google/android/gms/common/api/internal/zzj$zze;->zzpg()V

    goto :goto_41

    :cond_1f
    invoke-interface {v2}, Lcom/google/android/gms/common/api/internal/zzj$zze;->cancel()V

    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzj;->zzahW:Ljava/util/Set;

    invoke-interface {v0, v2}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    goto :goto_41

    :cond_28
    invoke-interface {v2}, Lcom/google/android/gms/common/api/internal/zzj$zze;->zzpe()V

    invoke-interface {v2}, Lcom/google/android/gms/common/api/internal/zzj$zze;->zzoR()Lcom/google/android/gms/common/api/Api$zzc;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/common/api/internal/zzj;->zza(Lcom/google/android/gms/common/api/Api$zzc;)Lcom/google/android/gms/common/api/Api$zzb;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/common/api/Api$zzb;->zzoT()Landroid/os/IBinder;

    move-result-object v3

    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzj;->zzahX:Lcom/google/android/gms/common/api/zza;

    invoke-static {v2, v0, v3}, Lcom/google/android/gms/common/api/internal/zzj;->zza(Lcom/google/android/gms/common/api/internal/zzj$zze;Lcom/google/android/gms/common/api/zza;Landroid/os/IBinder;)V

    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzj;->zzahW:Ljava/util/Set;

    invoke-interface {v0, v2}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    :goto_41
    goto :goto_6

    :cond_42
    return-void
.end method

.method public zzb(Lcom/google/android/gms/common/api/internal/zza$zza;)Lcom/google/android/gms/common/api/internal/zza$zza;
    .registers 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<A::Lcom/google/android/gms/common/api/Api$zzb;T:Lcom/google/android/gms/common/api/internal/zza$zza<+Lcom/google/android/gms/common/api/Result;TA;>;>(TT;)TT;"
        }
    .end annotation

    invoke-virtual {p1}, Lcom/google/android/gms/common/api/internal/zza$zza;->zzoR()Lcom/google/android/gms/common/api/Api$zzc;

    move-result-object v0

    if-eqz v0, :cond_8

    const/4 v0, 0x1

    goto :goto_9

    :cond_8
    const/4 v0, 0x0

    :goto_9
    const-string v1, "This task can not be executed (it\'s probably a Batch or malformed)"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/zzx;->zzb(ZLjava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzj;->zzXG:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    :try_start_13
    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzj;->zzahM:Lcom/google/android/gms/common/api/internal/zzp;

    if-nez v0, :cond_1f

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "GoogleApiClient is not connected yet."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1f
    invoke-virtual {p0}, Lcom/google/android/gms/common/api/internal/zzj;->zzpB()Z

    move-result v0

    if-eqz v0, :cond_4b

    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzj;->zzahN:Ljava/util/Queue;

    invoke-interface {v0, p1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    :goto_2a
    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzj;->zzahN:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_44

    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzj;->zzahN:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->remove()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/google/android/gms/common/api/internal/zzj$zze;

    invoke-virtual {p0, v2}, Lcom/google/android/gms/common/api/internal/zzj;->zzb(Lcom/google/android/gms/common/api/internal/zzj$zze;)V

    sget-object v0, Lcom/google/android/gms/common/api/Status;->zzagE:Lcom/google/android/gms/common/api/Status;

    invoke-interface {v2, v0}, Lcom/google/android/gms/common/api/internal/zzj$zze;->zzw(Lcom/google/android/gms/common/api/Status;)V
    :try_end_43
    .catchall {:try_start_13 .. :try_end_43} :catchall_57

    goto :goto_2a

    :cond_44
    move-object v2, p1

    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzj;->zzXG:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    return-object v2

    :cond_4b
    :try_start_4b
    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzj;->zzahM:Lcom/google/android/gms/common/api/internal/zzp;

    invoke-interface {v0, p1}, Lcom/google/android/gms/common/api/internal/zzp;->zzb(Lcom/google/android/gms/common/api/internal/zza$zza;)Lcom/google/android/gms/common/api/internal/zza$zza;
    :try_end_50
    .catchall {:try_start_4b .. :try_end_50} :catchall_57

    move-result-object v2

    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzj;->zzXG:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    return-object v2

    :catchall_57
    move-exception v3

    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzj;->zzXG:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v3
.end method

.method zzb(Lcom/google/android/gms/common/api/internal/zzj$zze;)V
    .registers 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<A::Lcom/google/android/gms/common/api/Api$zzb;>(Lcom/google/android/gms/common/api/internal/zzj$zze<TA;>;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzj;->zzahW:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzj;->zzaib:Lcom/google/android/gms/common/api/internal/zzj$zzd;

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/internal/zzj$zze;->zza(Lcom/google/android/gms/common/api/internal/zzj$zzd;)V

    return-void
.end method

.method public zzb(Lcom/google/android/gms/common/api/internal/zzx;)V
    .registers 6

    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzj;->zzXG:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    :try_start_5
    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzj;->zzaia:Ljava/util/Set;

    if-nez v0, :cond_16

    const-string v0, "GoogleApiClientImpl"

    const-string v1, "Attempted to remove pending transform when no transforms are registered."

    new-instance v2, Ljava/lang/Exception;

    invoke-direct {v2}, Ljava/lang/Exception;-><init>()V

    invoke-static {v0, v1, v2}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_36

    :cond_16
    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzj;->zzaia:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2b

    const-string v0, "GoogleApiClientImpl"

    const-string v1, "Failed to remove pending transform - this may lead to memory leaks!"

    new-instance v2, Ljava/lang/Exception;

    invoke-direct {v2}, Ljava/lang/Exception;-><init>()V

    invoke-static {v0, v1, v2}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_36

    :cond_2b
    invoke-virtual {p0}, Lcom/google/android/gms/common/api/internal/zzj;->zzpG()Z

    move-result v0

    if-nez v0, :cond_36

    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzj;->zzahM:Lcom/google/android/gms/common/api/internal/zzp;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/internal/zzp;->zzpj()V
    :try_end_36
    .catchall {:try_start_5 .. :try_end_36} :catchall_3c

    :cond_36
    :goto_36
    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzj;->zzXG:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto :goto_43

    :catchall_3c
    move-exception v3

    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzj;->zzXG:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v3

    :goto_43
    return-void
.end method

.method public zzc(IZ)V
    .registers 8

    const/4 v0, 0x1

    if-ne p1, v0, :cond_8

    if-nez p2, :cond_8

    invoke-virtual {p0}, Lcom/google/android/gms/common/api/internal/zzj;->zzpE()V

    :cond_8
    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzj;->zzahW:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_e
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2d

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/google/android/gms/common/api/internal/zzj$zze;

    if-eqz p2, :cond_20

    invoke-interface {v4}, Lcom/google/android/gms/common/api/internal/zzj$zze;->zzpe()V

    :cond_20
    new-instance v0, Lcom/google/android/gms/common/api/Status;

    const-string v1, "The connection to Google Play services was lost"

    const/16 v2, 0x8

    invoke-direct {v0, v2, v1}, Lcom/google/android/gms/common/api/Status;-><init>(ILjava/lang/String;)V

    invoke-interface {v4, v0}, Lcom/google/android/gms/common/api/internal/zzj$zze;->zzx(Lcom/google/android/gms/common/api/Status;)V

    goto :goto_e

    :cond_2d
    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzj;->zzahW:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzj;->zzahL:Lcom/google/android/gms/common/internal/zzk;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/common/internal/zzk;->zzbT(I)V

    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzj;->zzahL:Lcom/google/android/gms/common/internal/zzk;

    invoke-virtual {v0}, Lcom/google/android/gms/common/internal/zzk;->zzqQ()V

    const/4 v0, 0x2

    if-ne p1, v0, :cond_42

    invoke-direct {p0}, Lcom/google/android/gms/common/api/internal/zzj;->zzpC()V

    :cond_42
    return-void
.end method

.method public zzd(Lcom/google/android/gms/common/ConnectionResult;)V
    .registers 5

    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzj;->zzags:Lcom/google/android/gms/common/zzc;

    iget-object v1, p0, Lcom/google/android/gms/common/api/internal/zzj;->mContext:Landroid/content/Context;

    invoke-virtual {p1}, Lcom/google/android/gms/common/ConnectionResult;->getErrorCode()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/zzc;->zzd(Landroid/content/Context;I)Z

    move-result v0

    if-nez v0, :cond_11

    invoke-virtual {p0}, Lcom/google/android/gms/common/api/internal/zzj;->zzpF()Z

    :cond_11
    invoke-virtual {p0}, Lcom/google/android/gms/common/api/internal/zzj;->zzpB()Z

    move-result v0

    if-nez v0, :cond_21

    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzj;->zzahL:Lcom/google/android/gms/common/internal/zzk;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/common/internal/zzk;->zzk(Lcom/google/android/gms/common/ConnectionResult;)V

    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzj;->zzahL:Lcom/google/android/gms/common/internal/zzk;

    invoke-virtual {v0}, Lcom/google/android/gms/common/internal/zzk;->zzqQ()V

    :cond_21
    return-void
.end method

.method public zzi(Landroid/os/Bundle;)V
    .registers 3

    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzj;->zzahN:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_14

    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzj;->zzahN:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->remove()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/api/internal/zza$zza;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/common/api/internal/zzj;->zzb(Lcom/google/android/gms/common/api/internal/zza$zza;)Lcom/google/android/gms/common/api/internal/zza$zza;

    goto :goto_0

    :cond_14
    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzj;->zzahL:Lcom/google/android/gms/common/internal/zzk;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/common/internal/zzk;->zzk(Landroid/os/Bundle;)V

    return-void
.end method

.method zzpB()Z
    .registers 2

    iget-boolean v0, p0, Lcom/google/android/gms/common/api/internal/zzj;->zzahO:Z

    return v0
.end method

.method zzpE()V
    .registers 5

    invoke-virtual {p0}, Lcom/google/android/gms/common/api/internal/zzj;->zzpB()Z

    move-result v0

    if-eqz v0, :cond_7

    return-void

    :cond_7
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/common/api/internal/zzj;->zzahO:Z

    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzj;->zzahS:Lcom/google/android/gms/common/api/internal/zzj$zzc;

    if-nez v0, :cond_23

    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzj;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/common/api/internal/zzj$zzc;

    invoke-direct {v1, p0}, Lcom/google/android/gms/common/api/internal/zzj$zzc;-><init>(Lcom/google/android/gms/common/api/internal/zzj;)V

    iget-object v2, p0, Lcom/google/android/gms/common/api/internal/zzj;->zzags:Lcom/google/android/gms/common/zzc;

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/common/api/internal/zzn;->zza(Landroid/content/Context;Lcom/google/android/gms/common/api/internal/zzn;Lcom/google/android/gms/common/zzc;)Lcom/google/android/gms/common/api/internal/zzn;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/api/internal/zzj$zzc;

    iput-object v0, p0, Lcom/google/android/gms/common/api/internal/zzj;->zzahS:Lcom/google/android/gms/common/api/internal/zzj$zzc;

    :cond_23
    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzj;->zzahR:Lcom/google/android/gms/common/api/internal/zzj$zza;

    iget-object v1, p0, Lcom/google/android/gms/common/api/internal/zzj;->zzahR:Lcom/google/android/gms/common/api/internal/zzj$zza;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/google/android/gms/common/api/internal/zzj$zza;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    iget-wide v2, p0, Lcom/google/android/gms/common/api/internal/zzj;->zzahP:J

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/common/api/internal/zzj$zza;->sendMessageDelayed(Landroid/os/Message;J)Z

    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzj;->zzahR:Lcom/google/android/gms/common/api/internal/zzj$zza;

    iget-object v1, p0, Lcom/google/android/gms/common/api/internal/zzj;->zzahR:Lcom/google/android/gms/common/api/internal/zzj$zza;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Lcom/google/android/gms/common/api/internal/zzj$zza;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    iget-wide v2, p0, Lcom/google/android/gms/common/api/internal/zzj;->zzahQ:J

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/common/api/internal/zzj$zza;->sendMessageDelayed(Landroid/os/Message;J)Z

    return-void
.end method

.method zzpF()Z
    .registers 3

    invoke-virtual {p0}, Lcom/google/android/gms/common/api/internal/zzj;->zzpB()Z

    move-result v0

    if-nez v0, :cond_8

    const/4 v0, 0x0

    return v0

    :cond_8
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/common/api/internal/zzj;->zzahO:Z

    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzj;->zzahR:Lcom/google/android/gms/common/api/internal/zzj$zza;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/api/internal/zzj$zza;->removeMessages(I)V

    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzj;->zzahR:Lcom/google/android/gms/common/api/internal/zzj$zza;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/api/internal/zzj$zza;->removeMessages(I)V

    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzj;->zzahS:Lcom/google/android/gms/common/api/internal/zzj$zzc;

    if-eqz v0, :cond_23

    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzj;->zzahS:Lcom/google/android/gms/common/api/internal/zzj$zzc;

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/internal/zzj$zzc;->unregister()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/common/api/internal/zzj;->zzahS:Lcom/google/android/gms/common/api/internal/zzj$zzc;

    :cond_23
    const/4 v0, 0x1

    return v0
.end method

.method zzpG()Z
    .registers 4

    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzj;->zzXG:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    :try_start_5
    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzj;->zzaia:Ljava/util/Set;
    :try_end_7
    .catchall {:try_start_5 .. :try_end_7} :catchall_21

    if-nez v0, :cond_10

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzj;->zzXG:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    return v1

    :cond_10
    :try_start_10
    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzj;->zzaia:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z
    :try_end_15
    .catchall {:try_start_10 .. :try_end_15} :catchall_21

    move-result v0

    if-nez v0, :cond_1a

    const/4 v1, 0x1

    goto :goto_1b

    :cond_1a
    const/4 v1, 0x0

    :goto_1b
    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzj;->zzXG:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    return v1

    :catchall_21
    move-exception v2

    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzj;->zzXG:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v2
.end method

.method zzpH()Ljava/lang/String;
    .registers 6

    new-instance v4, Ljava/io/StringWriter;

    invoke-direct {v4}, Ljava/io/StringWriter;-><init>()V

    const-string v0, ""

    new-instance v1, Ljava/io/PrintWriter;

    invoke-direct {v1, v4}, Ljava/io/PrintWriter;-><init>(Ljava/io/Writer;)V

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {p0, v0, v2, v1, v3}, Lcom/google/android/gms/common/api/internal/zzj;->dump(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/io/StringWriter;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

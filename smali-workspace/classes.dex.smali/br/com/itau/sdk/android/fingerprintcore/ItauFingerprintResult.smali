.class public final enum Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintResult;
.super Ljava/lang/Enum;
.source "ItauFingerprintResult.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintResult;>;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintResult;

.field public static final enum AUTHENTICATED:Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintResult;

.field public static final enum FAILED:Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintResult;

.field public static final enum HELP:Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintResult;


# instance fields
.field private message:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .line 10
    new-instance v0, Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintResult;

    const-string v1, "FAILED"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintResult;->FAILED:Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintResult;

    .line 11
    new-instance v0, Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintResult;

    const-string v1, "HELP"

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintResult;->HELP:Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintResult;

    .line 12
    new-instance v0, Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintResult;

    const-string v1, "AUTHENTICATED"

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2}, Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintResult;->AUTHENTICATED:Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintResult;

    .line 8
    const/4 v0, 0x3

    new-array v0, v0, [Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintResult;

    sget-object v1, Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintResult;->FAILED:Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintResult;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sget-object v1, Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintResult;->HELP:Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintResult;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    sget-object v1, Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintResult;->AUTHENTICATED:Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintResult;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    sput-object v0, Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintResult;->$VALUES:[Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintResult;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 8
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 14
    const/4 v0, 0x0

    iput-object v0, p0, Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintResult;->message:Ljava/lang/String;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintResult;
    .registers 2
    .param p0, "name"    # Ljava/lang/String;

    .line 8
    const-class v0, Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintResult;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintResult;

    return-object v0
.end method

.method public static values()[Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintResult;
    .registers 1

    .line 8
    sget-object v0, Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintResult;->$VALUES:[Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintResult;

    invoke-virtual {v0}, [Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintResult;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintResult;

    return-object v0
.end method


# virtual methods
.method public getMessage()Ljava/lang/String;
    .registers 2

    .line 20
    iget-object v0, p0, Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintResult;->message:Ljava/lang/String;

    return-object v0
.end method

.method public setMessage(Ljava/lang/String;)Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintResult;
    .registers 4
    .param p1, "message"    # Ljava/lang/String;

    .line 24
    iget-object v0, p0, Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintResult;->message:Ljava/lang/String;

    if-eqz v0, :cond_4c

    .line 26
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "pt_BR"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 27
    new-instance v0, Ljava/lang/IllegalAccessError;

    const-string v1, "n\u00e3o foi poss\u00edvel mudar a mensagem!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalAccessError;-><init>(Ljava/lang/String;)V

    throw v0

    .line 28
    :cond_1c
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "en_US"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_34

    .line 29
    new-instance v0, Ljava/lang/IllegalAccessError;

    const-string v1, "cannot change message!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalAccessError;-><init>(Ljava/lang/String;)V

    throw v0

    .line 30
    :cond_34
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "es_ES"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4c

    .line 31
    new-instance v0, Ljava/lang/IllegalAccessError;

    const-string v1, "no puede cambiar el mensaje!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalAccessError;-><init>(Ljava/lang/String;)V

    throw v0

    .line 34
    :cond_4c
    iput-object p1, p0, Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintResult;->message:Ljava/lang/String;

    .line 35
    return-object p0
.end method

.class public Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;
.super Ljava/lang/Object;


# static fields
.field private static final a:[B

.field private static b:I

.field private static c:I


# direct methods
.method static constructor <clinit>()V
    .registers 3

    nop

    const/4 v0, 0x0

    sput v0, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->b:I

    const/4 v0, 0x1

    sput v0, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->c:I

    const/16 v0, 0x68

    new-array v0, v0, [B

    fill-array-data v0, :array_12

    sput-object v0, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a:[B

    return-void

    nop

    :array_12
    .array-data 1
        0x58t
        -0xft
        0x66t
        0x39t
        0x4t
        -0x1at
        0x10t
        0x2et
        -0x40t
        -0xbt
        0x3ct
        -0x29t
        -0x14t
        -0x12t
        0x5t
        -0x10t
        -0x3t
        0x27t
        -0x27t
        -0x5t
        -0x7t
        -0x13t
        -0x5t
        0x28t
        -0x28t
        -0x8t
        0x2t
        -0x12t
        0x5t
        -0x13t
        -0x2t
        0x1t
        0x0t
        0x31t
        -0x3at
        -0x11t
        -0x4t
        -0xbt
        0xat
        -0xet
        -0xbt
        0x41t
        -0x1at
        -0x31t
        -0x4t
        -0xbt
        0xat
        -0x18t
        -0x1t
        0x2t
        -0xet
        0x2t
        -0x12t
        0x7t
        0x9t
        -0x5t
        -0xbt
        0x8t
        -0x1t
        -0x8t
        0x19t
        -0x34t
        0x4t
        -0x5t
        -0x12t
        0x8t
        -0x1t
        -0x8t
        0xct
        -0x1at
        -0xct
        -0x8t
        -0x7t
        0x8t
        -0x1t
        -0x8t
        0x16t
        -0x2at
        -0xbt
        0x3t
        -0x3t
        -0xct
        0x29t
        -0x3ct
        0x0t
        0xat
        -0x13t
        -0x3t
        -0x14t
        0x1ft
        -0x16t
        -0x7t
        -0xdt
        0x5t
        -0xbt
        -0x3t
        0x1dt
        -0x31t
        0x6t
        -0x6t
        0x10t
        -0x16t
        -0x18t
        0x7t
    .end array-data
.end method

.method public constructor <init>()V
    .registers 1

    nop

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(ISB)Ljava/lang/String;
    .registers 12

    nop

    add-int/lit8 p1, p1, 0x4

    neg-int v1, p0

    or-int/lit8 v0, v1, 0x18

    shl-int/lit8 v0, v0, 0x1

    xor-int/lit8 v1, v1, 0x18

    sub-int p0, v0, v1

    const/4 v8, 0x0

    sget-object v7, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a:[B

    new-instance v0, Ljava/lang/String;

    neg-int v1, p2

    xor-int/lit8 v1, v1, -0x1

    rsub-int/lit8 v1, v1, 0x73

    add-int/lit8 p2, v1, -0x1

    new-array v1, p0, [B

    goto :goto_27

    :goto_1b
    :pswitch_1b
    neg-int v4, v3

    xor-int/2addr v4, v2

    neg-int v3, v3

    and-int/2addr v2, v3

    shl-int/lit8 v2, v2, 0x1

    add-int/2addr v2, v4

    add-int/lit8 p2, v2, -0x5

    add-int/lit8 p1, p1, 0x1

    goto :goto_48

    :goto_27
    :sswitch_27
    move v2, v8

    or-int/lit8 v3, v8, 0x1

    shl-int/lit8 v3, v3, 0x1

    xor-int/lit8 v4, v8, 0x1

    sub-int/2addr v3, v4

    move v8, v3

    int-to-byte v3, p2

    aput-byte v3, v1, v2

    if-ne v8, p0, :cond_36

    goto :goto_37

    :cond_36
    goto :goto_38

    :goto_37
    goto :goto_3a

    :goto_38
    goto/16 :goto_7b

    :goto_3a
    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Ljava/lang/String;-><init>([BI)V

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :pswitch_43
    :sswitch_43
    move v2, p2

    aget-byte v3, v7, p1

    goto/16 :goto_93

    :goto_48
    sget v3, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->b:I

    xor-int/lit8 v2, v3, 0x5

    and-int/lit8 v3, v3, 0x5

    shl-int/lit8 v3, v3, 0x1

    add-int/2addr v2, v3

    rem-int/lit16 v3, v2, 0x80

    sput v3, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->c:I

    rem-int/lit8 v2, v2, 0x2

    if-nez v2, :cond_5a

    goto :goto_5b

    :cond_5a
    goto :goto_5c

    :goto_5b
    goto :goto_78

    :goto_5c
    goto/16 :goto_b5

    :sswitch_5e
    move v2, v8

    xor-int/lit8 v3, v8, 0x1

    and-int/lit8 v4, v8, 0x1

    shl-int/lit8 v4, v4, 0x1

    add-int/2addr v3, v4

    move v8, v3

    int-to-byte v3, p2

    aput-byte v3, v1, v2

    if-ne v8, p0, :cond_6d

    goto :goto_6e

    :cond_6d
    goto :goto_6f

    :goto_6e
    goto :goto_3a

    :goto_6f
    goto/16 :goto_ce

    :goto_71
    sparse-switch v2, :sswitch_data_106

    nop

    :pswitch_75
    const/16 v2, 0x5f

    goto :goto_71

    :goto_78
    const/16 v2, 0x22

    goto :goto_71

    :goto_7b
    sget v3, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->b:I

    and-int/lit8 v2, v3, 0x37

    or-int/lit8 v3, v3, 0x37

    add-int/2addr v2, v3

    rem-int/lit16 v3, v2, 0x80

    sput v3, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->c:I

    rem-int/lit8 v2, v2, 0x2

    if-nez v2, :cond_8c

    goto/16 :goto_ea

    :cond_8c
    goto/16 :goto_e7

    :sswitch_8e
    move v2, p2

    aget-byte v3, v7, p1

    goto/16 :goto_1b

    :goto_93
    sget v5, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->b:I

    or-int/lit8 v4, v5, 0x61

    shl-int/lit8 v4, v4, 0x1

    xor-int/lit8 v5, v5, 0x61

    sub-int/2addr v4, v5

    rem-int/lit16 v5, v4, 0x80

    sput v5, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->c:I

    rem-int/lit8 v4, v4, 0x2

    if-nez v4, :cond_a6

    goto/16 :goto_f3

    :cond_a6
    goto/16 :goto_f1

    :pswitch_a8
    neg-int v4, v3

    xor-int/2addr v4, v2

    neg-int v3, v3

    and-int/2addr v2, v3

    shl-int/lit8 v2, v2, 0x1

    add-int/2addr v2, v4

    add-int/lit8 p2, v2, -0x5

    add-int/lit8 p1, p1, 0x1

    goto/16 :goto_48

    :goto_b5
    sget v3, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->b:I

    or-int/lit8 v2, v3, 0x27

    shl-int/lit8 v2, v2, 0x1

    xor-int/lit8 v3, v3, 0x27

    sub-int/2addr v2, v3

    rem-int/lit16 v3, v2, 0x80

    sput v3, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->c:I

    rem-int/lit8 v2, v2, 0x2

    if-nez v2, :cond_c8

    goto/16 :goto_fb

    :cond_c8
    goto/16 :goto_f9

    :pswitch_ca
    const/16 v2, 0x5f

    goto/16 :goto_71

    :goto_ce
    sget v2, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->c:I

    add-int/lit8 v2, v2, 0x65

    rem-int/lit16 v3, v2, 0x80

    sput v3, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->b:I

    rem-int/lit8 v2, v2, 0x2

    if-eqz v2, :cond_dc

    goto/16 :goto_103

    :cond_dc
    goto/16 :goto_101

    :pswitch_de
    move v2, p2

    aget-byte v3, v7, p1

    goto/16 :goto_93

    :goto_e3
    sparse-switch v2, :sswitch_data_110

    goto :goto_ea

    :goto_e7
    const/16 v2, 0x2b

    goto :goto_e3

    :goto_ea
    const/16 v2, 0x53

    goto :goto_e3

    :goto_ed
    packed-switch v4, :pswitch_data_11a

    nop

    :goto_f1
    const/4 v4, 0x0

    goto :goto_ed

    :goto_f3
    const/4 v4, 0x1

    goto :goto_ed

    :goto_f5
    packed-switch v2, :pswitch_data_122

    goto :goto_fb

    :goto_f9
    const/4 v2, 0x0

    goto :goto_f5

    :goto_fb
    const/4 v2, 0x1

    goto :goto_f5

    :goto_fd
    packed-switch v2, :pswitch_data_12a

    goto :goto_103

    :goto_101
    const/4 v2, 0x1

    goto :goto_fd

    :goto_103
    const/4 v2, 0x0

    goto :goto_fd

    nop

    :sswitch_data_106
    .sparse-switch
        0x22 -> :sswitch_5e
        0x5f -> :sswitch_27
    .end sparse-switch

    :sswitch_data_110
    .sparse-switch
        0x2b -> :sswitch_43
        0x53 -> :sswitch_8e
    .end sparse-switch

    :pswitch_data_11a
    .packed-switch 0x0
        :pswitch_1b
        :pswitch_a8
    .end packed-switch

    :pswitch_data_122
    .packed-switch 0x0
        :pswitch_75
        :pswitch_ca
    .end packed-switch

    :pswitch_data_12a
    .packed-switch 0x0
        :pswitch_de
        :pswitch_43
    .end packed-switch
.end method

.method public static checkApk(Landroid/content/Context;I)I
    .registers 17

    nop

    :try_start_1
    new-instance v1, Ljava/io/RandomAccessFile;
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_3} :catch_575

    goto :goto_a

    :catchall_4
    move-exception v0

    :try_start_5
    invoke-virtual {v0}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_a
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_a} :catch_575

    :goto_a
    :try_start_a
    sget-object v0, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a:[B

    const/16 v2, 0x1f

    aget-byte v0, v0, v2

    int-to-byte v0, v0

    xor-int/lit8 v2, v0, 0x16

    and-int/lit8 v3, v0, 0x16

    or-int/2addr v2, v3

    int-to-byte v2, v2

    sget-object v3, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a:[B

    const/16 v4, 0xd

    aget-byte v3, v3, v4

    neg-int v3, v3

    int-to-byte v3, v3

    invoke-static {v0, v2, v3}, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a(ISB)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    sget-object v2, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a:[B

    const/16 v3, 0x62

    aget-byte v2, v2, v3

    int-to-byte v2, v2

    sget-object v3, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a:[B

    const/16 v4, 0x44

    aget-byte v3, v3, v4

    int-to-byte v3, v3

    const/16 v4, 0x53

    invoke-static {v2, v4, v3}, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a(ISB)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, p0, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;
    :try_end_47
    .catchall {:try_start_a .. :try_end_47} :catchall_4

    :try_start_47
    sget-object v2, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a:[B

    const/16 v3, 0x4c

    aget-byte v3, v2, v3

    xor-int/lit8 v2, v3, 0x1

    and-int/lit8 v3, v3, 0x1

    shl-int/lit8 v3, v3, 0x1

    add-int/2addr v2, v3

    int-to-byte v2, v2

    sget-object v3, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a:[B

    const/16 v4, 0x1f

    aget-byte v3, v3, v4

    int-to-byte v3, v3

    const/16 v4, 0x35

    invoke-static {v2, v4, v3}, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a(ISB)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Ljava/io/RandomAccessFile;-><init>(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_65
    .catch Ljava/lang/Throwable; {:try_start_47 .. :try_end_65} :catch_575

    move-object p0, v1

    goto :goto_6d

    :catchall_67
    move-exception v0

    :try_start_68
    invoke-virtual {v0}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_6d
    .catchall {:try_start_68 .. :try_end_6d} :catchall_570

    :goto_6d
    :try_start_6d
    sget-object v0, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a:[B

    const/16 v2, 0x20

    aget-byte v0, v0, v2

    int-to-byte v0, v0

    int-to-byte v2, v0

    sget-object v3, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a:[B

    const/16 v4, 0x36

    aget-byte v3, v3, v4

    int-to-byte v3, v3

    invoke-static {v0, v2, v3}, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a(ISB)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    sget-object v2, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a:[B

    const/16 v3, 0xd

    aget-byte v2, v2, v3

    neg-int v2, v2

    int-to-byte v2, v2

    sget-object v3, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a:[B

    const/4 v4, 0x7

    aget-byte v4, v3, v4

    or-int/lit8 v3, v4, -0x1

    shl-int/lit8 v3, v3, 0x1

    xor-int/lit8 v4, v4, -0x1

    sub-int/2addr v3, v4

    int-to-byte v3, v3

    sget-object v4, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a:[B

    const/16 v5, 0x35

    aget-byte v4, v4, v5

    int-to-byte v4, v4

    invoke-static {v2, v3, v4}, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a(ISB)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J
    :try_end_b3
    .catchall {:try_start_6d .. :try_end_b3} :catchall_67

    move-result-wide v2

    const-wide/16 v4, 0x16

    sub-long v3, v2, v4

    goto :goto_bf

    :catchall_b9
    move-exception v0

    :try_start_ba
    invoke-virtual {v0}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_bf
    .catchall {:try_start_ba .. :try_end_bf} :catchall_570

    :goto_bf
    const/4 v0, 0x1

    :try_start_c0
    new-array v2, v0, [Ljava/lang/Object;

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    const/4 v3, 0x0

    aput-object v0, v2, v3

    sget-object v0, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a:[B

    const/16 v3, 0x20

    aget-byte v0, v0, v3

    int-to-byte v0, v0

    int-to-byte v3, v0

    sget-object v4, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a:[B

    const/16 v5, 0x36

    aget-byte v4, v4, v5

    int-to-byte v4, v4

    invoke-static {v0, v3, v4}, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a(ISB)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    sget-object v3, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a:[B

    const/16 v4, 0xc

    aget-byte v3, v3, v4

    neg-int v3, v3

    int-to-byte v3, v3

    sget-object v4, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a:[B

    const/16 v5, 0x21

    aget-byte v5, v4, v5

    or-int/lit8 v4, v5, 0x1

    shl-int/lit8 v4, v4, 0x1

    xor-int/lit8 v5, v5, 0x1

    sub-int/2addr v4, v5

    int-to-byte v4, v4

    sget-object v5, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a:[B

    const/16 v6, 0x20

    aget-byte v5, v5, v6

    int-to-byte v5, v5

    invoke-static {v3, v4, v5}, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a(ISB)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Class;

    sget-object v5, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    const/4 v6, 0x0

    aput-object v5, v4, v6

    invoke-virtual {v0, v3, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_110
    .catchall {:try_start_c0 .. :try_end_110} :catchall_b9

    :try_start_110
    new-instance v7, Ljava/util/zip/CRC32;

    invoke-direct {v7}, Ljava/util/zip/CRC32;-><init>()V

    const/16 v0, 0x8

    new-array v8, v0, [B
    :try_end_119
    .catchall {:try_start_110 .. :try_end_119} :catchall_570

    goto/16 :goto_57d

    :catchall_11b
    move-exception v0

    :try_start_11c
    invoke-virtual {v0}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_121
    .catchall {:try_start_11c .. :try_end_121} :catchall_570

    :sswitch_121
    const/4 v0, 0x1

    :try_start_122
    new-array v1, v0, [Ljava/lang/Object;

    const/4 v0, 0x0

    aput-object v8, v1, v0

    sget-object v0, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a:[B

    const/16 v2, 0x20

    aget-byte v0, v0, v2

    int-to-byte v0, v0

    int-to-byte v2, v0

    sget-object v3, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a:[B

    const/16 v4, 0x36

    aget-byte v3, v3, v4

    int-to-byte v3, v3

    invoke-static {v0, v2, v3}, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a(ISB)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    sget-object v2, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a:[B

    const/4 v3, 0x1

    aget-byte v2, v2, v3

    neg-int v2, v2

    int-to-byte v2, v2

    sget-object v3, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a:[B

    const/16 v4, 0x1f

    aget-byte v3, v3, v4

    int-to-byte v3, v3

    const/16 v4, 0x35

    invoke-static {v2, v4, v3}, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a(ISB)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Class;

    const-class v4, [B

    const/4 v5, 0x0

    aput-object v4, v3, v5

    invoke-virtual {v0, v2, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    invoke-virtual {v0, p0, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_161
    .catchall {:try_start_122 .. :try_end_161} :catchall_11b

    :try_start_161
    invoke-virtual {v7, v8}, Ljava/util/zip/CRC32;->update([B)V

    invoke-virtual {v7}, Ljava/util/zip/CRC32;->getValue()J
    :try_end_167
    .catchall {:try_start_161 .. :try_end_167} :catchall_570

    move-result-wide v0

    const-wide v2, 0xa985693fL

    cmp-long v0, v0, v2

    if-eqz v0, :cond_172

    goto :goto_173

    :cond_172
    goto :goto_175

    :goto_173
    goto/16 :goto_acb

    :goto_175
    goto/16 :goto_ade

    :goto_177
    and-int/lit8 v0, p1, 0x1

    xor-int/lit8 v0, v0, -0x1

    or-int/lit8 v1, p1, 0x1

    and-int v9, v0, v1

    :try_start_17f
    invoke-virtual {p0}, Ljava/io/RandomAccessFile;->close()V
    :try_end_182
    .catch Ljava/lang/Throwable; {:try_start_17f .. :try_end_182} :catch_575

    return v9

    :pswitch_183
    goto/16 :goto_af3

    :catchall_185
    move-exception v0

    :try_start_186
    invoke-virtual {v0}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_18b
    .catchall {:try_start_186 .. :try_end_18b} :catchall_570

    :pswitch_18b
    :try_start_18b
    sget-object v0, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a:[B

    const/16 v1, 0x20

    aget-byte v0, v0, v1

    int-to-byte v0, v0

    int-to-byte v1, v0

    sget-object v2, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a:[B

    const/16 v3, 0x36

    aget-byte v2, v2, v3

    int-to-byte v2, v2

    invoke-static {v0, v1, v2}, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a(ISB)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    sget-object v1, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a:[B

    const/4 v2, 0x1

    aget-byte v1, v1, v2

    neg-int v1, v1

    int-to-byte v1, v1

    sget-object v2, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a:[B

    const/16 v3, 0xa

    aget-byte v3, v2, v3

    and-int/lit8 v2, v3, 0x1

    or-int/lit8 v3, v3, 0x1

    add-int/2addr v2, v3

    int-to-byte v2, v2

    sget-object v3, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a:[B

    const/16 v4, 0x1f

    aget-byte v3, v3, v4

    int-to-byte v3, v3

    invoke-static {v1, v2, v3}, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a(ISB)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p0, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Short;

    invoke-virtual {v0}, Ljava/lang/Short;->shortValue()S
    :try_end_1cf
    .catchall {:try_start_18b .. :try_end_1cf} :catchall_185

    move-result v0

    :try_start_1d0
    invoke-static {v0}, Ljava/lang/Short;->reverseBytes(S)S
    :try_end_1d3
    .catchall {:try_start_1d0 .. :try_end_1d3} :catchall_570

    move-result v9

    move-object v0, p0

    const/4 v1, 0x6

    goto/16 :goto_b15

    :catchall_1d8
    move-exception v0

    :try_start_1d9
    invoke-virtual {v0}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_1de
    .catchall {:try_start_1d9 .. :try_end_1de} :catchall_570

    :goto_1de
    :pswitch_1de
    const/4 v2, 0x1

    :try_start_1df
    new-array v2, v2, [Ljava/lang/Object;

    const/4 v1, 0x6

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v3, 0x0

    aput-object v1, v2, v3

    sget-object v1, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a:[B

    const/16 v3, 0x20

    aget-byte v1, v1, v3

    int-to-byte v1, v1

    int-to-byte v3, v1

    sget-object v4, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a:[B

    const/16 v5, 0x36

    aget-byte v4, v4, v5

    int-to-byte v4, v4

    invoke-static {v1, v3, v4}, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a(ISB)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    sget-object v3, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a:[B

    const/4 v4, 0x1

    aget-byte v3, v3, v4

    neg-int v3, v3

    int-to-byte v3, v3

    sget-object v4, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a:[B

    const/16 v5, 0x20

    aget-byte v4, v4, v5

    int-to-byte v4, v4

    const/16 v5, 0x4b

    invoke-static {v3, v5, v4}, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a(ISB)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Class;

    sget-object v5, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    const/4 v6, 0x0

    aput-object v5, v4, v6

    invoke-virtual {v1, v3, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    invoke-virtual {v1, v0, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
    :try_end_229
    .catchall {:try_start_1df .. :try_end_229} :catchall_1d8

    move-object v0, p0

    goto/16 :goto_b38

    :catchall_22c
    move-exception v0

    :try_start_22d
    invoke-virtual {v0}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_232
    .catchall {:try_start_22d .. :try_end_232} :catchall_570

    :pswitch_232
    :try_start_232
    sget-object v1, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a:[B

    const/16 v2, 0x20

    aget-byte v1, v1, v2

    int-to-byte v1, v1

    int-to-byte v2, v1

    sget-object v3, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a:[B

    const/16 v4, 0x36

    aget-byte v3, v3, v4

    int-to-byte v3, v3

    invoke-static {v1, v2, v3}, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a(ISB)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    sget-object v2, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a:[B

    const/16 v3, 0x23

    aget-byte v2, v2, v3

    neg-int v2, v2

    int-to-byte v2, v2

    sget-object v3, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a:[B

    const/16 v4, 0x1f

    aget-byte v3, v3, v4

    int-to-byte v3, v3

    const/16 v4, 0x45

    invoke-static {v2, v4, v3}, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a(ISB)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I
    :try_end_26d
    .catchall {:try_start_232 .. :try_end_26d} :catchall_22c

    move-result v1

    :try_start_26e
    invoke-static {v1}, Ljava/lang/Integer;->reverseBytes(I)I
    :try_end_271
    .catchall {:try_start_26e .. :try_end_271} :catchall_570

    move-result v1

    int-to-long v1, v1

    goto/16 :goto_b5f

    :catchall_275
    move-exception v0

    :try_start_276
    invoke-virtual {v0}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_27b
    .catchall {:try_start_276 .. :try_end_27b} :catchall_570

    :sswitch_27b
    const/4 v3, 0x1

    :try_start_27c
    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v3, v2

    sget-object v1, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a:[B

    const/16 v2, 0x20

    aget-byte v1, v1, v2

    int-to-byte v1, v1

    int-to-byte v2, v1

    sget-object v4, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a:[B

    const/16 v5, 0x36

    aget-byte v4, v4, v5

    int-to-byte v4, v4

    invoke-static {v1, v2, v4}, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a(ISB)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    sget-object v2, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a:[B

    const/16 v4, 0xc

    aget-byte v2, v2, v4

    neg-int v2, v2

    int-to-byte v2, v2

    sget-object v4, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a:[B

    const/16 v5, 0x21

    aget-byte v5, v4, v5

    or-int/lit8 v4, v5, 0x1

    shl-int/lit8 v4, v4, 0x1

    xor-int/lit8 v5, v5, 0x1

    sub-int/2addr v4, v5

    int-to-byte v4, v4

    sget-object v5, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a:[B

    const/16 v6, 0x20

    aget-byte v5, v5, v6

    int-to-byte v5, v5

    invoke-static {v2, v4, v5}, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a(ISB)Ljava/lang/String;

    move-result-object v2

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Class;

    sget-object v5, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    const/4 v6, 0x0

    aput-object v5, v4, v6

    invoke-virtual {v1, v2, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    invoke-virtual {v1, v0, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_2cc
    .catchall {:try_start_27c .. :try_end_2cc} :catchall_275

    const/4 v10, -0x1

    goto/16 :goto_b89

    :sswitch_2cf
    move v0, v9

    or-int/lit8 v1, v9, -0x1

    shl-int/lit8 v1, v1, 0x1

    xor-int/lit8 v2, v9, -0x1

    sub-int/2addr v1, v2

    move v9, v1

    if-lez v0, :cond_2db

    goto :goto_2dc

    :cond_2db
    goto :goto_2de

    :goto_2dc
    goto/16 :goto_bac

    :goto_2de
    goto/16 :goto_bc2

    :sswitch_2e0
    move-object v0, p0

    move-object v1, v8

    goto/16 :goto_77c

    :catchall_2e4
    move-exception v0

    :try_start_2e5
    invoke-virtual {v0}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_2ea
    .catchall {:try_start_2e5 .. :try_end_2ea} :catchall_570

    :pswitch_2ea
    :sswitch_2ea
    const/4 v2, 0x1

    :try_start_2eb
    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v1, v2, v3

    sget-object v1, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a:[B

    const/16 v3, 0x20

    aget-byte v1, v1, v3

    int-to-byte v1, v1

    int-to-byte v3, v1

    sget-object v4, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a:[B

    const/16 v5, 0x36

    aget-byte v4, v4, v5

    int-to-byte v4, v4

    invoke-static {v1, v3, v4}, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a(ISB)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    sget-object v3, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a:[B

    const/4 v4, 0x1

    aget-byte v3, v3, v4

    neg-int v3, v3

    int-to-byte v3, v3

    sget-object v4, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a:[B

    const/16 v5, 0x1f

    aget-byte v4, v4, v5

    int-to-byte v4, v4

    const/16 v5, 0x35

    invoke-static {v3, v5, v4}, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a(ISB)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Class;

    const-class v5, [B

    const/4 v6, 0x0

    aput-object v5, v4, v6

    invoke-virtual {v1, v3, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    invoke-virtual {v1, v0, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_32a
    .catchall {:try_start_2eb .. :try_end_32a} :catchall_2e4

    :try_start_32a
    invoke-virtual {v7}, Ljava/util/zip/CRC32;->reset()V

    invoke-virtual {v7, v8}, Ljava/util/zip/CRC32;->update([B)V

    invoke-virtual {v7}, Ljava/util/zip/CRC32;->getValue()J
    :try_end_333
    .catchall {:try_start_32a .. :try_end_333} :catchall_570

    move-result-wide v0

    const-wide/32 v2, 0x52c68507

    cmp-long v0, v0, v2

    if-eqz v0, :cond_33c

    goto :goto_33d

    :cond_33c
    goto :goto_33f

    :goto_33d
    goto/16 :goto_bd8

    :goto_33f
    goto/16 :goto_9cb

    :goto_341
    :pswitch_341
    and-int/lit8 v0, p1, -0x3

    xor-int/lit8 v1, p1, -0x1

    and-int/lit8 v1, v1, 0x2

    or-int v11, v0, v1

    :try_start_349
    invoke-virtual {p0}, Ljava/io/RandomAccessFile;->close()V
    :try_end_34c
    .catch Ljava/lang/Throwable; {:try_start_349 .. :try_end_34c} :catch_575

    return v11

    :pswitch_34d
    goto/16 :goto_beb

    :catchall_34f
    move-exception v0

    :try_start_350
    invoke-virtual {v0}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_355
    .catchall {:try_start_350 .. :try_end_355} :catchall_570

    :sswitch_355
    const/4 v0, 0x1

    :try_start_356
    new-array v1, v0, [Ljava/lang/Object;

    const/16 v0, 0x14

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const/4 v2, 0x0

    aput-object v0, v1, v2

    sget-object v0, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a:[B

    const/16 v2, 0x20

    aget-byte v0, v0, v2

    int-to-byte v0, v0

    int-to-byte v2, v0

    sget-object v3, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a:[B

    const/16 v4, 0x36

    aget-byte v3, v3, v4

    int-to-byte v3, v3

    invoke-static {v0, v2, v3}, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a(ISB)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    sget-object v2, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a:[B

    const/4 v3, 0x1

    aget-byte v2, v2, v3

    neg-int v2, v2

    int-to-byte v2, v2

    sget-object v3, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a:[B

    const/16 v4, 0x20

    aget-byte v3, v3, v4

    int-to-byte v3, v3

    const/16 v4, 0x4b

    invoke-static {v2, v4, v3}, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a(ISB)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Class;

    sget-object v4, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    const/4 v5, 0x0

    aput-object v4, v3, v5

    invoke-virtual {v0, v2, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    invoke-virtual {v0, p0, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
    :try_end_3a1
    .catchall {:try_start_356 .. :try_end_3a1} :catchall_34f

    move-object v0, p0

    goto/16 :goto_c0b

    :catchall_3a4
    move-exception v0

    :try_start_3a5
    invoke-virtual {v0}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_3aa
    .catchall {:try_start_3a5 .. :try_end_3aa} :catchall_570

    :goto_3aa
    :pswitch_3aa
    :try_start_3aa
    sget-object v1, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a:[B

    const/16 v2, 0x20

    aget-byte v1, v1, v2

    int-to-byte v1, v1

    int-to-byte v2, v1

    sget-object v3, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a:[B

    const/16 v4, 0x36

    aget-byte v3, v3, v4

    int-to-byte v3, v3

    invoke-static {v1, v2, v3}, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a(ISB)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    sget-object v2, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a:[B

    const/4 v3, 0x1

    aget-byte v2, v2, v3

    neg-int v2, v2

    int-to-byte v2, v2

    sget-object v3, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a:[B

    const/16 v4, 0xa

    aget-byte v3, v3, v4

    add-int/lit8 v3, v3, 0x1

    int-to-byte v3, v3

    sget-object v4, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a:[B

    const/16 v5, 0x1f

    aget-byte v4, v4, v5

    int-to-byte v4, v4

    invoke-static {v2, v3, v4}, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a(ISB)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Short;

    invoke-virtual {v0}, Ljava/lang/Short;->shortValue()S
    :try_end_3eb
    .catchall {:try_start_3aa .. :try_end_3eb} :catchall_3a4

    move-result v0

    :try_start_3ec
    invoke-static {v0}, Ljava/lang/Short;->reverseBytes(S)S

    move-result v0

    new-array v11, v0, [B
    :try_end_3f2
    .catchall {:try_start_3ec .. :try_end_3f2} :catchall_570

    move-object v0, p0

    const/16 v1, 0xc

    goto/16 :goto_8b2

    :catchall_3f7
    move-exception v0

    :try_start_3f8
    invoke-virtual {v0}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_3fd
    .catchall {:try_start_3f8 .. :try_end_3fd} :catchall_570

    :sswitch_3fd
    const/4 v2, 0x1

    :try_start_3fe
    new-array v2, v2, [Ljava/lang/Object;

    const/16 v1, 0xc

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v3, 0x0

    aput-object v1, v2, v3

    sget-object v1, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a:[B

    const/16 v3, 0x20

    aget-byte v1, v1, v3

    int-to-byte v1, v1

    int-to-byte v3, v1

    sget-object v4, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a:[B

    const/16 v5, 0x36

    aget-byte v4, v4, v5

    int-to-byte v4, v4

    invoke-static {v1, v3, v4}, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a(ISB)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    sget-object v3, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a:[B

    const/4 v4, 0x1

    aget-byte v3, v3, v4

    neg-int v3, v3

    int-to-byte v3, v3

    sget-object v4, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a:[B

    const/16 v5, 0x20

    aget-byte v4, v4, v5

    int-to-byte v4, v4

    const/16 v5, 0x4b

    invoke-static {v3, v5, v4}, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a(ISB)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Class;

    sget-object v5, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    const/4 v6, 0x0

    aput-object v5, v4, v6

    invoke-virtual {v1, v3, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    invoke-virtual {v1, v0, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
    :try_end_449
    .catchall {:try_start_3fe .. :try_end_449} :catchall_3f7

    move-object v0, p0

    goto :goto_451

    :catchall_44b
    move-exception v0

    :try_start_44c
    invoke-virtual {v0}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_451
    .catchall {:try_start_44c .. :try_end_451} :catchall_570

    :goto_451
    :pswitch_451
    :try_start_451
    sget-object v1, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a:[B

    const/16 v2, 0x20

    aget-byte v1, v1, v2

    int-to-byte v1, v1

    int-to-byte v2, v1

    sget-object v3, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a:[B

    const/16 v4, 0x36

    aget-byte v3, v3, v4

    int-to-byte v3, v3

    invoke-static {v1, v2, v3}, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a(ISB)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    sget-object v2, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a:[B

    const/16 v3, 0x23

    aget-byte v2, v2, v3

    neg-int v2, v2

    int-to-byte v2, v2

    sget-object v3, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a:[B

    const/16 v4, 0x1f

    aget-byte v3, v3, v4

    int-to-byte v3, v3

    const/16 v4, 0x45

    invoke-static {v2, v4, v3}, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a(ISB)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
    :try_end_48c
    .catchall {:try_start_451 .. :try_end_48c} :catchall_44b

    move-result v0

    :try_start_48d
    invoke-static {v0}, Ljava/lang/Integer;->reverseBytes(I)I
    :try_end_490
    .catchall {:try_start_48d .. :try_end_490} :catchall_570

    move-result v12

    move-object v0, p0

    move-object v1, v11

    goto/16 :goto_c30

    :catchall_495
    move-exception v0

    :try_start_496
    invoke-virtual {v0}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_49b
    .catchall {:try_start_496 .. :try_end_49b} :catchall_570

    :sswitch_49b
    const/4 v2, 0x1

    :try_start_49c
    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v1, v2, v3

    sget-object v1, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a:[B

    const/16 v3, 0x20

    aget-byte v1, v1, v3

    int-to-byte v1, v1

    int-to-byte v3, v1

    sget-object v4, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a:[B

    const/16 v5, 0x36

    aget-byte v4, v4, v5

    int-to-byte v4, v4

    invoke-static {v1, v3, v4}, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a(ISB)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    sget-object v3, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a:[B

    const/4 v4, 0x1

    aget-byte v3, v3, v4

    neg-int v3, v3

    int-to-byte v3, v3

    sget-object v4, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a:[B

    const/16 v5, 0x1f

    aget-byte v4, v4, v5

    int-to-byte v4, v4

    const/16 v5, 0x35

    invoke-static {v3, v5, v4}, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a(ISB)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Class;

    const-class v5, [B

    const/4 v6, 0x0

    aput-object v5, v4, v6

    invoke-virtual {v1, v3, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    invoke-virtual {v1, v0, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_4db
    .catchall {:try_start_49c .. :try_end_4db} :catchall_495

    :try_start_4db
    invoke-virtual {v7, v11}, Ljava/util/zip/CRC32;->update([B)V

    invoke-virtual {v7}, Ljava/util/zip/CRC32;->getValue()J
    :try_end_4e1
    .catchall {:try_start_4db .. :try_end_4e1} :catchall_570

    move-result-wide v13

    int-to-long v0, v10

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-gez v0, :cond_4ea

    goto :goto_4eb

    :cond_4ea
    goto :goto_4ed

    :goto_4eb
    goto/16 :goto_c94

    :goto_4ed
    goto/16 :goto_ca7

    :pswitch_4ef
    const-wide v0, 0xc3ea30d6L

    cmp-long v0, v13, v0

    if-eqz v0, :cond_4f9

    goto :goto_4fa

    :cond_4f9
    goto :goto_4fc

    :goto_4fa
    goto/16 :goto_cba

    :goto_4fc
    goto/16 :goto_cd3

    :sswitch_4fe
    xor-int/lit8 v7, p1, 0x3

    :try_start_500
    invoke-virtual {p0}, Ljava/io/RandomAccessFile;->close()V
    :try_end_503
    .catch Ljava/lang/Throwable; {:try_start_500 .. :try_end_503} :catch_575

    return v7

    :sswitch_504
    move v10, v12

    goto/16 :goto_56a

    :pswitch_507
    const-wide v0, 0x896ee0d7L

    cmp-long v0, v13, v0

    if-eqz v0, :cond_511

    goto :goto_512

    :cond_511
    goto :goto_514

    :goto_512
    goto/16 :goto_cec

    :goto_514
    goto/16 :goto_d03

    :sswitch_516
    const-wide v0, 0xfd903b3fL

    cmp-long v0, v13, v0

    if-eqz v0, :cond_520

    goto :goto_521

    :cond_520
    goto :goto_523

    :goto_521
    goto/16 :goto_d1c

    :goto_523
    goto/16 :goto_d2f

    :sswitch_525
    const-wide v0, 0xe53be4fdL

    cmp-long v0, v13, v0

    if-nez v0, :cond_52f

    goto :goto_530

    :cond_52f
    goto :goto_532

    :goto_530
    goto/16 :goto_d47

    :goto_532
    goto/16 :goto_d5e

    :pswitch_534
    :sswitch_534
    if-ge v12, v10, :cond_537

    goto :goto_538

    :cond_537
    goto :goto_53a

    :goto_538
    goto/16 :goto_d72

    :goto_53a
    goto/16 :goto_d8a

    :sswitch_53c
    and-int/lit8 v0, p1, -0x6

    xor-int/lit8 v1, p1, -0x1

    and-int/lit8 v1, v1, 0x5

    or-int v7, v0, v1

    :try_start_544
    invoke-virtual {p0}, Ljava/io/RandomAccessFile;->close()V
    :try_end_547
    .catch Ljava/lang/Throwable; {:try_start_544 .. :try_end_547} :catch_575

    return v7

    :sswitch_548
    if-gez v12, :cond_54b

    goto :goto_54c

    :cond_54b
    goto :goto_54e

    :goto_54c
    goto/16 :goto_da1

    :goto_54e
    goto/16 :goto_db5

    :sswitch_550
    xor-int/lit8 v7, p1, 0x6

    :try_start_552
    invoke-virtual {p0}, Ljava/io/RandomAccessFile;->close()V
    :try_end_555
    .catch Ljava/lang/Throwable; {:try_start_552 .. :try_end_555} :catch_575

    return v7

    :sswitch_556
    if-le v12, v10, :cond_559

    goto :goto_55a

    :cond_559
    goto :goto_55c

    :goto_55a
    goto/16 :goto_dce

    :goto_55c
    goto/16 :goto_de1

    :sswitch_55e
    and-int/lit8 v0, p1, -0x8

    xor-int/lit8 v1, p1, -0x1

    and-int/lit8 v1, v1, 0x7

    or-int v7, v0, v1

    :try_start_566
    invoke-virtual {p0}, Ljava/io/RandomAccessFile;->close()V
    :try_end_569
    .catch Ljava/lang/Throwable; {:try_start_566 .. :try_end_569} :catch_575

    return v7

    :goto_56a
    :pswitch_56a
    :sswitch_56a
    goto/16 :goto_df9

    :pswitch_56c
    :try_start_56c
    invoke-virtual {p0}, Ljava/io/RandomAccessFile;->close()V
    :try_end_56f
    .catch Ljava/lang/Throwable; {:try_start_56c .. :try_end_56f} :catch_575

    return p1

    :catchall_570
    move-exception v7

    :try_start_571
    invoke-virtual {p0}, Ljava/io/RandomAccessFile;->close()V

    throw v7
    :try_end_575
    .catch Ljava/lang/Throwable; {:try_start_571 .. :try_end_575} :catch_575

    :catch_575
    and-int/lit8 v0, p1, -0x9

    xor-int/lit8 v1, p1, -0x1

    and-int/lit8 v1, v1, 0x8

    or-int/2addr v0, v1

    return v0

    :goto_57d
    sget v0, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->b:I

    add-int/lit8 v0, v0, 0x60

    add-int/lit8 v0, v0, -0x1

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->c:I

    rem-int/lit8 v0, v0, 0x2

    if-nez v0, :cond_58c

    goto :goto_58d

    :cond_58c
    goto :goto_58f

    :goto_58d
    goto/16 :goto_e20

    :goto_58f
    goto/16 :goto_a1d

    :goto_591
    const/4 v0, 0x1

    :try_start_592
    new-array v1, v0, [Ljava/lang/Object;

    const/4 v0, 0x0

    aput-object v8, v1, v0

    sget-object v0, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a:[B

    const/16 v2, 0x20

    aget-byte v0, v0, v2

    int-to-byte v0, v0

    int-to-byte v2, v0

    sget-object v3, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a:[B

    const/16 v4, 0x36

    aget-byte v3, v3, v4

    int-to-byte v3, v3

    invoke-static {v0, v2, v3}, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a(ISB)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    sget-object v2, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a:[B

    const/4 v3, 0x1

    aget-byte v2, v2, v3

    neg-int v2, v2

    int-to-byte v2, v2

    sget-object v3, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a:[B

    const/16 v4, 0x1f

    aget-byte v3, v3, v4

    int-to-byte v3, v3

    const/16 v4, 0x35

    invoke-static {v2, v4, v3}, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a(ISB)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Class;

    const-class v4, [B

    const/4 v5, 0x0

    aput-object v4, v3, v5

    invoke-virtual {v0, v2, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    invoke-virtual {v0, p0, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_5d1
    .catchall {:try_start_592 .. :try_end_5d1} :catchall_11b

    :try_start_5d1
    invoke-virtual {v7, v8}, Ljava/util/zip/CRC32;->update([B)V

    invoke-virtual {v7}, Ljava/util/zip/CRC32;->getValue()J
    :try_end_5d7
    .catchall {:try_start_5d1 .. :try_end_5d7} :catchall_570

    move-result-wide v0

    const-wide v2, 0xa985693fL

    cmp-long v0, v0, v2

    if-eqz v0, :cond_5e2

    goto :goto_5e3

    :cond_5e2
    goto :goto_5e5

    :goto_5e3
    goto/16 :goto_e34

    :goto_5e5
    goto/16 :goto_a28

    :pswitch_5e7
    sget v0, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->b:I

    add-int/lit8 v0, v0, 0x59

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->c:I

    rem-int/lit8 v0, v0, 0x2

    if-nez v0, :cond_5f4

    goto :goto_5f5

    :cond_5f4
    goto :goto_5f7

    :goto_5f5
    goto/16 :goto_e47

    :goto_5f7
    goto/16 :goto_e5f

    :goto_5f9
    :try_start_5f9
    sget-object v0, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a:[B

    const/16 v1, 0x20

    aget-byte v0, v0, v1

    int-to-byte v0, v0

    int-to-byte v1, v0

    sget-object v2, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a:[B

    const/16 v3, 0x36

    aget-byte v2, v2, v3

    int-to-byte v2, v2

    invoke-static {v0, v1, v2}, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a(ISB)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    sget-object v1, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a:[B

    const/4 v2, 0x1

    aget-byte v1, v1, v2

    neg-int v1, v1

    int-to-byte v1, v1

    sget-object v2, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a:[B

    const/16 v3, 0xa

    aget-byte v3, v2, v3

    xor-int/lit8 v2, v3, 0x1

    and-int/lit8 v3, v3, 0x1

    shl-int/lit8 v3, v3, 0x1

    add-int/2addr v2, v3

    int-to-byte v2, v2

    sget-object v3, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a:[B

    const/16 v4, 0x1f

    aget-byte v3, v3, v4

    int-to-byte v3, v3

    invoke-static {v1, v2, v3}, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a(ISB)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p0, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Short;

    invoke-virtual {v0}, Ljava/lang/Short;->shortValue()S
    :try_end_63f
    .catchall {:try_start_5f9 .. :try_end_63f} :catchall_185

    move-result v0

    :try_start_640
    invoke-static {v0}, Ljava/lang/Short;->reverseBytes(S)S
    :try_end_643
    .catchall {:try_start_640 .. :try_end_643} :catchall_570

    move-result v9

    move-object v0, p0

    goto/16 :goto_1de

    :pswitch_647
    sget v2, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->b:I

    add-int/lit8 v2, v2, 0x5

    rem-int/lit16 v3, v2, 0x80

    sput v3, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->c:I

    rem-int/lit8 v2, v2, 0x2

    if-nez v2, :cond_654

    goto :goto_655

    :cond_654
    goto :goto_657

    :goto_655
    goto/16 :goto_a3c

    :goto_657
    goto/16 :goto_a3a

    :goto_659
    const/4 v2, 0x1

    :try_start_65a
    new-array v2, v2, [Ljava/lang/Object;

    const/4 v1, 0x6

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v3, 0x0

    aput-object v1, v2, v3

    sget-object v1, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a:[B

    const/16 v3, 0x20

    aget-byte v1, v1, v3

    int-to-byte v1, v1

    int-to-byte v3, v1

    sget-object v4, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a:[B

    const/16 v5, 0x36

    aget-byte v4, v4, v5

    int-to-byte v4, v4

    invoke-static {v1, v3, v4}, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a(ISB)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    sget-object v3, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a:[B

    const/4 v4, 0x1

    aget-byte v3, v3, v4

    neg-int v3, v3

    int-to-byte v3, v3

    sget-object v4, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a:[B

    const/16 v5, 0x20

    aget-byte v4, v4, v5

    int-to-byte v4, v4

    const/16 v5, 0x4b

    invoke-static {v3, v5, v4}, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a(ISB)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Class;

    sget-object v5, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    const/4 v6, 0x0

    aput-object v5, v4, v6

    invoke-virtual {v1, v3, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    invoke-virtual {v1, v0, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
    :try_end_6a4
    .catchall {:try_start_65a .. :try_end_6a4} :catchall_1d8

    move-object v0, p0

    goto/16 :goto_e77

    :pswitch_6a7
    sget v2, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->b:I

    xor-int/lit8 v1, v2, 0xf

    and-int/lit8 v2, v2, 0xf

    shl-int/lit8 v2, v2, 0x1

    add-int/2addr v1, v2

    rem-int/lit16 v2, v1, 0x80

    sput v2, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->c:I

    rem-int/lit8 v1, v1, 0x2

    if-nez v1, :cond_6b9

    goto :goto_6ba

    :cond_6b9
    goto :goto_6bc

    :goto_6ba
    goto/16 :goto_ecf

    :goto_6bc
    goto/16 :goto_a43

    :goto_6be
    :try_start_6be
    sget-object v1, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a:[B

    const/16 v2, 0x20

    aget-byte v1, v1, v2

    int-to-byte v1, v1

    int-to-byte v2, v1

    sget-object v3, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a:[B

    const/16 v4, 0x36

    aget-byte v3, v3, v4

    int-to-byte v3, v3

    invoke-static {v1, v2, v3}, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a(ISB)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    sget-object v2, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a:[B

    const/16 v3, 0x23

    aget-byte v2, v2, v3

    neg-int v2, v2

    int-to-byte v2, v2

    sget-object v3, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a:[B

    const/16 v4, 0x1f

    aget-byte v3, v3, v4

    int-to-byte v3, v3

    const/16 v4, 0x45

    invoke-static {v2, v4, v3}, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a(ISB)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I
    :try_end_6f9
    .catchall {:try_start_6be .. :try_end_6f9} :catchall_22c

    move-result v1

    :try_start_6fa
    invoke-static {v1}, Ljava/lang/Integer;->reverseBytes(I)I
    :try_end_6fd
    .catchall {:try_start_6fa .. :try_end_6fd} :catchall_570

    move-result v1

    int-to-long v1, v1

    goto/16 :goto_ee7

    :pswitch_701
    sget v3, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->c:I

    add-int/lit8 v3, v3, 0x66

    add-int/lit8 v3, v3, -0x1

    rem-int/lit16 v4, v3, 0x80

    sput v4, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->b:I

    rem-int/lit8 v3, v3, 0x2

    if-eqz v3, :cond_710

    goto :goto_711

    :cond_710
    goto :goto_713

    :goto_711
    goto/16 :goto_f4b

    :goto_713
    goto/16 :goto_a4b

    :sswitch_715
    const/4 v3, 0x1

    :try_start_716
    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v3, v2

    sget-object v1, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a:[B

    const/16 v2, 0x20

    aget-byte v1, v1, v2

    int-to-byte v1, v1

    int-to-byte v2, v1

    sget-object v4, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a:[B

    const/16 v5, 0x36

    aget-byte v4, v4, v5

    int-to-byte v4, v4

    invoke-static {v1, v2, v4}, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a(ISB)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    sget-object v2, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a:[B

    const/16 v4, 0xc

    aget-byte v2, v2, v4

    neg-int v2, v2

    int-to-byte v2, v2

    sget-object v4, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a:[B

    const/16 v5, 0x21

    aget-byte v4, v4, v5

    add-int/lit8 v4, v4, 0x1

    int-to-byte v4, v4

    sget-object v5, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a:[B

    const/16 v6, 0x20

    aget-byte v5, v5, v6

    int-to-byte v5, v5

    invoke-static {v2, v4, v5}, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a(ISB)Ljava/lang/String;

    move-result-object v2

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Class;

    sget-object v5, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    const/4 v6, 0x0

    aput-object v5, v4, v6

    invoke-virtual {v1, v2, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    invoke-virtual {v1, v0, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_761
    .catchall {:try_start_716 .. :try_end_761} :catchall_275

    const/4 v10, -0x1

    goto/16 :goto_f5f

    :pswitch_764
    sget v0, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->c:I

    add-int/lit8 v0, v0, 0x1a

    add-int/lit8 v0, v0, -0x1

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->b:I

    rem-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_773

    goto :goto_774

    :cond_773
    goto :goto_776

    :goto_774
    goto/16 :goto_f88

    :goto_776
    goto/16 :goto_fa0

    :sswitch_778
    move-object v0, p0

    move-object v1, v8

    goto/16 :goto_fb8

    :goto_77c
    sget v2, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->c:I

    add-int/lit8 v2, v2, 0x1

    rem-int/lit16 v3, v2, 0x80

    sput v3, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->b:I

    rem-int/lit8 v2, v2, 0x2

    if-eqz v2, :cond_789

    goto :goto_78a

    :cond_789
    goto :goto_78c

    :goto_78a
    goto/16 :goto_1020

    :goto_78c
    goto/16 :goto_1037

    :sswitch_78e
    const/4 v2, 0x1

    :try_start_78f
    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v1, v2, v3

    sget-object v1, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a:[B

    const/16 v3, 0x20

    aget-byte v1, v1, v3

    int-to-byte v1, v1

    int-to-byte v3, v1

    sget-object v4, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a:[B

    const/16 v5, 0x36

    aget-byte v4, v4, v5

    int-to-byte v4, v4

    invoke-static {v1, v3, v4}, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a(ISB)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    sget-object v3, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a:[B

    const/4 v4, 0x1

    aget-byte v3, v3, v4

    neg-int v3, v3

    int-to-byte v3, v3

    sget-object v4, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a:[B

    const/16 v5, 0x1f

    aget-byte v4, v4, v5

    int-to-byte v4, v4

    const/16 v5, 0x35

    invoke-static {v3, v5, v4}, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a(ISB)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Class;

    const-class v5, [B

    const/4 v6, 0x0

    aput-object v5, v4, v6

    invoke-virtual {v1, v3, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    invoke-virtual {v1, v0, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_7ce
    .catchall {:try_start_78f .. :try_end_7ce} :catchall_2e4

    :try_start_7ce
    invoke-virtual {v7}, Ljava/util/zip/CRC32;->reset()V

    invoke-virtual {v7, v8}, Ljava/util/zip/CRC32;->update([B)V

    invoke-virtual {v7}, Ljava/util/zip/CRC32;->getValue()J
    :try_end_7d7
    .catchall {:try_start_7ce .. :try_end_7d7} :catchall_570

    move-result-wide v0

    const-wide/32 v2, 0x52c68507

    cmp-long v0, v0, v2

    if-eqz v0, :cond_7e0

    goto :goto_7e1

    :cond_7e0
    goto :goto_7e3

    :goto_7e1
    goto/16 :goto_a6a

    :goto_7e3
    goto/16 :goto_104d

    :sswitch_7e5
    sget v1, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->c:I

    xor-int/lit8 v0, v1, 0x75

    and-int/lit8 v1, v1, 0x75

    shl-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->b:I

    rem-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_7f7

    goto :goto_7f8

    :cond_7f7
    goto :goto_7fa

    :goto_7f8
    goto/16 :goto_1063

    :goto_7fa
    goto/16 :goto_107c

    :sswitch_7fc
    const/4 v0, 0x1

    :try_start_7fd
    new-array v1, v0, [Ljava/lang/Object;

    const/16 v0, 0x14

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const/4 v2, 0x0

    aput-object v0, v1, v2

    sget-object v0, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a:[B

    const/16 v2, 0x20

    aget-byte v0, v0, v2

    int-to-byte v0, v0

    int-to-byte v2, v0

    sget-object v3, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a:[B

    const/16 v4, 0x36

    aget-byte v3, v3, v4

    int-to-byte v3, v3

    invoke-static {v0, v2, v3}, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a(ISB)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    sget-object v2, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a:[B

    const/4 v3, 0x1

    aget-byte v2, v2, v3

    neg-int v2, v2

    int-to-byte v2, v2

    sget-object v3, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a:[B

    const/16 v4, 0x20

    aget-byte v3, v3, v4

    int-to-byte v3, v3

    const/16 v4, 0x4b

    invoke-static {v2, v4, v3}, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a(ISB)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Class;

    sget-object v4, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    const/4 v5, 0x0

    aput-object v4, v3, v5

    invoke-virtual {v0, v2, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    invoke-virtual {v0, p0, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
    :try_end_848
    .catchall {:try_start_7fd .. :try_end_848} :catchall_34f

    move-object v0, p0

    goto/16 :goto_3aa

    :sswitch_84b
    sget v2, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->c:I

    or-int/lit8 v1, v2, 0x21

    shl-int/lit8 v1, v1, 0x1

    xor-int/lit8 v2, v2, 0x21

    sub-int/2addr v1, v2

    rem-int/lit16 v2, v1, 0x80

    sput v2, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->b:I

    rem-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_85d

    goto :goto_85e

    :cond_85d
    goto :goto_860

    :goto_85e
    goto/16 :goto_a7c

    :goto_860
    goto/16 :goto_1095

    :pswitch_862
    :try_start_862
    sget-object v1, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a:[B

    const/16 v2, 0x20

    aget-byte v1, v1, v2

    int-to-byte v1, v1

    int-to-byte v2, v1

    sget-object v3, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a:[B

    const/16 v4, 0x36

    aget-byte v3, v3, v4

    int-to-byte v3, v3

    invoke-static {v1, v2, v3}, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a(ISB)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    sget-object v2, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a:[B

    const/4 v3, 0x1

    aget-byte v2, v2, v3

    neg-int v2, v2

    int-to-byte v2, v2

    sget-object v3, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a:[B

    const/16 v4, 0xa

    aget-byte v4, v3, v4

    or-int/lit8 v3, v4, 0x1

    shl-int/lit8 v3, v3, 0x1

    xor-int/lit8 v4, v4, 0x1

    sub-int/2addr v3, v4

    int-to-byte v3, v3

    sget-object v4, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a:[B

    const/16 v5, 0x1f

    aget-byte v4, v4, v5

    int-to-byte v4, v4

    invoke-static {v2, v3, v4}, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a(ISB)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Short;

    invoke-virtual {v0}, Ljava/lang/Short;->shortValue()S
    :try_end_8a8
    .catchall {:try_start_862 .. :try_end_8a8} :catchall_3a4

    move-result v0

    :try_start_8a9
    invoke-static {v0}, Ljava/lang/Short;->reverseBytes(S)S

    move-result v0

    new-array v11, v0, [B
    :try_end_8af
    .catchall {:try_start_8a9 .. :try_end_8af} :catchall_570

    move-object v0, p0

    goto/16 :goto_10ad

    :goto_8b2
    sget v3, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->b:I

    xor-int/lit8 v2, v3, 0x49

    and-int/lit8 v3, v3, 0x49

    shl-int/lit8 v3, v3, 0x1

    add-int/2addr v2, v3

    rem-int/lit16 v3, v2, 0x80

    sput v3, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->c:I

    rem-int/lit8 v2, v2, 0x2

    if-nez v2, :cond_8c4

    goto :goto_8c5

    :cond_8c4
    goto :goto_8c7

    :goto_8c5
    goto/16 :goto_a84

    :goto_8c7
    goto/16 :goto_a82

    :sswitch_8c9
    const/4 v2, 0x1

    :try_start_8ca
    new-array v2, v2, [Ljava/lang/Object;

    const/16 v1, 0xc

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v3, 0x0

    aput-object v1, v2, v3

    sget-object v1, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a:[B

    const/16 v3, 0x20

    aget-byte v1, v1, v3

    int-to-byte v1, v1

    int-to-byte v3, v1

    sget-object v4, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a:[B

    const/16 v5, 0x36

    aget-byte v4, v4, v5

    int-to-byte v4, v4

    invoke-static {v1, v3, v4}, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a(ISB)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    sget-object v3, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a:[B

    const/4 v4, 0x1

    aget-byte v3, v3, v4

    neg-int v3, v3

    int-to-byte v3, v3

    sget-object v4, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a:[B

    const/16 v5, 0x20

    aget-byte v4, v4, v5

    int-to-byte v4, v4

    const/16 v5, 0x4b

    invoke-static {v3, v5, v4}, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a(ISB)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Class;

    sget-object v5, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    const/4 v6, 0x0

    aput-object v5, v4, v6

    invoke-virtual {v1, v3, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    invoke-virtual {v1, v0, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
    :try_end_915
    .catchall {:try_start_8ca .. :try_end_915} :catchall_3f7

    move-object v0, p0

    goto/16 :goto_110f

    :pswitch_918
    sget v1, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->c:I

    xor-int/lit8 v0, v1, 0x47

    and-int/lit8 v1, v1, 0x47

    shl-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->b:I

    rem-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_92a

    goto :goto_92b

    :cond_92a
    goto :goto_92d

    :goto_92b
    goto/16 :goto_a8e

    :goto_92d
    goto/16 :goto_a8b

    :sswitch_92f
    const-wide v0, 0xe53be4fdL

    cmp-long v0, v13, v0

    if-nez v0, :cond_939

    goto :goto_93a

    :cond_939
    goto :goto_93c

    :goto_93a
    goto/16 :goto_a98

    :goto_93c
    goto/16 :goto_a95

    :sswitch_93e
    sget v1, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->b:I

    or-int/lit8 v0, v1, 0x13

    shl-int/lit8 v0, v0, 0x1

    xor-int/lit8 v1, v1, 0x13

    sub-int/2addr v0, v1

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->c:I

    rem-int/lit8 v0, v0, 0x2

    if-nez v0, :cond_950

    goto :goto_951

    :cond_950
    goto :goto_953

    :goto_951
    goto/16 :goto_aa2

    :goto_953
    goto/16 :goto_a9f

    :sswitch_955
    xor-int/lit8 v7, p1, 0x5

    :try_start_957
    invoke-virtual {p0}, Ljava/io/RandomAccessFile;->close()V
    :try_end_95a
    .catch Ljava/lang/Throwable; {:try_start_957 .. :try_end_95a} :catch_575

    return v7

    :sswitch_95b
    sget v1, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->c:I

    and-int/lit8 v0, v1, 0x1d

    or-int/lit8 v1, v1, 0x1d

    add-int/2addr v0, v1

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->b:I

    rem-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_96b

    goto :goto_96c

    :cond_96b
    goto :goto_96e

    :goto_96c
    goto/16 :goto_aac

    :goto_96e
    goto/16 :goto_aa9

    :sswitch_970
    xor-int/lit8 v7, p1, 0x6

    :try_start_972
    invoke-virtual {p0}, Ljava/io/RandomAccessFile;->close()V
    :try_end_975
    .catch Ljava/lang/Throwable; {:try_start_972 .. :try_end_975} :catch_575

    return v7

    :pswitch_976
    sget v0, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->c:I

    add-int/lit8 v0, v0, 0x7a

    add-int/lit8 v0, v0, -0x1

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->b:I

    rem-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_985

    goto :goto_986

    :cond_985
    goto :goto_988

    :goto_986
    goto/16 :goto_ab6

    :goto_988
    goto/16 :goto_ab3

    :sswitch_98a
    xor-int/lit8 v7, p1, 0x7

    :try_start_98c
    invoke-virtual {p0}, Ljava/io/RandomAccessFile;->close()V
    :try_end_98f
    .catch Ljava/lang/Throwable; {:try_start_98c .. :try_end_98f} :catch_575

    return v7

    :sswitch_990
    sget v1, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->c:I

    and-int/lit8 v0, v1, 0x25

    or-int/lit8 v1, v1, 0x25

    add-int/2addr v0, v1

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->b:I

    rem-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_9a0

    goto :goto_9a1

    :cond_9a0
    goto :goto_9a3

    :goto_9a1
    goto/16 :goto_ac0

    :goto_9a3
    goto/16 :goto_abd

    :sswitch_9a5
    move v0, v9

    xor-int/lit8 v1, v9, -0x1

    and-int/lit8 v2, v9, -0x1

    shl-int/lit8 v2, v2, 0x1

    add-int/2addr v1, v2

    move v9, v1

    if-lez v0, :cond_9b1

    goto :goto_9b2

    :cond_9b1
    goto :goto_9b4

    :goto_9b2
    goto/16 :goto_ac9

    :goto_9b4
    goto/16 :goto_ac7

    :goto_9b6
    packed-switch v0, :pswitch_data_1322

    goto/16 :goto_177

    :pswitch_9bb
    const/4 v0, 0x1

    goto :goto_9b6

    :pswitch_9bd
    const/4 v0, 0x0

    goto :goto_9b6

    :goto_9bf
    packed-switch v0, :pswitch_data_1328

    nop

    :pswitch_9c3
    const/4 v0, 0x0

    goto :goto_9bf

    :sswitch_9c5
    const/4 v0, 0x1

    goto :goto_9bf

    :goto_9c7
    packed-switch v0, :pswitch_data_1330

    nop

    :goto_9cb
    const/4 v0, 0x0

    goto :goto_9c7

    :pswitch_9cd
    const/4 v0, 0x1

    goto :goto_9c7

    :goto_9cf
    packed-switch v0, :pswitch_data_1338

    nop

    :pswitch_9d3
    const/4 v0, 0x0

    goto :goto_9cf

    :pswitch_9d5
    const/4 v0, 0x1

    goto :goto_9cf

    :goto_9d7
    sparse-switch v0, :sswitch_data_1340

    nop

    :sswitch_9db
    const/16 v0, 0x18

    goto :goto_9d7

    :pswitch_9de
    const/16 v0, 0x33

    goto :goto_9d7

    :goto_9e1
    sparse-switch v0, :sswitch_data_134a

    nop

    :sswitch_9e5
    const/16 v0, 0x57

    goto :goto_9e1

    :pswitch_9e8
    const/16 v0, 0x20

    goto :goto_9e1

    :goto_9eb
    packed-switch v0, :pswitch_data_1354

    nop

    :pswitch_9ef
    const/4 v0, 0x1

    goto :goto_9eb

    :sswitch_9f1
    const/4 v0, 0x0

    goto :goto_9eb

    :goto_9f3
    sparse-switch v0, :sswitch_data_135c

    goto :goto_9fa

    :pswitch_9f7
    const/16 v0, 0x31

    goto :goto_9f3

    :goto_9fa
    :pswitch_9fa
    const/16 v0, 0x2e

    goto :goto_9f3

    :goto_9fd
    sparse-switch v0, :sswitch_data_1366

    goto :goto_a04

    :sswitch_a01
    const/16 v0, 0x3a

    goto :goto_9fd

    :goto_a04
    :sswitch_a04
    const/4 v0, 0x3

    goto :goto_9fd

    :goto_a06
    sparse-switch v0, :sswitch_data_1370

    nop

    :sswitch_a0a
    const/16 v0, 0x34

    goto :goto_a06

    :sswitch_a0d
    const/16 v0, 0x24

    goto :goto_a06

    :goto_a10
    packed-switch v0, :pswitch_data_137a

    nop

    :pswitch_a14
    const/4 v0, 0x0

    goto :goto_a10

    :pswitch_a16
    const/4 v0, 0x1

    goto :goto_a10

    :goto_a18
    sparse-switch v0, :sswitch_data_1382

    goto/16 :goto_591

    :goto_a1d
    const/16 v0, 0x24

    goto :goto_a18

    :sswitch_a20
    const/16 v0, 0x60

    goto :goto_a18

    :goto_a23
    packed-switch v0, :pswitch_data_1388

    goto/16 :goto_177

    :goto_a28
    const/4 v0, 0x0

    goto :goto_a23

    :pswitch_a2a
    const/4 v0, 0x1

    goto :goto_a23

    :goto_a2c
    packed-switch v0, :pswitch_data_138e

    goto/16 :goto_5f9

    :sswitch_a31
    const/4 v0, 0x0

    goto :goto_a2c

    :sswitch_a33
    const/4 v0, 0x1

    goto :goto_a2c

    :goto_a35
    packed-switch v2, :pswitch_data_1394

    goto/16 :goto_659

    :goto_a3a
    const/4 v2, 0x0

    goto :goto_a35

    :goto_a3c
    const/4 v2, 0x1

    goto :goto_a35

    :goto_a3e
    packed-switch v1, :pswitch_data_139a

    goto/16 :goto_6be

    :goto_a43
    const/4 v1, 0x0

    goto :goto_a3e

    :sswitch_a45
    const/4 v1, 0x1

    goto :goto_a3e

    :goto_a47
    sparse-switch v3, :sswitch_data_13a0

    nop

    :goto_a4b
    const/16 v3, 0x52

    goto :goto_a47

    :sswitch_a4e
    const/16 v3, 0x38

    goto :goto_a47

    :goto_a51
    sparse-switch v0, :sswitch_data_13aa

    nop

    :sswitch_a55
    const/4 v0, 0x2

    goto :goto_a51

    :sswitch_a57
    const/4 v0, 0x1

    goto :goto_a51

    :goto_a59
    sparse-switch v2, :sswitch_data_13b4

    goto :goto_a60

    :pswitch_a5d
    const/16 v2, 0x4b

    goto :goto_a59

    :goto_a60
    :sswitch_a60
    const/16 v2, 0x25

    goto :goto_a59

    :goto_a63
    packed-switch v0, :pswitch_data_13be

    goto/16 :goto_341

    :pswitch_a68
    const/4 v0, 0x1

    goto :goto_a63

    :goto_a6a
    const/4 v0, 0x0

    goto :goto_a63

    :goto_a6c
    sparse-switch v0, :sswitch_data_13c4

    goto :goto_a73

    :pswitch_a70
    const/16 v0, 0x1d

    goto :goto_a6c

    :goto_a73
    :sswitch_a73
    const/16 v0, 0x56

    goto :goto_a6c

    :goto_a76
    packed-switch v1, :pswitch_data_13ce

    goto :goto_a7c

    :pswitch_a7a
    const/4 v1, 0x0

    goto :goto_a76

    :goto_a7c
    const/4 v1, 0x1

    goto :goto_a76

    :goto_a7e
    sparse-switch v2, :sswitch_data_13d6

    goto :goto_a84

    :goto_a82
    const/4 v2, 0x2

    goto :goto_a7e

    :goto_a84
    const/16 v2, 0x17

    goto :goto_a7e

    :goto_a87
    sparse-switch v0, :sswitch_data_13e0

    nop

    :goto_a8b
    const/16 v0, 0x40

    goto :goto_a87

    :goto_a8e
    const/16 v0, 0x35

    goto :goto_a87

    :goto_a91
    sparse-switch v0, :sswitch_data_13ea

    goto :goto_a98

    :goto_a95
    const/16 v0, 0x42

    goto :goto_a91

    :goto_a98
    const/16 v0, 0x47

    goto :goto_a91

    :goto_a9b
    sparse-switch v0, :sswitch_data_13f4

    nop

    :goto_a9f
    const/16 v0, 0x51

    goto :goto_a9b

    :goto_aa2
    const/16 v0, 0x3c

    goto :goto_a9b

    :goto_aa5
    sparse-switch v0, :sswitch_data_13fe

    nop

    :goto_aa9
    const/16 v0, 0x56

    goto :goto_aa5

    :goto_aac
    const/16 v0, 0x3d

    goto :goto_aa5

    :goto_aaf
    sparse-switch v0, :sswitch_data_1408

    nop

    :goto_ab3
    const/16 v0, 0xd

    goto :goto_aaf

    :goto_ab6
    const/16 v0, 0x12

    goto :goto_aaf

    :goto_ab9
    sparse-switch v0, :sswitch_data_1412

    goto :goto_ac0

    :goto_abd
    const/16 v0, 0x60

    goto :goto_ab9

    :goto_ac0
    const/16 v0, 0x10

    goto :goto_ab9

    :goto_ac3
    packed-switch v0, :pswitch_data_141c

    goto :goto_ac9

    :goto_ac7
    const/4 v0, 0x0

    goto :goto_ac3

    :goto_ac9
    const/4 v0, 0x1

    goto :goto_ac3

    :goto_acb
    sget v0, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->c:I

    add-int/lit8 v0, v0, 0x13

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->b:I

    rem-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_ad9

    goto/16 :goto_1169

    :cond_ad9
    goto/16 :goto_1167

    :pswitch_adb
    const/4 v0, 0x0

    goto/16 :goto_9b6

    :goto_ade
    sget v0, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->b:I

    add-int/lit8 v0, v0, 0x46

    add-int/lit8 v0, v0, -0x1

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->c:I

    rem-int/lit8 v0, v0, 0x2

    if-nez v0, :cond_aee

    goto/16 :goto_1171

    :cond_aee
    goto/16 :goto_116f

    :pswitch_af0
    const/4 v0, 0x1

    goto/16 :goto_9b6

    :goto_af3
    sget v0, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->c:I

    add-int/lit8 v0, v0, 0x62

    add-int/lit8 v0, v0, -0x1

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->b:I

    rem-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_b03

    goto/16 :goto_1179

    :cond_b03
    goto/16 :goto_1177

    :pswitch_b05
    sget v0, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->b:I

    add-int/lit8 v0, v0, 0x59

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->c:I

    rem-int/lit8 v0, v0, 0x2

    if-nez v0, :cond_b13

    goto/16 :goto_5f5

    :cond_b13
    goto/16 :goto_5f7

    :goto_b15
    sget v3, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->b:I

    and-int/lit8 v2, v3, 0x57

    or-int/lit8 v3, v3, 0x57

    add-int/2addr v2, v3

    rem-int/lit16 v3, v2, 0x80

    sput v3, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->c:I

    rem-int/lit8 v2, v2, 0x2

    if-nez v2, :cond_b26

    goto/16 :goto_1181

    :cond_b26
    goto/16 :goto_117f

    :pswitch_b28
    sget v2, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->b:I

    add-int/lit8 v2, v2, 0x5

    rem-int/lit16 v3, v2, 0x80

    sput v3, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->c:I

    rem-int/lit8 v2, v2, 0x2

    if-nez v2, :cond_b36

    goto/16 :goto_655

    :cond_b36
    goto/16 :goto_657

    :goto_b38
    sget v2, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->b:I

    xor-int/lit8 v1, v2, 0x79

    and-int/lit8 v2, v2, 0x79

    shl-int/lit8 v2, v2, 0x1

    add-int/2addr v1, v2

    rem-int/lit16 v2, v1, 0x80

    sput v2, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->c:I

    rem-int/lit8 v1, v1, 0x2

    if-nez v1, :cond_b4b

    goto/16 :goto_1189

    :cond_b4b
    goto/16 :goto_1187

    :pswitch_b4d
    sget v1, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->b:I

    add-int/lit8 v1, v1, 0x10

    add-int/lit8 v1, v1, -0x1

    rem-int/lit16 v2, v1, 0x80

    sput v2, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->c:I

    rem-int/lit8 v1, v1, 0x2

    if-nez v1, :cond_b5d

    goto/16 :goto_6ba

    :cond_b5d
    goto/16 :goto_6bc

    :goto_b5f
    sget v4, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->b:I

    or-int/lit8 v3, v4, 0x71

    shl-int/lit8 v3, v3, 0x1

    xor-int/lit8 v4, v4, 0x71

    sub-int/2addr v3, v4

    rem-int/lit16 v4, v3, 0x80

    sput v4, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->c:I

    rem-int/lit8 v3, v3, 0x2

    if-nez v3, :cond_b72

    goto/16 :goto_1191

    :cond_b72
    goto/16 :goto_118f

    :pswitch_b74
    sget v4, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->c:I

    xor-int/lit8 v3, v4, 0x65

    and-int/lit8 v4, v4, 0x65

    shl-int/lit8 v4, v4, 0x1

    add-int/2addr v3, v4

    rem-int/lit16 v4, v3, 0x80

    sput v4, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->b:I

    rem-int/lit8 v3, v3, 0x2

    if-eqz v3, :cond_b87

    goto/16 :goto_711

    :cond_b87
    goto/16 :goto_713

    :goto_b89
    sget v0, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->c:I

    add-int/lit8 v0, v0, 0x40

    add-int/lit8 v0, v0, -0x1

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->b:I

    rem-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_b99

    goto/16 :goto_119a

    :cond_b99
    goto/16 :goto_1197

    :sswitch_b9b
    move v0, v9

    add-int/lit8 v1, v9, -0x32

    add-int/lit8 v2, v1, -0x1

    and-int/lit8 v1, v2, 0x32

    or-int/lit8 v2, v2, 0x32

    add-int/2addr v1, v2

    move v9, v1

    if-lez v0, :cond_baa

    goto/16 :goto_2dc

    :cond_baa
    goto/16 :goto_2de

    :goto_bac
    sget v1, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->b:I

    and-int/lit8 v0, v1, 0x51

    or-int/lit8 v1, v1, 0x51

    add-int/2addr v0, v1

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->c:I

    rem-int/lit8 v0, v0, 0x2

    if-nez v0, :cond_bbd

    goto/16 :goto_11a4

    :cond_bbd
    goto/16 :goto_11a1

    :sswitch_bbf
    const/4 v0, 0x1

    goto/16 :goto_9bf

    :goto_bc2
    sget v1, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->b:I

    and-int/lit8 v0, v1, 0x9

    or-int/lit8 v1, v1, 0x9

    add-int/2addr v0, v1

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->c:I

    rem-int/lit8 v0, v0, 0x2

    if-nez v0, :cond_bd3

    goto/16 :goto_11ad

    :cond_bd3
    goto/16 :goto_11ab

    :pswitch_bd5
    const/4 v0, 0x0

    goto/16 :goto_9bf

    :goto_bd8
    sget v0, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->c:I

    add-int/lit8 v0, v0, 0xf

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->b:I

    rem-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_be6

    goto/16 :goto_11b5

    :cond_be6
    goto/16 :goto_11b3

    :pswitch_be8
    const/4 v0, 0x1

    goto/16 :goto_9c7

    :goto_beb
    sget v0, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->b:I

    add-int/lit8 v0, v0, 0x37

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->c:I

    rem-int/lit8 v0, v0, 0x2

    if-nez v0, :cond_bf9

    goto/16 :goto_11be

    :cond_bf9
    goto/16 :goto_11bb

    :sswitch_bfb
    sget v0, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->c:I

    add-int/lit8 v0, v0, 0x75

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->b:I

    rem-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_c09

    goto/16 :goto_7f8

    :cond_c09
    goto/16 :goto_7fa

    :goto_c0b
    sget v2, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->b:I

    xor-int/lit8 v1, v2, 0x37

    and-int/lit8 v2, v2, 0x37

    shl-int/lit8 v2, v2, 0x1

    add-int/2addr v1, v2

    rem-int/lit16 v2, v1, 0x80

    sput v2, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->c:I

    rem-int/lit8 v1, v1, 0x2

    if-nez v1, :cond_c1e

    goto/16 :goto_11c8

    :cond_c1e
    goto/16 :goto_11c5

    :sswitch_c20
    sget v1, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->c:I

    add-int/lit8 v1, v1, 0x21

    rem-int/lit16 v2, v1, 0x80

    sput v2, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->b:I

    rem-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_c2e

    goto/16 :goto_85e

    :cond_c2e
    goto/16 :goto_860

    :goto_c30
    sget v2, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->c:I

    add-int/lit8 v2, v2, 0x2

    add-int/lit8 v2, v2, -0x1

    rem-int/lit16 v3, v2, 0x80

    sput v3, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->b:I

    rem-int/lit8 v2, v2, 0x2

    if-eqz v2, :cond_c40

    goto/16 :goto_11d2

    :cond_c40
    goto/16 :goto_11cf

    :sswitch_c42
    const/4 v2, 0x1

    :try_start_c43
    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v1, v2, v3

    sget-object v1, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a:[B

    const/16 v3, 0x20

    aget-byte v1, v1, v3

    int-to-byte v1, v1

    int-to-byte v3, v1

    sget-object v4, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a:[B

    const/16 v5, 0x36

    aget-byte v4, v4, v5

    int-to-byte v4, v4

    invoke-static {v1, v3, v4}, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a(ISB)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    sget-object v3, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a:[B

    const/4 v4, 0x1

    aget-byte v3, v3, v4

    neg-int v3, v3

    int-to-byte v3, v3

    sget-object v4, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a:[B

    const/16 v5, 0x1f

    aget-byte v4, v4, v5

    int-to-byte v4, v4

    const/16 v5, 0x35

    invoke-static {v3, v5, v4}, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a(ISB)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Class;

    const-class v5, [B

    const/4 v6, 0x0

    aput-object v5, v4, v6

    invoke-virtual {v1, v3, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    invoke-virtual {v1, v0, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_c82
    .catchall {:try_start_c43 .. :try_end_c82} :catchall_495

    :try_start_c82
    invoke-virtual {v7, v11}, Ljava/util/zip/CRC32;->update([B)V

    invoke-virtual {v7}, Ljava/util/zip/CRC32;->getValue()J
    :try_end_c88
    .catchall {:try_start_c82 .. :try_end_c88} :catchall_570

    move-result-wide v13

    int-to-long v0, v10

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-gez v0, :cond_c92

    goto/16 :goto_4eb

    :cond_c92
    goto/16 :goto_4ed

    :goto_c94
    sget v0, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->b:I

    add-int/lit8 v0, v0, 0x71

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->c:I

    rem-int/lit8 v0, v0, 0x2

    if-nez v0, :cond_ca2

    goto/16 :goto_11db

    :cond_ca2
    goto/16 :goto_11d9

    :pswitch_ca4
    const/4 v0, 0x1

    goto/16 :goto_9cf

    :goto_ca7
    sget v0, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->c:I

    add-int/lit8 v0, v0, 0x19

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->b:I

    rem-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_cb5

    goto/16 :goto_11e3

    :cond_cb5
    goto/16 :goto_11e1

    :pswitch_cb7
    const/4 v0, 0x0

    goto/16 :goto_9cf

    :goto_cba
    sget v1, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->c:I

    xor-int/lit8 v0, v1, 0x29

    and-int/lit8 v1, v1, 0x29

    shl-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->b:I

    rem-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_ccd

    goto/16 :goto_11eb

    :cond_ccd
    goto/16 :goto_11e9

    :pswitch_ccf
    const/16 v0, 0x33

    goto/16 :goto_9d7

    :goto_cd3
    sget v1, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->c:I

    or-int/lit8 v0, v1, 0x15

    shl-int/lit8 v0, v0, 0x1

    xor-int/lit8 v1, v1, 0x15

    sub-int/2addr v0, v1

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->b:I

    rem-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_ce6

    goto/16 :goto_11f4

    :cond_ce6
    goto/16 :goto_11f1

    :sswitch_ce8
    const/16 v0, 0x18

    goto/16 :goto_9d7

    :goto_cec
    sget v1, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->c:I

    and-int/lit8 v0, v1, 0x4d

    or-int/lit8 v1, v1, 0x4d

    add-int/2addr v0, v1

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->b:I

    rem-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_cfd

    goto/16 :goto_11fd

    :cond_cfd
    goto/16 :goto_11fb

    :pswitch_cff
    const/16 v0, 0x20

    goto/16 :goto_9e1

    :goto_d03
    sget v1, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->c:I

    xor-int/lit8 v0, v1, 0x6f

    and-int/lit8 v1, v1, 0x6f

    shl-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->b:I

    rem-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_d16

    goto/16 :goto_1205

    :cond_d16
    goto/16 :goto_1203

    :sswitch_d18
    const/16 v0, 0x57

    goto/16 :goto_9e1

    :goto_d1c
    sget v0, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->c:I

    add-int/lit8 v0, v0, 0x3d

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->b:I

    rem-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_d2a

    goto/16 :goto_120f

    :cond_d2a
    goto/16 :goto_120c

    :sswitch_d2c
    const/4 v0, 0x0

    goto/16 :goto_9eb

    :goto_d2f
    sget v1, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->b:I

    or-int/lit8 v0, v1, 0x59

    shl-int/lit8 v0, v0, 0x1

    xor-int/lit8 v1, v1, 0x59

    sub-int/2addr v0, v1

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->c:I

    rem-int/lit8 v0, v0, 0x2

    if-nez v0, :cond_d42

    goto/16 :goto_1218

    :cond_d42
    goto/16 :goto_1216

    :pswitch_d44
    const/4 v0, 0x1

    goto/16 :goto_9eb

    :goto_d47
    sget v1, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->c:I

    and-int/lit8 v0, v1, 0x43

    or-int/lit8 v1, v1, 0x43

    add-int/2addr v0, v1

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->b:I

    rem-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_d58

    goto/16 :goto_1220

    :cond_d58
    goto/16 :goto_121e

    :pswitch_d5a
    const/16 v0, 0x2e

    goto/16 :goto_9f3

    :goto_d5e
    sget v0, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->c:I

    add-int/lit8 v0, v0, 0x2d

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->b:I

    rem-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_d6c

    goto/16 :goto_1228

    :cond_d6c
    goto/16 :goto_1226

    :pswitch_d6e
    const/16 v0, 0x31

    goto/16 :goto_9f3

    :goto_d72
    sget v1, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->c:I

    xor-int/lit8 v0, v1, 0x5b

    and-int/lit8 v1, v1, 0x5b

    shl-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->b:I

    rem-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_d85

    goto/16 :goto_1231

    :cond_d85
    goto/16 :goto_122e

    :sswitch_d87
    const/4 v0, 0x3

    goto/16 :goto_9fd

    :goto_d8a
    sget v1, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->c:I

    and-int/lit8 v0, v1, 0x79

    or-int/lit8 v1, v1, 0x79

    add-int/2addr v0, v1

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->b:I

    rem-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_d9b

    goto/16 :goto_123b

    :cond_d9b
    goto/16 :goto_1238

    :sswitch_d9d
    const/16 v0, 0x3a

    goto/16 :goto_9fd

    :goto_da1
    sget v0, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->b:I

    add-int/lit8 v0, v0, 0x4b

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->c:I

    rem-int/lit8 v0, v0, 0x2

    if-nez v0, :cond_daf

    goto/16 :goto_1245

    :cond_daf
    goto/16 :goto_1242

    :sswitch_db1
    const/16 v0, 0x24

    goto/16 :goto_a06

    :goto_db5
    sget v1, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->b:I

    xor-int/lit8 v0, v1, 0x5d

    and-int/lit8 v1, v1, 0x5d

    shl-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->c:I

    rem-int/lit8 v0, v0, 0x2

    if-nez v0, :cond_dc8

    goto/16 :goto_124f

    :cond_dc8
    goto/16 :goto_124c

    :sswitch_dca
    const/16 v0, 0x34

    goto/16 :goto_a06

    :goto_dce
    sget v0, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->c:I

    add-int/lit8 v0, v0, 0x3b

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->b:I

    rem-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_ddc

    goto/16 :goto_1258

    :cond_ddc
    goto/16 :goto_1256

    :pswitch_dde
    const/4 v0, 0x1

    goto/16 :goto_a10

    :goto_de1
    sget v1, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->b:I

    xor-int/lit8 v0, v1, 0x1

    and-int/lit8 v1, v1, 0x1

    shl-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->c:I

    rem-int/lit8 v0, v0, 0x2

    if-nez v0, :cond_df4

    goto/16 :goto_1260

    :cond_df4
    goto/16 :goto_125e

    :pswitch_df6
    const/4 v0, 0x0

    goto/16 :goto_a10

    :goto_df9
    sget v0, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->b:I

    add-int/lit8 v0, v0, 0x2

    add-int/lit8 v0, v0, -0x1

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->c:I

    rem-int/lit8 v0, v0, 0x2

    if-nez v0, :cond_e09

    goto/16 :goto_1269

    :cond_e09
    goto/16 :goto_1266

    :sswitch_e0b
    sget v1, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->c:I

    or-int/lit8 v0, v1, 0x25

    shl-int/lit8 v0, v0, 0x1

    xor-int/lit8 v1, v1, 0x25

    sub-int/2addr v0, v1

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->b:I

    rem-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_e1e

    goto/16 :goto_9a1

    :cond_e1e
    goto/16 :goto_9a3

    :goto_e20
    sget v0, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->b:I

    add-int/lit8 v0, v0, 0x3d

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->c:I

    rem-int/lit8 v0, v0, 0x2

    if-nez v0, :cond_e2e

    goto/16 :goto_1273

    :cond_e2e
    goto/16 :goto_1270

    :sswitch_e30
    const/16 v0, 0x60

    goto/16 :goto_a18

    :goto_e34
    sget v0, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->c:I

    add-int/lit8 v0, v0, 0x15

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->b:I

    rem-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_e42

    goto/16 :goto_127c

    :cond_e42
    goto/16 :goto_127a

    :pswitch_e44
    const/4 v0, 0x1

    goto/16 :goto_a23

    :goto_e47
    sget v1, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->b:I

    xor-int/lit8 v0, v1, 0x23

    and-int/lit8 v1, v1, 0x23

    shl-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->c:I

    rem-int/lit8 v0, v0, 0x2

    if-nez v0, :cond_e5a

    goto/16 :goto_1285

    :cond_e5a
    goto/16 :goto_1282

    :sswitch_e5c
    const/4 v0, 0x1

    goto/16 :goto_a2c

    :goto_e5f
    sget v1, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->b:I

    xor-int/lit8 v0, v1, 0x1b

    and-int/lit8 v1, v1, 0x1b

    shl-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->c:I

    rem-int/lit8 v0, v0, 0x2

    if-nez v0, :cond_e72

    goto/16 :goto_128f

    :cond_e72
    goto/16 :goto_128c

    :sswitch_e74
    const/4 v0, 0x0

    goto/16 :goto_a2c

    :goto_e77
    sget v2, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->c:I

    or-int/lit8 v1, v2, 0x59

    shl-int/lit8 v1, v1, 0x1

    xor-int/lit8 v2, v2, 0x59

    sub-int/2addr v1, v2

    rem-int/lit16 v2, v1, 0x80

    sput v2, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->b:I

    rem-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_e8a

    goto/16 :goto_1298

    :cond_e8a
    goto/16 :goto_1296

    :pswitch_e8c
    :try_start_e8c
    sget-object v1, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a:[B

    const/16 v2, 0x20

    aget-byte v1, v1, v2

    int-to-byte v1, v1

    int-to-byte v2, v1

    sget-object v3, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a:[B

    const/16 v4, 0x36

    aget-byte v3, v3, v4

    int-to-byte v3, v3

    invoke-static {v1, v2, v3}, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a(ISB)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    sget-object v2, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a:[B

    const/16 v3, 0x23

    aget-byte v2, v2, v3

    neg-int v2, v2

    int-to-byte v2, v2

    sget-object v3, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a:[B

    const/16 v4, 0x1f

    aget-byte v3, v3, v4

    int-to-byte v3, v3

    const/16 v4, 0x45

    invoke-static {v2, v4, v3}, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a(ISB)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I
    :try_end_ec7
    .catchall {:try_start_e8c .. :try_end_ec7} :catchall_22c

    move-result v1

    :try_start_ec8
    invoke-static {v1}, Ljava/lang/Integer;->reverseBytes(I)I
    :try_end_ecb
    .catchall {:try_start_ec8 .. :try_end_ecb} :catchall_570

    move-result v1

    int-to-long v1, v1

    goto/16 :goto_b5f

    :goto_ecf
    sget v2, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->b:I

    or-int/lit8 v1, v2, 0x2b

    shl-int/lit8 v1, v1, 0x1

    xor-int/lit8 v2, v2, 0x2b

    sub-int/2addr v1, v2

    rem-int/lit16 v2, v1, 0x80

    sput v2, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->c:I

    rem-int/lit8 v1, v1, 0x2

    if-nez v1, :cond_ee2

    goto/16 :goto_12a1

    :cond_ee2
    goto/16 :goto_129e

    :sswitch_ee4
    const/4 v1, 0x1

    goto/16 :goto_a3e

    :goto_ee7
    sget v4, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->c:I

    or-int/lit8 v3, v4, 0x7

    shl-int/lit8 v3, v3, 0x1

    xor-int/lit8 v4, v4, 0x7

    sub-int/2addr v3, v4

    rem-int/lit16 v4, v3, 0x80

    sput v4, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->b:I

    rem-int/lit8 v3, v3, 0x2

    if-eqz v3, :cond_efa

    goto/16 :goto_12ab

    :cond_efa
    goto/16 :goto_12a8

    :sswitch_efc
    const/4 v3, 0x1

    :try_start_efd
    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v3, v2

    sget-object v1, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a:[B

    const/16 v2, 0x20

    aget-byte v1, v1, v2

    int-to-byte v1, v1

    int-to-byte v2, v1

    sget-object v4, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a:[B

    const/16 v5, 0x36

    aget-byte v4, v4, v5

    int-to-byte v4, v4

    invoke-static {v1, v2, v4}, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a(ISB)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    sget-object v2, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a:[B

    const/16 v4, 0xc

    aget-byte v2, v2, v4

    neg-int v2, v2

    int-to-byte v2, v2

    sget-object v4, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a:[B

    const/16 v5, 0x21

    aget-byte v4, v4, v5

    add-int/lit8 v4, v4, 0x1

    int-to-byte v4, v4

    sget-object v5, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a:[B

    const/16 v6, 0x20

    aget-byte v5, v5, v6

    int-to-byte v5, v5

    invoke-static {v2, v4, v5}, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a(ISB)Ljava/lang/String;

    move-result-object v2

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Class;

    sget-object v5, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    const/4 v6, 0x0

    aput-object v5, v4, v6

    invoke-virtual {v1, v2, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    invoke-virtual {v1, v0, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_f48
    .catchall {:try_start_efd .. :try_end_f48} :catchall_275

    const/4 v10, -0x1

    goto/16 :goto_b89

    :goto_f4b
    sget v3, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->b:I

    add-int/lit8 v3, v3, 0x51

    rem-int/lit16 v4, v3, 0x80

    sput v4, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->c:I

    rem-int/lit8 v3, v3, 0x2

    if-nez v3, :cond_f59

    goto/16 :goto_12b5

    :cond_f59
    goto/16 :goto_12b2

    :sswitch_f5b
    const/16 v3, 0x38

    goto/16 :goto_a47

    :goto_f5f
    sget v1, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->c:I

    xor-int/lit8 v0, v1, 0x35

    and-int/lit8 v1, v1, 0x35

    shl-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->b:I

    rem-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_f72

    goto/16 :goto_12be

    :cond_f72
    goto/16 :goto_12bc

    :sswitch_f74
    move v0, v9

    xor-int/lit8 v1, v9, 0x11

    and-int/lit8 v2, v9, 0x11

    shl-int/lit8 v2, v2, 0x1

    add-int/2addr v2, v1

    and-int/lit8 v1, v2, -0x12

    or-int/lit8 v2, v2, -0x12

    add-int/2addr v1, v2

    move v9, v1

    if-lez v0, :cond_f86

    goto/16 :goto_2dc

    :cond_f86
    goto/16 :goto_2de

    :goto_f88
    sget v1, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->b:I

    or-int/lit8 v0, v1, 0x67

    shl-int/lit8 v0, v0, 0x1

    xor-int/lit8 v1, v1, 0x67

    sub-int/2addr v0, v1

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->c:I

    rem-int/lit8 v0, v0, 0x2

    if-nez v0, :cond_f9b

    goto/16 :goto_12c7

    :cond_f9b
    goto/16 :goto_12c5

    :sswitch_f9d
    const/4 v0, 0x1

    goto/16 :goto_a51

    :goto_fa0
    sget v1, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->b:I

    xor-int/lit8 v0, v1, 0x2d

    and-int/lit8 v1, v1, 0x2d

    shl-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->c:I

    rem-int/lit8 v0, v0, 0x2

    if-nez v0, :cond_fb3

    goto/16 :goto_12d1

    :cond_fb3
    goto/16 :goto_12ce

    :sswitch_fb5
    const/4 v0, 0x2

    goto/16 :goto_a51

    :goto_fb8
    sget v3, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->b:I

    and-int/lit8 v2, v3, 0x13

    or-int/lit8 v3, v3, 0x13

    add-int/2addr v2, v3

    rem-int/lit16 v3, v2, 0x80

    sput v3, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->c:I

    rem-int/lit8 v2, v2, 0x2

    if-nez v2, :cond_fc9

    goto/16 :goto_12da

    :cond_fc9
    goto/16 :goto_12d8

    :pswitch_fcb
    const/4 v2, 0x1

    :try_start_fcc
    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v1, v2, v3

    sget-object v1, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a:[B

    const/16 v3, 0x20

    aget-byte v1, v1, v3

    int-to-byte v1, v1

    int-to-byte v3, v1

    sget-object v4, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a:[B

    const/16 v5, 0x36

    aget-byte v4, v4, v5

    int-to-byte v4, v4

    invoke-static {v1, v3, v4}, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a(ISB)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    sget-object v3, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a:[B

    const/4 v4, 0x1

    aget-byte v3, v3, v4

    neg-int v3, v3

    int-to-byte v3, v3

    sget-object v4, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a:[B

    const/16 v5, 0x1f

    aget-byte v4, v4, v5

    int-to-byte v4, v4

    const/16 v5, 0x35

    invoke-static {v3, v5, v4}, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a(ISB)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Class;

    const-class v5, [B

    const/4 v6, 0x0

    aput-object v5, v4, v6

    invoke-virtual {v1, v3, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    invoke-virtual {v1, v0, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_100b
    .catchall {:try_start_fcc .. :try_end_100b} :catchall_2e4

    :try_start_100b
    invoke-virtual {v7}, Ljava/util/zip/CRC32;->reset()V

    invoke-virtual {v7, v8}, Ljava/util/zip/CRC32;->update([B)V

    invoke-virtual {v7}, Ljava/util/zip/CRC32;->getValue()J
    :try_end_1014
    .catchall {:try_start_100b .. :try_end_1014} :catchall_570

    move-result-wide v0

    const-wide/32 v2, 0x52c68507

    cmp-long v0, v0, v2

    if-eqz v0, :cond_101e

    goto/16 :goto_33d

    :cond_101e
    goto/16 :goto_33f

    :goto_1020
    sget v3, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->c:I

    and-int/lit8 v2, v3, 0x79

    or-int/lit8 v3, v3, 0x79

    add-int/2addr v2, v3

    rem-int/lit16 v3, v2, 0x80

    sput v3, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->b:I

    rem-int/lit8 v2, v2, 0x2

    if-eqz v2, :cond_1031

    goto/16 :goto_12e3

    :cond_1031
    goto/16 :goto_12e0

    :sswitch_1033
    const/16 v2, 0x25

    goto/16 :goto_a59

    :goto_1037
    sget v2, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->b:I

    add-int/lit8 v2, v2, 0x10

    add-int/lit8 v2, v2, -0x1

    rem-int/lit16 v3, v2, 0x80

    sput v3, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->c:I

    rem-int/lit8 v2, v2, 0x2

    if-nez v2, :cond_1047

    goto/16 :goto_12ec

    :cond_1047
    goto/16 :goto_12ea

    :pswitch_1049
    const/16 v2, 0x4b

    goto/16 :goto_a59

    :goto_104d
    sget v1, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->b:I

    and-int/lit8 v0, v1, 0x5f

    or-int/lit8 v1, v1, 0x5f

    add-int/2addr v0, v1

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->c:I

    rem-int/lit8 v0, v0, 0x2

    if-nez v0, :cond_105e

    goto/16 :goto_12f4

    :cond_105e
    goto/16 :goto_12f2

    :pswitch_1060
    const/4 v0, 0x1

    goto/16 :goto_a63

    :goto_1063
    sget v1, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->c:I

    or-int/lit8 v0, v1, 0x15

    shl-int/lit8 v0, v0, 0x1

    xor-int/lit8 v1, v1, 0x15

    sub-int/2addr v0, v1

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->b:I

    rem-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1076

    goto/16 :goto_12fc

    :cond_1076
    goto/16 :goto_12fa

    :sswitch_1078
    const/16 v0, 0x56

    goto/16 :goto_a6c

    :goto_107c
    sget v1, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->c:I

    or-int/lit8 v0, v1, 0x73

    shl-int/lit8 v0, v0, 0x1

    xor-int/lit8 v1, v1, 0x73

    sub-int/2addr v0, v1

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->b:I

    rem-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_108f

    goto/16 :goto_1305

    :cond_108f
    goto/16 :goto_1303

    :pswitch_1091
    const/16 v0, 0x1d

    goto/16 :goto_a6c

    :goto_1095
    sget v2, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->c:I

    xor-int/lit8 v1, v2, 0x1f

    and-int/lit8 v2, v2, 0x1f

    shl-int/lit8 v2, v2, 0x1

    add-int/2addr v1, v2

    rem-int/lit16 v2, v1, 0x80

    sput v2, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->b:I

    rem-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_10a8

    goto/16 :goto_130d

    :cond_10a8
    goto/16 :goto_130b

    :pswitch_10aa
    const/4 v1, 0x0

    goto/16 :goto_a76

    :goto_10ad
    sget v3, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->b:I

    and-int/lit8 v2, v3, 0xf

    or-int/lit8 v3, v3, 0xf

    add-int/2addr v2, v3

    rem-int/lit16 v3, v2, 0x80

    sput v3, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->c:I

    rem-int/lit8 v2, v2, 0x2

    if-nez v2, :cond_10be

    goto/16 :goto_1316

    :cond_10be
    goto/16 :goto_1313

    :sswitch_10c0
    const/4 v2, 0x1

    :try_start_10c1
    new-array v2, v2, [Ljava/lang/Object;

    const/16 v1, 0xc

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v3, 0x0

    aput-object v1, v2, v3

    sget-object v1, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a:[B

    const/16 v3, 0x20

    aget-byte v1, v1, v3

    int-to-byte v1, v1

    int-to-byte v3, v1

    sget-object v4, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a:[B

    const/16 v5, 0x36

    aget-byte v4, v4, v5

    int-to-byte v4, v4

    invoke-static {v1, v3, v4}, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a(ISB)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    sget-object v3, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a:[B

    const/4 v4, 0x1

    aget-byte v3, v3, v4

    neg-int v3, v3

    int-to-byte v3, v3

    sget-object v4, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a:[B

    const/16 v5, 0x20

    aget-byte v4, v4, v5

    int-to-byte v4, v4

    const/16 v5, 0x4b

    invoke-static {v3, v5, v4}, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a(ISB)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Class;

    sget-object v5, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    const/4 v6, 0x0

    aput-object v5, v4, v6

    invoke-virtual {v1, v3, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    invoke-virtual {v1, v0, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
    :try_end_110c
    .catchall {:try_start_10c1 .. :try_end_110c} :catchall_3f7

    move-object v0, p0

    goto/16 :goto_451

    :goto_110f
    sget v1, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->c:I

    add-int/lit8 v1, v1, 0x41

    rem-int/lit16 v2, v1, 0x80

    sput v2, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->b:I

    rem-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_111d

    goto/16 :goto_131f

    :cond_111d
    goto/16 :goto_131d

    :pswitch_111f
    :try_start_111f
    sget-object v1, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a:[B

    const/16 v2, 0x20

    aget-byte v1, v1, v2

    int-to-byte v1, v1

    int-to-byte v2, v1

    sget-object v3, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a:[B

    const/16 v4, 0x36

    aget-byte v3, v3, v4

    int-to-byte v3, v3

    invoke-static {v1, v2, v3}, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a(ISB)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    sget-object v2, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a:[B

    const/16 v3, 0x23

    aget-byte v2, v2, v3

    neg-int v2, v2

    int-to-byte v2, v2

    sget-object v3, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a:[B

    const/16 v4, 0x1f

    aget-byte v3, v3, v4

    int-to-byte v3, v3

    const/16 v4, 0x45

    invoke-static {v2, v4, v3}, Lcom/guardsquare/dexguard/runtime/detection/TamperDetector;->a(ISB)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
    :try_end_115a
    .catchall {:try_start_111f .. :try_end_115a} :catchall_44b

    move-result v0

    :try_start_115b
    invoke-static {v0}, Ljava/lang/Integer;->reverseBytes(I)I
    :try_end_115e
    .catchall {:try_start_115b .. :try_end_115e} :catchall_570

    move-result v12

    move-object v0, p0

    move-object v1, v11

    goto/16 :goto_c30

    :goto_1163
    packed-switch v0, :pswitch_data_1424

    nop

    :goto_1167
    const/4 v0, 0x0

    goto :goto_1163

    :goto_1169
    const/4 v0, 0x1

    goto :goto_1163

    :goto_116b
    packed-switch v0, :pswitch_data_142c

    nop

    :goto_116f
    const/4 v0, 0x0

    goto :goto_116b

    :goto_1171
    const/4 v0, 0x1

    goto :goto_116b

    :goto_1173
    packed-switch v0, :pswitch_data_1434

    goto :goto_1179

    :goto_1177
    const/4 v0, 0x0

    goto :goto_1173

    :goto_1179
    const/4 v0, 0x1

    goto :goto_1173

    :goto_117b
    packed-switch v2, :pswitch_data_143c

    goto :goto_1181

    :goto_117f
    const/4 v2, 0x1

    goto :goto_117b

    :goto_1181
    const/4 v2, 0x0

    goto :goto_117b

    :goto_1183
    packed-switch v1, :pswitch_data_1444

    goto :goto_1189

    :goto_1187
    const/4 v1, 0x1

    goto :goto_1183

    :goto_1189
    const/4 v1, 0x0

    goto :goto_1183

    :goto_118b
    packed-switch v3, :pswitch_data_144c

    nop

    :goto_118f
    const/4 v3, 0x0

    goto :goto_118b

    :goto_1191
    const/4 v3, 0x1

    goto :goto_118b

    :goto_1193
    sparse-switch v0, :sswitch_data_1454

    nop

    :goto_1197
    const/16 v0, 0x4c

    goto :goto_1193

    :goto_119a
    const/16 v0, 0x19

    goto :goto_1193

    :goto_119d
    sparse-switch v0, :sswitch_data_145e

    goto :goto_11a4

    :goto_11a1
    const/16 v0, 0x9

    goto :goto_119d

    :goto_11a4
    const/16 v0, 0x2c

    goto :goto_119d

    :goto_11a7
    packed-switch v0, :pswitch_data_1468

    nop

    :goto_11ab
    const/4 v0, 0x0

    goto :goto_11a7

    :goto_11ad
    const/4 v0, 0x1

    goto :goto_11a7

    :goto_11af
    packed-switch v0, :pswitch_data_1470

    goto :goto_11b5

    :goto_11b3
    const/4 v0, 0x0

    goto :goto_11af

    :goto_11b5
    const/4 v0, 0x1

    goto :goto_11af

    :goto_11b7
    sparse-switch v0, :sswitch_data_1478

    goto :goto_11be

    :goto_11bb
    const/16 v0, 0x63

    goto :goto_11b7

    :goto_11be
    const/16 v0, 0x32

    goto :goto_11b7

    :goto_11c1
    sparse-switch v1, :sswitch_data_1482

    nop

    :goto_11c5
    const/16 v1, 0x38

    goto :goto_11c1

    :goto_11c8
    const/16 v1, 0x2a

    goto :goto_11c1

    :goto_11cb
    sparse-switch v2, :sswitch_data_148c

    goto :goto_11d2

    :goto_11cf
    const/16 v2, 0x2c

    goto :goto_11cb

    :goto_11d2
    const/16 v2, 0x60

    goto :goto_11cb

    :goto_11d5
    packed-switch v0, :pswitch_data_1496

    goto :goto_11db

    :goto_11d9
    const/4 v0, 0x1

    goto :goto_11d5

    :goto_11db
    const/4 v0, 0x0

    goto :goto_11d5

    :goto_11dd
    packed-switch v0, :pswitch_data_149e

    goto :goto_11e3

    :goto_11e1
    const/4 v0, 0x0

    goto :goto_11dd

    :goto_11e3
    const/4 v0, 0x1

    goto :goto_11dd

    :goto_11e5
    packed-switch v0, :pswitch_data_14a6

    nop

    :goto_11e9
    const/4 v0, 0x1

    goto :goto_11e5

    :goto_11eb
    const/4 v0, 0x0

    goto :goto_11e5

    :goto_11ed
    sparse-switch v0, :sswitch_data_14ae

    goto :goto_11f4

    :goto_11f1
    const/16 v0, 0x43

    goto :goto_11ed

    :goto_11f4
    const/16 v0, 0x30

    goto :goto_11ed

    :goto_11f7
    packed-switch v0, :pswitch_data_14b8

    nop

    :goto_11fb
    const/4 v0, 0x1

    goto :goto_11f7

    :goto_11fd
    const/4 v0, 0x0

    goto :goto_11f7

    :goto_11ff
    sparse-switch v0, :sswitch_data_14c0

    nop

    :goto_1203
    const/4 v0, 0x3

    goto :goto_11ff

    :goto_1205
    const/16 v0, 0x12

    goto :goto_11ff

    :goto_1208
    sparse-switch v0, :sswitch_data_14ca

    goto :goto_120f

    :goto_120c
    const/16 v0, 0x28

    goto :goto_1208

    :goto_120f
    const/16 v0, 0x5a

    goto :goto_1208

    :goto_1212
    packed-switch v0, :pswitch_data_14d4

    nop

    :goto_1216
    const/4 v0, 0x1

    goto :goto_1212

    :goto_1218
    const/4 v0, 0x0

    goto :goto_1212

    :goto_121a
    packed-switch v0, :pswitch_data_14dc

    nop

    :goto_121e
    const/4 v0, 0x0

    goto :goto_121a

    :goto_1220
    const/4 v0, 0x1

    goto :goto_121a

    :goto_1222
    packed-switch v0, :pswitch_data_14e4

    nop

    :goto_1226
    const/4 v0, 0x0

    goto :goto_1222

    :goto_1228
    const/4 v0, 0x1

    goto :goto_1222

    :goto_122a
    sparse-switch v0, :sswitch_data_14ec

    goto :goto_1231

    :goto_122e
    const/16 v0, 0x53

    goto :goto_122a

    :goto_1231
    const/16 v0, 0x16

    goto :goto_122a

    :goto_1234
    sparse-switch v0, :sswitch_data_14f6

    goto :goto_123b

    :goto_1238
    const/16 v0, 0x61

    goto :goto_1234

    :goto_123b
    const/16 v0, 0x1f

    goto :goto_1234

    :goto_123e
    sparse-switch v0, :sswitch_data_1500

    goto :goto_1245

    :goto_1242
    const/16 v0, 0x26

    goto :goto_123e

    :goto_1245
    const/16 v0, 0x5a

    goto :goto_123e

    :goto_1248
    sparse-switch v0, :sswitch_data_150a

    goto :goto_124f

    :goto_124c
    const/16 v0, 0x3b

    goto :goto_1248

    :goto_124f
    const/16 v0, 0x5f

    goto :goto_1248

    :goto_1252
    packed-switch v0, :pswitch_data_1514

    goto :goto_1258

    :goto_1256
    const/4 v0, 0x0

    goto :goto_1252

    :goto_1258
    const/4 v0, 0x1

    goto :goto_1252

    :goto_125a
    packed-switch v0, :pswitch_data_151c

    goto :goto_1260

    :goto_125e
    const/4 v0, 0x0

    goto :goto_125a

    :goto_1260
    const/4 v0, 0x1

    goto :goto_125a

    :goto_1262
    sparse-switch v0, :sswitch_data_1524

    goto :goto_1269

    :goto_1266
    const/16 v0, 0x34

    goto :goto_1262

    :goto_1269
    const/16 v0, 0x4c

    goto :goto_1262

    :goto_126c
    sparse-switch v0, :sswitch_data_152e

    nop

    :goto_1270
    const/16 v0, 0x18

    goto :goto_126c

    :goto_1273
    const/16 v0, 0xd

    goto :goto_126c

    :goto_1276
    packed-switch v0, :pswitch_data_1538

    goto :goto_127c

    :goto_127a
    const/4 v0, 0x0

    goto :goto_1276

    :goto_127c
    const/4 v0, 0x1

    goto :goto_1276

    :goto_127e
    sparse-switch v0, :sswitch_data_1540

    nop

    :goto_1282
    const/16 v0, 0x21

    goto :goto_127e

    :goto_1285
    const/16 v0, 0x2e

    goto :goto_127e

    :goto_1288
    sparse-switch v0, :sswitch_data_154a

    goto :goto_128f

    :goto_128c
    const/16 v0, 0x3e

    goto :goto_1288

    :goto_128f
    const/16 v0, 0x28

    goto :goto_1288

    :goto_1292
    packed-switch v1, :pswitch_data_1554

    goto :goto_1298

    :goto_1296
    const/4 v1, 0x1

    goto :goto_1292

    :goto_1298
    const/4 v1, 0x0

    goto :goto_1292

    :goto_129a
    sparse-switch v1, :sswitch_data_155c

    nop

    :goto_129e
    const/16 v1, 0x14

    goto :goto_129a

    :goto_12a1
    const/16 v1, 0x1f

    goto :goto_129a

    :goto_12a4
    sparse-switch v3, :sswitch_data_1566

    goto :goto_12ab

    :goto_12a8
    const/16 v3, 0x5a

    goto :goto_12a4

    :goto_12ab
    const/16 v3, 0x31

    goto :goto_12a4

    :goto_12ae
    sparse-switch v3, :sswitch_data_1570

    nop

    :goto_12b2
    const/16 v3, 0xe

    goto :goto_12ae

    :goto_12b5
    const/16 v3, 0x1e

    goto :goto_12ae

    :goto_12b8
    sparse-switch v0, :sswitch_data_157a

    nop

    :goto_12bc
    const/4 v0, 0x0

    goto :goto_12b8

    :goto_12be
    const/16 v0, 0x16

    goto :goto_12b8

    :goto_12c1
    sparse-switch v0, :sswitch_data_1584

    goto :goto_12c7

    :goto_12c5
    const/4 v0, 0x5

    goto :goto_12c1

    :goto_12c7
    const/16 v0, 0x3d

    goto :goto_12c1

    :goto_12ca
    sparse-switch v0, :sswitch_data_158e

    nop

    :goto_12ce
    const/16 v0, 0x43

    goto :goto_12ca

    :goto_12d1
    const/16 v0, 0x16

    goto :goto_12ca

    :goto_12d4
    packed-switch v2, :pswitch_data_1598

    goto :goto_12da

    :goto_12d8
    const/4 v2, 0x0

    goto :goto_12d4

    :goto_12da
    const/4 v2, 0x1

    goto :goto_12d4

    :goto_12dc
    sparse-switch v2, :sswitch_data_15a0

    nop

    :goto_12e0
    const/16 v2, 0x43

    goto :goto_12dc

    :goto_12e3
    const/16 v2, 0x27

    goto :goto_12dc

    :goto_12e6
    packed-switch v2, :pswitch_data_15aa

    goto :goto_12ec

    :goto_12ea
    const/4 v2, 0x1

    goto :goto_12e6

    :goto_12ec
    const/4 v2, 0x0

    goto :goto_12e6

    :goto_12ee
    packed-switch v0, :pswitch_data_15b2

    nop

    :goto_12f2
    const/4 v0, 0x0

    goto :goto_12ee

    :goto_12f4
    const/4 v0, 0x1

    goto :goto_12ee

    :goto_12f6
    sparse-switch v0, :sswitch_data_15ba

    nop

    :goto_12fa
    const/4 v0, 0x1

    goto :goto_12f6

    :goto_12fc
    const/16 v0, 0x58

    goto :goto_12f6

    :goto_12ff
    packed-switch v0, :pswitch_data_15c4

    nop

    :goto_1303
    const/4 v0, 0x0

    goto :goto_12ff

    :goto_1305
    const/4 v0, 0x1

    goto :goto_12ff

    :goto_1307
    packed-switch v1, :pswitch_data_15cc

    goto :goto_130d

    :goto_130b
    const/4 v1, 0x1

    goto :goto_1307

    :goto_130d
    const/4 v1, 0x0

    goto :goto_1307

    :goto_130f
    sparse-switch v2, :sswitch_data_15d4

    goto :goto_1316

    :goto_1313
    const/16 v2, 0x26

    goto :goto_130f

    :goto_1316
    const/16 v2, 0x31

    goto :goto_130f

    :goto_1319
    packed-switch v1, :pswitch_data_15de

    nop

    :goto_131d
    const/4 v1, 0x1

    goto :goto_1319

    :goto_131f
    const/4 v1, 0x0

    goto :goto_1319

    nop

    :pswitch_data_1322
    .packed-switch 0x1
        :pswitch_183
    .end packed-switch

    :pswitch_data_1328
    .packed-switch 0x0
        :pswitch_56c
        :pswitch_764
    .end packed-switch

    :pswitch_data_1330
    .packed-switch 0x0
        :pswitch_34d
        :pswitch_341
    .end packed-switch

    :pswitch_data_1338
    .packed-switch 0x0
        :pswitch_507
        :pswitch_4ef
    .end packed-switch

    :sswitch_data_1340
    .sparse-switch
        0x18 -> :sswitch_504
        0x33 -> :sswitch_4fe
    .end sparse-switch

    :sswitch_data_134a
    .sparse-switch
        0x20 -> :sswitch_516
        0x57 -> :sswitch_534
    .end sparse-switch

    :pswitch_data_1354
    .packed-switch 0x0
        :pswitch_918
        :pswitch_534
    .end packed-switch

    :sswitch_data_135c
    .sparse-switch
        0x2e -> :sswitch_534
        0x31 -> :sswitch_548
    .end sparse-switch

    :sswitch_data_1366
    .sparse-switch
        0x3 -> :sswitch_93e
        0x3a -> :sswitch_56a
    .end sparse-switch

    :sswitch_data_1370
    .sparse-switch
        0x24 -> :sswitch_95b
        0x34 -> :sswitch_556
    .end sparse-switch

    :pswitch_data_137a
    .packed-switch 0x0
        :pswitch_56a
        :pswitch_976
    .end packed-switch

    :sswitch_data_1382
    .sparse-switch
        0x24 -> :sswitch_121
    .end sparse-switch

    :pswitch_data_1388
    .packed-switch 0x0
        :pswitch_183
    .end packed-switch

    :pswitch_data_138e
    .packed-switch 0x0
        :pswitch_18b
    .end packed-switch

    :pswitch_data_1394
    .packed-switch 0x0
        :pswitch_1de
    .end packed-switch

    :pswitch_data_139a
    .packed-switch 0x0
        :pswitch_232
    .end packed-switch

    :sswitch_data_13a0
    .sparse-switch
        0x38 -> :sswitch_715
        0x52 -> :sswitch_27b
    .end sparse-switch

    :sswitch_data_13aa
    .sparse-switch
        0x1 -> :sswitch_778
        0x2 -> :sswitch_2e0
    .end sparse-switch

    :sswitch_data_13b4
    .sparse-switch
        0x25 -> :sswitch_78e
        0x4b -> :sswitch_2ea
    .end sparse-switch

    :pswitch_data_13be
    .packed-switch 0x1
        :pswitch_34d
    .end packed-switch

    :sswitch_data_13c4
    .sparse-switch
        0x1d -> :sswitch_355
        0x56 -> :sswitch_7fc
    .end sparse-switch

    :pswitch_data_13ce
    .packed-switch 0x0
        :pswitch_3aa
        :pswitch_862
    .end packed-switch

    :sswitch_data_13d6
    .sparse-switch
        0x2 -> :sswitch_3fd
        0x17 -> :sswitch_8c9
    .end sparse-switch

    :sswitch_data_13e0
    .sparse-switch
        0x35 -> :sswitch_92f
        0x40 -> :sswitch_525
    .end sparse-switch

    :sswitch_data_13ea
    .sparse-switch
        0x42 -> :sswitch_548
        0x47 -> :sswitch_534
    .end sparse-switch

    :sswitch_data_13f4
    .sparse-switch
        0x3c -> :sswitch_955
        0x51 -> :sswitch_53c
    .end sparse-switch

    :sswitch_data_13fe
    .sparse-switch
        0x3d -> :sswitch_970
        0x56 -> :sswitch_550
    .end sparse-switch

    :sswitch_data_1408
    .sparse-switch
        0xd -> :sswitch_55e
        0x12 -> :sswitch_98a
    .end sparse-switch

    :sswitch_data_1412
    .sparse-switch
        0x10 -> :sswitch_9a5
        0x60 -> :sswitch_2cf
    .end sparse-switch

    :pswitch_data_141c
    .packed-switch 0x0
        :pswitch_56c
        :pswitch_764
    .end packed-switch

    :pswitch_data_1424
    .packed-switch 0x0
        :pswitch_9bd
        :pswitch_adb
    .end packed-switch

    :pswitch_data_142c
    .packed-switch 0x0
        :pswitch_9bb
        :pswitch_af0
    .end packed-switch

    :pswitch_data_1434
    .packed-switch 0x0
        :pswitch_5e7
        :pswitch_b05
    .end packed-switch

    :pswitch_data_143c
    .packed-switch 0x0
        :pswitch_b28
        :pswitch_647
    .end packed-switch

    :pswitch_data_1444
    .packed-switch 0x0
        :pswitch_b4d
        :pswitch_6a7
    .end packed-switch

    :pswitch_data_144c
    .packed-switch 0x0
        :pswitch_701
        :pswitch_b74
    .end packed-switch

    :sswitch_data_1454
    .sparse-switch
        0x19 -> :sswitch_b9b
        0x4c -> :sswitch_2cf
    .end sparse-switch

    :sswitch_data_145e
    .sparse-switch
        0x9 -> :sswitch_9c5
        0x2c -> :sswitch_bbf
    .end sparse-switch

    :pswitch_data_1468
    .packed-switch 0x0
        :pswitch_9c3
        :pswitch_bd5
    .end packed-switch

    :pswitch_data_1470
    .packed-switch 0x0
        :pswitch_9cd
        :pswitch_be8
    .end packed-switch

    :sswitch_data_1478
    .sparse-switch
        0x32 -> :sswitch_bfb
        0x63 -> :sswitch_7e5
    .end sparse-switch

    :sswitch_data_1482
    .sparse-switch
        0x2a -> :sswitch_c20
        0x38 -> :sswitch_84b
    .end sparse-switch

    :sswitch_data_148c
    .sparse-switch
        0x2c -> :sswitch_49b
        0x60 -> :sswitch_c42
    .end sparse-switch

    :pswitch_data_1496
    .packed-switch 0x0
        :pswitch_ca4
        :pswitch_9d5
    .end packed-switch

    :pswitch_data_149e
    .packed-switch 0x0
        :pswitch_9d3
        :pswitch_cb7
    .end packed-switch

    :pswitch_data_14a6
    .packed-switch 0x0
        :pswitch_ccf
        :pswitch_9de
    .end packed-switch

    :sswitch_data_14ae
    .sparse-switch
        0x30 -> :sswitch_ce8
        0x43 -> :sswitch_9db
    .end sparse-switch

    :pswitch_data_14b8
    .packed-switch 0x0
        :pswitch_cff
        :pswitch_9e8
    .end packed-switch

    :sswitch_data_14c0
    .sparse-switch
        0x3 -> :sswitch_9e5
        0x12 -> :sswitch_d18
    .end sparse-switch

    :sswitch_data_14ca
    .sparse-switch
        0x28 -> :sswitch_9f1
        0x5a -> :sswitch_d2c
    .end sparse-switch

    :pswitch_data_14d4
    .packed-switch 0x0
        :pswitch_d44
        :pswitch_9ef
    .end packed-switch

    :pswitch_data_14dc
    .packed-switch 0x0
        :pswitch_9fa
        :pswitch_d5a
    .end packed-switch

    :pswitch_data_14e4
    .packed-switch 0x0
        :pswitch_9f7
        :pswitch_d6e
    .end packed-switch

    :sswitch_data_14ec
    .sparse-switch
        0x16 -> :sswitch_d87
        0x53 -> :sswitch_a04
    .end sparse-switch

    :sswitch_data_14f6
    .sparse-switch
        0x1f -> :sswitch_d9d
        0x61 -> :sswitch_a01
    .end sparse-switch

    :sswitch_data_1500
    .sparse-switch
        0x26 -> :sswitch_a0d
        0x5a -> :sswitch_db1
    .end sparse-switch

    :sswitch_data_150a
    .sparse-switch
        0x3b -> :sswitch_a0a
        0x5f -> :sswitch_dca
    .end sparse-switch

    :pswitch_data_1514
    .packed-switch 0x0
        :pswitch_a16
        :pswitch_dde
    .end packed-switch

    :pswitch_data_151c
    .packed-switch 0x0
        :pswitch_a14
        :pswitch_df6
    .end packed-switch

    :sswitch_data_1524
    .sparse-switch
        0x34 -> :sswitch_990
        0x4c -> :sswitch_e0b
    .end sparse-switch

    :sswitch_data_152e
    .sparse-switch
        0xd -> :sswitch_e30
        0x18 -> :sswitch_a20
    .end sparse-switch

    :pswitch_data_1538
    .packed-switch 0x0
        :pswitch_a2a
        :pswitch_e44
    .end packed-switch

    :sswitch_data_1540
    .sparse-switch
        0x21 -> :sswitch_a33
        0x2e -> :sswitch_e5c
    .end sparse-switch

    :sswitch_data_154a
    .sparse-switch
        0x28 -> :sswitch_e74
        0x3e -> :sswitch_a31
    .end sparse-switch

    :pswitch_data_1554
    .packed-switch 0x0
        :pswitch_e8c
        :pswitch_232
    .end packed-switch

    :sswitch_data_155c
    .sparse-switch
        0x14 -> :sswitch_a45
        0x1f -> :sswitch_ee4
    .end sparse-switch

    :sswitch_data_1566
    .sparse-switch
        0x31 -> :sswitch_efc
        0x5a -> :sswitch_27b
    .end sparse-switch

    :sswitch_data_1570
    .sparse-switch
        0xe -> :sswitch_a4e
        0x1e -> :sswitch_f5b
    .end sparse-switch

    :sswitch_data_157a
    .sparse-switch
        0x0 -> :sswitch_2cf
        0x16 -> :sswitch_f74
    .end sparse-switch

    :sswitch_data_1584
    .sparse-switch
        0x5 -> :sswitch_a57
        0x3d -> :sswitch_f9d
    .end sparse-switch

    :sswitch_data_158e
    .sparse-switch
        0x16 -> :sswitch_fb5
        0x43 -> :sswitch_a55
    .end sparse-switch

    :pswitch_data_1598
    .packed-switch 0x0
        :pswitch_2ea
        :pswitch_fcb
    .end packed-switch

    :sswitch_data_15a0
    .sparse-switch
        0x27 -> :sswitch_1033
        0x43 -> :sswitch_a60
    .end sparse-switch

    :pswitch_data_15aa
    .packed-switch 0x0
        :pswitch_1049
        :pswitch_a5d
    .end packed-switch

    :pswitch_data_15b2
    .packed-switch 0x0
        :pswitch_a68
        :pswitch_1060
    .end packed-switch

    :sswitch_data_15ba
    .sparse-switch
        0x1 -> :sswitch_a73
        0x58 -> :sswitch_1078
    .end sparse-switch

    :pswitch_data_15c4
    .packed-switch 0x0
        :pswitch_a70
        :pswitch_1091
    .end packed-switch

    :pswitch_data_15cc
    .packed-switch 0x0
        :pswitch_10aa
        :pswitch_a7a
    .end packed-switch

    :sswitch_data_15d4
    .sparse-switch
        0x26 -> :sswitch_3fd
        0x31 -> :sswitch_10c0
    .end sparse-switch

    :pswitch_data_15de
    .packed-switch 0x0
        :pswitch_111f
        :pswitch_451
    .end packed-switch
.end method

.class public abstract Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/DataRenderer;
.super Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/Renderer;
.source "DataRenderer.java"


# instance fields
.field protected mAnimator:Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/ChartAnimator;

.field protected mDrawPaint:Landroid/graphics/Paint;

.field protected mHighlightPaint:Landroid/graphics/Paint;

.field protected mRenderPaint:Landroid/graphics/Paint;

.field protected mValuePaint:Landroid/graphics/Paint;


# direct methods
.method public constructor <init>(Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/ChartAnimator;Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;)V
    .registers 7
    .param p1, "animator"    # Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/ChartAnimator;
    .param p2, "viewPortHandler"    # Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;

    .line 41
    invoke-direct {p0, p2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/Renderer;-><init>(Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;)V

    .line 42
    iput-object p1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/DataRenderer;->mAnimator:Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/ChartAnimator;

    .line 45
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/DataRenderer;->mRenderPaint:Landroid/graphics/Paint;

    .line 46
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/DataRenderer;->mRenderPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 49
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/DataRenderer;->mDrawPaint:Landroid/graphics/Paint;

    .line 52
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/DataRenderer;->mValuePaint:Landroid/graphics/Paint;

    .line 53
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/DataRenderer;->mValuePaint:Landroid/graphics/Paint;

    const/16 v1, 0x3f

    const/16 v2, 0x3f

    const/16 v3, 0x3f

    invoke-static {v1, v2, v3}, Landroid/graphics/Color;->rgb(III)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 54
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/DataRenderer;->mValuePaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 55
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/DataRenderer;->mValuePaint:Landroid/graphics/Paint;

    const/high16 v1, 0x41100000    # 9.0f

    invoke-static {v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Utils;->convertDpToPixel(F)F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 58
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/DataRenderer;->mHighlightPaint:Landroid/graphics/Paint;

    .line 59
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/DataRenderer;->mHighlightPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 60
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/DataRenderer;->mHighlightPaint:Landroid/graphics/Paint;

    const/high16 v1, 0x40000000    # 2.0f

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 61
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/DataRenderer;->mHighlightPaint:Landroid/graphics/Paint;

    const/16 v1, 0xff

    const/16 v2, 0xbb

    const/16 v3, 0x73

    invoke-static {v1, v2, v3}, Landroid/graphics/Color;->rgb(III)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 62
    return-void
.end method


# virtual methods
.method protected applyValueTextStyle(Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;)V
    .registers 4
    .param p1, "set"    # Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet<*>;)V"
        }
    .end annotation

    .line 106
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/DataRenderer;->mValuePaint:Landroid/graphics/Paint;

    invoke-virtual {p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->getValueTextColor()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 107
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/DataRenderer;->mValuePaint:Landroid/graphics/Paint;

    invoke-virtual {p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->getValueTypeface()Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 108
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/DataRenderer;->mValuePaint:Landroid/graphics/Paint;

    invoke-virtual {p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->getValueTextSize()F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 109
    return-void
.end method

.method public abstract drawData(Landroid/graphics/Canvas;)V
.end method

.method public abstract drawExtras(Landroid/graphics/Canvas;)V
.end method

.method public abstract drawHighlighted(Landroid/graphics/Canvas;[Lbr/com/itau/widgets/graphicalfriedcake/charts/highlight/Highlight;)V
.end method

.method public abstract drawValues(Landroid/graphics/Canvas;)V
.end method

.method public getPaintHighlight()Landroid/graphics/Paint;
    .registers 2

    .line 83
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/DataRenderer;->mHighlightPaint:Landroid/graphics/Paint;

    return-object v0
.end method

.method public getPaintRender()Landroid/graphics/Paint;
    .registers 2

    .line 93
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/DataRenderer;->mRenderPaint:Landroid/graphics/Paint;

    return-object v0
.end method

.method public getPaintValues()Landroid/graphics/Paint;
    .registers 2

    .line 72
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/DataRenderer;->mValuePaint:Landroid/graphics/Paint;

    return-object v0
.end method

.method public abstract initBuffers()V
.end method

.class public abstract Lnet/hockeyapp/android/CrashManagerListener;
.super Lnet/hockeyapp/android/StringListener;
.source "CrashManagerListener.java"


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 37
    invoke-direct {p0}, Lnet/hockeyapp/android/StringListener;-><init>()V

    return-void
.end method


# virtual methods
.method public getContact()Ljava/lang/String;
    .registers 2

    .line 77
    const/4 v0, 0x0

    return-object v0
.end method

.method public getDescription()Ljava/lang/String;
    .registers 2

    .line 87
    const/4 v0, 0x0

    return-object v0
.end method

.method public getUserID()Ljava/lang/String;
    .registers 2

    .line 98
    const/4 v0, 0x0

    return-object v0
.end method

.method public ignoreDefaultHandler()Z
    .registers 2

    .line 45
    const/4 v0, 0x0

    return v0
.end method

.method public includeDeviceData()Z
    .registers 2

    .line 56
    const/4 v0, 0x1

    return v0
.end method

.method public includeDeviceIdentifier()Z
    .registers 2

    .line 66
    const/4 v0, 0x1

    return v0
.end method

.method public onConfirmedCrashesFound()V
    .registers 1

    .line 135
    return-void
.end method

.method public onCrashesFound()Z
    .registers 2

    .line 110
    const/4 v0, 0x0

    return v0
.end method

.method public onCrashesNotSent()V
    .registers 1

    .line 148
    return-void
.end method

.method public onCrashesSent()V
    .registers 1

    .line 141
    return-void
.end method

.method public onNewCrashesFound()V
    .registers 1

    .line 127
    return-void
.end method

.method public onUserDeniedCrashes()V
    .registers 1

    .line 154
    return-void
.end method

.method public shouldAutoUploadCrashes()Z
    .registers 2

    .line 120
    const/4 v0, 0x0

    return v0
.end method

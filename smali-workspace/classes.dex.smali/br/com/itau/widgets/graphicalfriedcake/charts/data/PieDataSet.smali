.class public Lbr/com/itau/widgets/graphicalfriedcake/charts/data/PieDataSet;
.super Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;
.source "PieDataSet.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet<Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;>;"
    }
.end annotation


# instance fields
.field private mShift:F

.field private mSliceSpace:F


# direct methods
.method public constructor <init>(Ljava/util/List;Ljava/lang/String;)V
    .registers 4
    .param p1, "yVals"    # Ljava/util/List;
    .param p2, "label"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;>;Ljava/lang/String;)V"
        }
    .end annotation

    .line 21
    invoke-direct {p0, p1, p2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;-><init>(Ljava/util/List;Ljava/lang/String;)V

    .line 13
    const/4 v0, 0x0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/PieDataSet;->mSliceSpace:F

    .line 17
    const/high16 v0, 0x41900000    # 18.0f

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/PieDataSet;->mShift:F

    .line 23
    return-void
.end method


# virtual methods
.method public copy()Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;
    .registers 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet<Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;>;"
        }
    .end annotation

    .line 30
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 33
    .local v1, "yVals":Ljava/util/List;, "Ljava/util/List<Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;>;"
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_6
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/PieDataSet;->mYVals:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_20

    .line 34
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/PieDataSet;->mYVals:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;->copy()Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 33
    add-int/lit8 v2, v2, 0x1

    goto :goto_6

    .line 38
    .end local v2    # "i":I
    :cond_20
    new-instance v2, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/PieDataSet;

    invoke-virtual {p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/PieDataSet;->getLabel()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v1, v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/PieDataSet;-><init>(Ljava/util/List;Ljava/lang/String;)V

    .line 39
    .local v2, "copied":Lbr/com/itau/widgets/graphicalfriedcake/charts/data/PieDataSet;
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/PieDataSet;->mColors:Ljava/util/List;

    iput-object v0, v2, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/PieDataSet;->mColors:Ljava/util/List;

    .line 40
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/PieDataSet;->mSliceSpace:F

    iput v0, v2, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/PieDataSet;->mSliceSpace:F

    .line 41
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/PieDataSet;->mShift:F

    iput v0, v2, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/PieDataSet;->mShift:F

    .line 42
    return-object v2
.end method

.method public getSelectionShift()F
    .registers 2

    .line 94
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/PieDataSet;->mShift:F

    return v0
.end method

.method public getSliceSpace()F
    .registers 2

    .line 72
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/PieDataSet;->mSliceSpace:F

    return v0
.end method

.method public setSelectionShift(F)V
    .registers 3
    .param p1, "shift"    # F

    .line 83
    invoke-static {p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Utils;->convertDpToPixel(F)F

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/PieDataSet;->mShift:F

    .line 84
    return-void
.end method

.method public setSliceSpace(F)V
    .registers 3
    .param p1, "degrees"    # F

    .line 55
    const/high16 v0, 0x42340000    # 45.0f

    cmpl-float v0, p1, v0

    if-lez v0, :cond_8

    .line 56
    const/high16 p1, 0x42340000    # 45.0f

    .line 57
    :cond_8
    const/4 v0, 0x0

    cmpg-float v0, p1, v0

    if-gez v0, :cond_e

    .line 58
    const/4 p1, 0x0

    .line 61
    :cond_e
    iput p1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/PieDataSet;->mSliceSpace:F

    .line 62
    return-void
.end method

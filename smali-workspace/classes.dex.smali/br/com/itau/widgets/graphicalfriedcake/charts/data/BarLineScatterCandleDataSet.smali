.class public abstract Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarLineScatterCandleDataSet;
.super Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;
.source "BarLineScatterCandleDataSet.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;>Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet<TT;>;"
    }
.end annotation


# instance fields
.field protected mHighLightColor:I


# direct methods
.method public constructor <init>(Ljava/util/List;Ljava/lang/String;)V
    .registers 6
    .param p1, "yVals"    # Ljava/util/List;
    .param p2, "label"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<TT;>;Ljava/lang/String;)V"
        }
    .end annotation

    .line 22
    invoke-direct {p0, p1, p2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;-><init>(Ljava/util/List;Ljava/lang/String;)V

    .line 18
    const/16 v0, 0xff

    const/16 v1, 0xbb

    const/16 v2, 0x73

    invoke-static {v0, v1, v2}, Landroid/graphics/Color;->rgb(III)I

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarLineScatterCandleDataSet;->mHighLightColor:I

    .line 23
    return-void
.end method


# virtual methods
.method public getHighLightColor()I
    .registers 2

    .line 44
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarLineScatterCandleDataSet;->mHighLightColor:I

    return v0
.end method

.method public setHighLightColor(I)V
    .registers 2
    .param p1, "color"    # I

    .line 34
    iput p1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarLineScatterCandleDataSet;->mHighLightColor:I

    .line 35
    return-void
.end method

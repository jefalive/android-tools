.class public Lcom/itau/empresas/api/model/AgenciaVO;
.super Ljava/lang/Object;
.source "AgenciaVO.java"


# instance fields
.field private abertura:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "Abertura"
    .end annotation
.end field

.field private atendePJ:Z
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "AtendePJ"
    .end annotation
.end field

.field private bairro:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "Bairro"
    .end annotation
.end field

.field private caixaEletronico:Ljava/lang/Object;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "CaixaEletronico"
    .end annotation
.end field

.field private caixaExclusivo:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "Caixa_exclusivo"
    .end annotation
.end field

.field private cep:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "Cep"
    .end annotation
.end field

.field private cidade:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "Cidade"
    .end annotation
.end field

.field private ddd:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "DDD"
    .end annotation
.end field

.field private endereco:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "Endereco"
    .end annotation
.end field

.field private fax:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "Fax"
    .end annotation
.end field

.field private fechamento:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "Fechamento"
    .end annotation
.end field

.field private horarioDiferenciado:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "Horario_diferenciado"
    .end annotation
.end field

.field private id:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "Id"
    .end annotation
.end field

.field private imagem:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "Imagem"
    .end annotation
.end field

.field private latitude:D
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "Latitude"
    .end annotation
.end field

.field private longitude:D
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "Longitude"
    .end annotation
.end field

.field private mensagem:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "Mensagem"
    .end annotation
.end field

.field private nome:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "Nome"
    .end annotation
.end field

.field private segmento:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "Segmento"
    .end annotation
.end field

.field private telefone:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "Telefone"
    .end annotation
.end field

.field private uf:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "Uf"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getAtendePJ()Z
    .registers 2

    .line 94
    iget-boolean v0, p0, Lcom/itau/empresas/api/model/AgenciaVO;->atendePJ:Z

    return v0
.end method

.method public getCaixaExclusivo()Ljava/lang/String;
    .registers 2

    .line 83
    iget-object v0, p0, Lcom/itau/empresas/api/model/AgenciaVO;->caixaExclusivo:Ljava/lang/String;

    if-nez v0, :cond_7

    .line 84
    const-string v0, ""

    return-object v0

    .line 86
    :cond_7
    iget-object v0, p0, Lcom/itau/empresas/api/model/AgenciaVO;->caixaExclusivo:Ljava/lang/String;

    return-object v0
.end method

.method public getComplementoEndereco()Ljava/lang/String;
    .registers 3

    .line 62
    iget-object v0, p0, Lcom/itau/empresas/api/model/AgenciaVO;->bairro:Ljava/lang/String;

    if-eqz v0, :cond_10

    iget-object v0, p0, Lcom/itau/empresas/api/model/AgenciaVO;->cep:Ljava/lang/String;

    if-eqz v0, :cond_10

    iget-object v0, p0, Lcom/itau/empresas/api/model/AgenciaVO;->cidade:Ljava/lang/String;

    if-eqz v0, :cond_10

    iget-object v0, p0, Lcom/itau/empresas/api/model/AgenciaVO;->uf:Ljava/lang/String;

    if-nez v0, :cond_13

    .line 63
    :cond_10
    const-string v0, ""

    return-object v0

    .line 65
    :cond_13
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/itau/empresas/api/model/AgenciaVO;->bairro:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/api/model/AgenciaVO;->cep:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/api/model/AgenciaVO;->cidade:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/api/model/AgenciaVO;->uf:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getEndereco()Ljava/lang/String;
    .registers 2

    .line 55
    iget-object v0, p0, Lcom/itau/empresas/api/model/AgenciaVO;->endereco:Ljava/lang/String;

    if-nez v0, :cond_7

    .line 56
    const-string v0, ""

    return-object v0

    .line 58
    :cond_7
    iget-object v0, p0, Lcom/itau/empresas/api/model/AgenciaVO;->endereco:Ljava/lang/String;

    return-object v0
.end method

.method public getHorario()Ljava/lang/String;
    .registers 3

    .line 76
    iget-object v0, p0, Lcom/itau/empresas/api/model/AgenciaVO;->abertura:Ljava/lang/String;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/itau/empresas/api/model/AgenciaVO;->fechamento:Ljava/lang/String;

    if-nez v0, :cond_b

    .line 77
    :cond_8
    const-string v0, ""

    return-object v0

    .line 79
    :cond_b
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "de segunda \u00e0 sexta das "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/api/model/AgenciaVO;->abertura:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " \u00e0s "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/api/model/AgenciaVO;->fechamento:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getHorarioDiferenciado()Ljava/lang/String;
    .registers 2

    .line 90
    iget-object v0, p0, Lcom/itau/empresas/api/model/AgenciaVO;->horarioDiferenciado:Ljava/lang/String;

    return-object v0
.end method

.method public getId()Ljava/lang/String;
    .registers 2

    .line 98
    iget-object v0, p0, Lcom/itau/empresas/api/model/AgenciaVO;->id:Ljava/lang/String;

    return-object v0
.end method

.method public getLatitude()D
    .registers 3

    .line 102
    iget-wide v0, p0, Lcom/itau/empresas/api/model/AgenciaVO;->latitude:D

    return-wide v0
.end method

.method public getLongitude()D
    .registers 3

    .line 106
    iget-wide v0, p0, Lcom/itau/empresas/api/model/AgenciaVO;->longitude:D

    return-wide v0
.end method

.method public getNome()Ljava/lang/String;
    .registers 2

    .line 51
    iget-object v0, p0, Lcom/itau/empresas/api/model/AgenciaVO;->nome:Ljava/lang/String;

    return-object v0
.end method

.method public getTelefone()Ljava/lang/String;
    .registers 3

    .line 69
    iget-object v0, p0, Lcom/itau/empresas/api/model/AgenciaVO;->ddd:Ljava/lang/String;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/itau/empresas/api/model/AgenciaVO;->telefone:Ljava/lang/String;

    if-nez v0, :cond_b

    .line 70
    :cond_8
    const-string v0, ""

    return-object v0

    .line 72
    :cond_b
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/itau/empresas/api/model/AgenciaVO;->ddd:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/api/model/AgenciaVO;->telefone:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.class public final Lcom/itau/empresas/feature/credito/lis/activity/LisComprovanteActivity_;
.super Lcom/itau/empresas/feature/credito/lis/activity/LisComprovanteActivity;
.source "LisComprovanteActivity_.java"

# interfaces
.implements Lorg/androidannotations/api/view/HasViews;
.implements Lorg/androidannotations/api/view/OnViewChangedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/itau/empresas/feature/credito/lis/activity/LisComprovanteActivity_$IntentBuilder_;
    }
.end annotation


# instance fields
.field private final onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;


# direct methods
.method public constructor <init>()V
    .registers 2

    .line 37
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisComprovanteActivity;-><init>()V

    .line 41
    new-instance v0, Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-direct {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisComprovanteActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    return-void
.end method

.method static synthetic access$001(Lcom/itau/empresas/feature/credito/lis/activity/LisComprovanteActivity_;)V
    .registers 1
    .param p0, "x0"    # Lcom/itau/empresas/feature/credito/lis/activity/LisComprovanteActivity_;

    .line 37
    invoke-super {p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisComprovanteActivity;->mostraDialogoDeProgresso()V

    return-void
.end method

.method static synthetic access$101(Lcom/itau/empresas/feature/credito/lis/activity/LisComprovanteActivity_;)V
    .registers 1
    .param p0, "x0"    # Lcom/itau/empresas/feature/credito/lis/activity/LisComprovanteActivity_;

    .line 37
    invoke-super {p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisComprovanteActivity;->escondeDialogoDeProgresso()V

    return-void
.end method

.method private init_(Landroid/os/Bundle;)V
    .registers 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 55
    new-instance v0, Lcom/itau/empresas/Preferences_;

    invoke-direct {v0, p0}, Lcom/itau/empresas/Preferences_;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisComprovanteActivity_;->prefs:Lcom/itau/empresas/Preferences_;

    .line 56
    invoke-static {p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->registerOnViewChangedListener(Lorg/androidannotations/api/view/OnViewChangedListener;)V

    .line 57
    invoke-static {}, Lcom/itau/empresas/CustomApplication_;->getInstance()Lcom/itau/empresas/CustomApplication;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisComprovanteActivity_;->application:Lcom/itau/empresas/CustomApplication;

    .line 58
    invoke-static {p0}, Lcom/itau/empresas/feature/credito/lis/LisController_;->getInstance_(Landroid/content/Context;)Lcom/itau/empresas/feature/credito/lis/LisController_;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisComprovanteActivity_;->lisController:Lcom/itau/empresas/feature/credito/lis/LisController;

    .line 59
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisComprovanteActivity_;->injectExtras_()V

    .line 60
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisComprovanteActivity_;->afterInject()V

    .line 61
    return-void
.end method

.method private injectExtras_()V
    .registers 3

    .line 117
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisComprovanteActivity_;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 118
    .local v1, "extras_":Landroid/os/Bundle;
    if-eqz v1, :cond_2c

    .line 119
    const-string v0, "contratacaoRespostaVO"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 120
    const-string v0, "contratacaoRespostaVO"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/feature/credito/lis/model/LisContratacaoRespostaVO;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisComprovanteActivity_;->contratacaoRespostaVO:Lcom/itau/empresas/feature/credito/lis/model/LisContratacaoRespostaVO;

    .line 122
    :cond_1c
    const-string v0, "lisAdicional"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2c

    .line 123
    const-string v0, "lisAdicional"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisComprovanteActivity_;->lisAdicional:Z

    .line 126
    :cond_2c
    return-void
.end method


# virtual methods
.method public escondeDialogoDeProgresso()V
    .registers 5

    .line 151
    const-string v0, ""

    new-instance v1, Lcom/itau/empresas/feature/credito/lis/activity/LisComprovanteActivity_$3;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisComprovanteActivity_$3;-><init>(Lcom/itau/empresas/feature/credito/lis/activity/LisComprovanteActivity_;)V

    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3}, Lorg/androidannotations/api/UiThreadExecutor;->runTask(Ljava/lang/String;Ljava/lang/Runnable;J)V

    .line 159
    return-void
.end method

.method public mostraDialogoDeProgresso()V
    .registers 5
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "CommitTransaction"
        }
    .end annotation

    .line 139
    const-string v0, ""

    new-instance v1, Lcom/itau/empresas/feature/credito/lis/activity/LisComprovanteActivity_$2;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisComprovanteActivity_$2;-><init>(Lcom/itau/empresas/feature/credito/lis/activity/LisComprovanteActivity_;)V

    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3}, Lorg/androidannotations/api/UiThreadExecutor;->runTask(Ljava/lang/String;Ljava/lang/Runnable;J)V

    .line 147
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 47
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisComprovanteActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-static {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    move-result-object v1

    .line 48
    .local v1, "previousNotifier":Lorg/androidannotations/api/view/OnViewChangedNotifier;
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/credito/lis/activity/LisComprovanteActivity_;->init_(Landroid/os/Bundle;)V

    .line 49
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/credito/lis/activity/LisComprovanteActivity;->onCreate(Landroid/os/Bundle;)V

    .line 50
    invoke-static {v1}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 51
    const v0, 0x7f030044

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/credito/lis/activity/LisComprovanteActivity_;->setContentView(I)V

    .line 52
    return-void
.end method

.method public onViewChanged(Lorg/androidannotations/api/view/HasViews;)V
    .registers 4
    .param p1, "hasViews"    # Lorg/androidannotations/api/view/HasViews;

    .line 95
    const v0, 0x7f0e01e2

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisComprovanteActivity_;->root:Landroid/widget/LinearLayout;

    .line 96
    const v0, 0x7f0e01e4

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisComprovanteActivity_;->textoContratacaoSucesso:Landroid/widget/TextView;

    .line 97
    const v0, 0x7f0e01e5

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisComprovanteActivity_;->textoLisComprovanteProduto:Landroid/widget/TextView;

    .line 98
    const v0, 0x7f0e01e6

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisComprovanteActivity_;->textoLisValorLimite:Landroid/widget/TextView;

    .line 99
    const v0, 0x7f0e01e7

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisComprovanteActivity_;->textoLisDataVencimento:Landroid/widget/TextView;

    .line 100
    const v0, 0x7f0e00b6

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/Toolbar;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisComprovanteActivity_;->toolBar:Landroid/support/v7/widget/Toolbar;

    .line 101
    const v0, 0x7f0e01e8

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 103
    .local v1, "view_botao_detalhe":Landroid/view/View;
    if-eqz v1, :cond_53

    .line 104
    new-instance v0, Lcom/itau/empresas/feature/credito/lis/activity/LisComprovanteActivity_$1;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisComprovanteActivity_$1;-><init>(Lcom/itau/empresas/feature/credito/lis/activity/LisComprovanteActivity_;)V

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 113
    :cond_53
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisComprovanteActivity_;->aoCarregarTela()V

    .line 114
    return-void
.end method

.method public setContentView(I)V
    .registers 3
    .param p1, "layoutResID"    # I

    .line 65
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/credito/lis/activity/LisComprovanteActivity;->setContentView(I)V

    .line 66
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisComprovanteActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 67
    return-void
.end method

.method public setContentView(Landroid/view/View;)V
    .registers 3
    .param p1, "view"    # Landroid/view/View;

    .line 77
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/credito/lis/activity/LisComprovanteActivity;->setContentView(Landroid/view/View;)V

    .line 78
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisComprovanteActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 79
    return-void
.end method

.method public setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .registers 4
    .param p1, "view"    # Landroid/view/View;
    .param p2, "params"    # Landroid/view/ViewGroup$LayoutParams;

    .line 71
    invoke-super {p0, p1, p2}, Lcom/itau/empresas/feature/credito/lis/activity/LisComprovanteActivity;->setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 72
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisComprovanteActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 73
    return-void
.end method

.method public setIntent(Landroid/content/Intent;)V
    .registers 2
    .param p1, "newIntent"    # Landroid/content/Intent;

    .line 130
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/credito/lis/activity/LisComprovanteActivity;->setIntent(Landroid/content/Intent;)V

    .line 131
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisComprovanteActivity_;->injectExtras_()V

    .line 132
    return-void
.end method

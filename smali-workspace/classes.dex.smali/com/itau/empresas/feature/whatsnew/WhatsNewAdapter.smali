.class Lcom/itau/empresas/feature/whatsnew/WhatsNewAdapter;
.super Landroid/support/v4/view/PagerAdapter;
.source "WhatsNewAdapter.java"


# instance fields
.field private final context:Landroid/content/Context;

.field private final listaWhatsNew:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Lcom/itau/empresas/feature/whatsnew/WhatsNewVO;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Landroid/content/Context;Ljava/util/List;)V
    .registers 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "listaWhatsNew"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Landroid/content/Context;Ljava/util/List<Lcom/itau/empresas/feature/whatsnew/WhatsNewVO;>;)V"
        }
    .end annotation

    .line 26
    invoke-direct {p0}, Landroid/support/v4/view/PagerAdapter;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/itau/empresas/feature/whatsnew/WhatsNewAdapter;->context:Landroid/content/Context;

    .line 28
    iput-object p2, p0, Lcom/itau/empresas/feature/whatsnew/WhatsNewAdapter;->listaWhatsNew:Ljava/util/List;

    .line 29
    return-void
.end method


# virtual methods
.method public destroyItem(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .registers 5
    .param p1, "container"    # Landroid/view/ViewGroup;
    .param p2, "position"    # I
    .param p3, "object"    # Ljava/lang/Object;

    .line 66
    move-object v0, p3

    check-cast v0, Landroid/widget/RelativeLayout;

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 67
    return-void
.end method

.method public getCount()I
    .registers 2

    .line 33
    iget-object v0, p0, Lcom/itau/empresas/feature/whatsnew/WhatsNewAdapter;->listaWhatsNew:Ljava/util/List;

    if-nez v0, :cond_6

    .line 34
    const/4 v0, 0x0

    return v0

    .line 36
    :cond_6
    iget-object v0, p0, Lcom/itau/empresas/feature/whatsnew/WhatsNewAdapter;->listaWhatsNew:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public instantiateItem(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .registers 10
    .param p1, "container"    # Landroid/view/ViewGroup;
    .param p2, "position"    # I

    .line 47
    iget-object v0, p0, Lcom/itau/empresas/feature/whatsnew/WhatsNewAdapter;->context:Landroid/content/Context;

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Landroid/view/LayoutInflater;

    .line 48
    .local v2, "layoutInflater":Landroid/view/LayoutInflater;
    const v0, 0x7f03011b

    const/4 v1, 0x0

    invoke-virtual {v2, v0, p1, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    .line 50
    .local v3, "itemView":Landroid/view/View;
    const v0, 0x7f0e0574

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Landroid/support/v7/widget/AppCompatImageView;

    .line 51
    .local v4, "imagem":Landroid/support/v7/widget/AppCompatImageView;
    iget-object v0, p0, Lcom/itau/empresas/feature/whatsnew/WhatsNewAdapter;->listaWhatsNew:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/feature/whatsnew/WhatsNewVO;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/whatsnew/WhatsNewVO;->getImagem()I

    move-result v0

    invoke-virtual {v4, v0}, Landroid/support/v7/widget/AppCompatImageView;->setImageResource(I)V

    .line 53
    const v0, 0x7f0e00c9

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Landroid/widget/TextView;

    .line 54
    .local v5, "textoTitulo":Landroid/widget/TextView;
    iget-object v0, p0, Lcom/itau/empresas/feature/whatsnew/WhatsNewAdapter;->listaWhatsNew:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/feature/whatsnew/WhatsNewVO;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/whatsnew/WhatsNewVO;->getTitulo()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 56
    const v0, 0x7f0e0355

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Landroid/widget/TextView;

    .line 57
    .local v6, "textoConteudo":Landroid/widget/TextView;
    iget-object v0, p0, Lcom/itau/empresas/feature/whatsnew/WhatsNewAdapter;->listaWhatsNew:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/feature/whatsnew/WhatsNewVO;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/whatsnew/WhatsNewVO;->getConteudo()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 59
    invoke-virtual {p1, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 61
    return-object v3
.end method

.method public isViewFromObject(Landroid/view/View;Ljava/lang/Object;)Z
    .registers 4
    .param p1, "view"    # Landroid/view/View;
    .param p2, "object"    # Ljava/lang/Object;

    .line 41
    if-ne p1, p2, :cond_4

    const/4 v0, 0x1

    goto :goto_5

    :cond_4
    const/4 v0, 0x0

    :goto_5
    return v0
.end method

.class public Lcom/itau/empresas/ui/view/CheckSucesso;
.super Landroid/view/View;
.source "CheckSucesso.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/itau/empresas/ui/view/CheckSucesso$ListenerAnimacao;,
        Lcom/itau/empresas/ui/view/CheckSucesso$Point;
    }
.end annotation


# static fields
.field private static final GREEN:I

.field private static final WHITE:I


# instance fields
.field private a:Lcom/itau/empresas/ui/view/CheckSucesso$Point;

.field private arcWidth:F

.field private arrowWidth:F

.field private b:Lcom/itau/empresas/ui/view/CheckSucesso$Point;

.field private c:Lcom/itau/empresas/ui/view/CheckSucesso$Point;

.field private checkedPaint:Landroid/graphics/Paint;

.field private checkedPath:Landroid/graphics/Path;

.field private circuloPaint:Landroid/graphics/Paint;

.field private count:I

.field private d:Lcom/itau/empresas/ui/view/CheckSucesso$Point;

.field private e:Lcom/itau/empresas/ui/view/CheckSucesso$Point;

.field private hookCount:I

.field private hookStepY:F

.field private isAnimating:Z

.field private isCompleted:Z

.field private isEnd:Z

.field private isFirst:Z

.field private isLoading:Z

.field private jumpPoint:Lcom/itau/empresas/ui/view/CheckSucesso$Point;

.field private length:F

.field lengthX:F

.field lengthY:F

.field private listenerAnimacao:Lcom/itau/empresas/ui/view/CheckSucesso$ListenerAnimacao;

.field private littleStep:F

.field private loadingPaint:Landroid/graphics/Paint;

.field private loadingWidth:F

.field private oval:Landroid/graphics/RectF;

.field private progresso:I

.field private radio:F

.field private smallPaint:Landroid/graphics/Paint;

.field private smallRadius:F

.field private x:F

.field private y:F


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .line 20
    const/16 v0, 0x52

    const/16 v1, 0xa0

    const/16 v2, 0x79

    invoke-static {v0, v1, v2}, Landroid/graphics/Color;->rgb(III)I

    move-result v0

    sput v0, Lcom/itau/empresas/ui/view/CheckSucesso;->GREEN:I

    .line 21
    const/16 v0, 0xff

    const/16 v1, 0xff

    const/16 v2, 0xff

    invoke-static {v0, v1, v2}, Landroid/graphics/Color;->rgb(III)I

    move-result v0

    sput v0, Lcom/itau/empresas/ui/view/CheckSucesso;->WHITE:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .param p1, "context"    # Landroid/content/Context;

    .line 73
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/itau/empresas/ui/view/CheckSucesso;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 74
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .line 77
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/itau/empresas/ui/view/CheckSucesso;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 78
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .line 81
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 37
    const v0, 0x44098000    # 550.0f

    iput v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->x:F

    .line 38
    const v0, 0x44098000    # 550.0f

    iput v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->y:F

    .line 39
    const/high16 v0, 0x43340000    # 180.0f

    iput v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->radio:F

    .line 40
    iget v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->radio:F

    const/high16 v1, 0x40400000    # 3.0f

    mul-float/2addr v0, v1

    const/high16 v1, 0x40800000    # 4.0f

    div-float/2addr v0, v1

    iput v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->lengthX:F

    .line 41
    iget v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->radio:F

    const/high16 v1, 0x40400000    # 3.0f

    mul-float/2addr v0, v1

    const/high16 v1, 0x40800000    # 4.0f

    div-float/2addr v0, v1

    iput v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->lengthY:F

    .line 42
    const/high16 v0, 0x41700000    # 15.0f

    iput v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->hookStepY:F

    .line 43
    const/high16 v0, 0x41000000    # 8.0f

    iput v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->littleStep:F

    .line 44
    const/high16 v0, 0x40a00000    # 5.0f

    iput v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->smallRadius:F

    .line 45
    const/high16 v0, 0x41a00000    # 20.0f

    iput v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->arcWidth:F

    .line 46
    const/high16 v0, 0x41200000    # 10.0f

    iput v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->arrowWidth:F

    .line 47
    const/high16 v0, 0x41200000    # 10.0f

    iput v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->loadingWidth:F

    .line 60
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->isFirst:Z

    .line 61
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->isAnimating:Z

    .line 62
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->isLoading:Z

    .line 63
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->isCompleted:Z

    .line 64
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->isEnd:Z

    .line 65
    const/4 v0, 0x0

    iput v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->count:I

    .line 67
    const/4 v0, 0x0

    iput v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->progresso:I

    .line 68
    const/4 v0, 0x0

    iput v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->hookCount:I

    .line 82
    invoke-direct {p0}, Lcom/itau/empresas/ui/view/CheckSucesso;->init()V

    .line 83
    return-void
.end method

.method static synthetic access$000(Lcom/itau/empresas/ui/view/CheckSucesso;)I
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/ui/view/CheckSucesso;

    .line 18
    iget v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->progresso:I

    return v0
.end method

.method static synthetic access$002(Lcom/itau/empresas/ui/view/CheckSucesso;I)I
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/ui/view/CheckSucesso;
    .param p1, "x1"    # I

    .line 18
    iput p1, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->progresso:I

    return p1
.end method

.method static synthetic access$300(Lcom/itau/empresas/ui/view/CheckSucesso;)V
    .registers 1
    .param p0, "x0"    # Lcom/itau/empresas/ui/view/CheckSucesso;

    .line 18
    invoke-direct {p0}, Lcom/itau/empresas/ui/view/CheckSucesso;->notificaListenerFim()V

    return-void
.end method

.method private afterCompleted(Landroid/graphics/Canvas;)V
    .registers 7
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .line 340
    iget v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->x:F

    iget v1, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->y:F

    iget v2, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->radio:F

    iget-object v3, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->loadingPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 341
    iget v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->hookCount:I

    int-to-long v0, v0

    invoke-static {v0, v1}, Ljava/math/BigDecimal;->valueOf(J)Ljava/math/BigDecimal;

    move-result-object v0

    const-wide/high16 v1, 0x4008000000000000L    # 3.0

    invoke-static {v1, v2}, Ljava/math/BigDecimal;->valueOf(D)Ljava/math/BigDecimal;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/math/BigDecimal;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5e

    .line 342
    iget-object v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->e:Lcom/itau/empresas/ui/view/CheckSucesso$Point;

    iget-object v1, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->e:Lcom/itau/empresas/ui/view/CheckSucesso$Point;

    # getter for: Lcom/itau/empresas/ui/view/CheckSucesso$Point;->y:F
    invoke-static {v1}, Lcom/itau/empresas/ui/view/CheckSucesso$Point;->access$200(Lcom/itau/empresas/ui/view/CheckSucesso$Point;)F

    move-result v1

    iget v2, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->littleStep:F

    add-float/2addr v1, v2

    # setter for: Lcom/itau/empresas/ui/view/CheckSucesso$Point;->y:F
    invoke-static {v0, v1}, Lcom/itau/empresas/ui/view/CheckSucesso$Point;->access$202(Lcom/itau/empresas/ui/view/CheckSucesso$Point;F)F

    .line 343
    iget-object v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->c:Lcom/itau/empresas/ui/view/CheckSucesso$Point;

    iget-object v1, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->c:Lcom/itau/empresas/ui/view/CheckSucesso$Point;

    # getter for: Lcom/itau/empresas/ui/view/CheckSucesso$Point;->x:F
    invoke-static {v1}, Lcom/itau/empresas/ui/view/CheckSucesso$Point;->access$100(Lcom/itau/empresas/ui/view/CheckSucesso$Point;)F

    move-result v1

    iget v2, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->littleStep:F

    sub-float/2addr v1, v2

    # setter for: Lcom/itau/empresas/ui/view/CheckSucesso$Point;->x:F
    invoke-static {v0, v1}, Lcom/itau/empresas/ui/view/CheckSucesso$Point;->access$102(Lcom/itau/empresas/ui/view/CheckSucesso$Point;F)F

    .line 344
    iget-object v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->d:Lcom/itau/empresas/ui/view/CheckSucesso$Point;

    iget-object v1, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->d:Lcom/itau/empresas/ui/view/CheckSucesso$Point;

    # getter for: Lcom/itau/empresas/ui/view/CheckSucesso$Point;->x:F
    invoke-static {v1}, Lcom/itau/empresas/ui/view/CheckSucesso$Point;->access$100(Lcom/itau/empresas/ui/view/CheckSucesso$Point;)F

    move-result v1

    iget v2, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->littleStep:F

    add-float/2addr v1, v2

    # setter for: Lcom/itau/empresas/ui/view/CheckSucesso$Point;->x:F
    invoke-static {v0, v1}, Lcom/itau/empresas/ui/view/CheckSucesso$Point;->access$102(Lcom/itau/empresas/ui/view/CheckSucesso$Point;F)F

    .line 345
    iget-object v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->d:Lcom/itau/empresas/ui/view/CheckSucesso$Point;

    iget-object v1, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->d:Lcom/itau/empresas/ui/view/CheckSucesso$Point;

    # getter for: Lcom/itau/empresas/ui/view/CheckSucesso$Point;->y:F
    invoke-static {v1}, Lcom/itau/empresas/ui/view/CheckSucesso$Point;->access$200(Lcom/itau/empresas/ui/view/CheckSucesso$Point;)F

    move-result v1

    iget v2, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->littleStep:F

    sub-float/2addr v1, v2

    # setter for: Lcom/itau/empresas/ui/view/CheckSucesso$Point;->y:F
    invoke-static {v0, v1}, Lcom/itau/empresas/ui/view/CheckSucesso$Point;->access$202(Lcom/itau/empresas/ui/view/CheckSucesso$Point;F)F

    .line 346
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->isCompleted:Z

    .line 347
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->isEnd:Z

    goto/16 :goto_c2

    .line 349
    :cond_5e
    iget-object v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->e:Lcom/itau/empresas/ui/view/CheckSucesso$Point;

    iget v1, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->x:F

    # setter for: Lcom/itau/empresas/ui/view/CheckSucesso$Point;->x:F
    invoke-static {v0, v1}, Lcom/itau/empresas/ui/view/CheckSucesso$Point;->access$102(Lcom/itau/empresas/ui/view/CheckSucesso$Point;F)F

    .line 350
    iget-object v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->e:Lcom/itau/empresas/ui/view/CheckSucesso$Point;

    iget v1, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->y:F

    iget v2, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->hookStepY:F

    iget v3, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->hookCount:I

    add-int/lit8 v3, v3, 0x1

    int-to-float v3, v3

    mul-float/2addr v2, v3

    add-float/2addr v1, v2

    # setter for: Lcom/itau/empresas/ui/view/CheckSucesso$Point;->y:F
    invoke-static {v0, v1}, Lcom/itau/empresas/ui/view/CheckSucesso$Point;->access$202(Lcom/itau/empresas/ui/view/CheckSucesso$Point;F)F

    .line 351
    iget v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->lengthX:F

    const/high16 v1, 0x40400000    # 3.0f

    mul-float/2addr v0, v1

    const/high16 v1, 0x40800000    # 4.0f

    div-float/2addr v0, v1

    iput v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->lengthX:F

    .line 352
    iget-object v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->c:Lcom/itau/empresas/ui/view/CheckSucesso$Point;

    iget v1, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->x:F

    iget v2, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->lengthX:F

    const/high16 v3, 0x40400000    # 3.0f

    mul-float/2addr v2, v3

    const/high16 v3, 0x40800000    # 4.0f

    div-float/2addr v2, v3

    sub-float/2addr v1, v2

    # setter for: Lcom/itau/empresas/ui/view/CheckSucesso$Point;->x:F
    invoke-static {v0, v1}, Lcom/itau/empresas/ui/view/CheckSucesso$Point;->access$102(Lcom/itau/empresas/ui/view/CheckSucesso$Point;F)F

    .line 353
    iget-object v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->c:Lcom/itau/empresas/ui/view/CheckSucesso$Point;

    iget v1, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->y:F

    # setter for: Lcom/itau/empresas/ui/view/CheckSucesso$Point;->y:F
    invoke-static {v0, v1}, Lcom/itau/empresas/ui/view/CheckSucesso$Point;->access$202(Lcom/itau/empresas/ui/view/CheckSucesso$Point;F)F

    .line 354
    iget-object v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->d:Lcom/itau/empresas/ui/view/CheckSucesso$Point;

    iget v1, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->x:F

    iget v2, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->lengthY:F

    add-float/2addr v1, v2

    iget v2, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->radio:F

    const/high16 v3, 0x41000000    # 8.0f

    div-float/2addr v2, v3

    iget v3, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->hookCount:I

    add-int/lit8 v3, v3, 0x1

    int-to-float v3, v3

    mul-float/2addr v2, v3

    sub-float/2addr v1, v2

    # setter for: Lcom/itau/empresas/ui/view/CheckSucesso$Point;->x:F
    invoke-static {v0, v1}, Lcom/itau/empresas/ui/view/CheckSucesso$Point;->access$102(Lcom/itau/empresas/ui/view/CheckSucesso$Point;F)F

    .line 355
    iget-object v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->d:Lcom/itau/empresas/ui/view/CheckSucesso$Point;

    iget v1, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->y:F

    iget v2, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->hookStepY:F

    iget v3, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->hookCount:I

    add-int/lit8 v3, v3, 0x1

    int-to-float v3, v3

    mul-float/2addr v2, v3

    sub-float/2addr v1, v2

    # setter for: Lcom/itau/empresas/ui/view/CheckSucesso$Point;->y:F
    invoke-static {v0, v1}, Lcom/itau/empresas/ui/view/CheckSucesso$Point;->access$202(Lcom/itau/empresas/ui/view/CheckSucesso$Point;F)F

    .line 356
    iget v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->hookCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->hookCount:I

    .line 358
    :goto_c2
    invoke-direct {p0}, Lcom/itau/empresas/ui/view/CheckSucesso;->desenhaCirculoFinal()V

    .line 359
    sget v0, Lcom/itau/empresas/ui/view/CheckSucesso;->WHITE:I

    invoke-direct {p0, p1, v0}, Lcom/itau/empresas/ui/view/CheckSucesso;->desenhaCheckInicial(Landroid/graphics/Canvas;I)V

    .line 360
    const-wide/16 v0, 0xa

    invoke-virtual {p0, v0, v1}, Lcom/itau/empresas/ui/view/CheckSucesso;->postInvalidateDelayed(J)V

    .line 361
    invoke-direct {p0}, Lcom/itau/empresas/ui/view/CheckSucesso;->handlerNotificacaoListener()V

    .line 362
    return-void
.end method

.method private convert(F)F
    .registers 4
    .param p1, "original"    # F

    .line 400
    iget v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->radio:F

    mul-float/2addr v0, p1

    const/high16 v1, 0x43340000    # 180.0f

    div-float/2addr v0, v1

    return v0
.end method

.method private desenhaCheck(Landroid/graphics/Canvas;I)V
    .registers 7
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "color"    # I

    .line 297
    iget-object v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->checkedPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p2}, Landroid/graphics/Paint;->setColor(I)V

    .line 298
    iget-object v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->checkedPath:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->e:Lcom/itau/empresas/ui/view/CheckSucesso$Point;

    # getter for: Lcom/itau/empresas/ui/view/CheckSucesso$Point;->x:F
    invoke-static {v1}, Lcom/itau/empresas/ui/view/CheckSucesso$Point;->access$100(Lcom/itau/empresas/ui/view/CheckSucesso$Point;)F

    move-result v1

    iget-object v2, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->e:Lcom/itau/empresas/ui/view/CheckSucesso$Point;

    # getter for: Lcom/itau/empresas/ui/view/CheckSucesso$Point;->y:F
    invoke-static {v2}, Lcom/itau/empresas/ui/view/CheckSucesso$Point;->access$200(Lcom/itau/empresas/ui/view/CheckSucesso$Point;)F

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 299
    iget-object v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->checkedPath:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->c:Lcom/itau/empresas/ui/view/CheckSucesso$Point;

    # getter for: Lcom/itau/empresas/ui/view/CheckSucesso$Point;->x:F
    invoke-static {v1}, Lcom/itau/empresas/ui/view/CheckSucesso$Point;->access$100(Lcom/itau/empresas/ui/view/CheckSucesso$Point;)F

    move-result v1

    iget-object v2, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->c:Lcom/itau/empresas/ui/view/CheckSucesso$Point;

    # getter for: Lcom/itau/empresas/ui/view/CheckSucesso$Point;->y:F
    invoke-static {v2}, Lcom/itau/empresas/ui/view/CheckSucesso$Point;->access$200(Lcom/itau/empresas/ui/view/CheckSucesso$Point;)F

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 300
    iget-object v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->checkedPath:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->checkedPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 302
    iget-object v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->checkedPath:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    .line 303
    iget-object v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->checkedPath:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->e:Lcom/itau/empresas/ui/view/CheckSucesso$Point;

    # getter for: Lcom/itau/empresas/ui/view/CheckSucesso$Point;->x:F
    invoke-static {v1}, Lcom/itau/empresas/ui/view/CheckSucesso$Point;->access$100(Lcom/itau/empresas/ui/view/CheckSucesso$Point;)F

    move-result v1

    iget-object v2, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->e:Lcom/itau/empresas/ui/view/CheckSucesso$Point;

    # getter for: Lcom/itau/empresas/ui/view/CheckSucesso$Point;->y:F
    invoke-static {v2}, Lcom/itau/empresas/ui/view/CheckSucesso$Point;->access$200(Lcom/itau/empresas/ui/view/CheckSucesso$Point;)F

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 304
    iget-object v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->checkedPath:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->d:Lcom/itau/empresas/ui/view/CheckSucesso$Point;

    # getter for: Lcom/itau/empresas/ui/view/CheckSucesso$Point;->x:F
    invoke-static {v1}, Lcom/itau/empresas/ui/view/CheckSucesso$Point;->access$100(Lcom/itau/empresas/ui/view/CheckSucesso$Point;)F

    move-result v1

    iget-object v2, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->d:Lcom/itau/empresas/ui/view/CheckSucesso$Point;

    # getter for: Lcom/itau/empresas/ui/view/CheckSucesso$Point;->y:F
    invoke-static {v2}, Lcom/itau/empresas/ui/view/CheckSucesso$Point;->access$200(Lcom/itau/empresas/ui/view/CheckSucesso$Point;)F

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 305
    iget-object v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->checkedPath:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->checkedPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 307
    iget-object v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->smallPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p2}, Landroid/graphics/Paint;->setColor(I)V

    .line 308
    iget-object v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->c:Lcom/itau/empresas/ui/view/CheckSucesso$Point;

    # getter for: Lcom/itau/empresas/ui/view/CheckSucesso$Point;->x:F
    invoke-static {v0}, Lcom/itau/empresas/ui/view/CheckSucesso$Point;->access$100(Lcom/itau/empresas/ui/view/CheckSucesso$Point;)F

    move-result v0

    iget-object v1, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->c:Lcom/itau/empresas/ui/view/CheckSucesso$Point;

    # getter for: Lcom/itau/empresas/ui/view/CheckSucesso$Point;->y:F
    invoke-static {v1}, Lcom/itau/empresas/ui/view/CheckSucesso$Point;->access$200(Lcom/itau/empresas/ui/view/CheckSucesso$Point;)F

    move-result v1

    iget v2, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->smallRadius:F

    iget-object v3, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->smallPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 309
    iget-object v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->d:Lcom/itau/empresas/ui/view/CheckSucesso$Point;

    # getter for: Lcom/itau/empresas/ui/view/CheckSucesso$Point;->x:F
    invoke-static {v0}, Lcom/itau/empresas/ui/view/CheckSucesso$Point;->access$100(Lcom/itau/empresas/ui/view/CheckSucesso$Point;)F

    move-result v0

    iget-object v1, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->d:Lcom/itau/empresas/ui/view/CheckSucesso$Point;

    # getter for: Lcom/itau/empresas/ui/view/CheckSucesso$Point;->y:F
    invoke-static {v1}, Lcom/itau/empresas/ui/view/CheckSucesso$Point;->access$200(Lcom/itau/empresas/ui/view/CheckSucesso$Point;)F

    move-result v1

    iget v2, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->smallRadius:F

    iget-object v3, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->smallPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 310
    iget-object v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->e:Lcom/itau/empresas/ui/view/CheckSucesso$Point;

    # getter for: Lcom/itau/empresas/ui/view/CheckSucesso$Point;->x:F
    invoke-static {v0}, Lcom/itau/empresas/ui/view/CheckSucesso$Point;->access$100(Lcom/itau/empresas/ui/view/CheckSucesso$Point;)F

    move-result v0

    iget-object v1, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->e:Lcom/itau/empresas/ui/view/CheckSucesso$Point;

    # getter for: Lcom/itau/empresas/ui/view/CheckSucesso$Point;->y:F
    invoke-static {v1}, Lcom/itau/empresas/ui/view/CheckSucesso$Point;->access$200(Lcom/itau/empresas/ui/view/CheckSucesso$Point;)F

    move-result v1

    iget v2, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->smallRadius:F

    iget-object v3, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->smallPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 311
    return-void
.end method

.method private desenhaCheckInicial(Landroid/graphics/Canvas;I)V
    .registers 9
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "color"    # I

    .line 315
    move-object v0, p1

    iget-object v5, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->checkedPaint:Landroid/graphics/Paint;

    const/high16 v1, 0x43340000    # 180.0f

    const v2, 0x4361582e

    const v3, 0x43089c52

    const/high16 v4, 0x43340000    # 180.0f

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 316
    iget-object v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->checkedPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p2}, Landroid/graphics/Paint;->setColor(I)V

    .line 318
    iget-object v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->checkedPath:Landroid/graphics/Path;

    const/high16 v1, 0x43340000    # 180.0f

    const v2, 0x4361582e

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 319
    iget-object v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->checkedPath:Landroid/graphics/Path;

    const v1, 0x43089c52

    const/high16 v2, 0x43340000    # 180.0f

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 321
    iget-object v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->checkedPath:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->checkedPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 323
    iget-object v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->checkedPath:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    .line 324
    iget-object v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->checkedPath:Landroid/graphics/Path;

    const/high16 v1, 0x43340000    # 180.0f

    const v2, 0x4361582e

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 325
    iget-object v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->checkedPath:Landroid/graphics/Path;

    const v1, 0x4374982e

    const v2, 0x4306a7d2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 326
    iget-object v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->checkedPath:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->checkedPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 328
    iget-object v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->smallPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p2}, Landroid/graphics/Paint;->setColor(I)V

    .line 329
    iget v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->smallRadius:F

    iget-object v1, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->smallPaint:Landroid/graphics/Paint;

    const v2, 0x43089c52

    const/high16 v3, 0x43340000    # 180.0f

    invoke-virtual {p1, v2, v3, v0, v1}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 330
    iget v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->smallRadius:F

    iget-object v1, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->smallPaint:Landroid/graphics/Paint;

    const v2, 0x4374982e

    const v3, 0x4306a7d2

    invoke-virtual {p1, v2, v3, v0, v1}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 331
    iget v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->smallRadius:F

    iget-object v1, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->smallPaint:Landroid/graphics/Paint;

    const/high16 v2, 0x43340000    # 180.0f

    const v3, 0x4361582e

    invoke-virtual {p1, v2, v3, v0, v1}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 332
    return-void
.end method

.method private desenhaCirculoFinal()V
    .registers 3

    .line 288
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->circuloPaint:Landroid/graphics/Paint;

    .line 289
    iget-object v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->circuloPaint:Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 290
    iget-object v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->circuloPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL_AND_STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 291
    iget-object v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->circuloPaint:Landroid/graphics/Paint;

    iget v1, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->arcWidth:F

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 292
    iget-object v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->circuloPaint:Landroid/graphics/Paint;

    sget v1, Lcom/itau/empresas/ui/view/CheckSucesso;->GREEN:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 293
    return-void
.end method

.method private desenhaCirculoInicial(Landroid/graphics/Canvas;)V
    .registers 9
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .line 251
    iget v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->progresso:I

    int-to-float v0, v0

    const/high16 v1, 0x42c80000    # 100.0f

    div-float/2addr v0, v1

    const/high16 v1, 0x43b40000    # 360.0f

    mul-float v6, v0, v1

    .line 252
    .local v6, "sweepAngle":F
    move-object v0, p1

    iget-object v1, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->oval:Landroid/graphics/RectF;

    const/4 v2, 0x0

    sub-float v3, v2, v6

    iget-object v5, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->loadingPaint:Landroid/graphics/Paint;

    const v2, 0x439d8000    # 315.0f

    const/4 v4, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    .line 253
    return-void
.end method

.method private handlerNotificacaoListener()V
    .registers 6

    .line 272
    const/4 v4, 0x1

    .line 273
    .local v4, "segundos":I
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/itau/empresas/ui/view/CheckSucesso$2;

    invoke-direct {v1, p0}, Lcom/itau/empresas/ui/view/CheckSucesso$2;-><init>(Lcom/itau/empresas/ui/view/CheckSucesso;)V

    const-wide/16 v2, 0x384

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 279
    return-void
.end method

.method private init()V
    .registers 5

    .line 151
    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/CheckSucesso;->getHeight()I

    move-result v0

    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/CheckSucesso;->getWidth()I

    move-result v1

    if-le v0, v1, :cond_12

    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/CheckSucesso;->getWidth()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    int-to-float v3, v0

    goto :goto_19

    :cond_12
    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/CheckSucesso;->getHeight()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    int-to-float v3, v0

    .line 152
    .local v3, "temp":F
    :goto_19
    const/high16 v0, 0x41200000    # 10.0f

    mul-float/2addr v0, v3

    const/high16 v1, 0x43340000    # 180.0f

    div-float/2addr v0, v1

    sub-float v0, v3, v0

    const/high16 v1, 0x41200000    # 10.0f

    mul-float/2addr v1, v3

    const/high16 v2, 0x43340000    # 180.0f

    div-float/2addr v1, v2

    sub-float/2addr v0, v1

    const/high16 v1, 0x40c00000    # 6.0f

    sub-float/2addr v0, v1

    iput v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->radio:F

    .line 153
    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/CheckSucesso;->getPaddingLeft()I

    move-result v0

    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/CheckSucesso;->getWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    int-to-float v0, v0

    iput v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->x:F

    .line 154
    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/CheckSucesso;->getPaddingTop()I

    move-result v0

    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/CheckSucesso;->getHeight()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    int-to-float v0, v0

    iput v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->y:F

    .line 155
    const/high16 v0, 0x41700000    # 15.0f

    invoke-direct {p0, v0}, Lcom/itau/empresas/ui/view/CheckSucesso;->convert(F)F

    move-result v0

    iput v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->hookStepY:F

    .line 156
    const/high16 v0, 0x41000000    # 8.0f

    invoke-direct {p0, v0}, Lcom/itau/empresas/ui/view/CheckSucesso;->convert(F)F

    move-result v0

    iput v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->littleStep:F

    .line 157
    const/high16 v0, 0x40a00000    # 5.0f

    invoke-direct {p0, v0}, Lcom/itau/empresas/ui/view/CheckSucesso;->convert(F)F

    move-result v0

    iput v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->smallRadius:F

    .line 158
    const/high16 v0, 0x41a00000    # 20.0f

    invoke-direct {p0, v0}, Lcom/itau/empresas/ui/view/CheckSucesso;->convert(F)F

    move-result v0

    iput v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->arcWidth:F

    .line 159
    const/high16 v0, 0x41200000    # 10.0f

    invoke-direct {p0, v0}, Lcom/itau/empresas/ui/view/CheckSucesso;->convert(F)F

    move-result v0

    iput v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->arrowWidth:F

    .line 160
    const/high16 v0, 0x41200000    # 10.0f

    invoke-direct {p0, v0}, Lcom/itau/empresas/ui/view/CheckSucesso;->convert(F)F

    move-result v0

    iput v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->loadingWidth:F

    .line 161
    iget v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->radio:F

    const/high16 v1, 0x40400000    # 3.0f

    mul-float/2addr v0, v1

    const/high16 v1, 0x40800000    # 4.0f

    div-float/2addr v0, v1

    iput v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->lengthX:F

    .line 162
    iget v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->radio:F

    const/high16 v1, 0x40400000    # 3.0f

    mul-float/2addr v0, v1

    const/high16 v1, 0x40800000    # 4.0f

    div-float/2addr v0, v1

    iput v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->lengthY:F

    .line 164
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->checkedPath:Landroid/graphics/Path;

    .line 165
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->oval:Landroid/graphics/RectF;

    .line 166
    iget-object v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->oval:Landroid/graphics/RectF;

    iget v1, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->x:F

    iget v2, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->radio:F

    sub-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->left:F

    .line 167
    iget-object v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->oval:Landroid/graphics/RectF;

    iget v1, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->y:F

    iget v2, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->radio:F

    sub-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->top:F

    .line 168
    iget-object v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->oval:Landroid/graphics/RectF;

    iget v1, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->x:F

    iget v2, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->radio:F

    add-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->right:F

    .line 169
    iget-object v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->oval:Landroid/graphics/RectF;

    iget v1, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->y:F

    iget v2, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->radio:F

    add-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->bottom:F

    .line 170
    iget v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->radio:F

    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr v0, v1

    iput v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->length:F

    .line 171
    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/CheckSucesso;->initializePaints()V

    .line 172
    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/CheckSucesso;->initializePoints()V

    .line 173
    return-void
.end method

.method private loading(Landroid/graphics/Canvas;)V
    .registers 4
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .line 245
    sget v0, Lcom/itau/empresas/ui/view/CheckSucesso;->GREEN:I

    invoke-direct {p0, p1, v0}, Lcom/itau/empresas/ui/view/CheckSucesso;->desenhaCheckInicial(Landroid/graphics/Canvas;I)V

    .line 246
    invoke-direct {p0, p1}, Lcom/itau/empresas/ui/view/CheckSucesso;->desenhaCirculoInicial(Landroid/graphics/Canvas;)V

    .line 247
    const-wide/16 v0, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/itau/empresas/ui/view/CheckSucesso;->postInvalidateDelayed(J)V

    .line 248
    return-void
.end method

.method private measure(IZ)I
    .registers 9
    .param p1, "measureSpec"    # I
    .param p2, "isWidth"    # Z

    .line 109
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v3

    .line 110
    .local v3, "mode":I
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v4

    .line 111
    .local v4, "size":I
    if-eqz p2, :cond_15

    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/CheckSucesso;->getPaddingLeft()I

    move-result v0

    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/CheckSucesso;->getPaddingRight()I

    move-result v1

    add-int v5, v0, v1

    goto :goto_1f

    :cond_15
    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/CheckSucesso;->getPaddingTop()I

    move-result v0

    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/CheckSucesso;->getPaddingBottom()I

    move-result v1

    add-int v5, v0, v1

    .line 112
    .local v5, "padding":I
    :goto_1f
    const/high16 v0, 0x40000000    # 2.0f

    if-ne v3, v0, :cond_25

    .line 113
    move v2, v4

    .local v2, "result":I
    goto :goto_40

    .line 115
    .end local v2    # "result":I
    :cond_25
    if-eqz p2, :cond_2c

    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/CheckSucesso;->getSuggestedMinimumWidth()I

    move-result v2

    goto :goto_30

    :cond_2c
    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/CheckSucesso;->getSuggestedMinimumHeight()I

    move-result v2

    .line 116
    .local v2, "result":I
    :goto_30
    add-int/2addr v2, v5

    .line 117
    const/high16 v0, -0x80000000

    if-ne v3, v0, :cond_40

    .line 118
    if-eqz p2, :cond_3c

    .line 119
    invoke-static {v2, v4}, Ljava/lang/Math;->max(II)I

    move-result v2

    goto :goto_40

    .line 121
    :cond_3c
    invoke-static {v2, v4}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 125
    :cond_40
    :goto_40
    return v2
.end method

.method private notificaListenerFim()V
    .registers 2

    .line 282
    iget-object v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->listenerAnimacao:Lcom/itau/empresas/ui/view/CheckSucesso$ListenerAnimacao;

    if-eqz v0, :cond_9

    .line 283
    iget-object v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->listenerAnimacao:Lcom/itau/empresas/ui/view/CheckSucesso$ListenerAnimacao;

    invoke-interface {v0}, Lcom/itau/empresas/ui/view/CheckSucesso$ListenerAnimacao;->finish()V

    .line 285
    :cond_9
    return-void
.end method


# virtual methods
.method public animating()V
    .registers 3

    .line 225
    iget v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->count:I

    const/16 v1, 0xc

    if-ge v0, v1, :cond_12

    .line 226
    iget v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->count:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->count:I

    .line 227
    const-wide/16 v0, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/itau/empresas/ui/view/CheckSucesso;->postInvalidateDelayed(J)V

    goto :goto_25

    .line 229
    :cond_12
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->isAnimating:Z

    .line 230
    iget v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->progresso:I

    const/16 v1, 0x64

    if-eq v0, v1, :cond_1f

    .line 231
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->isLoading:Z

    goto :goto_25

    .line 233
    :cond_1f
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->isLoading:Z

    .line 234
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->isCompleted:Z

    .line 237
    :goto_25
    return-void
.end method

.method protected iniciaDesenho(Landroid/graphics/Canvas;)V
    .registers 3
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .line 264
    iget-boolean v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->isEnd:Z

    if-eqz v0, :cond_9

    .line 265
    const/4 v0, -0x1

    invoke-direct {p0, p1, v0}, Lcom/itau/empresas/ui/view/CheckSucesso;->desenhaCheck(Landroid/graphics/Canvas;I)V

    goto :goto_e

    .line 267
    :cond_9
    iget-object v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->checkedPath:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    .line 269
    :goto_e
    return-void
.end method

.method protected initializePaints()V
    .registers 3

    .line 366
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->circuloPaint:Landroid/graphics/Paint;

    .line 367
    iget-object v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->circuloPaint:Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 368
    iget-object v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->circuloPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 369
    iget-object v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->circuloPaint:Landroid/graphics/Paint;

    iget v1, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->arcWidth:F

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 370
    iget-object v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->circuloPaint:Landroid/graphics/Paint;

    sget v1, Lcom/itau/empresas/ui/view/CheckSucesso;->WHITE:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 372
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->checkedPaint:Landroid/graphics/Paint;

    .line 373
    iget-object v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->checkedPaint:Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 374
    iget-object v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->checkedPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 375
    iget-object v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->checkedPaint:Landroid/graphics/Paint;

    iget v1, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->arrowWidth:F

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 376
    iget-object v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->checkedPaint:Landroid/graphics/Paint;

    sget v1, Lcom/itau/empresas/ui/view/CheckSucesso;->GREEN:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 378
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->smallPaint:Landroid/graphics/Paint;

    .line 379
    iget-object v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->smallPaint:Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 380
    iget-object v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->smallPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 381
    iget-object v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->smallPaint:Landroid/graphics/Paint;

    sget v1, Lcom/itau/empresas/ui/view/CheckSucesso;->GREEN:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 383
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->loadingPaint:Landroid/graphics/Paint;

    .line 384
    iget-object v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->loadingPaint:Landroid/graphics/Paint;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 385
    iget-object v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->loadingPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 386
    iget-object v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->loadingPaint:Landroid/graphics/Paint;

    iget v1, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->loadingWidth:F

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 387
    iget-object v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->loadingPaint:Landroid/graphics/Paint;

    sget v1, Lcom/itau/empresas/ui/view/CheckSucesso;->GREEN:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 388
    return-void
.end method

.method protected initializePoints()V
    .registers 6

    .line 391
    new-instance v0, Lcom/itau/empresas/ui/view/CheckSucesso$Point;

    iget v1, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->x:F

    iget v2, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->y:F

    iget v3, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->radio:F

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    add-float/2addr v2, v3

    invoke-direct {v0, v1, v2}, Lcom/itau/empresas/ui/view/CheckSucesso$Point;-><init>(FF)V

    iput-object v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->a:Lcom/itau/empresas/ui/view/CheckSucesso$Point;

    .line 392
    new-instance v0, Lcom/itau/empresas/ui/view/CheckSucesso$Point;

    iget v1, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->x:F

    iget v2, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->y:F

    iget v3, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->radio:F

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    sub-float/2addr v2, v3

    invoke-direct {v0, v1, v2}, Lcom/itau/empresas/ui/view/CheckSucesso$Point;-><init>(FF)V

    iput-object v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->b:Lcom/itau/empresas/ui/view/CheckSucesso$Point;

    .line 393
    new-instance v0, Lcom/itau/empresas/ui/view/CheckSucesso$Point;

    iget v1, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->x:F

    iget v2, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->radio:F

    const/high16 v3, 0x40800000    # 4.0f

    div-float/2addr v2, v3

    sub-float/2addr v1, v2

    iget v2, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->y:F

    iget v3, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->radio:F

    const/high16 v4, 0x40800000    # 4.0f

    div-float/2addr v3, v4

    add-float/2addr v2, v3

    invoke-direct {v0, v1, v2}, Lcom/itau/empresas/ui/view/CheckSucesso$Point;-><init>(FF)V

    iput-object v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->c:Lcom/itau/empresas/ui/view/CheckSucesso$Point;

    .line 394
    new-instance v0, Lcom/itau/empresas/ui/view/CheckSucesso$Point;

    iget v1, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->x:F

    iget v2, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->radio:F

    const/high16 v3, 0x40800000    # 4.0f

    div-float/2addr v2, v3

    add-float/2addr v1, v2

    iget v2, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->y:F

    iget v3, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->radio:F

    const/high16 v4, 0x40800000    # 4.0f

    div-float/2addr v3, v4

    add-float/2addr v2, v3

    invoke-direct {v0, v1, v2}, Lcom/itau/empresas/ui/view/CheckSucesso$Point;-><init>(FF)V

    iput-object v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->d:Lcom/itau/empresas/ui/view/CheckSucesso$Point;

    .line 395
    new-instance v0, Lcom/itau/empresas/ui/view/CheckSucesso$Point;

    iget v1, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->x:F

    iget v2, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->y:F

    iget v3, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->radio:F

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    add-float/2addr v2, v3

    invoke-direct {v0, v1, v2}, Lcom/itau/empresas/ui/view/CheckSucesso$Point;-><init>(FF)V

    iput-object v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->e:Lcom/itau/empresas/ui/view/CheckSucesso$Point;

    .line 396
    new-instance v0, Lcom/itau/empresas/ui/view/CheckSucesso$Point;

    invoke-direct {v0}, Lcom/itau/empresas/ui/view/CheckSucesso$Point;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->jumpPoint:Lcom/itau/empresas/ui/view/CheckSucesso$Point;

    .line 397
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .registers 6
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .line 130
    iget-boolean v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->isFirst:Z

    if-eqz v0, :cond_a

    .line 131
    invoke-direct {p0}, Lcom/itau/empresas/ui/view/CheckSucesso;->init()V

    .line 132
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->isFirst:Z

    .line 134
    :cond_a
    iget v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->x:F

    iget v1, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->y:F

    iget v2, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->radio:F

    iget-object v3, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->circuloPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 135
    invoke-virtual {p0, p1}, Lcom/itau/empresas/ui/view/CheckSucesso;->iniciaDesenho(Landroid/graphics/Canvas;)V

    .line 137
    iget-boolean v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->isAnimating:Z

    if-eqz v0, :cond_1f

    .line 138
    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/CheckSucesso;->animating()V

    .line 141
    :cond_1f
    iget-boolean v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->isLoading:Z

    if-eqz v0, :cond_26

    .line 142
    invoke-direct {p0, p1}, Lcom/itau/empresas/ui/view/CheckSucesso;->loading(Landroid/graphics/Canvas;)V

    .line 144
    :cond_26
    iget-boolean v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->isCompleted:Z

    if-eqz v0, :cond_2d

    .line 145
    invoke-direct {p0, p1}, Lcom/itau/empresas/ui/view/CheckSucesso;->afterCompleted(Landroid/graphics/Canvas;)V

    .line 147
    :cond_2d
    return-void
.end method

.method protected onMeasure(II)V
    .registers 5
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .line 104
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/itau/empresas/ui/view/CheckSucesso;->measure(IZ)I

    move-result v0

    const/4 v1, 0x0

    invoke-direct {p0, p2, v1}, Lcom/itau/empresas/ui/view/CheckSucesso;->measure(IZ)I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/itau/empresas/ui/view/CheckSucesso;->setMeasuredDimension(II)V

    .line 105
    return-void
.end method

.method public reset()V
    .registers 5

    .line 200
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->isAnimating:Z

    .line 201
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->isLoading:Z

    .line 202
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->isCompleted:Z

    .line 203
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->isEnd:Z

    .line 204
    iget v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->radio:F

    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr v0, v1

    iput v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->length:F

    .line 205
    const/4 v0, 0x0

    iput v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->count:I

    .line 206
    const/4 v0, 0x0

    iput v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->hookCount:I

    .line 207
    iget-object v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->jumpPoint:Lcom/itau/empresas/ui/view/CheckSucesso$Point;

    const/high16 v1, -0x40800000    # -1.0f

    # setter for: Lcom/itau/empresas/ui/view/CheckSucesso$Point;->x:F
    invoke-static {v0, v1}, Lcom/itau/empresas/ui/view/CheckSucesso$Point;->access$102(Lcom/itau/empresas/ui/view/CheckSucesso$Point;F)F

    .line 208
    const/4 v0, 0x0

    iput v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->progresso:I

    .line 209
    iget v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->radio:F

    const/high16 v1, 0x40400000    # 3.0f

    mul-float/2addr v0, v1

    const/high16 v1, 0x40800000    # 4.0f

    div-float/2addr v0, v1

    iput v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->lengthX:F

    .line 210
    iget v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->radio:F

    const/high16 v1, 0x40400000    # 3.0f

    mul-float/2addr v0, v1

    const/high16 v1, 0x40800000    # 4.0f

    div-float/2addr v0, v1

    iput v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->lengthY:F

    .line 211
    iget-object v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->a:Lcom/itau/empresas/ui/view/CheckSucesso$Point;

    iget v1, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->y:F

    iget v2, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->length:F

    add-float/2addr v1, v2

    # setter for: Lcom/itau/empresas/ui/view/CheckSucesso$Point;->y:F
    invoke-static {v0, v1}, Lcom/itau/empresas/ui/view/CheckSucesso$Point;->access$202(Lcom/itau/empresas/ui/view/CheckSucesso$Point;F)F

    .line 212
    iget-object v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->b:Lcom/itau/empresas/ui/view/CheckSucesso$Point;

    iget v1, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->y:F

    iget v2, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->length:F

    sub-float/2addr v1, v2

    # setter for: Lcom/itau/empresas/ui/view/CheckSucesso$Point;->y:F
    invoke-static {v0, v1}, Lcom/itau/empresas/ui/view/CheckSucesso$Point;->access$202(Lcom/itau/empresas/ui/view/CheckSucesso$Point;F)F

    .line 213
    iget-object v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->e:Lcom/itau/empresas/ui/view/CheckSucesso$Point;

    iget v1, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->y:F

    iget v2, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->length:F

    add-float/2addr v1, v2

    # setter for: Lcom/itau/empresas/ui/view/CheckSucesso$Point;->y:F
    invoke-static {v0, v1}, Lcom/itau/empresas/ui/view/CheckSucesso$Point;->access$202(Lcom/itau/empresas/ui/view/CheckSucesso$Point;F)F

    .line 214
    iget-object v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->c:Lcom/itau/empresas/ui/view/CheckSucesso$Point;

    iget v1, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->x:F

    iget v2, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->length:F

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v2, v3

    sub-float/2addr v1, v2

    # setter for: Lcom/itau/empresas/ui/view/CheckSucesso$Point;->x:F
    invoke-static {v0, v1}, Lcom/itau/empresas/ui/view/CheckSucesso$Point;->access$102(Lcom/itau/empresas/ui/view/CheckSucesso$Point;F)F

    .line 215
    iget-object v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->c:Lcom/itau/empresas/ui/view/CheckSucesso$Point;

    iget v1, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->y:F

    iget v2, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->length:F

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v2, v3

    add-float/2addr v1, v2

    # setter for: Lcom/itau/empresas/ui/view/CheckSucesso$Point;->y:F
    invoke-static {v0, v1}, Lcom/itau/empresas/ui/view/CheckSucesso$Point;->access$202(Lcom/itau/empresas/ui/view/CheckSucesso$Point;F)F

    .line 216
    iget-object v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->d:Lcom/itau/empresas/ui/view/CheckSucesso$Point;

    iget v1, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->x:F

    iget v2, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->length:F

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v2, v3

    add-float/2addr v1, v2

    # setter for: Lcom/itau/empresas/ui/view/CheckSucesso$Point;->x:F
    invoke-static {v0, v1}, Lcom/itau/empresas/ui/view/CheckSucesso$Point;->access$102(Lcom/itau/empresas/ui/view/CheckSucesso$Point;F)F

    .line 217
    iget-object v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->d:Lcom/itau/empresas/ui/view/CheckSucesso$Point;

    iget v1, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->y:F

    iget v2, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->length:F

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v2, v3

    add-float/2addr v1, v2

    # setter for: Lcom/itau/empresas/ui/view/CheckSucesso$Point;->y:F
    invoke-static {v0, v1}, Lcom/itau/empresas/ui/view/CheckSucesso$Point;->access$202(Lcom/itau/empresas/ui/view/CheckSucesso$Point;F)F

    .line 218
    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/CheckSucesso;->invalidate()V

    .line 219
    return-void
.end method

.method public setListenerAnimacao(Lcom/itau/empresas/ui/view/CheckSucesso$ListenerAnimacao;)V
    .registers 2
    .param p1, "listenerAnimacao"    # Lcom/itau/empresas/ui/view/CheckSucesso$ListenerAnimacao;

    .line 99
    iput-object p1, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->listenerAnimacao:Lcom/itau/empresas/ui/view/CheckSucesso$ListenerAnimacao;

    .line 100
    return-void
.end method

.method public setProgress(I)V
    .registers 3
    .param p1, "progress"    # I

    .line 86
    const/16 v0, 0x64

    if-le p1, v0, :cond_9

    .line 87
    const/16 v0, 0x64

    iput v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->progresso:I

    goto :goto_b

    .line 89
    :cond_9
    iput p1, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->progresso:I

    .line 92
    :goto_b
    const/16 v0, 0x64

    if-ne p1, v0, :cond_15

    .line 93
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->isLoading:Z

    .line 94
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->isCompleted:Z

    .line 96
    :cond_15
    return-void
.end method

.method public startAnimating()V
    .registers 8

    .line 179
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->isAnimating:Z

    .line 180
    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/CheckSucesso;->invalidate()V

    .line 181
    iget v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->count:I

    rem-int/lit8 v0, v0, 0x2

    if-nez v0, :cond_1f

    .line 182
    new-instance v6, Ljava/util/Timer;

    invoke-direct {v6}, Ljava/util/Timer;-><init>()V

    .line 183
    .local v6, "timer":Ljava/util/Timer;
    move-object v0, v6

    new-instance v1, Lcom/itau/empresas/ui/view/CheckSucesso$1;

    invoke-direct {v1, p0}, Lcom/itau/empresas/ui/view/CheckSucesso$1;-><init>(Lcom/itau/empresas/ui/view/CheckSucesso;)V

    const-wide/16 v2, 0x320

    const-wide/16 v4, 0xa

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V

    .line 190
    .end local v6    # "timer":Ljava/util/Timer;
    goto :goto_22

    .line 191
    :cond_1f
    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/CheckSucesso;->reset()V

    .line 193
    :goto_22
    iget v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->count:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso;->count:I

    .line 194
    return-void
.end method

.class public final Lbr/com/itau/sdk/android/core/BackendClient;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lbr/com/itau/sdk/android/core/BackendClient$a;,
        Lbr/com/itau/sdk/android/core/BackendClient$Builder;
    }
.end annotation


# static fields
.field private static final a:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private static b:Lbr/com/itau/sdk/android/core/BackendClient;

.field private static final i:[S

.field private static j:I


# instance fields
.field private final c:Lbr/com/itau/sdk/android/core/BackendClient$a;

.field private d:Lbr/com/itau/sdk/android/core/type/BackendCall;

.field private e:Lbr/com/itau/sdk/android/core/token/TokenAppByPassValidation;

.field private f:Lbr/com/itau/sdk/android/core/SDKTracker;

.field private g:Ljava/lang/Boolean;

.field private h:Lbr/com/itau/sdk/android/core/SDKCustomUi;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .line 55
    const/16 v0, 0x121

    new-array v0, v0, [S

    fill-array-data v0, :array_16

    sput-object v0, Lbr/com/itau/sdk/android/core/BackendClient;->i:[S

    const/16 v0, 0xd3

    sput v0, Lbr/com/itau/sdk/android/core/BackendClient;->j:I

    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    sput-object v0, Lbr/com/itau/sdk/android/core/BackendClient;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-void

    :array_16
    .array-data 2
        0x7bs
        0x3cs
        -0x32s
        -0x48s
        0x8s
        0x18s
        0x24s
        0x13s
        0x5s
        0x23s
        0xbs
        0x14s
        -0x1ds
        0x5bs
        0x10s
        0x1cs
        0x5s
        0x20s
        0x19s
        0xas
        0x1es
        0x14s
        0x21s
        0x11s
        0x12s
        0x10s
        0x1fs
        0x9s
        0x25s
        0x16s
        0x15s
        0x1bs
        0xas
        0x13s
        0x23s
        0x8s
        0x17s
        0x27s
        0x9s
        0x10s
        0x18s
        0x3es
        0x14s
        0x17s
        -0x3es
        0x5fs
        0x1bs
        0x11s
        0x21s
        0xbs
        0xes
        0x21s
        0x13s
        0x27s
        0x1s
        -0x2fs
        0x38s
        0x35s
        0x18s
        0x1es
        0x10s
        0x1fs
        0xcs
        -0xbs
        0x3fs
        0x13s
        0x12s
        0x1fs
        0x1cs
        -0x3es
        0x58s
        0x19s
        0x17s
        0x1fs
        0x19s
        0x9s
        -0x2fs
        0x57s
        0x18s
        0x16s
        0x18s
        0x24s
        0x16s
        0xcs
        0x1bs
        0xfs
        -0x31s
        0x5fs
        0x21s
        0x15s
        -0x3ds
        0x5fs
        0x1bs
        0x1bs
        0x17s
        0x3s
        0x23s
        0xbs
        0x18s
        0xabs
        -0x5es
        -0x39s
        0x5es
        0x8fs
        -0xabs
        0x5fs
        0x11s
        0x17s
        0x1fs
        0x1cs
        0xbs
        0x13s
        0x19s
        0x10s
        0x14s
        0x19s
        0x21s
        0x19s
        -0x3cs
        0x59s
        0x14s
        0x19s
        0x13s
        0x28s
        0x17s
        0x14s
        0x5s
        0x19s
        0x21s
        -0x38s
        0x17s
        0x13s
        -0xes
        0x2ds
        0x3fs
        0x19s
        0x15s
        0xds
        -0x2fs
        0x64s
        0x17s
        0x1bs
        -0x3es
        0x69s
        0x18s
        0x11s
        0x16s
        0x15s
        0x19s
        0x18s
        0x7s
        0x15s
        0x17s
        0x13s
        -0xes
        0x2ds
        -0x3s
        0x8s
        0x18s
        0x24s
        0x13s
        0x5s
        0x23s
        0xbs
        0x14s
        -0x1ds
        0x5cs
        0x11s
        0x12s
        0x10s
        0x1fs
        0x9s
        0x25s
        0x16s
        0x1ds
        0x8s
        0x21s
        0xas
        0x1es
        0x7s
        0x28s
        0x16s
        0x1as
        0xes
        0x19s
        0x8s
        -0x1cs
        0x42s
        0x1cs
        0xds
        0xes
        -0x2es
        0x64s
        0x17s
        0x1bs
        -0x3es
        0x5fs
        0x1bs
        0x1bs
        0x17s
        0x3s
        0x23s
        0x1cs
        0xbs
        0xes
        0x29s
        0x7s
        -0x2fs
        0x49s
        0x7s
        0x1ds
        0xes
        0x48s
        0x14s
        0x17s
        0x11s
        0x14s
        -0x2s
        0x2as
        -0x19s
        -0x4s
        0xabs
        -0x5es
        -0x39s
        0x5es
        0xfs
        0x2bs
        0x9s
        0xes
        -0x2bs
        0x68s
        0x9s
        0x22s
        0x1as
        0x6s
        0x24s
        0x17s
        0x15s
        -0x3ds
        0x66s
        0x7s
        0x27s
        0x5s
        -0x2bs
        0x6as
        0x7s
        0x1fs
        0x1cs
        0x3s
        0x27s
        0x8s
        0x18s
        0x24s
        0x13s
        0x5s
        0x23s
        0xbs
        0x14s
        -0x1ds
        0x5cs
        0x11s
        0x12s
        0x10s
        0x1fs
        0x9s
        0x25s
        0x16s
        0xfs
        0x1bs
        0x1bs
        0x17s
        0x3s
        0x21s
        0x16s
        0x19s
        0x1bs
        0xas
        0x13s
        0x23s
        0x8s
        0x17s
        0x27s
        0x9s
        0x10s
        0x18s
        -0x1cs
        0x64s
        -0x38s
        0x64s
    .end array-data
.end method

.method private constructor <init>(Lbr/com/itau/sdk/android/core/BackendClient$Builder;)V
    .registers 4

    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/sdk/android/core/BackendClient;->g:Ljava/lang/Boolean;

    .line 66
    new-instance v0, Lbr/com/itau/sdk/android/core/BackendClient$a;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, Lbr/com/itau/sdk/android/core/BackendClient$a;-><init>(Lbr/com/itau/sdk/android/core/BackendClient$Builder;Lbr/com/itau/sdk/android/core/b;)V

    iput-object v0, p0, Lbr/com/itau/sdk/android/core/BackendClient;->c:Lbr/com/itau/sdk/android/core/BackendClient$a;

    .line 67
    return-void
.end method

.method synthetic constructor <init>(Lbr/com/itau/sdk/android/core/BackendClient$Builder;Lbr/com/itau/sdk/android/core/b;)V
    .registers 3

    .line 53
    invoke-direct {p0, p1}, Lbr/com/itau/sdk/android/core/BackendClient;-><init>(Lbr/com/itau/sdk/android/core/BackendClient$Builder;)V

    return-void
.end method

.method static synthetic a(Lbr/com/itau/sdk/android/core/BackendClient;)Lbr/com/itau/sdk/android/core/BackendClient;
    .registers 1

    .line 53
    sput-object p0, Lbr/com/itau/sdk/android/core/BackendClient;->b:Lbr/com/itau/sdk/android/core/BackendClient;

    return-object p0
.end method

.method private static a(Lcom/google/gson/reflect/TypeToken;Ljava/lang/String;)Ljava/lang/Object;
    .registers 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:Ljava/lang/Object;>(Lcom/google/gson/reflect/TypeToken<TT;>;Ljava/lang/String;)TT;"
        }
    .end annotation

    .line 210
    sget-object v0, Lbr/com/itau/sdk/android/core/n;->b:Lbr/com/itau/sdk/android/core/endpoint/i;

    invoke-static {}, Lbr/com/itau/sdk/android/core/login/b;->a()Lbr/com/itau/sdk/android/core/login/b;

    move-result-object v1

    invoke-virtual {v1, p1}, Lbr/com/itau/sdk/android/core/login/b;->a(Ljava/lang/String;)Lcom/google/gson/JsonElement;

    move-result-object v1

    invoke-virtual {v0, v1, p0}, Lbr/com/itau/sdk/android/core/endpoint/i;->a(Lcom/google/gson/JsonElement;Lcom/google/gson/reflect/TypeToken;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method private static a(III)Ljava/lang/String;
    .registers 9

    rsub-int p1, p1, 0x11e

    new-instance v0, Ljava/lang/String;

    add-int/lit8 p0, p0, 0x4

    add-int/lit8 p2, p2, 0x25

    const/4 v4, -0x1

    sget-object v5, Lbr/com/itau/sdk/android/core/BackendClient;->i:[S

    new-array v1, p0, [B

    add-int/lit8 p0, p0, -0x1

    if-nez v5, :cond_18

    move v2, p1

    move v3, p0

    :goto_13
    add-int/2addr v2, v3

    add-int/lit8 p1, p1, 0x1

    add-int/lit8 p2, v2, -0x16

    :cond_18
    add-int/lit8 v4, v4, 0x1

    int-to-byte v2, p2

    aput-byte v2, v1, v4

    if-ne v4, p0, :cond_24

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Ljava/lang/String;-><init>([BI)V

    return-object v0

    :cond_24
    move v2, p2

    aget-short v3, v5, p1

    goto :goto_13
.end method

.method static synthetic a()Ljava/util/concurrent/atomic/AtomicBoolean;
    .registers 1

    .line 53
    sget-object v0, Lbr/com/itau/sdk/android/core/BackendClient;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-object v0
.end method

.method public static api(Ljava/lang/Class;)Ljava/lang/Object;
    .registers 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:Ljava/lang/Object;>(Ljava/lang/Class<TT;>;)TT;"
        }
    .end annotation

    .line 145
    invoke-static {}, Lbr/com/itau/sdk/android/core/BackendClient;->getInstance()Lbr/com/itau/sdk/android/core/BackendClient;

    move-result-object v0

    iget-object v0, v0, Lbr/com/itau/sdk/android/core/BackendClient;->c:Lbr/com/itau/sdk/android/core/BackendClient$a;

    const/4 v1, 0x0

    invoke-static {v0, p0, v1}, Lbr/com/itau/sdk/android/core/BackendClient$a;->a(Lbr/com/itau/sdk/android/core/BackendClient$a;Ljava/lang/Class;Lbr/com/itau/sdk/android/core/endpoint/ErrorHandler;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public static api(Ljava/lang/Class;Lbr/com/itau/sdk/android/core/endpoint/ErrorHandler;)Ljava/lang/Object;
    .registers 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:Ljava/lang/Object;>(Ljava/lang/Class<TT;>;Lbr/com/itau/sdk/android/core/endpoint/ErrorHandler;)TT;"
        }
    .end annotation

    .line 158
    invoke-static {}, Lbr/com/itau/sdk/android/core/BackendClient;->getInstance()Lbr/com/itau/sdk/android/core/BackendClient;

    move-result-object v0

    iget-object v0, v0, Lbr/com/itau/sdk/android/core/BackendClient;->c:Lbr/com/itau/sdk/android/core/BackendClient$a;

    invoke-static {v0, p0, p1}, Lbr/com/itau/sdk/android/core/BackendClient$a;->a(Lbr/com/itau/sdk/android/core/BackendClient$a;Ljava/lang/Class;Lbr/com/itau/sdk/android/core/endpoint/ErrorHandler;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method static synthetic b(Lbr/com/itau/sdk/android/core/BackendClient;)Lbr/com/itau/sdk/android/core/BackendClient$a;
    .registers 2

    .line 53
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/BackendClient;->c:Lbr/com/itau/sdk/android/core/BackendClient$a;

    return-object v0
.end method

.method static synthetic b()Lbr/com/itau/sdk/android/core/BackendClient;
    .registers 1

    .line 53
    sget-object v0, Lbr/com/itau/sdk/android/core/BackendClient;->b:Lbr/com/itau/sdk/android/core/BackendClient;

    return-object v0
.end method

.method public static buildDefaultUserAgent()V
    .registers 5

    .line 278
    invoke-static {}, Lbr/com/itau/sdk/android/core/BackendClient;->getInstance()Lbr/com/itau/sdk/android/core/BackendClient;

    move-result-object v0

    iget-object v0, v0, Lbr/com/itau/sdk/android/core/BackendClient;->c:Lbr/com/itau/sdk/android/core/BackendClient$a;

    invoke-virtual {v0}, Lbr/com/itau/sdk/android/core/BackendClient$a;->getInitUserAgent()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_45

    .line 279
    invoke-static {}, Lbr/com/itau/sdk/android/core/BackendClient;->getInstance()Lbr/com/itau/sdk/android/core/BackendClient;

    move-result-object v0

    iget-object v0, v0, Lbr/com/itau/sdk/android/core/BackendClient;->c:Lbr/com/itau/sdk/android/core/BackendClient$a;

    sget-object v1, Lbr/com/itau/sdk/android/core/BackendClient;->i:[S

    const/16 v2, 0x36

    aget-short v1, v1, v2

    add-int/lit8 v1, v1, -0x1

    invoke-static {v1, v1, v1}, Lbr/com/itau/sdk/android/core/BackendClient;->a(III)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    .line 280
    invoke-static {}, Lbr/com/itau/sdk/android/core/BackendClient;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lbr/com/itau/sdk/android/core/n;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    aput-object v3, v2, v4

    .line 281
    invoke-static {}, Lbr/com/itau/sdk/android/core/BackendClient;->getInstance()Lbr/com/itau/sdk/android/core/BackendClient;

    move-result-object v3

    iget-object v3, v3, Lbr/com/itau/sdk/android/core/BackendClient;->c:Lbr/com/itau/sdk/android/core/BackendClient$a;

    invoke-virtual {v3}, Lbr/com/itau/sdk/android/core/BackendClient$a;->getInitUserAgent()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    aput-object v3, v2, v4

    .line 280
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 279
    invoke-static {v0, v1}, Lbr/com/itau/sdk/android/core/BackendClient$a;->b(Lbr/com/itau/sdk/android/core/BackendClient$a;Ljava/lang/String;)Ljava/lang/String;

    goto :goto_56

    .line 283
    :cond_45
    invoke-static {}, Lbr/com/itau/sdk/android/core/BackendClient;->getInstance()Lbr/com/itau/sdk/android/core/BackendClient;

    move-result-object v0

    iget-object v0, v0, Lbr/com/itau/sdk/android/core/BackendClient;->c:Lbr/com/itau/sdk/android/core/BackendClient$a;

    invoke-static {}, Lbr/com/itau/sdk/android/core/BackendClient;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lbr/com/itau/sdk/android/core/n;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbr/com/itau/sdk/android/core/BackendClient$a;->b(Lbr/com/itau/sdk/android/core/BackendClient$a;Ljava/lang/String;)Ljava/lang/String;

    .line 285
    :goto_56
    return-void
.end method

.method private c()V
    .registers 9

    .line 74
    invoke-static {}, Lbr/com/itau/sdk/android/core/m;->a()Ljava/lang/Class;

    move-result-object v6

    .line 77
    :try_start_4
    invoke-virtual {v6}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbr/com/itau/sdk/android/core/SDKCustomUi;

    iput-object v0, p0, Lbr/com/itau/sdk/android/core/BackendClient;->h:Lbr/com/itau/sdk/android/core/SDKCustomUi;
    :try_end_c
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_c} :catch_d

    .line 80
    goto :goto_41

    .line 78
    :catch_d
    move-exception v7

    .line 79
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lbr/com/itau/sdk/android/core/BackendClient;->i:[S

    const/16 v3, 0x1a

    aget-short v2, v2, v3

    sget-object v3, Lbr/com/itau/sdk/android/core/BackendClient;->i:[S

    const/16 v4, 0x8b

    aget-short v3, v3, v4

    add-int/lit8 v3, v3, -0x1

    sget-object v4, Lbr/com/itau/sdk/android/core/BackendClient;->i:[S

    const/16 v5, 0x14

    aget-short v4, v4, v5

    invoke-static {v2, v3, v4}, Lbr/com/itau/sdk/android/core/BackendClient;->a(III)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v7}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 81
    :goto_41
    return-void
.end method

.method public static callApi(Lbr/com/itau/sdk/android/core/type/BackendCall;)V
    .registers 2

    .line 162
    invoke-static {}, Lbr/com/itau/sdk/android/core/BackendClient;->getInstance()Lbr/com/itau/sdk/android/core/BackendClient;

    move-result-object v0

    iget-object v0, v0, Lbr/com/itau/sdk/android/core/BackendClient;->c:Lbr/com/itau/sdk/android/core/BackendClient$a;

    invoke-static {v0}, Lbr/com/itau/sdk/android/core/BackendClient$a;->h(Lbr/com/itau/sdk/android/core/BackendClient$a;)Lde/greenrobot/event/EventBus;

    move-result-object v0

    invoke-virtual {v0, p0}, Lde/greenrobot/event/EventBus;->post(Ljava/lang/Object;)V

    .line 163
    return-void
.end method

.method public static cancelTokenApp(Lcom/google/gson/reflect/TypeToken;)Ljava/lang/Object;
    .registers 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:Ljava/lang/Object;>(Lcom/google/gson/reflect/TypeToken<TT;>;)TT;"
        }
    .end annotation

    .line 195
    sget-object v0, Lbr/com/itau/sdk/android/core/n;->b:Lbr/com/itau/sdk/android/core/endpoint/i;

    invoke-static {}, Lbr/com/itau/sdk/android/core/token/c;->d()Lbr/com/itau/sdk/android/core/token/c;

    move-result-object v1

    invoke-virtual {v1}, Lbr/com/itau/sdk/android/core/token/c;->c()Lcom/google/gson/JsonElement;

    move-result-object v1

    invoke-virtual {v0, v1, p0}, Lbr/com/itau/sdk/android/core/endpoint/i;->a(Lcom/google/gson/JsonElement;Lcom/google/gson/reflect/TypeToken;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public static cancelTokenApp(Ljava/lang/Class;)Ljava/lang/Object;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:Ljava/lang/Object;>(Ljava/lang/Class<TT;>;)TT;"
        }
    .end annotation

    .line 178
    invoke-static {p0}, Lcom/google/gson/reflect/TypeToken;->get(Ljava/lang/Class;)Lcom/google/gson/reflect/TypeToken;

    move-result-object v0

    invoke-static {v0}, Lbr/com/itau/sdk/android/core/BackendClient;->cancelTokenApp(Lcom/google/gson/reflect/TypeToken;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public static checkTokenApp(Lcom/google/gson/reflect/TypeToken;)Ljava/lang/Object;
    .registers 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:Ljava/lang/Object;>(Lcom/google/gson/reflect/TypeToken<TT;>;)TT;"
        }
    .end annotation

    .line 182
    sget-object v0, Lbr/com/itau/sdk/android/core/n;->b:Lbr/com/itau/sdk/android/core/endpoint/i;

    invoke-static {}, Lbr/com/itau/sdk/android/core/token/c;->d()Lbr/com/itau/sdk/android/core/token/c;

    move-result-object v1

    invoke-virtual {v1}, Lbr/com/itau/sdk/android/core/token/c;->a()Lcom/google/gson/JsonElement;

    move-result-object v1

    invoke-virtual {v0, v1, p0}, Lbr/com/itau/sdk/android/core/endpoint/i;->a(Lcom/google/gson/JsonElement;Lcom/google/gson/reflect/TypeToken;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public static checkTokenApp(Ljava/lang/Class;)Ljava/lang/Object;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:Ljava/lang/Object;>(Ljava/lang/Class<TT;>;)TT;"
        }
    .end annotation

    .line 166
    invoke-static {p0}, Lcom/google/gson/reflect/TypeToken;->get(Ljava/lang/Class;)Lcom/google/gson/reflect/TypeToken;

    move-result-object v0

    invoke-static {v0}, Lbr/com/itau/sdk/android/core/BackendClient;->checkTokenApp(Lcom/google/gson/reflect/TypeToken;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public static currentActivity()Landroid/app/Activity;
    .registers 1

    .line 70
    invoke-static {}, Lbr/com/itau/sdk/android/core/BackendClient;->getInstance()Lbr/com/itau/sdk/android/core/BackendClient;

    move-result-object v0

    iget-object v0, v0, Lbr/com/itau/sdk/android/core/BackendClient;->c:Lbr/com/itau/sdk/android/core/BackendClient$a;

    invoke-static {v0}, Lbr/com/itau/sdk/android/core/BackendClient$a;->a(Lbr/com/itau/sdk/android/core/BackendClient$a;)Landroid/app/Activity;

    move-result-object v0

    return-object v0
.end method

.method private d()V
    .registers 2

    .line 385
    invoke-static {}, Lbr/com/itau/sdk/android/core/BackendClient;->getFlowManager()Lbr/com/itau/sdk/android/core/FlowManager;

    move-result-object v0

    invoke-virtual {v0}, Lbr/com/itau/sdk/android/core/FlowManager;->c()V

    .line 386
    return-void
.end method

.method private e()V
    .registers 3

    .line 390
    :try_start_0
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/BackendClient;->d:Lbr/com/itau/sdk/android/core/type/BackendCall;

    invoke-interface {v0}, Lbr/com/itau/sdk/android/core/type/BackendCall;->callBackend()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_5} :catch_6

    .line 392
    goto :goto_7

    .line 391
    :catch_6
    move-exception v1

    .line 393
    :goto_7
    return-void
.end method

.method public static getContext()Landroid/content/Context;
    .registers 1

    .line 96
    invoke-static {}, Lbr/com/itau/sdk/android/core/BackendClient;->getInstance()Lbr/com/itau/sdk/android/core/BackendClient;

    move-result-object v0

    iget-object v0, v0, Lbr/com/itau/sdk/android/core/BackendClient;->c:Lbr/com/itau/sdk/android/core/BackendClient$a;

    invoke-static {v0}, Lbr/com/itau/sdk/android/core/BackendClient$a;->e(Lbr/com/itau/sdk/android/core/BackendClient$a;)Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method public static getFlowManager()Lbr/com/itau/sdk/android/core/FlowManager;
    .registers 1

    .line 118
    invoke-static {}, Lbr/com/itau/sdk/android/core/BackendClient;->getInstance()Lbr/com/itau/sdk/android/core/BackendClient;

    move-result-object v0

    iget-object v0, v0, Lbr/com/itau/sdk/android/core/BackendClient;->c:Lbr/com/itau/sdk/android/core/BackendClient$a;

    invoke-static {v0}, Lbr/com/itau/sdk/android/core/BackendClient$a;->f(Lbr/com/itau/sdk/android/core/BackendClient$a;)Lbr/com/itau/sdk/android/core/FlowManager;

    move-result-object v0

    return-object v0
.end method

.method public static getInstance()Lbr/com/itau/sdk/android/core/BackendClient;
    .registers 4

    .line 106
    sget-object v0, Lbr/com/itau/sdk/android/core/BackendClient;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 107
    sget-object v0, Lbr/com/itau/sdk/android/core/BackendClient;->b:Lbr/com/itau/sdk/android/core/BackendClient;

    return-object v0

    .line 109
    :cond_b
    new-instance v0, Ljava/lang/IllegalStateException;

    sget-object v1, Lbr/com/itau/sdk/android/core/BackendClient;->i:[S

    const/16 v2, 0x38

    aget-short v1, v1, v2

    add-int/lit8 v1, v1, -0x1

    sget-object v2, Lbr/com/itau/sdk/android/core/BackendClient;->i:[S

    const/16 v3, 0x7b

    aget-short v2, v2, v3

    const/16 v3, 0xf5

    invoke-static {v1, v3, v2}, Lbr/com/itau/sdk/android/core/BackendClient;->a(III)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static getOfflineOTP()Lbr/com/itau/security/token/model/OTPModel;
    .registers 2

    .line 232
    :try_start_0
    invoke-static {}, Lbr/com/itau/sdk/android/core/BackendClient;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lbr/com/itau/security/token/til/Token;->getOfflineOTP(Landroid/content/Context;)Lbr/com/itau/security/token/model/OTPModel;
    :try_end_7
    .catch Lbr/com/itau/security/token/exception/UnavailableTokenException; {:try_start_0 .. :try_end_7} :catch_9

    move-result-object v0

    return-object v0

    .line 233
    :catch_9
    move-exception v1

    .line 234
    const/4 v0, 0x0

    return-object v0
.end method

.method public static getStatusKeyAccess()Lbr/com/itau/sdk/android/core/login/model/RouterResponseKeyAccessDTO;
    .registers 1

    .line 223
    invoke-static {}, Lbr/com/itau/sdk/android/core/login/b;->a()Lbr/com/itau/sdk/android/core/login/b;

    move-result-object v0

    invoke-virtual {v0}, Lbr/com/itau/sdk/android/core/login/b;->d()Lbr/com/itau/sdk/android/core/login/model/RouterResponseKeyAccessDTO;

    move-result-object v0

    return-object v0
.end method

.method public static getUserAgent()Ljava/lang/String;
    .registers 1

    .line 274
    invoke-static {}, Lbr/com/itau/sdk/android/core/BackendClient;->getInstance()Lbr/com/itau/sdk/android/core/BackendClient;

    move-result-object v0

    iget-object v0, v0, Lbr/com/itau/sdk/android/core/BackendClient;->c:Lbr/com/itau/sdk/android/core/BackendClient$a;

    invoke-static {v0}, Lbr/com/itau/sdk/android/core/BackendClient$a;->i(Lbr/com/itau/sdk/android/core/BackendClient$a;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static hasKeyAccess(Ljava/lang/String;)Z
    .registers 2

    .line 227
    invoke-static {}, Lbr/com/itau/sdk/android/core/login/b;->a()Lbr/com/itau/sdk/android/core/login/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lbr/com/itau/sdk/android/core/login/b;->b(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static installTokenApp(Lcom/google/gson/reflect/TypeToken;)Ljava/lang/Object;
    .registers 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:Ljava/lang/Object;>(Lcom/google/gson/reflect/TypeToken<TT;>;)TT;"
        }
    .end annotation

    .line 186
    sget-object v0, Lbr/com/itau/sdk/android/core/n;->b:Lbr/com/itau/sdk/android/core/endpoint/i;

    invoke-static {}, Lbr/com/itau/sdk/android/core/token/c;->d()Lbr/com/itau/sdk/android/core/token/c;

    move-result-object v1

    invoke-virtual {v1}, Lbr/com/itau/sdk/android/core/token/c;->b()Lcom/google/gson/JsonElement;

    move-result-object v1

    invoke-virtual {v0, v1, p0}, Lbr/com/itau/sdk/android/core/endpoint/i;->a(Lcom/google/gson/JsonElement;Lcom/google/gson/reflect/TypeToken;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public static installTokenApp(Ljava/lang/Class;)Ljava/lang/Object;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:Ljava/lang/Object;>(Ljava/lang/Class<TT;>;)TT;"
        }
    .end annotation

    .line 170
    invoke-static {p0}, Lcom/google/gson/reflect/TypeToken;->get(Ljava/lang/Class;)Lcom/google/gson/reflect/TypeToken;

    move-result-object v0

    invoke-static {v0}, Lbr/com/itau/sdk/android/core/BackendClient;->installTokenApp(Lcom/google/gson/reflect/TypeToken;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public static installTokenAppOtherDevice(Lcom/google/gson/reflect/TypeToken;Ljava/lang/String;)Ljava/lang/Object;
    .registers 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:Ljava/lang/Object;>(Lcom/google/gson/reflect/TypeToken<TT;>;Ljava/lang/String;)TT;"
        }
    .end annotation

    .line 190
    sget-object v0, Lbr/com/itau/sdk/android/core/n;->b:Lbr/com/itau/sdk/android/core/endpoint/i;

    invoke-static {}, Lbr/com/itau/sdk/android/core/token/c;->d()Lbr/com/itau/sdk/android/core/token/c;

    move-result-object v1

    .line 191
    invoke-virtual {v1, p1}, Lbr/com/itau/sdk/android/core/token/c;->b(Ljava/lang/String;)Lcom/google/gson/JsonElement;

    move-result-object v1

    .line 190
    invoke-virtual {v0, v1, p0}, Lbr/com/itau/sdk/android/core/endpoint/i;->a(Lcom/google/gson/JsonElement;Lcom/google/gson/reflect/TypeToken;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public static installTokenAppOtherDevice(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Object;
    .registers 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:Ljava/lang/Object;>(Ljava/lang/Class<TT;>;Ljava/lang/String;)TT;"
        }
    .end annotation

    .line 174
    invoke-static {p0}, Lcom/google/gson/reflect/TypeToken;->get(Ljava/lang/Class;)Lcom/google/gson/reflect/TypeToken;

    move-result-object v0

    invoke-static {v0, p1}, Lbr/com/itau/sdk/android/core/BackendClient;->installTokenAppOtherDevice(Lcom/google/gson/reflect/TypeToken;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public static isValidSession()Z
    .registers 1

    .line 270
    invoke-static {}, Lbr/com/itau/sdk/android/core/o;->a()Lbr/com/itau/sdk/android/core/o;

    move-result-object v0

    invoke-virtual {v0}, Lbr/com/itau/sdk/android/core/o;->d()Z

    move-result v0

    return v0
.end method

.method public static loginKeyAccess(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Object;
    .registers 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:Ljava/lang/Object;>(Ljava/lang/Class<TT;>;Ljava/lang/String;)TT;"
        }
    .end annotation

    .line 199
    invoke-static {p1}, Lbr/com/itau/sdk/android/core/BackendClient;->hasKeyAccess(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1e

    .line 200
    new-instance v0, Lbr/com/itau/sdk/android/core/exception/KeyAccessException;

    sget-object v1, Lbr/com/itau/sdk/android/core/BackendClient;->i:[S

    const/16 v2, 0xf

    aget-short v1, v1, v2

    sget-object v2, Lbr/com/itau/sdk/android/core/BackendClient;->i:[S

    const/16 v3, 0xcd

    aget-short v2, v2, v3

    const/16 v3, 0xbb

    invoke-static {v1, v3, v2}, Lbr/com/itau/sdk/android/core/BackendClient;->a(III)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lbr/com/itau/sdk/android/core/exception/KeyAccessException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 203
    :cond_1e
    sget-object v0, Lbr/com/itau/sdk/android/core/BackendClient;->i:[S

    const/16 v1, 0x36

    aget-short v0, v0, v1

    sget-object v1, Lbr/com/itau/sdk/android/core/BackendClient;->i:[S

    const/16 v2, 0x29

    aget-short v1, v1, v2

    const/16 v2, 0x9c

    invoke-static {v0, v2, v1}, Lbr/com/itau/sdk/android/core/BackendClient;->a(III)Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lbr/com/itau/sdk/android/core/BackendClient;->getInstance()Lbr/com/itau/sdk/android/core/BackendClient;

    move-result-object v1

    iget-object v1, v1, Lbr/com/itau/sdk/android/core/BackendClient;->c:Lbr/com/itau/sdk/android/core/BackendClient$a;

    invoke-static {v1}, Lbr/com/itau/sdk/android/core/BackendClient$a;->i(Lbr/com/itau/sdk/android/core/BackendClient$a;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5c

    .line 204
    invoke-static {}, Lbr/com/itau/sdk/android/core/BackendClient;->getInstance()Lbr/com/itau/sdk/android/core/BackendClient;

    move-result-object v0

    iget-object v0, v0, Lbr/com/itau/sdk/android/core/BackendClient;->c:Lbr/com/itau/sdk/android/core/BackendClient$a;

    sget-object v1, Lbr/com/itau/sdk/android/core/BackendClient;->i:[S

    const/16 v2, 0xd9

    aget-short v1, v1, v2

    neg-int v1, v1

    or-int/lit16 v2, v1, 0x84

    sget-object v3, Lbr/com/itau/sdk/android/core/BackendClient;->i:[S

    const/16 v4, 0x29

    aget-short v3, v3, v4

    invoke-static {v1, v2, v3}, Lbr/com/itau/sdk/android/core/BackendClient;->a(III)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbr/com/itau/sdk/android/core/BackendClient$a;->a(Lbr/com/itau/sdk/android/core/BackendClient$a;Ljava/lang/String;)V

    .line 206
    :cond_5c
    invoke-static {p0}, Lcom/google/gson/reflect/TypeToken;->get(Ljava/lang/Class;)Lcom/google/gson/reflect/TypeToken;

    move-result-object v0

    invoke-static {v0, p1}, Lbr/com/itau/sdk/android/core/BackendClient;->a(Lcom/google/gson/reflect/TypeToken;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public static logout(Z)Z
    .registers 3

    .line 263
    invoke-static {}, Lbr/com/itau/sdk/android/core/c/a;->a()Lbr/com/itau/sdk/android/core/c/a;

    move-result-object v0

    invoke-virtual {v0, p0}, Lbr/com/itau/sdk/android/core/c/a;->a(Z)Z

    move-result v1

    .line 264
    if-eqz v1, :cond_11

    .line 265
    invoke-static {}, Lbr/com/itau/sdk/android/core/BackendClient;->getInstance()Lbr/com/itau/sdk/android/core/BackendClient;

    move-result-object v0

    invoke-virtual {v0}, Lbr/com/itau/sdk/android/core/BackendClient;->reset()V

    .line 266
    :cond_11
    return v1
.end method

.method public static registerKeyAccess()V
    .registers 1

    .line 215
    invoke-static {}, Lbr/com/itau/sdk/android/core/login/b;->a()Lbr/com/itau/sdk/android/core/login/b;

    move-result-object v0

    invoke-virtual {v0}, Lbr/com/itau/sdk/android/core/login/b;->b()V

    .line 216
    return-void
.end method

.method public static registerViewInBus(Landroid/view/View;)V
    .registers 2

    .line 127
    invoke-static {}, Lbr/com/itau/sdk/android/core/BackendClient;->getInstance()Lbr/com/itau/sdk/android/core/BackendClient;

    move-result-object v0

    iget-object v0, v0, Lbr/com/itau/sdk/android/core/BackendClient;->c:Lbr/com/itau/sdk/android/core/BackendClient$a;

    invoke-static {v0, p0}, Lbr/com/itau/sdk/android/core/BackendClient$a;->a(Lbr/com/itau/sdk/android/core/BackendClient$a;Landroid/view/View;)V

    .line 128
    return-void
.end method

.method public static sendFeedbackForApp(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/gson/reflect/TypeToken;)Ljava/lang/Object;
    .registers 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:Ljava/lang/Object;>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/gson/reflect/TypeToken<TT;>;)TT;"
        }
    .end annotation

    .line 253
    sget-object v0, Lbr/com/itau/sdk/android/core/n;->b:Lbr/com/itau/sdk/android/core/endpoint/i;

    .line 254
    invoke-static {}, Lbr/com/itau/sdk/android/core/a/b;->a()Lbr/com/itau/sdk/android/core/a/b;

    move-result-object v1

    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    invoke-virtual/range {v1 .. v6}, Lbr/com/itau/sdk/android/core/a/b;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/gson/JsonElement;

    move-result-object v1

    .line 253
    invoke-virtual {v0, v1, p5}, Lbr/com/itau/sdk/android/core/endpoint/i;->a(Lcom/google/gson/JsonElement;Lcom/google/gson/reflect/TypeToken;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public static sendFeedbackForApp(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;
    .registers 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:Ljava/lang/Object;>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Class<TT;>;)TT;"
        }
    .end annotation

    .line 244
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-static {p5}, Lcom/google/gson/reflect/TypeToken;->get(Ljava/lang/Class;)Lcom/google/gson/reflect/TypeToken;

    move-result-object v5

    invoke-static/range {v0 .. v5}, Lbr/com/itau/sdk/android/core/BackendClient;->sendFeedbackForApp(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/gson/reflect/TypeToken;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public static unregisteViewsInBus()V
    .registers 1

    .line 134
    invoke-static {}, Lbr/com/itau/sdk/android/core/BackendClient;->getInstance()Lbr/com/itau/sdk/android/core/BackendClient;

    move-result-object v0

    iget-object v0, v0, Lbr/com/itau/sdk/android/core/BackendClient;->c:Lbr/com/itau/sdk/android/core/BackendClient$a;

    invoke-static {v0}, Lbr/com/itau/sdk/android/core/BackendClient$a;->g(Lbr/com/itau/sdk/android/core/BackendClient$a;)V

    .line 135
    return-void
.end method

.method public static unregisterKeyAccess()Lbr/com/itau/sdk/android/core/login/model/RouterResponseKeyAccessDTO;
    .registers 1

    .line 219
    invoke-static {}, Lbr/com/itau/sdk/android/core/login/b;->a()Lbr/com/itau/sdk/android/core/login/b;

    move-result-object v0

    invoke-virtual {v0}, Lbr/com/itau/sdk/android/core/login/b;->c()Lbr/com/itau/sdk/android/core/login/model/RouterResponseKeyAccessDTO;

    move-result-object v0

    return-object v0
.end method

.method public static urlMiddleware()Ljava/lang/String;
    .registers 1

    .line 92
    invoke-static {}, Lbr/com/itau/sdk/android/core/BackendClient;->getInstance()Lbr/com/itau/sdk/android/core/BackendClient;

    move-result-object v0

    iget-object v0, v0, Lbr/com/itau/sdk/android/core/BackendClient;->c:Lbr/com/itau/sdk/android/core/BackendClient$a;

    invoke-static {v0}, Lbr/com/itau/sdk/android/core/BackendClient$a;->d(Lbr/com/itau/sdk/android/core/BackendClient$a;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static urlUniversal()Ljava/lang/String;
    .registers 1

    .line 88
    invoke-static {}, Lbr/com/itau/sdk/android/core/BackendClient;->getInstance()Lbr/com/itau/sdk/android/core/BackendClient;

    move-result-object v0

    iget-object v0, v0, Lbr/com/itau/sdk/android/core/BackendClient;->c:Lbr/com/itau/sdk/android/core/BackendClient$a;

    invoke-static {v0}, Lbr/com/itau/sdk/android/core/BackendClient$a;->c(Lbr/com/itau/sdk/android/core/BackendClient$a;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static urlUsabilidade()Ljava/lang/String;
    .registers 1

    .line 84
    invoke-static {}, Lbr/com/itau/sdk/android/core/BackendClient;->getInstance()Lbr/com/itau/sdk/android/core/BackendClient;

    move-result-object v0

    iget-object v0, v0, Lbr/com/itau/sdk/android/core/BackendClient;->c:Lbr/com/itau/sdk/android/core/BackendClient$a;

    invoke-static {v0}, Lbr/com/itau/sdk/android/core/BackendClient$a;->b(Lbr/com/itau/sdk/android/core/BackendClient$a;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public call(Lbr/com/itau/sdk/android/core/type/TokenCall;)V
    .registers 3

    .line 350
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/BackendClient;->c:Lbr/com/itau/sdk/android/core/BackendClient$a;

    invoke-virtual {v0, p1}, Lbr/com/itau/sdk/android/core/BackendClient$a;->callApi(Lbr/com/itau/sdk/android/core/type/TokenCall;)V

    .line 351
    return-void
.end method

.method public clone()Ljava/lang/Object;
    .registers 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .line 398
    new-instance v0, Ljava/lang/CloneNotSupportedException;

    sget-object v1, Lbr/com/itau/sdk/android/core/BackendClient;->i:[S

    const/16 v2, 0x55

    aget-short v1, v1, v2

    sget-object v2, Lbr/com/itau/sdk/android/core/BackendClient;->i:[S

    const/16 v3, 0x14

    aget-short v2, v2, v3

    const/16 v3, 0x98

    invoke-static {v1, v3, v2}, Lbr/com/itau/sdk/android/core/BackendClient;->a(III)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/CloneNotSupportedException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getSdkCustomUi()Lbr/com/itau/sdk/android/core/SDKCustomUi;
    .registers 2

    .line 313
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/BackendClient;->h:Lbr/com/itau/sdk/android/core/SDKCustomUi;

    if-nez v0, :cond_7

    .line 314
    invoke-direct {p0}, Lbr/com/itau/sdk/android/core/BackendClient;->c()V

    .line 316
    :cond_7
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/BackendClient;->h:Lbr/com/itau/sdk/android/core/SDKCustomUi;

    return-object v0
.end method

.method public getShouldCallPasswordValidation()Ljava/lang/Boolean;
    .registers 2

    .line 300
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/BackendClient;->g:Ljava/lang/Boolean;

    return-object v0
.end method

.method public getTokenAppByPassValidation()Lbr/com/itau/sdk/android/core/token/TokenAppByPassValidation;
    .registers 2

    .line 324
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/BackendClient;->e:Lbr/com/itau/sdk/android/core/token/TokenAppByPassValidation;

    return-object v0
.end method

.method public isForcePasswordValidation()Ljava/lang/Boolean;
    .registers 4

    .line 288
    sget-object v0, Lbr/com/itau/sdk/android/core/BackendClient;->i:[S

    const/16 v1, 0x1f

    aget-short v0, v0, v1

    sget v1, Lbr/com/itau/sdk/android/core/BackendClient;->j:I

    and-int/lit16 v1, v1, 0x3a5

    const/16 v2, 0x4e

    invoke-static {v0, v1, v2}, Lbr/com/itau/sdk/android/core/BackendClient;->a(III)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lbr/com/itau/sdk/android/core/RemoteBundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1a

    const/4 v0, 0x1

    goto :goto_1b

    :cond_1a
    const/4 v0, 0x0

    :goto_1b
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public isItokenAppInstallOtherDevice()Ljava/lang/Boolean;
    .registers 4

    .line 292
    sget-object v0, Lbr/com/itau/sdk/android/core/BackendClient;->i:[S

    const/16 v1, 0x16

    aget-short v0, v0, v1

    sget-object v1, Lbr/com/itau/sdk/android/core/BackendClient;->i:[S

    const/4 v2, 0x6

    aget-short v1, v1, v2

    const/16 v2, 0x4e

    invoke-static {v0, v1, v2}, Lbr/com/itau/sdk/android/core/BackendClient;->a(III)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lbr/com/itau/sdk/android/core/RemoteBundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1b

    const/4 v0, 0x1

    goto :goto_1c

    :cond_1b
    const/4 v0, 0x0

    :goto_1c
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public isSMSBlockItokenAppOtherDevice()Ljava/lang/Boolean;
    .registers 4

    .line 296
    sget-object v0, Lbr/com/itau/sdk/android/core/BackendClient;->i:[S

    const/16 v1, 0xe8

    aget-short v0, v0, v1

    const/16 v1, 0x11a

    const/16 v2, 0x4e

    invoke-static {v0, v1, v2}, Lbr/com/itau/sdk/android/core/BackendClient;->a(III)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lbr/com/itau/sdk/android/core/RemoteBundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_18

    const/4 v0, 0x1

    goto :goto_19

    :cond_18
    const/4 v0, 0x0

    :goto_19
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public isSessionValidOrEmpty()Z
    .registers 2

    .line 373
    invoke-static {}, Lbr/com/itau/sdk/android/core/o;->a()Lbr/com/itau/sdk/android/core/o;

    move-result-object v0

    invoke-virtual {v0}, Lbr/com/itau/sdk/android/core/o;->e()Z

    move-result v0

    return v0
.end method

.method public onEventBackgroundThread(Lbr/com/itau/sdk/android/core/type/BackendCall;)V
    .registers 2

    .line 355
    iput-object p1, p0, Lbr/com/itau/sdk/android/core/BackendClient;->d:Lbr/com/itau/sdk/android/core/type/BackendCall;

    .line 356
    invoke-direct {p0}, Lbr/com/itau/sdk/android/core/BackendClient;->e()V

    .line 357
    return-void
.end method

.method public onEventBackgroundThread(Lbr/com/itau/sdk/android/core/type/RetryLastBackendRequest;)V
    .registers 7

    .line 362
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/BackendClient;->d:Lbr/com/itau/sdk/android/core/type/BackendCall;

    if-nez v0, :cond_1e

    .line 363
    new-instance v0, Ljava/lang/RuntimeException;

    sget-object v1, Lbr/com/itau/sdk/android/core/BackendClient;->i:[S

    const/16 v2, 0xb6

    aget-short v1, v1, v2

    sget v2, Lbr/com/itau/sdk/android/core/BackendClient;->j:I

    and-int/lit16 v2, v2, 0x165

    sget-object v3, Lbr/com/itau/sdk/android/core/BackendClient;->i:[S

    const/16 v4, 0xcd

    aget-short v3, v3, v4

    invoke-static {v1, v2, v3}, Lbr/com/itau/sdk/android/core/BackendClient;->a(III)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 365
    :cond_1e
    invoke-direct {p0}, Lbr/com/itau/sdk/android/core/BackendClient;->e()V

    .line 366
    return-void
.end method

.method public reset()V
    .registers 2

    .line 369
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lbr/com/itau/sdk/android/core/BackendClient;->reset(Z)V

    .line 370
    return-void
.end method

.method public reset(Z)V
    .registers 4

    .line 377
    if-eqz p1, :cond_a

    .line 378
    invoke-static {}, Lbr/com/itau/sdk/android/core/o;->a()Lbr/com/itau/sdk/android/core/o;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lbr/com/itau/sdk/android/core/o;->a(Ljava/lang/String;)V

    .line 379
    :cond_a
    invoke-direct {p0}, Lbr/com/itau/sdk/android/core/BackendClient;->d()V

    .line 380
    invoke-static {}, Lbr/com/itau/sdk/android/core/o;->a()Lbr/com/itau/sdk/android/core/o;

    move-result-object v0

    invoke-virtual {v0}, Lbr/com/itau/sdk/android/core/o;->f()V

    .line 381
    invoke-static {}, Lbr/com/itau/sdk/android/core/BackendClient;->buildDefaultUserAgent()V

    .line 382
    return-void
.end method

.method public setSdkCustomUi(Lbr/com/itau/sdk/android/core/SDKCustomUi;)V
    .registers 2

    .line 320
    iput-object p1, p0, Lbr/com/itau/sdk/android/core/BackendClient;->h:Lbr/com/itau/sdk/android/core/SDKCustomUi;

    .line 321
    return-void
.end method

.method public setShouldCallPasswordValidation(Ljava/lang/Boolean;)V
    .registers 2

    .line 305
    iput-object p1, p0, Lbr/com/itau/sdk/android/core/BackendClient;->g:Ljava/lang/Boolean;

    .line 306
    return-void
.end method

.method public setTokenAppByPassValidation(Lbr/com/itau/sdk/android/core/token/TokenAppByPassValidation;)V
    .registers 2

    .line 328
    iput-object p1, p0, Lbr/com/itau/sdk/android/core/BackendClient;->e:Lbr/com/itau/sdk/android/core/token/TokenAppByPassValidation;

    .line 329
    return-void
.end method

.method public setTracker(Lbr/com/itau/sdk/android/core/SDKTracker;)V
    .registers 2

    .line 309
    iput-object p1, p0, Lbr/com/itau/sdk/android/core/BackendClient;->f:Lbr/com/itau/sdk/android/core/SDKTracker;

    .line 310
    return-void
.end method

.method public track(Lbr/com/itau/sdk/android/core/token/TokenEvent$Category;Lbr/com/itau/sdk/android/core/token/TokenEvent$Action;)V
    .registers 4

    .line 332
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/BackendClient;->f:Lbr/com/itau/sdk/android/core/SDKTracker;

    if-eqz v0, :cond_9

    .line 333
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/BackendClient;->f:Lbr/com/itau/sdk/android/core/SDKTracker;

    invoke-interface {v0, p1, p2}, Lbr/com/itau/sdk/android/core/SDKTracker;->onTrackingEvent(Lbr/com/itau/sdk/android/core/token/TokenEvent$Category;Lbr/com/itau/sdk/android/core/token/TokenEvent$Action;)V

    .line 335
    :cond_9
    return-void
.end method

.method public trackEnterScreen(Lbr/com/itau/sdk/android/core/token/TokenEvent$Screen;)V
    .registers 3

    .line 338
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/BackendClient;->f:Lbr/com/itau/sdk/android/core/SDKTracker;

    if-eqz v0, :cond_9

    .line 339
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/BackendClient;->f:Lbr/com/itau/sdk/android/core/SDKTracker;

    invoke-interface {v0, p1}, Lbr/com/itau/sdk/android/core/SDKTracker;->onEnterScreen(Lbr/com/itau/sdk/android/core/token/TokenEvent$Screen;)V

    .line 341
    :cond_9
    return-void
.end method

.method public trackLeaveScreen(Lbr/com/itau/sdk/android/core/token/TokenEvent$Screen;)V
    .registers 3

    .line 344
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/BackendClient;->f:Lbr/com/itau/sdk/android/core/SDKTracker;

    if-eqz v0, :cond_9

    .line 345
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/BackendClient;->f:Lbr/com/itau/sdk/android/core/SDKTracker;

    invoke-interface {v0, p1}, Lbr/com/itau/sdk/android/core/SDKTracker;->onLeaveScreen(Lbr/com/itau/sdk/android/core/token/TokenEvent$Screen;)V

    .line 347
    :cond_9
    return-void
.end method

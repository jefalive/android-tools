.class public Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity;
.super Lcom/itau/empresas/ui/activity/BaseActivity;
.source "LisSimulacaoActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field static condicoesGeraisAdicionalPDF:Ljava/lang/String;

.field static condicoesGeraisPDF:Ljava/lang/String;

.field static textoAlterarCorSpanCondicoesGerais:Ljava/lang/String;

.field static textoAlterarCorSpanConsultar:Ljava/lang/String;


# instance fields
.field botaoContratar:Landroid/widget/Button;

.field controller:Lcom/itau/empresas/feature/credito/lis/LisController;

.field descricaoDataVencimento:Landroid/widget/TextView;

.field limiteAdicional:Landroid/widget/LinearLayout;

.field lisAdicional:Z

.field lisSimulacaoValorAdicional:Landroid/widget/TextView;

.field ofertaContratacaoLisVo:Lcom/itau/empresas/feature/credito/lis/model/OfertaContratacaoLisVo;

.field rlLisEscolhaValor:Landroid/view/View;

.field root:Landroid/widget/LinearLayout;

.field simulacaoRespostaVO:Lcom/itau/empresas/feature/credito/lis/model/LisSimulacaoRespostaVO;

.field textoCustoEfetivoTotal:Landroid/widget/TextView;

.field textoDataVencimento:Landroid/widget/TextView;

.field textoDescricaoValor:Landroid/widget/TextView;

.field textoDiaVencimento:Landroid/widget/TextView;

.field textoIOF:Landroid/widget/TextView;

.field textoJurosMes:Landroid/widget/TextView;

.field textoLisCondicoesGerais:Landroid/widget/TextView;

.field textoPeriodicidade:Landroid/widget/TextView;

.field textoTarifaContratacao:Landroid/widget/TextView;

.field textoTotalOperacao:Landroid/widget/TextView;

.field textoValorEscolhido:Landroid/widget/TextView;

.field textoValorPreAprovado:Landroid/widget/TextView;

.field textviewLisSimualcaoCardCondicoesGerais:Landroid/widget/TextView;

.field toolBar:Landroid/support/v7/widget/Toolbar;


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 47
    invoke-direct {p0}, Lcom/itau/empresas/ui/activity/BaseActivity;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity;)Lcom/itau/empresas/feature/credito/lis/model/LisContratacaoEnvioVO;
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity;

    .line 47
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity;->geraContratacaoEnvio()Lcom/itau/empresas/feature/credito/lis/model/LisContratacaoEnvioVO;

    move-result-object v0

    return-object v0
.end method

.method private configuraAcessibilidade(Lcom/itau/empresas/feature/credito/lis/model/LisSimulacaoRespostaVO;)V
    .registers 5
    .param p1, "vo"    # Lcom/itau/empresas/feature/credito/lis/model/LisSimulacaoRespostaVO;

    .line 297
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity;->rlLisEscolhaValor:Landroid/view/View;

    .line 298
    invoke-virtual {p1}, Lcom/itau/empresas/feature/credito/lis/model/LisSimulacaoRespostaVO;->getValorLimiteContratado()Ljava/math/BigDecimal;

    move-result-object v1

    sget-object v2, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-static {v1, v2}, Lcom/itau/empresas/ui/util/ValorMonetarioUtils;->formataValorDecimalParaMonetario(Ljava/math/BigDecimal;Z)Ljava/lang/String;

    move-result-object v1

    .line 297
    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 299
    return-void
.end method

.method private configuraLayout(Lcom/itau/empresas/feature/credito/lis/model/LisSimulacaoRespostaVO;)V
    .registers 8
    .param p1, "vo"    # Lcom/itau/empresas/feature/credito/lis/model/LisSimulacaoRespostaVO;

    .line 98
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity;->textoValorEscolhido:Landroid/widget/TextView;

    .line 99
    invoke-virtual {p1}, Lcom/itau/empresas/feature/credito/lis/model/LisSimulacaoRespostaVO;->getValorLimiteContratado()Ljava/math/BigDecimal;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/itau/empresas/ui/util/ValorMonetarioUtils;->formataValorDecimalParaMonetario(Ljava/math/BigDecimal;Z)Ljava/lang/String;

    move-result-object v1

    .line 98
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 101
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity;->textoValorPreAprovado:Landroid/widget/TextView;

    .line 102
    const v1, 0x7f070613

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity;->ofertaContratacaoLisVo:Lcom/itau/empresas/feature/credito/lis/model/OfertaContratacaoLisVo;

    .line 105
    invoke-virtual {v3}, Lcom/itau/empresas/feature/credito/lis/model/OfertaContratacaoLisVo;->getValorNovoLimite()F

    move-result v3

    .line 104
    const/4 v4, 0x1

    invoke-static {v3, v4}, Lcom/itau/empresas/ui/util/ValorMonetarioUtils;->formataValorDecimalParaMonetario(FZ)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    aput-object v3, v2, v4

    .line 102
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 101
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 107
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity;->textoDiaVencimento:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/itau/empresas/feature/credito/lis/model/LisSimulacaoRespostaVO;->getDiaPagamento()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 108
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity;->textoJurosMes:Landroid/widget/TextView;

    .line 109
    invoke-virtual {p1}, Lcom/itau/empresas/feature/credito/lis/model/LisSimulacaoRespostaVO;->getTaxaJurosMes()F

    move-result v1

    .line 110
    invoke-virtual {p1}, Lcom/itau/empresas/feature/credito/lis/model/LisSimulacaoRespostaVO;->getTaxaJurosAno()F

    move-result v2

    .line 109
    invoke-static {p0, v1, v2}, Lcom/itau/empresas/ui/util/PorcentagemUtils;->formataPorcentagemTaxaMesEAno(Landroid/content/Context;FF)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 112
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity;->textoTarifaContratacao:Landroid/widget/TextView;

    .line 113
    const v1, 0x7f070612

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    .line 115
    invoke-virtual {p1}, Lcom/itau/empresas/feature/credito/lis/model/LisSimulacaoRespostaVO;->getValorTarifaContratacao()F

    move-result v3

    sget-object v4, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    .line 116
    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    .line 115
    invoke-static {v3, v4}, Lcom/itau/empresas/ui/util/ValorMonetarioUtils;->formataValorDecimalParaMonetario(FZ)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    aput-object v3, v2, v4

    .line 118
    invoke-virtual {p1}, Lcom/itau/empresas/feature/credito/lis/model/LisSimulacaoRespostaVO;->getPercentualCetTarifaContratacao()F

    move-result v3

    .line 117
    invoke-static {v3}, Lcom/itau/empresas/ui/util/PorcentagemUtils;->formataPorcentagemSimples(F)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    aput-object v3, v2, v4

    .line 113
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 112
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 120
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity;->textoIOF:Landroid/widget/TextView;

    .line 121
    const v1, 0x7f070612

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    .line 123
    invoke-virtual {p1}, Lcom/itau/empresas/feature/credito/lis/model/LisSimulacaoRespostaVO;->getValorIof()F

    move-result v3

    sget-object v4, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    invoke-static {v3, v4}, Lcom/itau/empresas/ui/util/ValorMonetarioUtils;->formataValorDecimalParaMonetario(FZ)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    aput-object v3, v2, v4

    .line 124
    invoke-virtual {p1}, Lcom/itau/empresas/feature/credito/lis/model/LisSimulacaoRespostaVO;->getPercentualCetIof()F

    move-result v3

    invoke-static {v3}, Lcom/itau/empresas/ui/util/PorcentagemUtils;->formataPorcentagemSimples(F)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    aput-object v3, v2, v4

    .line 121
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 120
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 126
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity;->textoTotalOperacao:Landroid/widget/TextView;

    .line 127
    const v1, 0x7f070612

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    .line 129
    invoke-virtual {p1}, Lcom/itau/empresas/feature/credito/lis/model/LisSimulacaoRespostaVO;->getValorTotalOperacao()F

    move-result v3

    sget-object v4, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    .line 130
    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    .line 129
    invoke-static {v3, v4}, Lcom/itau/empresas/ui/util/ValorMonetarioUtils;->formataValorDecimalParaMonetario(FZ)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    aput-object v3, v2, v4

    .line 132
    invoke-virtual {p1}, Lcom/itau/empresas/feature/credito/lis/model/LisSimulacaoRespostaVO;->getPercentualCetTotalOperacao()F

    move-result v3

    invoke-static {v3}, Lcom/itau/empresas/ui/util/PorcentagemUtils;->formataPorcentagemSimples(F)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    aput-object v3, v2, v4

    .line 127
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 126
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 134
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity;->textoCustoEfetivoTotal:Landroid/widget/TextView;

    .line 135
    const v1, 0x7f070611

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    .line 136
    invoke-virtual {p1}, Lcom/itau/empresas/feature/credito/lis/model/LisSimulacaoRespostaVO;->getTaxaCetMes()F

    move-result v3

    invoke-static {v3}, Lcom/itau/empresas/ui/util/PorcentagemUtils;->formataPorcentagemSimples(F)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    aput-object v3, v2, v4

    .line 137
    invoke-virtual {p1}, Lcom/itau/empresas/feature/credito/lis/model/LisSimulacaoRespostaVO;->getTaxaCetAno()F

    move-result v3

    invoke-static {v3}, Lcom/itau/empresas/ui/util/PorcentagemUtils;->formataPorcentagemSimples(F)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    aput-object v3, v2, v4

    .line 135
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 134
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 139
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity;->textoPeriodicidade:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/itau/empresas/feature/credito/lis/model/LisSimulacaoRespostaVO;->getPeriodicidadeCapitalizacao()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 141
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity;->textoDataVencimento:Landroid/widget/TextView;

    .line 142
    invoke-virtual {p1}, Lcom/itau/empresas/feature/credito/lis/model/LisSimulacaoRespostaVO;->getDataVencimentoOperacao()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/itau/empresas/ui/util/DataEHoraUtils;->formataDataComBarra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 144
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity;->textoLisCondicoesGerais:Landroid/widget/TextView;

    .line 145
    const v1, 0x7f0c000a

    invoke-static {p0, v1}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v1

    .line 146
    const v2, 0x7f0705a0

    invoke-virtual {p0, v2}, Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    sget-object v4, Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity;->textoAlterarCorSpanConsultar:Ljava/lang/String;

    const/4 v5, 0x0

    aput-object v4, v3, v5

    .line 145
    const/4 v4, 0x1

    invoke-static {v1, v4, v2, v3}, Lcom/itau/empresas/ui/util/ViewUtils;->destacaStringAlteraCor(IZLjava/lang/String;[Ljava/lang/String;)Landroid/text/Spannable;

    move-result-object v1

    .line 144
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 149
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity;->textviewLisSimualcaoCardCondicoesGerais:Landroid/widget/TextView;

    .line 150
    const v1, 0x7f0c000a

    invoke-static {p0, v1}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v1

    .line 151
    const v2, 0x7f0705aa

    invoke-virtual {p0, v2}, Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    sget-object v4, Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity;->textoAlterarCorSpanCondicoesGerais:Ljava/lang/String;

    const/4 v5, 0x0

    aput-object v4, v3, v5

    .line 150
    const/4 v4, 0x1

    invoke-static {v1, v4, v2, v3}, Lcom/itau/empresas/ui/util/ViewUtils;->destacaStringAlteraCor(IZLjava/lang/String;[Ljava/lang/String;)Landroid/text/Spannable;

    move-result-object v1

    .line 149
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 154
    iget-boolean v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity;->lisAdicional:Z

    if-eqz v0, :cond_19f

    .line 155
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity;->limiteAdicional:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 156
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity;->simulacaoRespostaVO:Lcom/itau/empresas/feature/credito/lis/model/LisSimulacaoRespostaVO;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/credito/lis/model/LisSimulacaoRespostaVO;->getValorLimiteAdicional()F

    move-result v0

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-lez v0, :cond_184

    .line 157
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity;->lisSimulacaoValorAdicional:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity;->simulacaoRespostaVO:Lcom/itau/empresas/feature/credito/lis/model/LisSimulacaoRespostaVO;

    .line 159
    invoke-virtual {v1}, Lcom/itau/empresas/feature/credito/lis/model/LisSimulacaoRespostaVO;->getValorLimiteAdicional()F

    move-result v1

    sget-object v2, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    .line 158
    invoke-static {v1, v2}, Lcom/itau/empresas/ui/util/ValorMonetarioUtils;->formataValorDecimalParaMonetario(FZ)Ljava/lang/String;

    move-result-object v1

    .line 157
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_18f

    .line 162
    :cond_184
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity;->lisSimulacaoValorAdicional:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity;->simulacaoRespostaVO:Lcom/itau/empresas/feature/credito/lis/model/LisSimulacaoRespostaVO;

    .line 163
    invoke-virtual {v1}, Lcom/itau/empresas/feature/credito/lis/model/LisSimulacaoRespostaVO;->getTextoLimiteAdicional()Ljava/lang/String;

    move-result-object v1

    .line 162
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 165
    :goto_18f
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity;->textoDescricaoValor:Landroid/widget/TextView;

    .line 166
    const v1, 0x7f07059b

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 167
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity;->descricaoDataVencimento:Landroid/widget/TextView;

    .line 168
    const v1, 0x7f07059c

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 171
    :cond_19f
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity;->botaoContratar:Landroid/widget/Button;

    const v1, 0x7f070428

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    .line 172
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity;->botaoContratar:Landroid/widget/Button;

    const-string v1, "fonts/itaufonts_master_24px_v1.ttf"

    invoke-static {v1, p0}, Lcom/itau/empresas/ui/view/Typefaces;->getTypeface(Ljava/lang/String;Landroid/content/Context;)Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTypeface(Landroid/graphics/Typeface;)V

    .line 173
    return-void
.end method

.method private constroiToolbar()V
    .registers 3

    .line 302
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity;->toolBar:Landroid/support/v7/widget/Toolbar;

    invoke-static {v0, p0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom;->Builder(Landroid/support/v7/widget/Toolbar;Landroid/support/v7/app/AppCompatActivity;)Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    move-result-object v0

    .line 303
    const v1, 0x7f070429

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->comTitulo(I)Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    move-result-object v0

    .line 304
    invoke-virtual {v0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->comClickListenerFinish()Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    move-result-object v0

    .line 305
    invoke-virtual {v0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->build()Lcom/itau/empresas/ui/util/listener/ToolbarCustom;

    .line 306
    return-void
.end method

.method private geraContratacaoEnvio()Lcom/itau/empresas/feature/credito/lis/model/LisContratacaoEnvioVO;
    .registers 10

    .line 229
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity;->application:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getDadosOperador()Lcom/itau/empresas/api/model/DadosOperadorVO;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/api/model/DadosOperadorVO;->getNomeOperador()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v1, 0x1e

    if-le v0, v1, :cond_24

    .line 231
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity;->application:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getDadosOperador()Lcom/itau/empresas/api/model/DadosOperadorVO;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/api/model/DadosOperadorVO;->getNomeOperador()Ljava/lang/String;

    move-result-object v0

    .line 232
    const/4 v1, 0x0

    const/16 v2, 0x1e

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v8

    .local v8, "nomeOperador":Ljava/lang/String;
    goto :goto_2e

    .line 234
    .end local v8    # "nomeOperador":Ljava/lang/String;
    :cond_24
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity;->application:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getDadosOperador()Lcom/itau/empresas/api/model/DadosOperadorVO;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/api/model/DadosOperadorVO;->getNomeOperador()Ljava/lang/String;

    move-result-object v8

    .line 237
    .local v8, "nomeOperador":Ljava/lang/String;
    :goto_2e
    new-instance v0, Lcom/itau/empresas/feature/credito/lis/model/LisContratacaoEnvioVO;

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity;->simulacaoRespostaVO:Lcom/itau/empresas/feature/credito/lis/model/LisSimulacaoRespostaVO;

    invoke-virtual {v1}, Lcom/itau/empresas/feature/credito/lis/model/LisSimulacaoRespostaVO;->getValorLimiteContratado()Ljava/math/BigDecimal;

    move-result-object v1

    invoke-virtual {v1}, Ljava/math/BigDecimal;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity;->simulacaoRespostaVO:Lcom/itau/empresas/feature/credito/lis/model/LisSimulacaoRespostaVO;

    .line 238
    invoke-virtual {v2}, Lcom/itau/empresas/feature/credito/lis/model/LisSimulacaoRespostaVO;->getDiaPagamento()Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity;->application:Lcom/itau/empresas/CustomApplication;

    .line 239
    invoke-virtual {v3}, Lcom/itau/empresas/CustomApplication;->getContaSelecionada()Lcom/itau/empresas/api/model/ContaOperadorVO;

    move-result-object v3

    invoke-virtual {v3}, Lcom/itau/empresas/api/model/ContaOperadorVO;->getAgencia()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity;->application:Lcom/itau/empresas/CustomApplication;

    .line 241
    invoke-virtual {v4}, Lcom/itau/empresas/CustomApplication;->getContaSelecionada()Lcom/itau/empresas/api/model/ContaOperadorVO;

    move-result-object v4

    invoke-virtual {v4}, Lcom/itau/empresas/api/model/ContaOperadorVO;->getConta()Ljava/lang/String;

    move-result-object v4

    .line 240
    const/4 v5, 0x7

    invoke-static {v5, v4}, Lcom/itau/empresas/ui/util/ViewUtils;->formataAdicionandoZeros(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity;->application:Lcom/itau/empresas/CustomApplication;

    .line 242
    invoke-virtual {v5}, Lcom/itau/empresas/CustomApplication;->getContaSelecionada()Lcom/itau/empresas/api/model/ContaOperadorVO;

    move-result-object v5

    invoke-virtual {v5}, Lcom/itau/empresas/api/model/ContaOperadorVO;->getDac()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity;->application:Lcom/itau/empresas/CustomApplication;

    .line 243
    invoke-virtual {v6}, Lcom/itau/empresas/CustomApplication;->getDadosOperador()Lcom/itau/empresas/api/model/DadosOperadorVO;

    move-result-object v6

    invoke-virtual {v6}, Lcom/itau/empresas/api/model/DadosOperadorVO;->getNumeroDocumento()Ljava/lang/String;

    move-result-object v6

    move-object v7, v8

    invoke-direct/range {v0 .. v7}, Lcom/itau/empresas/feature/credito/lis/model/LisContratacaoEnvioVO;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 237
    return-object v0
.end method


# virtual methods
.method aoCarregarTela()V
    .registers 2

    .line 91
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity;->constroiToolbar()V

    .line 92
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity;->simulacaoRespostaVO:Lcom/itau/empresas/feature/credito/lis/model/LisSimulacaoRespostaVO;

    invoke-direct {p0, v0}, Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity;->configuraLayout(Lcom/itau/empresas/feature/credito/lis/model/LisSimulacaoRespostaVO;)V

    .line 93
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity;->simulacaoRespostaVO:Lcom/itau/empresas/feature/credito/lis/model/LisSimulacaoRespostaVO;

    invoke-direct {p0, v0}, Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity;->configuraAcessibilidade(Lcom/itau/empresas/feature/credito/lis/model/LisSimulacaoRespostaVO;)V

    .line 94
    return-void
.end method

.method condicoesGerais()V
    .registers 2

    .line 177
    .line 178
    invoke-static {p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisCondicoesGeraisActivity;->intent(Landroid/content/Context;)Lcom/itau/empresas/feature/credito/lis/activity/LisCondicoesGeraisActivity_$IntentBuilder_;

    move-result-object v0

    .line 179
    invoke-virtual {v0}, Lcom/itau/empresas/feature/credito/lis/activity/LisCondicoesGeraisActivity_$IntentBuilder_;->start()Lorg/androidannotations/api/builder/PostActivityStarter;

    .line 180
    return-void
.end method

.method contratar()V
    .registers 6

    .line 204
    .line 205
    const v0, 0x7f0705ee

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity;->application:Lcom/itau/empresas/CustomApplication;

    .line 206
    invoke-virtual {v2}, Lcom/itau/empresas/CustomApplication;->getDadosOperador()Lcom/itau/empresas/api/model/DadosOperadorVO;

    move-result-object v2

    invoke-virtual {v2}, Lcom/itau/empresas/api/model/DadosOperadorVO;->getNomeOperador()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    sget-object v2, Lbr/com/concretesolutions/canarinho/formatador/Formatador;->CPF:Lbr/com/concretesolutions/canarinho/formatador/Formatador;

    iget-object v3, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity;->application:Lcom/itau/empresas/CustomApplication;

    .line 208
    invoke-virtual {v3}, Lcom/itau/empresas/CustomApplication;->getDadosOperador()Lcom/itau/empresas/api/model/DadosOperadorVO;

    move-result-object v3

    invoke-virtual {v3}, Lcom/itau/empresas/api/model/DadosOperadorVO;->getNumeroDocumento()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lbr/com/concretesolutions/canarinho/formatador/Formatador;->formata(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    aput-object v2, v1, v3

    .line 205
    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 210
    .local v4, "descricaoDialog":Ljava/lang/String;
    invoke-static {}, Lcom/itau/empresas/ui/dialog/RedesignDialog_;->builder()Lcom/itau/empresas/ui/dialog/RedesignDialog_$FragmentBuilder_;

    move-result-object v0

    .line 211
    invoke-virtual {v0}, Lcom/itau/empresas/ui/dialog/RedesignDialog_$FragmentBuilder_;->build()Lcom/itau/empresas/ui/dialog/RedesignDialog;

    move-result-object v0

    new-instance v1, Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity$1;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity$1;-><init>(Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity;)V

    .line 212
    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/dialog/RedesignDialog;->setContinuarClickListner(Landroid/view/View$OnClickListener;)Lcom/itau/empresas/ui/dialog/RedesignDialog;

    move-result-object v0

    .line 220
    invoke-virtual {v0, v4}, Lcom/itau/empresas/ui/dialog/RedesignDialog;->setDescricao(Ljava/lang/String;)Lcom/itau/empresas/ui/dialog/RedesignDialog;

    move-result-object v0

    .line 221
    const v1, 0x7f0705ef

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/dialog/RedesignDialog;->setTitulo(Ljava/lang/String;)Lcom/itau/empresas/ui/dialog/RedesignDialog;

    move-result-object v0

    .line 222
    invoke-virtual {v0, p0}, Lcom/itau/empresas/ui/dialog/RedesignDialog;->show(Landroid/support/v7/app/AppCompatActivity;)Lcom/itau/empresas/ui/dialog/BaseDialog;

    .line 223
    return-void
.end method

.method public hitAnalytics()Ljava/lang/String;
    .registers 2

    .line 86
    const v0, 0x7f070337

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method mostraPdfCondicoesLis()V
    .registers 3

    .line 193
    iget-boolean v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity;->lisAdicional:Z

    if-eqz v0, :cond_7

    sget-object v1, Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity;->condicoesGeraisAdicionalPDF:Ljava/lang/String;

    goto :goto_9

    :cond_7
    sget-object v1, Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity;->condicoesGeraisPDF:Ljava/lang/String;

    .line 195
    .line 196
    .local v1, "url":Ljava/lang/String;
    :goto_9
    invoke-static {p0}, Lcom/itau/empresas/ui/activity/PDFActivity;->intent(Landroid/content/Context;)Lcom/itau/empresas/ui/activity/PDFActivity_$IntentBuilder_;

    move-result-object v0

    .line 197
    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/activity/PDFActivity_$IntentBuilder_;->url(Ljava/lang/String;)Lcom/itau/empresas/ui/activity/PDFActivity_$IntentBuilder_;

    move-result-object v0

    .line 198
    invoke-virtual {v0}, Lcom/itau/empresas/ui/activity/PDFActivity_$IntentBuilder_;->start()Lorg/androidannotations/api/builder/PostActivityStarter;

    .line 199
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .registers 2
    .param p1, "v"    # Landroid/view/View;

    .line 293
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity;->onBackPressed()V

    .line 294
    return-void
.end method

.method public onEventMainThread(Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent$Conversion;)V
    .registers 6
    .param p1, "e"    # Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent$Conversion;

    .line 261
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity;->escondeDialogoDeProgresso()V

    .line 262
    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent$Conversion;->getException()Lbr/com/itau/sdk/android/core/exception/BackendException;

    move-result-object v0

    invoke-virtual {v0}, Lbr/com/itau/sdk/android/core/exception/BackendException;->getError()Lbr/com/itau/sdk/android/core/model/ErrorDTO;

    move-result-object v2

    .line 263
    .local v2, "error":Lbr/com/itau/sdk/android/core/model/ErrorDTO;
    if-eqz v2, :cond_12

    .line 264
    invoke-virtual {v2}, Lbr/com/itau/sdk/android/core/model/ErrorDTO;->getMensagem()Ljava/lang/String;

    move-result-object v3

    goto :goto_19

    :cond_12
    const v0, 0x7f0704d3

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 265
    .local v3, "mensagemErro":Ljava/lang/String;
    :goto_19
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity;->root:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-static {v0, v3, v1}, Lbr/com/itau/topsnackbar/TopSnackbar;->make(Landroid/view/ViewGroup;Ljava/lang/CharSequence;I)Lbr/com/itau/topsnackbar/TopSnackbar;

    move-result-object v0

    .line 266
    invoke-virtual {v0}, Lbr/com/itau/topsnackbar/TopSnackbar;->show()Lbr/com/itau/topsnackbar/TopSnackbar;

    .line 267
    return-void
.end method

.method public onEventMainThread(Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent$Session;)V
    .registers 5
    .param p1, "e"    # Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent$Session;

    .line 255
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity;->escondeDialogoDeProgresso()V

    .line 256
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity;->root:Landroid/widget/LinearLayout;

    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent$Session;->getException()Lbr/com/itau/sdk/android/core/exception/BackendException;

    move-result-object v1

    invoke-virtual {v1}, Lbr/com/itau/sdk/android/core/exception/BackendException;->getError()Lbr/com/itau/sdk/android/core/model/ErrorDTO;

    move-result-object v1

    invoke-virtual {v1}, Lbr/com/itau/sdk/android/core/model/ErrorDTO;->getMensagem()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lbr/com/itau/topsnackbar/TopSnackbar;->make(Landroid/view/ViewGroup;Ljava/lang/CharSequence;I)Lbr/com/itau/topsnackbar/TopSnackbar;

    move-result-object v0

    .line 257
    invoke-virtual {v0}, Lbr/com/itau/topsnackbar/TopSnackbar;->show()Lbr/com/itau/topsnackbar/TopSnackbar;

    .line 258
    return-void
.end method

.method public onEventMainThread(Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;)V
    .registers 5
    .param p1, "e"    # Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;

    .line 270
    invoke-static {p1, p0}, Lcom/itau/empresas/api/ApiHelper;->devoTratarEvento(Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;Lcom/itau/empresas/api/ApiConsumidor;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 271
    return-void

    .line 274
    :cond_7
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity;->escondeDialogoDeProgresso()V

    .line 275
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity;->root:Landroid/widget/LinearLayout;

    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;->getException()Lbr/com/itau/sdk/android/core/exception/BackendException;

    move-result-object v1

    invoke-virtual {v1}, Lbr/com/itau/sdk/android/core/exception/BackendException;->getError()Lbr/com/itau/sdk/android/core/model/ErrorDTO;

    move-result-object v1

    invoke-virtual {v1}, Lbr/com/itau/sdk/android/core/model/ErrorDTO;->getMensagem()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lbr/com/itau/topsnackbar/TopSnackbar;->make(Landroid/view/ViewGroup;Ljava/lang/CharSequence;I)Lbr/com/itau/topsnackbar/TopSnackbar;

    move-result-object v0

    .line 276
    invoke-virtual {v0}, Lbr/com/itau/topsnackbar/TopSnackbar;->show()Lbr/com/itau/topsnackbar/TopSnackbar;

    .line 277
    return-void
.end method

.method public onEventMainThread(Lcom/itau/empresas/feature/credito/lis/model/LisContratacaoRespostaVO;)V
    .registers 4
    .param p1, "lisContratacaoRespostaVO"    # Lcom/itau/empresas/feature/credito/lis/model/LisContratacaoRespostaVO;

    .line 280
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity;->escondeDialogoDeProgresso()V

    .line 281
    .line 282
    invoke-static {p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisComprovanteActivity;->intent(Landroid/content/Context;)Lcom/itau/empresas/feature/credito/lis/activity/LisComprovanteActivity_$IntentBuilder_;

    move-result-object v0

    .line 283
    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/credito/lis/activity/LisComprovanteActivity_$IntentBuilder_;->flags(I)Lorg/androidannotations/api/builder/IntentBuilder;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/feature/credito/lis/activity/LisComprovanteActivity_$IntentBuilder_;

    .line 284
    invoke-virtual {v0, p1}, Lcom/itau/empresas/feature/credito/lis/activity/LisComprovanteActivity_$IntentBuilder_;->contratacaoRespostaVO(Lcom/itau/empresas/feature/credito/lis/model/LisContratacaoRespostaVO;)Lcom/itau/empresas/feature/credito/lis/activity/LisComprovanteActivity_$IntentBuilder_;

    move-result-object v0

    iget-boolean v1, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity;->lisAdicional:Z

    .line 285
    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/credito/lis/activity/LisComprovanteActivity_$IntentBuilder_;->lisAdicional(Z)Lcom/itau/empresas/feature/credito/lis/activity/LisComprovanteActivity_$IntentBuilder_;

    move-result-object v0

    .line 286
    invoke-virtual {v0}, Lcom/itau/empresas/feature/credito/lis/activity/LisComprovanteActivity_$IntentBuilder_;->start()Lorg/androidannotations/api/builder/PostActivityStarter;

    .line 287
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity;->finish()V

    .line 288
    return-void
.end method

.method public opkeysConsumidas()Ljava/util/List;
    .registers 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Ljava/lang/String;>;"
        }
    .end annotation

    .line 249
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "creditoRotativoChequeEspecialSimular"

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const-string v1, "creditoRotativoChequeEspecialEfetivar"

    const/4 v2, 0x1

    aput-object v1, v0, v2

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method reiniciar()V
    .registers 3

    .line 184
    .line 185
    invoke-static {p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisSelecaoValoresActivity;->intent(Landroid/content/Context;)Lcom/itau/empresas/feature/credito/lis/activity/LisSelecaoValoresActivity_$IntentBuilder_;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity;->ofertaContratacaoLisVo:Lcom/itau/empresas/feature/credito/lis/model/OfertaContratacaoLisVo;

    .line 186
    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/credito/lis/activity/LisSelecaoValoresActivity_$IntentBuilder_;->ofertaContratacaoLisVo(Lcom/itau/empresas/feature/credito/lis/model/OfertaContratacaoLisVo;)Lcom/itau/empresas/feature/credito/lis/activity/LisSelecaoValoresActivity_$IntentBuilder_;

    move-result-object v0

    iget-boolean v1, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity;->lisAdicional:Z

    .line 187
    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/credito/lis/activity/LisSelecaoValoresActivity_$IntentBuilder_;->possuiProdutoLisAdiciona(Z)Lcom/itau/empresas/feature/credito/lis/activity/LisSelecaoValoresActivity_$IntentBuilder_;

    move-result-object v0

    .line 188
    invoke-virtual {v0}, Lcom/itau/empresas/feature/credito/lis/activity/LisSelecaoValoresActivity_$IntentBuilder_;->start()Lorg/androidannotations/api/builder/PostActivityStarter;

    .line 189
    return-void
.end method

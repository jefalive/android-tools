.class final Lbr/com/itau/textovoz/model/Menu$1;
.super Ljava/lang/Object;
.source "Menu.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lbr/com/itau/textovoz/model/Menu;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;Landroid/os/Parcelable$Creator<Lbr/com/itau/textovoz/model/Menu;>;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .registers 1

    .line 136
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lbr/com/itau/textovoz/model/Menu;
    .registers 3
    .param p1, "in"    # Landroid/os/Parcel;

    .line 139
    new-instance v0, Lbr/com/itau/textovoz/model/Menu;

    invoke-direct {v0, p1}, Lbr/com/itau/textovoz/model/Menu;-><init>(Landroid/os/Parcel;)V

    return-object v0
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .registers 3

    .line 136
    invoke-virtual {p0, p1}, Lbr/com/itau/textovoz/model/Menu$1;->createFromParcel(Landroid/os/Parcel;)Lbr/com/itau/textovoz/model/Menu;

    move-result-object v0

    return-object v0
.end method

.method public newArray(I)[Lbr/com/itau/textovoz/model/Menu;
    .registers 3
    .param p1, "size"    # I

    .line 144
    new-array v0, p1, [Lbr/com/itau/textovoz/model/Menu;

    return-object v0
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .registers 3

    .line 136
    invoke-virtual {p0, p1}, Lbr/com/itau/textovoz/model/Menu$1;->newArray(I)[Lbr/com/itau/textovoz/model/Menu;

    move-result-object v0

    return-object v0
.end method

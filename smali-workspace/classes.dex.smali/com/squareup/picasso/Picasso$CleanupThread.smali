.class Lcom/squareup/picasso/Picasso$CleanupThread;
.super Ljava/lang/Thread;
.source "Picasso.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/picasso/Picasso;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "CleanupThread"
.end annotation


# instance fields
.field private final handler:Landroid/os/Handler;

.field private final referenceQueue:Ljava/lang/ref/ReferenceQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/ReferenceQueue<Ljava/lang/Object;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Ljava/lang/ref/ReferenceQueue;Landroid/os/Handler;)V
    .registers 4
    .param p1, "referenceQueue"    # Ljava/lang/ref/ReferenceQueue;
    .param p2, "handler"    # Landroid/os/Handler;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/lang/ref/ReferenceQueue<Ljava/lang/Object;>;Landroid/os/Handler;)V"
        }
    .end annotation

    .line 596
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 597
    iput-object p1, p0, Lcom/squareup/picasso/Picasso$CleanupThread;->referenceQueue:Ljava/lang/ref/ReferenceQueue;

    .line 598
    iput-object p2, p0, Lcom/squareup/picasso/Picasso$CleanupThread;->handler:Landroid/os/Handler;

    .line 599
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/squareup/picasso/Picasso$CleanupThread;->setDaemon(Z)V

    .line 600
    const-string v0, "Picasso-refQueue"

    invoke-virtual {p0, v0}, Lcom/squareup/picasso/Picasso$CleanupThread;->setName(Ljava/lang/String;)V

    .line 601
    return-void
.end method


# virtual methods
.method public run()V
    .registers 6

    .line 604
    const/16 v0, 0xa

    invoke-static {v0}, Landroid/os/Process;->setThreadPriority(I)V

    .line 612
    :goto_5
    :try_start_5
    iget-object v0, p0, Lcom/squareup/picasso/Picasso$CleanupThread;->referenceQueue:Ljava/lang/ref/ReferenceQueue;

    .line 613
    const-wide/16 v1, 0x3e8

    invoke-virtual {v0, v1, v2}, Ljava/lang/ref/ReferenceQueue;->remove(J)Ljava/lang/ref/Reference;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/picasso/Action$RequestWeakReference;

    .line 614
    .local v3, "remove":Lcom/squareup/picasso/Action$RequestWeakReference;, "Lcom/squareup/picasso/Action$RequestWeakReference<*>;"
    iget-object v0, p0, Lcom/squareup/picasso/Picasso$CleanupThread;->handler:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v4

    .line 615
    .local v4, "message":Landroid/os/Message;
    if-eqz v3, :cond_25

    .line 616
    const/4 v0, 0x3

    iput v0, v4, Landroid/os/Message;->what:I

    .line 617
    iget-object v0, v3, Lcom/squareup/picasso/Action$RequestWeakReference;->action:Lcom/squareup/picasso/Action;

    iput-object v0, v4, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 618
    iget-object v0, p0, Lcom/squareup/picasso/Picasso$CleanupThread;->handler:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_28

    .line 620
    :cond_25
    invoke-virtual {v4}, Landroid/os/Message;->recycle()V
    :try_end_28
    .catch Ljava/lang/InterruptedException; {:try_start_5 .. :try_end_28} :catch_29
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_28} :catch_2b

    .line 631
    .end local v3    # "remove":Lcom/squareup/picasso/Action$RequestWeakReference;, "Lcom/squareup/picasso/Action$RequestWeakReference<*>;"
    .end local v3
    .end local v4    # "message":Landroid/os/Message;
    :goto_28
    goto :goto_5

    .line 622
    :catch_29
    move-exception v3

    .line 623
    .local v3, "e":Ljava/lang/InterruptedException;
    goto :goto_36

    .line 624
    .end local v3    # "e":Ljava/lang/InterruptedException;
    :catch_2b
    move-exception v3

    .line 625
    .local v3, "e":Ljava/lang/Exception;
    iget-object v0, p0, Lcom/squareup/picasso/Picasso$CleanupThread;->handler:Landroid/os/Handler;

    new-instance v1, Lcom/squareup/picasso/Picasso$CleanupThread$1;

    invoke-direct {v1, p0, v3}, Lcom/squareup/picasso/Picasso$CleanupThread$1;-><init>(Lcom/squareup/picasso/Picasso$CleanupThread;Ljava/lang/Exception;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 630
    .line 633
    .end local v3    # "e":Ljava/lang/Exception;
    :goto_36
    return-void
.end method

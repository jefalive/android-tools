.class public final Lcom/itau/empresas/ui/util/formatacao/FormatadorNumerico;
.super Ljava/lang/Object;
.source "FormatadorNumerico.java"


# static fields
.field private static final FORMATADOR_MOEDA:Ljava/text/DecimalFormat;

.field private static final FORMATADOR_MOEDA_SEM_CIFRAO:Ljava/text/DecimalFormat;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .line 15
    new-instance v0, Ljava/util/Locale;

    const-string v1, "pt"

    const-string v2, "BR"

    invoke-direct {v0, v1, v2}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 16
    invoke-static {v0}, Ljava/text/NumberFormat;->getCurrencyInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object v0

    check-cast v0, Ljava/text/DecimalFormat;

    sput-object v0, Lcom/itau/empresas/ui/util/formatacao/FormatadorNumerico;->FORMATADOR_MOEDA:Ljava/text/DecimalFormat;

    .line 17
    new-instance v0, Ljava/util/Locale;

    const-string v1, "pt"

    const-string v2, "BR"

    invoke-direct {v0, v1, v2}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 18
    invoke-static {v0}, Ljava/text/NumberFormat;->getCurrencyInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object v0

    check-cast v0, Ljava/text/DecimalFormat;

    sput-object v0, Lcom/itau/empresas/ui/util/formatacao/FormatadorNumerico;->FORMATADOR_MOEDA_SEM_CIFRAO:Ljava/text/DecimalFormat;

    .line 17
    return-void
.end method

.method public static decimalParaMonetario(Ljava/math/BigDecimal;Z)Ljava/lang/String;
    .registers 3
    .param p0, "valor"    # Ljava/math/BigDecimal;
    .param p1, "comCifrao"    # Z

    .line 46
    if-eqz p1, :cond_9

    sget-object v0, Lcom/itau/empresas/ui/util/formatacao/FormatadorNumerico;->FORMATADOR_MOEDA:Ljava/text/DecimalFormat;

    .line 47
    invoke-virtual {v0, p0}, Ljava/text/DecimalFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_f

    :cond_9
    sget-object v0, Lcom/itau/empresas/ui/util/formatacao/FormatadorNumerico;->FORMATADOR_MOEDA_SEM_CIFRAO:Ljava/text/DecimalFormat;

    .line 48
    invoke-virtual {v0, p0}, Ljava/text/DecimalFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 46
    :goto_f
    return-object v0
.end method

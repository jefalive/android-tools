.class public final Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosContratadosDetalhesActivity_;
.super Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosContratadosDetalhesActivity;
.source "EmprestimosParceladosContratadosDetalhesActivity_.java"

# interfaces
.implements Lorg/androidannotations/api/view/HasViews;
.implements Lorg/androidannotations/api/view/OnViewChangedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosContratadosDetalhesActivity_$IntentBuilder_;
    }
.end annotation


# instance fields
.field private final onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;


# direct methods
.method public constructor <init>()V
    .registers 2

    .line 34
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosContratadosDetalhesActivity;-><init>()V

    .line 38
    new-instance v0, Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-direct {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosContratadosDetalhesActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    return-void
.end method

.method static synthetic access$001(Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosContratadosDetalhesActivity_;)V
    .registers 1
    .param p0, "x0"    # Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosContratadosDetalhesActivity_;

    .line 34
    invoke-super {p0}, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosContratadosDetalhesActivity;->mostraDialogoDeProgresso()V

    return-void
.end method

.method static synthetic access$101(Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosContratadosDetalhesActivity_;)V
    .registers 1
    .param p0, "x0"    # Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosContratadosDetalhesActivity_;

    .line 34
    invoke-super {p0}, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosContratadosDetalhesActivity;->escondeDialogoDeProgresso()V

    return-void
.end method

.method private init_(Landroid/os/Bundle;)V
    .registers 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 51
    new-instance v0, Lcom/itau/empresas/Preferences_;

    invoke-direct {v0, p0}, Lcom/itau/empresas/Preferences_;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosContratadosDetalhesActivity_;->prefs:Lcom/itau/empresas/Preferences_;

    .line 52
    invoke-static {p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->registerOnViewChangedListener(Lorg/androidannotations/api/view/OnViewChangedListener;)V

    .line 53
    invoke-static {}, Lcom/itau/empresas/CustomApplication_;->getInstance()Lcom/itau/empresas/CustomApplication;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosContratadosDetalhesActivity_;->application:Lcom/itau/empresas/CustomApplication;

    .line 54
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosContratadosDetalhesActivity_;->injectExtras_()V

    .line 55
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosContratadosDetalhesActivity_;->afterInject()V

    .line 56
    return-void
.end method

.method private injectExtras_()V
    .registers 3

    .line 96
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosContratadosDetalhesActivity_;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 97
    .local v1, "extras_":Landroid/os/Bundle;
    if-eqz v1, :cond_1c

    .line 98
    const-string v0, "camposComprovanteVO"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 99
    const-string v0, "camposComprovanteVO"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/feature/credito/giro/model/CamposComprovanteVO;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosContratadosDetalhesActivity_;->camposComprovanteVO:Lcom/itau/empresas/feature/credito/giro/model/CamposComprovanteVO;

    .line 102
    :cond_1c
    return-void
.end method


# virtual methods
.method public escondeDialogoDeProgresso()V
    .registers 5

    .line 127
    const-string v0, ""

    new-instance v1, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosContratadosDetalhesActivity_$2;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosContratadosDetalhesActivity_$2;-><init>(Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosContratadosDetalhesActivity_;)V

    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3}, Lorg/androidannotations/api/UiThreadExecutor;->runTask(Ljava/lang/String;Ljava/lang/Runnable;J)V

    .line 135
    return-void
.end method

.method public mostraDialogoDeProgresso()V
    .registers 5
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "CommitTransaction"
        }
    .end annotation

    .line 115
    const-string v0, ""

    new-instance v1, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosContratadosDetalhesActivity_$1;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosContratadosDetalhesActivity_$1;-><init>(Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosContratadosDetalhesActivity_;)V

    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3}, Lorg/androidannotations/api/UiThreadExecutor;->runTask(Ljava/lang/String;Ljava/lang/Runnable;J)V

    .line 123
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 43
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosContratadosDetalhesActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-static {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    move-result-object v1

    .line 44
    .local v1, "previousNotifier":Lorg/androidannotations/api/view/OnViewChangedNotifier;
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosContratadosDetalhesActivity_;->init_(Landroid/os/Bundle;)V

    .line 45
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosContratadosDetalhesActivity;->onCreate(Landroid/os/Bundle;)V

    .line 46
    invoke-static {v1}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 47
    const v0, 0x7f030030

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosContratadosDetalhesActivity_;->setContentView(I)V

    .line 48
    return-void
.end method

.method public onViewChanged(Lorg/androidannotations/api/view/HasViews;)V
    .registers 3
    .param p1, "hasViews"    # Lorg/androidannotations/api/view/HasViews;

    .line 90
    const v0, 0x7f0e00b6

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/Toolbar;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosContratadosDetalhesActivity_;->toolBar:Landroid/support/v7/widget/Toolbar;

    .line 91
    const v0, 0x7f0e0164

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosContratadosDetalhesActivity_;->viewCreditoContratadosDetalhe:Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;

    .line 92
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosContratadosDetalhesActivity_;->aoCarregarElementosDeTela()V

    .line 93
    return-void
.end method

.method public setContentView(I)V
    .registers 3
    .param p1, "layoutResID"    # I

    .line 60
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosContratadosDetalhesActivity;->setContentView(I)V

    .line 61
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosContratadosDetalhesActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 62
    return-void
.end method

.method public setContentView(Landroid/view/View;)V
    .registers 3
    .param p1, "view"    # Landroid/view/View;

    .line 72
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosContratadosDetalhesActivity;->setContentView(Landroid/view/View;)V

    .line 73
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosContratadosDetalhesActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 74
    return-void
.end method

.method public setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .registers 4
    .param p1, "view"    # Landroid/view/View;
    .param p2, "params"    # Landroid/view/ViewGroup$LayoutParams;

    .line 66
    invoke-super {p0, p1, p2}, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosContratadosDetalhesActivity;->setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 67
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosContratadosDetalhesActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 68
    return-void
.end method

.method public setIntent(Landroid/content/Intent;)V
    .registers 2
    .param p1, "newIntent"    # Landroid/content/Intent;

    .line 106
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosContratadosDetalhesActivity;->setIntent(Landroid/content/Intent;)V

    .line 107
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosContratadosDetalhesActivity_;->injectExtras_()V

    .line 108
    return-void
.end method

.class Lcom/itau/empresas/controller/SessaoController$1;
.super Ljava/lang/Object;
.source "SessaoController.java"

# interfaces
.implements Lbr/com/itau/sdk/android/core/type/BackendCall;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/itau/empresas/controller/SessaoController;->trocarConta(Lcom/itau/empresas/api/model/ContaOperadorVO;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/itau/empresas/controller/SessaoController;

.field final synthetic val$conta:Lcom/itau/empresas/api/model/ContaOperadorVO;


# direct methods
.method constructor <init>(Lcom/itau/empresas/controller/SessaoController;Lcom/itau/empresas/api/model/ContaOperadorVO;)V
    .registers 3
    .param p1, "this$0"    # Lcom/itau/empresas/controller/SessaoController;

    .line 26
    iput-object p1, p0, Lcom/itau/empresas/controller/SessaoController$1;->this$0:Lcom/itau/empresas/controller/SessaoController;

    iput-object p2, p0, Lcom/itau/empresas/controller/SessaoController$1;->val$conta:Lcom/itau/empresas/api/model/ContaOperadorVO;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public callBackend()V
    .registers 5

    .line 29
    new-instance v3, Lcom/itau/empresas/api/model/TrocaContaVO;

    iget-object v0, p0, Lcom/itau/empresas/controller/SessaoController$1;->val$conta:Lcom/itau/empresas/api/model/ContaOperadorVO;

    invoke-virtual {v0}, Lcom/itau/empresas/api/model/ContaOperadorVO;->getAgencia()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/controller/SessaoController$1;->val$conta:Lcom/itau/empresas/api/model/ContaOperadorVO;

    .line 30
    invoke-virtual {v1}, Lcom/itau/empresas/api/model/ContaOperadorVO;->getConta()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/itau/empresas/controller/SessaoController$1;->val$conta:Lcom/itau/empresas/api/model/ContaOperadorVO;

    .line 31
    invoke-virtual {v2}, Lcom/itau/empresas/api/model/ContaOperadorVO;->getDac()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v3, v0, v1, v2}, Lcom/itau/empresas/api/model/TrocaContaVO;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 32
    .local v3, "contaSelecionada":Lcom/itau/empresas/api/model/TrocaContaVO;
    iget-object v0, p0, Lcom/itau/empresas/controller/SessaoController$1;->this$0:Lcom/itau/empresas/controller/SessaoController;

    iget-object v0, v0, Lcom/itau/empresas/controller/SessaoController;->app:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getEventoTracker()Lcom/itau/empresas/ui/util/analytics/TrackerEvento;

    move-result-object v0

    const-string v1, "trocaConta"

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/analytics/TrackerEvento;->inicioMonitorarTempo(Ljava/lang/String;)V

    .line 33
    iget-object v0, p0, Lcom/itau/empresas/controller/SessaoController$1;->this$0:Lcom/itau/empresas/controller/SessaoController;

    invoke-virtual {v0}, Lcom/itau/empresas/controller/SessaoController;->api()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/api/Api;

    invoke-interface {v0, v3}, Lcom/itau/empresas/api/Api;->trocarContaSessao(Lcom/itau/empresas/api/model/TrocaContaVO;)Ljava/lang/Void;

    .line 34
    iget-object v0, p0, Lcom/itau/empresas/controller/SessaoController$1;->this$0:Lcom/itau/empresas/controller/SessaoController;

    iget-object v0, v0, Lcom/itau/empresas/controller/SessaoController;->app:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getEventoTracker()Lcom/itau/empresas/ui/util/analytics/TrackerEvento;

    move-result-object v0

    const-string v1, "trocaConta"

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/analytics/TrackerEvento;->fimMonitorarTempo(Ljava/lang/String;)V

    .line 35
    iget-object v0, p0, Lcom/itau/empresas/controller/SessaoController$1;->this$0:Lcom/itau/empresas/controller/SessaoController;

    iget-object v0, v0, Lcom/itau/empresas/controller/SessaoController;->menuController:Lcom/itau/empresas/feature/login/controller/MenuController;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/login/controller/MenuController;->carregarMenu()V

    .line 36
    iget-object v0, p0, Lcom/itau/empresas/controller/SessaoController$1;->this$0:Lcom/itau/empresas/controller/SessaoController;

    iget-object v0, v0, Lcom/itau/empresas/controller/SessaoController;->app:Lcom/itau/empresas/CustomApplication;

    iget-object v1, p0, Lcom/itau/empresas/controller/SessaoController$1;->val$conta:Lcom/itau/empresas/api/model/ContaOperadorVO;

    invoke-virtual {v0, v1}, Lcom/itau/empresas/CustomApplication;->setContaSelecionada(Lcom/itau/empresas/api/model/ContaOperadorVO;)V

    .line 37
    iget-object v0, p0, Lcom/itau/empresas/controller/SessaoController$1;->val$conta:Lcom/itau/empresas/api/model/ContaOperadorVO;

    invoke-static {v0}, Lcom/itau/empresas/controller/BaseController;->post(Ljava/lang/Object;)V

    .line 38
    return-void
.end method

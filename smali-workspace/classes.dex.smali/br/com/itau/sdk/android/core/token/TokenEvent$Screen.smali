.class public final enum Lbr/com/itau/sdk/android/core/token/TokenEvent$Screen;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lbr/com/itau/sdk/android/core/token/TokenEvent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Screen"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<Lbr/com/itau/sdk/android/core/token/TokenEvent$Screen;>;"
    }
.end annotation


# static fields
.field private static final $:[B

.field private static $$:I

.field private static final synthetic $VALUES:[Lbr/com/itau/sdk/android/core/token/TokenEvent$Screen;

.field public static final enum PASSWORD:Lbr/com/itau/sdk/android/core/token/TokenEvent$Screen;

.field public static final enum TOKEN:Lbr/com/itau/sdk/android/core/token/TokenEvent$Screen;


# direct methods
.method private static $(III)Ljava/lang/String;
    .registers 9

    const/4 v4, 0x0

    new-instance v0, Ljava/lang/String;

    add-int/lit8 p0, p0, 0x4

    sget-object v5, Lbr/com/itau/sdk/android/core/token/TokenEvent$Screen;->$:[B

    mul-int/lit8 p2, p2, 0x3

    add-int/lit8 p2, p2, 0x5

    mul-int/lit8 p1, p1, 0x4

    rsub-int/lit8 p1, p1, 0x54

    new-array v1, p2, [B

    add-int/lit8 p2, p2, -0x1

    if-nez v5, :cond_1b

    move v2, p1

    move v3, p2

    :goto_17
    neg-int v3, v3

    add-int/2addr v2, v3

    add-int/lit8 p1, v2, 0x1

    :cond_1b
    add-int/lit8 p0, p0, 0x1

    int-to-byte v2, p1

    aput-byte v2, v1, v4

    if-ne v4, p2, :cond_27

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Ljava/lang/String;-><init>([BI)V

    return-object v0

    :cond_27
    move v2, p1

    aget-byte v3, v5, p0

    add-int/lit8 v4, v4, 0x1

    goto :goto_17
.end method

.method static constructor <clinit>()V
    .registers 4

    .line 68
    const/16 v0, 0xf

    new-array v0, v0, [B

    fill-array-data v0, :array_4c

    sput-object v0, Lbr/com/itau/sdk/android/core/token/TokenEvent$Screen;->$:[B

    const/16 v0, 0x53

    sput v0, Lbr/com/itau/sdk/android/core/token/TokenEvent$Screen;->$$:I

    new-instance v0, Lbr/com/itau/sdk/android/core/token/TokenEvent$Screen;

    sget-object v1, Lbr/com/itau/sdk/android/core/token/TokenEvent$Screen;->$:[B

    const/16 v2, 0xb

    aget-byte v1, v1, v2

    neg-int v1, v1

    sget-object v2, Lbr/com/itau/sdk/android/core/token/TokenEvent$Screen;->$:[B

    const/16 v3, 0xa

    aget-byte v2, v2, v3

    invoke-static {v1, v2, v2}, Lbr/com/itau/sdk/android/core/token/TokenEvent$Screen;->$(III)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lbr/com/itau/sdk/android/core/token/TokenEvent$Screen;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbr/com/itau/sdk/android/core/token/TokenEvent$Screen;->PASSWORD:Lbr/com/itau/sdk/android/core/token/TokenEvent$Screen;

    .line 73
    new-instance v0, Lbr/com/itau/sdk/android/core/token/TokenEvent$Screen;

    sget-object v1, Lbr/com/itau/sdk/android/core/token/TokenEvent$Screen;->$:[B

    const/16 v2, 0xa

    aget-byte v1, v1, v2

    neg-int v1, v1

    add-int/lit8 v2, v1, 0x1

    invoke-static {v1, v2, v2}, Lbr/com/itau/sdk/android/core/token/TokenEvent$Screen;->$(III)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lbr/com/itau/sdk/android/core/token/TokenEvent$Screen;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbr/com/itau/sdk/android/core/token/TokenEvent$Screen;->TOKEN:Lbr/com/itau/sdk/android/core/token/TokenEvent$Screen;

    .line 64
    const/4 v0, 0x2

    new-array v0, v0, [Lbr/com/itau/sdk/android/core/token/TokenEvent$Screen;

    sget-object v1, Lbr/com/itau/sdk/android/core/token/TokenEvent$Screen;->PASSWORD:Lbr/com/itau/sdk/android/core/token/TokenEvent$Screen;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sget-object v1, Lbr/com/itau/sdk/android/core/token/TokenEvent$Screen;->TOKEN:Lbr/com/itau/sdk/android/core/token/TokenEvent$Screen;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    sput-object v0, Lbr/com/itau/sdk/android/core/token/TokenEvent$Screen;->$VALUES:[Lbr/com/itau/sdk/android/core/token/TokenEvent$Screen;

    return-void

    nop

    :array_4c
    .array-data 1
        0x2bt
        -0x4at
        -0x28t
        0x41t
        0x6t
        0x5t
        0x7t
        -0x8t
        0x10t
        -0x11t
        0x1t
        -0x3t
        0x9t
        -0x2t
        0xft
    .end array-data
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 64
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lbr/com/itau/sdk/android/core/token/TokenEvent$Screen;
    .registers 2

    .line 64
    const-class v0, Lbr/com/itau/sdk/android/core/token/TokenEvent$Screen;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lbr/com/itau/sdk/android/core/token/TokenEvent$Screen;

    return-object v0
.end method

.method public static values()[Lbr/com/itau/sdk/android/core/token/TokenEvent$Screen;
    .registers 1

    .line 64
    sget-object v0, Lbr/com/itau/sdk/android/core/token/TokenEvent$Screen;->$VALUES:[Lbr/com/itau/sdk/android/core/token/TokenEvent$Screen;

    invoke-virtual {v0}, [Lbr/com/itau/sdk/android/core/token/TokenEvent$Screen;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lbr/com/itau/sdk/android/core/token/TokenEvent$Screen;

    return-object v0
.end method

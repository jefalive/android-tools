.class public Lcom/itau/empresas/feature/recarga/contatos/adapter/ContatosRecargaAdapter;
.super Landroid/widget/BaseAdapter;
.source "ContatosRecargaAdapter.java"

# interfaces
.implements Landroid/widget/Filterable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/itau/empresas/feature/recarga/contatos/adapter/ContatosRecargaAdapter$WidgetFilter;
    }
.end annotation


# instance fields
.field activity:Lcom/itau/empresas/ui/activity/BaseActivity;

.field contatoRecargaController:Lcom/itau/empresas/feature/recarga/contatos/controller/ContatoRecargaController;

.field private listaFiltrada:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;>;"
        }
    .end annotation
.end field

.field private mContatosRecarga:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 23
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    return-void
.end method

.method static synthetic access$002(Lcom/itau/empresas/feature/recarga/contatos/adapter/ContatosRecargaAdapter;Ljava/util/List;)Ljava/util/List;
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/feature/recarga/contatos/adapter/ContatosRecargaAdapter;
    .param p1, "x1"    # Ljava/util/List;

    .line 23
    iput-object p1, p0, Lcom/itau/empresas/feature/recarga/contatos/adapter/ContatosRecargaAdapter;->mContatosRecarga:Ljava/util/List;

    return-object p1
.end method


# virtual methods
.method public getCount()I
    .registers 2

    .line 43
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/adapter/ContatosRecargaAdapter;->mContatosRecarga:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getFilter()Landroid/widget/Filter;
    .registers 3

    .line 66
    new-instance v0, Lcom/itau/empresas/feature/recarga/contatos/adapter/ContatosRecargaAdapter$1;

    iget-object v1, p0, Lcom/itau/empresas/feature/recarga/contatos/adapter/ContatosRecargaAdapter;->listaFiltrada:Ljava/util/List;

    invoke-direct {v0, p0, v1}, Lcom/itau/empresas/feature/recarga/contatos/adapter/ContatosRecargaAdapter$1;-><init>(Lcom/itau/empresas/feature/recarga/contatos/adapter/ContatosRecargaAdapter;Ljava/util/List;)V

    return-object v0
.end method

.method public getItem(I)Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;
    .registers 3
    .param p1, "i"    # I

    .line 61
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/adapter/ContatosRecargaAdapter;->mContatosRecarga:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;

    return-object v0
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .registers 3

    .line 22
    invoke-virtual {p0, p1}, Lcom/itau/empresas/feature/recarga/contatos/adapter/ContatosRecargaAdapter;->getItem(I)Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .registers 4
    .param p1, "i"    # I

    .line 38
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 8
    .param p1, "i"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "viewGroup"    # Landroid/view/ViewGroup;

    .line 49
    if-nez p2, :cond_9

    .line 50
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/adapter/ContatosRecargaAdapter;->activity:Lcom/itau/empresas/ui/activity/BaseActivity;

    invoke-static {v0}, Lcom/itau/empresas/feature/recarga/contatos/customview/ItemContatoRecargaView_;->build(Landroid/content/Context;)Lcom/itau/empresas/feature/recarga/contatos/customview/ItemContatoRecargaView;

    move-result-object v2

    .local v2, "view":Landroid/view/View;
    goto :goto_a

    .line 52
    .end local v2    # "view":Landroid/view/View;
    :cond_9
    move-object v2, p2

    .line 54
    .local v2, "view":Landroid/view/View;
    :goto_a
    invoke-virtual {p0, p1}, Lcom/itau/empresas/feature/recarga/contatos/adapter/ContatosRecargaAdapter;->getItem(I)Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;

    move-result-object v3

    .line 56
    .local v3, "contatoRecarga":Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;
    move-object v0, v2

    check-cast v0, Lcom/itau/empresas/feature/recarga/contatos/customview/ItemContatoRecargaView;

    const/4 v1, 0x0

    invoke-virtual {v0, v3, v1}, Lcom/itau/empresas/feature/recarga/contatos/customview/ItemContatoRecargaView;->ajustaDados(Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;Z)Lcom/itau/empresas/feature/recarga/contatos/customview/ItemContatoRecargaView;

    move-result-object v0

    return-object v0
.end method

.method public setItems(Ljava/util/List;)V
    .registers 3
    .param p1, "contatosRecargaVO"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;>;)V"
        }
    .end annotation

    .line 32
    iput-object p1, p0, Lcom/itau/empresas/feature/recarga/contatos/adapter/ContatosRecargaAdapter;->mContatosRecarga:Ljava/util/List;

    .line 33
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/adapter/ContatosRecargaAdapter;->mContatosRecarga:Ljava/util/List;

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/adapter/ContatosRecargaAdapter;->listaFiltrada:Ljava/util/List;

    .line 34
    return-void
.end method

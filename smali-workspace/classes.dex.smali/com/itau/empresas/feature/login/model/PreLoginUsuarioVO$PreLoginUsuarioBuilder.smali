.class public Lcom/itau/empresas/feature/login/model/PreLoginUsuarioVO$PreLoginUsuarioBuilder;
.super Ljava/lang/Object;
.source "PreLoginUsuarioVO.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/itau/empresas/feature/login/model/PreLoginUsuarioVO;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "PreLoginUsuarioBuilder"
.end annotation


# instance fields
.field private agencia:Ljava/lang/String;

.field private conta:Ljava/lang/String;

.field private dac:Ljava/lang/String;

.field private nrDocumento:Ljava/lang/String;

.field private operador:Ljava/lang/String;

.field private tipoDocumento:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public agencia(Ljava/lang/String;)Lcom/itau/empresas/feature/login/model/PreLoginUsuarioVO$PreLoginUsuarioBuilder;
    .registers 2
    .param p1, "agencia"    # Ljava/lang/String;

    .line 70
    iput-object p1, p0, Lcom/itau/empresas/feature/login/model/PreLoginUsuarioVO$PreLoginUsuarioBuilder;->agencia:Ljava/lang/String;

    .line 71
    return-object p0
.end method

.method public build()Lcom/itau/empresas/feature/login/model/PreLoginUsuarioVO;
    .registers 8

    .line 100
    new-instance v0, Lcom/itau/empresas/feature/login/model/PreLoginUsuarioVO;

    iget-object v1, p0, Lcom/itau/empresas/feature/login/model/PreLoginUsuarioVO$PreLoginUsuarioBuilder;->agencia:Ljava/lang/String;

    iget-object v2, p0, Lcom/itau/empresas/feature/login/model/PreLoginUsuarioVO$PreLoginUsuarioBuilder;->conta:Ljava/lang/String;

    iget-object v3, p0, Lcom/itau/empresas/feature/login/model/PreLoginUsuarioVO$PreLoginUsuarioBuilder;->dac:Ljava/lang/String;

    iget-object v4, p0, Lcom/itau/empresas/feature/login/model/PreLoginUsuarioVO$PreLoginUsuarioBuilder;->tipoDocumento:Ljava/lang/String;

    iget-object v5, p0, Lcom/itau/empresas/feature/login/model/PreLoginUsuarioVO$PreLoginUsuarioBuilder;->nrDocumento:Ljava/lang/String;

    iget-object v6, p0, Lcom/itau/empresas/feature/login/model/PreLoginUsuarioVO$PreLoginUsuarioBuilder;->operador:Ljava/lang/String;

    if-nez v6, :cond_13

    const-string v6, ""

    goto :goto_15

    :cond_13
    iget-object v6, p0, Lcom/itau/empresas/feature/login/model/PreLoginUsuarioVO$PreLoginUsuarioBuilder;->operador:Ljava/lang/String;

    :goto_15
    invoke-direct/range {v0 .. v6}, Lcom/itau/empresas/feature/login/model/PreLoginUsuarioVO;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public conta(Ljava/lang/String;)Lcom/itau/empresas/feature/login/model/PreLoginUsuarioVO$PreLoginUsuarioBuilder;
    .registers 2
    .param p1, "conta"    # Ljava/lang/String;

    .line 75
    iput-object p1, p0, Lcom/itau/empresas/feature/login/model/PreLoginUsuarioVO$PreLoginUsuarioBuilder;->conta:Ljava/lang/String;

    .line 76
    return-object p0
.end method

.method public dac(Ljava/lang/String;)Lcom/itau/empresas/feature/login/model/PreLoginUsuarioVO$PreLoginUsuarioBuilder;
    .registers 2
    .param p1, "dac"    # Ljava/lang/String;

    .line 80
    iput-object p1, p0, Lcom/itau/empresas/feature/login/model/PreLoginUsuarioVO$PreLoginUsuarioBuilder;->dac:Ljava/lang/String;

    .line 81
    return-object p0
.end method

.method public numeroDocumento(Ljava/lang/String;)Lcom/itau/empresas/feature/login/model/PreLoginUsuarioVO$PreLoginUsuarioBuilder;
    .registers 2
    .param p1, "nrDocumento"    # Ljava/lang/String;

    .line 95
    iput-object p1, p0, Lcom/itau/empresas/feature/login/model/PreLoginUsuarioVO$PreLoginUsuarioBuilder;->nrDocumento:Ljava/lang/String;

    .line 96
    return-object p0
.end method

.method public operador(Ljava/lang/String;)Lcom/itau/empresas/feature/login/model/PreLoginUsuarioVO$PreLoginUsuarioBuilder;
    .registers 2
    .param p1, "operador"    # Ljava/lang/String;

    .line 85
    iput-object p1, p0, Lcom/itau/empresas/feature/login/model/PreLoginUsuarioVO$PreLoginUsuarioBuilder;->operador:Ljava/lang/String;

    .line 86
    return-object p0
.end method

.method public tipoDocumento(Ljava/lang/String;)Lcom/itau/empresas/feature/login/model/PreLoginUsuarioVO$PreLoginUsuarioBuilder;
    .registers 2
    .param p1, "tipoDocumento"    # Ljava/lang/String;

    .line 90
    iput-object p1, p0, Lcom/itau/empresas/feature/login/model/PreLoginUsuarioVO$PreLoginUsuarioBuilder;->tipoDocumento:Ljava/lang/String;

    .line 91
    return-object p0
.end method

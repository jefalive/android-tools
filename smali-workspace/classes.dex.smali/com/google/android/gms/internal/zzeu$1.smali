.class Lcom/google/android/gms/internal/zzeu$1;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/gms/internal/zzeu;->zzc(Ljava/util/List;)Lcom/google/android/gms/internal/zzes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;Ljava/util/concurrent/Callable<Lcom/google/android/gms/internal/zzes;>;"
    }
.end annotation


# instance fields
.field final synthetic zzCA:Lcom/google/android/gms/internal/zzer;

.field final synthetic zzCB:Lcom/google/android/gms/internal/zzeu;


# direct methods
.method constructor <init>(Lcom/google/android/gms/internal/zzeu;Lcom/google/android/gms/internal/zzer;)V
    .registers 3

    iput-object p1, p0, Lcom/google/android/gms/internal/zzeu$1;->zzCB:Lcom/google/android/gms/internal/zzeu;

    iput-object p2, p0, Lcom/google/android/gms/internal/zzeu$1;->zzCA:Lcom/google/android/gms/internal/zzer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public synthetic call()Ljava/lang/Object;
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/gms/internal/zzeu$1;->zzeE()Lcom/google/android/gms/internal/zzes;

    move-result-object v0

    return-object v0
.end method

.method public zzeE()Lcom/google/android/gms/internal/zzes;
    .registers 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/gms/internal/zzeu$1;->zzCB:Lcom/google/android/gms/internal/zzeu;

    invoke-static {v0}, Lcom/google/android/gms/internal/zzeu;->zza(Lcom/google/android/gms/internal/zzeu;)Ljava/lang/Object;

    move-result-object v5

    monitor-enter v5

    :try_start_7
    iget-object v0, p0, Lcom/google/android/gms/internal/zzeu$1;->zzCB:Lcom/google/android/gms/internal/zzeu;

    invoke-static {v0}, Lcom/google/android/gms/internal/zzeu;->zzb(Lcom/google/android/gms/internal/zzeu;)Z
    :try_end_c
    .catchall {:try_start_7 .. :try_end_c} :catchall_14

    move-result v0

    if-eqz v0, :cond_12

    monitor-exit v5

    const/4 v0, 0x0

    return-object v0

    :cond_12
    monitor-exit v5

    goto :goto_17

    :catchall_14
    move-exception v6

    monitor-exit v5

    throw v6

    :goto_17
    iget-object v0, p0, Lcom/google/android/gms/internal/zzeu$1;->zzCA:Lcom/google/android/gms/internal/zzer;

    iget-object v1, p0, Lcom/google/android/gms/internal/zzeu$1;->zzCB:Lcom/google/android/gms/internal/zzeu;

    invoke-static {v1}, Lcom/google/android/gms/internal/zzeu;->zzc(Lcom/google/android/gms/internal/zzeu;)J

    move-result-wide v1

    iget-object v3, p0, Lcom/google/android/gms/internal/zzeu$1;->zzCB:Lcom/google/android/gms/internal/zzeu;

    invoke-static {v3}, Lcom/google/android/gms/internal/zzeu;->zzd(Lcom/google/android/gms/internal/zzeu;)J

    move-result-wide v3

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/gms/internal/zzer;->zza(JJ)Lcom/google/android/gms/internal/zzes;

    move-result-object v0

    return-object v0
.end method

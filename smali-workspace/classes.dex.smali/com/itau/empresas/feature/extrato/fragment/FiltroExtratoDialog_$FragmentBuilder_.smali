.class public Lcom/itau/empresas/feature/extrato/fragment/FiltroExtratoDialog_$FragmentBuilder_;
.super Lorg/androidannotations/api/builder/FragmentBuilder;
.source "FiltroExtratoDialog_.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/itau/empresas/feature/extrato/fragment/FiltroExtratoDialog_;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "FragmentBuilder_"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/androidannotations/api/builder/FragmentBuilder<Lcom/itau/empresas/feature/extrato/fragment/FiltroExtratoDialog_$FragmentBuilder_;Lcom/itau/empresas/feature/extrato/fragment/FiltroExtratoDialog;>;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 129
    invoke-direct {p0}, Lorg/androidannotations/api/builder/FragmentBuilder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/itau/empresas/feature/extrato/fragment/FiltroExtratoDialog;
    .registers 3

    .line 135
    new-instance v1, Lcom/itau/empresas/feature/extrato/fragment/FiltroExtratoDialog_;

    invoke-direct {v1}, Lcom/itau/empresas/feature/extrato/fragment/FiltroExtratoDialog_;-><init>()V

    .line 136
    .local v1, "fragment_":Lcom/itau/empresas/feature/extrato/fragment/FiltroExtratoDialog_;
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/FiltroExtratoDialog_$FragmentBuilder_;->args:Landroid/os/Bundle;

    invoke-virtual {v1, v0}, Lcom/itau/empresas/feature/extrato/fragment/FiltroExtratoDialog_;->setArguments(Landroid/os/Bundle;)V

    .line 137
    return-object v1
.end method

.method public quantidadeDias(I)Lcom/itau/empresas/feature/extrato/fragment/FiltroExtratoDialog_$FragmentBuilder_;
    .registers 4
    .param p1, "quantidadeDias"    # I

    .line 147
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/FiltroExtratoDialog_$FragmentBuilder_;->args:Landroid/os/Bundle;

    const-string v1, "quantidadeDias"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 148
    return-object p0
.end method

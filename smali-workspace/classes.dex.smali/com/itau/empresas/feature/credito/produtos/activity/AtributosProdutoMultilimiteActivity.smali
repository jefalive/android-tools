.class public Lcom/itau/empresas/feature/credito/produtos/activity/AtributosProdutoMultilimiteActivity;
.super Lcom/itau/empresas/ui/activity/BaseActivity;
.source "AtributosProdutoMultilimiteActivity.java"


# instance fields
.field adapter:Lcom/itau/empresas/adapter/DetalhesMultilimiteAdapter;

.field atributoVOs:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<Lcom/itau/empresas/feature/credito/produtos/model/DetalhesMultilimiteAtributoVO;>;"
        }
    .end annotation
.end field

.field lista:Landroid/widget/ListView;

.field produtosController:Lcom/itau/empresas/controller/ProdutosController;

.field titulo:Ljava/lang/String;

.field toolBar:Landroid/support/v7/widget/Toolbar;


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 24
    invoke-direct {p0}, Lcom/itau/empresas/ui/activity/BaseActivity;-><init>()V

    return-void
.end method

.method public static intent(Landroid/content/Context;)Lcom/itau/empresas/feature/credito/produtos/activity/AtributosProdutoMultilimiteActivity_$IntentBuilder_;
    .registers 2
    .param p0, "context"    # Landroid/content/Context;

    .line 60
    new-instance v0, Lcom/itau/empresas/feature/credito/produtos/activity/AtributosProdutoMultilimiteActivity_$IntentBuilder_;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/credito/produtos/activity/AtributosProdutoMultilimiteActivity_$IntentBuilder_;-><init>(Landroid/content/Context;)V

    return-object v0
.end method


# virtual methods
.method aoIniciarActivity()V
    .registers 4

    .line 46
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/activity/AtributosProdutoMultilimiteActivity;->toolBar:Landroid/support/v7/widget/Toolbar;

    const v1, 0x7f0e05c2

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Landroid/widget/TextView;

    .line 47
    .local v2, "tituloToolbar":Landroid/widget/TextView;
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/activity/AtributosProdutoMultilimiteActivity;->titulo:Ljava/lang/String;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 49
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/activity/AtributosProdutoMultilimiteActivity;->adapter:Lcom/itau/empresas/adapter/DetalhesMultilimiteAdapter;

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/produtos/activity/AtributosProdutoMultilimiteActivity;->atributoVOs:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Lcom/itau/empresas/adapter/DetalhesMultilimiteAdapter;->setData(Ljava/util/List;)V

    .line 50
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/activity/AtributosProdutoMultilimiteActivity;->lista:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/produtos/activity/AtributosProdutoMultilimiteActivity;->adapter:Lcom/itau/empresas/adapter/DetalhesMultilimiteAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 51
    return-void
.end method

.method public opkeysConsumidas()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Ljava/lang/String;>;"
        }
    .end annotation

    .line 56
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    return-object v0
.end method

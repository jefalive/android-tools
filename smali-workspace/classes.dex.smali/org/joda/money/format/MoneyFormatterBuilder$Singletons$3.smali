.class final enum Lorg/joda/money/format/MoneyFormatterBuilder$Singletons$3;
.super Lorg/joda/money/format/MoneyFormatterBuilder$Singletons;
.source "MoneyFormatterBuilder.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/joda/money/format/MoneyFormatterBuilder$Singletons;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4008
    name = null
.end annotation


# direct methods
.method constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .registers 5
    .param p3, "x0"    # Ljava/lang/String;

    .line 377
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lorg/joda/money/format/MoneyFormatterBuilder$Singletons;-><init>(Ljava/lang/String;ILjava/lang/String;Lorg/joda/money/format/MoneyFormatterBuilder$1;)V

    return-void
.end method


# virtual methods
.method public parse(Lorg/joda/money/format/MoneyParseContext;)V
    .registers 8
    .param p1, "context"    # Lorg/joda/money/format/MoneyParseContext;

    .line 384
    const/4 v2, 0x0

    .line 385
    .local v2, "count":I
    :goto_1
    const/4 v0, 0x3

    if-ge v2, v0, :cond_28

    invoke-virtual {p1}, Lorg/joda/money/format/MoneyParseContext;->getIndex()I

    move-result v0

    add-int/2addr v0, v2

    invoke-virtual {p1}, Lorg/joda/money/format/MoneyParseContext;->getTextLength()I

    move-result v1

    if-ge v0, v1, :cond_28

    .line 386
    invoke-virtual {p1}, Lorg/joda/money/format/MoneyParseContext;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p1}, Lorg/joda/money/format/MoneyParseContext;->getIndex()I

    move-result v1

    add-int/2addr v1, v2

    invoke-interface {v0, v1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v3

    .line 387
    .local v3, "ch":C
    const/16 v0, 0x30

    if-lt v3, v0, :cond_28

    const/16 v0, 0x39

    if-le v3, v0, :cond_25

    .line 388
    goto :goto_28

    .line 385
    .end local v3    # "ch":C
    :cond_25
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 391
    :cond_28
    :goto_28
    invoke-virtual {p1}, Lorg/joda/money/format/MoneyParseContext;->getIndex()I

    move-result v0

    add-int v3, v0, v2

    .line 392
    .local v3, "endPos":I
    invoke-virtual {p1}, Lorg/joda/money/format/MoneyParseContext;->getIndex()I

    move-result v0

    invoke-virtual {p1, v0, v3}, Lorg/joda/money/format/MoneyParseContext;->getTextSubstring(II)Ljava/lang/String;

    move-result-object v4

    .line 394
    .local v4, "code":Ljava/lang/String;
    :try_start_36
    invoke-static {v4}, Lorg/joda/money/CurrencyUnit;->ofNumericCode(Ljava/lang/String;)Lorg/joda/money/CurrencyUnit;

    move-result-object v0

    invoke-virtual {p1, v0}, Lorg/joda/money/format/MoneyParseContext;->setCurrency(Lorg/joda/money/CurrencyUnit;)V

    .line 395
    invoke-virtual {p1, v3}, Lorg/joda/money/format/MoneyParseContext;->setIndex(I)V
    :try_end_40
    .catch Lorg/joda/money/IllegalCurrencyException; {:try_start_36 .. :try_end_40} :catch_41

    .line 398
    goto :goto_45

    .line 396
    :catch_41
    move-exception v5

    .line 397
    .local v5, "ex":Lorg/joda/money/IllegalCurrencyException;
    invoke-virtual {p1}, Lorg/joda/money/format/MoneyParseContext;->setError()V

    .line 399
    .end local v5    # "ex":Lorg/joda/money/IllegalCurrencyException;
    :goto_45
    return-void
.end method

.method public print(Lorg/joda/money/format/MoneyPrintContext;Ljava/lang/Appendable;Lorg/joda/money/BigMoney;)V
    .registers 5
    .param p1, "context"    # Lorg/joda/money/format/MoneyPrintContext;
    .param p2, "appendable"    # Ljava/lang/Appendable;
    .param p3, "money"    # Lorg/joda/money/BigMoney;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 380
    invoke-virtual {p3}, Lorg/joda/money/BigMoney;->getCurrencyUnit()Lorg/joda/money/CurrencyUnit;

    move-result-object v0

    invoke-virtual {v0}, Lorg/joda/money/CurrencyUnit;->getNumericCode()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p2, v0}, Ljava/lang/Appendable;->append(Ljava/lang/CharSequence;)Ljava/lang/Appendable;

    .line 381
    return-void
.end method

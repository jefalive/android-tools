.class Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity$2;
.super Ljava/lang/Object;
.source "PagamentoDarfActivity.java"

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->carregaElementosDeTela()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;


# direct methods
.method constructor <init>(Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;)V
    .registers 2
    .param p1, "this$0"    # Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;

    .line 171
    iput-object p1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity$2;->this$0:Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFocusChange(Landroid/view/View;Z)V
    .registers 5
    .param p1, "view"    # Landroid/view/View;
    .param p2, "hasFocus"    # Z

    .line 174
    if-nez p2, :cond_14

    .line 175
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity$2;->this$0:Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;

    iget-object v1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity$2;->this$0:Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;

    iget-object v1, v1, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->campoCpfCnpjContribuinte:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    # invokes: Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->cpfCnpjInvalido(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->access$200(Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;Ljava/lang/String;)V

    goto :goto_19

    .line 177
    :cond_14
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity$2;->this$0:Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;

    # invokes: Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->removeHint()V
    invoke-static {v0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->access$300(Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;)V

    .line 179
    :goto_19
    return-void
.end method

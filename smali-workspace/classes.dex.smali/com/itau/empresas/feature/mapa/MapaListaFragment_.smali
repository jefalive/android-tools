.class public final Lcom/itau/empresas/feature/mapa/MapaListaFragment_;
.super Lcom/itau/empresas/feature/mapa/MapaListaFragment;
.source "MapaListaFragment_.java"

# interfaces
.implements Lorg/androidannotations/api/view/HasViews;
.implements Lorg/androidannotations/api/view/OnViewChangedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/itau/empresas/feature/mapa/MapaListaFragment_$FragmentBuilder_;
    }
.end annotation


# instance fields
.field private contentView_:Landroid/view/View;

.field private final onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;


# direct methods
.method public constructor <init>()V
    .registers 2

    .line 24
    invoke-direct {p0}, Lcom/itau/empresas/feature/mapa/MapaListaFragment;-><init>()V

    .line 28
    new-instance v0, Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-direct {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaListaFragment_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    return-void
.end method

.method public static builder()Lcom/itau/empresas/feature/mapa/MapaListaFragment_$FragmentBuilder_;
    .registers 1

    .line 76
    new-instance v0, Lcom/itau/empresas/feature/mapa/MapaListaFragment_$FragmentBuilder_;

    invoke-direct {v0}, Lcom/itau/empresas/feature/mapa/MapaListaFragment_$FragmentBuilder_;-><init>()V

    return-object v0
.end method

.method private init_(Landroid/os/Bundle;)V
    .registers 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 65
    invoke-static {p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->registerOnViewChangedListener(Lorg/androidannotations/api/view/OnViewChangedListener;)V

    .line 66
    invoke-static {}, Lcom/itau/empresas/CustomApplication_;->getInstance()Lcom/itau/empresas/CustomApplication;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaListaFragment_;->application:Lcom/itau/empresas/CustomApplication;

    .line 67
    return-void
.end method


# virtual methods
.method public findViewById(I)Landroid/view/View;
    .registers 3
    .param p1, "id"    # I

    .line 41
    iget-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaListaFragment_;->contentView_:Landroid/view/View;

    if-nez v0, :cond_6

    .line 42
    const/4 v0, 0x0

    return-object v0

    .line 44
    :cond_6
    iget-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaListaFragment_;->contentView_:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 33
    iget-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaListaFragment_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-static {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    move-result-object v1

    .line 34
    .local v1, "previousNotifier":Lorg/androidannotations/api/view/OnViewChangedNotifier;
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/mapa/MapaListaFragment_;->init_(Landroid/os/Bundle;)V

    .line 35
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/mapa/MapaListaFragment;->onCreate(Landroid/os/Bundle;)V

    .line 36
    invoke-static {v1}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 37
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .registers 6
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .line 49
    invoke-super {p0, p1, p2, p3}, Lcom/itau/empresas/feature/mapa/MapaListaFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaListaFragment_;->contentView_:Landroid/view/View;

    .line 50
    iget-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaListaFragment_;->contentView_:Landroid/view/View;

    if-nez v0, :cond_14

    .line 51
    const v0, 0x7f0300b4

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaListaFragment_;->contentView_:Landroid/view/View;

    .line 53
    :cond_14
    iget-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaListaFragment_;->contentView_:Landroid/view/View;

    return-object v0
.end method

.method public onDestroyView()V
    .registers 2

    .line 58
    invoke-super {p0}, Lcom/itau/empresas/feature/mapa/MapaListaFragment;->onDestroyView()V

    .line 59
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaListaFragment_;->contentView_:Landroid/view/View;

    .line 60
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaListaFragment_;->listaAgencias:Landroid/widget/ListView;

    .line 61
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaListaFragment_;->textoSemAgencias:Landroid/widget/TextView;

    .line 62
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .registers 4
    .param p1, "item"    # Landroid/view/MenuItem;

    .line 88
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    .line 89
    .local v1, "itemId_":I
    const v0, 0x7f0e06c5

    if-ne v1, v0, :cond_e

    .line 90
    invoke-virtual {p0}, Lcom/itau/empresas/feature/mapa/MapaListaFragment_;->menuFilter()V

    .line 91
    const/4 v0, 0x1

    return v0

    .line 93
    :cond_e
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/mapa/MapaListaFragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0
.end method

.method public onViewChanged(Lorg/androidannotations/api/view/HasViews;)V
    .registers 3
    .param p1, "hasViews"    # Lorg/androidannotations/api/view/HasViews;

    .line 81
    const v0, 0x7f0e042e

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaListaFragment_;->listaAgencias:Landroid/widget/ListView;

    .line 82
    const v0, 0x7f0e042d

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaListaFragment_;->textoSemAgencias:Landroid/widget/TextView;

    .line 83
    invoke-virtual {p0}, Lcom/itau/empresas/feature/mapa/MapaListaFragment_;->carregarTela()V

    .line 84
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .registers 4
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;

    .line 71
    invoke-super {p0, p1, p2}, Lcom/itau/empresas/feature/mapa/MapaListaFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 72
    iget-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaListaFragment_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 73
    return-void
.end method

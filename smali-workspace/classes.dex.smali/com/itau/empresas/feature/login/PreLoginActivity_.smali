.class public final Lcom/itau/empresas/feature/login/PreLoginActivity_;
.super Lcom/itau/empresas/feature/login/PreLoginActivity;
.source "PreLoginActivity_.java"

# interfaces
.implements Lorg/androidannotations/api/view/HasViews;
.implements Lorg/androidannotations/api/view/OnViewChangedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/itau/empresas/feature/login/PreLoginActivity_$IntentBuilder_;
    }
.end annotation


# instance fields
.field private final onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;


# direct methods
.method public constructor <init>()V
    .registers 2

    .line 34
    invoke-direct {p0}, Lcom/itau/empresas/feature/login/PreLoginActivity;-><init>()V

    .line 38
    new-instance v0, Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-direct {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/feature/login/PreLoginActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    return-void
.end method

.method static synthetic access$001(Lcom/itau/empresas/feature/login/PreLoginActivity_;)V
    .registers 1
    .param p0, "x0"    # Lcom/itau/empresas/feature/login/PreLoginActivity_;

    .line 34
    invoke-super {p0}, Lcom/itau/empresas/feature/login/PreLoginActivity;->mostraDialogoDeProgresso()V

    return-void
.end method

.method static synthetic access$101(Lcom/itau/empresas/feature/login/PreLoginActivity_;)V
    .registers 1
    .param p0, "x0"    # Lcom/itau/empresas/feature/login/PreLoginActivity_;

    .line 34
    invoke-super {p0}, Lcom/itau/empresas/feature/login/PreLoginActivity;->escondeDialogoDeProgresso()V

    return-void
.end method

.method private init_(Landroid/os/Bundle;)V
    .registers 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 53
    new-instance v0, Lcom/itau/empresas/Preferences_;

    invoke-direct {v0, p0}, Lcom/itau/empresas/Preferences_;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/itau/empresas/feature/login/PreLoginActivity_;->prefs:Lcom/itau/empresas/Preferences_;

    .line 54
    invoke-static {p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->registerOnViewChangedListener(Lorg/androidannotations/api/view/OnViewChangedListener;)V

    .line 55
    invoke-static {}, Lcom/itau/empresas/CustomApplication_;->getInstance()Lcom/itau/empresas/CustomApplication;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/login/PreLoginActivity_;->application:Lcom/itau/empresas/CustomApplication;

    .line 56
    invoke-direct {p0}, Lcom/itau/empresas/feature/login/PreLoginActivity_;->injectExtras_()V

    .line 57
    invoke-virtual {p0}, Lcom/itau/empresas/feature/login/PreLoginActivity_;->afterInject()V

    .line 58
    return-void
.end method

.method private injectExtras_()V
    .registers 3

    .line 121
    invoke-virtual {p0}, Lcom/itau/empresas/feature/login/PreLoginActivity_;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 122
    .local v1, "extras_":Landroid/os/Bundle;
    if-eqz v1, :cond_3c

    .line 123
    const-string v0, "boletoPdfUri"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 124
    const-string v0, "boletoPdfUri"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/itau/empresas/feature/login/PreLoginActivity_;->boletoPdfUri:Landroid/net/Uri;

    .line 126
    :cond_1c
    const-string v0, "navegacaoVindoDoLogin"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2c

    .line 127
    const-string v0, "navegacaoVindoDoLogin"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/itau/empresas/feature/login/PreLoginActivity_;->navegacaoVindoDoLogin:Z

    .line 129
    :cond_2c
    const-string v0, "instalarIToken"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3c

    .line 130
    const-string v0, "instalarIToken"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/itau/empresas/feature/login/PreLoginActivity_;->instalarIToken:Z

    .line 133
    :cond_3c
    return-void
.end method

.method public static intent(Landroid/content/Context;)Lcom/itau/empresas/feature/login/PreLoginActivity_$IntentBuilder_;
    .registers 2
    .param p0, "context"    # Landroid/content/Context;

    .line 79
    new-instance v0, Lcom/itau/empresas/feature/login/PreLoginActivity_$IntentBuilder_;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/login/PreLoginActivity_$IntentBuilder_;-><init>(Landroid/content/Context;)V

    return-object v0
.end method


# virtual methods
.method public escondeDialogoDeProgresso()V
    .registers 5

    .line 158
    const-string v0, ""

    new-instance v1, Lcom/itau/empresas/feature/login/PreLoginActivity_$4;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/login/PreLoginActivity_$4;-><init>(Lcom/itau/empresas/feature/login/PreLoginActivity_;)V

    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3}, Lorg/androidannotations/api/UiThreadExecutor;->runTask(Ljava/lang/String;Ljava/lang/Runnable;J)V

    .line 166
    return-void
.end method

.method public mostraDialogoDeProgresso()V
    .registers 5
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "CommitTransaction"
        }
    .end annotation

    .line 146
    const-string v0, ""

    new-instance v1, Lcom/itau/empresas/feature/login/PreLoginActivity_$3;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/login/PreLoginActivity_$3;-><init>(Lcom/itau/empresas/feature/login/PreLoginActivity_;)V

    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3}, Lorg/androidannotations/api/UiThreadExecutor;->runTask(Ljava/lang/String;Ljava/lang/Runnable;J)V

    .line 154
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 45
    iget-object v0, p0, Lcom/itau/empresas/feature/login/PreLoginActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-static {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    move-result-object v1

    .line 46
    .local v1, "previousNotifier":Lorg/androidannotations/api/view/OnViewChangedNotifier;
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/login/PreLoginActivity_;->init_(Landroid/os/Bundle;)V

    .line 47
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/login/PreLoginActivity;->onCreate(Landroid/os/Bundle;)V

    .line 48
    invoke-static {v1}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 49
    const v0, 0x7f030059

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/login/PreLoginActivity_;->setContentView(I)V

    .line 50
    return-void
.end method

.method public onViewChanged(Lorg/androidannotations/api/view/HasViews;)V
    .registers 5
    .param p1, "hasViews"    # Lorg/androidannotations/api/view/HasViews;

    .line 92
    const v0, 0x7f0e05ba

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/SwitchCompat;

    iput-object v0, p0, Lcom/itau/empresas/feature/login/PreLoginActivity_;->senhaSalva:Landroid/support/v7/widget/SwitchCompat;

    .line 93
    const v0, 0x7f0e0297

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/itau/empresas/feature/login/PreLoginActivity_;->clPreLogin:Landroid/view/ViewGroup;

    .line 94
    const v0, 0x7f0e05bb

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 95
    .local v1, "view_botao_acessar":Landroid/view/View;
    const v0, 0x7f0e05bc

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 97
    .local v2, "view_link_abra_sua_conta":Landroid/view/View;
    if-eqz v1, :cond_2e

    .line 98
    new-instance v0, Lcom/itau/empresas/feature/login/PreLoginActivity_$1;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/login/PreLoginActivity_$1;-><init>(Lcom/itau/empresas/feature/login/PreLoginActivity_;)V

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 107
    :cond_2e
    if-eqz v2, :cond_38

    .line 108
    new-instance v0, Lcom/itau/empresas/feature/login/PreLoginActivity_$2;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/login/PreLoginActivity_$2;-><init>(Lcom/itau/empresas/feature/login/PreLoginActivity_;)V

    invoke-virtual {v2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 117
    :cond_38
    invoke-virtual {p0}, Lcom/itau/empresas/feature/login/PreLoginActivity_;->aoCriar()V

    .line 118
    return-void
.end method

.method public setContentView(I)V
    .registers 3
    .param p1, "layoutResID"    # I

    .line 62
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/login/PreLoginActivity;->setContentView(I)V

    .line 63
    iget-object v0, p0, Lcom/itau/empresas/feature/login/PreLoginActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 64
    return-void
.end method

.method public setContentView(Landroid/view/View;)V
    .registers 3
    .param p1, "view"    # Landroid/view/View;

    .line 74
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/login/PreLoginActivity;->setContentView(Landroid/view/View;)V

    .line 75
    iget-object v0, p0, Lcom/itau/empresas/feature/login/PreLoginActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 76
    return-void
.end method

.method public setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .registers 4
    .param p1, "view"    # Landroid/view/View;
    .param p2, "params"    # Landroid/view/ViewGroup$LayoutParams;

    .line 68
    invoke-super {p0, p1, p2}, Lcom/itau/empresas/feature/login/PreLoginActivity;->setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 69
    iget-object v0, p0, Lcom/itau/empresas/feature/login/PreLoginActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 70
    return-void
.end method

.method public setIntent(Landroid/content/Intent;)V
    .registers 2
    .param p1, "newIntent"    # Landroid/content/Intent;

    .line 137
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/login/PreLoginActivity;->setIntent(Landroid/content/Intent;)V

    .line 138
    invoke-direct {p0}, Lcom/itau/empresas/feature/login/PreLoginActivity_;->injectExtras_()V

    .line 139
    return-void
.end method

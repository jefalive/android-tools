.class public Lcom/itau/empresas/ui/view/LinksView;
.super Landroid/widget/RelativeLayout;
.source "LinksView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/itau/empresas/ui/view/LinksView$LinkViewException;
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 2
    .param p1, "context"    # Landroid/content/Context;

    .line 39
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 40
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .line 43
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 44
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .line 47
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 48
    return-void
.end method

.method private abreDialogDeInstalacaoDoItoken()V
    .registers 6

    .line 64
    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/LinksView;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 65
    .local v2, "context":Landroid/content/Context;
    instance-of v0, v2, Landroid/support/v7/app/AppCompatActivity;

    if-nez v0, :cond_e

    .line 66
    new-instance v0, Lcom/itau/empresas/ui/view/LinksView$LinkViewException;

    invoke-direct {v0}, Lcom/itau/empresas/ui/view/LinksView$LinkViewException;-><init>()V

    throw v0

    .line 67
    :cond_e
    move-object v3, v2

    check-cast v3, Landroid/support/v7/app/AppCompatActivity;

    .line 69
    .local v3, "activity":Landroid/support/v7/app/AppCompatActivity;
    invoke-static {v2}, Lcom/itau/empresas/feature/itoken/ITokenNaoInstaladoDialogFragment;->newInstace(Landroid/content/Context;)Lcom/itau/empresas/feature/itoken/ITokenNaoInstaladoDialogFragment;

    move-result-object v4

    .line 72
    .local v4, "dialog":Lcom/itau/empresas/feature/itoken/ITokenNaoInstaladoDialogFragment;
    new-instance v0, Lcom/itau/empresas/ui/view/LinksView$1;

    invoke-direct {v0, p0, v2}, Lcom/itau/empresas/ui/view/LinksView$1;-><init>(Lcom/itau/empresas/ui/view/LinksView;Landroid/content/Context;)V

    invoke-virtual {v4, v0}, Lcom/itau/empresas/feature/itoken/ITokenNaoInstaladoDialogFragment;->setListener(Lcom/itau/empresas/feature/itoken/ITokenNaoInstaladoDialogFragment$Listener;)V

    .line 78
    invoke-virtual {v3}, Landroid/support/v7/app/AppCompatActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const-string v1, "itokenDialog"

    invoke-virtual {v4, v0, v1}, Lcom/itau/empresas/feature/itoken/ITokenNaoInstaladoDialogFragment;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 79
    return-void
.end method

.method private abreInstaladorDoItoken(Landroid/content/Context;)V
    .registers 5
    .param p1, "context"    # Landroid/content/Context;

    .line 82
    const/4 v2, 0x0

    .line 83
    .local v2, "installIntent":Landroid/content/Intent;
    instance-of v0, p1, Lcom/itau/empresas/feature/login/PreLoginActivity;

    if-eqz v0, :cond_e

    .line 84
    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/LinksView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/itau/empresas/feature/login/PreLoginActivity;->intentItoken(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v2

    goto :goto_1a

    .line 85
    :cond_e
    instance-of v0, p1, Lcom/itau/empresas/feature/login/LoginActivity;

    if-eqz v0, :cond_1a

    .line 86
    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/LinksView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/itau/empresas/feature/login/LoginActivity;->intentItoken(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v2

    .line 87
    :cond_1a
    :goto_1a
    if-nez v2, :cond_29

    .line 88
    const-string v0, "LinksView"

    const-string v1, "abreDialogDeInstalacaoDoItoken: LinksView foi utilizada fora das telas de login"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 90
    new-instance v0, Lcom/itau/empresas/ui/view/LinksView$LinkViewException;

    invoke-direct {v0}, Lcom/itau/empresas/ui/view/LinksView$LinkViewException;-><init>()V

    throw v0

    .line 92
    :cond_29
    invoke-virtual {p1, v2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 93
    return-void
.end method

.method static synthetic access$000(Lcom/itau/empresas/ui/view/LinksView;Landroid/content/Context;)V
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/ui/view/LinksView;
    .param p1, "x1"    # Landroid/content/Context;

    .line 31
    invoke-direct {p0, p1}, Lcom/itau/empresas/ui/view/LinksView;->abreInstaladorDoItoken(Landroid/content/Context;)V

    return-void
.end method

.method private disparaEvento(Ljava/lang/String;)V
    .registers 6
    .param p1, "botao"    # Ljava/lang/String;

    .line 133
    invoke-static {}, Lcom/itau/empresas/feature/analytics/AnalyticsHelper;->getInstance()Lcom/itau/empresas/feature/analytics/AnalyticsHelper;

    move-result-object v3

    .line 134
    .local v3, "analyticsHelper":Lcom/itau/empresas/feature/analytics/AnalyticsHelper;
    const-string v0, "Clique Botao"

    .line 137
    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/LinksView;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0700be

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 135
    invoke-virtual {v3, v0, v1, p1}, Lcom/itau/empresas/feature/analytics/AnalyticsHelper;->eventoAdobeAction(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 139
    const-string v0, "Clique Botao"

    .line 142
    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/LinksView;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0700ba

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "1"

    .line 140
    invoke-virtual {v3, v0, v1, v2}, Lcom/itau/empresas/feature/analytics/AnalyticsHelper;->eventoAdobeAction(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 144
    return-void
.end method

.method private isAplicativoInstalado(Ljava/lang/String;)Z
    .registers 6
    .param p1, "pacote"    # Ljava/lang/String;

    .line 122
    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/LinksView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 124
    .local v2, "pm":Landroid/content/pm/PackageManager;
    const/4 v0, 0x1

    :try_start_9
    invoke-virtual {v2, p1, v0}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_c
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_9 .. :try_end_c} :catch_e

    .line 125
    const/4 v0, 0x1

    return v0

    .line 126
    :catch_e
    move-exception v3

    .line 127
    .local v3, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v0, "link rede"

    invoke-virtual {v3}, Landroid/content/pm/PackageManager$NameNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 128
    const/4 v0, 0x0

    return v0
.end method


# virtual methods
.method public abreAjuda()V
    .registers 3

    .line 97
    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/LinksView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/itau/empresas/ui/activity/GenericaActivity;->intent(Landroid/content/Context;)Lcom/itau/empresas/ui/activity/GenericaActivity_$IntentBuilder_;

    move-result-object v0

    const-class v1, Lcom/itau/empresas/ui/fragment/OutrosFragment_;

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/activity/GenericaActivity_$IntentBuilder_;->fragmentClass(Ljava/lang/Class;)Lcom/itau/empresas/ui/activity/GenericaActivity_$IntentBuilder_;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/ui/activity/GenericaActivity_$IntentBuilder_;->start()Lorg/androidannotations/api/builder/PostActivityStarter;

    .line 98
    return-void
.end method

.method public abreRede()V
    .registers 8

    .line 102
    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/LinksView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0703bd

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 103
    .local v3, "pacote":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/LinksView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    .line 105
    .local v4, "pm":Landroid/content/pm/PackageManager;
    const-string v0, "Rede"

    invoke-direct {p0, v0}, Lcom/itau/empresas/ui/view/LinksView;->disparaEvento(Ljava/lang/String;)V

    .line 107
    invoke-direct {p0, v3}, Lcom/itau/empresas/ui/view/LinksView;->isAplicativoInstalado(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2a

    .line 108
    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/LinksView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v4, v3}, Landroid/content/pm/PackageManager;->getLaunchIntentForPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 109
    return-void

    .line 112
    :cond_2a
    new-instance v5, Landroid/content/Intent;

    const-string v0, "android.intent.action.VIEW"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "https://play.google.com/store/apps/details?id="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-direct {v5, v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 113
    .line 114
    .local v5, "loja":Landroid/content/Intent;
    const/high16 v0, 0x10000

    invoke-virtual {v4, v5, v0}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v6

    .line 116
    .local v6, "resolveInfoList":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    invoke-interface {v6}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5b

    .line 117
    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/LinksView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 119
    :cond_5b
    return-void
.end method

.method public abreToken()V
    .registers 5

    .line 52
    invoke-static {}, Lbr/com/itau/sdk/android/core/BackendClient;->getOfflineOTP()Lbr/com/itau/security/token/model/OTPModel;

    move-result-object v2

    .line 53
    .local v2, "otp":Lbr/com/itau/security/token/model/OTPModel;
    if-nez v2, :cond_a

    .line 54
    invoke-direct {p0}, Lcom/itau/empresas/ui/view/LinksView;->abreDialogDeInstalacaoDoItoken()V

    goto :goto_18

    .line 56
    :cond_a
    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/LinksView;->getContext()Landroid/content/Context;

    move-result-object v3

    .line 57
    .local v3, "context":Landroid/content/Context;
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/itau/empresas/feature/itoken/MostraItokenActivity;

    invoke-direct {v0, v3, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v3, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 60
    .end local v3    # "context":Landroid/content/Context;
    :goto_18
    const-string v0, "iToken"

    invoke-direct {p0, v0}, Lcom/itau/empresas/ui/view/LinksView;->disparaEvento(Ljava/lang/String;)V

    .line 61
    return-void
.end method

.class public final Lcom/itau/empresas/feature/login/strategy/PreLoginStrategyFactory;
.super Ljava/lang/Object;
.source "PreLoginStrategyFactory.java"


# direct methods
.method public static agenciaEConta(Lcom/itau/empresas/feature/login/controller/PreLoginController;Lcom/itau/empresas/feature/login/model/PreLoginUsuarioVO;Lcom/itau/empresas/feature/login/strategy/ListenerStrategy;)Lcom/itau/empresas/feature/login/strategy/PreLoginStrategy;
    .registers 9
    .param p0, "controller"    # Lcom/itau/empresas/feature/login/controller/PreLoginController;
    .param p1, "usuarioVO"    # Lcom/itau/empresas/feature/login/model/PreLoginUsuarioVO;
    .param p2, "listener"    # Lcom/itau/empresas/feature/login/strategy/ListenerStrategy;

    .line 24
    new-instance v0, Lcom/itau/empresas/feature/login/strategy/PreLoginAgenciaContaStrategy;

    invoke-virtual {p1}, Lcom/itau/empresas/feature/login/model/PreLoginUsuarioVO;->getAgencia()Ljava/lang/String;

    move-result-object v2

    .line 25
    invoke-virtual {p1}, Lcom/itau/empresas/feature/login/model/PreLoginUsuarioVO;->getConta()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lcom/itau/empresas/feature/login/model/PreLoginUsuarioVO;->getDac()Ljava/lang/String;

    move-result-object v4

    move-object v1, p0

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/itau/empresas/feature/login/strategy/PreLoginAgenciaContaStrategy;-><init>(Lcom/itau/empresas/feature/login/controller/PreLoginController;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/itau/empresas/feature/login/strategy/ListenerStrategy;)V

    .line 24
    return-object v0
.end method

.method public static documento(Lcom/itau/empresas/feature/login/controller/PreLoginController;Lcom/itau/empresas/feature/login/model/PreLoginUsuarioVO;Lcom/itau/empresas/feature/login/strategy/ListenerStrategy;)Lcom/itau/empresas/feature/login/strategy/PreLoginStrategy;
    .registers 11
    .param p0, "controller"    # Lcom/itau/empresas/feature/login/controller/PreLoginController;
    .param p1, "usuarioVO"    # Lcom/itau/empresas/feature/login/model/PreLoginUsuarioVO;
    .param p2, "listener"    # Lcom/itau/empresas/feature/login/strategy/ListenerStrategy;

    .line 37
    new-instance v0, Lcom/itau/empresas/feature/login/strategy/PreLoginAgenciaContaDocStrategy;

    invoke-virtual {p1}, Lcom/itau/empresas/feature/login/model/PreLoginUsuarioVO;->getAgencia()Ljava/lang/String;

    move-result-object v2

    .line 38
    invoke-virtual {p1}, Lcom/itau/empresas/feature/login/model/PreLoginUsuarioVO;->getConta()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lcom/itau/empresas/feature/login/model/PreLoginUsuarioVO;->getDac()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1}, Lcom/itau/empresas/feature/login/model/PreLoginUsuarioVO;->getNrDocumento()Ljava/lang/String;

    move-result-object v5

    .line 39
    invoke-virtual {p1}, Lcom/itau/empresas/feature/login/model/PreLoginUsuarioVO;->getTipoDocumento()Ljava/lang/String;

    move-result-object v6

    move-object v1, p0

    move-object v7, p2

    invoke-direct/range {v0 .. v7}, Lcom/itau/empresas/feature/login/strategy/PreLoginAgenciaContaDocStrategy;-><init>(Lcom/itau/empresas/feature/login/controller/PreLoginController;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/itau/empresas/feature/login/strategy/ListenerStrategy;)V

    .line 37
    return-object v0
.end method

.method public static operadorUnico(Lcom/itau/empresas/feature/login/controller/PreLoginController;Lcom/itau/empresas/feature/login/model/PreLoginUsuarioVO;Lcom/itau/empresas/feature/login/strategy/ListenerStrategy;)Lcom/itau/empresas/feature/login/strategy/PreLoginStrategy;
    .registers 5
    .param p0, "controller"    # Lcom/itau/empresas/feature/login/controller/PreLoginController;
    .param p1, "usuarioVO"    # Lcom/itau/empresas/feature/login/model/PreLoginUsuarioVO;
    .param p2, "listener"    # Lcom/itau/empresas/feature/login/strategy/ListenerStrategy;

    .line 51
    new-instance v0, Lcom/itau/empresas/feature/login/strategy/PreLoginOperadorUnicoStrategy;

    invoke-virtual {p1}, Lcom/itau/empresas/feature/login/model/PreLoginUsuarioVO;->getOperador()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p0, v1, p2}, Lcom/itau/empresas/feature/login/strategy/PreLoginOperadorUnicoStrategy;-><init>(Lcom/itau/empresas/feature/login/controller/PreLoginController;Ljava/lang/String;Lcom/itau/empresas/feature/login/strategy/ListenerStrategy;)V

    return-object v0
.end method

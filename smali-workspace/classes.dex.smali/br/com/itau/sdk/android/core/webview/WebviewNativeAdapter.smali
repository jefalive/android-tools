.class public Lbr/com/itau/sdk/android/core/webview/WebviewNativeAdapter;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final $:[B

.field private static $$:I = 0x0

.field private static final ITAU_DEFAULT_CATEGORY:Ljava/lang/String; = "com.itau.category.DEFAULT"


# instance fields
.field private itauWebViewFragment:Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;

.field private json:Ljava/lang/String;


# direct methods
.method private static $(III)Ljava/lang/String;
    .registers 9

    mul-int/lit8 p2, p2, 0x3

    rsub-int/lit8 p2, p2, 0x1f

    new-instance v0, Ljava/lang/String;

    add-int/lit8 p1, p1, 0x4

    const/4 v4, 0x0

    sget-object v5, Lbr/com/itau/sdk/android/core/webview/WebviewNativeAdapter;->$:[B

    add-int/lit8 p0, p0, 0x61

    new-array v1, p2, [B

    add-int/lit8 p2, p2, -0x1

    if-nez v5, :cond_18

    move v2, p0

    move v3, p1

    :goto_15
    add-int/2addr v2, v3

    add-int/lit8 p0, v2, -0x8

    :cond_18
    add-int/lit8 p1, p1, 0x1

    int-to-byte v2, p0

    aput-byte v2, v1, v4

    move v2, v4

    add-int/lit8 v4, v4, 0x1

    if-ne v2, p2, :cond_27

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Ljava/lang/String;-><init>([BI)V

    return-object v0

    :cond_27
    move v2, p0

    aget-byte v3, v5, p1

    goto :goto_15
.end method

.method static constructor <clinit>()V
    .registers 3

    const/16 v0, 0x3d

    new-array v0, v0, [B

    fill-array-data v0, :array_e

    sput-object v0, Lbr/com/itau/sdk/android/core/webview/WebviewNativeAdapter;->$:[B

    const/16 v0, 0x79

    sput v0, Lbr/com/itau/sdk/android/core/webview/WebviewNativeAdapter;->$$:I

    return-void

    :array_e
    .array-data 1
        0x6t
        -0x3ft
        -0x1et
        -0x3t
        0x5t
        0x1bt
        -0xbt
        0x14t
        0x6t
        -0x37t
        0x43t
        0x13t
        -0xbt
        0x1ct
        -0x3ft
        0x3dt
        0x6t
        0x1bt
        -0x7t
        0xat
        0x10t
        0xbt
        0xft
        -0x43t
        0x1et
        0x9t
        0x9t
        0x3t
        0x1ct
        -0x1t
        0x10t
        0x15t
        -0x2t
        0x16t
        0x5t
        0x2t
        0x3t
        -0x2et
        0x43t
        0xdt
        0xet
        -0x7t
        0x11t
        0xet
        -0x3et
        0x3dt
        0x6t
        0x1bt
        -0x7t
        0xat
        0x10t
        0xbt
        0xft
        -0x43t
        0x1et
        0x9t
        0x9t
        0x3t
        0x1ct
        -0x1t
        0x10t
    .end array-data
.end method

.method public constructor <init>(Ljava/lang/String;Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;)V
    .registers 3

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lbr/com/itau/sdk/android/core/webview/WebviewNativeAdapter;->json:Ljava/lang/String;

    .line 22
    iput-object p2, p0, Lbr/com/itau/sdk/android/core/webview/WebviewNativeAdapter;->itauWebViewFragment:Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;

    .line 23
    return-void
.end method


# virtual methods
.method public resolveIntent()V
    .registers 7

    .line 26
    new-instance v0, Lcom/google/gson/Gson;

    invoke-direct {v0}, Lcom/google/gson/Gson;-><init>()V

    iget-object v1, p0, Lbr/com/itau/sdk/android/core/webview/WebviewNativeAdapter;->json:Ljava/lang/String;

    const-class v2, Lbr/com/itau/sdk/android/core/model/WebviewNativeProtocolDTO;

    invoke-virtual {v0, v1, v2}, Lcom/google/gson/Gson;->fromJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lbr/com/itau/sdk/android/core/model/WebviewNativeProtocolDTO;

    .line 27
    new-instance v5, Landroid/content/Intent;

    invoke-direct {v5}, Landroid/content/Intent;-><init>()V

    .line 28
    const/high16 v0, 0x20000000

    invoke-virtual {v5, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 29
    invoke-virtual {v4}, Lbr/com/itau/sdk/android/core/model/WebviewNativeProtocolDTO;->getAction()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 31
    sget-object v0, Lbr/com/itau/sdk/android/core/webview/WebviewNativeAdapter;->$:[B

    const/16 v1, 0x1b

    aget-byte v0, v0, v1

    sget-object v1, Lbr/com/itau/sdk/android/core/webview/WebviewNativeAdapter;->$:[B

    const/16 v2, 0x1d

    aget-byte v1, v1, v2

    sget-object v2, Lbr/com/itau/sdk/android/core/webview/WebviewNativeAdapter;->$:[B

    const/16 v3, 0x19

    aget-byte v2, v2, v3

    invoke-static {v0, v1, v2}, Lbr/com/itau/sdk/android/core/webview/WebviewNativeAdapter;->$(III)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4}, Lbr/com/itau/sdk/android/core/model/WebviewNativeProtocolDTO;->getData()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 33
    sget-object v0, Lbr/com/itau/sdk/android/core/webview/WebviewNativeAdapter;->$:[B

    const/16 v1, 0x23

    aget-byte v0, v0, v1

    invoke-static {v0, v0, v0}, Lbr/com/itau/sdk/android/core/webview/WebviewNativeAdapter;->$(III)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4}, Lbr/com/itau/sdk/android/core/model/WebviewNativeProtocolDTO;->getCategory()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_63

    .line 34
    invoke-virtual {v4}, Lbr/com/itau/sdk/android/core/model/WebviewNativeProtocolDTO;->getCategory()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 35
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/webview/WebviewNativeAdapter;->itauWebViewFragment:Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;

    invoke-virtual {v0}, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/support/v4/app/FragmentActivity;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_83

    .line 37
    :cond_63
    sget-object v0, Lbr/com/itau/sdk/android/core/webview/WebviewNativeAdapter;->$:[B

    const/16 v1, 0x1d

    aget-byte v0, v0, v1

    add-int/lit8 v0, v0, 0x1

    or-int/lit8 v1, v0, 0x1a

    sget-object v2, Lbr/com/itau/sdk/android/core/webview/WebviewNativeAdapter;->$:[B

    const/16 v3, 0x1d

    aget-byte v2, v2, v3

    add-int/lit8 v2, v2, 0x1

    invoke-static {v0, v1, v2}, Lbr/com/itau/sdk/android/core/webview/WebviewNativeAdapter;->$(III)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 38
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/webview/WebviewNativeAdapter;->itauWebViewFragment:Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;

    const/16 v1, 0x10

    invoke-virtual {v0, v5, v1}, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 40
    :goto_83
    return-void
.end method

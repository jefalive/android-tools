.class public Lcom/google/android/gms/tagmanager/Container;
.super Ljava/lang/Object;


# instance fields
.field private final zzbhM:Ljava/lang/String;

.field private zzbhO:Lcom/google/android/gms/tagmanager/zzcp;


# direct methods
.method private declared-synchronized zzGc()Lcom/google/android/gms/tagmanager/zzcp;
    .registers 3

    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/tagmanager/Container;->zzbhO:Lcom/google/android/gms/tagmanager/zzcp;
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5

    monitor-exit p0

    return-object v0

    :catchall_5
    move-exception v1

    monitor-exit p0

    throw v1
.end method


# virtual methods
.method public getContainerId()Ljava/lang/String;
    .registers 2

    iget-object v0, p0, Lcom/google/android/gms/tagmanager/Container;->zzbhM:Ljava/lang/String;

    return-object v0
.end method

.method release()V
    .registers 2

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/tagmanager/Container;->zzbhO:Lcom/google/android/gms/tagmanager/zzcp;

    return-void
.end method

.method public zzfR(Ljava/lang/String;)V
    .registers 3

    invoke-direct {p0}, Lcom/google/android/gms/tagmanager/Container;->zzGc()Lcom/google/android/gms/tagmanager/zzcp;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/gms/tagmanager/zzcp;->zzfR(Ljava/lang/String;)V

    return-void
.end method

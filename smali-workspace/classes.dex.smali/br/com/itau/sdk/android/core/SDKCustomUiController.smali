.class public Lbr/com/itau/sdk/android/core/SDKCustomUiController;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lbr/com/itau/sdk/android/core/SDKCustomUiController$ValidatePasswordResponseDTO;,
        Lbr/com/itau/sdk/android/core/SDKCustomUiController$ValidacaoFisicoResponseDTO;,
        Lbr/com/itau/sdk/android/core/SDKCustomUiController$ValidacaoAppResponseDTO;,
        Lbr/com/itau/sdk/android/core/SDKCustomUiController$ValidacaoSMSResponseDTO;,
        Lbr/com/itau/sdk/android/core/SDKCustomUiController$EnvioSMSResponseDTO;
    }
.end annotation


# direct methods
.method public static enviaSMS(Z)V
    .registers 2

    .line 14
    invoke-static {}, Lbr/com/itau/sdk/android/core/token/c;->d()Lbr/com/itau/sdk/android/core/token/c;

    move-result-object v0

    invoke-virtual {v0, p0}, Lbr/com/itau/sdk/android/core/token/c;->a(Z)V

    .line 15
    return-void
.end method

.method public static setTokenType(Ljava/lang/String;)V
    .registers 2

    .line 34
    invoke-static {}, Lbr/com/itau/sdk/android/core/token/c;->d()Lbr/com/itau/sdk/android/core/token/c;

    move-result-object v0

    invoke-virtual {v0, p0}, Lbr/com/itau/sdk/android/core/token/c;->d(Ljava/lang/String;)V

    .line 35
    return-void
.end method

.method public static validaTokenAPP(Ljava/lang/String;Ljava/lang/String;)V
    .registers 3

    .line 22
    invoke-static {}, Lbr/com/itau/sdk/android/core/token/c;->d()Lbr/com/itau/sdk/android/core/token/c;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lbr/com/itau/sdk/android/core/token/c;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 23
    return-void
.end method

.method public static validaTokenFisico(Ljava/lang/String;Ljava/lang/String;)V
    .registers 3

    .line 26
    invoke-static {}, Lbr/com/itau/sdk/android/core/token/c;->d()Lbr/com/itau/sdk/android/core/token/c;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lbr/com/itau/sdk/android/core/token/c;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 27
    return-void
.end method

.method public static validaTokenSms(Ljava/lang/String;Ljava/lang/String;)V
    .registers 3

    .line 18
    invoke-static {}, Lbr/com/itau/sdk/android/core/token/c;->d()Lbr/com/itau/sdk/android/core/token/c;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lbr/com/itau/sdk/android/core/token/c;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 19
    return-void
.end method

.method public static validatePassword(Ljava/lang/String;)V
    .registers 2

    .line 30
    invoke-static {}, Lbr/com/itau/sdk/android/core/token/c;->d()Lbr/com/itau/sdk/android/core/token/c;

    move-result-object v0

    invoke-virtual {v0, p0}, Lbr/com/itau/sdk/android/core/token/c;->c(Ljava/lang/String;)V

    .line 31
    return-void
.end method

.class Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal$AtualizaFragmentoOnTabSelectedListener;
.super Ljava/lang/Object;
.source "ExtratoFragmentPrincipal.java"

# interfaces
.implements Landroid/support/design/widget/TabLayout$OnTabSelectedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AtualizaFragmentoOnTabSelectedListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;


# direct methods
.method private constructor <init>(Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;)V
    .registers 2

    .line 444
    iput-object p1, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal$AtualizaFragmentoOnTabSelectedListener;->this$0:Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal$1;)V
    .registers 3
    .param p1, "x0"    # Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;
    .param p2, "x1"    # Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal$1;

    .line 444
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal$AtualizaFragmentoOnTabSelectedListener;-><init>(Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;)V

    return-void
.end method


# virtual methods
.method public onTabReselected(Landroid/support/design/widget/TabLayout$Tab;)V
    .registers 2
    .param p1, "tab"    # Landroid/support/design/widget/TabLayout$Tab;

    .line 472
    return-void
.end method

.method public onTabSelected(Landroid/support/design/widget/TabLayout$Tab;)V
    .registers 6
    .param p1, "tab"    # Landroid/support/design/widget/TabLayout$Tab;

    .line 448
    invoke-virtual {p1}, Landroid/support/design/widget/TabLayout$Tab;->getPosition()I

    move-result v3

    .line 449
    .local v3, "posicaoTab":I
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal$AtualizaFragmentoOnTabSelectedListener;->this$0:Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;

    iget-object v0, v0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->pagerExtrato:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, v3}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 450
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal$AtualizaFragmentoOnTabSelectedListener;->this$0:Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;

    # getter for: Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->extratoVO:Lcom/itau/empresas/api/model/ExtratoVO;
    invoke-static {v0}, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->access$200(Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;)Lcom/itau/empresas/api/model/ExtratoVO;

    move-result-object v0

    if-nez v0, :cond_14

    .line 451
    return-void

    .line 453
    :cond_14
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal$AtualizaFragmentoOnTabSelectedListener;->this$0:Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;

    # invokes: Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->trataDadosLancamentos()V
    invoke-static {v0}, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->access$300(Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;)V

    .line 454
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal$AtualizaFragmentoOnTabSelectedListener;->this$0:Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;

    iget-object v1, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal$AtualizaFragmentoOnTabSelectedListener;->this$0:Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;

    # getter for: Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->listaLancamentos:Ljava/util/List;
    invoke-static {v1}, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->access$400(Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;)Ljava/util/List;

    move-result-object v1

    # invokes: Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->exibirDadosNoFragmentSelecionado(ILjava/util/List;)V
    invoke-static {v0, v3, v1}, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->access$500(Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;ILjava/util/List;)V

    .line 456
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal$AtualizaFragmentoOnTabSelectedListener;->this$0:Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;

    const/4 v1, 0x1

    # invokes: Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->alterarStyleTab(Landroid/support/design/widget/TabLayout$Tab;Z)V
    invoke-static {v0, p1, v1}, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->access$600(Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;Landroid/support/design/widget/TabLayout$Tab;Z)V

    .line 457
    invoke-virtual {p1}, Landroid/support/design/widget/TabLayout$Tab;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    if-nez v0, :cond_31

    .line 458
    return-void

    .line 461
    :cond_31
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal$AtualizaFragmentoOnTabSelectedListener;->this$0:Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;

    iget-object v1, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal$AtualizaFragmentoOnTabSelectedListener;->this$0:Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;

    const v2, 0x7f0702c8

    invoke-virtual {v1, v2}, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Landroid/support/design/widget/TabLayout$Tab;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->analyticsHit(Ljava/lang/String;Ljava/lang/String;)V

    .line 462
    return-void
.end method

.method public onTabUnselected(Landroid/support/design/widget/TabLayout$Tab;)V
    .registers 4
    .param p1, "tab"    # Landroid/support/design/widget/TabLayout$Tab;

    .line 466
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal$AtualizaFragmentoOnTabSelectedListener;->this$0:Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;

    const/4 v1, 0x0

    # invokes: Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->alterarStyleTab(Landroid/support/design/widget/TabLayout$Tab;Z)V
    invoke-static {v0, p1, v1}, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->access$600(Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;Landroid/support/design/widget/TabLayout$Tab;Z)V

    .line 467
    return-void
.end method

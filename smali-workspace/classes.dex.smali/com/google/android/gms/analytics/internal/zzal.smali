.class public Lcom/google/android/gms/analytics/internal/zzal;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/analytics/internal/zzp;


# instance fields
.field public zzOV:Ljava/lang/String;

.field public zzTo:D

.field public zzTp:I

.field public zzTq:I

.field public zzTr:I

.field public zzTs:I

.field public zzTt:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/high16 v0, -0x4010000000000000L    # -1.0

    iput-wide v0, p0, Lcom/google/android/gms/analytics/internal/zzal;->zzTo:D

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/analytics/internal/zzal;->zzTp:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/analytics/internal/zzal;->zzTq:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/analytics/internal/zzal;->zzTr:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/analytics/internal/zzal;->zzTs:I

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/analytics/internal/zzal;->zzTt:Ljava/util/Map;

    return-void
.end method


# virtual methods
.method public getSessionTimeout()I
    .registers 2

    iget v0, p0, Lcom/google/android/gms/analytics/internal/zzal;->zzTp:I

    return v0
.end method

.method public getTrackingId()Ljava/lang/String;
    .registers 2

    iget-object v0, p0, Lcom/google/android/gms/analytics/internal/zzal;->zzOV:Ljava/lang/String;

    return-object v0
.end method

.method public zzbr(Ljava/lang/String;)Ljava/lang/String;
    .registers 4

    iget-object v0, p0, Lcom/google/android/gms/analytics/internal/zzal;->zzTt:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Ljava/lang/String;

    if-eqz v1, :cond_d

    move-object v0, v1

    goto :goto_e

    :cond_d
    move-object v0, p1

    :goto_e
    return-object v0
.end method

.method public zzlT()Z
    .registers 2

    iget-object v0, p0, Lcom/google/android/gms/analytics/internal/zzal;->zzOV:Ljava/lang/String;

    if-eqz v0, :cond_6

    const/4 v0, 0x1

    goto :goto_7

    :cond_6
    const/4 v0, 0x0

    :goto_7
    return v0
.end method

.method public zzlU()Z
    .registers 5

    iget-wide v0, p0, Lcom/google/android/gms/analytics/internal/zzal;->zzTo:D

    const-wide/16 v2, 0x0

    cmpl-double v0, v0, v2

    if-ltz v0, :cond_a

    const/4 v0, 0x1

    goto :goto_b

    :cond_a
    const/4 v0, 0x0

    :goto_b
    return v0
.end method

.method public zzlV()D
    .registers 3

    iget-wide v0, p0, Lcom/google/android/gms/analytics/internal/zzal;->zzTo:D

    return-wide v0
.end method

.method public zzlW()Z
    .registers 2

    iget v0, p0, Lcom/google/android/gms/analytics/internal/zzal;->zzTp:I

    if-ltz v0, :cond_6

    const/4 v0, 0x1

    goto :goto_7

    :cond_6
    const/4 v0, 0x0

    :goto_7
    return v0
.end method

.method public zzlX()Z
    .registers 3

    iget v0, p0, Lcom/google/android/gms/analytics/internal/zzal;->zzTq:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_7

    const/4 v0, 0x1

    goto :goto_8

    :cond_7
    const/4 v0, 0x0

    :goto_8
    return v0
.end method

.method public zzlY()Z
    .registers 3

    iget v0, p0, Lcom/google/android/gms/analytics/internal/zzal;->zzTq:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_7

    const/4 v0, 0x1

    goto :goto_8

    :cond_7
    const/4 v0, 0x0

    :goto_8
    return v0
.end method

.method public zzlZ()Z
    .registers 3

    iget v0, p0, Lcom/google/android/gms/analytics/internal/zzal;->zzTr:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_7

    const/4 v0, 0x1

    goto :goto_8

    :cond_7
    const/4 v0, 0x0

    :goto_8
    return v0
.end method

.method public zzma()Z
    .registers 3

    iget v0, p0, Lcom/google/android/gms/analytics/internal/zzal;->zzTr:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_7

    const/4 v0, 0x1

    goto :goto_8

    :cond_7
    const/4 v0, 0x0

    :goto_8
    return v0
.end method

.method public zzmb()Z
    .registers 3

    iget v0, p0, Lcom/google/android/gms/analytics/internal/zzal;->zzTs:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_7

    const/4 v0, 0x1

    goto :goto_8

    :cond_7
    const/4 v0, 0x0

    :goto_8
    return v0
.end method

.method public zzo(Landroid/app/Activity;)Ljava/lang/String;
    .registers 3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/analytics/internal/zzal;->zzbr(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

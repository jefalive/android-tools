.class public Lnet/hockeyapp/android/utils/VersionHelper;
.super Ljava/lang/Object;
.source "VersionHelper.java"


# instance fields
.field private currentVersionCode:I

.field private listener:Lnet/hockeyapp/android/UpdateInfoListener;

.field private newest:Lorg/json/JSONObject;

.field private sortedVersions:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<Lorg/json/JSONObject;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Lnet/hockeyapp/android/UpdateInfoListener;)V
    .registers 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "infoJSON"    # Ljava/lang/String;
    .param p3, "listener"    # Lnet/hockeyapp/android/UpdateInfoListener;

    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    iput-object p3, p0, Lnet/hockeyapp/android/utils/VersionHelper;->listener:Lnet/hockeyapp/android/UpdateInfoListener;

    .line 65
    invoke-direct {p0, p1, p2}, Lnet/hockeyapp/android/utils/VersionHelper;->loadVersions(Landroid/content/Context;Ljava/lang/String;)V

    .line 66
    invoke-direct {p0}, Lnet/hockeyapp/android/utils/VersionHelper;->sortVersions()V

    .line 67
    return-void
.end method

.method public static compareVersionStrings(Ljava/lang/String;Ljava/lang/String;)I
    .registers 8
    .param p0, "left"    # Ljava/lang/String;
    .param p1, "right"    # Ljava/lang/String;

    .line 274
    if-eqz p0, :cond_4

    if-nez p1, :cond_6

    .line 275
    :cond_4
    const/4 v0, 0x0

    return v0

    .line 280
    :cond_6
    :try_start_6
    new-instance v2, Ljava/util/Scanner;

    const-string v0, "\\-.*"

    const-string v1, ""

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/util/Scanner;-><init>(Ljava/lang/String;)V

    .line 281
    .local v2, "leftScanner":Ljava/util/Scanner;
    new-instance v3, Ljava/util/Scanner;

    const-string v0, "\\-.*"

    const-string v1, ""

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0}, Ljava/util/Scanner;-><init>(Ljava/lang/String;)V

    .line 282
    .local v3, "rightScanner":Ljava/util/Scanner;
    const-string v0, "\\."

    invoke-virtual {v2, v0}, Ljava/util/Scanner;->useDelimiter(Ljava/lang/String;)Ljava/util/Scanner;

    .line 283
    const-string v0, "\\."

    invoke-virtual {v3, v0}, Ljava/util/Scanner;->useDelimiter(Ljava/lang/String;)Ljava/util/Scanner;

    .line 286
    :goto_2a
    invoke-virtual {v2}, Ljava/util/Scanner;->hasNextInt()Z

    move-result v0

    if-eqz v0, :cond_47

    invoke-virtual {v3}, Ljava/util/Scanner;->hasNextInt()Z

    move-result v0

    if-eqz v0, :cond_47

    .line 287
    invoke-virtual {v2}, Ljava/util/Scanner;->nextInt()I

    move-result v4

    .line 288
    .local v4, "leftValue":I
    invoke-virtual {v3}, Ljava/util/Scanner;->nextInt()I
    :try_end_3d
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_3d} :catch_59

    move-result v5

    .line 289
    .local v5, "rightValue":I
    if-ge v4, v5, :cond_42

    .line 290
    const/4 v0, -0x1

    return v0

    .line 292
    :cond_42
    if-le v4, v5, :cond_46

    .line 293
    const/4 v0, 0x1

    return v0

    .line 295
    .end local v4    # "leftValue":I
    .end local v5    # "rightValue":I
    :cond_46
    goto :goto_2a

    .line 298
    :cond_47
    :try_start_47
    invoke-virtual {v2}, Ljava/util/Scanner;->hasNextInt()Z
    :try_end_4a
    .catch Ljava/lang/Exception; {:try_start_47 .. :try_end_4a} :catch_59

    move-result v0

    if-eqz v0, :cond_4f

    .line 299
    const/4 v0, 0x1

    return v0

    .line 302
    :cond_4f
    :try_start_4f
    invoke-virtual {v3}, Ljava/util/Scanner;->hasNextInt()Z
    :try_end_52
    .catch Ljava/lang/Exception; {:try_start_4f .. :try_end_52} :catch_59

    move-result v0

    if-eqz v0, :cond_57

    .line 303
    const/4 v0, -0x1

    return v0

    .line 307
    :cond_57
    const/4 v0, 0x0

    return v0

    .line 310
    .end local v2    # "leftScanner":Ljava/util/Scanner;
    .end local v3    # "rightScanner":Ljava/util/Scanner;
    :catch_59
    move-exception v2

    .line 312
    .local v2, "e":Ljava/lang/Exception;
    const/4 v0, 0x0

    return v0
.end method

.method private static failSafeGetLongFromJSON(Lorg/json/JSONObject;Ljava/lang/String;J)J
    .registers 7
    .param p0, "json"    # Lorg/json/JSONObject;
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "defaultValue"    # J

    .line 146
    :try_start_0
    invoke-virtual {p0, p1}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_3} :catch_5

    move-result-wide v0

    return-wide v0

    .line 148
    :catch_5
    move-exception v2

    .line 149
    .local v2, "e":Lorg/json/JSONException;
    return-wide p2
.end method

.method private static failSafeGetStringFromJSON(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .registers 5
    .param p0, "json"    # Lorg/json/JSONObject;
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "defaultValue"    # Ljava/lang/String;

    .line 137
    :try_start_0
    invoke-virtual {p0, p1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_3} :catch_5

    move-result-object v0

    return-object v0

    .line 139
    :catch_5
    move-exception v1

    .line 140
    .local v1, "e":Lorg/json/JSONException;
    return-object p2
.end method

.method private getRestoreButton(ILorg/json/JSONObject;)Ljava/lang/String;
    .registers 7
    .param p1, "count"    # I
    .param p2, "version"    # Lorg/json/JSONObject;

    .line 182
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 184
    .local v2, "result":Ljava/lang/StringBuilder;
    invoke-direct {p0, p2}, Lnet/hockeyapp/android/utils/VersionHelper;->getVersionID(Lorg/json/JSONObject;)Ljava/lang/String;

    move-result-object v3

    .line 185
    .local v3, "versionID":Ljava/lang/String;
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_2b

    .line 186
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "<a href=\'restore:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\'  style=\'background: #c8c8c8; color: #000; display: block; float: right; padding: 7px; margin: 0px 10px 10px; text-decoration: none;\'>Restore</a>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 189
    :cond_2b
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getSeparator()Ljava/lang/Object;
    .registers 2

    .line 178
    const-string v0, "<hr style=\'border-top: 1px solid #c8c8c8; border-bottom: 0px; margin: 40px 10px 0px 10px;\' />"

    return-object v0
.end method

.method private getVersionCode(Lorg/json/JSONObject;)I
    .registers 5
    .param p1, "version"    # Lorg/json/JSONObject;

    .line 226
    const/4 v1, 0x0

    .line 228
    .local v1, "versionCode":I
    const-string v0, "version"

    :try_start_3
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I
    :try_end_6
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_6} :catch_9

    move-result v0

    move v1, v0

    .line 231
    goto :goto_a

    .line 230
    :catch_9
    move-exception v2

    .line 232
    :goto_a
    return v1
.end method

.method private getVersionID(Lorg/json/JSONObject;)Ljava/lang/String;
    .registers 5
    .param p1, "version"    # Lorg/json/JSONObject;

    .line 193
    const-string v1, ""

    .line 195
    .local v1, "versionID":Ljava/lang/String;
    const-string v0, "id"

    :try_start_4
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_7
    .catch Lorg/json/JSONException; {:try_start_4 .. :try_end_7} :catch_a

    move-result-object v0

    move-object v1, v0

    .line 198
    goto :goto_b

    .line 197
    :catch_a
    move-exception v2

    .line 199
    :goto_b
    return-object v1
.end method

.method private getVersionLine(ILorg/json/JSONObject;)Ljava/lang/String;
    .registers 9
    .param p1, "count"    # I
    .param p2, "version"    # Lorg/json/JSONObject;

    .line 203
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 205
    .local v2, "result":Ljava/lang/StringBuilder;
    iget-object v0, p0, Lnet/hockeyapp/android/utils/VersionHelper;->newest:Lorg/json/JSONObject;

    invoke-direct {p0, v0}, Lnet/hockeyapp/android/utils/VersionHelper;->getVersionCode(Lorg/json/JSONObject;)I

    move-result v3

    .line 206
    .local v3, "newestCode":I
    invoke-direct {p0, p2}, Lnet/hockeyapp/android/utils/VersionHelper;->getVersionCode(Lorg/json/JSONObject;)I

    move-result v4

    .line 207
    .local v4, "versionCode":I
    invoke-direct {p0, p2}, Lnet/hockeyapp/android/utils/VersionHelper;->getVersionName(Lorg/json/JSONObject;)Ljava/lang/String;

    move-result-object v5

    .line 209
    .local v5, "versionName":Ljava/lang/String;
    const-string v0, "<div style=\'padding: 20px 10px 10px;\'><strong>"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 210
    if-nez p1, :cond_20

    .line 211
    const-string v0, "Newest version:"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_54

    .line 214
    :cond_20
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Version "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "): "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 215
    if-eq v4, v3, :cond_54

    iget v0, p0, Lnet/hockeyapp/android/utils/VersionHelper;->currentVersionCode:I

    if-ne v4, v0, :cond_54

    .line 216
    const/4 v0, -0x1

    iput v0, p0, Lnet/hockeyapp/android/utils/VersionHelper;->currentVersionCode:I

    .line 217
    const-string v0, "[INSTALLED]"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 220
    :cond_54
    :goto_54
    const-string v0, "</strong></div>"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 222
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getVersionName(Lorg/json/JSONObject;)Ljava/lang/String;
    .registers 5
    .param p1, "version"    # Lorg/json/JSONObject;

    .line 236
    const-string v1, ""

    .line 238
    .local v1, "versionName":Ljava/lang/String;
    const-string v0, "shortversion"

    :try_start_4
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_7
    .catch Lorg/json/JSONException; {:try_start_4 .. :try_end_7} :catch_a

    move-result-object v0

    move-object v1, v0

    .line 241
    goto :goto_b

    .line 240
    :catch_a
    move-exception v2

    .line 242
    :goto_b
    return-object v1
.end method

.method private getVersionNotes(ILorg/json/JSONObject;)Ljava/lang/String;
    .registers 7
    .param p1, "count"    # I
    .param p2, "version"    # Lorg/json/JSONObject;

    .line 246
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 248
    .local v2, "result":Ljava/lang/StringBuilder;
    const-string v0, "notes"

    const-string v1, ""

    invoke-static {p2, v0, v1}, Lnet/hockeyapp/android/utils/VersionHelper;->failSafeGetStringFromJSON(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 249
    .local v3, "notes":Ljava/lang/String;
    const-string v0, "<div style=\'padding: 0px 10px;\'>"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 250
    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_22

    .line 251
    const-string v0, "<em>No information.</em>"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_25

    .line 254
    :cond_22
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 256
    :goto_25
    const-string v0, "</div>"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 258
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static isNewerThanLastUpdateTime(Landroid/content/Context;J)Z
    .registers 12
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "timestamp"    # J

    .line 325
    if-nez p0, :cond_4

    .line 326
    const/4 v0, 0x0

    return v0

    .line 330
    :cond_4
    :try_start_4
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    .line 331
    .local v4, "pm":Landroid/content/pm/PackageManager;
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v4, v0, v1}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v5

    .line 332
    .local v5, "appInfo":Landroid/content/pm/ApplicationInfo;
    iget-object v6, v5, Landroid/content/pm/ApplicationInfo;->sourceDir:Ljava/lang/String;

    .line 336
    .local v6, "appFile":Ljava/lang/String;
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->lastModified()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2
    :try_end_1f
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_4 .. :try_end_1f} :catch_2b

    const-wide/16 v2, 0x708

    add-long v7, v0, v2

    .line 338
    .local v7, "lastModified":J
    cmp-long v0, p1, v7

    if-lez v0, :cond_29

    const/4 v0, 0x1

    goto :goto_2a

    :cond_29
    const/4 v0, 0x0

    :goto_2a
    return v0

    .line 340
    .end local v4    # "pm":Landroid/content/pm/PackageManager;
    .end local v5    # "appInfo":Landroid/content/pm/ApplicationInfo;
    .end local v6    # "appFile":Ljava/lang/String;
    .end local v7    # "lastModified":J
    :catch_2b
    move-exception v4

    .line 341
    .local v4, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v4}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    .line 342
    const/4 v0, 0x0

    return v0
.end method

.method private loadVersions(Landroid/content/Context;Ljava/lang/String;)V
    .registers 11
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "infoJSON"    # Ljava/lang/String;

    .line 70
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    iput-object v0, p0, Lnet/hockeyapp/android/utils/VersionHelper;->newest:Lorg/json/JSONObject;

    .line 71
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lnet/hockeyapp/android/utils/VersionHelper;->sortedVersions:Ljava/util/ArrayList;

    .line 72
    iget-object v0, p0, Lnet/hockeyapp/android/utils/VersionHelper;->listener:Lnet/hockeyapp/android/UpdateInfoListener;

    invoke-interface {v0}, Lnet/hockeyapp/android/UpdateInfoListener;->getCurrentVersionCode()I

    move-result v0

    iput v0, p0, Lnet/hockeyapp/android/utils/VersionHelper;->currentVersionCode:I

    .line 75
    :try_start_16
    new-instance v2, Lorg/json/JSONArray;

    invoke-direct {v2, p2}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 77
    .local v2, "versions":Lorg/json/JSONArray;
    iget-object v0, p0, Lnet/hockeyapp/android/utils/VersionHelper;->listener:Lnet/hockeyapp/android/UpdateInfoListener;

    invoke-interface {v0}, Lnet/hockeyapp/android/UpdateInfoListener;->getCurrentVersionCode()I

    move-result v3

    .line 78
    .local v3, "versionCode":I
    const/4 v4, 0x0

    .local v4, "index":I
    :goto_22
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v0

    if-ge v4, v0, :cond_62

    .line 79
    invoke-virtual {v2, v4}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v5

    .line 80
    .local v5, "entry":Lorg/json/JSONObject;
    const-string v0, "version"

    invoke-virtual {v5, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v0

    if-le v0, v3, :cond_36

    const/4 v6, 0x1

    goto :goto_37

    :cond_36
    const/4 v6, 0x0

    .line 81
    .local v6, "largerVersionCode":Z
    :goto_37
    const-string v0, "version"

    invoke-virtual {v5, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v0

    if-ne v0, v3, :cond_4d

    const-string v0, "timestamp"

    invoke-virtual {v5, v0}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    invoke-static {p1, v0, v1}, Lnet/hockeyapp/android/utils/VersionHelper;->isNewerThanLastUpdateTime(Landroid/content/Context;J)Z

    move-result v0

    if-eqz v0, :cond_4d

    const/4 v7, 0x1

    goto :goto_4e

    :cond_4d
    const/4 v7, 0x0

    .line 83
    .local v7, "newerApkFile":Z
    :goto_4e
    if-nez v6, :cond_52

    if-eqz v7, :cond_5a

    .line 84
    :cond_52
    iput-object v5, p0, Lnet/hockeyapp/android/utils/VersionHelper;->newest:Lorg/json/JSONObject;

    .line 85
    const-string v0, "version"

    invoke-virtual {v5, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v3

    .line 87
    :cond_5a
    iget-object v0, p0, Lnet/hockeyapp/android/utils/VersionHelper;->sortedVersions:Ljava/util/ArrayList;

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_5f
    .catch Lorg/json/JSONException; {:try_start_16 .. :try_end_5f} :catch_63
    .catch Ljava/lang/NullPointerException; {:try_start_16 .. :try_end_5f} :catch_65

    .line 78
    .end local v5    # "entry":Lorg/json/JSONObject;
    .end local v6    # "largerVersionCode":Z
    .end local v7    # "newerApkFile":Z
    add-int/lit8 v4, v4, 0x1

    goto :goto_22

    .line 93
    .end local v2    # "versions":Lorg/json/JSONArray;
    .end local v3    # "versionCode":I
    .end local v4    # "index":I
    :cond_62
    goto :goto_66

    .line 90
    :catch_63
    move-exception v2

    .line 93
    goto :goto_66

    .line 92
    :catch_65
    move-exception v2

    .line 94
    :goto_66
    return-void
.end method

.method public static mapGoogleVersion(Ljava/lang/String;)Ljava/lang/String;
    .registers 2
    .param p0, "version"    # Ljava/lang/String;

    .line 353
    if-eqz p0, :cond_a

    const-string v0, "L"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 354
    :cond_a
    const-string v0, "5.0"

    return-object v0

    .line 357
    :cond_d
    return-object p0
.end method

.method private sortVersions()V
    .registers 3

    .line 97
    iget-object v0, p0, Lnet/hockeyapp/android/utils/VersionHelper;->sortedVersions:Ljava/util/ArrayList;

    new-instance v1, Lnet/hockeyapp/android/utils/VersionHelper$1;

    invoke-direct {v1, p0}, Lnet/hockeyapp/android/utils/VersionHelper$1;-><init>(Lnet/hockeyapp/android/utils/VersionHelper;)V

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 112
    return-void
.end method


# virtual methods
.method public getFileDateString()Ljava/lang/String;
    .registers 9

    .line 119
    iget-object v0, p0, Lnet/hockeyapp/android/utils/VersionHelper;->newest:Lorg/json/JSONObject;

    const-string v1, "timestamp"

    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3}, Lnet/hockeyapp/android/utils/VersionHelper;->failSafeGetLongFromJSON(Lorg/json/JSONObject;Ljava/lang/String;J)J

    move-result-wide v4

    .line 120
    .local v4, "timestamp":J
    new-instance v6, Ljava/util/Date;

    const-wide/16 v0, 0x3e8

    mul-long/2addr v0, v4

    invoke-direct {v6, v0, v1}, Ljava/util/Date;-><init>(J)V

    .line 121
    .local v6, "date":Ljava/util/Date;
    new-instance v7, Ljava/text/SimpleDateFormat;

    const-string v0, "dd.MM.yyyy"

    invoke-direct {v7, v0}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 122
    .local v7, "dateFormat":Ljava/text/SimpleDateFormat;
    invoke-virtual {v7, v6}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getFileSizeBytes()J
    .registers 8

    .line 126
    iget-object v0, p0, Lnet/hockeyapp/android/utils/VersionHelper;->newest:Lorg/json/JSONObject;

    const-string v1, "external"

    const-string v2, "false"

    invoke-static {v0, v1, v2}, Lnet/hockeyapp/android/utils/VersionHelper;->failSafeGetStringFromJSON(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    .line 127
    .local v4, "external":Z
    iget-object v0, p0, Lnet/hockeyapp/android/utils/VersionHelper;->newest:Lorg/json/JSONObject;

    const-string v1, "appsize"

    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3}, Lnet/hockeyapp/android/utils/VersionHelper;->failSafeGetLongFromJSON(Lorg/json/JSONObject;Ljava/lang/String;J)J

    move-result-wide v5

    .line 132
    .local v5, "appSize":J
    if-eqz v4, :cond_27

    const-wide/16 v0, 0x0

    cmp-long v0, v5, v0

    if-nez v0, :cond_27

    const-wide/16 v0, -0x1

    goto :goto_28

    :cond_27
    move-wide v0, v5

    :goto_28
    return-wide v0
.end method

.method public getReleaseNotes(Z)Ljava/lang/String;
    .registers 7
    .param p1, "showRestore"    # Z

    .line 154
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 155
    .local v1, "result":Ljava/lang/StringBuilder;
    const-string v0, "<html>"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 156
    const-string v0, "<body style=\'padding: 0px 0px 20px 0px\'>"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 158
    const/4 v2, 0x0

    .line 159
    .local v2, "count":I
    iget-object v0, p0, Lnet/hockeyapp/android/utils/VersionHelper;->sortedVersions:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_16
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_46

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lorg/json/JSONObject;

    .line 160
    .local v4, "version":Lorg/json/JSONObject;
    if-lez v2, :cond_35

    .line 161
    invoke-direct {p0}, Lnet/hockeyapp/android/utils/VersionHelper;->getSeparator()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 162
    if-eqz p1, :cond_35

    .line 163
    invoke-direct {p0, v2, v4}, Lnet/hockeyapp/android/utils/VersionHelper;->getRestoreButton(ILorg/json/JSONObject;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 166
    :cond_35
    invoke-direct {p0, v2, v4}, Lnet/hockeyapp/android/utils/VersionHelper;->getVersionLine(ILorg/json/JSONObject;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 167
    invoke-direct {p0, v2, v4}, Lnet/hockeyapp/android/utils/VersionHelper;->getVersionNotes(ILorg/json/JSONObject;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 168
    add-int/lit8 v2, v2, 0x1

    .line 169
    .end local v4    # "version":Lorg/json/JSONObject;
    goto :goto_16

    .line 171
    :cond_46
    const-string v0, "</body>"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 172
    const-string v0, "</html>"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 174
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getVersionString()Ljava/lang/String;
    .registers 5

    .line 115
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lnet/hockeyapp/android/utils/VersionHelper;->newest:Lorg/json/JSONObject;

    const-string v2, "shortversion"

    const-string v3, ""

    invoke-static {v1, v2, v3}, Lnet/hockeyapp/android/utils/VersionHelper;->failSafeGetStringFromJSON(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lnet/hockeyapp/android/utils/VersionHelper;->newest:Lorg/json/JSONObject;

    const-string v2, "version"

    const-string v3, ""

    invoke-static {v1, v2, v3}, Lnet/hockeyapp/android/utils/VersionHelper;->failSafeGetStringFromJSON(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

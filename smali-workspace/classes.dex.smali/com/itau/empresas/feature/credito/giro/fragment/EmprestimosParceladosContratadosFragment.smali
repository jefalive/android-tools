.class public Lcom/itau/empresas/feature/credito/giro/fragment/EmprestimosParceladosContratadosFragment;
.super Lcom/itau/empresas/ui/fragment/BaseFragment;
.source "EmprestimosParceladosContratadosFragment.java"


# instance fields
.field adapter:Lcom/itau/empresas/feature/credito/giro/adapter/EmprestimosParceladosContratadosAdapter;

.field controller:Lcom/itau/empresas/feature/credito/giro/controller/EmprestimosParceladosController;

.field listaEmprestimosParceladosContratados:Landroid/widget/ListView;

.field listaEmprestimosParceladosContratadosVO:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosContratadosVO;>;"
        }
    .end annotation
.end field

.field llCreditoContratadosSemContratacao:Landroid/widget/LinearLayout;

.field private quantidadeDeComprovantesProcessados:I

.field private quantideDeComprovantes:I

.field textoCreditoContratadosSemContratados:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 36
    invoke-direct {p0}, Lcom/itau/empresas/ui/fragment/BaseFragment;-><init>()V

    return-void
.end method

.method private consultaOfertasContratadas()V
    .registers 6

    .line 73
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/giro/fragment/EmprestimosParceladosContratadosFragment;->ocultarMensagemErro()V

    .line 75
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/giro/fragment/EmprestimosParceladosContratadosFragment;->mostraDialogoDeProgresso()V

    .line 77
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/fragment/EmprestimosParceladosContratadosFragment;->controller:Lcom/itau/empresas/feature/credito/giro/controller/EmprestimosParceladosController;

    .line 78
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/giro/fragment/EmprestimosParceladosContratadosFragment;->getDadosUsuario()Lcom/itau/empresas/api/model/ContaOperadorVO;

    move-result-object v1

    invoke-virtual {v1}, Lcom/itau/empresas/api/model/ContaOperadorVO;->getAgencia()Ljava/lang/String;

    move-result-object v1

    .line 79
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/giro/fragment/EmprestimosParceladosContratadosFragment;->getDadosUsuario()Lcom/itau/empresas/api/model/ContaOperadorVO;

    move-result-object v2

    invoke-virtual {v2}, Lcom/itau/empresas/api/model/ContaOperadorVO;->getConta()Ljava/lang/String;

    move-result-object v2

    .line 80
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/giro/fragment/EmprestimosParceladosContratadosFragment;->getDadosUsuario()Lcom/itau/empresas/api/model/ContaOperadorVO;

    move-result-object v3

    invoke-virtual {v3}, Lcom/itau/empresas/api/model/ContaOperadorVO;->getDac()Ljava/lang/String;

    move-result-object v3

    const-string v4, "alteracao_giro_ds_bkl_cei"

    .line 77
    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/itau/empresas/feature/credito/giro/controller/EmprestimosParceladosController;->consultaContratadas(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 82
    return-void
.end method

.method private getDadosUsuario()Lcom/itau/empresas/api/model/ContaOperadorVO;
    .registers 2

    .line 68
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/fragment/EmprestimosParceladosContratadosFragment;->application:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getContaSelecionada()Lcom/itau/empresas/api/model/ContaOperadorVO;

    move-result-object v0

    return-object v0
.end method

.method private incrementaComprovantesProcessados()V
    .registers 3

    .line 139
    iget v0, p0, Lcom/itau/empresas/feature/credito/giro/fragment/EmprestimosParceladosContratadosFragment;->quantidadeDeComprovantesProcessados:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/itau/empresas/feature/credito/giro/fragment/EmprestimosParceladosContratadosFragment;->quantidadeDeComprovantesProcessados:I

    .line 140
    return-void
.end method

.method private montaTextoContratadosSemContratacao()V
    .registers 12

    .line 91
    const-string v4, "Selecione "

    .line 92
    .local v4, "textoParte1":Ljava/lang/String;
    const-string v5, "oportunidades "

    .line 93
    .local v5, "textoParte2":Ljava/lang/String;
    const-string v6, "para contratar."

    .line 95
    .local v6, "textoParte3":Ljava/lang/String;
    new-instance v7, Landroid/text/SpannableString;

    invoke-direct {v7, v4}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 96
    .local v7, "spannableString1":Landroid/text/SpannableString;
    new-instance v0, Landroid/text/style/ForegroundColorSpan;

    .line 97
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/giro/fragment/EmprestimosParceladosContratadosFragment;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0c0035

    invoke-static {v1, v2}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v1

    invoke-direct {v0, v1}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    .line 98
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v1

    .line 96
    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v7, v0, v2, v1, v3}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 100
    new-instance v8, Landroid/text/SpannableString;

    invoke-direct {v8, v5}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 101
    .local v8, "spannableString2":Landroid/text/SpannableString;
    new-instance v0, Landroid/text/style/ForegroundColorSpan;

    .line 102
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/giro/fragment/EmprestimosParceladosContratadosFragment;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0c00cd

    invoke-static {v1, v2}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v1

    invoke-direct {v0, v1}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    .line 103
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v1

    .line 101
    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v8, v0, v2, v1, v3}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 105
    new-instance v0, Landroid/text/style/StyleSpan;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/text/style/StyleSpan;-><init>(I)V

    .line 107
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v1

    .line 105
    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v8, v0, v2, v1, v3}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 109
    new-instance v9, Landroid/text/SpannableString;

    invoke-direct {v9, v6}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 110
    .local v9, "spannableString3":Landroid/text/SpannableString;
    new-instance v0, Landroid/text/style/ForegroundColorSpan;

    .line 111
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/giro/fragment/EmprestimosParceladosContratadosFragment;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0c0035

    invoke-static {v1, v2}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v1

    invoke-direct {v0, v1}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    .line 112
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v1

    .line 110
    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v9, v0, v2, v1, v3}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 114
    new-instance v10, Landroid/text/SpannableStringBuilder;

    invoke-direct {v10}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 115
    .local v10, "builder":Landroid/text/SpannableStringBuilder;
    invoke-virtual {v10, v7}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    .line 116
    invoke-virtual {v0, v8}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    .line 117
    invoke-virtual {v0, v9}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 119
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/fragment/EmprestimosParceladosContratadosFragment;->textoCreditoContratadosSemContratados:Landroid/widget/TextView;

    invoke-virtual {v0, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 120
    return-void
.end method


# virtual methods
.method public carregarElementosDeTela()V
    .registers 1

    .line 62
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/giro/fragment/EmprestimosParceladosContratadosFragment;->mostraEstadoVazio()V

    .line 64
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/giro/fragment/EmprestimosParceladosContratadosFragment;->consultaOfertasContratadas()V

    .line 65
    return-void
.end method

.method public checaRespostaAPIProcessaDialogDeProgresso()V
    .registers 3

    .line 131
    iget v0, p0, Lcom/itau/empresas/feature/credito/giro/fragment/EmprestimosParceladosContratadosFragment;->quantidadeDeComprovantesProcessados:I

    iget v1, p0, Lcom/itau/empresas/feature/credito/giro/fragment/EmprestimosParceladosContratadosFragment;->quantideDeComprovantes:I

    if-ne v0, v1, :cond_9

    .line 133
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/giro/fragment/EmprestimosParceladosContratadosFragment;->escondeDialogoDeProgresso()V

    .line 135
    :cond_9
    return-void
.end method

.method public mostraEstadoVazio()V
    .registers 3

    .line 85
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/giro/fragment/EmprestimosParceladosContratadosFragment;->montaTextoContratadosSemContratacao()V

    .line 86
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/fragment/EmprestimosParceladosContratadosFragment;->llCreditoContratadosSemContratacao:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 87
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/fragment/EmprestimosParceladosContratadosFragment;->listaEmprestimosParceladosContratados:Landroid/widget/ListView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setVisibility(I)V

    .line 88
    return-void
.end method

.method public onEventMainThread(Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent$Conversion;)V
    .registers 4
    .param p1, "e"    # Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent$Conversion;

    .line 144
    invoke-static {p1, p0}, Lcom/itau/empresas/api/ApiHelper;->devoTratarEvento(Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;Lcom/itau/empresas/api/ApiConsumidor;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 145
    return-void

    .line 148
    :cond_7
    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent$Conversion;->getException()Lbr/com/itau/sdk/android/core/exception/BackendException;

    move-result-object v0

    invoke-virtual {v0}, Lbr/com/itau/sdk/android/core/exception/BackendException;->getOpKey()Ljava/lang/String;

    move-result-object v0

    const-string v1, "comprovanteIdReemissao"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 149
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/giro/fragment/EmprestimosParceladosContratadosFragment;->incrementaComprovantesProcessados()V

    .line 152
    :cond_1a
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/giro/fragment/EmprestimosParceladosContratadosFragment;->checaRespostaAPIProcessaDialogDeProgresso()V

    .line 153
    return-void
.end method

.method public onEventMainThread(Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;)V
    .registers 4
    .param p1, "e"    # Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;

    .line 157
    invoke-static {p1, p0}, Lcom/itau/empresas/api/ApiHelper;->devoTratarEvento(Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;Lcom/itau/empresas/api/ApiConsumidor;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 158
    return-void

    .line 161
    :cond_7
    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;->getException()Lbr/com/itau/sdk/android/core/exception/BackendException;

    move-result-object v0

    invoke-virtual {v0}, Lbr/com/itau/sdk/android/core/exception/BackendException;->getOpKey()Ljava/lang/String;

    move-result-object v0

    const-string v1, "comprovanteIdReemissao"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 162
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/giro/fragment/EmprestimosParceladosContratadosFragment;->incrementaComprovantesProcessados()V

    .line 165
    :cond_1a
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/giro/fragment/EmprestimosParceladosContratadosFragment;->checaRespostaAPIProcessaDialogDeProgresso()V

    .line 166
    return-void
.end method

.method public onEventMainThread(Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosContratadosVO;)V
    .registers 7
    .param p1, "comprovante"    # Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosContratadosVO;

    .line 191
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/giro/fragment/EmprestimosParceladosContratadosFragment;->mostraDialogoDeProgresso()V

    .line 194
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/giro/fragment/EmprestimosParceladosContratadosFragment;->incrementaComprovantesProcessados()V

    .line 196
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/giro/fragment/EmprestimosParceladosContratadosFragment;->checaRespostaAPIProcessaDialogDeProgresso()V

    .line 198
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/fragment/EmprestimosParceladosContratadosFragment;->listaEmprestimosParceladosContratadosVO:Ljava/util/List;

    if-nez v0, :cond_14

    .line 200
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/fragment/EmprestimosParceladosContratadosFragment;->listaEmprestimosParceladosContratadosVO:Ljava/util/List;

    .line 203
    :cond_14
    const/4 v2, 0x0

    .line 205
    .local v2, "comprovanteJaCarregado":Z
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/fragment/EmprestimosParceladosContratadosFragment;->listaEmprestimosParceladosContratadosVO:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1b
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_39

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosContratadosVO;

    .line 207
    .local v4, "contratado":Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosContratadosVO;
    invoke-virtual {v4}, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosContratadosVO;->getIdComprovante()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosContratadosVO;->getIdComprovante()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_38

    .line 208
    const/4 v2, 0x1

    .line 209
    goto :goto_39

    .line 211
    .end local v4    # "contratado":Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosContratadosVO;
    :cond_38
    goto :goto_1b

    .line 213
    :cond_39
    :goto_39
    if-nez v2, :cond_40

    .line 215
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/fragment/EmprestimosParceladosContratadosFragment;->listaEmprestimosParceladosContratadosVO:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 218
    :cond_40
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/fragment/EmprestimosParceladosContratadosFragment;->llCreditoContratadosSemContratacao:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 219
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/fragment/EmprestimosParceladosContratadosFragment;->listaEmprestimosParceladosContratados:Landroid/widget/ListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setVisibility(I)V

    .line 221
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/fragment/EmprestimosParceladosContratadosFragment;->adapter:Lcom/itau/empresas/feature/credito/giro/adapter/EmprestimosParceladosContratadosAdapter;

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/giro/fragment/EmprestimosParceladosContratadosFragment;->listaEmprestimosParceladosContratadosVO:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/credito/giro/adapter/EmprestimosParceladosContratadosAdapter;->setData(Ljava/util/List;)V

    .line 222
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/fragment/EmprestimosParceladosContratadosFragment;->listaEmprestimosParceladosContratados:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/giro/fragment/EmprestimosParceladosContratadosFragment;->adapter:Lcom/itau/empresas/feature/credito/giro/adapter/EmprestimosParceladosContratadosAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 223
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/fragment/EmprestimosParceladosContratadosFragment;->listaEmprestimosParceladosContratados:Landroid/widget/ListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setDivider(Landroid/graphics/drawable/Drawable;)V

    .line 224
    return-void
.end method

.method public onEventMainThread(Ljava/util/List;)V
    .registers 9
    .param p1, "emprestimosContratados"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosContratadosIdentificadorVO;>;)V"
        }
    .end annotation

    .line 171
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/giro/fragment/EmprestimosParceladosContratadosFragment;->mostraDialogoDeProgresso()V

    .line 173
    if-eqz p1, :cond_e

    .line 175
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    iput v0, p0, Lcom/itau/empresas/feature/credito/giro/fragment/EmprestimosParceladosContratadosFragment;->quantideDeComprovantes:I

    .line 176
    const/4 v0, 0x0

    iput v0, p0, Lcom/itau/empresas/feature/credito/giro/fragment/EmprestimosParceladosContratadosFragment;->quantidadeDeComprovantesProcessados:I

    .line 179
    :cond_e
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_12
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_46

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosContratadosIdentificadorVO;

    .line 181
    .local v6, "emprestimoContrado":Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosContratadosIdentificadorVO;
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/fragment/EmprestimosParceladosContratadosFragment;->controller:Lcom/itau/empresas/feature/credito/giro/controller/EmprestimosParceladosController;

    .line 182
    invoke-virtual {v6}, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosContratadosIdentificadorVO;->getIdComprovate()Ljava/lang/String;

    move-result-object v1

    .line 183
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/giro/fragment/EmprestimosParceladosContratadosFragment;->getDadosUsuario()Lcom/itau/empresas/api/model/ContaOperadorVO;

    move-result-object v2

    invoke-virtual {v2}, Lcom/itau/empresas/api/model/ContaOperadorVO;->getAgencia()Ljava/lang/String;

    move-result-object v2

    .line 184
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/giro/fragment/EmprestimosParceladosContratadosFragment;->getDadosUsuario()Lcom/itau/empresas/api/model/ContaOperadorVO;

    move-result-object v3

    invoke-virtual {v3}, Lcom/itau/empresas/api/model/ContaOperadorVO;->getConta()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x7

    invoke-static {v4, v3}, Lcom/itau/empresas/ui/util/ViewUtils;->formataAdicionandoZeros(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 185
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/giro/fragment/EmprestimosParceladosContratadosFragment;->getDadosUsuario()Lcom/itau/empresas/api/model/ContaOperadorVO;

    move-result-object v4

    invoke-virtual {v4}, Lcom/itau/empresas/api/model/ContaOperadorVO;->getDac()Ljava/lang/String;

    move-result-object v4

    .line 181
    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/itau/empresas/feature/credito/giro/controller/EmprestimosParceladosController;->consultaContratadaComprovante(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 186
    .end local v6    # "emprestimoContrado":Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosContratadosIdentificadorVO;
    goto :goto_12

    .line 187
    :cond_46
    return-void
.end method

.method public opkeysConsumidas()Ljava/util/List;
    .registers 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Ljava/lang/String;>;"
        }
    .end annotation

    .line 125
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "comprovantesReemissao"

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const-string v1, "comprovanteIdReemissao"

    const/4 v2, 0x1

    aput-object v1, v0, v2

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

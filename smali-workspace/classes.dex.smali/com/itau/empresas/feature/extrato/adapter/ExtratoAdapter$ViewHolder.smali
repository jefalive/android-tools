.class Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter$ViewHolder;
.super Landroid/support/v7/widget/RecyclerView$ViewHolder;
.source "ExtratoAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "ViewHolder"
.end annotation


# instance fields
.field private icone:Lcom/itau/empresas/ui/view/TextViewIcon;

.field private linhaVertical:Landroid/view/View;

.field private rlDados:Landroid/widget/RelativeLayout;

.field private textoDescricao:Landroid/widget/TextView;

.field private textoValor:Landroid/widget/TextView;


# direct methods
.method private constructor <init>(Landroid/view/View;)V
    .registers 3
    .param p1, "itemView"    # Landroid/view/View;

    .line 390
    invoke-direct {p0, p1}, Landroid/support/v7/widget/RecyclerView$ViewHolder;-><init>(Landroid/view/View;)V

    .line 391
    const v0, 0x7f0e051f

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter$ViewHolder;->rlDados:Landroid/widget/RelativeLayout;

    .line 392
    const v0, 0x7f0e0549

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/ui/view/TextViewIcon;

    iput-object v0, p0, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter$ViewHolder;->icone:Lcom/itau/empresas/ui/view/TextViewIcon;

    .line 393
    const v0, 0x7f0e054a

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter$ViewHolder;->textoDescricao:Landroid/widget/TextView;

    .line 394
    const v0, 0x7f0e054b

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter$ViewHolder;->textoValor:Landroid/widget/TextView;

    .line 395
    const v0, 0x7f0e03aa

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter$ViewHolder;->linhaVertical:Landroid/view/View;

    .line 396
    return-void
.end method

.method synthetic constructor <init>(Landroid/view/View;Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter$1;)V
    .registers 3
    .param p1, "x0"    # Landroid/view/View;
    .param p2, "x1"    # Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter$1;

    .line 381
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter$ViewHolder;-><init>(Landroid/view/View;)V

    return-void
.end method

.method static synthetic access$1000(Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter$ViewHolder;)Landroid/widget/RelativeLayout;
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter$ViewHolder;

    .line 381
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter$ViewHolder;->rlDados:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter$ViewHolder;)Lcom/itau/empresas/ui/view/TextViewIcon;
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter$ViewHolder;

    .line 381
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter$ViewHolder;->icone:Lcom/itau/empresas/ui/view/TextViewIcon;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter$ViewHolder;)Landroid/widget/TextView;
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter$ViewHolder;

    .line 381
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter$ViewHolder;->textoDescricao:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter$ViewHolder;)Landroid/widget/TextView;
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter$ViewHolder;

    .line 381
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter$ViewHolder;->textoValor:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter$ViewHolder;)Landroid/view/View;
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter$ViewHolder;

    .line 381
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter$ViewHolder;->linhaVertical:Landroid/view/View;

    return-object v0
.end method

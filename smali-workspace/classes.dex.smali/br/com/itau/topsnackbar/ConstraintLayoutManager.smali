.class final Lbr/com/itau/topsnackbar/ConstraintLayoutManager;
.super Lbr/com/itau/topsnackbar/ViewManager;
.source "ConstraintLayoutManager.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lbr/com/itau/topsnackbar/ViewManager<Landroid/support/constraint/ConstraintLayout;>;"
    }
.end annotation


# instance fields
.field private final firstView:Landroid/view/View;

.field private final toolbar:Landroid/view/View;


# direct methods
.method constructor <init>(Landroid/support/constraint/ConstraintLayout;)V
    .registers 3
    .param p1, "parent"    # Landroid/support/constraint/ConstraintLayout;

    .line 14
    invoke-direct {p0, p1}, Lbr/com/itau/topsnackbar/ViewManager;-><init>(Landroid/view/ViewGroup;)V

    .line 15
    invoke-static {p1}, Lbr/com/itau/topsnackbar/ConstraintLayoutManager;->findToolbar(Landroid/support/constraint/ConstraintLayout;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/topsnackbar/ConstraintLayoutManager;->toolbar:Landroid/view/View;

    .line 16
    iget-object v0, p0, Lbr/com/itau/topsnackbar/ConstraintLayoutManager;->toolbar:Landroid/view/View;

    invoke-direct {p0, v0}, Lbr/com/itau/topsnackbar/ConstraintLayoutManager;->findViewBellow(Landroid/view/View;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/topsnackbar/ConstraintLayoutManager;->firstView:Landroid/view/View;

    .line 17
    return-void
.end method

.method private static findToolbar(Landroid/support/constraint/ConstraintLayout;)Landroid/view/View;
    .registers 6
    .param p0, "parent"    # Landroid/support/constraint/ConstraintLayout;

    .line 20
    invoke-virtual {p0}, Landroid/support/constraint/ConstraintLayout;->getChildCount()I

    move-result v1

    .line 21
    .local v1, "childCount":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_5
    if-ge v2, v1, :cond_1f

    .line 22
    invoke-virtual {p0, v2}, Landroid/support/constraint/ConstraintLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 23
    .local v3, "view":Landroid/view/View;
    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    .line 24
    .local v4, "simpleName":Ljava/lang/String;
    const-string v0, "Toolbar"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 25
    return-object v3

    .line 21
    .end local v3    # "view":Landroid/view/View;
    .end local v4    # "simpleName":Ljava/lang/String;
    :cond_1c
    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    .line 27
    .end local v2    # "i":I
    :cond_1f
    const/4 v0, 0x0

    return-object v0
.end method

.method private findViewBellow(Landroid/view/View;)Landroid/view/View;
    .registers 9
    .param p1, "toolbar"    # Landroid/view/View;

    .line 31
    if-nez p1, :cond_4

    const/4 v0, 0x0

    return-object v0

    .line 32
    :cond_4
    iget-object v0, p0, Lbr/com/itau/topsnackbar/ConstraintLayoutManager;->parent:Landroid/view/ViewGroup;

    check-cast v0, Landroid/support/constraint/ConstraintLayout;

    invoke-virtual {v0}, Landroid/support/constraint/ConstraintLayout;->getChildCount()I

    move-result v1

    .line 33
    .local v1, "childCount":I
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    .line 34
    .local v2, "toolbarId":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_11
    if-ge v3, v1, :cond_2a

    .line 35
    iget-object v0, p0, Lbr/com/itau/topsnackbar/ConstraintLayoutManager;->parent:Landroid/view/ViewGroup;

    check-cast v0, Landroid/support/constraint/ConstraintLayout;

    invoke-virtual {v0, v3}, Landroid/support/constraint/ConstraintLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 36
    .local v4, "view":Landroid/view/View;
    invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Landroid/support/constraint/ConstraintLayout$LayoutParams;

    .line 37
    .local v5, "params":Landroid/support/constraint/ConstraintLayout$LayoutParams;
    iget v6, v5, Landroid/support/constraint/ConstraintLayout$LayoutParams;->topToBottom:I

    .line 38
    .local v6, "bellowId":I
    if-ne v6, v2, :cond_27

    return-object v4

    .line 34
    .end local v4    # "view":Landroid/view/View;
    .end local v5    # "params":Landroid/support/constraint/ConstraintLayout$LayoutParams;
    .end local v6    # "bellowId":I
    :cond_27
    add-int/lit8 v3, v3, 0x1

    goto :goto_11

    .line 40
    .end local v3    # "i":I
    :cond_2a
    const/4 v0, 0x0

    return-object v0
.end method

.method private makeFirstViewBelow(Landroid/view/View;)V
    .registers 4
    .param p1, "content"    # Landroid/view/View;

    .line 53
    iget-object v0, p0, Lbr/com/itau/topsnackbar/ConstraintLayoutManager;->firstView:Landroid/view/View;

    if-nez v0, :cond_5

    return-void

    .line 54
    :cond_5
    iget-object v0, p0, Lbr/com/itau/topsnackbar/ConstraintLayoutManager;->firstView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Landroid/support/constraint/ConstraintLayout$LayoutParams;

    .line 55
    .local v1, "params":Landroid/support/constraint/ConstraintLayout$LayoutParams;
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    iput v0, v1, Landroid/support/constraint/ConstraintLayout$LayoutParams;->topToBottom:I

    .line 56
    iget-object v0, p0, Lbr/com/itau/topsnackbar/ConstraintLayoutManager;->firstView:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 57
    return-void
.end method


# virtual methods
.method addContent(Landroid/view/View;)V
    .registers 4
    .param p1, "content"    # Landroid/view/View;

    .line 44
    iget-object v0, p0, Lbr/com/itau/topsnackbar/ConstraintLayoutManager;->parent:Landroid/view/ViewGroup;

    check-cast v0, Landroid/support/constraint/ConstraintLayout;

    invoke-virtual {v0, p1}, Landroid/support/constraint/ConstraintLayout;->addView(Landroid/view/View;)V

    .line 45
    iget-object v0, p0, Lbr/com/itau/topsnackbar/ConstraintLayoutManager;->toolbar:Landroid/view/View;

    if-nez v0, :cond_c

    return-void

    .line 46
    :cond_c
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Landroid/support/constraint/ConstraintLayout$LayoutParams;

    .line 47
    .local v1, "params":Landroid/support/constraint/ConstraintLayout$LayoutParams;
    iget-object v0, p0, Lbr/com/itau/topsnackbar/ConstraintLayoutManager;->toolbar:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getId()I

    move-result v0

    iput v0, v1, Landroid/support/constraint/ConstraintLayout$LayoutParams;->topToBottom:I

    .line 48
    invoke-virtual {p1, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 49
    invoke-direct {p0, p1}, Lbr/com/itau/topsnackbar/ConstraintLayoutManager;->makeFirstViewBelow(Landroid/view/View;)V

    .line 50
    return-void
.end method

.method removeContent(Landroid/view/View;)V
    .registers 3
    .param p1, "content"    # Landroid/view/View;

    .line 61
    iget-object v0, p0, Lbr/com/itau/topsnackbar/ConstraintLayoutManager;->parent:Landroid/view/ViewGroup;

    check-cast v0, Landroid/support/constraint/ConstraintLayout;

    invoke-virtual {v0, p1}, Landroid/support/constraint/ConstraintLayout;->removeView(Landroid/view/View;)V

    .line 62
    iget-object v0, p0, Lbr/com/itau/topsnackbar/ConstraintLayoutManager;->toolbar:Landroid/view/View;

    if-eqz v0, :cond_14

    iget-object v0, p0, Lbr/com/itau/topsnackbar/ConstraintLayoutManager;->firstView:Landroid/view/View;

    if-eqz v0, :cond_14

    iget-object v0, p0, Lbr/com/itau/topsnackbar/ConstraintLayoutManager;->toolbar:Landroid/view/View;

    invoke-direct {p0, v0}, Lbr/com/itau/topsnackbar/ConstraintLayoutManager;->makeFirstViewBelow(Landroid/view/View;)V

    .line 63
    :cond_14
    return-void
.end method

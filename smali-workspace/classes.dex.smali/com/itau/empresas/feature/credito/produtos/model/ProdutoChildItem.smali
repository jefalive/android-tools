.class public Lcom/itau/empresas/feature/credito/produtos/model/ProdutoChildItem;
.super Ljava/lang/Object;
.source "ProdutoChildItem.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private descricao:Ljava/lang/String;

.field private detalheProdutoMultiLimite:Lcom/itau/empresas/feature/credito/produtos/model/MultiLimiteVO$DetalheProdutoMultiLimiteVO;

.field private produtosParcelados:Lcom/itau/empresas/feature/credito/produtos/model/ProdutosParceladosVO;

.field private produtosRotativos:Lcom/itau/empresas/feature/credito/produtos/model/ProdutosRotativosVO;

.field private valor:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getDescricao()Ljava/lang/String;
    .registers 2

    .line 15
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/model/ProdutoChildItem;->descricao:Ljava/lang/String;

    return-object v0
.end method

.method public getValor()Ljava/lang/String;
    .registers 2

    .line 23
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/model/ProdutoChildItem;->valor:Ljava/lang/String;

    return-object v0
.end method

.method public setDescricao(Ljava/lang/String;)V
    .registers 2
    .param p1, "descricao"    # Ljava/lang/String;

    .line 19
    iput-object p1, p0, Lcom/itau/empresas/feature/credito/produtos/model/ProdutoChildItem;->descricao:Ljava/lang/String;

    .line 20
    return-void
.end method

.method public setDetalheProdutoMultiLimite(Lcom/itau/empresas/feature/credito/produtos/model/MultiLimiteVO$DetalheProdutoMultiLimiteVO;)V
    .registers 2
    .param p1, "detalheProdutoMultiLimite"    # Lcom/itau/empresas/feature/credito/produtos/model/MultiLimiteVO$DetalheProdutoMultiLimiteVO;

    .line 52
    iput-object p1, p0, Lcom/itau/empresas/feature/credito/produtos/model/ProdutoChildItem;->detalheProdutoMultiLimite:Lcom/itau/empresas/feature/credito/produtos/model/MultiLimiteVO$DetalheProdutoMultiLimiteVO;

    .line 53
    return-void
.end method

.method public setProdutosParcelados(Lcom/itau/empresas/feature/credito/produtos/model/ProdutosParceladosVO;)V
    .registers 2
    .param p1, "produtosParcelados"    # Lcom/itau/empresas/feature/credito/produtos/model/ProdutosParceladosVO;

    .line 35
    iput-object p1, p0, Lcom/itau/empresas/feature/credito/produtos/model/ProdutoChildItem;->produtosParcelados:Lcom/itau/empresas/feature/credito/produtos/model/ProdutosParceladosVO;

    .line 36
    return-void
.end method

.method public setProdutosRotativos(Lcom/itau/empresas/feature/credito/produtos/model/ProdutosRotativosVO;)V
    .registers 2
    .param p1, "produtosRotativos"    # Lcom/itau/empresas/feature/credito/produtos/model/ProdutosRotativosVO;

    .line 43
    iput-object p1, p0, Lcom/itau/empresas/feature/credito/produtos/model/ProdutoChildItem;->produtosRotativos:Lcom/itau/empresas/feature/credito/produtos/model/ProdutosRotativosVO;

    .line 44
    return-void
.end method

.method public setValor(Ljava/lang/String;)V
    .registers 2
    .param p1, "valor"    # Ljava/lang/String;

    .line 27
    iput-object p1, p0, Lcom/itau/empresas/feature/credito/produtos/model/ProdutoChildItem;->valor:Ljava/lang/String;

    .line 28
    return-void
.end method

.class final Lcom/itau/empresas/ui/view/wraper/LinearLayoutWrapper$ListViewAdapterInterator;
.super Ljava/lang/Object;
.source "LinearLayoutWrapper.java"

# interfaces
.implements Lcom/itau/empresas/ui/view/wraper/LinearLayoutWrapper$AdapterInterator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/itau/empresas/ui/view/wraper/LinearLayoutWrapper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ListViewAdapterInterator"
.end annotation


# instance fields
.field private final adapter:Landroid/widget/Adapter;


# direct methods
.method public constructor <init>(Landroid/widget/Adapter;)V
    .registers 2
    .param p1, "adapter"    # Landroid/widget/Adapter;

    .line 163
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 164
    iput-object p1, p0, Lcom/itau/empresas/ui/view/wraper/LinearLayoutWrapper$ListViewAdapterInterator;->adapter:Landroid/widget/Adapter;

    .line 165
    return-void
.end method


# virtual methods
.method public getCount()I
    .registers 2

    .line 169
    iget-object v0, p0, Lcom/itau/empresas/ui/view/wraper/LinearLayoutWrapper$ListViewAdapterInterator;->adapter:Landroid/widget/Adapter;

    invoke-interface {v0}, Landroid/widget/Adapter;->getCount()I

    move-result v0

    return v0
.end method

.method public getView(ILandroid/view/ViewGroup;)Landroid/view/View;
    .registers 5
    .param p1, "index"    # I
    .param p2, "container"    # Landroid/view/ViewGroup;

    .line 174
    iget-object v0, p0, Lcom/itau/empresas/ui/view/wraper/LinearLayoutWrapper$ListViewAdapterInterator;->adapter:Landroid/widget/Adapter;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1, p2}, Landroid/widget/Adapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

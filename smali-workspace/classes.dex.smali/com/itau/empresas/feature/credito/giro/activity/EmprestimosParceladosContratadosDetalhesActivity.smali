.class public Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosContratadosDetalhesActivity;
.super Lcom/itau/empresas/ui/activity/BaseActivity;
.source "EmprestimosParceladosContratadosDetalhesActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field camposComprovanteVO:Lcom/itau/empresas/feature/credito/giro/model/CamposComprovanteVO;

.field toolBar:Landroid/support/v7/widget/Toolbar;

.field viewCreditoContratadosDetalhe:Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 35
    invoke-direct {p0}, Lcom/itau/empresas/ui/activity/BaseActivity;-><init>()V

    return-void
.end method

.method private compartilhaPrintTelaComprovante(Landroid/view/View;)V
    .registers 7
    .param p1, "view"    # Landroid/view/View;

    .line 111
    const v0, 0x7f0e0626

    invoke-static {p1, v0}, Lcom/itau/empresas/ui/util/ViewUtils;->escondeView(Landroid/view/View;I)V

    .line 113
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 114
    .local v2, "views":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    invoke-interface {v2, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 116
    const/4 v3, 0x0

    .line 118
    .local v3, "file":Ljava/io/File;
    const-string v0, "credito_detalhado_contratado_comprovante"

    .line 119
    :try_start_11
    invoke-static {p0, v2, v0}, Lcom/itau/empresas/ui/util/BitmapUtils;->salvarViewsPrint(Landroid/content/Context;Ljava/util/List;Ljava/lang/String;)Ljava/io/File;
    :try_end_14
    .catch Ljava/io/IOException; {:try_start_11 .. :try_end_14} :catch_17

    move-result-object v0

    move-object v3, v0

    .line 122
    goto :goto_27

    .line 120
    :catch_17
    move-exception v4

    .line 121
    .local v4, "e":Ljava/io/IOException;
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v4}, Lbr/com/itau/textovoz/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 124
    .end local v4    # "e":Ljava/io/IOException;
    :goto_27
    if-eqz v3, :cond_2c

    .line 125
    invoke-static {p0, v3}, Lcom/itau/empresas/ui/util/CompartilhamentoUtils;->compartilharArquivoImagem(Landroid/app/Activity;Ljava/io/File;)V

    .line 127
    :cond_2c
    return-void
.end method

.method private constroiToolbar()V
    .registers 3

    .line 54
    const/4 v1, 0x0

    .line 55
    .local v1, "id":I
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosContratadosDetalhesActivity;->camposComprovanteVO:Lcom/itau/empresas/feature/credito/giro/model/CamposComprovanteVO;

    if-eqz v0, :cond_14

    .line 56
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosContratadosDetalhesActivity;->camposComprovanteVO:Lcom/itau/empresas/feature/credito/giro/model/CamposComprovanteVO;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/credito/giro/model/CamposComprovanteVO;->getIndicadorSeguro()Z

    move-result v0

    if-eqz v0, :cond_11

    .line 57
    const v1, 0x7f070699

    goto :goto_14

    .line 60
    :cond_11
    const v1, 0x7f07069a

    .line 64
    :cond_14
    :goto_14
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosContratadosDetalhesActivity;->toolBar:Landroid/support/v7/widget/Toolbar;

    invoke-static {v0, p0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom;->Builder(Landroid/support/v7/widget/Toolbar;Landroid/support/v7/app/AppCompatActivity;)Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    move-result-object v0

    .line 66
    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->comTitulo(I)Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    move-result-object v0

    .line 67
    invoke-virtual {v0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->comClickListenerFinish()Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    move-result-object v0

    .line 68
    invoke-virtual {v0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->build()Lcom/itau/empresas/ui/util/listener/ToolbarCustom;

    .line 69
    return-void
.end method

.method public static intent(Landroid/content/Context;)Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosContratadosDetalhesActivity_$IntentBuilder_;
    .registers 2
    .param p0, "context"    # Landroid/content/Context;

    .line 135
    new-instance v0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosContratadosDetalhesActivity_$IntentBuilder_;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosContratadosDetalhesActivity_$IntentBuilder_;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method private verificaPermissoes()V
    .registers 4

    .line 73
    .line 74
    invoke-static {}, Lcom/itau/empresas/ui/util/PermissoesUtil;->getPermissionWriteExternalStorage()[Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/itau/empresas/ui/util/PermissoesUtil;->hasSelfPermissions(Landroid/content/Context;[Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_19

    .line 75
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x17

    if-lt v0, v1, :cond_36

    .line 76
    invoke-static {}, Lcom/itau/empresas/ui/util/PermissoesUtil;->getPermissionWriteExternalStorage()[Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosContratadosDetalhesActivity;->requestPermissions([Ljava/lang/String;I)V

    goto :goto_36

    .line 81
    :cond_19
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosContratadosDetalhesActivity;->viewCreditoContratadosDetalhe:Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;

    iget-object v0, v0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->botaoCreditoDetalhadoCompartilhar:Landroid/widget/Button;

    .line 82
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 83
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosContratadosDetalhesActivity;->viewCreditoContratadosDetalhe:Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;

    iget-object v0, v0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->svEmprestimosParceladosSimulacao:Landroid/widget/ScrollView;

    .line 84
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 85
    .local v2, "view":Landroid/view/View;
    invoke-direct {p0, v2}, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosContratadosDetalhesActivity;->compartilhaPrintTelaComprovante(Landroid/view/View;)V

    .line 86
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosContratadosDetalhesActivity;->viewCreditoContratadosDetalhe:Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;

    iget-object v0, v0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->botaoCreditoDetalhadoCompartilhar:Landroid/widget/Button;

    .line 87
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 89
    .end local v2    # "view":Landroid/view/View;
    :cond_36
    :goto_36
    return-void
.end method


# virtual methods
.method aoCarregarElementosDeTela()V
    .registers 3

    .line 45
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosContratadosDetalhesActivity;->constroiToolbar()V

    .line 46
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosContratadosDetalhesActivity;->viewCreditoContratadosDetalhe:Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosContratadosDetalhesActivity;->camposComprovanteVO:Lcom/itau/empresas/feature/credito/giro/model/CamposComprovanteVO;

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->bind(Lcom/itau/empresas/feature/credito/giro/model/CamposComprovanteVO;)V

    .line 47
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .registers 2
    .param p1, "v"    # Landroid/view/View;

    .line 130
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosContratadosDetalhesActivity;->onBackPressed()V

    .line 131
    return-void
.end method

.method public onEventMainThread(Lcom/itau/empresas/Evento$EventoSolicitarVerificacaoPermissaoCompartilhar;)V
    .registers 2
    .param p1, "evento"    # Lcom/itau/empresas/Evento$EventoSolicitarVerificacaoPermissaoCompartilhar;

    .line 92
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosContratadosDetalhesActivity;->verificaPermissoes()V

    .line 93
    return-void
.end method

.method public onRequestPermissionsResult(I[Ljava/lang/String;[I)V
    .registers 7
    .param p1, "requestCode"    # I
    .param p2, "permissions"    # [Ljava/lang/String;
    .param p3, "grantResults"    # [I

    .line 97
    const/4 v0, 0x4

    if-ne p1, v0, :cond_28

    array-length v0, p3

    if-lez v0, :cond_28

    const/4 v0, 0x0

    aget v0, p3, v0

    if-nez v0, :cond_28

    .line 99
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosContratadosDetalhesActivity;->viewCreditoContratadosDetalhe:Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;

    iget-object v0, v0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->botaoCreditoDetalhadoCompartilhar:Landroid/widget/Button;

    .line 100
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 101
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosContratadosDetalhesActivity;->viewCreditoContratadosDetalhe:Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;

    iget-object v0, v0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->svEmprestimosParceladosSimulacao:Landroid/widget/ScrollView;

    .line 102
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 103
    .local v2, "view":Landroid/view/View;
    invoke-direct {p0, v2}, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosContratadosDetalhesActivity;->compartilhaPrintTelaComprovante(Landroid/view/View;)V

    .line 104
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosContratadosDetalhesActivity;->viewCreditoContratadosDetalhe:Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;

    iget-object v0, v0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->botaoCreditoDetalhadoCompartilhar:Landroid/widget/Button;

    .line 105
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 107
    .end local v2    # "view":Landroid/view/View;
    :cond_28
    return-void
.end method

.method public opkeysConsumidas()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Ljava/lang/String;>;"
        }
    .end annotation

    .line 50
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

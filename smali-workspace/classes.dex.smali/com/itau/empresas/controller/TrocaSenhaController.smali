.class public Lcom/itau/empresas/controller/TrocaSenhaController;
.super Lcom/itau/empresas/controller/BaseController;
.source "TrocaSenhaController.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/itau/empresas/controller/BaseController<Lcom/itau/empresas/api/Api;>;"
    }
.end annotation


# instance fields
.field menuController:Lcom/itau/empresas/feature/login/controller/MenuController;

.field operadorController:Lcom/itau/empresas/feature/login/controller/OperadorController;


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 16
    invoke-direct {p0}, Lcom/itau/empresas/controller/BaseController;-><init>()V

    return-void
.end method


# virtual methods
.method public cadastroSenha(Ljava/lang/String;Lcom/itau/empresas/api/model/CadastroSenhaEletronicaVO;)V
    .registers 4
    .param p1, "codigoOperador"    # Ljava/lang/String;
    .param p2, "cadastroSenhaVO"    # Lcom/itau/empresas/api/model/CadastroSenhaEletronicaVO;

    .line 27
    new-instance v0, Lcom/itau/empresas/controller/TrocaSenhaController$1;

    invoke-direct {v0, p0, p1, p2}, Lcom/itau/empresas/controller/TrocaSenhaController$1;-><init>(Lcom/itau/empresas/controller/TrocaSenhaController;Ljava/lang/String;Lcom/itau/empresas/api/model/CadastroSenhaEletronicaVO;)V

    invoke-static {v0}, Lbr/com/itau/sdk/android/core/BackendClient;->callApi(Lbr/com/itau/sdk/android/core/type/BackendCall;)V

    .line 33
    return-void
.end method

.method public trocarSenhaObrigatoria(Ljava/lang/String;Lcom/itau/empresas/api/model/CadastroSenhaEletronicaVO;)V
    .registers 4
    .param p1, "codigoOperador"    # Ljava/lang/String;
    .param p2, "cadastroSenhaVO"    # Lcom/itau/empresas/api/model/CadastroSenhaEletronicaVO;

    .line 37
    new-instance v0, Lcom/itau/empresas/controller/TrocaSenhaController$2;

    invoke-direct {v0, p0, p1, p2}, Lcom/itau/empresas/controller/TrocaSenhaController$2;-><init>(Lcom/itau/empresas/controller/TrocaSenhaController;Ljava/lang/String;Lcom/itau/empresas/api/model/CadastroSenhaEletronicaVO;)V

    invoke-static {v0}, Lbr/com/itau/sdk/android/core/BackendClient;->callApi(Lbr/com/itau/sdk/android/core/type/BackendCall;)V

    .line 48
    return-void
.end method

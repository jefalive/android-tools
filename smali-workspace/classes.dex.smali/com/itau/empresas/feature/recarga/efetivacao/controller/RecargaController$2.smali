.class Lcom/itau/empresas/feature/recarga/efetivacao/controller/RecargaController$2;
.super Ljava/lang/Object;
.source "RecargaController.java"

# interfaces
.implements Lbr/com/itau/sdk/android/core/type/BackendCall;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/itau/empresas/feature/recarga/efetivacao/controller/RecargaController;->buscaDetalheOperadoraRecarga(Ljava/lang/String;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/itau/empresas/feature/recarga/efetivacao/controller/RecargaController;

.field final synthetic val$idOperadora:Ljava/lang/String;

.field final synthetic val$nomeRegiao:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/itau/empresas/feature/recarga/efetivacao/controller/RecargaController;Ljava/lang/String;Ljava/lang/String;)V
    .registers 4
    .param p1, "this$0"    # Lcom/itau/empresas/feature/recarga/efetivacao/controller/RecargaController;

    .line 50
    iput-object p1, p0, Lcom/itau/empresas/feature/recarga/efetivacao/controller/RecargaController$2;->this$0:Lcom/itau/empresas/feature/recarga/efetivacao/controller/RecargaController;

    iput-object p2, p0, Lcom/itau/empresas/feature/recarga/efetivacao/controller/RecargaController$2;->val$idOperadora:Ljava/lang/String;

    iput-object p3, p0, Lcom/itau/empresas/feature/recarga/efetivacao/controller/RecargaController$2;->val$nomeRegiao:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public callBackend()V
    .registers 10

    .line 53
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/controller/RecargaController$2;->this$0:Lcom/itau/empresas/feature/recarga/efetivacao/controller/RecargaController;

    iget-object v0, v0, Lcom/itau/empresas/feature/recarga/efetivacao/controller/RecargaController;->app:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getContaSelecionada()Lcom/itau/empresas/api/model/ContaOperadorVO;

    move-result-object v8

    .line 55
    .local v8, "contaSelecionada":Lcom/itau/empresas/api/model/ContaOperadorVO;
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/controller/RecargaController$2;->this$0:Lcom/itau/empresas/feature/recarga/efetivacao/controller/RecargaController;

    # invokes: Lcom/itau/empresas/feature/recarga/efetivacao/controller/RecargaController;->api()Ljava/lang/Object;
    invoke-static {v0}, Lcom/itau/empresas/feature/recarga/efetivacao/controller/RecargaController;->access$400(Lcom/itau/empresas/feature/recarga/efetivacao/controller/RecargaController;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/api/Api;

    iget-object v1, p0, Lcom/itau/empresas/feature/recarga/efetivacao/controller/RecargaController$2;->val$idOperadora:Ljava/lang/String;

    iget-object v2, p0, Lcom/itau/empresas/feature/recarga/efetivacao/controller/RecargaController$2;->val$nomeRegiao:Ljava/lang/String;

    .line 57
    invoke-virtual {v8}, Lcom/itau/empresas/api/model/ContaOperadorVO;->getAgencia()Ljava/lang/String;

    move-result-object v3

    .line 58
    invoke-virtual {v8}, Lcom/itau/empresas/api/model/ContaOperadorVO;->getConta()Ljava/lang/String;

    move-result-object v4

    .line 59
    invoke-virtual {v8}, Lcom/itau/empresas/api/model/ContaOperadorVO;->getDac()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/itau/empresas/feature/recarga/efetivacao/controller/RecargaController$2;->this$0:Lcom/itau/empresas/feature/recarga/efetivacao/controller/RecargaController;

    iget-object v6, v6, Lcom/itau/empresas/feature/recarga/efetivacao/controller/RecargaController;->app:Lcom/itau/empresas/CustomApplication;

    .line 61
    invoke-virtual {v6}, Lcom/itau/empresas/CustomApplication;->getCodigoOperador()Ljava/lang/String;

    move-result-object v7

    .line 55
    const/4 v6, 0x0

    invoke-interface/range {v0 .. v7}, Lcom/itau/empresas/api/Api;->buscaDetalheOperadoraRecarga(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/itau/empresas/feature/recarga/contatos/model/RecargaDetalheOperadoraVO;

    move-result-object v0

    # invokes: Lcom/itau/empresas/feature/recarga/efetivacao/controller/RecargaController;->post(Ljava/lang/Object;)V
    invoke-static {v0}, Lcom/itau/empresas/feature/recarga/efetivacao/controller/RecargaController;->access$500(Ljava/lang/Object;)V

    .line 62
    return-void
.end method

.class Lnet/hockeyapp/android/utils/VersionHelper$1;
.super Ljava/lang/Object;
.source "VersionHelper.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lnet/hockeyapp/android/utils/VersionHelper;->sortVersions()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;Ljava/util/Comparator<Lorg/json/JSONObject;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lnet/hockeyapp/android/utils/VersionHelper;


# direct methods
.method constructor <init>(Lnet/hockeyapp/android/utils/VersionHelper;)V
    .registers 2
    .param p1, "this$0"    # Lnet/hockeyapp/android/utils/VersionHelper;

    .line 97
    iput-object p1, p0, Lnet/hockeyapp/android/utils/VersionHelper$1;->this$0:Lnet/hockeyapp/android/utils/VersionHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .registers 5

    .line 97
    move-object v0, p1

    check-cast v0, Lorg/json/JSONObject;

    move-object v1, p2

    check-cast v1, Lorg/json/JSONObject;

    invoke-virtual {p0, v0, v1}, Lnet/hockeyapp/android/utils/VersionHelper$1;->compare(Lorg/json/JSONObject;Lorg/json/JSONObject;)I

    move-result v0

    return v0
.end method

.method public compare(Lorg/json/JSONObject;Lorg/json/JSONObject;)I
    .registers 6
    .param p1, "object1"    # Lorg/json/JSONObject;
    .param p2, "object2"    # Lorg/json/JSONObject;

    .line 100
    const-string v0, "version"

    :try_start_2
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v0

    const-string v1, "version"

    invoke-virtual {p2, v1}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I
    :try_end_b
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_b} :catch_11
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_b} :catch_13

    move-result v1

    if-le v0, v1, :cond_10

    .line 101
    const/4 v0, 0x0

    return v0

    .line 107
    :cond_10
    goto :goto_14

    .line 104
    :catch_11
    move-exception v2

    .line 107
    goto :goto_14

    .line 106
    :catch_13
    move-exception v2

    .line 109
    :goto_14
    const/4 v0, 0x0

    return v0
.end method

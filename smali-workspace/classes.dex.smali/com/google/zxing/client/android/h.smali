.class public final Lcom/google/zxing/client/android/h;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/content/DialogInterface$OnCancelListener;
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field private final a:Landroid/app/Activity;


# direct methods
.method public constructor <init>(Landroid/app/Activity;)V
    .registers 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/zxing/client/android/h;->a:Landroid/app/Activity;

    return-void
.end method

.method private a()V
    .registers 2

    iget-object v0, p0, Lcom/google/zxing/client/android/h;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    return-void
.end method


# virtual methods
.method public onCancel(Landroid/content/DialogInterface;)V
    .registers 2

    invoke-direct {p0}, Lcom/google/zxing/client/android/h;->a()V

    return-void
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .registers 3

    invoke-direct {p0}, Lcom/google/zxing/client/android/h;->a()V

    return-void
.end method

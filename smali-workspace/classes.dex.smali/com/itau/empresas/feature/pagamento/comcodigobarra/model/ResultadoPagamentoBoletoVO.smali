.class public Lcom/itau/empresas/feature/pagamento/comcodigobarra/model/ResultadoPagamentoBoletoVO;
.super Ljava/lang/Object;
.source "ResultadoPagamentoBoletoVO.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private dadosComplementaresPagamentoCodigoBarra:Lcom/itau/empresas/feature/pagamento/comcodigobarra/model/DadosComplementaresPagamentoCodigoBarraVO;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "dados_complementares_pagamento_codigo_barra"
    .end annotation
.end field

.field private dadosConfirmacaoComprovante:Lcom/itau/empresas/feature/pagamento/comcodigobarra/model/DadosConfirmacaoComprovanteVO;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "dados_confirmacao_comprovante_pagamento"
    .end annotation
.end field

.field private dadosPagamentoCodigoBarraComCartao:Lcom/itau/empresas/api/model/DadosPagamentoCodigoBarraComCartaoVO;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "dados_pagamento_codigo_barra_com_cartao"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

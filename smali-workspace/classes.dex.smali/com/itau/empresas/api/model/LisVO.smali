.class public Lcom/itau/empresas/api/model/LisVO;
.super Ljava/lang/Object;
.source "LisVO.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private dataVencimento:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "data_vencimento"
    .end annotation
.end field

.field private limiteAdicional:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "limite_adicional"
    .end annotation
.end field

.field private limiteDisponivel:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "limite_disponivel"
    .end annotation
.end field

.field private limiteLis:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "limite_lis"
    .end annotation
.end field

.field private limiteTotal:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "limite_total"
    .end annotation
.end field

.field private limiteUtilizado:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "limite_utilizado"
    .end annotation
.end field

.field private taxas:Lcom/itau/empresas/api/model/TaxasLimiteVO;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "taxas"
    .end annotation
.end field

.field private utilizacao:Lcom/itau/empresas/api/model/UtilizacaoVO;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "utilizacao"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getDataVencimento()Ljava/lang/String;
    .registers 2

    .line 55
    iget-object v0, p0, Lcom/itau/empresas/api/model/LisVO;->dataVencimento:Ljava/lang/String;

    return-object v0
.end method

.method public getLimiteAdicional()Ljava/lang/String;
    .registers 2

    .line 42
    iget-object v0, p0, Lcom/itau/empresas/api/model/LisVO;->limiteAdicional:Ljava/lang/String;

    return-object v0
.end method

.method public getLimiteDisponivel()Ljava/lang/String;
    .registers 2

    .line 38
    iget-object v0, p0, Lcom/itau/empresas/api/model/LisVO;->limiteDisponivel:Ljava/lang/String;

    return-object v0
.end method

.method public getLimiteLis()Ljava/lang/String;
    .registers 2

    .line 51
    iget-object v0, p0, Lcom/itau/empresas/api/model/LisVO;->limiteLis:Ljava/lang/String;

    return-object v0
.end method

.method public getLimiteTotal()Ljava/lang/String;
    .registers 2

    .line 34
    iget-object v0, p0, Lcom/itau/empresas/api/model/LisVO;->limiteTotal:Ljava/lang/String;

    return-object v0
.end method

.method public getLimiteUtilizado()Ljava/lang/String;
    .registers 2

    .line 46
    iget-object v0, p0, Lcom/itau/empresas/api/model/LisVO;->limiteUtilizado:Ljava/lang/String;

    return-object v0
.end method

.method public getTaxas()Lcom/itau/empresas/api/model/TaxasLimiteVO;
    .registers 2

    .line 63
    iget-object v0, p0, Lcom/itau/empresas/api/model/LisVO;->taxas:Lcom/itau/empresas/api/model/TaxasLimiteVO;

    return-object v0
.end method

.method public getUtilizacao()Lcom/itau/empresas/api/model/UtilizacaoVO;
    .registers 2

    .line 59
    iget-object v0, p0, Lcom/itau/empresas/api/model/LisVO;->utilizacao:Lcom/itau/empresas/api/model/UtilizacaoVO;

    return-object v0
.end method

.class public Lcom/itau/empresas/api/ApiHelper;
.super Ljava/lang/Object;
.source "ApiHelper.java"


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static devoTratarEvento(Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;Lcom/itau/empresas/api/ApiConsumidor;)Z
    .registers 6
    .param p0, "evento"    # Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;
    .param p1, "consumidor"    # Lcom/itau/empresas/api/ApiConsumidor;

    .line 19
    invoke-virtual {p0}, Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;->getException()Lbr/com/itau/sdk/android/core/exception/BackendException;

    move-result-object v1

    .line 20
    .local v1, "ex":Lbr/com/itau/sdk/android/core/exception/BackendException;
    if-eqz v1, :cond_17

    .line 21
    invoke-virtual {v1}, Lbr/com/itau/sdk/android/core/exception/BackendException;->getOpKey()Ljava/lang/String;

    move-result-object v2

    .line 22
    .local v2, "opkey":Ljava/lang/String;
    invoke-interface {p1}, Lcom/itau/empresas/api/ApiConsumidor;->opkeysConsumidas()Ljava/util/List;

    move-result-object v3

    .line 23
    .local v3, "opkeysUsadas":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-eqz v2, :cond_17

    if-eqz v3, :cond_17

    .line 24
    invoke-interface {v3, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0

    .line 28
    .end local v2    # "opkey":Ljava/lang/String;
    .end local v3    # "opkeysUsadas":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v3
    :cond_17
    const/4 v0, 0x0

    return v0
.end method

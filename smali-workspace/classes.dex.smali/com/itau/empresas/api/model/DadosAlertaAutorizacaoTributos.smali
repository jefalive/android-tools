.class public Lcom/itau/empresas/api/model/DadosAlertaAutorizacaoTributos;
.super Ljava/lang/Object;
.source "DadosAlertaAutorizacaoTributos.java"


# instance fields
.field private codigoMotivo:Ljava/lang/String;

.field private colorStatusPendente:I

.field private dadodStatusAutorizacaoTributos:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<Ljava/lang/String;Lcom/itau/empresas/api/model/StatusPagamentoTributos;>;"
        }
    .end annotation
.end field

.field private situcao:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .registers 3
    .param p1, "situacao"    # Ljava/lang/String;
    .param p2, "codigoMotivo"    # Ljava/lang/String;

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    iput-object p2, p0, Lcom/itau/empresas/api/model/DadosAlertaAutorizacaoTributos;->codigoMotivo:Ljava/lang/String;

    .line 19
    iput-object p1, p0, Lcom/itau/empresas/api/model/DadosAlertaAutorizacaoTributos;->situcao:Ljava/lang/String;

    .line 20
    invoke-direct {p0}, Lcom/itau/empresas/api/model/DadosAlertaAutorizacaoTributos;->setColorStatusPendente()V

    .line 21
    invoke-direct {p0}, Lcom/itau/empresas/api/model/DadosAlertaAutorizacaoTributos;->init()V

    .line 22
    return-void
.end method

.method private init()V
    .registers 9

    .line 25
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/api/model/DadosAlertaAutorizacaoTributos;->dadodStatusAutorizacaoTributos:Ljava/util/Map;

    .line 26
    iget-object v0, p0, Lcom/itau/empresas/api/model/DadosAlertaAutorizacaoTributos;->dadodStatusAutorizacaoTributos:Ljava/util/Map;

    const-string v1, "EF"

    new-instance v2, Lcom/itau/empresas/api/model/StatusPagamentoTributos;

    const-string v3, "com sucesso"

    const-string v7, "pagamento efetivado"

    const v4, 0x7f0c014d

    const v5, 0x7f0c014d

    const v6, 0x7f07025e

    invoke-direct/range {v2 .. v7}, Lcom/itau/empresas/api/model/StatusPagamentoTributos;-><init>(Ljava/lang/String;IIILjava/lang/String;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 30
    iget-object v0, p0, Lcom/itau/empresas/api/model/DadosAlertaAutorizacaoTributos;->dadodStatusAutorizacaoTributos:Ljava/util/Map;

    const-string v1, "PE"

    new-instance v2, Lcom/itau/empresas/api/model/StatusPagamentoTributos;

    const-string v3, "pendente de efetiva\u00e7\u00e3o"

    iget v4, p0, Lcom/itau/empresas/api/model/DadosAlertaAutorizacaoTributos;->colorStatusPendente:I

    iget v5, p0, Lcom/itau/empresas/api/model/DadosAlertaAutorizacaoTributos;->colorStatusPendente:I

    const-string v7, "pagamento"

    const v6, 0x7f07024d

    invoke-direct/range {v2 .. v7}, Lcom/itau/empresas/api/model/StatusPagamentoTributos;-><init>(Ljava/lang/String;IIILjava/lang/String;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 34
    iget-object v0, p0, Lcom/itau/empresas/api/model/DadosAlertaAutorizacaoTributos;->dadodStatusAutorizacaoTributos:Ljava/util/Map;

    const-string v1, "PA"

    new-instance v2, Lcom/itau/empresas/api/model/StatusPagamentoTributos;

    const-string v3, "parcialmente autorizado"

    const-string v7, "pagamento "

    const v4, 0x7f0c00cf

    const v5, 0x7f0c00cf

    const v6, 0x7f07027d

    invoke-direct/range {v2 .. v7}, Lcom/itau/empresas/api/model/StatusPagamentoTributos;-><init>(Ljava/lang/String;IIILjava/lang/String;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 38
    iget-object v0, p0, Lcom/itau/empresas/api/model/DadosAlertaAutorizacaoTributos;->dadodStatusAutorizacaoTributos:Ljava/util/Map;

    const-string v1, "NA"

    new-instance v2, Lcom/itau/empresas/api/model/StatusPagamentoTributos;

    const-string v3, "n\u00e3o autorizado"

    const-string v7, "pagamento"

    const v4, 0x7f0c014f

    const v5, 0x7f0c014f

    const v6, 0x7f070260

    invoke-direct/range {v2 .. v7}, Lcom/itau/empresas/api/model/StatusPagamentoTributos;-><init>(Ljava/lang/String;IIILjava/lang/String;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 42
    return-void
.end method

.method private setColorStatusPendente()V
    .registers 3

    .line 45
    iget-object v0, p0, Lcom/itau/empresas/api/model/DadosAlertaAutorizacaoTributos;->codigoMotivo:Ljava/lang/String;

    const-string v1, "08"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 46
    const v0, 0x7f0c00cf

    iput v0, p0, Lcom/itau/empresas/api/model/DadosAlertaAutorizacaoTributos;->colorStatusPendente:I

    goto :goto_15

    .line 48
    :cond_10
    const v0, 0x7f0c012b

    iput v0, p0, Lcom/itau/empresas/api/model/DadosAlertaAutorizacaoTributos;->colorStatusPendente:I

    .line 51
    :goto_15
    return-void
.end method


# virtual methods
.method public getDadodStatusAutorizacaoTributos()Lcom/itau/empresas/api/model/StatusPagamentoTributos;
    .registers 3

    .line 54
    iget-object v0, p0, Lcom/itau/empresas/api/model/DadosAlertaAutorizacaoTributos;->dadodStatusAutorizacaoTributos:Ljava/util/Map;

    iget-object v1, p0, Lcom/itau/empresas/api/model/DadosAlertaAutorizacaoTributos;->situcao:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/api/model/StatusPagamentoTributos;

    return-object v0
.end method

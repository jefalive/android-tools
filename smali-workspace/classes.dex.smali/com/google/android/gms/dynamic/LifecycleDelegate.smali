.class public interface abstract Lcom/google/android/gms/dynamic/LifecycleDelegate;
.super Ljava/lang/Object;


# virtual methods
.method public abstract onCreate(Landroid/os/Bundle;)V
.end method

.method public abstract onDestroy()V
.end method

.method public abstract onPause()V
.end method

.method public abstract onResume()V
.end method

.class public Lcom/itau/empresas/feature/credito/produtos/model/ProdutosVO;
.super Ljava/lang/Object;
.source "ProdutosVO.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field listaProdutosParcelados:Ljava/util/List;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "produtos_parcelados"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Lcom/itau/empresas/feature/credito/produtos/model/ProdutosParceladosVO;>;"
        }
    .end annotation
.end field

.field listaProdutosRotativos:Ljava/util/List;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "produtos_rotativos"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Lcom/itau/empresas/feature/credito/produtos/model/ProdutosRotativosVO;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 2

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/model/ProdutosVO;->listaProdutosRotativos:Ljava/util/List;

    .line 14
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/model/ProdutosVO;->listaProdutosParcelados:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public getListaProdutosParcelados()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Lcom/itau/empresas/feature/credito/produtos/model/ProdutosParceladosVO;>;"
        }
    .end annotation

    .line 22
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/model/ProdutosVO;->listaProdutosParcelados:Ljava/util/List;

    return-object v0
.end method

.method public getListaProdutosRotativos()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Lcom/itau/empresas/feature/credito/produtos/model/ProdutosRotativosVO;>;"
        }
    .end annotation

    .line 18
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/model/ProdutosVO;->listaProdutosRotativos:Ljava/util/List;

    return-object v0
.end method

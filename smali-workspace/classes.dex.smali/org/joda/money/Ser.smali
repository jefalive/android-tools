.class final Lorg/joda/money/Ser;
.super Ljava/lang/Object;
.source "Ser.java"

# interfaces
.implements Ljava/io/Externalizable;


# static fields
.field static final BIG_MONEY:B = 0x42t

.field static final CURRENCY_UNIT:B = 0x43t

.field static final MONEY:B = 0x4dt


# instance fields
.field private object:Ljava/lang/Object;

.field private type:B


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    return-void
.end method

.method constructor <init>(BLjava/lang/Object;)V
    .registers 3
    .param p1, "type"    # B
    .param p2, "object"    # Ljava/lang/Object;

    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    iput-byte p1, p0, Lorg/joda/money/Ser;->type:B

    .line 61
    iput-object p2, p0, Lorg/joda/money/Ser;->object:Ljava/lang/Object;

    .line 62
    return-void
.end method

.method private readBigMoney(Ljava/io/ObjectInput;)Lorg/joda/money/BigMoney;
    .registers 8
    .param p1, "in"    # Ljava/io/ObjectInput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 136
    invoke-direct {p0, p1}, Lorg/joda/money/Ser;->readCurrency(Ljava/io/ObjectInput;)Lorg/joda/money/CurrencyUnit;

    move-result-object v2

    .line 137
    .local v2, "currency":Lorg/joda/money/CurrencyUnit;
    invoke-interface {p1}, Ljava/io/ObjectInput;->readInt()I

    move-result v0

    new-array v3, v0, [B

    .line 138
    .local v3, "bytes":[B
    invoke-interface {p1, v3}, Ljava/io/ObjectInput;->readFully([B)V

    .line 139
    new-instance v4, Ljava/math/BigDecimal;

    new-instance v0, Ljava/math/BigInteger;

    invoke-direct {v0, v3}, Ljava/math/BigInteger;-><init>([B)V

    invoke-interface {p1}, Ljava/io/ObjectInput;->readInt()I

    move-result v1

    invoke-direct {v4, v0, v1}, Ljava/math/BigDecimal;-><init>(Ljava/math/BigInteger;I)V

    .line 140
    .local v4, "bd":Ljava/math/BigDecimal;
    new-instance v5, Lorg/joda/money/BigMoney;

    invoke-direct {v5, v2, v4}, Lorg/joda/money/BigMoney;-><init>(Lorg/joda/money/CurrencyUnit;Ljava/math/BigDecimal;)V

    .line 141
    .local v5, "bigMoney":Lorg/joda/money/BigMoney;
    return-object v5
.end method

.method private readCurrency(Ljava/io/ObjectInput;)Lorg/joda/money/CurrencyUnit;
    .registers 7
    .param p1, "in"    # Ljava/io/ObjectInput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 145
    invoke-interface {p1}, Ljava/io/ObjectInput;->readUTF()Ljava/lang/String;

    move-result-object v3

    .line 146
    .local v3, "code":Ljava/lang/String;
    invoke-static {v3}, Lorg/joda/money/CurrencyUnit;->of(Ljava/lang/String;)Lorg/joda/money/CurrencyUnit;

    move-result-object v4

    .line 147
    .local v4, "singletonCurrency":Lorg/joda/money/CurrencyUnit;
    invoke-virtual {v4}, Lorg/joda/money/CurrencyUnit;->getNumericCode()I

    move-result v0

    invoke-interface {p1}, Ljava/io/ObjectInput;->readShort()S

    move-result v1

    if-eq v0, v1, :cond_2b

    .line 148
    new-instance v0, Ljava/io/InvalidObjectException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Deserialization found a mismatch in the numeric code for currency "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/InvalidObjectException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 150
    :cond_2b
    invoke-virtual {v4}, Lorg/joda/money/CurrencyUnit;->getDefaultFractionDigits()I

    move-result v0

    invoke-interface {p1}, Ljava/io/ObjectInput;->readShort()S

    move-result v1

    if-eq v0, v1, :cond_4e

    .line 151
    new-instance v0, Ljava/io/InvalidObjectException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Deserialization found a mismatch in the decimal places for currency "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/InvalidObjectException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 153
    :cond_4e
    return-object v4
.end method

.method private readResolve()Ljava/lang/Object;
    .registers 2

    .line 162
    iget-object v0, p0, Lorg/joda/money/Ser;->object:Ljava/lang/Object;

    return-object v0
.end method

.method private writeBigMoney(Ljava/io/ObjectOutput;Lorg/joda/money/BigMoney;)V
    .registers 5
    .param p1, "out"    # Ljava/io/ObjectOutput;
    .param p2, "obj"    # Lorg/joda/money/BigMoney;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 96
    invoke-virtual {p2}, Lorg/joda/money/BigMoney;->getCurrencyUnit()Lorg/joda/money/CurrencyUnit;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lorg/joda/money/Ser;->writeCurrency(Ljava/io/ObjectOutput;Lorg/joda/money/CurrencyUnit;)V

    .line 97
    invoke-virtual {p2}, Lorg/joda/money/BigMoney;->getAmount()Ljava/math/BigDecimal;

    move-result-object v0

    invoke-virtual {v0}, Ljava/math/BigDecimal;->unscaledValue()Ljava/math/BigInteger;

    move-result-object v0

    invoke-virtual {v0}, Ljava/math/BigInteger;->toByteArray()[B

    move-result-object v1

    .line 98
    .local v1, "bytes":[B
    array-length v0, v1

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeInt(I)V

    .line 99
    invoke-interface {p1, v1}, Ljava/io/ObjectOutput;->write([B)V

    .line 100
    invoke-virtual {p2}, Lorg/joda/money/BigMoney;->getScale()I

    move-result v0

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeInt(I)V

    .line 101
    return-void
.end method

.method private writeCurrency(Ljava/io/ObjectOutput;Lorg/joda/money/CurrencyUnit;)V
    .registers 4
    .param p1, "out"    # Ljava/io/ObjectOutput;
    .param p2, "obj"    # Lorg/joda/money/CurrencyUnit;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 104
    invoke-virtual {p2}, Lorg/joda/money/CurrencyUnit;->getCode()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeUTF(Ljava/lang/String;)V

    .line 105
    invoke-virtual {p2}, Lorg/joda/money/CurrencyUnit;->getNumericCode()I

    move-result v0

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeShort(I)V

    .line 106
    invoke-virtual {p2}, Lorg/joda/money/CurrencyUnit;->getDefaultFractionDigits()I

    move-result v0

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeShort(I)V

    .line 107
    return-void
.end method


# virtual methods
.method public readExternal(Ljava/io/ObjectInput;)V
    .registers 4
    .param p1, "in"    # Ljava/io/ObjectInput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .line 117
    invoke-interface {p1}, Ljava/io/ObjectInput;->readByte()B

    move-result v0

    iput-byte v0, p0, Lorg/joda/money/Ser;->type:B

    .line 118
    iget-byte v0, p0, Lorg/joda/money/Ser;->type:B

    sparse-switch v0, :sswitch_data_2e

    goto :goto_26

    .line 120
    :sswitch_c
    invoke-direct {p0, p1}, Lorg/joda/money/Ser;->readBigMoney(Ljava/io/ObjectInput;)Lorg/joda/money/BigMoney;

    move-result-object v0

    iput-object v0, p0, Lorg/joda/money/Ser;->object:Ljava/lang/Object;

    .line 121
    return-void

    .line 124
    :sswitch_13
    new-instance v0, Lorg/joda/money/Money;

    invoke-direct {p0, p1}, Lorg/joda/money/Ser;->readBigMoney(Ljava/io/ObjectInput;)Lorg/joda/money/BigMoney;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/joda/money/Money;-><init>(Lorg/joda/money/BigMoney;)V

    iput-object v0, p0, Lorg/joda/money/Ser;->object:Ljava/lang/Object;

    .line 125
    return-void

    .line 128
    :sswitch_1f
    invoke-direct {p0, p1}, Lorg/joda/money/Ser;->readCurrency(Ljava/io/ObjectInput;)Lorg/joda/money/CurrencyUnit;

    move-result-object v0

    iput-object v0, p0, Lorg/joda/money/Ser;->object:Ljava/lang/Object;

    .line 129
    return-void

    .line 132
    :goto_26
    new-instance v0, Ljava/io/StreamCorruptedException;

    const-string v1, "Serialization input has invalid type"

    invoke-direct {v0, v1}, Ljava/io/StreamCorruptedException;-><init>(Ljava/lang/String;)V

    throw v0

    :sswitch_data_2e
    .sparse-switch
        0x42 -> :sswitch_c
        0x43 -> :sswitch_1f
        0x4d -> :sswitch_13
    .end sparse-switch
.end method

.method public writeExternal(Ljava/io/ObjectOutput;)V
    .registers 5
    .param p1, "out"    # Ljava/io/ObjectOutput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 74
    iget-byte v0, p0, Lorg/joda/money/Ser;->type:B

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeByte(I)V

    .line 75
    iget-byte v0, p0, Lorg/joda/money/Ser;->type:B

    sparse-switch v0, :sswitch_data_32

    goto :goto_2a

    .line 77
    :sswitch_b
    iget-object v0, p0, Lorg/joda/money/Ser;->object:Ljava/lang/Object;

    move-object v2, v0

    check-cast v2, Lorg/joda/money/BigMoney;

    .line 78
    .local v2, "obj":Lorg/joda/money/BigMoney;
    invoke-direct {p0, p1, v2}, Lorg/joda/money/Ser;->writeBigMoney(Ljava/io/ObjectOutput;Lorg/joda/money/BigMoney;)V

    .line 79
    return-void

    .line 82
    .end local v2    # "obj":Lorg/joda/money/BigMoney;
    :sswitch_14
    iget-object v0, p0, Lorg/joda/money/Ser;->object:Ljava/lang/Object;

    move-object v2, v0

    check-cast v2, Lorg/joda/money/Money;

    .line 83
    .local v2, "obj":Lorg/joda/money/Money;
    invoke-virtual {v2}, Lorg/joda/money/Money;->toBigMoney()Lorg/joda/money/BigMoney;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lorg/joda/money/Ser;->writeBigMoney(Ljava/io/ObjectOutput;Lorg/joda/money/BigMoney;)V

    .line 84
    return-void

    .line 87
    .end local v2    # "obj":Lorg/joda/money/Money;
    :sswitch_21
    iget-object v0, p0, Lorg/joda/money/Ser;->object:Ljava/lang/Object;

    move-object v2, v0

    check-cast v2, Lorg/joda/money/CurrencyUnit;

    .line 88
    .local v2, "obj":Lorg/joda/money/CurrencyUnit;
    invoke-direct {p0, p1, v2}, Lorg/joda/money/Ser;->writeCurrency(Ljava/io/ObjectOutput;Lorg/joda/money/CurrencyUnit;)V

    .line 89
    return-void

    .line 92
    .end local v2    # "obj":Lorg/joda/money/CurrencyUnit;
    :goto_2a
    new-instance v0, Ljava/io/InvalidClassException;

    const-string v1, "Joda-Money bug: Serialization broken"

    invoke-direct {v0, v1}, Ljava/io/InvalidClassException;-><init>(Ljava/lang/String;)V

    throw v0

    :sswitch_data_32
    .sparse-switch
        0x42 -> :sswitch_b
        0x43 -> :sswitch_21
        0x4d -> :sswitch_14
    .end sparse-switch
.end method

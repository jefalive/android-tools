.class final synthetic Lbr/com/itau/sdk/android/core/login/c;
.super Ljava/lang/Object;

# interfaces
.implements Lbr/com/itau/sdk/android/core/login/EletronicPassword$Callback;


# instance fields
.field private final a:Lbr/com/itau/sdk/android/core/login/b;

.field private final b:Lbr/com/itau/sdk/android/core/login/EletronicPassword;


# direct methods
.method private constructor <init>(Lbr/com/itau/sdk/android/core/login/b;Lbr/com/itau/sdk/android/core/login/EletronicPassword;)V
    .registers 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lbr/com/itau/sdk/android/core/login/c;->a:Lbr/com/itau/sdk/android/core/login/b;

    iput-object p2, p0, Lbr/com/itau/sdk/android/core/login/c;->b:Lbr/com/itau/sdk/android/core/login/EletronicPassword;

    return-void
.end method

.method public static a(Lbr/com/itau/sdk/android/core/login/b;Lbr/com/itau/sdk/android/core/login/EletronicPassword;)Lbr/com/itau/sdk/android/core/login/EletronicPassword$Callback;
    .registers 3

    new-instance v0, Lbr/com/itau/sdk/android/core/login/c;

    invoke-direct {v0, p0, p1}, Lbr/com/itau/sdk/android/core/login/c;-><init>(Lbr/com/itau/sdk/android/core/login/b;Lbr/com/itau/sdk/android/core/login/EletronicPassword;)V

    return-object v0
.end method


# virtual methods
.method public confirm(Ljava/lang/String;)V
    .registers 4
    .annotation runtime Ljava/lang/invoke/LambdaForm$Hidden;
    .end annotation

    iget-object v0, p0, Lbr/com/itau/sdk/android/core/login/c;->a:Lbr/com/itau/sdk/android/core/login/b;

    iget-object v1, p0, Lbr/com/itau/sdk/android/core/login/c;->b:Lbr/com/itau/sdk/android/core/login/EletronicPassword;

    invoke-static {v0, v1, p1}, Lbr/com/itau/sdk/android/core/login/b;->a(Lbr/com/itau/sdk/android/core/login/b;Lbr/com/itau/sdk/android/core/login/EletronicPassword;Ljava/lang/String;)V

    return-void
.end method

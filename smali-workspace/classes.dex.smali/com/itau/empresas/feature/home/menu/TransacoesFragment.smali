.class public Lcom/itau/empresas/feature/home/menu/TransacoesFragment;
.super Landroid/support/v4/app/Fragment;
.source "TransacoesFragment.java"

# interfaces
.implements Lcom/itau/empresas/feature/home/menu/adapter/CardMenuListener;


# static fields
.field public static final TAG:Ljava/lang/String;


# instance fields
.field private abridor:Lcom/itau/empresas/feature/home/AbridorDeFragment;

.field menu:Lcom/itau/empresas/api/model/MenuVO;

.field recyclerViewMenu:Landroid/support/v7/widget/RecyclerView;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .line 32
    const-class v0, Lcom/itau/empresas/feature/home/menu/TransacoesFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/itau/empresas/feature/home/menu/TransacoesFragment;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .registers 1

    .line 30
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    return-void
.end method

.method private configuraDataExibida(Landroid/view/View;)V
    .registers 6
    .param p1, "view"    # Landroid/view/View;

    .line 105
    const v0, 0x7f0e0483

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Landroid/widget/TextView;

    .line 106
    .local v3, "data":Landroid/widget/TextView;
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    .line 107
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    const/4 v2, 0x5

    invoke-virtual {v1, v2}, Ljava/util/Calendar;->get(I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    .line 106
    const v1, 0x7f0706c8

    invoke-virtual {p0, v1, v0}, Lcom/itau/empresas/feature/home/menu/TransacoesFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 108
    return-void
.end method

.method private configuraEmptyState(Landroid/view/View;)V
    .registers 6
    .param p1, "view"    # Landroid/view/View;

    .line 116
    invoke-virtual {p0}, Lcom/itau/empresas/feature/home/menu/TransacoesFragment;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 117
    const v1, 0x7f0e0481

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    .line 118
    const v2, 0x7f070473

    invoke-virtual {p0, v2}, Lcom/itau/empresas/feature/home/menu/TransacoesFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 116
    const v3, 0x7f07028b

    invoke-static {v0, v1, v2, v3}, Lcom/itau/empresas/ui/util/EmptyStateUtil;->mostraEmptyState(Landroid/content/Context;Landroid/view/ViewGroup;Ljava/lang/String;I)V

    .line 120
    return-void
.end method

.method private criaRecyclerView(Landroid/view/View;)V
    .registers 4
    .param p1, "view"    # Landroid/view/View;

    .line 111
    const v0, 0x7f0e0477

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    iput-object v0, p0, Lcom/itau/empresas/feature/home/menu/TransacoesFragment;->recyclerViewMenu:Landroid/support/v7/widget/RecyclerView;

    .line 112
    iget-object v0, p0, Lcom/itau/empresas/feature/home/menu/TransacoesFragment;->recyclerViewMenu:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, Lcom/itau/empresas/feature/home/menu/TransacoesFragment;->menu:Lcom/itau/empresas/api/model/MenuVO;

    invoke-static {v0, v1, p0}, Lcom/itau/empresas/feature/home/menu/ConfiguradorRecyclerViewMenu;->configura(Landroid/support/v7/widget/RecyclerView;Lcom/itau/empresas/api/model/MenuVO;Lcom/itau/empresas/feature/home/menu/adapter/CardMenuListener;)V

    .line 113
    return-void
.end method

.method private disparaEventoAnalytics()V
    .registers 5

    .line 127
    invoke-static {}, Lcom/itau/empresas/feature/analytics/AnalyticsHelper;->getInstance()Lcom/itau/empresas/feature/analytics/AnalyticsHelper;

    move-result-object v3

    .line 129
    .local v3, "analyticsHelper":Lcom/itau/empresas/feature/analytics/AnalyticsHelper;
    iget-object v0, p0, Lcom/itau/empresas/feature/home/menu/TransacoesFragment;->menu:Lcom/itau/empresas/api/model/MenuVO;

    if-eqz v0, :cond_14

    iget-object v0, p0, Lcom/itau/empresas/feature/home/menu/TransacoesFragment;->menu:Lcom/itau/empresas/api/model/MenuVO;

    invoke-virtual {v0}, Lcom/itau/empresas/api/model/MenuVO;->getFilhos()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2d

    .line 130
    .line 131
    :cond_14
    const v0, 0x7f0700fc

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/home/menu/TransacoesFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 132
    const v1, 0x7f0700d1

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/home/menu/TransacoesFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 133
    const v2, 0x7f0700c3

    invoke-virtual {p0, v2}, Lcom/itau/empresas/feature/home/menu/TransacoesFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 130
    invoke-virtual {v3, v0, v1, v2}, Lcom/itau/empresas/feature/analytics/AnalyticsHelper;->eventoAdobeState(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 134
    return-void

    .line 137
    .line 138
    :cond_2d
    const v0, 0x7f0700f9

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/home/menu/TransacoesFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 139
    const v1, 0x7f0700d1

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/home/menu/TransacoesFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 140
    const v2, 0x7f0700c3

    invoke-virtual {p0, v2}, Lcom/itau/empresas/feature/home/menu/TransacoesFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 137
    invoke-virtual {v3, v0, v1, v2}, Lcom/itau/empresas/feature/analytics/AnalyticsHelper;->eventoAdobeState(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 141
    return-void
.end method

.method private iniciaLayout(Landroid/view/View;)V
    .registers 3
    .param p1, "view"    # Landroid/view/View;

    .line 96
    iget-object v0, p0, Lcom/itau/empresas/feature/home/menu/TransacoesFragment;->menu:Lcom/itau/empresas/api/model/MenuVO;

    if-eqz v0, :cond_10

    iget-object v0, p0, Lcom/itau/empresas/feature/home/menu/TransacoesFragment;->menu:Lcom/itau/empresas/api/model/MenuVO;

    invoke-virtual {v0}, Lcom/itau/empresas/api/model/MenuVO;->getFilhos()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_14

    .line 97
    :cond_10
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/home/menu/TransacoesFragment;->configuraEmptyState(Landroid/view/View;)V

    goto :goto_1a

    .line 99
    :cond_14
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/home/menu/TransacoesFragment;->criaRecyclerView(Landroid/view/View;)V

    .line 100
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/home/menu/TransacoesFragment;->configuraDataExibida(Landroid/view/View;)V

    .line 102
    :goto_1a
    return-void
.end method

.method public static newInstance(Lcom/itau/empresas/api/model/MenuVO;)Lcom/itau/empresas/feature/home/menu/TransacoesFragment;
    .registers 4
    .param p0, "itemMenu"    # Lcom/itau/empresas/api/model/MenuVO;

    .line 41
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 42
    .local v1, "args":Landroid/os/Bundle;
    const-string v0, "menuItem"

    invoke-virtual {v1, v0, p0}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 44
    new-instance v2, Lcom/itau/empresas/feature/home/menu/TransacoesFragment;

    invoke-direct {v2}, Lcom/itau/empresas/feature/home/menu/TransacoesFragment;-><init>()V

    .line 45
    .local v2, "fragment":Lcom/itau/empresas/feature/home/menu/TransacoesFragment;
    invoke-virtual {v2, v1}, Lcom/itau/empresas/feature/home/menu/TransacoesFragment;->setArguments(Landroid/os/Bundle;)V

    .line 46
    return-object v2
.end method


# virtual methods
.method public menuSelecionado(Lcom/itau/empresas/api/model/MenuVO;Z)V
    .registers 6
    .param p1, "menu"    # Lcom/itau/empresas/api/model/MenuVO;
    .param p2, "temSubMenu"    # Z

    .line 78
    if-nez p1, :cond_3

    .line 79
    return-void

    .line 80
    :cond_3
    if-eqz p2, :cond_11

    .line 81
    invoke-virtual {p0}, Lcom/itau/empresas/feature/home/menu/TransacoesFragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/itau/empresas/feature/home/menu/SubMenuActivity;->intent(Landroid/content/Context;Lcom/itau/empresas/api/model/MenuVO;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/home/menu/TransacoesFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_39

    .line 83
    :cond_11
    invoke-virtual {p1}, Lcom/itau/empresas/api/model/MenuVO;->getChaveMobile()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/itau/empresas/ui/util/menu/InformacaoMenu;->getInstance(Ljava/lang/String;)Lcom/itau/empresas/ui/util/menu/InformacaoMenu;

    move-result-object v0

    .line 84
    invoke-virtual {v0}, Lcom/itau/empresas/ui/util/menu/InformacaoMenu;->getCommand()Lcom/itau/empresas/ui/util/menu/Command;

    move-result-object v2

    .line 85
    .local v2, "command":Lcom/itau/empresas/ui/util/menu/Command;
    if-eqz v2, :cond_39

    .line 86
    invoke-virtual {p0}, Lcom/itau/empresas/feature/home/menu/TransacoesFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Landroid/support/v7/app/AppCompatActivity;

    invoke-interface {v2, v0, p1}, Lcom/itau/empresas/ui/util/menu/Command;->executar(Landroid/support/v7/app/AppCompatActivity;Lcom/itau/empresas/api/model/MenuVO;)V

    .line 88
    .line 89
    invoke-virtual {p0}, Lcom/itau/empresas/feature/home/menu/TransacoesFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/CustomApplication;

    .line 90
    invoke-virtual {p1}, Lcom/itau/empresas/api/model/MenuVO;->getChaveMobile()Ljava/lang/String;

    move-result-object v1

    .line 88
    invoke-static {v0, v1}, Lcom/itau/empresas/ui/util/AtendimentoUtil;->registraContextualizacaoFiltroAtendimento(Lcom/itau/empresas/CustomApplication;Ljava/lang/String;)V

    .line 93
    .end local v2    # "command":Lcom/itau/empresas/ui/util/menu/Command;
    :cond_39
    :goto_39
    return-void
.end method

.method public onAttach(Landroid/content/Context;)V
    .registers 3
    .param p1, "context"    # Landroid/content/Context;

    .line 50
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onAttach(Landroid/content/Context;)V

    .line 51
    iget-object v0, p0, Lcom/itau/empresas/feature/home/menu/TransacoesFragment;->abridor:Lcom/itau/empresas/feature/home/AbridorDeFragment;

    if-eqz v0, :cond_8

    .line 52
    return-void

    .line 53
    :cond_8
    instance-of v0, p1, Lcom/itau/empresas/feature/home/AbridorDeFragment;

    if-eqz v0, :cond_11

    .line 54
    move-object v0, p1

    check-cast v0, Lcom/itau/empresas/feature/home/AbridorDeFragment;

    iput-object v0, p0, Lcom/itau/empresas/feature/home/menu/TransacoesFragment;->abridor:Lcom/itau/empresas/feature/home/AbridorDeFragment;

    .line 55
    :cond_11
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 69
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 71
    invoke-virtual {p0}, Lcom/itau/empresas/feature/home/menu/TransacoesFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_17

    .line 72
    invoke-virtual {p0}, Lcom/itau/empresas/feature/home/menu/TransacoesFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "menuItem"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/api/model/MenuVO;

    iput-object v0, p0, Lcom/itau/empresas/feature/home/menu/TransacoesFragment;->menu:Lcom/itau/empresas/api/model/MenuVO;

    .line 74
    :cond_17
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .registers 7
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .line 61
    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    const v1, 0x7f0300c2

    invoke-virtual {p1, v1, p2, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 62
    .local v2, "view":Landroid/view/View;
    invoke-direct {p0, v2}, Lcom/itau/empresas/feature/home/menu/TransacoesFragment;->iniciaLayout(Landroid/view/View;)V

    .line 63
    invoke-direct {p0}, Lcom/itau/empresas/feature/home/menu/TransacoesFragment;->disparaEventoAnalytics()V

    .line 64
    return-object v2
.end method

.method public setAbridor(Lcom/itau/empresas/feature/home/AbridorDeFragment;)V
    .registers 2
    .param p1, "abridor"    # Lcom/itau/empresas/feature/home/AbridorDeFragment;

    .line 123
    iput-object p1, p0, Lcom/itau/empresas/feature/home/menu/TransacoesFragment;->abridor:Lcom/itau/empresas/feature/home/AbridorDeFragment;

    .line 124
    return-void
.end method

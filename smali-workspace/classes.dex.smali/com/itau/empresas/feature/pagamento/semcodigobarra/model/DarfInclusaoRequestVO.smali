.class public Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;
.super Ljava/lang/Object;
.source "DarfInclusaoRequestVO.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private cnpjContribuinte:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "cnpj_contribuinte"
    .end annotation
.end field

.field private cnpjEmpresa:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "cnpj_empresa"
    .end annotation
.end field

.field private codigoOperador:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "codigo_operador"
    .end annotation
.end field

.field private codigoReceita:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "codigo_receita"
    .end annotation
.end field

.field private cpfContribuinte:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "cpf_contribuinte"
    .end annotation
.end field

.field private dataApuracao:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "data_apuracao"
    .end annotation
.end field

.field private dataPagamento:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "data_pagamento"
    .end annotation
.end field

.field private dataVencimento:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "data_vencimento"
    .end annotation
.end field

.field private duplicidade:Z
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "duplicidade"
    .end annotation
.end field

.field private idConta:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "id_conta"
    .end annotation
.end field

.field private informativoPagador:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "informativo_pagador"
    .end annotation
.end field

.field private nomeContribuinte:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "nome_contribuinte"
    .end annotation
.end field

.field private nomeEmpresa:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "nome_empresa"
    .end annotation
.end field

.field private numeroReferencia:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "numero_referencia"
    .end annotation
.end field

.field private percentual:Ljava/math/BigDecimal;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "percentual"
    .end annotation
.end field

.field private referenciaEmpresa:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "referencia_empresa"
    .end annotation
.end field

.field private simples:Z
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "simples"
    .end annotation
.end field

.field private tipoUsuario:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "tipo_usuario"
    .end annotation
.end field

.field private valorJuros:Ljava/math/BigDecimal;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "valor_juros"
    .end annotation
.end field

.field private valorMultas:Ljava/math/BigDecimal;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "valor_multas"
    .end annotation
.end field

.field private valorPrincipal:Ljava/math/BigDecimal;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "valor_principal"
    .end annotation
.end field

.field private valorReceita:Ljava/math/BigDecimal;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "valor_receita"
    .end annotation
.end field

.field private valorTotal:Ljava/math/BigDecimal;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "valor_total"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private setaCpfCnpj(Ljava/lang/String;)V
    .registers 4
    .param p1, "valor"    # Ljava/lang/String;

    .line 150
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v1, 0xb

    if-ne v0, v1, :cond_f

    .line 151
    iput-object p1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;->cpfContribuinte:Ljava/lang/String;

    .line 152
    const-string v0, ""

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;->cnpjContribuinte:Ljava/lang/String;

    goto :goto_15

    .line 154
    :cond_f
    const-string v0, ""

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;->cpfContribuinte:Ljava/lang/String;

    .line 155
    iput-object p1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;->cnpjContribuinte:Ljava/lang/String;

    .line 157
    :goto_15
    return-void
.end method


# virtual methods
.method public getCnpjContribuinte()Ljava/lang/String;
    .registers 3

    .line 138
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;->cnpjContribuinte:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_11

    sget-object v0, Lbr/com/concretesolutions/canarinho/formatador/FormatadorCPFCNPJ;->CPF_CNPJ:Lbr/com/concretesolutions/canarinho/formatador/Formatador;

    iget-object v1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;->cnpjContribuinte:Ljava/lang/String;

    .line 139
    invoke-interface {v0, v1}, Lbr/com/concretesolutions/canarinho/formatador/Formatador;->desformata(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_13

    :cond_11
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;->cnpjContribuinte:Ljava/lang/String;

    .line 138
    :goto_13
    return-object v0
.end method

.method public getCnpjEmpresa()Ljava/lang/String;
    .registers 3

    .line 100
    sget-object v0, Lbr/com/concretesolutions/canarinho/formatador/Formatador;->CNPJ:Lbr/com/concretesolutions/canarinho/formatador/Formatador;

    iget-object v1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;->cnpjEmpresa:Ljava/lang/String;

    invoke-interface {v0, v1}, Lbr/com/concretesolutions/canarinho/formatador/Formatador;->desformata(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getCodigoReceita()Ljava/lang/String;
    .registers 2

    .line 187
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;->codigoReceita:Ljava/lang/String;

    return-object v0
.end method

.method public getCpfContribuinte()Ljava/lang/String;
    .registers 2

    .line 160
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;->cpfContribuinte:Ljava/lang/String;

    return-object v0
.end method

.method public getDataApuracao()Ljava/lang/String;
    .registers 2

    .line 173
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;->dataApuracao:Ljava/lang/String;

    return-object v0
.end method

.method public getDataPagamento()Ljava/lang/String;
    .registers 2

    .line 217
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;->dataPagamento:Ljava/lang/String;

    return-object v0
.end method

.method public getInformativoPagador()Ljava/lang/String;
    .registers 2

    .line 68
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;->informativoPagador:Ljava/lang/String;

    return-object v0
.end method

.method public getNomeContribuinte()Ljava/lang/String;
    .registers 2

    .line 130
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;->nomeContribuinte:Ljava/lang/String;

    return-object v0
.end method

.method public getNomeEmpresa()Ljava/lang/String;
    .registers 2

    .line 114
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;->nomeEmpresa:Ljava/lang/String;

    return-object v0
.end method

.method public getNumeroReferencia()Ljava/lang/String;
    .registers 2

    .line 195
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;->numeroReferencia:Ljava/lang/String;

    return-object v0
.end method

.method public getReferenciaEmpresa()Ljava/lang/String;
    .registers 2

    .line 76
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;->referenciaEmpresa:Ljava/lang/String;

    return-object v0
.end method

.method public getValorJuros()Ljava/math/BigDecimal;
    .registers 2

    .line 247
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;->valorJuros:Ljava/math/BigDecimal;

    return-object v0
.end method

.method public getValorMultas()Ljava/math/BigDecimal;
    .registers 2

    .line 239
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;->valorMultas:Ljava/math/BigDecimal;

    return-object v0
.end method

.method public getValorPrincipal()Ljava/math/BigDecimal;
    .registers 2

    .line 231
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;->valorPrincipal:Ljava/math/BigDecimal;

    return-object v0
.end method

.method public getValorTotal()Ljava/math/BigDecimal;
    .registers 2

    .line 255
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;->valorTotal:Ljava/math/BigDecimal;

    return-object v0
.end method

.method public setCnpjContribuinte(Ljava/lang/String;)V
    .registers 3
    .param p1, "cnpjContribuinte"    # Ljava/lang/String;

    .line 143
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_17

    sget-object v0, Lbr/com/concretesolutions/canarinho/formatador/FormatadorCPFCNPJ;->CPF_CNPJ:Lbr/com/concretesolutions/canarinho/formatador/Formatador;

    .line 144
    invoke-interface {v0, p1}, Lbr/com/concretesolutions/canarinho/formatador/Formatador;->estaFormatado(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_17

    .line 145
    sget-object v0, Lbr/com/concretesolutions/canarinho/formatador/FormatadorCPFCNPJ;->CPF_CNPJ:Lbr/com/concretesolutions/canarinho/formatador/Formatador;

    invoke-interface {v0, p1}, Lbr/com/concretesolutions/canarinho/formatador/Formatador;->desformata(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;->setaCpfCnpj(Ljava/lang/String;)V

    .line 147
    :cond_17
    return-void
.end method

.method public setCnpjEmpresa(Ljava/lang/String;)V
    .registers 3
    .param p1, "cnpjEmpresa"    # Ljava/lang/String;

    .line 105
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_17

    sget-object v0, Lbr/com/concretesolutions/canarinho/formatador/FormatadorCPFCNPJ;->CPF_CNPJ:Lbr/com/concretesolutions/canarinho/formatador/Formatador;

    .line 106
    invoke-interface {v0, p1}, Lbr/com/concretesolutions/canarinho/formatador/Formatador;->estaFormatado(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_17

    .line 107
    sget-object v0, Lbr/com/concretesolutions/canarinho/formatador/FormatadorCPFCNPJ;->CPF_CNPJ:Lbr/com/concretesolutions/canarinho/formatador/Formatador;

    invoke-interface {v0, p1}, Lbr/com/concretesolutions/canarinho/formatador/Formatador;->desformata(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;->cnpjEmpresa:Ljava/lang/String;

    goto :goto_19

    .line 109
    :cond_17
    iput-object p1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;->cnpjEmpresa:Ljava/lang/String;

    .line 111
    :goto_19
    return-void
.end method

.method public setCodigoOperador(Ljava/lang/String;)V
    .registers 2
    .param p1, "codigoOperador"    # Ljava/lang/String;

    .line 88
    iput-object p1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;->codigoOperador:Ljava/lang/String;

    .line 89
    return-void
.end method

.method public setCodigoReceita(Ljava/lang/String;)V
    .registers 2
    .param p1, "codigoReceita"    # Ljava/lang/String;

    .line 191
    iput-object p1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;->codigoReceita:Ljava/lang/String;

    .line 192
    return-void
.end method

.method public setDataApuracao(Ljava/lang/String;)V
    .registers 4
    .param p1, "dataApuracao"    # Ljava/lang/String;

    .line 178
    :try_start_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_c

    .line 179
    invoke-static {p1}, Lcom/itau/empresas/ui/util/formatacao/DateUtils;->paraApi(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;->dataApuracao:Ljava/lang/String;
    :try_end_c
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_c} :catch_d

    .line 183
    :cond_c
    goto :goto_10

    .line 181
    :catch_d
    move-exception v1

    .line 182
    .local v1, "e":Ljava/text/ParseException;
    iput-object p1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;->dataApuracao:Ljava/lang/String;

    .line 184
    .end local v1    # "e":Ljava/text/ParseException;
    :goto_10
    return-void
.end method

.method public setDataPagamento(Ljava/lang/String;)V
    .registers 5
    .param p1, "dataPagamento"    # Ljava/lang/String;

    .line 222
    :try_start_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_c

    .line 223
    invoke-static {p1}, Lcom/itau/empresas/ui/util/formatacao/DateUtils;->paraApi(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;->dataPagamento:Ljava/lang/String;
    :try_end_c
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_c} :catch_d

    .line 227
    :cond_c
    goto :goto_17

    .line 225
    :catch_d
    move-exception v2

    .line 226
    .local v2, "e":Ljava/text/ParseException;
    const-string v0, "Erro"

    invoke-virtual {v2}, Ljava/text/ParseException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 228
    .end local v2    # "e":Ljava/text/ParseException;
    :goto_17
    return-void
.end method

.method public setDataVencimento(Ljava/lang/String;)V
    .registers 4
    .param p1, "dataVencimento"    # Ljava/lang/String;

    .line 208
    :try_start_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_c

    .line 209
    invoke-static {p1}, Lcom/itau/empresas/ui/util/formatacao/DateUtils;->paraApi(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;->dataVencimento:Ljava/lang/String;
    :try_end_c
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_c} :catch_d

    .line 213
    :cond_c
    goto :goto_10

    .line 211
    :catch_d
    move-exception v1

    .line 212
    .local v1, "e":Ljava/text/ParseException;
    iput-object p1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;->dataVencimento:Ljava/lang/String;

    .line 214
    .end local v1    # "e":Ljava/text/ParseException;
    :goto_10
    return-void
.end method

.method public setDuplicidade(Z)V
    .registers 2
    .param p1, "duplicidade"    # Z

    .line 126
    iput-boolean p1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;->duplicidade:Z

    .line 127
    return-void
.end method

.method public setIdConta(Ljava/lang/String;)V
    .registers 2
    .param p1, "idConta"    # Ljava/lang/String;

    .line 96
    iput-object p1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;->idConta:Ljava/lang/String;

    .line 97
    return-void
.end method

.method public setInformativoPagador(Ljava/lang/String;)V
    .registers 2
    .param p1, "informativoPagador"    # Ljava/lang/String;

    .line 72
    iput-object p1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;->informativoPagador:Ljava/lang/String;

    .line 73
    return-void
.end method

.method public setNomeContribuinte(Ljava/lang/String;)V
    .registers 2
    .param p1, "nomeContribuinte"    # Ljava/lang/String;

    .line 134
    iput-object p1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;->nomeContribuinte:Ljava/lang/String;

    .line 135
    return-void
.end method

.method public setNomeEmpresa(Ljava/lang/String;)V
    .registers 2
    .param p1, "nomeEmpresa"    # Ljava/lang/String;

    .line 118
    iput-object p1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;->nomeEmpresa:Ljava/lang/String;

    .line 119
    return-void
.end method

.method public setNumeroReferencia(Ljava/lang/String;)V
    .registers 2
    .param p1, "numeroReferencia"    # Ljava/lang/String;

    .line 199
    iput-object p1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;->numeroReferencia:Ljava/lang/String;

    .line 200
    return-void
.end method

.method public setPercentual(Ljava/math/BigDecimal;)V
    .registers 2
    .param p1, "percentual"    # Ljava/math/BigDecimal;

    .line 291
    iput-object p1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;->percentual:Ljava/math/BigDecimal;

    .line 292
    return-void
.end method

.method public setReferenciaEmpresa(Ljava/lang/String;)V
    .registers 2
    .param p1, "referenciaEmpresa"    # Ljava/lang/String;

    .line 80
    iput-object p1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;->referenciaEmpresa:Ljava/lang/String;

    .line 81
    return-void
.end method

.method public setSimples(Z)V
    .registers 2
    .param p1, "simples"    # Z

    .line 267
    iput-boolean p1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;->simples:Z

    .line 268
    return-void
.end method

.method public setTipoUsuario(Ljava/lang/String;)V
    .registers 2
    .param p1, "tipoUsuario"    # Ljava/lang/String;

    .line 275
    iput-object p1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;->tipoUsuario:Ljava/lang/String;

    .line 276
    return-void
.end method

.method public setValorJuros(Ljava/math/BigDecimal;)V
    .registers 2
    .param p1, "valorJuros"    # Ljava/math/BigDecimal;

    .line 251
    iput-object p1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;->valorJuros:Ljava/math/BigDecimal;

    .line 252
    return-void
.end method

.method public setValorMultas(Ljava/math/BigDecimal;)V
    .registers 2
    .param p1, "valorMultas"    # Ljava/math/BigDecimal;

    .line 243
    iput-object p1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;->valorMultas:Ljava/math/BigDecimal;

    .line 244
    return-void
.end method

.method public setValorPrincipal(Ljava/math/BigDecimal;)V
    .registers 2
    .param p1, "valorPrincipal"    # Ljava/math/BigDecimal;

    .line 235
    iput-object p1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;->valorPrincipal:Ljava/math/BigDecimal;

    .line 236
    return-void
.end method

.method public setValorReceita(Ljava/math/BigDecimal;)V
    .registers 2
    .param p1, "valorReceita"    # Ljava/math/BigDecimal;

    .line 283
    iput-object p1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;->valorReceita:Ljava/math/BigDecimal;

    .line 284
    return-void
.end method

.method public setValorTotal(Ljava/math/BigDecimal;)V
    .registers 2
    .param p1, "valorTotal"    # Ljava/math/BigDecimal;

    .line 259
    iput-object p1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;->valorTotal:Ljava/math/BigDecimal;

    .line 260
    return-void
.end method

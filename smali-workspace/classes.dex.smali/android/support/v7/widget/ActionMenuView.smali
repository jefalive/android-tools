.class public Landroid/support/v7/widget/ActionMenuView;
.super Landroid/support/v7/widget/LinearLayoutCompat;
.source "ActionMenuView.java"

# interfaces
.implements Landroid/support/v7/view/menu/MenuBuilder$ItemInvoker;
.implements Landroid/support/v7/view/menu/MenuView;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/support/v7/widget/ActionMenuView$LayoutParams;,
        Landroid/support/v7/widget/ActionMenuView$ActionMenuChildView;,
        Landroid/support/v7/widget/ActionMenuView$ActionMenuPresenterCallback;,
        Landroid/support/v7/widget/ActionMenuView$MenuBuilderCallback;,
        Landroid/support/v7/widget/ActionMenuView$OnMenuItemClickListener;
    }
.end annotation


# instance fields
.field private mActionMenuPresenterCallback:Landroid/support/v7/view/menu/MenuPresenter$Callback;

.field private mFormatItems:Z

.field private mFormatItemsWidth:I

.field private mGeneratedItemPadding:I

.field private mMenu:Landroid/support/v7/view/menu/MenuBuilder;

.field mMenuBuilderCallback:Landroid/support/v7/view/menu/MenuBuilder$Callback;

.field private mMinCellSize:I

.field mOnMenuItemClickListener:Landroid/support/v7/widget/ActionMenuView$OnMenuItemClickListener;

.field private mPopupContext:Landroid/content/Context;

.field private mPopupTheme:I

.field private mPresenter:Landroid/support/v7/widget/ActionMenuPresenter;

.field private mReserveOverflow:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .param p1, "context"    # Landroid/content/Context;

    .line 76
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/support/v7/widget/ActionMenuView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 77
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .line 80
    invoke-direct {p0, p1, p2}, Landroid/support/v7/widget/LinearLayoutCompat;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 81
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/ActionMenuView;->setBaselineAligned(Z)V

    .line 82
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v1, v0, Landroid/util/DisplayMetrics;->density:F

    .line 83
    .local v1, "density":F
    const/high16 v0, 0x42600000    # 56.0f

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, Landroid/support/v7/widget/ActionMenuView;->mMinCellSize:I

    .line 84
    const/high16 v0, 0x40800000    # 4.0f

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, Landroid/support/v7/widget/ActionMenuView;->mGeneratedItemPadding:I

    .line 85
    iput-object p1, p0, Landroid/support/v7/widget/ActionMenuView;->mPopupContext:Landroid/content/Context;

    .line 86
    const/4 v0, 0x0

    iput v0, p0, Landroid/support/v7/widget/ActionMenuView;->mPopupTheme:I

    .line 87
    return-void
.end method

.method static measureChildForCells(Landroid/view/View;IIII)I
    .registers 16
    .param p0, "child"    # Landroid/view/View;
    .param p1, "cellSize"    # I
    .param p2, "cellsRemaining"    # I
    .param p3, "parentHeightMeasureSpec"    # I
    .param p4, "parentHeightPadding"    # I

    .line 404
    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Landroid/support/v7/widget/ActionMenuView$LayoutParams;

    .line 406
    .local v2, "lp":Landroid/support/v7/widget/ActionMenuView$LayoutParams;
    invoke-static {p3}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    sub-int v3, v0, p4

    .line 408
    .local v3, "childHeightSize":I
    invoke-static {p3}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v4

    .line 409
    .local v4, "childHeightMode":I
    invoke-static {v3, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    .line 411
    .local v5, "childHeightSpec":I
    instance-of v0, p0, Landroid/support/v7/view/menu/ActionMenuItemView;

    if-eqz v0, :cond_1d

    move-object v6, p0

    check-cast v6, Landroid/support/v7/view/menu/ActionMenuItemView;

    goto :goto_1e

    :cond_1d
    const/4 v6, 0x0

    .line 413
    .local v6, "itemView":Landroid/support/v7/view/menu/ActionMenuItemView;
    :goto_1e
    if-eqz v6, :cond_28

    invoke-virtual {v6}, Landroid/support/v7/view/menu/ActionMenuItemView;->hasText()Z

    move-result v0

    if-eqz v0, :cond_28

    const/4 v7, 0x1

    goto :goto_29

    :cond_28
    const/4 v7, 0x0

    .line 415
    .local v7, "hasText":Z
    :goto_29
    const/4 v8, 0x0

    .line 416
    .local v8, "cellsUsed":I
    if-lez p2, :cond_4e

    if-eqz v7, :cond_31

    const/4 v0, 0x2

    if-lt p2, v0, :cond_4e

    .line 417
    :cond_31
    mul-int v0, p1, p2

    const/high16 v1, -0x80000000

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v9

    .line 419
    .local v9, "childWidthSpec":I
    invoke-virtual {p0, v9, v5}, Landroid/view/View;->measure(II)V

    .line 421
    invoke-virtual {p0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v10

    .line 422
    .local v10, "measuredWidth":I
    div-int v8, v10, p1

    .line 423
    rem-int v0, v10, p1

    if-eqz v0, :cond_48

    add-int/lit8 v8, v8, 0x1

    .line 424
    :cond_48
    if-eqz v7, :cond_4e

    const/4 v0, 0x2

    if-ge v8, v0, :cond_4e

    const/4 v8, 0x2

    .line 427
    .end local v9    # "childWidthSpec":I
    .end local v10    # "measuredWidth":I
    :cond_4e
    iget-boolean v0, v2, Landroid/support/v7/widget/ActionMenuView$LayoutParams;->isOverflowButton:Z

    if-nez v0, :cond_56

    if-eqz v7, :cond_56

    const/4 v9, 0x1

    goto :goto_57

    :cond_56
    const/4 v9, 0x0

    .line 428
    .local v9, "expandable":Z
    :goto_57
    iput-boolean v9, v2, Landroid/support/v7/widget/ActionMenuView$LayoutParams;->expandable:Z

    .line 430
    iput v8, v2, Landroid/support/v7/widget/ActionMenuView$LayoutParams;->cellsUsed:I

    .line 431
    mul-int v10, v8, p1

    .line 432
    .local v10, "targetWidth":I
    const/high16 v0, 0x40000000    # 2.0f

    invoke-static {v10, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-virtual {p0, v0, v5}, Landroid/view/View;->measure(II)V

    .line 434
    return v8
.end method

.method private onMeasureExactFormat(II)V
    .registers 35
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .line 178
    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v5

    .line 179
    .local v5, "heightMode":I
    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v6

    .line 180
    .local v6, "widthSize":I
    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v7

    .line 182
    .local v7, "heightSize":I
    invoke-virtual/range {p0 .. p0}, Landroid/support/v7/widget/ActionMenuView;->getPaddingLeft()I

    move-result v0

    invoke-virtual/range {p0 .. p0}, Landroid/support/v7/widget/ActionMenuView;->getPaddingRight()I

    move-result v1

    add-int v8, v0, v1

    .line 183
    .local v8, "widthPadding":I
    invoke-virtual/range {p0 .. p0}, Landroid/support/v7/widget/ActionMenuView;->getPaddingTop()I

    move-result v0

    invoke-virtual/range {p0 .. p0}, Landroid/support/v7/widget/ActionMenuView;->getPaddingBottom()I

    move-result v1

    add-int v9, v0, v1

    .line 185
    .local v9, "heightPadding":I
    move/from16 v0, p2

    const/4 v1, -0x2

    invoke-static {v0, v9, v1}, Landroid/support/v7/widget/ActionMenuView;->getChildMeasureSpec(III)I

    move-result v10

    .line 188
    .local v10, "itemHeightSpec":I
    sub-int/2addr v6, v8

    .line 191
    move-object/from16 v0, p0

    iget v0, v0, Landroid/support/v7/widget/ActionMenuView;->mMinCellSize:I

    div-int v11, v6, v0

    .line 192
    .local v11, "cellCount":I
    move-object/from16 v0, p0

    iget v0, v0, Landroid/support/v7/widget/ActionMenuView;->mMinCellSize:I

    rem-int v12, v6, v0

    .line 194
    .local v12, "cellSizeRemaining":I
    if-nez v11, :cond_3d

    .line 196
    move-object/from16 v0, p0

    const/4 v1, 0x0

    invoke-virtual {v0, v6, v1}, Landroid/support/v7/widget/ActionMenuView;->setMeasuredDimension(II)V

    .line 197
    return-void

    .line 200
    :cond_3d
    move-object/from16 v0, p0

    iget v0, v0, Landroid/support/v7/widget/ActionMenuView;->mMinCellSize:I

    div-int v1, v12, v11

    add-int v13, v0, v1

    .line 202
    .local v13, "cellSize":I
    move v14, v11

    .line 203
    .local v14, "cellsRemaining":I
    const/4 v15, 0x0

    .line 204
    .local v15, "maxChildHeight":I
    const/16 v16, 0x0

    .line 205
    .local v16, "maxCellsUsed":I
    const/16 v17, 0x0

    .line 206
    .local v17, "expandableItemCount":I
    const/16 v18, 0x0

    .line 207
    .local v18, "visibleItemCount":I
    const/16 v19, 0x0

    .line 210
    .local v19, "hasOverflow":Z
    const-wide/16 v20, 0x0

    .line 212
    .local v20, "smallestItemsAt":J
    invoke-virtual/range {p0 .. p0}, Landroid/support/v7/widget/ActionMenuView;->getChildCount()I

    move-result v22

    .line 213
    .local v22, "childCount":I
    const/16 v23, 0x0

    .local v23, "i":I
    :goto_57
    move/from16 v0, v23

    move/from16 v1, v22

    if-ge v0, v1, :cond_105

    .line 214
    move-object/from16 v0, p0

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/ActionMenuView;->getChildAt(I)Landroid/view/View;

    move-result-object v24

    .line 215
    .local v24, "child":Landroid/view/View;
    invoke-virtual/range {v24 .. v24}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_6f

    goto/16 :goto_101

    .line 217
    :cond_6f
    move-object/from16 v0, v24

    instance-of v0, v0, Landroid/support/v7/view/menu/ActionMenuItemView;

    move/from16 v25, v0

    .line 218
    .local v25, "isGeneratedItem":Z
    add-int/lit8 v18, v18, 0x1

    .line 220
    if-eqz v25, :cond_88

    .line 223
    move-object/from16 v0, p0

    iget v0, v0, Landroid/support/v7/widget/ActionMenuView;->mGeneratedItemPadding:I

    move-object/from16 v1, p0

    iget v1, v1, Landroid/support/v7/widget/ActionMenuView;->mGeneratedItemPadding:I

    move-object/from16 v2, v24

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v2, v0, v3, v1, v4}, Landroid/view/View;->setPadding(IIII)V

    .line 226
    :cond_88
    invoke-virtual/range {v24 .. v24}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    move-object/from16 v26, v0

    check-cast v26, Landroid/support/v7/widget/ActionMenuView$LayoutParams;

    .line 227
    .local v26, "lp":Landroid/support/v7/widget/ActionMenuView$LayoutParams;
    const/4 v0, 0x0

    move-object/from16 v1, v26

    iput-boolean v0, v1, Landroid/support/v7/widget/ActionMenuView$LayoutParams;->expanded:Z

    .line 228
    const/4 v0, 0x0

    move-object/from16 v1, v26

    iput v0, v1, Landroid/support/v7/widget/ActionMenuView$LayoutParams;->extraPixels:I

    .line 229
    const/4 v0, 0x0

    move-object/from16 v1, v26

    iput v0, v1, Landroid/support/v7/widget/ActionMenuView$LayoutParams;->cellsUsed:I

    .line 230
    const/4 v0, 0x0

    move-object/from16 v1, v26

    iput-boolean v0, v1, Landroid/support/v7/widget/ActionMenuView$LayoutParams;->expandable:Z

    .line 231
    const/4 v0, 0x0

    move-object/from16 v1, v26

    iput v0, v1, Landroid/support/v7/widget/ActionMenuView$LayoutParams;->leftMargin:I

    .line 232
    const/4 v0, 0x0

    move-object/from16 v1, v26

    iput v0, v1, Landroid/support/v7/widget/ActionMenuView$LayoutParams;->rightMargin:I

    .line 233
    if-eqz v25, :cond_bc

    move-object/from16 v0, v24

    check-cast v0, Landroid/support/v7/view/menu/ActionMenuItemView;

    invoke-virtual {v0}, Landroid/support/v7/view/menu/ActionMenuItemView;->hasText()Z

    move-result v0

    if-eqz v0, :cond_bc

    const/4 v0, 0x1

    goto :goto_bd

    :cond_bc
    const/4 v0, 0x0

    :goto_bd
    move-object/from16 v1, v26

    iput-boolean v0, v1, Landroid/support/v7/widget/ActionMenuView$LayoutParams;->preventEdgeOffset:Z

    .line 236
    move-object/from16 v0, v26

    iget-boolean v0, v0, Landroid/support/v7/widget/ActionMenuView$LayoutParams;->isOverflowButton:Z

    if-eqz v0, :cond_ca

    const/16 v27, 0x1

    goto :goto_cc

    :cond_ca
    move/from16 v27, v14

    .line 238
    .local v27, "cellsAvailable":I
    :goto_cc
    move-object/from16 v0, v24

    move/from16 v1, v27

    invoke-static {v0, v13, v1, v10, v9}, Landroid/support/v7/widget/ActionMenuView;->measureChildForCells(Landroid/view/View;IIII)I

    move-result v28

    .line 241
    .local v28, "cellsUsed":I
    move/from16 v0, v16

    move/from16 v1, v28

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v16

    .line 242
    move-object/from16 v0, v26

    iget-boolean v0, v0, Landroid/support/v7/widget/ActionMenuView$LayoutParams;->expandable:Z

    if-eqz v0, :cond_e4

    add-int/lit8 v17, v17, 0x1

    .line 243
    :cond_e4
    move-object/from16 v0, v26

    iget-boolean v0, v0, Landroid/support/v7/widget/ActionMenuView$LayoutParams;->isOverflowButton:Z

    if-eqz v0, :cond_ec

    const/16 v19, 0x1

    .line 245
    :cond_ec
    sub-int v14, v14, v28

    .line 246
    invoke-virtual/range {v24 .. v24}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    invoke-static {v15, v0}, Ljava/lang/Math;->max(II)I

    move-result v15

    .line 247
    move/from16 v0, v28

    const/4 v1, 0x1

    if-ne v0, v1, :cond_101

    const/4 v0, 0x1

    shl-int v0, v0, v23

    int-to-long v0, v0

    or-long v20, v20, v0

    .line 213
    .end local v24    # "child":Landroid/view/View;
    .end local v25    # "isGeneratedItem":Z
    .end local v26    # "lp":Landroid/support/v7/widget/ActionMenuView$LayoutParams;
    .end local v27    # "cellsAvailable":I
    .end local v28    # "cellsUsed":I
    :cond_101
    :goto_101
    add-int/lit8 v23, v23, 0x1

    goto/16 :goto_57

    .line 252
    .end local v23    # "i":I
    :cond_105
    if-eqz v19, :cond_10f

    move/from16 v0, v18

    const/4 v1, 0x2

    if-ne v0, v1, :cond_10f

    const/16 v23, 0x1

    goto :goto_111

    :cond_10f
    const/16 v23, 0x0

    .line 257
    .local v23, "centerSingleExpandedItem":Z
    :goto_111
    const/16 v24, 0x0

    .line 258
    .local v24, "needsExpansion":Z
    :goto_113
    if-lez v17, :cond_1d7

    if-lez v14, :cond_1d7

    .line 259
    const v25, 0x7fffffff

    .line 260
    .local v25, "minCells":I
    const-wide/16 v26, 0x0

    .line 261
    .local v26, "minCellsAt":J
    const/16 v28, 0x0

    .line 262
    .local v28, "minCellsItemCount":I
    const/16 v29, 0x0

    .local v29, "i":I
    :goto_120
    move/from16 v0, v29

    move/from16 v1, v22

    if-ge v0, v1, :cond_167

    .line 263
    move-object/from16 v0, p0

    move/from16 v1, v29

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/ActionMenuView;->getChildAt(I)Landroid/view/View;

    move-result-object v30

    .line 264
    .local v30, "child":Landroid/view/View;
    invoke-virtual/range {v30 .. v30}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    move-object/from16 v31, v0

    check-cast v31, Landroid/support/v7/widget/ActionMenuView$LayoutParams;

    .line 267
    .local v31, "lp":Landroid/support/v7/widget/ActionMenuView$LayoutParams;
    move-object/from16 v0, v31

    iget-boolean v0, v0, Landroid/support/v7/widget/ActionMenuView$LayoutParams;->expandable:Z

    if-nez v0, :cond_13d

    goto :goto_164

    .line 270
    :cond_13d
    move-object/from16 v0, v31

    iget v0, v0, Landroid/support/v7/widget/ActionMenuView$LayoutParams;->cellsUsed:I

    move/from16 v1, v25

    if-ge v0, v1, :cond_154

    .line 271
    move-object/from16 v0, v31

    iget v0, v0, Landroid/support/v7/widget/ActionMenuView$LayoutParams;->cellsUsed:I

    move/from16 v25, v0

    .line 272
    const/4 v0, 0x1

    shl-int v0, v0, v29

    int-to-long v1, v0

    move-wide/from16 v26, v1

    .line 273
    const/16 v28, 0x1

    goto :goto_164

    .line 274
    :cond_154
    move-object/from16 v0, v31

    iget v0, v0, Landroid/support/v7/widget/ActionMenuView$LayoutParams;->cellsUsed:I

    move/from16 v1, v25

    if-ne v0, v1, :cond_164

    .line 275
    const/4 v0, 0x1

    shl-int v0, v0, v29

    int-to-long v0, v0

    or-long v26, v26, v0

    .line 276
    add-int/lit8 v28, v28, 0x1

    .line 262
    .end local v30    # "child":Landroid/view/View;
    .end local v31    # "lp":Landroid/support/v7/widget/ActionMenuView$LayoutParams;
    :cond_164
    :goto_164
    add-int/lit8 v29, v29, 0x1

    goto :goto_120

    .line 281
    .end local v29    # "i":I
    :cond_167
    or-long v20, v20, v26

    .line 283
    move/from16 v0, v28

    if-le v0, v14, :cond_16f

    goto/16 :goto_1d7

    .line 286
    :cond_16f
    add-int/lit8 v25, v25, 0x1

    .line 288
    const/16 v29, 0x0

    .local v29, "i":I
    :goto_173
    move/from16 v0, v29

    move/from16 v1, v22

    if-ge v0, v1, :cond_1d3

    .line 289
    move-object/from16 v0, p0

    move/from16 v1, v29

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/ActionMenuView;->getChildAt(I)Landroid/view/View;

    move-result-object v30

    .line 290
    .local v30, "child":Landroid/view/View;
    invoke-virtual/range {v30 .. v30}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    move-object/from16 v31, v0

    check-cast v31, Landroid/support/v7/widget/ActionMenuView$LayoutParams;

    .line 291
    .local v31, "lp":Landroid/support/v7/widget/ActionMenuView$LayoutParams;
    const/4 v0, 0x1

    shl-int v0, v0, v29

    int-to-long v0, v0

    and-long v0, v0, v26

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_1a4

    .line 293
    move-object/from16 v0, v31

    iget v0, v0, Landroid/support/v7/widget/ActionMenuView$LayoutParams;->cellsUsed:I

    move/from16 v1, v25

    if-ne v0, v1, :cond_1d0

    const/4 v0, 0x1

    shl-int v0, v0, v29

    int-to-long v0, v0

    or-long v20, v20, v0

    goto :goto_1d0

    .line 297
    :cond_1a4
    if-eqz v23, :cond_1bf

    move-object/from16 v0, v31

    iget-boolean v0, v0, Landroid/support/v7/widget/ActionMenuView$LayoutParams;->preventEdgeOffset:Z

    if-eqz v0, :cond_1bf

    const/4 v0, 0x1

    if-ne v14, v0, :cond_1bf

    .line 299
    move-object/from16 v0, p0

    iget v0, v0, Landroid/support/v7/widget/ActionMenuView;->mGeneratedItemPadding:I

    add-int/2addr v0, v13

    move-object/from16 v1, p0

    iget v1, v1, Landroid/support/v7/widget/ActionMenuView;->mGeneratedItemPadding:I

    move-object/from16 v2, v30

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v2, v0, v3, v1, v4}, Landroid/view/View;->setPadding(IIII)V

    .line 301
    :cond_1bf
    move-object/from16 v0, v31

    iget v0, v0, Landroid/support/v7/widget/ActionMenuView$LayoutParams;->cellsUsed:I

    add-int/lit8 v0, v0, 0x1

    move-object/from16 v1, v31

    iput v0, v1, Landroid/support/v7/widget/ActionMenuView$LayoutParams;->cellsUsed:I

    .line 302
    const/4 v0, 0x1

    move-object/from16 v1, v31

    iput-boolean v0, v1, Landroid/support/v7/widget/ActionMenuView$LayoutParams;->expanded:Z

    .line 303
    add-int/lit8 v14, v14, -0x1

    .line 288
    .end local v30    # "child":Landroid/view/View;
    .end local v31    # "lp":Landroid/support/v7/widget/ActionMenuView$LayoutParams;
    :cond_1d0
    :goto_1d0
    add-int/lit8 v29, v29, 0x1

    goto :goto_173

    .line 306
    .end local v29    # "i":I
    :cond_1d3
    const/16 v24, 0x1

    .line 307
    .end local v25    # "minCells":I
    .end local v26    # "minCellsAt":J
    .end local v28    # "minCellsItemCount":I
    goto/16 :goto_113

    .line 312
    :cond_1d7
    :goto_1d7
    if-nez v19, :cond_1e1

    move/from16 v0, v18

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1e1

    const/16 v25, 0x1

    goto :goto_1e3

    :cond_1e1
    const/16 v25, 0x0

    .line 313
    .local v25, "singleItem":Z
    :goto_1e3
    if-lez v14, :cond_2db

    const-wide/16 v0, 0x0

    cmp-long v0, v20, v0

    if-eqz v0, :cond_2db

    add-int/lit8 v0, v18, -0x1

    if-lt v14, v0, :cond_1f6

    if-nez v25, :cond_1f6

    move/from16 v0, v16

    const/4 v1, 0x1

    if-le v0, v1, :cond_2db

    .line 315
    :cond_1f6
    invoke-static/range {v20 .. v21}, Ljava/lang/Long;->bitCount(J)I

    move-result v0

    int-to-float v1, v0

    move/from16 v26, v1

    .line 317
    .local v26, "expandCount":F
    if-nez v25, :cond_24a

    .line 319
    const-wide/16 v0, 0x1

    and-long v0, v0, v20

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_222

    .line 320
    move-object/from16 v0, p0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/ActionMenuView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    move-object/from16 v27, v0

    check-cast v27, Landroid/support/v7/widget/ActionMenuView$LayoutParams;

    .line 321
    .local v27, "lp":Landroid/support/v7/widget/ActionMenuView$LayoutParams;
    move-object/from16 v0, v27

    iget-boolean v0, v0, Landroid/support/v7/widget/ActionMenuView$LayoutParams;->preventEdgeOffset:Z

    if-nez v0, :cond_222

    const/high16 v0, 0x3f000000    # 0.5f

    sub-float v26, v26, v0

    .line 323
    .end local v27    # "lp":Landroid/support/v7/widget/ActionMenuView$LayoutParams;
    :cond_222
    add-int/lit8 v0, v22, -0x1

    const/4 v1, 0x1

    shl-int v0, v1, v0

    int-to-long v0, v0

    and-long v0, v0, v20

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_24a

    .line 324
    add-int/lit8 v0, v22, -0x1

    move-object/from16 v1, p0

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/ActionMenuView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    move-object/from16 v27, v0

    check-cast v27, Landroid/support/v7/widget/ActionMenuView$LayoutParams;

    .line 325
    .local v27, "lp":Landroid/support/v7/widget/ActionMenuView$LayoutParams;
    move-object/from16 v0, v27

    iget-boolean v0, v0, Landroid/support/v7/widget/ActionMenuView$LayoutParams;->preventEdgeOffset:Z

    if-nez v0, :cond_24a

    const/high16 v0, 0x3f000000    # 0.5f

    sub-float v26, v26, v0

    .line 329
    .end local v27    # "lp":Landroid/support/v7/widget/ActionMenuView$LayoutParams;
    :cond_24a
    const/4 v0, 0x0

    cmpl-float v0, v26, v0

    if-lez v0, :cond_258

    mul-int v0, v14, v13

    int-to-float v0, v0

    div-float v0, v0, v26

    float-to-int v1, v0

    move/from16 v27, v1

    goto :goto_25a

    :cond_258
    const/16 v27, 0x0

    .line 332
    .local v27, "extraPixels":I
    :goto_25a
    const/16 v28, 0x0

    .local v28, "i":I
    :goto_25c
    move/from16 v0, v28

    move/from16 v1, v22

    if-ge v0, v1, :cond_2da

    .line 333
    const/4 v0, 0x1

    shl-int v0, v0, v28

    int-to-long v0, v0

    and-long v0, v0, v20

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_270

    goto/16 :goto_2d6

    .line 335
    :cond_270
    move-object/from16 v0, p0

    move/from16 v1, v28

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/ActionMenuView;->getChildAt(I)Landroid/view/View;

    move-result-object v29

    .line 336
    .local v29, "child":Landroid/view/View;
    invoke-virtual/range {v29 .. v29}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    move-object/from16 v30, v0

    check-cast v30, Landroid/support/v7/widget/ActionMenuView$LayoutParams;

    .line 337
    .local v30, "lp":Landroid/support/v7/widget/ActionMenuView$LayoutParams;
    move-object/from16 v0, v29

    instance-of v0, v0, Landroid/support/v7/view/menu/ActionMenuItemView;

    if-eqz v0, :cond_2a5

    .line 339
    move/from16 v0, v27

    move-object/from16 v1, v30

    iput v0, v1, Landroid/support/v7/widget/ActionMenuView$LayoutParams;->extraPixels:I

    .line 340
    const/4 v0, 0x1

    move-object/from16 v1, v30

    iput-boolean v0, v1, Landroid/support/v7/widget/ActionMenuView$LayoutParams;->expanded:Z

    .line 341
    if-nez v28, :cond_2a2

    move-object/from16 v0, v30

    iget-boolean v0, v0, Landroid/support/v7/widget/ActionMenuView$LayoutParams;->preventEdgeOffset:Z

    if-nez v0, :cond_2a2

    .line 344
    move/from16 v0, v27

    neg-int v0, v0

    div-int/lit8 v0, v0, 0x2

    move-object/from16 v1, v30

    iput v0, v1, Landroid/support/v7/widget/ActionMenuView$LayoutParams;->leftMargin:I

    .line 346
    :cond_2a2
    const/16 v24, 0x1

    goto :goto_2d6

    .line 347
    :cond_2a5
    move-object/from16 v0, v30

    iget-boolean v0, v0, Landroid/support/v7/widget/ActionMenuView$LayoutParams;->isOverflowButton:Z

    if-eqz v0, :cond_2c2

    .line 348
    move/from16 v0, v27

    move-object/from16 v1, v30

    iput v0, v1, Landroid/support/v7/widget/ActionMenuView$LayoutParams;->extraPixels:I

    .line 349
    const/4 v0, 0x1

    move-object/from16 v1, v30

    iput-boolean v0, v1, Landroid/support/v7/widget/ActionMenuView$LayoutParams;->expanded:Z

    .line 350
    move/from16 v0, v27

    neg-int v0, v0

    div-int/lit8 v0, v0, 0x2

    move-object/from16 v1, v30

    iput v0, v1, Landroid/support/v7/widget/ActionMenuView$LayoutParams;->rightMargin:I

    .line 351
    const/16 v24, 0x1

    goto :goto_2d6

    .line 356
    :cond_2c2
    if-eqz v28, :cond_2ca

    .line 357
    div-int/lit8 v0, v27, 0x2

    move-object/from16 v1, v30

    iput v0, v1, Landroid/support/v7/widget/ActionMenuView$LayoutParams;->leftMargin:I

    .line 359
    :cond_2ca
    add-int/lit8 v0, v22, -0x1

    move/from16 v1, v28

    if-eq v1, v0, :cond_2d6

    .line 360
    div-int/lit8 v0, v27, 0x2

    move-object/from16 v1, v30

    iput v0, v1, Landroid/support/v7/widget/ActionMenuView$LayoutParams;->rightMargin:I

    .line 332
    .end local v29    # "child":Landroid/view/View;
    .end local v30    # "lp":Landroid/support/v7/widget/ActionMenuView$LayoutParams;
    :cond_2d6
    :goto_2d6
    add-int/lit8 v28, v28, 0x1

    goto/16 :goto_25c

    .line 365
    .end local v28    # "i":I
    :cond_2da
    const/4 v14, 0x0

    .line 369
    .end local v26    # "expandCount":F
    .end local v27    # "extraPixels":I
    :cond_2db
    if-eqz v24, :cond_317

    .line 370
    const/16 v26, 0x0

    .local v26, "i":I
    :goto_2df
    move/from16 v0, v26

    move/from16 v1, v22

    if-ge v0, v1, :cond_317

    .line 371
    move-object/from16 v0, p0

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/ActionMenuView;->getChildAt(I)Landroid/view/View;

    move-result-object v27

    .line 372
    .local v27, "child":Landroid/view/View;
    invoke-virtual/range {v27 .. v27}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    move-object/from16 v28, v0

    check-cast v28, Landroid/support/v7/widget/ActionMenuView$LayoutParams;

    .line 374
    .local v28, "lp":Landroid/support/v7/widget/ActionMenuView$LayoutParams;
    move-object/from16 v0, v28

    iget-boolean v0, v0, Landroid/support/v7/widget/ActionMenuView$LayoutParams;->expanded:Z

    if-nez v0, :cond_2fc

    goto :goto_314

    .line 376
    :cond_2fc
    move-object/from16 v0, v28

    iget v0, v0, Landroid/support/v7/widget/ActionMenuView$LayoutParams;->cellsUsed:I

    mul-int/2addr v0, v13

    move-object/from16 v1, v28

    iget v1, v1, Landroid/support/v7/widget/ActionMenuView$LayoutParams;->extraPixels:I

    add-int v29, v0, v1

    .line 377
    .local v29, "width":I
    move/from16 v0, v29

    const/high16 v1, 0x40000000    # 2.0f

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    move-object/from16 v1, v27

    invoke-virtual {v1, v0, v10}, Landroid/view/View;->measure(II)V

    .line 370
    .end local v27    # "child":Landroid/view/View;
    .end local v28    # "lp":Landroid/support/v7/widget/ActionMenuView$LayoutParams;
    .end local v29    # "width":I
    :goto_314
    add-int/lit8 v26, v26, 0x1

    goto :goto_2df

    .line 382
    .end local v26    # "i":I
    :cond_317
    const/high16 v0, 0x40000000    # 2.0f

    if-eq v5, v0, :cond_31c

    .line 383
    move v7, v15

    .line 386
    :cond_31c
    move-object/from16 v0, p0

    invoke-virtual {v0, v6, v7}, Landroid/support/v7/widget/ActionMenuView;->setMeasuredDimension(II)V

    .line 387
    return-void
.end method


# virtual methods
.method protected checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z
    .registers 3
    .param p1, "p"    # Landroid/view/ViewGroup$LayoutParams;

    .line 610
    if-eqz p1, :cond_8

    instance-of v0, p1, Landroid/support/v7/widget/ActionMenuView$LayoutParams;

    if-eqz v0, :cond_8

    const/4 v0, 0x1

    goto :goto_9

    :cond_8
    const/4 v0, 0x0

    :goto_9
    return v0
.end method

.method public dismissPopupMenus()V
    .registers 2

    .line 720
    iget-object v0, p0, Landroid/support/v7/widget/ActionMenuView;->mPresenter:Landroid/support/v7/widget/ActionMenuPresenter;

    if-eqz v0, :cond_9

    .line 721
    iget-object v0, p0, Landroid/support/v7/widget/ActionMenuView;->mPresenter:Landroid/support/v7/widget/ActionMenuPresenter;

    invoke-virtual {v0}, Landroid/support/v7/widget/ActionMenuPresenter;->dismissPopupMenus()Z

    .line 723
    :cond_9
    return-void
.end method

.method public dispatchPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)Z
    .registers 3
    .param p1, "event"    # Landroid/view/accessibility/AccessibilityEvent;

    .line 746
    const/4 v0, 0x0

    return v0
.end method

.method protected generateDefaultLayoutParams()Landroid/support/v7/widget/ActionMenuView$LayoutParams;
    .registers 4

    .line 583
    new-instance v2, Landroid/support/v7/widget/ActionMenuView$LayoutParams;

    const/4 v0, -0x2

    const/4 v1, -0x2

    invoke-direct {v2, v0, v1}, Landroid/support/v7/widget/ActionMenuView$LayoutParams;-><init>(II)V

    .line 585
    .local v2, "params":Landroid/support/v7/widget/ActionMenuView$LayoutParams;
    const/16 v0, 0x10

    iput v0, v2, Landroid/support/v7/widget/ActionMenuView$LayoutParams;->gravity:I

    .line 586
    return-object v2
.end method

.method protected bridge synthetic generateDefaultLayoutParams()Landroid/support/v7/widget/LinearLayoutCompat$LayoutParams;
    .registers 2

    .line 48
    invoke-virtual {p0}, Landroid/support/v7/widget/ActionMenuView;->generateDefaultLayoutParams()Landroid/support/v7/widget/ActionMenuView$LayoutParams;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
    .registers 2

    .line 48
    invoke-virtual {p0}, Landroid/support/v7/widget/ActionMenuView;->generateDefaultLayoutParams()Landroid/support/v7/widget/ActionMenuView$LayoutParams;

    move-result-object v0

    return-object v0
.end method

.method public generateLayoutParams(Landroid/util/AttributeSet;)Landroid/support/v7/widget/ActionMenuView$LayoutParams;
    .registers 4
    .param p1, "attrs"    # Landroid/util/AttributeSet;

    .line 591
    new-instance v0, Landroid/support/v7/widget/ActionMenuView$LayoutParams;

    invoke-virtual {p0}, Landroid/support/v7/widget/ActionMenuView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Landroid/support/v7/widget/ActionMenuView$LayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-object v0
.end method

.method protected generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/support/v7/widget/ActionMenuView$LayoutParams;
    .registers 4
    .param p1, "p"    # Landroid/view/ViewGroup$LayoutParams;

    .line 596
    if-eqz p1, :cond_1d

    .line 597
    instance-of v0, p1, Landroid/support/v7/widget/ActionMenuView$LayoutParams;

    if-eqz v0, :cond_f

    new-instance v1, Landroid/support/v7/widget/ActionMenuView$LayoutParams;

    move-object v0, p1

    check-cast v0, Landroid/support/v7/widget/ActionMenuView$LayoutParams;

    invoke-direct {v1, v0}, Landroid/support/v7/widget/ActionMenuView$LayoutParams;-><init>(Landroid/support/v7/widget/ActionMenuView$LayoutParams;)V

    goto :goto_14

    :cond_f
    new-instance v1, Landroid/support/v7/widget/ActionMenuView$LayoutParams;

    invoke-direct {v1, p1}, Landroid/support/v7/widget/ActionMenuView$LayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    .line 600
    .local v1, "result":Landroid/support/v7/widget/ActionMenuView$LayoutParams;
    :goto_14
    iget v0, v1, Landroid/support/v7/widget/ActionMenuView$LayoutParams;->gravity:I

    if-gtz v0, :cond_1c

    .line 601
    const/16 v0, 0x10

    iput v0, v1, Landroid/support/v7/widget/ActionMenuView$LayoutParams;->gravity:I

    .line 603
    :cond_1c
    return-object v1

    .line 605
    .end local v1    # "result":Landroid/support/v7/widget/ActionMenuView$LayoutParams;
    :cond_1d
    invoke-virtual {p0}, Landroid/support/v7/widget/ActionMenuView;->generateDefaultLayoutParams()Landroid/support/v7/widget/ActionMenuView$LayoutParams;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic generateLayoutParams(Landroid/util/AttributeSet;)Landroid/support/v7/widget/LinearLayoutCompat$LayoutParams;
    .registers 3

    .line 48
    invoke-virtual {p0, p1}, Landroid/support/v7/widget/ActionMenuView;->generateLayoutParams(Landroid/util/AttributeSet;)Landroid/support/v7/widget/ActionMenuView$LayoutParams;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/support/v7/widget/LinearLayoutCompat$LayoutParams;
    .registers 3

    .line 48
    invoke-virtual {p0, p1}, Landroid/support/v7/widget/ActionMenuView;->generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/support/v7/widget/ActionMenuView$LayoutParams;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
    .registers 3

    .line 48
    invoke-virtual {p0, p1}, Landroid/support/v7/widget/ActionMenuView;->generateLayoutParams(Landroid/util/AttributeSet;)Landroid/support/v7/widget/ActionMenuView$LayoutParams;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;
    .registers 3

    .line 48
    invoke-virtual {p0, p1}, Landroid/support/v7/widget/ActionMenuView;->generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/support/v7/widget/ActionMenuView$LayoutParams;

    move-result-object v0

    return-object v0
.end method

.method public generateOverflowButtonLayoutParams()Landroid/support/v7/widget/ActionMenuView$LayoutParams;
    .registers 3

    .line 616
    invoke-virtual {p0}, Landroid/support/v7/widget/ActionMenuView;->generateDefaultLayoutParams()Landroid/support/v7/widget/ActionMenuView$LayoutParams;

    move-result-object v1

    .line 617
    .local v1, "result":Landroid/support/v7/widget/ActionMenuView$LayoutParams;
    const/4 v0, 0x1

    iput-boolean v0, v1, Landroid/support/v7/widget/ActionMenuView$LayoutParams;->isOverflowButton:Z

    .line 618
    return-object v1
.end method

.method public getMenu()Landroid/view/Menu;
    .registers 5

    .line 648
    iget-object v0, p0, Landroid/support/v7/widget/ActionMenuView;->mMenu:Landroid/support/v7/view/menu/MenuBuilder;

    if-nez v0, :cond_45

    .line 649
    invoke-virtual {p0}, Landroid/support/v7/widget/ActionMenuView;->getContext()Landroid/content/Context;

    move-result-object v3

    .line 650
    .local v3, "context":Landroid/content/Context;
    new-instance v0, Landroid/support/v7/view/menu/MenuBuilder;

    invoke-direct {v0, v3}, Landroid/support/v7/view/menu/MenuBuilder;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Landroid/support/v7/widget/ActionMenuView;->mMenu:Landroid/support/v7/view/menu/MenuBuilder;

    .line 651
    iget-object v0, p0, Landroid/support/v7/widget/ActionMenuView;->mMenu:Landroid/support/v7/view/menu/MenuBuilder;

    new-instance v1, Landroid/support/v7/widget/ActionMenuView$MenuBuilderCallback;

    invoke-direct {v1, p0}, Landroid/support/v7/widget/ActionMenuView$MenuBuilderCallback;-><init>(Landroid/support/v7/widget/ActionMenuView;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/view/menu/MenuBuilder;->setCallback(Landroid/support/v7/view/menu/MenuBuilder$Callback;)V

    .line 652
    new-instance v0, Landroid/support/v7/widget/ActionMenuPresenter;

    invoke-direct {v0, v3}, Landroid/support/v7/widget/ActionMenuPresenter;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Landroid/support/v7/widget/ActionMenuView;->mPresenter:Landroid/support/v7/widget/ActionMenuPresenter;

    .line 653
    iget-object v0, p0, Landroid/support/v7/widget/ActionMenuView;->mPresenter:Landroid/support/v7/widget/ActionMenuPresenter;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/ActionMenuPresenter;->setReserveOverflow(Z)V

    .line 654
    iget-object v0, p0, Landroid/support/v7/widget/ActionMenuView;->mPresenter:Landroid/support/v7/widget/ActionMenuPresenter;

    iget-object v1, p0, Landroid/support/v7/widget/ActionMenuView;->mActionMenuPresenterCallback:Landroid/support/v7/view/menu/MenuPresenter$Callback;

    if-eqz v1, :cond_2f

    iget-object v1, p0, Landroid/support/v7/widget/ActionMenuView;->mActionMenuPresenterCallback:Landroid/support/v7/view/menu/MenuPresenter$Callback;

    goto :goto_34

    :cond_2f
    new-instance v1, Landroid/support/v7/widget/ActionMenuView$ActionMenuPresenterCallback;

    invoke-direct {v1, p0}, Landroid/support/v7/widget/ActionMenuView$ActionMenuPresenterCallback;-><init>(Landroid/support/v7/widget/ActionMenuView;)V

    :goto_34
    invoke-virtual {v0, v1}, Landroid/support/v7/widget/ActionMenuPresenter;->setCallback(Landroid/support/v7/view/menu/MenuPresenter$Callback;)V

    .line 656
    iget-object v0, p0, Landroid/support/v7/widget/ActionMenuView;->mMenu:Landroid/support/v7/view/menu/MenuBuilder;

    iget-object v1, p0, Landroid/support/v7/widget/ActionMenuView;->mPresenter:Landroid/support/v7/widget/ActionMenuPresenter;

    iget-object v2, p0, Landroid/support/v7/widget/ActionMenuView;->mPopupContext:Landroid/content/Context;

    invoke-virtual {v0, v1, v2}, Landroid/support/v7/view/menu/MenuBuilder;->addMenuPresenter(Landroid/support/v7/view/menu/MenuPresenter;Landroid/content/Context;)V

    .line 657
    iget-object v0, p0, Landroid/support/v7/widget/ActionMenuView;->mPresenter:Landroid/support/v7/widget/ActionMenuPresenter;

    invoke-virtual {v0, p0}, Landroid/support/v7/widget/ActionMenuPresenter;->setMenuView(Landroid/support/v7/widget/ActionMenuView;)V

    .line 660
    .end local v3    # "context":Landroid/content/Context;
    :cond_45
    iget-object v0, p0, Landroid/support/v7/widget/ActionMenuView;->mMenu:Landroid/support/v7/view/menu/MenuBuilder;

    return-object v0
.end method

.method protected hasSupportDividerBeforeChildAt(I)Z
    .registers 6
    .param p1, "childIndex"    # I

    .line 730
    if-nez p1, :cond_4

    .line 731
    const/4 v0, 0x0

    return v0

    .line 733
    :cond_4
    add-int/lit8 v0, p1, -0x1

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/ActionMenuView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 734
    .local v1, "childBefore":Landroid/view/View;
    invoke-virtual {p0, p1}, Landroid/support/v7/widget/ActionMenuView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 735
    .local v2, "child":Landroid/view/View;
    const/4 v3, 0x0

    .line 736
    .local v3, "result":Z
    invoke-virtual {p0}, Landroid/support/v7/widget/ActionMenuView;->getChildCount()I

    move-result v0

    if-ge p1, v0, :cond_22

    instance-of v0, v1, Landroid/support/v7/widget/ActionMenuView$ActionMenuChildView;

    if-eqz v0, :cond_22

    .line 737
    move-object v0, v1

    check-cast v0, Landroid/support/v7/widget/ActionMenuView$ActionMenuChildView;

    invoke-interface {v0}, Landroid/support/v7/widget/ActionMenuView$ActionMenuChildView;->needsDividerAfter()Z

    move-result v0

    or-int/lit8 v3, v0, 0x0

    .line 739
    :cond_22
    if-lez p1, :cond_30

    instance-of v0, v2, Landroid/support/v7/widget/ActionMenuView$ActionMenuChildView;

    if-eqz v0, :cond_30

    .line 740
    move-object v0, v2

    check-cast v0, Landroid/support/v7/widget/ActionMenuView$ActionMenuChildView;

    invoke-interface {v0}, Landroid/support/v7/widget/ActionMenuView$ActionMenuChildView;->needsDividerBefore()Z

    move-result v0

    or-int/2addr v3, v0

    .line 742
    :cond_30
    return v3
.end method

.method public hideOverflowMenu()Z
    .registers 2

    .line 697
    iget-object v0, p0, Landroid/support/v7/widget/ActionMenuView;->mPresenter:Landroid/support/v7/widget/ActionMenuPresenter;

    if-eqz v0, :cond_e

    iget-object v0, p0, Landroid/support/v7/widget/ActionMenuView;->mPresenter:Landroid/support/v7/widget/ActionMenuPresenter;

    invoke-virtual {v0}, Landroid/support/v7/widget/ActionMenuPresenter;->hideOverflowMenu()Z

    move-result v0

    if-eqz v0, :cond_e

    const/4 v0, 0x1

    goto :goto_f

    :cond_e
    const/4 v0, 0x0

    :goto_f
    return v0
.end method

.method public initialize(Landroid/support/v7/view/menu/MenuBuilder;)V
    .registers 2
    .param p1, "menu"    # Landroid/support/v7/view/menu/MenuBuilder;

    .line 636
    iput-object p1, p0, Landroid/support/v7/widget/ActionMenuView;->mMenu:Landroid/support/v7/view/menu/MenuBuilder;

    .line 637
    return-void
.end method

.method public invokeItem(Landroid/support/v7/view/menu/MenuItemImpl;)Z
    .registers 4
    .param p1, "item"    # Landroid/support/v7/view/menu/MenuItemImpl;

    .line 624
    iget-object v0, p0, Landroid/support/v7/widget/ActionMenuView;->mMenu:Landroid/support/v7/view/menu/MenuBuilder;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Landroid/support/v7/view/menu/MenuBuilder;->performItemAction(Landroid/view/MenuItem;I)Z

    move-result v0

    return v0
.end method

.method public isOverflowMenuShowPending()Z
    .registers 2

    .line 713
    iget-object v0, p0, Landroid/support/v7/widget/ActionMenuView;->mPresenter:Landroid/support/v7/widget/ActionMenuPresenter;

    if-eqz v0, :cond_e

    iget-object v0, p0, Landroid/support/v7/widget/ActionMenuView;->mPresenter:Landroid/support/v7/widget/ActionMenuPresenter;

    invoke-virtual {v0}, Landroid/support/v7/widget/ActionMenuPresenter;->isOverflowMenuShowPending()Z

    move-result v0

    if-eqz v0, :cond_e

    const/4 v0, 0x1

    goto :goto_f

    :cond_e
    const/4 v0, 0x0

    :goto_f
    return v0
.end method

.method public isOverflowMenuShowing()Z
    .registers 2

    .line 707
    iget-object v0, p0, Landroid/support/v7/widget/ActionMenuView;->mPresenter:Landroid/support/v7/widget/ActionMenuPresenter;

    if-eqz v0, :cond_e

    iget-object v0, p0, Landroid/support/v7/widget/ActionMenuView;->mPresenter:Landroid/support/v7/widget/ActionMenuPresenter;

    invoke-virtual {v0}, Landroid/support/v7/widget/ActionMenuPresenter;->isOverflowMenuShowing()Z

    move-result v0

    if-eqz v0, :cond_e

    const/4 v0, 0x1

    goto :goto_f

    :cond_e
    const/4 v0, 0x0

    :goto_f
    return v0
.end method

.method public isOverflowReserved()Z
    .registers 2

    .line 572
    iget-boolean v0, p0, Landroid/support/v7/widget/ActionMenuView;->mReserveOverflow:Z

    return v0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .registers 4
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .line 128
    invoke-super {p0, p1}, Landroid/support/v7/widget/LinearLayoutCompat;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 130
    iget-object v0, p0, Landroid/support/v7/widget/ActionMenuView;->mPresenter:Landroid/support/v7/widget/ActionMenuPresenter;

    if-eqz v0, :cond_1f

    .line 131
    iget-object v0, p0, Landroid/support/v7/widget/ActionMenuView;->mPresenter:Landroid/support/v7/widget/ActionMenuPresenter;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/ActionMenuPresenter;->updateMenuView(Z)V

    .line 133
    iget-object v0, p0, Landroid/support/v7/widget/ActionMenuView;->mPresenter:Landroid/support/v7/widget/ActionMenuPresenter;

    invoke-virtual {v0}, Landroid/support/v7/widget/ActionMenuPresenter;->isOverflowMenuShowing()Z

    move-result v0

    if-eqz v0, :cond_1f

    .line 134
    iget-object v0, p0, Landroid/support/v7/widget/ActionMenuView;->mPresenter:Landroid/support/v7/widget/ActionMenuPresenter;

    invoke-virtual {v0}, Landroid/support/v7/widget/ActionMenuPresenter;->hideOverflowMenu()Z

    .line 135
    iget-object v0, p0, Landroid/support/v7/widget/ActionMenuView;->mPresenter:Landroid/support/v7/widget/ActionMenuPresenter;

    invoke-virtual {v0}, Landroid/support/v7/widget/ActionMenuPresenter;->showOverflowMenu()Z

    .line 138
    :cond_1f
    return-void
.end method

.method public onDetachedFromWindow()V
    .registers 1

    .line 544
    invoke-super {p0}, Landroid/support/v7/widget/LinearLayoutCompat;->onDetachedFromWindow()V

    .line 545
    invoke-virtual {p0}, Landroid/support/v7/widget/ActionMenuView;->dismissPopupMenus()V

    .line 546
    return-void
.end method

.method protected onLayout(ZIIII)V
    .registers 29
    .param p1, "changed"    # Z
    .param p2, "left"    # I
    .param p3, "top"    # I
    .param p4, "right"    # I
    .param p5, "bottom"    # I

    .line 439
    move-object/from16 v0, p0

    iget-boolean v0, v0, Landroid/support/v7/widget/ActionMenuView;->mFormatItems:Z

    if-nez v0, :cond_a

    .line 440
    invoke-super/range {p0 .. p5}, Landroid/support/v7/widget/LinearLayoutCompat;->onLayout(ZIIII)V

    .line 441
    return-void

    .line 444
    :cond_a
    invoke-virtual/range {p0 .. p0}, Landroid/support/v7/widget/ActionMenuView;->getChildCount()I

    move-result v5

    .line 445
    .local v5, "childCount":I
    sub-int v0, p5, p3

    div-int/lit8 v6, v0, 0x2

    .line 446
    .local v6, "midVertical":I
    invoke-virtual/range {p0 .. p0}, Landroid/support/v7/widget/ActionMenuView;->getDividerWidth()I

    move-result v7

    .line 447
    .local v7, "dividerWidth":I
    const/4 v8, 0x0

    .line 448
    .local v8, "overflowWidth":I
    const/4 v9, 0x0

    .line 449
    .local v9, "nonOverflowWidth":I
    const/4 v10, 0x0

    .line 450
    .local v10, "nonOverflowCount":I
    sub-int v0, p4, p2

    invoke-virtual/range {p0 .. p0}, Landroid/support/v7/widget/ActionMenuView;->getPaddingRight()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual/range {p0 .. p0}, Landroid/support/v7/widget/ActionMenuView;->getPaddingLeft()I

    move-result v1

    sub-int v11, v0, v1

    .line 451
    .local v11, "widthRemaining":I
    const/4 v12, 0x0

    .line 452
    .local v12, "hasOverflow":Z
    invoke-static/range {p0 .. p0}, Landroid/support/v7/widget/ViewUtils;->isLayoutRtl(Landroid/view/View;)Z

    move-result v13

    .line 453
    .local v13, "isLayoutRtl":Z
    const/4 v14, 0x0

    .local v14, "i":I
    :goto_2c
    if-ge v14, v5, :cond_b3

    .line 454
    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Landroid/support/v7/widget/ActionMenuView;->getChildAt(I)Landroid/view/View;

    move-result-object v15

    .line 455
    .local v15, "v":Landroid/view/View;
    invoke-virtual {v15}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_3e

    .line 456
    goto/16 :goto_af

    .line 459
    :cond_3e
    invoke-virtual {v15}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    move-object/from16 v16, v0

    check-cast v16, Landroid/support/v7/widget/ActionMenuView$LayoutParams;

    .line 460
    .local v16, "p":Landroid/support/v7/widget/ActionMenuView$LayoutParams;
    move-object/from16 v0, v16

    iget-boolean v0, v0, Landroid/support/v7/widget/ActionMenuView$LayoutParams;->isOverflowButton:Z

    if-eqz v0, :cond_91

    .line 461
    invoke-virtual {v15}, Landroid/view/View;->getMeasuredWidth()I

    move-result v8

    .line 462
    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Landroid/support/v7/widget/ActionMenuView;->hasSupportDividerBeforeChildAt(I)Z

    move-result v0

    if-eqz v0, :cond_59

    .line 463
    add-int/2addr v8, v7

    .line 465
    :cond_59
    invoke-virtual {v15}, Landroid/view/View;->getMeasuredHeight()I

    move-result v17

    .line 468
    .local v17, "height":I
    if-eqz v13, :cond_6c

    .line 469
    invoke-virtual/range {p0 .. p0}, Landroid/support/v7/widget/ActionMenuView;->getPaddingLeft()I

    move-result v0

    move-object/from16 v1, v16

    iget v1, v1, Landroid/support/v7/widget/ActionMenuView$LayoutParams;->leftMargin:I

    add-int v19, v0, v1

    .line 470
    .local v19, "l":I
    add-int v18, v19, v8

    .local v18, "r":I
    goto :goto_7d

    .line 472
    .end local v18    # "r":I
    .end local v19    # "l":I
    :cond_6c
    invoke-virtual/range {p0 .. p0}, Landroid/support/v7/widget/ActionMenuView;->getWidth()I

    move-result v0

    invoke-virtual/range {p0 .. p0}, Landroid/support/v7/widget/ActionMenuView;->getPaddingRight()I

    move-result v1

    sub-int/2addr v0, v1

    move-object/from16 v1, v16

    iget v1, v1, Landroid/support/v7/widget/ActionMenuView$LayoutParams;->rightMargin:I

    sub-int v18, v0, v1

    .line 473
    .local v18, "r":I
    sub-int v19, v18, v8

    .line 475
    .local v19, "l":I
    :goto_7d
    div-int/lit8 v0, v17, 0x2

    sub-int v20, v6, v0

    .line 476
    .local v20, "t":I
    add-int v21, v20, v17

    .line 477
    .local v21, "b":I
    move/from16 v0, v19

    move/from16 v1, v20

    move/from16 v2, v18

    move/from16 v3, v21

    invoke-virtual {v15, v0, v1, v2, v3}, Landroid/view/View;->layout(IIII)V

    .line 479
    sub-int/2addr v11, v8

    .line 480
    const/4 v12, 0x1

    .line 481
    .end local v17    # "height":I
    .end local v18    # "r":I
    .end local v19    # "l":I
    .end local v20    # "t":I
    .end local v21    # "b":I
    goto :goto_af

    .line 482
    :cond_91
    invoke-virtual {v15}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    move-object/from16 v1, v16

    iget v1, v1, Landroid/support/v7/widget/ActionMenuView$LayoutParams;->leftMargin:I

    add-int/2addr v0, v1

    move-object/from16 v1, v16

    iget v1, v1, Landroid/support/v7/widget/ActionMenuView$LayoutParams;->rightMargin:I

    add-int v17, v0, v1

    .line 483
    .local v17, "size":I
    add-int v9, v9, v17

    .line 484
    sub-int v11, v11, v17

    .line 485
    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Landroid/support/v7/widget/ActionMenuView;->hasSupportDividerBeforeChildAt(I)Z

    move-result v0

    if-eqz v0, :cond_ad

    .line 486
    add-int/2addr v9, v7

    .line 488
    :cond_ad
    add-int/lit8 v10, v10, 0x1

    .line 453
    .end local v15    # "v":Landroid/view/View;
    .end local v16    # "p":Landroid/support/v7/widget/ActionMenuView$LayoutParams;
    .end local v17    # "size":I
    :goto_af
    add-int/lit8 v14, v14, 0x1

    goto/16 :goto_2c

    .line 492
    .end local v14    # "i":I
    :cond_b3
    const/4 v0, 0x1

    if-ne v5, v0, :cond_df

    if-nez v12, :cond_df

    .line 494
    move-object/from16 v0, p0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/ActionMenuView;->getChildAt(I)Landroid/view/View;

    move-result-object v14

    .line 495
    .local v14, "v":Landroid/view/View;
    invoke-virtual {v14}, Landroid/view/View;->getMeasuredWidth()I

    move-result v15

    .line 496
    .local v15, "width":I
    invoke-virtual {v14}, Landroid/view/View;->getMeasuredHeight()I

    move-result v16

    .line 497
    .local v16, "height":I
    sub-int v0, p4, p2

    div-int/lit8 v17, v0, 0x2

    .line 498
    .local v17, "midHorizontal":I
    div-int/lit8 v0, v15, 0x2

    sub-int v18, v17, v0

    .line 499
    .local v18, "l":I
    div-int/lit8 v0, v16, 0x2

    sub-int v19, v6, v0

    .line 500
    .local v19, "t":I
    add-int v0, v18, v15

    add-int v1, v19, v16

    move/from16 v2, v18

    move/from16 v3, v19

    invoke-virtual {v14, v2, v3, v0, v1}, Landroid/view/View;->layout(IIII)V

    .line 501
    return-void

    .line 504
    .end local v14    # "v":Landroid/view/View;
    .end local v15    # "width":I
    .end local v16    # "height":I
    .end local v17    # "midHorizontal":I
    .end local v18    # "l":I
    .end local v19    # "t":I
    :cond_df
    if-eqz v12, :cond_e3

    const/4 v0, 0x0

    goto :goto_e4

    :cond_e3
    const/4 v0, 0x1

    :goto_e4
    sub-int v14, v10, v0

    .line 505
    .local v14, "spacerCount":I
    if-lez v14, :cond_eb

    div-int v0, v11, v14

    goto :goto_ec

    :cond_eb
    const/4 v0, 0x0

    :goto_ec
    const/4 v1, 0x0

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v15

    .line 507
    .local v15, "spacerSize":I
    if-eqz v13, :cond_14f

    .line 508
    invoke-virtual/range {p0 .. p0}, Landroid/support/v7/widget/ActionMenuView;->getWidth()I

    move-result v0

    invoke-virtual/range {p0 .. p0}, Landroid/support/v7/widget/ActionMenuView;->getPaddingRight()I

    move-result v1

    sub-int v16, v0, v1

    .line 509
    .local v16, "startRight":I
    const/16 v17, 0x0

    .local v17, "i":I
    :goto_ff
    move/from16 v0, v17

    if-ge v0, v5, :cond_14d

    .line 510
    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/ActionMenuView;->getChildAt(I)Landroid/view/View;

    move-result-object v18

    .line 511
    .local v18, "v":Landroid/view/View;
    invoke-virtual/range {v18 .. v18}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    move-object/from16 v19, v0

    check-cast v19, Landroid/support/v7/widget/ActionMenuView$LayoutParams;

    .line 512
    .local v19, "lp":Landroid/support/v7/widget/ActionMenuView$LayoutParams;
    invoke-virtual/range {v18 .. v18}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-eq v0, v1, :cond_14a

    move-object/from16 v0, v19

    iget-boolean v0, v0, Landroid/support/v7/widget/ActionMenuView$LayoutParams;->isOverflowButton:Z

    if-eqz v0, :cond_122

    .line 513
    goto :goto_14a

    .line 516
    :cond_122
    move-object/from16 v0, v19

    iget v0, v0, Landroid/support/v7/widget/ActionMenuView$LayoutParams;->rightMargin:I

    sub-int v16, v16, v0

    .line 517
    invoke-virtual/range {v18 .. v18}, Landroid/view/View;->getMeasuredWidth()I

    move-result v20

    .line 518
    .local v20, "width":I
    invoke-virtual/range {v18 .. v18}, Landroid/view/View;->getMeasuredHeight()I

    move-result v21

    .line 519
    .local v21, "height":I
    div-int/lit8 v0, v21, 0x2

    sub-int v22, v6, v0

    .line 520
    .local v22, "t":I
    sub-int v0, v16, v20

    add-int v1, v22, v21

    move-object/from16 v2, v18

    move/from16 v3, v22

    move/from16 v4, v16

    invoke-virtual {v2, v0, v3, v4, v1}, Landroid/view/View;->layout(IIII)V

    .line 521
    move-object/from16 v0, v19

    iget v0, v0, Landroid/support/v7/widget/ActionMenuView$LayoutParams;->leftMargin:I

    add-int v0, v0, v20

    add-int/2addr v0, v15

    sub-int v16, v16, v0

    .line 509
    .end local v18    # "v":Landroid/view/View;
    .end local v19    # "lp":Landroid/support/v7/widget/ActionMenuView$LayoutParams;
    .end local v20    # "width":I
    .end local v21    # "height":I
    .end local v22    # "t":I
    :cond_14a
    :goto_14a
    add-int/lit8 v17, v17, 0x1

    goto :goto_ff

    .line 523
    .end local v16    # "startRight":I
    .end local v17    # "i":I
    :cond_14d
    goto/16 :goto_1a3

    .line 524
    :cond_14f
    invoke-virtual/range {p0 .. p0}, Landroid/support/v7/widget/ActionMenuView;->getPaddingLeft()I

    move-result v16

    .line 525
    .local v16, "startLeft":I
    const/16 v17, 0x0

    .local v17, "i":I
    :goto_155
    move/from16 v0, v17

    if-ge v0, v5, :cond_1a3

    .line 526
    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/ActionMenuView;->getChildAt(I)Landroid/view/View;

    move-result-object v18

    .line 527
    .local v18, "v":Landroid/view/View;
    invoke-virtual/range {v18 .. v18}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    move-object/from16 v19, v0

    check-cast v19, Landroid/support/v7/widget/ActionMenuView$LayoutParams;

    .line 528
    .local v19, "lp":Landroid/support/v7/widget/ActionMenuView$LayoutParams;
    invoke-virtual/range {v18 .. v18}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-eq v0, v1, :cond_1a0

    move-object/from16 v0, v19

    iget-boolean v0, v0, Landroid/support/v7/widget/ActionMenuView$LayoutParams;->isOverflowButton:Z

    if-eqz v0, :cond_178

    .line 529
    goto :goto_1a0

    .line 532
    :cond_178
    move-object/from16 v0, v19

    iget v0, v0, Landroid/support/v7/widget/ActionMenuView$LayoutParams;->leftMargin:I

    add-int v16, v16, v0

    .line 533
    invoke-virtual/range {v18 .. v18}, Landroid/view/View;->getMeasuredWidth()I

    move-result v20

    .line 534
    .local v20, "width":I
    invoke-virtual/range {v18 .. v18}, Landroid/view/View;->getMeasuredHeight()I

    move-result v21

    .line 535
    .local v21, "height":I
    div-int/lit8 v0, v21, 0x2

    sub-int v22, v6, v0

    .line 536
    .local v22, "t":I
    add-int v0, v16, v20

    add-int v1, v22, v21

    move-object/from16 v2, v18

    move/from16 v3, v16

    move/from16 v4, v22

    invoke-virtual {v2, v3, v4, v0, v1}, Landroid/view/View;->layout(IIII)V

    .line 537
    move-object/from16 v0, v19

    iget v0, v0, Landroid/support/v7/widget/ActionMenuView$LayoutParams;->rightMargin:I

    add-int v0, v0, v20

    add-int/2addr v0, v15

    add-int v16, v16, v0

    .line 525
    .end local v18    # "v":Landroid/view/View;
    .end local v19    # "lp":Landroid/support/v7/widget/ActionMenuView$LayoutParams;
    .end local v20    # "width":I
    .end local v21    # "height":I
    .end local v22    # "t":I
    :cond_1a0
    :goto_1a0
    add-int/lit8 v17, v17, 0x1

    goto :goto_155

    .line 540
    .end local v16    # "startLeft":I
    .end local v17    # "i":I
    :cond_1a3
    :goto_1a3
    return-void
.end method

.method protected onMeasure(II)V
    .registers 11
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .line 147
    iget-boolean v2, p0, Landroid/support/v7/widget/ActionMenuView;->mFormatItems:Z

    .line 148
    .local v2, "wasFormatted":Z
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    const/high16 v1, 0x40000000    # 2.0f

    if-ne v0, v1, :cond_c

    const/4 v0, 0x1

    goto :goto_d

    :cond_c
    const/4 v0, 0x0

    :goto_d
    iput-boolean v0, p0, Landroid/support/v7/widget/ActionMenuView;->mFormatItems:Z

    .line 150
    iget-boolean v0, p0, Landroid/support/v7/widget/ActionMenuView;->mFormatItems:Z

    if-eq v2, v0, :cond_16

    .line 151
    const/4 v0, 0x0

    iput v0, p0, Landroid/support/v7/widget/ActionMenuView;->mFormatItemsWidth:I

    .line 156
    :cond_16
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v3

    .line 157
    .local v3, "widthSize":I
    iget-boolean v0, p0, Landroid/support/v7/widget/ActionMenuView;->mFormatItems:Z

    if-eqz v0, :cond_2e

    iget-object v0, p0, Landroid/support/v7/widget/ActionMenuView;->mMenu:Landroid/support/v7/view/menu/MenuBuilder;

    if-eqz v0, :cond_2e

    iget v0, p0, Landroid/support/v7/widget/ActionMenuView;->mFormatItemsWidth:I

    if-eq v3, v0, :cond_2e

    .line 158
    iput v3, p0, Landroid/support/v7/widget/ActionMenuView;->mFormatItemsWidth:I

    .line 159
    iget-object v0, p0, Landroid/support/v7/widget/ActionMenuView;->mMenu:Landroid/support/v7/view/menu/MenuBuilder;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v7/view/menu/MenuBuilder;->onItemsChanged(Z)V

    .line 162
    :cond_2e
    invoke-virtual {p0}, Landroid/support/v7/widget/ActionMenuView;->getChildCount()I

    move-result v4

    .line 163
    .local v4, "childCount":I
    iget-boolean v0, p0, Landroid/support/v7/widget/ActionMenuView;->mFormatItems:Z

    if-eqz v0, :cond_3c

    if-lez v4, :cond_3c

    .line 164
    invoke-direct {p0, p1, p2}, Landroid/support/v7/widget/ActionMenuView;->onMeasureExactFormat(II)V

    goto :goto_56

    .line 167
    :cond_3c
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_3d
    if-ge v5, v4, :cond_53

    .line 168
    invoke-virtual {p0, v5}, Landroid/support/v7/widget/ActionMenuView;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    .line 169
    .local v6, "child":Landroid/view/View;
    invoke-virtual {v6}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Landroid/support/v7/widget/ActionMenuView$LayoutParams;

    .line 170
    .local v7, "lp":Landroid/support/v7/widget/ActionMenuView$LayoutParams;
    const/4 v0, 0x0

    iput v0, v7, Landroid/support/v7/widget/ActionMenuView$LayoutParams;->rightMargin:I

    const/4 v0, 0x0

    iput v0, v7, Landroid/support/v7/widget/ActionMenuView$LayoutParams;->leftMargin:I

    .line 167
    .end local v6    # "child":Landroid/view/View;
    .end local v7    # "lp":Landroid/support/v7/widget/ActionMenuView$LayoutParams;
    add-int/lit8 v5, v5, 0x1

    goto :goto_3d

    .line 172
    .end local v5    # "i":I
    :cond_53
    invoke-super {p0, p1, p2}, Landroid/support/v7/widget/LinearLayoutCompat;->onMeasure(II)V

    .line 174
    :goto_56
    return-void
.end method

.method public peekMenu()Landroid/support/v7/view/menu/MenuBuilder;
    .registers 2

    .line 679
    iget-object v0, p0, Landroid/support/v7/widget/ActionMenuView;->mMenu:Landroid/support/v7/view/menu/MenuBuilder;

    return-object v0
.end method

.method public setExpandedActionViewsExclusive(Z)V
    .registers 3
    .param p1, "exclusive"    # Z

    .line 752
    iget-object v0, p0, Landroid/support/v7/widget/ActionMenuView;->mPresenter:Landroid/support/v7/widget/ActionMenuPresenter;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/ActionMenuPresenter;->setExpandedActionViewsExclusive(Z)V

    .line 753
    return-void
.end method

.method public setMenuCallbacks(Landroid/support/v7/view/menu/MenuPresenter$Callback;Landroid/support/v7/view/menu/MenuBuilder$Callback;)V
    .registers 3
    .param p1, "pcb"    # Landroid/support/v7/view/menu/MenuPresenter$Callback;
    .param p2, "mcb"    # Landroid/support/v7/view/menu/MenuBuilder$Callback;

    .line 669
    iput-object p1, p0, Landroid/support/v7/widget/ActionMenuView;->mActionMenuPresenterCallback:Landroid/support/v7/view/menu/MenuPresenter$Callback;

    .line 670
    iput-object p2, p0, Landroid/support/v7/widget/ActionMenuView;->mMenuBuilderCallback:Landroid/support/v7/view/menu/MenuBuilder$Callback;

    .line 671
    return-void
.end method

.method public setOnMenuItemClickListener(Landroid/support/v7/widget/ActionMenuView$OnMenuItemClickListener;)V
    .registers 2
    .param p1, "listener"    # Landroid/support/v7/widget/ActionMenuView$OnMenuItemClickListener;

    .line 141
    iput-object p1, p0, Landroid/support/v7/widget/ActionMenuView;->mOnMenuItemClickListener:Landroid/support/v7/widget/ActionMenuView$OnMenuItemClickListener;

    .line 142
    return-void
.end method

.method public setOverflowIcon(Landroid/graphics/drawable/Drawable;)V
    .registers 3
    .param p1, "icon"    # Landroid/graphics/drawable/Drawable;

    .line 554
    invoke-virtual {p0}, Landroid/support/v7/widget/ActionMenuView;->getMenu()Landroid/view/Menu;

    .line 555
    iget-object v0, p0, Landroid/support/v7/widget/ActionMenuView;->mPresenter:Landroid/support/v7/widget/ActionMenuPresenter;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/ActionMenuPresenter;->setOverflowIcon(Landroid/graphics/drawable/Drawable;)V

    .line 556
    return-void
.end method

.method public setOverflowReserved(Z)V
    .registers 2
    .param p1, "reserveOverflow"    # Z

    .line 578
    iput-boolean p1, p0, Landroid/support/v7/widget/ActionMenuView;->mReserveOverflow:Z

    .line 579
    return-void
.end method

.method public setPopupTheme(I)V
    .registers 4
    .param p1, "resId"    # I

    .line 97
    iget v0, p0, Landroid/support/v7/widget/ActionMenuView;->mPopupTheme:I

    if-eq v0, p1, :cond_1a

    .line 98
    iput p1, p0, Landroid/support/v7/widget/ActionMenuView;->mPopupTheme:I

    .line 99
    if-nez p1, :cond_f

    .line 100
    invoke-virtual {p0}, Landroid/support/v7/widget/ActionMenuView;->getContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/widget/ActionMenuView;->mPopupContext:Landroid/content/Context;

    goto :goto_1a

    .line 102
    :cond_f
    new-instance v0, Landroid/view/ContextThemeWrapper;

    invoke-virtual {p0}, Landroid/support/v7/widget/ActionMenuView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Landroid/support/v7/widget/ActionMenuView;->mPopupContext:Landroid/content/Context;

    .line 105
    :cond_1a
    :goto_1a
    return-void
.end method

.method public setPresenter(Landroid/support/v7/widget/ActionMenuPresenter;)V
    .registers 3
    .param p1, "presenter"    # Landroid/support/v7/widget/ActionMenuPresenter;

    .line 122
    iput-object p1, p0, Landroid/support/v7/widget/ActionMenuView;->mPresenter:Landroid/support/v7/widget/ActionMenuPresenter;

    .line 123
    iget-object v0, p0, Landroid/support/v7/widget/ActionMenuView;->mPresenter:Landroid/support/v7/widget/ActionMenuPresenter;

    invoke-virtual {v0, p0}, Landroid/support/v7/widget/ActionMenuPresenter;->setMenuView(Landroid/support/v7/widget/ActionMenuView;)V

    .line 124
    return-void
.end method

.method public showOverflowMenu()Z
    .registers 2

    .line 688
    iget-object v0, p0, Landroid/support/v7/widget/ActionMenuView;->mPresenter:Landroid/support/v7/widget/ActionMenuPresenter;

    if-eqz v0, :cond_e

    iget-object v0, p0, Landroid/support/v7/widget/ActionMenuView;->mPresenter:Landroid/support/v7/widget/ActionMenuPresenter;

    invoke-virtual {v0}, Landroid/support/v7/widget/ActionMenuPresenter;->showOverflowMenu()Z

    move-result v0

    if-eqz v0, :cond_e

    const/4 v0, 0x1

    goto :goto_f

    :cond_e
    const/4 v0, 0x0

    :goto_f
    return v0
.end method

.class Lcom/google/android/gms/ads/internal/request/zzb$1;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/gms/ads/internal/request/zzb;->zzbr()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic zzHj:Lcom/google/android/gms/ads/internal/request/zzb;


# direct methods
.method constructor <init>(Lcom/google/android/gms/ads/internal/request/zzb;)V
    .registers 2

    iput-object p1, p0, Lcom/google/android/gms/ads/internal/request/zzb$1;->zzHj:Lcom/google/android/gms/ads/internal/request/zzb;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .registers 6

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/request/zzb$1;->zzHj:Lcom/google/android/gms/ads/internal/request/zzb;

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/request/zzb;->zza(Lcom/google/android/gms/ads/internal/request/zzb;)Ljava/lang/Object;

    move-result-object v3

    monitor-enter v3

    :try_start_7
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/request/zzb$1;->zzHj:Lcom/google/android/gms/ads/internal/request/zzb;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/request/zzb;->zzHi:Lcom/google/android/gms/internal/zzit;
    :try_end_b
    .catchall {:try_start_7 .. :try_end_b} :catchall_1e

    if-nez v0, :cond_f

    monitor-exit v3

    return-void

    :cond_f
    :try_start_f
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/request/zzb$1;->zzHj:Lcom/google/android/gms/ads/internal/request/zzb;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/internal/request/zzb;->onStop()V

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/request/zzb$1;->zzHj:Lcom/google/android/gms/ads/internal/request/zzb;

    const-string v1, "Timed out waiting for ad response."

    const/4 v2, 0x2

    invoke-static {v0, v2, v1}, Lcom/google/android/gms/ads/internal/request/zzb;->zza(Lcom/google/android/gms/ads/internal/request/zzb;ILjava/lang/String;)V
    :try_end_1c
    .catchall {:try_start_f .. :try_end_1c} :catchall_1e

    monitor-exit v3

    goto :goto_21

    :catchall_1e
    move-exception v4

    monitor-exit v3

    throw v4

    :goto_21
    return-void
.end method

.class public Lcom/google/android/gms/internal/zzbd;
.super Ljava/lang/Object;


# annotations
.annotation runtime Lcom/google/android/gms/internal/zzhb;
.end annotation


# instance fields
.field private final zzpV:Ljava/lang/Object;

.field private zzsW:I

.field private zzsX:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Lcom/google/android/gms/internal/zzbc;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/internal/zzbd;->zzpV:Ljava/lang/Object;

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/internal/zzbd;->zzsX:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public zza(Lcom/google/android/gms/internal/zzbc;)Z
    .registers 5

    iget-object v1, p0, Lcom/google/android/gms/internal/zzbd;->zzpV:Ljava/lang/Object;

    monitor-enter v1

    :try_start_3
    iget-object v0, p0, Lcom/google/android/gms/internal/zzbd;->zzsX:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z
    :try_end_8
    .catchall {:try_start_3 .. :try_end_8} :catchall_11

    move-result v0

    if-eqz v0, :cond_e

    monitor-exit v1

    const/4 v0, 0x1

    return v0

    :cond_e
    monitor-exit v1

    const/4 v0, 0x0

    return v0

    :catchall_11
    move-exception v2

    monitor-exit v1

    throw v2
.end method

.method public zzb(Lcom/google/android/gms/internal/zzbc;)Z
    .registers 8

    iget-object v2, p0, Lcom/google/android/gms/internal/zzbd;->zzpV:Ljava/lang/Object;

    monitor-enter v2

    :try_start_3
    iget-object v0, p0, Lcom/google/android/gms/internal/zzbd;->zzsX:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_9
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2d

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/google/android/gms/internal/zzbc;

    if-eq p1, v4, :cond_2c

    invoke-virtual {v4}, Lcom/google/android/gms/internal/zzbc;->zzcy()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/gms/internal/zzbc;->zzcy()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2c

    invoke-interface {v3}, Ljava/util/Iterator;->remove()V
    :try_end_29
    .catchall {:try_start_3 .. :try_end_29} :catchall_30

    monitor-exit v2

    const/4 v0, 0x1

    return v0

    :cond_2c
    goto :goto_9

    :cond_2d
    monitor-exit v2

    const/4 v0, 0x0

    return v0

    :catchall_30
    move-exception v5

    monitor-exit v2

    throw v5
.end method

.method public zzc(Lcom/google/android/gms/internal/zzbc;)V
    .registers 6

    iget-object v2, p0, Lcom/google/android/gms/internal/zzbd;->zzpV:Ljava/lang/Object;

    monitor-enter v2

    :try_start_3
    iget-object v0, p0, Lcom/google/android/gms/internal/zzbd;->zzsX:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/16 v1, 0xa

    if-lt v0, v1, :cond_2f

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Queue is full, current size = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/internal/zzbd;->zzsX:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/internal/zzin;->zzaI(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/internal/zzbd;->zzsX:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    :cond_2f
    iget v0, p0, Lcom/google/android/gms/internal/zzbd;->zzsW:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/google/android/gms/internal/zzbd;->zzsW:I

    invoke-virtual {p1, v0}, Lcom/google/android/gms/internal/zzbc;->zzh(I)V

    iget-object v0, p0, Lcom/google/android/gms/internal/zzbd;->zzsX:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_3d
    .catchall {:try_start_3 .. :try_end_3d} :catchall_3f

    monitor-exit v2

    goto :goto_42

    :catchall_3f
    move-exception v3

    monitor-exit v2

    throw v3

    :goto_42
    return-void
.end method

.method public zzcF()Lcom/google/android/gms/internal/zzbc;
    .registers 10

    iget-object v2, p0, Lcom/google/android/gms/internal/zzbd;->zzpV:Ljava/lang/Object;

    monitor-enter v2

    :try_start_3
    iget-object v0, p0, Lcom/google/android/gms/internal/zzbd;->zzsX:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_13

    const-string v0, "Queue empty"

    invoke-static {v0}, Lcom/google/android/gms/internal/zzin;->zzaI(Ljava/lang/String;)V
    :try_end_10
    .catchall {:try_start_3 .. :try_end_10} :catchall_51

    monitor-exit v2

    const/4 v0, 0x0

    return-object v0

    :cond_13
    :try_start_13
    iget-object v0, p0, Lcom/google/android/gms/internal/zzbd;->zzsX:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x2

    if-lt v0, v1, :cond_42

    const/4 v3, 0x0

    const/high16 v4, -0x80000000

    iget-object v0, p0, Lcom/google/android/gms/internal/zzbd;->zzsX:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_25
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3b

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/google/android/gms/internal/zzbc;

    invoke-virtual {v6}, Lcom/google/android/gms/internal/zzbc;->getScore()I

    move-result v7

    if-le v7, v4, :cond_3a

    move v4, v7

    move-object v3, v6

    :cond_3a
    goto :goto_25

    :cond_3b
    iget-object v0, p0, Lcom/google/android/gms/internal/zzbd;->zzsX:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->remove(Ljava/lang/Object;)Z
    :try_end_40
    .catchall {:try_start_13 .. :try_end_40} :catchall_51

    monitor-exit v2

    return-object v3

    :cond_42
    :try_start_42
    iget-object v0, p0, Lcom/google/android/gms/internal/zzbd;->zzsX:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/google/android/gms/internal/zzbc;

    invoke-virtual {v3}, Lcom/google/android/gms/internal/zzbc;->zzcA()V
    :try_end_4f
    .catchall {:try_start_42 .. :try_end_4f} :catchall_51

    monitor-exit v2

    return-object v3

    :catchall_51
    move-exception v8

    monitor-exit v2

    throw v8
.end method

.class public Lcom/itau/empresas/feature/credito/produtos/model/MultiLimiteVO$DetalheProdutoMultiLimiteVO;
.super Ljava/lang/Object;
.source "MultiLimiteVO.java"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/itau/empresas/feature/credito/produtos/model/MultiLimiteVO;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "DetalheProdutoMultiLimiteVO"
.end annotation


# instance fields
.field private descricaoProduto:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "descricao_produto"
    .end annotation
.end field

.field private limiteConcedido:Ljava/lang/Float;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "limite_concedido"
    .end annotation
.end field

.field private limiteContratado:Ljava/lang/Float;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "limite_contratado"
    .end annotation
.end field

.field private limiteDisponivel:Ljava/lang/Float;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "limite_disponivel"
    .end annotation
.end field

.field private limiteUtilizado:Ljava/lang/Float;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "limite_utilizado"
    .end annotation
.end field

.field private porcentagemGarantias:Ljava/lang/Float;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "porcentagem_garantias"
    .end annotation
.end field

.field private taxaCetAnual:Ljava/lang/Float;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "taxa_cet_anual"
    .end annotation
.end field

.field private taxaCetMensal:Ljava/lang/Float;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "taxa_cet_mensal"
    .end annotation
.end field

.field private taxaContrato:Ljava/lang/Float;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "taxa_contrato"
    .end annotation
.end field


# virtual methods
.method public getDescricaoProduto()Ljava/lang/String;
    .registers 2

    .line 107
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/model/MultiLimiteVO$DetalheProdutoMultiLimiteVO;->descricaoProduto:Ljava/lang/String;

    return-object v0
.end method

.method public getLimiteDisponivel()Ljava/lang/Float;
    .registers 2

    .line 112
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/model/MultiLimiteVO$DetalheProdutoMultiLimiteVO;->limiteDisponivel:Ljava/lang/Float;

    return-object v0
.end method

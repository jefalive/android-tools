.class public Lcom/itau/empresas/ui/dialog/PedagioWebViewDialog;
.super Lcom/itau/empresas/ui/dialog/BaseDialog;
.source "PedagioWebViewDialog.java"


# instance fields
.field adapter:Lcom/itau/empresas/adapter/ItemPedagioAdapter;

.field application:Lcom/itau/empresas/CustomApplication;

.field categoriaAnalytics:Ljava/lang/String;

.field chaves:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<Ljava/lang/String;>;"
        }
    .end annotation
.end field

.field labels:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<Ljava/lang/String;>;"
        }
    .end annotation
.end field

.field listItemPopup:Landroid/widget/ListView;


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 25
    invoke-direct {p0}, Lcom/itau/empresas/ui/dialog/BaseDialog;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/itau/empresas/ui/dialog/PedagioWebViewDialog;Ljava/lang/String;Ljava/lang/String;)V
    .registers 3
    .param p0, "x0"    # Lcom/itau/empresas/ui/dialog/PedagioWebViewDialog;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Ljava/lang/String;

    .line 25
    invoke-direct {p0, p1, p2}, Lcom/itau/empresas/ui/dialog/PedagioWebViewDialog;->inicializarWebView(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private inicializarWebView(Ljava/lang/String;Ljava/lang/String;)V
    .registers 4
    .param p1, "chave"    # Ljava/lang/String;
    .param p2, "label"    # Ljava/lang/String;

    .line 84
    invoke-virtual {p0}, Lcom/itau/empresas/ui/dialog/PedagioWebViewDialog;->dismiss()V

    .line 85
    invoke-virtual {p0}, Lcom/itau/empresas/ui/dialog/PedagioWebViewDialog;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0, p1, p2}, Lcom/itau/empresas/ui/activity/WebViewActivity;->iniciarWebViewActivity(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 86
    return-void
.end method


# virtual methods
.method aoInicializar()V
    .registers 7

    .line 51
    iget-object v0, p0, Lcom/itau/empresas/ui/dialog/PedagioWebViewDialog;->labels:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iget-object v1, p0, Lcom/itau/empresas/ui/dialog/PedagioWebViewDialog;->chaves:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ne v0, v1, :cond_16

    iget-object v0, p0, Lcom/itau/empresas/ui/dialog/PedagioWebViewDialog;->labels:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 52
    :cond_16
    invoke-virtual {p0}, Lcom/itau/empresas/ui/dialog/PedagioWebViewDialog;->dismiss()V

    .line 53
    return-void

    .line 55
    :cond_1a
    iget-object v0, p0, Lcom/itau/empresas/ui/dialog/PedagioWebViewDialog;->labels:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_42

    iget-object v0, p0, Lcom/itau/empresas/ui/dialog/PedagioWebViewDialog;->chaves:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_42

    .line 56
    iget-object v0, p0, Lcom/itau/empresas/ui/dialog/PedagioWebViewDialog;->chaves:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v1, p0, Lcom/itau/empresas/ui/dialog/PedagioWebViewDialog;->labels:Ljava/util/ArrayList;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lcom/itau/empresas/ui/dialog/PedagioWebViewDialog;->inicializarWebView(Ljava/lang/String;Ljava/lang/String;)V

    .line 57
    return-void

    .line 60
    :cond_42
    iget-object v0, p0, Lcom/itau/empresas/ui/dialog/PedagioWebViewDialog;->application:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getEventoTracker()Lcom/itau/empresas/ui/util/analytics/TrackerEvento;

    move-result-object v3

    .line 61
    .local v3, "tracker":Lcom/itau/empresas/ui/util/analytics/TrackerEvento;
    iget-object v0, p0, Lcom/itau/empresas/ui/dialog/PedagioWebViewDialog;->labels:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_4e
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6d

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Ljava/lang/String;

    .line 62
    .local v5, "l":Ljava/lang/String;
    new-instance v0, Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;

    iget-object v1, p0, Lcom/itau/empresas/ui/dialog/PedagioWebViewDialog;->categoriaAnalytics:Ljava/lang/String;

    .line 63
    const v2, 0x7f0702aa

    invoke-virtual {p0, v2}, Lcom/itau/empresas/ui/dialog/PedagioWebViewDialog;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2, v5}, Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 62
    invoke-virtual {v3, v0}, Lcom/itau/empresas/ui/util/analytics/TrackerEvento;->enviarHitEvento(Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;)V

    .line 64
    .end local v5    # "l":Ljava/lang/String;
    goto :goto_4e

    .line 66
    :cond_6d
    iget-object v0, p0, Lcom/itau/empresas/ui/dialog/PedagioWebViewDialog;->adapter:Lcom/itau/empresas/adapter/ItemPedagioAdapter;

    iget-object v1, p0, Lcom/itau/empresas/ui/dialog/PedagioWebViewDialog;->labels:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Lcom/itau/empresas/adapter/ItemPedagioAdapter;->inicializar(Ljava/util/List;)V

    .line 67
    iget-object v0, p0, Lcom/itau/empresas/ui/dialog/PedagioWebViewDialog;->listItemPopup:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/itau/empresas/ui/dialog/PedagioWebViewDialog;->adapter:Lcom/itau/empresas/adapter/ItemPedagioAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 68
    iget-object v0, p0, Lcom/itau/empresas/ui/dialog/PedagioWebViewDialog;->listItemPopup:Landroid/widget/ListView;

    new-instance v1, Lcom/itau/empresas/ui/dialog/PedagioWebViewDialog$1;

    invoke-direct {v1, p0, v3}, Lcom/itau/empresas/ui/dialog/PedagioWebViewDialog$1;-><init>(Lcom/itau/empresas/ui/dialog/PedagioWebViewDialog;Lcom/itau/empresas/ui/util/analytics/TrackerEvento;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 81
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 44
    invoke-super {p0, p1}, Lcom/itau/empresas/ui/dialog/BaseDialog;->onCreate(Landroid/os/Bundle;)V

    .line 45
    const/4 v0, 0x2

    const v1, 0x7f090144

    invoke-virtual {p0, v0, v1}, Lcom/itau/empresas/ui/dialog/PedagioWebViewDialog;->setStyle(II)V

    .line 46
    return-void
.end method

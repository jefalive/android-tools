.class abstract Lcom/itau/empresas/feature/recarga/contatos/adapter/ContatosRecargaAdapter$WidgetFilter;
.super Landroid/widget/Filter;
.source "ContatosRecargaAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/itau/empresas/feature/recarga/contatos/adapter/ContatosRecargaAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x40a
    name = "WidgetFilter"
.end annotation


# instance fields
.field private final listaFiltrada:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Ljava/util/List;)V
    .registers 2
    .param p1, "listaFiltrada"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;>;)V"
        }
    .end annotation

    .line 82
    invoke-direct {p0}, Landroid/widget/Filter;-><init>()V

    .line 83
    iput-object p1, p0, Lcom/itau/empresas/feature/recarga/contatos/adapter/ContatosRecargaAdapter$WidgetFilter;->listaFiltrada:Ljava/util/List;

    .line 84
    return-void
.end method


# virtual methods
.method protected performFiltering(Ljava/lang/CharSequence;)Landroid/widget/Filter$FilterResults;
    .registers 10
    .param p1, "busca"    # Ljava/lang/CharSequence;

    .line 88
    new-instance v2, Landroid/widget/Filter$FilterResults;

    invoke-direct {v2}, Landroid/widget/Filter$FilterResults;-><init>()V

    .line 90
    .local v2, "resultado":Landroid/widget/Filter$FilterResults;
    iget-object v3, p0, Lcom/itau/empresas/feature/recarga/contatos/adapter/ContatosRecargaAdapter$WidgetFilter;->listaFiltrada:Ljava/util/List;

    .line 91
    .local v3, "novaLista":Ljava/util/List;, "Ljava/util/List<Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;>;"
    if-eqz p1, :cond_f

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-nez v0, :cond_19

    .line 92
    :cond_f
    iput-object v3, v2, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    .line 93
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    iput v0, v2, Landroid/widget/Filter$FilterResults;->count:I

    goto/16 :goto_84

    .line 95
    :cond_19
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 96
    .local v4, "listaContatosFiltrada":Ljava/util/List;, "Ljava/util/List<Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;>;"
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/adapter/ContatosRecargaAdapter$WidgetFilter;->listaFiltrada:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_24
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7c

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;

    .line 97
    .local v6, "c":Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6}, Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;->getDdd()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v6}, Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;->getTelefone()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 98
    .local v7, "numero":Ljava/lang/String;
    invoke-virtual {v6}, Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;->getApelido()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_78

    .line 99
    invoke-virtual {v7}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_7b

    .line 100
    :cond_78
    invoke-interface {v4, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 102
    .end local v6    # "c":Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;
    .end local v7    # "numero":Ljava/lang/String;
    :cond_7b
    goto :goto_24

    .line 103
    :cond_7c
    iput-object v4, v2, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    .line 104
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    iput v0, v2, Landroid/widget/Filter$FilterResults;->count:I

    .line 107
    .end local v4    # "listaContatosFiltrada":Ljava/util/List;, "Ljava/util/List<Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;>;"
    .end local v4
    :goto_84
    return-object v2
.end method

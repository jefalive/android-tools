.class public Lcom/itau/empresas/ui/dialog/FiltroComprovantesDialog;
.super Lcom/itau/empresas/ui/dialog/BaseDialog;
.source "FiltroComprovantesDialog.java"


# instance fields
.field botaoConfirmarFiltrosComprovantes:Landroid/widget/Button;

.field private confirmarListener:Landroid/view/View$OnClickListener;

.field dias:[Ljava/lang/String;

.field escolhePeriodo:Lbr/com/itau/library/LinearValuePickerView;

.field selecionado:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 21
    invoke-direct {p0}, Lcom/itau/empresas/ui/dialog/BaseDialog;-><init>()V

    return-void
.end method


# virtual methods
.method public configuraViews()V
    .registers 3

    .line 51
    iget-object v0, p0, Lcom/itau/empresas/ui/dialog/FiltroComprovantesDialog;->escolhePeriodo:Lbr/com/itau/library/LinearValuePickerView;

    iget-object v1, p0, Lcom/itau/empresas/ui/dialog/FiltroComprovantesDialog;->dias:[Ljava/lang/String;

    invoke-virtual {v0, v1}, Lbr/com/itau/library/LinearValuePickerView;->setValues([Ljava/lang/String;)V

    .line 52
    iget-object v0, p0, Lcom/itau/empresas/ui/dialog/FiltroComprovantesDialog;->selecionado:Ljava/lang/Integer;

    if-eqz v0, :cond_16

    .line 53
    iget-object v0, p0, Lcom/itau/empresas/ui/dialog/FiltroComprovantesDialog;->escolhePeriodo:Lbr/com/itau/library/LinearValuePickerView;

    iget-object v1, p0, Lcom/itau/empresas/ui/dialog/FiltroComprovantesDialog;->selecionado:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lbr/com/itau/library/LinearValuePickerView;->setSelectedPosition(I)V

    .line 56
    :cond_16
    iget-object v0, p0, Lcom/itau/empresas/ui/dialog/FiltroComprovantesDialog;->confirmarListener:Landroid/view/View$OnClickListener;

    if-eqz v0, :cond_21

    .line 57
    iget-object v0, p0, Lcom/itau/empresas/ui/dialog/FiltroComprovantesDialog;->botaoConfirmarFiltrosComprovantes:Landroid/widget/Button;

    iget-object v1, p0, Lcom/itau/empresas/ui/dialog/FiltroComprovantesDialog;->confirmarListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 59
    :cond_21
    return-void
.end method

.method public getDia(I)I
    .registers 3
    .param p1, "index"    # I

    .line 73
    iget-object v0, p0, Lcom/itau/empresas/ui/dialog/FiltroComprovantesDialog;->dias:[Ljava/lang/String;

    aget-object v0, v0, p1

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public getEscolhePeriodo()Lbr/com/itau/library/LinearValuePickerView;
    .registers 2

    .line 69
    iget-object v0, p0, Lcom/itau/empresas/ui/dialog/FiltroComprovantesDialog;->escolhePeriodo:Lbr/com/itau/library/LinearValuePickerView;

    return-object v0
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .registers 6
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 39
    invoke-super {p0, p1}, Lcom/itau/empresas/ui/dialog/BaseDialog;->onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;

    move-result-object v3

    .line 41
    .local v3, "dialog":Landroid/app/Dialog;
    invoke-virtual {v3}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/Window;->requestFeature(I)Z

    .line 42
    invoke-virtual {v3}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    .line 43
    invoke-virtual {v0, v1}, Landroid/view/Window;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 44
    const/4 v0, 0x0

    invoke-virtual {v3, v0}, Landroid/app/Dialog;->setCancelable(Z)V

    .line 45
    return-object v3
.end method

.method public setConfirmarListener(Landroid/view/View$OnClickListener;)V
    .registers 3
    .param p1, "confirmarListener"    # Landroid/view/View$OnClickListener;

    .line 62
    iput-object p1, p0, Lcom/itau/empresas/ui/dialog/FiltroComprovantesDialog;->confirmarListener:Landroid/view/View$OnClickListener;

    .line 63
    iget-object v0, p0, Lcom/itau/empresas/ui/dialog/FiltroComprovantesDialog;->botaoConfirmarFiltrosComprovantes:Landroid/widget/Button;

    if-eqz v0, :cond_d

    if-eqz p1, :cond_d

    .line 64
    iget-object v0, p0, Lcom/itau/empresas/ui/dialog/FiltroComprovantesDialog;->botaoConfirmarFiltrosComprovantes:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 66
    :cond_d
    return-void
.end method

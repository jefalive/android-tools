.class final Lcom/adobe/mobile/AnalyticsTrackTimedAction;
.super Lcom/adobe/mobile/AbstractDatabaseBacking;
.source "AnalyticsTrackTimedAction.java"


# static fields
.field private static _instance:Lcom/adobe/mobile/AnalyticsTrackTimedAction;

.field private static final _instanceMutex:Ljava/lang/Object;


# instance fields
.field private sqlDeleteAction:Landroid/database/sqlite/SQLiteStatement;

.field private sqlDeleteContextDataForAction:Landroid/database/sqlite/SQLiteStatement;

.field private sqlExistsAction:Ljava/lang/String;

.field private sqlExistsContextDataByActionAndKey:Ljava/lang/String;

.field private sqlInsertAction:Landroid/database/sqlite/SQLiteStatement;

.field private sqlInsertContextData:Landroid/database/sqlite/SQLiteStatement;

.field private sqlSelectAction:Ljava/lang/String;

.field private sqlSelectContextDataForAction:Ljava/lang/String;

.field private sqlUpdateAction:Landroid/database/sqlite/SQLiteStatement;

.field private sqlUpdateActionsClearAdjustedTime:Landroid/database/sqlite/SQLiteStatement;

.field private sqlUpdateContextData:Landroid/database/sqlite/SQLiteStatement;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .line 52
    const/4 v0, 0x0

    sput-object v0, Lcom/adobe/mobile/AnalyticsTrackTimedAction;->_instance:Lcom/adobe/mobile/AnalyticsTrackTimedAction;

    .line 53
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/adobe/mobile/AnalyticsTrackTimedAction;->_instanceMutex:Ljava/lang/Object;

    return-void
.end method

.method private constructor <init>()V
    .registers 4

    .line 64
    invoke-direct {p0}, Lcom/adobe/mobile/AbstractDatabaseBacking;-><init>()V

    .line 65
    const-string v0, "ADBMobileTimedActionsCache.sqlite"

    iput-object v0, p0, Lcom/adobe/mobile/AnalyticsTrackTimedAction;->fileName:Ljava/lang/String;

    .line 66
    const-string v0, "Analytics"

    iput-object v0, p0, Lcom/adobe/mobile/AnalyticsTrackTimedAction;->logPrefix:Ljava/lang/String;

    .line 67
    new-instance v0, Ljava/io/File;

    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getCacheDirectory()Ljava/io/File;

    move-result-object v1

    iget-object v2, p0, Lcom/adobe/mobile/AnalyticsTrackTimedAction;->fileName:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/adobe/mobile/AnalyticsTrackTimedAction;->initDatabaseBacking(Ljava/io/File;)V

    .line 68
    return-void
.end method

.method public static sharedInstance()Lcom/adobe/mobile/AnalyticsTrackTimedAction;
    .registers 3

    .line 55
    sget-object v1, Lcom/adobe/mobile/AnalyticsTrackTimedAction;->_instanceMutex:Ljava/lang/Object;

    monitor-enter v1

    .line 56
    :try_start_3
    sget-object v0, Lcom/adobe/mobile/AnalyticsTrackTimedAction;->_instance:Lcom/adobe/mobile/AnalyticsTrackTimedAction;

    if-nez v0, :cond_e

    .line 57
    new-instance v0, Lcom/adobe/mobile/AnalyticsTrackTimedAction;

    invoke-direct {v0}, Lcom/adobe/mobile/AnalyticsTrackTimedAction;-><init>()V

    sput-object v0, Lcom/adobe/mobile/AnalyticsTrackTimedAction;->_instance:Lcom/adobe/mobile/AnalyticsTrackTimedAction;

    .line 60
    :cond_e
    sget-object v0, Lcom/adobe/mobile/AnalyticsTrackTimedAction;->_instance:Lcom/adobe/mobile/AnalyticsTrackTimedAction;
    :try_end_10
    .catchall {:try_start_3 .. :try_end_10} :catchall_12

    monitor-exit v1

    return-object v0

    .line 61
    :catchall_12
    move-exception v2

    monitor-exit v1

    throw v2
.end method


# virtual methods
.method protected initializeDatabase()V
    .registers 6

    .line 281
    :try_start_0
    iget-object v0, p0, Lcom/adobe/mobile/AnalyticsTrackTimedAction;->database:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "CREATE TABLE IF NOT EXISTS TIMEDACTIONS (ID INTEGER PRIMARY KEY AUTOINCREMENT, NAME TEXT, STARTTIME INTEGER, ADJSTARTTIME INTEGER)"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 282
    iget-object v0, p0, Lcom/adobe/mobile/AnalyticsTrackTimedAction;->database:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "CREATE TABLE IF NOT EXISTS CONTEXTDATA (ID INTEGER PRIMARY KEY AUTOINCREMENT, ACTIONID INTEGER, KEY TEXT, VALUE TEXT, FOREIGN KEY(ACTIONID) REFERENCES TIMEDACTIONS(ID))"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V
    :try_end_e
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_e} :catch_f
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_e} :catch_25

    .line 289
    goto :goto_3a

    .line 284
    :catch_f
    move-exception v4

    .line 285
    .local v4, "e":Landroid/database/SQLException;
    const-string v0, "%s - Unable to open or create timed actions database (%s)"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/adobe/mobile/AnalyticsTrackTimedAction;->logPrefix:Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-virtual {v4}, Landroid/database/SQLException;->getMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logErrorFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 289
    .end local v4    # "e":Landroid/database/SQLException;
    goto :goto_3a

    .line 287
    :catch_25
    move-exception v4

    .line 288
    .local v4, "e":Ljava/lang/Exception;
    const-string v0, "%s - Uknown error creating timed actions database (%s)"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/adobe/mobile/AnalyticsTrackTimedAction;->logPrefix:Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-virtual {v4}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logErrorFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 290
    .end local v4    # "e":Ljava/lang/Exception;
    :goto_3a
    return-void
.end method

.method protected postMigrate()V
    .registers 1

    .line 273
    return-void
.end method

.method protected postReset()V
    .registers 1

    .line 276
    return-void
.end method

.method protected preMigrate()V
    .registers 8

    .line 257
    new-instance v4, Ljava/io/File;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getCacheDirectory()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "ADBMobileDataCache.sqlite"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/adobe/mobile/AnalyticsTrackTimedAction;->fileName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 258
    .local v4, "oldFile":Ljava/io/File;
    new-instance v5, Ljava/io/File;

    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getCacheDirectory()Ljava/io/File;

    move-result-object v0

    iget-object v1, p0, Lcom/adobe/mobile/AnalyticsTrackTimedAction;->fileName:Ljava/lang/String;

    invoke-direct {v5, v0, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 261
    .local v5, "newFile":Ljava/io/File;
    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_62

    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_62

    .line 263
    :try_start_39
    invoke-virtual {v4, v5}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    move-result v0

    if-nez v0, :cond_4c

    .line 264
    const-string v0, "%s - Unable to migrate old Timed Actions db, creating new Timed Actions db (move file returned false)"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/adobe/mobile/AnalyticsTrackTimedAction;->logPrefix:Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logWarningFormat(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_4c
    .catch Ljava/lang/Exception; {:try_start_39 .. :try_end_4c} :catch_4d

    .line 268
    :cond_4c
    goto :goto_62

    .line 266
    :catch_4d
    move-exception v6

    .line 267
    .local v6, "e":Ljava/lang/Exception;
    const-string v0, "%s - Unable to migrate old Timed Actions db, creating new Timed Actions db (%s)"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/adobe/mobile/AnalyticsTrackTimedAction;->logPrefix:Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-virtual {v6}, Ljava/lang/Exception;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logWarningFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 270
    .end local v6    # "e":Ljava/lang/Exception;
    :cond_62
    :goto_62
    return-void
.end method

.method protected prepareStatements()V
    .registers 6

    .line 295
    const-string v0, "SELECT ID, STARTTIME, ADJSTARTTIME FROM TIMEDACTIONS WHERE NAME=?"

    iput-object v0, p0, Lcom/adobe/mobile/AnalyticsTrackTimedAction;->sqlSelectAction:Ljava/lang/String;

    .line 296
    const-string v0, "SELECT COUNT(*) FROM TIMEDACTIONS WHERE NAME=?"

    iput-object v0, p0, Lcom/adobe/mobile/AnalyticsTrackTimedAction;->sqlExistsAction:Ljava/lang/String;

    .line 297
    const-string v0, "SELECT KEY, VALUE FROM CONTEXTDATA WHERE ACTIONID=?"

    iput-object v0, p0, Lcom/adobe/mobile/AnalyticsTrackTimedAction;->sqlSelectContextDataForAction:Ljava/lang/String;

    .line 298
    const-string v0, "SELECT COUNT(*) FROM CONTEXTDATA WHERE ACTIONID=? AND KEY=?"

    iput-object v0, p0, Lcom/adobe/mobile/AnalyticsTrackTimedAction;->sqlExistsContextDataByActionAndKey:Ljava/lang/String;

    .line 303
    :try_start_10
    iget-object v0, p0, Lcom/adobe/mobile/AnalyticsTrackTimedAction;->database:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "INSERT INTO TIMEDACTIONS (NAME, STARTTIME, ADJSTARTTIME) VALUES (@NAME, @START, @START)"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v0

    iput-object v0, p0, Lcom/adobe/mobile/AnalyticsTrackTimedAction;->sqlInsertAction:Landroid/database/sqlite/SQLiteStatement;

    .line 304
    iget-object v0, p0, Lcom/adobe/mobile/AnalyticsTrackTimedAction;->database:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "UPDATE TIMEDACTIONS SET ADJSTARTTIME=ADJSTARTTIME+@DELTA WHERE ADJSTARTTIME!=0"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v0

    iput-object v0, p0, Lcom/adobe/mobile/AnalyticsTrackTimedAction;->sqlUpdateAction:Landroid/database/sqlite/SQLiteStatement;

    .line 305
    iget-object v0, p0, Lcom/adobe/mobile/AnalyticsTrackTimedAction;->database:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "UPDATE TIMEDACTIONS SET ADJSTARTTIME=0"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v0

    iput-object v0, p0, Lcom/adobe/mobile/AnalyticsTrackTimedAction;->sqlUpdateActionsClearAdjustedTime:Landroid/database/sqlite/SQLiteStatement;

    .line 306
    iget-object v0, p0, Lcom/adobe/mobile/AnalyticsTrackTimedAction;->database:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "DELETE FROM TIMEDACTIONS WHERE ID=@ID"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v0

    iput-object v0, p0, Lcom/adobe/mobile/AnalyticsTrackTimedAction;->sqlDeleteAction:Landroid/database/sqlite/SQLiteStatement;

    .line 307
    iget-object v0, p0, Lcom/adobe/mobile/AnalyticsTrackTimedAction;->database:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "INSERT INTO CONTEXTDATA(ACTIONID, KEY, VALUE) VALUES (@ACTIONID, @KEY, @VALUE)"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v0

    iput-object v0, p0, Lcom/adobe/mobile/AnalyticsTrackTimedAction;->sqlInsertContextData:Landroid/database/sqlite/SQLiteStatement;

    .line 308
    iget-object v0, p0, Lcom/adobe/mobile/AnalyticsTrackTimedAction;->database:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "UPDATE CONTEXTDATA SET VALUE=@VALUE WHERE ACTIONID=@ACTIONID AND KEY=@KEY"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v0

    iput-object v0, p0, Lcom/adobe/mobile/AnalyticsTrackTimedAction;->sqlUpdateContextData:Landroid/database/sqlite/SQLiteStatement;

    .line 309
    iget-object v0, p0, Lcom/adobe/mobile/AnalyticsTrackTimedAction;->database:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "DELETE FROM CONTEXTDATA WHERE ACTIONID=@ACTIONID"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v0

    iput-object v0, p0, Lcom/adobe/mobile/AnalyticsTrackTimedAction;->sqlDeleteContextDataForAction:Landroid/database/sqlite/SQLiteStatement;
    :try_end_56
    .catch Landroid/database/SQLException; {:try_start_10 .. :try_end_56} :catch_57
    .catch Ljava/lang/Exception; {:try_start_10 .. :try_end_56} :catch_68

    .line 316
    goto :goto_78

    .line 311
    :catch_57
    move-exception v4

    .line 312
    .local v4, "e":Landroid/database/SQLException;
    const-string v0, "Analytics - unable to prepare the needed SQL statements for interacting with the timed actions database (%s)"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v4}, Landroid/database/SQLException;->getMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logErrorFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 316
    .end local v4    # "e":Landroid/database/SQLException;
    goto :goto_78

    .line 314
    :catch_68
    move-exception v4

    .line 315
    .local v4, "e":Ljava/lang/Exception;
    const-string v0, "Analytics - Unknown error preparing sql statements (%s)"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v4}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logErrorFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 317
    .end local v4    # "e":Ljava/lang/Exception;
    :goto_78
    return-void
.end method

.method protected trackTimedActionUpdateActionsClearAdjustedStartTime()V
    .registers 8

    .line 191
    iget-object v4, p0, Lcom/adobe/mobile/AnalyticsTrackTimedAction;->dbMutex:Ljava/lang/Object;

    monitor-enter v4

    .line 193
    :try_start_3
    iget-object v0, p0, Lcom/adobe/mobile/AnalyticsTrackTimedAction;->sqlUpdateActionsClearAdjustedTime:Landroid/database/sqlite/SQLiteStatement;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteStatement;->execute()V

    .line 194
    iget-object v0, p0, Lcom/adobe/mobile/AnalyticsTrackTimedAction;->sqlUpdateActionsClearAdjustedTime:Landroid/database/sqlite/SQLiteStatement;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteStatement;->clearBindings()V
    :try_end_d
    .catch Landroid/database/SQLException; {:try_start_3 .. :try_end_d} :catch_e
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_d} :catch_27
    .catchall {:try_start_3 .. :try_end_d} :catchall_41

    .line 207
    goto :goto_3f

    .line 196
    :catch_e
    move-exception v5

    .line 197
    .local v5, "e":Landroid/database/SQLException;
    const-string v0, "%s - Unable to update adjusted time for timed actions after crash (%s)"

    const/4 v1, 0x2

    :try_start_12
    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/adobe/mobile/AnalyticsTrackTimedAction;->logPrefix:Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-virtual {v5}, Landroid/database/SQLException;->getMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logErrorFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 200
    invoke-virtual {p0, v5}, Lcom/adobe/mobile/AnalyticsTrackTimedAction;->resetDatabase(Ljava/lang/Exception;)V

    .line 207
    .end local v5    # "e":Landroid/database/SQLException;
    goto :goto_3f

    .line 202
    :catch_27
    move-exception v5

    .line 203
    .local v5, "e":Ljava/lang/Exception;
    const-string v0, "%s - Unknown error clearing adjusted start times for timed actions (%s)"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/adobe/mobile/AnalyticsTrackTimedAction;->logPrefix:Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-virtual {v5}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logErrorFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 206
    invoke-virtual {p0, v5}, Lcom/adobe/mobile/AnalyticsTrackTimedAction;->resetDatabase(Ljava/lang/Exception;)V
    :try_end_3f
    .catchall {:try_start_12 .. :try_end_3f} :catchall_41

    .line 208
    .end local v5    # "e":Ljava/lang/Exception;
    :goto_3f
    monitor-exit v4

    goto :goto_44

    :catchall_41
    move-exception v6

    monitor-exit v4

    throw v6

    .line 209
    :goto_44
    return-void
.end method

.method protected trackTimedActionUpdateAdjustedStartTime(J)V
    .registers 10
    .param p1, "timeDelta"    # J

    .line 172
    iget-object v4, p0, Lcom/adobe/mobile/AnalyticsTrackTimedAction;->dbMutex:Ljava/lang/Object;

    monitor-enter v4

    .line 174
    :try_start_3
    iget-object v0, p0, Lcom/adobe/mobile/AnalyticsTrackTimedAction;->sqlUpdateAction:Landroid/database/sqlite/SQLiteStatement;

    const/4 v1, 0x1

    invoke-virtual {v0, v1, p1, p2}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 175
    iget-object v0, p0, Lcom/adobe/mobile/AnalyticsTrackTimedAction;->sqlUpdateAction:Landroid/database/sqlite/SQLiteStatement;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteStatement;->execute()V

    .line 176
    iget-object v0, p0, Lcom/adobe/mobile/AnalyticsTrackTimedAction;->sqlUpdateAction:Landroid/database/sqlite/SQLiteStatement;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteStatement;->clearBindings()V
    :try_end_13
    .catch Landroid/database/SQLException; {:try_start_3 .. :try_end_13} :catch_14
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_13} :catch_2d
    .catchall {:try_start_3 .. :try_end_13} :catchall_44

    .line 186
    goto :goto_42

    .line 178
    :catch_14
    move-exception v5

    .line 179
    .local v5, "e":Landroid/database/SQLException;
    const-string v0, "%s - Unable to bind prepared statement values for updating the adjusted start time for timed action (%s)"

    const/4 v1, 0x2

    :try_start_18
    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/adobe/mobile/AnalyticsTrackTimedAction;->logPrefix:Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-virtual {v5}, Landroid/database/SQLException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logErrorFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 182
    invoke-virtual {p0, v5}, Lcom/adobe/mobile/AnalyticsTrackTimedAction;->resetDatabase(Ljava/lang/Exception;)V

    .line 186
    .end local v5    # "e":Landroid/database/SQLException;
    goto :goto_42

    .line 184
    :catch_2d
    move-exception v5

    .line 185
    .local v5, "e":Ljava/lang/Exception;
    const-string v0, "%s - Unable to adjust start times for timed actions (%s)"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/adobe/mobile/AnalyticsTrackTimedAction;->logPrefix:Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-virtual {v5}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logErrorFormat(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_42
    .catchall {:try_start_18 .. :try_end_42} :catchall_44

    .line 187
    .end local v5    # "e":Ljava/lang/Exception;
    :goto_42
    monitor-exit v4

    goto :goto_47

    :catchall_44
    move-exception v6

    monitor-exit v4

    throw v6

    .line 188
    :goto_47
    return-void
.end method

.class public Lbr/com/itau/widgets/graphicalfriedcake/charts/components/LimitLine;
.super Ljava/lang/Object;
.source "LimitLine.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lbr/com/itau/widgets/graphicalfriedcake/charts/components/LimitLine$LimitLabelPosition;
    }
.end annotation


# instance fields
.field private mDashPathEffect:Landroid/graphics/DashPathEffect;

.field private mLabel:Ljava/lang/String;

.field private mLabelPosition:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/LimitLine$LimitLabelPosition;

.field private mLimit:F

.field private mLineColor:I

.field private mLineWidth:F

.field private mTextSize:F

.field private mTextStyle:Landroid/graphics/Paint$Style;

.field private mValueTextColor:I


# direct methods
.method public constructor <init>(F)V
    .registers 5
    .param p1, "limit"    # F

    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    const/4 v0, 0x0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/LimitLine;->mLimit:F

    .line 25
    const/high16 v0, 0x40000000    # 2.0f

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/LimitLine;->mLineWidth:F

    .line 29
    const/16 v0, 0xed

    const/16 v1, 0x5b

    const/16 v2, 0x5b

    invoke-static {v0, v1, v2}, Landroid/graphics/Color;->rgb(III)I

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/LimitLine;->mLineColor:I

    .line 33
    const/high16 v0, -0x1000000

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/LimitLine;->mValueTextColor:I

    .line 37
    const/high16 v0, 0x41500000    # 13.0f

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/LimitLine;->mTextSize:F

    .line 41
    sget-object v0, Landroid/graphics/Paint$Style;->FILL_AND_STROKE:Landroid/graphics/Paint$Style;

    iput-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/LimitLine;->mTextStyle:Landroid/graphics/Paint$Style;

    .line 45
    const-string v0, ""

    iput-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/LimitLine;->mLabel:Ljava/lang/String;

    .line 49
    const/4 v0, 0x0

    iput-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/LimitLine;->mDashPathEffect:Landroid/graphics/DashPathEffect;

    .line 53
    sget-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/LimitLine$LimitLabelPosition;->POS_RIGHT:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/LimitLine$LimitLabelPosition;

    iput-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/LimitLine;->mLabelPosition:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/LimitLine$LimitLabelPosition;

    .line 69
    iput p1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/LimitLine;->mLimit:F

    .line 70
    return-void
.end method

.method public constructor <init>(FLjava/lang/String;)V
    .registers 6
    .param p1, "limit"    # F
    .param p2, "label"    # Ljava/lang/String;

    .line 80
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    const/4 v0, 0x0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/LimitLine;->mLimit:F

    .line 25
    const/high16 v0, 0x40000000    # 2.0f

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/LimitLine;->mLineWidth:F

    .line 29
    const/16 v0, 0xed

    const/16 v1, 0x5b

    const/16 v2, 0x5b

    invoke-static {v0, v1, v2}, Landroid/graphics/Color;->rgb(III)I

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/LimitLine;->mLineColor:I

    .line 33
    const/high16 v0, -0x1000000

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/LimitLine;->mValueTextColor:I

    .line 37
    const/high16 v0, 0x41500000    # 13.0f

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/LimitLine;->mTextSize:F

    .line 41
    sget-object v0, Landroid/graphics/Paint$Style;->FILL_AND_STROKE:Landroid/graphics/Paint$Style;

    iput-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/LimitLine;->mTextStyle:Landroid/graphics/Paint$Style;

    .line 45
    const-string v0, ""

    iput-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/LimitLine;->mLabel:Ljava/lang/String;

    .line 49
    const/4 v0, 0x0

    iput-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/LimitLine;->mDashPathEffect:Landroid/graphics/DashPathEffect;

    .line 53
    sget-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/LimitLine$LimitLabelPosition;->POS_RIGHT:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/LimitLine$LimitLabelPosition;

    iput-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/LimitLine;->mLabelPosition:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/LimitLine$LimitLabelPosition;

    .line 81
    iput p1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/LimitLine;->mLimit:F

    .line 82
    iput-object p2, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/LimitLine;->mLabel:Ljava/lang/String;

    .line 83
    return-void
.end method


# virtual methods
.method public disableDashedLine()V
    .registers 2

    .line 162
    const/4 v0, 0x0

    iput-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/LimitLine;->mDashPathEffect:Landroid/graphics/DashPathEffect;

    .line 163
    return-void
.end method

.method public enableDashedLine(FFF)V
    .registers 7
    .param p1, "lineLength"    # F
    .param p2, "spaceLength"    # F
    .param p3, "phase"    # F

    .line 152
    new-instance v0, Landroid/graphics/DashPathEffect;

    const/4 v1, 0x2

    new-array v1, v1, [F

    const/4 v2, 0x0

    aput p1, v1, v2

    const/4 v2, 0x1

    aput p2, v1, v2

    invoke-direct {v0, v1, p3}, Landroid/graphics/DashPathEffect;-><init>([FF)V

    iput-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/LimitLine;->mDashPathEffect:Landroid/graphics/DashPathEffect;

    .line 155
    return-void
.end method

.method public getDashPathEffect()Landroid/graphics/DashPathEffect;
    .registers 2

    .line 183
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/LimitLine;->mDashPathEffect:Landroid/graphics/DashPathEffect;

    return-object v0
.end method

.method public getLabel()Ljava/lang/String;
    .registers 2

    .line 266
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/LimitLine;->mLabel:Ljava/lang/String;

    return-object v0
.end method

.method public getLabelPosition()Lbr/com/itau/widgets/graphicalfriedcake/charts/components/LimitLine$LimitLabelPosition;
    .registers 2

    .line 245
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/LimitLine;->mLabelPosition:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/LimitLine$LimitLabelPosition;

    return-object v0
.end method

.method public getLimit()F
    .registers 2

    .line 92
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/LimitLine;->mLimit:F

    return v0
.end method

.method public getLineColor()I
    .registers 2

    .line 140
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/LimitLine;->mLineColor:I

    return v0
.end method

.method public getLineWidth()F
    .registers 2

    .line 119
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/LimitLine;->mLineWidth:F

    return v0
.end method

.method public getTextColor()I
    .registers 2

    .line 203
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/LimitLine;->mValueTextColor:I

    return v0
.end method

.method public getTextSize()F
    .registers 2

    .line 286
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/LimitLine;->mTextSize:F

    return v0
.end method

.method public getTextStyle()Landroid/graphics/Paint$Style;
    .registers 2

    .line 224
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/LimitLine;->mTextStyle:Landroid/graphics/Paint$Style;

    return-object v0
.end method

.method public isDashedLineEnabled()Z
    .registers 2

    .line 173
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/LimitLine;->mDashPathEffect:Landroid/graphics/DashPathEffect;

    if-nez v0, :cond_6

    const/4 v0, 0x0

    goto :goto_7

    :cond_6
    const/4 v0, 0x1

    :goto_7
    return v0
.end method

.method public setLabel(Ljava/lang/String;)V
    .registers 2
    .param p1, "label"    # Ljava/lang/String;

    .line 256
    iput-object p1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/LimitLine;->mLabel:Ljava/lang/String;

    .line 257
    return-void
.end method

.method public setLabelPosition(Lbr/com/itau/widgets/graphicalfriedcake/charts/components/LimitLine$LimitLabelPosition;)V
    .registers 2
    .param p1, "pos"    # Lbr/com/itau/widgets/graphicalfriedcake/charts/components/LimitLine$LimitLabelPosition;

    .line 235
    iput-object p1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/LimitLine;->mLabelPosition:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/LimitLine$LimitLabelPosition;

    .line 236
    return-void
.end method

.method public setLineColor(I)V
    .registers 2
    .param p1, "color"    # I

    .line 130
    iput p1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/LimitLine;->mLineColor:I

    .line 131
    return-void
.end method

.method public setLineWidth(F)V
    .registers 3
    .param p1, "width"    # F

    .line 105
    const v0, 0x3e4ccccd    # 0.2f

    cmpg-float v0, p1, v0

    if-gez v0, :cond_a

    .line 106
    const p1, 0x3e4ccccd    # 0.2f

    .line 107
    :cond_a
    const/high16 v0, 0x41400000    # 12.0f

    cmpl-float v0, p1, v0

    if-lez v0, :cond_12

    .line 108
    const/high16 p1, 0x41400000    # 12.0f

    .line 109
    :cond_12
    invoke-static {p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Utils;->convertDpToPixel(F)F

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/LimitLine;->mLineWidth:F

    .line 110
    return-void
.end method

.method public setTextColor(I)V
    .registers 2
    .param p1, "color"    # I

    .line 193
    iput p1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/LimitLine;->mValueTextColor:I

    .line 194
    return-void
.end method

.method public setTextSize(F)V
    .registers 3
    .param p1, "size"    # F

    .line 276
    invoke-static {p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Utils;->convertDpToPixel(F)F

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/LimitLine;->mTextSize:F

    .line 277
    return-void
.end method

.method public setTextStyle(Landroid/graphics/Paint$Style;)V
    .registers 2
    .param p1, "style"    # Landroid/graphics/Paint$Style;

    .line 214
    iput-object p1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/LimitLine;->mTextStyle:Landroid/graphics/Paint$Style;

    .line 215
    return-void
.end method

.class public final Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesDetalheActivity_;
.super Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesDetalheActivity;
.source "ComprovantesDetalheActivity_.java"

# interfaces
.implements Lorg/androidannotations/api/view/HasViews;
.implements Lorg/androidannotations/api/view/OnViewChangedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesDetalheActivity_$IntentBuilder_;
    }
.end annotation


# instance fields
.field private final onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;


# direct methods
.method public constructor <init>()V
    .registers 2

    .line 38
    invoke-direct {p0}, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesDetalheActivity;-><init>()V

    .line 42
    new-instance v0, Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-direct {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesDetalheActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    return-void
.end method

.method static synthetic access$001(Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesDetalheActivity_;)V
    .registers 1
    .param p0, "x0"    # Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesDetalheActivity_;

    .line 38
    invoke-super {p0}, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesDetalheActivity;->mostraDialogoDeProgresso()V

    return-void
.end method

.method static synthetic access$101(Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesDetalheActivity_;)V
    .registers 1
    .param p0, "x0"    # Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesDetalheActivity_;

    .line 38
    invoke-super {p0}, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesDetalheActivity;->escondeDialogoDeProgresso()V

    return-void
.end method

.method private init_(Landroid/os/Bundle;)V
    .registers 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 55
    new-instance v0, Lcom/itau/empresas/Preferences_;

    invoke-direct {v0, p0}, Lcom/itau/empresas/Preferences_;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesDetalheActivity_;->prefs:Lcom/itau/empresas/Preferences_;

    .line 56
    invoke-static {p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->registerOnViewChangedListener(Lorg/androidannotations/api/view/OnViewChangedListener;)V

    .line 57
    invoke-static {}, Lcom/itau/empresas/CustomApplication_;->getInstance()Lcom/itau/empresas/CustomApplication;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesDetalheActivity_;->application:Lcom/itau/empresas/CustomApplication;

    .line 58
    invoke-direct {p0}, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesDetalheActivity_;->injectExtras_()V

    .line 59
    invoke-virtual {p0}, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesDetalheActivity_;->afterInject()V

    .line 60
    return-void
.end method

.method private injectExtras_()V
    .registers 3

    .line 131
    invoke-virtual {p0}, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesDetalheActivity_;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 132
    .local v1, "extras_":Landroid/os/Bundle;
    if-eqz v1, :cond_1c

    .line 133
    const-string v0, "comprovantesVO"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 134
    const-string v0, "comprovantesVO"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/feature/comprovantes/model/ComprovantesVO;

    iput-object v0, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesDetalheActivity_;->comprovantesVO:Lcom/itau/empresas/feature/comprovantes/model/ComprovantesVO;

    .line 137
    :cond_1c
    return-void
.end method

.method public static intent(Landroid/content/Context;)Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesDetalheActivity_$IntentBuilder_;
    .registers 2
    .param p0, "context"    # Landroid/content/Context;

    .line 81
    new-instance v0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesDetalheActivity_$IntentBuilder_;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesDetalheActivity_$IntentBuilder_;-><init>(Landroid/content/Context;)V

    return-object v0
.end method


# virtual methods
.method public escondeDialogoDeProgresso()V
    .registers 5

    .line 162
    const-string v0, ""

    new-instance v1, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesDetalheActivity_$4;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesDetalheActivity_$4;-><init>(Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesDetalheActivity_;)V

    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3}, Lorg/androidannotations/api/UiThreadExecutor;->runTask(Ljava/lang/String;Ljava/lang/Runnable;J)V

    .line 170
    return-void
.end method

.method public mostraDialogoDeProgresso()V
    .registers 5
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "CommitTransaction"
        }
    .end annotation

    .line 150
    const-string v0, ""

    new-instance v1, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesDetalheActivity_$3;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesDetalheActivity_$3;-><init>(Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesDetalheActivity_;)V

    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3}, Lorg/androidannotations/api/UiThreadExecutor;->runTask(Ljava/lang/String;Ljava/lang/Runnable;J)V

    .line 158
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 47
    iget-object v0, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesDetalheActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-static {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    move-result-object v1

    .line 48
    .local v1, "previousNotifier":Lorg/androidannotations/api/view/OnViewChangedNotifier;
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesDetalheActivity_;->init_(Landroid/os/Bundle;)V

    .line 49
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesDetalheActivity;->onCreate(Landroid/os/Bundle;)V

    .line 50
    invoke-static {v1}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 51
    const v0, 0x7f030026

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesDetalheActivity_;->setContentView(I)V

    .line 52
    return-void
.end method

.method public onViewChanged(Lorg/androidannotations/api/view/HasViews;)V
    .registers 4
    .param p1, "hasViews"    # Lorg/androidannotations/api/view/HasViews;

    .line 94
    const v0, 0x7f0e00fd

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesDetalheActivity_;->textoNomeOperacaoDetalheComprovante:Landroid/widget/TextView;

    .line 95
    const v0, 0x7f0e0100

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesDetalheActivity_;->textoValorComprovanteDetalhe:Landroid/widget/TextView;

    .line 96
    const v0, 0x7f0e0103

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesDetalheActivity_;->textoDataPagamentoDetalhe:Landroid/widget/TextView;

    .line 97
    const v0, 0x7f0e0106

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesDetalheActivity_;->textoDescricaoIdentificacaoDetalhe:Landroid/widget/TextView;

    .line 98
    const v0, 0x7f0e00fa

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/ui/view/MensagemAvisoView;

    iput-object v0, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesDetalheActivity_;->mensagemAvisoComprovante:Lcom/itau/empresas/ui/view/MensagemAvisoView;

    .line 99
    const v0, 0x7f0e00f8

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ScrollView;

    iput-object v0, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesDetalheActivity_;->comprovantesDetalheScrollConteiner:Landroid/widget/ScrollView;

    .line 100
    const v0, 0x7f0e00f7

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesDetalheActivity_;->comprovantesDetalheConteiner:Landroid/widget/LinearLayout;

    .line 101
    const v0, 0x7f0e0107

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesDetalheActivity_;->comprovanteDetalheBotoes:Landroid/widget/LinearLayout;

    .line 102
    const v0, 0x7f0e0104

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesDetalheActivity_;->comprovanteDetalheIdentificacao:Landroid/widget/LinearLayout;

    .line 103
    const v0, 0x7f0e010b

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesDetalheActivity_;->botaoEnviarDetalheComprovanteEmail:Landroid/widget/TextView;

    .line 104
    const v0, 0x7f0e0109

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesDetalheActivity_;->botaoEnviarDetalheComprovante:Landroid/widget/TextView;

    .line 105
    const v0, 0x7f0e010a

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesDetalheActivity_;->comprovanteDetalheBotaoEmail:Landroid/widget/LinearLayout;

    .line 106
    const v0, 0x7f0e00b6

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/Toolbar;

    iput-object v0, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesDetalheActivity_;->toolBar:Landroid/support/v7/widget/Toolbar;

    .line 107
    iget-object v0, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesDetalheActivity_;->botaoEnviarDetalheComprovante:Landroid/widget/TextView;

    if-eqz v0, :cond_9d

    .line 108
    iget-object v0, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesDetalheActivity_;->botaoEnviarDetalheComprovante:Landroid/widget/TextView;

    new-instance v1, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesDetalheActivity_$1;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesDetalheActivity_$1;-><init>(Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesDetalheActivity_;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 117
    :cond_9d
    iget-object v0, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesDetalheActivity_;->botaoEnviarDetalheComprovanteEmail:Landroid/widget/TextView;

    if-eqz v0, :cond_ab

    .line 118
    iget-object v0, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesDetalheActivity_;->botaoEnviarDetalheComprovanteEmail:Landroid/widget/TextView;

    new-instance v1, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesDetalheActivity_$2;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesDetalheActivity_$2;-><init>(Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesDetalheActivity_;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 127
    :cond_ab
    invoke-virtual {p0}, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesDetalheActivity_;->carregarElementosTela()V

    .line 128
    return-void
.end method

.method public setContentView(I)V
    .registers 3
    .param p1, "layoutResID"    # I

    .line 64
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesDetalheActivity;->setContentView(I)V

    .line 65
    iget-object v0, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesDetalheActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 66
    return-void
.end method

.method public setContentView(Landroid/view/View;)V
    .registers 3
    .param p1, "view"    # Landroid/view/View;

    .line 76
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesDetalheActivity;->setContentView(Landroid/view/View;)V

    .line 77
    iget-object v0, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesDetalheActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 78
    return-void
.end method

.method public setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .registers 4
    .param p1, "view"    # Landroid/view/View;
    .param p2, "params"    # Landroid/view/ViewGroup$LayoutParams;

    .line 70
    invoke-super {p0, p1, p2}, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesDetalheActivity;->setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 71
    iget-object v0, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesDetalheActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 72
    return-void
.end method

.method public setIntent(Landroid/content/Intent;)V
    .registers 2
    .param p1, "newIntent"    # Landroid/content/Intent;

    .line 141
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesDetalheActivity;->setIntent(Landroid/content/Intent;)V

    .line 142
    invoke-direct {p0}, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesDetalheActivity_;->injectExtras_()V

    .line 143
    return-void
.end method

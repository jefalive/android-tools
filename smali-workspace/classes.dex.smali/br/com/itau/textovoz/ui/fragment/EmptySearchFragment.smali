.class public Lbr/com/itau/textovoz/ui/fragment/EmptySearchFragment;
.super Landroid/support/v4/app/Fragment;
.source "EmptySearchFragment.java"


# static fields
.field private static ARG_KEY_TEXT:Ljava/lang/String;


# instance fields
.field private searchText:Ljava/lang/String;

.field private textEnteredTextView:Lbr/com/itau/textovoz/ui/view/custom/CustomTextView;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .line 18
    const-string v0, "searchText"

    sput-object v0, Lbr/com/itau/textovoz/ui/fragment/EmptySearchFragment;->ARG_KEY_TEXT:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .registers 1

    .line 17
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    return-void
.end method

.method public static newInstance(Ljava/lang/String;)Landroid/support/v4/app/Fragment;
    .registers 4
    .param p0, "searchText"    # Ljava/lang/String;

    .line 23
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 24
    .local v1, "b":Landroid/os/Bundle;
    sget-object v0, Lbr/com/itau/textovoz/ui/fragment/EmptySearchFragment;->ARG_KEY_TEXT:Ljava/lang/String;

    invoke-virtual {v1, v0, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 26
    new-instance v2, Lbr/com/itau/textovoz/ui/fragment/EmptySearchFragment;

    invoke-direct {v2}, Lbr/com/itau/textovoz/ui/fragment/EmptySearchFragment;-><init>()V

    .line 27
    .local v2, "f":Landroid/support/v4/app/Fragment;
    invoke-virtual {v2, v1}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 29
    return-object v2
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .registers 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 34
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 36
    invoke-virtual {p0}, Lbr/com/itau/textovoz/ui/fragment/EmptySearchFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    sget-object v1, Lbr/com/itau/textovoz/ui/fragment/EmptySearchFragment;->ARG_KEY_TEXT:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/textovoz/ui/fragment/EmptySearchFragment;->searchText:Ljava/lang/String;

    .line 37
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .registers 7
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .line 42
    sget v0, Lbr/com/itau/textovoz/R$layout;->fragment_empty_list:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 44
    .local v2, "v":Landroid/view/View;
    sget v0, Lbr/com/itau/textovoz/R$id;->tv_search_entered:I

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lbr/com/itau/textovoz/ui/view/custom/CustomTextView;

    iput-object v0, p0, Lbr/com/itau/textovoz/ui/fragment/EmptySearchFragment;->textEnteredTextView:Lbr/com/itau/textovoz/ui/view/custom/CustomTextView;

    .line 45
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/fragment/EmptySearchFragment;->textEnteredTextView:Lbr/com/itau/textovoz/ui/view/custom/CustomTextView;

    iget-object v1, p0, Lbr/com/itau/textovoz/ui/fragment/EmptySearchFragment;->searchText:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lbr/com/itau/textovoz/ui/view/custom/CustomTextView;->setText(Ljava/lang/CharSequence;)V

    .line 47
    return-object v2
.end method

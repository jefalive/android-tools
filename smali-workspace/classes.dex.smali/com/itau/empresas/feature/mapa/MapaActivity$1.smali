.class Lcom/itau/empresas/feature/mapa/MapaActivity$1;
.super Ljava/lang/Object;
.source "MapaActivity.java"

# interfaces
.implements Landroid/support/v4/view/ViewPager$OnPageChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/itau/empresas/feature/mapa/MapaActivity;->afterViews()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/itau/empresas/feature/mapa/MapaActivity;


# direct methods
.method constructor <init>(Lcom/itau/empresas/feature/mapa/MapaActivity;)V
    .registers 2
    .param p1, "this$0"    # Lcom/itau/empresas/feature/mapa/MapaActivity;

    .line 68
    iput-object p1, p0, Lcom/itau/empresas/feature/mapa/MapaActivity$1;->this$0:Lcom/itau/empresas/feature/mapa/MapaActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPageScrollStateChanged(I)V
    .registers 2
    .param p1, "state"    # I

    .line 84
    return-void
.end method

.method public onPageScrolled(IFI)V
    .registers 4
    .param p1, "position"    # I
    .param p2, "positionOffset"    # F
    .param p3, "positionOffsetPixels"    # I

    .line 73
    return-void
.end method

.method public onPageSelected(I)V
    .registers 5
    .param p1, "position"    # I

    .line 77
    iget-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaActivity$1;->this$0:Lcom/itau/empresas/feature/mapa/MapaActivity;

    iget-object v1, p0, Lcom/itau/empresas/feature/mapa/MapaActivity$1;->this$0:Lcom/itau/empresas/feature/mapa/MapaActivity;

    const v2, 0x7f0702c8

    invoke-virtual {v1, v2}, Lcom/itau/empresas/feature/mapa/MapaActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/itau/empresas/feature/mapa/MapaActivity$1;->this$0:Lcom/itau/empresas/feature/mapa/MapaActivity;

    iget-object v2, v2, Lcom/itau/empresas/feature/mapa/MapaActivity;->tabsMap:Landroid/support/design/widget/TabLayout;

    .line 78
    invoke-virtual {v2, p1}, Landroid/support/design/widget/TabLayout;->getTabAt(I)Landroid/support/design/widget/TabLayout$Tab;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/design/widget/TabLayout$Tab;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    .line 77
    invoke-virtual {v0, v1, v2}, Lcom/itau/empresas/feature/mapa/MapaActivity;->analyticsHit(Ljava/lang/String;Ljava/lang/String;)V

    .line 79
    return-void
.end method

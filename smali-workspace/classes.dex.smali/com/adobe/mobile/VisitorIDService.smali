.class final Lcom/adobe/mobile/VisitorIDService;
.super Ljava/lang/Object;
.source "VisitorIDService.java"


# static fields
.field static SERVER:Ljava/lang/String;

.field private static _instance:Lcom/adobe/mobile/VisitorIDService;

.field private static final _instanceMutex:Ljava/lang/Object;


# instance fields
.field private _aamIdString:Ljava/lang/String;

.field private _analyticsIdString:Ljava/lang/String;

.field private _blob:Ljava/lang/String;

.field private _customerIds:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Lcom/adobe/mobile/VisitorID;>;"
        }
    .end annotation
.end field

.field private _lastSync:J

.field private _locationHint:Ljava/lang/String;

.field private _marketingCloudID:Ljava/lang/String;

.field private _targetCustomerId:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
        }
    .end annotation
.end field

.field private _ttl:J

.field private final _visitorIDExector:Ljava/util/concurrent/ExecutorService;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .line 33
    const-string v0, "dpm.demdex.net"

    sput-object v0, Lcom/adobe/mobile/VisitorIDService;->SERVER:Ljava/lang/String;

    .line 76
    const/4 v0, 0x0

    sput-object v0, Lcom/adobe/mobile/VisitorIDService;->_instance:Lcom/adobe/mobile/VisitorIDService;

    .line 77
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/adobe/mobile/VisitorIDService;->_instanceMutex:Ljava/lang/Object;

    return-void
.end method

.method protected constructor <init>()V
    .registers 2

    .line 89
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 91
    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/adobe/mobile/VisitorIDService;->_visitorIDExector:Ljava/util/concurrent/ExecutorService;

    .line 93
    invoke-virtual {p0}, Lcom/adobe/mobile/VisitorIDService;->resetVariablesFromSharedPreferences()V

    .line 95
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/adobe/mobile/VisitorIDService;->idSync(Ljava/util/Map;)V

    .line 96
    return-void
.end method

.method private _generateAnalyticsCustomerIdString(Ljava/util/List;)Ljava/lang/String;
    .registers 7
    .param p1, "visitorIDs"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<Lcom/adobe/mobile/VisitorID;>;)Ljava/lang/String;"
        }
    .end annotation

    .line 821
    if-nez p1, :cond_4

    .line 822
    const/4 v0, 0x0

    return-object v0

    .line 825
    :cond_4
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 826
    .local v2, "visitorIdMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_d
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_35

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/adobe/mobile/VisitorID;

    .line 827
    .local v4, "visitorID":Lcom/adobe/mobile/VisitorID;
    invoke-virtual {v4}, Lcom/adobe/mobile/VisitorID;->serializeIdentifierKeyForAnalyticsID()Ljava/lang/String;

    move-result-object v0

    iget-object v1, v4, Lcom/adobe/mobile/VisitorID;->id:Ljava/lang/String;

    invoke-virtual {v2, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 828
    invoke-virtual {v4}, Lcom/adobe/mobile/VisitorID;->serializeAuthenticationKeyForAnalyticsID()Ljava/lang/String;

    move-result-object v0

    iget-object v1, v4, Lcom/adobe/mobile/VisitorID;->authenticationState:Lcom/adobe/mobile/VisitorID$VisitorIDAuthenticationState;

    invoke-virtual {v1}, Lcom/adobe/mobile/VisitorID$VisitorIDAuthenticationState;->getValue()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 829
    .end local v4    # "visitorID":Lcom/adobe/mobile/VisitorID;
    goto :goto_d

    .line 831
    :cond_35
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 832
    .local v3, "translatedIds":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    const-string v0, "cid"

    invoke-static {v2}, Lcom/adobe/mobile/StaticMethods;->translateContextData(Ljava/util/Map;)Lcom/adobe/mobile/ContextData;

    move-result-object v1

    invoke-virtual {v3, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 835
    new-instance v4, Ljava/lang/StringBuilder;

    const/16 v0, 0x800

    invoke-direct {v4, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 836
    .local v4, "requestString":Ljava/lang/StringBuilder;
    invoke-static {v3, v4}, Lcom/adobe/mobile/StaticMethods;->serializeToQueryString(Ljava/util/Map;Ljava/lang/StringBuilder;)V

    .line 838
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private _generateCustomerIdString(Ljava/util/List;)Ljava/lang/String;
    .registers 7
    .param p1, "newVisitorIDs"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<Lcom/adobe/mobile/VisitorID;>;)Ljava/lang/String;"
        }
    .end annotation

    .line 781
    if-nez p1, :cond_4

    .line 782
    const/4 v0, 0x0

    return-object v0

    .line 785
    :cond_4
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 786
    .local v1, "customerIdString":Ljava/lang/StringBuilder;
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_d
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_51

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/adobe/mobile/VisitorID;

    .line 787
    .local v3, "newVisitorID":Lcom/adobe/mobile/VisitorID;
    const-string v0, "&"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 788
    const-string v0, "d_cid_ic"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 789
    const-string v0, "="

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 790
    iget-object v0, v3, Lcom/adobe/mobile/VisitorID;->idType:Ljava/lang/String;

    invoke-static {v0}, Lcom/adobe/mobile/StaticMethods;->URLEncode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 791
    const-string v0, "%01"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 793
    iget-object v0, v3, Lcom/adobe/mobile/VisitorID;->id:Ljava/lang/String;

    invoke-static {v0}, Lcom/adobe/mobile/StaticMethods;->URLEncode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 794
    .local v4, "urlEncodedID":Ljava/lang/String;
    if-eqz v4, :cond_42

    .line 795
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 798
    :cond_42
    const-string v0, "%01"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 799
    iget-object v0, v3, Lcom/adobe/mobile/VisitorID;->authenticationState:Lcom/adobe/mobile/VisitorID$VisitorIDAuthenticationState;

    invoke-virtual {v0}, Lcom/adobe/mobile/VisitorID$VisitorIDAuthenticationState;->getValue()I

    move-result v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 800
    .end local v3    # "newVisitorID":Lcom/adobe/mobile/VisitorID;
    .end local v4    # "urlEncodedID":Ljava/lang/String;
    goto :goto_d

    .line 801
    :cond_51
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private _generateCustomerIds(Ljava/util/Map;Lcom/adobe/mobile/VisitorID$VisitorIDAuthenticationState;)Ljava/util/List;
    .registers 12
    .param p1, "identifiers"    # Ljava/util/Map;
    .param p2, "authenticationState"    # Lcom/adobe/mobile/VisitorID$VisitorIDAuthenticationState;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;Lcom/adobe/mobile/VisitorID$VisitorIDAuthenticationState;)Ljava/util/List<Lcom/adobe/mobile/VisitorID;>;"
        }
    .end annotation

    .line 842
    if-nez p1, :cond_4

    .line 843
    const/4 v0, 0x0

    return-object v0

    .line 846
    :cond_4
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4, p1}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    .line 847
    .local v4, "identifiersCopy":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 849
    .local v5, "tempIds":Ljava/util/List;, "Ljava/util/List<Lcom/adobe/mobile/VisitorID;>;"
    invoke-virtual {v4}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_16
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4b

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Ljava/util/Map$Entry;

    .line 851
    .local v7, "newID":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    :try_start_23
    new-instance v0, Lcom/adobe/mobile/VisitorID;

    const-string v1, "d_cid_ic"

    invoke-interface {v7}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-interface {v7}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3, p2}, Lcom/adobe/mobile/VisitorID;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/adobe/mobile/VisitorID$VisitorIDAuthenticationState;)V

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_39
    .catch Ljava/lang/IllegalStateException; {:try_start_23 .. :try_end_39} :catch_3a

    .line 854
    goto :goto_4a

    .line 852
    :catch_3a
    move-exception v8

    .line 853
    .local v8, "ex":Ljava/lang/IllegalStateException;
    const-string v0, "ID Service - Unable to create ID after encoding:(%s)"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v8}, Ljava/lang/IllegalStateException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logWarningFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 855
    .end local v7    # "newID":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v7
    .end local v8    # "ex":Ljava/lang/IllegalStateException;
    :goto_4a
    goto :goto_16

    .line 857
    :cond_4b
    return-object v5
.end method

.method private _generateInternalIdString(Ljava/util/Map;)Ljava/lang/String;
    .registers 7
    .param p1, "dpids"    # Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;)Ljava/lang/String;"
        }
    .end annotation

    .line 805
    if-nez p1, :cond_4

    .line 806
    const/4 v0, 0x0

    return-object v0

    .line 809
    :cond_4
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1, p1}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    .line 810
    .local v1, "dpidsCopy":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 811
    .local v2, "internalIdString":Ljava/lang/StringBuilder;
    invoke-virtual {v1}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_16
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_48

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Ljava/util/Map$Entry;

    .line 812
    .local v4, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v0, "&d_cid="

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 813
    invoke-interface {v4}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/adobe/mobile/StaticMethods;->URLEncode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 814
    const-string v0, "%01"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 815
    invoke-interface {v4}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/adobe/mobile/StaticMethods;->URLEncode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 816
    .end local v4    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v4
    goto :goto_16

    .line 817
    :cond_48
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private _generateMID()Ljava/lang/String;
    .registers 11

    .line 624
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v5

    .line 626
    .local v5, "uuid":Ljava/util/UUID;
    invoke-virtual {v5}, Ljava/util/UUID;->getMostSignificantBits()J

    move-result-wide v6

    .line 627
    .local v6, "most":J
    invoke-virtual {v5}, Ljava/util/UUID;->getLeastSignificantBits()J

    move-result-wide v8

    .line 630
    .local v8, "least":J
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "%019d%019d"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const-wide/16 v3, 0x0

    cmp-long v3, v6, v3

    if-gez v3, :cond_1b

    neg-long v3, v6

    goto :goto_1c

    :cond_1b
    move-wide v3, v6

    :goto_1c
    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    const/4 v4, 0x0

    aput-object v3, v2, v4

    const-wide/16 v3, 0x0

    cmp-long v3, v8, v3

    if-gez v3, :cond_2b

    neg-long v3, v8

    goto :goto_2c

    :cond_2b
    move-wide v3, v8

    :goto_2c
    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    const/4 v4, 0x1

    aput-object v3, v2, v4

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private _generateStoredCustomerIdString(Ljava/util/List;)Ljava/lang/String;
    .registers 6
    .param p1, "visitorIDs"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<Lcom/adobe/mobile/VisitorID;>;)Ljava/lang/String;"
        }
    .end annotation

    .line 757
    if-nez p1, :cond_4

    .line 758
    const/4 v0, 0x0

    return-object v0

    .line 761
    :cond_4
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 762
    .local v1, "customerIdString":Ljava/lang/StringBuilder;
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_d
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4b

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/adobe/mobile/VisitorID;

    .line 763
    .local v3, "visitorID":Lcom/adobe/mobile/VisitorID;
    const-string v0, "&"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 764
    const-string v0, "d_cid_ic"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 765
    const-string v0, "="

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 766
    iget-object v0, v3, Lcom/adobe/mobile/VisitorID;->idType:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 767
    const-string v0, "%01"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 769
    iget-object v0, v3, Lcom/adobe/mobile/VisitorID;->id:Ljava/lang/String;

    if-eqz v0, :cond_3c

    .line 770
    iget-object v0, v3, Lcom/adobe/mobile/VisitorID;->id:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 773
    :cond_3c
    const-string v0, "%01"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 774
    iget-object v0, v3, Lcom/adobe/mobile/VisitorID;->authenticationState:Lcom/adobe/mobile/VisitorID$VisitorIDAuthenticationState;

    invoke-virtual {v0}, Lcom/adobe/mobile/VisitorID$VisitorIDAuthenticationState;->getValue()I

    move-result v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 775
    .end local v3    # "visitorID":Lcom/adobe/mobile/VisitorID;
    goto :goto_d

    .line 777
    :cond_4b
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private _generateTargetCustomerId(Ljava/util/List;)Ljava/util/HashMap;
    .registers 9
    .param p1, "visitorIDs"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<Lcom/adobe/mobile/VisitorID;>;)Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
        }
    .end annotation

    .line 530
    if-nez p1, :cond_4

    .line 531
    const/4 v0, 0x0

    return-object v0

    .line 533
    :cond_4
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    .line 534
    .local v4, "targetCustomerId":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_d
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_49

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/adobe/mobile/VisitorID;

    .line 535
    .local v6, "visitorID":Lcom/adobe/mobile/VisitorID;
    const-string v0, "vst.%s.id"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, v6, Lcom/adobe/mobile/VisitorID;->idType:Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, v6, Lcom/adobe/mobile/VisitorID;->id:Ljava/lang/String;

    invoke-virtual {v4, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 536
    const-string v0, "vst.%s.authState"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, v6, Lcom/adobe/mobile/VisitorID;->idType:Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, v6, Lcom/adobe/mobile/VisitorID;->authenticationState:Lcom/adobe/mobile/VisitorID$VisitorIDAuthenticationState;

    invoke-virtual {v1}, Lcom/adobe/mobile/VisitorID$VisitorIDAuthenticationState;->getValue()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v4, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 537
    .end local v6    # "visitorID":Lcom/adobe/mobile/VisitorID;
    goto :goto_d

    .line 538
    :cond_49
    return-object v4
.end method

.method private _mergeCustomerIds(Ljava/util/List;)Ljava/util/List;
    .registers 11
    .param p1, "newCustomerIds"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<Lcom/adobe/mobile/VisitorID;>;)Ljava/util/List<Lcom/adobe/mobile/VisitorID;>;"
        }
    .end annotation

    .line 861
    if-nez p1, :cond_5

    .line 862
    iget-object v0, p0, Lcom/adobe/mobile/VisitorIDService;->_customerIds:Ljava/util/List;

    return-object v0

    .line 865
    :cond_5
    iget-object v0, p0, Lcom/adobe/mobile/VisitorIDService;->_customerIds:Ljava/util/List;

    if-eqz v0, :cond_11

    new-instance v4, Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/adobe/mobile/VisitorIDService;->_customerIds:Ljava/util/List;

    invoke-direct {v4, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    goto :goto_16

    :cond_11
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 868
    .local v4, "tempIds":Ljava/util/List;, "Ljava/util/List<Lcom/adobe/mobile/VisitorID;>;"
    :goto_16
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1a
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5d

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/adobe/mobile/VisitorID;

    .line 870
    .local v6, "newID":Lcom/adobe/mobile/VisitorID;
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_2b
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_48

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/adobe/mobile/VisitorID;

    .line 871
    .local v8, "visitorID":Lcom/adobe/mobile/VisitorID;
    iget-object v0, v6, Lcom/adobe/mobile/VisitorID;->idType:Ljava/lang/String;

    iget-object v1, v6, Lcom/adobe/mobile/VisitorID;->id:Ljava/lang/String;

    invoke-virtual {v8, v0, v1}, Lcom/adobe/mobile/VisitorID;->isVisitorID(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_47

    .line 872
    iget-object v0, v6, Lcom/adobe/mobile/VisitorID;->authenticationState:Lcom/adobe/mobile/VisitorID$VisitorIDAuthenticationState;

    iput-object v0, v8, Lcom/adobe/mobile/VisitorID;->authenticationState:Lcom/adobe/mobile/VisitorID$VisitorIDAuthenticationState;

    .line 873
    goto :goto_1a

    .line 875
    .end local v8    # "visitorID":Lcom/adobe/mobile/VisitorID;
    :cond_47
    goto :goto_2b

    .line 878
    :cond_48
    :try_start_48
    invoke-interface {v4, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_4b
    .catch Ljava/lang/IllegalStateException; {:try_start_48 .. :try_end_4b} :catch_4c

    .line 881
    goto :goto_5c

    .line 879
    :catch_4c
    move-exception v7

    .line 880
    .local v7, "ex":Ljava/lang/IllegalStateException;
    const-string v0, "ID Service - Unable to create ID after encoding:(%s)"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v7}, Ljava/lang/IllegalStateException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logWarningFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 882
    .end local v6    # "newID":Lcom/adobe/mobile/VisitorID;
    .end local v7    # "ex":Ljava/lang/IllegalStateException;
    :goto_5c
    goto :goto_1a

    .line 884
    :cond_5d
    return-object v4
.end method

.method private _parseIdString(Ljava/lang/String;)Ljava/util/List;
    .registers 14
    .param p1, "idString"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/lang/String;)Ljava/util/List<Lcom/adobe/mobile/VisitorID;>;"
        }
    .end annotation

    .line 888
    if-nez p1, :cond_4

    .line 889
    const/4 v0, 0x0

    return-object v0

    .line 892
    :cond_4
    const-string v0, "&"

    invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v5

    .line 893
    .local v5, "customerIdComponentsArray":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 895
    .local v6, "visitorIDs":Ljava/util/List;, "Ljava/util/List<Lcom/adobe/mobile/VisitorID;>;"
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_17
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_a9

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Ljava/lang/String;

    .line 896
    .local v8, "customerIdString":Ljava/lang/String;
    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_a7

    .line 897
    const-string v0, "="

    invoke-virtual {v8, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v9

    .line 898
    .local v9, "internalId":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v0, 0x1

    invoke-interface {v9, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v1, "%01"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v10

    .line 899
    .local v10, "idinfo":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_53

    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x3

    if-eq v0, v1, :cond_54

    .line 900
    :cond_53
    return-object v6

    .line 904
    :cond_54
    :try_start_54
    new-instance v11, Lcom/adobe/mobile/VisitorID;

    const/4 v0, 0x0

    invoke-interface {v9, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const/4 v1, 0x0

    invoke-interface {v10, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    const/4 v2, 0x1

    invoke-interface {v10, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-static {}, Lcom/adobe/mobile/VisitorID$VisitorIDAuthenticationState;->values()[Lcom/adobe/mobile/VisitorID$VisitorIDAuthenticationState;

    move-result-object v3

    const/4 v4, 0x2

    invoke-interface {v10, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    aget-object v3, v3, v4

    invoke-direct {v11, v0, v1, v2, v3}, Lcom/adobe/mobile/VisitorID;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/adobe/mobile/VisitorID$VisitorIDAuthenticationState;)V

    .line 905
    .local v11, "id":Lcom/adobe/mobile/VisitorID;
    invoke-interface {v6, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_82
    .catch Ljava/lang/NumberFormatException; {:try_start_54 .. :try_end_82} :catch_83
    .catch Ljava/lang/IllegalStateException; {:try_start_54 .. :try_end_82} :catch_97

    .line 911
    .end local v11    # "id":Lcom/adobe/mobile/VisitorID;
    goto :goto_a7

    .line 907
    :catch_83
    move-exception v11

    .line 908
    .local v11, "ex":Ljava/lang/NumberFormatException;
    const-string v0, "ID Service - Unable to parse visitor ID: (%s) exception:(%s)"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-virtual {v11}, Ljava/lang/NumberFormatException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logErrorFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 911
    .end local v11    # "ex":Ljava/lang/NumberFormatException;
    goto :goto_a7

    .line 909
    :catch_97
    move-exception v11

    .line 910
    .local v11, "ex":Ljava/lang/IllegalStateException;
    const-string v0, "ID Service - Unable to create ID after encoding:(%s)"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v11}, Ljava/lang/IllegalStateException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logWarningFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 913
    .end local v8    # "customerIdString":Ljava/lang/String;
    .end local v9    # "internalId":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v9
    .end local v10    # "idinfo":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v10
    .end local v11    # "ex":Ljava/lang/IllegalStateException;
    :cond_a7
    :goto_a7
    goto/16 :goto_17

    .line 916
    :cond_a9
    return-object v6
.end method

.method static synthetic access$000(Lcom/adobe/mobile/VisitorIDService;Ljava/lang/String;)Ljava/util/List;
    .registers 3
    .param p0, "x0"    # Lcom/adobe/mobile/VisitorIDService;
    .param p1, "x1"    # Ljava/lang/String;

    .line 31
    invoke-direct {p0, p1}, Lcom/adobe/mobile/VisitorIDService;->_parseIdString(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100(Lcom/adobe/mobile/VisitorIDService;Ljava/util/List;)V
    .registers 2
    .param p0, "x0"    # Lcom/adobe/mobile/VisitorIDService;
    .param p1, "x1"    # Ljava/util/List;

    .line 31
    invoke-direct {p0, p1}, Lcom/adobe/mobile/VisitorIDService;->updateCustomerIds(Ljava/util/List;)V

    return-void
.end method

.method static synthetic access$1000(Lcom/adobe/mobile/VisitorIDService;)Ljava/lang/String;
    .registers 2
    .param p0, "x0"    # Lcom/adobe/mobile/VisitorIDService;

    .line 31
    invoke-direct {p0}, Lcom/adobe/mobile/VisitorIDService;->_generateMID()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1100(Lcom/adobe/mobile/VisitorIDService;Ljava/util/List;)Ljava/util/List;
    .registers 3
    .param p0, "x0"    # Lcom/adobe/mobile/VisitorIDService;
    .param p1, "x1"    # Ljava/util/List;

    .line 31
    invoke-direct {p0, p1}, Lcom/adobe/mobile/VisitorIDService;->_mergeCustomerIds(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1200(Lcom/adobe/mobile/VisitorIDService;)Ljava/util/List;
    .registers 2
    .param p0, "x0"    # Lcom/adobe/mobile/VisitorIDService;

    .line 31
    iget-object v0, p0, Lcom/adobe/mobile/VisitorIDService;->_customerIds:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/adobe/mobile/VisitorIDService;Ljava/util/List;)Ljava/lang/String;
    .registers 3
    .param p0, "x0"    # Lcom/adobe/mobile/VisitorIDService;
    .param p1, "x1"    # Ljava/util/List;

    .line 31
    invoke-direct {p0, p1}, Lcom/adobe/mobile/VisitorIDService;->_generateStoredCustomerIdString(Ljava/util/List;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1400(Lcom/adobe/mobile/VisitorIDService;)Ljava/lang/String;
    .registers 2
    .param p0, "x0"    # Lcom/adobe/mobile/VisitorIDService;

    .line 31
    iget-object v0, p0, Lcom/adobe/mobile/VisitorIDService;->_analyticsIdString:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/adobe/mobile/VisitorIDService;)Ljava/lang/String;
    .registers 2
    .param p0, "x0"    # Lcom/adobe/mobile/VisitorIDService;

    .line 31
    iget-object v0, p0, Lcom/adobe/mobile/VisitorIDService;->_aamIdString:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/adobe/mobile/VisitorIDService;)Ljava/lang/String;
    .registers 2
    .param p0, "x0"    # Lcom/adobe/mobile/VisitorIDService;

    .line 31
    iget-object v0, p0, Lcom/adobe/mobile/VisitorIDService;->_marketingCloudID:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$202(Lcom/adobe/mobile/VisitorIDService;Ljava/lang/String;)Ljava/lang/String;
    .registers 2
    .param p0, "x0"    # Lcom/adobe/mobile/VisitorIDService;
    .param p1, "x1"    # Ljava/lang/String;

    .line 31
    iput-object p1, p0, Lcom/adobe/mobile/VisitorIDService;->_marketingCloudID:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$300(Lcom/adobe/mobile/VisitorIDService;)Ljava/lang/String;
    .registers 2
    .param p0, "x0"    # Lcom/adobe/mobile/VisitorIDService;

    .line 31
    iget-object v0, p0, Lcom/adobe/mobile/VisitorIDService;->_locationHint:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$302(Lcom/adobe/mobile/VisitorIDService;Ljava/lang/String;)Ljava/lang/String;
    .registers 2
    .param p0, "x0"    # Lcom/adobe/mobile/VisitorIDService;
    .param p1, "x1"    # Ljava/lang/String;

    .line 31
    iput-object p1, p0, Lcom/adobe/mobile/VisitorIDService;->_locationHint:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$400(Lcom/adobe/mobile/VisitorIDService;)Ljava/lang/String;
    .registers 2
    .param p0, "x0"    # Lcom/adobe/mobile/VisitorIDService;

    .line 31
    iget-object v0, p0, Lcom/adobe/mobile/VisitorIDService;->_blob:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$402(Lcom/adobe/mobile/VisitorIDService;Ljava/lang/String;)Ljava/lang/String;
    .registers 2
    .param p0, "x0"    # Lcom/adobe/mobile/VisitorIDService;
    .param p1, "x1"    # Ljava/lang/String;

    .line 31
    iput-object p1, p0, Lcom/adobe/mobile/VisitorIDService;->_blob:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$500(Lcom/adobe/mobile/VisitorIDService;)J
    .registers 3
    .param p0, "x0"    # Lcom/adobe/mobile/VisitorIDService;

    .line 31
    iget-wide v0, p0, Lcom/adobe/mobile/VisitorIDService;->_ttl:J

    return-wide v0
.end method

.method static synthetic access$502(Lcom/adobe/mobile/VisitorIDService;J)J
    .registers 3
    .param p0, "x0"    # Lcom/adobe/mobile/VisitorIDService;
    .param p1, "x1"    # J

    .line 31
    iput-wide p1, p0, Lcom/adobe/mobile/VisitorIDService;->_ttl:J

    return-wide p1
.end method

.method static synthetic access$600(Lcom/adobe/mobile/VisitorIDService;)J
    .registers 3
    .param p0, "x0"    # Lcom/adobe/mobile/VisitorIDService;

    .line 31
    iget-wide v0, p0, Lcom/adobe/mobile/VisitorIDService;->_lastSync:J

    return-wide v0
.end method

.method static synthetic access$602(Lcom/adobe/mobile/VisitorIDService;J)J
    .registers 3
    .param p0, "x0"    # Lcom/adobe/mobile/VisitorIDService;
    .param p1, "x1"    # J

    .line 31
    iput-wide p1, p0, Lcom/adobe/mobile/VisitorIDService;->_lastSync:J

    return-wide p1
.end method

.method static synthetic access$700(Lcom/adobe/mobile/VisitorIDService;Ljava/util/Map;Lcom/adobe/mobile/VisitorID$VisitorIDAuthenticationState;)Ljava/util/List;
    .registers 4
    .param p0, "x0"    # Lcom/adobe/mobile/VisitorIDService;
    .param p1, "x1"    # Ljava/util/Map;
    .param p2, "x2"    # Lcom/adobe/mobile/VisitorID$VisitorIDAuthenticationState;

    .line 31
    invoke-direct {p0, p1, p2}, Lcom/adobe/mobile/VisitorIDService;->_generateCustomerIds(Ljava/util/Map;Lcom/adobe/mobile/VisitorID$VisitorIDAuthenticationState;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$800(Lcom/adobe/mobile/VisitorIDService;Ljava/util/List;)Ljava/lang/String;
    .registers 3
    .param p0, "x0"    # Lcom/adobe/mobile/VisitorIDService;
    .param p1, "x1"    # Ljava/util/List;

    .line 31
    invoke-direct {p0, p1}, Lcom/adobe/mobile/VisitorIDService;->_generateCustomerIdString(Ljava/util/List;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$900(Lcom/adobe/mobile/VisitorIDService;Ljava/util/Map;)Ljava/lang/String;
    .registers 3
    .param p0, "x0"    # Lcom/adobe/mobile/VisitorIDService;
    .param p1, "x1"    # Ljava/util/Map;

    .line 31
    invoke-direct {p0, p1}, Lcom/adobe/mobile/VisitorIDService;->_generateInternalIdString(Ljava/util/Map;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static sharedInstance()Lcom/adobe/mobile/VisitorIDService;
    .registers 3

    .line 79
    sget-object v1, Lcom/adobe/mobile/VisitorIDService;->_instanceMutex:Ljava/lang/Object;

    monitor-enter v1

    .line 80
    :try_start_3
    sget-object v0, Lcom/adobe/mobile/VisitorIDService;->_instance:Lcom/adobe/mobile/VisitorIDService;

    if-nez v0, :cond_e

    .line 81
    new-instance v0, Lcom/adobe/mobile/VisitorIDService;

    invoke-direct {v0}, Lcom/adobe/mobile/VisitorIDService;-><init>()V

    sput-object v0, Lcom/adobe/mobile/VisitorIDService;->_instance:Lcom/adobe/mobile/VisitorIDService;

    .line 84
    :cond_e
    sget-object v0, Lcom/adobe/mobile/VisitorIDService;->_instance:Lcom/adobe/mobile/VisitorIDService;
    :try_end_10
    .catchall {:try_start_3 .. :try_end_10} :catchall_12

    monitor-exit v1

    return-object v0

    .line 85
    :catchall_12
    move-exception v2

    monitor-exit v1

    throw v2
.end method

.method private updateCustomerIds(Ljava/util/List;)V
    .registers 3
    .param p1, "customerIds"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<Lcom/adobe/mobile/VisitorID;>;)V"
        }
    .end annotation

    .line 131
    iput-object p1, p0, Lcom/adobe/mobile/VisitorIDService;->_customerIds:Ljava/util/List;

    .line 132
    iget-object v0, p0, Lcom/adobe/mobile/VisitorIDService;->_customerIds:Ljava/util/List;

    invoke-direct {p0, v0}, Lcom/adobe/mobile/VisitorIDService;->_generateAnalyticsCustomerIdString(Ljava/util/List;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/adobe/mobile/VisitorIDService;->_analyticsIdString:Ljava/lang/String;

    .line 133
    iget-object v0, p0, Lcom/adobe/mobile/VisitorIDService;->_customerIds:Ljava/util/List;

    invoke-direct {p0, v0}, Lcom/adobe/mobile/VisitorIDService;->_generateCustomerIdString(Ljava/util/List;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/adobe/mobile/VisitorIDService;->_aamIdString:Ljava/lang/String;

    .line 134
    iget-object v0, p0, Lcom/adobe/mobile/VisitorIDService;->_customerIds:Ljava/util/List;

    invoke-direct {p0, v0}, Lcom/adobe/mobile/VisitorIDService;->_generateTargetCustomerId(Ljava/util/List;)Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/adobe/mobile/VisitorIDService;->_targetCustomerId:Ljava/util/HashMap;

    .line 136
    return-void
.end method


# virtual methods
.method protected final getAAMParameterString()Ljava/lang/String;
    .registers 8

    .line 482
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 483
    .local v4, "returnValue":Ljava/lang/StringBuilder;
    new-instance v5, Ljava/util/concurrent/FutureTask;

    new-instance v0, Lcom/adobe/mobile/VisitorIDService$9;

    invoke-direct {v0, p0, v4}, Lcom/adobe/mobile/VisitorIDService$9;-><init>(Lcom/adobe/mobile/VisitorIDService;Ljava/lang/StringBuilder;)V

    invoke-direct {v5, v0}, Ljava/util/concurrent/FutureTask;-><init>(Ljava/util/concurrent/Callable;)V

    .line 517
    .local v5, "f":Ljava/util/concurrent/FutureTask;, "Ljava/util/concurrent/FutureTask<Ljava/lang/Void;>;"
    iget-object v0, p0, Lcom/adobe/mobile/VisitorIDService;->_visitorIDExector:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0, v5}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 520
    :try_start_14
    invoke-virtual {v5}, Ljava/util/concurrent/FutureTask;->get()Ljava/lang/Object;
    :try_end_17
    .catch Ljava/lang/Exception; {:try_start_14 .. :try_end_17} :catch_18

    .line 523
    goto :goto_28

    .line 521
    :catch_18
    move-exception v6

    .line 522
    .local v6, "ex":Ljava/lang/Exception;
    const-string v0, "ID Service - Unable to retrieve audience manager parameters from queue(%s)"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v6}, Ljava/lang/Exception;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logErrorFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 525
    .end local v6    # "ex":Ljava/lang/Exception;
    :goto_28
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected final getAnalyticsIDRequestParameterString()Ljava/lang/String;
    .registers 8

    .line 360
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 362
    .local v4, "returnValue":Ljava/lang/StringBuilder;
    new-instance v5, Ljava/util/concurrent/FutureTask;

    new-instance v0, Lcom/adobe/mobile/VisitorIDService$5;

    invoke-direct {v0, p0, v4}, Lcom/adobe/mobile/VisitorIDService$5;-><init>(Lcom/adobe/mobile/VisitorIDService;Ljava/lang/StringBuilder;)V

    invoke-direct {v5, v0}, Ljava/util/concurrent/FutureTask;-><init>(Ljava/util/concurrent/Callable;)V

    .line 381
    .local v5, "f":Ljava/util/concurrent/FutureTask;, "Ljava/util/concurrent/FutureTask<Ljava/lang/Void;>;"
    iget-object v0, p0, Lcom/adobe/mobile/VisitorIDService;->_visitorIDExector:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0, v5}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 384
    :try_start_14
    invoke-virtual {v5}, Ljava/util/concurrent/FutureTask;->get()Ljava/lang/Object;
    :try_end_17
    .catch Ljava/lang/Exception; {:try_start_14 .. :try_end_17} :catch_18

    .line 387
    goto :goto_28

    .line 385
    :catch_18
    move-exception v6

    .line 386
    .local v6, "ex":Ljava/lang/Exception;
    const-string v0, "ID Service - Unable to retrieve analytics id parameters from queue(%s)"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v6}, Ljava/lang/Exception;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logErrorFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 389
    .end local v6    # "ex":Ljava/lang/Exception;
    :goto_28
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected final getAnalyticsIdString()Ljava/lang/String;
    .registers 7

    .line 429
    new-instance v4, Ljava/util/concurrent/FutureTask;

    new-instance v0, Lcom/adobe/mobile/VisitorIDService$7;

    invoke-direct {v0, p0}, Lcom/adobe/mobile/VisitorIDService$7;-><init>(Lcom/adobe/mobile/VisitorIDService;)V

    invoke-direct {v4, v0}, Ljava/util/concurrent/FutureTask;-><init>(Ljava/util/concurrent/Callable;)V

    .line 436
    .local v4, "f":Ljava/util/concurrent/FutureTask;, "Ljava/util/concurrent/FutureTask<Ljava/lang/String;>;"
    iget-object v0, p0, Lcom/adobe/mobile/VisitorIDService;->_visitorIDExector:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0, v4}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 439
    :try_start_f
    invoke-virtual {v4}, Ljava/util/concurrent/FutureTask;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;
    :try_end_15
    .catch Ljava/lang/Exception; {:try_start_f .. :try_end_15} :catch_16

    return-object v0

    .line 440
    :catch_16
    move-exception v5

    .line 441
    .local v5, "ex":Ljava/lang/Exception;
    const-string v0, "ID Service - Unable to retrieve analytics id string from queue(%s)"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v5}, Ljava/lang/Exception;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logErrorFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 442
    const-string v0, ""

    return-object v0
.end method

.method protected final getAnalyticsIdVisitorParameters()Ljava/util/Map;
    .registers 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
        }
    .end annotation

    .line 447
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    .line 448
    .local v4, "analyticsIdVisitorParameters":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    new-instance v5, Ljava/util/concurrent/FutureTask;

    new-instance v0, Lcom/adobe/mobile/VisitorIDService$8;

    invoke-direct {v0, p0, v4}, Lcom/adobe/mobile/VisitorIDService$8;-><init>(Lcom/adobe/mobile/VisitorIDService;Ljava/util/Map;)V

    invoke-direct {v5, v0}, Ljava/util/concurrent/FutureTask;-><init>(Ljava/util/concurrent/Callable;)V

    .line 470
    .local v5, "f":Ljava/util/concurrent/FutureTask;, "Ljava/util/concurrent/FutureTask<Ljava/lang/Void;>;"
    iget-object v0, p0, Lcom/adobe/mobile/VisitorIDService;->_visitorIDExector:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0, v5}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 473
    :try_start_14
    invoke-virtual {v5}, Ljava/util/concurrent/FutureTask;->get()Ljava/lang/Object;
    :try_end_17
    .catch Ljava/lang/Exception; {:try_start_14 .. :try_end_17} :catch_18

    .line 476
    goto :goto_28

    .line 474
    :catch_18
    move-exception v6

    .line 475
    .local v6, "ex":Ljava/lang/Exception;
    const-string v0, "ID Service - Unable to retrieve analytics parameters from queue(%s)"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v6}, Ljava/lang/Exception;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logErrorFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 478
    .end local v6    # "ex":Ljava/lang/Exception;
    :goto_28
    return-object v4
.end method

.method protected final getMarketingCloudID()Ljava/lang/String;
    .registers 7

    .line 324
    new-instance v4, Ljava/util/concurrent/FutureTask;

    new-instance v0, Lcom/adobe/mobile/VisitorIDService$3;

    invoke-direct {v0, p0}, Lcom/adobe/mobile/VisitorIDService$3;-><init>(Lcom/adobe/mobile/VisitorIDService;)V

    invoke-direct {v4, v0}, Ljava/util/concurrent/FutureTask;-><init>(Ljava/util/concurrent/Callable;)V

    .line 331
    .local v4, "f":Ljava/util/concurrent/FutureTask;, "Ljava/util/concurrent/FutureTask<Ljava/lang/String;>;"
    iget-object v0, p0, Lcom/adobe/mobile/VisitorIDService;->_visitorIDExector:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0, v4}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 334
    :try_start_f
    invoke-virtual {v4}, Ljava/util/concurrent/FutureTask;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;
    :try_end_15
    .catch Ljava/lang/Exception; {:try_start_f .. :try_end_15} :catch_16

    return-object v0

    .line 335
    :catch_16
    move-exception v5

    .line 336
    .local v5, "ex":Ljava/lang/Exception;
    const-string v0, "ID Service - Unable to retrieve marketing cloud id from queue(%s)"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v5}, Ljava/lang/Exception;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logErrorFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 337
    const/4 v0, 0x0

    return-object v0
.end method

.method protected idSync(Ljava/util/Map;)V
    .registers 5
    .param p1, "identifiers"    # Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;)V"
        }
    .end annotation

    .line 149
    sget-object v0, Lcom/adobe/mobile/VisitorID$VisitorIDAuthenticationState;->VISITOR_ID_AUTHENTICATION_STATE_UNKNOWN:Lcom/adobe/mobile/VisitorID$VisitorIDAuthenticationState;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {p0, p1, v1, v0, v2}, Lcom/adobe/mobile/VisitorIDService;->idSync(Ljava/util/Map;Ljava/util/Map;Lcom/adobe/mobile/VisitorID$VisitorIDAuthenticationState;Z)V

    .line 150
    return-void
.end method

.method protected idSync(Ljava/util/Map;Ljava/util/Map;Lcom/adobe/mobile/VisitorID$VisitorIDAuthenticationState;Z)V
    .registers 14
    .param p1, "identifiers"    # Ljava/util/Map;
    .param p2, "dpids"    # Ljava/util/Map;
    .param p3, "authenticationState"    # Lcom/adobe/mobile/VisitorID$VisitorIDAuthenticationState;
    .param p4, "forceResync"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;Lcom/adobe/mobile/VisitorID$VisitorIDAuthenticationState;Z)V"
        }
    .end annotation

    .line 162
    if-eqz p1, :cond_8

    new-instance v7, Ljava/util/HashMap;

    invoke-direct {v7, p1}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    goto :goto_9

    :cond_8
    const/4 v7, 0x0

    .line 163
    .local v7, "identifiersCopy":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    :goto_9
    if-eqz p2, :cond_11

    new-instance v8, Ljava/util/HashMap;

    invoke-direct {v8, p2}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    goto :goto_12

    :cond_11
    const/4 v8, 0x0

    .line 165
    .local v8, "dpidsCopy":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    :goto_12
    iget-object v0, p0, Lcom/adobe/mobile/VisitorIDService;->_visitorIDExector:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/adobe/mobile/VisitorIDService$2;

    move-object v2, p0

    move v3, p4

    move-object v4, v7

    move-object v5, v8

    move-object v6, p3

    invoke-direct/range {v1 .. v6}, Lcom/adobe/mobile/VisitorIDService$2;-><init>(Lcom/adobe/mobile/VisitorIDService;ZLjava/util/HashMap;Ljava/util/HashMap;Lcom/adobe/mobile/VisitorID$VisitorIDAuthenticationState;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 301
    return-void
.end method

.method protected final parseResponse([B)Lorg/json/JSONObject;
    .registers 8
    .param p1, "response"    # [B

    .line 304
    if-nez p1, :cond_4

    .line 305
    const/4 v0, 0x0

    return-object v0

    .line 311
    :cond_4
    :try_start_4
    new-instance v4, Lorg/json/JSONObject;

    new-instance v0, Ljava/lang/String;

    const-string v1, "UTF-8"

    invoke-direct {v0, p1, v1}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    invoke-direct {v4, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V
    :try_end_10
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_4 .. :try_end_10} :catch_11
    .catch Lorg/json/JSONException; {:try_start_4 .. :try_end_10} :catch_23

    .line 318
    .local v4, "responseObject":Lorg/json/JSONObject;
    goto :goto_35

    .line 312
    .end local v4    # "responseObject":Lorg/json/JSONObject;
    :catch_11
    move-exception v5

    .line 313
    .local v5, "ex":Ljava/io/UnsupportedEncodingException;
    const-string v0, "ID Service - Unable to decode response(%s)"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v5}, Ljava/io/UnsupportedEncodingException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logErrorFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 314
    const/4 v0, 0x0

    return-object v0

    .line 315
    .end local v5    # "ex":Ljava/io/UnsupportedEncodingException;
    :catch_23
    move-exception v5

    .line 316
    .local v5, "ex":Lorg/json/JSONException;
    const-string v0, "ID Service - Unable to parse response(%s)"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v5}, Lorg/json/JSONException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logDebugFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 317
    const/4 v0, 0x0

    return-object v0

    .line 319
    .local v4, "responseObject":Lorg/json/JSONObject;
    .end local v5    # "ex":Lorg/json/JSONException;
    :goto_35
    return-object v4
.end method

.method protected resetVariablesFromSharedPreferences()V
    .registers 7

    .line 99
    new-instance v4, Ljava/util/concurrent/FutureTask;

    new-instance v0, Lcom/adobe/mobile/VisitorIDService$1;

    invoke-direct {v0, p0}, Lcom/adobe/mobile/VisitorIDService$1;-><init>(Lcom/adobe/mobile/VisitorIDService;)V

    invoke-direct {v4, v0}, Ljava/util/concurrent/FutureTask;-><init>(Ljava/util/concurrent/Callable;)V

    .line 120
    .local v4, "f":Ljava/util/concurrent/FutureTask;, "Ljava/util/concurrent/FutureTask<Ljava/lang/Void;>;"
    iget-object v0, p0, Lcom/adobe/mobile/VisitorIDService;->_visitorIDExector:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0, v4}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 123
    :try_start_f
    invoke-virtual {v4}, Ljava/util/concurrent/FutureTask;->get()Ljava/lang/Object;
    :try_end_12
    .catch Ljava/lang/Exception; {:try_start_f .. :try_end_12} :catch_13

    .line 126
    goto :goto_23

    .line 124
    :catch_13
    move-exception v5

    .line 125
    .local v5, "ex":Ljava/lang/Exception;
    const-string v0, "ID Service - Unable to initialize visitor ID variables(%s)"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v5}, Ljava/lang/Exception;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logErrorFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 128
    .end local v5    # "ex":Ljava/lang/Exception;
    :goto_23
    return-void
.end method

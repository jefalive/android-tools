.class public final Lbr/com/itau/sdk/android/core/model/UiModelDTO;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private numeroSerieTokenAplicativo:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "numeroSerieTokenAplicativo"
    .end annotation
.end field

.field private numeroSerieTokenFisico:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "numeroSerieTokenFisico"
    .end annotation
.end field

.field private numeroSerieTokenSMS:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "numeroSerieTokenSMS"
    .end annotation
.end field

.field private numeroTelefoneSMS:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "numeroTelefoneSMS"
    .end annotation
.end field

.field private qtdDigitosSenha:I
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "qtdDigitosSenha"
    .end annotation
.end field

.field private rsa2048Key:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "rsa2048Key"
    .end annotation
.end field

.field private textoSenha:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "textoSenha"
    .end annotation
.end field

.field private tokenAplicativo:Lbr/com/itau/sdk/android/core/model/TokenDTO;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "tokenAplicativo"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getNumeroSerieTokenAplicativo()Ljava/lang/String;
    .registers 2

    .line 61
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/model/UiModelDTO;->numeroSerieTokenAplicativo:Ljava/lang/String;

    return-object v0
.end method

.method public getNumeroSerieTokenFisico()Ljava/lang/String;
    .registers 2

    .line 45
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/model/UiModelDTO;->numeroSerieTokenFisico:Ljava/lang/String;

    return-object v0
.end method

.method public getNumeroSerieTokenSMS()Ljava/lang/String;
    .registers 2

    .line 49
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/model/UiModelDTO;->numeroSerieTokenSMS:Ljava/lang/String;

    return-object v0
.end method

.method public getNumeroTelefoneSMS()Ljava/lang/String;
    .registers 2

    .line 65
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/model/UiModelDTO;->numeroTelefoneSMS:Ljava/lang/String;

    return-object v0
.end method

.method public getQtdDigitosSenha()I
    .registers 2

    .line 41
    iget v0, p0, Lbr/com/itau/sdk/android/core/model/UiModelDTO;->qtdDigitosSenha:I

    return v0
.end method

.method public getRsa2048Key()Ljava/lang/String;
    .registers 2

    .line 33
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/model/UiModelDTO;->rsa2048Key:Ljava/lang/String;

    return-object v0
.end method

.method public getTextoSenha()Ljava/lang/String;
    .registers 2

    .line 37
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/model/UiModelDTO;->textoSenha:Ljava/lang/String;

    return-object v0
.end method

.method public getTokenAplicativo()Lbr/com/itau/sdk/android/core/model/TokenDTO;
    .registers 2

    .line 29
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/model/UiModelDTO;->tokenAplicativo:Lbr/com/itau/sdk/android/core/model/TokenDTO;

    return-object v0
.end method

.method public setNumeroSerieTokenAplicativo(Ljava/lang/String;)V
    .registers 2

    .line 53
    iput-object p1, p0, Lbr/com/itau/sdk/android/core/model/UiModelDTO;->numeroSerieTokenAplicativo:Ljava/lang/String;

    .line 54
    return-void
.end method

.method public setNumeroSerieTokenFisico(Ljava/lang/String;)V
    .registers 2

    .line 57
    iput-object p1, p0, Lbr/com/itau/sdk/android/core/model/UiModelDTO;->numeroSerieTokenFisico:Ljava/lang/String;

    .line 58
    return-void
.end method

.method public setNumeroTelefoneSMS(Ljava/lang/String;)V
    .registers 2

    .line 69
    iput-object p1, p0, Lbr/com/itau/sdk/android/core/model/UiModelDTO;->numeroTelefoneSMS:Ljava/lang/String;

    .line 70
    return-void
.end method

.method public setTokenAplicativo(Lbr/com/itau/sdk/android/core/model/TokenDTO;)V
    .registers 2

    .line 25
    iput-object p1, p0, Lbr/com/itau/sdk/android/core/model/UiModelDTO;->tokenAplicativo:Lbr/com/itau/sdk/android/core/model/TokenDTO;

    .line 26
    return-void
.end method

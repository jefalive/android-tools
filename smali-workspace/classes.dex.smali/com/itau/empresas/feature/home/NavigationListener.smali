.class public Lcom/itau/empresas/feature/home/NavigationListener;
.super Ljava/lang/Object;
.source "NavigationListener.java"

# interfaces
.implements Landroid/support/design/widget/BottomNavigationView$OnNavigationItemSelectedListener;


# instance fields
.field private final abridor:Lcom/itau/empresas/feature/home/AbridorDeFragment;

.field private itemSelecionado:Landroid/view/MenuItem;


# direct methods
.method public constructor <init>(Lcom/itau/empresas/feature/home/HomeLogadaActivity;)V
    .registers 2
    .param p1, "abridor"    # Lcom/itau/empresas/feature/home/HomeLogadaActivity;

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object p1, p0, Lcom/itau/empresas/feature/home/NavigationListener;->abridor:Lcom/itau/empresas/feature/home/AbridorDeFragment;

    .line 33
    return-void
.end method

.method private abreFragmentExtrato(Lcom/itau/empresas/CustomApplication;)V
    .registers 6
    .param p1, "application"    # Lcom/itau/empresas/CustomApplication;

    .line 75
    invoke-static {}, Lcom/itau/empresas/ui/util/menu/MenuBuscaHelper;->getInstance()Lcom/itau/empresas/ui/util/menu/MenuBuscaHelper;

    move-result-object v0

    .line 76
    invoke-virtual {p1}, Lcom/itau/empresas/CustomApplication;->getMenuVO()Lcom/itau/empresas/api/model/MenuVO;

    move-result-object v1

    const-string v2, "tabbar_extrato"

    invoke-virtual {v0, v1, v2}, Lcom/itau/empresas/ui/util/menu/MenuBuscaHelper;->buscaPorChave(Lcom/itau/empresas/api/model/MenuVO;Ljava/lang/String;)Lcom/itau/empresas/api/model/MenuVO;

    move-result-object v3

    .line 78
    .local v3, "menuVO":Lcom/itau/empresas/api/model/MenuVO;
    if-eqz v3, :cond_23

    .line 79
    invoke-static {}, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal_;->builder()Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal_$FragmentBuilder_;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal_$FragmentBuilder_;->build()Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/home/NavigationListener;->abreFragment(Landroid/support/v4/app/Fragment;)V

    .line 80
    .line 81
    invoke-virtual {v3}, Lcom/itau/empresas/api/model/MenuVO;->getChaveMobile()Ljava/lang/String;

    move-result-object v0

    .line 80
    invoke-static {p1, v0}, Lcom/itau/empresas/ui/util/AtendimentoUtil;->registraContextualizacaoFiltroAtendimento(Lcom/itau/empresas/CustomApplication;Ljava/lang/String;)V

    goto :goto_2a

    .line 83
    :cond_23
    invoke-direct {p0}, Lcom/itau/empresas/feature/home/NavigationListener;->constroiEmptyStateExtrato()Landroid/support/v4/app/Fragment;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/home/NavigationListener;->abreFragment(Landroid/support/v4/app/Fragment;)V

    .line 85
    :goto_2a
    return-void
.end method

.method private abreTrasacoesFragment(Lcom/itau/empresas/CustomApplication;)V
    .registers 6
    .param p1, "application"    # Lcom/itau/empresas/CustomApplication;

    .line 68
    invoke-static {}, Lcom/itau/empresas/ui/util/menu/MenuBuscaHelper;->getInstance()Lcom/itau/empresas/ui/util/menu/MenuBuscaHelper;

    move-result-object v0

    invoke-virtual {p1}, Lcom/itau/empresas/CustomApplication;->getMenuVO()Lcom/itau/empresas/api/model/MenuVO;

    move-result-object v1

    const-string v2, "tabbar_transacoes"

    invoke-virtual {v0, v1, v2}, Lcom/itau/empresas/ui/util/menu/MenuBuscaHelper;->buscaPorChave(Lcom/itau/empresas/api/model/MenuVO;Ljava/lang/String;)Lcom/itau/empresas/api/model/MenuVO;

    move-result-object v0

    .line 67
    invoke-static {v0}, Lcom/itau/empresas/feature/home/menu/TransacoesFragment;->newInstance(Lcom/itau/empresas/api/model/MenuVO;)Lcom/itau/empresas/feature/home/menu/TransacoesFragment;

    move-result-object v3

    .line 70
    .local v3, "transacoesFragment":Lcom/itau/empresas/feature/home/menu/TransacoesFragment;
    iget-object v0, p0, Lcom/itau/empresas/feature/home/NavigationListener;->abridor:Lcom/itau/empresas/feature/home/AbridorDeFragment;

    invoke-virtual {v3, v0}, Lcom/itau/empresas/feature/home/menu/TransacoesFragment;->setAbridor(Lcom/itau/empresas/feature/home/AbridorDeFragment;)V

    .line 71
    invoke-virtual {p0, v3}, Lcom/itau/empresas/feature/home/NavigationListener;->abreFragment(Landroid/support/v4/app/Fragment;)V

    .line 72
    return-void
.end method

.method private constroiEmptyStateExtrato()Landroid/support/v4/app/Fragment;
    .registers 3

    .line 88
    invoke-direct {p0}, Lcom/itau/empresas/feature/home/NavigationListener;->disparaEventoAnalytics()V

    .line 89
    invoke-static {}, Lcom/itau/empresas/feature/home/EmptyStateFragment_;->builder()Lcom/itau/empresas/feature/home/EmptyStateFragment_$FragmentBuilder_;

    move-result-object v0

    .line 90
    const v1, 0x7f07020a

    invoke-static {v1}, Lcom/itau/empresas/feature/home/NavigationListener;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 89
    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/home/EmptyStateFragment_$FragmentBuilder_;->descricao(Ljava/lang/String;)Lcom/itau/empresas/feature/home/EmptyStateFragment_$FragmentBuilder_;

    move-result-object v0

    .line 91
    const v1, 0x7f0200d7

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/home/EmptyStateFragment_$FragmentBuilder_;->icone(Ljava/lang/Integer;)Lcom/itau/empresas/feature/home/EmptyStateFragment_$FragmentBuilder_;

    move-result-object v0

    .line 92
    invoke-virtual {v0}, Lcom/itau/empresas/feature/home/EmptyStateFragment_$FragmentBuilder_;->build()Lcom/itau/empresas/feature/home/EmptyStateFragment;

    move-result-object v0

    .line 89
    return-object v0
.end method

.method private disparaEventoAnalytics()V
    .registers 5

    .line 121
    invoke-static {}, Lcom/itau/empresas/feature/analytics/AnalyticsHelper;->getInstance()Lcom/itau/empresas/feature/analytics/AnalyticsHelper;

    move-result-object v3

    .line 122
    .line 123
    .local v3, "analyticsHelper":Lcom/itau/empresas/feature/analytics/AnalyticsHelper;
    const v0, 0x7f0700e1

    invoke-static {v0}, Lcom/itau/empresas/feature/home/NavigationListener;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 124
    const v1, 0x7f0700c8

    invoke-static {v1}, Lcom/itau/empresas/feature/home/NavigationListener;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 125
    const v2, 0x7f0700c3

    invoke-static {v2}, Lcom/itau/empresas/feature/home/NavigationListener;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 122
    invoke-virtual {v3, v0, v1, v2}, Lcom/itau/empresas/feature/analytics/AnalyticsHelper;->eventoAdobeState(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 127
    return-void
.end method

.method private static getString(I)Ljava/lang/String;
    .registers 3
    .param p0, "textId"    # I

    .line 96
    invoke-static {}, Lcom/itau/empresas/CustomApplication;->getSelfInstance()Lcom/itau/empresas/CustomApplication;

    move-result-object v1

    .line 97
    .local v1, "selfInstance":Lcom/itau/empresas/CustomApplication;
    if-nez v1, :cond_9

    .line 98
    const-string v0, ""

    return-object v0

    .line 99
    :cond_9
    invoke-virtual {v1, p0}, Lcom/itau/empresas/CustomApplication;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method abreFragment(Landroid/support/v4/app/Fragment;)V
    .registers 4
    .param p1, "fragment"    # Landroid/support/v4/app/Fragment;

    .line 103
    iget-object v0, p0, Lcom/itau/empresas/feature/home/NavigationListener;->abridor:Lcom/itau/empresas/feature/home/AbridorDeFragment;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, Lcom/itau/empresas/feature/home/AbridorDeFragment;->abreFragment(Landroid/support/v4/app/Fragment;Z)V

    .line 104
    return-void
.end method

.method public getSelectedItemId()I
    .registers 2

    .line 111
    iget-object v0, p0, Lcom/itau/empresas/feature/home/NavigationListener;->itemSelecionado:Landroid/view/MenuItem;

    if-eqz v0, :cond_b

    .line 112
    iget-object v0, p0, Lcom/itau/empresas/feature/home/NavigationListener;->itemSelecionado:Landroid/view/MenuItem;

    invoke-interface {v0}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    return v0

    .line 113
    :cond_b
    const/4 v0, 0x0

    return v0
.end method

.method public isHomeSelecionada()Z
    .registers 3

    .line 117
    invoke-virtual {p0}, Lcom/itau/empresas/feature/home/NavigationListener;->getSelectedItemId()I

    move-result v0

    const v1, 0x7f0e06c7

    if-ne v0, v1, :cond_b

    const/4 v0, 0x1

    goto :goto_c

    :cond_b
    const/4 v0, 0x0

    :goto_c
    return v0
.end method

.method public onNavigationItemSelected(Landroid/view/MenuItem;)Z
    .registers 7
    .param p1, "item"    # Landroid/view/MenuItem;

    .line 37
    invoke-static {}, Lcom/itau/empresas/CustomApplication;->getSelfInstance()Lcom/itau/empresas/CustomApplication;

    move-result-object v3

    .line 39
    .local v3, "application":Lcom/itau/empresas/CustomApplication;
    iput-object p1, p0, Lcom/itau/empresas/feature/home/NavigationListener;->itemSelecionado:Landroid/view/MenuItem;

    .line 41
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_48

    goto :goto_44

    .line 43
    :pswitch_e
    invoke-static {}, Lcom/itau/empresas/feature/home/HomeFragment_;->builder()Lcom/itau/empresas/feature/home/HomeFragment_$FragmentBuilder_;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/feature/home/HomeFragment_$FragmentBuilder_;->build()Lcom/itau/empresas/feature/home/HomeFragment;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/home/NavigationListener;->abreFragment(Landroid/support/v4/app/Fragment;)V

    .line 44
    goto :goto_46

    .line 46
    :pswitch_1a
    invoke-direct {p0, v3}, Lcom/itau/empresas/feature/home/NavigationListener;->abreFragmentExtrato(Lcom/itau/empresas/CustomApplication;)V

    .line 47
    goto :goto_46

    .line 49
    :pswitch_1e
    invoke-static {}, Lcom/itau/empresas/feature/atendimento/fragment/AtendimentoFragment_;->builder()Lcom/itau/empresas/feature/atendimento/fragment/AtendimentoFragment_$FragmentBuilder_;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/feature/atendimento/fragment/AtendimentoFragment_$FragmentBuilder_;->build()Lcom/itau/empresas/feature/atendimento/fragment/AtendimentoFragment;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/home/NavigationListener;->abreFragment(Landroid/support/v4/app/Fragment;)V

    .line 50
    goto :goto_46

    .line 52
    :pswitch_2a
    invoke-static {}, Lcom/itau/empresas/ui/util/menu/MenuBuscaHelper;->getInstance()Lcom/itau/empresas/ui/util/menu/MenuBuscaHelper;

    move-result-object v0

    invoke-virtual {v3}, Lcom/itau/empresas/CustomApplication;->getMenuVO()Lcom/itau/empresas/api/model/MenuVO;

    move-result-object v1

    const-string v2, "tabbar_servicos"

    invoke-virtual {v0, v1, v2}, Lcom/itau/empresas/ui/util/menu/MenuBuscaHelper;->buscaPorChave(Lcom/itau/empresas/api/model/MenuVO;Ljava/lang/String;)Lcom/itau/empresas/api/model/MenuVO;

    move-result-object v4

    .line 54
    .local v4, "menuVO":Lcom/itau/empresas/api/model/MenuVO;
    invoke-static {v4}, Lcom/itau/empresas/feature/home/menu/ServicosFragment;->newInstance(Lcom/itau/empresas/api/model/MenuVO;)Lcom/itau/empresas/feature/home/menu/ServicosFragment;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/home/NavigationListener;->abreFragment(Landroid/support/v4/app/Fragment;)V

    .line 55
    goto :goto_46

    .line 57
    .end local v4    # "menuVO":Lcom/itau/empresas/api/model/MenuVO;
    :pswitch_40
    invoke-direct {p0, v3}, Lcom/itau/empresas/feature/home/NavigationListener;->abreTrasacoesFragment(Lcom/itau/empresas/CustomApplication;)V

    .line 58
    goto :goto_46

    .line 60
    :goto_44
    const/4 v0, 0x0

    return v0

    .line 63
    :goto_46
    const/4 v0, 0x1

    return v0

    :pswitch_data_48
    .packed-switch 0x7f0e06c7
        :pswitch_e
        :pswitch_1a
        :pswitch_40
        :pswitch_2a
        :pswitch_1e
    .end packed-switch
.end method

.method public recarregarFragmentAtual()V
    .registers 2

    .line 107
    iget-object v0, p0, Lcom/itau/empresas/feature/home/NavigationListener;->itemSelecionado:Landroid/view/MenuItem;

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/home/NavigationListener;->onNavigationItemSelected(Landroid/view/MenuItem;)Z

    .line 108
    return-void
.end method

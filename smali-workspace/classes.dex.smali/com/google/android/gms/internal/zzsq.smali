.class public final Lcom/google/android/gms/internal/zzsq;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Cloneable;


# static fields
.field private static final zzbum:Lcom/google/android/gms/internal/zzsr;


# instance fields
.field private mSize:I

.field private zzbun:Z

.field private zzbuo:[I

.field private zzbup:[Lcom/google/android/gms/internal/zzsr;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    new-instance v0, Lcom/google/android/gms/internal/zzsr;

    invoke-direct {v0}, Lcom/google/android/gms/internal/zzsr;-><init>()V

    sput-object v0, Lcom/google/android/gms/internal/zzsq;->zzbum:Lcom/google/android/gms/internal/zzsr;

    return-void
.end method

.method constructor <init>()V
    .registers 2

    const/16 v0, 0xa

    invoke-direct {p0, v0}, Lcom/google/android/gms/internal/zzsq;-><init>(I)V

    return-void
.end method

.method constructor <init>(I)V
    .registers 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/internal/zzsq;->zzbun:Z

    invoke-direct {p0, p1}, Lcom/google/android/gms/internal/zzsq;->idealIntArraySize(I)I

    move-result p1

    new-array v0, p1, [I

    iput-object v0, p0, Lcom/google/android/gms/internal/zzsq;->zzbuo:[I

    new-array v0, p1, [Lcom/google/android/gms/internal/zzsr;

    iput-object v0, p0, Lcom/google/android/gms/internal/zzsq;->zzbup:[Lcom/google/android/gms/internal/zzsr;

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/internal/zzsq;->mSize:I

    return-void
.end method

.method private gc()V
    .registers 8

    iget v1, p0, Lcom/google/android/gms/internal/zzsq;->mSize:I

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/gms/internal/zzsq;->zzbuo:[I

    iget-object v4, p0, Lcom/google/android/gms/internal/zzsq;->zzbup:[Lcom/google/android/gms/internal/zzsr;

    const/4 v5, 0x0

    :goto_8
    if-ge v5, v1, :cond_20

    aget-object v6, v4, v5

    sget-object v0, Lcom/google/android/gms/internal/zzsq;->zzbum:Lcom/google/android/gms/internal/zzsr;

    if-eq v6, v0, :cond_1d

    if-eq v5, v2, :cond_1b

    aget v0, v3, v5

    aput v0, v3, v2

    aput-object v6, v4, v2

    const/4 v0, 0x0

    aput-object v0, v4, v5

    :cond_1b
    add-int/lit8 v2, v2, 0x1

    :cond_1d
    add-int/lit8 v5, v5, 0x1

    goto :goto_8

    :cond_20
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/internal/zzsq;->zzbun:Z

    iput v2, p0, Lcom/google/android/gms/internal/zzsq;->mSize:I

    return-void
.end method

.method private idealByteArraySize(I)I
    .registers 5
    .param p1, "need"    # I

    const/4 v2, 0x4

    :goto_1
    const/16 v0, 0x20

    if-ge v2, v0, :cond_13

    const/4 v0, 0x1

    shl-int/2addr v0, v2

    add-int/lit8 v0, v0, -0xc

    if-gt p1, v0, :cond_10

    const/4 v0, 0x1

    shl-int/2addr v0, v2

    add-int/lit8 v0, v0, -0xc

    return v0

    :cond_10
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_13
    return p1
.end method

.method private idealIntArraySize(I)I
    .registers 4
    .param p1, "need"    # I

    mul-int/lit8 v0, p1, 0x4

    invoke-direct {p0, v0}, Lcom/google/android/gms/internal/zzsq;->idealByteArraySize(I)I

    move-result v0

    div-int/lit8 v0, v0, 0x4

    return v0
.end method

.method private zza([I[II)Z
    .registers 7

    const/4 v2, 0x0

    :goto_1
    if-ge v2, p3, :cond_e

    aget v0, p1, v2

    aget v1, p2, v2

    if-eq v0, v1, :cond_b

    const/4 v0, 0x0

    return v0

    :cond_b
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_e
    const/4 v0, 0x1

    return v0
.end method

.method private zza([Lcom/google/android/gms/internal/zzsr;[Lcom/google/android/gms/internal/zzsr;I)Z
    .registers 7

    const/4 v2, 0x0

    :goto_1
    if-ge v2, p3, :cond_12

    aget-object v0, p1, v2

    aget-object v1, p2, v2

    invoke-virtual {v0, v1}, Lcom/google/android/gms/internal/zzsr;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_f

    const/4 v0, 0x0

    return v0

    :cond_f
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_12
    const/4 v0, 0x1

    return v0
.end method

.method private zzmH(I)I
    .registers 8

    const/4 v2, 0x0

    iget v0, p0, Lcom/google/android/gms/internal/zzsq;->mSize:I

    add-int/lit8 v3, v0, -0x1

    :goto_5
    if-gt v2, v3, :cond_1b

    add-int v0, v2, v3

    ushr-int/lit8 v4, v0, 0x1

    iget-object v0, p0, Lcom/google/android/gms/internal/zzsq;->zzbuo:[I

    aget v5, v0, v4

    if-ge v5, p1, :cond_14

    add-int/lit8 v2, v4, 0x1

    goto :goto_1a

    :cond_14
    if-le v5, p1, :cond_19

    add-int/lit8 v3, v4, -0x1

    goto :goto_1a

    :cond_19
    return v4

    :goto_1a
    goto :goto_5

    :cond_1b
    xor-int/lit8 v0, v2, -0x1

    return v0
.end method


# virtual methods
.method public synthetic clone()Ljava/lang/Object;
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/gms/internal/zzsq;->zzJq()Lcom/google/android/gms/internal/zzsq;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 6
    .param p1, "o"    # Ljava/lang/Object;

    if-ne p1, p0, :cond_4

    const/4 v0, 0x1

    return v0

    :cond_4
    instance-of v0, p1, Lcom/google/android/gms/internal/zzsq;

    if-nez v0, :cond_a

    const/4 v0, 0x0

    return v0

    :cond_a
    move-object v3, p1

    check-cast v3, Lcom/google/android/gms/internal/zzsq;

    invoke-virtual {p0}, Lcom/google/android/gms/internal/zzsq;->size()I

    move-result v0

    invoke-virtual {v3}, Lcom/google/android/gms/internal/zzsq;->size()I

    move-result v1

    if-eq v0, v1, :cond_19

    const/4 v0, 0x0

    return v0

    :cond_19
    iget-object v0, p0, Lcom/google/android/gms/internal/zzsq;->zzbuo:[I

    iget-object v1, v3, Lcom/google/android/gms/internal/zzsq;->zzbuo:[I

    iget v2, p0, Lcom/google/android/gms/internal/zzsq;->mSize:I

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/gms/internal/zzsq;->zza([I[II)Z

    move-result v0

    if-eqz v0, :cond_33

    iget-object v0, p0, Lcom/google/android/gms/internal/zzsq;->zzbup:[Lcom/google/android/gms/internal/zzsr;

    iget-object v1, v3, Lcom/google/android/gms/internal/zzsq;->zzbup:[Lcom/google/android/gms/internal/zzsr;

    iget v2, p0, Lcom/google/android/gms/internal/zzsq;->mSize:I

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/gms/internal/zzsq;->zza([Lcom/google/android/gms/internal/zzsr;[Lcom/google/android/gms/internal/zzsr;I)Z

    move-result v0

    if-eqz v0, :cond_33

    const/4 v0, 0x1

    goto :goto_34

    :cond_33
    const/4 v0, 0x0

    :goto_34
    return v0
.end method

.method public hashCode()I
    .registers 5

    iget-boolean v0, p0, Lcom/google/android/gms/internal/zzsq;->zzbun:Z

    if-eqz v0, :cond_7

    invoke-direct {p0}, Lcom/google/android/gms/internal/zzsq;->gc()V

    :cond_7
    const/16 v2, 0x11

    const/4 v3, 0x0

    :goto_a
    iget v0, p0, Lcom/google/android/gms/internal/zzsq;->mSize:I

    if-ge v3, v0, :cond_25

    mul-int/lit8 v0, v2, 0x1f

    iget-object v1, p0, Lcom/google/android/gms/internal/zzsq;->zzbuo:[I

    aget v1, v1, v3

    add-int v2, v0, v1

    mul-int/lit8 v0, v2, 0x1f

    iget-object v1, p0, Lcom/google/android/gms/internal/zzsq;->zzbup:[Lcom/google/android/gms/internal/zzsr;

    aget-object v1, v1, v3

    invoke-virtual {v1}, Lcom/google/android/gms/internal/zzsr;->hashCode()I

    move-result v1

    add-int v2, v0, v1

    add-int/lit8 v3, v3, 0x1

    goto :goto_a

    :cond_25
    return v2
.end method

.method public isEmpty()Z
    .registers 2

    invoke-virtual {p0}, Lcom/google/android/gms/internal/zzsq;->size()I

    move-result v0

    if-nez v0, :cond_8

    const/4 v0, 0x1

    goto :goto_9

    :cond_8
    const/4 v0, 0x0

    :goto_9
    return v0
.end method

.method size()I
    .registers 2

    iget-boolean v0, p0, Lcom/google/android/gms/internal/zzsq;->zzbun:Z

    if-eqz v0, :cond_7

    invoke-direct {p0}, Lcom/google/android/gms/internal/zzsq;->gc()V

    :cond_7
    iget v0, p0, Lcom/google/android/gms/internal/zzsq;->mSize:I

    return v0
.end method

.method public final zzJq()Lcom/google/android/gms/internal/zzsq;
    .registers 8

    invoke-virtual {p0}, Lcom/google/android/gms/internal/zzsq;->size()I

    move-result v4

    new-instance v5, Lcom/google/android/gms/internal/zzsq;

    invoke-direct {v5, v4}, Lcom/google/android/gms/internal/zzsq;-><init>(I)V

    iget-object v0, p0, Lcom/google/android/gms/internal/zzsq;->zzbuo:[I

    iget-object v1, v5, Lcom/google/android/gms/internal/zzsq;->zzbuo:[I

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-static {v0, v2, v1, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    const/4 v6, 0x0

    :goto_13
    if-ge v6, v4, :cond_2a

    iget-object v0, p0, Lcom/google/android/gms/internal/zzsq;->zzbup:[Lcom/google/android/gms/internal/zzsr;

    aget-object v0, v0, v6

    if-eqz v0, :cond_27

    iget-object v0, v5, Lcom/google/android/gms/internal/zzsq;->zzbup:[Lcom/google/android/gms/internal/zzsr;

    iget-object v1, p0, Lcom/google/android/gms/internal/zzsq;->zzbup:[Lcom/google/android/gms/internal/zzsr;

    aget-object v1, v1, v6

    invoke-virtual {v1}, Lcom/google/android/gms/internal/zzsr;->zzJr()Lcom/google/android/gms/internal/zzsr;

    move-result-object v1

    aput-object v1, v0, v6

    :cond_27
    add-int/lit8 v6, v6, 0x1

    goto :goto_13

    :cond_2a
    iput v4, v5, Lcom/google/android/gms/internal/zzsq;->mSize:I

    return-object v5
.end method

.method zza(ILcom/google/android/gms/internal/zzsr;)V
    .registers 11

    invoke-direct {p0, p1}, Lcom/google/android/gms/internal/zzsq;->zzmH(I)I

    move-result v4

    if-ltz v4, :cond_c

    iget-object v0, p0, Lcom/google/android/gms/internal/zzsq;->zzbup:[Lcom/google/android/gms/internal/zzsr;

    aput-object p2, v0, v4

    goto/16 :goto_8d

    :cond_c
    xor-int/lit8 v4, v4, -0x1

    iget v0, p0, Lcom/google/android/gms/internal/zzsq;->mSize:I

    if-ge v4, v0, :cond_23

    iget-object v0, p0, Lcom/google/android/gms/internal/zzsq;->zzbup:[Lcom/google/android/gms/internal/zzsr;

    aget-object v0, v0, v4

    sget-object v1, Lcom/google/android/gms/internal/zzsq;->zzbum:Lcom/google/android/gms/internal/zzsr;

    if-ne v0, v1, :cond_23

    iget-object v0, p0, Lcom/google/android/gms/internal/zzsq;->zzbuo:[I

    aput p1, v0, v4

    iget-object v0, p0, Lcom/google/android/gms/internal/zzsq;->zzbup:[Lcom/google/android/gms/internal/zzsr;

    aput-object p2, v0, v4

    return-void

    :cond_23
    iget-boolean v0, p0, Lcom/google/android/gms/internal/zzsq;->zzbun:Z

    if-eqz v0, :cond_37

    iget v0, p0, Lcom/google/android/gms/internal/zzsq;->mSize:I

    iget-object v1, p0, Lcom/google/android/gms/internal/zzsq;->zzbuo:[I

    array-length v1, v1

    if-lt v0, v1, :cond_37

    invoke-direct {p0}, Lcom/google/android/gms/internal/zzsq;->gc()V

    invoke-direct {p0, p1}, Lcom/google/android/gms/internal/zzsq;->zzmH(I)I

    move-result v0

    xor-int/lit8 v4, v0, -0x1

    :cond_37
    iget v0, p0, Lcom/google/android/gms/internal/zzsq;->mSize:I

    iget-object v1, p0, Lcom/google/android/gms/internal/zzsq;->zzbuo:[I

    array-length v1, v1

    if-lt v0, v1, :cond_62

    iget v0, p0, Lcom/google/android/gms/internal/zzsq;->mSize:I

    add-int/lit8 v0, v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/gms/internal/zzsq;->idealIntArraySize(I)I

    move-result v5

    new-array v6, v5, [I

    new-array v7, v5, [Lcom/google/android/gms/internal/zzsr;

    iget-object v0, p0, Lcom/google/android/gms/internal/zzsq;->zzbuo:[I

    iget-object v1, p0, Lcom/google/android/gms/internal/zzsq;->zzbuo:[I

    array-length v1, v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-static {v0, v2, v6, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v0, p0, Lcom/google/android/gms/internal/zzsq;->zzbup:[Lcom/google/android/gms/internal/zzsr;

    iget-object v1, p0, Lcom/google/android/gms/internal/zzsq;->zzbup:[Lcom/google/android/gms/internal/zzsr;

    array-length v1, v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-static {v0, v2, v7, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v6, p0, Lcom/google/android/gms/internal/zzsq;->zzbuo:[I

    iput-object v7, p0, Lcom/google/android/gms/internal/zzsq;->zzbup:[Lcom/google/android/gms/internal/zzsr;

    :cond_62
    iget v0, p0, Lcom/google/android/gms/internal/zzsq;->mSize:I

    sub-int/2addr v0, v4

    if-eqz v0, :cond_7f

    iget-object v0, p0, Lcom/google/android/gms/internal/zzsq;->zzbuo:[I

    iget-object v1, p0, Lcom/google/android/gms/internal/zzsq;->zzbuo:[I

    add-int/lit8 v2, v4, 0x1

    iget v3, p0, Lcom/google/android/gms/internal/zzsq;->mSize:I

    sub-int/2addr v3, v4

    invoke-static {v0, v4, v1, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v0, p0, Lcom/google/android/gms/internal/zzsq;->zzbup:[Lcom/google/android/gms/internal/zzsr;

    iget-object v1, p0, Lcom/google/android/gms/internal/zzsq;->zzbup:[Lcom/google/android/gms/internal/zzsr;

    add-int/lit8 v2, v4, 0x1

    iget v3, p0, Lcom/google/android/gms/internal/zzsq;->mSize:I

    sub-int/2addr v3, v4

    invoke-static {v0, v4, v1, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_7f
    iget-object v0, p0, Lcom/google/android/gms/internal/zzsq;->zzbuo:[I

    aput p1, v0, v4

    iget-object v0, p0, Lcom/google/android/gms/internal/zzsq;->zzbup:[Lcom/google/android/gms/internal/zzsr;

    aput-object p2, v0, v4

    iget v0, p0, Lcom/google/android/gms/internal/zzsq;->mSize:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/gms/internal/zzsq;->mSize:I

    :goto_8d
    return-void
.end method

.method zzmF(I)Lcom/google/android/gms/internal/zzsr;
    .registers 5

    invoke-direct {p0, p1}, Lcom/google/android/gms/internal/zzsq;->zzmH(I)I

    move-result v2

    if-ltz v2, :cond_e

    iget-object v0, p0, Lcom/google/android/gms/internal/zzsq;->zzbup:[Lcom/google/android/gms/internal/zzsr;

    aget-object v0, v0, v2

    sget-object v1, Lcom/google/android/gms/internal/zzsq;->zzbum:Lcom/google/android/gms/internal/zzsr;

    if-ne v0, v1, :cond_10

    :cond_e
    const/4 v0, 0x0

    return-object v0

    :cond_10
    iget-object v0, p0, Lcom/google/android/gms/internal/zzsq;->zzbup:[Lcom/google/android/gms/internal/zzsr;

    aget-object v0, v0, v2

    return-object v0
.end method

.method zzmG(I)Lcom/google/android/gms/internal/zzsr;
    .registers 3

    iget-boolean v0, p0, Lcom/google/android/gms/internal/zzsq;->zzbun:Z

    if-eqz v0, :cond_7

    invoke-direct {p0}, Lcom/google/android/gms/internal/zzsq;->gc()V

    :cond_7
    iget-object v0, p0, Lcom/google/android/gms/internal/zzsq;->zzbup:[Lcom/google/android/gms/internal/zzsr;

    aget-object v0, v0, p1

    return-object v0
.end method

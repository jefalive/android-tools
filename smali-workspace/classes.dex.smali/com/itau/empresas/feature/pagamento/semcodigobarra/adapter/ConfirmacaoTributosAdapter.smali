.class public Lcom/itau/empresas/feature/pagamento/semcodigobarra/adapter/ConfirmacaoTributosAdapter;
.super Landroid/widget/BaseAdapter;
.source "ConfirmacaoTributosAdapter.java"


# instance fields
.field private list:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosConfirmacaoTributo;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 17
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public getCount()I
    .registers 2

    .line 27
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/adapter/ConfirmacaoTributosAdapter;->list:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosConfirmacaoTributo;
    .registers 3
    .param p1, "position"    # I

    .line 32
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/adapter/ConfirmacaoTributosAdapter;->list:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosConfirmacaoTributo;

    return-object v0
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .registers 3

    .line 17
    invoke-virtual {p0, p1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/adapter/ConfirmacaoTributosAdapter;->getItem(I)Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosConfirmacaoTributo;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .registers 4
    .param p1, "position"    # I

    .line 37
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 11
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .line 42
    .line 43
    invoke-virtual {p3}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Landroid/view/LayoutInflater;

    .line 44
    .local v2, "inflater":Landroid/view/LayoutInflater;
    if-nez p2, :cond_18

    const v0, 0x7f0300ec

    const/4 v1, 0x0

    invoke-virtual {v2, v0, p3, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    goto :goto_19

    :cond_18
    move-object v3, p2

    .line 46
    .local v3, "view":Landroid/view/View;
    :goto_19
    invoke-virtual {p0, p1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/adapter/ConfirmacaoTributosAdapter;->getItem(I)Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosConfirmacaoTributo;

    move-result-object v4

    .line 47
    .local v4, "dadosComprovante":Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosConfirmacaoTributo;
    const v0, 0x7f0e0505

    invoke-static {v3, v0}, Lcom/itau/empresas/ui/util/ViewHolder;->get(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Landroid/widget/TextView;

    .line 48
    .local v5, "label":Landroid/widget/TextView;
    const v0, 0x7f0e0506

    invoke-static {v3, v0}, Lcom/itau/empresas/ui/util/ViewHolder;->get(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Landroid/widget/TextView;

    .line 49
    .local v6, "valor":Landroid/widget/TextView;
    if-eqz v4, :cond_41

    .line 50
    invoke-virtual {v4}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosConfirmacaoTributo;->getValor()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 51
    invoke-virtual {v4}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosConfirmacaoTributo;->getLabel()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 53
    :cond_41
    if-eqz v4, :cond_66

    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v4}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosConfirmacaoTributo;->isViewChange()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_66

    .line 54
    invoke-virtual {p3}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 55
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 56
    const v1, 0x7f0801d5

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    .line 54
    const/4 v1, 0x0

    invoke-virtual {v6, v1, v0}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 60
    :cond_66
    return-object v3
.end method

.method public setList(Ljava/util/List;)V
    .registers 2
    .param p1, "list"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosConfirmacaoTributo;>;)V"
        }
    .end annotation

    .line 22
    iput-object p1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/adapter/ConfirmacaoTributosAdapter;->list:Ljava/util/List;

    .line 23
    return-void
.end method

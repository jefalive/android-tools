.class public final Lbr/com/concretesolutions/canarinho/validator/ValidadorBoleto;
.super Ljava/lang/Object;
.source "ValidadorBoleto.java"

# interfaces
.implements Lbr/com/concretesolutions/canarinho/validator/Validador;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lbr/com/concretesolutions/canarinho/validator/ValidadorBoleto$SingletonHolder;
    }
.end annotation


# static fields
.field public static final MOD_10:Lbr/com/concretesolutions/canarinho/DigitoPara;

.field public static final MOD_11:Lbr/com/concretesolutions/canarinho/DigitoPara;

.field private static final PADRAO_APENAS_NUMEROS:Ljava/util/regex/Pattern;

.field private static final PADRAO_PARA_LIMPAR:Ljava/util/regex/Pattern;


# direct methods
.method static constructor <clinit>()V
    .registers 5

    .line 21
    new-instance v0, Lbr/com/concretesolutions/canarinho/DigitoPara$Builder;

    invoke-direct {v0}, Lbr/com/concretesolutions/canarinho/DigitoPara$Builder;-><init>()V

    .line 22
    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Lbr/com/concretesolutions/canarinho/DigitoPara$Builder;->mod(I)Lbr/com/concretesolutions/canarinho/DigitoPara$Builder;

    move-result-object v0

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Integer;

    .line 23
    const/4 v2, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x1

    aput-object v2, v1, v3

    invoke-virtual {v0, v1}, Lbr/com/concretesolutions/canarinho/DigitoPara$Builder;->comMultiplicadores([Ljava/lang/Integer;)Lbr/com/concretesolutions/canarinho/DigitoPara$Builder;

    move-result-object v0

    .line 24
    invoke-virtual {v0}, Lbr/com/concretesolutions/canarinho/DigitoPara$Builder;->somandoIndividualmente()Lbr/com/concretesolutions/canarinho/DigitoPara$Builder;

    move-result-object v0

    const-string v1, "0"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Integer;

    .line 25
    const/16 v3, 0xa

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const/4 v4, 0x0

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Lbr/com/concretesolutions/canarinho/DigitoPara$Builder;->trocandoPorSeEncontrar(Ljava/lang/String;[Ljava/lang/Integer;)Lbr/com/concretesolutions/canarinho/DigitoPara$Builder;

    move-result-object v0

    .line 26
    invoke-virtual {v0}, Lbr/com/concretesolutions/canarinho/DigitoPara$Builder;->complementarAoModulo()Lbr/com/concretesolutions/canarinho/DigitoPara$Builder;

    move-result-object v0

    .line 27
    invoke-virtual {v0}, Lbr/com/concretesolutions/canarinho/DigitoPara$Builder;->build()Lbr/com/concretesolutions/canarinho/DigitoPara;

    move-result-object v0

    sput-object v0, Lbr/com/concretesolutions/canarinho/validator/ValidadorBoleto;->MOD_10:Lbr/com/concretesolutions/canarinho/DigitoPara;

    .line 32
    new-instance v0, Lbr/com/concretesolutions/canarinho/DigitoPara$Builder;

    invoke-direct {v0}, Lbr/com/concretesolutions/canarinho/DigitoPara$Builder;-><init>()V

    const-string v1, "0"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Integer;

    .line 33
    const/16 v3, 0xa

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const/4 v4, 0x0

    aput-object v3, v2, v4

    const/16 v3, 0xb

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const/4 v4, 0x1

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Lbr/com/concretesolutions/canarinho/DigitoPara$Builder;->trocandoPorSeEncontrar(Ljava/lang/String;[Ljava/lang/Integer;)Lbr/com/concretesolutions/canarinho/DigitoPara$Builder;

    move-result-object v0

    .line 34
    invoke-virtual {v0}, Lbr/com/concretesolutions/canarinho/DigitoPara$Builder;->complementarAoModulo()Lbr/com/concretesolutions/canarinho/DigitoPara$Builder;

    move-result-object v0

    .line 35
    invoke-virtual {v0}, Lbr/com/concretesolutions/canarinho/DigitoPara$Builder;->build()Lbr/com/concretesolutions/canarinho/DigitoPara;

    move-result-object v0

    sput-object v0, Lbr/com/concretesolutions/canarinho/validator/ValidadorBoleto;->MOD_11:Lbr/com/concretesolutions/canarinho/DigitoPara;

    .line 37
    const-string v0, "[\\s.]"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lbr/com/concretesolutions/canarinho/validator/ValidadorBoleto;->PADRAO_PARA_LIMPAR:Ljava/util/regex/Pattern;

    .line 38
    const-string v0, "[\\d]*"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lbr/com/concretesolutions/canarinho/validator/ValidadorBoleto;->PADRAO_APENAS_NUMEROS:Ljava/util/regex/Pattern;

    return-void
.end method

.method private constructor <init>()V
    .registers 1

    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    return-void
.end method

.method synthetic constructor <init>(Lbr/com/concretesolutions/canarinho/validator/ValidadorBoleto$1;)V
    .registers 2
    .param p1, "x0"    # Lbr/com/concretesolutions/canarinho/validator/ValidadorBoleto$1;

    .line 16
    invoke-direct {p0}, Lbr/com/concretesolutions/canarinho/validator/ValidadorBoleto;-><init>()V

    return-void
.end method

.method private ehTributo(Ljava/lang/CharSequence;)Z
    .registers 4
    .param p1, "valor"    # Ljava/lang/CharSequence;

    .line 137
    const/4 v0, 0x0

    invoke-interface {p1, v0}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    const/16 v1, 0x38

    if-ne v0, v1, :cond_b

    const/4 v0, 0x1

    goto :goto_c

    :cond_b
    const/4 v0, 0x0

    :goto_c
    return v0
.end method

.method public static getInstance()Lbr/com/concretesolutions/canarinho/validator/ValidadorBoleto;
    .registers 1

    .line 45
    # getter for: Lbr/com/concretesolutions/canarinho/validator/ValidadorBoleto$SingletonHolder;->INSTANCE:Lbr/com/concretesolutions/canarinho/validator/ValidadorBoleto;
    invoke-static {}, Lbr/com/concretesolutions/canarinho/validator/ValidadorBoleto$SingletonHolder;->access$000()Lbr/com/concretesolutions/canarinho/validator/ValidadorBoleto;

    move-result-object v0

    return-object v0
.end method

.method private validaBloco(Ljava/lang/String;Lbr/com/concretesolutions/canarinho/validator/Validador$ResultadoParcial;Lbr/com/concretesolutions/canarinho/DigitoPara;IILjava/lang/String;)Z
    .registers 11
    .param p1, "valor"    # Ljava/lang/String;
    .param p2, "resultadoParcial"    # Lbr/com/concretesolutions/canarinho/validator/Validador$ResultadoParcial;
    .param p3, "mod"    # Lbr/com/concretesolutions/canarinho/DigitoPara;
    .param p4, "tamanhoMinimo"    # I
    .param p5, "st"    # I
    .param p6, "mensagem"    # Ljava/lang/String;

    .line 143
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-ge v0, p4, :cond_c

    .line 144
    const/4 v0, 0x1

    invoke-virtual {p2, v0}, Lbr/com/concretesolutions/canarinho/validator/Validador$ResultadoParcial;->parcialmenteValido(Z)Lbr/com/concretesolutions/canarinho/validator/Validador$ResultadoParcial;

    .line 145
    const/4 v0, 0x0

    return v0

    .line 148
    :cond_c
    add-int/lit8 v2, p4, -0x1

    .line 150
    .local v2, "end":I
    invoke-virtual {p1, p5, v2}, Ljava/lang/String;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Lbr/com/concretesolutions/canarinho/DigitoPara;->calcula(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v3

    .line 152
    .local v3, "digito":C
    invoke-virtual {p1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    if-eq v3, v0, :cond_46

    .line 153
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " bloco inv\u00e1lido"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 154
    invoke-virtual {p2, v0}, Lbr/com/concretesolutions/canarinho/validator/Validador$ResultadoParcial;->mensagem(Ljava/lang/String;)Lbr/com/concretesolutions/canarinho/validator/Validador$ResultadoParcial;

    move-result-object v0

    .line 155
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lbr/com/concretesolutions/canarinho/validator/Validador$ResultadoParcial;->parcialmenteValido(Z)Lbr/com/concretesolutions/canarinho/validator/Validador$ResultadoParcial;

    move-result-object v0

    .line 156
    invoke-virtual {v0}, Lbr/com/concretesolutions/canarinho/validator/Validador$ResultadoParcial;->isParcialmenteValido()Z

    move-result v0

    return v0

    .line 159
    :cond_46
    const/4 v0, 0x1

    return v0
.end method

.method private validaNormal(Ljava/lang/String;Lbr/com/concretesolutions/canarinho/validator/Validador$ResultadoParcial;)Lbr/com/concretesolutions/canarinho/validator/Validador$ResultadoParcial;
    .registers 10
    .param p1, "valor"    # Ljava/lang/String;
    .param p2, "rParcial"    # Lbr/com/concretesolutions/canarinho/validator/Validador$ResultadoParcial;

    .line 87
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    sget-object v3, Lbr/com/concretesolutions/canarinho/validator/ValidadorBoleto;->MOD_10:Lbr/com/concretesolutions/canarinho/DigitoPara;

    const-string v6, "Primeiro"

    const/16 v4, 0xa

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v6}, Lbr/com/concretesolutions/canarinho/validator/ValidadorBoleto;->validaBloco(Ljava/lang/String;Lbr/com/concretesolutions/canarinho/validator/Validador$ResultadoParcial;Lbr/com/concretesolutions/canarinho/DigitoPara;IILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_11

    .line 88
    return-object p2

    .line 91
    :cond_11
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    sget-object v3, Lbr/com/concretesolutions/canarinho/validator/ValidadorBoleto;->MOD_10:Lbr/com/concretesolutions/canarinho/DigitoPara;

    const-string v6, "Segundo"

    const/16 v4, 0x15

    const/16 v5, 0xa

    invoke-direct/range {v0 .. v6}, Lbr/com/concretesolutions/canarinho/validator/ValidadorBoleto;->validaBloco(Ljava/lang/String;Lbr/com/concretesolutions/canarinho/validator/Validador$ResultadoParcial;Lbr/com/concretesolutions/canarinho/DigitoPara;IILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_23

    .line 92
    return-object p2

    .line 95
    :cond_23
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    sget-object v3, Lbr/com/concretesolutions/canarinho/validator/ValidadorBoleto;->MOD_10:Lbr/com/concretesolutions/canarinho/DigitoPara;

    const-string v6, "Terceiro"

    const/16 v4, 0x20

    const/16 v5, 0x15

    invoke-direct/range {v0 .. v6}, Lbr/com/concretesolutions/canarinho/validator/ValidadorBoleto;->validaBloco(Ljava/lang/String;Lbr/com/concretesolutions/canarinho/validator/Validador$ResultadoParcial;Lbr/com/concretesolutions/canarinho/DigitoPara;IILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_35

    .line 96
    return-object p2

    .line 99
    :cond_35
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v1, 0x2f

    if-ge v0, v1, :cond_3e

    .line 100
    return-object p2

    .line 103
    :cond_3e
    const/4 v0, 0x1

    invoke-virtual {p2, v0}, Lbr/com/concretesolutions/canarinho/validator/Validador$ResultadoParcial;->parcialmenteValido(Z)Lbr/com/concretesolutions/canarinho/validator/Validador$ResultadoParcial;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lbr/com/concretesolutions/canarinho/validator/Validador$ResultadoParcial;->totalmenteValido(Z)Lbr/com/concretesolutions/canarinho/validator/Validador$ResultadoParcial;

    move-result-object v0

    return-object v0
.end method

.method private validaTributo(Ljava/lang/String;Lbr/com/concretesolutions/canarinho/validator/Validador$ResultadoParcial;)Lbr/com/concretesolutions/canarinho/validator/Validador$ResultadoParcial;
    .registers 12
    .param p1, "valor"    # Ljava/lang/String;
    .param p2, "rParcial"    # Lbr/com/concretesolutions/canarinho/validator/Validador$ResultadoParcial;

    .line 108
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x3

    if-ge v0, v1, :cond_d

    .line 109
    const/4 v0, 0x1

    invoke-virtual {p2, v0}, Lbr/com/concretesolutions/canarinho/validator/Validador$ResultadoParcial;->parcialmenteValido(Z)Lbr/com/concretesolutions/canarinho/validator/Validador$ResultadoParcial;

    move-result-object v0

    return-object v0

    .line 113
    :cond_d
    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v0

    const/16 v1, 0x36

    if-eq v0, v1, :cond_1f

    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v0

    const/16 v1, 0x37

    if-ne v0, v1, :cond_21

    :cond_1f
    const/4 v7, 0x1

    goto :goto_22

    :cond_21
    const/4 v7, 0x0

    .line 114
    .local v7, "ehMod10":Z
    :goto_22
    if-eqz v7, :cond_27

    sget-object v8, Lbr/com/concretesolutions/canarinho/validator/ValidadorBoleto;->MOD_10:Lbr/com/concretesolutions/canarinho/DigitoPara;

    goto :goto_29

    :cond_27
    sget-object v8, Lbr/com/concretesolutions/canarinho/validator/ValidadorBoleto;->MOD_11:Lbr/com/concretesolutions/canarinho/DigitoPara;

    .line 116
    .local v8, "digitoPara":Lbr/com/concretesolutions/canarinho/DigitoPara;
    :goto_29
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, v8

    const-string v6, "Primeiro"

    const/16 v4, 0xc

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v6}, Lbr/com/concretesolutions/canarinho/validator/ValidadorBoleto;->validaBloco(Ljava/lang/String;Lbr/com/concretesolutions/canarinho/validator/Validador$ResultadoParcial;Lbr/com/concretesolutions/canarinho/DigitoPara;IILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_39

    .line 117
    return-object p2

    .line 120
    :cond_39
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, v8

    const-string v6, "Segundo"

    const/16 v4, 0x18

    const/16 v5, 0xc

    invoke-direct/range {v0 .. v6}, Lbr/com/concretesolutions/canarinho/validator/ValidadorBoleto;->validaBloco(Ljava/lang/String;Lbr/com/concretesolutions/canarinho/validator/Validador$ResultadoParcial;Lbr/com/concretesolutions/canarinho/DigitoPara;IILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4a

    .line 121
    return-object p2

    .line 124
    :cond_4a
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, v8

    const-string v6, "Terceiro"

    const/16 v4, 0x24

    const/16 v5, 0x18

    invoke-direct/range {v0 .. v6}, Lbr/com/concretesolutions/canarinho/validator/ValidadorBoleto;->validaBloco(Ljava/lang/String;Lbr/com/concretesolutions/canarinho/validator/Validador$ResultadoParcial;Lbr/com/concretesolutions/canarinho/DigitoPara;IILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5b

    .line 125
    return-object p2

    .line 128
    :cond_5b
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, v8

    const-string v6, "Quarto"

    const/16 v4, 0x30

    const/16 v5, 0x24

    invoke-direct/range {v0 .. v6}, Lbr/com/concretesolutions/canarinho/validator/ValidadorBoleto;->validaBloco(Ljava/lang/String;Lbr/com/concretesolutions/canarinho/validator/Validador$ResultadoParcial;Lbr/com/concretesolutions/canarinho/DigitoPara;IILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_6c

    .line 129
    return-object p2

    .line 133
    :cond_6c
    const/4 v0, 0x1

    invoke-virtual {p2, v0}, Lbr/com/concretesolutions/canarinho/validator/Validador$ResultadoParcial;->parcialmenteValido(Z)Lbr/com/concretesolutions/canarinho/validator/Validador$ResultadoParcial;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lbr/com/concretesolutions/canarinho/validator/Validador$ResultadoParcial;->totalmenteValido(Z)Lbr/com/concretesolutions/canarinho/validator/Validador$ResultadoParcial;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public ehValido(Landroid/text/Editable;Lbr/com/concretesolutions/canarinho/validator/Validador$ResultadoParcial;)Lbr/com/concretesolutions/canarinho/validator/Validador$ResultadoParcial;
    .registers 6
    .param p1, "valor"    # Landroid/text/Editable;
    .param p2, "resultadoParcial"    # Lbr/com/concretesolutions/canarinho/validator/Validador$ResultadoParcial;

    .line 62
    if-eqz p2, :cond_4

    if-nez p1, :cond_c

    .line 63
    :cond_4
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Campos n\u00e3o podem ser nulos"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 66
    :cond_c
    sget-object v0, Lbr/com/concretesolutions/canarinho/validator/ValidadorBoleto;->PADRAO_PARA_LIMPAR:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/util/regex/Matcher;->replaceAll(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 68
    .local v2, "valorDesformatado":Ljava/lang/String;
    sget-object v0, Lbr/com/concretesolutions/canarinho/validator/ValidadorBoleto;->PADRAO_APENAS_NUMEROS:Ljava/util/regex/Pattern;

    invoke-virtual {v0, v2}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    if-nez v0, :cond_2c

    .line 69
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Apenas n\u00fameros, \'.\' e espa\u00e7os s\u00e3o v\u00e1lidos"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 72
    :cond_2c
    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Lbr/com/concretesolutions/canarinho/validator/Validador$ResultadoParcial;->totalmenteValido(Z)Lbr/com/concretesolutions/canarinho/validator/Validador$ResultadoParcial;

    .line 74
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_42

    .line 75
    .line 76
    const/4 v0, 0x1

    invoke-virtual {p2, v0}, Lbr/com/concretesolutions/canarinho/validator/Validador$ResultadoParcial;->parcialmenteValido(Z)Lbr/com/concretesolutions/canarinho/validator/Validador$ResultadoParcial;

    move-result-object v0

    const-string v1, ""

    .line 77
    invoke-virtual {v0, v1}, Lbr/com/concretesolutions/canarinho/validator/Validador$ResultadoParcial;->mensagem(Ljava/lang/String;)Lbr/com/concretesolutions/canarinho/validator/Validador$ResultadoParcial;

    move-result-object v0

    return-object v0

    .line 80
    :cond_42
    invoke-direct {p0, v2}, Lbr/com/concretesolutions/canarinho/validator/ValidadorBoleto;->ehTributo(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4d

    .line 81
    invoke-direct {p0, v2, p2}, Lbr/com/concretesolutions/canarinho/validator/ValidadorBoleto;->validaTributo(Ljava/lang/String;Lbr/com/concretesolutions/canarinho/validator/Validador$ResultadoParcial;)Lbr/com/concretesolutions/canarinho/validator/Validador$ResultadoParcial;

    move-result-object v0

    goto :goto_51

    .line 82
    :cond_4d
    invoke-direct {p0, v2, p2}, Lbr/com/concretesolutions/canarinho/validator/ValidadorBoleto;->validaNormal(Ljava/lang/String;Lbr/com/concretesolutions/canarinho/validator/Validador$ResultadoParcial;)Lbr/com/concretesolutions/canarinho/validator/Validador$ResultadoParcial;

    move-result-object v0

    :goto_51
    return-object v0
.end method

.method public ehValido(Ljava/lang/String;)Z
    .registers 5
    .param p1, "valor"    # Ljava/lang/String;

    .line 51
    if-nez p1, :cond_a

    .line 52
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Campos n\u00e3o podem ser nulos"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 55
    :cond_a
    sget-object v0, Lbr/com/concretesolutions/canarinho/formatador/Formatador$Padroes;->PADRAO_SOMENTE_NUMEROS:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/util/regex/Matcher;->replaceAll(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 56
    .local v2, "valorSemFormatacao":Ljava/lang/String;
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0, v2}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    new-instance v1, Lbr/com/concretesolutions/canarinho/validator/Validador$ResultadoParcial;

    invoke-direct {v1}, Lbr/com/concretesolutions/canarinho/validator/Validador$ResultadoParcial;-><init>()V

    invoke-virtual {p0, v0, v1}, Lbr/com/concretesolutions/canarinho/validator/ValidadorBoleto;->ehValido(Landroid/text/Editable;Lbr/com/concretesolutions/canarinho/validator/Validador$ResultadoParcial;)Lbr/com/concretesolutions/canarinho/validator/Validador$ResultadoParcial;

    move-result-object v0

    invoke-virtual {v0}, Lbr/com/concretesolutions/canarinho/validator/Validador$ResultadoParcial;->isValido()Z

    move-result v0

    return v0
.end method

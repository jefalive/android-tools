.class Lcom/itau/empresas/feature/login/PreLoginWhatsNew;
.super Ljava/lang/Object;
.source "PreLoginWhatsNew.java"


# instance fields
.field private final preLoginActivity:Lcom/itau/empresas/feature/login/PreLoginActivity;


# direct methods
.method constructor <init>(Lcom/itau/empresas/feature/login/PreLoginActivity;)V
    .registers 2
    .param p1, "preLoginActivity"    # Lcom/itau/empresas/feature/login/PreLoginActivity;

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/itau/empresas/feature/login/PreLoginWhatsNew;->preLoginActivity:Lcom/itau/empresas/feature/login/PreLoginActivity;

    .line 23
    return-void
.end method


# virtual methods
.method deveExibirWhatsNews()Z
    .registers 7

    .line 27
    iget-object v0, p0, Lcom/itau/empresas/feature/login/PreLoginWhatsNew;->preLoginActivity:Lcom/itau/empresas/feature/login/PreLoginActivity;

    const-string v1, "whats-new"

    .line 28
    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/itau/empresas/feature/login/PreLoginActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v3

    .line 29
    .local v3, "prefs":Landroid/content/SharedPreferences;
    invoke-static {}, Lcom/itau/empresas/ui/util/VersaoUtils;->versaoSemMinor()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v3, v0, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    .line 31
    .local v4, "jaLeuWhatsNew":Z
    if-nez v4, :cond_22

    .line 32
    new-instance v5, Landroid/content/Intent;

    iget-object v0, p0, Lcom/itau/empresas/feature/login/PreLoginWhatsNew;->preLoginActivity:Lcom/itau/empresas/feature/login/PreLoginActivity;

    const-class v1, Lcom/itau/empresas/feature/whatsnew/WhatsNewIntroducaoActivity;

    invoke-direct {v5, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 33
    .local v5, "intent":Landroid/content/Intent;
    iget-object v0, p0, Lcom/itau/empresas/feature/login/PreLoginWhatsNew;->preLoginActivity:Lcom/itau/empresas/feature/login/PreLoginActivity;

    invoke-virtual {v0, v5}, Lcom/itau/empresas/feature/login/PreLoginActivity;->startActivity(Landroid/content/Intent;)V

    .line 36
    .end local v5    # "intent":Landroid/content/Intent;
    :cond_22
    const/4 v0, 0x0

    return v0
.end method

.class public Lcom/itau/empresas/api/model/ComprovanteServiceVO;
.super Ljava/lang/Object;
.source "ComprovanteServiceVO.java"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::Ljava/io/Serializable;>Ljava/lang/Object;Ljava/io/Serializable;"
    }
.end annotation


# instance fields
.field private agenciaTransacao:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/Expose;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "agencia_transacao"
    .end annotation
.end field

.field private autenticacaoCaixa:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/Expose;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "autenticacao_caixa"
    .end annotation
.end field

.field private autenticacaoDigital:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/Expose;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "autenticacao_digital"
    .end annotation
.end field

.field private autenticacaoHistorica:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/Expose;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "autenticacao_historica"
    .end annotation
.end field

.field private bandeiraCartao:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/Expose;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "bandeira_cartao"
    .end annotation
.end field

.field private codigoBandeiraCartao:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/Expose;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "codigo_bandeira_cartao"
    .end annotation
.end field

.field private codigoTransacao:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/Expose;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "codigo_transacao"
    .end annotation
.end field

.field private comprovantes:Ljava/util/List;
    .annotation runtime Lcom/google/gson/annotations/Expose;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "campos_comprovante"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<TT;>;"
        }
    .end annotation
.end field

.field private dataEfetivaTransacao:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/Expose;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "data_efetiva_transacao"
    .end annotation
.end field

.field private dataRealTransacao:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/Expose;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "data_real_transacao"
    .end annotation
.end field

.field private descricaoExtrato:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/Expose;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "descricao_extrato"
    .end annotation
.end field

.field private formaPagamentoCaixa:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/Expose;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "forma_pagamento_caixa"
    .end annotation
.end field

.field private horaRealTransacao:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/Expose;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "hora_real_transacao"
    .end annotation
.end field

.field private idCartao:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/Expose;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "id_cartao"
    .end annotation
.end field

.field private idComprovante:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/Expose;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "id_comprovante"
    .end annotation
.end field

.field private nomeAgenciaTransacao:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/Expose;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "nome_agencia_transacao"
    .end annotation
.end field

.field private nomeCanal:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/Expose;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "nome_canal"
    .end annotation
.end field

.field private nomeCliente:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/Expose;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "nome_cliente"
    .end annotation
.end field

.field private nomeTitular:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/Expose;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "nome_titular"
    .end annotation
.end field

.field private numeroOperador:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/Expose;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "numero_operador"
    .end annotation
.end field

.field private numeroRegistroCaixa:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/Expose;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "numero_registro_caixa"
    .end annotation
.end field

.field private quantidadeRegistros:Ljava/lang/Integer;
    .annotation runtime Lcom/google/gson/annotations/Expose;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "quantidade_registros"
    .end annotation
.end field

.field private textoCliente:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/Expose;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "texto_cliente"
    .end annotation
.end field

.field private ultimosDigitosCartao:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/Expose;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "ultimos_digitos_cartao"
    .end annotation
.end field

.field private valorAutenticacaoCaixa:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/Expose;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "valor_autenticacao_caixa"
    .end annotation
.end field

.field private valorTransacao:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/Expose;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "valor_transacao"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getAutenticacaoDigital()Ljava/lang/String;
    .registers 2

    .line 116
    iget-object v0, p0, Lcom/itau/empresas/api/model/ComprovanteServiceVO;->autenticacaoDigital:Ljava/lang/String;

    return-object v0
.end method

.method public getComprovantes()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<TT;>;"
        }
    .end annotation

    .line 192
    iget-object v0, p0, Lcom/itau/empresas/api/model/ComprovanteServiceVO;->comprovantes:Ljava/util/List;

    return-object v0
.end method

.method public getDataEfetivaTransacao()Ljava/lang/String;
    .registers 2

    .line 104
    iget-object v0, p0, Lcom/itau/empresas/api/model/ComprovanteServiceVO;->dataEfetivaTransacao:Ljava/lang/String;

    return-object v0
.end method

.method public getDataRealTransacao()Ljava/lang/String;
    .registers 2

    .line 108
    iget-object v0, p0, Lcom/itau/empresas/api/model/ComprovanteServiceVO;->dataRealTransacao:Ljava/lang/String;

    return-object v0
.end method

.method public getHoraRealTransacao()Ljava/lang/String;
    .registers 2

    .line 112
    iget-object v0, p0, Lcom/itau/empresas/api/model/ComprovanteServiceVO;->horaRealTransacao:Ljava/lang/String;

    return-object v0
.end method

.method public getNomeCanal()Ljava/lang/String;
    .registers 2

    .line 128
    iget-object v0, p0, Lcom/itau/empresas/api/model/ComprovanteServiceVO;->nomeCanal:Ljava/lang/String;

    return-object v0
.end method

.method public getNomeCliente()Ljava/lang/String;
    .registers 2

    .line 100
    iget-object v0, p0, Lcom/itau/empresas/api/model/ComprovanteServiceVO;->nomeCliente:Ljava/lang/String;

    return-object v0
.end method

.method public getValorTransacao()Ljava/lang/String;
    .registers 2

    .line 120
    iget-object v0, p0, Lcom/itau/empresas/api/model/ComprovanteServiceVO;->valorTransacao:Ljava/lang/String;

    return-object v0
.end method

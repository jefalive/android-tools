.class abstract Landroid/support/transition/TransitionPort;
.super Ljava/lang/Object;
.source "TransitionPort.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/support/transition/TransitionPort$AnimationInfo;,
        Landroid/support/transition/TransitionPort$TransitionListenerAdapter;,
        Landroid/support/transition/TransitionPort$TransitionListener;
    }
.end annotation


# static fields
.field private static sRunningAnimators:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal<Landroid/support/v4/util/ArrayMap<Landroid/animation/Animator;Landroid/support/transition/TransitionPort$AnimationInfo;>;>;"
        }
    .end annotation
.end field


# instance fields
.field mAnimators:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<Landroid/animation/Animator;>;"
        }
    .end annotation
.end field

.field mCanRemoveViews:Z

.field mCurrentAnimators:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<Landroid/animation/Animator;>;"
        }
    .end annotation
.end field

.field mDuration:J

.field private mEndValues:Landroid/support/transition/TransitionValuesMaps;

.field private mEnded:Z

.field mInterpolator:Landroid/animation/TimeInterpolator;

.field mListeners:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<Landroid/support/transition/TransitionPort$TransitionListener;>;"
        }
    .end annotation
.end field

.field private mName:Ljava/lang/String;

.field mNumInstances:I

.field mParent:Landroid/support/transition/TransitionSetPort;

.field mPaused:Z

.field mSceneRoot:Landroid/view/ViewGroup;

.field mStartDelay:J

.field private mStartValues:Landroid/support/transition/TransitionValuesMaps;

.field mTargetChildExcludes:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<Landroid/view/View;>;"
        }
    .end annotation
.end field

.field mTargetExcludes:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<Landroid/view/View;>;"
        }
    .end annotation
.end field

.field mTargetIdChildExcludes:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
        }
    .end annotation
.end field

.field mTargetIdExcludes:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
        }
    .end annotation
.end field

.field mTargetIds:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
        }
    .end annotation
.end field

.field mTargetTypeChildExcludes:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<Ljava/lang/Class;>;"
        }
    .end annotation
.end field

.field mTargetTypeExcludes:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<Ljava/lang/Class;>;"
        }
    .end annotation
.end field

.field mTargets:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<Landroid/view/View;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .line 43
    new-instance v0, Ljava/lang/ThreadLocal;

    invoke-direct {v0}, Ljava/lang/ThreadLocal;-><init>()V

    sput-object v0, Landroid/support/transition/TransitionPort;->sRunningAnimators:Ljava/lang/ThreadLocal;

    return-void
.end method

.method public constructor <init>()V
    .registers 3

    .line 117
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Landroid/support/transition/TransitionPort;->mStartDelay:J

    .line 48
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Landroid/support/transition/TransitionPort;->mDuration:J

    .line 50
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/transition/TransitionPort;->mInterpolator:Landroid/animation/TimeInterpolator;

    .line 52
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/transition/TransitionPort;->mTargetIds:Ljava/util/ArrayList;

    .line 54
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/transition/TransitionPort;->mTargets:Ljava/util/ArrayList;

    .line 56
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/transition/TransitionPort;->mTargetIdExcludes:Ljava/util/ArrayList;

    .line 58
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/transition/TransitionPort;->mTargetExcludes:Ljava/util/ArrayList;

    .line 60
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/transition/TransitionPort;->mTargetTypeExcludes:Ljava/util/ArrayList;

    .line 62
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/transition/TransitionPort;->mTargetIdChildExcludes:Ljava/util/ArrayList;

    .line 64
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/transition/TransitionPort;->mTargetChildExcludes:Ljava/util/ArrayList;

    .line 66
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/transition/TransitionPort;->mTargetTypeChildExcludes:Ljava/util/ArrayList;

    .line 68
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/transition/TransitionPort;->mParent:Landroid/support/transition/TransitionSetPort;

    .line 71
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/transition/TransitionPort;->mSceneRoot:Landroid/view/ViewGroup;

    .line 79
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/transition/TransitionPort;->mCanRemoveViews:Z

    .line 83
    const/4 v0, 0x0

    iput v0, p0, Landroid/support/transition/TransitionPort;->mNumInstances:I

    .line 86
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/transition/TransitionPort;->mPaused:Z

    .line 89
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/transition/TransitionPort;->mListeners:Ljava/util/ArrayList;

    .line 93
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/transition/TransitionPort;->mAnimators:Ljava/util/ArrayList;

    .line 95
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/support/transition/TransitionPort;->mName:Ljava/lang/String;

    .line 97
    new-instance v0, Landroid/support/transition/TransitionValuesMaps;

    invoke-direct {v0}, Landroid/support/transition/TransitionValuesMaps;-><init>()V

    iput-object v0, p0, Landroid/support/transition/TransitionPort;->mStartValues:Landroid/support/transition/TransitionValuesMaps;

    .line 100
    new-instance v0, Landroid/support/transition/TransitionValuesMaps;

    invoke-direct {v0}, Landroid/support/transition/TransitionValuesMaps;-><init>()V

    iput-object v0, p0, Landroid/support/transition/TransitionPort;->mEndValues:Landroid/support/transition/TransitionValuesMaps;

    .line 105
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/transition/TransitionPort;->mCurrentAnimators:Ljava/util/ArrayList;

    .line 109
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/transition/TransitionPort;->mEnded:Z

    .line 118
    return-void
.end method

.method private captureHierarchy(Landroid/view/View;Z)V
    .registers 12
    .param p1, "view"    # Landroid/view/View;
    .param p2, "start"    # Z

    .line 683
    if-nez p1, :cond_3

    .line 684
    return-void

    .line 686
    :cond_3
    const/4 v2, 0x0

    .line 687
    .local v2, "isListViewItem":Z
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    instance-of v0, v0, Landroid/widget/ListView;

    if-eqz v0, :cond_d

    .line 688
    const/4 v2, 0x1

    .line 690
    :cond_d
    if-eqz v2, :cond_20

    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    invoke-interface {v0}, Landroid/widget/ListAdapter;->hasStableIds()Z

    move-result v0

    if-nez v0, :cond_20

    .line 692
    return-void

    .line 694
    :cond_20
    const/4 v3, -0x1

    .line 695
    .local v3, "id":I
    const-wide/16 v4, -0x1

    .line 696
    .local v4, "itemId":J
    if-nez v2, :cond_2a

    .line 697
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v3

    goto :goto_39

    .line 699
    :cond_2a
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Landroid/widget/ListView;

    .line 700
    .local v6, "listview":Landroid/widget/ListView;
    invoke-virtual {v6, p1}, Landroid/widget/ListView;->getPositionForView(Landroid/view/View;)I

    move-result v7

    .line 701
    .local v7, "position":I
    invoke-virtual {v6, v7}, Landroid/widget/ListView;->getItemIdAtPosition(I)J

    move-result-wide v4

    .line 704
    .end local v6    # "listview":Landroid/widget/ListView;
    .end local v7    # "position":I
    :goto_39
    iget-object v0, p0, Landroid/support/transition/TransitionPort;->mTargetIdExcludes:Ljava/util/ArrayList;

    if-eqz v0, :cond_4a

    iget-object v0, p0, Landroid/support/transition/TransitionPort;->mTargetIdExcludes:Ljava/util/ArrayList;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4a

    .line 705
    return-void

    .line 707
    :cond_4a
    iget-object v0, p0, Landroid/support/transition/TransitionPort;->mTargetExcludes:Ljava/util/ArrayList;

    if-eqz v0, :cond_57

    iget-object v0, p0, Landroid/support/transition/TransitionPort;->mTargetExcludes:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_57

    .line 708
    return-void

    .line 710
    :cond_57
    iget-object v0, p0, Landroid/support/transition/TransitionPort;->mTargetTypeExcludes:Ljava/util/ArrayList;

    if-eqz v0, :cond_78

    if-eqz p1, :cond_78

    .line 711
    iget-object v0, p0, Landroid/support/transition/TransitionPort;->mTargetTypeExcludes:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v6

    .line 712
    .local v6, "numTypes":I
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_64
    if-ge v7, v6, :cond_78

    .line 713
    iget-object v0, p0, Landroid/support/transition/TransitionPort;->mTargetTypeExcludes:Ljava/util/ArrayList;

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    invoke-virtual {v0, p1}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_75

    .line 714
    return-void

    .line 712
    :cond_75
    add-int/lit8 v7, v7, 0x1

    goto :goto_64

    .line 718
    .end local v6    # "numTypes":I
    .end local v7    # "i":I
    :cond_78
    new-instance v6, Landroid/support/transition/TransitionValues;

    invoke-direct {v6}, Landroid/support/transition/TransitionValues;-><init>()V

    .line 719
    .local v6, "values":Landroid/support/transition/TransitionValues;
    iput-object p1, v6, Landroid/support/transition/TransitionValues;->view:Landroid/view/View;

    .line 720
    if-eqz p2, :cond_85

    .line 721
    invoke-virtual {p0, v6}, Landroid/support/transition/TransitionPort;->captureStartValues(Landroid/support/transition/TransitionValues;)V

    goto :goto_88

    .line 723
    :cond_85
    invoke-virtual {p0, v6}, Landroid/support/transition/TransitionPort;->captureEndValues(Landroid/support/transition/TransitionValues;)V

    .line 725
    :goto_88
    if-eqz p2, :cond_a5

    .line 726
    if-nez v2, :cond_9d

    .line 727
    iget-object v0, p0, Landroid/support/transition/TransitionPort;->mStartValues:Landroid/support/transition/TransitionValuesMaps;

    iget-object v0, v0, Landroid/support/transition/TransitionValuesMaps;->viewValues:Landroid/support/v4/util/ArrayMap;

    invoke-virtual {v0, p1, v6}, Landroid/support/v4/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 728
    if-ltz v3, :cond_bf

    .line 729
    iget-object v0, p0, Landroid/support/transition/TransitionPort;->mStartValues:Landroid/support/transition/TransitionValuesMaps;

    iget-object v0, v0, Landroid/support/transition/TransitionValuesMaps;->idValues:Landroid/util/SparseArray;

    invoke-virtual {v0, v3, v6}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    goto :goto_bf

    .line 732
    :cond_9d
    iget-object v0, p0, Landroid/support/transition/TransitionPort;->mStartValues:Landroid/support/transition/TransitionValuesMaps;

    iget-object v0, v0, Landroid/support/transition/TransitionValuesMaps;->itemIdValues:Landroid/support/v4/util/LongSparseArray;

    invoke-virtual {v0, v4, v5, v6}, Landroid/support/v4/util/LongSparseArray;->put(JLjava/lang/Object;)V

    goto :goto_bf

    .line 735
    :cond_a5
    if-nez v2, :cond_b8

    .line 736
    iget-object v0, p0, Landroid/support/transition/TransitionPort;->mEndValues:Landroid/support/transition/TransitionValuesMaps;

    iget-object v0, v0, Landroid/support/transition/TransitionValuesMaps;->viewValues:Landroid/support/v4/util/ArrayMap;

    invoke-virtual {v0, p1, v6}, Landroid/support/v4/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 737
    if-ltz v3, :cond_bf

    .line 738
    iget-object v0, p0, Landroid/support/transition/TransitionPort;->mEndValues:Landroid/support/transition/TransitionValuesMaps;

    iget-object v0, v0, Landroid/support/transition/TransitionValuesMaps;->idValues:Landroid/util/SparseArray;

    invoke-virtual {v0, v3, v6}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    goto :goto_bf

    .line 741
    :cond_b8
    iget-object v0, p0, Landroid/support/transition/TransitionPort;->mEndValues:Landroid/support/transition/TransitionValuesMaps;

    iget-object v0, v0, Landroid/support/transition/TransitionValuesMaps;->itemIdValues:Landroid/support/v4/util/LongSparseArray;

    invoke-virtual {v0, v4, v5, v6}, Landroid/support/v4/util/LongSparseArray;->put(JLjava/lang/Object;)V

    .line 744
    :cond_bf
    :goto_bf
    instance-of v0, p1, Landroid/view/ViewGroup;

    if-eqz v0, :cond_116

    .line 746
    iget-object v0, p0, Landroid/support/transition/TransitionPort;->mTargetIdChildExcludes:Ljava/util/ArrayList;

    if-eqz v0, :cond_d4

    iget-object v0, p0, Landroid/support/transition/TransitionPort;->mTargetIdChildExcludes:Ljava/util/ArrayList;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d4

    .line 747
    return-void

    .line 749
    :cond_d4
    iget-object v0, p0, Landroid/support/transition/TransitionPort;->mTargetChildExcludes:Ljava/util/ArrayList;

    if-eqz v0, :cond_e1

    iget-object v0, p0, Landroid/support/transition/TransitionPort;->mTargetChildExcludes:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e1

    .line 750
    return-void

    .line 752
    :cond_e1
    iget-object v0, p0, Landroid/support/transition/TransitionPort;->mTargetTypeChildExcludes:Ljava/util/ArrayList;

    if-eqz v0, :cond_102

    if-eqz p1, :cond_102

    .line 753
    iget-object v0, p0, Landroid/support/transition/TransitionPort;->mTargetTypeChildExcludes:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v7

    .line 754
    .local v7, "numTypes":I
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_ee
    if-ge v8, v7, :cond_102

    .line 755
    iget-object v0, p0, Landroid/support/transition/TransitionPort;->mTargetTypeChildExcludes:Ljava/util/ArrayList;

    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    invoke-virtual {v0, p1}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_ff

    .line 756
    return-void

    .line 754
    :cond_ff
    add-int/lit8 v8, v8, 0x1

    goto :goto_ee

    .line 760
    .end local v7    # "numTypes":I
    .end local v8    # "i":I
    :cond_102
    move-object v7, p1

    check-cast v7, Landroid/view/ViewGroup;

    .line 761
    .local v7, "parent":Landroid/view/ViewGroup;
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_106
    invoke-virtual {v7}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    if-ge v8, v0, :cond_116

    .line 762
    invoke-virtual {v7, v8}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Landroid/support/transition/TransitionPort;->captureHierarchy(Landroid/view/View;Z)V

    .line 761
    add-int/lit8 v8, v8, 0x1

    goto :goto_106

    .line 765
    .end local v7    # "parent":Landroid/view/ViewGroup;
    .end local v8    # "i":I
    :cond_116
    return-void
.end method

.method private static getRunningAnimators()Landroid/support/v4/util/ArrayMap;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Landroid/support/v4/util/ArrayMap<Landroid/animation/Animator;Landroid/support/transition/TransitionPort$AnimationInfo;>;"
        }
    .end annotation

    .line 121
    sget-object v0, Landroid/support/transition/TransitionPort;->sRunningAnimators:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Landroid/support/v4/util/ArrayMap;

    .line 122
    .local v1, "runningAnimators":Landroid/support/v4/util/ArrayMap;, "Landroid/support/v4/util/ArrayMap<Landroid/animation/Animator;Landroid/support/transition/TransitionPort$AnimationInfo;>;"
    if-nez v1, :cond_15

    .line 123
    new-instance v1, Landroid/support/v4/util/ArrayMap;

    invoke-direct {v1}, Landroid/support/v4/util/ArrayMap;-><init>()V

    .line 124
    sget-object v0, Landroid/support/transition/TransitionPort;->sRunningAnimators:Ljava/lang/ThreadLocal;

    invoke-virtual {v0, v1}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    .line 126
    :cond_15
    return-object v1
.end method

.method private runAnimator(Landroid/animation/Animator;Landroid/support/v4/util/ArrayMap;)V
    .registers 4
    .param p1, "animator"    # Landroid/animation/Animator;
    .param p2, "runningAnimators"    # Landroid/support/v4/util/ArrayMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Landroid/animation/Animator;Landroid/support/v4/util/ArrayMap<Landroid/animation/Animator;Landroid/support/transition/TransitionPort$AnimationInfo;>;)V"
        }
    .end annotation

    .line 436
    if-eqz p1, :cond_d

    .line 438
    new-instance v0, Landroid/support/transition/TransitionPort$1;

    invoke-direct {v0, p0, p2}, Landroid/support/transition/TransitionPort$1;-><init>(Landroid/support/transition/TransitionPort;Landroid/support/v4/util/ArrayMap;)V

    invoke-virtual {p1, v0}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 450
    invoke-virtual {p0, p1}, Landroid/support/transition/TransitionPort;->animate(Landroid/animation/Animator;)V

    .line 452
    :cond_d
    return-void
.end method


# virtual methods
.method public addListener(Landroid/support/transition/TransitionPort$TransitionListener;)Landroid/support/transition/TransitionPort;
    .registers 3
    .param p1, "listener"    # Landroid/support/transition/TransitionPort$TransitionListener;

    .line 1049
    iget-object v0, p0, Landroid/support/transition/TransitionPort;->mListeners:Ljava/util/ArrayList;

    if-nez v0, :cond_b

    .line 1050
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/transition/TransitionPort;->mListeners:Ljava/util/ArrayList;

    .line 1052
    :cond_b
    iget-object v0, p0, Landroid/support/transition/TransitionPort;->mListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1053
    return-object p0
.end method

.method protected animate(Landroid/animation/Animator;)V
    .registers 6
    .param p1, "animator"    # Landroid/animation/Animator;

    .line 931
    if-nez p1, :cond_6

    .line 932
    invoke-virtual {p0}, Landroid/support/transition/TransitionPort;->end()V

    goto :goto_40

    .line 934
    :cond_6
    invoke-virtual {p0}, Landroid/support/transition/TransitionPort;->getDuration()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-ltz v0, :cond_17

    .line 935
    invoke-virtual {p0}, Landroid/support/transition/TransitionPort;->getDuration()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    .line 937
    :cond_17
    invoke-virtual {p0}, Landroid/support/transition/TransitionPort;->getStartDelay()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-ltz v0, :cond_28

    .line 938
    invoke-virtual {p0}, Landroid/support/transition/TransitionPort;->getStartDelay()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Landroid/animation/Animator;->setStartDelay(J)V

    .line 940
    :cond_28
    invoke-virtual {p0}, Landroid/support/transition/TransitionPort;->getInterpolator()Landroid/animation/TimeInterpolator;

    move-result-object v0

    if-eqz v0, :cond_35

    .line 941
    invoke-virtual {p0}, Landroid/support/transition/TransitionPort;->getInterpolator()Landroid/animation/TimeInterpolator;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/animation/Animator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 943
    :cond_35
    new-instance v0, Landroid/support/transition/TransitionPort$2;

    invoke-direct {v0, p0}, Landroid/support/transition/TransitionPort$2;-><init>(Landroid/support/transition/TransitionPort;)V

    invoke-virtual {p1, v0}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 950
    invoke-virtual {p1}, Landroid/animation/Animator;->start()V

    .line 952
    :goto_40
    return-void
.end method

.method public abstract captureEndValues(Landroid/support/transition/TransitionValues;)V
.end method

.method public abstract captureStartValues(Landroid/support/transition/TransitionValues;)V
.end method

.method captureValues(Landroid/view/ViewGroup;Z)V
    .registers 8
    .param p1, "sceneRoot"    # Landroid/view/ViewGroup;
    .param p2, "start"    # Z

    .line 603
    invoke-virtual {p0, p2}, Landroid/support/transition/TransitionPort;->clearValues(Z)V

    .line 604
    iget-object v0, p0, Landroid/support/transition/TransitionPort;->mTargetIds:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-gtz v0, :cond_13

    iget-object v0, p0, Landroid/support/transition/TransitionPort;->mTargets:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_ae

    .line 605
    :cond_13
    iget-object v0, p0, Landroid/support/transition/TransitionPort;->mTargetIds:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_6d

    .line 606
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1c
    iget-object v0, p0, Landroid/support/transition/TransitionPort;->mTargetIds:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_6d

    .line 607
    iget-object v0, p0, Landroid/support/transition/TransitionPort;->mTargetIds:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 608
    .local v2, "id":I
    invoke-virtual {p1, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 609
    .local v3, "view":Landroid/view/View;
    if-eqz v3, :cond_69

    .line 610
    new-instance v4, Landroid/support/transition/TransitionValues;

    invoke-direct {v4}, Landroid/support/transition/TransitionValues;-><init>()V

    .line 611
    .local v4, "values":Landroid/support/transition/TransitionValues;
    iput-object v3, v4, Landroid/support/transition/TransitionValues;->view:Landroid/view/View;

    .line 612
    if-eqz p2, :cond_43

    .line 613
    invoke-virtual {p0, v4}, Landroid/support/transition/TransitionPort;->captureStartValues(Landroid/support/transition/TransitionValues;)V

    goto :goto_46

    .line 615
    :cond_43
    invoke-virtual {p0, v4}, Landroid/support/transition/TransitionPort;->captureEndValues(Landroid/support/transition/TransitionValues;)V

    .line 617
    :goto_46
    if-eqz p2, :cond_59

    .line 618
    iget-object v0, p0, Landroid/support/transition/TransitionPort;->mStartValues:Landroid/support/transition/TransitionValuesMaps;

    iget-object v0, v0, Landroid/support/transition/TransitionValuesMaps;->viewValues:Landroid/support/v4/util/ArrayMap;

    invoke-virtual {v0, v3, v4}, Landroid/support/v4/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 619
    if-ltz v2, :cond_69

    .line 620
    iget-object v0, p0, Landroid/support/transition/TransitionPort;->mStartValues:Landroid/support/transition/TransitionValuesMaps;

    iget-object v0, v0, Landroid/support/transition/TransitionValuesMaps;->idValues:Landroid/util/SparseArray;

    invoke-virtual {v0, v2, v4}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    goto :goto_69

    .line 623
    :cond_59
    iget-object v0, p0, Landroid/support/transition/TransitionPort;->mEndValues:Landroid/support/transition/TransitionValuesMaps;

    iget-object v0, v0, Landroid/support/transition/TransitionValuesMaps;->viewValues:Landroid/support/v4/util/ArrayMap;

    invoke-virtual {v0, v3, v4}, Landroid/support/v4/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 624
    if-ltz v2, :cond_69

    .line 625
    iget-object v0, p0, Landroid/support/transition/TransitionPort;->mEndValues:Landroid/support/transition/TransitionValuesMaps;

    iget-object v0, v0, Landroid/support/transition/TransitionValuesMaps;->idValues:Landroid/util/SparseArray;

    invoke-virtual {v0, v2, v4}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 606
    .end local v2    # "id":I
    .end local v3    # "view":Landroid/view/View;
    .end local v4    # "values":Landroid/support/transition/TransitionValues;
    :cond_69
    :goto_69
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_1c

    .line 631
    .end local v1    # "i":I
    :cond_6d
    iget-object v0, p0, Landroid/support/transition/TransitionPort;->mTargets:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_b1

    .line 632
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_76
    iget-object v0, p0, Landroid/support/transition/TransitionPort;->mTargets:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_ad

    .line 633
    iget-object v0, p0, Landroid/support/transition/TransitionPort;->mTargets:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Landroid/view/View;

    .line 634
    .local v2, "view":Landroid/view/View;
    if-eqz v2, :cond_aa

    .line 635
    new-instance v3, Landroid/support/transition/TransitionValues;

    invoke-direct {v3}, Landroid/support/transition/TransitionValues;-><init>()V

    .line 636
    .local v3, "values":Landroid/support/transition/TransitionValues;
    iput-object v2, v3, Landroid/support/transition/TransitionValues;->view:Landroid/view/View;

    .line 637
    if-eqz p2, :cond_96

    .line 638
    invoke-virtual {p0, v3}, Landroid/support/transition/TransitionPort;->captureStartValues(Landroid/support/transition/TransitionValues;)V

    goto :goto_99

    .line 640
    :cond_96
    invoke-virtual {p0, v3}, Landroid/support/transition/TransitionPort;->captureEndValues(Landroid/support/transition/TransitionValues;)V

    .line 642
    :goto_99
    if-eqz p2, :cond_a3

    .line 643
    iget-object v0, p0, Landroid/support/transition/TransitionPort;->mStartValues:Landroid/support/transition/TransitionValuesMaps;

    iget-object v0, v0, Landroid/support/transition/TransitionValuesMaps;->viewValues:Landroid/support/v4/util/ArrayMap;

    invoke-virtual {v0, v2, v3}, Landroid/support/v4/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_aa

    .line 645
    :cond_a3
    iget-object v0, p0, Landroid/support/transition/TransitionPort;->mEndValues:Landroid/support/transition/TransitionValuesMaps;

    iget-object v0, v0, Landroid/support/transition/TransitionValuesMaps;->viewValues:Landroid/support/v4/util/ArrayMap;

    invoke-virtual {v0, v2, v3}, Landroid/support/v4/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 632
    .end local v2    # "view":Landroid/view/View;
    .end local v3    # "values":Landroid/support/transition/TransitionValues;
    :cond_aa
    :goto_aa
    add-int/lit8 v1, v1, 0x1

    goto :goto_76

    .end local v1    # "i":I
    :cond_ad
    goto :goto_b1

    .line 651
    :cond_ae
    invoke-direct {p0, p1, p2}, Landroid/support/transition/TransitionPort;->captureHierarchy(Landroid/view/View;Z)V

    .line 653
    :cond_b1
    :goto_b1
    return-void
.end method

.method clearValues(Z)V
    .registers 3
    .param p1, "start"    # Z

    .line 661
    if-eqz p1, :cond_18

    .line 662
    iget-object v0, p0, Landroid/support/transition/TransitionPort;->mStartValues:Landroid/support/transition/TransitionValuesMaps;

    iget-object v0, v0, Landroid/support/transition/TransitionValuesMaps;->viewValues:Landroid/support/v4/util/ArrayMap;

    invoke-virtual {v0}, Landroid/support/v4/util/ArrayMap;->clear()V

    .line 663
    iget-object v0, p0, Landroid/support/transition/TransitionPort;->mStartValues:Landroid/support/transition/TransitionValuesMaps;

    iget-object v0, v0, Landroid/support/transition/TransitionValuesMaps;->idValues:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    .line 664
    iget-object v0, p0, Landroid/support/transition/TransitionPort;->mStartValues:Landroid/support/transition/TransitionValuesMaps;

    iget-object v0, v0, Landroid/support/transition/TransitionValuesMaps;->itemIdValues:Landroid/support/v4/util/LongSparseArray;

    invoke-virtual {v0}, Landroid/support/v4/util/LongSparseArray;->clear()V

    goto :goto_2d

    .line 666
    :cond_18
    iget-object v0, p0, Landroid/support/transition/TransitionPort;->mEndValues:Landroid/support/transition/TransitionValuesMaps;

    iget-object v0, v0, Landroid/support/transition/TransitionValuesMaps;->viewValues:Landroid/support/v4/util/ArrayMap;

    invoke-virtual {v0}, Landroid/support/v4/util/ArrayMap;->clear()V

    .line 667
    iget-object v0, p0, Landroid/support/transition/TransitionPort;->mEndValues:Landroid/support/transition/TransitionValuesMaps;

    iget-object v0, v0, Landroid/support/transition/TransitionValuesMaps;->idValues:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    .line 668
    iget-object v0, p0, Landroid/support/transition/TransitionPort;->mEndValues:Landroid/support/transition/TransitionValuesMaps;

    iget-object v0, v0, Landroid/support/transition/TransitionValuesMaps;->itemIdValues:Landroid/support/v4/util/LongSparseArray;

    invoke-virtual {v0}, Landroid/support/v4/util/LongSparseArray;->clear()V

    .line 670
    :goto_2d
    return-void
.end method

.method public clone()Landroid/support/transition/TransitionPort;
    .registers 4

    .line 1083
    const/4 v1, 0x0

    .line 1085
    .local v1, "clone":Landroid/support/transition/TransitionPort;
    :try_start_1
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/transition/TransitionPort;

    move-object v1, v0

    .line 1086
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, v1, Landroid/support/transition/TransitionPort;->mAnimators:Ljava/util/ArrayList;

    .line 1087
    new-instance v0, Landroid/support/transition/TransitionValuesMaps;

    invoke-direct {v0}, Landroid/support/transition/TransitionValuesMaps;-><init>()V

    iput-object v0, v1, Landroid/support/transition/TransitionPort;->mStartValues:Landroid/support/transition/TransitionValuesMaps;

    .line 1088
    new-instance v0, Landroid/support/transition/TransitionValuesMaps;

    invoke-direct {v0}, Landroid/support/transition/TransitionValuesMaps;-><init>()V

    iput-object v0, v1, Landroid/support/transition/TransitionPort;->mEndValues:Landroid/support/transition/TransitionValuesMaps;
    :try_end_1d
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_1 .. :try_end_1d} :catch_1e

    .line 1090
    goto :goto_1f

    .line 1089
    :catch_1e
    move-exception v2

    .line 1092
    :goto_1f
    return-object v1
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .line 36
    invoke-virtual {p0}, Landroid/support/transition/TransitionPort;->clone()Landroid/support/transition/TransitionPort;

    move-result-object v0

    return-object v0
.end method

.method public createAnimator(Landroid/view/ViewGroup;Landroid/support/transition/TransitionValues;Landroid/support/transition/TransitionValues;)Landroid/animation/Animator;
    .registers 5
    .param p1, "sceneRoot"    # Landroid/view/ViewGroup;
    .param p2, "startValues"    # Landroid/support/transition/TransitionValues;
    .param p3, "endValues"    # Landroid/support/transition/TransitionValues;

    .line 163
    const/4 v0, 0x0

    return-object v0
.end method

.method protected createAnimators(Landroid/view/ViewGroup;Landroid/support/transition/TransitionValuesMaps;Landroid/support/transition/TransitionValuesMaps;)V
    .registers 30
    .param p1, "sceneRoot"    # Landroid/view/ViewGroup;
    .param p2, "startValues"    # Landroid/support/transition/TransitionValuesMaps;
    .param p3, "endValues"    # Landroid/support/transition/TransitionValuesMaps;

    .line 183
    new-instance v5, Landroid/support/v4/util/ArrayMap;

    move-object/from16 v0, p3

    iget-object v0, v0, Landroid/support/transition/TransitionValuesMaps;->viewValues:Landroid/support/v4/util/ArrayMap;

    invoke-direct {v5, v0}, Landroid/support/v4/util/ArrayMap;-><init>(Landroid/support/v4/util/SimpleArrayMap;)V

    .line 185
    .local v5, "endCopy":Landroid/support/v4/util/ArrayMap;, "Landroid/support/v4/util/ArrayMap<Landroid/view/View;Landroid/support/transition/TransitionValues;>;"
    new-instance v6, Landroid/util/SparseArray;

    move-object/from16 v0, p3

    iget-object v0, v0, Landroid/support/transition/TransitionValuesMaps;->idValues:Landroid/util/SparseArray;

    .line 186
    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    invoke-direct {v6, v0}, Landroid/util/SparseArray;-><init>(I)V

    .line 187
    .local v6, "endIdCopy":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Landroid/support/transition/TransitionValues;>;"
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_17
    move-object/from16 v0, p3

    iget-object v0, v0, Landroid/support/transition/TransitionValuesMaps;->idValues:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-ge v7, v0, :cond_37

    .line 188
    move-object/from16 v0, p3

    iget-object v0, v0, Landroid/support/transition/TransitionValuesMaps;->idValues:Landroid/util/SparseArray;

    invoke-virtual {v0, v7}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v8

    .line 189
    .local v8, "id":I
    move-object/from16 v0, p3

    iget-object v0, v0, Landroid/support/transition/TransitionValuesMaps;->idValues:Landroid/util/SparseArray;

    invoke-virtual {v0, v7}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v6, v8, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 187
    .end local v8    # "id":I
    add-int/lit8 v7, v7, 0x1

    goto :goto_17

    .line 191
    .end local v7    # "i":I
    :cond_37
    new-instance v7, Landroid/support/v4/util/LongSparseArray;

    move-object/from16 v0, p3

    iget-object v0, v0, Landroid/support/transition/TransitionValuesMaps;->itemIdValues:Landroid/support/v4/util/LongSparseArray;

    .line 192
    invoke-virtual {v0}, Landroid/support/v4/util/LongSparseArray;->size()I

    move-result v0

    invoke-direct {v7, v0}, Landroid/support/v4/util/LongSparseArray;-><init>(I)V

    .line 193
    .local v7, "endItemIdCopy":Landroid/support/v4/util/LongSparseArray;, "Landroid/support/v4/util/LongSparseArray<Landroid/support/transition/TransitionValues;>;"
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_45
    move-object/from16 v0, p3

    iget-object v0, v0, Landroid/support/transition/TransitionValuesMaps;->itemIdValues:Landroid/support/v4/util/LongSparseArray;

    invoke-virtual {v0}, Landroid/support/v4/util/LongSparseArray;->size()I

    move-result v0

    if-ge v8, v0, :cond_65

    .line 194
    move-object/from16 v0, p3

    iget-object v0, v0, Landroid/support/transition/TransitionValuesMaps;->itemIdValues:Landroid/support/v4/util/LongSparseArray;

    invoke-virtual {v0, v8}, Landroid/support/v4/util/LongSparseArray;->keyAt(I)J

    move-result-wide v9

    .line 195
    .local v9, "id":J
    move-object/from16 v0, p3

    iget-object v0, v0, Landroid/support/transition/TransitionValuesMaps;->itemIdValues:Landroid/support/v4/util/LongSparseArray;

    invoke-virtual {v0, v8}, Landroid/support/v4/util/LongSparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v7, v9, v10, v0}, Landroid/support/v4/util/LongSparseArray;->put(JLjava/lang/Object;)V

    .line 193
    .end local v9    # "id":J
    add-int/lit8 v8, v8, 0x1

    goto :goto_45

    .line 199
    .end local v8    # "i":I
    :cond_65
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 200
    .local v8, "startValuesList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/support/transition/TransitionValues;>;"
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 201
    .local v9, "endValuesList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/support/transition/TransitionValues;>;"
    move-object/from16 v0, p2

    iget-object v0, v0, Landroid/support/transition/TransitionValuesMaps;->viewValues:Landroid/support/v4/util/ArrayMap;

    invoke-virtual {v0}, Landroid/support/v4/util/ArrayMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_7b
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_151

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v11, v0

    check-cast v11, Landroid/view/View;

    .line 203
    .local v11, "view":Landroid/view/View;
    const/4 v13, 0x0

    .line 204
    .local v13, "end":Landroid/support/transition/TransitionValues;
    const/4 v14, 0x0

    .line 205
    .local v14, "isInListView":Z
    invoke-virtual {v11}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    instance-of v0, v0, Landroid/widget/ListView;

    if-eqz v0, :cond_93

    .line 206
    const/4 v14, 0x1

    .line 208
    :cond_93
    if-nez v14, :cond_11c

    .line 209
    invoke-virtual {v11}, Landroid/view/View;->getId()I

    move-result v15

    .line 210
    .local v15, "id":I
    move-object/from16 v0, p2

    iget-object v0, v0, Landroid/support/transition/TransitionValuesMaps;->viewValues:Landroid/support/v4/util/ArrayMap;

    invoke-virtual {v0, v11}, Landroid/support/v4/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_af

    move-object/from16 v0, p2

    iget-object v0, v0, Landroid/support/transition/TransitionValuesMaps;->viewValues:Landroid/support/v4/util/ArrayMap;

    .line 211
    invoke-virtual {v0, v11}, Landroid/support/v4/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v12, v0

    check-cast v12, Landroid/support/transition/TransitionValues;

    goto :goto_ba

    :cond_af
    move-object/from16 v0, p2

    iget-object v0, v0, Landroid/support/transition/TransitionValuesMaps;->idValues:Landroid/util/SparseArray;

    invoke-virtual {v0, v15}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v12, v0

    check-cast v12, Landroid/support/transition/TransitionValues;

    .line 212
    .local v12, "start":Landroid/support/transition/TransitionValues;
    :goto_ba
    move-object/from16 v0, p3

    iget-object v0, v0, Landroid/support/transition/TransitionValuesMaps;->viewValues:Landroid/support/v4/util/ArrayMap;

    invoke-virtual {v0, v11}, Landroid/support/v4/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_d3

    .line 213
    move-object/from16 v0, p3

    iget-object v0, v0, Landroid/support/transition/TransitionValuesMaps;->viewValues:Landroid/support/v4/util/ArrayMap;

    invoke-virtual {v0, v11}, Landroid/support/v4/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v13, v0

    check-cast v13, Landroid/support/transition/TransitionValues;

    .line 214
    invoke-virtual {v5, v11}, Landroid/support/v4/util/ArrayMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_109

    .line 215
    :cond_d3
    const/4 v0, -0x1

    if-eq v15, v0, :cond_109

    .line 216
    move-object/from16 v0, p3

    iget-object v0, v0, Landroid/support/transition/TransitionValuesMaps;->idValues:Landroid/util/SparseArray;

    invoke-virtual {v0, v15}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v13, v0

    check-cast v13, Landroid/support/transition/TransitionValues;

    .line 217
    const/16 v16, 0x0

    .line 218
    .local v16, "removeView":Landroid/view/View;
    invoke-virtual {v5}, Landroid/support/v4/util/ArrayMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v17

    :goto_eb
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_102

    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object/from16 v18, v0

    check-cast v18, Landroid/view/View;

    .line 219
    .local v18, "viewToRemove":Landroid/view/View;
    invoke-virtual/range {v18 .. v18}, Landroid/view/View;->getId()I

    move-result v0

    if-ne v0, v15, :cond_101

    .line 220
    move-object/from16 v16, v18

    .line 222
    .end local v18    # "viewToRemove":Landroid/view/View;
    :cond_101
    goto :goto_eb

    .line 223
    :cond_102
    if-eqz v16, :cond_109

    .line 224
    move-object/from16 v0, v16

    invoke-virtual {v5, v0}, Landroid/support/v4/util/ArrayMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 227
    .end local v16    # "removeView":Landroid/view/View;
    :cond_109
    :goto_109
    invoke-virtual {v6, v15}, Landroid/util/SparseArray;->remove(I)V

    .line 228
    int-to-long v0, v15

    move-object/from16 v2, p0

    invoke-virtual {v2, v11, v0, v1}, Landroid/support/transition/TransitionPort;->isValidTarget(Landroid/view/View;J)Z

    move-result v0

    if-eqz v0, :cond_11b

    .line 229
    invoke-virtual {v8, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 230
    invoke-virtual {v9, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 232
    .end local v15    # "id":I
    :cond_11b
    goto :goto_14f

    .line 233
    .end local v12    # "start":Landroid/support/transition/TransitionValues;
    :cond_11c
    invoke-virtual {v11}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    move-object v15, v0

    check-cast v15, Landroid/widget/ListView;

    .line 234
    .local v15, "parent":Landroid/widget/ListView;
    invoke-virtual {v15}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    invoke-interface {v0}, Landroid/widget/ListAdapter;->hasStableIds()Z

    move-result v0

    if-eqz v0, :cond_14f

    .line 235
    invoke-virtual {v15, v11}, Landroid/widget/ListView;->getPositionForView(Landroid/view/View;)I

    move-result v16

    .line 236
    .local v16, "position":I
    move/from16 v0, v16

    invoke-virtual {v15, v0}, Landroid/widget/ListView;->getItemIdAtPosition(I)J

    move-result-wide v17

    .line 237
    .local v17, "itemId":J
    move-object/from16 v0, p2

    iget-object v0, v0, Landroid/support/transition/TransitionValuesMaps;->itemIdValues:Landroid/support/v4/util/LongSparseArray;

    move-wide/from16 v1, v17

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/util/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object v0

    move-object v12, v0

    check-cast v12, Landroid/support/transition/TransitionValues;

    .line 238
    .local v12, "start":Landroid/support/transition/TransitionValues;
    move-wide/from16 v0, v17

    invoke-virtual {v7, v0, v1}, Landroid/support/v4/util/LongSparseArray;->remove(J)V

    .line 240
    invoke-virtual {v8, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 241
    invoke-virtual {v9, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 244
    .end local v11    # "view":Landroid/view/View;
    .end local v12    # "start":Landroid/support/transition/TransitionValues;
    .end local v13    # "end":Landroid/support/transition/TransitionValues;
    .end local v14    # "isInListView":Z
    .end local v15    # "parent":Landroid/widget/ListView;
    .end local v16    # "position":I
    .end local v17    # "itemId":J
    :cond_14f
    :goto_14f
    goto/16 :goto_7b

    .line 245
    :cond_151
    move-object/from16 v0, p2

    iget-object v0, v0, Landroid/support/transition/TransitionValuesMaps;->itemIdValues:Landroid/support/v4/util/LongSparseArray;

    invoke-virtual {v0}, Landroid/support/v4/util/LongSparseArray;->size()I

    move-result v10

    .line 246
    .local v10, "startItemIdCopySize":I
    const/4 v11, 0x0

    .local v11, "i":I
    :goto_15a
    if-ge v11, v10, :cond_18f

    .line 247
    move-object/from16 v0, p2

    iget-object v0, v0, Landroid/support/transition/TransitionValuesMaps;->itemIdValues:Landroid/support/v4/util/LongSparseArray;

    invoke-virtual {v0, v11}, Landroid/support/v4/util/LongSparseArray;->keyAt(I)J

    move-result-wide v12

    .line 248
    .local v12, "id":J
    move-object/from16 v0, p0

    const/4 v1, 0x0

    invoke-virtual {v0, v1, v12, v13}, Landroid/support/transition/TransitionPort;->isValidTarget(Landroid/view/View;J)Z

    move-result v0

    if-eqz v0, :cond_18c

    .line 249
    move-object/from16 v0, p2

    iget-object v0, v0, Landroid/support/transition/TransitionValuesMaps;->itemIdValues:Landroid/support/v4/util/LongSparseArray;

    invoke-virtual {v0, v12, v13}, Landroid/support/v4/util/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object v0

    move-object v14, v0

    check-cast v14, Landroid/support/transition/TransitionValues;

    .line 250
    .local v14, "start":Landroid/support/transition/TransitionValues;
    move-object/from16 v0, p3

    iget-object v0, v0, Landroid/support/transition/TransitionValuesMaps;->itemIdValues:Landroid/support/v4/util/LongSparseArray;

    invoke-virtual {v0, v12, v13}, Landroid/support/v4/util/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object v0

    move-object v15, v0

    check-cast v15, Landroid/support/transition/TransitionValues;

    .line 251
    .local v15, "end":Landroid/support/transition/TransitionValues;
    invoke-virtual {v7, v12, v13}, Landroid/support/v4/util/LongSparseArray;->remove(J)V

    .line 252
    invoke-virtual {v8, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 253
    invoke-virtual {v9, v15}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 246
    .end local v12    # "id":J
    .end local v14    # "start":Landroid/support/transition/TransitionValues;
    .end local v15    # "end":Landroid/support/transition/TransitionValues;
    :cond_18c
    add-int/lit8 v11, v11, 0x1

    goto :goto_15a

    .line 257
    .end local v11    # "i":I
    :cond_18f
    invoke-virtual {v5}, Landroid/support/v4/util/ArrayMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :goto_197
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1e3

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v12, v0

    check-cast v12, Landroid/view/View;

    .line 258
    .local v12, "view":Landroid/view/View;
    invoke-virtual {v12}, Landroid/view/View;->getId()I

    move-result v13

    .line 259
    .local v13, "id":I
    int-to-long v0, v13

    move-object/from16 v2, p0

    invoke-virtual {v2, v12, v0, v1}, Landroid/support/transition/TransitionPort;->isValidTarget(Landroid/view/View;J)Z

    move-result v0

    if-eqz v0, :cond_1e2

    .line 260
    move-object/from16 v0, p2

    iget-object v0, v0, Landroid/support/transition/TransitionValuesMaps;->viewValues:Landroid/support/v4/util/ArrayMap;

    invoke-virtual {v0, v12}, Landroid/support/v4/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1c7

    move-object/from16 v0, p2

    iget-object v0, v0, Landroid/support/transition/TransitionValuesMaps;->viewValues:Landroid/support/v4/util/ArrayMap;

    .line 261
    invoke-virtual {v0, v12}, Landroid/support/v4/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v14, v0

    check-cast v14, Landroid/support/transition/TransitionValues;

    goto :goto_1d2

    :cond_1c7
    move-object/from16 v0, p2

    iget-object v0, v0, Landroid/support/transition/TransitionValuesMaps;->idValues:Landroid/util/SparseArray;

    invoke-virtual {v0, v13}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v14, v0

    check-cast v14, Landroid/support/transition/TransitionValues;

    .line 262
    .local v14, "start":Landroid/support/transition/TransitionValues;
    :goto_1d2
    invoke-virtual {v5, v12}, Landroid/support/v4/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v15, v0

    check-cast v15, Landroid/support/transition/TransitionValues;

    .line 263
    .local v15, "end":Landroid/support/transition/TransitionValues;
    invoke-virtual {v6, v13}, Landroid/util/SparseArray;->remove(I)V

    .line 264
    invoke-virtual {v8, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 265
    invoke-virtual {v9, v15}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 267
    .end local v12    # "view":Landroid/view/View;
    .end local v13    # "id":I
    .end local v14    # "start":Landroid/support/transition/TransitionValues;
    .end local v15    # "end":Landroid/support/transition/TransitionValues;
    :cond_1e2
    goto :goto_197

    .line 268
    :cond_1e3
    invoke-virtual {v6}, Landroid/util/SparseArray;->size()I

    move-result v11

    .line 269
    .local v11, "endIdCopySize":I
    const/4 v12, 0x0

    .local v12, "i":I
    :goto_1e8
    if-ge v12, v11, :cond_213

    .line 270
    invoke-virtual {v6, v12}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v13

    .line 271
    .local v13, "id":I
    int-to-long v0, v13

    move-object/from16 v2, p0

    const/4 v3, 0x0

    invoke-virtual {v2, v3, v0, v1}, Landroid/support/transition/TransitionPort;->isValidTarget(Landroid/view/View;J)Z

    move-result v0

    if-eqz v0, :cond_210

    .line 272
    move-object/from16 v0, p2

    iget-object v0, v0, Landroid/support/transition/TransitionValuesMaps;->idValues:Landroid/util/SparseArray;

    invoke-virtual {v0, v13}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v14, v0

    check-cast v14, Landroid/support/transition/TransitionValues;

    .line 273
    .local v14, "start":Landroid/support/transition/TransitionValues;
    invoke-virtual {v6, v13}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v15, v0

    check-cast v15, Landroid/support/transition/TransitionValues;

    .line 274
    .local v15, "end":Landroid/support/transition/TransitionValues;
    invoke-virtual {v8, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 275
    invoke-virtual {v9, v15}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 269
    .end local v13    # "id":I
    .end local v14    # "start":Landroid/support/transition/TransitionValues;
    .end local v15    # "end":Landroid/support/transition/TransitionValues;
    :cond_210
    add-int/lit8 v12, v12, 0x1

    goto :goto_1e8

    .line 278
    .end local v12    # "i":I
    :cond_213
    invoke-virtual {v7}, Landroid/support/v4/util/LongSparseArray;->size()I

    move-result v12

    .line 279
    .local v12, "endItemIdCopySize":I
    const/4 v13, 0x0

    .local v13, "i":I
    :goto_218
    if-ge v13, v12, :cond_23f

    .line 280
    invoke-virtual {v7, v13}, Landroid/support/v4/util/LongSparseArray;->keyAt(I)J

    move-result-wide v14

    .line 282
    .local v14, "id":J
    move-object/from16 v0, p2

    iget-object v0, v0, Landroid/support/transition/TransitionValuesMaps;->itemIdValues:Landroid/support/v4/util/LongSparseArray;

    invoke-virtual {v0, v14, v15}, Landroid/support/v4/util/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object v0

    move-object/from16 v16, v0

    check-cast v16, Landroid/support/transition/TransitionValues;

    .line 283
    .local v16, "start":Landroid/support/transition/TransitionValues;
    invoke-virtual {v7, v14, v15}, Landroid/support/v4/util/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object v0

    move-object/from16 v17, v0

    check-cast v17, Landroid/support/transition/TransitionValues;

    .line 284
    .local v17, "end":Landroid/support/transition/TransitionValues;
    move-object/from16 v0, v16

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 285
    move-object/from16 v0, v17

    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 279
    .end local v14    # "id":J
    .end local v16    # "start":Landroid/support/transition/TransitionValues;
    .end local v17    # "end":Landroid/support/transition/TransitionValues;
    add-int/lit8 v13, v13, 0x1

    goto :goto_218

    .line 287
    .end local v13    # "i":I
    :cond_23f
    invoke-static {}, Landroid/support/transition/TransitionPort;->getRunningAnimators()Landroid/support/v4/util/ArrayMap;

    move-result-object v13

    .line 288
    .local v13, "runningAnimators":Landroid/support/v4/util/ArrayMap;, "Landroid/support/v4/util/ArrayMap<Landroid/animation/Animator;Landroid/support/transition/TransitionPort$AnimationInfo;>;"
    const/4 v14, 0x0

    .local v14, "i":I
    :goto_244
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v14, v0, :cond_34c

    .line 289
    invoke-virtual {v8, v14}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v15, v0

    check-cast v15, Landroid/support/transition/TransitionValues;

    .line 290
    .local v15, "start":Landroid/support/transition/TransitionValues;
    invoke-virtual {v9, v14}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object/from16 v16, v0

    check-cast v16, Landroid/support/transition/TransitionValues;

    .line 292
    .local v16, "end":Landroid/support/transition/TransitionValues;
    if-nez v15, :cond_25d

    if-eqz v16, :cond_348

    .line 293
    :cond_25d
    if-eqz v15, :cond_267

    move-object/from16 v0, v16

    invoke-virtual {v15, v0}, Landroid/support/transition/TransitionValues;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_348

    .line 313
    :cond_267
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v16

    invoke-virtual {v0, v1, v15, v2}, Landroid/support/transition/TransitionPort;->createAnimator(Landroid/view/ViewGroup;Landroid/support/transition/TransitionValues;Landroid/support/transition/TransitionValues;)Landroid/animation/Animator;

    move-result-object v17

    .line 314
    .local v17, "animator":Landroid/animation/Animator;
    if-eqz v17, :cond_348

    .line 317
    const/16 v19, 0x0

    .line 318
    .local v19, "infoValues":Landroid/support/transition/TransitionValues;
    if-eqz v16, :cond_31f

    .line 319
    move-object/from16 v0, v16

    iget-object v0, v0, Landroid/support/transition/TransitionValues;->view:Landroid/view/View;

    move-object/from16 v18, v0

    .line 320
    .local v18, "view":Landroid/view/View;
    invoke-virtual/range {p0 .. p0}, Landroid/support/transition/TransitionPort;->getTransitionProperties()[Ljava/lang/String;

    move-result-object v20

    .line 321
    .local v20, "properties":[Ljava/lang/String;
    if-eqz v18, :cond_31e

    if-eqz v20, :cond_31e

    move-object/from16 v0, v20

    array-length v0, v0

    if-lez v0, :cond_31e

    .line 322
    new-instance v19, Landroid/support/transition/TransitionValues;

    invoke-direct/range {v19 .. v19}, Landroid/support/transition/TransitionValues;-><init>()V

    .line 323
    move-object/from16 v0, v18

    move-object/from16 v1, v19

    iput-object v0, v1, Landroid/support/transition/TransitionValues;->view:Landroid/view/View;

    .line 324
    move-object/from16 v0, p3

    iget-object v0, v0, Landroid/support/transition/TransitionValuesMaps;->viewValues:Landroid/support/v4/util/ArrayMap;

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/support/v4/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object/from16 v21, v0

    check-cast v21, Landroid/support/transition/TransitionValues;

    .line 325
    .local v21, "newValues":Landroid/support/transition/TransitionValues;
    if-eqz v21, :cond_2c4

    .line 326
    const/16 v22, 0x0

    .local v22, "j":I
    :goto_2a7
    move-object/from16 v0, v20

    array-length v0, v0

    move/from16 v1, v22

    if-ge v1, v0, :cond_2c4

    .line 327
    move-object/from16 v0, v19

    iget-object v0, v0, Landroid/support/transition/TransitionValues;->values:Ljava/util/Map;

    aget-object v1, v20, v22

    move-object/from16 v2, v21

    iget-object v2, v2, Landroid/support/transition/TransitionValues;->values:Ljava/util/Map;

    aget-object v3, v20, v22

    .line 328
    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    .line 327
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 326
    add-int/lit8 v22, v22, 0x1

    goto :goto_2a7

    .line 331
    .end local v22    # "j":I
    :cond_2c4
    invoke-virtual {v13}, Landroid/support/v4/util/ArrayMap;->size()I

    move-result v22

    .line 332
    .local v22, "numExistingAnims":I
    const/16 v23, 0x0

    .local v23, "j":I
    :goto_2ca
    move/from16 v0, v23

    move/from16 v1, v22

    if-ge v0, v1, :cond_31e

    .line 333
    move/from16 v0, v23

    invoke-virtual {v13, v0}, Landroid/support/v4/util/ArrayMap;->keyAt(I)Ljava/lang/Object;

    move-result-object v0

    move-object/from16 v24, v0

    check-cast v24, Landroid/animation/Animator;

    .line 334
    .local v24, "anim":Landroid/animation/Animator;
    move-object/from16 v0, v24

    invoke-virtual {v13, v0}, Landroid/support/v4/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object/from16 v25, v0

    check-cast v25, Landroid/support/transition/TransitionPort$AnimationInfo;

    .line 335
    .local v25, "info":Landroid/support/transition/TransitionPort$AnimationInfo;
    move-object/from16 v0, v25

    iget-object v0, v0, Landroid/support/transition/TransitionPort$AnimationInfo;->values:Landroid/support/transition/TransitionValues;

    if-eqz v0, :cond_31b

    move-object/from16 v0, v25

    iget-object v0, v0, Landroid/support/transition/TransitionPort$AnimationInfo;->view:Landroid/view/View;

    move-object/from16 v1, v18

    if-ne v0, v1, :cond_31b

    move-object/from16 v0, v25

    iget-object v0, v0, Landroid/support/transition/TransitionPort$AnimationInfo;->name:Ljava/lang/String;

    if-nez v0, :cond_2fe

    .line 336
    invoke-virtual/range {p0 .. p0}, Landroid/support/transition/TransitionPort;->getName()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_30c

    :cond_2fe
    move-object/from16 v0, v25

    iget-object v0, v0, Landroid/support/transition/TransitionPort$AnimationInfo;->name:Ljava/lang/String;

    .line 337
    invoke-virtual/range {p0 .. p0}, Landroid/support/transition/TransitionPort;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_31b

    .line 338
    :cond_30c
    move-object/from16 v0, v25

    iget-object v0, v0, Landroid/support/transition/TransitionPort$AnimationInfo;->values:Landroid/support/transition/TransitionValues;

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/support/transition/TransitionValues;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_31b

    .line 340
    const/16 v17, 0x0

    .line 341
    goto :goto_31e

    .line 332
    .end local v24    # "anim":Landroid/animation/Animator;
    .end local v25    # "info":Landroid/support/transition/TransitionPort$AnimationInfo;
    :cond_31b
    add-int/lit8 v23, v23, 0x1

    goto :goto_2ca

    .line 346
    .end local v20    # "properties":[Ljava/lang/String;
    .end local v21    # "newValues":Landroid/support/transition/TransitionValues;
    .end local v22    # "numExistingAnims":I
    .end local v23    # "j":I
    :cond_31e
    :goto_31e
    goto :goto_323

    .line 347
    .end local v18    # "view":Landroid/view/View;
    :cond_31f
    iget-object v0, v15, Landroid/support/transition/TransitionValues;->view:Landroid/view/View;

    move-object/from16 v18, v0

    .line 349
    .local v18, "view":Landroid/view/View;
    :goto_323
    if-eqz v17, :cond_348

    .line 350
    new-instance v20, Landroid/support/transition/TransitionPort$AnimationInfo;

    invoke-virtual/range {p0 .. p0}, Landroid/support/transition/TransitionPort;->getName()Ljava/lang/String;

    move-result-object v0

    .line 351
    invoke-static/range {p1 .. p1}, Landroid/support/transition/WindowIdPort;->getWindowId(Landroid/view/View;)Landroid/support/transition/WindowIdPort;

    move-result-object v1

    move-object/from16 v2, v20

    move-object/from16 v3, v18

    move-object/from16 v4, v19

    invoke-direct {v2, v3, v0, v1, v4}, Landroid/support/transition/TransitionPort$AnimationInfo;-><init>(Landroid/view/View;Ljava/lang/String;Landroid/support/transition/WindowIdPort;Landroid/support/transition/TransitionValues;)V

    .line 352
    .local v20, "info":Landroid/support/transition/TransitionPort$AnimationInfo;
    move-object/from16 v0, v17

    move-object/from16 v1, v20

    invoke-virtual {v13, v0, v1}, Landroid/support/v4/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 353
    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/support/transition/TransitionPort;->mAnimators:Ljava/util/ArrayList;

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 288
    .end local v15    # "start":Landroid/support/transition/TransitionValues;
    .end local v16    # "end":Landroid/support/transition/TransitionValues;
    .end local v17    # "animator":Landroid/animation/Animator;
    .end local v18    # "view":Landroid/view/View;
    .end local v19    # "infoValues":Landroid/support/transition/TransitionValues;
    .end local v20    # "info":Landroid/support/transition/TransitionPort$AnimationInfo;
    :cond_348
    add-int/lit8 v14, v14, 0x1

    goto/16 :goto_244

    .line 359
    .end local v14    # "i":I
    :cond_34c
    return-void
.end method

.method protected end()V
    .registers 6

    .line 990
    iget v0, p0, Landroid/support/transition/TransitionPort;->mNumInstances:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Landroid/support/transition/TransitionPort;->mNumInstances:I

    .line 991
    iget v0, p0, Landroid/support/transition/TransitionPort;->mNumInstances:I

    if-nez v0, :cond_6b

    .line 992
    iget-object v0, p0, Landroid/support/transition/TransitionPort;->mListeners:Ljava/util/ArrayList;

    if-eqz v0, :cond_32

    iget-object v0, p0, Landroid/support/transition/TransitionPort;->mListeners:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_32

    .line 993
    iget-object v0, p0, Landroid/support/transition/TransitionPort;->mListeners:Ljava/util/ArrayList;

    .line 994
    invoke-virtual {v0}, Ljava/util/ArrayList;->clone()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Ljava/util/ArrayList;

    .line 995
    .local v2, "tmpListeners":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/support/transition/TransitionPort$TransitionListener;>;"
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 996
    .local v3, "numListeners":I
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_24
    if-ge v4, v3, :cond_32

    .line 997
    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/transition/TransitionPort$TransitionListener;

    invoke-interface {v0, p0}, Landroid/support/transition/TransitionPort$TransitionListener;->onTransitionEnd(Landroid/support/transition/TransitionPort;)V

    .line 996
    add-int/lit8 v4, v4, 0x1

    goto :goto_24

    .line 1000
    .end local v2    # "tmpListeners":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/support/transition/TransitionPort$TransitionListener;>;"
    .end local v2
    .end local v3    # "numListeners":I
    .end local v4    # "i":I
    :cond_32
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_33
    iget-object v0, p0, Landroid/support/transition/TransitionPort;->mStartValues:Landroid/support/transition/TransitionValuesMaps;

    iget-object v0, v0, Landroid/support/transition/TransitionValuesMaps;->itemIdValues:Landroid/support/v4/util/LongSparseArray;

    invoke-virtual {v0}, Landroid/support/v4/util/LongSparseArray;->size()I

    move-result v0

    if-ge v2, v0, :cond_4d

    .line 1001
    iget-object v0, p0, Landroid/support/transition/TransitionPort;->mStartValues:Landroid/support/transition/TransitionValuesMaps;

    iget-object v0, v0, Landroid/support/transition/TransitionValuesMaps;->itemIdValues:Landroid/support/v4/util/LongSparseArray;

    invoke-virtual {v0, v2}, Landroid/support/v4/util/LongSparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Landroid/support/transition/TransitionValues;

    .line 1002
    .local v3, "tv":Landroid/support/transition/TransitionValues;
    iget-object v4, v3, Landroid/support/transition/TransitionValues;->view:Landroid/view/View;

    .line 1000
    .end local v3    # "tv":Landroid/support/transition/TransitionValues;
    add-int/lit8 v2, v2, 0x1

    goto :goto_33

    .line 1007
    .end local v2    # "i":I
    :cond_4d
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_4e
    iget-object v0, p0, Landroid/support/transition/TransitionPort;->mEndValues:Landroid/support/transition/TransitionValuesMaps;

    iget-object v0, v0, Landroid/support/transition/TransitionValuesMaps;->itemIdValues:Landroid/support/v4/util/LongSparseArray;

    invoke-virtual {v0}, Landroid/support/v4/util/LongSparseArray;->size()I

    move-result v0

    if-ge v2, v0, :cond_68

    .line 1008
    iget-object v0, p0, Landroid/support/transition/TransitionPort;->mEndValues:Landroid/support/transition/TransitionValuesMaps;

    iget-object v0, v0, Landroid/support/transition/TransitionValuesMaps;->itemIdValues:Landroid/support/v4/util/LongSparseArray;

    invoke-virtual {v0, v2}, Landroid/support/v4/util/LongSparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Landroid/support/transition/TransitionValues;

    .line 1009
    .local v3, "tv":Landroid/support/transition/TransitionValues;
    iget-object v4, v3, Landroid/support/transition/TransitionValues;->view:Landroid/view/View;

    .line 1007
    .end local v3    # "tv":Landroid/support/transition/TransitionValues;
    add-int/lit8 v2, v2, 0x1

    goto :goto_4e

    .line 1014
    .end local v2    # "i":I
    :cond_68
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/transition/TransitionPort;->mEnded:Z

    .line 1016
    :cond_6b
    return-void
.end method

.method public getDuration()J
    .registers 3

    .line 130
    iget-wide v0, p0, Landroid/support/transition/TransitionPort;->mDuration:J

    return-wide v0
.end method

.method public getInterpolator()Landroid/animation/TimeInterpolator;
    .registers 2

    .line 148
    iget-object v0, p0, Landroid/support/transition/TransitionPort;->mInterpolator:Landroid/animation/TimeInterpolator;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .registers 2

    .line 1096
    iget-object v0, p0, Landroid/support/transition/TransitionPort;->mName:Ljava/lang/String;

    return-object v0
.end method

.method public getStartDelay()J
    .registers 3

    .line 139
    iget-wide v0, p0, Landroid/support/transition/TransitionPort;->mStartDelay:J

    return-wide v0
.end method

.method public getTransitionProperties()[Ljava/lang/String;
    .registers 2

    .line 157
    const/4 v0, 0x0

    return-object v0
.end method

.method isValidTarget(Landroid/view/View;J)Z
    .registers 9
    .param p1, "target"    # Landroid/view/View;
    .param p2, "targetId"    # J

    .line 372
    iget-object v0, p0, Landroid/support/transition/TransitionPort;->mTargetIdExcludes:Ljava/util/ArrayList;

    if-eqz v0, :cond_13

    iget-object v0, p0, Landroid/support/transition/TransitionPort;->mTargetIdExcludes:Ljava/util/ArrayList;

    long-to-int v1, p2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 373
    const/4 v0, 0x0

    return v0

    .line 375
    :cond_13
    iget-object v0, p0, Landroid/support/transition/TransitionPort;->mTargetExcludes:Ljava/util/ArrayList;

    if-eqz v0, :cond_21

    iget-object v0, p0, Landroid/support/transition/TransitionPort;->mTargetExcludes:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_21

    .line 376
    const/4 v0, 0x0

    return v0

    .line 378
    :cond_21
    iget-object v0, p0, Landroid/support/transition/TransitionPort;->mTargetTypeExcludes:Ljava/util/ArrayList;

    if-eqz v0, :cond_44

    if-eqz p1, :cond_44

    .line 379
    iget-object v0, p0, Landroid/support/transition/TransitionPort;->mTargetTypeExcludes:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 380
    .local v2, "numTypes":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_2e
    if-ge v3, v2, :cond_44

    .line 381
    iget-object v0, p0, Landroid/support/transition/TransitionPort;->mTargetTypeExcludes:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Ljava/lang/Class;

    .line 382
    .local v4, "type":Ljava/lang/Class;
    invoke-virtual {v4, p1}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_41

    .line 383
    const/4 v0, 0x0

    return v0

    .line 380
    .end local v4    # "type":Ljava/lang/Class;
    :cond_41
    add-int/lit8 v3, v3, 0x1

    goto :goto_2e

    .line 387
    .end local v2    # "numTypes":I
    .end local v3    # "i":I
    :cond_44
    iget-object v0, p0, Landroid/support/transition/TransitionPort;->mTargetIds:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_56

    iget-object v0, p0, Landroid/support/transition/TransitionPort;->mTargets:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_56

    .line 388
    const/4 v0, 0x1

    return v0

    .line 390
    :cond_56
    iget-object v0, p0, Landroid/support/transition/TransitionPort;->mTargetIds:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_7d

    .line 391
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_5f
    iget-object v0, p0, Landroid/support/transition/TransitionPort;->mTargetIds:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v2, v0, :cond_7d

    .line 392
    iget-object v0, p0, Landroid/support/transition/TransitionPort;->mTargetIds:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-long v0, v0

    cmp-long v0, v0, p2

    if-nez v0, :cond_7a

    .line 393
    const/4 v0, 0x1

    return v0

    .line 391
    :cond_7a
    add-int/lit8 v2, v2, 0x1

    goto :goto_5f

    .line 397
    .end local v2    # "i":I
    :cond_7d
    if-eqz p1, :cond_9d

    iget-object v0, p0, Landroid/support/transition/TransitionPort;->mTargets:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_9d

    .line 398
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_88
    iget-object v0, p0, Landroid/support/transition/TransitionPort;->mTargets:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v2, v0, :cond_9d

    .line 399
    iget-object v0, p0, Landroid/support/transition/TransitionPort;->mTargets:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-ne v0, p1, :cond_9a

    .line 400
    const/4 v0, 0x1

    return v0

    .line 398
    :cond_9a
    add-int/lit8 v2, v2, 0x1

    goto :goto_88

    .line 404
    .end local v2    # "i":I
    :cond_9d
    const/4 v0, 0x0

    return v0
.end method

.method public pause(Landroid/view/View;)V
    .registers 9
    .param p1, "sceneRoot"    # Landroid/view/View;

    .line 799
    iget-boolean v0, p0, Landroid/support/transition/TransitionPort;->mEnded:Z

    if-nez v0, :cond_5f

    .line 800
    invoke-static {}, Landroid/support/transition/TransitionPort;->getRunningAnimators()Landroid/support/v4/util/ArrayMap;

    move-result-object v1

    .line 801
    .local v1, "runningAnimators":Landroid/support/v4/util/ArrayMap;, "Landroid/support/v4/util/ArrayMap<Landroid/animation/Animator;Landroid/support/transition/TransitionPort$AnimationInfo;>;"
    invoke-virtual {v1}, Landroid/support/v4/util/ArrayMap;->size()I

    move-result v2

    .line 802
    .local v2, "numOldAnims":I
    invoke-static {p1}, Landroid/support/transition/WindowIdPort;->getWindowId(Landroid/view/View;)Landroid/support/transition/WindowIdPort;

    move-result-object v3

    .line 803
    .local v3, "windowId":Landroid/support/transition/WindowIdPort;
    add-int/lit8 v4, v2, -0x1

    .local v4, "i":I
    :goto_12
    if-ltz v4, :cond_34

    .line 804
    invoke-virtual {v1, v4}, Landroid/support/v4/util/ArrayMap;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Landroid/support/transition/TransitionPort$AnimationInfo;

    .line 805
    .local v5, "info":Landroid/support/transition/TransitionPort$AnimationInfo;
    iget-object v0, v5, Landroid/support/transition/TransitionPort$AnimationInfo;->view:Landroid/view/View;

    if-eqz v0, :cond_31

    iget-object v0, v5, Landroid/support/transition/TransitionPort$AnimationInfo;->windowId:Landroid/support/transition/WindowIdPort;

    invoke-virtual {v3, v0}, Landroid/support/transition/WindowIdPort;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_31

    .line 806
    invoke-virtual {v1, v4}, Landroid/support/v4/util/ArrayMap;->keyAt(I)Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Landroid/animation/Animator;

    .line 807
    .local v6, "anim":Landroid/animation/Animator;
    invoke-virtual {v6}, Landroid/animation/Animator;->cancel()V

    .line 803
    .end local v5    # "info":Landroid/support/transition/TransitionPort$AnimationInfo;
    .end local v6    # "anim":Landroid/animation/Animator;
    :cond_31
    add-int/lit8 v4, v4, -0x1

    goto :goto_12

    .line 810
    .end local v4    # "i":I
    :cond_34
    iget-object v0, p0, Landroid/support/transition/TransitionPort;->mListeners:Ljava/util/ArrayList;

    if-eqz v0, :cond_5c

    iget-object v0, p0, Landroid/support/transition/TransitionPort;->mListeners:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_5c

    .line 811
    iget-object v0, p0, Landroid/support/transition/TransitionPort;->mListeners:Ljava/util/ArrayList;

    .line 812
    invoke-virtual {v0}, Ljava/util/ArrayList;->clone()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Ljava/util/ArrayList;

    .line 813
    .local v4, "tmpListeners":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/support/transition/TransitionPort$TransitionListener;>;"
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v5

    .line 814
    .local v5, "numListeners":I
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_4e
    if-ge v6, v5, :cond_5c

    .line 815
    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/transition/TransitionPort$TransitionListener;

    invoke-interface {v0, p0}, Landroid/support/transition/TransitionPort$TransitionListener;->onTransitionPause(Landroid/support/transition/TransitionPort;)V

    .line 814
    add-int/lit8 v6, v6, 0x1

    goto :goto_4e

    .line 818
    .end local v4    # "tmpListeners":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/support/transition/TransitionPort$TransitionListener;>;"
    .end local v4
    .end local v5    # "numListeners":I
    .end local v6    # "i":I
    :cond_5c
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/transition/TransitionPort;->mPaused:Z

    .line 820
    .end local v1    # "runningAnimators":Landroid/support/v4/util/ArrayMap;, "Landroid/support/v4/util/ArrayMap<Landroid/animation/Animator;Landroid/support/transition/TransitionPort$AnimationInfo;>;"
    .end local v1
    .end local v2    # "numOldAnims":I
    .end local v3    # "windowId":Landroid/support/transition/WindowIdPort;
    :cond_5f
    return-void
.end method

.method playTransition(Landroid/view/ViewGroup;)V
    .registers 19
    .param p1, "sceneRoot"    # Landroid/view/ViewGroup;

    .line 862
    invoke-static {}, Landroid/support/transition/TransitionPort;->getRunningAnimators()Landroid/support/v4/util/ArrayMap;

    move-result-object v4

    .line 863
    .local v4, "runningAnimators":Landroid/support/v4/util/ArrayMap;, "Landroid/support/v4/util/ArrayMap<Landroid/animation/Animator;Landroid/support/transition/TransitionPort$AnimationInfo;>;"
    invoke-virtual {v4}, Landroid/support/v4/util/ArrayMap;->size()I

    move-result v5

    .line 864
    .local v5, "numOldAnims":I
    add-int/lit8 v6, v5, -0x1

    .local v6, "i":I
    :goto_a
    if-ltz v6, :cond_ac

    .line 865
    invoke-virtual {v4, v6}, Landroid/support/v4/util/ArrayMap;->keyAt(I)Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Landroid/animation/Animator;

    .line 866
    .local v7, "anim":Landroid/animation/Animator;
    if-eqz v7, :cond_a8

    .line 867
    invoke-virtual {v4, v7}, Landroid/support/v4/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Landroid/support/transition/TransitionPort$AnimationInfo;

    .line 868
    .local v8, "oldInfo":Landroid/support/transition/TransitionPort$AnimationInfo;
    if-eqz v8, :cond_a8

    iget-object v0, v8, Landroid/support/transition/TransitionPort$AnimationInfo;->view:Landroid/view/View;

    if-eqz v0, :cond_a8

    iget-object v0, v8, Landroid/support/transition/TransitionPort$AnimationInfo;->view:Landroid/view/View;

    .line 869
    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual/range {p1 .. p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    if-ne v0, v1, :cond_a8

    .line 870
    const/4 v9, 0x0

    .line 871
    .local v9, "cancel":Z
    iget-object v10, v8, Landroid/support/transition/TransitionPort$AnimationInfo;->values:Landroid/support/transition/TransitionValues;

    .line 872
    .local v10, "oldValues":Landroid/support/transition/TransitionValues;
    iget-object v11, v8, Landroid/support/transition/TransitionPort$AnimationInfo;->view:Landroid/view/View;

    .line 873
    .local v11, "oldView":Landroid/view/View;
    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/support/transition/TransitionPort;->mEndValues:Landroid/support/transition/TransitionValuesMaps;

    iget-object v0, v0, Landroid/support/transition/TransitionValuesMaps;->viewValues:Landroid/support/v4/util/ArrayMap;

    if-eqz v0, :cond_49

    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/support/transition/TransitionPort;->mEndValues:Landroid/support/transition/TransitionValuesMaps;

    iget-object v0, v0, Landroid/support/transition/TransitionValuesMaps;->viewValues:Landroid/support/v4/util/ArrayMap;

    .line 874
    invoke-virtual {v0, v11}, Landroid/support/v4/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v12, v0

    check-cast v12, Landroid/support/transition/TransitionValues;

    goto :goto_4a

    :cond_49
    const/4 v12, 0x0

    .line 875
    .local v12, "newValues":Landroid/support/transition/TransitionValues;
    :goto_4a
    if-nez v12, :cond_5d

    .line 876
    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/support/transition/TransitionPort;->mEndValues:Landroid/support/transition/TransitionValuesMaps;

    iget-object v0, v0, Landroid/support/transition/TransitionValuesMaps;->idValues:Landroid/util/SparseArray;

    invoke-virtual {v11}, Landroid/view/View;->getId()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v12, v0

    check-cast v12, Landroid/support/transition/TransitionValues;

    .line 878
    :cond_5d
    if-eqz v10, :cond_93

    .line 881
    if-eqz v12, :cond_93

    .line 882
    iget-object v0, v10, Landroid/support/transition/TransitionValues;->values:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :goto_6b
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_93

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v14, v0

    check-cast v14, Ljava/lang/String;

    .line 883
    .local v14, "key":Ljava/lang/String;
    iget-object v0, v10, Landroid/support/transition/TransitionValues;->values:Ljava/util/Map;

    invoke-interface {v0, v14}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v15

    .line 884
    .local v15, "oldValue":Ljava/lang/Object;
    iget-object v0, v12, Landroid/support/transition/TransitionValues;->values:Ljava/util/Map;

    invoke-interface {v0, v14}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v16

    .line 885
    .local v16, "newValue":Ljava/lang/Object;
    if-eqz v15, :cond_92

    if-eqz v16, :cond_92

    .line 886
    move-object/from16 v0, v16

    invoke-virtual {v15, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_92

    .line 887
    const/4 v9, 0x1

    .line 893
    goto :goto_93

    .line 895
    .end local v14    # "key":Ljava/lang/String;
    .end local v15    # "oldValue":Ljava/lang/Object;
    .end local v16    # "newValue":Ljava/lang/Object;
    :cond_92
    goto :goto_6b

    .line 898
    :cond_93
    :goto_93
    if-eqz v9, :cond_a8

    .line 899
    invoke-virtual {v7}, Landroid/animation/Animator;->isRunning()Z

    move-result v0

    if-nez v0, :cond_a1

    invoke-virtual {v7}, Landroid/animation/Animator;->isStarted()Z

    move-result v0

    if-eqz v0, :cond_a5

    .line 903
    :cond_a1
    invoke-virtual {v7}, Landroid/animation/Animator;->cancel()V

    goto :goto_a8

    .line 908
    :cond_a5
    invoke-virtual {v4, v7}, Landroid/support/v4/util/ArrayMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 864
    .end local v7    # "anim":Landroid/animation/Animator;
    .end local v8    # "oldInfo":Landroid/support/transition/TransitionPort$AnimationInfo;
    .end local v9    # "cancel":Z
    .end local v10    # "oldValues":Landroid/support/transition/TransitionValues;
    .end local v11    # "oldView":Landroid/view/View;
    .end local v12    # "newValues":Landroid/support/transition/TransitionValues;
    :cond_a8
    :goto_a8
    add-int/lit8 v6, v6, -0x1

    goto/16 :goto_a

    .line 915
    .end local v6    # "i":I
    :cond_ac
    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/support/transition/TransitionPort;->mStartValues:Landroid/support/transition/TransitionValuesMaps;

    move-object/from16 v1, p0

    iget-object v1, v1, Landroid/support/transition/TransitionPort;->mEndValues:Landroid/support/transition/TransitionValuesMaps;

    move-object/from16 v2, p0

    move-object/from16 v3, p1

    invoke-virtual {v2, v3, v0, v1}, Landroid/support/transition/TransitionPort;->createAnimators(Landroid/view/ViewGroup;Landroid/support/transition/TransitionValuesMaps;Landroid/support/transition/TransitionValuesMaps;)V

    .line 916
    invoke-virtual/range {p0 .. p0}, Landroid/support/transition/TransitionPort;->runAnimators()V

    .line 917
    return-void
.end method

.method public removeListener(Landroid/support/transition/TransitionPort$TransitionListener;)Landroid/support/transition/TransitionPort;
    .registers 3
    .param p1, "listener"    # Landroid/support/transition/TransitionPort$TransitionListener;

    .line 1057
    iget-object v0, p0, Landroid/support/transition/TransitionPort;->mListeners:Ljava/util/ArrayList;

    if-nez v0, :cond_5

    .line 1058
    return-object p0

    .line 1060
    :cond_5
    iget-object v0, p0, Landroid/support/transition/TransitionPort;->mListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 1061
    iget-object v0, p0, Landroid/support/transition/TransitionPort;->mListeners:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_15

    .line 1062
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/transition/TransitionPort;->mListeners:Ljava/util/ArrayList;

    .line 1064
    :cond_15
    return-object p0
.end method

.method public resume(Landroid/view/View;)V
    .registers 9
    .param p1, "sceneRoot"    # Landroid/view/View;

    .line 831
    iget-boolean v0, p0, Landroid/support/transition/TransitionPort;->mPaused:Z

    if-eqz v0, :cond_63

    .line 832
    iget-boolean v0, p0, Landroid/support/transition/TransitionPort;->mEnded:Z

    if-nez v0, :cond_60

    .line 833
    invoke-static {}, Landroid/support/transition/TransitionPort;->getRunningAnimators()Landroid/support/v4/util/ArrayMap;

    move-result-object v1

    .line 834
    .local v1, "runningAnimators":Landroid/support/v4/util/ArrayMap;, "Landroid/support/v4/util/ArrayMap<Landroid/animation/Animator;Landroid/support/transition/TransitionPort$AnimationInfo;>;"
    invoke-virtual {v1}, Landroid/support/v4/util/ArrayMap;->size()I

    move-result v2

    .line 835
    .local v2, "numOldAnims":I
    invoke-static {p1}, Landroid/support/transition/WindowIdPort;->getWindowId(Landroid/view/View;)Landroid/support/transition/WindowIdPort;

    move-result-object v3

    .line 836
    .local v3, "windowId":Landroid/support/transition/WindowIdPort;
    add-int/lit8 v4, v2, -0x1

    .local v4, "i":I
    :goto_16
    if-ltz v4, :cond_38

    .line 837
    invoke-virtual {v1, v4}, Landroid/support/v4/util/ArrayMap;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Landroid/support/transition/TransitionPort$AnimationInfo;

    .line 838
    .local v5, "info":Landroid/support/transition/TransitionPort$AnimationInfo;
    iget-object v0, v5, Landroid/support/transition/TransitionPort$AnimationInfo;->view:Landroid/view/View;

    if-eqz v0, :cond_35

    iget-object v0, v5, Landroid/support/transition/TransitionPort$AnimationInfo;->windowId:Landroid/support/transition/WindowIdPort;

    invoke-virtual {v3, v0}, Landroid/support/transition/WindowIdPort;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_35

    .line 839
    invoke-virtual {v1, v4}, Landroid/support/v4/util/ArrayMap;->keyAt(I)Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Landroid/animation/Animator;

    .line 840
    .local v6, "anim":Landroid/animation/Animator;
    invoke-virtual {v6}, Landroid/animation/Animator;->end()V

    .line 836
    .end local v5    # "info":Landroid/support/transition/TransitionPort$AnimationInfo;
    .end local v6    # "anim":Landroid/animation/Animator;
    :cond_35
    add-int/lit8 v4, v4, -0x1

    goto :goto_16

    .line 843
    .end local v4    # "i":I
    :cond_38
    iget-object v0, p0, Landroid/support/transition/TransitionPort;->mListeners:Ljava/util/ArrayList;

    if-eqz v0, :cond_60

    iget-object v0, p0, Landroid/support/transition/TransitionPort;->mListeners:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_60

    .line 844
    iget-object v0, p0, Landroid/support/transition/TransitionPort;->mListeners:Ljava/util/ArrayList;

    .line 845
    invoke-virtual {v0}, Ljava/util/ArrayList;->clone()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Ljava/util/ArrayList;

    .line 846
    .local v4, "tmpListeners":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/support/transition/TransitionPort$TransitionListener;>;"
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v5

    .line 847
    .local v5, "numListeners":I
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_52
    if-ge v6, v5, :cond_60

    .line 848
    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/transition/TransitionPort$TransitionListener;

    invoke-interface {v0, p0}, Landroid/support/transition/TransitionPort$TransitionListener;->onTransitionResume(Landroid/support/transition/TransitionPort;)V

    .line 847
    add-int/lit8 v6, v6, 0x1

    goto :goto_52

    .line 852
    .end local v1    # "runningAnimators":Landroid/support/v4/util/ArrayMap;, "Landroid/support/v4/util/ArrayMap<Landroid/animation/Animator;Landroid/support/transition/TransitionPort$AnimationInfo;>;"
    .end local v1
    .end local v2    # "numOldAnims":I
    .end local v3    # "windowId":Landroid/support/transition/WindowIdPort;
    .end local v4    # "tmpListeners":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/support/transition/TransitionPort$TransitionListener;>;"
    .end local v4
    .end local v5    # "numListeners":I
    .end local v6    # "i":I
    :cond_60
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/transition/TransitionPort;->mPaused:Z

    .line 854
    :cond_63
    return-void
.end method

.method protected runAnimators()V
    .registers 5

    .line 418
    invoke-virtual {p0}, Landroid/support/transition/TransitionPort;->start()V

    .line 419
    invoke-static {}, Landroid/support/transition/TransitionPort;->getRunningAnimators()Landroid/support/v4/util/ArrayMap;

    move-result-object v1

    .line 421
    .local v1, "runningAnimators":Landroid/support/v4/util/ArrayMap;, "Landroid/support/v4/util/ArrayMap<Landroid/animation/Animator;Landroid/support/transition/TransitionPort$AnimationInfo;>;"
    iget-object v0, p0, Landroid/support/transition/TransitionPort;->mAnimators:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_d
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_27

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Landroid/animation/Animator;

    .line 425
    .local v3, "anim":Landroid/animation/Animator;
    invoke-virtual {v1, v3}, Landroid/support/v4/util/ArrayMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_26

    .line 426
    invoke-virtual {p0}, Landroid/support/transition/TransitionPort;->start()V

    .line 427
    invoke-direct {p0, v3, v1}, Landroid/support/transition/TransitionPort;->runAnimator(Landroid/animation/Animator;Landroid/support/v4/util/ArrayMap;)V

    .line 429
    .end local v3    # "anim":Landroid/animation/Animator;
    :cond_26
    goto :goto_d

    .line 430
    :cond_27
    iget-object v0, p0, Landroid/support/transition/TransitionPort;->mAnimators:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 431
    invoke-virtual {p0}, Landroid/support/transition/TransitionPort;->end()V

    .line 432
    return-void
.end method

.method public setDuration(J)Landroid/support/transition/TransitionPort;
    .registers 3
    .param p1, "duration"    # J

    .line 134
    iput-wide p1, p0, Landroid/support/transition/TransitionPort;->mDuration:J

    .line 135
    return-object p0
.end method

.method public setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/support/transition/TransitionPort;
    .registers 2
    .param p1, "interpolator"    # Landroid/animation/TimeInterpolator;

    .line 152
    iput-object p1, p0, Landroid/support/transition/TransitionPort;->mInterpolator:Landroid/animation/TimeInterpolator;

    .line 153
    return-object p0
.end method

.method protected start()V
    .registers 6

    .line 963
    iget v0, p0, Landroid/support/transition/TransitionPort;->mNumInstances:I

    if-nez v0, :cond_2f

    .line 964
    iget-object v0, p0, Landroid/support/transition/TransitionPort;->mListeners:Ljava/util/ArrayList;

    if-eqz v0, :cond_2c

    iget-object v0, p0, Landroid/support/transition/TransitionPort;->mListeners:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_2c

    .line 965
    iget-object v0, p0, Landroid/support/transition/TransitionPort;->mListeners:Ljava/util/ArrayList;

    .line 966
    invoke-virtual {v0}, Ljava/util/ArrayList;->clone()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Ljava/util/ArrayList;

    .line 967
    .local v2, "tmpListeners":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/support/transition/TransitionPort$TransitionListener;>;"
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 968
    .local v3, "numListeners":I
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_1e
    if-ge v4, v3, :cond_2c

    .line 969
    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/transition/TransitionPort$TransitionListener;

    invoke-interface {v0, p0}, Landroid/support/transition/TransitionPort$TransitionListener;->onTransitionStart(Landroid/support/transition/TransitionPort;)V

    .line 968
    add-int/lit8 v4, v4, 0x1

    goto :goto_1e

    .line 972
    .end local v2    # "tmpListeners":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/support/transition/TransitionPort$TransitionListener;>;"
    .end local v2
    .end local v3    # "numListeners":I
    .end local v4    # "i":I
    :cond_2c
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/transition/TransitionPort;->mEnded:Z

    .line 974
    :cond_2f
    iget v0, p0, Landroid/support/transition/TransitionPort;->mNumInstances:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Landroid/support/transition/TransitionPort;->mNumInstances:I

    .line 975
    return-void
.end method

.method public toString()Ljava/lang/String;
    .registers 2

    .line 1078
    const-string v0, ""

    invoke-virtual {p0, v0}, Landroid/support/transition/TransitionPort;->toString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method toString(Ljava/lang/String;)Ljava/lang/String;
    .registers 8
    .param p1, "indent"    # Ljava/lang/String;

    .line 1100
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "@"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 1101
    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ": "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1102
    .local v4, "result":Ljava/lang/String;
    iget-wide v0, p0, Landroid/support/transition/TransitionPort;->mDuration:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_58

    .line 1103
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "dur("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Landroid/support/transition/TransitionPort;->mDuration:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ") "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1105
    :cond_58
    iget-wide v0, p0, Landroid/support/transition/TransitionPort;->mStartDelay:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_7f

    .line 1106
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "dly("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Landroid/support/transition/TransitionPort;->mStartDelay:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ") "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1108
    :cond_7f
    iget-object v0, p0, Landroid/support/transition/TransitionPort;->mInterpolator:Landroid/animation/TimeInterpolator;

    if-eqz v0, :cond_a2

    .line 1109
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "interp("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Landroid/support/transition/TransitionPort;->mInterpolator:Landroid/animation/TimeInterpolator;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ") "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1111
    :cond_a2
    iget-object v0, p0, Landroid/support/transition/TransitionPort;->mTargetIds:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-gtz v0, :cond_b2

    iget-object v0, p0, Landroid/support/transition/TransitionPort;->mTargets:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_158

    .line 1112
    :cond_b2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "tgts("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1113
    iget-object v0, p0, Landroid/support/transition/TransitionPort;->mTargetIds:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_105

    .line 1114
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_ce
    iget-object v0, p0, Landroid/support/transition/TransitionPort;->mTargetIds:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v5, v0, :cond_105

    .line 1115
    if-lez v5, :cond_eb

    .line 1116
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1118
    :cond_eb
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Landroid/support/transition/TransitionPort;->mTargetIds:Ljava/util/ArrayList;

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1114
    add-int/lit8 v5, v5, 0x1

    goto :goto_ce

    .line 1121
    .end local v5    # "i":I
    :cond_105
    iget-object v0, p0, Landroid/support/transition/TransitionPort;->mTargets:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_145

    .line 1122
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_10e
    iget-object v0, p0, Landroid/support/transition/TransitionPort;->mTargets:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v5, v0, :cond_145

    .line 1123
    if-lez v5, :cond_12b

    .line 1124
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1126
    :cond_12b
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Landroid/support/transition/TransitionPort;->mTargets:Ljava/util/ArrayList;

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1122
    add-int/lit8 v5, v5, 0x1

    goto :goto_10e

    .line 1129
    .end local v5    # "i":I
    :cond_145
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1131
    :cond_158
    return-object v4
.end method

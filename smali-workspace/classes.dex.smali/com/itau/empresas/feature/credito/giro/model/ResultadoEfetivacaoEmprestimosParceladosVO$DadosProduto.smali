.class public Lcom/itau/empresas/feature/credito/giro/model/ResultadoEfetivacaoEmprestimosParceladosVO$DadosProduto;
.super Ljava/lang/Object;
.source "ResultadoEfetivacaoEmprestimosParceladosVO.java"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/itau/empresas/feature/credito/giro/model/ResultadoEfetivacaoEmprestimosParceladosVO;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "DadosProduto"
.end annotation


# instance fields
.field private codigoProduto:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "codigo_produto"
    .end annotation
.end field

.field private descricaoProduto:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "descricao_produto"
    .end annotation
.end field

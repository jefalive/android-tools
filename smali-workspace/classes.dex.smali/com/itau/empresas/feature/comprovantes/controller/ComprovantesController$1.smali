.class Lcom/itau/empresas/feature/comprovantes/controller/ComprovantesController$1;
.super Ljava/lang/Object;
.source "ComprovantesController.java"

# interfaces
.implements Lbr/com/itau/sdk/android/core/type/BackendCall;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/itau/empresas/feature/comprovantes/controller/ComprovantesController;->buscacomprovantes(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/itau/empresas/feature/comprovantes/controller/ComprovantesController;

.field final synthetic val$dataFim:Ljava/lang/String;

.field final synthetic val$dataInicio:Ljava/lang/String;

.field final synthetic val$quantidadeRegistros:Ljava/lang/String;

.field final synthetic val$tipo:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/itau/empresas/feature/comprovantes/controller/ComprovantesController;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 6
    .param p1, "this$0"    # Lcom/itau/empresas/feature/comprovantes/controller/ComprovantesController;

    .line 25
    iput-object p1, p0, Lcom/itau/empresas/feature/comprovantes/controller/ComprovantesController$1;->this$0:Lcom/itau/empresas/feature/comprovantes/controller/ComprovantesController;

    iput-object p2, p0, Lcom/itau/empresas/feature/comprovantes/controller/ComprovantesController$1;->val$dataInicio:Ljava/lang/String;

    iput-object p3, p0, Lcom/itau/empresas/feature/comprovantes/controller/ComprovantesController$1;->val$dataFim:Ljava/lang/String;

    iput-object p4, p0, Lcom/itau/empresas/feature/comprovantes/controller/ComprovantesController$1;->val$quantidadeRegistros:Ljava/lang/String;

    iput-object p5, p0, Lcom/itau/empresas/feature/comprovantes/controller/ComprovantesController$1;->val$tipo:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public callBackend()V
    .registers 9

    .line 28
    iget-object v0, p0, Lcom/itau/empresas/feature/comprovantes/controller/ComprovantesController$1;->this$0:Lcom/itau/empresas/feature/comprovantes/controller/ComprovantesController;

    # invokes: Lcom/itau/empresas/feature/comprovantes/controller/ComprovantesController;->api()Ljava/lang/Object;
    invoke-static {v0}, Lcom/itau/empresas/feature/comprovantes/controller/ComprovantesController;->access$000(Lcom/itau/empresas/feature/comprovantes/controller/ComprovantesController;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/api/Api;

    iget-object v1, p0, Lcom/itau/empresas/feature/comprovantes/controller/ComprovantesController$1;->this$0:Lcom/itau/empresas/feature/comprovantes/controller/ComprovantesController;

    iget-object v1, v1, Lcom/itau/empresas/feature/comprovantes/controller/ComprovantesController;->app:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v1}, Lcom/itau/empresas/CustomApplication;->getContaSelecionada()Lcom/itau/empresas/api/model/ContaOperadorVO;

    move-result-object v1

    invoke-virtual {v1}, Lcom/itau/empresas/api/model/ContaOperadorVO;->getAgencia()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/itau/empresas/feature/comprovantes/controller/ComprovantesController$1;->this$0:Lcom/itau/empresas/feature/comprovantes/controller/ComprovantesController;

    iget-object v2, v2, Lcom/itau/empresas/feature/comprovantes/controller/ComprovantesController;->app:Lcom/itau/empresas/CustomApplication;

    .line 29
    invoke-virtual {v2}, Lcom/itau/empresas/CustomApplication;->getContaSelecionada()Lcom/itau/empresas/api/model/ContaOperadorVO;

    move-result-object v2

    invoke-virtual {v2}, Lcom/itau/empresas/api/model/ContaOperadorVO;->getConta()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/itau/empresas/feature/comprovantes/controller/ComprovantesController$1;->this$0:Lcom/itau/empresas/feature/comprovantes/controller/ComprovantesController;

    iget-object v3, v3, Lcom/itau/empresas/feature/comprovantes/controller/ComprovantesController;->app:Lcom/itau/empresas/CustomApplication;

    .line 30
    invoke-virtual {v3}, Lcom/itau/empresas/CustomApplication;->getContaSelecionada()Lcom/itau/empresas/api/model/ContaOperadorVO;

    move-result-object v3

    invoke-virtual {v3}, Lcom/itau/empresas/api/model/ContaOperadorVO;->getDac()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/itau/empresas/feature/comprovantes/controller/ComprovantesController$1;->val$dataInicio:Ljava/lang/String;

    iget-object v5, p0, Lcom/itau/empresas/feature/comprovantes/controller/ComprovantesController$1;->val$dataFim:Ljava/lang/String;

    iget-object v6, p0, Lcom/itau/empresas/feature/comprovantes/controller/ComprovantesController$1;->val$quantidadeRegistros:Ljava/lang/String;

    iget-object v7, p0, Lcom/itau/empresas/feature/comprovantes/controller/ComprovantesController$1;->val$tipo:Ljava/lang/String;

    .line 28
    invoke-interface/range {v0 .. v7}, Lcom/itau/empresas/api/Api;->buscaComprovantes(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    # invokes: Lcom/itau/empresas/feature/comprovantes/controller/ComprovantesController;->post(Ljava/lang/Object;)V
    invoke-static {v0}, Lcom/itau/empresas/feature/comprovantes/controller/ComprovantesController;->access$100(Ljava/lang/Object;)V

    .line 35
    return-void
.end method

.class public final enum Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintExceptionResult;
.super Ljava/lang/Enum;
.source "ItauFingerprintExceptionResult.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintExceptionResult;>;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintExceptionResult;

.field public static final enum STATUS_AUTHENTIFICATION_FAILED:Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintExceptionResult;

.field public static final enum STATUS_BUTTON_PRESSED:Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintExceptionResult;

.field public static final enum STATUS_OPERATION_DENIED:Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintExceptionResult;

.field public static final enum STATUS_QUALITY_FAILED:Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintExceptionResult;

.field public static final enum STATUS_SENSOR_ERROR:Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintExceptionResult;

.field public static final enum STATUS_TIMEOUT:Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintExceptionResult;

.field public static final enum STATUS_USER_CANCELLED:Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintExceptionResult;

.field public static final enum STATUS_USER_CANCELLED_BY_TOUCH_OUTSIDE:Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintExceptionResult;

.field public static final enum UNEXPECTED:Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintExceptionResult;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .line 4
    new-instance v0, Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintExceptionResult;

    const-string v1, "STATUS_TIMEOUT"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintExceptionResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintExceptionResult;->STATUS_TIMEOUT:Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintExceptionResult;

    .line 5
    new-instance v0, Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintExceptionResult;

    const-string v1, "STATUS_SENSOR_ERROR"

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintExceptionResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintExceptionResult;->STATUS_SENSOR_ERROR:Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintExceptionResult;

    .line 6
    new-instance v0, Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintExceptionResult;

    const-string v1, "STATUS_USER_CANCELLED"

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2}, Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintExceptionResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintExceptionResult;->STATUS_USER_CANCELLED:Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintExceptionResult;

    .line 7
    new-instance v0, Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintExceptionResult;

    const-string v1, "STATUS_QUALITY_FAILED"

    const/4 v2, 0x3

    invoke-direct {v0, v1, v2}, Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintExceptionResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintExceptionResult;->STATUS_QUALITY_FAILED:Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintExceptionResult;

    .line 8
    new-instance v0, Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintExceptionResult;

    const-string v1, "STATUS_USER_CANCELLED_BY_TOUCH_OUTSIDE"

    const/4 v2, 0x4

    invoke-direct {v0, v1, v2}, Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintExceptionResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintExceptionResult;->STATUS_USER_CANCELLED_BY_TOUCH_OUTSIDE:Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintExceptionResult;

    .line 9
    new-instance v0, Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintExceptionResult;

    const-string v1, "STATUS_BUTTON_PRESSED"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintExceptionResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintExceptionResult;->STATUS_BUTTON_PRESSED:Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintExceptionResult;

    .line 10
    new-instance v0, Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintExceptionResult;

    const-string v1, "STATUS_OPERATION_DENIED"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintExceptionResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintExceptionResult;->STATUS_OPERATION_DENIED:Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintExceptionResult;

    .line 11
    new-instance v0, Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintExceptionResult;

    const-string v1, "STATUS_AUTHENTIFICATION_FAILED"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintExceptionResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintExceptionResult;->STATUS_AUTHENTIFICATION_FAILED:Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintExceptionResult;

    .line 12
    new-instance v0, Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintExceptionResult;

    const-string v1, "UNEXPECTED"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintExceptionResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintExceptionResult;->UNEXPECTED:Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintExceptionResult;

    .line 3
    const/16 v0, 0x9

    new-array v0, v0, [Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintExceptionResult;

    sget-object v1, Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintExceptionResult;->STATUS_TIMEOUT:Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintExceptionResult;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sget-object v1, Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintExceptionResult;->STATUS_SENSOR_ERROR:Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintExceptionResult;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    sget-object v1, Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintExceptionResult;->STATUS_USER_CANCELLED:Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintExceptionResult;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    sget-object v1, Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintExceptionResult;->STATUS_QUALITY_FAILED:Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintExceptionResult;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    sget-object v1, Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintExceptionResult;->STATUS_USER_CANCELLED_BY_TOUCH_OUTSIDE:Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintExceptionResult;

    const/4 v2, 0x4

    aput-object v1, v0, v2

    sget-object v1, Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintExceptionResult;->STATUS_BUTTON_PRESSED:Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintExceptionResult;

    const/4 v2, 0x5

    aput-object v1, v0, v2

    sget-object v1, Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintExceptionResult;->STATUS_OPERATION_DENIED:Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintExceptionResult;

    const/4 v2, 0x6

    aput-object v1, v0, v2

    sget-object v1, Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintExceptionResult;->STATUS_AUTHENTIFICATION_FAILED:Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintExceptionResult;

    const/4 v2, 0x7

    aput-object v1, v0, v2

    sget-object v1, Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintExceptionResult;->UNEXPECTED:Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintExceptionResult;

    const/16 v2, 0x8

    aput-object v1, v0, v2

    sput-object v0, Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintExceptionResult;->$VALUES:[Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintExceptionResult;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 3
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintExceptionResult;
    .registers 2
    .param p0, "name"    # Ljava/lang/String;

    .line 3
    const-class v0, Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintExceptionResult;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintExceptionResult;

    return-object v0
.end method

.method public static values()[Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintExceptionResult;
    .registers 1

    .line 3
    sget-object v0, Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintExceptionResult;->$VALUES:[Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintExceptionResult;

    invoke-virtual {v0}, [Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintExceptionResult;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintExceptionResult;

    return-object v0
.end method

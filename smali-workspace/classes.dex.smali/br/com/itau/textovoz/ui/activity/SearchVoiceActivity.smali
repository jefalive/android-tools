.class public Lbr/com/itau/textovoz/ui/activity/SearchVoiceActivity;
.super Lbr/com/itau/textovoz/ui/activity/BaseActivity;
.source "SearchVoiceActivity.java"

# interfaces
.implements Landroid/speech/RecognitionListener;


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private customTransitional:Lbr/com/itau/textovoz/ui/view/custom/CustomTextView;

.field private linearDescription:Landroid/widget/LinearLayout;

.field private linearHelp:Landroid/widget/LinearLayout;

.field private linearSuggestions:Landroid/widget/LinearLayout;

.field private linearTransitional:Landroid/widget/LinearLayout;

.field private listing:Z

.field private final onClickListener:Landroid/view/View$OnClickListener;

.field private performingSpeechSetup:Z

.field private soundIndicator:Lbr/com/itau/textovoz/ui/view/custom/SoundIndicator;

.field private speechRecognizer:Landroid/speech/SpeechRecognizer;

.field private speechRecognizerIntent:Landroid/content/Intent;

.field private textViewTitle:Landroid/widget/TextView;

.field private textViewVoice:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .line 30
    const-class v0, Lbr/com/itau/textovoz/ui/activity/SearchVoiceActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lbr/com/itau/textovoz/ui/activity/SearchVoiceActivity;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .registers 2

    .line 26
    invoke-direct {p0}, Lbr/com/itau/textovoz/ui/activity/BaseActivity;-><init>()V

    .line 45
    new-instance v0, Lbr/com/itau/textovoz/ui/activity/SearchVoiceActivity$1;

    invoke-direct {v0, p0}, Lbr/com/itau/textovoz/ui/activity/SearchVoiceActivity$1;-><init>(Lbr/com/itau/textovoz/ui/activity/SearchVoiceActivity;)V

    iput-object v0, p0, Lbr/com/itau/textovoz/ui/activity/SearchVoiceActivity;->onClickListener:Landroid/view/View$OnClickListener;

    return-void
.end method

.method static synthetic access$000(Lbr/com/itau/textovoz/ui/activity/SearchVoiceActivity;)V
    .registers 1
    .param p0, "x0"    # Lbr/com/itau/textovoz/ui/activity/SearchVoiceActivity;

    .line 26
    invoke-direct {p0}, Lbr/com/itau/textovoz/ui/activity/SearchVoiceActivity;->closeActivity()V

    return-void
.end method

.method private closeActivity()V
    .registers 3

    .line 197
    invoke-virtual {p0}, Lbr/com/itau/textovoz/ui/activity/SearchVoiceActivity;->finish()V

    .line 198
    sget v0, Lbr/com/itau/textovoz/R$anim;->push_up_in:I

    sget v1, Lbr/com/itau/textovoz/R$anim;->push_up_out:I

    invoke-virtual {p0, v0, v1}, Lbr/com/itau/textovoz/ui/activity/SearchVoiceActivity;->overridePendingTransition(II)V

    .line 199
    return-void
.end method

.method private closeActivityError(I)V
    .registers 5
    .param p1, "error"    # I

    .line 280
    invoke-virtual {p0}, Lbr/com/itau/textovoz/ui/activity/SearchVoiceActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    .line 281
    .local v2, "intent":Landroid/content/Intent;
    const-string v0, "return_voice"

    invoke-virtual {p0, p1}, Lbr/com/itau/textovoz/ui/activity/SearchVoiceActivity;->getErrorText(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 282
    const/4 v0, 0x0

    invoke-virtual {p0, v0, v2}, Lbr/com/itau/textovoz/ui/activity/SearchVoiceActivity;->setResult(ILandroid/content/Intent;)V

    .line 283
    invoke-virtual {p0}, Lbr/com/itau/textovoz/ui/activity/SearchVoiceActivity;->finish()V

    .line 284
    sget v0, Lbr/com/itau/textovoz/R$anim;->push_up_in:I

    sget v1, Lbr/com/itau/textovoz/R$anim;->push_up_out:I

    invoke-virtual {p0, v0, v1}, Lbr/com/itau/textovoz/ui/activity/SearchVoiceActivity;->overridePendingTransition(II)V

    .line 285
    return-void
.end method

.method private error(Ljava/lang/String;)V
    .registers 4
    .param p1, "message"    # Ljava/lang/String;

    .line 266
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/activity/SearchVoiceActivity;->textViewTitle:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 267
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbr/com/itau/textovoz/ui/activity/SearchVoiceActivity;->performingSpeechSetup:Z

    .line 268
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbr/com/itau/textovoz/ui/activity/SearchVoiceActivity;->listing:Z

    .line 270
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/activity/SearchVoiceActivity;->linearHelp:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 271
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/activity/SearchVoiceActivity;->linearSuggestions:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 272
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/activity/SearchVoiceActivity;->linearDescription:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 274
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/activity/SearchVoiceActivity;->linearTransitional:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 276
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/activity/SearchVoiceActivity;->soundIndicator:Lbr/com/itau/textovoz/ui/view/custom/SoundIndicator;

    invoke-virtual {v0}, Lbr/com/itau/textovoz/ui/view/custom/SoundIndicator;->clear()V

    .line 277
    return-void
.end method

.method private initVoice()V
    .registers 4

    .line 217
    iget-boolean v0, p0, Lbr/com/itau/textovoz/ui/activity/SearchVoiceActivity;->listing:Z

    if-nez v0, :cond_2b

    .line 218
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbr/com/itau/textovoz/ui/activity/SearchVoiceActivity;->listing:Z

    .line 220
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/activity/SearchVoiceActivity;->textViewTitle:Landroid/widget/TextView;

    invoke-virtual {p0}, Lbr/com/itau/textovoz/ui/activity/SearchVoiceActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lbr/com/itau/textovoz/R$string;->help_search:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 221
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbr/com/itau/textovoz/ui/activity/SearchVoiceActivity;->performingSpeechSetup:Z

    .line 222
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/activity/SearchVoiceActivity;->textViewVoice:Landroid/widget/TextView;

    sget v1, Lbr/com/itau/textovoz/R$string;->description_voice_button_image:I

    invoke-virtual {p0, v1}, Lbr/com/itau/textovoz/ui/activity/SearchVoiceActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbr/com/itau/textovoz/util/AccessibilityUtils;->setRequestFocus(Landroid/view/View;Ljava/lang/String;)V

    .line 224
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/activity/SearchVoiceActivity;->speechRecognizer:Landroid/speech/SpeechRecognizer;

    iget-object v1, p0, Lbr/com/itau/textovoz/ui/activity/SearchVoiceActivity;->speechRecognizerIntent:Landroid/content/Intent;

    invoke-virtual {v0, v1}, Landroid/speech/SpeechRecognizer;->startListening(Landroid/content/Intent;)V

    .line 226
    :cond_2b
    return-void
.end method

.method private setColors(Ljava/lang/String;)V
    .registers 11
    .param p1, "segment"    # Ljava/lang/String;

    .line 83
    sget v0, Lbr/com/itau/textovoz/R$id;->voice_close:I

    invoke-virtual {p0, v0}, Lbr/com/itau/textovoz/ui/activity/SearchVoiceActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Landroid/widget/TextView;

    .line 84
    .local v3, "textViewClose":Landroid/widget/TextView;
    sget v0, Lbr/com/itau/textovoz/R$id;->help_search_voice:I

    invoke-virtual {p0, v0}, Lbr/com/itau/textovoz/ui/activity/SearchVoiceActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lbr/com/itau/textovoz/ui/activity/SearchVoiceActivity;->textViewTitle:Landroid/widget/TextView;

    .line 85
    sget v0, Lbr/com/itau/textovoz/R$id;->help_voice_suggestions:I

    invoke-virtual {p0, v0}, Lbr/com/itau/textovoz/ui/activity/SearchVoiceActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Landroid/widget/TextView;

    .line 86
    .local v4, "textViewSuggestions":Landroid/widget/TextView;
    sget v0, Lbr/com/itau/textovoz/R$id;->help_voice_balance:I

    invoke-virtual {p0, v0}, Lbr/com/itau/textovoz/ui/activity/SearchVoiceActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Landroid/widget/TextView;

    .line 87
    .local v5, "textViewBalance":Landroid/widget/TextView;
    sget v0, Lbr/com/itau/textovoz/R$id;->help_voice_transfer:I

    invoke-virtual {p0, v0}, Lbr/com/itau/textovoz/ui/activity/SearchVoiceActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Landroid/widget/TextView;

    .line 88
    .local v6, "textViewTransfer":Landroid/widget/TextView;
    sget v0, Lbr/com/itau/textovoz/R$id;->help_voice_limit:I

    invoke-virtual {p0, v0}, Lbr/com/itau/textovoz/ui/activity/SearchVoiceActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Landroid/widget/TextView;

    .line 89
    .local v7, "textViewLimit":Landroid/widget/TextView;
    sget v0, Lbr/com/itau/textovoz/R$id;->background_gradient_search:I

    invoke-virtual {p0, v0}, Lbr/com/itau/textovoz/ui/activity/SearchVoiceActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Landroid/support/v4/widget/NestedScrollView;

    .line 91
    .local v8, "relativeLayout":Landroid/support/v4/widget/NestedScrollView;
    sget v0, Lbr/com/itau/textovoz/R$id;->linear_help:I

    invoke-virtual {p0, v0}, Lbr/com/itau/textovoz/ui/activity/SearchVoiceActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lbr/com/itau/textovoz/ui/activity/SearchVoiceActivity;->linearHelp:Landroid/widget/LinearLayout;

    .line 92
    sget v0, Lbr/com/itau/textovoz/R$id;->linear_suggestions:I

    invoke-virtual {p0, v0}, Lbr/com/itau/textovoz/ui/activity/SearchVoiceActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lbr/com/itau/textovoz/ui/activity/SearchVoiceActivity;->linearSuggestions:Landroid/widget/LinearLayout;

    .line 93
    sget v0, Lbr/com/itau/textovoz/R$id;->linear_description:I

    invoke-virtual {p0, v0}, Lbr/com/itau/textovoz/ui/activity/SearchVoiceActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lbr/com/itau/textovoz/ui/activity/SearchVoiceActivity;->linearDescription:Landroid/widget/LinearLayout;

    .line 94
    sget v0, Lbr/com/itau/textovoz/R$id;->linear_transitional:I

    invoke-virtual {p0, v0}, Lbr/com/itau/textovoz/ui/activity/SearchVoiceActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lbr/com/itau/textovoz/ui/activity/SearchVoiceActivity;->linearTransitional:Landroid/widget/LinearLayout;

    .line 95
    sget v0, Lbr/com/itau/textovoz/R$id;->custom_text_transitional:I

    invoke-virtual {p0, v0}, Lbr/com/itau/textovoz/ui/activity/SearchVoiceActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lbr/com/itau/textovoz/ui/view/custom/CustomTextView;

    iput-object v0, p0, Lbr/com/itau/textovoz/ui/activity/SearchVoiceActivity;->customTransitional:Lbr/com/itau/textovoz/ui/view/custom/CustomTextView;

    .line 96
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/activity/SearchVoiceActivity;->customTransitional:Lbr/com/itau/textovoz/ui/view/custom/CustomTextView;

    new-instance v1, Lbr/com/itau/textovoz/listener/LinkMovementMethodOverride;

    invoke-direct {v1}, Lbr/com/itau/textovoz/listener/LinkMovementMethodOverride;-><init>()V

    invoke-virtual {v0, v1}, Lbr/com/itau/textovoz/ui/view/custom/CustomTextView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 98
    invoke-static {p1}, Lbr/com/itau/textovoz/util/CommonUtils;->getSegment(Ljava/lang/String;)I

    move-result v0

    const/4 v1, 0x4

    if-ne v1, v0, :cond_117

    .line 100
    if-eqz v8, :cond_a6

    .line 101
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-ge v0, v1, :cond_99

    .line 102
    invoke-virtual {p0}, Lbr/com/itau/textovoz/ui/activity/SearchVoiceActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lbr/com/itau/textovoz/R$drawable;->background_gradient_search_person:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v8, v0}, Landroid/support/v4/widget/NestedScrollView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_a6

    .line 105
    :cond_99
    invoke-virtual {p0}, Lbr/com/itau/textovoz/ui/activity/SearchVoiceActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lbr/com/itau/textovoz/R$drawable;->background_gradient_search_person:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v8, v0}, Landroid/support/v4/widget/NestedScrollView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 110
    :cond_a6
    :goto_a6
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/activity/SearchVoiceActivity;->textViewTitle:Landroid/widget/TextView;

    if-eqz v0, :cond_b9

    .line 111
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/activity/SearchVoiceActivity;->textViewTitle:Landroid/widget/TextView;

    invoke-virtual {p0}, Lbr/com/itau/textovoz/ui/activity/SearchVoiceActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lbr/com/itau/textovoz/R$color;->help_search_personnalite_title_color:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 115
    :cond_b9
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/activity/SearchVoiceActivity;->textViewVoice:Landroid/widget/TextView;

    if-eqz v0, :cond_cc

    .line 116
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/activity/SearchVoiceActivity;->textViewVoice:Landroid/widget/TextView;

    invoke-virtual {p0}, Lbr/com/itau/textovoz/ui/activity/SearchVoiceActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lbr/com/itau/textovoz/R$color;->personnalite_theme_color:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 120
    :cond_cc
    if-eqz v3, :cond_db

    .line 121
    invoke-virtual {p0}, Lbr/com/itau/textovoz/ui/activity/SearchVoiceActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lbr/com/itau/textovoz/R$color;->help_search_personnalite_text_color:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 125
    :cond_db
    if-eqz v4, :cond_ea

    .line 126
    invoke-virtual {p0}, Lbr/com/itau/textovoz/ui/activity/SearchVoiceActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lbr/com/itau/textovoz/R$color;->help_search_personnalite_text_color:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 130
    :cond_ea
    if-eqz v5, :cond_f9

    .line 131
    invoke-virtual {p0}, Lbr/com/itau/textovoz/ui/activity/SearchVoiceActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lbr/com/itau/textovoz/R$color;->help_search_personnalite_text_color:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v5, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 135
    :cond_f9
    if-eqz v6, :cond_108

    .line 136
    invoke-virtual {p0}, Lbr/com/itau/textovoz/ui/activity/SearchVoiceActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lbr/com/itau/textovoz/R$color;->help_search_personnalite_text_color:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v6, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 140
    :cond_108
    if-eqz v7, :cond_117

    .line 141
    invoke-virtual {p0}, Lbr/com/itau/textovoz/ui/activity/SearchVoiceActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lbr/com/itau/textovoz/R$color;->help_search_personnalite_text_color:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v7, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 145
    :cond_117
    return-void
.end method

.method private startSR()V
    .registers 4

    .line 202
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/activity/SearchVoiceActivity;->speechRecognizer:Landroid/speech/SpeechRecognizer;

    if-nez v0, :cond_f

    .line 203
    invoke-static {p0}, Landroid/speech/SpeechRecognizer;->createSpeechRecognizer(Landroid/content/Context;)Landroid/speech/SpeechRecognizer;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/textovoz/ui/activity/SearchVoiceActivity;->speechRecognizer:Landroid/speech/SpeechRecognizer;

    .line 204
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/activity/SearchVoiceActivity;->speechRecognizer:Landroid/speech/SpeechRecognizer;

    invoke-virtual {v0, p0}, Landroid/speech/SpeechRecognizer;->setRecognitionListener(Landroid/speech/RecognitionListener;)V

    .line 207
    :cond_f
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.speech.action.RECOGNIZE_SPEECH"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lbr/com/itau/textovoz/ui/activity/SearchVoiceActivity;->speechRecognizerIntent:Landroid/content/Intent;

    .line 208
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/activity/SearchVoiceActivity;->speechRecognizerIntent:Landroid/content/Intent;

    const-string v1, "android.speech.extra.LANGUAGE"

    const-string v2, "pt-BR"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 209
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/activity/SearchVoiceActivity;->speechRecognizerIntent:Landroid/content/Intent;

    const-string v1, "calling_package"

    .line 210
    invoke-virtual {p0}, Lbr/com/itau/textovoz/ui/activity/SearchVoiceActivity;->getPackageName()Ljava/lang/String;

    move-result-object v2

    .line 209
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 211
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/activity/SearchVoiceActivity;->speechRecognizerIntent:Landroid/content/Intent;

    const-string v1, "android.speech.extra.LANGUAGE_MODEL"

    const-string v2, "free_form"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 213
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/activity/SearchVoiceActivity;->speechRecognizerIntent:Landroid/content/Intent;

    const-string v1, "android.speech.extra.PARTIAL_RESULTS"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 214
    return-void
.end method

.method private stopRecognizer()V
    .registers 2

    .line 185
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/activity/SearchVoiceActivity;->speechRecognizer:Landroid/speech/SpeechRecognizer;

    if-eqz v0, :cond_9

    .line 186
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/activity/SearchVoiceActivity;->speechRecognizer:Landroid/speech/SpeechRecognizer;

    invoke-virtual {v0}, Landroid/speech/SpeechRecognizer;->destroy()V

    .line 188
    :cond_9
    return-void
.end method


# virtual methods
.method clickVoice()V
    .registers 1

    .line 175
    invoke-direct {p0}, Lbr/com/itau/textovoz/ui/activity/SearchVoiceActivity;->initVoice()V

    .line 176
    return-void
.end method

.method public getErrorText(I)Ljava/lang/String;
    .registers 4
    .param p1, "errorCode"    # I

    .line 230
    packed-switch p1, :pswitch_data_48

    goto/16 :goto_44

    .line 232
    :pswitch_5
    sget v0, Lbr/com/itau/textovoz/R$string;->error_audio:I

    invoke-virtual {p0, v0}, Lbr/com/itau/textovoz/ui/activity/SearchVoiceActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 233
    .local v1, "message":Ljava/lang/String;
    goto :goto_46

    .line 235
    .end local v1    # "message":Ljava/lang/String;
    :pswitch_c
    sget v0, Lbr/com/itau/textovoz/R$string;->error_client:I

    invoke-virtual {p0, v0}, Lbr/com/itau/textovoz/ui/activity/SearchVoiceActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 236
    .local v1, "message":Ljava/lang/String;
    goto :goto_46

    .line 238
    .end local v1    # "message":Ljava/lang/String;
    :pswitch_13
    sget v0, Lbr/com/itau/textovoz/R$string;->error_insufficient_permissions:I

    invoke-virtual {p0, v0}, Lbr/com/itau/textovoz/ui/activity/SearchVoiceActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 239
    .local v1, "message":Ljava/lang/String;
    goto :goto_46

    .line 241
    .end local v1    # "message":Ljava/lang/String;
    :pswitch_1a
    sget v0, Lbr/com/itau/textovoz/R$string;->error_network:I

    invoke-virtual {p0, v0}, Lbr/com/itau/textovoz/ui/activity/SearchVoiceActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 242
    .local v1, "message":Ljava/lang/String;
    goto :goto_46

    .line 244
    .end local v1    # "message":Ljava/lang/String;
    :pswitch_21
    sget v0, Lbr/com/itau/textovoz/R$string;->error_network_timeout:I

    invoke-virtual {p0, v0}, Lbr/com/itau/textovoz/ui/activity/SearchVoiceActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 245
    .local v1, "message":Ljava/lang/String;
    goto :goto_46

    .line 247
    .end local v1    # "message":Ljava/lang/String;
    :pswitch_28
    sget v0, Lbr/com/itau/textovoz/R$string;->error_no_match:I

    invoke-virtual {p0, v0}, Lbr/com/itau/textovoz/ui/activity/SearchVoiceActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 248
    .local v1, "message":Ljava/lang/String;
    goto :goto_46

    .line 250
    .end local v1    # "message":Ljava/lang/String;
    :pswitch_2f
    sget v0, Lbr/com/itau/textovoz/R$string;->error_recognizer_busy:I

    invoke-virtual {p0, v0}, Lbr/com/itau/textovoz/ui/activity/SearchVoiceActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 251
    .local v1, "message":Ljava/lang/String;
    goto :goto_46

    .line 253
    .end local v1    # "message":Ljava/lang/String;
    :pswitch_36
    sget v0, Lbr/com/itau/textovoz/R$string;->error_server:I

    invoke-virtual {p0, v0}, Lbr/com/itau/textovoz/ui/activity/SearchVoiceActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 254
    .local v1, "message":Ljava/lang/String;
    goto :goto_46

    .line 256
    .end local v1    # "message":Ljava/lang/String;
    :pswitch_3d
    sget v0, Lbr/com/itau/textovoz/R$string;->error_speech_timeout:I

    invoke-virtual {p0, v0}, Lbr/com/itau/textovoz/ui/activity/SearchVoiceActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 257
    .local v1, "message":Ljava/lang/String;
    goto :goto_46

    .line 259
    .end local v1    # "message":Ljava/lang/String;
    :goto_44
    const-string v1, "Didn\'t understand, please try again."

    .line 262
    .local v1, "message":Ljava/lang/String;
    :goto_46
    return-object v1

    nop

    :pswitch_data_48
    .packed-switch 0x1
        :pswitch_21
        :pswitch_1a
        :pswitch_5
        :pswitch_36
        :pswitch_c
        :pswitch_3d
        :pswitch_28
        :pswitch_2f
        :pswitch_13
    .end packed-switch
.end method

.method public onBackPressed()V
    .registers 3

    .line 192
    invoke-super {p0}, Lbr/com/itau/textovoz/ui/activity/BaseActivity;->onBackPressed()V

    .line 193
    sget v0, Lbr/com/itau/textovoz/R$anim;->push_up_in:I

    sget v1, Lbr/com/itau/textovoz/R$anim;->push_up_out:I

    invoke-virtual {p0, v0, v1}, Lbr/com/itau/textovoz/ui/activity/SearchVoiceActivity;->overridePendingTransition(II)V

    .line 194
    return-void
.end method

.method public onBeginningOfSpeech()V
    .registers 1

    .line 294
    return-void
.end method

.method public onBufferReceived([B)V
    .registers 2
    .param p1, "bytes"    # [B

    .line 303
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .registers 7
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 58
    invoke-super {p0, p1}, Lbr/com/itau/textovoz/ui/activity/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 59
    sget v0, Lbr/com/itau/textovoz/R$layout;->activity_search_voice:I

    invoke-virtual {p0, v0}, Lbr/com/itau/textovoz/ui/activity/SearchVoiceActivity;->setContentView(I)V

    .line 61
    sget v0, Lbr/com/itau/textovoz/R$id;->voice_close:I

    invoke-virtual {p0, v0}, Lbr/com/itau/textovoz/ui/activity/SearchVoiceActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Landroid/widget/TextView;

    .line 62
    .local v2, "imageViewClose":Landroid/widget/TextView;
    sget v0, Lbr/com/itau/textovoz/R$id;->image_view_search_voice:I

    invoke-virtual {p0, v0}, Lbr/com/itau/textovoz/ui/activity/SearchVoiceActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lbr/com/itau/textovoz/ui/activity/SearchVoiceActivity;->textViewVoice:Landroid/widget/TextView;

    .line 63
    sget v0, Lbr/com/itau/textovoz/R$id;->sound_indicator:I

    invoke-virtual {p0, v0}, Lbr/com/itau/textovoz/ui/activity/SearchVoiceActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lbr/com/itau/textovoz/ui/view/custom/SoundIndicator;

    iput-object v0, p0, Lbr/com/itau/textovoz/ui/activity/SearchVoiceActivity;->soundIndicator:Lbr/com/itau/textovoz/ui/view/custom/SoundIndicator;

    .line 65
    invoke-virtual {p0}, Lbr/com/itau/textovoz/ui/activity/SearchVoiceActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    .line 66
    .local v3, "extras":Landroid/os/Bundle;
    if-eqz v3, :cond_38

    .line 67
    const-string v0, "type"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 68
    .local v4, "segment":Ljava/lang/String;
    invoke-direct {p0, v4}, Lbr/com/itau/textovoz/ui/activity/SearchVoiceActivity;->setColors(Ljava/lang/String;)V

    .line 71
    .end local v4    # "segment":Ljava/lang/String;
    :cond_38
    if-eqz v2, :cond_3f

    .line 72
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/activity/SearchVoiceActivity;->onClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 74
    :cond_3f
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/activity/SearchVoiceActivity;->textViewVoice:Landroid/widget/TextView;

    if-eqz v0, :cond_4a

    .line 75
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/activity/SearchVoiceActivity;->textViewVoice:Landroid/widget/TextView;

    iget-object v1, p0, Lbr/com/itau/textovoz/ui/activity/SearchVoiceActivity;->onClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 78
    :cond_4a
    invoke-direct {p0}, Lbr/com/itau/textovoz/ui/activity/SearchVoiceActivity;->startSR()V

    .line 79
    invoke-direct {p0}, Lbr/com/itau/textovoz/ui/activity/SearchVoiceActivity;->initVoice()V

    .line 80
    return-void
.end method

.method protected onDestroy()V
    .registers 1

    .line 180
    invoke-direct {p0}, Lbr/com/itau/textovoz/ui/activity/SearchVoiceActivity;->stopRecognizer()V

    .line 181
    invoke-super {p0}, Lbr/com/itau/textovoz/ui/activity/BaseActivity;->onDestroy()V

    .line 182
    return-void
.end method

.method public onEndOfSpeech()V
    .registers 1

    .line 307
    return-void
.end method

.method public onError(I)V
    .registers 4
    .param p1, "error"    # I

    .line 311
    iget-boolean v0, p0, Lbr/com/itau/textovoz/ui/activity/SearchVoiceActivity;->performingSpeechSetup:Z

    if-eqz v0, :cond_8

    const/4 v0, 0x7

    if-ne p1, v0, :cond_8

    .line 312
    return-void

    .line 315
    :cond_8
    sget-object v0, Lbr/com/itau/textovoz/ui/activity/SearchVoiceActivity;->TAG:Ljava/lang/String;

    invoke-virtual {p0, p1}, Lbr/com/itau/textovoz/ui/activity/SearchVoiceActivity;->getErrorText(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 317
    const/4 v0, 0x6

    if-ne p1, v0, :cond_22

    .line 318
    invoke-virtual {p0}, Lbr/com/itau/textovoz/ui/activity/SearchVoiceActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lbr/com/itau/textovoz/R$string;->voice_search_try:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lbr/com/itau/textovoz/ui/activity/SearchVoiceActivity;->error(Ljava/lang/String;)V

    goto :goto_36

    .line 319
    :cond_22
    const/4 v0, 0x7

    if-ne p1, v0, :cond_33

    .line 320
    invoke-virtual {p0}, Lbr/com/itau/textovoz/ui/activity/SearchVoiceActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lbr/com/itau/textovoz/R$string;->voice_search_voice:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lbr/com/itau/textovoz/ui/activity/SearchVoiceActivity;->error(Ljava/lang/String;)V

    goto :goto_36

    .line 322
    :cond_33
    invoke-direct {p0, p1}, Lbr/com/itau/textovoz/ui/activity/SearchVoiceActivity;->closeActivityError(I)V

    .line 324
    :goto_36
    return-void
.end method

.method public onEvent(ILandroid/os/Bundle;)V
    .registers 3
    .param p1, "i"    # I
    .param p2, "bundle"    # Landroid/os/Bundle;

    .line 379
    return-void
.end method

.method public onPartialResults(Landroid/os/Bundle;)V
    .registers 7
    .param p1, "bundle"    # Landroid/os/Bundle;

    .line 347
    const-string v0, "results_recognition"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    .line 349
    .local v2, "matches":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-eqz v2, :cond_7f

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_7f

    .line 351
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/activity/SearchVoiceActivity;->linearHelp:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-eq v0, v1, :cond_47

    iget-object v0, p0, Lbr/com/itau/textovoz/ui/activity/SearchVoiceActivity;->linearSuggestions:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-eq v0, v1, :cond_47

    iget-object v0, p0, Lbr/com/itau/textovoz/ui/activity/SearchVoiceActivity;->linearDescription:Landroid/widget/LinearLayout;

    .line 353
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-eq v0, v1, :cond_47

    .line 355
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/activity/SearchVoiceActivity;->linearHelp:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 356
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/activity/SearchVoiceActivity;->linearSuggestions:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 357
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/activity/SearchVoiceActivity;->linearDescription:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 359
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/activity/SearchVoiceActivity;->linearTransitional:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 362
    :cond_47
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 364
    .local v3, "stringBuilder":Ljava/lang/StringBuilder;
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_4d
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v4, v0, :cond_65

    .line 365
    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 364
    add-int/lit8 v4, v4, 0x1

    goto :goto_4d

    .line 368
    .end local v4    # "i":I
    :cond_65
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    const/16 v1, 0x3c

    if-le v0, v1, :cond_76

    .line 369
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/activity/SearchVoiceActivity;->speechRecognizer:Landroid/speech/SpeechRecognizer;

    if-eqz v0, :cond_76

    .line 370
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/activity/SearchVoiceActivity;->speechRecognizer:Landroid/speech/SpeechRecognizer;

    invoke-virtual {v0}, Landroid/speech/SpeechRecognizer;->stopListening()V

    .line 373
    :cond_76
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/activity/SearchVoiceActivity;->customTransitional:Lbr/com/itau/textovoz/ui/view/custom/CustomTextView;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbr/com/itau/textovoz/ui/view/custom/CustomTextView;->setText(Ljava/lang/CharSequence;)V

    .line 375
    .end local v3    # "stringBuilder":Ljava/lang/StringBuilder;
    :cond_7f
    return-void
.end method

.method public onReadyForSpeech(Landroid/os/Bundle;)V
    .registers 3
    .param p1, "bundle"    # Landroid/os/Bundle;

    .line 289
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbr/com/itau/textovoz/ui/activity/SearchVoiceActivity;->performingSpeechSetup:Z

    .line 290
    return-void
.end method

.method public onResults(Landroid/os/Bundle;)V
    .registers 7
    .param p1, "bundle"    # Landroid/os/Bundle;

    .line 328
    const-string v0, "results_recognition"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    .line 331
    .local v2, "matches":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v0, 0x0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Ljava/lang/String;

    .line 333
    .local v3, "text":Ljava/lang/String;
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v1, 0x3c

    if-le v0, v1, :cond_20

    .line 334
    sget v0, Lbr/com/itau/textovoz/R$string;->voice_search_error_max_chars:I

    invoke-virtual {p0, v0}, Lbr/com/itau/textovoz/ui/activity/SearchVoiceActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lbr/com/itau/textovoz/ui/activity/SearchVoiceActivity;->error(Ljava/lang/String;)V

    goto :goto_3e

    .line 336
    :cond_20
    invoke-virtual {p0}, Lbr/com/itau/textovoz/ui/activity/SearchVoiceActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    .line 338
    .local v4, "intent":Landroid/content/Intent;
    sget v0, Lbr/com/itau/textovoz/R$anim;->push_up_in:I

    sget v1, Lbr/com/itau/textovoz/R$anim;->push_up_out:I

    invoke-virtual {p0, v0, v1}, Lbr/com/itau/textovoz/ui/activity/SearchVoiceActivity;->overridePendingTransition(II)V

    .line 339
    const-string v0, "return_voice"

    const/4 v1, 0x0

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v4, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 340
    const/4 v0, -0x1

    invoke-virtual {p0, v0, v4}, Lbr/com/itau/textovoz/ui/activity/SearchVoiceActivity;->setResult(ILandroid/content/Intent;)V

    .line 341
    invoke-virtual {p0}, Lbr/com/itau/textovoz/ui/activity/SearchVoiceActivity;->finish()V

    .line 343
    .end local v4    # "intent":Landroid/content/Intent;
    :goto_3e
    return-void
.end method

.method public onRmsChanged(F)V
    .registers 3
    .param p1, "rmsdB"    # F

    .line 298
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/activity/SearchVoiceActivity;->soundIndicator:Lbr/com/itau/textovoz/ui/view/custom/SoundIndicator;

    invoke-virtual {v0, p1}, Lbr/com/itau/textovoz/ui/view/custom/SoundIndicator;->setSoundLevel(F)V

    .line 299
    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .registers 5
    .param p1, "hasFocus"    # Z
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .line 151
    invoke-super {p0, p1}, Lbr/com/itau/textovoz/ui/activity/BaseActivity;->onWindowFocusChanged(Z)V

    .line 153
    if-eqz p1, :cond_2c

    .line 155
    invoke-virtual {p0}, Lbr/com/itau/textovoz/ui/activity/SearchVoiceActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getSystemUiVisibility()I

    move-result v2

    .line 157
    .local v2, "uiOptions":I
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_19

    .line 159
    xor-int/lit16 v2, v2, 0x706

    .line 166
    :cond_19
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-lt v0, v1, :cond_21

    .line 167
    xor-int/lit16 v2, v2, 0x1000

    .line 170
    :cond_21
    invoke-virtual {p0}, Lbr/com/itau/textovoz/ui/activity/SearchVoiceActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setSystemUiVisibility(I)V

    .line 172
    .end local v2    # "uiOptions":I
    :cond_2c
    return-void
.end method

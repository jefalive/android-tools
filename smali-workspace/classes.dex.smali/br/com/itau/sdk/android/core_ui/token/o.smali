.class final synthetic Lbr/com/itau/sdk/android/core_ui/token/o;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private final a:Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;

.field private final b:Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;

.field private final c:Lbr/com/itau/sdk/android/core_ui/token/q;


# direct methods
.method private constructor <init>(Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;Lbr/com/itau/sdk/android/core_ui/token/q;)V
    .registers 4

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lbr/com/itau/sdk/android/core_ui/token/o;->a:Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;

    iput-object p2, p0, Lbr/com/itau/sdk/android/core_ui/token/o;->b:Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;

    iput-object p3, p0, Lbr/com/itau/sdk/android/core_ui/token/o;->c:Lbr/com/itau/sdk/android/core_ui/token/q;

    return-void
.end method

.method public static a(Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;Lbr/com/itau/sdk/android/core_ui/token/q;)Landroid/view/View$OnClickListener;
    .registers 4

    new-instance v0, Lbr/com/itau/sdk/android/core_ui/token/o;

    invoke-direct {v0, p0, p1, p2}, Lbr/com/itau/sdk/android/core_ui/token/o;-><init>(Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;Lbr/com/itau/sdk/android/core_ui/token/q;)V

    return-object v0
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .registers 5
    .annotation runtime Ljava/lang/invoke/LambdaForm$Hidden;
    .end annotation

    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/o;->a:Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;

    iget-object v1, p0, Lbr/com/itau/sdk/android/core_ui/token/o;->b:Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;

    iget-object v2, p0, Lbr/com/itau/sdk/android/core_ui/token/o;->c:Lbr/com/itau/sdk/android/core_ui/token/q;

    invoke-static {v0, v1, v2, p1}, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->b(Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;Lbr/com/itau/sdk/android/core_ui/token/q;Landroid/view/View;)V

    return-void
.end method

.class public Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosComprovanteContratacaoActivity_$IntentBuilder_;
.super Lorg/androidannotations/api/builder/ActivityIntentBuilder;
.source "EmprestimosParceladosComprovanteContratacaoActivity_.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosComprovanteContratacaoActivity_;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "IntentBuilder_"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/androidannotations/api/builder/ActivityIntentBuilder<Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosComprovanteContratacaoActivity_$IntentBuilder_;>;"
    }
.end annotation


# instance fields
.field private fragmentSupport_:Landroid/support/v4/app/Fragment;

.field private fragment_:Landroid/app/Fragment;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .param p1, "context"    # Landroid/content/Context;

    .line 157
    const-class v0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosComprovanteContratacaoActivity_;

    invoke-direct {p0, p1, v0}, Lorg/androidannotations/api/builder/ActivityIntentBuilder;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 158
    return-void
.end method


# virtual methods
.method public devedorSolidario(Z)Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosComprovanteContratacaoActivity_$IntentBuilder_;
    .registers 3
    .param p1, "devedorSolidario"    # Z

    .line 224
    const-string v0, "devedorSolidario"

    invoke-super {p0, v0, p1}, Lorg/androidannotations/api/builder/ActivityIntentBuilder;->extra(Ljava/lang/String;Z)Lorg/androidannotations/api/builder/IntentBuilder;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosComprovanteContratacaoActivity_$IntentBuilder_;

    return-object v0
.end method

.method public efetivacaoVO(Lcom/itau/empresas/feature/credito/giro/model/ResultadoEfetivacaoEmprestimosParceladosVO;)Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosComprovanteContratacaoActivity_$IntentBuilder_;
    .registers 3
    .param p1, "efetivacaoVO"    # Lcom/itau/empresas/feature/credito/giro/model/ResultadoEfetivacaoEmprestimosParceladosVO;

    .line 234
    const-string v0, "efetivacaoVO"

    invoke-super {p0, v0, p1}, Lorg/androidannotations/api/builder/ActivityIntentBuilder;->extra(Ljava/lang/String;Ljava/io/Serializable;)Lorg/androidannotations/api/builder/IntentBuilder;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosComprovanteContratacaoActivity_$IntentBuilder_;

    return-object v0
.end method

.method public simulacaoVO(Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoSaidaVO;)Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosComprovanteContratacaoActivity_$IntentBuilder_;
    .registers 3
    .param p1, "simulacaoVO"    # Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoSaidaVO;

    .line 204
    const-string v0, "simulacaoVO"

    invoke-super {p0, v0, p1}, Lorg/androidannotations/api/builder/ActivityIntentBuilder;->extra(Ljava/lang/String;Ljava/io/Serializable;)Lorg/androidannotations/api/builder/IntentBuilder;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosComprovanteContratacaoActivity_$IntentBuilder_;

    return-object v0
.end method

.method public startForResult(I)Lorg/androidannotations/api/builder/PostActivityStarter;
    .registers 6
    .param p1, "requestCode"    # I

    .line 172
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosComprovanteContratacaoActivity_$IntentBuilder_;->fragmentSupport_:Landroid/support/v4/app/Fragment;

    if-eqz v0, :cond_c

    .line 173
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosComprovanteContratacaoActivity_$IntentBuilder_;->fragmentSupport_:Landroid/support/v4/app/Fragment;

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosComprovanteContratacaoActivity_$IntentBuilder_;->intent:Landroid/content/Intent;

    invoke-virtual {v0, v1, p1}, Landroid/support/v4/app/Fragment;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_52

    .line 175
    :cond_c
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosComprovanteContratacaoActivity_$IntentBuilder_;->fragment_:Landroid/app/Fragment;

    if-eqz v0, :cond_28

    .line 176
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_20

    .line 177
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosComprovanteContratacaoActivity_$IntentBuilder_;->fragment_:Landroid/app/Fragment;

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosComprovanteContratacaoActivity_$IntentBuilder_;->intent:Landroid/content/Intent;

    iget-object v2, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosComprovanteContratacaoActivity_$IntentBuilder_;->lastOptions:Landroid/os/Bundle;

    invoke-virtual {v0, v1, p1, v2}, Landroid/app/Fragment;->startActivityForResult(Landroid/content/Intent;ILandroid/os/Bundle;)V

    goto :goto_52

    .line 179
    :cond_20
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosComprovanteContratacaoActivity_$IntentBuilder_;->fragment_:Landroid/app/Fragment;

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosComprovanteContratacaoActivity_$IntentBuilder_;->intent:Landroid/content/Intent;

    invoke-virtual {v0, v1, p1}, Landroid/app/Fragment;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_52

    .line 182
    :cond_28
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosComprovanteContratacaoActivity_$IntentBuilder_;->context:Landroid/content/Context;

    instance-of v0, v0, Landroid/app/Activity;

    if-eqz v0, :cond_3b

    .line 183
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosComprovanteContratacaoActivity_$IntentBuilder_;->context:Landroid/content/Context;

    move-object v3, v0

    check-cast v3, Landroid/app/Activity;

    .line 184
    .local v3, "activity":Landroid/app/Activity;
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosComprovanteContratacaoActivity_$IntentBuilder_;->intent:Landroid/content/Intent;

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosComprovanteContratacaoActivity_$IntentBuilder_;->lastOptions:Landroid/os/Bundle;

    invoke-static {v3, v0, p1, v1}, Landroid/support/v4/app/ActivityCompat;->startActivityForResult(Landroid/app/Activity;Landroid/content/Intent;ILandroid/os/Bundle;)V

    .line 185
    .end local v3    # "activity":Landroid/app/Activity;
    goto :goto_52

    .line 186
    :cond_3b
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_4b

    .line 187
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosComprovanteContratacaoActivity_$IntentBuilder_;->context:Landroid/content/Context;

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosComprovanteContratacaoActivity_$IntentBuilder_;->intent:Landroid/content/Intent;

    iget-object v2, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosComprovanteContratacaoActivity_$IntentBuilder_;->lastOptions:Landroid/os/Bundle;

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;Landroid/os/Bundle;)V

    goto :goto_52

    .line 189
    :cond_4b
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosComprovanteContratacaoActivity_$IntentBuilder_;->context:Landroid/content/Context;

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosComprovanteContratacaoActivity_$IntentBuilder_;->intent:Landroid/content/Intent;

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 194
    :goto_52
    new-instance v0, Lorg/androidannotations/api/builder/PostActivityStarter;

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosComprovanteContratacaoActivity_$IntentBuilder_;->context:Landroid/content/Context;

    invoke-direct {v0, v1}, Lorg/androidannotations/api/builder/PostActivityStarter;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method public temSeguro(Z)Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosComprovanteContratacaoActivity_$IntentBuilder_;
    .registers 3
    .param p1, "temSeguro"    # Z

    .line 214
    const-string v0, "temSeguro"

    invoke-super {p0, v0, p1}, Lorg/androidannotations/api/builder/ActivityIntentBuilder;->extra(Ljava/lang/String;Z)Lorg/androidannotations/api/builder/IntentBuilder;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosComprovanteContratacaoActivity_$IntentBuilder_;

    return-object v0
.end method

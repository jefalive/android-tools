.class public Lcom/itau/empresas/feature/credito/lis/adapter/LisCondicoesGeraisAdapter;
.super Landroid/support/v7/widget/RecyclerView$Adapter;
.source "LisCondicoesGeraisAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/itau/empresas/feature/credito/lis/adapter/LisCondicoesGeraisAdapter$CondicoesGeraisViewHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/support/v7/widget/RecyclerView$Adapter<Lcom/itau/empresas/feature/credito/lis/adapter/LisCondicoesGeraisAdapter$CondicoesGeraisViewHolder;>;Landroid/view/View$OnClickListener;"
    }
.end annotation


# instance fields
.field private final condicoesGeraisVO:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Lcom/itau/empresas/feature/credito/lis/model/LisCondicoesGeraisVO;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/List;)V
    .registers 2
    .param p1, "condicoesGeraisVO"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<Lcom/itau/empresas/feature/credito/lis/model/LisCondicoesGeraisVO;>;)V"
        }
    .end annotation

    .line 20
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView$Adapter;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/itau/empresas/feature/credito/lis/adapter/LisCondicoesGeraisAdapter;->condicoesGeraisVO:Ljava/util/List;

    .line 22
    return-void
.end method


# virtual methods
.method public getItemCount()I
    .registers 2

    .line 48
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/adapter/LisCondicoesGeraisAdapter;->condicoesGeraisVO:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public bridge synthetic onBindViewHolder(Landroid/support/v7/widget/RecyclerView$ViewHolder;I)V
    .registers 4

    .line 14
    move-object v0, p1

    check-cast v0, Lcom/itau/empresas/feature/credito/lis/adapter/LisCondicoesGeraisAdapter$CondicoesGeraisViewHolder;

    invoke-virtual {p0, v0, p2}, Lcom/itau/empresas/feature/credito/lis/adapter/LisCondicoesGeraisAdapter;->onBindViewHolder(Lcom/itau/empresas/feature/credito/lis/adapter/LisCondicoesGeraisAdapter$CondicoesGeraisViewHolder;I)V

    return-void
.end method

.method public onBindViewHolder(Lcom/itau/empresas/feature/credito/lis/adapter/LisCondicoesGeraisAdapter$CondicoesGeraisViewHolder;I)V
    .registers 6
    .param p1, "holder"    # Lcom/itau/empresas/feature/credito/lis/adapter/LisCondicoesGeraisAdapter$CondicoesGeraisViewHolder;
    .param p2, "position"    # I

    .line 38
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/adapter/LisCondicoesGeraisAdapter;->condicoesGeraisVO:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/itau/empresas/feature/credito/lis/model/LisCondicoesGeraisVO;

    .line 40
    .local v2, "vo":Lcom/itau/empresas/feature/credito/lis/model/LisCondicoesGeraisVO;
    invoke-virtual {p1}, Lcom/itau/empresas/feature/credito/lis/adapter/LisCondicoesGeraisAdapter$CondicoesGeraisViewHolder;->getIcone()Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v2}, Lcom/itau/empresas/feature/credito/lis/model/LisCondicoesGeraisVO;->getIcone()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 41
    invoke-virtual {p1}, Lcom/itau/empresas/feature/credito/lis/adapter/LisCondicoesGeraisAdapter$CondicoesGeraisViewHolder;->getTextoTitulo()Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v2}, Lcom/itau/empresas/feature/credito/lis/model/LisCondicoesGeraisVO;->getTitulo()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 42
    invoke-virtual {p1}, Lcom/itau/empresas/feature/credito/lis/adapter/LisCondicoesGeraisAdapter$CondicoesGeraisViewHolder;->getTextoDescricao()Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v2}, Lcom/itau/empresas/feature/credito/lis/model/LisCondicoesGeraisVO;->getDescricao()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 43
    invoke-virtual {p1}, Lcom/itau/empresas/feature/credito/lis/adapter/LisCondicoesGeraisAdapter$CondicoesGeraisViewHolder;->getTextoDescricao()Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v2}, Lcom/itau/empresas/feature/credito/lis/model/LisCondicoesGeraisVO;->getDescricaoAcessibilidade()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 44
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .registers 5
    .param p1, "v"    # Landroid/view/View;

    .line 54
    const v0, 0x7f0e0550

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 55
    .local v1, "viewById":Landroid/view/View;
    const v0, 0x7f0e054f

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 57
    .local v2, "seta":Landroid/view/View;
    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1e

    .line 58
    const/16 v0, 0x8

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 59
    const/4 v0, 0x0

    invoke-virtual {v2, v0}, Landroid/view/View;->setRotation(F)V

    goto :goto_27

    .line 61
    :cond_1e
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 62
    const/high16 v0, 0x43340000    # 180.0f

    invoke-virtual {v2, v0}, Landroid/view/View;->setRotation(F)V

    .line 64
    :goto_27
    return-void
.end method

.method public bridge synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .registers 4

    .line 14
    invoke-virtual {p0, p1, p2}, Lcom/itau/empresas/feature/credito/lis/adapter/LisCondicoesGeraisAdapter;->onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/itau/empresas/feature/credito/lis/adapter/LisCondicoesGeraisAdapter$CondicoesGeraisViewHolder;

    move-result-object v0

    return-object v0
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/itau/empresas/feature/credito/lis/adapter/LisCondicoesGeraisAdapter$CondicoesGeraisViewHolder;
    .registers 7
    .param p1, "parent"    # Landroid/view/ViewGroup;
    .param p2, "viewType"    # I

    .line 28
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 29
    const v1, 0x7f030105

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    .line 31
    .local v3, "view":Landroid/view/View;
    invoke-virtual {v3, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 33
    new-instance v0, Lcom/itau/empresas/feature/credito/lis/adapter/LisCondicoesGeraisAdapter$CondicoesGeraisViewHolder;

    invoke-direct {v0, p0, v3}, Lcom/itau/empresas/feature/credito/lis/adapter/LisCondicoesGeraisAdapter$CondicoesGeraisViewHolder;-><init>(Lcom/itau/empresas/feature/credito/lis/adapter/LisCondicoesGeraisAdapter;Landroid/view/View;)V

    return-object v0
.end method

.class public final Lbr/com/itau/sdk/android/fingerprintcore/android/AndroidFingerprintManager;
.super Ljava/lang/Object;
.source "AndroidFingerprintManager.java"

# interfaces
.implements Lbr/com/itau/sdk/android/fingerprintcore/FingerprintManager;


# instance fields
.field private final manager:Landroid/support/v4/hardware/fingerprint/FingerprintManagerCompat;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .registers 3
    .param p1, "context"    # Landroid/content/Context;

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    invoke-static {p1}, Landroid/support/v4/hardware/fingerprint/FingerprintManagerCompat;->from(Landroid/content/Context;)Landroid/support/v4/hardware/fingerprint/FingerprintManagerCompat;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/sdk/android/fingerprintcore/android/AndroidFingerprintManager;->manager:Landroid/support/v4/hardware/fingerprint/FingerprintManagerCompat;

    .line 20
    return-void
.end method

.method private createAuthenticationCallback(Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintCallback;)Landroid/support/v4/hardware/fingerprint/FingerprintManagerCompat$AuthenticationCallback;
    .registers 3
    .param p1, "callback"    # Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintCallback;

    .line 63
    new-instance v0, Lbr/com/itau/sdk/android/fingerprintcore/android/AndroidFingerprintManager$1;

    invoke-direct {v0, p0, p1}, Lbr/com/itau/sdk/android/fingerprintcore/android/AndroidFingerprintManager$1;-><init>(Lbr/com/itau/sdk/android/fingerprintcore/android/AndroidFingerprintManager;Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintCallback;)V

    return-object v0
.end method

.method public static getInstance(Landroid/content/Context;)Lbr/com/itau/sdk/android/fingerprintcore/android/AndroidFingerprintManager;
    .registers 3
    .param p0, "context"    # Landroid/content/Context;

    .line 23
    new-instance v0, Lbr/com/itau/sdk/android/fingerprintcore/android/AndroidFingerprintManager;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lbr/com/itau/sdk/android/fingerprintcore/android/AndroidFingerprintManager;-><init>(Landroid/content/Context;)V

    return-object v0
.end method


# virtual methods
.method public authenticate(Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintCallback;Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintCanceller;)Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintCanceller;
    .registers 10
    .param p1, "callback"    # Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintCallback;
    .param p2, "canceller"    # Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintCanceller;

    .line 53
    .line 54
    invoke-direct {p0, p1}, Lbr/com/itau/sdk/android/fingerprintcore/android/AndroidFingerprintManager;->createAuthenticationCallback(Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintCallback;)Landroid/support/v4/hardware/fingerprint/FingerprintManagerCompat$AuthenticationCallback;

    move-result-object v6

    .line 55
    .local v6, "authCallback":Landroid/support/v4/hardware/fingerprint/FingerprintManagerCompat$AuthenticationCallback;
    iget-object v0, p0, Lbr/com/itau/sdk/android/fingerprintcore/android/AndroidFingerprintManager;->manager:Landroid/support/v4/hardware/fingerprint/FingerprintManagerCompat;

    move-object v1, p2

    check-cast v1, Lbr/com/itau/sdk/android/fingerprintcore/android/AndroidFingerprintCanceller;

    .line 56
    invoke-virtual {v1}, Lbr/com/itau/sdk/android/fingerprintcore/android/AndroidFingerprintCanceller;->getCancellationSignal()Landroid/support/v4/os/CancellationSignal;

    move-result-object v3

    move-object v4, v6

    .line 55
    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/support/v4/hardware/fingerprint/FingerprintManagerCompat;->authenticate(Landroid/support/v4/hardware/fingerprint/FingerprintManagerCompat$CryptoObject;ILandroid/support/v4/os/CancellationSignal;Landroid/support/v4/hardware/fingerprint/FingerprintManagerCompat$AuthenticationCallback;Landroid/os/Handler;)V

    .line 58
    return-object p2
.end method

.method public hasEnrolledFingerprints()Z
    .registers 2

    .line 43
    iget-object v0, p0, Lbr/com/itau/sdk/android/fingerprintcore/android/AndroidFingerprintManager;->manager:Landroid/support/v4/hardware/fingerprint/FingerprintManagerCompat;

    invoke-virtual {v0}, Landroid/support/v4/hardware/fingerprint/FingerprintManagerCompat;->hasEnrolledFingerprints()Z

    move-result v0

    return v0
.end method

.method public isAvailable()Z
    .registers 2

    .line 33
    invoke-virtual {p0}, Lbr/com/itau/sdk/android/fingerprintcore/android/AndroidFingerprintManager;->isHardwareDetected()Z

    move-result v0

    if-eqz v0, :cond_e

    invoke-virtual {p0}, Lbr/com/itau/sdk/android/fingerprintcore/android/AndroidFingerprintManager;->hasEnrolledFingerprints()Z

    move-result v0

    if-eqz v0, :cond_e

    const/4 v0, 0x1

    goto :goto_f

    :cond_e
    const/4 v0, 0x0

    :goto_f
    return v0
.end method

.method public isHardwareDetected()Z
    .registers 2

    .line 38
    iget-object v0, p0, Lbr/com/itau/sdk/android/fingerprintcore/android/AndroidFingerprintManager;->manager:Landroid/support/v4/hardware/fingerprint/FingerprintManagerCompat;

    invoke-virtual {v0}, Landroid/support/v4/hardware/fingerprint/FingerprintManagerCompat;->isHardwareDetected()Z

    move-result v0

    return v0
.end method

.method public isUnavailable()Z
    .registers 2

    .line 28
    invoke-virtual {p0}, Lbr/com/itau/sdk/android/fingerprintcore/android/AndroidFingerprintManager;->isAvailable()Z

    move-result v0

    if-nez v0, :cond_8

    const/4 v0, 0x1

    goto :goto_9

    :cond_8
    const/4 v0, 0x0

    :goto_9
    return v0
.end method

.class public final Lcom/itau/empresas/ui/fragment/OutrosFragment_;
.super Lcom/itau/empresas/ui/fragment/OutrosFragment;
.source "OutrosFragment_.java"

# interfaces
.implements Lorg/androidannotations/api/view/HasViews;
.implements Lorg/androidannotations/api/view/OnViewChangedListener;


# instance fields
.field private contentView_:Landroid/view/View;

.field private final onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;


# direct methods
.method public constructor <init>()V
    .registers 2

    .line 24
    invoke-direct {p0}, Lcom/itau/empresas/ui/fragment/OutrosFragment;-><init>()V

    .line 28
    new-instance v0, Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-direct {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/ui/fragment/OutrosFragment_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    return-void
.end method

.method private init_(Landroid/os/Bundle;)V
    .registers 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 77
    invoke-static {p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->registerOnViewChangedListener(Lorg/androidannotations/api/view/OnViewChangedListener;)V

    .line 78
    invoke-direct {p0}, Lcom/itau/empresas/ui/fragment/OutrosFragment_;->injectFragmentArguments_()V

    .line 79
    invoke-static {}, Lcom/itau/empresas/CustomApplication_;->getInstance()Lcom/itau/empresas/CustomApplication;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/ui/fragment/OutrosFragment_;->application:Lcom/itau/empresas/CustomApplication;

    .line 80
    return-void
.end method

.method private injectFragmentArguments_()V
    .registers 3

    .line 195
    invoke-virtual {p0}, Lcom/itau/empresas/ui/fragment/OutrosFragment_;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    .line 196
    .local v1, "args_":Landroid/os/Bundle;
    if-eqz v1, :cond_16

    .line 197
    const-string v0, "startedFrom"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_16

    .line 198
    const-string v0, "startedFrom"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/ui/fragment/OutrosFragment_;->startedFrom:Ljava/lang/String;

    .line 201
    :cond_16
    return-void
.end method


# virtual methods
.method public findViewById(I)Landroid/view/View;
    .registers 3
    .param p1, "id"    # I

    .line 42
    iget-object v0, p0, Lcom/itau/empresas/ui/fragment/OutrosFragment_;->contentView_:Landroid/view/View;

    if-nez v0, :cond_6

    .line 43
    const/4 v0, 0x0

    return-object v0

    .line 45
    :cond_6
    iget-object v0, p0, Lcom/itau/empresas/ui/fragment/OutrosFragment_;->contentView_:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 34
    iget-object v0, p0, Lcom/itau/empresas/ui/fragment/OutrosFragment_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-static {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    move-result-object v1

    .line 35
    .local v1, "previousNotifier":Lorg/androidannotations/api/view/OnViewChangedNotifier;
    invoke-direct {p0, p1}, Lcom/itau/empresas/ui/fragment/OutrosFragment_;->init_(Landroid/os/Bundle;)V

    .line 36
    invoke-super {p0, p1}, Lcom/itau/empresas/ui/fragment/OutrosFragment;->onCreate(Landroid/os/Bundle;)V

    .line 37
    invoke-static {v1}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 38
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .registers 6
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .line 50
    invoke-super {p0, p1, p2, p3}, Lcom/itau/empresas/ui/fragment/OutrosFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/ui/fragment/OutrosFragment_;->contentView_:Landroid/view/View;

    .line 51
    iget-object v0, p0, Lcom/itau/empresas/ui/fragment/OutrosFragment_;->contentView_:Landroid/view/View;

    if-nez v0, :cond_14

    .line 52
    const v0, 0x7f0300b9

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/ui/fragment/OutrosFragment_;->contentView_:Landroid/view/View;

    .line 54
    :cond_14
    iget-object v0, p0, Lcom/itau/empresas/ui/fragment/OutrosFragment_;->contentView_:Landroid/view/View;

    return-object v0
.end method

.method public onDestroyView()V
    .registers 2

    .line 59
    invoke-super {p0}, Lcom/itau/empresas/ui/fragment/OutrosFragment;->onDestroyView()V

    .line 60
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/itau/empresas/ui/fragment/OutrosFragment_;->contentView_:Landroid/view/View;

    .line 61
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/itau/empresas/ui/fragment/OutrosFragment_;->textoAplicativo:Landroid/widget/TextView;

    .line 62
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/itau/empresas/ui/fragment/OutrosFragment_;->textoSeguranca:Landroid/widget/TextView;

    .line 63
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/itau/empresas/ui/fragment/OutrosFragment_;->textoRecomendar:Landroid/widget/TextView;

    .line 64
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/itau/empresas/ui/fragment/OutrosFragment_;->textoTermosDeUso:Landroid/widget/TextView;

    .line 65
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/itau/empresas/ui/fragment/OutrosFragment_;->textoAvaliar:Landroid/widget/TextView;

    .line 66
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/itau/empresas/ui/fragment/OutrosFragment_;->textoVersao:Landroid/widget/TextView;

    .line 67
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/itau/empresas/ui/fragment/OutrosFragment_;->textoDataAtualizacao:Landroid/widget/TextView;

    .line 68
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/itau/empresas/ui/fragment/OutrosFragment_;->rlOutrosContato:Landroid/widget/RelativeLayout;

    .line 69
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/itau/empresas/ui/fragment/OutrosFragment_;->rlOutrosBeta:Landroid/widget/RelativeLayout;

    .line 70
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/itau/empresas/ui/fragment/OutrosFragment_;->rlOutrosAplicativos:Landroid/widget/RelativeLayout;

    .line 71
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/itau/empresas/ui/fragment/OutrosFragment_;->linhaOutrosContato:Landroid/view/View;

    .line 72
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/itau/empresas/ui/fragment/OutrosFragment_;->linhaOutrosBeta:Landroid/view/View;

    .line 73
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/itau/empresas/ui/fragment/OutrosFragment_;->linhaOutrosAplicativos:Landroid/view/View;

    .line 74
    return-void
.end method

.method public onViewChanged(Lorg/androidannotations/api/view/HasViews;)V
    .registers 7
    .param p1, "hasViews"    # Lorg/androidannotations/api/view/HasViews;

    .line 94
    const v0, 0x7f0e0463

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/ui/fragment/OutrosFragment_;->textoAplicativo:Landroid/widget/TextView;

    .line 95
    const v0, 0x7f0e0465

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/ui/fragment/OutrosFragment_;->textoSeguranca:Landroid/widget/TextView;

    .line 96
    const v0, 0x7f0e0457

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/ui/fragment/OutrosFragment_;->textoRecomendar:Landroid/widget/TextView;

    .line 97
    const v0, 0x7f0e018d

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/ui/fragment/OutrosFragment_;->textoTermosDeUso:Landroid/widget/TextView;

    .line 98
    const v0, 0x7f0e0454

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/ui/fragment/OutrosFragment_;->textoAvaliar:Landroid/widget/TextView;

    .line 99
    const v0, 0x7f0e044d

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/ui/fragment/OutrosFragment_;->textoVersao:Landroid/widget/TextView;

    .line 100
    const v0, 0x7f0e044e

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/ui/fragment/OutrosFragment_;->textoDataAtualizacao:Landroid/widget/TextView;

    .line 101
    const v0, 0x7f0e0450

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/itau/empresas/ui/fragment/OutrosFragment_;->rlOutrosContato:Landroid/widget/RelativeLayout;

    .line 102
    const v0, 0x7f0e045d

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/itau/empresas/ui/fragment/OutrosFragment_;->rlOutrosBeta:Landroid/widget/RelativeLayout;

    .line 103
    const v0, 0x7f0e0461

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/itau/empresas/ui/fragment/OutrosFragment_;->rlOutrosAplicativos:Landroid/widget/RelativeLayout;

    .line 104
    const v0, 0x7f0e044f

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/ui/fragment/OutrosFragment_;->linhaOutrosContato:Landroid/view/View;

    .line 105
    const v0, 0x7f0e045c

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/ui/fragment/OutrosFragment_;->linhaOutrosBeta:Landroid/view/View;

    .line 106
    const v0, 0x7f0e0460

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/ui/fragment/OutrosFragment_;->linhaOutrosAplicativos:Landroid/view/View;

    .line 107
    const v0, 0x7f0e0452

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 108
    .local v2, "view_outros_texto":Landroid/view/View;
    const v0, 0x7f0e045f

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 109
    .local v3, "view_texto_outros_aplicativo_beta":Landroid/view/View;
    const v0, 0x7f0e0459

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v4

    .line 111
    .local v4, "view_rl_mapa":Landroid/view/View;
    iget-object v0, p0, Lcom/itau/empresas/ui/fragment/OutrosFragment_;->textoAplicativo:Landroid/widget/TextView;

    if-eqz v0, :cond_ac

    .line 112
    iget-object v0, p0, Lcom/itau/empresas/ui/fragment/OutrosFragment_;->textoAplicativo:Landroid/widget/TextView;

    new-instance v1, Lcom/itau/empresas/ui/fragment/OutrosFragment_$1;

    invoke-direct {v1, p0}, Lcom/itau/empresas/ui/fragment/OutrosFragment_$1;-><init>(Lcom/itau/empresas/ui/fragment/OutrosFragment_;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 121
    :cond_ac
    iget-object v0, p0, Lcom/itau/empresas/ui/fragment/OutrosFragment_;->textoSeguranca:Landroid/widget/TextView;

    if-eqz v0, :cond_ba

    .line 122
    iget-object v0, p0, Lcom/itau/empresas/ui/fragment/OutrosFragment_;->textoSeguranca:Landroid/widget/TextView;

    new-instance v1, Lcom/itau/empresas/ui/fragment/OutrosFragment_$2;

    invoke-direct {v1, p0}, Lcom/itau/empresas/ui/fragment/OutrosFragment_$2;-><init>(Lcom/itau/empresas/ui/fragment/OutrosFragment_;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 131
    :cond_ba
    iget-object v0, p0, Lcom/itau/empresas/ui/fragment/OutrosFragment_;->textoRecomendar:Landroid/widget/TextView;

    if-eqz v0, :cond_c8

    .line 132
    iget-object v0, p0, Lcom/itau/empresas/ui/fragment/OutrosFragment_;->textoRecomendar:Landroid/widget/TextView;

    new-instance v1, Lcom/itau/empresas/ui/fragment/OutrosFragment_$3;

    invoke-direct {v1, p0}, Lcom/itau/empresas/ui/fragment/OutrosFragment_$3;-><init>(Lcom/itau/empresas/ui/fragment/OutrosFragment_;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 141
    :cond_c8
    iget-object v0, p0, Lcom/itau/empresas/ui/fragment/OutrosFragment_;->textoTermosDeUso:Landroid/widget/TextView;

    if-eqz v0, :cond_d6

    .line 142
    iget-object v0, p0, Lcom/itau/empresas/ui/fragment/OutrosFragment_;->textoTermosDeUso:Landroid/widget/TextView;

    new-instance v1, Lcom/itau/empresas/ui/fragment/OutrosFragment_$4;

    invoke-direct {v1, p0}, Lcom/itau/empresas/ui/fragment/OutrosFragment_$4;-><init>(Lcom/itau/empresas/ui/fragment/OutrosFragment_;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 151
    :cond_d6
    iget-object v0, p0, Lcom/itau/empresas/ui/fragment/OutrosFragment_;->textoAvaliar:Landroid/widget/TextView;

    if-eqz v0, :cond_e4

    .line 152
    iget-object v0, p0, Lcom/itau/empresas/ui/fragment/OutrosFragment_;->textoAvaliar:Landroid/widget/TextView;

    new-instance v1, Lcom/itau/empresas/ui/fragment/OutrosFragment_$5;

    invoke-direct {v1, p0}, Lcom/itau/empresas/ui/fragment/OutrosFragment_$5;-><init>(Lcom/itau/empresas/ui/fragment/OutrosFragment_;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 161
    :cond_e4
    if-eqz v2, :cond_ee

    .line 162
    new-instance v0, Lcom/itau/empresas/ui/fragment/OutrosFragment_$6;

    invoke-direct {v0, p0}, Lcom/itau/empresas/ui/fragment/OutrosFragment_$6;-><init>(Lcom/itau/empresas/ui/fragment/OutrosFragment_;)V

    invoke-virtual {v2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 171
    :cond_ee
    if-eqz v3, :cond_f8

    .line 172
    new-instance v0, Lcom/itau/empresas/ui/fragment/OutrosFragment_$7;

    invoke-direct {v0, p0}, Lcom/itau/empresas/ui/fragment/OutrosFragment_$7;-><init>(Lcom/itau/empresas/ui/fragment/OutrosFragment_;)V

    invoke-virtual {v3, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 181
    :cond_f8
    if-eqz v4, :cond_102

    .line 182
    new-instance v0, Lcom/itau/empresas/ui/fragment/OutrosFragment_$8;

    invoke-direct {v0, p0}, Lcom/itau/empresas/ui/fragment/OutrosFragment_$8;-><init>(Lcom/itau/empresas/ui/fragment/OutrosFragment_;)V

    invoke-virtual {v4, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 191
    :cond_102
    invoke-virtual {p0}, Lcom/itau/empresas/ui/fragment/OutrosFragment_;->aoIniciarActivity()V

    .line 192
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .registers 4
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;

    .line 84
    invoke-super {p0, p1, p2}, Lcom/itau/empresas/ui/fragment/OutrosFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 85
    iget-object v0, p0, Lcom/itau/empresas/ui/fragment/OutrosFragment_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 86
    return-void
.end method

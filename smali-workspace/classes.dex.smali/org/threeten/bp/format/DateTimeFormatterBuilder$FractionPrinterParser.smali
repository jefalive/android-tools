.class final Lorg/threeten/bp/format/DateTimeFormatterBuilder$FractionPrinterParser;
.super Ljava/lang/Object;
.source "DateTimeFormatterBuilder.java"

# interfaces
.implements Lorg/threeten/bp/format/DateTimeFormatterBuilder$DateTimePrinterParser;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/threeten/bp/format/DateTimeFormatterBuilder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "FractionPrinterParser"
.end annotation


# instance fields
.field private final decimalPoint:Z

.field private final field:Lorg/threeten/bp/temporal/TemporalField;

.field private final maxWidth:I

.field private final minWidth:I


# direct methods
.method constructor <init>(Lorg/threeten/bp/temporal/TemporalField;IIZ)V
    .registers 8
    .param p1, "field"    # Lorg/threeten/bp/temporal/TemporalField;
    .param p2, "minWidth"    # I
    .param p3, "maxWidth"    # I
    .param p4, "decimalPoint"    # Z

    .line 2674
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2675
    const-string v0, "field"

    invoke-static {p1, v0}, Lorg/threeten/bp/jdk8/Jdk8Methods;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 2676
    invoke-interface {p1}, Lorg/threeten/bp/temporal/TemporalField;->range()Lorg/threeten/bp/temporal/ValueRange;

    move-result-object v0

    invoke-virtual {v0}, Lorg/threeten/bp/temporal/ValueRange;->isFixed()Z

    move-result v0

    if-nez v0, :cond_2b

    .line 2677
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Field must have a fixed set of values: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2679
    :cond_2b
    if-ltz p2, :cond_31

    const/16 v0, 0x9

    if-le p2, v0, :cond_4a

    .line 2680
    :cond_31
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Minimum width must be from 0 to 9 inclusive but was "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2682
    :cond_4a
    const/4 v0, 0x1

    if-lt p3, v0, :cond_51

    const/16 v0, 0x9

    if-le p3, v0, :cond_6a

    .line 2683
    :cond_51
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Maximum width must be from 1 to 9 inclusive but was "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2685
    :cond_6a
    if-ge p3, p2, :cond_8f

    .line 2686
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Maximum width must exceed or equal the minimum width but "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " < "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2689
    :cond_8f
    iput-object p1, p0, Lorg/threeten/bp/format/DateTimeFormatterBuilder$FractionPrinterParser;->field:Lorg/threeten/bp/temporal/TemporalField;

    .line 2690
    iput p2, p0, Lorg/threeten/bp/format/DateTimeFormatterBuilder$FractionPrinterParser;->minWidth:I

    .line 2691
    iput p3, p0, Lorg/threeten/bp/format/DateTimeFormatterBuilder$FractionPrinterParser;->maxWidth:I

    .line 2692
    iput-boolean p4, p0, Lorg/threeten/bp/format/DateTimeFormatterBuilder$FractionPrinterParser;->decimalPoint:Z

    .line 2693
    return-void
.end method

.method private convertFromFraction(Ljava/math/BigDecimal;)J
    .registers 9
    .param p1, "fraction"    # Ljava/math/BigDecimal;

    .line 2809
    iget-object v0, p0, Lorg/threeten/bp/format/DateTimeFormatterBuilder$FractionPrinterParser;->field:Lorg/threeten/bp/temporal/TemporalField;

    invoke-interface {v0}, Lorg/threeten/bp/temporal/TemporalField;->range()Lorg/threeten/bp/temporal/ValueRange;

    move-result-object v3

    .line 2810
    .local v3, "range":Lorg/threeten/bp/temporal/ValueRange;
    invoke-virtual {v3}, Lorg/threeten/bp/temporal/ValueRange;->getMinimum()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/math/BigDecimal;->valueOf(J)Ljava/math/BigDecimal;

    move-result-object v4

    .line 2811
    .local v4, "minBD":Ljava/math/BigDecimal;
    invoke-virtual {v3}, Lorg/threeten/bp/temporal/ValueRange;->getMaximum()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/math/BigDecimal;->valueOf(J)Ljava/math/BigDecimal;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/math/BigDecimal;->subtract(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v0

    sget-object v1, Ljava/math/BigDecimal;->ONE:Ljava/math/BigDecimal;

    invoke-virtual {v0, v1}, Ljava/math/BigDecimal;->add(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v5

    .line 2812
    .local v5, "rangeBD":Ljava/math/BigDecimal;
    invoke-virtual {p1, v5}, Ljava/math/BigDecimal;->multiply(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v0

    sget-object v1, Ljava/math/RoundingMode;->FLOOR:Ljava/math/RoundingMode;

    const/4 v2, 0x0

    invoke-virtual {v0, v2, v1}, Ljava/math/BigDecimal;->setScale(ILjava/math/RoundingMode;)Ljava/math/BigDecimal;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/math/BigDecimal;->add(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v6

    .line 2813
    .local v6, "valueBD":Ljava/math/BigDecimal;
    invoke-virtual {v6}, Ljava/math/BigDecimal;->longValueExact()J

    move-result-wide v0

    return-wide v0
.end method

.method private convertToFraction(J)Ljava/math/BigDecimal;
    .registers 10
    .param p1, "value"    # J

    .line 2782
    iget-object v0, p0, Lorg/threeten/bp/format/DateTimeFormatterBuilder$FractionPrinterParser;->field:Lorg/threeten/bp/temporal/TemporalField;

    invoke-interface {v0}, Lorg/threeten/bp/temporal/TemporalField;->range()Lorg/threeten/bp/temporal/ValueRange;

    move-result-object v2

    .line 2783
    .local v2, "range":Lorg/threeten/bp/temporal/ValueRange;
    iget-object v0, p0, Lorg/threeten/bp/format/DateTimeFormatterBuilder$FractionPrinterParser;->field:Lorg/threeten/bp/temporal/TemporalField;

    invoke-virtual {v2, p1, p2, v0}, Lorg/threeten/bp/temporal/ValueRange;->checkValidValue(JLorg/threeten/bp/temporal/TemporalField;)J

    .line 2784
    invoke-virtual {v2}, Lorg/threeten/bp/temporal/ValueRange;->getMinimum()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/math/BigDecimal;->valueOf(J)Ljava/math/BigDecimal;

    move-result-object v3

    .line 2785
    .local v3, "minBD":Ljava/math/BigDecimal;
    invoke-virtual {v2}, Lorg/threeten/bp/temporal/ValueRange;->getMaximum()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/math/BigDecimal;->valueOf(J)Ljava/math/BigDecimal;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/math/BigDecimal;->subtract(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v0

    sget-object v1, Ljava/math/BigDecimal;->ONE:Ljava/math/BigDecimal;

    invoke-virtual {v0, v1}, Ljava/math/BigDecimal;->add(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v4

    .line 2786
    .local v4, "rangeBD":Ljava/math/BigDecimal;
    invoke-static {p1, p2}, Ljava/math/BigDecimal;->valueOf(J)Ljava/math/BigDecimal;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/math/BigDecimal;->subtract(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v5

    .line 2787
    .local v5, "valueBD":Ljava/math/BigDecimal;
    sget-object v0, Ljava/math/RoundingMode;->FLOOR:Ljava/math/RoundingMode;

    const/16 v1, 0x9

    invoke-virtual {v5, v4, v1, v0}, Ljava/math/BigDecimal;->divide(Ljava/math/BigDecimal;ILjava/math/RoundingMode;)Ljava/math/BigDecimal;

    move-result-object v6

    .line 2789
    .local v6, "fraction":Ljava/math/BigDecimal;
    sget-object v0, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    invoke-virtual {v6, v0}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result v0

    if-nez v0, :cond_40

    sget-object v0, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    goto :goto_44

    :cond_40
    invoke-virtual {v6}, Ljava/math/BigDecimal;->stripTrailingZeros()Ljava/math/BigDecimal;

    move-result-object v0

    :goto_44
    return-object v0
.end method


# virtual methods
.method public parse(Lorg/threeten/bp/format/DateTimeParseContext;Ljava/lang/CharSequence;I)I
    .registers 20
    .param p1, "context"    # Lorg/threeten/bp/format/DateTimeParseContext;
    .param p2, "text"    # Ljava/lang/CharSequence;
    .param p3, "position"    # I

    .line 2727
    invoke-virtual/range {p1 .. p1}, Lorg/threeten/bp/format/DateTimeParseContext;->isStrict()Z

    move-result v0

    if-eqz v0, :cond_b

    move-object/from16 v0, p0

    iget v6, v0, Lorg/threeten/bp/format/DateTimeFormatterBuilder$FractionPrinterParser;->minWidth:I

    goto :goto_c

    :cond_b
    const/4 v6, 0x0

    .line 2728
    .local v6, "effectiveMin":I
    :goto_c
    invoke-virtual/range {p1 .. p1}, Lorg/threeten/bp/format/DateTimeParseContext;->isStrict()Z

    move-result v0

    if-eqz v0, :cond_17

    move-object/from16 v0, p0

    iget v7, v0, Lorg/threeten/bp/format/DateTimeFormatterBuilder$FractionPrinterParser;->maxWidth:I

    goto :goto_19

    :cond_17
    const/16 v7, 0x9

    .line 2729
    .local v7, "effectiveMax":I
    :goto_19
    invoke-interface/range {p2 .. p2}, Ljava/lang/CharSequence;->length()I

    move-result v8

    .line 2730
    .local v8, "length":I
    move/from16 v0, p3

    if-ne v0, v8, :cond_29

    .line 2732
    if-lez v6, :cond_26

    xor-int/lit8 v0, p3, -0x1

    goto :goto_28

    :cond_26
    move/from16 v0, p3

    :goto_28
    return v0

    .line 2734
    :cond_29
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lorg/threeten/bp/format/DateTimeFormatterBuilder$FractionPrinterParser;->decimalPoint:Z

    if-eqz v0, :cond_4b

    .line 2735
    move-object/from16 v0, p2

    move/from16 v1, p3

    invoke-interface {v0, v1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    invoke-virtual/range {p1 .. p1}, Lorg/threeten/bp/format/DateTimeParseContext;->getSymbols()Lorg/threeten/bp/format/DecimalStyle;

    move-result-object v1

    invoke-virtual {v1}, Lorg/threeten/bp/format/DecimalStyle;->getDecimalSeparator()C

    move-result v1

    if-eq v0, v1, :cond_49

    .line 2737
    if-lez v6, :cond_46

    xor-int/lit8 v0, p3, -0x1

    goto :goto_48

    :cond_46
    move/from16 v0, p3

    :goto_48
    return v0

    .line 2739
    :cond_49
    add-int/lit8 p3, p3, 0x1

    .line 2741
    :cond_4b
    add-int v9, p3, v6

    .line 2742
    .local v9, "minEndPos":I
    if-le v9, v8, :cond_52

    .line 2743
    xor-int/lit8 v0, p3, -0x1

    return v0

    .line 2745
    :cond_52
    add-int v0, p3, v7

    invoke-static {v0, v8}, Ljava/lang/Math;->min(II)I

    move-result v10

    .line 2746
    .local v10, "maxEndPos":I
    const/4 v11, 0x0

    .line 2747
    .local v11, "total":I
    move/from16 v12, p3

    .line 2748
    .local v12, "pos":I
    :goto_5b
    if-ge v12, v10, :cond_7d

    .line 2749
    move v0, v12

    add-int/lit8 v12, v12, 0x1

    move-object/from16 v1, p2

    invoke-interface {v1, v0}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v13

    .line 2750
    .local v13, "ch":C
    invoke-virtual/range {p1 .. p1}, Lorg/threeten/bp/format/DateTimeParseContext;->getSymbols()Lorg/threeten/bp/format/DecimalStyle;

    move-result-object v0

    invoke-virtual {v0, v13}, Lorg/threeten/bp/format/DecimalStyle;->convertToDigit(C)I

    move-result v14

    .line 2751
    .local v14, "digit":I
    if-gez v14, :cond_78

    .line 2752
    if-ge v12, v9, :cond_75

    .line 2753
    xor-int/lit8 v0, p3, -0x1

    return v0

    .line 2755
    :cond_75
    add-int/lit8 v12, v12, -0x1

    .line 2756
    goto :goto_7d

    .line 2758
    :cond_78
    mul-int/lit8 v0, v11, 0xa

    add-int v11, v0, v14

    .line 2759
    .end local v13    # "ch":C
    .end local v14    # "digit":I
    goto :goto_5b

    .line 2760
    :cond_7d
    :goto_7d
    new-instance v0, Ljava/math/BigDecimal;

    invoke-direct {v0, v11}, Ljava/math/BigDecimal;-><init>(I)V

    sub-int v1, v12, p3

    invoke-virtual {v0, v1}, Ljava/math/BigDecimal;->movePointLeft(I)Ljava/math/BigDecimal;

    move-result-object v13

    .line 2761
    .local v13, "fraction":Ljava/math/BigDecimal;
    move-object/from16 v0, p0

    invoke-direct {v0, v13}, Lorg/threeten/bp/format/DateTimeFormatterBuilder$FractionPrinterParser;->convertFromFraction(Ljava/math/BigDecimal;)J

    move-result-wide v14

    .line 2762
    .local v14, "value":J
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iget-object v1, v1, Lorg/threeten/bp/format/DateTimeFormatterBuilder$FractionPrinterParser;->field:Lorg/threeten/bp/temporal/TemporalField;

    move-wide v2, v14

    move/from16 v4, p3

    move v5, v12

    invoke-virtual/range {v0 .. v5}, Lorg/threeten/bp/format/DateTimeParseContext;->setParsedField(Lorg/threeten/bp/temporal/TemporalField;JII)I

    move-result v0

    return v0
.end method

.method public print(Lorg/threeten/bp/format/DateTimePrintContext;Ljava/lang/StringBuilder;)Z
    .registers 10
    .param p1, "context"    # Lorg/threeten/bp/format/DateTimePrintContext;
    .param p2, "buf"    # Ljava/lang/StringBuilder;

    .line 2697
    iget-object v0, p0, Lorg/threeten/bp/format/DateTimeFormatterBuilder$FractionPrinterParser;->field:Lorg/threeten/bp/temporal/TemporalField;

    invoke-virtual {p1, v0}, Lorg/threeten/bp/format/DateTimePrintContext;->getValue(Lorg/threeten/bp/temporal/TemporalField;)Ljava/lang/Long;

    move-result-object v2

    .line 2698
    .local v2, "value":Ljava/lang/Long;
    if-nez v2, :cond_a

    .line 2699
    const/4 v0, 0x0

    return v0

    .line 2701
    :cond_a
    invoke-virtual {p1}, Lorg/threeten/bp/format/DateTimePrintContext;->getSymbols()Lorg/threeten/bp/format/DecimalStyle;

    move-result-object v3

    .line 2702
    .local v3, "symbols":Lorg/threeten/bp/format/DecimalStyle;
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lorg/threeten/bp/format/DateTimeFormatterBuilder$FractionPrinterParser;->convertToFraction(J)Ljava/math/BigDecimal;

    move-result-object v4

    .line 2703
    .local v4, "fraction":Ljava/math/BigDecimal;
    invoke-virtual {v4}, Ljava/math/BigDecimal;->scale()I

    move-result v0

    if-nez v0, :cond_3b

    .line 2704
    iget v0, p0, Lorg/threeten/bp/format/DateTimeFormatterBuilder$FractionPrinterParser;->minWidth:I

    if-lez v0, :cond_6c

    .line 2705
    iget-boolean v0, p0, Lorg/threeten/bp/format/DateTimeFormatterBuilder$FractionPrinterParser;->decimalPoint:Z

    if-eqz v0, :cond_2b

    .line 2706
    invoke-virtual {v3}, Lorg/threeten/bp/format/DecimalStyle;->getDecimalSeparator()C

    move-result v0

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 2708
    :cond_2b
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_2c
    iget v0, p0, Lorg/threeten/bp/format/DateTimeFormatterBuilder$FractionPrinterParser;->minWidth:I

    if-ge v5, v0, :cond_3a

    .line 2709
    invoke-virtual {v3}, Lorg/threeten/bp/format/DecimalStyle;->getZeroDigit()C

    move-result v0

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 2708
    add-int/lit8 v5, v5, 0x1

    goto :goto_2c

    .end local v5    # "i":I
    :cond_3a
    goto :goto_6c

    .line 2713
    :cond_3b
    invoke-virtual {v4}, Ljava/math/BigDecimal;->scale()I

    move-result v0

    iget v1, p0, Lorg/threeten/bp/format/DateTimeFormatterBuilder$FractionPrinterParser;->minWidth:I

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    iget v1, p0, Lorg/threeten/bp/format/DateTimeFormatterBuilder$FractionPrinterParser;->maxWidth:I

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v5

    .line 2714
    .local v5, "outputScale":I
    sget-object v0, Ljava/math/RoundingMode;->FLOOR:Ljava/math/RoundingMode;

    invoke-virtual {v4, v5, v0}, Ljava/math/BigDecimal;->setScale(ILjava/math/RoundingMode;)Ljava/math/BigDecimal;

    move-result-object v4

    .line 2715
    invoke-virtual {v4}, Ljava/math/BigDecimal;->toPlainString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v6

    .line 2716
    .local v6, "str":Ljava/lang/String;
    invoke-virtual {v3, v6}, Lorg/threeten/bp/format/DecimalStyle;->convertNumberToI18N(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 2717
    iget-boolean v0, p0, Lorg/threeten/bp/format/DateTimeFormatterBuilder$FractionPrinterParser;->decimalPoint:Z

    if-eqz v0, :cond_69

    .line 2718
    invoke-virtual {v3}, Lorg/threeten/bp/format/DecimalStyle;->getDecimalSeparator()C

    move-result v0

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 2720
    :cond_69
    invoke-virtual {p2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2722
    .end local v5    # "outputScale":I
    .end local v6    # "str":Ljava/lang/String;
    :cond_6c
    :goto_6c
    const/4 v0, 0x1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .registers 4

    .line 2818
    iget-boolean v0, p0, Lorg/threeten/bp/format/DateTimeFormatterBuilder$FractionPrinterParser;->decimalPoint:Z

    if-eqz v0, :cond_7

    const-string v2, ",DecimalPoint"

    goto :goto_9

    :cond_7
    const-string v2, ""

    .line 2819
    .local v2, "decimal":Ljava/lang/String;
    :goto_9
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Fraction("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/threeten/bp/format/DateTimeFormatterBuilder$FractionPrinterParser;->field:Lorg/threeten/bp/temporal/TemporalField;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lorg/threeten/bp/format/DateTimeFormatterBuilder$FractionPrinterParser;->minWidth:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lorg/threeten/bp/format/DateTimeFormatterBuilder$FractionPrinterParser;->maxWidth:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

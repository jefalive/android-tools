.class public Lcom/itau/empresas/feature/login/PreLoginActivity;
.super Lcom/itau/empresas/ui/activity/BaseActivity;
.source "PreLoginActivity.java"

# interfaces
.implements Lcom/itau/empresas/feature/login/strategy/ListenerStrategy;
.implements Lcom/itau/empresas/feature/login/PreLoginInput$PreLoginInputListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/itau/empresas/feature/login/PreLoginActivity$EventoPreLoginMensagemErro;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private abrirTeclado:Z

.field private analyticsHelper:Lcom/itau/empresas/feature/analytics/AnalyticsHelper;

.field boletoPdfUri:Landroid/net/Uri;

.field clPreLogin:Landroid/view/ViewGroup;

.field private controller:Lcom/itau/empresas/feature/login/controller/PreLoginController;

.field instalarIToken:Z

.field navegacaoVindoDoLogin:Z

.field private preLoginInput:Lcom/itau/empresas/feature/login/PreLoginInput;

.field senhaSalva:Landroid/support/v7/widget/SwitchCompat;

.field private strategyAtual:Lcom/itau/empresas/feature/login/strategy/PreLoginStrategy;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .line 41
    const-class v0, Lcom/itau/empresas/feature/login/PreLoginActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/itau/empresas/feature/login/PreLoginActivity;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .registers 2

    .line 38
    invoke-direct {p0}, Lcom/itau/empresas/ui/activity/BaseActivity;-><init>()V

    .line 44
    invoke-static {}, Lcom/itau/empresas/feature/login/controller/PreLoginController;->getInstance()Lcom/itau/empresas/feature/login/controller/PreLoginController;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/login/PreLoginActivity;->controller:Lcom/itau/empresas/feature/login/controller/PreLoginController;

    .line 64
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/itau/empresas/feature/login/PreLoginActivity;->abrirTeclado:Z

    return-void
.end method

.method static synthetic access$000(Lcom/itau/empresas/feature/login/PreLoginActivity;)Lcom/itau/empresas/feature/login/PreLoginInput;
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/feature/login/PreLoginActivity;

    .line 38
    iget-object v0, p0, Lcom/itau/empresas/feature/login/PreLoginActivity;->preLoginInput:Lcom/itau/empresas/feature/login/PreLoginInput;

    return-object v0
.end method

.method static synthetic access$100()Ljava/lang/String;
    .registers 1

    .line 38
    sget-object v0, Lcom/itau/empresas/feature/login/PreLoginActivity;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/itau/empresas/feature/login/PreLoginActivity;)Lcom/itau/empresas/feature/login/strategy/PreLoginStrategy;
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/feature/login/PreLoginActivity;

    .line 38
    iget-object v0, p0, Lcom/itau/empresas/feature/login/PreLoginActivity;->strategyAtual:Lcom/itau/empresas/feature/login/strategy/PreLoginStrategy;

    return-object v0
.end method

.method private disparaEventoAdobe(Lcom/itau/empresas/feature/login/PreLoginActivity$EventoPreLoginMensagemErro;)V
    .registers 6
    .param p1, "e"    # Lcom/itau/empresas/feature/login/PreLoginActivity$EventoPreLoginMensagemErro;

    .line 273
    invoke-static {}, Lcom/itau/empresas/feature/analytics/AnalyticsHelper;->getInstance()Lcom/itau/empresas/feature/analytics/AnalyticsHelper;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/login/PreLoginActivity;->analyticsHelper:Lcom/itau/empresas/feature/analytics/AnalyticsHelper;

    .line 274
    iget-object v0, p0, Lcom/itau/empresas/feature/login/PreLoginActivity;->analyticsHelper:Lcom/itau/empresas/feature/analytics/AnalyticsHelper;

    .line 276
    const v1, 0x7f0700ec

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/login/PreLoginActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "Erro"

    .line 278
    # getter for: Lcom/itau/empresas/feature/login/PreLoginActivity$EventoPreLoginMensagemErro;->message:Ljava/lang/String;
    invoke-static {p1}, Lcom/itau/empresas/feature/login/PreLoginActivity$EventoPreLoginMensagemErro;->access$400(Lcom/itau/empresas/feature/login/PreLoginActivity$EventoPreLoginMensagemErro;)Ljava/lang/String;

    move-result-object v3

    .line 275
    invoke-virtual {v0, v1, v2, v3}, Lcom/itau/empresas/feature/analytics/AnalyticsHelper;->eventoAdobeAction(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 279
    .line 280
    const v0, 0x7f0700ec

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/login/PreLoginActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 281
    const v1, 0x7f0700cc

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/login/PreLoginActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 282
    const v2, 0x7f0700c4

    invoke-virtual {p0, v2}, Lcom/itau/empresas/feature/login/PreLoginActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 279
    invoke-direct {p0, v0, v1, v2}, Lcom/itau/empresas/feature/login/PreLoginActivity;->disparaEventoAnalytcs(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 283
    return-void
.end method

.method private disparaEventoAnalytcs(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 5
    .param p1, "nome"    # Ljava/lang/String;
    .param p2, "secao"    # Ljava/lang/String;
    .param p3, "estadoLogin"    # Ljava/lang/String;

    .line 286
    invoke-static {}, Lcom/itau/empresas/feature/analytics/AnalyticsHelper;->getInstance()Lcom/itau/empresas/feature/analytics/AnalyticsHelper;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/login/PreLoginActivity;->analyticsHelper:Lcom/itau/empresas/feature/analytics/AnalyticsHelper;

    .line 287
    iget-object v0, p0, Lcom/itau/empresas/feature/login/PreLoginActivity;->analyticsHelper:Lcom/itau/empresas/feature/analytics/AnalyticsHelper;

    invoke-virtual {v0, p1, p2, p3}, Lcom/itau/empresas/feature/analytics/AnalyticsHelper;->eventoAdobeState(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 288
    return-void
.end method

.method private executaModoMultiDocumento()V
    .registers 2

    .line 194
    new-instance v0, Lcom/itau/empresas/feature/login/PreLoginActivity$3;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/login/PreLoginActivity$3;-><init>(Lcom/itau/empresas/feature/login/PreLoginActivity;)V

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/login/PreLoginActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 201
    return-void
.end method

.method private executaModoMultiOperador()V
    .registers 2

    .line 182
    new-instance v0, Lcom/itau/empresas/feature/login/PreLoginActivity$2;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/login/PreLoginActivity$2;-><init>(Lcom/itau/empresas/feature/login/PreLoginActivity;)V

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/login/PreLoginActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 191
    return-void
.end method

.method private executaModoUnicoDocumento()V
    .registers 2

    .line 204
    new-instance v0, Lcom/itau/empresas/feature/login/PreLoginActivity$4;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/login/PreLoginActivity$4;-><init>(Lcom/itau/empresas/feature/login/PreLoginActivity;)V

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/login/PreLoginActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 211
    return-void
.end method

.method public static intent(Landroid/content/Context;)Lcom/itau/empresas/feature/login/PreLoginActivity_$IntentBuilder_;
    .registers 2
    .param p0, "context"    # Landroid/content/Context;

    .line 67
    invoke-static {p0}, Lcom/itau/empresas/feature/login/PreLoginActivity_;->intent(Landroid/content/Context;)Lcom/itau/empresas/feature/login/PreLoginActivity_$IntentBuilder_;

    move-result-object v0

    return-object v0
.end method

.method public static intentItoken(Landroid/content/Context;)Landroid/content/Intent;
    .registers 3
    .param p0, "context"    # Landroid/content/Context;

    .line 269
    invoke-static {p0}, Lcom/itau/empresas/feature/login/PreLoginActivity;->intent(Landroid/content/Context;)Lcom/itau/empresas/feature/login/PreLoginActivity_$IntentBuilder_;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/login/PreLoginActivity_$IntentBuilder_;->instalarIToken(Z)Lcom/itau/empresas/feature/login/PreLoginActivity_$IntentBuilder_;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/feature/login/PreLoginActivity_$IntentBuilder_;->get()Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method private verificaAcessoLembrado()Lcom/itau/empresas/api/model/OperadorVO;
    .registers 3

    .line 112
    new-instance v1, Lcom/itau/empresas/feature/login/LembrarAcesso;

    iget-object v0, p0, Lcom/itau/empresas/feature/login/PreLoginActivity;->application:Lcom/itau/empresas/CustomApplication;

    .line 113
    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/itau/empresas/feature/login/LembrarAcesso;-><init>(Landroid/content/SharedPreferences;)V

    .line 114
    .local v1, "lembrarAcesso":Lcom/itau/empresas/feature/login/LembrarAcesso;
    invoke-virtual {v1}, Lcom/itau/empresas/feature/login/LembrarAcesso;->consultarAcesso()Lcom/itau/empresas/api/model/OperadorVO;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method aoClicarNoTextoAbraSuaConta()V
    .registers 5

    .line 170
    const v0, 0x7f070291

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/login/PreLoginActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/login/PreLoginActivity;->analyticsHit(Ljava/lang/String;)V

    .line 171
    .line 172
    const v0, 0x7f0700b9

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/login/PreLoginActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 173
    const v1, 0x7f0700ca

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/login/PreLoginActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 174
    const v2, 0x7f0700c4

    invoke-virtual {p0, v2}, Lcom/itau/empresas/feature/login/PreLoginActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 171
    invoke-direct {p0, v0, v1, v2}, Lcom/itau/empresas/feature/login/PreLoginActivity;->disparaEventoAnalytcs(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 176
    new-instance v3, Landroid/content/Intent;

    const-string v0, "android.intent.action.VIEW"

    invoke-direct {v3, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 177
    .local v3, "i":Landroid/content/Intent;
    const v0, 0x7f070449

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/login/PreLoginActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 178
    invoke-virtual {p0, v3}, Lcom/itau/empresas/feature/login/PreLoginActivity;->startActivity(Landroid/content/Intent;)V

    .line 179
    return-void
.end method

.method aoCriar()V
    .registers 5

    .line 72
    sget-object v0, Lcom/itau/empresas/feature/login/PreLoginActivity;->TAG:Ljava/lang/String;

    const-string v1, "aoCriar() called"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 75
    new-instance v0, Lcom/itau/empresas/feature/login/PreLoginInputImpl;

    const v1, 0x7f0e029a

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/login/PreLoginActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Lcom/itau/empresas/feature/login/PreLoginInputImpl;-><init>(Landroid/view/View;Lcom/itau/empresas/feature/login/PreLoginInput$PreLoginInputListener;)V

    iput-object v0, p0, Lcom/itau/empresas/feature/login/PreLoginActivity;->preLoginInput:Lcom/itau/empresas/feature/login/PreLoginInput;

    .line 77
    new-instance v2, Lcom/itau/empresas/feature/login/PreLoginWhatsNew;

    invoke-direct {v2, p0}, Lcom/itau/empresas/feature/login/PreLoginWhatsNew;-><init>(Lcom/itau/empresas/feature/login/PreLoginActivity;)V

    .line 78
    .local v2, "preLoginWhatsNew":Lcom/itau/empresas/feature/login/PreLoginWhatsNew;
    invoke-virtual {v2}, Lcom/itau/empresas/feature/login/PreLoginWhatsNew;->deveExibirWhatsNews()Z

    move-result v0

    if-eqz v0, :cond_21

    .line 79
    return-void

    .line 82
    :cond_21
    invoke-direct {p0}, Lcom/itau/empresas/feature/login/PreLoginActivity;->verificaAcessoLembrado()Lcom/itau/empresas/api/model/OperadorVO;

    move-result-object v3

    .line 83
    .local v3, "operadorVO":Lcom/itau/empresas/api/model/OperadorVO;
    iget-boolean v0, p0, Lcom/itau/empresas/feature/login/PreLoginActivity;->navegacaoVindoDoLogin:Z

    if-nez v0, :cond_36

    if-eqz v3, :cond_36

    .line 84
    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/itau/empresas/feature/login/PreLoginActivity;->abrirTeclado:Z

    .line 85
    invoke-virtual {p0, v3}, Lcom/itau/empresas/feature/login/PreLoginActivity;->sucesso(Lcom/itau/empresas/api/model/OperadorVO;)V

    .line 87
    :cond_36
    return-void
.end method

.method public clicouEmAcessar()V
    .registers 3

    .line 124
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/itau/empresas/feature/login/PreLoginActivity;->abrirTeclado:Z

    .line 125
    iget-object v0, p0, Lcom/itau/empresas/feature/login/PreLoginActivity;->preLoginInput:Lcom/itau/empresas/feature/login/PreLoginInput;

    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-interface {v0, v1}, Lcom/itau/empresas/feature/login/PreLoginInput;->alternaExibicaoBotaoEntrarParProgresso(Z)V

    .line 126
    iget-object v0, p0, Lcom/itau/empresas/feature/login/PreLoginActivity;->preLoginInput:Lcom/itau/empresas/feature/login/PreLoginInput;

    invoke-interface {v0}, Lcom/itau/empresas/feature/login/PreLoginInput;->postaTipoDeInput()V

    .line 127
    return-void
.end method

.method public falha(Ljava/lang/String;)V
    .registers 5
    .param p1, "backendException"    # Ljava/lang/String;

    .line 220
    invoke-static {}, Lde/greenrobot/event/EventBus;->getDefault()Lde/greenrobot/event/EventBus;

    move-result-object v0

    new-instance v1, Lcom/itau/empresas/feature/login/PreLoginActivity$EventoPreLoginMensagemErro;

    const/4 v2, 0x0

    invoke-direct {v1, p1, v2}, Lcom/itau/empresas/feature/login/PreLoginActivity$EventoPreLoginMensagemErro;-><init>(Ljava/lang/String;Lcom/itau/empresas/feature/login/PreLoginActivity$1;)V

    invoke-virtual {v0, v1}, Lde/greenrobot/event/EventBus;->post(Ljava/lang/Object;)V

    .line 222
    return-void
.end method

.method public onEventMainThread(Lcom/itau/empresas/feature/login/PreLoginActivity$EventoPreLoginMensagemErro;)V
    .registers 5
    .param p1, "e"    # Lcom/itau/empresas/feature/login/PreLoginActivity$EventoPreLoginMensagemErro;

    .line 238
    iget-object v0, p0, Lcom/itau/empresas/feature/login/PreLoginActivity;->preLoginInput:Lcom/itau/empresas/feature/login/PreLoginInput;

    sget-object v1, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-interface {v0, v1}, Lcom/itau/empresas/feature/login/PreLoginInput;->alternaExibicaoBotaoEntrarParProgresso(Z)V

    .line 239
    iget-object v0, p0, Lcom/itau/empresas/feature/login/PreLoginActivity;->clPreLogin:Landroid/view/ViewGroup;

    # getter for: Lcom/itau/empresas/feature/login/PreLoginActivity$EventoPreLoginMensagemErro;->message:Ljava/lang/String;
    invoke-static {p1}, Lcom/itau/empresas/feature/login/PreLoginActivity$EventoPreLoginMensagemErro;->access$400(Lcom/itau/empresas/feature/login/PreLoginActivity$EventoPreLoginMensagemErro;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, -0x2

    invoke-static {v0, v1, v2}, Lbr/com/itau/topsnackbar/TopSnackbar;->make(Landroid/view/ViewGroup;Ljava/lang/CharSequence;I)Lbr/com/itau/topsnackbar/TopSnackbar;

    move-result-object v0

    invoke-virtual {v0}, Lbr/com/itau/topsnackbar/TopSnackbar;->show()Lbr/com/itau/topsnackbar/TopSnackbar;

    .line 241
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/login/PreLoginActivity;->disparaEventoAdobe(Lcom/itau/empresas/feature/login/PreLoginActivity$EventoPreLoginMensagemErro;)V

    .line 242
    return-void
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .registers 5
    .param p1, "intent"    # Landroid/content/Intent;

    .line 98
    invoke-super {p0, p1}, Lcom/itau/empresas/ui/activity/BaseActivity;->onNewIntent(Landroid/content/Intent;)V

    .line 99
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 100
    .local v1, "extras":Landroid/os/Bundle;
    if-nez v1, :cond_a

    .line 101
    return-void

    .line 104
    :cond_a
    const-string v0, "instalarIToken"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/itau/empresas/feature/login/PreLoginActivity;->instalarIToken:Z

    .line 105
    invoke-virtual {p0}, Lcom/itau/empresas/feature/login/PreLoginActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    .line 106
    .local v2, "intentAntigo":Landroid/content/Intent;
    invoke-virtual {v2, v1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 107
    invoke-virtual {p0, v2}, Lcom/itau/empresas/feature/login/PreLoginActivity;->setIntent(Landroid/content/Intent;)V

    .line 108
    return-void
.end method

.method protected onResume()V
    .registers 3

    .line 91
    invoke-super {p0}, Lcom/itau/empresas/ui/activity/BaseActivity;->onResume()V

    .line 92
    iget-object v0, p0, Lcom/itau/empresas/feature/login/PreLoginActivity;->clPreLogin:Landroid/view/ViewGroup;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 93
    iget-object v0, p0, Lcom/itau/empresas/feature/login/PreLoginActivity;->preLoginInput:Lcom/itau/empresas/feature/login/PreLoginInput;

    sget-object v1, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-interface {v0, v1}, Lcom/itau/empresas/feature/login/PreLoginInput;->alternaExibicaoBotaoEntrarParProgresso(Z)V

    .line 94
    return-void
.end method

.method public opkeysConsumidas()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Ljava/lang/String;>;"
        }
    .end annotation

    .line 119
    const-string v0, ""

    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public proximoPasso(I)V
    .registers 2
    .param p1, "tipoErro"    # I

    .line 152
    sparse-switch p1, :sswitch_data_10

    goto :goto_f

    .line 154
    :sswitch_4
    invoke-direct {p0}, Lcom/itau/empresas/feature/login/PreLoginActivity;->executaModoUnicoDocumento()V

    .line 155
    goto :goto_f

    .line 157
    :sswitch_8
    invoke-direct {p0}, Lcom/itau/empresas/feature/login/PreLoginActivity;->executaModoMultiDocumento()V

    .line 158
    goto :goto_f

    .line 160
    :sswitch_c
    invoke-direct {p0}, Lcom/itau/empresas/feature/login/PreLoginActivity;->executaModoMultiOperador()V

    .line 161
    .line 166
    :goto_f
    :sswitch_f
    return-void

    :sswitch_data_10
    .sparse-switch
        0x1 -> :sswitch_4
        0x2 -> :sswitch_8
        0x4 -> :sswitch_c
        0x63 -> :sswitch_f
    .end sparse-switch
.end method

.method public recebeConteudo(ILcom/itau/empresas/feature/login/model/PreLoginUsuarioVO;)V
    .registers 4
    .param p1, "modo"    # I
    .param p2, "usuarioVO"    # Lcom/itau/empresas/feature/login/model/PreLoginUsuarioVO;

    .line 246
    packed-switch p1, :pswitch_data_36

    goto :goto_30

    .line 248
    :pswitch_4
    iget-object v0, p0, Lcom/itau/empresas/feature/login/PreLoginActivity;->controller:Lcom/itau/empresas/feature/login/controller/PreLoginController;

    invoke-static {v0, p2, p0}, Lcom/itau/empresas/feature/login/strategy/PreLoginStrategyFactory;->documento(Lcom/itau/empresas/feature/login/controller/PreLoginController;Lcom/itau/empresas/feature/login/model/PreLoginUsuarioVO;Lcom/itau/empresas/feature/login/strategy/ListenerStrategy;)Lcom/itau/empresas/feature/login/strategy/PreLoginStrategy;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/login/PreLoginActivity;->strategyAtual:Lcom/itau/empresas/feature/login/strategy/PreLoginStrategy;

    .line 249
    goto :goto_30

    .line 251
    :pswitch_d
    iget-object v0, p0, Lcom/itau/empresas/feature/login/PreLoginActivity;->controller:Lcom/itau/empresas/feature/login/controller/PreLoginController;

    invoke-static {v0, p2, p0}, Lcom/itau/empresas/feature/login/strategy/PreLoginStrategyFactory;->documento(Lcom/itau/empresas/feature/login/controller/PreLoginController;Lcom/itau/empresas/feature/login/model/PreLoginUsuarioVO;Lcom/itau/empresas/feature/login/strategy/ListenerStrategy;)Lcom/itau/empresas/feature/login/strategy/PreLoginStrategy;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/login/PreLoginActivity;->strategyAtual:Lcom/itau/empresas/feature/login/strategy/PreLoginStrategy;

    .line 252
    goto :goto_30

    .line 254
    :pswitch_16
    iget-object v0, p0, Lcom/itau/empresas/feature/login/PreLoginActivity;->controller:Lcom/itau/empresas/feature/login/controller/PreLoginController;

    invoke-static {v0, p2, p0}, Lcom/itau/empresas/feature/login/strategy/PreLoginStrategyFactory;->operadorUnico(Lcom/itau/empresas/feature/login/controller/PreLoginController;Lcom/itau/empresas/feature/login/model/PreLoginUsuarioVO;Lcom/itau/empresas/feature/login/strategy/ListenerStrategy;)Lcom/itau/empresas/feature/login/strategy/PreLoginStrategy;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/login/PreLoginActivity;->strategyAtual:Lcom/itau/empresas/feature/login/strategy/PreLoginStrategy;

    .line 255
    goto :goto_30

    .line 257
    :pswitch_1f
    iget-object v0, p0, Lcom/itau/empresas/feature/login/PreLoginActivity;->controller:Lcom/itau/empresas/feature/login/controller/PreLoginController;

    invoke-static {v0, p2, p0}, Lcom/itau/empresas/feature/login/strategy/PreLoginStrategyFactory;->agenciaEConta(Lcom/itau/empresas/feature/login/controller/PreLoginController;Lcom/itau/empresas/feature/login/model/PreLoginUsuarioVO;Lcom/itau/empresas/feature/login/strategy/ListenerStrategy;)Lcom/itau/empresas/feature/login/strategy/PreLoginStrategy;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/login/PreLoginActivity;->strategyAtual:Lcom/itau/empresas/feature/login/strategy/PreLoginStrategy;

    .line 258
    goto :goto_30

    .line 260
    :pswitch_28
    iget-object v0, p0, Lcom/itau/empresas/feature/login/PreLoginActivity;->controller:Lcom/itau/empresas/feature/login/controller/PreLoginController;

    invoke-static {v0, p2, p0}, Lcom/itau/empresas/feature/login/strategy/PreLoginStrategyFactory;->operadorUnico(Lcom/itau/empresas/feature/login/controller/PreLoginController;Lcom/itau/empresas/feature/login/model/PreLoginUsuarioVO;Lcom/itau/empresas/feature/login/strategy/ListenerStrategy;)Lcom/itau/empresas/feature/login/strategy/PreLoginStrategy;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/login/PreLoginActivity;->strategyAtual:Lcom/itau/empresas/feature/login/strategy/PreLoginStrategy;

    .line 261
    .line 265
    :goto_30
    iget-object v0, p0, Lcom/itau/empresas/feature/login/PreLoginActivity;->strategyAtual:Lcom/itau/empresas/feature/login/strategy/PreLoginStrategy;

    invoke-interface {v0}, Lcom/itau/empresas/feature/login/strategy/PreLoginStrategy;->executar()V

    .line 266
    return-void

    :pswitch_data_36
    .packed-switch 0x0
        :pswitch_28
        :pswitch_1f
        :pswitch_4
        :pswitch_d
        :pswitch_16
    .end packed-switch
.end method

.method public sucesso(Lcom/itau/empresas/api/model/OperadorVO;)V
    .registers 4
    .param p1, "operadorVO"    # Lcom/itau/empresas/api/model/OperadorVO;

    .line 132
    new-instance v0, Lcom/itau/empresas/feature/login/PreLoginActivity$1;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/login/PreLoginActivity$1;-><init>(Lcom/itau/empresas/feature/login/PreLoginActivity;)V

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/login/PreLoginActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 138
    iget-object v0, p0, Lcom/itau/empresas/feature/login/PreLoginActivity;->application:Lcom/itau/empresas/CustomApplication;

    invoke-static {v0, p1}, Lcom/itau/empresas/feature/chaveacesso/ChaveAcessoUtil;->defineChaveAcesso(Lcom/itau/empresas/CustomApplication;Lcom/itau/empresas/api/model/OperadorVO;)V

    .line 140
    invoke-static {p0}, Lcom/itau/empresas/feature/login/LoginActivity;->intent(Landroid/content/Context;)Lcom/itau/empresas/feature/login/LoginActivity_$IntentBuilder_;

    move-result-object v0

    .line 141
    invoke-virtual {v0, p1}, Lcom/itau/empresas/feature/login/LoginActivity_$IntentBuilder_;->operador(Lcom/itau/empresas/api/model/OperadorVO;)Lcom/itau/empresas/feature/login/LoginActivity_$IntentBuilder_;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/login/PreLoginActivity;->senhaSalva:Landroid/support/v7/widget/SwitchCompat;

    .line 142
    invoke-virtual {v1}, Landroid/support/v7/widget/SwitchCompat;->isChecked()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/login/LoginActivity_$IntentBuilder_;->lembrarAcesso(Z)Lcom/itau/empresas/feature/login/LoginActivity_$IntentBuilder_;

    move-result-object v0

    iget-boolean v1, p0, Lcom/itau/empresas/feature/login/PreLoginActivity;->abrirTeclado:Z

    .line 143
    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/login/LoginActivity_$IntentBuilder_;->iniciaExibindoTeclado(Z)Lcom/itau/empresas/feature/login/LoginActivity_$IntentBuilder_;

    move-result-object v0

    iget-boolean v1, p0, Lcom/itau/empresas/feature/login/PreLoginActivity;->instalarIToken:Z

    .line 144
    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/login/LoginActivity_$IntentBuilder_;->instalarIToken(Z)Lcom/itau/empresas/feature/login/LoginActivity_$IntentBuilder_;

    move-result-object v0

    .line 145
    invoke-virtual {v0}, Lcom/itau/empresas/feature/login/LoginActivity_$IntentBuilder_;->start()Lorg/androidannotations/api/builder/PostActivityStarter;

    .line 146
    invoke-virtual {p0}, Lcom/itau/empresas/feature/login/PreLoginActivity;->finish()V

    .line 147
    return-void
.end method

.class Lbr/com/itau/widgets/material/MaterialEditText$1;
.super Ljava/lang/Object;
.source "MaterialEditText.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lbr/com/itau/widgets/material/MaterialEditText;->initTextWatcher()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lbr/com/itau/widgets/material/MaterialEditText;


# direct methods
.method constructor <init>(Lbr/com/itau/widgets/material/MaterialEditText;)V
    .registers 2

    .line 524
    iput-object p1, p0, Lbr/com/itau/widgets/material/MaterialEditText$1;->this$0:Lbr/com/itau/widgets/material/MaterialEditText;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .registers 4
    .param p1, "s"    # Landroid/text/Editable;

    .line 535
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialEditText$1;->this$0:Lbr/com/itau/widgets/material/MaterialEditText;

    # invokes: Lbr/com/itau/widgets/material/MaterialEditText;->checkCharactersCount()V
    invoke-static {v0}, Lbr/com/itau/widgets/material/MaterialEditText;->access$000(Lbr/com/itau/widgets/material/MaterialEditText;)V

    .line 536
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialEditText$1;->this$0:Lbr/com/itau/widgets/material/MaterialEditText;

    # getter for: Lbr/com/itau/widgets/material/MaterialEditText;->autoValidate:Z
    invoke-static {v0}, Lbr/com/itau/widgets/material/MaterialEditText;->access$100(Lbr/com/itau/widgets/material/MaterialEditText;)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 537
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialEditText$1;->this$0:Lbr/com/itau/widgets/material/MaterialEditText;

    invoke-virtual {v0}, Lbr/com/itau/widgets/material/MaterialEditText;->validate()Z

    goto :goto_19

    .line 539
    :cond_13
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialEditText$1;->this$0:Lbr/com/itau/widgets/material/MaterialEditText;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lbr/com/itau/widgets/material/MaterialEditText;->setError(Ljava/lang/CharSequence;)V

    .line 541
    :goto_19
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialEditText$1;->this$0:Lbr/com/itau/widgets/material/MaterialEditText;

    invoke-virtual {v0}, Lbr/com/itau/widgets/material/MaterialEditText;->postInvalidate()V

    .line 542
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .registers 5
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "count"    # I
    .param p4, "after"    # I

    .line 527
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .registers 5
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "before"    # I
    .param p4, "count"    # I

    .line 531
    return-void
.end method

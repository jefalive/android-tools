.class public Lbr/com/itau/sdk/android/core/model/RequestDTO;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private body:Ljava/lang/Object;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "body"
    .end annotation
.end field

.field private final header:Ljava/util/List;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "header"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Lbr/com/itau/sdk/android/core/model/KeyValue;>;"
        }
    .end annotation
.end field

.field private method:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "method"
    .end annotation
.end field

.field private final path:Ljava/util/List;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "path"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Lbr/com/itau/sdk/android/core/model/KeyValue;>;"
        }
    .end annotation
.end field

.field private final query:Ljava/util/List;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "query"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Lbr/com/itau/sdk/android/core/model/KeyValue;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 2

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lbr/com/itau/sdk/android/core/model/RequestDTO;->query:Ljava/util/List;

    .line 15
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lbr/com/itau/sdk/android/core/model/RequestDTO;->path:Ljava/util/List;

    .line 17
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lbr/com/itau/sdk/android/core/model/RequestDTO;->header:Ljava/util/List;

    .line 25
    return-void
.end method

.method public constructor <init>(Lbr/com/itau/sdk/android/core/endpoint/http/CallMethod;)V
    .registers 3

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lbr/com/itau/sdk/android/core/model/RequestDTO;->query:Ljava/util/List;

    .line 15
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lbr/com/itau/sdk/android/core/model/RequestDTO;->path:Ljava/util/List;

    .line 17
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lbr/com/itau/sdk/android/core/model/RequestDTO;->header:Ljava/util/List;

    .line 28
    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/endpoint/http/CallMethod;->name()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/sdk/android/core/model/RequestDTO;->method:Ljava/lang/String;

    .line 29
    return-void
.end method


# virtual methods
.method protected addAllHeader(Ljava/util/List;)V
    .registers 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<Lbr/com/itau/sdk/android/core/model/KeyValue;>;)V"
        }
    .end annotation

    .line 83
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/model/RequestDTO;->header:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 84
    return-void
.end method

.method protected addAllPath(Ljava/util/List;)V
    .registers 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<Lbr/com/itau/sdk/android/core/model/KeyValue;>;)V"
        }
    .end annotation

    .line 79
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/model/RequestDTO;->path:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 80
    return-void
.end method

.method protected addAllQuery(Ljava/util/List;)V
    .registers 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<Lbr/com/itau/sdk/android/core/model/KeyValue;>;)V"
        }
    .end annotation

    .line 75
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/model/RequestDTO;->query:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 76
    return-void
.end method

.method public addHeader(Ljava/lang/String;Ljava/lang/String;)Lbr/com/itau/sdk/android/core/model/RequestDTO;
    .registers 5

    .line 42
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/model/RequestDTO;->header:Ljava/util/List;

    new-instance v1, Lbr/com/itau/sdk/android/core/model/KeyValue;

    invoke-direct {v1, p1, p2}, Lbr/com/itau/sdk/android/core/model/KeyValue;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 43
    return-object p0
.end method

.method public addPath(Ljava/lang/String;Ljava/lang/String;)Lbr/com/itau/sdk/android/core/model/RequestDTO;
    .registers 5

    .line 32
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/model/RequestDTO;->path:Ljava/util/List;

    new-instance v1, Lbr/com/itau/sdk/android/core/model/KeyValue;

    invoke-direct {v1, p1, p2}, Lbr/com/itau/sdk/android/core/model/KeyValue;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 33
    return-object p0
.end method

.method public addQuery(Ljava/lang/String;Ljava/lang/String;)Lbr/com/itau/sdk/android/core/model/RequestDTO;
    .registers 5

    .line 37
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/model/RequestDTO;->query:Ljava/util/List;

    new-instance v1, Lbr/com/itau/sdk/android/core/model/KeyValue;

    invoke-direct {v1, p1, p2}, Lbr/com/itau/sdk/android/core/model/KeyValue;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 38
    return-object p0
.end method

.method public getBody()Ljava/lang/Object;
    .registers 2

    .line 67
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/model/RequestDTO;->body:Ljava/lang/Object;

    return-object v0
.end method

.method public getHeader()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Lbr/com/itau/sdk/android/core/model/KeyValue;>;"
        }
    .end annotation

    .line 55
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/model/RequestDTO;->header:Ljava/util/List;

    return-object v0
.end method

.method public getMethod()Ljava/lang/String;
    .registers 2

    .line 59
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/model/RequestDTO;->method:Ljava/lang/String;

    return-object v0
.end method

.method public getPath()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Lbr/com/itau/sdk/android/core/model/KeyValue;>;"
        }
    .end annotation

    .line 51
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/model/RequestDTO;->path:Ljava/util/List;

    return-object v0
.end method

.method public getQuery()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Lbr/com/itau/sdk/android/core/model/KeyValue;>;"
        }
    .end annotation

    .line 47
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/model/RequestDTO;->query:Ljava/util/List;

    return-object v0
.end method

.method public setBody(Ljava/lang/Object;)V
    .registers 2

    .line 71
    iput-object p1, p0, Lbr/com/itau/sdk/android/core/model/RequestDTO;->body:Ljava/lang/Object;

    .line 72
    return-void
.end method

.method protected setMethod(Ljava/lang/String;)V
    .registers 2

    .line 63
    iput-object p1, p0, Lbr/com/itau/sdk/android/core/model/RequestDTO;->method:Ljava/lang/String;

    .line 64
    return-void
.end method

.class public Lcom/itau/empresas/CustomApplication;
.super Landroid/support/multidex/MultiDexApplication;
.source "CustomApplication.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/itau/empresas/CustomApplication$ContaNulaSelecionada;,
        Lcom/itau/empresas/CustomApplication$Exibicao;
    }
.end annotation


# static fields
.field private static selfInstance:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<Lcom/itau/empresas/CustomApplication;>;"
        }
    .end annotation
.end field


# instance fields
.field private chaveAcesso:I

.field private codigoOperador:Ljava/lang/String;

.field private contaSelecionada:Lcom/itau/empresas/api/model/ContaOperadorVO;

.field private contextoAtendimento:Ljava/lang/String;

.field private dadosOperador:Lcom/itau/empresas/api/model/DadosOperadorVO;

.field private eventoTracker:Lcom/itau/empresas/ui/util/analytics/TrackerEvento;

.field gerenciadorSessaoUsuario:Lcom/itau/empresas/GerenciadorSessaoUsuario;

.field private lastActivityListener:Lcom/itau/empresas/LastActivityListener;

.field private menuVO:Lcom/itau/empresas/api/model/MenuVO;

.field private operador:Lcom/itau/empresas/api/model/OperadorVO;

.field private pendenciasVO:Lcom/itau/empresas/feature/pendencias/ListaPendenciasVO;

.field private perfilLogado:Lcom/itau/empresas/api/model/PerfilOperadorVO;

.field private permissaoAutorizante:Lcom/itau/empresas/api/model/MenuVO;

.field private sharedPreferences:Landroid/content/SharedPreferences;


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 49
    invoke-direct {p0}, Landroid/support/multidex/MultiDexApplication;-><init>()V

    return-void
.end method

.method public static getSelfInstance()Lcom/itau/empresas/CustomApplication;
    .registers 1

    .line 318
    sget-object v0, Lcom/itau/empresas/CustomApplication;->selfInstance:Ljava/lang/ref/WeakReference;

    if-nez v0, :cond_6

    .line 319
    const/4 v0, 0x0

    return-object v0

    .line 320
    :cond_6
    sget-object v0, Lcom/itau/empresas/CustomApplication;->selfInstance:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/CustomApplication;

    return-object v0
.end method

.method public static setSelfInstance(Lcom/itau/empresas/CustomApplication;)V
    .registers 2
    .param p0, "selfInstance"    # Lcom/itau/empresas/CustomApplication;

    .line 154
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    sput-object v0, Lcom/itau/empresas/CustomApplication;->selfInstance:Ljava/lang/ref/WeakReference;

    .line 155
    return-void
.end method


# virtual methods
.method public getChaveAcesso()I
    .registers 2

    .line 309
    iget v0, p0, Lcom/itau/empresas/CustomApplication;->chaveAcesso:I

    return v0
.end method

.method public getCodigoOperador()Ljava/lang/String;
    .registers 3

    .line 218
    iget-object v0, p0, Lcom/itau/empresas/CustomApplication;->codigoOperador:Ljava/lang/String;

    if-nez v0, :cond_e

    .line 219
    iget-object v0, p0, Lcom/itau/empresas/CustomApplication;->gerenciadorSessaoUsuario:Lcom/itau/empresas/GerenciadorSessaoUsuario;

    const-string v1, "codigoOperador"

    invoke-virtual {v0, v1}, Lcom/itau/empresas/GerenciadorSessaoUsuario;->buscaPreferencia(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/CustomApplication;->codigoOperador:Ljava/lang/String;

    .line 222
    :cond_e
    iget-object v0, p0, Lcom/itau/empresas/CustomApplication;->codigoOperador:Ljava/lang/String;

    return-object v0
.end method

.method public getContaSelecionada()Lcom/itau/empresas/api/model/ContaOperadorVO;
    .registers 4

    .line 200
    iget-object v0, p0, Lcom/itau/empresas/CustomApplication;->contaSelecionada:Lcom/itau/empresas/api/model/ContaOperadorVO;

    if-nez v0, :cond_12

    .line 201
    iget-object v0, p0, Lcom/itau/empresas/CustomApplication;->gerenciadorSessaoUsuario:Lcom/itau/empresas/GerenciadorSessaoUsuario;

    const-string v1, "conta_selecionada"

    const-class v2, Lcom/itau/empresas/api/model/ContaOperadorVO;

    .line 202
    invoke-virtual {v0, v1, v2}, Lcom/itau/empresas/GerenciadorSessaoUsuario;->buscaPreferencia(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/api/model/ContaOperadorVO;

    iput-object v0, p0, Lcom/itau/empresas/CustomApplication;->contaSelecionada:Lcom/itau/empresas/api/model/ContaOperadorVO;

    .line 204
    :cond_12
    iget-object v0, p0, Lcom/itau/empresas/CustomApplication;->contaSelecionada:Lcom/itau/empresas/api/model/ContaOperadorVO;

    if-nez v0, :cond_27

    .line 205
    new-instance v0, Lcom/itau/empresas/CustomApplication$ContaNulaSelecionada;

    invoke-direct {v0}, Lcom/itau/empresas/CustomApplication$ContaNulaSelecionada;-><init>()V

    invoke-static {v0}, Lcom/crashlytics/android/Crashlytics;->logException(Ljava/lang/Throwable;)V

    .line 206
    iget-object v0, p0, Lcom/itau/empresas/CustomApplication;->lastActivityListener:Lcom/itau/empresas/LastActivityListener;

    invoke-virtual {v0}, Lcom/itau/empresas/LastActivityListener;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0, p0}, Lcom/itau/empresas/feature/logout/LogoutHelper;->trataSessaoExpirada(Landroid/app/Activity;Lcom/itau/empresas/CustomApplication;)V

    .line 208
    :cond_27
    iget-object v0, p0, Lcom/itau/empresas/CustomApplication;->contaSelecionada:Lcom/itau/empresas/api/model/ContaOperadorVO;

    return-object v0
.end method

.method public getContextoAtendimento()Ljava/lang/String;
    .registers 3

    .line 185
    iget-object v0, p0, Lcom/itau/empresas/CustomApplication;->contextoAtendimento:Ljava/lang/String;

    if-nez v0, :cond_e

    .line 186
    iget-object v0, p0, Lcom/itau/empresas/CustomApplication;->gerenciadorSessaoUsuario:Lcom/itau/empresas/GerenciadorSessaoUsuario;

    const-string v1, "menu"

    .line 187
    invoke-virtual {v0, v1}, Lcom/itau/empresas/GerenciadorSessaoUsuario;->buscaPreferencia(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/CustomApplication;->contextoAtendimento:Ljava/lang/String;

    .line 189
    :cond_e
    iget-object v0, p0, Lcom/itau/empresas/CustomApplication;->contextoAtendimento:Ljava/lang/String;

    return-object v0
.end method

.method public getDadosOperador()Lcom/itau/empresas/api/model/DadosOperadorVO;
    .registers 4

    .line 231
    iget-object v0, p0, Lcom/itau/empresas/CustomApplication;->dadosOperador:Lcom/itau/empresas/api/model/DadosOperadorVO;

    if-nez v0, :cond_12

    .line 232
    iget-object v0, p0, Lcom/itau/empresas/CustomApplication;->gerenciadorSessaoUsuario:Lcom/itau/empresas/GerenciadorSessaoUsuario;

    const-string v1, "dadosOperador"

    const-class v2, Lcom/itau/empresas/api/model/DadosOperadorVO;

    .line 233
    invoke-virtual {v0, v1, v2}, Lcom/itau/empresas/GerenciadorSessaoUsuario;->buscaPreferencia(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/api/model/DadosOperadorVO;

    iput-object v0, p0, Lcom/itau/empresas/CustomApplication;->dadosOperador:Lcom/itau/empresas/api/model/DadosOperadorVO;

    .line 235
    :cond_12
    const-string v0, "CustomApplication"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getDadosOperador() returned: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/itau/empresas/CustomApplication;->dadosOperador:Lcom/itau/empresas/api/model/DadosOperadorVO;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 236
    iget-object v0, p0, Lcom/itau/empresas/CustomApplication;->dadosOperador:Lcom/itau/empresas/api/model/DadosOperadorVO;

    return-object v0
.end method

.method public getEventoTracker()Lcom/itau/empresas/ui/util/analytics/TrackerEvento;
    .registers 2

    .line 257
    iget-object v0, p0, Lcom/itau/empresas/CustomApplication;->eventoTracker:Lcom/itau/empresas/ui/util/analytics/TrackerEvento;

    return-object v0
.end method

.method public getExibicao(Lcom/itau/empresas/CustomApplication$Exibicao;)Lcom/itau/empresas/api/model/MenuVO;
    .registers 5
    .param p1, "perfilAutorizante"    # Lcom/itau/empresas/CustomApplication$Exibicao;

    .line 277
    invoke-static {}, Lcom/itau/empresas/ui/util/menu/MenuBuscaHelper;->getInstance()Lcom/itau/empresas/ui/util/menu/MenuBuscaHelper;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/CustomApplication;->permissaoAutorizante:Lcom/itau/empresas/api/model/MenuVO;

    .line 278
    invoke-virtual {p1}, Lcom/itau/empresas/CustomApplication$Exibicao;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/itau/empresas/ui/util/menu/MenuBuscaHelper;->buscaPorNome(Lcom/itau/empresas/api/model/MenuVO;Ljava/lang/String;)Lcom/itau/empresas/api/model/MenuVO;

    move-result-object v0

    .line 277
    return-object v0
.end method

.method public getMenuVO()Lcom/itau/empresas/api/model/MenuVO;
    .registers 2

    .line 261
    iget-object v0, p0, Lcom/itau/empresas/CustomApplication;->menuVO:Lcom/itau/empresas/api/model/MenuVO;

    return-object v0
.end method

.method public getOperador()Lcom/itau/empresas/api/model/OperadorVO;
    .registers 4

    .line 77
    iget-object v0, p0, Lcom/itau/empresas/CustomApplication;->operador:Lcom/itau/empresas/api/model/OperadorVO;

    if-nez v0, :cond_d

    .line 78
    iget-object v0, p0, Lcom/itau/empresas/CustomApplication;->gerenciadorSessaoUsuario:Lcom/itau/empresas/GerenciadorSessaoUsuario;

    const-string v1, "operador"

    const-class v2, Lcom/itau/empresas/api/model/OperadorVO;

    invoke-virtual {v0, v1, v2}, Lcom/itau/empresas/GerenciadorSessaoUsuario;->buscaPreferencia(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    .line 80
    :cond_d
    iget-object v0, p0, Lcom/itau/empresas/CustomApplication;->operador:Lcom/itau/empresas/api/model/OperadorVO;

    return-object v0
.end method

.method public getPendenciasVO()Lcom/itau/empresas/feature/pendencias/ListaPendenciasVO;
    .registers 4

    .line 296
    iget-object v0, p0, Lcom/itau/empresas/CustomApplication;->pendenciasVO:Lcom/itau/empresas/feature/pendencias/ListaPendenciasVO;

    if-nez v0, :cond_12

    .line 297
    iget-object v0, p0, Lcom/itau/empresas/CustomApplication;->gerenciadorSessaoUsuario:Lcom/itau/empresas/GerenciadorSessaoUsuario;

    const-string v1, "pendencia"

    const-class v2, Lcom/itau/empresas/feature/pendencias/ListaPendenciasVO;

    .line 298
    invoke-virtual {v0, v1, v2}, Lcom/itau/empresas/GerenciadorSessaoUsuario;->buscaPreferencia(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/feature/pendencias/ListaPendenciasVO;

    iput-object v0, p0, Lcom/itau/empresas/CustomApplication;->pendenciasVO:Lcom/itau/empresas/feature/pendencias/ListaPendenciasVO;

    .line 300
    :cond_12
    iget-object v0, p0, Lcom/itau/empresas/CustomApplication;->pendenciasVO:Lcom/itau/empresas/feature/pendencias/ListaPendenciasVO;

    return-object v0
.end method

.method public getPerfilLogado()Lcom/itau/empresas/api/model/PerfilOperadorVO;
    .registers 4

    .line 172
    iget-object v0, p0, Lcom/itau/empresas/CustomApplication;->perfilLogado:Lcom/itau/empresas/api/model/PerfilOperadorVO;

    if-nez v0, :cond_12

    .line 173
    iget-object v0, p0, Lcom/itau/empresas/CustomApplication;->gerenciadorSessaoUsuario:Lcom/itau/empresas/GerenciadorSessaoUsuario;

    const-string v1, "perfilLogado"

    const-class v2, Lcom/itau/empresas/api/model/PerfilOperadorVO;

    .line 174
    invoke-virtual {v0, v1, v2}, Lcom/itau/empresas/GerenciadorSessaoUsuario;->buscaPreferencia(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/api/model/PerfilOperadorVO;

    iput-object v0, p0, Lcom/itau/empresas/CustomApplication;->perfilLogado:Lcom/itau/empresas/api/model/PerfilOperadorVO;

    .line 176
    :cond_12
    iget-object v0, p0, Lcom/itau/empresas/CustomApplication;->perfilLogado:Lcom/itau/empresas/api/model/PerfilOperadorVO;

    return-object v0
.end method

.method public getPermissaoAutorizante()Lcom/itau/empresas/api/model/MenuVO;
    .registers 2

    .line 269
    iget-object v0, p0, Lcom/itau/empresas/CustomApplication;->permissaoAutorizante:Lcom/itau/empresas/api/model/MenuVO;

    return-object v0
.end method

.method public getSharedPreferences()Landroid/content/SharedPreferences;
    .registers 3

    .line 245
    iget-object v0, p0, Lcom/itau/empresas/CustomApplication;->sharedPreferences:Landroid/content/SharedPreferences;

    if-nez v0, :cond_e

    .line 246
    new-instance v0, Lbr/com/itau/security/securestorage/SecureStorage;

    const-string v1, "PREMP"

    invoke-direct {v0, p0, v1}, Lbr/com/itau/security/securestorage/SecureStorage;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/itau/empresas/CustomApplication;->setSharedPreferences(Landroid/content/SharedPreferences;)V

    .line 248
    :cond_e
    iget-object v0, p0, Lcom/itau/empresas/CustomApplication;->sharedPreferences:Landroid/content/SharedPreferences;

    return-object v0
.end method

.method public limpaCachePendencias()V
    .registers 3

    .line 291
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/itau/empresas/CustomApplication;->pendenciasVO:Lcom/itau/empresas/feature/pendencias/ListaPendenciasVO;

    .line 292
    iget-object v0, p0, Lcom/itau/empresas/CustomApplication;->gerenciadorSessaoUsuario:Lcom/itau/empresas/GerenciadorSessaoUsuario;

    const-string v1, "pendencia"

    invoke-virtual {v0, v1}, Lcom/itau/empresas/GerenciadorSessaoUsuario;->removerPreferencia(Ljava/lang/String;)V

    .line 293
    return-void
.end method

.method public limparCache()V
    .registers 2

    .line 161
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/itau/empresas/CustomApplication;->perfilLogado:Lcom/itau/empresas/api/model/PerfilOperadorVO;

    .line 162
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/itau/empresas/CustomApplication;->contaSelecionada:Lcom/itau/empresas/api/model/ContaOperadorVO;

    .line 163
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/itau/empresas/CustomApplication;->dadosOperador:Lcom/itau/empresas/api/model/DadosOperadorVO;

    .line 164
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/itau/empresas/CustomApplication;->codigoOperador:Ljava/lang/String;

    .line 165
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/itau/empresas/CustomApplication;->menuVO:Lcom/itau/empresas/api/model/MenuVO;

    .line 166
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/itau/empresas/CustomApplication;->operador:Lcom/itau/empresas/api/model/OperadorVO;

    .line 167
    iget-object v0, p0, Lcom/itau/empresas/CustomApplication;->gerenciadorSessaoUsuario:Lcom/itau/empresas/GerenciadorSessaoUsuario;

    invoke-virtual {v0}, Lcom/itau/empresas/GerenciadorSessaoUsuario;->limpaPreferencia()V

    .line 168
    return-void
.end method

.method public onCreate()V
    .registers 6

    .line 95
    invoke-super {p0}, Landroid/support/multidex/MultiDexApplication;->onCreate()V

    .line 96
    invoke-static {p0}, Lcom/jakewharton/threetenabp/AndroidThreeTen;->init(Landroid/app/Application;)V

    .line 97
    invoke-static {p0}, Lcom/itau/empresas/CustomApplication;->setSelfInstance(Lcom/itau/empresas/CustomApplication;)V

    .line 98
    new-instance v0, Lcom/itau/empresas/ui/util/analytics/TrackerEvento;

    invoke-direct {v0}, Lcom/itau/empresas/ui/util/analytics/TrackerEvento;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/CustomApplication;->eventoTracker:Lcom/itau/empresas/ui/util/analytics/TrackerEvento;

    .line 107
    const/4 v0, 0x1

    new-array v0, v0, [Lio/fabric/sdk/android/Kit;

    new-instance v1, Lcom/crashlytics/android/Crashlytics;

    invoke-direct {v1}, Lcom/crashlytics/android/Crashlytics;-><init>()V

    const/4 v2, 0x0

    aput-object v1, v0, v2

    invoke-static {p0, v0}, Lio/fabric/sdk/android/Fabric;->with(Landroid/content/Context;[Lio/fabric/sdk/android/Kit;)Lio/fabric/sdk/android/Fabric;

    .line 108
    invoke-static {p0}, Lcom/google/android/gms/analytics/GoogleAnalytics;->getInstance(Landroid/content/Context;)Lcom/google/android/gms/analytics/GoogleAnalytics;

    move-result-object v3

    .line 109
    .local v3, "analytics":Lcom/google/android/gms/analytics/GoogleAnalytics;
    invoke-static {p0}, Lcom/facebook/FacebookSdk;->sdkInitialize(Landroid/content/Context;)V

    .line 110
    invoke-static {p0}, Lcom/facebook/appevents/AppEventsLogger;->activateApp(Landroid/app/Application;)V

    .line 113
    :try_start_28
    invoke-virtual {p0}, Lcom/itau/empresas/CustomApplication;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    const-string v1, "ADBMobileConfig.json"

    invoke-virtual {v0, v1}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v4

    .line 114
    .local v4, "configInput":Ljava/io/InputStream;
    invoke-static {v4}, Lcom/adobe/mobile/Config;->overrideConfigStream(Ljava/io/InputStream;)V

    .line 115
    invoke-virtual {p0}, Lcom/itau/empresas/CustomApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/adobe/mobile/Config;->setContext(Landroid/content/Context;)V
    :try_end_3c
    .catch Ljava/io/IOException; {:try_start_28 .. :try_end_3c} :catch_3d

    .line 118
    .end local v4    # "configInput":Ljava/io/InputStream;
    goto :goto_47

    .line 116
    :catch_3d
    move-exception v4

    .line 117
    .local v4, "e":Ljava/io/IOException;
    const-string v0, "adobeAnalytics"

    invoke-virtual {v4}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v4}, Lbr/com/itau/textovoz/util/Logger;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 120
    .end local v4    # "e":Ljava/io/IOException;
    :goto_47
    const v0, 0x7f050007

    invoke-virtual {v3, v0}, Lcom/google/android/gms/analytics/GoogleAnalytics;->newTracker(I)Lcom/google/android/gms/analytics/Tracker;

    move-result-object v4

    .line 122
    .local v4, "tracker":Lcom/google/android/gms/analytics/Tracker;
    iget-object v0, p0, Lcom/itau/empresas/CustomApplication;->eventoTracker:Lcom/itau/empresas/ui/util/analytics/TrackerEvento;

    new-instance v1, Lcom/itau/empresas/ui/util/analytics/GoogleAnalyticsObserver;

    invoke-direct {v1, v4}, Lcom/itau/empresas/ui/util/analytics/GoogleAnalyticsObserver;-><init>(Lcom/google/android/gms/analytics/Tracker;)V

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/analytics/TrackerEvento;->addObserver(Ljava/util/Observer;)V

    .line 123
    iget-object v0, p0, Lcom/itau/empresas/CustomApplication;->eventoTracker:Lcom/itau/empresas/ui/util/analytics/TrackerEvento;

    new-instance v1, Lcom/itau/empresas/ui/util/analytics/FacebookObserver;

    invoke-static {p0}, Lcom/facebook/appevents/AppEventsLogger;->newLogger(Landroid/content/Context;)Lcom/facebook/appevents/AppEventsLogger;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/itau/empresas/ui/util/analytics/FacebookObserver;-><init>(Lcom/facebook/appevents/AppEventsLogger;)V

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/analytics/TrackerEvento;->addObserver(Ljava/util/Observer;)V

    .line 128
    .end local v3    # "analytics":Lcom/google/android/gms/analytics/GoogleAnalytics;
    .end local v4    # "tracker":Lcom/google/android/gms/analytics/Tracker;
    invoke-static {}, Lde/greenrobot/event/EventBus;->builder()Lde/greenrobot/event/EventBusBuilder;

    move-result-object v0

    .line 129
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lde/greenrobot/event/EventBusBuilder;->throwSubscriberException(Z)Lde/greenrobot/event/EventBusBuilder;

    move-result-object v0

    .line 130
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lde/greenrobot/event/EventBusBuilder;->sendNoSubscriberEvent(Z)Lde/greenrobot/event/EventBusBuilder;

    move-result-object v0

    .line 131
    invoke-virtual {v0}, Lde/greenrobot/event/EventBusBuilder;->installDefaultEventBus()Lde/greenrobot/event/EventBus;

    .line 133
    new-instance v0, Lbr/com/itau/sdk/android/core/BackendClient$Builder;

    invoke-direct {v0, p0}, Lbr/com/itau/sdk/android/core/BackendClient$Builder;-><init>(Landroid/content/Context;)V

    const-string v1, "mob.ilic"

    .line 134
    invoke-virtual {v0, v1}, Lbr/com/itau/sdk/android/core/BackendClient$Builder;->withLicensePathFromAssets(Ljava/lang/String;)Lbr/com/itau/sdk/android/core/BackendClient$Builder;

    move-result-object v0

    const-string v1, "f187e10db35f7c7d"

    .line 135
    invoke-virtual {v0, v1}, Lbr/com/itau/sdk/android/core/BackendClient$Builder;->withActivationKey(Ljava/lang/String;)Lbr/com/itau/sdk/android/core/BackendClient$Builder;

    move-result-object v0

    .line 136
    const-wide/16 v1, 0x258

    invoke-virtual {v0, v1, v2}, Lbr/com/itau/sdk/android/core/BackendClient$Builder;->withTimeout(J)Lbr/com/itau/sdk/android/core/BackendClient$Builder;

    move-result-object v0

    .line 137
    invoke-virtual {v0}, Lbr/com/itau/sdk/android/core/BackendClient$Builder;->extraConfiguration()Lbr/com/itau/sdk/android/core/ExtraConfiguration;

    move-result-object v0

    const-string v1, "https://piloto.itau/router-app/router/mobile"

    .line 138
    invoke-virtual {v0, v1}, Lbr/com/itau/sdk/android/core/ExtraConfiguration;->withRouterUrl(Ljava/lang/String;)Lbr/com/itau/sdk/android/core/ExtraConfiguration;

    move-result-object v0

    const-string v1, "https://ww70.itau.com.br/pcms/middleware.svc/"

    .line 139
    invoke-virtual {v0, v1}, Lbr/com/itau/sdk/android/core/ExtraConfiguration;->withMiddlewareUrl(Ljava/lang/String;)Lbr/com/itau/sdk/android/core/ExtraConfiguration;

    move-result-object v0

    const-string v1, "https://bankline.itau.com.br/vsnet/wap2/"

    .line 140
    invoke-virtual {v0, v1}, Lbr/com/itau/sdk/android/core/ExtraConfiguration;->withUsabilidadeUrl(Ljava/lang/String;)Lbr/com/itau/sdk/android/core/ExtraConfiguration;

    move-result-object v0

    const-string v1, "mobile.asp"

    .line 141
    invoke-virtual {v0, v1}, Lbr/com/itau/sdk/android/core/ExtraConfiguration;->withUsabilidadePage(Ljava/lang/String;)Lbr/com/itau/sdk/android/core/ExtraConfiguration;

    move-result-object v0

    .line 142
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lbr/com/itau/sdk/android/core/ExtraConfiguration;->withUseCryptoPayload(Z)Lbr/com/itau/sdk/android/core/ExtraConfiguration;

    move-result-object v0

    sget-object v1, Lbr/com/itau/sdk/android/core/RouterEnvironment;->MOBILE:Lbr/com/itau/sdk/android/core/RouterEnvironment;

    .line 143
    invoke-virtual {v0, v1}, Lbr/com/itau/sdk/android/core/ExtraConfiguration;->withRouterUrlForRelease(Lbr/com/itau/sdk/android/core/RouterEnvironment;)Lbr/com/itau/sdk/android/core/ExtraConfiguration;

    move-result-object v0

    const-string v1, "AppItauEmpresas"

    .line 144
    invoke-virtual {v0, v1}, Lbr/com/itau/sdk/android/core/ExtraConfiguration;->withUserAgent(Ljava/lang/String;)Lbr/com/itau/sdk/android/core/ExtraConfiguration;

    move-result-object v0

    .line 145
    invoke-virtual {v0}, Lbr/com/itau/sdk/android/core/ExtraConfiguration;->builder()Lbr/com/itau/sdk/android/core/BackendClient$Builder;

    move-result-object v0

    .line 146
    invoke-virtual {v0}, Lbr/com/itau/sdk/android/core/BackendClient$Builder;->init()Lbr/com/itau/sdk/android/core/BackendClient;

    move-result-object v0

    new-instance v1, Lcom/itau/empresas/ui/util/analytics/TokenAnalytics;

    iget-object v2, p0, Lcom/itau/empresas/CustomApplication;->eventoTracker:Lcom/itau/empresas/ui/util/analytics/TrackerEvento;

    invoke-direct {v1, p0, v2}, Lcom/itau/empresas/ui/util/analytics/TokenAnalytics;-><init>(Lcom/itau/empresas/CustomApplication;Lcom/itau/empresas/ui/util/analytics/TrackerEvento;)V

    .line 147
    invoke-virtual {v0, v1}, Lbr/com/itau/sdk/android/core/BackendClient;->setTracker(Lbr/com/itau/sdk/android/core/SDKTracker;)V

    .line 149
    invoke-static {p0}, Lcom/itau/empresas/LogoutActivityListener_;->getInstance_(Landroid/content/Context;)Lcom/itau/empresas/LogoutActivityListener_;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/itau/empresas/CustomApplication;->registerActivityLifecycleCallbacks(Landroid/app/Application$ActivityLifecycleCallbacks;)V

    .line 150
    new-instance v0, Lcom/itau/empresas/LastActivityListener;

    invoke-direct {v0}, Lcom/itau/empresas/LastActivityListener;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/CustomApplication;->lastActivityListener:Lcom/itau/empresas/LastActivityListener;

    invoke-virtual {p0, v0}, Lcom/itau/empresas/CustomApplication;->registerActivityLifecycleCallbacks(Landroid/app/Application$ActivityLifecycleCallbacks;)V

    .line 151
    return-void
.end method

.method public setChaveAcesso(I)V
    .registers 2
    .param p1, "chaveAcesso"    # I

    .line 313
    iput p1, p0, Lcom/itau/empresas/CustomApplication;->chaveAcesso:I

    .line 314
    return-void
.end method

.method public setCodigoOperador(Ljava/lang/String;)V
    .registers 4
    .param p1, "codigoOperador"    # Ljava/lang/String;

    .line 226
    iget-object v0, p0, Lcom/itau/empresas/CustomApplication;->gerenciadorSessaoUsuario:Lcom/itau/empresas/GerenciadorSessaoUsuario;

    const-string v1, "codigoOperador"

    invoke-virtual {v0, v1, p1}, Lcom/itau/empresas/GerenciadorSessaoUsuario;->salvaPreferencia(Ljava/lang/String;Ljava/lang/String;)V

    .line 227
    iput-object p1, p0, Lcom/itau/empresas/CustomApplication;->codigoOperador:Ljava/lang/String;

    .line 228
    return-void
.end method

.method public setContaSelecionada(Lcom/itau/empresas/api/model/ContaOperadorVO;)V
    .registers 4
    .param p1, "contaSelecionada"    # Lcom/itau/empresas/api/model/ContaOperadorVO;

    .line 212
    iget-object v0, p0, Lcom/itau/empresas/CustomApplication;->gerenciadorSessaoUsuario:Lcom/itau/empresas/GerenciadorSessaoUsuario;

    const-string v1, "conta_selecionada"

    invoke-virtual {v0, v1, p1}, Lcom/itau/empresas/GerenciadorSessaoUsuario;->salvaPreferencia(Ljava/lang/String;Ljava/lang/Object;)V

    .line 214
    iput-object p1, p0, Lcom/itau/empresas/CustomApplication;->contaSelecionada:Lcom/itau/empresas/api/model/ContaOperadorVO;

    .line 215
    return-void
.end method

.method public setContextoAtendimento(Ljava/lang/String;)Ljava/lang/String;
    .registers 4
    .param p1, "contextoAtendimento"    # Ljava/lang/String;

    .line 193
    iget-object v0, p0, Lcom/itau/empresas/CustomApplication;->gerenciadorSessaoUsuario:Lcom/itau/empresas/GerenciadorSessaoUsuario;

    const-string v1, "menu"

    invoke-virtual {v0, v1, p1}, Lcom/itau/empresas/GerenciadorSessaoUsuario;->salvaPreferencia(Ljava/lang/String;Ljava/lang/String;)V

    .line 195
    iput-object p1, p0, Lcom/itau/empresas/CustomApplication;->contextoAtendimento:Ljava/lang/String;

    return-object p1
.end method

.method public setDadosOperador(Lcom/itau/empresas/api/model/DadosOperadorVO;)V
    .registers 4
    .param p1, "dadosOperador"    # Lcom/itau/empresas/api/model/DadosOperadorVO;

    .line 240
    iget-object v0, p0, Lcom/itau/empresas/CustomApplication;->gerenciadorSessaoUsuario:Lcom/itau/empresas/GerenciadorSessaoUsuario;

    const-string v1, "dadosOperador"

    invoke-virtual {v0, v1, p1}, Lcom/itau/empresas/GerenciadorSessaoUsuario;->salvaPreferencia(Ljava/lang/String;Ljava/lang/Object;)V

    .line 241
    iput-object p1, p0, Lcom/itau/empresas/CustomApplication;->dadosOperador:Lcom/itau/empresas/api/model/DadosOperadorVO;

    .line 242
    return-void
.end method

.method public setMenuVO(Lcom/itau/empresas/api/model/MenuVO;)V
    .registers 2
    .param p1, "menuVO"    # Lcom/itau/empresas/api/model/MenuVO;

    .line 265
    iput-object p1, p0, Lcom/itau/empresas/CustomApplication;->menuVO:Lcom/itau/empresas/api/model/MenuVO;

    .line 266
    return-void
.end method

.method public setPendenciasVO(Lcom/itau/empresas/feature/pendencias/ListaPendenciasVO;)V
    .registers 4
    .param p1, "pendenciasVO"    # Lcom/itau/empresas/feature/pendencias/ListaPendenciasVO;

    .line 304
    iget-object v0, p0, Lcom/itau/empresas/CustomApplication;->gerenciadorSessaoUsuario:Lcom/itau/empresas/GerenciadorSessaoUsuario;

    const-string v1, "pendencia"

    invoke-virtual {v0, v1, p1}, Lcom/itau/empresas/GerenciadorSessaoUsuario;->salvaPreferencia(Ljava/lang/String;Ljava/lang/Object;)V

    .line 305
    iput-object p1, p0, Lcom/itau/empresas/CustomApplication;->pendenciasVO:Lcom/itau/empresas/feature/pendencias/ListaPendenciasVO;

    .line 306
    return-void
.end method

.method public setPerfilLogado(Lcom/itau/empresas/api/model/PerfilOperadorVO;)V
    .registers 4
    .param p1, "perfilLogado"    # Lcom/itau/empresas/api/model/PerfilOperadorVO;

    .line 180
    iget-object v0, p0, Lcom/itau/empresas/CustomApplication;->gerenciadorSessaoUsuario:Lcom/itau/empresas/GerenciadorSessaoUsuario;

    const-string v1, "perfilLogado"

    invoke-virtual {v0, v1, p1}, Lcom/itau/empresas/GerenciadorSessaoUsuario;->salvaPreferencia(Ljava/lang/String;Ljava/lang/Object;)V

    .line 181
    iput-object p1, p0, Lcom/itau/empresas/CustomApplication;->perfilLogado:Lcom/itau/empresas/api/model/PerfilOperadorVO;

    .line 182
    return-void
.end method

.method public setPermissaoAutorizante(Lcom/itau/empresas/api/model/MenuVO;)V
    .registers 2
    .param p1, "permissaoAutorizante"    # Lcom/itau/empresas/api/model/MenuVO;

    .line 273
    iput-object p1, p0, Lcom/itau/empresas/CustomApplication;->permissaoAutorizante:Lcom/itau/empresas/api/model/MenuVO;

    .line 274
    return-void
.end method

.method public setSharedPreferences(Landroid/content/SharedPreferences;)V
    .registers 2
    .param p1, "sharedPreferences"    # Landroid/content/SharedPreferences;

    .line 253
    iput-object p1, p0, Lcom/itau/empresas/CustomApplication;->sharedPreferences:Landroid/content/SharedPreferences;

    .line 254
    return-void
.end method

.method public verificarPermissao(Lcom/itau/empresas/CustomApplication$Exibicao;)Z
    .registers 5
    .param p1, "perfilAutorizante"    # Lcom/itau/empresas/CustomApplication$Exibicao;

    .line 286
    invoke-static {}, Lcom/itau/empresas/ui/util/menu/MenuBuscaHelper;->getInstance()Lcom/itau/empresas/ui/util/menu/MenuBuscaHelper;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/CustomApplication;->permissaoAutorizante:Lcom/itau/empresas/api/model/MenuVO;

    .line 287
    invoke-virtual {p1}, Lcom/itau/empresas/CustomApplication$Exibicao;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/itau/empresas/ui/util/menu/MenuBuscaHelper;->buscaPorNome(Lcom/itau/empresas/api/model/MenuVO;Ljava/lang/String;)Lcom/itau/empresas/api/model/MenuVO;

    move-result-object v0

    if-eqz v0, :cond_16

    const/4 v0, 0x1

    goto :goto_17

    :cond_16
    const/4 v0, 0x0

    .line 286
    :goto_17
    return v0
.end method

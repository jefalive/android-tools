.class public final Lcom/google/android/gms/measurement/zzg;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/measurement/zzg$zzb;,
        Lcom/google/android/gms/measurement/zzg$zzc;,
        Lcom/google/android/gms/measurement/zzg$zza;
    }
.end annotation


# static fields
.field private static volatile zzaUv:Lcom/google/android/gms/measurement/zzg;


# instance fields
.field private final mContext:Landroid/content/Context;

.field private volatile zzQX:Lcom/google/android/gms/internal/zzpq;

.field private final zzaUw:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Lcom/google/android/gms/measurement/zzh;>;"
        }
    .end annotation
.end field

.field private final zzaUx:Lcom/google/android/gms/measurement/zzb;

.field private final zzaUy:Lcom/google/android/gms/measurement/zzg$zza;

.field private zzaUz:Ljava/lang/Thread$UncaughtExceptionHandler;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .registers 4

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/common/internal/zzx;->zzz(Ljava/lang/Object;)Ljava/lang/Object;

    iput-object v1, p0, Lcom/google/android/gms/measurement/zzg;->mContext:Landroid/content/Context;

    new-instance v0, Lcom/google/android/gms/measurement/zzg$zza;

    invoke-direct {v0, p0}, Lcom/google/android/gms/measurement/zzg$zza;-><init>(Lcom/google/android/gms/measurement/zzg;)V

    iput-object v0, p0, Lcom/google/android/gms/measurement/zzg;->zzaUy:Lcom/google/android/gms/measurement/zzg$zza;

    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/measurement/zzg;->zzaUw:Ljava/util/List;

    new-instance v0, Lcom/google/android/gms/measurement/zzb;

    invoke-direct {v0}, Lcom/google/android/gms/measurement/zzb;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/measurement/zzg;->zzaUx:Lcom/google/android/gms/measurement/zzb;

    return-void
.end method

.method static synthetic zza(Lcom/google/android/gms/measurement/zzg;)Ljava/util/List;
    .registers 2

    iget-object v0, p0, Lcom/google/android/gms/measurement/zzg;->zzaUw:Ljava/util/List;

    return-object v0
.end method

.method static synthetic zza(Lcom/google/android/gms/measurement/zzg;Lcom/google/android/gms/measurement/zzc;)V
    .registers 2

    invoke-direct {p0, p1}, Lcom/google/android/gms/measurement/zzg;->zzb(Lcom/google/android/gms/measurement/zzc;)V

    return-void
.end method

.method public static zzaS(Landroid/content/Context;)Lcom/google/android/gms/measurement/zzg;
    .registers 4

    invoke-static {p0}, Lcom/google/android/gms/common/internal/zzx;->zzz(Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/measurement/zzg;->zzaUv:Lcom/google/android/gms/measurement/zzg;

    if-nez v0, :cond_1a

    const-class v1, Lcom/google/android/gms/measurement/zzg;

    monitor-enter v1

    :try_start_a
    sget-object v0, Lcom/google/android/gms/measurement/zzg;->zzaUv:Lcom/google/android/gms/measurement/zzg;

    if-nez v0, :cond_15

    new-instance v0, Lcom/google/android/gms/measurement/zzg;

    invoke-direct {v0, p0}, Lcom/google/android/gms/measurement/zzg;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/google/android/gms/measurement/zzg;->zzaUv:Lcom/google/android/gms/measurement/zzg;
    :try_end_15
    .catchall {:try_start_a .. :try_end_15} :catchall_17

    :cond_15
    monitor-exit v1

    goto :goto_1a

    :catchall_17
    move-exception v2

    monitor-exit v1

    throw v2

    :cond_1a
    :goto_1a
    sget-object v0, Lcom/google/android/gms/measurement/zzg;->zzaUv:Lcom/google/android/gms/measurement/zzg;

    return-object v0
.end method

.method static synthetic zzb(Lcom/google/android/gms/measurement/zzg;)Ljava/lang/Thread$UncaughtExceptionHandler;
    .registers 2

    iget-object v0, p0, Lcom/google/android/gms/measurement/zzg;->zzaUz:Ljava/lang/Thread$UncaughtExceptionHandler;

    return-object v0
.end method

.method private zzb(Lcom/google/android/gms/measurement/zzc;)V
    .registers 9

    const-string v0, "deliver should be called from worker thread"

    invoke-static {v0}, Lcom/google/android/gms/common/internal/zzx;->zzcE(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/gms/measurement/zzc;->zzAz()Z

    move-result v0

    const-string v1, "Measurement must be submitted"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/zzx;->zzb(ZLjava/lang/Object;)V

    invoke-virtual {p1}, Lcom/google/android/gms/measurement/zzc;->zzAw()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_19

    return-void

    :cond_19
    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_22
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_41

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/google/android/gms/measurement/zzi;

    invoke-interface {v5}, Lcom/google/android/gms/measurement/zzi;->zziA()Landroid/net/Uri;

    move-result-object v6

    invoke-interface {v3, v6}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3a

    goto :goto_22

    :cond_3a
    invoke-interface {v3, v6}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    invoke-interface {v5, p1}, Lcom/google/android/gms/measurement/zzi;->zzb(Lcom/google/android/gms/measurement/zzc;)V

    goto :goto_22

    :cond_41
    return-void
.end method

.method public static zzjk()V
    .registers 2

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    instance-of v0, v0, Lcom/google/android/gms/measurement/zzg$zzc;

    if-nez v0, :cond_10

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Call expected from worker thread"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_10
    return-void
.end method


# virtual methods
.method public getContext()Landroid/content/Context;
    .registers 2

    iget-object v0, p0, Lcom/google/android/gms/measurement/zzg;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method public zzAH()Lcom/google/android/gms/internal/zzpq;
    .registers 13

    iget-object v0, p0, Lcom/google/android/gms/measurement/zzg;->zzQX:Lcom/google/android/gms/internal/zzpq;

    if-nez v0, :cond_6f

    move-object v3, p0

    monitor-enter v3

    :try_start_6
    iget-object v0, p0, Lcom/google/android/gms/measurement/zzg;->zzQX:Lcom/google/android/gms/internal/zzpq;

    if-nez v0, :cond_6a

    new-instance v4, Lcom/google/android/gms/internal/zzpq;

    invoke-direct {v4}, Lcom/google/android/gms/internal/zzpq;-><init>()V

    iget-object v0, p0, Lcom/google/android/gms/measurement/zzg;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    iget-object v0, p0, Lcom/google/android/gms/measurement/zzg;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Lcom/google/android/gms/internal/zzpq;->setAppId(Ljava/lang/String;)V

    invoke-virtual {v5, v6}, Landroid/content/pm/PackageManager;->getInstallerPackageName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Lcom/google/android/gms/internal/zzpq;->setAppInstallerId(Ljava/lang/String;)V
    :try_end_25
    .catchall {:try_start_6 .. :try_end_25} :catchall_6c

    move-object v7, v6

    const/4 v8, 0x0

    :try_start_27
    iget-object v0, p0, Lcom/google/android/gms/measurement/zzg;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v5, v0, v1}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v9

    if-eqz v9, :cond_48

    iget-object v0, v9, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    invoke-virtual {v5, v0}, Landroid/content/pm/PackageManager;->getApplicationLabel(Landroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;

    move-result-object v10

    invoke-static {v10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_45

    invoke-interface {v10}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v7, v0

    :cond_45
    iget-object v0, v9, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;
    :try_end_47
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_27 .. :try_end_47} :catch_49
    .catchall {:try_start_27 .. :try_end_47} :catchall_6c

    move-object v8, v0

    :cond_48
    goto :goto_62

    :catch_49
    move-exception v9

    const-string v0, "GAv4"

    :try_start_4c
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Error retrieving package info: appName set to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_62
    invoke-virtual {v4, v7}, Lcom/google/android/gms/internal/zzpq;->setAppName(Ljava/lang/String;)V

    invoke-virtual {v4, v8}, Lcom/google/android/gms/internal/zzpq;->setAppVersion(Ljava/lang/String;)V

    iput-object v4, p0, Lcom/google/android/gms/measurement/zzg;->zzQX:Lcom/google/android/gms/internal/zzpq;
    :try_end_6a
    .catchall {:try_start_4c .. :try_end_6a} :catchall_6c

    :cond_6a
    monitor-exit v3

    goto :goto_6f

    :catchall_6c
    move-exception v11

    monitor-exit v3

    throw v11

    :cond_6f
    :goto_6f
    iget-object v0, p0, Lcom/google/android/gms/measurement/zzg;->zzQX:Lcom/google/android/gms/internal/zzpq;

    return-object v0
.end method

.method public zzAI()Lcom/google/android/gms/internal/zzps;
    .registers 4

    iget-object v0, p0, Lcom/google/android/gms/measurement/zzg;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    new-instance v2, Lcom/google/android/gms/internal/zzps;

    invoke-direct {v2}, Lcom/google/android/gms/internal/zzps;-><init>()V

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/analytics/internal/zzam;->zza(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/google/android/gms/internal/zzps;->setLanguage(Ljava/lang/String;)V

    iget v0, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    invoke-virtual {v2, v0}, Lcom/google/android/gms/internal/zzps;->zziB(I)V

    iget v0, v1, Landroid/util/DisplayMetrics;->heightPixels:I

    invoke-virtual {v2, v0}, Lcom/google/android/gms/internal/zzps;->zziC(I)V

    return-object v2
.end method

.method public zza(Ljava/lang/Thread$UncaughtExceptionHandler;)V
    .registers 2

    iput-object p1, p0, Lcom/google/android/gms/measurement/zzg;->zzaUz:Ljava/lang/Thread$UncaughtExceptionHandler;

    return-void
.end method

.method public zzc(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/Future;
    .registers 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:Ljava/lang/Object;>(Ljava/util/concurrent/Callable<TV;>;)Ljava/util/concurrent/Future<TV;>;"
        }
    .end annotation

    invoke-static {p1}, Lcom/google/android/gms/common/internal/zzx;->zzz(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    instance-of v0, v0, Lcom/google/android/gms/measurement/zzg$zzc;

    if-eqz v0, :cond_14

    new-instance v1, Ljava/util/concurrent/FutureTask;

    invoke-direct {v1, p1}, Ljava/util/concurrent/FutureTask;-><init>(Ljava/util/concurrent/Callable;)V

    invoke-virtual {v1}, Ljava/util/concurrent/FutureTask;->run()V

    return-object v1

    :cond_14
    iget-object v0, p0, Lcom/google/android/gms/measurement/zzg;->zzaUy:Lcom/google/android/gms/measurement/zzg$zza;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/measurement/zzg$zza;->submit(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/Future;

    move-result-object v0

    return-object v0
.end method

.method zze(Lcom/google/android/gms/measurement/zzc;)V
    .registers 5

    invoke-virtual {p1}, Lcom/google/android/gms/measurement/zzc;->zzAD()Z

    move-result v0

    if-eqz v0, :cond_e

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Measurement prototype can\'t be submitted"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_e
    invoke-virtual {p1}, Lcom/google/android/gms/measurement/zzc;->zzAz()Z

    move-result v0

    if-eqz v0, :cond_1c

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Measurement can only be submitted once"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1c
    invoke-virtual {p1}, Lcom/google/android/gms/measurement/zzc;->zzAu()Lcom/google/android/gms/measurement/zzc;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/measurement/zzc;->zzAA()V

    iget-object v0, p0, Lcom/google/android/gms/measurement/zzg;->zzaUy:Lcom/google/android/gms/measurement/zzg$zza;

    new-instance v1, Lcom/google/android/gms/measurement/zzg$1;

    invoke-direct {v1, p0, v2}, Lcom/google/android/gms/measurement/zzg$1;-><init>(Lcom/google/android/gms/measurement/zzg;Lcom/google/android/gms/measurement/zzc;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/measurement/zzg$zza;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public zzf(Ljava/lang/Runnable;)V
    .registers 3

    invoke-static {p1}, Lcom/google/android/gms/common/internal/zzx;->zzz(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/gms/measurement/zzg;->zzaUy:Lcom/google/android/gms/measurement/zzg$zza;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/measurement/zzg$zza;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    return-void
.end method

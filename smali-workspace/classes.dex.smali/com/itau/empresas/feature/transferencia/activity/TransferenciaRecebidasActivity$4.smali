.class Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity$4;
.super Ljava/lang/Object;
.source "TransferenciaRecebidasActivity.java"

# interfaces
.implements Landroid/support/v7/widget/SearchView$OnQueryTextListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;->pesquisarTransferenciaRecebidas()Landroid/support/v7/widget/SearchView$OnQueryTextListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;


# direct methods
.method constructor <init>(Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;)V
    .registers 2
    .param p1, "this$0"    # Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;

    .line 318
    iput-object p1, p0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity$4;->this$0:Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onQueryTextChange(Ljava/lang/String;)Z
    .registers 3
    .param p1, "query"    # Ljava/lang/String;

    .line 329
    iget-object v0, p0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity$4;->this$0:Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;

    # getter for: Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;->transferenciaRecebidasAdapter:Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter;
    invoke-static {v0}, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;->access$200(Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;)Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter;

    move-result-object v0

    if-eqz v0, :cond_15

    .line 330
    iget-object v0, p0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity$4;->this$0:Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;

    # getter for: Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;->transferenciaRecebidasAdapter:Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter;
    invoke-static {v0}, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;->access$200(Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;)Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter;->getFilter()Landroid/widget/Filter;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/Filter;->filter(Ljava/lang/CharSequence;)V

    .line 332
    :cond_15
    const/4 v0, 0x0

    return v0
.end method

.method public onQueryTextSubmit(Ljava/lang/String;)Z
    .registers 3
    .param p1, "query"    # Ljava/lang/String;

    .line 321
    iget-object v0, p0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity$4;->this$0:Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;

    # getter for: Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;->transferenciaRecebidasAdapter:Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter;
    invoke-static {v0}, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;->access$200(Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;)Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter;

    move-result-object v0

    if-eqz v0, :cond_15

    .line 322
    iget-object v0, p0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity$4;->this$0:Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;

    # getter for: Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;->transferenciaRecebidasAdapter:Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter;
    invoke-static {v0}, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;->access$200(Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;)Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter;->getFilter()Landroid/widget/Filter;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/Filter;->filter(Ljava/lang/CharSequence;)V

    .line 324
    :cond_15
    const/4 v0, 0x0

    return v0
.end method

.class public final Lcc/b;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final i:[B

.field private static j:I


# instance fields
.field a:I

.field public b:J

.field public c:[B

.field public d:I

.field public e:I

.field public f:Z

.field public g:I

.field public h:I


# direct methods
.method static constructor <clinit>()V
    .registers 3

    const/16 v0, 0x6d

    new-array v0, v0, [B

    fill-array-data v0, :array_e

    sput-object v0, Lcc/b;->i:[B

    const/16 v0, 0xfa

    sput v0, Lcc/b;->j:I

    return-void

    :array_e
    .array-data 1
        0x70t
        -0x7bt
        -0x43t
        0x3ft
        0x49t
        -0x1dt
        0x2t
        0xet
        -0x14t
        -0x5t
        -0x6t
        0x8t
        -0x3at
        -0x1dt
        0x49t
        -0x4ct
        -0x11t
        0x3et
        0xdt
        -0x8t
        -0x5t
        -0x12t
        0x4t
        0x1t
        -0x2dt
        0x18t
        0x0t
        -0xet
        -0x1at
        0x1at
        -0x1t
        -0x3bt
        -0x1dt
        0x49t
        -0x4ct
        -0x11t
        0x40t
        0x5t
        -0xet
        -0x2et
        -0x1dt
        0x49t
        -0x4ct
        -0x11t
        0x44t
        -0xct
        0x2t
        0x6t
        -0x22t
        0x13t
        -0x2t
        -0xct
        -0x2ft
        0x2ct
        -0x12t
        -0x9t
        -0x29t
        -0x1dt
        0x49t
        -0x4ct
        -0x11t
        0x47t
        -0xft
        0x2t
        0x6t
        -0x22t
        0x13t
        -0x2t
        -0xct
        -0x2ft
        0x2ct
        -0x12t
        -0x9t
        -0x29t
        -0x1dt
        0x49t
        -0x4ct
        -0x11t
        0x48t
        -0x3t
        -0x10t
        0xct
        -0xet
        0x4t
        -0x7t
        -0x3bt
        -0x1dt
        0x49t
        -0x4ct
        -0x11t
        0x4bt
        -0x6t
        -0x1t
        -0x3bt
        -0x1dt
        0x49t
        -0x4ct
        -0x11t
        0x4dt
        -0x12t
        -0x9t
        -0x2t
        -0x19t
        0x1at
        -0x1t
        -0x3bt
        -0x1dt
        0x49t
        -0x1bt
    .end array-data
.end method

.method public constructor <init>()V
    .registers 1

    .line 93
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 94
    return-void
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .registers 10

    .line 104
    sget-object v0, Lcc/b;->i:[B

    const/16 v1, 0x1a

    aget-byte v0, v0, v1

    int-to-byte v4, v0

    int-to-byte v5, v4

    sget-object v0, Lcc/b;->i:[B

    const/16 v1, 0x1e

    aget-byte v0, v0, v1

    int-to-byte v6, v0

    .line 1000
    sget-object v7, Lcc/b;->i:[B

    add-int/lit8 v6, v6, 0x4

    mul-int/lit8 v5, v5, 0x4

    add-int/lit8 v5, v5, 0x6a

    const/4 v8, 0x0

    new-instance v0, Ljava/lang/String;

    mul-int/lit8 v4, v4, 0x4

    add-int/lit8 v4, v4, 0x25

    new-array v1, v5, [B

    add-int/lit8 v5, v5, -0x1

    if-nez v7, :cond_29

    move v2, v5

    move v3, v4

    :goto_26
    add-int/2addr v2, v3

    add-int/lit8 v4, v2, 0x5

    :cond_29
    add-int/lit8 v6, v6, 0x1

    int-to-byte v2, v4

    aput-byte v2, v1, v8

    if-ne v8, v5, :cond_35

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Ljava/lang/String;-><init>([BI)V

    goto :goto_3b

    :cond_35
    add-int/lit8 v8, v8, 0x1

    move v2, v4

    aget-byte v3, v7, v6

    goto :goto_26

    .line 104
    :goto_3b
    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0x9

    new-array v1, v1, [Ljava/lang/Object;

    .line 105
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    iget-object v2, p0, Lcc/b;->c:[B

    invoke-static {v2}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    aput-object v2, v1, v3

    iget v2, p0, Lcc/b;->g:I

    .line 106
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x2

    aput-object v2, v1, v3

    iget-boolean v2, p0, Lcc/b;->f:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    const/4 v3, 0x3

    aput-object v2, v1, v3

    iget v2, p0, Lcc/b;->a:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x4

    aput-object v2, v1, v3

    iget-wide v2, p0, Lcc/b;->b:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    const/4 v3, 0x5

    aput-object v2, v1, v3

    iget v2, p0, Lcc/b;->h:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x6

    aput-object v2, v1, v3

    iget v2, p0, Lcc/b;->d:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x7

    aput-object v2, v1, v3

    iget v2, p0, Lcc/b;->e:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/16 v3, 0x8

    aput-object v2, v1, v3

    .line 104
    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

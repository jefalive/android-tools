.class public Lcom/itau/empresas/api/model/ResultadoTransferenciaVO;
.super Ljava/lang/Object;
.source "ResultadoTransferenciaVO.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private dadosComplementaresTransferencia:Lcom/itau/empresas/api/model/DadosComplementaresTransferenciaVO;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "dados_complementares_transferencia"
    .end annotation
.end field

.field private dadosConfirmacaoComprovante:Lcom/itau/empresas/feature/pagamento/comcodigobarra/model/DadosConfirmacaoComprovanteVO;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "dados_confirmacao_comprovante"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

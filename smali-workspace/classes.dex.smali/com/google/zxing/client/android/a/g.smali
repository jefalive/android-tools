.class public final Lcom/google/zxing/client/android/a/g;
.super Ljava/lang/Object;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:Lcom/google/zxing/client/android/a/d;

.field private d:Landroid/hardware/Camera;

.field private e:Lcom/google/zxing/client/android/a/a;

.field private f:Landroid/graphics/Rect;

.field private g:Landroid/graphics/Rect;

.field private h:Z

.field private i:Z

.field private j:I

.field private k:I

.field private l:I

.field private final m:Lcom/google/zxing/client/android/a/i;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    const-class v0, Lcom/google/zxing/client/android/a/g;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/zxing/client/android/a/g;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 4

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/zxing/client/android/a/g;->j:I

    iput-object p1, p0, Lcom/google/zxing/client/android/a/g;->b:Landroid/content/Context;

    new-instance v0, Lcom/google/zxing/client/android/a/d;

    invoke-direct {v0, p1}, Lcom/google/zxing/client/android/a/d;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/zxing/client/android/a/g;->c:Lcom/google/zxing/client/android/a/d;

    new-instance v0, Lcom/google/zxing/client/android/a/i;

    iget-object v1, p0, Lcom/google/zxing/client/android/a/g;->c:Lcom/google/zxing/client/android/a/d;

    invoke-direct {v0, v1}, Lcom/google/zxing/client/android/a/i;-><init>(Lcom/google/zxing/client/android/a/d;)V

    iput-object v0, p0, Lcom/google/zxing/client/android/a/g;->m:Lcom/google/zxing/client/android/a/i;

    return-void
.end method

.method private static a(III)I
    .registers 6

    mul-int/lit8 v0, p0, 0x5

    div-int/lit8 v2, v0, 0x8

    if-ge v2, p1, :cond_7

    return p1

    :cond_7
    if-le v2, p2, :cond_a

    return p2

    :cond_a
    return v2
.end method


# virtual methods
.method public a([BII)Lcom/google/zxing/i;
    .registers 14

    invoke-virtual {p0}, Lcom/google/zxing/client/android/a/g;->h()Landroid/graphics/Rect;

    move-result-object v9

    if-nez v9, :cond_8

    const/4 v0, 0x0

    return-object v0

    :cond_8
    new-instance v0, Lcom/google/zxing/i;

    iget v4, v9, Landroid/graphics/Rect;->left:I

    iget v5, v9, Landroid/graphics/Rect;->top:I

    invoke-virtual {v9}, Landroid/graphics/Rect;->width()I

    move-result v6

    invoke-virtual {v9}, Landroid/graphics/Rect;->height()I

    move-result v7

    move-object v1, p1

    move v2, p2

    move v3, p3

    const/4 v8, 0x0

    invoke-direct/range {v0 .. v8}, Lcom/google/zxing/i;-><init>([BIIIIIIZ)V

    return-object v0
.end method

.method public declared-synchronized a(I)V
    .registers 3

    monitor-enter p0

    :try_start_1
    iget-boolean v0, p0, Lcom/google/zxing/client/android/a/g;->h:Z

    if-eqz v0, :cond_b

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_b
    iput p1, p0, Lcom/google/zxing/client/android/a/g;->j:I
    :try_end_d
    .catchall {:try_start_1 .. :try_end_d} :catchall_f

    monitor-exit p0

    return-void

    :catchall_f
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized a(II)V
    .registers 9

    monitor-enter p0

    :try_start_1
    iget-boolean v0, p0, Lcom/google/zxing/client/android/a/g;->h:Z

    if-eqz v0, :cond_4a

    iget-object v0, p0, Lcom/google/zxing/client/android/a/g;->c:Lcom/google/zxing/client/android/a/d;

    invoke-virtual {v0}, Lcom/google/zxing/client/android/a/d;->b()Landroid/graphics/Point;

    move-result-object v3

    iget v0, v3, Landroid/graphics/Point;->x:I

    if-le p1, v0, :cond_11

    iget p1, v3, Landroid/graphics/Point;->x:I

    :cond_11
    iget v0, v3, Landroid/graphics/Point;->y:I

    if-le p2, v0, :cond_17

    iget p2, v3, Landroid/graphics/Point;->y:I

    :cond_17
    iget v0, v3, Landroid/graphics/Point;->x:I

    sub-int/2addr v0, p1

    div-int/lit8 v4, v0, 0x2

    iget v0, v3, Landroid/graphics/Point;->y:I

    sub-int/2addr v0, p2

    div-int/lit8 v5, v0, 0x2

    new-instance v0, Landroid/graphics/Rect;

    add-int v1, v4, p1

    add-int v2, v5, p2

    invoke-direct {v0, v4, v5, v1, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v0, p0, Lcom/google/zxing/client/android/a/g;->f:Landroid/graphics/Rect;

    sget-object v0, Lcom/google/zxing/client/android/a/g;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Calculated manual framing rect: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/zxing/client/android/a/g;->f:Landroid/graphics/Rect;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/zxing/client/android/a/g;->g:Landroid/graphics/Rect;

    goto :goto_4e

    :cond_4a
    iput p1, p0, Lcom/google/zxing/client/android/a/g;->k:I

    iput p2, p0, Lcom/google/zxing/client/android/a/g;->l:I
    :try_end_4e
    .catchall {:try_start_1 .. :try_end_4e} :catchall_50

    :goto_4e
    monitor-exit p0

    return-void

    :catchall_50
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized a(Landroid/os/Handler;I)V
    .registers 5

    monitor-enter p0

    :try_start_1
    iget-object v1, p0, Lcom/google/zxing/client/android/a/g;->d:Landroid/hardware/Camera;

    if-eqz v1, :cond_13

    iget-boolean v0, p0, Lcom/google/zxing/client/android/a/g;->i:Z

    if-eqz v0, :cond_13

    iget-object v0, p0, Lcom/google/zxing/client/android/a/g;->m:Lcom/google/zxing/client/android/a/i;

    invoke-virtual {v0, p1, p2}, Lcom/google/zxing/client/android/a/i;->a(Landroid/os/Handler;I)V

    iget-object v0, p0, Lcom/google/zxing/client/android/a/g;->m:Lcom/google/zxing/client/android/a/i;

    invoke-virtual {v1, v0}, Landroid/hardware/Camera;->setOneShotPreviewCallback(Landroid/hardware/Camera$PreviewCallback;)V
    :try_end_13
    .catchall {:try_start_1 .. :try_end_13} :catchall_15

    :cond_13
    monitor-exit p0

    return-void

    :catchall_15
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized a(Landroid/view/SurfaceHolder;)V
    .registers 10

    monitor-enter p0

    :try_start_1
    iget-object v3, p0, Lcom/google/zxing/client/android/a/g;->d:Landroid/hardware/Camera;

    if-nez v3, :cond_1e

    iget v0, p0, Lcom/google/zxing/client/android/a/g;->j:I

    if-ltz v0, :cond_10

    iget v0, p0, Lcom/google/zxing/client/android/a/g;->j:I

    invoke-static {v0}, Lcom/google/zxing/client/android/a/a/a;->a(I)Landroid/hardware/Camera;

    move-result-object v3

    goto :goto_14

    :cond_10
    invoke-static {}, Lcom/google/zxing/client/android/a/a/a;->a()Landroid/hardware/Camera;

    move-result-object v3

    :goto_14
    if-nez v3, :cond_1c

    new-instance v0, Ljava/io/IOException;

    invoke-direct {v0}, Ljava/io/IOException;-><init>()V

    throw v0

    :cond_1c
    iput-object v3, p0, Lcom/google/zxing/client/android/a/g;->d:Landroid/hardware/Camera;

    :cond_1e
    invoke-virtual {v3, p1}, Landroid/hardware/Camera;->setPreviewDisplay(Landroid/view/SurfaceHolder;)V

    iget-boolean v0, p0, Lcom/google/zxing/client/android/a/g;->h:Z

    if-nez v0, :cond_42

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/zxing/client/android/a/g;->h:Z

    iget-object v0, p0, Lcom/google/zxing/client/android/a/g;->c:Lcom/google/zxing/client/android/a/d;

    invoke-virtual {v0, v3}, Lcom/google/zxing/client/android/a/d;->a(Landroid/hardware/Camera;)V

    iget v0, p0, Lcom/google/zxing/client/android/a/g;->k:I

    if-lez v0, :cond_42

    iget v0, p0, Lcom/google/zxing/client/android/a/g;->l:I

    if-lez v0, :cond_42

    iget v0, p0, Lcom/google/zxing/client/android/a/g;->k:I

    iget v1, p0, Lcom/google/zxing/client/android/a/g;->l:I

    invoke-virtual {p0, v0, v1}, Lcom/google/zxing/client/android/a/g;->a(II)V

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/zxing/client/android/a/g;->k:I

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/zxing/client/android/a/g;->l:I

    :cond_42
    invoke-virtual {v3}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v4

    if-nez v4, :cond_4a

    const/4 v5, 0x0

    goto :goto_4e

    :cond_4a
    invoke-virtual {v4}, Landroid/hardware/Camera$Parameters;->flatten()Ljava/lang/String;
    :try_end_4d
    .catchall {:try_start_1 .. :try_end_4d} :catchall_92

    move-result-object v5

    :goto_4e
    :try_start_4e
    iget-object v0, p0, Lcom/google/zxing/client/android/a/g;->c:Lcom/google/zxing/client/android/a/d;

    const/4 v1, 0x0

    invoke-virtual {v0, v3, v1}, Lcom/google/zxing/client/android/a/d;->a(Landroid/hardware/Camera;Z)V
    :try_end_54
    .catch Ljava/lang/RuntimeException; {:try_start_4e .. :try_end_54} :catch_55
    .catchall {:try_start_4e .. :try_end_54} :catchall_92

    goto :goto_90

    :catch_55
    move-exception v6

    :try_start_56
    sget-object v0, Lcom/google/zxing/client/android/a/g;->a:Ljava/lang/String;

    const-string v1, "Camera rejected parameters. Setting only minimal safe-mode parameters"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v0, Lcom/google/zxing/client/android/a/g;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Resetting to saved camera params: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz v5, :cond_90

    invoke-virtual {v3}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v4

    invoke-virtual {v4, v5}, Landroid/hardware/Camera$Parameters;->unflatten(Ljava/lang/String;)V
    :try_end_7e
    .catchall {:try_start_56 .. :try_end_7e} :catchall_92

    :try_start_7e
    invoke-virtual {v3, v4}, Landroid/hardware/Camera;->setParameters(Landroid/hardware/Camera$Parameters;)V

    iget-object v0, p0, Lcom/google/zxing/client/android/a/g;->c:Lcom/google/zxing/client/android/a/d;

    const/4 v1, 0x1

    invoke-virtual {v0, v3, v1}, Lcom/google/zxing/client/android/a/d;->a(Landroid/hardware/Camera;Z)V
    :try_end_87
    .catch Ljava/lang/RuntimeException; {:try_start_7e .. :try_end_87} :catch_88
    .catchall {:try_start_7e .. :try_end_87} :catchall_92

    goto :goto_90

    :catch_88
    move-exception v7

    :try_start_89
    sget-object v0, Lcom/google/zxing/client/android/a/g;->a:Ljava/lang/String;

    const-string v1, "Camera rejected even safe-mode parameters! No configuration"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_90
    .catchall {:try_start_89 .. :try_end_90} :catchall_92

    :cond_90
    :goto_90
    monitor-exit p0

    return-void

    :catchall_92
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized a(Z)V
    .registers 4

    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lcom/google/zxing/client/android/a/g;->c:Lcom/google/zxing/client/android/a/d;

    iget-object v1, p0, Lcom/google/zxing/client/android/a/g;->d:Landroid/hardware/Camera;

    invoke-virtual {v0, v1}, Lcom/google/zxing/client/android/a/d;->b(Landroid/hardware/Camera;)Z

    move-result v0

    if-eq p1, v0, :cond_28

    iget-object v0, p0, Lcom/google/zxing/client/android/a/g;->d:Landroid/hardware/Camera;

    if-eqz v0, :cond_28

    iget-object v0, p0, Lcom/google/zxing/client/android/a/g;->e:Lcom/google/zxing/client/android/a/a;

    if-eqz v0, :cond_18

    iget-object v0, p0, Lcom/google/zxing/client/android/a/g;->e:Lcom/google/zxing/client/android/a/a;

    invoke-virtual {v0}, Lcom/google/zxing/client/android/a/a;->b()V

    :cond_18
    iget-object v0, p0, Lcom/google/zxing/client/android/a/g;->c:Lcom/google/zxing/client/android/a/d;

    iget-object v1, p0, Lcom/google/zxing/client/android/a/g;->d:Landroid/hardware/Camera;

    invoke-virtual {v0, v1, p1}, Lcom/google/zxing/client/android/a/d;->b(Landroid/hardware/Camera;Z)V

    iget-object v0, p0, Lcom/google/zxing/client/android/a/g;->e:Lcom/google/zxing/client/android/a/a;

    if-eqz v0, :cond_28

    iget-object v0, p0, Lcom/google/zxing/client/android/a/g;->e:Lcom/google/zxing/client/android/a/a;

    invoke-virtual {v0}, Lcom/google/zxing/client/android/a/a;->a()V
    :try_end_28
    .catchall {:try_start_1 .. :try_end_28} :catchall_2a

    :cond_28
    monitor-exit p0

    return-void

    :catchall_2a
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public a()Z
    .registers 5

    iget-object v0, p0, Lcom/google/zxing/client/android/a/g;->d:Landroid/hardware/Camera;

    if-nez v0, :cond_6

    const/4 v0, 0x0

    return v0

    :cond_6
    iget-object v0, p0, Lcom/google/zxing/client/android/a/g;->d:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v2

    invoke-virtual {v2}, Landroid/hardware/Camera$Parameters;->getFlashMode()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_14

    const/4 v0, 0x0

    return v0

    :cond_14
    invoke-virtual {v2}, Landroid/hardware/Camera$Parameters;->getSupportedFlashModes()Ljava/util/List;

    move-result-object v3

    if-eqz v3, :cond_36

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_36

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_38

    const/4 v0, 0x0

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v1, "off"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_38

    :cond_36
    const/4 v0, 0x0

    return v0

    :cond_38
    const/4 v0, 0x1

    return v0
.end method

.method public declared-synchronized b()Z
    .registers 3

    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lcom/google/zxing/client/android/a/g;->d:Landroid/hardware/Camera;
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_a

    if-eqz v0, :cond_7

    const/4 v0, 0x1

    goto :goto_8

    :cond_7
    const/4 v0, 0x0

    :goto_8
    monitor-exit p0

    return v0

    :catchall_a
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized c()V
    .registers 3

    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lcom/google/zxing/client/android/a/g;->d:Landroid/hardware/Camera;

    if-eqz v0, :cond_13

    iget-object v0, p0, Lcom/google/zxing/client/android/a/g;->d:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->release()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/zxing/client/android/a/g;->d:Landroid/hardware/Camera;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/zxing/client/android/a/g;->f:Landroid/graphics/Rect;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/zxing/client/android/a/g;->g:Landroid/graphics/Rect;
    :try_end_13
    .catchall {:try_start_1 .. :try_end_13} :catchall_15

    :cond_13
    monitor-exit p0

    return-void

    :catchall_15
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized d()V
    .registers 5

    monitor-enter p0

    :try_start_1
    iget-object v3, p0, Lcom/google/zxing/client/android/a/g;->d:Landroid/hardware/Camera;

    if-eqz v3, :cond_1a

    iget-boolean v0, p0, Lcom/google/zxing/client/android/a/g;->i:Z

    if-nez v0, :cond_1a

    invoke-virtual {v3}, Landroid/hardware/Camera;->startPreview()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/zxing/client/android/a/g;->i:Z

    new-instance v0, Lcom/google/zxing/client/android/a/a;

    iget-object v1, p0, Lcom/google/zxing/client/android/a/g;->b:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/zxing/client/android/a/g;->d:Landroid/hardware/Camera;

    invoke-direct {v0, v1, v2}, Lcom/google/zxing/client/android/a/a;-><init>(Landroid/content/Context;Landroid/hardware/Camera;)V

    iput-object v0, p0, Lcom/google/zxing/client/android/a/g;->e:Lcom/google/zxing/client/android/a/a;
    :try_end_1a
    .catchall {:try_start_1 .. :try_end_1a} :catchall_1c

    :cond_1a
    monitor-exit p0

    return-void

    :catchall_1c
    move-exception v3

    monitor-exit p0

    throw v3
.end method

.method public declared-synchronized e()V
    .registers 5

    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lcom/google/zxing/client/android/a/g;->e:Lcom/google/zxing/client/android/a/a;

    if-eqz v0, :cond_d

    iget-object v0, p0, Lcom/google/zxing/client/android/a/g;->e:Lcom/google/zxing/client/android/a/a;

    invoke-virtual {v0}, Lcom/google/zxing/client/android/a/a;->b()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/zxing/client/android/a/g;->e:Lcom/google/zxing/client/android/a/a;

    :cond_d
    iget-object v0, p0, Lcom/google/zxing/client/android/a/g;->d:Landroid/hardware/Camera;

    if-eqz v0, :cond_24

    iget-boolean v0, p0, Lcom/google/zxing/client/android/a/g;->i:Z

    if-eqz v0, :cond_24

    iget-object v0, p0, Lcom/google/zxing/client/android/a/g;->d:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->stopPreview()V

    iget-object v0, p0, Lcom/google/zxing/client/android/a/g;->m:Lcom/google/zxing/client/android/a/i;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/zxing/client/android/a/i;->a(Landroid/os/Handler;I)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/zxing/client/android/a/g;->i:Z
    :try_end_24
    .catchall {:try_start_1 .. :try_end_24} :catchall_26

    :cond_24
    monitor-exit p0

    return-void

    :catchall_26
    move-exception v3

    monitor-exit p0

    throw v3
.end method

.method public declared-synchronized f()V
    .registers 5

    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lcom/google/zxing/client/android/a/g;->d:Landroid/hardware/Camera;

    if-eqz v0, :cond_2b

    iget-object v0, p0, Lcom/google/zxing/client/android/a/g;->e:Lcom/google/zxing/client/android/a/a;

    if-eqz v0, :cond_e

    iget-object v0, p0, Lcom/google/zxing/client/android/a/g;->e:Lcom/google/zxing/client/android/a/a;

    invoke-virtual {v0}, Lcom/google/zxing/client/android/a/a;->b()V

    :cond_e
    iget-object v0, p0, Lcom/google/zxing/client/android/a/g;->c:Lcom/google/zxing/client/android/a/d;

    iget-object v1, p0, Lcom/google/zxing/client/android/a/g;->d:Landroid/hardware/Camera;

    invoke-virtual {v0, v1}, Lcom/google/zxing/client/android/a/d;->b(Landroid/hardware/Camera;)Z

    move-result v3

    iget-object v0, p0, Lcom/google/zxing/client/android/a/g;->c:Lcom/google/zxing/client/android/a/d;

    iget-object v1, p0, Lcom/google/zxing/client/android/a/g;->d:Landroid/hardware/Camera;

    if-nez v3, :cond_1e

    const/4 v2, 0x1

    goto :goto_1f

    :cond_1e
    const/4 v2, 0x0

    :goto_1f
    invoke-virtual {v0, v1, v2}, Lcom/google/zxing/client/android/a/d;->b(Landroid/hardware/Camera;Z)V

    iget-object v0, p0, Lcom/google/zxing/client/android/a/g;->e:Lcom/google/zxing/client/android/a/a;

    if-eqz v0, :cond_2b

    iget-object v0, p0, Lcom/google/zxing/client/android/a/g;->e:Lcom/google/zxing/client/android/a/a;

    invoke-virtual {v0}, Lcom/google/zxing/client/android/a/a;->a()V
    :try_end_2b
    .catchall {:try_start_1 .. :try_end_2b} :catchall_2d

    :cond_2b
    monitor-exit p0

    return-void

    :catchall_2d
    move-exception v3

    monitor-exit p0

    throw v3
.end method

.method public declared-synchronized g()Landroid/graphics/Rect;
    .registers 9

    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lcom/google/zxing/client/android/a/g;->f:Landroid/graphics/Rect;

    if-nez v0, :cond_5a

    iget-object v0, p0, Lcom/google/zxing/client/android/a/g;->d:Landroid/hardware/Camera;

    if-nez v0, :cond_c

    monitor-exit p0

    const/4 v0, 0x0

    return-object v0

    :cond_c
    iget-object v0, p0, Lcom/google/zxing/client/android/a/g;->c:Lcom/google/zxing/client/android/a/d;

    invoke-virtual {v0}, Lcom/google/zxing/client/android/a/d;->b()Landroid/graphics/Point;

    move-result-object v3

    if-nez v3, :cond_17

    monitor-exit p0

    const/4 v0, 0x0

    return-object v0

    :cond_17
    iget v0, v3, Landroid/graphics/Point;->x:I

    const/16 v1, 0xf0

    const/16 v2, 0x4b0

    invoke-static {v0, v1, v2}, Lcom/google/zxing/client/android/a/g;->a(III)I

    move-result v4

    iget v0, v3, Landroid/graphics/Point;->y:I

    const/16 v1, 0xf0

    const/16 v2, 0x2a3

    invoke-static {v0, v1, v2}, Lcom/google/zxing/client/android/a/g;->a(III)I

    move-result v5

    iget v0, v3, Landroid/graphics/Point;->x:I

    sub-int/2addr v0, v4

    div-int/lit8 v6, v0, 0x2

    iget v0, v3, Landroid/graphics/Point;->y:I

    sub-int/2addr v0, v5

    div-int/lit8 v7, v0, 0x2

    new-instance v0, Landroid/graphics/Rect;

    add-int v1, v6, v4

    add-int v2, v7, v5

    invoke-direct {v0, v6, v7, v1, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v0, p0, Lcom/google/zxing/client/android/a/g;->f:Landroid/graphics/Rect;

    sget-object v0, Lcom/google/zxing/client/android/a/g;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Calculated framing rect: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/zxing/client/android/a/g;->f:Landroid/graphics/Rect;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_5a
    iget-object v0, p0, Lcom/google/zxing/client/android/a/g;->f:Landroid/graphics/Rect;
    :try_end_5c
    .catchall {:try_start_1 .. :try_end_5c} :catchall_5e

    monitor-exit p0

    return-object v0

    :catchall_5e
    move-exception v3

    monitor-exit p0

    throw v3
.end method

.method public declared-synchronized h()Landroid/graphics/Rect;
    .registers 7

    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lcom/google/zxing/client/android/a/g;->g:Landroid/graphics/Rect;

    if-nez v0, :cond_50

    invoke-virtual {p0}, Lcom/google/zxing/client/android/a/g;->g()Landroid/graphics/Rect;

    move-result-object v2

    if-nez v2, :cond_e

    monitor-exit p0

    const/4 v0, 0x0

    return-object v0

    :cond_e
    new-instance v3, Landroid/graphics/Rect;

    invoke-direct {v3, v2}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    iget-object v0, p0, Lcom/google/zxing/client/android/a/g;->c:Lcom/google/zxing/client/android/a/d;

    invoke-virtual {v0}, Lcom/google/zxing/client/android/a/d;->a()Landroid/graphics/Point;

    move-result-object v4

    iget-object v0, p0, Lcom/google/zxing/client/android/a/g;->c:Lcom/google/zxing/client/android/a/d;

    invoke-virtual {v0}, Lcom/google/zxing/client/android/a/d;->b()Landroid/graphics/Point;

    move-result-object v5

    if-eqz v4, :cond_23

    if-nez v5, :cond_26

    :cond_23
    monitor-exit p0

    const/4 v0, 0x0

    return-object v0

    :cond_26
    iget v0, v3, Landroid/graphics/Rect;->left:I

    iget v1, v4, Landroid/graphics/Point;->x:I

    mul-int/2addr v0, v1

    iget v1, v5, Landroid/graphics/Point;->x:I

    div-int/2addr v0, v1

    iput v0, v3, Landroid/graphics/Rect;->left:I

    iget v0, v3, Landroid/graphics/Rect;->right:I

    iget v1, v4, Landroid/graphics/Point;->x:I

    mul-int/2addr v0, v1

    iget v1, v5, Landroid/graphics/Point;->x:I

    div-int/2addr v0, v1

    iput v0, v3, Landroid/graphics/Rect;->right:I

    iget v0, v3, Landroid/graphics/Rect;->top:I

    iget v1, v4, Landroid/graphics/Point;->y:I

    mul-int/2addr v0, v1

    iget v1, v5, Landroid/graphics/Point;->y:I

    div-int/2addr v0, v1

    iput v0, v3, Landroid/graphics/Rect;->top:I

    iget v0, v3, Landroid/graphics/Rect;->bottom:I

    iget v1, v4, Landroid/graphics/Point;->y:I

    mul-int/2addr v0, v1

    iget v1, v5, Landroid/graphics/Point;->y:I

    div-int/2addr v0, v1

    iput v0, v3, Landroid/graphics/Rect;->bottom:I

    iput-object v3, p0, Lcom/google/zxing/client/android/a/g;->g:Landroid/graphics/Rect;

    :cond_50
    iget-object v0, p0, Lcom/google/zxing/client/android/a/g;->g:Landroid/graphics/Rect;
    :try_end_52
    .catchall {:try_start_1 .. :try_end_52} :catchall_54

    monitor-exit p0

    return-object v0

    :catchall_54
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public i()Z
    .registers 7

    iget-object v0, p0, Lcom/google/zxing/client/android/a/g;->d:Landroid/hardware/Camera;

    if-eqz v0, :cond_34

    iget-object v0, p0, Lcom/google/zxing/client/android/a/g;->d:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v0

    invoke-virtual {v0}, Landroid/hardware/Camera$Parameters;->getSupportedFocusModes()Ljava/util/List;

    move-result-object v2

    if-eqz v2, :cond_18

    const-string v0, "auto"

    invoke-interface {v2, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1a

    :cond_18
    const/4 v0, 0x0

    return v0

    :cond_1a
    const/16 v3, 0x280

    const/16 v4, 0x1e0

    iget-object v0, p0, Lcom/google/zxing/client/android/a/g;->c:Lcom/google/zxing/client/android/a/d;

    invoke-virtual {v0}, Lcom/google/zxing/client/android/a/d;->a()Landroid/graphics/Point;

    move-result-object v5

    iget v0, v5, Landroid/graphics/Point;->x:I

    const/16 v1, 0x280

    if-lt v0, v1, :cond_30

    iget v0, v5, Landroid/graphics/Point;->y:I

    const/16 v1, 0x1e0

    if-ge v0, v1, :cond_32

    :cond_30
    const/4 v0, 0x0

    return v0

    :cond_32
    const/4 v0, 0x1

    return v0

    :cond_34
    const/4 v0, 0x0

    return v0
.end method

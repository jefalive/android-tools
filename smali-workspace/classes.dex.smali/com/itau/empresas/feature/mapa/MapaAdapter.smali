.class public Lcom/itau/empresas/feature/mapa/MapaAdapter;
.super Landroid/support/v4/app/FragmentPagerAdapter;
.source "MapaAdapter.java"


# instance fields
.field private activity:Landroid/app/Activity;

.field private agencias:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Lcom/itau/empresas/api/model/AgenciaVO;>;"
        }
    .end annotation
.end field

.field private mapaFragment:Lcom/itau/empresas/feature/mapa/MapaFragment;

.field private mapaListaFragment:Lcom/itau/empresas/feature/mapa/MapaListaFragment;

.field private pages:[Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/support/v4/app/FragmentManager;Landroid/app/Activity;)V
    .registers 3
    .param p1, "fm"    # Landroid/support/v4/app/FragmentManager;
    .param p2, "activity"    # Landroid/app/Activity;

    .line 24
    invoke-direct {p0, p1}, Landroid/support/v4/app/FragmentPagerAdapter;-><init>(Landroid/support/v4/app/FragmentManager;)V

    .line 25
    iput-object p2, p0, Lcom/itau/empresas/feature/mapa/MapaAdapter;->activity:Landroid/app/Activity;

    .line 26
    return-void
.end method

.method private getMapaFragment()Landroid/support/v4/app/Fragment;
    .registers 3

    .line 41
    iget-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaAdapter;->mapaFragment:Lcom/itau/empresas/feature/mapa/MapaFragment;

    if-nez v0, :cond_e

    .line 42
    invoke-static {}, Lcom/itau/empresas/feature/mapa/MapaFragment_;->builder()Lcom/itau/empresas/feature/mapa/MapaFragment_$FragmentBuilder_;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/feature/mapa/MapaFragment_$FragmentBuilder_;->build()Lcom/itau/empresas/feature/mapa/MapaFragment;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaAdapter;->mapaFragment:Lcom/itau/empresas/feature/mapa/MapaFragment;

    .line 43
    :cond_e
    iget-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaAdapter;->agencias:Ljava/util/List;

    if-eqz v0, :cond_19

    .line 44
    iget-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaAdapter;->mapaFragment:Lcom/itau/empresas/feature/mapa/MapaFragment;

    iget-object v1, p0, Lcom/itau/empresas/feature/mapa/MapaAdapter;->agencias:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/mapa/MapaFragment;->setAgencias(Ljava/util/List;)V

    .line 45
    :cond_19
    iget-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaAdapter;->mapaFragment:Lcom/itau/empresas/feature/mapa/MapaFragment;

    return-object v0
.end method

.method private getMapaListaFragment()Landroid/support/v4/app/Fragment;
    .registers 3

    .line 49
    iget-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaAdapter;->mapaListaFragment:Lcom/itau/empresas/feature/mapa/MapaListaFragment;

    if-nez v0, :cond_e

    .line 50
    invoke-static {}, Lcom/itau/empresas/feature/mapa/MapaListaFragment_;->builder()Lcom/itau/empresas/feature/mapa/MapaListaFragment_$FragmentBuilder_;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/feature/mapa/MapaListaFragment_$FragmentBuilder_;->build()Lcom/itau/empresas/feature/mapa/MapaListaFragment;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaAdapter;->mapaListaFragment:Lcom/itau/empresas/feature/mapa/MapaListaFragment;

    .line 51
    :cond_e
    iget-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaAdapter;->agencias:Ljava/util/List;

    if-eqz v0, :cond_19

    .line 52
    iget-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaAdapter;->mapaListaFragment:Lcom/itau/empresas/feature/mapa/MapaListaFragment;

    iget-object v1, p0, Lcom/itau/empresas/feature/mapa/MapaAdapter;->agencias:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/mapa/MapaListaFragment;->setAgencias(Ljava/util/List;)V

    .line 53
    :cond_19
    iget-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaAdapter;->mapaListaFragment:Lcom/itau/empresas/feature/mapa/MapaListaFragment;

    return-object v0
.end method

.method private initTitlesList()[Ljava/lang/String;
    .registers 4

    .line 68
    iget-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaAdapter;->pages:[Ljava/lang/String;

    if-nez v0, :cond_21

    .line 69
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    iget-object v1, p0, Lcom/itau/empresas/feature/mapa/MapaAdapter;->activity:Landroid/app/Activity;

    const v2, 0x7f070459

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/itau/empresas/feature/mapa/MapaAdapter;->activity:Landroid/app/Activity;

    .line 70
    const v2, 0x7f070434

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    aput-object v1, v0, v2

    iput-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaAdapter;->pages:[Ljava/lang/String;

    .line 72
    :cond_21
    iget-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaAdapter;->pages:[Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public getCount()I
    .registers 2

    .line 58
    const/4 v0, 0x2

    return v0
.end method

.method public getItem(I)Landroid/support/v4/app/Fragment;
    .registers 3
    .param p1, "position"    # I

    .line 30
    sparse-switch p1, :sswitch_data_10

    goto :goto_e

    .line 32
    :sswitch_4
    invoke-direct {p0}, Lcom/itau/empresas/feature/mapa/MapaAdapter;->getMapaFragment()Landroid/support/v4/app/Fragment;

    move-result-object v0

    return-object v0

    .line 34
    :sswitch_9
    invoke-direct {p0}, Lcom/itau/empresas/feature/mapa/MapaAdapter;->getMapaListaFragment()Landroid/support/v4/app/Fragment;

    move-result-object v0

    return-object v0

    .line 36
    :goto_e
    const/4 v0, 0x0

    return-object v0

    :sswitch_data_10
    .sparse-switch
        0x0 -> :sswitch_4
        0x1 -> :sswitch_9
    .end sparse-switch
.end method

.method public getPageTitle(I)Ljava/lang/CharSequence;
    .registers 3
    .param p1, "position"    # I

    .line 63
    invoke-direct {p0}, Lcom/itau/empresas/feature/mapa/MapaAdapter;->initTitlesList()[Ljava/lang/String;

    .line 64
    iget-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaAdapter;->pages:[Ljava/lang/String;

    aget-object v0, v0, p1

    return-object v0
.end method

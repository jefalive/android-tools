.class public final Lcom/google/android/gms/ads/search/SearchAdRequest;
.super Ljava/lang/Object;


# static fields
.field public static final DEVICE_ID_EMULATOR:Ljava/lang/String;


# instance fields
.field private final zzOA:I

.field private final zzOB:I

.field private final zzOC:I

.field private final zzOD:I

.field private final zzOE:Ljava/lang/String;

.field private final zzOF:I

.field private final zzOG:Ljava/lang/String;

.field private final zzOH:I

.field private final zzOI:I

.field private final zzOJ:Ljava/lang/String;

.field private final zzOx:I

.field private final zzOy:I

.field private final zzOz:I

.field private final zzxO:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    sget-object v0, Lcom/google/android/gms/ads/internal/client/zzaa;->DEVICE_ID_EMULATOR:Ljava/lang/String;

    sput-object v0, Lcom/google/android/gms/ads/search/SearchAdRequest;->DEVICE_ID_EMULATOR:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getAnchorTextColor()I
    .registers 2

    iget v0, p0, Lcom/google/android/gms/ads/search/SearchAdRequest;->zzOx:I

    return v0
.end method

.method public getBackgroundColor()I
    .registers 2

    iget v0, p0, Lcom/google/android/gms/ads/search/SearchAdRequest;->zzxO:I

    return v0
.end method

.method public getBackgroundGradientBottom()I
    .registers 2

    iget v0, p0, Lcom/google/android/gms/ads/search/SearchAdRequest;->zzOy:I

    return v0
.end method

.method public getBackgroundGradientTop()I
    .registers 2

    iget v0, p0, Lcom/google/android/gms/ads/search/SearchAdRequest;->zzOz:I

    return v0
.end method

.method public getBorderColor()I
    .registers 2

    iget v0, p0, Lcom/google/android/gms/ads/search/SearchAdRequest;->zzOA:I

    return v0
.end method

.method public getBorderThickness()I
    .registers 2

    iget v0, p0, Lcom/google/android/gms/ads/search/SearchAdRequest;->zzOB:I

    return v0
.end method

.method public getBorderType()I
    .registers 2

    iget v0, p0, Lcom/google/android/gms/ads/search/SearchAdRequest;->zzOC:I

    return v0
.end method

.method public getCallButtonColor()I
    .registers 2

    iget v0, p0, Lcom/google/android/gms/ads/search/SearchAdRequest;->zzOD:I

    return v0
.end method

.method public getCustomChannels()Ljava/lang/String;
    .registers 2

    iget-object v0, p0, Lcom/google/android/gms/ads/search/SearchAdRequest;->zzOE:Ljava/lang/String;

    return-object v0
.end method

.method public getDescriptionTextColor()I
    .registers 2

    iget v0, p0, Lcom/google/android/gms/ads/search/SearchAdRequest;->zzOF:I

    return v0
.end method

.method public getFontFace()Ljava/lang/String;
    .registers 2

    iget-object v0, p0, Lcom/google/android/gms/ads/search/SearchAdRequest;->zzOG:Ljava/lang/String;

    return-object v0
.end method

.method public getHeaderTextColor()I
    .registers 2

    iget v0, p0, Lcom/google/android/gms/ads/search/SearchAdRequest;->zzOH:I

    return v0
.end method

.method public getHeaderTextSize()I
    .registers 2

    iget v0, p0, Lcom/google/android/gms/ads/search/SearchAdRequest;->zzOI:I

    return v0
.end method

.method public getQuery()Ljava/lang/String;
    .registers 2

    iget-object v0, p0, Lcom/google/android/gms/ads/search/SearchAdRequest;->zzOJ:Ljava/lang/String;

    return-object v0
.end method

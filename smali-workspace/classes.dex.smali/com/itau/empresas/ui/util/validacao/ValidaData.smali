.class public Lcom/itau/empresas/ui/util/validacao/ValidaData;
.super Ljava/lang/Object;
.source "ValidaData.java"


# static fields
.field private static formatadorBrasileiro:Lorg/threeten/bp/format/DateTimeFormatter;


# direct methods
.method public static dataSelecionadaEhMenorQueAtual(Ljava/lang/String;)Z
    .registers 5
    .param p0, "dataSelecionada"    # Ljava/lang/String;

    .line 19
    const-string v0, "dd/MM/yyyy"

    invoke-static {v0}, Lorg/threeten/bp/format/DateTimeFormatter;->ofPattern(Ljava/lang/String;)Lorg/threeten/bp/format/DateTimeFormatter;

    move-result-object v1

    .line 20
    .local v1, "formatter":Lorg/threeten/bp/format/DateTimeFormatter;
    invoke-static {p0, v1}, Lorg/threeten/bp/LocalDate;->parse(Ljava/lang/CharSequence;Lorg/threeten/bp/format/DateTimeFormatter;)Lorg/threeten/bp/LocalDate;

    move-result-object v2

    .line 21
    .local v2, "dateSelecionadaFormatada":Lorg/threeten/bp/LocalDate;
    invoke-static {}, Lcom/itau/empresas/ui/util/validacao/ValidaData;->hojeFormatoBrasileiro()Lorg/threeten/bp/LocalDate;

    move-result-object v0

    invoke-static {v0, v2}, Lorg/threeten/bp/Period;->between(Lorg/threeten/bp/LocalDate;Lorg/threeten/bp/LocalDate;)Lorg/threeten/bp/Period;

    move-result-object v3

    .line 23
    .local v3, "period":Lorg/threeten/bp/Period;
    invoke-virtual {v3}, Lorg/threeten/bp/Period;->getDays()I

    move-result v0

    if-gez v0, :cond_1a

    const/4 v0, 0x1

    goto :goto_1b

    :cond_1a
    const/4 v0, 0x0

    :goto_1b
    return v0
.end method

.method private static hojeFormatoBrasileiro()Lorg/threeten/bp/LocalDate;
    .registers 6

    .line 27
    sget-object v0, Lorg/threeten/bp/format/FormatStyle;->MEDIUM:Lorg/threeten/bp/format/FormatStyle;

    .line 28
    invoke-static {v0}, Lorg/threeten/bp/format/DateTimeFormatter;->ofLocalizedDate(Lorg/threeten/bp/format/FormatStyle;)Lorg/threeten/bp/format/DateTimeFormatter;

    move-result-object v0

    new-instance v1, Ljava/util/Locale;

    const-string v2, "pt"

    const-string v3, "br"

    invoke-direct {v1, v2, v3}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 29
    invoke-virtual {v0, v1}, Lorg/threeten/bp/format/DateTimeFormatter;->withLocale(Ljava/util/Locale;)Lorg/threeten/bp/format/DateTimeFormatter;

    move-result-object v0

    sput-object v0, Lcom/itau/empresas/ui/util/validacao/ValidaData;->formatadorBrasileiro:Lorg/threeten/bp/format/DateTimeFormatter;

    .line 30
    invoke-static {}, Lorg/threeten/bp/LocalDate;->now()Lorg/threeten/bp/LocalDate;

    move-result-object v4

    .line 31
    .local v4, "agora":Lorg/threeten/bp/LocalDate;
    sget-object v0, Lcom/itau/empresas/ui/util/validacao/ValidaData;->formatadorBrasileiro:Lorg/threeten/bp/format/DateTimeFormatter;

    invoke-virtual {v4, v0}, Lorg/threeten/bp/LocalDate;->format(Lorg/threeten/bp/format/DateTimeFormatter;)Ljava/lang/String;

    move-result-object v5

    .line 32
    .local v5, "dataFormatada":Ljava/lang/String;
    sget-object v0, Lcom/itau/empresas/ui/util/validacao/ValidaData;->formatadorBrasileiro:Lorg/threeten/bp/format/DateTimeFormatter;

    invoke-static {v5, v0}, Lorg/threeten/bp/LocalDate;->parse(Ljava/lang/CharSequence;Lorg/threeten/bp/format/DateTimeFormatter;)Lorg/threeten/bp/LocalDate;

    move-result-object v0

    return-object v0
.end method

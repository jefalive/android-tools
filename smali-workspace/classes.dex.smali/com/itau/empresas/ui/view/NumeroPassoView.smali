.class public Lcom/itau/empresas/ui/view/NumeroPassoView;
.super Landroid/widget/RelativeLayout;
.source "NumeroPassoView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/itau/empresas/ui/view/NumeroPassoView$Estado;
    }
.end annotation


# instance fields
.field private backgroundLevel:I

.field campoNumeroPasso:Landroid/widget/TextView;

.field campoTituloPasso:Landroid/widget/TextView;

.field imgSeta:Landroid/widget/ImageView;

.field private numeroPasso:I

.field private titulo:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 2
    .param p1, "context"    # Landroid/content/Context;

    .line 34
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 35
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .line 38
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 39
    invoke-direct {p0, p1, p2}, Lcom/itau/empresas/ui/view/NumeroPassoView;->init(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 40
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .line 43
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 44
    invoke-direct {p0, p1, p2}, Lcom/itau/empresas/ui/view/NumeroPassoView;->init(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 45
    return-void
.end method

.method private init(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .line 48
    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/NumeroPassoView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    sget-object v1, Lcom/itau/empresas/R$styleable;->NumeroPassoView_:[I

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, p2, v1, v2, v3}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v4

    .line 54
    .local v4, "a":Landroid/content/res/TypedArray;
    const/4 v0, 0x0

    const/4 v1, 0x1

    :try_start_12
    invoke-virtual {v4, v0, v1}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    iput v0, p0, Lcom/itau/empresas/ui/view/NumeroPassoView;->numeroPasso:I

    .line 55
    const/4 v0, 0x1

    invoke-virtual {v4, v0}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/ui/view/NumeroPassoView;->titulo:Ljava/lang/String;

    .line 56
    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-virtual {v4, v0, v1}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    iput v0, p0, Lcom/itau/empresas/ui/view/NumeroPassoView;->backgroundLevel:I
    :try_end_27
    .catchall {:try_start_12 .. :try_end_27} :catchall_2b

    .line 58
    invoke-virtual {v4}, Landroid/content/res/TypedArray;->recycle()V

    .line 59
    goto :goto_30

    .line 58
    :catchall_2b
    move-exception v5

    invoke-virtual {v4}, Landroid/content/res/TypedArray;->recycle()V

    throw v5

    .line 60
    :goto_30
    return-void
.end method

.method private setBackgroundLevel(I)V
    .registers 5
    .param p1, "level"    # I

    .line 76
    if-nez p1, :cond_2b

    .line 77
    iget-object v0, p0, Lcom/itau/empresas/ui/view/NumeroPassoView;->imgSeta:Landroid/widget/ImageView;

    const v1, 0x7f0201f1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 78
    iget-object v0, p0, Lcom/itau/empresas/ui/view/NumeroPassoView;->campoNumeroPasso:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/NumeroPassoView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c00cd

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 79
    iget-object v0, p0, Lcom/itau/empresas/ui/view/NumeroPassoView;->campoTituloPasso:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/NumeroPassoView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c00cd

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_7f

    .line 81
    :cond_2b
    const/4 v0, 0x1

    if-ne p1, v0, :cond_57

    .line 82
    iget-object v0, p0, Lcom/itau/empresas/ui/view/NumeroPassoView;->imgSeta:Landroid/widget/ImageView;

    const v1, 0x7f0201ee

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 83
    iget-object v0, p0, Lcom/itau/empresas/ui/view/NumeroPassoView;->campoNumeroPasso:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/NumeroPassoView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c0035

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 84
    iget-object v0, p0, Lcom/itau/empresas/ui/view/NumeroPassoView;->campoTituloPasso:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/NumeroPassoView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c0035

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_7f

    .line 87
    :cond_57
    iget-object v0, p0, Lcom/itau/empresas/ui/view/NumeroPassoView;->imgSeta:Landroid/widget/ImageView;

    const v1, 0x7f0201ee

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 88
    iget-object v0, p0, Lcom/itau/empresas/ui/view/NumeroPassoView;->campoNumeroPasso:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/NumeroPassoView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c0044

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 89
    iget-object v0, p0, Lcom/itau/empresas/ui/view/NumeroPassoView;->campoTituloPasso:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/NumeroPassoView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c0044

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 92
    :goto_7f
    return-void
.end method


# virtual methods
.method depoisDeCarregarONumeroDoPasso()V
    .registers 4

    .line 64
    iget-object v0, p0, Lcom/itau/empresas/ui/view/NumeroPassoView;->campoNumeroPasso:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/itau/empresas/ui/view/NumeroPassoView;->numeroPasso:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 65
    iget-object v0, p0, Lcom/itau/empresas/ui/view/NumeroPassoView;->campoTituloPasso:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/itau/empresas/ui/view/NumeroPassoView;->titulo:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 66
    iget v0, p0, Lcom/itau/empresas/ui/view/NumeroPassoView;->backgroundLevel:I

    invoke-direct {p0, v0}, Lcom/itau/empresas/ui/view/NumeroPassoView;->setBackgroundLevel(I)V

    .line 67
    return-void
.end method

.method public setBackgroundLevel(Lcom/itau/empresas/ui/view/NumeroPassoView$Estado;)V
    .registers 4
    .param p1, "estado"    # Lcom/itau/empresas/ui/view/NumeroPassoView$Estado;

    .line 70
    sget-object v0, Lcom/itau/empresas/ui/view/NumeroPassoView$Estado;->COMPLETADO:Lcom/itau/empresas/ui/view/NumeroPassoView$Estado;

    if-ne p1, v0, :cond_6

    const/4 v1, 0x0

    goto :goto_d

    :cond_6
    sget-object v0, Lcom/itau/empresas/ui/view/NumeroPassoView$Estado;->ATIVO:Lcom/itau/empresas/ui/view/NumeroPassoView$Estado;

    if-ne p1, v0, :cond_c

    const/4 v1, 0x1

    goto :goto_d

    :cond_c
    const/4 v1, 0x2

    .line 71
    .local v1, "level":I
    :goto_d
    invoke-direct {p0, v1}, Lcom/itau/empresas/ui/view/NumeroPassoView;->setBackgroundLevel(I)V

    .line 72
    return-void
.end method

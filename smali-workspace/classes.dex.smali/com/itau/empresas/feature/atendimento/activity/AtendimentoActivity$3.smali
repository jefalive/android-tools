.class Lcom/itau/empresas/feature/atendimento/activity/AtendimentoActivity$3;
.super Ljava/lang/Object;
.source "AtendimentoActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/itau/empresas/feature/atendimento/activity/AtendimentoActivity;->onEventMainThread(Lcom/itau/empresas/feature/atendimento/model/AlertaVO;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/itau/empresas/feature/atendimento/activity/AtendimentoActivity;

.field final synthetic val$alerta:Lcom/itau/empresas/feature/atendimento/model/AlertaVO;


# direct methods
.method constructor <init>(Lcom/itau/empresas/feature/atendimento/activity/AtendimentoActivity;Lcom/itau/empresas/feature/atendimento/model/AlertaVO;)V
    .registers 3
    .param p1, "this$0"    # Lcom/itau/empresas/feature/atendimento/activity/AtendimentoActivity;

    .line 218
    iput-object p1, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoActivity$3;->this$0:Lcom/itau/empresas/feature/atendimento/activity/AtendimentoActivity;

    iput-object p2, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoActivity$3;->val$alerta:Lcom/itau/empresas/feature/atendimento/model/AlertaVO;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .registers 6
    .param p1, "v"    # Landroid/view/View;

    .line 221
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoAlertasActivity;->intent(Landroid/content/Context;)Lcom/itau/empresas/feature/atendimento/activity/AtendimentoAlertasActivity_$IntentBuilder_;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoActivity$3;->this$0:Lcom/itau/empresas/feature/atendimento/activity/AtendimentoActivity;

    .line 222
    const v2, 0x7f0706fd

    invoke-virtual {v1, v2}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoActivity$3;->val$alerta:Lcom/itau/empresas/feature/atendimento/model/AlertaVO;

    .line 223
    invoke-virtual {v2}, Lcom/itau/empresas/feature/atendimento/model/AlertaVO;->getAlertas()Ljava/util/List;

    move-result-object v2

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/itau/empresas/feature/atendimento/model/AlertaFAQ;

    invoke-virtual {v2}, Lcom/itau/empresas/feature/atendimento/model/AlertaFAQ;->getTitulo_mobile()Ljava/lang/String;

    move-result-object v2

    .line 222
    invoke-virtual {v0, v1, v2}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoAlertasActivity_$IntentBuilder_;->extra(Ljava/lang/String;Ljava/lang/String;)Lorg/androidannotations/api/builder/IntentBuilder;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoAlertasActivity_$IntentBuilder_;

    iget-object v1, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoActivity$3;->this$0:Lcom/itau/empresas/feature/atendimento/activity/AtendimentoActivity;

    .line 224
    const v2, 0x7f07070d

    invoke-virtual {v1, v2}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoActivity$3;->val$alerta:Lcom/itau/empresas/feature/atendimento/model/AlertaVO;

    .line 225
    invoke-virtual {v2}, Lcom/itau/empresas/feature/atendimento/model/AlertaVO;->getAlertas()Ljava/util/List;

    move-result-object v2

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/itau/empresas/feature/atendimento/model/AlertaFAQ;

    invoke-virtual {v2}, Lcom/itau/empresas/feature/atendimento/model/AlertaFAQ;->getMensagem()Ljava/lang/String;

    move-result-object v2

    .line 224
    invoke-virtual {v0, v1, v2}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoAlertasActivity_$IntentBuilder_;->extra(Ljava/lang/String;Ljava/lang/String;)Lorg/androidannotations/api/builder/IntentBuilder;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoAlertasActivity_$IntentBuilder_;

    .line 226
    invoke-virtual {v0}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoAlertasActivity_$IntentBuilder_;->start()Lorg/androidannotations/api/builder/PostActivityStarter;

    .line 227
    return-void
.end method

.class public Lcom/itau/empresas/api/universal/model/DetalhesMultilimiteXMLVO;
.super Ljava/lang/Object;
.source "DetalhesMultilimiteXMLVO.java"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/itau/empresas/api/universal/model/DetalhesMultilimiteXMLVO$Detalhe;,
        Lcom/itau/empresas/api/universal/model/DetalhesMultilimiteXMLVO$Produto;,
        Lcom/itau/empresas/api/universal/model/DetalhesMultilimiteXMLVO$Dados;
    }
.end annotation

.annotation runtime Lorg/simpleframework/xml/Root;
    name = "RESPOSTA"
.end annotation


# instance fields
.field cabWeb:Lcom/itau/empresas/api/universal/model/CabWeb;
    .annotation runtime Lorg/simpleframework/xml/Element;
        name = "CAB_WEB"
        required = false
    .end annotation
.end field

.field dados:Lcom/itau/empresas/api/universal/model/DetalhesMultilimiteXMLVO$Dados;
    .annotation runtime Lorg/simpleframework/xml/Element;
        name = "DADOS"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getDados()Lcom/itau/empresas/api/universal/model/DetalhesMultilimiteXMLVO$Dados;
    .registers 2

    .line 20
    iget-object v0, p0, Lcom/itau/empresas/api/universal/model/DetalhesMultilimiteXMLVO;->dados:Lcom/itau/empresas/api/universal/model/DetalhesMultilimiteXMLVO$Dados;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    .line 29
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "DetalhesMultilimiteXMLVO{cabWeb="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/api/universal/model/DetalhesMultilimiteXMLVO;->cabWeb:Lcom/itau/empresas/api/universal/model/CabWeb;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", dados="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/api/universal/model/DetalhesMultilimiteXMLVO;->dados:Lcom/itau/empresas/api/universal/model/DetalhesMultilimiteXMLVO$Dados;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

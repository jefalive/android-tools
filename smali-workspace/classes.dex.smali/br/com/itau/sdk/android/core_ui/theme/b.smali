.class public final Lbr/com/itau/sdk/android/core_ui/theme/b;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a()I
    .registers 3

    .line 31
    invoke-static {}, Lbr/com/itau/sdk/android/core_ui/theme/b;->n()Lbr/com/itau/sdk/android/core/theme/BaseSDKThemeManager;

    move-result-object v0

    sget v1, Lbr/com/itau/sdk/android/core_ui/R$attr;->itausdkcore_custom_primary_color:I

    sget v2, Lbr/com/itau/sdk/android/core_ui/R$color;->itausdkcore_ok_background_color:I

    invoke-virtual {v0, v1, v2}, Lbr/com/itau/sdk/android/core/theme/BaseSDKThemeManager;->getColor(II)I

    move-result v0

    return v0
.end method

.method public static a(Landroid/content/Context;)Landroid/support/v7/app/AlertDialog$Builder;
    .registers 3

    .line 86
    new-instance v0, Landroid/support/v7/app/AlertDialog$Builder;

    sget v1, Lbr/com/itau/sdk/android/core_ui/R$style;->itausdkcore_TokenDialogError:I

    invoke-direct {v0, p0, v1}, Landroid/support/v7/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    return-object v0
.end method

.method public static a(Landroid/app/Dialog;)V
    .registers 5

    .line 108
    invoke-static {}, Lbr/com/itau/sdk/android/core_ui/theme/b;->g()I

    move-result v2

    .line 109
    if-eqz v2, :cond_1d

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_1d

    .line 110
    invoke-virtual {p0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v3

    .line 111
    const/high16 v0, -0x80000000

    invoke-virtual {v3, v0}, Landroid/view/Window;->addFlags(I)V

    .line 112
    const/high16 v0, 0x4000000

    invoke-virtual {v3, v0}, Landroid/view/Window;->clearFlags(I)V

    .line 113
    invoke-virtual {v3, v2}, Landroid/view/Window;->setStatusBarColor(I)V

    .line 115
    :cond_1d
    return-void
.end method

.method public static a(Landroid/support/v7/app/AlertDialog;)V
    .registers 2

    .line 104
    invoke-static {}, Lbr/com/itau/sdk/android/core_ui/theme/b;->a()I

    move-result v0

    invoke-static {p0, v0}, Lbr/com/itau/sdk/android/core_ui/theme/b;->a(Landroid/support/v7/app/AlertDialog;I)V

    .line 105
    return-void
.end method

.method private static a(Landroid/support/v7/app/AlertDialog;I)V
    .registers 3

    .line 132
    if-nez p0, :cond_3

    .line 133
    return-void

    .line 135
    :cond_3
    const/4 v0, -0x2

    invoke-static {p0, v0, p1}, Lbr/com/itau/sdk/android/core_ui/theme/b;->a(Landroid/support/v7/app/AlertDialog;II)V

    .line 136
    const/4 v0, -0x1

    invoke-static {p0, v0, p1}, Lbr/com/itau/sdk/android/core_ui/theme/b;->a(Landroid/support/v7/app/AlertDialog;II)V

    .line 137
    const/4 v0, -0x3

    invoke-static {p0, v0, p1}, Lbr/com/itau/sdk/android/core_ui/theme/b;->a(Landroid/support/v7/app/AlertDialog;II)V

    .line 138
    return-void
.end method

.method private static a(Landroid/support/v7/app/AlertDialog;II)V
    .registers 6

    .line 141
    if-nez p0, :cond_3

    .line 142
    return-void

    .line 144
    :cond_3
    invoke-virtual {p0, p1}, Landroid/support/v7/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v2

    .line 145
    if-eqz v2, :cond_1f

    .line 146
    invoke-virtual {v2, p2}, Landroid/widget/Button;->setTextColor(I)V

    .line 147
    invoke-static {}, Lbr/com/itau/sdk/android/core_ui/theme/SDKThemeManager;->getInstance()Lbr/com/itau/sdk/android/core_ui/theme/SDKThemeManager;

    move-result-object v0

    invoke-virtual {p0}, Landroid/support/v7/app/AlertDialog;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v1

    invoke-static {v0, v1}, Lbr/com/itau/sdk/android/core/util/TypefaceUtils;->getFontBold(Lbr/com/itau/sdk/android/core/theme/BaseSDKThemeManager;Landroid/content/res/AssetManager;)Landroid/graphics/Typeface;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/Button;->setTypeface(Landroid/graphics/Typeface;)V

    .line 149
    :cond_1f
    return-void
.end method

.method public static a(Landroid/view/View;)V
    .registers 2

    .line 90
    invoke-static {}, Lbr/com/itau/sdk/android/core_ui/theme/b;->a()I

    move-result v0

    invoke-virtual {p0, v0}, Landroid/view/View;->setBackgroundColor(I)V

    .line 91
    return-void
.end method

.method public static a(Landroid/widget/ImageView;)V
    .registers 2

    .line 100
    invoke-static {}, Lbr/com/itau/sdk/android/core_ui/theme/b;->a()I

    move-result v0

    invoke-virtual {p0, v0}, Landroid/widget/ImageView;->setColorFilter(I)V

    .line 101
    return-void
.end method

.method public static a(Landroid/widget/ProgressBar;)V
    .registers 4

    .line 94
    .line 95
    invoke-virtual {p0}, Landroid/widget/ProgressBar;->getIndeterminateDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 96
    invoke-static {}, Lbr/com/itau/sdk/android/core_ui/theme/b;->a()I

    move-result v1

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->MULTIPLY:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 97
    return-void
.end method

.method public static b()I
    .registers 3

    .line 36
    invoke-static {}, Lbr/com/itau/sdk/android/core_ui/theme/b;->n()Lbr/com/itau/sdk/android/core/theme/BaseSDKThemeManager;

    move-result-object v0

    sget v1, Lbr/com/itau/sdk/android/core_ui/R$attr;->itausdkcore_custom_icon_sms_token:I

    sget v2, Lbr/com/itau/sdk/android/core_ui/R$drawable;->ic_token_sms_itau:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lbr/com/itau/sdk/android/core/theme/BaseSDKThemeManager;->getValue(ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public static b(Landroid/view/View;)V
    .registers 6

    .line 118
    instance-of v0, p0, Landroid/view/ViewGroup;

    if-eqz v0, :cond_32

    move-object v0, p0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    if-lez v0, :cond_32

    .line 119
    move-object v0, p0

    check-cast v0, Landroid/view/ViewGroup;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 120
    instance-of v0, v2, Landroid/widget/ProgressBar;

    if-eqz v0, :cond_32

    .line 121
    move-object v3, v2

    check-cast v3, Landroid/widget/ProgressBar;

    .line 122
    invoke-static {v3}, Lbr/com/itau/sdk/android/core_ui/theme/b;->a(Landroid/widget/ProgressBar;)V

    .line 123
    invoke-virtual {v3}, Landroid/widget/ProgressBar;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    .line 124
    invoke-static {}, Lbr/com/itau/sdk/android/core_ui/theme/b;->j()I

    move-result v0

    iput v0, v4, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 125
    invoke-static {}, Lbr/com/itau/sdk/android/core_ui/theme/b;->k()I

    move-result v0

    iput v0, v4, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 126
    invoke-virtual {v3, v4}, Landroid/widget/ProgressBar;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 129
    :cond_32
    return-void
.end method

.method public static c()I
    .registers 3

    .line 40
    invoke-static {}, Lbr/com/itau/sdk/android/core_ui/theme/b;->n()Lbr/com/itau/sdk/android/core/theme/BaseSDKThemeManager;

    move-result-object v0

    sget v1, Lbr/com/itau/sdk/android/core_ui/R$attr;->itausdkcore_custom_icon_aplicativo_token:I

    sget v2, Lbr/com/itau/sdk/android/core_ui/R$drawable;->ic_token_aplicativo_itau:I

    .line 41
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 40
    invoke-virtual {v0, v1, v2}, Lbr/com/itau/sdk/android/core/theme/BaseSDKThemeManager;->getValue(ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public static d()I
    .registers 3

    .line 45
    invoke-static {}, Lbr/com/itau/sdk/android/core_ui/theme/b;->n()Lbr/com/itau/sdk/android/core/theme/BaseSDKThemeManager;

    move-result-object v0

    sget v1, Lbr/com/itau/sdk/android/core_ui/R$attr;->itausdkcore_custom_icon_chaveiro_token:I

    sget v2, Lbr/com/itau/sdk/android/core_ui/R$drawable;->ic_token_chaveiro_itau:I

    .line 46
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 45
    invoke-virtual {v0, v1, v2}, Lbr/com/itau/sdk/android/core/theme/BaseSDKThemeManager;->getValue(ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public static e()I
    .registers 3

    .line 50
    invoke-static {}, Lbr/com/itau/sdk/android/core_ui/theme/b;->n()Lbr/com/itau/sdk/android/core/theme/BaseSDKThemeManager;

    move-result-object v0

    sget v1, Lbr/com/itau/sdk/android/core_ui/R$attr;->itausdkcore_custom_icon_back:I

    sget v2, Lbr/com/itau/sdk/android/core_ui/R$drawable;->itausdkcore_ic_seta:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lbr/com/itau/sdk/android/core/theme/BaseSDKThemeManager;->getValue(ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public static f()I
    .registers 3

    .line 54
    invoke-static {}, Lbr/com/itau/sdk/android/core_ui/theme/b;->n()Lbr/com/itau/sdk/android/core/theme/BaseSDKThemeManager;

    move-result-object v0

    sget v1, Lbr/com/itau/sdk/android/core_ui/R$attr;->itausdkcore_custom_icon_close:I

    sget v2, Lbr/com/itau/sdk/android/core_ui/R$drawable;->itausdkcore_ic_fechar:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lbr/com/itau/sdk/android/core/theme/BaseSDKThemeManager;->getValue(ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public static g()I
    .registers 3

    .line 58
    invoke-static {}, Lbr/com/itau/sdk/android/core_ui/theme/b;->n()Lbr/com/itau/sdk/android/core/theme/BaseSDKThemeManager;

    move-result-object v0

    sget v1, Lbr/com/itau/sdk/android/core_ui/R$attr;->itausdkcore_custom_status_bar_color:I

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lbr/com/itau/sdk/android/core/theme/BaseSDKThemeManager;->getValue(ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public static h()I
    .registers 3

    .line 62
    invoke-static {}, Lbr/com/itau/sdk/android/core_ui/theme/b;->n()Lbr/com/itau/sdk/android/core/theme/BaseSDKThemeManager;

    move-result-object v0

    sget v1, Lbr/com/itau/sdk/android/core_ui/R$attr;->itausdkcore_fingerprint_icon:I

    sget v2, Lbr/com/itau/sdk/android/core_ui/R$drawable;->itausdkcore_ic_finger_print_branco:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lbr/com/itau/sdk/android/core/theme/BaseSDKThemeManager;->getValue(ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public static i()I
    .registers 3

    .line 66
    invoke-static {}, Lbr/com/itau/sdk/android/core_ui/theme/b;->n()Lbr/com/itau/sdk/android/core/theme/BaseSDKThemeManager;

    move-result-object v0

    sget v1, Lbr/com/itau/sdk/android/core_ui/R$attr;->itausdkcore_eletronic_password_icon:I

    sget v2, Lbr/com/itau/sdk/android/core_ui/R$drawable;->itausdkcore_ic_senha_branca:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lbr/com/itau/sdk/android/core/theme/BaseSDKThemeManager;->getValue(ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public static j()I
    .registers 3

    .line 70
    invoke-static {}, Lbr/com/itau/sdk/android/core_ui/theme/b;->n()Lbr/com/itau/sdk/android/core/theme/BaseSDKThemeManager;

    move-result-object v0

    sget v1, Lbr/com/itau/sdk/android/core_ui/R$attr;->itausdkcore_custom_loading_width:I

    sget v2, Lbr/com/itau/sdk/android/core_ui/R$dimen;->itausdkcore_default_loading_width:I

    .line 71
    invoke-virtual {v0, v1, v2}, Lbr/com/itau/sdk/android/core/theme/BaseSDKThemeManager;->getDimension(II)I

    move-result v0

    .line 70
    return v0
.end method

.method public static k()I
    .registers 3

    .line 76
    invoke-static {}, Lbr/com/itau/sdk/android/core_ui/theme/b;->n()Lbr/com/itau/sdk/android/core/theme/BaseSDKThemeManager;

    move-result-object v0

    sget v1, Lbr/com/itau/sdk/android/core_ui/R$attr;->itausdkcore_custom_loading_height:I

    sget v2, Lbr/com/itau/sdk/android/core_ui/R$dimen;->itausdkcore_default_loading_height:I

    .line 77
    invoke-virtual {v0, v1, v2}, Lbr/com/itau/sdk/android/core/theme/BaseSDKThemeManager;->getDimension(II)I

    move-result v0

    .line 76
    return v0
.end method

.method private static n()Lbr/com/itau/sdk/android/core/theme/BaseSDKThemeManager;
    .registers 1

    .line 157
    invoke-static {}, Lbr/com/itau/sdk/android/core_ui/theme/SDKThemeManager;->getInstance()Lbr/com/itau/sdk/android/core_ui/theme/SDKThemeManager;

    move-result-object v0

    return-object v0
.end method

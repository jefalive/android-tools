.class Lcom/itau/empresas/feature/login/LoginActivity$1;
.super Ljava/lang/Object;
.source "LoginActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/itau/empresas/feature/login/LoginActivity;->onEventMainThread(Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintException;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/itau/empresas/feature/login/LoginActivity;

.field final synthetic val$dialog:Lcom/itau/empresas/ui/dialog/CustomAlertDialog;


# direct methods
.method constructor <init>(Lcom/itau/empresas/feature/login/LoginActivity;Lcom/itau/empresas/ui/dialog/CustomAlertDialog;)V
    .registers 3
    .param p1, "this$0"    # Lcom/itau/empresas/feature/login/LoginActivity;

    .line 278
    iput-object p1, p0, Lcom/itau/empresas/feature/login/LoginActivity$1;->this$0:Lcom/itau/empresas/feature/login/LoginActivity;

    iput-object p2, p0, Lcom/itau/empresas/feature/login/LoginActivity$1;->val$dialog:Lcom/itau/empresas/ui/dialog/CustomAlertDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .registers 4
    .param p1, "view"    # Landroid/view/View;

    .line 281
    iget-object v0, p0, Lcom/itau/empresas/feature/login/LoginActivity$1;->val$dialog:Lcom/itau/empresas/ui/dialog/CustomAlertDialog;

    invoke-virtual {v0}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog;->dismiss()V

    .line 282
    iget-object v0, p0, Lcom/itau/empresas/feature/login/LoginActivity$1;->this$0:Lcom/itau/empresas/feature/login/LoginActivity;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/login/LoginActivity;->escondeDialogoDeProgresso()V

    .line 283
    iget-object v0, p0, Lcom/itau/empresas/feature/login/LoginActivity$1;->this$0:Lcom/itau/empresas/feature/login/LoginActivity;

    iget-object v0, v0, Lcom/itau/empresas/feature/login/LoginActivity;->loginFingerPrint:Lcom/itau/empresas/feature/login/LoginFingerPrint;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/login/LoginFingerPrint;->setLogarComTeclado(Z)V

    .line 284
    iget-object v0, p0, Lcom/itau/empresas/feature/login/LoginActivity$1;->this$0:Lcom/itau/empresas/feature/login/LoginActivity;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/login/LoginActivity;->chamaAPITeclado()V

    .line 285
    iget-object v0, p0, Lcom/itau/empresas/feature/login/LoginActivity$1;->this$0:Lcom/itau/empresas/feature/login/LoginActivity;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/login/LoginActivity;->mostraDialogoDeProgresso()V

    .line 286
    return-void
.end method

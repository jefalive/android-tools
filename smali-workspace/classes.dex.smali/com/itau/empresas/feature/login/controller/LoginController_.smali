.class public final Lcom/itau/empresas/feature/login/controller/LoginController_;
.super Lcom/itau/empresas/feature/login/controller/LoginController;
.source "LoginController_.java"


# instance fields
.field private context_:Landroid/content/Context;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .registers 2
    .param p1, "context"    # Landroid/content/Context;

    .line 18
    invoke-direct {p0}, Lcom/itau/empresas/feature/login/controller/LoginController;-><init>()V

    .line 19
    iput-object p1, p0, Lcom/itau/empresas/feature/login/controller/LoginController_;->context_:Landroid/content/Context;

    .line 20
    invoke-direct {p0}, Lcom/itau/empresas/feature/login/controller/LoginController_;->init_()V

    .line 21
    return-void
.end method

.method public static getInstance_(Landroid/content/Context;)Lcom/itau/empresas/feature/login/controller/LoginController_;
    .registers 2
    .param p0, "context"    # Landroid/content/Context;

    .line 24
    new-instance v0, Lcom/itau/empresas/feature/login/controller/LoginController_;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/login/controller/LoginController_;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method private init_()V
    .registers 2

    .line 28
    invoke-static {}, Lcom/itau/empresas/CustomApplication_;->getInstance()Lcom/itau/empresas/CustomApplication;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/login/controller/LoginController_;->app:Lcom/itau/empresas/CustomApplication;

    .line 29
    iget-object v0, p0, Lcom/itau/empresas/feature/login/controller/LoginController_;->context_:Landroid/content/Context;

    invoke-static {v0}, Lcom/itau/empresas/feature/login/controller/MenuController_;->getInstance_(Landroid/content/Context;)Lcom/itau/empresas/feature/login/controller/MenuController_;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/login/controller/LoginController_;->menuController:Lcom/itau/empresas/feature/login/controller/MenuController;

    .line 30
    iget-object v0, p0, Lcom/itau/empresas/feature/login/controller/LoginController_;->context_:Landroid/content/Context;

    invoke-static {v0}, Lcom/itau/empresas/feature/login/controller/OperadorController_;->getInstance_(Landroid/content/Context;)Lcom/itau/empresas/feature/login/controller/OperadorController_;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/login/controller/LoginController_;->operadorController:Lcom/itau/empresas/feature/login/controller/OperadorController;

    .line 31
    return-void
.end method

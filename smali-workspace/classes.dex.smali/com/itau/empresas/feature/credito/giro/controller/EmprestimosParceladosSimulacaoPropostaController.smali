.class public Lcom/itau/empresas/feature/credito/giro/controller/EmprestimosParceladosSimulacaoPropostaController;
.super Lcom/itau/empresas/controller/BaseController;
.source "EmprestimosParceladosSimulacaoPropostaController.java"


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 14
    invoke-direct {p0}, Lcom/itau/empresas/controller/BaseController;-><init>()V

    return-void
.end method

.method static synthetic access$000(Ljava/lang/Object;)V
    .registers 1
    .param p0, "x0"    # Ljava/lang/Object;

    .line 14
    invoke-static {p0}, Lcom/itau/empresas/feature/credito/giro/controller/EmprestimosParceladosSimulacaoPropostaController;->post(Ljava/lang/Object;)V

    return-void
.end method

.method public static getApi()Lcom/itau/empresas/api/credito/ApiGiro;
    .registers 1

    .line 17
    const-class v0, Lcom/itau/empresas/api/credito/ApiGiro;

    invoke-static {v0}, Lbr/com/itau/sdk/android/core/BackendClient;->api(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/api/credito/ApiGiro;

    return-object v0
.end method


# virtual methods
.method public consultarSimulacaoProposta(Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;)V
    .registers 5
    .param p1, "simulacao"    # Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;

    .line 26
    .line 27
    :try_start_0
    invoke-virtual {p1}, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;->clone()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;

    .line 29
    .local v2, "aceita":Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;
    const/4 v0, 0x0

    invoke-virtual {v2, v0}, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;->setOfertaProdutosParceladosSaidaVO(Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosSaidaVO;)V

    .line 30
    const/4 v0, 0x0

    invoke-virtual {v2, v0}, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;->setOfertaEspecial(Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO$OfertaEspecialAceita;)V

    .line 31
    const/4 v0, 0x0

    invoke-virtual {v2, v0}, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;->setQuantidadeParcelas(Ljava/lang/Integer;)V

    .line 33
    new-instance v0, Lcom/itau/empresas/feature/credito/giro/controller/EmprestimosParceladosSimulacaoPropostaController$1;

    invoke-direct {v0, p0, v2}, Lcom/itau/empresas/feature/credito/giro/controller/EmprestimosParceladosSimulacaoPropostaController$1;-><init>(Lcom/itau/empresas/feature/credito/giro/controller/EmprestimosParceladosSimulacaoPropostaController;Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;)V

    invoke-static {v0}, Lbr/com/itau/sdk/android/core/BackendClient;->callApi(Lbr/com/itau/sdk/android/core/type/BackendCall;)V
    :try_end_1b
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_1b} :catch_1c

    .line 42
    .end local v2    # "aceita":Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;
    goto :goto_2c

    .line 40
    :catch_1c
    move-exception v2

    .line 41
    .local v2, "e":Ljava/lang/CloneNotSupportedException;
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2}, Ljava/lang/CloneNotSupportedException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v2}, Lbr/com/itau/textovoz/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 43
    .end local v2    # "e":Ljava/lang/CloneNotSupportedException;
    :goto_2c
    return-void
.end method

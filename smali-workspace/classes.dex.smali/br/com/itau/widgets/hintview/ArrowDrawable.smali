.class final Lbr/com/itau/widgets/hintview/ArrowDrawable;
.super Landroid/graphics/drawable/ColorDrawable;
.source "ArrowDrawable.java"


# instance fields
.field private final mBackgroundColor:I

.field private final mGravity:I

.field private final mPaint:Landroid/graphics/Paint;

.field private mPath:Landroid/graphics/Path;


# direct methods
.method constructor <init>(II)V
    .registers 5
    .param p1, "foregroundColor"    # I
    .param p2, "gravity"    # I

    .line 49
    invoke-direct {p0}, Landroid/graphics/drawable/ColorDrawable;-><init>()V

    .line 43
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lbr/com/itau/widgets/hintview/ArrowDrawable;->mPaint:Landroid/graphics/Paint;

    .line 50
    invoke-static {p2}, Lbr/com/itau/widgets/hintview/Util;->gravityToArrowDirection(I)I

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/hintview/ArrowDrawable;->mGravity:I

    .line 51
    const/4 v0, 0x0

    iput v0, p0, Lbr/com/itau/widgets/hintview/ArrowDrawable;->mBackgroundColor:I

    .line 53
    iget-object v0, p0, Lbr/com/itau/widgets/hintview/ArrowDrawable;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 54
    return-void
.end method

.method private declared-synchronized updatePath(Landroid/graphics/Rect;)V
    .registers 6
    .param p1, "bounds"    # Landroid/graphics/Rect;

    monitor-enter p0

    .line 63
    :try_start_1
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lbr/com/itau/widgets/hintview/ArrowDrawable;->mPath:Landroid/graphics/Path;

    .line 65
    iget v0, p0, Lbr/com/itau/widgets/hintview/ArrowDrawable;->mGravity:I

    sparse-switch v0, :sswitch_data_10a

    goto/16 :goto_103

    .line 67
    :sswitch_f
    iget-object v0, p0, Lbr/com/itau/widgets/hintview/ArrowDrawable;->mPath:Landroid/graphics/Path;

    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 68
    iget-object v0, p0, Lbr/com/itau/widgets/hintview/ArrowDrawable;->mPath:Landroid/graphics/Path;

    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    const/4 v2, 0x0

    invoke-virtual {v0, v2, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 69
    iget-object v0, p0, Lbr/com/itau/widgets/hintview/ArrowDrawable;->mPath:Landroid/graphics/Path;

    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v1

    int-to-float v1, v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 70
    iget-object v0, p0, Lbr/com/itau/widgets/hintview/ArrowDrawable;->mPath:Landroid/graphics/Path;

    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 71
    goto/16 :goto_103

    .line 73
    :sswitch_47
    iget-object v0, p0, Lbr/com/itau/widgets/hintview/ArrowDrawable;->mPath:Landroid/graphics/Path;

    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v1

    int-to-float v1, v1

    const/4 v2, 0x0

    invoke-virtual {v0, v2, v1}, Landroid/graphics/Path;->moveTo(FF)V

    .line 74
    iget-object v0, p0, Lbr/com/itau/widgets/hintview/ArrowDrawable;->mPath:Landroid/graphics/Path;

    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 75
    iget-object v0, p0, Lbr/com/itau/widgets/hintview/ArrowDrawable;->mPath:Landroid/graphics/Path;

    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 76
    iget-object v0, p0, Lbr/com/itau/widgets/hintview/ArrowDrawable;->mPath:Landroid/graphics/Path;

    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v1

    int-to-float v1, v1

    const/4 v2, 0x0

    invoke-virtual {v0, v2, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 77
    goto/16 :goto_103

    .line 79
    :sswitch_7b
    iget-object v0, p0, Lbr/com/itau/widgets/hintview/ArrowDrawable;->mPath:Landroid/graphics/Path;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 80
    iget-object v0, p0, Lbr/com/itau/widgets/hintview/ArrowDrawable;->mPath:Landroid/graphics/Path;

    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 81
    iget-object v0, p0, Lbr/com/itau/widgets/hintview/ArrowDrawable;->mPath:Landroid/graphics/Path;

    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v1

    int-to-float v1, v1

    const/4 v2, 0x0

    invoke-virtual {v0, v2, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 82
    iget-object v0, p0, Lbr/com/itau/widgets/hintview/ArrowDrawable;->mPath:Landroid/graphics/Path;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 83
    goto :goto_103

    .line 85
    :sswitch_a6
    iget-object v0, p0, Lbr/com/itau/widgets/hintview/ArrowDrawable;->mPath:Landroid/graphics/Path;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 86
    iget-object v0, p0, Lbr/com/itau/widgets/hintview/ArrowDrawable;->mPath:Landroid/graphics/Path;

    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 87
    iget-object v0, p0, Lbr/com/itau/widgets/hintview/ArrowDrawable;->mPath:Landroid/graphics/Path;

    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v1

    int-to-float v1, v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 88
    iget-object v0, p0, Lbr/com/itau/widgets/hintview/ArrowDrawable;->mPath:Landroid/graphics/Path;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 89
    goto :goto_103

    .line 91
    :sswitch_d1
    iget-object v0, p0, Lbr/com/itau/widgets/hintview/ArrowDrawable;->mPath:Landroid/graphics/Path;

    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v1

    int-to-float v1, v1

    const/4 v2, 0x0

    invoke-virtual {v0, v2, v1}, Landroid/graphics/Path;->moveTo(FF)V

    .line 92
    iget-object v0, p0, Lbr/com/itau/widgets/hintview/ArrowDrawable;->mPath:Landroid/graphics/Path;

    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 93
    iget-object v0, p0, Lbr/com/itau/widgets/hintview/ArrowDrawable;->mPath:Landroid/graphics/Path;

    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 94
    iget-object v0, p0, Lbr/com/itau/widgets/hintview/ArrowDrawable;->mPath:Landroid/graphics/Path;

    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v1

    int-to-float v1, v1

    const/4 v2, 0x0

    invoke-virtual {v0, v2, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 98
    :goto_103
    iget-object v0, p0, Lbr/com/itau/widgets/hintview/ArrowDrawable;->mPath:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->close()V
    :try_end_108
    .catchall {:try_start_1 .. :try_end_108} :catchall_120

    .line 99
    monitor-exit p0

    return-void

    :sswitch_data_10a
    .sparse-switch
        0x7 -> :sswitch_d1
        0x30 -> :sswitch_47
        0x50 -> :sswitch_a6
        0x800003 -> :sswitch_f
        0x800005 -> :sswitch_7b
    .end sparse-switch

    :catchall_120
    move-exception p1

    monitor-exit p0

    throw p1
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;)V
    .registers 4
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .line 103
    iget v0, p0, Lbr/com/itau/widgets/hintview/ArrowDrawable;->mBackgroundColor:I

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 104
    iget-object v0, p0, Lbr/com/itau/widgets/hintview/ArrowDrawable;->mPath:Landroid/graphics/Path;

    if-nez v0, :cond_10

    .line 105
    invoke-virtual {p0}, Lbr/com/itau/widgets/hintview/ArrowDrawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    invoke-direct {p0, v0}, Lbr/com/itau/widgets/hintview/ArrowDrawable;->updatePath(Landroid/graphics/Rect;)V

    .line 107
    :cond_10
    iget-object v0, p0, Lbr/com/itau/widgets/hintview/ArrowDrawable;->mPath:Landroid/graphics/Path;

    iget-object v1, p0, Lbr/com/itau/widgets/hintview/ArrowDrawable;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 108
    return-void
.end method

.method public getOpacity()I
    .registers 3

    .line 126
    iget-object v0, p0, Lbr/com/itau/widgets/hintview/ArrowDrawable;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getColorFilter()Landroid/graphics/ColorFilter;

    move-result-object v0

    if-eqz v0, :cond_a

    .line 127
    const/4 v0, -0x3

    return v0

    .line 130
    :cond_a
    iget-object v0, p0, Lbr/com/itau/widgets/hintview/ArrowDrawable;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getColor()I

    move-result v0

    ushr-int/lit8 v0, v0, 0x18

    sparse-switch v0, :sswitch_data_1c

    goto :goto_1a

    .line 132
    :sswitch_16
    const/4 v0, -0x1

    return v0

    .line 134
    :sswitch_18
    const/4 v0, -0x2

    return v0

    .line 136
    :goto_1a
    const/4 v0, -0x3

    return v0

    :sswitch_data_1c
    .sparse-switch
        0x0 -> :sswitch_18
        0xff -> :sswitch_16
    .end sparse-switch
.end method

.method protected onBoundsChange(Landroid/graphics/Rect;)V
    .registers 2
    .param p1, "bounds"    # Landroid/graphics/Rect;

    .line 58
    invoke-super {p0, p1}, Landroid/graphics/drawable/ColorDrawable;->onBoundsChange(Landroid/graphics/Rect;)V

    .line 59
    invoke-direct {p0, p1}, Lbr/com/itau/widgets/hintview/ArrowDrawable;->updatePath(Landroid/graphics/Rect;)V

    .line 60
    return-void
.end method

.method public setAlpha(I)V
    .registers 3
    .param p1, "alpha"    # I

    .line 112
    iget-object v0, p0, Lbr/com/itau/widgets/hintview/ArrowDrawable;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 113
    return-void
.end method

.method public setColor(I)V
    .registers 3
    .param p1, "color"    # I

    .line 116
    iget-object v0, p0, Lbr/com/itau/widgets/hintview/ArrowDrawable;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 117
    return-void
.end method

.method public setColorFilter(Landroid/graphics/ColorFilter;)V
    .registers 3
    .param p1, "colorFilter"    # Landroid/graphics/ColorFilter;

    .line 121
    iget-object v0, p0, Lbr/com/itau/widgets/hintview/ArrowDrawable;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    .line 122
    return-void
.end method

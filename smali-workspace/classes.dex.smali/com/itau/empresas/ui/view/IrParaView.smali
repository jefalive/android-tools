.class public Lcom/itau/empresas/ui/view/IrParaView;
.super Landroid/widget/LinearLayout;
.source "IrParaView.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 2
    .param p1, "context"    # Landroid/content/Context;

    .line 18
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 19
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .line 22
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 23
    return-void
.end method


# virtual methods
.method aoClicarEmContaCorrente()V
    .registers 4

    .line 32
    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/IrParaView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/itau/empresas/feature/home/HomeLogadaActivity;->intent(Landroid/content/Context;)Lcom/itau/empresas/feature/home/HomeLogadaActivity_$IntentBuilder_;

    move-result-object v0

    const-string v1, "EXTRA_EXIBIR_EXTRATO"

    .line 33
    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/itau/empresas/feature/home/HomeLogadaActivity_$IntentBuilder_;->extra(Ljava/lang/String;Z)Lorg/androidannotations/api/builder/IntentBuilder;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/feature/home/HomeLogadaActivity_$IntentBuilder_;

    .line 34
    invoke-virtual {v0}, Lcom/itau/empresas/feature/home/HomeLogadaActivity_$IntentBuilder_;->start()Lorg/androidannotations/api/builder/PostActivityStarter;

    .line 35
    return-void
.end method

.method aoClicarEmInicio()V
    .registers 2

    .line 27
    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/IrParaView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/itau/empresas/feature/home/HomeLogadaActivity;->intent(Landroid/content/Context;)Lcom/itau/empresas/feature/home/HomeLogadaActivity_$IntentBuilder_;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/feature/home/HomeLogadaActivity_$IntentBuilder_;->start()Lorg/androidannotations/api/builder/PostActivityStarter;

    .line 28
    return-void
.end method

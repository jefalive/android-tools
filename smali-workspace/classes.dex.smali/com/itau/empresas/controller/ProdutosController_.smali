.class public final Lcom/itau/empresas/controller/ProdutosController_;
.super Lcom/itau/empresas/controller/ProdutosController;
.source "ProdutosController_.java"


# instance fields
.field private context_:Landroid/content/Context;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .registers 2
    .param p1, "context"    # Landroid/content/Context;

    .line 18
    invoke-direct {p0}, Lcom/itau/empresas/controller/ProdutosController;-><init>()V

    .line 19
    iput-object p1, p0, Lcom/itau/empresas/controller/ProdutosController_;->context_:Landroid/content/Context;

    .line 20
    invoke-direct {p0}, Lcom/itau/empresas/controller/ProdutosController_;->init_()V

    .line 21
    return-void
.end method

.method public static getInstance_(Landroid/content/Context;)Lcom/itau/empresas/controller/ProdutosController_;
    .registers 2
    .param p0, "context"    # Landroid/content/Context;

    .line 24
    new-instance v0, Lcom/itau/empresas/controller/ProdutosController_;

    invoke-direct {v0, p0}, Lcom/itau/empresas/controller/ProdutosController_;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method private init_()V
    .registers 2

    .line 28
    iget-object v0, p0, Lcom/itau/empresas/controller/ProdutosController_;->context_:Landroid/content/Context;

    invoke-static {v0}, Lcom/itau/empresas/api/universal/converter/DetalhesMultilimiteConverter_;->getInstance_(Landroid/content/Context;)Lcom/itau/empresas/api/universal/converter/DetalhesMultilimiteConverter_;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/controller/ProdutosController_;->converter:Lcom/itau/empresas/api/universal/converter/DetalhesMultilimiteConverter;

    .line 29
    return-void
.end method

.class Lcom/itau/empresas/api/model/RetornoAgenciasVO$Dados;
.super Ljava/lang/Object;
.source "RetornoAgenciasVO.java"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/itau/empresas/api/model/RetornoAgenciasVO;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "Dados"
.end annotation


# instance fields
.field private d:Ljava/util/List;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "d"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Ljava/util/List<Lcom/itau/empresas/api/model/AgenciaVO;>;>;"
        }
    .end annotation
.end field


# virtual methods
.method public getD()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Ljava/util/List<Lcom/itau/empresas/api/model/AgenciaVO;>;>;"
        }
    .end annotation

    .line 41
    iget-object v0, p0, Lcom/itau/empresas/api/model/RetornoAgenciasVO$Dados;->d:Ljava/util/List;

    return-object v0
.end method

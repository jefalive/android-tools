.class public Lnet/hockeyapp/android/tasks/ParseFeedbackTask;
.super Landroid/os/AsyncTask;
.source "ParseFeedbackTask.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<Ljava/lang/Void;Ljava/lang/Void;Lnet/hockeyapp/android/objects/FeedbackResponse;>;"
    }
.end annotation


# instance fields
.field private context:Landroid/content/Context;

.field private feedbackResponse:Ljava/lang/String;

.field private handler:Landroid/os/Handler;

.field private requestType:Ljava/lang/String;

.field private urlString:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Landroid/os/Handler;Ljava/lang/String;)V
    .registers 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "feedbackResponse"    # Ljava/lang/String;
    .param p3, "handler"    # Landroid/os/Handler;
    .param p4, "requestType"    # Ljava/lang/String;

    .line 71
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 72
    iput-object p1, p0, Lnet/hockeyapp/android/tasks/ParseFeedbackTask;->context:Landroid/content/Context;

    .line 73
    iput-object p2, p0, Lnet/hockeyapp/android/tasks/ParseFeedbackTask;->feedbackResponse:Ljava/lang/String;

    .line 74
    iput-object p3, p0, Lnet/hockeyapp/android/tasks/ParseFeedbackTask;->handler:Landroid/os/Handler;

    .line 75
    iput-object p4, p0, Lnet/hockeyapp/android/tasks/ParseFeedbackTask;->requestType:Ljava/lang/String;

    .line 76
    const/4 v0, 0x0

    iput-object v0, p0, Lnet/hockeyapp/android/tasks/ParseFeedbackTask;->urlString:Ljava/lang/String;

    .line 77
    return-void
.end method

.method private checkForNewAnswers(Ljava/util/ArrayList;)V
    .registers 12
    .param p1, "messages"    # Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/ArrayList<Lnet/hockeyapp/android/objects/FeedbackMessage;>;)V"
        }
    .end annotation

    .line 118
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lnet/hockeyapp/android/objects/FeedbackMessage;

    .line 119
    .local v3, "latestMessage":Lnet/hockeyapp/android/objects/FeedbackMessage;
    invoke-virtual {v3}, Lnet/hockeyapp/android/objects/FeedbackMessage;->getId()I

    move-result v4

    .line 121
    .local v4, "idLatestMessage":I
    iget-object v0, p0, Lnet/hockeyapp/android/tasks/ParseFeedbackTask;->context:Landroid/content/Context;

    const-string v1, "net.hockeyapp.android.feedback"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v5

    .line 123
    .local v5, "preferences":Landroid/content/SharedPreferences;
    iget-object v0, p0, Lnet/hockeyapp/android/tasks/ParseFeedbackTask;->requestType:Ljava/lang/String;

    const-string v1, "send"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_38

    .line 124
    invoke-interface {v5}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "idLastMessageSend"

    .line 125
    invoke-interface {v0, v1, v4}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "idLastMessageProcessed"

    .line 126
    invoke-interface {v0, v1, v4}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 124
    invoke-static {v0}, Lnet/hockeyapp/android/utils/PrefsUtil;->applyChanges(Landroid/content/SharedPreferences$Editor;)V

    goto :goto_73

    .line 128
    :cond_38
    iget-object v0, p0, Lnet/hockeyapp/android/tasks/ParseFeedbackTask;->requestType:Ljava/lang/String;

    const-string v1, "fetch"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_73

    .line 129
    const-string v0, "idLastMessageSend"

    const/4 v1, -0x1

    invoke-interface {v5, v0, v1}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v6

    .line 130
    .local v6, "idLastMessageSend":I
    const-string v0, "idLastMessageProcessed"

    const/4 v1, -0x1

    invoke-interface {v5, v0, v1}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v7

    .line 132
    .local v7, "idLastMessageProcessed":I
    if-eq v4, v6, :cond_73

    if-eq v4, v7, :cond_73

    .line 134
    invoke-interface {v5}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "idLastMessageProcessed"

    invoke-interface {v0, v1, v4}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-static {v0}, Lnet/hockeyapp/android/utils/PrefsUtil;->applyChanges(Landroid/content/SharedPreferences$Editor;)V

    .line 135
    const/4 v8, 0x0

    .line 137
    .local v8, "eventHandled":Z
    invoke-static {}, Lnet/hockeyapp/android/FeedbackManager;->getLastListener()Lnet/hockeyapp/android/FeedbackManagerListener;

    move-result-object v9

    .line 138
    .local v9, "listener":Lnet/hockeyapp/android/FeedbackManagerListener;
    if-eqz v9, :cond_6c

    .line 139
    invoke-virtual {v9, v3}, Lnet/hockeyapp/android/FeedbackManagerListener;->feedbackAnswered(Lnet/hockeyapp/android/objects/FeedbackMessage;)Z

    move-result v8

    .line 142
    :cond_6c
    if-nez v8, :cond_73

    .line 143
    iget-object v0, p0, Lnet/hockeyapp/android/tasks/ParseFeedbackTask;->context:Landroid/content/Context;

    invoke-direct {p0, v0}, Lnet/hockeyapp/android/tasks/ParseFeedbackTask;->startNotification(Landroid/content/Context;)V

    .line 147
    .end local v6    # "idLastMessageSend":I
    .end local v7    # "idLastMessageProcessed":I
    .end local v8    # "eventHandled":Z
    .end local v9    # "listener":Lnet/hockeyapp/android/FeedbackManagerListener;
    :cond_73
    :goto_73
    return-void
.end method

.method private startNotification(Landroid/content/Context;)V
    .registers 12
    .param p1, "context"    # Landroid/content/Context;

    .line 151
    iget-object v0, p0, Lnet/hockeyapp/android/tasks/ParseFeedbackTask;->urlString:Ljava/lang/String;

    if-nez v0, :cond_5

    .line 152
    return-void

    .line 155
    :cond_5
    const-string v0, "notification"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Landroid/app/NotificationManager;

    .line 156
    .local v4, "notificationManager":Landroid/app/NotificationManager;
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const-string v1, "ic_menu_refresh"

    const-string v2, "drawable"

    const-string v3, "android"

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v5

    .line 157
    .local v5, "iconId":I
    new-instance v6, Landroid/app/Notification;

    const-string v0, "New Answer to Your Feedback."

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-direct {v6, v5, v0, v1, v2}, Landroid/app/Notification;-><init>(ILjava/lang/CharSequence;J)V

    .line 159
    .local v6, "notification":Landroid/app/Notification;
    const/4 v7, 0x0

    .line 160
    .local v7, "activityClass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    invoke-static {}, Lnet/hockeyapp/android/FeedbackManager;->getLastListener()Lnet/hockeyapp/android/FeedbackManagerListener;

    move-result-object v0

    if-eqz v0, :cond_36

    .line 161
    invoke-static {}, Lnet/hockeyapp/android/FeedbackManager;->getLastListener()Lnet/hockeyapp/android/FeedbackManagerListener;

    move-result-object v0

    invoke-virtual {v0}, Lnet/hockeyapp/android/FeedbackManagerListener;->getFeedbackActivityClass()Ljava/lang/Class;

    move-result-object v7

    .line 163
    :cond_36
    if-nez v7, :cond_3a

    .line 164
    const-class v7, Lnet/hockeyapp/android/FeedbackActivity;

    .line 167
    :cond_3a
    new-instance v8, Landroid/content/Intent;

    invoke-direct {v8}, Landroid/content/Intent;-><init>()V

    .line 168
    .local v8, "intent":Landroid/content/Intent;
    const/high16 v0, 0x30000000

    invoke-virtual {v8, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 169
    invoke-virtual {v8, p1, v7}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 170
    const-string v0, "url"

    iget-object v1, p0, Lnet/hockeyapp/android/tasks/ParseFeedbackTask;->urlString:Ljava/lang/String;

    invoke-virtual {v8, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 172
    const/4 v0, 0x0

    const/high16 v1, 0x40000000    # 2.0f

    invoke-static {p1, v0, v8, v1}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v9

    .line 173
    .local v9, "pendingIntent":Landroid/app/PendingIntent;
    const-string v0, "HockeyApp Feedback"

    const-string v1, "A new answer to your feedback is available."

    invoke-virtual {v6, p1, v0, v1, v9}, Landroid/app/Notification;->setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    .line 174
    const/4 v0, 0x2

    invoke-virtual {v4, v0, v6}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 175
    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3

    .line 59
    move-object v0, p1

    check-cast v0, [Ljava/lang/Void;

    invoke-virtual {p0, v0}, Lnet/hockeyapp/android/tasks/ParseFeedbackTask;->doInBackground([Ljava/lang/Void;)Lnet/hockeyapp/android/objects/FeedbackResponse;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Lnet/hockeyapp/android/objects/FeedbackResponse;
    .registers 7
    .param p1, "params"    # [Ljava/lang/Void;

    .line 85
    iget-object v0, p0, Lnet/hockeyapp/android/tasks/ParseFeedbackTask;->context:Landroid/content/Context;

    if-eqz v0, :cond_2e

    iget-object v0, p0, Lnet/hockeyapp/android/tasks/ParseFeedbackTask;->feedbackResponse:Ljava/lang/String;

    if-eqz v0, :cond_2e

    .line 86
    invoke-static {}, Lnet/hockeyapp/android/utils/FeedbackParser;->getInstance()Lnet/hockeyapp/android/utils/FeedbackParser;

    move-result-object v0

    iget-object v1, p0, Lnet/hockeyapp/android/tasks/ParseFeedbackTask;->feedbackResponse:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lnet/hockeyapp/android/utils/FeedbackParser;->parseFeedbackResponse(Ljava/lang/String;)Lnet/hockeyapp/android/objects/FeedbackResponse;

    move-result-object v2

    .line 88
    .local v2, "response":Lnet/hockeyapp/android/objects/FeedbackResponse;
    if-eqz v2, :cond_2d

    .line 89
    invoke-virtual {v2}, Lnet/hockeyapp/android/objects/FeedbackResponse;->getFeedback()Lnet/hockeyapp/android/objects/Feedback;

    move-result-object v3

    .line 90
    .local v3, "feedback":Lnet/hockeyapp/android/objects/Feedback;
    if-eqz v3, :cond_2d

    .line 91
    invoke-virtual {v2}, Lnet/hockeyapp/android/objects/FeedbackResponse;->getFeedback()Lnet/hockeyapp/android/objects/Feedback;

    move-result-object v0

    invoke-virtual {v0}, Lnet/hockeyapp/android/objects/Feedback;->getMessages()Ljava/util/ArrayList;

    move-result-object v4

    .line 92
    .local v4, "messages":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lnet/hockeyapp/android/objects/FeedbackMessage;>;"
    if-eqz v4, :cond_2d

    invoke-virtual {v4}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2d

    .line 93
    invoke-direct {p0, v4}, Lnet/hockeyapp/android/tasks/ParseFeedbackTask;->checkForNewAnswers(Ljava/util/ArrayList;)V

    .line 98
    .end local v3    # "feedback":Lnet/hockeyapp/android/objects/Feedback;
    .end local v4    # "messages":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lnet/hockeyapp/android/objects/FeedbackMessage;>;"
    .end local v4
    :cond_2d
    return-object v2

    .line 101
    .end local v2    # "response":Lnet/hockeyapp/android/objects/FeedbackResponse;
    :cond_2e
    const/4 v0, 0x0

    return-object v0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .registers 3

    .line 59
    move-object v0, p1

    check-cast v0, Lnet/hockeyapp/android/objects/FeedbackResponse;

    invoke-virtual {p0, v0}, Lnet/hockeyapp/android/tasks/ParseFeedbackTask;->onPostExecute(Lnet/hockeyapp/android/objects/FeedbackResponse;)V

    return-void
.end method

.method protected onPostExecute(Lnet/hockeyapp/android/objects/FeedbackResponse;)V
    .registers 5
    .param p1, "result"    # Lnet/hockeyapp/android/objects/FeedbackResponse;

    .line 106
    if-eqz p1, :cond_1d

    iget-object v0, p0, Lnet/hockeyapp/android/tasks/ParseFeedbackTask;->handler:Landroid/os/Handler;

    if-eqz v0, :cond_1d

    .line 107
    new-instance v1, Landroid/os/Message;

    invoke-direct {v1}, Landroid/os/Message;-><init>()V

    .line 108
    .local v1, "msg":Landroid/os/Message;
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 110
    .local v2, "bundle":Landroid/os/Bundle;
    const-string v0, "parse_feedback_response"

    invoke-virtual {v2, v0, p1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 111
    invoke-virtual {v1, v2}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 113
    iget-object v0, p0, Lnet/hockeyapp/android/tasks/ParseFeedbackTask;->handler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 115
    .end local v1    # "msg":Landroid/os/Message;
    .end local v2    # "bundle":Landroid/os/Bundle;
    :cond_1d
    return-void
.end method

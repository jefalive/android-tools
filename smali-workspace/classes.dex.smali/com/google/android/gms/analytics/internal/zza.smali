.class public Lcom/google/android/gms/analytics/internal/zza;
.super Lcom/google/android/gms/analytics/internal/zzd;


# static fields
.field public static zzPV:Z


# instance fields
.field private zzPW:Lcom/google/android/gms/ads/identifier/AdvertisingIdClient$Info;

.field private final zzPX:Lcom/google/android/gms/analytics/internal/zzaj;

.field private zzPY:Ljava/lang/String;

.field private zzPZ:Z

.field private zzQa:Ljava/lang/Object;


# direct methods
.method constructor <init>(Lcom/google/android/gms/analytics/internal/zzf;)V
    .registers 4

    invoke-direct {p0, p1}, Lcom/google/android/gms/analytics/internal/zzd;-><init>(Lcom/google/android/gms/analytics/internal/zzf;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/analytics/internal/zza;->zzPZ:Z

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/analytics/internal/zza;->zzQa:Ljava/lang/Object;

    new-instance v0, Lcom/google/android/gms/analytics/internal/zzaj;

    invoke-virtual {p1}, Lcom/google/android/gms/analytics/internal/zzf;->zzjl()Lcom/google/android/gms/internal/zzmq;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/analytics/internal/zzaj;-><init>(Lcom/google/android/gms/internal/zzmq;)V

    iput-object v0, p0, Lcom/google/android/gms/analytics/internal/zza;->zzPX:Lcom/google/android/gms/analytics/internal/zzaj;

    return-void
.end method

.method private zza(Lcom/google/android/gms/ads/identifier/AdvertisingIdClient$Info;Lcom/google/android/gms/ads/identifier/AdvertisingIdClient$Info;)Z
    .registers 9

    if-nez p2, :cond_4

    const/4 v1, 0x0

    goto :goto_8

    :cond_4
    invoke-virtual {p2}, Lcom/google/android/gms/ads/identifier/AdvertisingIdClient$Info;->getId()Ljava/lang/String;

    move-result-object v1

    :goto_8
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_10

    const/4 v0, 0x1

    return v0

    :cond_10
    invoke-virtual {p0}, Lcom/google/android/gms/analytics/internal/zza;->zzjr()Lcom/google/android/gms/analytics/internal/zzn;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/analytics/internal/zzn;->zzkk()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/analytics/internal/zza;->zzQa:Ljava/lang/Object;

    monitor-enter v3

    :try_start_1b
    iget-boolean v0, p0, Lcom/google/android/gms/analytics/internal/zza;->zzPZ:Z

    if-nez v0, :cond_29

    invoke-virtual {p0}, Lcom/google/android/gms/analytics/internal/zza;->zzjb()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/analytics/internal/zza;->zzPY:Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/analytics/internal/zza;->zzPZ:Z

    goto :goto_69

    :cond_29
    iget-object v0, p0, Lcom/google/android/gms/analytics/internal/zza;->zzPY:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_69

    if-nez p1, :cond_35

    const/4 v4, 0x0

    goto :goto_39

    :cond_35
    invoke-virtual {p1}, Lcom/google/android/gms/ads/identifier/AdvertisingIdClient$Info;->getId()Ljava/lang/String;

    move-result-object v4

    :goto_39
    if-nez v4, :cond_52

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/analytics/internal/zza;->zzbc(Ljava/lang/String;)Z
    :try_end_4f
    .catchall {:try_start_1b .. :try_end_4f} :catchall_c3

    move-result v0

    monitor-exit v3

    return v0

    :cond_52
    :try_start_52
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/analytics/internal/zza;->zzbb(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/analytics/internal/zza;->zzPY:Ljava/lang/String;

    :cond_69
    :goto_69
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/analytics/internal/zza;->zzbb(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z
    :try_end_81
    .catchall {:try_start_52 .. :try_end_81} :catchall_c3

    move-result v0

    if-eqz v0, :cond_87

    monitor-exit v3

    const/4 v0, 0x0

    return v0

    :cond_87
    :try_start_87
    iget-object v0, p0, Lcom/google/android/gms/analytics/internal/zza;->zzPY:Ljava/lang/String;

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_8c
    .catchall {:try_start_87 .. :try_end_8c} :catchall_c3

    move-result v0

    if-eqz v0, :cond_92

    monitor-exit v3

    const/4 v0, 0x1

    return v0

    :cond_92
    :try_start_92
    iget-object v0, p0, Lcom/google/android/gms/analytics/internal/zza;->zzPY:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_ac

    const-string v0, "Resetting the client id because Advertising Id changed."

    invoke-virtual {p0, v0}, Lcom/google/android/gms/analytics/internal/zza;->zzbd(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/gms/analytics/internal/zza;->zzjr()Lcom/google/android/gms/analytics/internal/zzn;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/analytics/internal/zzn;->zzkl()Ljava/lang/String;

    move-result-object v2

    const-string v0, "New client Id"

    invoke-virtual {p0, v0, v2}, Lcom/google/android/gms/analytics/internal/zza;->zza(Ljava/lang/String;Ljava/lang/Object;)V

    :cond_ac
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/analytics/internal/zza;->zzbc(Ljava/lang/String;)Z
    :try_end_c0
    .catchall {:try_start_92 .. :try_end_c0} :catchall_c3

    move-result v0

    monitor-exit v3

    return v0

    :catchall_c3
    move-exception v5

    monitor-exit v3

    throw v5
.end method

.method private static zzbb(Ljava/lang/String;)Ljava/lang/String;
    .registers 8

    const-string v0, "MD5"

    invoke-static {v0}, Lcom/google/android/gms/analytics/internal/zzam;->zzbv(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v6

    if-nez v6, :cond_a

    const/4 v0, 0x0

    return-object v0

    :cond_a
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "%032X"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    new-instance v3, Ljava/math/BigInteger;

    invoke-virtual {p0}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    invoke-virtual {v6, v4}, Ljava/security/MessageDigest;->digest([B)[B

    move-result-object v4

    const/4 v5, 0x1

    invoke-direct {v3, v5, v4}, Ljava/math/BigInteger;-><init>(I[B)V

    const/4 v4, 0x0

    aput-object v3, v2, v4

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private zzbc(Ljava/lang/String;)Z
    .registers 7

    :try_start_0
    invoke-static {p1}, Lcom/google/android/gms/analytics/internal/zza;->zzbb(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v0, "Storing hashed adid."

    invoke-virtual {p0, v0}, Lcom/google/android/gms/analytics/internal/zza;->zzbd(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/gms/analytics/internal/zza;->getContext()Landroid/content/Context;

    move-result-object v3

    const-string v0, "gaClientIdData"

    const/4 v1, 0x0

    invoke-virtual {v3, v0, v1}, Landroid/content/Context;->openFileOutput(Ljava/lang/String;I)Ljava/io/FileOutputStream;

    move-result-object v4

    invoke-virtual {v2}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/io/FileOutputStream;->write([B)V

    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V

    iput-object v2, p0, Lcom/google/android/gms/analytics/internal/zza;->zzPY:Ljava/lang/String;
    :try_end_20
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_20} :catch_22

    const/4 v0, 0x1

    return v0

    :catch_22
    move-exception v2

    const-string v0, "Error creating hash file"

    invoke-virtual {p0, v0, v2}, Lcom/google/android/gms/analytics/internal/zza;->zze(Ljava/lang/String;Ljava/lang/Object;)V

    const/4 v0, 0x0

    return v0
.end method

.method private declared-synchronized zziZ()Lcom/google/android/gms/ads/identifier/AdvertisingIdClient$Info;
    .registers 5

    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/analytics/internal/zza;->zzPX:Lcom/google/android/gms/analytics/internal/zzaj;

    const-wide/16 v1, 0x3e8

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/analytics/internal/zzaj;->zzv(J)Z

    move-result v0

    if-eqz v0, :cond_2e

    iget-object v0, p0, Lcom/google/android/gms/analytics/internal/zza;->zzPX:Lcom/google/android/gms/analytics/internal/zzaj;

    invoke-virtual {v0}, Lcom/google/android/gms/analytics/internal/zzaj;->start()V

    invoke-virtual {p0}, Lcom/google/android/gms/analytics/internal/zza;->zzja()Lcom/google/android/gms/ads/identifier/AdvertisingIdClient$Info;

    move-result-object v3

    iget-object v0, p0, Lcom/google/android/gms/analytics/internal/zza;->zzPW:Lcom/google/android/gms/ads/identifier/AdvertisingIdClient$Info;

    invoke-direct {p0, v0, v3}, Lcom/google/android/gms/analytics/internal/zza;->zza(Lcom/google/android/gms/ads/identifier/AdvertisingIdClient$Info;Lcom/google/android/gms/ads/identifier/AdvertisingIdClient$Info;)Z

    move-result v0

    if-eqz v0, :cond_1f

    iput-object v3, p0, Lcom/google/android/gms/analytics/internal/zza;->zzPW:Lcom/google/android/gms/ads/identifier/AdvertisingIdClient$Info;

    goto :goto_2e

    :cond_1f
    const-string v0, "Failed to reset client id on adid change. Not using adid"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/analytics/internal/zza;->zzbh(Ljava/lang/String;)V

    new-instance v0, Lcom/google/android/gms/ads/identifier/AdvertisingIdClient$Info;

    const-string v1, ""

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/ads/identifier/AdvertisingIdClient$Info;-><init>(Ljava/lang/String;Z)V

    iput-object v0, p0, Lcom/google/android/gms/analytics/internal/zza;->zzPW:Lcom/google/android/gms/ads/identifier/AdvertisingIdClient$Info;

    :cond_2e
    :goto_2e
    iget-object v0, p0, Lcom/google/android/gms/analytics/internal/zza;->zzPW:Lcom/google/android/gms/ads/identifier/AdvertisingIdClient$Info;
    :try_end_30
    .catchall {:try_start_1 .. :try_end_30} :catchall_32

    monitor-exit p0

    return-object v0

    :catchall_32
    move-exception v3

    monitor-exit p0

    throw v3
.end method


# virtual methods
.method protected zziJ()V
    .registers 1

    return-void
.end method

.method public zziU()Z
    .registers 3

    invoke-virtual {p0}, Lcom/google/android/gms/analytics/internal/zza;->zzjv()V

    invoke-direct {p0}, Lcom/google/android/gms/analytics/internal/zza;->zziZ()Lcom/google/android/gms/ads/identifier/AdvertisingIdClient$Info;

    move-result-object v1

    if-eqz v1, :cond_13

    invoke-virtual {v1}, Lcom/google/android/gms/ads/identifier/AdvertisingIdClient$Info;->isLimitAdTrackingEnabled()Z

    move-result v0

    if-nez v0, :cond_11

    const/4 v0, 0x1

    goto :goto_12

    :cond_11
    const/4 v0, 0x0

    :goto_12
    return v0

    :cond_13
    const/4 v0, 0x0

    return v0
.end method

.method public zziY()Ljava/lang/String;
    .registers 4

    invoke-virtual {p0}, Lcom/google/android/gms/analytics/internal/zza;->zzjv()V

    invoke-direct {p0}, Lcom/google/android/gms/analytics/internal/zza;->zziZ()Lcom/google/android/gms/ads/identifier/AdvertisingIdClient$Info;

    move-result-object v1

    const/4 v2, 0x0

    if-eqz v1, :cond_e

    invoke-virtual {v1}, Lcom/google/android/gms/ads/identifier/AdvertisingIdClient$Info;->getId()Ljava/lang/String;

    move-result-object v2

    :cond_e
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_16

    const/4 v0, 0x0

    return-object v0

    :cond_16
    return-object v2
.end method

.method protected zzja()Lcom/google/android/gms/ads/identifier/AdvertisingIdClient$Info;
    .registers 4

    const/4 v1, 0x0

    :try_start_1
    invoke-virtual {p0}, Lcom/google/android/gms/analytics/internal/zza;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/ads/identifier/AdvertisingIdClient;->getAdvertisingIdInfo(Landroid/content/Context;)Lcom/google/android/gms/ads/identifier/AdvertisingIdClient$Info;
    :try_end_8
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_8} :catch_b
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_8} :catch_12

    move-result-object v0

    move-object v1, v0

    goto :goto_1f

    :catch_b
    move-exception v2

    const-string v0, "IllegalStateException getting Ad Id Info. If you would like to see Audience reports, please ensure that you have added \'<meta-data android:name=\"com.google.android.gms.version\" android:value=\"@integer/google_play_services_version\" />\' to your application manifest file. See http://goo.gl/naFqQk for details."

    invoke-virtual {p0, v0}, Lcom/google/android/gms/analytics/internal/zza;->zzbg(Ljava/lang/String;)V

    goto :goto_1f

    :catch_12
    move-exception v2

    sget-boolean v0, Lcom/google/android/gms/analytics/internal/zza;->zzPV:Z

    if-nez v0, :cond_1f

    const/4 v0, 0x1

    sput-boolean v0, Lcom/google/android/gms/analytics/internal/zza;->zzPV:Z

    const-string v0, "Error getting advertiser id"

    invoke-virtual {p0, v0, v2}, Lcom/google/android/gms/analytics/internal/zza;->zzd(Ljava/lang/String;Ljava/lang/Object;)V

    :cond_1f
    :goto_1f
    return-object v1
.end method

.method protected zzjb()Ljava/lang/String;
    .registers 7

    const/4 v2, 0x0

    :try_start_1
    invoke-virtual {p0}, Lcom/google/android/gms/analytics/internal/zza;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "gaClientIdData"

    invoke-virtual {v0, v1}, Landroid/content/Context;->openFileInput(Ljava/lang/String;)Ljava/io/FileInputStream;

    move-result-object v3

    const/16 v0, 0x80

    new-array v4, v0, [B

    const/4 v0, 0x0

    const/16 v1, 0x80

    invoke-virtual {v3, v4, v0, v1}, Ljava/io/FileInputStream;->read([BII)I

    move-result v5

    invoke-virtual {v3}, Ljava/io/FileInputStream;->available()I

    move-result v0

    if-lez v0, :cond_2e

    const-string v0, "Hash file seems corrupted, deleting it."

    invoke-virtual {p0, v0}, Lcom/google/android/gms/analytics/internal/zza;->zzbg(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V

    invoke-virtual {p0}, Lcom/google/android/gms/analytics/internal/zza;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "gaClientIdData"

    invoke-virtual {v0, v1}, Landroid/content/Context;->deleteFile(Ljava/lang/String;)Z

    goto :goto_43

    :cond_2e
    if-gtz v5, :cond_39

    const-string v0, "Hash file is empty."

    invoke-virtual {p0, v0}, Lcom/google/android/gms/analytics/internal/zza;->zzbd(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V

    goto :goto_43

    :cond_39
    new-instance v0, Ljava/lang/String;

    const/4 v1, 0x0

    invoke-direct {v0, v4, v1, v5}, Ljava/lang/String;-><init>([BII)V

    move-object v2, v0

    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_43
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_43} :catch_44
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_43} :catch_46

    :goto_43
    goto :goto_55

    :catch_44
    move-exception v3

    goto :goto_55

    :catch_46
    move-exception v3

    const-string v0, "Error reading Hash file, deleting it"

    invoke-virtual {p0, v0, v3}, Lcom/google/android/gms/analytics/internal/zza;->zzd(Ljava/lang/String;Ljava/lang/Object;)V

    invoke-virtual {p0}, Lcom/google/android/gms/analytics/internal/zza;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "gaClientIdData"

    invoke-virtual {v0, v1}, Landroid/content/Context;->deleteFile(Ljava/lang/String;)Z

    :goto_55
    return-object v2
.end method

.class public Lcom/itau/empresas/feature/pagamento/comcodigobarra/model/DadosBoletoVO;
.super Ljava/lang/Object;
.source "DadosBoletoVO.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private dataPagamento:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "data_pagamento"
    .end annotation
.end field

.field private dataVencimento:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "data_vencimento"
    .end annotation
.end field

.field private identificacaoComprovante:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "identificacao_comprovante"
    .end annotation
.end field

.field private valorDesconto:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "valor_desconto"
    .end annotation
.end field

.field private valorDocumento:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "valor_documento"
    .end annotation
.end field

.field private valorJuros:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "valor_juros"
    .end annotation
.end field

.field private valorPagamento:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "valor_pagamento"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

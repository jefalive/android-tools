.class public final Lbr/com/itau/sdk/android/core/model/DataReqResDTO;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private data:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "data"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 2

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    const/4 v0, 0x0

    iput-object v0, p0, Lbr/com/itau/sdk/android/core/model/DataReqResDTO;->data:Ljava/lang/String;

    .line 16
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .registers 2

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    iput-object p1, p0, Lbr/com/itau/sdk/android/core/model/DataReqResDTO;->data:Ljava/lang/String;

    .line 12
    return-void
.end method


# virtual methods
.method public getData()Ljava/lang/String;
    .registers 2

    .line 19
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/model/DataReqResDTO;->data:Ljava/lang/String;

    return-object v0
.end method

.method public setData(Ljava/lang/String;)V
    .registers 2

    .line 23
    iput-object p1, p0, Lbr/com/itau/sdk/android/core/model/DataReqResDTO;->data:Ljava/lang/String;

    .line 24
    return-void
.end method

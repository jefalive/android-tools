.class public final Lorg/threeten/bp/format/DecimalStyle;
.super Ljava/lang/Object;
.source "DecimalStyle.java"


# static fields
.field private static final CACHE:Ljava/util/concurrent/ConcurrentMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentMap<Ljava/util/Locale;Lorg/threeten/bp/format/DecimalStyle;>;"
        }
    .end annotation
.end field

.field public static final STANDARD:Lorg/threeten/bp/format/DecimalStyle;


# instance fields
.field private final decimalSeparator:C

.field private final negativeSign:C

.field private final positiveSign:C

.field private final zeroDigit:C


# direct methods
.method static constructor <clinit>()V
    .registers 5

    .line 60
    new-instance v0, Lorg/threeten/bp/format/DecimalStyle;

    const/16 v1, 0x30

    const/16 v2, 0x2b

    const/16 v3, 0x2d

    const/16 v4, 0x2e

    invoke-direct {v0, v1, v2, v3, v4}, Lorg/threeten/bp/format/DecimalStyle;-><init>(CCCC)V

    sput-object v0, Lorg/threeten/bp/format/DecimalStyle;->STANDARD:Lorg/threeten/bp/format/DecimalStyle;

    .line 64
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    const/16 v1, 0x10

    const/high16 v2, 0x3f400000    # 0.75f

    const/4 v3, 0x2

    invoke-direct {v0, v1, v2, v3}, Ljava/util/concurrent/ConcurrentHashMap;-><init>(IFI)V

    sput-object v0, Lorg/threeten/bp/format/DecimalStyle;->CACHE:Ljava/util/concurrent/ConcurrentMap;

    return-void
.end method

.method private constructor <init>(CCCC)V
    .registers 5
    .param p1, "zeroChar"    # C
    .param p2, "positiveSignChar"    # C
    .param p3, "negativeSignChar"    # C
    .param p4, "decimalPointChar"    # C

    .line 147
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 148
    iput-char p1, p0, Lorg/threeten/bp/format/DecimalStyle;->zeroDigit:C

    .line 149
    iput-char p2, p0, Lorg/threeten/bp/format/DecimalStyle;->positiveSign:C

    .line 150
    iput-char p3, p0, Lorg/threeten/bp/format/DecimalStyle;->negativeSign:C

    .line 151
    iput-char p4, p0, Lorg/threeten/bp/format/DecimalStyle;->decimalSeparator:C

    .line 152
    return-void
.end method


# virtual methods
.method convertNumberToI18N(Ljava/lang/String;)Ljava/lang/String;
    .registers 7
    .param p1, "numericText"    # Ljava/lang/String;

    .line 290
    iget-char v0, p0, Lorg/threeten/bp/format/DecimalStyle;->zeroDigit:C

    const/16 v1, 0x30

    if-ne v0, v1, :cond_7

    .line 291
    return-object p1

    .line 293
    :cond_7
    iget-char v0, p0, Lorg/threeten/bp/format/DecimalStyle;->zeroDigit:C

    add-int/lit8 v2, v0, -0x30

    .line 294
    .local v2, "diff":I
    invoke-virtual {p1}, Ljava/lang/String;->toCharArray()[C

    move-result-object v3

    .line 295
    .local v3, "array":[C
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_10
    array-length v0, v3

    if-ge v4, v0, :cond_1c

    .line 296
    aget-char v0, v3, v4

    add-int/2addr v0, v2

    int-to-char v0, v0

    aput-char v0, v3, v4

    .line 295
    add-int/lit8 v4, v4, 0x1

    goto :goto_10

    .line 298
    .end local v4    # "i":I
    :cond_1c
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/lang/String;-><init>([C)V

    return-object v0
.end method

.method convertToDigit(C)I
    .registers 4
    .param p1, "ch"    # C

    .line 279
    iget-char v0, p0, Lorg/threeten/bp/format/DecimalStyle;->zeroDigit:C

    sub-int v1, p1, v0

    .line 280
    .local v1, "val":I
    if-ltz v1, :cond_c

    const/16 v0, 0x9

    if-gt v1, v0, :cond_c

    move v0, v1

    goto :goto_d

    :cond_c
    const/4 v0, -0x1

    :goto_d
    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 5
    .param p1, "obj"    # Ljava/lang/Object;

    .line 310
    if-ne p0, p1, :cond_4

    .line 311
    const/4 v0, 0x1

    return v0

    .line 313
    :cond_4
    instance-of v0, p1, Lorg/threeten/bp/format/DecimalStyle;

    if-eqz v0, :cond_27

    .line 314
    move-object v2, p1

    check-cast v2, Lorg/threeten/bp/format/DecimalStyle;

    .line 315
    .local v2, "other":Lorg/threeten/bp/format/DecimalStyle;
    iget-char v0, p0, Lorg/threeten/bp/format/DecimalStyle;->zeroDigit:C

    iget-char v1, v2, Lorg/threeten/bp/format/DecimalStyle;->zeroDigit:C

    if-ne v0, v1, :cond_25

    iget-char v0, p0, Lorg/threeten/bp/format/DecimalStyle;->positiveSign:C

    iget-char v1, v2, Lorg/threeten/bp/format/DecimalStyle;->positiveSign:C

    if-ne v0, v1, :cond_25

    iget-char v0, p0, Lorg/threeten/bp/format/DecimalStyle;->negativeSign:C

    iget-char v1, v2, Lorg/threeten/bp/format/DecimalStyle;->negativeSign:C

    if-ne v0, v1, :cond_25

    iget-char v0, p0, Lorg/threeten/bp/format/DecimalStyle;->decimalSeparator:C

    iget-char v1, v2, Lorg/threeten/bp/format/DecimalStyle;->decimalSeparator:C

    if-ne v0, v1, :cond_25

    const/4 v0, 0x1

    goto :goto_26

    :cond_25
    const/4 v0, 0x0

    :goto_26
    return v0

    .line 318
    .end local v2    # "other":Lorg/threeten/bp/format/DecimalStyle;
    :cond_27
    const/4 v0, 0x0

    return v0
.end method

.method public getDecimalSeparator()C
    .registers 2

    .line 252
    iget-char v0, p0, Lorg/threeten/bp/format/DecimalStyle;->decimalSeparator:C

    return v0
.end method

.method public getNegativeSign()C
    .registers 2

    .line 223
    iget-char v0, p0, Lorg/threeten/bp/format/DecimalStyle;->negativeSign:C

    return v0
.end method

.method public getPositiveSign()C
    .registers 2

    .line 194
    iget-char v0, p0, Lorg/threeten/bp/format/DecimalStyle;->positiveSign:C

    return v0
.end method

.method public getZeroDigit()C
    .registers 2

    .line 164
    iget-char v0, p0, Lorg/threeten/bp/format/DecimalStyle;->zeroDigit:C

    return v0
.end method

.method public hashCode()I
    .registers 3

    .line 328
    iget-char v0, p0, Lorg/threeten/bp/format/DecimalStyle;->zeroDigit:C

    iget-char v1, p0, Lorg/threeten/bp/format/DecimalStyle;->positiveSign:C

    add-int/2addr v0, v1

    iget-char v1, p0, Lorg/threeten/bp/format/DecimalStyle;->negativeSign:C

    add-int/2addr v0, v1

    iget-char v1, p0, Lorg/threeten/bp/format/DecimalStyle;->decimalSeparator:C

    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    .line 339
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "DecimalStyle["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-char v1, p0, Lorg/threeten/bp/format/DecimalStyle;->zeroDigit:C

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-char v1, p0, Lorg/threeten/bp/format/DecimalStyle;->positiveSign:C

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-char v1, p0, Lorg/threeten/bp/format/DecimalStyle;->negativeSign:C

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-char v1, p0, Lorg/threeten/bp/format/DecimalStyle;->decimalSeparator:C

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

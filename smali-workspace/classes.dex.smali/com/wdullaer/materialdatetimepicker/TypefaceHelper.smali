.class public Lcom/wdullaer/materialdatetimepicker/TypefaceHelper;
.super Ljava/lang/Object;
.source "TypefaceHelper.java"


# static fields
.field private static final cache:Landroid/support/v4/util/SimpleArrayMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/support/v4/util/SimpleArrayMap<Ljava/lang/String;Landroid/graphics/Typeface;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .line 26
    new-instance v0, Landroid/support/v4/util/SimpleArrayMap;

    invoke-direct {v0}, Landroid/support/v4/util/SimpleArrayMap;-><init>()V

    sput-object v0, Lcom/wdullaer/materialdatetimepicker/TypefaceHelper;->cache:Landroid/support/v4/util/SimpleArrayMap;

    return-void
.end method

.method public constructor <init>()V
    .registers 1

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static get(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/Typeface;
    .registers 9
    .param p0, "c"    # Landroid/content/Context;
    .param p1, "name"    # Ljava/lang/String;

    .line 29
    sget-object v4, Lcom/wdullaer/materialdatetimepicker/TypefaceHelper;->cache:Landroid/support/v4/util/SimpleArrayMap;

    monitor-enter v4

    .line 30
    :try_start_3
    sget-object v0, Lcom/wdullaer/materialdatetimepicker/TypefaceHelper;->cache:Landroid/support/v4/util/SimpleArrayMap;

    invoke-virtual {v0, p1}, Landroid/support/v4/util/SimpleArrayMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_26

    .line 31
    invoke-virtual {p0}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    const-string v1, "fonts/%s.ttf"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/graphics/Typeface;->createFromAsset(Landroid/content/res/AssetManager;Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v5

    .line 33
    .local v5, "t":Landroid/graphics/Typeface;
    sget-object v0, Lcom/wdullaer/materialdatetimepicker/TypefaceHelper;->cache:Landroid/support/v4/util/SimpleArrayMap;

    invoke-virtual {v0, p1, v5}, Landroid/support/v4/util/SimpleArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_24
    .catchall {:try_start_3 .. :try_end_24} :catchall_30

    .line 34
    monitor-exit v4

    return-object v5

    .line 36
    .end local v5    # "t":Landroid/graphics/Typeface;
    :cond_26
    :try_start_26
    sget-object v0, Lcom/wdullaer/materialdatetimepicker/TypefaceHelper;->cache:Landroid/support/v4/util/SimpleArrayMap;

    invoke-virtual {v0, p1}, Landroid/support/v4/util/SimpleArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Typeface;
    :try_end_2e
    .catchall {:try_start_26 .. :try_end_2e} :catchall_30

    monitor-exit v4

    return-object v0

    .line 37
    :catchall_30
    move-exception v6

    monitor-exit v4

    throw v6
.end method

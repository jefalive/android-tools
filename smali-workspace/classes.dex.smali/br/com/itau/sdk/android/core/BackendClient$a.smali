.class final Lbr/com/itau/sdk/android/core/BackendClient$a;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lbr/com/itau/sdk/android/core/BackendClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "a"
.end annotation


# static fields
.field private static final n:[B

.field private static o:I


# instance fields
.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<Ljava/lang/Class<*>;Ljava/lang/Object;>;"
        }
    .end annotation
.end field

.field private final b:Lbr/com/itau/sdk/android/core/endpoint/h;

.field private final c:Lbr/com/itau/sdk/android/core/FlowManager;

.field private final d:Lde/greenrobot/event/EventBus;

.field private final e:Lde/greenrobot/event/EventBus;

.field private final f:Lbr/com/itau/sdk/android/core/a;

.field private final g:Ljava/lang/String;

.field private final h:Ljava/lang/String;

.field private final i:Ljava/lang/String;

.field private final j:Landroid/content/Context;

.field private final k:Lbr/com/itau/sdk/android/core/h;

.field private l:Ljava/lang/String;

.field private m:Lbr/com/itau/sdk/android/core/BackendClient$Builder;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    const/16 v0, 0x55

    new-array v0, v0, [B

    fill-array-data v0, :array_e

    sput-object v0, Lbr/com/itau/sdk/android/core/BackendClient$a;->n:[B

    const/16 v0, 0x38

    sput v0, Lbr/com/itau/sdk/android/core/BackendClient$a;->o:I

    return-void

    :array_e
    .array-data 1
        0x63t
        0x1ft
        0xet
        -0x7ft
        0x50t
        -0x4ct
        0x50t
        -0x4ct
        0x50t
        0x23t
        -0x4dt
        0x52t
        -0xdt
        0x15t
        -0xat
        0x2et
        0x8t
        -0x7t
        -0x6t
        -0x42t
        0x50t
        0x3t
        0x7t
        -0x52t
        0x45t
        0xet
        0x1t
        -0x6t
        0x5t
        0x0t
        0x10t
        -0x1t
        -0xbt
        -0x43t
        0x4at
        0xet
        0x2t
        -0x2t
        -0x4et
        0x45t
        0xbt
        -0x1t
        -0x2t
        0xbt
        0x8t
        0x13t
        0x13t
        -0xft
        0xet
        -0x6t
        0x11t
        -0xdt
        0xft
        -0x50t
        0x4ft
        0xat
        0x0t
        0x3t
        -0x52t
        0x44t
        0x5t
        -0x43t
        0x43t
        -0x3ft
        0x50t
        0x3t
        0x1t
        -0x4ct
        0x50t
        0x9t
        -0x7t
        0x2t
        -0x4at
        0x4bt
        0x7t
        0x8t
        -0xdt
        0xft
        -0xat
        -0x3t
        0x4t
        0x4t
        0x50t
        -0x4ct
        0x50t
    .end array-data
.end method

.method private constructor <init>(Lbr/com/itau/sdk/android/core/BackendClient$Builder;)V
    .registers 13

    .line 536
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 517
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lbr/com/itau/sdk/android/core/BackendClient$a;->a:Ljava/util/Map;

    .line 519
    new-instance v0, Lbr/com/itau/sdk/android/core/FlowManager;

    invoke-direct {v0}, Lbr/com/itau/sdk/android/core/FlowManager;-><init>()V

    iput-object v0, p0, Lbr/com/itau/sdk/android/core/BackendClient$a;->c:Lbr/com/itau/sdk/android/core/FlowManager;

    .line 520
    invoke-static {}, Lde/greenrobot/event/EventBus;->builder()Lde/greenrobot/event/EventBusBuilder;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lde/greenrobot/event/EventBusBuilder;->logNoSubscriberMessages(Z)Lde/greenrobot/event/EventBusBuilder;

    move-result-object v0

    .line 521
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lde/greenrobot/event/EventBusBuilder;->sendNoSubscriberEvent(Z)Lde/greenrobot/event/EventBusBuilder;

    move-result-object v0

    .line 522
    invoke-virtual {v0}, Lde/greenrobot/event/EventBusBuilder;->build()Lde/greenrobot/event/EventBus;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/sdk/android/core/BackendClient$a;->d:Lde/greenrobot/event/EventBus;

    .line 523
    invoke-static {}, Lde/greenrobot/event/EventBus;->builder()Lde/greenrobot/event/EventBusBuilder;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lde/greenrobot/event/EventBusBuilder;->logNoSubscriberMessages(Z)Lde/greenrobot/event/EventBusBuilder;

    move-result-object v0

    .line 524
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lde/greenrobot/event/EventBusBuilder;->sendNoSubscriberEvent(Z)Lde/greenrobot/event/EventBusBuilder;

    move-result-object v0

    .line 525
    invoke-virtual {v0}, Lde/greenrobot/event/EventBusBuilder;->build()Lde/greenrobot/event/EventBus;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/sdk/android/core/BackendClient$a;->e:Lde/greenrobot/event/EventBus;

    .line 538
    invoke-static {p1}, Lbr/com/itau/sdk/android/core/BackendClient$Builder;->a(Lbr/com/itau/sdk/android/core/BackendClient$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/sdk/android/core/BackendClient$a;->g:Ljava/lang/String;

    .line 539
    invoke-static {p1}, Lbr/com/itau/sdk/android/core/BackendClient$Builder;->b(Lbr/com/itau/sdk/android/core/BackendClient$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/sdk/android/core/BackendClient$a;->h:Ljava/lang/String;

    .line 540
    invoke-static {p1}, Lbr/com/itau/sdk/android/core/BackendClient$Builder;->c(Lbr/com/itau/sdk/android/core/BackendClient$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/sdk/android/core/BackendClient$a;->i:Ljava/lang/String;

    .line 541
    new-instance v0, Lbr/com/itau/sdk/android/core/a;

    invoke-direct {v0}, Lbr/com/itau/sdk/android/core/a;-><init>()V

    iput-object v0, p0, Lbr/com/itau/sdk/android/core/BackendClient$a;->f:Lbr/com/itau/sdk/android/core/a;

    .line 542
    iput-object p1, p0, Lbr/com/itau/sdk/android/core/BackendClient$a;->m:Lbr/com/itau/sdk/android/core/BackendClient$Builder;

    .line 544
    invoke-static {p1}, Lbr/com/itau/sdk/android/core/BackendClient$Builder;->d(Lbr/com/itau/sdk/android/core/BackendClient$Builder;)Ljava/lang/String;

    move-result-object v6

    .line 545
    invoke-static {p1}, Lbr/com/itau/sdk/android/core/BackendClient$Builder;->e(Lbr/com/itau/sdk/android/core/BackendClient$Builder;)Ljava/lang/String;

    move-result-object v7

    .line 547
    new-instance v8, Lokhttp3/OkHttpClient$Builder;

    invoke-direct {v8}, Lokhttp3/OkHttpClient$Builder;-><init>()V

    .line 548
    invoke-static {v8}, Lbr/com/itau/sdk/android/core/i;->a(Lokhttp3/OkHttpClient$Builder;)V

    .line 550
    invoke-static {p1}, Lbr/com/itau/sdk/android/core/BackendClient$Builder;->f(Lbr/com/itau/sdk/android/core/BackendClient$Builder;)Ljava/lang/ref/WeakReference;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Landroid/app/Application;

    .line 551
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/BackendClient$a;->f:Lbr/com/itau/sdk/android/core/a;

    invoke-virtual {v9, v0}, Landroid/app/Application;->registerActivityLifecycleCallbacks(Landroid/app/Application$ActivityLifecycleCallbacks;)V

    .line 552
    iput-object v9, p0, Lbr/com/itau/sdk/android/core/BackendClient$a;->j:Landroid/content/Context;

    .line 554
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/BackendClient$a;->j:Landroid/content/Context;

    invoke-static {v0}, Lbr/com/itau/security/did/DID;->init(Landroid/content/Context;)V

    .line 556
    invoke-static {p1}, Lbr/com/itau/sdk/android/core/BackendClient$Builder;->g(Lbr/com/itau/sdk/android/core/BackendClient$Builder;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_b4

    .line 557
    sget-object v0, Lbr/com/itau/sdk/android/core/BackendClient$a;->n:[B

    const/16 v1, 0x26

    aget-byte v0, v0, v1

    neg-int v0, v0

    const/16 v1, 0x2b

    const/16 v2, 0x22

    invoke-static {v1, v2, v0}, Lbr/com/itau/sdk/android/core/BackendClient$a;->a(III)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lbr/com/itau/sdk/android/core/BackendClient$a;->j:Landroid/content/Context;

    invoke-static {v2}, Lbr/com/itau/sdk/android/core/n;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {p1}, Lbr/com/itau/sdk/android/core/BackendClient$Builder;->g(Lbr/com/itau/sdk/android/core/BackendClient$Builder;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/sdk/android/core/BackendClient$a;->l:Ljava/lang/String;

    goto :goto_bc

    .line 559
    :cond_b4
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/BackendClient$a;->j:Landroid/content/Context;

    invoke-static {v0}, Lbr/com/itau/sdk/android/core/n;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/sdk/android/core/BackendClient$a;->l:Ljava/lang/String;

    .line 562
    :goto_bc
    const/4 v10, 0x0

    .line 564
    invoke-static {p1}, Lbr/com/itau/sdk/android/core/BackendClient$Builder;->h(Lbr/com/itau/sdk/android/core/BackendClient$Builder;)Z

    move-result v0

    if-eqz v0, :cond_c9

    .line 565
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/BackendClient$a;->j:Landroid/content/Context;

    invoke-direct {p0, v0, p1}, Lbr/com/itau/sdk/android/core/BackendClient$a;->a(Landroid/content/Context;Lbr/com/itau/sdk/android/core/BackendClient$Builder;)Ljava/lang/String;

    move-result-object v10

    .line 567
    :cond_c9
    new-instance v0, Lbr/com/itau/sdk/android/core/h;

    invoke-direct {v0}, Lbr/com/itau/sdk/android/core/h;-><init>()V

    iput-object v0, p0, Lbr/com/itau/sdk/android/core/BackendClient$a;->k:Lbr/com/itau/sdk/android/core/h;

    .line 569
    const/4 v0, 0x0

    invoke-virtual {v8, v0}, Lokhttp3/OkHttpClient$Builder;->followRedirects(Z)Lokhttp3/OkHttpClient$Builder;

    .line 570
    invoke-static {p1}, Lbr/com/itau/sdk/android/core/BackendClient$Builder;->i(Lbr/com/itau/sdk/android/core/BackendClient$Builder;)J

    move-result-wide v0

    invoke-static {p1}, Lbr/com/itau/sdk/android/core/BackendClient$Builder;->j(Lbr/com/itau/sdk/android/core/BackendClient$Builder;)Ljava/util/concurrent/TimeUnit;

    move-result-object v2

    invoke-virtual {v8, v0, v1, v2}, Lokhttp3/OkHttpClient$Builder;->connectTimeout(JLjava/util/concurrent/TimeUnit;)Lokhttp3/OkHttpClient$Builder;

    .line 571
    invoke-static {p1}, Lbr/com/itau/sdk/android/core/BackendClient$Builder;->i(Lbr/com/itau/sdk/android/core/BackendClient$Builder;)J

    move-result-wide v0

    invoke-static {p1}, Lbr/com/itau/sdk/android/core/BackendClient$Builder;->j(Lbr/com/itau/sdk/android/core/BackendClient$Builder;)Ljava/util/concurrent/TimeUnit;

    move-result-object v2

    invoke-virtual {v8, v0, v1, v2}, Lokhttp3/OkHttpClient$Builder;->readTimeout(JLjava/util/concurrent/TimeUnit;)Lokhttp3/OkHttpClient$Builder;

    .line 572
    invoke-static {p1}, Lbr/com/itau/sdk/android/core/BackendClient$Builder;->i(Lbr/com/itau/sdk/android/core/BackendClient$Builder;)J

    move-result-wide v0

    invoke-static {p1}, Lbr/com/itau/sdk/android/core/BackendClient$Builder;->j(Lbr/com/itau/sdk/android/core/BackendClient$Builder;)Ljava/util/concurrent/TimeUnit;

    move-result-object v2

    invoke-virtual {v8, v0, v1, v2}, Lokhttp3/OkHttpClient$Builder;->writeTimeout(JLjava/util/concurrent/TimeUnit;)Lokhttp3/OkHttpClient$Builder;

    .line 573
    invoke-virtual {v8}, Lokhttp3/OkHttpClient$Builder;->networkInterceptors()Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lbr/com/itau/sdk/android/core/BackendClient$a;->k:Lbr/com/itau/sdk/android/core/h;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 574
    invoke-virtual {v8}, Lokhttp3/OkHttpClient$Builder;->networkInterceptors()Ljava/util/List;

    move-result-object v0

    new-instance v1, Lbr/com/itau/sdk/android/core/b/a;

    invoke-direct {v1}, Lbr/com/itau/sdk/android/core/b/a;-><init>()V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 575
    invoke-virtual {v8}, Lokhttp3/OkHttpClient$Builder;->networkInterceptors()Ljava/util/List;

    move-result-object v0

    new-instance v1, Lbr/com/itau/sdk/android/core/e;

    .line 576
    invoke-static {p1}, Lbr/com/itau/sdk/android/core/BackendClient$Builder;->h(Lbr/com/itau/sdk/android/core/BackendClient$Builder;)Z

    move-result v2

    invoke-static {p1}, Lbr/com/itau/sdk/android/core/BackendClient$Builder;->k(Lbr/com/itau/sdk/android/core/BackendClient$Builder;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v10, v3}, Lbr/com/itau/sdk/android/core/e;-><init>(ZLjava/lang/String;Ljava/lang/String;)V

    .line 575
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 578
    new-instance v0, Lbr/com/itau/sdk/android/core/endpoint/h;

    invoke-virtual {v8}, Lokhttp3/OkHttpClient$Builder;->build()Lokhttp3/OkHttpClient;

    move-result-object v3

    sget-object v4, Lbr/com/itau/sdk/android/core/endpoint/ErrorHandler;->DEFAULT:Lbr/com/itau/sdk/android/core/endpoint/ErrorHandler;

    .line 579
    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/BackendClient$Builder;->extraConfiguration()Lbr/com/itau/sdk/android/core/ExtraConfiguration;

    move-result-object v1

    invoke-virtual {v1}, Lbr/com/itau/sdk/android/core/ExtraConfiguration;->getMiddlewareCaller()Lbr/com/itau/sdk/android/core/endpoint/legacy/MiddlewareCaller;

    move-result-object v5

    move-object v1, v7

    move-object v2, v6

    invoke-direct/range {v0 .. v5}, Lbr/com/itau/sdk/android/core/endpoint/h;-><init>(Ljava/lang/String;Ljava/lang/String;Lokhttp3/OkHttpClient;Lbr/com/itau/sdk/android/core/endpoint/ErrorHandler;Lbr/com/itau/sdk/android/core/endpoint/legacy/MiddlewareCaller;)V

    iput-object v0, p0, Lbr/com/itau/sdk/android/core/BackendClient$a;->b:Lbr/com/itau/sdk/android/core/endpoint/h;

    .line 580
    return-void
.end method

.method synthetic constructor <init>(Lbr/com/itau/sdk/android/core/BackendClient$Builder;Lbr/com/itau/sdk/android/core/b;)V
    .registers 3

    .line 515
    invoke-direct {p0, p1}, Lbr/com/itau/sdk/android/core/BackendClient$a;-><init>(Lbr/com/itau/sdk/android/core/BackendClient$Builder;)V

    return-void
.end method

.method private a()Landroid/app/Activity;
    .registers 2

    .line 604
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/BackendClient$a;->f:Lbr/com/itau/sdk/android/core/a;

    invoke-virtual {v0}, Lbr/com/itau/sdk/android/core/a;->a()Landroid/app/Activity;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lbr/com/itau/sdk/android/core/BackendClient$a;)Landroid/app/Activity;
    .registers 2

    .line 515
    invoke-direct {p0}, Lbr/com/itau/sdk/android/core/BackendClient$a;->a()Landroid/app/Activity;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lbr/com/itau/sdk/android/core/BackendClient$a;Ljava/lang/Class;Lbr/com/itau/sdk/android/core/endpoint/ErrorHandler;)Ljava/lang/Object;
    .registers 4

    .line 515
    invoke-direct {p0, p1, p2}, Lbr/com/itau/sdk/android/core/BackendClient$a;->a(Ljava/lang/Class;Lbr/com/itau/sdk/android/core/endpoint/ErrorHandler;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/Class;Lbr/com/itau/sdk/android/core/endpoint/ErrorHandler;)Ljava/lang/Object;
    .registers 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:Ljava/lang/Object;>(Ljava/lang/Class<TT;>;Lbr/com/itau/sdk/android/core/endpoint/ErrorHandler;)TT;"
        }
    .end annotation

    .line 609
    if-eqz p1, :cond_d

    invoke-virtual {p1}, Ljava/lang/Class;->isInterface()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 610
    invoke-direct {p0, p1, p2}, Lbr/com/itau/sdk/android/core/BackendClient$a;->b(Ljava/lang/Class;Lbr/com/itau/sdk/android/core/endpoint/ErrorHandler;)Ljava/lang/Object;

    move-result-object v0

    return-object v0

    .line 612
    :cond_d
    new-instance v0, Ljava/lang/RuntimeException;

    sget-object v1, Lbr/com/itau/sdk/android/core/BackendClient$a;->n:[B

    const/16 v2, 0x1d

    aget-byte v1, v1, v2

    or-int/lit8 v2, v1, 0x29

    invoke-static {v1, v1, v2}, Lbr/com/itau/sdk/android/core/BackendClient$a;->a(III)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private static a(III)Ljava/lang/String;
    .registers 9

    rsub-int/lit8 p1, p1, 0x26

    rsub-int/lit8 p0, p0, 0x50

    add-int/lit8 p2, p2, 0x4

    sget-object v4, Lbr/com/itau/sdk/android/core/BackendClient$a;->n:[B

    const/4 v5, 0x0

    new-instance v0, Ljava/lang/String;

    new-array v1, p1, [B

    if-nez v4, :cond_16

    move v2, p0

    move v3, p2

    :goto_11
    add-int/lit8 p2, p2, 0x1

    add-int/2addr v2, v3

    add-int/lit8 p0, v2, -0x2

    :cond_16
    move v2, v5

    add-int/lit8 v5, v5, 0x1

    int-to-byte v3, p0

    aput-byte v3, v1, v2

    if-ne v5, p1, :cond_23

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Ljava/lang/String;-><init>([BI)V

    return-object v0

    :cond_23
    move v2, p0

    aget-byte v3, v4, p2

    goto :goto_11
.end method

.method private a(Landroid/content/Context;Lbr/com/itau/sdk/android/core/BackendClient$Builder;)Ljava/lang/String;
    .registers 6

    .line 634
    invoke-direct {p0, p1, p2}, Lbr/com/itau/sdk/android/core/BackendClient$a;->b(Landroid/content/Context;Lbr/com/itau/sdk/android/core/BackendClient$Builder;)[B

    move-result-object v2

    .line 636
    invoke-static {p2}, Lbr/com/itau/sdk/android/core/BackendClient$Builder;->k(Lbr/com/itau/sdk/android/core/BackendClient$Builder;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v2}, Lbr/com/itau/security/middlewarechannel/Configuration;->activate(Ljava/lang/String;[B)V

    .line 637
    invoke-static {p1}, Lbr/com/itau/security/middlewarechannel/Utilities;->init(Landroid/content/Context;)V

    .line 639
    invoke-static {p2}, Lbr/com/itau/sdk/android/core/BackendClient$Builder;->c(Lbr/com/itau/sdk/android/core/BackendClient$Builder;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_27

    const-string v0, ""

    invoke-static {p2}, Lbr/com/itau/sdk/android/core/BackendClient$Builder;->c(Lbr/com/itau/sdk/android/core/BackendClient$Builder;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_27

    .line 640
    invoke-static {p2}, Lbr/com/itau/sdk/android/core/BackendClient$Builder;->c(Lbr/com/itau/sdk/android/core/BackendClient$Builder;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lbr/com/itau/security/middlewarechannel/Configuration;->setServer(Ljava/lang/String;)V

    .line 642
    :cond_27
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>([B)V

    return-object v0
.end method

.method private a(Landroid/view/View;)V
    .registers 3

    .line 678
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/BackendClient$a;->f:Lbr/com/itau/sdk/android/core/a;

    invoke-virtual {v0, p1}, Lbr/com/itau/sdk/android/core/a;->a(Landroid/view/View;)V

    .line 679
    return-void
.end method

.method static synthetic a(Lbr/com/itau/sdk/android/core/BackendClient$a;Landroid/view/View;)V
    .registers 2

    .line 515
    invoke-direct {p0, p1}, Lbr/com/itau/sdk/android/core/BackendClient$a;->a(Landroid/view/View;)V

    return-void
.end method

.method static synthetic a(Lbr/com/itau/sdk/android/core/BackendClient$a;Ljava/lang/String;)V
    .registers 2

    .line 515
    invoke-direct {p0, p1}, Lbr/com/itau/sdk/android/core/BackendClient$a;->a(Ljava/lang/String;)V

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .registers 7

    .line 591
    invoke-static {}, Lbr/com/itau/sdk/android/core/BackendClient;->getInstance()Lbr/com/itau/sdk/android/core/BackendClient;

    move-result-object v0

    invoke-static {v0}, Lbr/com/itau/sdk/android/core/BackendClient;->b(Lbr/com/itau/sdk/android/core/BackendClient;)Lbr/com/itau/sdk/android/core/BackendClient$a;

    move-result-object v0

    invoke-virtual {v0}, Lbr/com/itau/sdk/android/core/BackendClient$a;->getInitUserAgent()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4f

    .line 592
    invoke-static {}, Lbr/com/itau/sdk/android/core/BackendClient;->getInstance()Lbr/com/itau/sdk/android/core/BackendClient;

    move-result-object v0

    invoke-static {v0}, Lbr/com/itau/sdk/android/core/BackendClient;->b(Lbr/com/itau/sdk/android/core/BackendClient;)Lbr/com/itau/sdk/android/core/BackendClient$a;

    move-result-object v0

    sget-object v1, Lbr/com/itau/sdk/android/core/BackendClient$a;->n:[B

    const/16 v2, 0x1d

    aget-byte v1, v1, v2

    const/16 v2, 0x2b

    const/16 v3, 0x20

    invoke-static {v2, v3, v1}, Lbr/com/itau/sdk/android/core/BackendClient$a;->a(III)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    .line 593
    invoke-static {}, Lbr/com/itau/sdk/android/core/BackendClient;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lbr/com/itau/sdk/android/core/n;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    aput-object v3, v2, v4

    const/4 v3, 0x1

    aput-object p1, v2, v3

    .line 595
    invoke-static {}, Lbr/com/itau/sdk/android/core/BackendClient;->getInstance()Lbr/com/itau/sdk/android/core/BackendClient;

    move-result-object v3

    invoke-static {v3}, Lbr/com/itau/sdk/android/core/BackendClient;->b(Lbr/com/itau/sdk/android/core/BackendClient;)Lbr/com/itau/sdk/android/core/BackendClient$a;

    move-result-object v3

    invoke-virtual {v3}, Lbr/com/itau/sdk/android/core/BackendClient$a;->getInitUserAgent()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x2

    aput-object v3, v2, v4

    .line 593
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lbr/com/itau/sdk/android/core/BackendClient$a;->l:Ljava/lang/String;

    goto :goto_7d

    .line 597
    :cond_4f
    invoke-static {}, Lbr/com/itau/sdk/android/core/BackendClient;->getInstance()Lbr/com/itau/sdk/android/core/BackendClient;

    move-result-object v0

    invoke-static {v0}, Lbr/com/itau/sdk/android/core/BackendClient;->b(Lbr/com/itau/sdk/android/core/BackendClient;)Lbr/com/itau/sdk/android/core/BackendClient$a;

    move-result-object v0

    sget-object v1, Lbr/com/itau/sdk/android/core/BackendClient$a;->n:[B

    const/16 v2, 0x26

    aget-byte v1, v1, v2

    neg-int v1, v1

    const/16 v2, 0x2b

    const/16 v3, 0x22

    invoke-static {v2, v3, v1}, Lbr/com/itau/sdk/android/core/BackendClient$a;->a(III)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    .line 598
    invoke-static {}, Lbr/com/itau/sdk/android/core/BackendClient;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lbr/com/itau/sdk/android/core/n;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    aput-object v3, v2, v4

    const/4 v3, 0x1

    aput-object p1, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lbr/com/itau/sdk/android/core/BackendClient$a;->l:Ljava/lang/String;

    .line 601
    :goto_7d
    return-void
.end method

.method private b(Ljava/lang/Class;Lbr/com/itau/sdk/android/core/endpoint/ErrorHandler;)Ljava/lang/Object;
    .registers 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:Ljava/lang/Object;>(Ljava/lang/Class<TT;>;Lbr/com/itau/sdk/android/core/endpoint/ErrorHandler;)TT;"
        }
    .end annotation

    .line 616
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/BackendClient$a;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 618
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/BackendClient$a;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0

    .line 622
    :cond_f
    if-eqz p2, :cond_18

    .line 623
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/BackendClient$a;->b:Lbr/com/itau/sdk/android/core/endpoint/h;

    invoke-virtual {v0, p1, p2}, Lbr/com/itau/sdk/android/core/endpoint/h;->a(Ljava/lang/Class;Lbr/com/itau/sdk/android/core/endpoint/ErrorHandler;)Ljava/lang/Object;

    move-result-object v1

    goto :goto_1e

    .line 625
    :cond_18
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/BackendClient$a;->b:Lbr/com/itau/sdk/android/core/endpoint/h;

    invoke-virtual {v0, p1}, Lbr/com/itau/sdk/android/core/endpoint/h;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    .line 628
    :goto_1e
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/BackendClient$a;->a:Ljava/util/Map;

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 629
    return-object v1
.end method

.method static synthetic b(Lbr/com/itau/sdk/android/core/BackendClient$a;)Ljava/lang/String;
    .registers 2

    .line 515
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/BackendClient$a;->g:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic b(Lbr/com/itau/sdk/android/core/BackendClient$a;Ljava/lang/String;)Ljava/lang/String;
    .registers 2

    .line 515
    iput-object p1, p0, Lbr/com/itau/sdk/android/core/BackendClient$a;->l:Ljava/lang/String;

    return-object p1
.end method

.method private b()V
    .registers 2

    .line 682
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/BackendClient$a;->f:Lbr/com/itau/sdk/android/core/a;

    invoke-virtual {v0}, Lbr/com/itau/sdk/android/core/a;->b()V

    .line 683
    return-void
.end method

.method private b(Landroid/content/Context;Lbr/com/itau/sdk/android/core/BackendClient$Builder;)[B
    .registers 17

    .line 647
    const/4 v5, 0x0

    .line 650
    :try_start_1
    invoke-static/range {p2 .. p2}, Lbr/com/itau/sdk/android/core/BackendClient$Builder;->l(Lbr/com/itau/sdk/android/core/BackendClient$Builder;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_11

    invoke-static/range {p2 .. p2}, Lbr/com/itau/sdk/android/core/BackendClient$Builder;->l(Lbr/com/itau/sdk/android/core/BackendClient$Builder;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_13

    :cond_11
    const/4 v6, 0x1

    goto :goto_14

    :cond_13
    const/4 v6, 0x0

    .line 651
    :goto_14
    if-eqz v6, :cond_31

    .line 652
    new-instance v0, Ljava/io/IOException;

    sget-object v1, Lbr/com/itau/sdk/android/core/BackendClient$a;->n:[B

    const/16 v2, 0x24

    aget-byte v1, v1, v2

    sget-object v2, Lbr/com/itau/sdk/android/core/BackendClient$a;->n:[B

    const/4 v3, 0x1

    aget-byte v2, v2, v3

    sget-object v3, Lbr/com/itau/sdk/android/core/BackendClient$a;->n:[B

    const/16 v4, 0x1c

    aget-byte v3, v3, v4

    invoke-static {v1, v2, v3}, Lbr/com/itau/sdk/android/core/BackendClient$a;->a(III)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 654
    :cond_31
    invoke-virtual {p1}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    invoke-static/range {p2 .. p2}, Lbr/com/itau/sdk/android/core/BackendClient$Builder;->l(Lbr/com/itau/sdk/android/core/BackendClient$Builder;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v0

    move-object v5, v0

    .line 655
    new-instance v7, Ljava/io/BufferedInputStream;

    invoke-direct {v7, v5}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    .line 656
    invoke-virtual {v5}, Ljava/io/InputStream;->available()I

    move-result v0

    new-array v8, v0, [B

    .line 657
    invoke-virtual {v7, v8}, Ljava/io/BufferedInputStream;->read([B)I

    move-result v9

    .line 659
    if-nez v9, :cond_6c

    .line 660
    new-instance v0, Ljava/lang/RuntimeException;

    sget-object v1, Lbr/com/itau/sdk/android/core/BackendClient$a;->n:[B

    const/16 v2, 0xc

    aget-byte v1, v1, v2

    neg-int v1, v1

    sget-object v2, Lbr/com/itau/sdk/android/core/BackendClient$a;->n:[B

    const/16 v3, 0x16

    aget-byte v2, v2, v3

    sget-object v3, Lbr/com/itau/sdk/android/core/BackendClient$a;->n:[B

    const/16 v4, 0x28

    aget-byte v3, v3, v4

    invoke-static {v1, v2, v3}, Lbr/com/itau/sdk/android/core/BackendClient$a;->a(III)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_6c
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_6c} :catch_75
    .catchall {:try_start_1 .. :try_end_6c} :catchall_93

    .line 662
    :cond_6c
    move-object v10, v8

    .line 667
    if-eqz v5, :cond_74

    .line 669
    :try_start_6f
    invoke-virtual {v5}, Ljava/io/InputStream;->close()V
    :try_end_72
    .catch Ljava/io/IOException; {:try_start_6f .. :try_end_72} :catch_73

    .line 672
    goto :goto_74

    .line 670
    :catch_73
    move-exception v11

    .line 662
    :cond_74
    :goto_74
    return-object v10

    .line 663
    :catch_75
    move-exception v6

    .line 664
    :try_start_76
    new-instance v0, Ljava/lang/RuntimeException;

    sget-object v1, Lbr/com/itau/sdk/android/core/BackendClient$a;->n:[B

    const/16 v2, 0xc

    aget-byte v1, v1, v2

    neg-int v1, v1

    sget-object v2, Lbr/com/itau/sdk/android/core/BackendClient$a;->n:[B

    const/16 v3, 0x16

    aget-byte v2, v2, v3

    sget-object v3, Lbr/com/itau/sdk/android/core/BackendClient$a;->n:[B

    const/16 v4, 0x28

    aget-byte v3, v3, v4

    invoke-static {v1, v2, v3}, Lbr/com/itau/sdk/android/core/BackendClient$a;->a(III)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_93
    .catchall {:try_start_76 .. :try_end_93} :catchall_93

    .line 667
    :catchall_93
    move-exception v12

    if-eqz v5, :cond_9b

    .line 669
    :try_start_96
    invoke-virtual {v5}, Ljava/io/InputStream;->close()V
    :try_end_99
    .catch Ljava/io/IOException; {:try_start_96 .. :try_end_99} :catch_9a

    .line 672
    goto :goto_9b

    .line 670
    :catch_9a
    move-exception v13

    .line 672
    :cond_9b
    :goto_9b
    throw v12
.end method

.method static synthetic c(Lbr/com/itau/sdk/android/core/BackendClient$a;)Ljava/lang/String;
    .registers 2

    .line 515
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/BackendClient$a;->h:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic d(Lbr/com/itau/sdk/android/core/BackendClient$a;)Ljava/lang/String;
    .registers 2

    .line 515
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/BackendClient$a;->i:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic e(Lbr/com/itau/sdk/android/core/BackendClient$a;)Landroid/content/Context;
    .registers 2

    .line 515
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/BackendClient$a;->j:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic f(Lbr/com/itau/sdk/android/core/BackendClient$a;)Lbr/com/itau/sdk/android/core/FlowManager;
    .registers 2

    .line 515
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/BackendClient$a;->c:Lbr/com/itau/sdk/android/core/FlowManager;

    return-object v0
.end method

.method static synthetic g(Lbr/com/itau/sdk/android/core/BackendClient$a;)V
    .registers 1

    .line 515
    invoke-direct {p0}, Lbr/com/itau/sdk/android/core/BackendClient$a;->b()V

    return-void
.end method

.method static synthetic h(Lbr/com/itau/sdk/android/core/BackendClient$a;)Lde/greenrobot/event/EventBus;
    .registers 2

    .line 515
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/BackendClient$a;->d:Lde/greenrobot/event/EventBus;

    return-object v0
.end method

.method static synthetic i(Lbr/com/itau/sdk/android/core/BackendClient$a;)Ljava/lang/String;
    .registers 2

    .line 515
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/BackendClient$a;->l:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic j(Lbr/com/itau/sdk/android/core/BackendClient$a;)Lde/greenrobot/event/EventBus;
    .registers 2

    .line 515
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/BackendClient$a;->e:Lde/greenrobot/event/EventBus;

    return-object v0
.end method


# virtual methods
.method public callApi(Lbr/com/itau/sdk/android/core/type/TokenCall;)V
    .registers 3

    .line 587
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/BackendClient$a;->e:Lde/greenrobot/event/EventBus;

    invoke-virtual {v0, p1}, Lde/greenrobot/event/EventBus;->post(Ljava/lang/Object;)V

    .line 588
    return-void
.end method

.method public getInitUserAgent()Ljava/lang/String;
    .registers 2

    .line 583
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/BackendClient$a;->m:Lbr/com/itau/sdk/android/core/BackendClient$Builder;

    invoke-static {v0}, Lbr/com/itau/sdk/android/core/BackendClient$Builder;->g(Lbr/com/itau/sdk/android/core/BackendClient$Builder;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

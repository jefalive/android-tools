.class final Lcom/google/zxing/client/android/a/f;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/util/Comparator;


# direct methods
.method constructor <init>()V
    .registers 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/hardware/Camera$Size;Landroid/hardware/Camera$Size;)I
    .registers 7

    iget v0, p1, Landroid/hardware/Camera$Size;->height:I

    iget v1, p1, Landroid/hardware/Camera$Size;->width:I

    mul-int v2, v0, v1

    iget v0, p2, Landroid/hardware/Camera$Size;->height:I

    iget v1, p2, Landroid/hardware/Camera$Size;->width:I

    mul-int v3, v0, v1

    if-ge v3, v2, :cond_10

    const/4 v0, -0x1

    return v0

    :cond_10
    if-le v3, v2, :cond_14

    const/4 v0, 0x1

    return v0

    :cond_14
    const/4 v0, 0x0

    return v0
.end method

.method public synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .registers 5

    move-object v0, p1

    check-cast v0, Landroid/hardware/Camera$Size;

    move-object v1, p2

    check-cast v1, Landroid/hardware/Camera$Size;

    invoke-virtual {p0, v0, v1}, Lcom/google/zxing/client/android/a/f;->a(Landroid/hardware/Camera$Size;Landroid/hardware/Camera$Size;)I

    move-result v0

    return v0
.end method

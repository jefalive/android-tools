.class public Lcom/itau/empresas/feature/home/EmptyStateFragment;
.super Landroid/support/v4/app/Fragment;
.source "EmptyStateFragment.java"


# instance fields
.field descricao:Ljava/lang/String;

.field emptyStateImage:Landroid/widget/ImageView;

.field emptyStateText:Landroid/widget/TextView;

.field icone:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 16
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    return-void
.end method


# virtual methods
.method aoCarregarTela()V
    .registers 4

    .line 30
    iget-object v0, p0, Lcom/itau/empresas/feature/home/EmptyStateFragment;->emptyStateImage:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/itau/empresas/feature/home/EmptyStateFragment;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/itau/empresas/feature/home/EmptyStateFragment;->icone:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Landroid/support/v4/content/ContextCompat;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 31
    iget-object v0, p0, Lcom/itau/empresas/feature/home/EmptyStateFragment;->emptyStateText:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/itau/empresas/feature/home/EmptyStateFragment;->descricao:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 32
    return-void
.end method

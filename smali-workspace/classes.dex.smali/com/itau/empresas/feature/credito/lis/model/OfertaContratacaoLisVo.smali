.class public Lcom/itau/empresas/feature/credito/lis/model/OfertaContratacaoLisVo;
.super Ljava/lang/Object;
.source "OfertaContratacaoLisVo.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private agencia:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "agencia"
    .end annotation
.end field

.field private conta:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "conta"
    .end annotation
.end field

.field private diaPagamentoEncargo:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "dia_pagamento_encargo"
    .end annotation
.end field

.field private digitoConta:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "digito_conta"
    .end annotation
.end field

.field private indicadorContaContratual:Z
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "indicador_conta_contratual"
    .end annotation
.end field

.field private limitesOfertados:Ljava/util/List;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "limites_ofertados"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Lcom/itau/empresas/feature/credito/lis/model/LisLimiteOfertadoVO;>;"
        }
    .end annotation
.end field

.field private produtosOfertados:Ljava/util/List;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "produtos_ofertados"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Lcom/itau/empresas/feature/credito/lis/model/LisProdutoOfertadoVO;>;"
        }
    .end annotation
.end field

.field private taxaJurosMes:Ljava/lang/Float;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "taxa_juros_mes"
    .end annotation
.end field

.field private valorLimiteAdicional:I
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "valor_limite_adicional"
    .end annotation
.end field

.field private valorLimiteMinimo:F
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "valor_limite_minimo"
    .end annotation
.end field

.field private valorNovoLimite:F
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "valor_novo_limite"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getLimitesOfertados()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Lcom/itau/empresas/feature/credito/lis/model/LisLimiteOfertadoVO;>;"
        }
    .end annotation

    .line 82
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/model/OfertaContratacaoLisVo;->limitesOfertados:Ljava/util/List;

    return-object v0
.end method

.method public getProdutosOfertados()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Lcom/itau/empresas/feature/credito/lis/model/LisProdutoOfertadoVO;>;"
        }
    .end annotation

    .line 86
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/model/OfertaContratacaoLisVo;->produtosOfertados:Ljava/util/List;

    return-object v0
.end method

.method public getTaxaJurosMes()Ljava/lang/Float;
    .registers 2

    .line 62
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/model/OfertaContratacaoLisVo;->taxaJurosMes:Ljava/lang/Float;

    return-object v0
.end method

.method public getValorLimiteMinimo()F
    .registers 2

    .line 66
    iget v0, p0, Lcom/itau/empresas/feature/credito/lis/model/OfertaContratacaoLisVo;->valorLimiteMinimo:F

    return v0
.end method

.method public getValorNovoLimite()F
    .registers 2

    .line 58
    iget v0, p0, Lcom/itau/empresas/feature/credito/lis/model/OfertaContratacaoLisVo;->valorNovoLimite:F

    return v0
.end method

.class Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity$9;
.super Ljava/lang/Object;
.source "PagamentoDarfActivity.java"

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->listenerChange(Landroid/view/ViewGroup;Landroid/widget/TextView;Landroid/widget/TextView;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;

.field final synthetic val$editText:Landroid/widget/TextView;

.field final synthetic val$label:Landroid/widget/TextView;

.field final synthetic val$viewGroup:Landroid/view/ViewGroup;


# direct methods
.method constructor <init>(Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;Landroid/widget/TextView;Landroid/view/ViewGroup;Landroid/widget/TextView;)V
    .registers 5
    .param p1, "this$0"    # Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;

    .line 646
    iput-object p1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity$9;->this$0:Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;

    iput-object p2, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity$9;->val$label:Landroid/widget/TextView;

    iput-object p3, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity$9;->val$viewGroup:Landroid/view/ViewGroup;

    iput-object p4, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity$9;->val$editText:Landroid/widget/TextView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFocusChange(Landroid/view/View;Z)V
    .registers 6
    .param p1, "view"    # Landroid/view/View;
    .param p2, "hasFocus"    # Z

    .line 649
    if-nez p2, :cond_1a

    .line 650
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity$9;->this$0:Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;

    iget-object v1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity$9;->val$label:Landroid/widget/TextView;

    const/4 v2, 0x0

    # invokes: Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->trocaCorLabel(Landroid/widget/TextView;Z)V
    invoke-static {v0, v1, v2}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->access$900(Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;Landroid/widget/TextView;Z)V

    .line 651
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity$9;->this$0:Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;

    iget-object v1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity$9;->val$viewGroup:Landroid/view/ViewGroup;

    const/4 v2, 0x0

    # invokes: Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->trocaCorBackground(Landroid/view/ViewGroup;Z)V
    invoke-static {v0, v1, v2}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->access$800(Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;Landroid/view/ViewGroup;Z)V

    .line 652
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity$9;->this$0:Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;

    iget-object v1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity$9;->val$editText:Landroid/widget/TextView;

    # invokes: Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->removeEditText(Landroid/widget/TextView;)V
    invoke-static {v0, v1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->access$1000(Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;Landroid/widget/TextView;)V

    goto :goto_1f

    .line 654
    :cond_1a
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity$9;->this$0:Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;

    # invokes: Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->removeHint()V
    invoke-static {v0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->access$300(Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;)V

    .line 656
    :goto_1f
    return-void
.end method

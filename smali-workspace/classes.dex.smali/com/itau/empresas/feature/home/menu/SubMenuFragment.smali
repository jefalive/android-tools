.class public final Lcom/itau/empresas/feature/home/menu/SubMenuFragment;
.super Landroid/support/v4/app/Fragment;
.source "SubMenuFragment.java"

# interfaces
.implements Lcom/itau/empresas/feature/home/menu/adapter/CardMenuListener;
.implements Landroid/view/View$OnClickListener;


# static fields
.field public static final TAG:Ljava/lang/String;


# instance fields
.field menu:Lcom/itau/empresas/api/model/MenuVO;

.field recyclerViewMenu:Landroid/support/v7/widget/RecyclerView;

.field toolBar:Landroid/support/v7/widget/Toolbar;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .line 27
    const-class v0, Lcom/itau/empresas/feature/home/menu/SubMenuFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/itau/empresas/feature/home/menu/SubMenuFragment;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .registers 1

    .line 25
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    return-void
.end method

.method private constroiToolbar()V
    .registers 3

    .line 90
    iget-object v0, p0, Lcom/itau/empresas/feature/home/menu/SubMenuFragment;->toolBar:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {p0}, Lcom/itau/empresas/feature/home/menu/SubMenuFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    check-cast v1, Landroid/support/v7/app/AppCompatActivity;

    invoke-static {v0, v1}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom;->Builder(Landroid/support/v7/widget/Toolbar;Landroid/support/v7/app/AppCompatActivity;)Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/home/menu/SubMenuFragment;->menu:Lcom/itau/empresas/api/model/MenuVO;

    .line 91
    invoke-virtual {v1}, Lcom/itau/empresas/api/model/MenuVO;->getNome()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->comTitulo(Ljava/lang/String;)Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    move-result-object v0

    .line 92
    invoke-virtual {v0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->comClickListenerFinish()Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    move-result-object v0

    .line 93
    invoke-virtual {v0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->build()Lcom/itau/empresas/ui/util/listener/ToolbarCustom;

    .line 94
    return-void
.end method

.method private criaReyclerView(Landroid/view/View;)V
    .registers 5
    .param p1, "view"    # Landroid/view/View;

    .line 83
    const v0, 0x7f0e0477

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    iput-object v0, p0, Lcom/itau/empresas/feature/home/menu/SubMenuFragment;->recyclerViewMenu:Landroid/support/v7/widget/RecyclerView;

    .line 84
    iget-object v0, p0, Lcom/itau/empresas/feature/home/menu/SubMenuFragment;->recyclerViewMenu:Landroid/support/v7/widget/RecyclerView;

    new-instance v1, Landroid/support/v7/widget/LinearLayoutManager;

    .line 85
    invoke-virtual {p0}, Lcom/itau/empresas/feature/home/menu/SubMenuFragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/support/v7/widget/LinearLayoutManager;-><init>(Landroid/content/Context;)V

    .line 84
    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(Landroid/support/v7/widget/RecyclerView$LayoutManager;)V

    .line 86
    iget-object v0, p0, Lcom/itau/empresas/feature/home/menu/SubMenuFragment;->recyclerViewMenu:Landroid/support/v7/widget/RecyclerView;

    new-instance v1, Lcom/itau/empresas/feature/home/menu/adapter/SubMenuAdapter;

    iget-object v2, p0, Lcom/itau/empresas/feature/home/menu/SubMenuFragment;->menu:Lcom/itau/empresas/api/model/MenuVO;

    invoke-virtual {v2}, Lcom/itau/empresas/api/model/MenuVO;->getFilhos()Ljava/util/List;

    move-result-object v2

    invoke-direct {v1, v2, p0}, Lcom/itau/empresas/feature/home/menu/adapter/SubMenuAdapter;-><init>(Ljava/util/List;Lcom/itau/empresas/feature/home/menu/adapter/CardMenuListener;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 87
    return-void
.end method

.method private disparaEventoAnalytics()V
    .registers 5

    .line 97
    invoke-static {}, Lcom/itau/empresas/feature/analytics/AnalyticsHelper;->getInstance()Lcom/itau/empresas/feature/analytics/AnalyticsHelper;

    move-result-object v3

    .line 99
    .local v3, "analyticsHelper":Lcom/itau/empresas/feature/analytics/AnalyticsHelper;
    iget-object v0, p0, Lcom/itau/empresas/feature/home/menu/SubMenuFragment;->menu:Lcom/itau/empresas/api/model/MenuVO;

    if-nez v0, :cond_9

    .line 100
    return-void

    .line 103
    :cond_9
    const-string v0, "transacoes_transferencias"

    iget-object v1, p0, Lcom/itau/empresas/feature/home/menu/SubMenuFragment;->menu:Lcom/itau/empresas/api/model/MenuVO;

    invoke-virtual {v1}, Lcom/itau/empresas/api/model/MenuVO;->getChaveMobile()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_30

    .line 104
    .line 106
    const v0, 0x7f0700fe

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/home/menu/SubMenuFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 107
    const v1, 0x7f0700d1

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/home/menu/SubMenuFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 108
    const v2, 0x7f0700c3

    invoke-virtual {p0, v2}, Lcom/itau/empresas/feature/home/menu/SubMenuFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 105
    invoke-virtual {v3, v0, v1, v2}, Lcom/itau/empresas/feature/analytics/AnalyticsHelper;->eventoAdobeState(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 109
    return-void

    .line 112
    :cond_30
    const-string v0, "transacoes_autorizacao_de_transacoes"

    iget-object v1, p0, Lcom/itau/empresas/feature/home/menu/SubMenuFragment;->menu:Lcom/itau/empresas/api/model/MenuVO;

    invoke-virtual {v1}, Lcom/itau/empresas/api/model/MenuVO;->getChaveMobile()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_56

    .line 113
    .line 115
    const v0, 0x7f0700fa

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/home/menu/SubMenuFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 116
    const v1, 0x7f0700d1

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/home/menu/SubMenuFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 117
    const v2, 0x7f0700c3

    invoke-virtual {p0, v2}, Lcom/itau/empresas/feature/home/menu/SubMenuFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 114
    invoke-virtual {v3, v0, v1, v2}, Lcom/itau/empresas/feature/analytics/AnalyticsHelper;->eventoAdobeState(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 120
    :cond_56
    return-void
.end method

.method public static newInstance(Lcom/itau/empresas/api/model/MenuVO;)Lcom/itau/empresas/feature/home/menu/SubMenuFragment;
    .registers 4
    .param p0, "menu"    # Lcom/itau/empresas/api/model/MenuVO;

    .line 36
    new-instance v1, Lcom/itau/empresas/feature/home/menu/SubMenuFragment;

    invoke-direct {v1}, Lcom/itau/empresas/feature/home/menu/SubMenuFragment;-><init>()V

    .line 37
    .local v1, "fragment":Lcom/itau/empresas/feature/home/menu/SubMenuFragment;
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 38
    .local v2, "args":Landroid/os/Bundle;
    const-string v0, "menuItem"

    invoke-virtual {v2, v0, p0}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 39
    invoke-virtual {v1, v2}, Lcom/itau/empresas/feature/home/menu/SubMenuFragment;->setArguments(Landroid/os/Bundle;)V

    .line 40
    return-object v1
.end method


# virtual methods
.method public menuSelecionado(Lcom/itau/empresas/api/model/MenuVO;Z)V
    .registers 5
    .param p1, "menu"    # Lcom/itau/empresas/api/model/MenuVO;
    .param p2, "temSubMenu"    # Z

    .line 64
    if-nez p1, :cond_3

    .line 65
    return-void

    .line 66
    :cond_3
    if-eqz p2, :cond_11

    .line 67
    invoke-virtual {p0}, Lcom/itau/empresas/feature/home/menu/SubMenuFragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/itau/empresas/feature/home/menu/SubMenuActivity;->intent(Landroid/content/Context;Lcom/itau/empresas/api/model/MenuVO;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/home/menu/SubMenuFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_28

    .line 69
    :cond_11
    invoke-virtual {p1}, Lcom/itau/empresas/api/model/MenuVO;->getChaveMobile()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/itau/empresas/ui/util/menu/InformacaoMenu;->getInstance(Ljava/lang/String;)Lcom/itau/empresas/ui/util/menu/InformacaoMenu;

    move-result-object v0

    .line 70
    invoke-virtual {v0}, Lcom/itau/empresas/ui/util/menu/InformacaoMenu;->getCommand()Lcom/itau/empresas/ui/util/menu/Command;

    move-result-object v1

    .line 71
    .local v1, "command":Lcom/itau/empresas/ui/util/menu/Command;
    if-eqz v1, :cond_28

    .line 72
    invoke-virtual {p0}, Lcom/itau/empresas/feature/home/menu/SubMenuFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Landroid/support/v7/app/AppCompatActivity;

    invoke-interface {v1, v0, p1}, Lcom/itau/empresas/ui/util/menu/Command;->executar(Landroid/support/v7/app/AppCompatActivity;Lcom/itau/empresas/api/model/MenuVO;)V

    .line 75
    .end local v1    # "command":Lcom/itau/empresas/ui/util/menu/Command;
    :cond_28
    :goto_28
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .registers 3
    .param p1, "v"    # Landroid/view/View;

    .line 79
    invoke-virtual {p0}, Lcom/itau/empresas/feature/home/menu/SubMenuFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->onBackPressed()V

    .line 80
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 57
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 59
    invoke-virtual {p0}, Lcom/itau/empresas/feature/home/menu/SubMenuFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "menuItem"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/api/model/MenuVO;

    iput-object v0, p0, Lcom/itau/empresas/feature/home/menu/SubMenuFragment;->menu:Lcom/itau/empresas/api/model/MenuVO;

    .line 60
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .registers 7
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .line 47
    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    const v1, 0x7f0300c0

    invoke-virtual {p1, v1, p2, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 48
    .local v2, "view":Landroid/view/View;
    const v0, 0x7f0e00b6

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/Toolbar;

    iput-object v0, p0, Lcom/itau/empresas/feature/home/menu/SubMenuFragment;->toolBar:Landroid/support/v7/widget/Toolbar;

    .line 49
    invoke-direct {p0, v2}, Lcom/itau/empresas/feature/home/menu/SubMenuFragment;->criaReyclerView(Landroid/view/View;)V

    .line 50
    invoke-direct {p0}, Lcom/itau/empresas/feature/home/menu/SubMenuFragment;->constroiToolbar()V

    .line 51
    invoke-direct {p0}, Lcom/itau/empresas/feature/home/menu/SubMenuFragment;->disparaEventoAnalytics()V

    .line 52
    return-object v2
.end method

.class public Lcom/itau/empresas/feature/comprovantes/controller/ComprovantesController;
.super Lcom/itau/empresas/controller/BaseController;
.source "ComprovantesController.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/itau/empresas/controller/BaseController<Lcom/itau/empresas/api/Api;>;"
    }
.end annotation


# instance fields
.field app:Lcom/itau/empresas/CustomApplication;


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 15
    invoke-direct {p0}, Lcom/itau/empresas/controller/BaseController;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/itau/empresas/feature/comprovantes/controller/ComprovantesController;)Ljava/lang/Object;
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/feature/comprovantes/controller/ComprovantesController;

    .line 15
    invoke-virtual {p0}, Lcom/itau/empresas/feature/comprovantes/controller/ComprovantesController;->api()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100(Ljava/lang/Object;)V
    .registers 1
    .param p0, "x0"    # Ljava/lang/Object;

    .line 15
    invoke-static {p0}, Lcom/itau/empresas/feature/comprovantes/controller/ComprovantesController;->post(Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic access$200(Lcom/itau/empresas/feature/comprovantes/controller/ComprovantesController;)Ljava/lang/Object;
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/feature/comprovantes/controller/ComprovantesController;

    .line 15
    invoke-virtual {p0}, Lcom/itau/empresas/feature/comprovantes/controller/ComprovantesController;->api()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$300(Ljava/lang/Object;)V
    .registers 1
    .param p0, "x0"    # Ljava/lang/Object;

    .line 15
    invoke-static {p0}, Lcom/itau/empresas/feature/comprovantes/controller/ComprovantesController;->post(Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public buscacomprovantes(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 11
    .param p1, "dataInicio"    # Ljava/lang/String;
    .param p2, "dataFim"    # Ljava/lang/String;
    .param p3, "quantidadeRegistros"    # Ljava/lang/String;
    .param p4, "tipo"    # Ljava/lang/String;

    .line 25
    new-instance v0, Lcom/itau/empresas/feature/comprovantes/controller/ComprovantesController$1;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/itau/empresas/feature/comprovantes/controller/ComprovantesController$1;-><init>(Lcom/itau/empresas/feature/comprovantes/controller/ComprovantesController;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Lbr/com/itau/sdk/android/core/BackendClient;->callApi(Lbr/com/itau/sdk/android/core/type/BackendCall;)V

    .line 37
    return-void
.end method

.method public enviarComprovante(Ljava/lang/String;Lcom/itau/empresas/api/model/EnvioComprovanteVO;)V
    .registers 4
    .param p1, "idComprovante"    # Ljava/lang/String;
    .param p2, "envioComprovanteVO"    # Lcom/itau/empresas/api/model/EnvioComprovanteVO;

    .line 41
    new-instance v0, Lcom/itau/empresas/feature/comprovantes/controller/ComprovantesController$2;

    invoke-direct {v0, p0, p1, p2}, Lcom/itau/empresas/feature/comprovantes/controller/ComprovantesController$2;-><init>(Lcom/itau/empresas/feature/comprovantes/controller/ComprovantesController;Ljava/lang/String;Lcom/itau/empresas/api/model/EnvioComprovanteVO;)V

    invoke-static {v0}, Lbr/com/itau/sdk/android/core/BackendClient;->callApi(Lbr/com/itau/sdk/android/core/type/BackendCall;)V

    .line 47
    return-void
.end method

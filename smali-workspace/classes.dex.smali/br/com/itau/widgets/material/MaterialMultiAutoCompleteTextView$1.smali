.class Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView$1;
.super Ljava/lang/Object;
.source "MaterialMultiAutoCompleteTextView.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->initTextWatcher()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;


# direct methods
.method constructor <init>(Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;)V
    .registers 2

    .line 411
    iput-object p1, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView$1;->this$0:Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .registers 4
    .param p1, "s"    # Landroid/text/Editable;

    .line 422
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView$1;->this$0:Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;

    # invokes: Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->checkCharactersCount()V
    invoke-static {v0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->access$000(Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;)V

    .line 423
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView$1;->this$0:Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;

    # getter for: Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->autoValidate:Z
    invoke-static {v0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->access$100(Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 424
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView$1;->this$0:Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;

    invoke-virtual {v0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->validate()Z

    goto :goto_19

    .line 426
    :cond_13
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView$1;->this$0:Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->setError(Ljava/lang/CharSequence;)V

    .line 428
    :goto_19
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView$1;->this$0:Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;

    invoke-virtual {v0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->postInvalidate()V

    .line 429
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .registers 5
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "count"    # I
    .param p4, "after"    # I

    .line 414
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .registers 5
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "before"    # I
    .param p4, "count"    # I

    .line 418
    return-void
.end method

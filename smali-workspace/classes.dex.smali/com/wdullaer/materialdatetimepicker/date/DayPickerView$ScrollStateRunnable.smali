.class public Lcom/wdullaer/materialdatetimepicker/date/DayPickerView$ScrollStateRunnable;
.super Ljava/lang/Object;
.source "DayPickerView.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/wdullaer/materialdatetimepicker/date/DayPickerView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "ScrollStateRunnable"
.end annotation


# instance fields
.field private mNewState:I

.field final synthetic this$0:Lcom/wdullaer/materialdatetimepicker/date/DayPickerView;


# direct methods
.method protected constructor <init>(Lcom/wdullaer/materialdatetimepicker/date/DayPickerView;)V
    .registers 2

    .line 285
    iput-object p1, p0, Lcom/wdullaer/materialdatetimepicker/date/DayPickerView$ScrollStateRunnable;->this$0:Lcom/wdullaer/materialdatetimepicker/date/DayPickerView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public doScrollStateChange(Landroid/widget/AbsListView;I)V
    .registers 6
    .param p1, "view"    # Landroid/widget/AbsListView;
    .param p2, "scrollState"    # I

    .line 296
    iget-object v0, p0, Lcom/wdullaer/materialdatetimepicker/date/DayPickerView$ScrollStateRunnable;->this$0:Lcom/wdullaer/materialdatetimepicker/date/DayPickerView;

    iget-object v0, v0, Lcom/wdullaer/materialdatetimepicker/date/DayPickerView;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, p0}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 297
    iput p2, p0, Lcom/wdullaer/materialdatetimepicker/date/DayPickerView$ScrollStateRunnable;->mNewState:I

    .line 298
    iget-object v0, p0, Lcom/wdullaer/materialdatetimepicker/date/DayPickerView$ScrollStateRunnable;->this$0:Lcom/wdullaer/materialdatetimepicker/date/DayPickerView;

    iget-object v0, v0, Lcom/wdullaer/materialdatetimepicker/date/DayPickerView;->mHandler:Landroid/os/Handler;

    const-wide/16 v1, 0x28

    invoke-virtual {v0, p0, v1, v2}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 299
    return-void
.end method

.method public run()V
    .registers 12

    .line 303
    iget-object v0, p0, Lcom/wdullaer/materialdatetimepicker/date/DayPickerView$ScrollStateRunnable;->this$0:Lcom/wdullaer/materialdatetimepicker/date/DayPickerView;

    iget v1, p0, Lcom/wdullaer/materialdatetimepicker/date/DayPickerView$ScrollStateRunnable;->mNewState:I

    iput v1, v0, Lcom/wdullaer/materialdatetimepicker/date/DayPickerView;->mCurrentScrollState:I

    .line 304
    const-string v0, "MonthFragment"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_37

    .line 305
    const-string v0, "MonthFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "new scroll state: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/wdullaer/materialdatetimepicker/date/DayPickerView$ScrollStateRunnable;->mNewState:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " old state: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/wdullaer/materialdatetimepicker/date/DayPickerView$ScrollStateRunnable;->this$0:Lcom/wdullaer/materialdatetimepicker/date/DayPickerView;

    iget v2, v2, Lcom/wdullaer/materialdatetimepicker/date/DayPickerView;->mPreviousScrollState:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 309
    :cond_37
    iget v0, p0, Lcom/wdullaer/materialdatetimepicker/date/DayPickerView$ScrollStateRunnable;->mNewState:I

    if-nez v0, :cond_ac

    iget-object v0, p0, Lcom/wdullaer/materialdatetimepicker/date/DayPickerView$ScrollStateRunnable;->this$0:Lcom/wdullaer/materialdatetimepicker/date/DayPickerView;

    iget v0, v0, Lcom/wdullaer/materialdatetimepicker/date/DayPickerView;->mPreviousScrollState:I

    if-eqz v0, :cond_ac

    iget-object v0, p0, Lcom/wdullaer/materialdatetimepicker/date/DayPickerView$ScrollStateRunnable;->this$0:Lcom/wdullaer/materialdatetimepicker/date/DayPickerView;

    iget v0, v0, Lcom/wdullaer/materialdatetimepicker/date/DayPickerView;->mPreviousScrollState:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_ac

    .line 312
    iget-object v0, p0, Lcom/wdullaer/materialdatetimepicker/date/DayPickerView$ScrollStateRunnable;->this$0:Lcom/wdullaer/materialdatetimepicker/date/DayPickerView;

    iget v1, p0, Lcom/wdullaer/materialdatetimepicker/date/DayPickerView$ScrollStateRunnable;->mNewState:I

    iput v1, v0, Lcom/wdullaer/materialdatetimepicker/date/DayPickerView;->mPreviousScrollState:I

    .line 313
    const/4 v3, 0x0

    .line 314
    .local v3, "i":I
    iget-object v0, p0, Lcom/wdullaer/materialdatetimepicker/date/DayPickerView$ScrollStateRunnable;->this$0:Lcom/wdullaer/materialdatetimepicker/date/DayPickerView;

    invoke-virtual {v0, v3}, Lcom/wdullaer/materialdatetimepicker/date/DayPickerView;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 315
    .local v4, "child":Landroid/view/View;
    :goto_55
    if-eqz v4, :cond_66

    invoke-virtual {v4}, Landroid/view/View;->getBottom()I

    move-result v0

    if-gtz v0, :cond_66

    .line 316
    iget-object v0, p0, Lcom/wdullaer/materialdatetimepicker/date/DayPickerView$ScrollStateRunnable;->this$0:Lcom/wdullaer/materialdatetimepicker/date/DayPickerView;

    add-int/lit8 v3, v3, 0x1

    invoke-virtual {v0, v3}, Lcom/wdullaer/materialdatetimepicker/date/DayPickerView;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    goto :goto_55

    .line 318
    :cond_66
    if-nez v4, :cond_69

    .line 320
    return-void

    .line 322
    :cond_69
    iget-object v0, p0, Lcom/wdullaer/materialdatetimepicker/date/DayPickerView$ScrollStateRunnable;->this$0:Lcom/wdullaer/materialdatetimepicker/date/DayPickerView;

    invoke-virtual {v0}, Lcom/wdullaer/materialdatetimepicker/date/DayPickerView;->getFirstVisiblePosition()I

    move-result v5

    .line 323
    .local v5, "firstPosition":I
    iget-object v0, p0, Lcom/wdullaer/materialdatetimepicker/date/DayPickerView$ScrollStateRunnable;->this$0:Lcom/wdullaer/materialdatetimepicker/date/DayPickerView;

    invoke-virtual {v0}, Lcom/wdullaer/materialdatetimepicker/date/DayPickerView;->getLastVisiblePosition()I

    move-result v6

    .line 324
    .local v6, "lastPosition":I
    if-eqz v5, :cond_83

    iget-object v0, p0, Lcom/wdullaer/materialdatetimepicker/date/DayPickerView$ScrollStateRunnable;->this$0:Lcom/wdullaer/materialdatetimepicker/date/DayPickerView;

    invoke-virtual {v0}, Lcom/wdullaer/materialdatetimepicker/date/DayPickerView;->getCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-eq v6, v0, :cond_83

    const/4 v7, 0x1

    goto :goto_84

    :cond_83
    const/4 v7, 0x0

    .line 325
    .local v7, "scroll":Z
    :goto_84
    invoke-virtual {v4}, Landroid/view/View;->getTop()I

    move-result v8

    .line 326
    .local v8, "top":I
    invoke-virtual {v4}, Landroid/view/View;->getBottom()I

    move-result v9

    .line 327
    .local v9, "bottom":I
    iget-object v0, p0, Lcom/wdullaer/materialdatetimepicker/date/DayPickerView$ScrollStateRunnable;->this$0:Lcom/wdullaer/materialdatetimepicker/date/DayPickerView;

    invoke-virtual {v0}, Lcom/wdullaer/materialdatetimepicker/date/DayPickerView;->getHeight()I

    move-result v0

    div-int/lit8 v10, v0, 0x2

    .line 328
    .local v10, "midpoint":I
    if-eqz v7, :cond_ab

    sget v0, Lcom/wdullaer/materialdatetimepicker/date/DayPickerView;->LIST_TOP_OFFSET:I

    if-ge v8, v0, :cond_ab

    .line 329
    if-le v9, v10, :cond_a4

    .line 330
    iget-object v0, p0, Lcom/wdullaer/materialdatetimepicker/date/DayPickerView$ScrollStateRunnable;->this$0:Lcom/wdullaer/materialdatetimepicker/date/DayPickerView;

    const/16 v1, 0xfa

    invoke-virtual {v0, v8, v1}, Lcom/wdullaer/materialdatetimepicker/date/DayPickerView;->smoothScrollBy(II)V

    goto :goto_ab

    .line 332
    :cond_a4
    iget-object v0, p0, Lcom/wdullaer/materialdatetimepicker/date/DayPickerView$ScrollStateRunnable;->this$0:Lcom/wdullaer/materialdatetimepicker/date/DayPickerView;

    const/16 v1, 0xfa

    invoke-virtual {v0, v9, v1}, Lcom/wdullaer/materialdatetimepicker/date/DayPickerView;->smoothScrollBy(II)V

    .line 335
    .end local v3    # "i":I
    .end local v4    # "child":Landroid/view/View;
    .end local v5    # "firstPosition":I
    .end local v6    # "lastPosition":I
    .end local v7    # "scroll":Z
    .end local v8    # "top":I
    .end local v9    # "bottom":I
    .end local v10    # "midpoint":I
    :cond_ab
    :goto_ab
    goto :goto_b2

    .line 336
    :cond_ac
    iget-object v0, p0, Lcom/wdullaer/materialdatetimepicker/date/DayPickerView$ScrollStateRunnable;->this$0:Lcom/wdullaer/materialdatetimepicker/date/DayPickerView;

    iget v1, p0, Lcom/wdullaer/materialdatetimepicker/date/DayPickerView$ScrollStateRunnable;->mNewState:I

    iput v1, v0, Lcom/wdullaer/materialdatetimepicker/date/DayPickerView;->mPreviousScrollState:I

    .line 338
    :goto_b2
    return-void
.end method

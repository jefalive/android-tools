.class public Lbr/com/itau/widgets/graphicalfriedcake/charts/data/RadarDataSet;
.super Lbr/com/itau/widgets/graphicalfriedcake/charts/data/LineRadarDataSet;
.source "RadarDataSet.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lbr/com/itau/widgets/graphicalfriedcake/charts/data/LineRadarDataSet<Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;>;"
    }
.end annotation


# direct methods
.method public constructor <init>(Ljava/util/List;Ljava/lang/String;)V
    .registers 3
    .param p1, "yVals"    # Ljava/util/List;
    .param p2, "label"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;>;Ljava/lang/String;)V"
        }
    .end annotation

    .line 11
    invoke-direct {p0, p1, p2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/LineRadarDataSet;-><init>(Ljava/util/List;Ljava/lang/String;)V

    .line 12
    return-void
.end method


# virtual methods
.method public copy()Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;
    .registers 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet<Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;>;"
        }
    .end annotation

    .line 19
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 22
    .local v1, "yVals":Ljava/util/List;, "Ljava/util/List<Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;>;"
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_6
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/RadarDataSet;->mYVals:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_20

    .line 23
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/RadarDataSet;->mYVals:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;->copy()Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 22
    add-int/lit8 v2, v2, 0x1

    goto :goto_6

    .line 27
    .end local v2    # "i":I
    :cond_20
    new-instance v2, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/RadarDataSet;

    invoke-virtual {p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/RadarDataSet;->getLabel()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v1, v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/RadarDataSet;-><init>(Ljava/util/List;Ljava/lang/String;)V

    .line 28
    .local v2, "copied":Lbr/com/itau/widgets/graphicalfriedcake/charts/data/RadarDataSet;
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/RadarDataSet;->mColors:Ljava/util/List;

    iput-object v0, v2, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/RadarDataSet;->mColors:Ljava/util/List;

    .line 29
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/RadarDataSet;->mHighLightColor:I

    iput v0, v2, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/RadarDataSet;->mHighLightColor:I

    .line 32
    return-object v2
.end method

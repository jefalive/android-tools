.class final Lorg/threeten/bp/format/DateTimeFormatterBuilder$LocalizedOffsetPrinterParser;
.super Ljava/lang/Object;
.source "DateTimeFormatterBuilder.java"

# interfaces
.implements Lorg/threeten/bp/format/DateTimeFormatterBuilder$DateTimePrinterParser;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/threeten/bp/format/DateTimeFormatterBuilder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "LocalizedOffsetPrinterParser"
.end annotation


# instance fields
.field private final style:Lorg/threeten/bp/format/TextStyle;


# direct methods
.method public constructor <init>(Lorg/threeten/bp/format/TextStyle;)V
    .registers 2
    .param p1, "style"    # Lorg/threeten/bp/format/TextStyle;

    .line 3212
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3213
    iput-object p1, p0, Lorg/threeten/bp/format/DateTimeFormatterBuilder$LocalizedOffsetPrinterParser;->style:Lorg/threeten/bp/format/TextStyle;

    .line 3214
    return-void
.end method


# virtual methods
.method public parse(Lorg/threeten/bp/format/DateTimeParseContext;Ljava/lang/CharSequence;I)I
    .registers 18
    .param p1, "context"    # Lorg/threeten/bp/format/DateTimeParseContext;
    .param p2, "text"    # Ljava/lang/CharSequence;
    .param p3, "position"    # I

    .line 3246
    move-object v0, p1

    move-object/from16 v1, p2

    move/from16 v2, p3

    const-string v3, "GMT"

    const/4 v4, 0x0

    const/4 v5, 0x3

    invoke-virtual/range {v0 .. v5}, Lorg/threeten/bp/format/DateTimeParseContext;->subSequenceEquals(Ljava/lang/CharSequence;ILjava/lang/CharSequence;II)Z

    move-result v0

    if-nez v0, :cond_12

    .line 3247
    xor-int/lit8 v0, p3, -0x1

    return v0

    .line 3249
    :cond_12
    add-int/lit8 p3, p3, 0x3

    .line 3250
    iget-object v0, p0, Lorg/threeten/bp/format/DateTimeFormatterBuilder$LocalizedOffsetPrinterParser;->style:Lorg/threeten/bp/format/TextStyle;

    sget-object v1, Lorg/threeten/bp/format/TextStyle;->FULL:Lorg/threeten/bp/format/TextStyle;

    if-ne v0, v1, :cond_2c

    .line 3251
    new-instance v0, Lorg/threeten/bp/format/DateTimeFormatterBuilder$OffsetIdPrinterParser;

    const-string v1, ""

    const-string v2, "+HH:MM:ss"

    invoke-direct {v0, v1, v2}, Lorg/threeten/bp/format/DateTimeFormatterBuilder$OffsetIdPrinterParser;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v1, p2

    move/from16 v2, p3

    invoke-virtual {v0, p1, v1, v2}, Lorg/threeten/bp/format/DateTimeFormatterBuilder$OffsetIdPrinterParser;->parse(Lorg/threeten/bp/format/DateTimeParseContext;Ljava/lang/CharSequence;I)I

    move-result v0

    return v0

    .line 3253
    :cond_2c
    invoke-interface/range {p2 .. p2}, Ljava/lang/CharSequence;->length()I

    move-result v6

    .line 3254
    .local v6, "end":I
    move/from16 v0, p3

    if-ne v0, v6, :cond_42

    .line 3255
    move-object v0, p1

    sget-object v1, Lorg/threeten/bp/temporal/ChronoField;->OFFSET_SECONDS:Lorg/threeten/bp/temporal/ChronoField;

    move/from16 v4, p3

    move/from16 v5, p3

    const-wide/16 v2, 0x0

    invoke-virtual/range {v0 .. v5}, Lorg/threeten/bp/format/DateTimeParseContext;->setParsedField(Lorg/threeten/bp/temporal/TemporalField;JII)I

    move-result v0

    return v0

    .line 3257
    :cond_42
    move-object/from16 v0, p2

    move/from16 v1, p3

    invoke-interface {v0, v1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v7

    .line 3258
    .local v7, "sign":C
    const/16 v0, 0x2b

    if-eq v7, v0, :cond_60

    const/16 v0, 0x2d

    if-eq v7, v0, :cond_60

    .line 3259
    move-object v0, p1

    sget-object v1, Lorg/threeten/bp/temporal/ChronoField;->OFFSET_SECONDS:Lorg/threeten/bp/temporal/ChronoField;

    move/from16 v4, p3

    move/from16 v5, p3

    const-wide/16 v2, 0x0

    invoke-virtual/range {v0 .. v5}, Lorg/threeten/bp/format/DateTimeParseContext;->setParsedField(Lorg/threeten/bp/temporal/TemporalField;JII)I

    move-result v0

    return v0

    .line 3261
    :cond_60
    const/16 v0, 0x2d

    if-ne v7, v0, :cond_66

    const/4 v8, -0x1

    goto :goto_67

    :cond_66
    const/4 v8, 0x1

    .line 3262
    .local v8, "negative":I
    :goto_67
    move/from16 v0, p3

    if-ne v0, v6, :cond_6e

    .line 3263
    xor-int/lit8 v0, p3, -0x1

    return v0

    .line 3265
    :cond_6e
    add-int/lit8 p3, p3, 0x1

    .line 3267
    move-object/from16 v0, p2

    move/from16 v1, p3

    invoke-interface {v0, v1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v9

    .line 3268
    .local v9, "ch":C
    const/16 v0, 0x30

    if-lt v9, v0, :cond_80

    const/16 v0, 0x39

    if-le v9, v0, :cond_83

    .line 3269
    :cond_80
    xor-int/lit8 v0, p3, -0x1

    return v0

    .line 3271
    :cond_83
    add-int/lit8 p3, p3, 0x1

    .line 3272
    add-int/lit8 v10, v9, -0x30

    .line 3273
    .local v10, "hour":I
    move/from16 v0, p3

    if-eq v0, v6, :cond_aa

    .line 3274
    move-object/from16 v0, p2

    move/from16 v1, p3

    invoke-interface {v0, v1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v9

    .line 3275
    const/16 v0, 0x30

    if-lt v9, v0, :cond_aa

    const/16 v0, 0x39

    if-gt v9, v0, :cond_aa

    .line 3276
    mul-int/lit8 v0, v10, 0xa

    add-int/lit8 v1, v9, -0x30

    add-int v10, v0, v1

    .line 3277
    const/16 v0, 0x17

    if-le v10, v0, :cond_a8

    .line 3278
    xor-int/lit8 v0, p3, -0x1

    return v0

    .line 3280
    :cond_a8
    add-int/lit8 p3, p3, 0x1

    .line 3283
    :cond_aa
    move/from16 v0, p3

    if-eq v0, v6, :cond_ba

    move-object/from16 v0, p2

    move/from16 v1, p3

    invoke-interface {v0, v1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    const/16 v1, 0x3a

    if-eq v0, v1, :cond_cb

    .line 3284
    :cond_ba
    mul-int/lit16 v0, v8, 0xe10

    mul-int v11, v0, v10

    .line 3285
    .local v11, "offset":I
    move-object v0, p1

    sget-object v1, Lorg/threeten/bp/temporal/ChronoField;->OFFSET_SECONDS:Lorg/threeten/bp/temporal/ChronoField;

    int-to-long v2, v11

    move/from16 v4, p3

    move/from16 v5, p3

    invoke-virtual/range {v0 .. v5}, Lorg/threeten/bp/format/DateTimeParseContext;->setParsedField(Lorg/threeten/bp/temporal/TemporalField;JII)I

    move-result v0

    return v0

    .line 3287
    .end local v11    # "offset":I
    :cond_cb
    add-int/lit8 p3, p3, 0x1

    .line 3289
    add-int/lit8 v0, v6, -0x2

    move/from16 v1, p3

    if-le v1, v0, :cond_d6

    .line 3290
    xor-int/lit8 v0, p3, -0x1

    return v0

    .line 3292
    :cond_d6
    move-object/from16 v0, p2

    move/from16 v1, p3

    invoke-interface {v0, v1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v9

    .line 3293
    const/16 v0, 0x30

    if-lt v9, v0, :cond_e6

    const/16 v0, 0x39

    if-le v9, v0, :cond_e9

    .line 3294
    :cond_e6
    xor-int/lit8 v0, p3, -0x1

    return v0

    .line 3296
    :cond_e9
    add-int/lit8 p3, p3, 0x1

    .line 3297
    add-int/lit8 v11, v9, -0x30

    .line 3298
    .local v11, "min":I
    move-object/from16 v0, p2

    move/from16 v1, p3

    invoke-interface {v0, v1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v9

    .line 3299
    const/16 v0, 0x30

    if-lt v9, v0, :cond_fd

    const/16 v0, 0x39

    if-le v9, v0, :cond_100

    .line 3300
    :cond_fd
    xor-int/lit8 v0, p3, -0x1

    return v0

    .line 3302
    :cond_100
    add-int/lit8 p3, p3, 0x1

    .line 3303
    mul-int/lit8 v0, v11, 0xa

    add-int/lit8 v1, v9, -0x30

    add-int v11, v0, v1

    .line 3304
    const/16 v0, 0x3b

    if-le v11, v0, :cond_10f

    .line 3305
    xor-int/lit8 v0, p3, -0x1

    return v0

    .line 3307
    :cond_10f
    move/from16 v0, p3

    if-eq v0, v6, :cond_11f

    move-object/from16 v0, p2

    move/from16 v1, p3

    invoke-interface {v0, v1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    const/16 v1, 0x3a

    if-eq v0, v1, :cond_133

    .line 3308
    :cond_11f
    mul-int/lit16 v0, v10, 0xe10

    mul-int/lit8 v1, v11, 0x3c

    add-int/2addr v0, v1

    mul-int v12, v8, v0

    .line 3309
    .local v12, "offset":I
    move-object v0, p1

    sget-object v1, Lorg/threeten/bp/temporal/ChronoField;->OFFSET_SECONDS:Lorg/threeten/bp/temporal/ChronoField;

    int-to-long v2, v12

    move/from16 v4, p3

    move/from16 v5, p3

    invoke-virtual/range {v0 .. v5}, Lorg/threeten/bp/format/DateTimeParseContext;->setParsedField(Lorg/threeten/bp/temporal/TemporalField;JII)I

    move-result v0

    return v0

    .line 3311
    .end local v12    # "offset":I
    :cond_133
    add-int/lit8 p3, p3, 0x1

    .line 3313
    add-int/lit8 v0, v6, -0x2

    move/from16 v1, p3

    if-le v1, v0, :cond_13e

    .line 3314
    xor-int/lit8 v0, p3, -0x1

    return v0

    .line 3316
    :cond_13e
    move-object/from16 v0, p2

    move/from16 v1, p3

    invoke-interface {v0, v1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v9

    .line 3317
    const/16 v0, 0x30

    if-lt v9, v0, :cond_14e

    const/16 v0, 0x39

    if-le v9, v0, :cond_151

    .line 3318
    :cond_14e
    xor-int/lit8 v0, p3, -0x1

    return v0

    .line 3320
    :cond_151
    add-int/lit8 p3, p3, 0x1

    .line 3321
    add-int/lit8 v12, v9, -0x30

    .line 3322
    .local v12, "sec":I
    move-object/from16 v0, p2

    move/from16 v1, p3

    invoke-interface {v0, v1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v9

    .line 3323
    const/16 v0, 0x30

    if-lt v9, v0, :cond_165

    const/16 v0, 0x39

    if-le v9, v0, :cond_168

    .line 3324
    :cond_165
    xor-int/lit8 v0, p3, -0x1

    return v0

    .line 3326
    :cond_168
    add-int/lit8 p3, p3, 0x1

    .line 3327
    mul-int/lit8 v0, v12, 0xa

    add-int/lit8 v1, v9, -0x30

    add-int v12, v0, v1

    .line 3328
    const/16 v0, 0x3b

    if-le v12, v0, :cond_177

    .line 3329
    xor-int/lit8 v0, p3, -0x1

    return v0

    .line 3331
    :cond_177
    mul-int/lit16 v0, v10, 0xe10

    mul-int/lit8 v1, v11, 0x3c

    add-int/2addr v0, v1

    add-int/2addr v0, v12

    mul-int v13, v8, v0

    .line 3332
    .local v13, "offset":I
    move-object v0, p1

    sget-object v1, Lorg/threeten/bp/temporal/ChronoField;->OFFSET_SECONDS:Lorg/threeten/bp/temporal/ChronoField;

    int-to-long v2, v13

    move/from16 v4, p3

    move/from16 v5, p3

    invoke-virtual/range {v0 .. v5}, Lorg/threeten/bp/format/DateTimeParseContext;->setParsedField(Lorg/threeten/bp/temporal/TemporalField;JII)I

    move-result v0

    return v0
.end method

.method public print(Lorg/threeten/bp/format/DateTimePrintContext;Ljava/lang/StringBuilder;)Z
    .registers 11
    .param p1, "context"    # Lorg/threeten/bp/format/DateTimePrintContext;
    .param p2, "buf"    # Ljava/lang/StringBuilder;

    .line 3218
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->OFFSET_SECONDS:Lorg/threeten/bp/temporal/ChronoField;

    invoke-virtual {p1, v0}, Lorg/threeten/bp/format/DateTimePrintContext;->getValue(Lorg/threeten/bp/temporal/TemporalField;)Ljava/lang/Long;

    move-result-object v3

    .line 3219
    .local v3, "offsetSecs":Ljava/lang/Long;
    if-nez v3, :cond_a

    .line 3220
    const/4 v0, 0x0

    return v0

    .line 3222
    :cond_a
    const-string v0, "GMT"

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3223
    iget-object v0, p0, Lorg/threeten/bp/format/DateTimeFormatterBuilder$LocalizedOffsetPrinterParser;->style:Lorg/threeten/bp/format/TextStyle;

    sget-object v1, Lorg/threeten/bp/format/TextStyle;->FULL:Lorg/threeten/bp/format/TextStyle;

    if-ne v0, v1, :cond_23

    .line 3224
    new-instance v0, Lorg/threeten/bp/format/DateTimeFormatterBuilder$OffsetIdPrinterParser;

    const-string v1, ""

    const-string v2, "+HH:MM:ss"

    invoke-direct {v0, v1, v2}, Lorg/threeten/bp/format/DateTimeFormatterBuilder$OffsetIdPrinterParser;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, p1, p2}, Lorg/threeten/bp/format/DateTimeFormatterBuilder$OffsetIdPrinterParser;->print(Lorg/threeten/bp/format/DateTimePrintContext;Ljava/lang/StringBuilder;)Z

    move-result v0

    return v0

    .line 3226
    :cond_23
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-static {v0, v1}, Lorg/threeten/bp/jdk8/Jdk8Methods;->safeToInt(J)I

    move-result v4

    .line 3227
    .local v4, "totalSecs":I
    if-eqz v4, :cond_85

    .line 3228
    div-int/lit16 v0, v4, 0xe10

    rem-int/lit8 v0, v0, 0x64

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v5

    .line 3229
    .local v5, "absHours":I
    div-int/lit8 v0, v4, 0x3c

    rem-int/lit8 v0, v0, 0x3c

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v6

    .line 3230
    .local v6, "absMinutes":I
    rem-int/lit8 v0, v4, 0x3c

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v7

    .line 3231
    .local v7, "absSeconds":I
    if-gez v4, :cond_48

    const-string v0, "-"

    goto :goto_4a

    :cond_48
    const-string v0, "+"

    :goto_4a
    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 3232
    if-gtz v6, :cond_55

    if-lez v7, :cond_85

    .line 3233
    :cond_55
    const-string v0, ":"

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    div-int/lit8 v1, v6, 0xa

    add-int/lit8 v1, v1, 0x30

    int-to-char v1, v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    rem-int/lit8 v1, v6, 0xa

    add-int/lit8 v1, v1, 0x30

    int-to-char v1, v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 3235
    if-lez v7, :cond_85

    .line 3236
    const-string v0, ":"

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    div-int/lit8 v1, v7, 0xa

    add-int/lit8 v1, v1, 0x30

    int-to-char v1, v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    rem-int/lit8 v1, v7, 0xa

    add-int/lit8 v1, v1, 0x30

    int-to-char v1, v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 3241
    .end local v5    # "absHours":I
    .end local v6    # "absMinutes":I
    .end local v7    # "absSeconds":I
    :cond_85
    const/4 v0, 0x1

    return v0
.end method

.class public final Lbr/com/itau/security/did/DID;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static a:Ljava/lang/String;

.field private static final b:Ljava/lang/String;

.field private static final d:[B

.field private static e:I


# instance fields
.field private c:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 8

    .line 17
    const/16 v0, 0x10

    new-array v0, v0, [B

    fill-array-data v0, :array_46

    sput-object v0, Lbr/com/itau/security/did/DID;->d:[B

    const/16 v0, 0xc4

    sput v0, Lbr/com/itau/security/did/DID;->e:I

    .line 4000
    const/16 v5, 0x75

    const/4 v6, 0x0

    const/4 v4, 0x3

    new-instance v0, Ljava/lang/String;

    sget-object v7, Lbr/com/itau/security/did/DID;->d:[B

    const/16 v1, 0xd

    new-array v1, v1, [B

    if-nez v7, :cond_23

    const/16 v2, 0x75

    const/16 v3, 0xc

    :goto_1f
    neg-int v3, v3

    add-int/2addr v2, v3

    add-int/lit8 v5, v2, 0x1

    :cond_23
    add-int/lit8 v4, v4, 0x1

    int-to-byte v2, v5

    aput-byte v2, v1, v6

    const/16 v2, 0xc

    if-ne v6, v2, :cond_31

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Ljava/lang/String;-><init>([BI)V

    goto :goto_37

    :cond_31
    move v2, v5

    aget-byte v3, v7, v4

    add-int/lit8 v6, v6, 0x1

    goto :goto_1f

    .line 17
    :goto_37
    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lbr/com/itau/security/did/DID;->a:Ljava/lang/String;

    .line 18
    const-class v0, Lbr/com/itau/security/did/DID;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lbr/com/itau/security/did/DID;->b:Ljava/lang/String;

    return-void

    :array_46
    .array-data 1
        0x4dt
        0x44t
        0x39t
        -0x3ft
        0x3t
        0xft
        -0xct
        0x14t
        -0x14t
        0xct
        -0x3t
        0x9t
        -0x14t
        0xct
        0x2t
        0xat
    .end array-data
.end method

.method private constructor <init>()V
    .registers 1

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    return-void
.end method

.method public synthetic constructor <init>(B)V
    .registers 2

    .line 15
    invoke-direct {p0}, Lbr/com/itau/security/did/DID;-><init>()V

    return-void
.end method

.method public static a()Lbr/com/itau/security/did/DID;
    .registers 1

    .line 31
    invoke-static {}, Ldid/c;->a()Lbr/com/itau/security/did/DID;

    move-result-object v0

    return-object v0
.end method

.method public static synthetic a(Lbr/com/itau/security/did/DID;)Ljava/lang/String;
    .registers 2

    .line 15
    iget-object v0, p0, Lbr/com/itau/security/did/DID;->c:Ljava/lang/String;

    return-object v0
.end method

.method public static synthetic a(Lbr/com/itau/security/did/DID;Ljava/lang/String;)Ljava/lang/String;
    .registers 2

    .line 15
    iput-object p1, p0, Lbr/com/itau/security/did/DID;->c:Ljava/lang/String;

    return-object p1
.end method

.method public static synthetic b()Ljava/lang/String;
    .registers 1

    .line 15
    sget-object v0, Lbr/com/itau/security/did/DID;->b:Ljava/lang/String;

    return-object v0
.end method

.method private static c()Ljava/lang/String;
    .registers 8

    const/16 v6, 0x75

    const/4 v4, 0x0

    const/4 v5, 0x3

    new-instance v0, Ljava/lang/String;

    sget-object v7, Lbr/com/itau/security/did/DID;->d:[B

    const/16 v1, 0xd

    new-array v1, v1, [B

    if-nez v7, :cond_16

    const/16 v2, 0x75

    const/16 v3, 0xc

    :goto_12
    neg-int v3, v3

    add-int/2addr v2, v3

    add-int/lit8 v6, v2, 0x1

    :cond_16
    add-int/lit8 v5, v5, 0x1

    int-to-byte v2, v6

    aput-byte v2, v1, v4

    const/16 v2, 0xc

    if-ne v4, v2, :cond_24

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Ljava/lang/String;-><init>([BI)V

    return-object v0

    :cond_24
    move v2, v6

    aget-byte v3, v7, v5

    add-int/lit8 v4, v4, 0x1

    goto :goto_12
.end method

.method public static getDID(Landroid/content/Context;)Ljava/lang/String;
    .registers 2

    .line 84
    invoke-static {p0}, Ldid/d;->c(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getHDID(Landroid/content/Context;)Ljava/lang/String;
    .registers 2

    .line 80
    invoke-static {p0}, Ldid/d;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getOldDID(Landroid/content/Context;)Ljava/lang/String;
    .registers 2

    .line 88
    invoke-static {p0}, Ldid/d;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getWDID()Ljava/lang/String;
    .registers 1

    .line 2031
    invoke-static {}, Ldid/c;->a()Lbr/com/itau/security/did/DID;

    move-result-object v0

    .line 74
    iget-object v0, v0, Lbr/com/itau/security/did/DID;->c:Ljava/lang/String;

    if-nez v0, :cond_a

    .line 75
    const/4 v0, 0x0

    return-object v0

    .line 3031
    :cond_a
    invoke-static {}, Ldid/c;->a()Lbr/com/itau/security/did/DID;

    move-result-object v0

    .line 76
    iget-object v0, v0, Lbr/com/itau/security/did/DID;->c:Ljava/lang/String;

    invoke-static {v0}, Ldid/d;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static init(Landroid/content/Context;)V
    .registers 11

    .line 36
    new-instance v8, Ldid/f;

    invoke-direct {v8, p0}, Ldid/f;-><init>(Landroid/content/Context;)V

    .line 37
    sget-object v0, Lbr/com/itau/security/did/DID;->a:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v8, v0, v1}, Ldid/f;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    const-string v1, "666666666666666666666666666666666666"

    :try_start_f
    invoke-virtual {v9}, Ljava/lang/String;->getBytes()[B

    move-result-object v6

    const-string v1, "DID.smali hack"

    invoke-static {v6}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {v1, v9}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_27
    :try_end_20
    .catch Ljava/lang/Exception; {:try_start_f .. :try_end_20} :catch_20

    :catch_20
    move-exception v1

    .line 46
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_27

    const-string v1, ""

    .line 39
    :goto_27
    invoke-virtual {v9}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_34

    .line 1031
    invoke-static {}, Ldid/c;->a()Lbr/com/itau/security/did/DID;

    move-result-object v0

    .line 40
    iput-object v9, v0, Lbr/com/itau/security/did/DID;->c:Ljava/lang/String;

    .line 41
    return-void

    .line 44
    :cond_34
    new-instance v9, Landroid/content/Intent;

    invoke-direct {v9}, Landroid/content/Intent;-><init>()V

    .line 45
    sget-object v0, Lbr/com/itau/security/did/WeakDIDReceiver;->a:Ljava/lang/String;

    invoke-virtual {v9, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 47
    move-object v0, p0

    move-object v1, v9

    sget-object v2, Lbr/com/itau/security/did/WeakDIDReceiver;->b:Ljava/lang/String;

    new-instance v3, Ldid/b;

    invoke-direct {v3, v8}, Ldid/b;-><init>(Ldid/f;)V

    const/4 v4, 0x0

    const/4 v5, -0x1

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/content/Context;->sendOrderedBroadcast(Landroid/content/Intent;Ljava/lang/String;Landroid/content/BroadcastReceiver;Landroid/os/Handler;ILjava/lang/String;Landroid/os/Bundle;)V

    .line 71
    return-void
.end method

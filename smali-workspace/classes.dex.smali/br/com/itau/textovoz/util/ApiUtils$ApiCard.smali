.class public final enum Lbr/com/itau/textovoz/util/ApiUtils$ApiCard;
.super Ljava/lang/Enum;
.source "ApiUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lbr/com/itau/textovoz/util/ApiUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ApiCard"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<Lbr/com/itau/textovoz/util/ApiUtils$ApiCard;>;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lbr/com/itau/textovoz/util/ApiUtils$ApiCard;

.field public static final enum BALANCE:Lbr/com/itau/textovoz/util/ApiUtils$ApiCard;


# instance fields
.field private item:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 4

    .line 28
    new-instance v0, Lbr/com/itau/textovoz/util/ApiUtils$ApiCard;

    const-string v1, "BALANCE"

    const-string v2, "saldo"

    const/4 v3, 0x0

    invoke-direct {v0, v1, v3, v2}, Lbr/com/itau/textovoz/util/ApiUtils$ApiCard;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lbr/com/itau/textovoz/util/ApiUtils$ApiCard;->BALANCE:Lbr/com/itau/textovoz/util/ApiUtils$ApiCard;

    .line 27
    const/4 v0, 0x1

    new-array v0, v0, [Lbr/com/itau/textovoz/util/ApiUtils$ApiCard;

    sget-object v1, Lbr/com/itau/textovoz/util/ApiUtils$ApiCard;->BALANCE:Lbr/com/itau/textovoz/util/ApiUtils$ApiCard;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sput-object v0, Lbr/com/itau/textovoz/util/ApiUtils$ApiCard;->$VALUES:[Lbr/com/itau/textovoz/util/ApiUtils$ApiCard;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .registers 4
    .param p3, "url"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/lang/String;)V"
        }
    .end annotation

    .line 31
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 32
    iput-object p3, p0, Lbr/com/itau/textovoz/util/ApiUtils$ApiCard;->item:Ljava/lang/String;

    .line 33
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lbr/com/itau/textovoz/util/ApiUtils$ApiCard;
    .registers 2
    .param p0, "name"    # Ljava/lang/String;

    .line 27
    const-class v0, Lbr/com/itau/textovoz/util/ApiUtils$ApiCard;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lbr/com/itau/textovoz/util/ApiUtils$ApiCard;

    return-object v0
.end method

.method public static values()[Lbr/com/itau/textovoz/util/ApiUtils$ApiCard;
    .registers 1

    .line 27
    sget-object v0, Lbr/com/itau/textovoz/util/ApiUtils$ApiCard;->$VALUES:[Lbr/com/itau/textovoz/util/ApiUtils$ApiCard;

    invoke-virtual {v0}, [Lbr/com/itau/textovoz/util/ApiUtils$ApiCard;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lbr/com/itau/textovoz/util/ApiUtils$ApiCard;

    return-object v0
.end method


# virtual methods
.method public getItem()Ljava/lang/String;
    .registers 2

    .line 36
    iget-object v0, p0, Lbr/com/itau/textovoz/util/ApiUtils$ApiCard;->item:Ljava/lang/String;

    return-object v0
.end method

.class public final Lbr/com/itau/sdk/android/core/n;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:Lcom/google/gson/Gson;

.field public static final b:Lbr/com/itau/sdk/android/core/endpoint/i;

.field public static final c:Lokhttp3/MediaType;

.field private static final d:[B

.field private static e:I


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .line 24
    const/16 v0, 0xdf

    new-array v0, v0, [B

    fill-array-data v0, :array_3c

    sput-object v0, Lbr/com/itau/sdk/android/core/n;->d:[B

    const/16 v0, 0x17

    sput v0, Lbr/com/itau/sdk/android/core/n;->e:I

    new-instance v0, Lcom/google/gson/GsonBuilder;

    invoke-direct {v0}, Lcom/google/gson/GsonBuilder;-><init>()V

    invoke-virtual {v0}, Lcom/google/gson/GsonBuilder;->disableHtmlEscaping()Lcom/google/gson/GsonBuilder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/gson/GsonBuilder;->create()Lcom/google/gson/Gson;

    move-result-object v0

    sput-object v0, Lbr/com/itau/sdk/android/core/n;->a:Lcom/google/gson/Gson;

    .line 29
    new-instance v0, Lbr/com/itau/sdk/android/core/endpoint/i;

    sget-object v1, Lbr/com/itau/sdk/android/core/n;->a:Lcom/google/gson/Gson;

    invoke-direct {v0, v1}, Lbr/com/itau/sdk/android/core/endpoint/i;-><init>(Lcom/google/gson/Gson;)V

    sput-object v0, Lbr/com/itau/sdk/android/core/n;->b:Lbr/com/itau/sdk/android/core/endpoint/i;

    .line 34
    sget-object v0, Lbr/com/itau/sdk/android/core/n;->d:[B

    const/16 v1, 0xde

    aget-byte v0, v0, v1

    neg-int v0, v0

    add-int/lit8 v1, v0, -0x3

    const/16 v2, 0xae

    invoke-static {v0, v1, v2}, Lbr/com/itau/sdk/android/core/n;->a(III)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lokhttp3/MediaType;->parse(Ljava/lang/String;)Lokhttp3/MediaType;

    move-result-object v0

    sput-object v0, Lbr/com/itau/sdk/android/core/n;->c:Lokhttp3/MediaType;

    return-void

    nop

    :array_3c
    .array-data 1
        0x73t
        0xat
        0x53t
        0x6ft
        0x30t
        0x1t
        -0x3t
        -0x2t
        -0x5t
        -0x1t
        0x14t
        -0xat
        0x7t
        0x0t
        -0x4dt
        0x4ft
        -0xct
        0xdt
        -0x7t
        -0x44t
        0x4ft
        0x2t
        0x6t
        -0x53t
        0x47t
        0xat
        0x7t
        -0x6t
        -0x9t
        0x5t
        -0x35t
        0x25t
        0xet
        -0x9t
        0xft
        -0x2t
        -0x5t
        -0x4t
        -0x28t
        0x35t
        0x5t
        -0x13t
        0x18t
        -0x10t
        0xet
        0x2t
        -0x9t
        0x7t
        0x0t
        -0x30t
        -0x17t
        0x4ft
        -0x37t
        0x2at
        0x2t
        0x12t
        -0xct
        -0x5t
        0x3t
        -0x5t
        0xft
        0x3t
        -0xat
        0x2t
        0x8t
        -0x2et
        -0x17t
        0x4ft
        -0x37t
        0x2at
        0x2t
        0x12t
        -0xct
        -0x5t
        0x3t
        -0x5t
        0xft
        -0xbt
        0xbt
        -0x5t
        0xet
        -0x34t
        -0x17t
        0x4ft
        -0x37t
        0x2at
        0x2t
        0x12t
        -0xct
        -0x5t
        0x3t
        -0x5t
        0x16t
        0x6t
        -0x8t
        -0xat
        -0x27t
        -0x17t
        0x4ft
        -0x37t
        0x27t
        0x10t
        0x1t
        -0x10t
        0xbt
        -0x4t
        -0x26t
        -0x17t
        0x4ft
        -0x37t
        0x27t
        0x10t
        0x1t
        -0x10t
        0x18t
        -0x10t
        0xet
        0x2t
        -0x9t
        0x7t
        0x0t
        -0x30t
        -0x17t
        0x4ft
        -0x37t
        0x38t
        -0x2t
        0x1t
        0x6t
        -0x36t
        -0x17t
        0x4ft
        -0x37t
        -0x5t
        -0xbt
        0x12t
        0x3t
        -0x3t
        -0x7t
        0x8t
        0x0t
        -0x8t
        0xet
        0x1t
        -0x2t
        -0x2t
        0x1t
        0x6t
        -0x14t
        0xbt
        0x6t
        -0x7t
        0xat
        0x7t
        0x13t
        0x9t
        -0x4ct
        0x53t
        -0x2t
        0x1t
        0x6t
        -0x4dt
        0x4at
        -0x4t
        0x2t
        0xat
        0x7t
        -0xat
        -0x2t
        0x4t
        -0x5t
        -0x1t
        0x4t
        0xct
        -0x12t
        0x2t
        0xbt
        -0x6t
        0x10t
        0x10t
        0x1t
        -0x3t
        -0x2t
        -0x5t
        -0x1t
        0x14t
        -0xat
        0x7t
        0x0t
        -0x3et
        0x3ct
        0xat
        -0x3t
        0x0t
        -0x32t
        -0x1at
        0x44t
        0x6t
        -0x6t
        0x12t
        0x2t
        -0xdt
        0x10t
        -0x36t
        0x19t
        0x0t
        -0xdt
        -0x18t
        0xct
        0xdt
        -0x1t
        -0x2ct
        0x53t
        -0x2t
        0x1t
        0x6t
        -0xdt
        0x6t
        -0x9t
        0xct
        0x9t
        -0x21t
        -0x20t
    .end array-data
.end method

.method private static a(III)Ljava/lang/String;
    .registers 9

    sget-object v4, Lbr/com/itau/sdk/android/core/n;->d:[B

    new-instance v0, Ljava/lang/String;

    add-int/lit8 p1, p1, 0x2

    add-int/lit8 p0, p0, 0x41

    add-int/lit8 p2, p2, 0x4

    const/4 v5, 0x0

    new-array v1, p1, [B

    add-int/lit8 p1, p1, -0x1

    if-nez v4, :cond_16

    move v2, p1

    move v3, p2

    :goto_13
    add-int/2addr v2, v3

    add-int/lit8 p0, v2, -0x1

    :cond_16
    add-int/lit8 p2, p2, 0x1

    int-to-byte v2, p0

    aput-byte v2, v1, v5

    if-ne v5, p1, :cond_22

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Ljava/lang/String;-><init>([BI)V

    return-object v0

    :cond_22
    move v2, p0

    add-int/lit8 v5, v5, 0x1

    aget-byte v3, v4, p2

    goto :goto_13
.end method

.method public static a(Landroid/content/Context;)Ljava/lang/String;
    .registers 8

    .line 64
    sget-object v0, Lbr/com/itau/sdk/android/core/n;->d:[B

    const/16 v1, 0x41

    aget-byte v0, v0, v1

    neg-int v0, v0

    sget-object v1, Lbr/com/itau/sdk/android/core/n;->d:[B

    const/16 v2, 0x2a

    aget-byte v1, v1, v2

    const/16 v2, 0x67

    invoke-static {v0, v2, v1}, Lbr/com/itau/sdk/android/core/n;->a(III)Ljava/lang/String;

    move-result-object v5

    .line 66
    invoke-static {p0}, Lbr/com/itau/sdk/android/core/n;->d(Landroid/content/Context;)Landroid/content/pm/PackageInfo;

    move-result-object v6

    .line 68
    sget-object v0, Lbr/com/itau/sdk/android/core/n;->d:[B

    const/16 v1, 0x41

    aget-byte v0, v0, v1

    neg-int v0, v0

    sget-object v1, Lbr/com/itau/sdk/android/core/n;->d:[B

    const/16 v2, 0x2a

    aget-byte v1, v1, v2

    const/16 v2, 0x67

    invoke-static {v0, v2, v1}, Lbr/com/itau/sdk/android/core/n;->a(III)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x7

    new-array v1, v1, [Ljava/lang/Object;

    sget-object v2, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    sget-object v2, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const/4 v3, 0x1

    aput-object v2, v1, v3

    sget-object v2, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    const/4 v3, 0x2

    aput-object v2, v1, v3

    .line 73
    invoke-static {p0}, Lbr/com/itau/sdk/android/core/n;->c(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_51

    sget-object v2, Lbr/com/itau/sdk/android/core/n;->d:[B

    const/16 v3, 0xa9

    aget-byte v2, v2, v3

    const/16 v3, 0x33

    const/16 v4, 0xa9

    invoke-static {v3, v2, v4}, Lbr/com/itau/sdk/android/core/n;->a(III)Ljava/lang/String;

    move-result-object v2

    goto :goto_64

    :cond_51
    sget-object v2, Lbr/com/itau/sdk/android/core/n;->d:[B

    const/16 v3, 0xc2

    aget-byte v2, v2, v3

    neg-int v2, v2

    sget-object v3, Lbr/com/itau/sdk/android/core/n;->d:[B

    const/16 v4, 0x40

    aget-byte v3, v3, v4

    const/16 v4, 0x80

    invoke-static {v2, v3, v4}, Lbr/com/itau/sdk/android/core/n;->a(III)Ljava/lang/String;

    move-result-object v2

    :goto_64
    const/4 v3, 0x3

    aput-object v2, v1, v3

    iget-object v2, v6, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    const/4 v3, 0x4

    aput-object v2, v1, v3

    iget-object v2, v6, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    const/4 v3, 0x5

    aput-object v2, v1, v3

    .line 76
    invoke-static {p0}, Lbr/com/itau/sdk/android/core/n;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x6

    aput-object v2, v1, v3

    .line 69
    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 68
    return-object v0
.end method

.method protected static b(Landroid/content/Context;)Ljava/lang/String;
    .registers 8

    .line 87
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 89
    sget-object v0, Lbr/com/itau/sdk/android/core/n;->d:[B

    const/4 v1, 0x4

    aget-byte v0, v0, v1

    add-int/lit8 v0, v0, 0x1

    sget-object v1, Lbr/com/itau/sdk/android/core/n;->d:[B

    const/16 v2, 0xc

    aget-byte v1, v1, v2

    const/16 v2, 0x8c

    invoke-static {v0, v1, v2}, Lbr/com/itau/sdk/android/core/n;->a(III)Ljava/lang/String;

    move-result-object v4

    .line 92
    sget-boolean v0, Lbr/com/itau/sdk/android/core/d;->a:Z

    if-eqz v0, :cond_45

    .line 93
    sget-object v0, Lbr/com/itau/sdk/android/core/n;->d:[B

    const/16 v1, 0xad

    aget-byte v0, v0, v1

    or-int/lit16 v1, v0, 0x90

    const/16 v2, 0x2d

    invoke-static {v2, v0, v1}, Lbr/com/itau/sdk/android/core/n;->a(III)Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Lbr/com/itau/sdk/android/core/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 94
    sget v0, Lbr/com/itau/sdk/android/core/n;->e:I

    and-int/lit8 v0, v0, 0x79

    sget-object v1, Lbr/com/itau/sdk/android/core/n;->d:[B

    const/16 v2, 0xd

    aget-byte v1, v1, v2

    or-int/lit16 v2, v1, 0x94

    invoke-static {v0, v1, v2}, Lbr/com/itau/sdk/android/core/n;->a(III)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 95
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 99
    :cond_45
    :try_start_45
    new-instance v5, Lrc/itau/In;

    invoke-direct {v5}, Lrc/itau/In;-><init>()V

    .line 100
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Lrc/itau/In;->c(Ljava/lang/String;)I

    move-result v6

    .line 102
    packed-switch v6, :pswitch_data_124

    goto/16 :goto_d5

    .line 104
    :pswitch_57
    sget-object v0, Lbr/com/itau/sdk/android/core/n;->d:[B

    const/16 v1, 0x37

    aget-byte v0, v0, v1

    sget-object v1, Lbr/com/itau/sdk/android/core/n;->d:[B

    const/16 v2, 0x16

    aget-byte v1, v1, v2

    const/16 v2, 0x95

    invoke-static {v0, v1, v2}, Lbr/com/itau/sdk/android/core/n;->a(III)Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Lbr/com/itau/sdk/android/core/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 105
    sget v0, Lbr/com/itau/sdk/android/core/n;->e:I

    and-int/lit8 v0, v0, 0x79

    sget-object v1, Lbr/com/itau/sdk/android/core/n;->d:[B

    const/16 v2, 0xd

    aget-byte v1, v1, v2

    or-int/lit16 v2, v1, 0xd8

    invoke-static {v0, v1, v2}, Lbr/com/itau/sdk/android/core/n;->a(III)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 106
    goto/16 :goto_f9

    .line 108
    :pswitch_81
    sget-object v0, Lbr/com/itau/sdk/android/core/n;->d:[B

    const/16 v1, 0x15

    aget-byte v0, v0, v1

    sget-object v1, Lbr/com/itau/sdk/android/core/n;->d:[B

    const/16 v2, 0x16

    aget-byte v1, v1, v2

    const/16 v2, 0xcc

    invoke-static {v0, v1, v2}, Lbr/com/itau/sdk/android/core/n;->a(III)Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Lbr/com/itau/sdk/android/core/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 109
    sget v0, Lbr/com/itau/sdk/android/core/n;->e:I

    and-int/lit8 v0, v0, 0x79

    sget-object v1, Lbr/com/itau/sdk/android/core/n;->d:[B

    const/16 v2, 0xd

    aget-byte v1, v1, v2

    or-int/lit16 v2, v1, 0xd9

    invoke-static {v0, v1, v2}, Lbr/com/itau/sdk/android/core/n;->a(III)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 110
    goto :goto_f9

    .line 112
    :pswitch_aa
    sget-object v0, Lbr/com/itau/sdk/android/core/n;->d:[B

    const/16 v1, 0x1f

    aget-byte v0, v0, v1

    add-int/lit8 v0, v0, -0x1

    sget-object v1, Lbr/com/itau/sdk/android/core/n;->d:[B

    const/16 v2, 0x15

    aget-byte v1, v1, v2

    const/16 v2, 0x89

    invoke-static {v0, v1, v2}, Lbr/com/itau/sdk/android/core/n;->a(III)Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Lbr/com/itau/sdk/android/core/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 113
    sget v0, Lbr/com/itau/sdk/android/core/n;->e:I

    and-int/lit8 v0, v0, 0x79

    sget-object v1, Lbr/com/itau/sdk/android/core/n;->d:[B

    const/16 v2, 0xd

    aget-byte v1, v1, v2

    or-int/lit16 v2, v1, 0x94

    invoke-static {v0, v1, v2}, Lbr/com/itau/sdk/android/core/n;->a(III)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 114
    goto :goto_f9

    .line 116
    :goto_d5
    sget-object v0, Lbr/com/itau/sdk/android/core/n;->d:[B

    const/16 v1, 0xad

    aget-byte v0, v0, v1

    or-int/lit16 v1, v0, 0x90

    const/16 v2, 0x2d

    invoke-static {v2, v0, v1}, Lbr/com/itau/sdk/android/core/n;->a(III)Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Lbr/com/itau/sdk/android/core/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 117
    sget v0, Lbr/com/itau/sdk/android/core/n;->e:I

    and-int/lit8 v0, v0, 0x79

    sget-object v1, Lbr/com/itau/sdk/android/core/n;->d:[B

    const/16 v2, 0xd

    aget-byte v1, v1, v2

    or-int/lit16 v2, v1, 0x94

    invoke-static {v0, v1, v2}, Lbr/com/itau/sdk/android/core/n;->a(III)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_f9
    .catch Ljava/lang/Exception; {:try_start_45 .. :try_end_f9} :catch_fa

    .line 123
    :goto_f9
    goto :goto_11f

    .line 120
    :catch_fa
    move-exception v5

    .line 121
    sget-object v0, Lbr/com/itau/sdk/android/core/n;->d:[B

    const/16 v1, 0xad

    aget-byte v0, v0, v1

    or-int/lit16 v1, v0, 0x90

    const/16 v2, 0x2d

    invoke-static {v2, v0, v1}, Lbr/com/itau/sdk/android/core/n;->a(III)Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Lbr/com/itau/sdk/android/core/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 122
    sget v0, Lbr/com/itau/sdk/android/core/n;->e:I

    and-int/lit8 v0, v0, 0x79

    sget-object v1, Lbr/com/itau/sdk/android/core/n;->d:[B

    const/16 v2, 0xd

    aget-byte v1, v1, v2

    or-int/lit16 v2, v1, 0x94

    invoke-static {v0, v1, v2}, Lbr/com/itau/sdk/android/core/n;->a(III)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 125
    :goto_11f
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :pswitch_data_124
    .packed-switch 0x0
        :pswitch_57
        :pswitch_81
        :pswitch_aa
    .end packed-switch
.end method

.method public static c(Landroid/content/Context;)Z
    .registers 10

    .line 136
    new-instance v3, Landroid/util/DisplayMetrics;

    invoke-direct {v3}, Landroid/util/DisplayMetrics;-><init>()V

    .line 137
    sget-object v0, Lbr/com/itau/sdk/android/core/n;->d:[B

    const/16 v1, 0x81

    aget-byte v0, v0, v1

    neg-int v0, v0

    sget-object v1, Lbr/com/itau/sdk/android/core/n;->d:[B

    const/16 v2, 0xa9

    aget-byte v1, v1, v2

    const/16 v2, 0xd3

    invoke-static {v0, v1, v2}, Lbr/com/itau/sdk/android/core/n;->a(III)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Landroid/view/WindowManager;

    .line 138
    invoke-interface {v4}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 140
    iget v5, v3, Landroid/util/DisplayMetrics;->density:F

    .line 141
    iget v0, v3, Landroid/util/DisplayMetrics;->widthPixels:I

    int-to-float v0, v0

    div-float v6, v0, v5

    .line 142
    iget v0, v3, Landroid/util/DisplayMetrics;->heightPixels:I

    int-to-float v0, v0

    div-float v7, v0, v5

    .line 143
    invoke-static {v6, v7}, Ljava/lang/Math;->min(FF)F

    move-result v8

    .line 145
    const/high16 v0, 0x44160000    # 600.0f

    cmpl-float v0, v8, v0

    if-ltz v0, :cond_3e

    const/4 v0, 0x1

    goto :goto_3f

    :cond_3e
    const/4 v0, 0x0

    :goto_3f
    return v0
.end method

.method private static d(Landroid/content/Context;)Landroid/content/pm/PackageInfo;
    .registers 8

    .line 48
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    .line 50
    :try_start_4
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v5, v0, v1}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_c
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_4 .. :try_end_c} :catch_e

    move-result-object v0

    return-object v0

    .line 51
    :catch_e
    move-exception v6

    .line 52
    new-instance v0, Ljava/lang/RuntimeException;

    sget-object v1, Lbr/com/itau/sdk/android/core/n;->d:[B

    const/16 v2, 0xd

    aget-byte v1, v1, v2

    sget-object v2, Lbr/com/itau/sdk/android/core/n;->d:[B

    const/16 v3, 0x2a

    aget-byte v2, v2, v3

    sget-object v3, Lbr/com/itau/sdk/android/core/n;->d:[B

    const/16 v4, 0x9

    aget-byte v3, v3, v4

    invoke-static {v1, v2, v3}, Lbr/com/itau/sdk/android/core/n;->a(III)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

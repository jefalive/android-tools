.class public Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoRecargaVO;
.super Ljava/lang/Object;
.source "ResultadoRecargaVO.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field codigoPagamento:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "codigo_pagamento"
    .end annotation
.end field

.field dataRecarga:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "data_recarga"
    .end annotation
.end field

.field dddTelefone:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "ddd_telefone"
    .end annotation
.end field

.field identificacaoComprovante:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "identificacao_comprovante"
    .end annotation
.end field

.field mensagemAviso:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "mensagem_aviso"
    .end annotation
.end field

.field moedaRecarga:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "moeda_recarga"
    .end annotation
.end field

.field nomeOperadora:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "nome_operadora"
    .end annotation
.end field

.field nomeRegiao:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "nome_regiao"
    .end annotation
.end field

.field numeroTelefone:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "numero_telefone"
    .end annotation
.end field

.field valorBonus:Ljava/lang/Float;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "valor_bonus"
    .end annotation
.end field

.field valorRecarga:Ljava/lang/Float;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "valor_recarga"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getCodigoPagamento()Ljava/lang/String;
    .registers 2

    .line 65
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoRecargaVO;->codigoPagamento:Ljava/lang/String;

    return-object v0
.end method

.method public getDataRecarga()Ljava/lang/String;
    .registers 2

    .line 57
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoRecargaVO;->dataRecarga:Ljava/lang/String;

    return-object v0
.end method

.method public getDddTelefone()Ljava/lang/String;
    .registers 2

    .line 42
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoRecargaVO;->dddTelefone:Ljava/lang/String;

    invoke-static {v0}, Lcom/itau/empresas/ui/util/ViewUtils;->removaPaddingsZero(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getIdentificacaoComprovante()Ljava/lang/String;
    .registers 2

    .line 77
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoRecargaVO;->identificacaoComprovante:Ljava/lang/String;

    return-object v0
.end method

.method public getNomeOperadora()Ljava/lang/String;
    .registers 2

    .line 34
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoRecargaVO;->nomeOperadora:Ljava/lang/String;

    return-object v0
.end method

.method public getNomeRegiao()Ljava/lang/String;
    .registers 2

    .line 69
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoRecargaVO;->nomeRegiao:Ljava/lang/String;

    return-object v0
.end method

.method public getNumeroTelefone()Ljava/lang/String;
    .registers 3

    .line 46
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoRecargaVO;->numeroTelefone:Ljava/lang/String;

    if-eqz v0, :cond_16

    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoRecargaVO;->numeroTelefone:Ljava/lang/String;

    const-string v1, "0"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_16

    .line 47
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoRecargaVO;->numeroTelefone:Ljava/lang/String;

    invoke-static {v0}, Lcom/itau/empresas/ui/util/ViewUtils;->removaPaddingsZero(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoRecargaVO;->numeroTelefone:Ljava/lang/String;

    .line 49
    :cond_16
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoRecargaVO;->numeroTelefone:Ljava/lang/String;

    return-object v0
.end method

.method public getValorBonus()Ljava/lang/Float;
    .registers 2

    .line 61
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoRecargaVO;->valorBonus:Ljava/lang/Float;

    return-object v0
.end method

.method public getValorRecarga()Ljava/lang/Float;
    .registers 2

    .line 53
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoRecargaVO;->valorRecarga:Ljava/lang/Float;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    .line 83
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ResultadoRecargaVO{codigoPagamento=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoRecargaVO;->codigoPagamento:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mensagemAviso=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoRecargaVO;->mensagemAviso:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", valorBonus="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoRecargaVO;->valorBonus:Ljava/lang/Float;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", dataRecarga=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoRecargaVO;->dataRecarga:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", valorRecarga="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoRecargaVO;->valorRecarga:Ljava/lang/Float;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", moedaRecarga=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoRecargaVO;->moedaRecarga:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", numeroTelefone=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoRecargaVO;->numeroTelefone:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", dddTelefone=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoRecargaVO;->dddTelefone:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", nomeOperadora=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoRecargaVO;->nomeOperadora:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", nomeRegiao=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoRecargaVO;->nomeRegiao:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", identificacaoComprovante =\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoRecargaVO;->identificacaoComprovante:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.class public Lcom/itau/empresas/ui/util/HintUtils;
.super Ljava/lang/Object;
.source "HintUtils.java"


# instance fields
.field private sharedPreferences:Landroid/content/SharedPreferences;


# direct methods
.method public constructor <init>(Landroid/content/SharedPreferences;)V
    .registers 2
    .param p1, "sharedPreferences"    # Landroid/content/SharedPreferences;

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    iput-object p1, p0, Lcom/itau/empresas/ui/util/HintUtils;->sharedPreferences:Landroid/content/SharedPreferences;

    .line 14
    return-void
.end method

.method public static montaHintAlerta(Lbr/com/itau/widgets/hintview/Hint$Builder;Ljava/lang/String;I)Lbr/com/itau/widgets/hintview/Hint$Builder;
    .registers 4
    .param p0, "builder"    # Lbr/com/itau/widgets/hintview/Hint$Builder;
    .param p1, "mensagem"    # Ljava/lang/String;
    .param p2, "gravity"    # I

    .line 32
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lbr/com/itau/widgets/hintview/Hint$Builder;->setCancelable(Z)Lbr/com/itau/widgets/hintview/Hint$Builder;

    move-result-object v0

    .line 33
    invoke-virtual {v0, p2}, Lbr/com/itau/widgets/hintview/Hint$Builder;->setGravity(I)Lbr/com/itau/widgets/hintview/Hint$Builder;

    move-result-object v0

    .line 34
    invoke-virtual {v0, p1}, Lbr/com/itau/widgets/hintview/Hint$Builder;->setText(Ljava/lang/CharSequence;)Lbr/com/itau/widgets/hintview/Hint$Builder;

    move-result-object v0

    .line 32
    return-object v0
.end method

.method public static montaHintAlertaMenuItem(Lbr/com/itau/widgets/hintview/Hint$Builder;Ljava/lang/String;I)Lbr/com/itau/widgets/hintview/Hint$Builder;
    .registers 4
    .param p0, "builder"    # Lbr/com/itau/widgets/hintview/Hint$Builder;
    .param p1, "mensagem"    # Ljava/lang/String;
    .param p2, "gravity"    # I

    .line 39
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lbr/com/itau/widgets/hintview/Hint$Builder;->setCancelable(Z)Lbr/com/itau/widgets/hintview/Hint$Builder;

    move-result-object v0

    .line 40
    invoke-virtual {v0, p2}, Lbr/com/itau/widgets/hintview/Hint$Builder;->setGravity(I)Lbr/com/itau/widgets/hintview/Hint$Builder;

    move-result-object v0

    .line 41
    invoke-virtual {v0, p1}, Lbr/com/itau/widgets/hintview/Hint$Builder;->setText(Ljava/lang/CharSequence;)Lbr/com/itau/widgets/hintview/Hint$Builder;

    move-result-object v0

    .line 39
    return-object v0
.end method

.method public static montaHintErro(Lbr/com/itau/widgets/hintview/Hint$Builder;Ljava/lang/String;)Lbr/com/itau/widgets/hintview/Hint$Builder;
    .registers 3
    .param p0, "builder"    # Lbr/com/itau/widgets/hintview/Hint$Builder;
    .param p1, "mensagem"    # Ljava/lang/String;

    .line 26
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lbr/com/itau/widgets/hintview/Hint$Builder;->setCancelable(Z)Lbr/com/itau/widgets/hintview/Hint$Builder;

    move-result-object v0

    .line 27
    invoke-virtual {v0, p1}, Lbr/com/itau/widgets/hintview/Hint$Builder;->setText(Ljava/lang/CharSequence;)Lbr/com/itau/widgets/hintview/Hint$Builder;

    move-result-object v0

    .line 26
    return-object v0
.end method


# virtual methods
.method public consultarPreferenciasQuantidade(Ljava/lang/String;)I
    .registers 4
    .param p1, "chave"    # Ljava/lang/String;

    .line 45
    iget-object v0, p0, Lcom/itau/empresas/ui/util/HintUtils;->sharedPreferences:Landroid/content/SharedPreferences;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public desativarHint(Ljava/lang/String;Z)V
    .registers 5
    .param p1, "chave"    # Ljava/lang/String;
    .param p2, "valor"    # Z

    .line 56
    iget-object v0, p0, Lcom/itau/empresas/ui/util/HintUtils;->sharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 57
    .local v1, "ed":Landroid/content/SharedPreferences$Editor;
    invoke-interface {v1, p1, p2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 58
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 59
    return-void
.end method

.method public exibeHint(Ljava/lang/String;)Z
    .registers 4
    .param p1, "chave"    # Ljava/lang/String;

    .line 62
    iget-object v0, p0, Lcom/itau/empresas/ui/util/HintUtils;->sharedPreferences:Landroid/content/SharedPreferences;

    const/4 v1, 0x1

    invoke-interface {v0, p1, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public registraAcesso(Ljava/lang/String;I)V
    .registers 5
    .param p1, "chave"    # Ljava/lang/String;
    .param p2, "valor"    # I

    .line 50
    iget-object v0, p0, Lcom/itau/empresas/ui/util/HintUtils;->sharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 51
    .local v1, "ed":Landroid/content/SharedPreferences$Editor;
    invoke-interface {v1, p1, p2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 52
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 53
    return-void
.end method

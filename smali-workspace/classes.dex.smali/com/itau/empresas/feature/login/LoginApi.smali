.class public interface abstract Lcom/itau/empresas/feature/login/LoginApi;
.super Ljava/lang/Object;
.source "LoginApi.java"


# virtual methods
.method public abstract buscaContasOperador(Ljava/lang/String;)Ljava/util/List;
    .param p1    # Ljava/lang/String;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Path;
            value = "codigo_operador"
        .end annotation
    .end param
    .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/GET;
        opKey = "listaContas"
    .end annotation

    .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Headers;
        value = {
            "itau-chave: 38d6287a-9f93-4e70-8d03-dac48cdb5a09"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/lang/String;)Ljava/util/List<Lcom/itau/empresas/api/model/ContaOperadorVO;>;"
        }
    .end annotation
.end method

.method public abstract consultaDadosOperador(Ljava/lang/String;)Lcom/itau/empresas/api/model/DadosOperadorVO;
    .param p1    # Ljava/lang/String;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Path;
            value = "codigo_operador"
        .end annotation
    .end param
    .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/GET;
        opKey = "operadorDetalhes"
    .end annotation

    .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Headers;
        value = {
            "itau-chave: 38d6287a-9f93-4e70-8d03-dac48cdb5a09"
        }
    .end annotation
.end method

.method public abstract login(Lcom/itau/empresas/api/model/LoginInputVO;)Lcom/itau/empresas/api/model/PerfilOperadorVO;
    .param p1    # Lcom/itau/empresas/api/model/LoginInputVO;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Body;
        .end annotation
    .end param
    .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Headers;
        value = {
            "itau-chave: 38d6287a-9f93-4e70-8d03-dac48cdb5a09"
        }
    .end annotation

    .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/POST;
        opKey = "logon"
    .end annotation
.end method

.method public abstract menu(I)Lcom/itau/empresas/api/model/MenuVO;
    .param p1    # I
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Path;
            value = "versao"
        .end annotation
    .end param
    .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Headers;
        value = {
            "itau-chave: 38d6287a-9f93-4e70-8d03-dac48cdb5a09"
        }
    .end annotation

    .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/POST;
        opKey = "menu"
    .end annotation
.end method

.method public abstract tecladoVirtual()Lcom/itau/empresas/feature/login/model/TecladoVirtualVO;
    .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Headers;
        value = {
            "itau-chave: 38d6287a-9f93-4e70-8d03-dac48cdb5a09"
        }
    .end annotation

    .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/POST;
        opKey = "teclado"
    .end annotation
.end method

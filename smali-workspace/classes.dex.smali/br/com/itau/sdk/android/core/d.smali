.class public final Lbr/com/itau/sdk/android/core/d;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:Z

.field private static final g:[B

.field private static h:I


# direct methods
.method static constructor <clinit>()V
    .registers 4

    .line 7
    const/16 v0, 0x28

    new-array v0, v0, [B

    fill-array-data v0, :array_28

    sput-object v0, Lbr/com/itau/sdk/android/core/d;->g:[B

    const/16 v0, 0x29

    sput v0, Lbr/com/itau/sdk/android/core/d;->h:I

    sget-object v0, Lbr/com/itau/sdk/android/core/d;->g:[B

    const/4 v1, 0x0

    aget-byte v0, v0, v1

    sget-object v1, Lbr/com/itau/sdk/android/core/d;->g:[B

    const/16 v2, 0x15

    aget-byte v1, v1, v2

    neg-int v1, v1

    sget v2, Lbr/com/itau/sdk/android/core/d;->h:I

    and-int/lit16 v2, v2, 0xf0

    invoke-static {v0, v1, v2}, Lbr/com/itau/sdk/android/core/d;->a(III)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lbr/com/itau/sdk/android/core/d;->a:Z

    return-void

    :array_28
    .array-data 1
        0x8t
        0x72t
        -0x25t
        0x12t
        0x11t
        -0x43t
        0x36t
        0xdt
        -0x1t
        -0x3et
        0x3ct
        0xct
        -0x12t
        0x15t
        -0x46t
        0x46t
        -0xet
        0x8t
        -0x3ct
        0x34t
        0xet
        -0x9t
        0xft
        -0x2t
        -0x5t
        -0x4t
        -0x35t
        0x36t
        0xdt
        0x4t
        -0xct
        -0xct
        0x8t
        -0x6t
        -0x3t
        0x13t
        -0xdt
        -0x1t
        0x4t
        -0xft
    .end array-data
.end method

.method public constructor <init>()V
    .registers 1

    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(III)Ljava/lang/String;
    .registers 9

    add-int/lit8 p2, p2, 0x4

    mul-int/lit8 p0, p0, 0x3

    rsub-int/lit8 p0, p0, 0x1c

    sget-object v5, Lbr/com/itau/sdk/android/core/d;->g:[B

    mul-int/lit8 p1, p1, 0x2

    add-int/lit8 p1, p1, 0x62

    new-instance v0, Ljava/lang/String;

    const/4 v4, 0x0

    new-array v1, p0, [B

    add-int/lit8 p0, p0, -0x1

    if-nez v5, :cond_1a

    move v2, p0

    move v3, p2

    :goto_17
    add-int/2addr v2, v3

    add-int/lit8 p1, v2, -0x1

    :cond_1a
    add-int/lit8 p2, p2, 0x1

    int-to-byte v2, p1

    aput-byte v2, v1, v4

    if-ne v4, p0, :cond_26

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Ljava/lang/String;-><init>([BI)V

    return-object v0

    :cond_26
    move v2, p1

    add-int/lit8 v4, v4, 0x1

    aget-byte v3, v5, p2

    goto :goto_17
.end method

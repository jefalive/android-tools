.class public abstract Lcom/google/android/gms/internal/zzso;
.super Lcom/google/android/gms/internal/zzsu;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<M:Lcom/google/android/gms/internal/zzso<TM;>;>Lcom/google/android/gms/internal/zzsu;"
    }
.end annotation


# instance fields
.field protected zzbuj:Lcom/google/android/gms/internal/zzsq;


# direct methods
.method public constructor <init>()V
    .registers 1

    invoke-direct {p0}, Lcom/google/android/gms/internal/zzsu;-><init>()V

    return-void
.end method


# virtual methods
.method public synthetic clone()Lcom/google/android/gms/internal/zzsu;
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/gms/internal/zzso;->zzJp()Lcom/google/android/gms/internal/zzso;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/gms/internal/zzso;->zzJp()Lcom/google/android/gms/internal/zzso;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/android/gms/internal/zzsn;)V
    .registers 5
    .param p1, "output"    # Lcom/google/android/gms/internal/zzsn;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/gms/internal/zzso;->zzbuj:Lcom/google/android/gms/internal/zzsq;

    if-nez v0, :cond_5

    return-void

    :cond_5
    const/4 v1, 0x0

    :goto_6
    iget-object v0, p0, Lcom/google/android/gms/internal/zzso;->zzbuj:Lcom/google/android/gms/internal/zzsq;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/zzsq;->size()I

    move-result v0

    if-ge v1, v0, :cond_1a

    iget-object v0, p0, Lcom/google/android/gms/internal/zzso;->zzbuj:Lcom/google/android/gms/internal/zzsq;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/internal/zzsq;->zzmG(I)Lcom/google/android/gms/internal/zzsr;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/google/android/gms/internal/zzsr;->writeTo(Lcom/google/android/gms/internal/zzsn;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_6

    :cond_1a
    return-void
.end method

.method public zzJp()Lcom/google/android/gms/internal/zzso;
    .registers 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TM;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    invoke-super {p0}, Lcom/google/android/gms/internal/zzsu;->clone()Lcom/google/android/gms/internal/zzsu;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/google/android/gms/internal/zzso;

    invoke-static {p0, v1}, Lcom/google/android/gms/internal/zzss;->zza(Lcom/google/android/gms/internal/zzso;Lcom/google/android/gms/internal/zzso;)V

    return-object v1
.end method

.method protected final zza(Lcom/google/android/gms/internal/zzsm;I)Z
    .registers 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p1}, Lcom/google/android/gms/internal/zzsm;->getPosition()I

    move-result v1

    invoke-virtual {p1, p2}, Lcom/google/android/gms/internal/zzsm;->zzmo(I)Z

    move-result v0

    if-nez v0, :cond_c

    const/4 v0, 0x0

    return v0

    :cond_c
    invoke-static {p2}, Lcom/google/android/gms/internal/zzsx;->zzmJ(I)I

    move-result v2

    invoke-virtual {p1}, Lcom/google/android/gms/internal/zzsm;->getPosition()I

    move-result v3

    sub-int v0, v3, v1

    invoke-virtual {p1, v1, v0}, Lcom/google/android/gms/internal/zzsm;->zzz(II)[B

    move-result-object v4

    new-instance v5, Lcom/google/android/gms/internal/zzsw;

    invoke-direct {v5, p2, v4}, Lcom/google/android/gms/internal/zzsw;-><init>(I[B)V

    const/4 v6, 0x0

    iget-object v0, p0, Lcom/google/android/gms/internal/zzso;->zzbuj:Lcom/google/android/gms/internal/zzsq;

    if-nez v0, :cond_2c

    new-instance v0, Lcom/google/android/gms/internal/zzsq;

    invoke-direct {v0}, Lcom/google/android/gms/internal/zzsq;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/internal/zzso;->zzbuj:Lcom/google/android/gms/internal/zzsq;

    goto :goto_32

    :cond_2c
    iget-object v0, p0, Lcom/google/android/gms/internal/zzso;->zzbuj:Lcom/google/android/gms/internal/zzsq;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/internal/zzsq;->zzmF(I)Lcom/google/android/gms/internal/zzsr;

    move-result-object v6

    :goto_32
    if-nez v6, :cond_3e

    new-instance v6, Lcom/google/android/gms/internal/zzsr;

    invoke-direct {v6}, Lcom/google/android/gms/internal/zzsr;-><init>()V

    iget-object v0, p0, Lcom/google/android/gms/internal/zzso;->zzbuj:Lcom/google/android/gms/internal/zzsq;

    invoke-virtual {v0, v2, v6}, Lcom/google/android/gms/internal/zzsq;->zza(ILcom/google/android/gms/internal/zzsr;)V

    :cond_3e
    invoke-virtual {v6, v5}, Lcom/google/android/gms/internal/zzsr;->zza(Lcom/google/android/gms/internal/zzsw;)V

    const/4 v0, 0x1

    return v0
.end method

.method protected zzz()I
    .registers 5

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/gms/internal/zzso;->zzbuj:Lcom/google/android/gms/internal/zzsq;

    if-eqz v0, :cond_1c

    const/4 v2, 0x0

    :goto_6
    iget-object v0, p0, Lcom/google/android/gms/internal/zzso;->zzbuj:Lcom/google/android/gms/internal/zzsq;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/zzsq;->size()I

    move-result v0

    if-ge v2, v0, :cond_1c

    iget-object v0, p0, Lcom/google/android/gms/internal/zzso;->zzbuj:Lcom/google/android/gms/internal/zzsq;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/internal/zzsq;->zzmG(I)Lcom/google/android/gms/internal/zzsr;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/internal/zzsr;->zzz()I

    move-result v0

    add-int/2addr v1, v0

    add-int/lit8 v2, v2, 0x1

    goto :goto_6

    :cond_1c
    return v1
.end method

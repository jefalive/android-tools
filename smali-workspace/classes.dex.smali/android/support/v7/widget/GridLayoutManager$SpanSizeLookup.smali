.class public abstract Landroid/support/v7/widget/GridLayoutManager$SpanSizeLookup;
.super Ljava/lang/Object;
.source "GridLayoutManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/support/v7/widget/GridLayoutManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "SpanSizeLookup"
.end annotation


# instance fields
.field private mCacheSpanIndices:Z

.field final mSpanIndexCache:Landroid/util/SparseIntArray;


# direct methods
.method public constructor <init>()V
    .registers 2

    .line 835
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 837
    new-instance v0, Landroid/util/SparseIntArray;

    invoke-direct {v0}, Landroid/util/SparseIntArray;-><init>()V

    iput-object v0, p0, Landroid/support/v7/widget/GridLayoutManager$SpanSizeLookup;->mSpanIndexCache:Landroid/util/SparseIntArray;

    .line 839
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v7/widget/GridLayoutManager$SpanSizeLookup;->mCacheSpanIndices:Z

    return-void
.end method


# virtual methods
.method findReferenceIndexFromCache(I)I
    .registers 8
    .param p1, "position"    # I

    .line 945
    const/4 v2, 0x0

    .line 946
    .local v2, "lo":I
    iget-object v0, p0, Landroid/support/v7/widget/GridLayoutManager$SpanSizeLookup;->mSpanIndexCache:Landroid/util/SparseIntArray;

    invoke-virtual {v0}, Landroid/util/SparseIntArray;->size()I

    move-result v0

    add-int/lit8 v3, v0, -0x1

    .line 948
    .local v3, "hi":I
    :goto_9
    if-gt v2, v3, :cond_1d

    .line 949
    add-int v0, v2, v3

    ushr-int/lit8 v4, v0, 0x1

    .line 950
    .local v4, "mid":I
    iget-object v0, p0, Landroid/support/v7/widget/GridLayoutManager$SpanSizeLookup;->mSpanIndexCache:Landroid/util/SparseIntArray;

    invoke-virtual {v0, v4}, Landroid/util/SparseIntArray;->keyAt(I)I

    move-result v5

    .line 951
    .local v5, "midVal":I
    if-ge v5, p1, :cond_1a

    .line 952
    add-int/lit8 v2, v4, 0x1

    goto :goto_1c

    .line 954
    :cond_1a
    add-int/lit8 v3, v4, -0x1

    .line 956
    .end local v4    # "mid":I
    .end local v5    # "midVal":I
    :goto_1c
    goto :goto_9

    .line 957
    :cond_1d
    add-int/lit8 v4, v2, -0x1

    .line 958
    .local v4, "index":I
    if-ltz v4, :cond_30

    iget-object v0, p0, Landroid/support/v7/widget/GridLayoutManager$SpanSizeLookup;->mSpanIndexCache:Landroid/util/SparseIntArray;

    invoke-virtual {v0}, Landroid/util/SparseIntArray;->size()I

    move-result v0

    if-ge v4, v0, :cond_30

    .line 959
    iget-object v0, p0, Landroid/support/v7/widget/GridLayoutManager$SpanSizeLookup;->mSpanIndexCache:Landroid/util/SparseIntArray;

    invoke-virtual {v0, v4}, Landroid/util/SparseIntArray;->keyAt(I)I

    move-result v0

    return v0

    .line 961
    :cond_30
    const/4 v0, -0x1

    return v0
.end method

.method getCachedSpanIndex(II)I
    .registers 7
    .param p1, "position"    # I
    .param p2, "spanCount"    # I

    .line 878
    iget-boolean v0, p0, Landroid/support/v7/widget/GridLayoutManager$SpanSizeLookup;->mCacheSpanIndices:Z

    if-nez v0, :cond_9

    .line 879
    invoke-virtual {p0, p1, p2}, Landroid/support/v7/widget/GridLayoutManager$SpanSizeLookup;->getSpanIndex(II)I

    move-result v0

    return v0

    .line 881
    :cond_9
    iget-object v0, p0, Landroid/support/v7/widget/GridLayoutManager$SpanSizeLookup;->mSpanIndexCache:Landroid/util/SparseIntArray;

    const/4 v1, -0x1

    invoke-virtual {v0, p1, v1}, Landroid/util/SparseIntArray;->get(II)I

    move-result v2

    .line 882
    .local v2, "existing":I
    const/4 v0, -0x1

    if-eq v2, v0, :cond_14

    .line 883
    return v2

    .line 885
    :cond_14
    invoke-virtual {p0, p1, p2}, Landroid/support/v7/widget/GridLayoutManager$SpanSizeLookup;->getSpanIndex(II)I

    move-result v3

    .line 886
    .local v3, "value":I
    iget-object v0, p0, Landroid/support/v7/widget/GridLayoutManager$SpanSizeLookup;->mSpanIndexCache:Landroid/util/SparseIntArray;

    invoke-virtual {v0, p1, v3}, Landroid/util/SparseIntArray;->put(II)V

    .line 887
    return v3
.end method

.method public getSpanGroupIndex(II)I
    .registers 9
    .param p1, "adapterPosition"    # I
    .param p2, "spanCount"    # I

    .line 975
    const/4 v1, 0x0

    .line 976
    .local v1, "span":I
    const/4 v2, 0x0

    .line 977
    .local v2, "group":I
    invoke-virtual {p0, p1}, Landroid/support/v7/widget/GridLayoutManager$SpanSizeLookup;->getSpanSize(I)I

    move-result v3

    .line 978
    .local v3, "positionSpanSize":I
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_7
    if-ge v4, p1, :cond_1c

    .line 979
    invoke-virtual {p0, v4}, Landroid/support/v7/widget/GridLayoutManager$SpanSizeLookup;->getSpanSize(I)I

    move-result v5

    .line 980
    .local v5, "size":I
    add-int/2addr v1, v5

    .line 981
    if-ne v1, p2, :cond_14

    .line 982
    const/4 v1, 0x0

    .line 983
    add-int/lit8 v2, v2, 0x1

    goto :goto_19

    .line 984
    :cond_14
    if-le v1, p2, :cond_19

    .line 986
    move v1, v5

    .line 987
    add-int/lit8 v2, v2, 0x1

    .line 978
    .end local v5    # "size":I
    :cond_19
    :goto_19
    add-int/lit8 v4, v4, 0x1

    goto :goto_7

    .line 990
    .end local v4    # "i":I
    :cond_1c
    add-int v0, v1, v3

    if-le v0, p2, :cond_22

    .line 991
    add-int/lit8 v2, v2, 0x1

    .line 993
    :cond_22
    return v2
.end method

.method public getSpanIndex(II)I
    .registers 10
    .param p1, "position"    # I
    .param p2, "spanCount"    # I

    .line 914
    invoke-virtual {p0, p1}, Landroid/support/v7/widget/GridLayoutManager$SpanSizeLookup;->getSpanSize(I)I

    move-result v2

    .line 915
    .local v2, "positionSpanSize":I
    if-ne v2, p2, :cond_8

    .line 916
    const/4 v0, 0x0

    return v0

    .line 918
    :cond_8
    const/4 v3, 0x0

    .line 919
    .local v3, "span":I
    const/4 v4, 0x0

    .line 921
    .local v4, "startPos":I
    iget-boolean v0, p0, Landroid/support/v7/widget/GridLayoutManager$SpanSizeLookup;->mCacheSpanIndices:Z

    if-eqz v0, :cond_2a

    iget-object v0, p0, Landroid/support/v7/widget/GridLayoutManager$SpanSizeLookup;->mSpanIndexCache:Landroid/util/SparseIntArray;

    invoke-virtual {v0}, Landroid/util/SparseIntArray;->size()I

    move-result v0

    if-lez v0, :cond_2a

    .line 922
    invoke-virtual {p0, p1}, Landroid/support/v7/widget/GridLayoutManager$SpanSizeLookup;->findReferenceIndexFromCache(I)I

    move-result v5

    .line 923
    .local v5, "prevKey":I
    if-ltz v5, :cond_2a

    .line 924
    iget-object v0, p0, Landroid/support/v7/widget/GridLayoutManager$SpanSizeLookup;->mSpanIndexCache:Landroid/util/SparseIntArray;

    invoke-virtual {v0, v5}, Landroid/util/SparseIntArray;->get(I)I

    move-result v0

    invoke-virtual {p0, v5}, Landroid/support/v7/widget/GridLayoutManager$SpanSizeLookup;->getSpanSize(I)I

    move-result v1

    add-int v3, v0, v1

    .line 925
    add-int/lit8 v4, v5, 0x1

    .line 928
    .end local v5    # "prevKey":I
    :cond_2a
    move v5, v4

    .local v5, "i":I
    :goto_2b
    if-ge v5, p1, :cond_3c

    .line 929
    invoke-virtual {p0, v5}, Landroid/support/v7/widget/GridLayoutManager$SpanSizeLookup;->getSpanSize(I)I

    move-result v6

    .line 930
    .local v6, "size":I
    add-int/2addr v3, v6

    .line 931
    if-ne v3, p2, :cond_36

    .line 932
    const/4 v3, 0x0

    goto :goto_39

    .line 933
    :cond_36
    if-le v3, p2, :cond_39

    .line 935
    move v3, v6

    .line 928
    .end local v6    # "size":I
    :cond_39
    :goto_39
    add-int/lit8 v5, v5, 0x1

    goto :goto_2b

    .line 938
    .end local v5    # "i":I
    :cond_3c
    add-int v0, v3, v2

    if-gt v0, p2, :cond_41

    .line 939
    return v3

    .line 941
    :cond_41
    const/4 v0, 0x0

    return v0
.end method

.method public abstract getSpanSize(I)I
.end method

.method public invalidateSpanIndexCache()V
    .registers 2

    .line 865
    iget-object v0, p0, Landroid/support/v7/widget/GridLayoutManager$SpanSizeLookup;->mSpanIndexCache:Landroid/util/SparseIntArray;

    invoke-virtual {v0}, Landroid/util/SparseIntArray;->clear()V

    .line 866
    return-void
.end method

.class public final Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;
.super Lbr/com/itau/sdk/android/core/token/ui/BaseTokenDialog;
.source "SourceFile"


# static fields
.field private static final h:[S

.field private static i:I


# instance fields
.field private a:Lbr/com/itau/sdk/android/core_ui/token/SMSTokenView;

.field private b:Lbr/com/itau/sdk/android/core_ui/token/ChaveiroTokenView;

.field private c:Lbr/com/itau/sdk/android/core_ui/token/APPTokenView;

.field private d:Landroid/widget/ImageView;

.field private e:Landroid/widget/TextView;

.field private f:Ljava/util/List;

.field private g:Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    const/16 v0, 0xf0

    new-array v0, v0, [S

    fill-array-data v0, :array_e

    sput-object v0, Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;->h:[S

    const/16 v0, 0xb3

    sput v0, Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;->i:I

    return-void

    :array_e
    .array-data 2
        0x8s
        0x64s
        0x49s
        0xes
        -0x8s
        -0x38s
        -0x19s
        -0x17s
        -0x26s
        -0x1es
        -0x2bs
        -0xcs
        -0x27s
        -0x20s
        -0x1fs
        -0x21s
        -0xds
        -0x21s
        -0x23s
        0x32s
        -0x66s
        -0x8s
        -0x38s
        -0x19s
        -0x17s
        -0x26s
        -0x31s
        -0x13s
        -0x21s
        -0x25s
        -0x14s
        -0x12s
        0x1s
        -0x3bs
        -0x20s
        -0x1as
        -0x2fs
        -0x1es
        -0x1bs
        -0xcs
        -0x2es
        0x1s
        -0x38s
        -0x19s
        -0x17s
        -0x26s
        0x10s
        -0x4cs
        -0x1ds
        -0x1bs
        -0x2as
        -0x12s
        -0x1fs
        -0x24s
        -0x12s
        -0x2es
        0x12s
        -0x60s
        -0xads
        0x72s
        -0x22s
        -0x1bs
        -0x25s
        0x32s
        -0x61s
        -0x22s
        -0x1bs
        -0x1fs
        -0x28s
        -0xas
        -0x20s
        -0x28s
        0x32s
        -0x62s
        -0x2bs
        -0x1es
        -0x8as
        0xa4s
        -0x66s
        -0x22s
        -0x12s
        -0x29s
        -0x20s
        -0x1ds
        -0x10s
        -0x2cs
        -0x18s
        0x24s
        -0xfs
        -0x4ds
        -0x3cs
        -0x20s
        0x35s
        -0x63s
        -0x18s
        -0x32s
        -0x16s
        -0x20s
        0x29s
        -0x11s
        -0x71s
        -0xes
        -0x26s
        -0x23s
        -0xes
        0x28s
        -0x6bs
        -0x1es
        -0x24s
        -0x8s
        -0x29s
        -0x15s
        -0x26s
        -0x23s
        -0xes
        0x1as
        -0x1es
        -0x2bs
        -0xcs
        -0x27s
        -0x20s
        -0x1fs
        -0x21s
        -0xds
        -0x19s
        -0x2es
        -0x2cs
        -0x19s
        -0x1as
        -0x17s
        -0x1bs
        -0x30s
        -0x12s
        -0x2as
        -0x16s
        -0x17s
        -0x23s
        0x12s
        -0x70s
        -0xfs
        -0x2ds
        0x38s
        -0x66s
        -0x8s
        -0x38s
        -0x19s
        -0x17s
        -0x26s
        0x31s
        -0x62s
        -0x2bs
        -0x1es
        -0x8as
        0xa4s
        -0x5fs
        -0x27s
        -0x20s
        -0x1fs
        -0x21s
        -0xds
        -0x19s
        -0x20s
        -0x28s
        -0x22s
        -0x16s
        -0x32s
        -0xcs
        -0x21s
        -0x26s
        -0x1as
        -0x36s
        -0x11s
        -0xa4s
        0xads
        -0x61s
        -0x1es
        -0x2es
        -0xcs
        0x28s
        -0x73s
        -0x8s
        -0x28s
        -0x1as
        -0x18s
        -0x1as
        -0x2es
        0x35s
        -0x6as
        -0x11s
        -0x25s
        -0x27s
        0x36s
        -0x2fs
        -0xbs
        -0x60s
        -0xads
        0x72s
        -0x22s
        -0x1bs
        -0x25s
        -0x21s
        0x36s
        -0x6bs
        -0x10s
        0x24s
        -0x70s
        -0xfs
        -0x29s
        -0x21s
        -0x92s
        0x5fs
        -0x12s
        -0x23s
        -0x15s
        0x24s
        -0x6ds
        -0xes
        -0x2es
        -0xcs
        0x24s
        -0x63s
        -0x18s
        -0x36s
        -0x8s
        -0x2as
        0x35s
        -0x6cs
        0x32s
        -0x61s
        -0x1es
        -0x2bs
        -0xcs
        -0x27s
        -0x20s
        -0x1fs
        -0x21s
        -0xds
        -0x21s
        -0x23s
        0x24s
    .end array-data
.end method

.method public constructor <init>()V
    .registers 1

    .line 32
    invoke-direct {p0}, Lbr/com/itau/sdk/android/core/token/ui/BaseTokenDialog;-><init>()V

    return-void
.end method

.method private static a(III)Ljava/lang/String;
    .registers 9

    rsub-int p0, p0, 0xaa

    rsub-int/lit8 p1, p1, 0x73

    const/4 v5, 0x0

    add-int/lit8 p2, p2, 0x3

    sget-object v4, Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;->h:[S

    new-instance v0, Ljava/lang/String;

    new-array v1, p2, [B

    if-nez v4, :cond_17

    move v2, p1

    move v3, p0

    :goto_11
    add-int/lit8 p0, p0, 0x1

    neg-int v3, v3

    add-int/2addr v2, v3

    add-int/lit8 p1, v2, -0x1d

    :cond_17
    move v2, v5

    int-to-byte v3, p1

    add-int/lit8 v5, v5, 0x1

    aput-byte v3, v1, v2

    if-ne v5, p2, :cond_24

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Ljava/lang/String;-><init>([BI)V

    return-object v0

    :cond_24
    move v2, p1

    aget-short v3, v4, p0

    goto :goto_11
.end method

.method static synthetic a(Landroid/content/DialogInterface;I)V
    .registers 2

    invoke-static {p0, p1}, Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;->b(Landroid/content/DialogInterface;I)V

    return-void
.end method

.method private synthetic a(Landroid/view/View;)V
    .registers 2

    .line 98
    invoke-virtual {p0}, Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;->onBackPressed()V

    return-void
.end method

.method static synthetic a(Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;)V
    .registers 1

    invoke-direct {p0}, Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;->d()V

    return-void
.end method

.method static synthetic a(Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;Landroid/content/DialogInterface;I)V
    .registers 3

    invoke-direct {p0, p1, p2}, Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;->c(Landroid/content/DialogInterface;I)V

    return-void
.end method

.method static synthetic a(Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;Landroid/view/View;)V
    .registers 2

    invoke-direct {p0, p1}, Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;->a(Landroid/view/View;)V

    return-void
.end method

.method static synthetic a(Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;Ljava/lang/String;Ljava/lang/String;)V
    .registers 3

    invoke-direct {p0, p1, p2}, Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;->c(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private synthetic a(Ljava/lang/String;Ljava/lang/String;)V
    .registers 7

    .line 65
    invoke-virtual {p0}, Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;->showLoading()V

    .line 66
    sget-object v0, Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;->h:[S

    const/16 v1, 0x55

    aget-short v0, v0, v1

    neg-int v0, v0

    sget-object v1, Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;->h:[S

    const/16 v2, 0x38

    aget-short v1, v1, v2

    sget-object v2, Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;->h:[S

    const/4 v3, 0x0

    aget-short v2, v2, v3

    add-int/lit8 v2, v2, -0x1

    invoke-static {v0, v1, v2}, Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;->a(III)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;->setTokenSelecionado(Ljava/lang/String;)V

    .line 67
    invoke-static {p1, p2}, Lbr/com/itau/sdk/android/core/SDKCustomUiController;->validaTokenAPP(Ljava/lang/String;Ljava/lang/String;)V

    .line 68
    return-void
.end method

.method private b()V
    .registers 3

    .line 124
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;->d:Landroid/widget/ImageView;

    invoke-static {}, Lbr/com/itau/sdk/android/core_ui/theme/b;->e()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 125
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;->d:Landroid/widget/ImageView;

    invoke-static {v0}, Lbr/com/itau/sdk/android/core_ui/theme/b;->a(Landroid/widget/ImageView;)V

    .line 126
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;->loading:Landroid/view/View;

    invoke-static {v0}, Lbr/com/itau/sdk/android/core_ui/theme/b;->b(Landroid/view/View;)V

    .line 127
    return-void
.end method

.method private static synthetic b(Landroid/content/DialogInterface;I)V
    .registers 2

    .line 248
    return-void
.end method

.method static synthetic b(Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;Ljava/lang/String;Ljava/lang/String;)V
    .registers 3

    invoke-direct {p0, p1, p2}, Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;->b(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private synthetic b(Ljava/lang/String;Ljava/lang/String;)V
    .registers 6

    .line 60
    invoke-virtual {p0}, Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;->showLoading()V

    .line 61
    sget-object v0, Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;->h:[S

    const/4 v1, 0x0

    aget-short v0, v0, v1

    add-int/lit8 v0, v0, -0x1

    sget-object v1, Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;->h:[S

    const/16 v2, 0x2e

    aget-short v1, v1, v2

    const/4 v2, 0x5

    invoke-static {v0, v1, v2}, Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;->a(III)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;->setTokenSelecionado(Ljava/lang/String;)V

    .line 62
    invoke-static {p1, p2}, Lbr/com/itau/sdk/android/core/SDKCustomUiController;->validaTokenFisico(Ljava/lang/String;Ljava/lang/String;)V

    .line 63
    return-void
.end method

.method private c()V
    .registers 10

    .line 130
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 132
    invoke-virtual {p0}, Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;->getUiModel()Lbr/com/itau/sdk/android/core/model/UiModelDTO;

    move-result-object v0

    invoke-virtual {v0}, Lbr/com/itau/sdk/android/core/model/UiModelDTO;->getNumeroSerieTokenSMS()Ljava/lang/String;

    move-result-object v5

    .line 134
    invoke-virtual {p0, v5}, Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;->isValidToken(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_23

    .line 136
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;->a:Lbr/com/itau/sdk/android/core_ui/token/SMSTokenView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lbr/com/itau/sdk/android/core_ui/token/SMSTokenView;->setVisibility(I)V

    .line 137
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;->a:Lbr/com/itau/sdk/android/core_ui/token/SMSTokenView;

    invoke-virtual {v0}, Lbr/com/itau/sdk/android/core_ui/token/SMSTokenView;->c()V

    .line 139
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;->a:Lbr/com/itau/sdk/android/core_ui/token/SMSTokenView;

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 142
    :cond_23
    invoke-virtual {p0}, Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;->getUiModel()Lbr/com/itau/sdk/android/core/model/UiModelDTO;

    move-result-object v0

    invoke-virtual {v0}, Lbr/com/itau/sdk/android/core/model/UiModelDTO;->getNumeroSerieTokenFisico()Ljava/lang/String;

    move-result-object v6

    .line 144
    invoke-virtual {p0, v6}, Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;->isValidToken(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5f

    .line 146
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;->b:Lbr/com/itau/sdk/android/core_ui/token/ChaveiroTokenView;

    invoke-virtual {v0, v6}, Lbr/com/itau/sdk/android/core_ui/token/ChaveiroTokenView;->setToken(Ljava/lang/String;)V

    .line 147
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;->b:Lbr/com/itau/sdk/android/core_ui/token/ChaveiroTokenView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lbr/com/itau/sdk/android/core_ui/token/ChaveiroTokenView;->setVisibility(I)V

    .line 148
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;->b:Lbr/com/itau/sdk/android/core_ui/token/ChaveiroTokenView;

    invoke-virtual {v0}, Lbr/com/itau/sdk/android/core_ui/token/ChaveiroTokenView;->c()V

    .line 150
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;->a:Lbr/com/itau/sdk/android/core_ui/token/SMSTokenView;

    invoke-virtual {v0}, Lbr/com/itau/sdk/android/core_ui/token/SMSTokenView;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_5a

    .line 151
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;->b:Lbr/com/itau/sdk/android/core_ui/token/ChaveiroTokenView;

    .line 152
    invoke-virtual {v0}, Lbr/com/itau/sdk/android/core_ui/token/ChaveiroTokenView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Landroid/widget/RelativeLayout$LayoutParams;

    .line 153
    sget v0, Lbr/com/itau/sdk/android/core_ui/R$id;->token_divisor:I

    const/4 v1, 0x3

    invoke-virtual {v7, v1, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 156
    :cond_5a
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;->b:Lbr/com/itau/sdk/android/core_ui/token/ChaveiroTokenView;

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 159
    :cond_5f
    invoke-virtual {p0}, Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;->getUiModel()Lbr/com/itau/sdk/android/core/model/UiModelDTO;

    move-result-object v0

    invoke-virtual {v0}, Lbr/com/itau/sdk/android/core/model/UiModelDTO;->getNumeroSerieTokenAplicativo()Ljava/lang/String;

    move-result-object v7

    .line 161
    invoke-virtual {p0, v7}, Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;->isValidToken(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b5

    .line 163
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;->c:Lbr/com/itau/sdk/android/core_ui/token/APPTokenView;

    invoke-virtual {v0, v7}, Lbr/com/itau/sdk/android/core_ui/token/APPTokenView;->setToken(Ljava/lang/String;)V

    .line 164
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;->c:Lbr/com/itau/sdk/android/core_ui/token/APPTokenView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lbr/com/itau/sdk/android/core_ui/token/APPTokenView;->setVisibility(I)V

    .line 165
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;->c:Lbr/com/itau/sdk/android/core_ui/token/APPTokenView;

    invoke-virtual {v0}, Lbr/com/itau/sdk/android/core_ui/token/APPTokenView;->c()V

    .line 167
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;->b:Lbr/com/itau/sdk/android/core_ui/token/ChaveiroTokenView;

    invoke-virtual {v0}, Lbr/com/itau/sdk/android/core_ui/token/ChaveiroTokenView;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_b0

    .line 169
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;->a:Lbr/com/itau/sdk/android/core_ui/token/SMSTokenView;

    invoke-virtual {v0}, Lbr/com/itau/sdk/android/core_ui/token/SMSTokenView;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_a1

    .line 170
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;->b:Lbr/com/itau/sdk/android/core_ui/token/ChaveiroTokenView;

    .line 171
    invoke-virtual {v0}, Lbr/com/itau/sdk/android/core_ui/token/ChaveiroTokenView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Landroid/widget/RelativeLayout$LayoutParams;

    .line 172
    sget v0, Lbr/com/itau/sdk/android/core_ui/R$id;->token_divisor:I

    const/4 v1, 0x3

    invoke-virtual {v8, v1, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 173
    goto :goto_b0

    .line 175
    :cond_a1
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;->b:Lbr/com/itau/sdk/android/core_ui/token/ChaveiroTokenView;

    .line 176
    invoke-virtual {v0}, Lbr/com/itau/sdk/android/core_ui/token/ChaveiroTokenView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Landroid/widget/RelativeLayout$LayoutParams;

    .line 177
    sget v0, Lbr/com/itau/sdk/android/core_ui/R$id;->sms_token_type:I

    const/4 v1, 0x3

    invoke-virtual {v8, v1, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 181
    :cond_b0
    :goto_b0
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;->c:Lbr/com/itau/sdk/android/core_ui/token/APPTokenView;

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 184
    :cond_b5
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_c9

    .line 185
    const/4 v0, 0x0

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;

    iget-object v0, v0, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->t:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->performClick()Z

    goto :goto_f8

    .line 186
    :cond_c9
    sget v0, Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;->i:I

    and-int/lit16 v0, v0, 0x3dc

    sget-object v1, Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;->h:[S

    const/16 v2, 0x1e

    aget-short v1, v1, v2

    neg-int v1, v1

    sget-object v2, Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;->h:[S

    const/16 v3, 0x6f

    aget-short v2, v2, v3

    neg-int v2, v2

    invoke-static {v0, v1, v2}, Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;->a(III)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;->getOpKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f8

    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;->a:Lbr/com/itau/sdk/android/core_ui/token/SMSTokenView;

    invoke-virtual {v0}, Lbr/com/itau/sdk/android/core_ui/token/SMSTokenView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_f8

    .line 187
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;->a:Lbr/com/itau/sdk/android/core_ui/token/SMSTokenView;

    iget-object v0, v0, Lbr/com/itau/sdk/android/core_ui/token/SMSTokenView;->t:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->performClick()Z

    .line 189
    :cond_f8
    :goto_f8
    return-void
.end method

.method private synthetic c(Landroid/content/DialogInterface;I)V
    .registers 9

    .line 237
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;->setIsUnlockTokenScenario(Z)V

    .line 238
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;->e:Landroid/widget/TextView;

    sget v1, Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;->i:I

    and-int/lit16 v1, v1, 0x3ed

    sget-object v2, Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;->h:[S

    const/16 v3, 0x58

    aget-short v2, v2, v3

    neg-int v2, v2

    invoke-static {v1, v2, v2}, Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;->a(III)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 240
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1e
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_33

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;

    .line 241
    invoke-virtual {p0}, Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;->getTokenSelecionado()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->a(Ljava/lang/String;)V

    goto :goto_1e

    .line 244
    :cond_33
    return-void
.end method

.method static synthetic c(Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;Ljava/lang/String;Ljava/lang/String;)V
    .registers 3

    invoke-direct {p0, p1, p2}, Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private synthetic c(Ljava/lang/String;Ljava/lang/String;)V
    .registers 6

    .line 55
    invoke-virtual {p0}, Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;->showLoading()V

    .line 56
    sget-object v0, Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;->h:[S

    const/16 v1, 0x12

    aget-short v0, v0, v1

    neg-int v0, v0

    sget-object v1, Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;->h:[S

    const/16 v2, 0x20

    aget-short v1, v1, v2

    add-int/lit8 v1, v1, -0x1

    invoke-static {v0, v1, v1}, Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;->a(III)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;->setTokenSelecionado(Ljava/lang/String;)V

    .line 57
    invoke-static {p1, p2}, Lbr/com/itau/sdk/android/core/SDKCustomUiController;->validaTokenSms(Ljava/lang/String;Ljava/lang/String;)V

    .line 58
    return-void
.end method

.method private synthetic d()V
    .registers 3

    .line 116
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;->d:Landroid/widget/ImageView;

    sget v1, Lbr/com/itau/sdk/android/core_ui/R$string;->itausdkcore_bt_voltar:I

    invoke-virtual {p0, v1}, Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 117
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;->e:Landroid/widget/TextView;

    sget v1, Lbr/com/itau/sdk/android/core_ui/R$string;->itausdkcore_dialog_token_title:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 118
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;->e:Landroid/widget/TextView;

    sget v1, Lbr/com/itau/sdk/android/core_ui/R$string;->itausdkcore_dialog_token_title:I

    invoke-virtual {p0, v1}, Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 119
    return-void
.end method


# virtual methods
.method public a()V
    .registers 3

    .line 54
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;->a:Lbr/com/itau/sdk/android/core_ui/token/SMSTokenView;

    invoke-static {p0}, Lbr/com/itau/sdk/android/core_ui/token/g;->a(Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;)Lbr/com/itau/sdk/android/core_ui/token/q;

    move-result-object v1

    invoke-virtual {v0, p0, v1}, Lbr/com/itau/sdk/android/core_ui/token/SMSTokenView;->a(Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;Lbr/com/itau/sdk/android/core_ui/token/q;)Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;

    .line 59
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;->b:Lbr/com/itau/sdk/android/core_ui/token/ChaveiroTokenView;

    invoke-static {p0}, Lbr/com/itau/sdk/android/core_ui/token/h;->a(Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;)Lbr/com/itau/sdk/android/core_ui/token/q;

    move-result-object v1

    invoke-virtual {v0, p0, v1}, Lbr/com/itau/sdk/android/core_ui/token/ChaveiroTokenView;->a(Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;Lbr/com/itau/sdk/android/core_ui/token/q;)Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;

    .line 64
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;->c:Lbr/com/itau/sdk/android/core_ui/token/APPTokenView;

    invoke-static {p0}, Lbr/com/itau/sdk/android/core_ui/token/i;->a(Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;)Lbr/com/itau/sdk/android/core_ui/token/q;

    move-result-object v1

    invoke-virtual {v0, p0, v1}, Lbr/com/itau/sdk/android/core_ui/token/APPTokenView;->a(Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;Lbr/com/itau/sdk/android/core_ui/token/q;)Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;

    .line 69
    return-void
.end method

.method public a(Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;)V
    .registers 3

    .line 206
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;->g:Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;

    if-eqz v0, :cond_11

    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;->g:Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_11

    .line 207
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;->g:Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;

    invoke-virtual {v0}, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->d()V

    .line 210
    :cond_11
    iput-object p1, p0, Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;->g:Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;

    .line 211
    return-void
.end method

.method a(Lbr/com/itau/sdk/android/core/type/SecurityAssertResponseDTO;)Z
    .registers 12

    .line 214
    invoke-virtual {p0}, Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;->hideKeyboard()V

    .line 216
    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/type/SecurityAssertResponseDTO;->getMensagem()Ljava/lang/String;

    move-result-object v8

    .line 218
    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1f

    .line 219
    sget-object v0, Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;->h:[S

    const/16 v1, 0x3b

    aget-short v0, v0, v1

    sget-object v1, Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;->h:[S

    const/16 v2, 0x57

    aget-short v1, v1, v2

    const/16 v2, 0x3a

    invoke-static {v0, v1, v2}, Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;->a(III)Ljava/lang/String;

    move-result-object v8

    .line 222
    :cond_1f
    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/type/SecurityAssertResponseDTO;->isSucesso()Z

    move-result v0

    if-nez v0, :cond_2a

    .line 223
    invoke-virtual {p0, v8}, Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;->showError(Ljava/lang/String;)V

    .line 224
    const/4 v0, 0x0

    return v0

    .line 227
    :cond_2a
    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/type/SecurityAssertResponseDTO;->isTokenValidoInformarNovoCodigo()Ljava/lang/Boolean;

    move-result-object v0

    if-eqz v0, :cond_110

    .line 228
    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/type/SecurityAssertResponseDTO;->isTokenValidoInformarNovoCodigo()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_110

    .line 230
    invoke-virtual {p0}, Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lbr/com/itau/sdk/android/core_ui/theme/b;->a(Landroid/content/Context;)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v0

    .line 231
    invoke-static {}, Lbr/com/itau/sdk/android/core_ui/theme/SDKThemeManager;->getInstance()Lbr/com/itau/sdk/android/core_ui/theme/SDKThemeManager;

    move-result-object v1

    invoke-virtual {p0}, Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;->getContext()Landroid/content/Context;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;->h:[S

    const/16 v5, 0xf

    aget-short v4, v4, v5

    neg-int v4, v4

    sget-object v5, Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;->h:[S

    const/16 v6, 0x57

    aget-short v5, v5, v6

    sget-object v6, Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;->h:[S

    const/16 v7, 0x45

    aget-short v6, v6, v7

    neg-int v6, v6

    invoke-static {v4, v5, v6}, Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;->a(III)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 232
    invoke-virtual {p0}, Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;->getTokenSelecionado()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;->h:[S

    const/16 v5, 0x6f

    aget-short v4, v4, v5

    neg-int v4, v4

    sget-object v5, Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;->h:[S

    const/16 v6, 0xb

    aget-short v5, v5, v6

    neg-int v5, v5

    const/16 v6, 0x53

    invoke-static {v4, v6, v5}, Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;->a(III)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 231
    invoke-static {v1, v2, v3}, Lbr/com/itau/sdk/android/core/util/TypefaceUtils;->getBoldText(Lbr/com/itau/sdk/android/core/theme/BaseSDKThemeManager;Landroid/content/Context;Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v0

    .line 233
    invoke-static {}, Lbr/com/itau/sdk/android/core_ui/theme/SDKThemeManager;->getInstance()Lbr/com/itau/sdk/android/core_ui/theme/SDKThemeManager;

    move-result-object v1

    invoke-virtual {p0}, Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;->getContext()Landroid/content/Context;

    move-result-object v2

    sget-object v3, Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;->h:[S

    const/16 v4, 0x20

    aget-short v3, v3, v4

    add-int/lit8 v3, v3, -0x1

    sget-object v4, Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;->h:[S

    const/16 v5, 0x30

    aget-short v4, v4, v5

    neg-int v4, v4

    const/16 v5, 0x44

    invoke-static {v3, v4, v5}, Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;->a(III)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lbr/com/itau/sdk/android/core/util/TypefaceUtils;->getRegularText(Lbr/com/itau/sdk/android/core/theme/BaseSDKThemeManager;Landroid/content/Context;Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v0

    .line 235
    invoke-static {}, Lbr/com/itau/sdk/android/core_ui/theme/SDKThemeManager;->getInstance()Lbr/com/itau/sdk/android/core_ui/theme/SDKThemeManager;

    move-result-object v1

    .line 236
    invoke-virtual {p0}, Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;->getContext()Landroid/content/Context;

    move-result-object v2

    sget-object v3, Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;->h:[S

    const/16 v4, 0xbf

    aget-short v3, v3, v4

    sget-object v4, Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;->h:[S

    const/16 v5, 0x58

    aget-short v4, v4, v5

    neg-int v4, v4

    sget-object v5, Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;->h:[S

    const/4 v6, 0x0

    aget-short v5, v5, v6

    invoke-static {v3, v4, v5}, Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;->a(III)Ljava/lang/String;

    move-result-object v3

    .line 235
    invoke-static {v1, v2, v3}, Lbr/com/itau/sdk/android/core/util/TypefaceUtils;->getBoldText(Lbr/com/itau/sdk/android/core/theme/BaseSDKThemeManager;Landroid/content/Context;Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-static {p0}, Lbr/com/itau/sdk/android/core_ui/token/l;->a(Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;)Landroid/content/DialogInterface$OnClickListener;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/support/v7/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v0

    .line 245
    invoke-static {}, Lbr/com/itau/sdk/android/core_ui/theme/SDKThemeManager;->getInstance()Lbr/com/itau/sdk/android/core_ui/theme/SDKThemeManager;

    move-result-object v1

    .line 246
    invoke-virtual {p0}, Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;->getContext()Landroid/content/Context;

    move-result-object v2

    sget-object v3, Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;->h:[S

    const/16 v4, 0x2e

    aget-short v3, v3, v4

    const/16 v4, 0x79

    const/4 v5, 0x5

    invoke-static {v4, v3, v5}, Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;->a(III)Ljava/lang/String;

    move-result-object v3

    .line 245
    invoke-static {v1, v2, v3}, Lbr/com/itau/sdk/android/core/util/TypefaceUtils;->getBoldText(Lbr/com/itau/sdk/android/core/theme/BaseSDKThemeManager;Landroid/content/Context;Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-static {}, Lbr/com/itau/sdk/android/core_ui/token/m;->a()Landroid/content/DialogInterface$OnClickListener;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/support/v7/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v0

    .line 249
    invoke-virtual {v0}, Landroid/support/v7/app/AlertDialog$Builder;->show()Landroid/support/v7/app/AlertDialog;

    move-result-object v9

    .line 250
    invoke-static {v9}, Lbr/com/itau/sdk/android/core_ui/theme/b;->a(Landroid/support/v7/app/AlertDialog;)V

    .line 252
    const/4 v0, 0x0

    return v0

    .line 255
    :cond_110
    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/type/SecurityAssertResponseDTO;->isTokenValido()Ljava/lang/Boolean;

    move-result-object v0

    if-eqz v0, :cond_125

    .line 256
    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/type/SecurityAssertResponseDTO;->isTokenValido()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_125

    .line 257
    invoke-virtual {p0, v8}, Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;->showError(Ljava/lang/String;)V

    .line 258
    const/4 v0, 0x0

    return v0

    .line 261
    :cond_125
    invoke-virtual {p0}, Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;->getTokenSelecionado()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lbr/com/itau/sdk/android/core/SDKCustomUiController;->setTokenType(Ljava/lang/String;)V

    .line 262
    const/4 v0, 0x1

    return v0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .registers 4

    .line 110
    invoke-super {p0, p1}, Lbr/com/itau/sdk/android/core/token/ui/BaseTokenDialog;->onActivityCreated(Landroid/os/Bundle;)V

    .line 112
    invoke-virtual {p0}, Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;->checkTokenDataIsValid()Z

    move-result v0

    if-eqz v0, :cond_15

    .line 113
    invoke-direct {p0}, Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;->c()V

    .line 115
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;->e:Landroid/widget/TextView;

    invoke-static {p0}, Lbr/com/itau/sdk/android/core_ui/token/k;->a(Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->post(Ljava/lang/Runnable;)Z

    .line 121
    :cond_15
    return-void
.end method

.method public onBackPressed()V
    .registers 5

    .line 192
    invoke-virtual {p0}, Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;->isUnlockTokenScenario()Z

    move-result v0

    if-nez v0, :cond_15

    .line 193
    invoke-static {}, Lbr/com/itau/sdk/android/core/BackendClient;->getInstance()Lbr/com/itau/sdk/android/core/BackendClient;

    move-result-object v0

    sget-object v1, Lbr/com/itau/sdk/android/core/token/TokenEvent$Category;->TOKEN:Lbr/com/itau/sdk/android/core/token/TokenEvent$Category;

    sget-object v2, Lbr/com/itau/sdk/android/core/token/TokenEvent$Action;->BACK:Lbr/com/itau/sdk/android/core/token/TokenEvent$Action;

    invoke-virtual {v0, v1, v2}, Lbr/com/itau/sdk/android/core/BackendClient;->track(Lbr/com/itau/sdk/android/core/token/TokenEvent$Category;Lbr/com/itau/sdk/android/core/token/TokenEvent$Action;)V

    .line 195
    invoke-super {p0}, Lbr/com/itau/sdk/android/core/token/ui/BaseTokenDialog;->onBackPressed()V

    .line 196
    return-void

    .line 199
    :cond_15
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;->setIsUnlockTokenScenario(Z)V

    .line 200
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;->e:Landroid/widget/TextView;

    sget-object v1, Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;->h:[S

    const/16 v2, 0x45

    aget-short v1, v1, v2

    neg-int v1, v1

    sget v2, Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;->i:I

    and-int/lit8 v2, v2, 0xf

    const/16 v3, 0xa6

    invoke-static {v3, v1, v2}, Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;->a(III)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 201
    invoke-direct {p0}, Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;->c()V

    .line 203
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .registers 5

    .line 48
    invoke-static {}, Lbr/com/itau/sdk/android/core_ui/theme/SDKThemeManager;->getInstance()Lbr/com/itau/sdk/android/core_ui/theme/SDKThemeManager;

    move-result-object v1

    .line 49
    invoke-virtual {v1}, Lbr/com/itau/sdk/android/core/theme/BaseSDKThemeManager;->getDialogTheme()I

    move-result v0

    invoke-super {p0, v0}, Lbr/com/itau/sdk/android/core/token/ui/BaseTokenDialog;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v2

    .line 50
    invoke-virtual {v1, v2}, Lbr/com/itau/sdk/android/core/theme/BaseSDKThemeManager;->checkDialogTheme(Landroid/app/Dialog;)Landroid/app/Dialog;

    move-result-object v0

    return-object v0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .registers 8

    .line 88
    sget v0, Lbr/com/itau/sdk/android/core_ui/R$layout;->itausdkcore_dialog_token:I

    invoke-virtual {p1, v0, p2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 90
    sget v0, Lbr/com/itau/sdk/android/core_ui/R$id;->sms_token_type:I

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lbr/com/itau/sdk/android/core_ui/token/SMSTokenView;

    iput-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;->a:Lbr/com/itau/sdk/android/core_ui/token/SMSTokenView;

    .line 91
    sget v0, Lbr/com/itau/sdk/android/core_ui/R$id;->chaveiro_token_type:I

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lbr/com/itau/sdk/android/core_ui/token/ChaveiroTokenView;

    iput-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;->b:Lbr/com/itau/sdk/android/core_ui/token/ChaveiroTokenView;

    .line 92
    sget v0, Lbr/com/itau/sdk/android/core_ui/R$id;->aplicativo_token_type:I

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lbr/com/itau/sdk/android/core_ui/token/APPTokenView;

    iput-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;->c:Lbr/com/itau/sdk/android/core_ui/token/APPTokenView;

    .line 93
    sget v0, Lbr/com/itau/sdk/android/core_ui/R$id;->token_title:I

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;->e:Landroid/widget/TextView;

    .line 94
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;->e:Landroid/widget/TextView;

    invoke-static {}, Lbr/com/itau/sdk/android/core_ui/theme/SDKThemeManager;->getInstance()Lbr/com/itau/sdk/android/core_ui/theme/SDKThemeManager;

    move-result-object v1

    invoke-virtual {p0}, Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v2

    invoke-static {v1, v2}, Lbr/com/itau/sdk/android/core/util/TypefaceUtils;->getFontBold(Lbr/com/itau/sdk/android/core/theme/BaseSDKThemeManager;Landroid/content/res/AssetManager;)Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 95
    sget v0, Lbr/com/itau/sdk/android/core_ui/R$id;->global_loading:I

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;->loading:Landroid/view/View;

    .line 97
    sget v0, Lbr/com/itau/sdk/android/core_ui/R$id;->token_back:I

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;->d:Landroid/widget/ImageView;

    .line 98
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;->d:Landroid/widget/ImageView;

    invoke-static {p0}, Lbr/com/itau/sdk/android/core_ui/token/j;->a(Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 100
    invoke-virtual {p0}, Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;->a()V

    .line 101
    invoke-direct {p0}, Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;->b()V

    .line 103
    const/4 v0, 0x3

    new-array v0, v0, [Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;

    iget-object v1, p0, Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;->c:Lbr/com/itau/sdk/android/core_ui/token/APPTokenView;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    iget-object v1, p0, Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;->b:Lbr/com/itau/sdk/android/core_ui/token/ChaveiroTokenView;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    iget-object v1, p0, Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;->a:Lbr/com/itau/sdk/android/core_ui/token/SMSTokenView;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;->f:Ljava/util/List;

    .line 105
    return-object v3
.end method

.method public onPause()V
    .registers 1

    .line 73
    invoke-static {}, Lbr/com/itau/sdk/android/core/BackendClient;->unregisteViewsInBus()V

    .line 74
    invoke-super {p0}, Lbr/com/itau/sdk/android/core/token/ui/BaseTokenDialog;->onPause()V

    .line 75
    return-void
.end method

.method public onResume()V
    .registers 2

    .line 79
    invoke-super {p0}, Lbr/com/itau/sdk/android/core/token/ui/BaseTokenDialog;->onResume()V

    .line 80
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;->a:Lbr/com/itau/sdk/android/core_ui/token/SMSTokenView;

    invoke-static {v0}, Lbr/com/itau/sdk/android/core/BackendClient;->registerViewInBus(Landroid/view/View;)V

    .line 81
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;->b:Lbr/com/itau/sdk/android/core_ui/token/ChaveiroTokenView;

    invoke-static {v0}, Lbr/com/itau/sdk/android/core/BackendClient;->registerViewInBus(Landroid/view/View;)V

    .line 82
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;->c:Lbr/com/itau/sdk/android/core_ui/token/APPTokenView;

    invoke-static {v0}, Lbr/com/itau/sdk/android/core/BackendClient;->registerViewInBus(Landroid/view/View;)V

    .line 83
    return-void
.end method

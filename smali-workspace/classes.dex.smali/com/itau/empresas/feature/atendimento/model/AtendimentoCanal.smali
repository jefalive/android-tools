.class public Lcom/itau/empresas/feature/atendimento/model/AtendimentoCanal;
.super Ljava/lang/Object;
.source "AtendimentoCanal.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field canal:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "canal"
    .end annotation
.end field

.field icone:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "icone"
    .end annotation
.end field

.field textoExplicativo:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "texto_explicativo"
    .end annotation
.end field

.field url:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "url"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getCanal()Ljava/lang/String;
    .registers 2

    .line 50
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/model/AtendimentoCanal;->canal:Ljava/lang/String;

    return-object v0
.end method

.method public getTextoExplicativo()Ljava/lang/String;
    .registers 2

    .line 54
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/model/AtendimentoCanal;->textoExplicativo:Ljava/lang/String;

    return-object v0
.end method

.method public setCanal(Ljava/lang/String;)V
    .registers 2
    .param p1, "canal"    # Ljava/lang/String;

    .line 26
    iput-object p1, p0, Lcom/itau/empresas/feature/atendimento/model/AtendimentoCanal;->canal:Ljava/lang/String;

    .line 27
    return-void
.end method

.method public setIcone(Ljava/lang/String;)V
    .registers 2
    .param p1, "icone"    # Ljava/lang/String;

    .line 38
    iput-object p1, p0, Lcom/itau/empresas/feature/atendimento/model/AtendimentoCanal;->icone:Ljava/lang/String;

    .line 39
    return-void
.end method

.method public setTextoExplicativo(Ljava/lang/String;)V
    .registers 2
    .param p1, "textoExplicativo"    # Ljava/lang/String;

    .line 30
    iput-object p1, p0, Lcom/itau/empresas/feature/atendimento/model/AtendimentoCanal;->textoExplicativo:Ljava/lang/String;

    .line 31
    return-void
.end method

.method public setUrl(Ljava/lang/String;)V
    .registers 2
    .param p1, "url"    # Ljava/lang/String;

    .line 34
    iput-object p1, p0, Lcom/itau/empresas/feature/atendimento/model/AtendimentoCanal;->url:Ljava/lang/String;

    .line 35
    return-void
.end method

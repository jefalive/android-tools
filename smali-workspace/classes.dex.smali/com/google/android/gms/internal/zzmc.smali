.class public final Lcom/google/android/gms/internal/zzmc;
.super Landroid/widget/ImageView;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/internal/zzmc$zza;
    }
.end annotation


# instance fields
.field private zzakt:I

.field private zzaku:Lcom/google/android/gms/internal/zzmc$zza;

.field private zzakv:I

.field private zzakw:F


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .registers 5
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    iget-object v0, p0, Lcom/google/android/gms/internal/zzmc;->zzaku:Lcom/google/android/gms/internal/zzmc$zza;

    if-eqz v0, :cond_15

    iget-object v0, p0, Lcom/google/android/gms/internal/zzmc;->zzaku:Lcom/google/android/gms/internal/zzmc$zza;

    invoke-virtual {p0}, Lcom/google/android/gms/internal/zzmc;->getWidth()I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/gms/internal/zzmc;->getHeight()I

    move-result v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/gms/internal/zzmc$zza;->zzl(II)Landroid/graphics/Path;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->clipPath(Landroid/graphics/Path;)Z

    :cond_15
    invoke-super {p0, p1}, Landroid/widget/ImageView;->onDraw(Landroid/graphics/Canvas;)V

    iget v0, p0, Lcom/google/android/gms/internal/zzmc;->zzakt:I

    if-eqz v0, :cond_21

    iget v0, p0, Lcom/google/android/gms/internal/zzmc;->zzakt:I

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->drawColor(I)V

    :cond_21
    return-void
.end method

.method protected onMeasure(II)V
    .registers 7
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    invoke-super {p0, p1, p2}, Landroid/widget/ImageView;->onMeasure(II)V

    iget v0, p0, Lcom/google/android/gms/internal/zzmc;->zzakv:I

    packed-switch v0, :pswitch_data_22

    goto :goto_1d

    :pswitch_9
    invoke-virtual {p0}, Lcom/google/android/gms/internal/zzmc;->getMeasuredHeight()I

    move-result v3

    int-to-float v0, v3

    iget v1, p0, Lcom/google/android/gms/internal/zzmc;->zzakw:F

    mul-float/2addr v0, v1

    float-to-int v2, v0

    goto :goto_1e

    :pswitch_13
    invoke-virtual {p0}, Lcom/google/android/gms/internal/zzmc;->getMeasuredWidth()I

    move-result v2

    int-to-float v0, v2

    iget v1, p0, Lcom/google/android/gms/internal/zzmc;->zzakw:F

    div-float/2addr v0, v1

    float-to-int v3, v0

    goto :goto_1e

    :goto_1d
    :pswitch_1d
    return-void

    :goto_1e
    invoke-virtual {p0, v2, v3}, Lcom/google/android/gms/internal/zzmc;->setMeasuredDimension(II)V

    return-void

    :pswitch_data_22
    .packed-switch 0x0
        :pswitch_1d
        :pswitch_9
        :pswitch_13
    .end packed-switch
.end method

.class Lcom/google/android/gms/internal/zzgw$5;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/internal/zziw$zza;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/gms/internal/zzgw;->zza(Lorg/json/JSONObject;ZZ)Lcom/google/android/gms/internal/zzjg;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;Lcom/google/android/gms/internal/zziw$zza<Lcom/google/android/gms/ads/internal/formats/zzc;>;"
    }
.end annotation


# instance fields
.field final synthetic zzDr:Ljava/lang/String;

.field final synthetic zzGP:Lcom/google/android/gms/internal/zzgw;

.field final synthetic zzGZ:Z

.field final synthetic zzHa:D


# direct methods
.method constructor <init>(Lcom/google/android/gms/internal/zzgw;ZDLjava/lang/String;)V
    .registers 6

    iput-object p1, p0, Lcom/google/android/gms/internal/zzgw$5;->zzGP:Lcom/google/android/gms/internal/zzgw;

    iput-boolean p2, p0, Lcom/google/android/gms/internal/zzgw$5;->zzGZ:Z

    iput-wide p3, p0, Lcom/google/android/gms/internal/zzgw$5;->zzHa:D

    iput-object p5, p0, Lcom/google/android/gms/internal/zzgw$5;->zzDr:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public zzg(Ljava/io/InputStream;)Lcom/google/android/gms/ads/internal/formats/zzc;
    .registers 9

    const/4 v5, 0x0

    :try_start_1
    invoke-static {p1}, Lcom/google/android/gms/internal/zzna;->zzk(Ljava/io/InputStream;)[B
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_4} :catch_7

    move-result-object v0

    move-object v5, v0

    goto :goto_8

    :catch_7
    move-exception v6

    :goto_8
    if-nez v5, :cond_14

    iget-object v0, p0, Lcom/google/android/gms/internal/zzgw$5;->zzGP:Lcom/google/android/gms/internal/zzgw;

    iget-boolean v1, p0, Lcom/google/android/gms/internal/zzgw$5;->zzGZ:Z

    const/4 v2, 0x2

    invoke-virtual {v0, v2, v1}, Lcom/google/android/gms/internal/zzgw;->zza(IZ)V

    const/4 v0, 0x0

    return-object v0

    :cond_14
    array-length v0, v5

    const/4 v1, 0x0

    invoke-static {v5, v1, v0}, Landroid/graphics/BitmapFactory;->decodeByteArray([BII)Landroid/graphics/Bitmap;

    move-result-object v6

    if-nez v6, :cond_26

    iget-object v0, p0, Lcom/google/android/gms/internal/zzgw$5;->zzGP:Lcom/google/android/gms/internal/zzgw;

    iget-boolean v1, p0, Lcom/google/android/gms/internal/zzgw$5;->zzGZ:Z

    const/4 v2, 0x2

    invoke-virtual {v0, v2, v1}, Lcom/google/android/gms/internal/zzgw;->zza(IZ)V

    const/4 v0, 0x0

    return-object v0

    :cond_26
    iget-wide v0, p0, Lcom/google/android/gms/internal/zzgw$5;->zzHa:D

    const-wide/high16 v2, 0x4064000000000000L    # 160.0

    mul-double/2addr v0, v2

    double-to-int v0, v0

    invoke-virtual {v6, v0}, Landroid/graphics/Bitmap;->setDensity(I)V

    new-instance v0, Lcom/google/android/gms/ads/internal/formats/zzc;

    new-instance v1, Landroid/graphics/drawable/BitmapDrawable;

    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object v2

    invoke-direct {v1, v2, v6}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    iget-object v2, p0, Lcom/google/android/gms/internal/zzgw$5;->zzDr:Ljava/lang/String;

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    iget-wide v3, p0, Lcom/google/android/gms/internal/zzgw$5;->zzHa:D

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/gms/ads/internal/formats/zzc;-><init>(Landroid/graphics/drawable/Drawable;Landroid/net/Uri;D)V

    return-object v0
.end method

.method public zzgo()Lcom/google/android/gms/ads/internal/formats/zzc;
    .registers 4

    iget-object v0, p0, Lcom/google/android/gms/internal/zzgw$5;->zzGP:Lcom/google/android/gms/internal/zzgw;

    iget-boolean v1, p0, Lcom/google/android/gms/internal/zzgw$5;->zzGZ:Z

    const/4 v2, 0x2

    invoke-virtual {v0, v2, v1}, Lcom/google/android/gms/internal/zzgw;->zza(IZ)V

    const/4 v0, 0x0

    return-object v0
.end method

.method public synthetic zzgp()Ljava/lang/Object;
    .registers 2

    invoke-virtual {p0}, Lcom/google/android/gms/internal/zzgw$5;->zzgo()Lcom/google/android/gms/ads/internal/formats/zzc;

    move-result-object v0

    return-object v0
.end method

.method public synthetic zzh(Ljava/io/InputStream;)Ljava/lang/Object;
    .registers 3

    invoke-virtual {p0, p1}, Lcom/google/android/gms/internal/zzgw$5;->zzg(Ljava/io/InputStream;)Lcom/google/android/gms/ads/internal/formats/zzc;

    move-result-object v0

    return-object v0
.end method

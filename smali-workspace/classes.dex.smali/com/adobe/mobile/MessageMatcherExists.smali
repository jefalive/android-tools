.class Lcom/adobe/mobile/MessageMatcherExists;
.super Lcom/adobe/mobile/MessageMatcher;
.source "MessageMatcherExists.java"


# direct methods
.method constructor <init>()V
    .registers 1

    .line 5
    invoke-direct {p0}, Lcom/adobe/mobile/MessageMatcher;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs matchesInMaps([Ljava/util/Map;)Z
    .registers 8
    .param p1, "maps"    # [Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;)Z"
        }
    .end annotation

    .line 8
    const/4 v1, 0x0

    .line 10
    .local v1, "value":Ljava/lang/Object;
    if-eqz p1, :cond_6

    array-length v0, p1

    if-gtz v0, :cond_8

    .line 11
    :cond_6
    const/4 v0, 0x0

    return v0

    .line 14
    :cond_8
    move-object v2, p1

    array-length v3, v2

    const/4 v4, 0x0

    :goto_b
    if-ge v4, v3, :cond_24

    aget-object v5, v2, v4

    .line 15
    .local v5, "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    if-nez v5, :cond_12

    .line 16
    goto :goto_21

    .line 19
    :cond_12
    iget-object v0, p0, Lcom/adobe/mobile/MessageMatcherExists;->key:Ljava/lang/String;

    invoke-interface {v5, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_21

    .line 20
    iget-object v0, p0, Lcom/adobe/mobile/MessageMatcherExists;->key:Ljava/lang/String;

    invoke-interface {v5, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 21
    goto :goto_24

    .line 14
    .end local v5    # "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    .end local v5
    :cond_21
    :goto_21
    add-int/lit8 v4, v4, 0x1

    goto :goto_b

    .line 25
    :cond_24
    :goto_24
    if-eqz v1, :cond_28

    const/4 v0, 0x1

    goto :goto_29

    :cond_28
    const/4 v0, 0x0

    :goto_29
    return v0
.end method

.class public Lbr/com/itau/textovoz/model/Receipt;
.super Ljava/lang/Object;
.source "Receipt.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<Lbr/com/itau/textovoz/model/Receipt;>;"
        }
    .end annotation
.end field


# instance fields
.field private transactionDate:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "dataMov"
    .end annotation
.end field

.field private transactionDescription:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "descricao"
    .end annotation
.end field

.field private transactionHistory:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "histor"
    .end annotation
.end field

.field private transactionValue:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "valor"
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .line 46
    new-instance v0, Lbr/com/itau/textovoz/model/Receipt$1;

    invoke-direct {v0}, Lbr/com/itau/textovoz/model/Receipt$1;-><init>()V

    sput-object v0, Lbr/com/itau/textovoz/model/Receipt;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .registers 3
    .param p1, "in"    # Landroid/os/Parcel;

    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/textovoz/model/Receipt;->transactionDate:Ljava/lang/String;

    .line 41
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/textovoz/model/Receipt;->transactionDescription:Ljava/lang/String;

    .line 42
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/textovoz/model/Receipt;->transactionHistory:Ljava/lang/String;

    .line 43
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/textovoz/model/Receipt;->transactionValue:Ljava/lang/String;

    .line 44
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lbr/com/itau/textovoz/model/Receipt$1;)V
    .registers 3
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lbr/com/itau/textovoz/model/Receipt$1;

    .line 12
    invoke-direct {p0, p1}, Lbr/com/itau/textovoz/model/Receipt;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .registers 2

    .line 28
    const/4 v0, 0x0

    return v0
.end method

.method public getTransactionDate()Ljava/lang/String;
    .registers 2

    .line 61
    iget-object v0, p0, Lbr/com/itau/textovoz/model/Receipt;->transactionDate:Ljava/lang/String;

    return-object v0
.end method

.method public getTransactionDescription()Ljava/lang/String;
    .registers 2

    .line 69
    iget-object v0, p0, Lbr/com/itau/textovoz/model/Receipt;->transactionDescription:Ljava/lang/String;

    return-object v0
.end method

.method public getTransactionHistory()Ljava/lang/String;
    .registers 2

    .line 77
    iget-object v0, p0, Lbr/com/itau/textovoz/model/Receipt;->transactionHistory:Ljava/lang/String;

    return-object v0
.end method

.method public getTransactionValue()Ljava/lang/String;
    .registers 2

    .line 85
    iget-object v0, p0, Lbr/com/itau/textovoz/model/Receipt;->transactionValue:Ljava/lang/String;

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 4
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .line 33
    iget-object v0, p0, Lbr/com/itau/textovoz/model/Receipt;->transactionDate:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 34
    iget-object v0, p0, Lbr/com/itau/textovoz/model/Receipt;->transactionDescription:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 35
    iget-object v0, p0, Lbr/com/itau/textovoz/model/Receipt;->transactionHistory:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 36
    iget-object v0, p0, Lbr/com/itau/textovoz/model/Receipt;->transactionValue:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 37
    return-void
.end method

.class final Lcom/google/android/gms/internal/zzam$zzb;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/internal/zzam;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "zzb"
.end annotation


# instance fields
.field private zzoq:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/internal/zzam$zzb;->zzoq:Landroid/content/Context;

    iget-object v0, p0, Lcom/google/android/gms/internal/zzam$zzb;->zzoq:Landroid/content/Context;

    if-nez v0, :cond_f

    iput-object p1, p0, Lcom/google/android/gms/internal/zzam$zzb;->zzoq:Landroid/content/Context;

    :cond_f
    return-void
.end method


# virtual methods
.method public run()V
    .registers 6

    const-class v1, Lcom/google/android/gms/internal/zzam;

    monitor-enter v1

    :try_start_3
    invoke-static {}, Lcom/google/android/gms/internal/zzam;->zzZ()Lcom/google/android/gms/ads/identifier/AdvertisingIdClient;

    move-result-object v0

    if-nez v0, :cond_1a

    const/4 v0, 0x1

    invoke-static {v0}, Lcom/google/android/gms/ads/identifier/AdvertisingIdClient;->setShouldSkipGmsCoreVersionCheck(Z)V

    new-instance v2, Lcom/google/android/gms/ads/identifier/AdvertisingIdClient;

    iget-object v0, p0, Lcom/google/android/gms/internal/zzam$zzb;->zzoq:Landroid/content/Context;

    invoke-direct {v2, v0}, Lcom/google/android/gms/ads/identifier/AdvertisingIdClient;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2}, Lcom/google/android/gms/ads/identifier/AdvertisingIdClient;->start()V

    invoke-static {v2}, Lcom/google/android/gms/internal/zzam;->zza(Lcom/google/android/gms/ads/identifier/AdvertisingIdClient;)Lcom/google/android/gms/ads/identifier/AdvertisingIdClient;
    :try_end_1a
    .catch Lcom/google/android/gms/common/GooglePlayServicesNotAvailableException; {:try_start_3 .. :try_end_1a} :catch_22
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_1a} :catch_22
    .catch Lcom/google/android/gms/common/GooglePlayServicesRepairableException; {:try_start_3 .. :try_end_1a} :catch_22
    .catchall {:try_start_3 .. :try_end_1a} :catchall_2f

    :cond_1a
    :try_start_1a
    invoke-static {}, Lcom/google/android/gms/internal/zzam;->zzaa()Ljava/util/concurrent/CountDownLatch;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V
    :try_end_21
    .catchall {:try_start_1a .. :try_end_21} :catchall_3a

    goto :goto_38

    :catch_22
    move-exception v2

    const/4 v0, 0x0

    :try_start_24
    invoke-static {v0}, Lcom/google/android/gms/internal/zzam;->zza(Lcom/google/android/gms/ads/identifier/AdvertisingIdClient;)Lcom/google/android/gms/ads/identifier/AdvertisingIdClient;
    :try_end_27
    .catchall {:try_start_24 .. :try_end_27} :catchall_2f

    :try_start_27
    invoke-static {}, Lcom/google/android/gms/internal/zzam;->zzaa()Ljava/util/concurrent/CountDownLatch;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    goto :goto_38

    :catchall_2f
    move-exception v3

    invoke-static {}, Lcom/google/android/gms/internal/zzam;->zzaa()Ljava/util/concurrent/CountDownLatch;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    throw v3
    :try_end_38
    .catchall {:try_start_27 .. :try_end_38} :catchall_3a

    :goto_38
    monitor-exit v1

    goto :goto_3d

    :catchall_3a
    move-exception v4

    monitor-exit v1

    throw v4

    :goto_3d
    return-void
.end method

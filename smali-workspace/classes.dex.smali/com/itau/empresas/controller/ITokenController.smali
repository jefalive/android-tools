.class public Lcom/itau/empresas/controller/ITokenController;
.super Lcom/itau/empresas/controller/BaseController;
.source "ITokenController.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/itau/empresas/controller/BaseController<Lcom/itau/empresas/api/Api;>;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 12
    invoke-direct {p0}, Lcom/itau/empresas/controller/BaseController;-><init>()V

    return-void
.end method


# virtual methods
.method public cancelarTokenApp()V
    .registers 2

    .line 56
    new-instance v0, Lcom/itau/empresas/controller/ITokenController$3;

    invoke-direct {v0, p0}, Lcom/itau/empresas/controller/ITokenController$3;-><init>(Lcom/itau/empresas/controller/ITokenController;)V

    invoke-static {v0}, Lbr/com/itau/sdk/android/core/BackendClient;->callApi(Lbr/com/itau/sdk/android/core/type/BackendCall;)V

    .line 62
    return-void
.end method

.method public consultarTokenApp()V
    .registers 2

    .line 37
    new-instance v0, Lcom/itau/empresas/controller/ITokenController$1;

    invoke-direct {v0, p0}, Lcom/itau/empresas/controller/ITokenController$1;-><init>(Lcom/itau/empresas/controller/ITokenController;)V

    invoke-static {v0}, Lbr/com/itau/sdk/android/core/BackendClient;->callApi(Lbr/com/itau/sdk/android/core/type/BackendCall;)V

    .line 44
    return-void
.end method

.method public instalarTokenApp()V
    .registers 2

    .line 47
    new-instance v0, Lcom/itau/empresas/controller/ITokenController$2;

    invoke-direct {v0, p0}, Lcom/itau/empresas/controller/ITokenController$2;-><init>(Lcom/itau/empresas/controller/ITokenController;)V

    invoke-static {v0}, Lbr/com/itau/sdk/android/core/BackendClient;->callApi(Lbr/com/itau/sdk/android/core/type/BackendCall;)V

    .line 53
    return-void
.end method

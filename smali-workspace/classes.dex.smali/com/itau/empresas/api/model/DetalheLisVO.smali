.class public Lcom/itau/empresas/api/model/DetalheLisVO;
.super Ljava/lang/Object;
.source "DetalheLisVO.java"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/itau/empresas/api/model/DetalheLisVO$Tipo;
    }
.end annotation


# instance fields
.field private tipo:Lcom/itau/empresas/api/model/DetalheLisVO$Tipo;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "tipo"
    .end annotation
.end field

.field private titulo:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "titulo"
    .end annotation
.end field

.field private valor:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "valor"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getTipo()Lcom/itau/empresas/api/model/DetalheLisVO$Tipo;
    .registers 2

    .line 33
    iget-object v0, p0, Lcom/itau/empresas/api/model/DetalheLisVO;->tipo:Lcom/itau/empresas/api/model/DetalheLisVO$Tipo;

    return-object v0
.end method

.method public getTitulo()Ljava/lang/String;
    .registers 2

    .line 17
    iget-object v0, p0, Lcom/itau/empresas/api/model/DetalheLisVO;->titulo:Ljava/lang/String;

    return-object v0
.end method

.method public getValor()Ljava/lang/String;
    .registers 2

    .line 25
    iget-object v0, p0, Lcom/itau/empresas/api/model/DetalheLisVO;->valor:Ljava/lang/String;

    return-object v0
.end method

.method public setTipo(Lcom/itau/empresas/api/model/DetalheLisVO$Tipo;)V
    .registers 2
    .param p1, "tipo"    # Lcom/itau/empresas/api/model/DetalheLisVO$Tipo;

    .line 37
    iput-object p1, p0, Lcom/itau/empresas/api/model/DetalheLisVO;->tipo:Lcom/itau/empresas/api/model/DetalheLisVO$Tipo;

    .line 38
    return-void
.end method

.method public setTitulo(Ljava/lang/String;)V
    .registers 2
    .param p1, "titulo"    # Ljava/lang/String;

    .line 21
    iput-object p1, p0, Lcom/itau/empresas/api/model/DetalheLisVO;->titulo:Ljava/lang/String;

    .line 22
    return-void
.end method

.method public setValor(Ljava/lang/String;)V
    .registers 2
    .param p1, "valor"    # Ljava/lang/String;

    .line 29
    iput-object p1, p0, Lcom/itau/empresas/api/model/DetalheLisVO;->valor:Ljava/lang/String;

    .line 30
    return-void
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    .line 42
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "DetalhesLisProdutoVO{titulo=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/api/model/DetalheLisVO;->titulo:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", valor=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/api/model/DetalheLisVO;->valor:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", tipo="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/api/model/DetalheLisVO;->tipo:Lcom/itau/empresas/api/model/DetalheLisVO$Tipo;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

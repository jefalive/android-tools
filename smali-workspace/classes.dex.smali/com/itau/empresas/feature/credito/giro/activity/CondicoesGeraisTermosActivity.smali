.class public Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisTermosActivity;
.super Lcom/itau/empresas/ui/activity/BaseActivity;
.source "CondicoesGeraisTermosActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisTermosActivity$TermosWebViewClient;
    }
.end annotation


# instance fields
.field condicoesGeraisTermosWebview:Landroid/webkit/WebView;

.field termo:Ljava/lang/String;

.field toolBar:Landroid/support/v7/widget/Toolbar;


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 24
    invoke-direct {p0}, Lcom/itau/empresas/ui/activity/BaseActivity;-><init>()V

    return-void
.end method

.method private carregaUrl()Ljava/lang/String;
    .registers 3

    .line 54
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisTermosActivity;->termo:Ljava/lang/String;

    const-string v1, "giro"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_16

    .line 55
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisTermosActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0701a6

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 59
    :cond_16
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisTermosActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0701a7

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private constroiToolbar()V
    .registers 3

    .line 41
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisTermosActivity;->toolBar:Landroid/support/v7/widget/Toolbar;

    invoke-static {v0, p0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom;->Builder(Landroid/support/v7/widget/Toolbar;Landroid/support/v7/app/AppCompatActivity;)Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    move-result-object v0

    .line 42
    const v1, 0x7f070697

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->comTitulo(I)Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    move-result-object v0

    .line 43
    invoke-virtual {v0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->comClickListenerFinish()Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    move-result-object v0

    .line 44
    invoke-virtual {v0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->build()Lcom/itau/empresas/ui/util/listener/ToolbarCustom;

    .line 45
    return-void
.end method

.method public static intent(Landroid/content/Context;)Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisTermosActivity_$IntentBuilder_;
    .registers 2
    .param p0, "context"    # Landroid/content/Context;

    .line 87
    new-instance v0, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisTermosActivity_$IntentBuilder_;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisTermosActivity_$IntentBuilder_;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method private montaUrlWebview()Ljava/lang/String;
    .registers 3

    .line 49
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisTermosActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0704ac

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 50
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisTermosActivity;->carregaUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 49
    return-object v0
.end method


# virtual methods
.method aoCarregarElementosDeTela()V
    .registers 3

    .line 34
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisTermosActivity;->constroiToolbar()V

    .line 36
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisTermosActivity;->condicoesGeraisTermosWebview:Landroid/webkit/WebView;

    new-instance v1, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisTermosActivity$TermosWebViewClient;

    invoke-direct {v1}, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisTermosActivity$TermosWebViewClient;-><init>()V

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 37
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisTermosActivity;->condicoesGeraisTermosWebview:Landroid/webkit/WebView;

    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisTermosActivity;->montaUrlWebview()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 38
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .registers 4
    .param p1, "menu"    # Landroid/view/Menu;

    .line 69
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisTermosActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f0f0002

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 70
    const/4 v0, 0x1

    return v0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .registers 4
    .param p1, "item"    # Landroid/view/MenuItem;

    .line 74
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x7f0e06cc

    if-ne v0, v1, :cond_12

    .line 75
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisTermosActivity;->carregaUrl()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/itau/empresas/ui/util/CompartilhamentoUtils;->compartilharPDF(Landroid/app/Activity;Ljava/lang/String;)V

    .line 76
    const/4 v0, 0x1

    return v0

    .line 79
    :cond_12
    invoke-super {p0, p1}, Lcom/itau/empresas/ui/activity/BaseActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0
.end method

.method public opkeysConsumidas()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Ljava/lang/String;>;"
        }
    .end annotation

    .line 65
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

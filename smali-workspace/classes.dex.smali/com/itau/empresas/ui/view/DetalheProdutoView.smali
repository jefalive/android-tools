.class public Lcom/itau/empresas/ui/view/DetalheProdutoView;
.super Landroid/widget/LinearLayout;
.source "DetalheProdutoView.java"


# instance fields
.field public textoTipo:Landroid/widget/TextView;

.field public textoValor:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 2
    .param p1, "context"    # Landroid/content/Context;

    .line 26
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 27
    return-void
.end method

.method private setValor(Ljava/lang/String;)V
    .registers 3
    .param p1, "valor"    # Ljava/lang/String;

    .line 58
    if-eqz p1, :cond_7

    .line 59
    iget-object v0, p0, Lcom/itau/empresas/ui/view/DetalheProdutoView;->textoValor:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 61
    :cond_7
    return-void
.end method


# virtual methods
.method public bind(Lcom/itau/empresas/api/model/DetalheLisVO;)V
    .registers 5
    .param p1, "atributoVO"    # Lcom/itau/empresas/api/model/DetalheLisVO;

    .line 36
    iget-object v0, p0, Lcom/itau/empresas/ui/view/DetalheProdutoView;->textoTipo:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/itau/empresas/api/model/DetalheLisVO;->getTitulo()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 39
    :try_start_9
    sget-object v0, Lcom/itau/empresas/ui/view/DetalheProdutoView$1;->$SwitchMap$com$itau$empresas$api$model$DetalheLisVO$Tipo:[I

    invoke-virtual {p1}, Lcom/itau/empresas/api/model/DetalheLisVO;->getTipo()Lcom/itau/empresas/api/model/DetalheLisVO$Tipo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/itau/empresas/api/model/DetalheLisVO$Tipo;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_44

    goto :goto_39

    .line 41
    .line 42
    :pswitch_19
    invoke-virtual {p1}, Lcom/itau/empresas/api/model/DetalheLisVO;->getValor()Ljava/lang/String;

    move-result-object v0

    .line 41
    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/itau/empresas/ui/util/ValorMonetarioUtils;->formataValorDecimalParaMonetario(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/itau/empresas/ui/view/DetalheProdutoView;->setValor(Ljava/lang/String;)V

    .line 43
    goto :goto_39

    .line 45
    :pswitch_26
    invoke-virtual {p1}, Lcom/itau/empresas/api/model/DetalheLisVO;->getValor()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/itau/empresas/ui/util/DataEHoraUtils;->formataDataComBarra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/itau/empresas/ui/view/DetalheProdutoView;->setValor(Ljava/lang/String;)V

    .line 46
    goto :goto_39

    .line 48
    :pswitch_32
    invoke-virtual {p1}, Lcom/itau/empresas/api/model/DetalheLisVO;->getValor()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/itau/empresas/ui/view/DetalheProdutoView;->setValor(Ljava/lang/String;)V
    :try_end_39
    .catch Ljava/lang/IllegalArgumentException; {:try_start_9 .. :try_end_39} :catch_3a

    .line 54
    :goto_39
    goto :goto_42

    .line 51
    :catch_3a
    move-exception v2

    .line 53
    .local v2, "ex":Ljava/lang/IllegalArgumentException;
    invoke-virtual {p1}, Lcom/itau/empresas/api/model/DetalheLisVO;->getValor()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/itau/empresas/ui/view/DetalheProdutoView;->setValor(Ljava/lang/String;)V

    .line 55
    .end local v2    # "ex":Ljava/lang/IllegalArgumentException;
    :goto_42
    return-void

    nop

    :pswitch_data_44
    .packed-switch 0x1
        :pswitch_19
        :pswitch_26
        :pswitch_32
    .end packed-switch
.end method

.method public bind(Lcom/itau/empresas/feature/credito/produtos/model/DetalhesMultilimiteAtributoVO;)V
    .registers 4
    .param p1, "atributoVO"    # Lcom/itau/empresas/feature/credito/produtos/model/DetalhesMultilimiteAtributoVO;

    .line 30
    iget-object v0, p0, Lcom/itau/empresas/ui/view/DetalheProdutoView;->textoTipo:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/itau/empresas/feature/credito/produtos/model/DetalhesMultilimiteAtributoVO;->getLiteral()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 32
    invoke-virtual {p1}, Lcom/itau/empresas/feature/credito/produtos/model/DetalhesMultilimiteAtributoVO;->getValor()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/itau/empresas/ui/view/DetalheProdutoView;->setValor(Ljava/lang/String;)V

    .line 33
    return-void
.end method

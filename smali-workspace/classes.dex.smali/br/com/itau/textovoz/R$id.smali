.class public final Lbr/com/itau/textovoz/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lbr/com/itau/textovoz/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final Accelerate:I = 0x7f0e0066

.field public static final AccelerateDecelerate:I = 0x7f0e0067

.field public static final Decelerate:I = 0x7f0e0068

.field public static final Linear:I = 0x7f0e0069

.field public static final action0:I = 0x7f0e0594

.field public static final action_bar:I = 0x7f0e00a5

.field public static final action_bar_activity_content:I = 0x7f0e0000

.field public static final action_bar_container:I = 0x7f0e00a4

.field public static final action_bar_root:I = 0x7f0e00a0

.field public static final action_bar_spinner:I = 0x7f0e0001

.field public static final action_bar_subtitle:I = 0x7f0e0085

.field public static final action_bar_title:I = 0x7f0e0084

.field public static final action_context_bar:I = 0x7f0e00a6

.field public static final action_divider:I = 0x7f0e0598

.field public static final action_menu_divider:I = 0x7f0e0002

.field public static final action_menu_presenter:I = 0x7f0e0003

.field public static final action_mode_bar:I = 0x7f0e00a2

.field public static final action_mode_bar_stub:I = 0x7f0e00a1

.field public static final action_mode_close_button:I = 0x7f0e0086

.field public static final activity_chooser_view_content:I = 0x7f0e0087

.field public static final add:I = 0x7f0e0036

.field public static final alertTitle:I = 0x7f0e0093

.field public static final all:I = 0x7f0e0024

.field public static final always:I = 0x7f0e0061

.field public static final aplicativo_token_type:I = 0x7f0e04e0

.field public static final auto:I = 0x7f0e003d

.field public static final background_gradient_search:I = 0x7f0e02f9

.field public static final beginning:I = 0x7f0e0057

.field public static final bottom:I = 0x7f0e003e

.field public static final btn_feed_nao_library:I = 0x7f0e0631

.field public static final btn_feed_sim_library:I = 0x7f0e0632

.field public static final btn_feedback_no:I = 0x7f0e0471

.field public static final btn_feedback_yes:I = 0x7f0e0470

.field public static final buttonPanel:I = 0x7f0e008e

.field public static final button_ok_unlock:I = 0x7f0e04db

.field public static final cancel_action:I = 0x7f0e0595

.field public static final card_dialog:I = 0x7f0e04b9

.field public static final card_view_faq:I = 0x7f0e035f

.field public static final card_view_faq_item_description:I = 0x7f0e0361

.field public static final card_view_faq_item_title:I = 0x7f0e0360

.field public static final card_view_need_help_tel:I = 0x7f0e046d

.field public static final center:I = 0x7f0e0042

.field public static final center_horizontal:I = 0x7f0e0043

.field public static final center_vertical:I = 0x7f0e0044

.field public static final chaveiro_token_type:I = 0x7f0e04df

.field public static final checkbox:I = 0x7f0e009c

.field public static final chronometer:I = 0x7f0e059d

.field public static final clip_horizontal:I = 0x7f0e004a

.field public static final clip_vertical:I = 0x7f0e004b

.field public static final collapseActionView:I = 0x7f0e0062

.field public static final contentPanel:I = 0x7f0e0094

.field public static final custom:I = 0x7f0e009a

.field public static final customPanel:I = 0x7f0e0099

.field public static final custom_text_transitional:I = 0x7f0e0304

.field public static final decor_content_parent:I = 0x7f0e00a3

.field public static final default_activity_button:I = 0x7f0e008a

.field public static final design_bottom_sheet:I = 0x7f0e0380

.field public static final design_menu_item_action_area:I = 0x7f0e0387

.field public static final design_menu_item_action_area_stub:I = 0x7f0e0386

.field public static final design_menu_item_text:I = 0x7f0e0385

.field public static final design_navigation_view:I = 0x7f0e0384

.field public static final disableHome:I = 0x7f0e002b

.field public static final divisor:I = 0x7f0e04cd

.field public static final divisor_sms:I = 0x7f0e04d7

.field public static final edit_query:I = 0x7f0e00a7

.field public static final edit_query_more:I = 0x7f0e065a

.field public static final end:I = 0x7f0e0045

.field public static final end_padder:I = 0x7f0e05a6

.field public static final enterAlways:I = 0x7f0e0031

.field public static final enterAlwaysCollapsed:I = 0x7f0e0032

.field public static final error_system_close:I = 0x7f0e0170

.field public static final exitUntilCollapsed:I = 0x7f0e0033

.field public static final expand_activities_button:I = 0x7f0e0088

.field public static final expanded_menu:I = 0x7f0e009b

.field public static final feed_back_library:I = 0x7f0e0189

.field public static final fill:I = 0x7f0e004c

.field public static final fill_horizontal:I = 0x7f0e004d

.field public static final fill_vertical:I = 0x7f0e0046

.field public static final first_token:I = 0x7f0e04d1

.field public static final fixed:I = 0x7f0e0075

.field public static final frame_container_search:I = 0x7f0e01be

.field public static final frame_token_back_container:I = 0x7f0e04ba

.field public static final global_loading:I = 0x7f0e04b1

.field public static final help_search_voice:I = 0x7f0e02fb

.field public static final help_voice_balance:I = 0x7f0e02ff

.field public static final help_voice_limit:I = 0x7f0e0301

.field public static final help_voice_suggestions:I = 0x7f0e02fd

.field public static final help_voice_transfer:I = 0x7f0e0300

.field public static final home:I = 0x7f0e0005

.field public static final homeAsUp:I = 0x7f0e002c

.field public static final home_linear_list_container:I = 0x7f0e01cd

.field public static final home_scroll_view:I = 0x7f0e01cc

.field public static final ic_attendance_arrow:I = 0x7f0e05ce

.field public static final ic_attendance_channel:I = 0x7f0e05cc

.field public static final ic_seta:I = 0x7f0e04cb

.field public static final icon:I = 0x7f0e008c

.field public static final id_attendance_search_fragment:I = 0x7f0e0006

.field public static final id_cards_search_list_fragment:I = 0x7f0e0007

.field public static final id_empty_search_fragment:I = 0x7f0e0008

.field public static final id_faq_list_fragment:I = 0x7f0e0009

.field public static final id_feedback_search_fragment:I = 0x7f0e000a

.field public static final id_menu_list_fragment:I = 0x7f0e000b

.field public static final id_receipts_search_fragment:I = 0x7f0e000c

.field public static final ifRoom:I = 0x7f0e0063

.field public static final image:I = 0x7f0e0089

.field public static final image_menu_icon_search:I = 0x7f0e05c0

.field public static final image_view_expanded:I = 0x7f0e0366

.field public static final image_view_search_voice:I = 0x7f0e0307

.field public static final img_close_yes_library:I = 0x7f0e0637

.field public static final info:I = 0x7f0e059e

.field public static final itausdkcore_message_token_number:I = 0x7f0e04dc

.field public static final item_layout_container:I = 0x7f0e05bd

.field public static final item_touch_helper_previous_elevation:I = 0x7f0e000d

.field public static final left:I = 0x7f0e003f

.field public static final line1:I = 0x7f0e05a3

.field public static final line3:I = 0x7f0e05a5

.field public static final linear_balance_description:I = 0x7f0e0362

.field public static final linear_description:I = 0x7f0e02fe

.field public static final linear_description_content:I = 0x7f0e0171

.field public static final linear_expanded:I = 0x7f0e0367

.field public static final linear_extract:I = 0x7f0e0364

.field public static final linear_feedback_collaboration:I = 0x7f0e0472

.field public static final linear_help:I = 0x7f0e02fa

.field public static final linear_item_very_common:I = 0x7f0e01c2

.field public static final linear_layout_need_help:I = 0x7f0e01c9

.field public static final linear_links_container_library:I = 0x7f0e0187

.field public static final linear_recycler:I = 0x7f0e01c0

.field public static final linear_root:I = 0x7f0e01bb

.field public static final linear_sound:I = 0x7f0e0303

.field public static final linear_suggestions:I = 0x7f0e02fc

.field public static final linear_transitional:I = 0x7f0e0302

.field public static final listMode:I = 0x7f0e0028

.field public static final list_channels_library:I = 0x7f0e0188

.field public static final list_item:I = 0x7f0e008b

.field public static final media_actions:I = 0x7f0e0597

.field public static final menu_search:I = 0x7f0e06d2

.field public static final message:I = 0x7f0e04bc

.field public static final message_number_text:I = 0x7f0e04d2

.field public static final message_number_time:I = 0x7f0e04d6

.field public static final middle:I = 0x7f0e0058

.field public static final mini:I = 0x7f0e0056

.field public static final more_items_btn:I = 0x7f0e0681

.field public static final multiply:I = 0x7f0e0037

.field public static final navigation_header_container:I = 0x7f0e0383

.field public static final nested_scrollview:I = 0x7f0e01bc

.field public static final never:I = 0x7f0e0064

.field public static final none:I = 0x7f0e0027

.field public static final normal:I = 0x7f0e0029

.field public static final oveflow_message_textView:I = 0x7f0e0602

.field public static final parallax:I = 0x7f0e0048

.field public static final parentPanel:I = 0x7f0e0090

.field public static final pin:I = 0x7f0e0049

.field public static final progress_account_loading:I = 0x7f0e036b

.field public static final progress_circular:I = 0x7f0e000e

.field public static final progress_horizontal:I = 0x7f0e000f

.field public static final progress_search_loading:I = 0x7f0e01cb

.field public static final radio:I = 0x7f0e009e

.field public static final receipt_date_textView:I = 0x7f0e065e

.field public static final receipt_description_textView:I = 0x7f0e065d

.field public static final receipt_title_textView:I = 0x7f0e065b

.field public static final receipt_value_textView:I = 0x7f0e065c

.field public static final recycler_view:I = 0x7f0e01c1

.field public static final recycler_view_faq_search:I = 0x7f0e0413

.field public static final recycler_view_menus_search:I = 0x7f0e044b

.field public static final recycler_view_receipts_search:I = 0x7f0e046c

.field public static final relative_answer:I = 0x7f0e0184

.field public static final relative_answer_foot_library:I = 0x7f0e062f

.field public static final relative_answer_yes_library:I = 0x7f0e0633

.field public static final relative_balance_account_balance_container:I = 0x7f0e0369

.field public static final relative_bottom:I = 0x7f0e0305

.field public static final relative_feedback_yes_no:I = 0x7f0e01c3

.field public static final relative_icon_row:I = 0x7f0e05bf

.field public static final relative_link:I = 0x7f0e0666

.field public static final relative_search_cards_top:I = 0x7f0e01bd

.field public static final relative_search_container:I = 0x7f0e01bf

.field public static final relative_search_empty_container:I = 0x7f0e01c4

.field public static final relative_search_loading_container:I = 0x7f0e01ca

.field public static final relative_top:I = 0x7f0e016f

.field public static final right:I = 0x7f0e0040

.field public static final screen:I = 0x7f0e0038

.field public static final scroll:I = 0x7f0e0034

.field public static final scrollIndicatorDown:I = 0x7f0e0098

.field public static final scrollIndicatorUp:I = 0x7f0e0095

.field public static final scrollView:I = 0x7f0e0096

.field public static final scroll_answer_library:I = 0x7f0e0183

.field public static final scrollable:I = 0x7f0e0076

.field public static final search_badge:I = 0x7f0e00a9

.field public static final search_bar:I = 0x7f0e00a8

.field public static final search_button:I = 0x7f0e00aa

.field public static final search_close_btn:I = 0x7f0e00af

.field public static final search_edit_frame:I = 0x7f0e00ab

.field public static final search_empty_glass:I = 0x7f0e01c5

.field public static final search_font_icon_help_phone:I = 0x7f0e046e

.field public static final search_go_btn:I = 0x7f0e00b1

.field public static final search_mag_icon:I = 0x7f0e00ac

.field public static final search_plate:I = 0x7f0e00ad

.field public static final search_src_text:I = 0x7f0e00ae

.field public static final search_voice_btn:I = 0x7f0e00b2

.field public static final second_divisor:I = 0x7f0e04d9

.field public static final second_token_number_edit:I = 0x7f0e04da

.field public static final second_token_number_text:I = 0x7f0e04d8

.field public static final select_dialog_listview:I = 0x7f0e00b3

.field public static final shortcut:I = 0x7f0e009d

.field public static final showCustom:I = 0x7f0e002d

.field public static final showHome:I = 0x7f0e002e

.field public static final showTitle:I = 0x7f0e002f

.field public static final sms_token_type:I = 0x7f0e04de

.field public static final snackbar_action:I = 0x7f0e0382

.field public static final snackbar_text:I = 0x7f0e0381

.field public static final snap:I = 0x7f0e0035

.field public static final sound_indicator:I = 0x7f0e0306

.field public static final spacer:I = 0x7f0e008f

.field public static final split_action_bar:I = 0x7f0e0010

.field public static final src_atop:I = 0x7f0e0039

.field public static final src_in:I = 0x7f0e003a

.field public static final src_over:I = 0x7f0e003b

.field public static final start:I = 0x7f0e0047

.field public static final status_bar_latest_event_content:I = 0x7f0e0596

.field public static final submenuarrow:I = 0x7f0e009f

.field public static final submit_area:I = 0x7f0e00b0

.field public static final tabMode:I = 0x7f0e002a

.field public static final text:I = 0x7f0e0011

.field public static final text2:I = 0x7f0e05a4

.field public static final textSpacerNoButtons:I = 0x7f0e0097

.field public static final textView_see_extract:I = 0x7f0e0363

.field public static final text_answer_library:I = 0x7f0e0186

.field public static final text_attendance_channel:I = 0x7f0e05cd

.field public static final text_attendance_channel_description:I = 0x7f0e05cb

.field public static final text_balance_account_error:I = 0x7f0e0368

.field public static final text_faq_library:I = 0x7f0e0185

.field public static final text_input_password_toggle:I = 0x7f0e0388

.field public static final text_name_item_list:I = 0x7f0e05c1

.field public static final text_name_title_list:I = 0x7f0e05be

.field public static final text_view_currency_signal_currency:I = 0x7f0e0603

.field public static final text_view_currency_value_currency:I = 0x7f0e0604

.field public static final text_view_currency_value_value:I = 0x7f0e0605

.field public static final text_view_expanded:I = 0x7f0e0365

.field public static final time:I = 0x7f0e059c

.field public static final title:I = 0x7f0e008d

.field public static final title_template:I = 0x7f0e0092

.field public static final token_back:I = 0x7f0e04bb

.field public static final token_dialog_locked:I = 0x7f0e04c7

.field public static final token_dialog_unlocked:I = 0x7f0e04dd

.field public static final token_divisor:I = 0x7f0e04c0

.field public static final token_edit_container:I = 0x7f0e04ce

.field public static final token_icon:I = 0x7f0e04c9

.field public static final token_label:I = 0x7f0e04ca

.field public static final token_loading:I = 0x7f0e04cc

.field public static final token_number_edit:I = 0x7f0e04d3

.field public static final token_password_button:I = 0x7f0e04be

.field public static final token_password_edit:I = 0x7f0e04bd

.field public static final token_primary_action:I = 0x7f0e04d4

.field public static final token_secondary_action:I = 0x7f0e04d5

.field public static final token_title:I = 0x7f0e04bf

.field public static final token_type_container:I = 0x7f0e04c8

.field public static final toolbar_library:I = 0x7f0e05c3

.field public static final top:I = 0x7f0e0041

.field public static final topPanel:I = 0x7f0e0091

.field public static final touch_outside:I = 0x7f0e037f

.field public static final tv_custom_color_feedback:I = 0x7f0e0473

.field public static final tv_search_entered:I = 0x7f0e01c7

.field public static final tv_search_key:I = 0x7f0e01c8

.field public static final tv_search_not:I = 0x7f0e01c6

.field public static final txt_about_link:I = 0x7f0e0667

.field public static final txt_error_system_color:I = 0x7f0e0174

.field public static final txt_error_system_hello:I = 0x7f0e0172

.field public static final txt_error_system_search:I = 0x7f0e0173

.field public static final txt_error_system_try:I = 0x7f0e0175

.field public static final txt_important_library:I = 0x7f0e0636

.field public static final txt_info_util_library:I = 0x7f0e0630

.field public static final txt_obrigado:I = 0x7f0e0638

.field public static final txt_thank_you_library:I = 0x7f0e0635

.field public static final unlocking_divisor:I = 0x7f0e04cf

.field public static final unlocking_warning:I = 0x7f0e04d0

.field public static final up:I = 0x7f0e0015

.field public static final useLogo:I = 0x7f0e0030

.field public static final view_balance_account_currency:I = 0x7f0e036a

.field public static final view_linear_feedback:I = 0x7f0e046f

.field public static final view_offset_helper:I = 0x7f0e0016

.field public static final view_space_link:I = 0x7f0e0668

.field public static final view_top_library:I = 0x7f0e0634

.field public static final voice_close:I = 0x7f0e0308

.field public static final webview_dialog:I = 0x7f0e04c1

.field public static final webview_title:I = 0x7f0e04c2

.field public static final withText:I = 0x7f0e0065

.field public static final wrap_content:I = 0x7f0e003c


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 1073
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.class Lbr/com/itau/textovoz/ui/view/custom/PulseLayout$1;
.super Ljava/lang/Object;
.source "PulseLayout.java"

# interfaces
.implements Landroid/animation/Animator$AnimatorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lbr/com/itau/textovoz/ui/view/custom/PulseLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lbr/com/itau/textovoz/ui/view/custom/PulseLayout;


# direct methods
.method constructor <init>(Lbr/com/itau/textovoz/ui/view/custom/PulseLayout;)V
    .registers 2
    .param p1, "this$0"    # Lbr/com/itau/textovoz/ui/view/custom/PulseLayout;

    .line 329
    iput-object p1, p0, Lbr/com/itau/textovoz/ui/view/custom/PulseLayout$1;->this$0:Lbr/com/itau/textovoz/ui/view/custom/PulseLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationCancel(Landroid/animation/Animator;)V
    .registers 4
    .param p1, "animator"    # Landroid/animation/Animator;

    .line 343
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/view/custom/PulseLayout$1;->this$0:Lbr/com/itau/textovoz/ui/view/custom/PulseLayout;

    const/4 v1, 0x0

    # setter for: Lbr/com/itau/textovoz/ui/view/custom/PulseLayout;->isStarted:Z
    invoke-static {v0, v1}, Lbr/com/itau/textovoz/ui/view/custom/PulseLayout;->access$402(Lbr/com/itau/textovoz/ui/view/custom/PulseLayout;Z)Z

    .line 344
    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .registers 4
    .param p1, "animator"    # Landroid/animation/Animator;

    .line 338
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/view/custom/PulseLayout$1;->this$0:Lbr/com/itau/textovoz/ui/view/custom/PulseLayout;

    const/4 v1, 0x0

    # setter for: Lbr/com/itau/textovoz/ui/view/custom/PulseLayout;->isStarted:Z
    invoke-static {v0, v1}, Lbr/com/itau/textovoz/ui/view/custom/PulseLayout;->access$402(Lbr/com/itau/textovoz/ui/view/custom/PulseLayout;Z)Z

    .line 339
    return-void
.end method

.method public onAnimationRepeat(Landroid/animation/Animator;)V
    .registers 2
    .param p1, "animator"    # Landroid/animation/Animator;

    .line 348
    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .registers 4
    .param p1, "animator"    # Landroid/animation/Animator;

    .line 333
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/view/custom/PulseLayout$1;->this$0:Lbr/com/itau/textovoz/ui/view/custom/PulseLayout;

    const/4 v1, 0x1

    # setter for: Lbr/com/itau/textovoz/ui/view/custom/PulseLayout;->isStarted:Z
    invoke-static {v0, v1}, Lbr/com/itau/textovoz/ui/view/custom/PulseLayout;->access$402(Lbr/com/itau/textovoz/ui/view/custom/PulseLayout;Z)Z

    .line 334
    return-void
.end method

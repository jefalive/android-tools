.class public Lcom/itau/empresas/ui/view/MesEAnoPickerDialog;
.super Landroid/support/v4/app/DialogFragment;
.source "MesEAnoPickerDialog.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/itau/empresas/ui/view/MesEAnoPickerDialog$MesEAnoPickerDialogListener;
    }
.end annotation


# instance fields
.field private anoPicker:Landroid/widget/NumberPicker;

.field private builder:Landroid/support/v7/app/AlertDialog$Builder;

.field private dialog:Landroid/view/View;

.field private listener:Lcom/itau/empresas/ui/view/MesEAnoPickerDialog$MesEAnoPickerDialogListener;

.field private mesPicker:Landroid/widget/NumberPicker;

.field private numeroMaximoMeses:I


# direct methods
.method public constructor <init>()V
    .registers 2

    .line 19
    invoke-direct {p0}, Landroid/support/v4/app/DialogFragment;-><init>()V

    .line 29
    const/16 v0, 0xc

    iput v0, p0, Lcom/itau/empresas/ui/view/MesEAnoPickerDialog;->numeroMaximoMeses:I

    return-void
.end method

.method static synthetic access$000(Lcom/itau/empresas/ui/view/MesEAnoPickerDialog;)Ljava/lang/String;
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/ui/view/MesEAnoPickerDialog;

    .line 19
    invoke-direct {p0}, Lcom/itau/empresas/ui/view/MesEAnoPickerDialog;->getMesAnoFormatado()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100(Lcom/itau/empresas/ui/view/MesEAnoPickerDialog;)Lcom/itau/empresas/ui/view/MesEAnoPickerDialog$MesEAnoPickerDialogListener;
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/ui/view/MesEAnoPickerDialog;

    .line 19
    iget-object v0, p0, Lcom/itau/empresas/ui/view/MesEAnoPickerDialog;->listener:Lcom/itau/empresas/ui/view/MesEAnoPickerDialog$MesEAnoPickerDialogListener;

    return-object v0
.end method

.method static synthetic access$200(Lcom/itau/empresas/ui/view/MesEAnoPickerDialog;)Landroid/widget/NumberPicker;
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/ui/view/MesEAnoPickerDialog;

    .line 19
    iget-object v0, p0, Lcom/itau/empresas/ui/view/MesEAnoPickerDialog;->mesPicker:Landroid/widget/NumberPicker;

    return-object v0
.end method

.method static synthetic access$300(I)Ljava/lang/String;
    .registers 2
    .param p0, "x0"    # I

    .line 19
    invoke-static {p0}, Lcom/itau/empresas/ui/view/MesEAnoPickerDialog;->setMesCompetencia(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$400(Lcom/itau/empresas/ui/view/MesEAnoPickerDialog;)Landroid/widget/NumberPicker;
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/ui/view/MesEAnoPickerDialog;

    .line 19
    iget-object v0, p0, Lcom/itau/empresas/ui/view/MesEAnoPickerDialog;->anoPicker:Landroid/widget/NumberPicker;

    return-object v0
.end method

.method private getMesAnoFormatado()Ljava/lang/String;
    .registers 3

    .line 84
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/itau/empresas/ui/view/MesEAnoPickerDialog;->mesPicker:Landroid/widget/NumberPicker;

    invoke-virtual {v1}, Landroid/widget/NumberPicker;->getValue()I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/ui/view/MesEAnoPickerDialog;->anoPicker:Landroid/widget/NumberPicker;

    .line 85
    invoke-virtual {v1}, Landroid/widget/NumberPicker;->getValue()I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 84
    return-object v0
.end method

.method private listenerOnclick()V
    .registers 4

    .line 67
    iget-object v0, p0, Lcom/itau/empresas/ui/view/MesEAnoPickerDialog;->builder:Landroid/support/v7/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/itau/empresas/ui/view/MesEAnoPickerDialog;->dialog:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/support/v7/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/itau/empresas/ui/view/MesEAnoPickerDialog$2;

    invoke-direct {v1, p0}, Lcom/itau/empresas/ui/view/MesEAnoPickerDialog$2;-><init>(Lcom/itau/empresas/ui/view/MesEAnoPickerDialog;)V

    .line 68
    const v2, 0x7f07048b

    invoke-virtual {v0, v2, v1}, Landroid/support/v7/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/itau/empresas/ui/view/MesEAnoPickerDialog$1;

    invoke-direct {v1, p0}, Lcom/itau/empresas/ui/view/MesEAnoPickerDialog$1;-><init>(Lcom/itau/empresas/ui/view/MesEAnoPickerDialog;)V

    .line 75
    const v2, 0x7f070175

    invoke-virtual {v0, v2, v1}, Landroid/support/v7/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/AlertDialog$Builder;

    .line 80
    return-void
.end method

.method private static setMesCompetencia(I)Ljava/lang/String;
    .registers 3
    .param p0, "mes"    # I

    .line 89
    const/16 v0, 0xa

    if-ge p0, v0, :cond_f

    .line 90
    const-string v0, "0"

    invoke-static {p0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 92
    :cond_f
    invoke-static {p0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private setaValoresPicker()V
    .registers 5

    .line 56
    invoke-static {}, Lorg/threeten/bp/LocalDate;->now()Lorg/threeten/bp/LocalDate;

    move-result-object v3

    .line 57
    .local v3, "localDate":Lorg/threeten/bp/LocalDate;
    iget-object v0, p0, Lcom/itau/empresas/ui/view/MesEAnoPickerDialog;->mesPicker:Landroid/widget/NumberPicker;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/NumberPicker;->setMinValue(I)V

    .line 58
    iget-object v0, p0, Lcom/itau/empresas/ui/view/MesEAnoPickerDialog;->mesPicker:Landroid/widget/NumberPicker;

    iget v1, p0, Lcom/itau/empresas/ui/view/MesEAnoPickerDialog;->numeroMaximoMeses:I

    invoke-virtual {v0, v1}, Landroid/widget/NumberPicker;->setMaxValue(I)V

    .line 59
    iget-object v0, p0, Lcom/itau/empresas/ui/view/MesEAnoPickerDialog;->mesPicker:Landroid/widget/NumberPicker;

    invoke-virtual {v3}, Lorg/threeten/bp/LocalDate;->getMonthValue()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/NumberPicker;->setValue(I)V

    .line 61
    iget-object v0, p0, Lcom/itau/empresas/ui/view/MesEAnoPickerDialog;->anoPicker:Landroid/widget/NumberPicker;

    invoke-virtual {v3}, Lorg/threeten/bp/LocalDate;->getYear()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Landroid/widget/NumberPicker;->setMinValue(I)V

    .line 62
    iget-object v0, p0, Lcom/itau/empresas/ui/view/MesEAnoPickerDialog;->anoPicker:Landroid/widget/NumberPicker;

    const/16 v1, 0x833

    invoke-virtual {v0, v1}, Landroid/widget/NumberPicker;->setMaxValue(I)V

    .line 63
    iget-object v0, p0, Lcom/itau/empresas/ui/view/MesEAnoPickerDialog;->anoPicker:Landroid/widget/NumberPicker;

    invoke-virtual {v3}, Lorg/threeten/bp/LocalDate;->getYear()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/NumberPicker;->setValue(I)V

    .line 64
    return-void
.end method


# virtual methods
.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .registers 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 37
    new-instance v0, Landroid/support/v7/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/MesEAnoPickerDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v7/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/itau/empresas/ui/view/MesEAnoPickerDialog;->builder:Landroid/support/v7/app/AlertDialog$Builder;

    .line 38
    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/MesEAnoPickerDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v2

    .line 39
    .local v2, "inflater":Landroid/view/LayoutInflater;
    const v0, 0x7f030075

    const/4 v1, 0x0

    invoke-virtual {v2, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/ui/view/MesEAnoPickerDialog;->dialog:Landroid/view/View;

    .line 40
    iget-object v0, p0, Lcom/itau/empresas/ui/view/MesEAnoPickerDialog;->dialog:Landroid/view/View;

    const v1, 0x7f0e0377

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/NumberPicker;

    iput-object v0, p0, Lcom/itau/empresas/ui/view/MesEAnoPickerDialog;->mesPicker:Landroid/widget/NumberPicker;

    .line 41
    iget-object v0, p0, Lcom/itau/empresas/ui/view/MesEAnoPickerDialog;->dialog:Landroid/view/View;

    const v1, 0x7f0e0378

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/NumberPicker;

    iput-object v0, p0, Lcom/itau/empresas/ui/view/MesEAnoPickerDialog;->anoPicker:Landroid/widget/NumberPicker;

    .line 42
    invoke-direct {p0}, Lcom/itau/empresas/ui/view/MesEAnoPickerDialog;->setaValoresPicker()V

    .line 43
    invoke-direct {p0}, Lcom/itau/empresas/ui/view/MesEAnoPickerDialog;->listenerOnclick()V

    .line 44
    iget-object v0, p0, Lcom/itau/empresas/ui/view/MesEAnoPickerDialog;->builder:Landroid/support/v7/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/support/v7/app/AlertDialog$Builder;->create()Landroid/support/v7/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method

.method public setListener(Lcom/itau/empresas/ui/view/MesEAnoPickerDialog$MesEAnoPickerDialogListener;)V
    .registers 2
    .param p1, "listener"    # Lcom/itau/empresas/ui/view/MesEAnoPickerDialog$MesEAnoPickerDialogListener;

    .line 32
    iput-object p1, p0, Lcom/itau/empresas/ui/view/MesEAnoPickerDialog;->listener:Lcom/itau/empresas/ui/view/MesEAnoPickerDialog$MesEAnoPickerDialogListener;

    .line 33
    return-void
.end method

.method public setNumeroMaximoMeses(I)V
    .registers 2
    .param p1, "numeroMaximoMeses"    # I

    .line 52
    iput p1, p0, Lcom/itau/empresas/ui/view/MesEAnoPickerDialog;->numeroMaximoMeses:I

    .line 53
    return-void
.end method

.method public show(Landroid/support/v4/app/FragmentManager;)V
    .registers 3
    .param p1, "fragmentManager"    # Landroid/support/v4/app/FragmentManager;

    .line 48
    const-string v0, "MesEAnoPickerDialog"

    invoke-virtual {p0, p1, v0}, Lcom/itau/empresas/ui/view/MesEAnoPickerDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 49
    return-void
.end method

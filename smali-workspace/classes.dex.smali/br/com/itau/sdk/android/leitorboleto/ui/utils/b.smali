.class public Lbr/com/itau/sdk/android/leitorboleto/ui/utils/b;
.super Ljava/lang/Object;


# static fields
.field private static a:Landroid/graphics/Typeface;

.field private static b:Landroid/graphics/Typeface;


# direct methods
.method public static a(Landroid/content/res/AssetManager;)Landroid/graphics/Typeface;
    .registers 2

    sget-object v0, Lbr/com/itau/sdk/android/leitorboleto/ui/utils/b;->a:Landroid/graphics/Typeface;

    if-nez v0, :cond_c

    const-string v0, "fonts/Arial.ttf"

    invoke-static {p0, v0}, Landroid/graphics/Typeface;->createFromAsset(Landroid/content/res/AssetManager;Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v0

    sput-object v0, Lbr/com/itau/sdk/android/leitorboleto/ui/utils/b;->a:Landroid/graphics/Typeface;

    :cond_c
    sget-object v0, Lbr/com/itau/sdk/android/leitorboleto/ui/utils/b;->a:Landroid/graphics/Typeface;

    return-object v0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/CharSequence;
    .registers 7

    new-instance v3, Landroid/text/SpannableStringBuilder;

    invoke-direct {v3, p1}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    new-instance v4, Lbr/com/itau/sdk/android/leitorboleto/ui/utils/a;

    invoke-virtual {p0}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    invoke-static {v0}, Lbr/com/itau/sdk/android/leitorboleto/ui/utils/b;->a(Landroid/content/res/AssetManager;)Landroid/graphics/Typeface;

    move-result-object v0

    invoke-direct {v4, v0}, Lbr/com/itau/sdk/android/leitorboleto/ui/utils/a;-><init>(Landroid/graphics/Typeface;)V

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x0

    const/16 v2, 0x21

    invoke-virtual {v3, v4, v1, v0, v2}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    return-object v3
.end method

.method public static b(Landroid/content/res/AssetManager;)Landroid/graphics/Typeface;
    .registers 2

    sget-object v0, Lbr/com/itau/sdk/android/leitorboleto/ui/utils/b;->b:Landroid/graphics/Typeface;

    if-nez v0, :cond_c

    const-string v0, "fonts/Arial-bold.ttf"

    invoke-static {p0, v0}, Landroid/graphics/Typeface;->createFromAsset(Landroid/content/res/AssetManager;Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v0

    sput-object v0, Lbr/com/itau/sdk/android/leitorboleto/ui/utils/b;->b:Landroid/graphics/Typeface;

    :cond_c
    sget-object v0, Lbr/com/itau/sdk/android/leitorboleto/ui/utils/b;->b:Landroid/graphics/Typeface;

    return-object v0
.end method

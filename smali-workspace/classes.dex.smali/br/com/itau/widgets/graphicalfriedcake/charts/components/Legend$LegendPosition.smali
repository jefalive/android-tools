.class public final enum Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;
.super Ljava/lang/Enum;
.source "Legend.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "LegendPosition"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;>;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;

.field public static final enum BELOW_CHART_CENTER:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;

.field public static final enum BELOW_CHART_LEFT:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;

.field public static final enum BELOW_CHART_RIGHT:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;

.field public static final enum LEFT_OF_CHART:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;

.field public static final enum LEFT_OF_CHART_CENTER:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;

.field public static final enum LEFT_OF_CHART_INSIDE:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;

.field public static final enum LegendPosition:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;

.field public static final enum PIECHART_CENTER:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;

.field public static final enum RIGHT_OF_CHART:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;

.field public static final enum RIGHT_OF_CHART_CENTER:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;

.field public static final enum RIGHT_OF_CHART_INSIDE:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .line 23
    new-instance v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;

    const-string v1, "RIGHT_OF_CHART"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;->RIGHT_OF_CHART:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;

    new-instance v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;

    const-string v1, "RIGHT_OF_CHART_CENTER"

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;->RIGHT_OF_CHART_CENTER:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;

    new-instance v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;

    const-string v1, "RIGHT_OF_CHART_INSIDE"

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;->RIGHT_OF_CHART_INSIDE:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;

    .line 24
    new-instance v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;

    const-string v1, "LEFT_OF_CHART"

    const/4 v2, 0x3

    invoke-direct {v0, v1, v2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;->LEFT_OF_CHART:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;

    new-instance v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;

    const-string v1, "LEFT_OF_CHART_CENTER"

    const/4 v2, 0x4

    invoke-direct {v0, v1, v2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;->LEFT_OF_CHART_CENTER:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;

    new-instance v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;

    const-string v1, "LEFT_OF_CHART_INSIDE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;->LEFT_OF_CHART_INSIDE:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;

    .line 25
    new-instance v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;

    const-string v1, "BELOW_CHART_LEFT"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;->BELOW_CHART_LEFT:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;

    new-instance v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;

    const-string v1, "BELOW_CHART_RIGHT"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;->BELOW_CHART_RIGHT:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;

    new-instance v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;

    const-string v1, "BELOW_CHART_CENTER"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;->BELOW_CHART_CENTER:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;

    .line 26
    new-instance v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;

    const-string v1, "LegendPosition"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;->LegendPosition:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;

    new-instance v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;

    const-string v1, "PIECHART_CENTER"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;->PIECHART_CENTER:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;

    .line 22
    const/16 v0, 0xb

    new-array v0, v0, [Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;

    sget-object v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;->RIGHT_OF_CHART:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sget-object v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;->RIGHT_OF_CHART_CENTER:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    sget-object v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;->RIGHT_OF_CHART_INSIDE:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    sget-object v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;->LEFT_OF_CHART:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    sget-object v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;->LEFT_OF_CHART_CENTER:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;

    const/4 v2, 0x4

    aput-object v1, v0, v2

    sget-object v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;->LEFT_OF_CHART_INSIDE:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;

    const/4 v2, 0x5

    aput-object v1, v0, v2

    sget-object v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;->BELOW_CHART_LEFT:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;

    const/4 v2, 0x6

    aput-object v1, v0, v2

    sget-object v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;->BELOW_CHART_RIGHT:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;

    const/4 v2, 0x7

    aput-object v1, v0, v2

    sget-object v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;->BELOW_CHART_CENTER:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;

    const/16 v2, 0x8

    aput-object v1, v0, v2

    sget-object v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;->LegendPosition:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;

    const/16 v2, 0x9

    aput-object v1, v0, v2

    sget-object v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;->PIECHART_CENTER:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;

    const/16 v2, 0xa

    aput-object v1, v0, v2

    sput-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;->$VALUES:[Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 22
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;
    .registers 2
    .param p0, "name"    # Ljava/lang/String;

    .line 22
    const-class v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;

    return-object v0
.end method

.method public static values()[Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;
    .registers 1

    .line 22
    sget-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;->$VALUES:[Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;

    invoke-virtual {v0}, [Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;

    return-object v0
.end method

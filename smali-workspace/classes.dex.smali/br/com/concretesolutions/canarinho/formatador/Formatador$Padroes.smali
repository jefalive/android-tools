.class public abstract Lbr/com/concretesolutions/canarinho/formatador/Formatador$Padroes;
.super Ljava/lang/Object;
.source "Formatador.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lbr/com/concretesolutions/canarinho/formatador/Formatador;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Padroes"
.end annotation


# static fields
.field public static final CEP_DESFORMATADO:Ljava/util/regex/Pattern;

.field public static final CEP_FORMATADO:Ljava/util/regex/Pattern;

.field public static final CNPJ_DESFORMATADO:Ljava/util/regex/Pattern;

.field public static final CNPJ_FORMATADO:Ljava/util/regex/Pattern;

.field public static final CPF_DESFORMATADO:Ljava/util/regex/Pattern;

.field public static final CPF_FORMATADO:Ljava/util/regex/Pattern;

.field public static final PADRAO_SOMENTE_NUMEROS:Ljava/util/regex/Pattern;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .line 95
    const-string v0, "(\\d{5})-(\\d{3})"

    .line 96
    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lbr/com/concretesolutions/canarinho/formatador/Formatador$Padroes;->CEP_FORMATADO:Ljava/util/regex/Pattern;

    .line 97
    const-string v0, "(\\d{5})(\\d{3})"

    .line 98
    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lbr/com/concretesolutions/canarinho/formatador/Formatador$Padroes;->CEP_DESFORMATADO:Ljava/util/regex/Pattern;

    .line 99
    const-string v0, "(\\d{2})[.](\\d{3})[.](\\d{3})/(\\d{4})-(\\d{2})"

    .line 100
    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lbr/com/concretesolutions/canarinho/formatador/Formatador$Padroes;->CNPJ_FORMATADO:Ljava/util/regex/Pattern;

    .line 101
    const-string v0, "(\\d{2})(\\d{3})(\\d{3})(\\d{4})(\\d{2})"

    .line 102
    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lbr/com/concretesolutions/canarinho/formatador/Formatador$Padroes;->CNPJ_DESFORMATADO:Ljava/util/regex/Pattern;

    .line 103
    const-string v0, "(\\d{3})[.](\\d{3})[.](\\d{3})-(\\d{2})"

    .line 104
    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lbr/com/concretesolutions/canarinho/formatador/Formatador$Padroes;->CPF_FORMATADO:Ljava/util/regex/Pattern;

    .line 105
    const-string v0, "(\\d{3})(\\d{3})(\\d{3})(\\d{2})"

    .line 106
    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lbr/com/concretesolutions/canarinho/formatador/Formatador$Padroes;->CPF_DESFORMATADO:Ljava/util/regex/Pattern;

    .line 107
    const-string v0, "[^0-9]"

    .line 108
    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lbr/com/concretesolutions/canarinho/formatador/Formatador$Padroes;->PADRAO_SOMENTE_NUMEROS:Ljava/util/regex/Pattern;

    .line 107
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    .line 93
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

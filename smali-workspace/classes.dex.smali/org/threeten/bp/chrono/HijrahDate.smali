.class public final Lorg/threeten/bp/chrono/HijrahDate;
.super Lorg/threeten/bp/chrono/ChronoDateImpl;
.source "HijrahDate.java"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/threeten/bp/chrono/HijrahDate$1;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/threeten/bp/chrono/ChronoDateImpl<Lorg/threeten/bp/chrono/HijrahDate;>;Ljava/io/Serializable;"
    }
.end annotation


# static fields
.field private static final ADJUSTED_CYCLES:[Ljava/lang/Long;

.field private static final ADJUSTED_CYCLE_YEARS:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<Ljava/lang/Integer;[Ljava/lang/Integer;>;"
        }
    .end annotation
.end field

.field private static final ADJUSTED_LEAST_MAX_VALUES:[Ljava/lang/Integer;

.field private static final ADJUSTED_MAX_VALUES:[Ljava/lang/Integer;

.field private static final ADJUSTED_MIN_VALUES:[Ljava/lang/Integer;

.field private static final ADJUSTED_MONTH_DAYS:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<Ljava/lang/Integer;[Ljava/lang/Integer;>;"
        }
    .end annotation
.end field

.field private static final ADJUSTED_MONTH_LENGTHS:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<Ljava/lang/Integer;[Ljava/lang/Integer;>;"
        }
    .end annotation
.end field

.field private static final CYCLEYEAR_START_DATE:[I

.field private static final DEFAULT_CONFIG_PATH:Ljava/lang/String;

.field private static final DEFAULT_CYCLE_YEARS:[Ljava/lang/Integer;

.field private static final DEFAULT_LEAP_MONTH_DAYS:[Ljava/lang/Integer;

.field private static final DEFAULT_LEAP_MONTH_LENGTHS:[Ljava/lang/Integer;

.field private static final DEFAULT_MONTH_DAYS:[Ljava/lang/Integer;

.field private static final DEFAULT_MONTH_LENGTHS:[Ljava/lang/Integer;

.field private static final FILE_SEP:C

.field private static final LEAP_MONTH_LENGTH:[I

.field private static final LEAP_NUM_DAYS:[I

.field private static final LEAST_MAX_VALUES:[I

.field private static final MAX_VALUES:[I

.field private static final MIN_VALUES:[I

.field private static final MONTH_LENGTH:[I

.field private static final NUM_DAYS:[I

.field private static final PATH_SEP:Ljava/lang/String;

.field private static final serialVersionUID:J = -0x4846033461a5e4e4L


# instance fields
.field private final transient dayOfMonth:I

.field private final transient dayOfWeek:Lorg/threeten/bp/DayOfWeek;

.field private final transient dayOfYear:I

.field private final transient era:Lorg/threeten/bp/chrono/HijrahEra;

.field private final gregorianEpochDay:J

.field private final transient isLeapYear:Z

.field private final transient monthOfYear:I

.field private final transient yearOfEra:I


# direct methods
.method static constructor <clinit>()V
    .registers 5

    .line 136
    const/16 v0, 0xc

    new-array v0, v0, [I

    fill-array-data v0, :array_1a2

    sput-object v0, Lorg/threeten/bp/chrono/HijrahDate;->NUM_DAYS:[I

    .line 141
    const/16 v0, 0xc

    new-array v0, v0, [I

    fill-array-data v0, :array_1be

    sput-object v0, Lorg/threeten/bp/chrono/HijrahDate;->LEAP_NUM_DAYS:[I

    .line 146
    const/16 v0, 0xc

    new-array v0, v0, [I

    fill-array-data v0, :array_1da

    sput-object v0, Lorg/threeten/bp/chrono/HijrahDate;->MONTH_LENGTH:[I

    .line 151
    const/16 v0, 0xc

    new-array v0, v0, [I

    fill-array-data v0, :array_1f6

    sput-object v0, Lorg/threeten/bp/chrono/HijrahDate;->LEAP_MONTH_LENGTH:[I

    .line 168
    const/4 v0, 0x7

    new-array v0, v0, [I

    fill-array-data v0, :array_212

    sput-object v0, Lorg/threeten/bp/chrono/HijrahDate;->MIN_VALUES:[I

    .line 182
    const/4 v0, 0x7

    new-array v0, v0, [I

    fill-array-data v0, :array_224

    sput-object v0, Lorg/threeten/bp/chrono/HijrahDate;->LEAST_MAX_VALUES:[I

    .line 196
    const/4 v0, 0x7

    new-array v0, v0, [I

    fill-array-data v0, :array_236

    sput-object v0, Lorg/threeten/bp/chrono/HijrahDate;->MAX_VALUES:[I

    .line 220
    const/16 v0, 0x1e

    new-array v0, v0, [I

    fill-array-data v0, :array_248

    sput-object v0, Lorg/threeten/bp/chrono/HijrahDate;->CYCLEYEAR_START_DATE:[I

    .line 257
    sget-char v0, Ljava/io/File;->separatorChar:C

    sput-char v0, Lorg/threeten/bp/chrono/HijrahDate;->FILE_SEP:C

    .line 261
    sget-object v0, Ljava/io/File;->pathSeparator:Ljava/lang/String;

    sput-object v0, Lorg/threeten/bp/chrono/HijrahDate;->PATH_SEP:Ljava/lang/String;

    .line 269
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "org"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-char v1, Lorg/threeten/bp/chrono/HijrahDate;->FILE_SEP:C

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "threeten"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-char v1, Lorg/threeten/bp/chrono/HijrahDate;->FILE_SEP:C

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "bp"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-char v1, Lorg/threeten/bp/chrono/HijrahDate;->FILE_SEP:C

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "chrono"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lorg/threeten/bp/chrono/HijrahDate;->DEFAULT_CONFIG_PATH:Ljava/lang/String;

    .line 274
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lorg/threeten/bp/chrono/HijrahDate;->ADJUSTED_MONTH_DAYS:Ljava/util/HashMap;

    .line 279
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lorg/threeten/bp/chrono/HijrahDate;->ADJUSTED_MONTH_LENGTHS:Ljava/util/HashMap;

    .line 285
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lorg/threeten/bp/chrono/HijrahDate;->ADJUSTED_CYCLE_YEARS:Ljava/util/HashMap;

    .line 331
    sget-object v0, Lorg/threeten/bp/chrono/HijrahDate;->NUM_DAYS:[I

    array-length v0, v0

    new-array v0, v0, [Ljava/lang/Integer;

    sput-object v0, Lorg/threeten/bp/chrono/HijrahDate;->DEFAULT_MONTH_DAYS:[Ljava/lang/Integer;

    .line 332
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_9f
    sget-object v0, Lorg/threeten/bp/chrono/HijrahDate;->NUM_DAYS:[I

    array-length v0, v0

    if-ge v4, v0, :cond_b4

    .line 333
    sget-object v0, Lorg/threeten/bp/chrono/HijrahDate;->DEFAULT_MONTH_DAYS:[Ljava/lang/Integer;

    new-instance v1, Ljava/lang/Integer;

    sget-object v2, Lorg/threeten/bp/chrono/HijrahDate;->NUM_DAYS:[I

    aget v2, v2, v4

    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    aput-object v1, v0, v4

    .line 332
    add-int/lit8 v4, v4, 0x1

    goto :goto_9f

    .line 336
    .end local v4    # "i":I
    :cond_b4
    sget-object v0, Lorg/threeten/bp/chrono/HijrahDate;->LEAP_NUM_DAYS:[I

    array-length v0, v0

    new-array v0, v0, [Ljava/lang/Integer;

    sput-object v0, Lorg/threeten/bp/chrono/HijrahDate;->DEFAULT_LEAP_MONTH_DAYS:[Ljava/lang/Integer;

    .line 337
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_bc
    sget-object v0, Lorg/threeten/bp/chrono/HijrahDate;->LEAP_NUM_DAYS:[I

    array-length v0, v0

    if-ge v4, v0, :cond_d1

    .line 338
    sget-object v0, Lorg/threeten/bp/chrono/HijrahDate;->DEFAULT_LEAP_MONTH_DAYS:[Ljava/lang/Integer;

    new-instance v1, Ljava/lang/Integer;

    sget-object v2, Lorg/threeten/bp/chrono/HijrahDate;->LEAP_NUM_DAYS:[I

    aget v2, v2, v4

    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    aput-object v1, v0, v4

    .line 337
    add-int/lit8 v4, v4, 0x1

    goto :goto_bc

    .line 341
    .end local v4    # "i":I
    :cond_d1
    sget-object v0, Lorg/threeten/bp/chrono/HijrahDate;->MONTH_LENGTH:[I

    array-length v0, v0

    new-array v0, v0, [Ljava/lang/Integer;

    sput-object v0, Lorg/threeten/bp/chrono/HijrahDate;->DEFAULT_MONTH_LENGTHS:[Ljava/lang/Integer;

    .line 342
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_d9
    sget-object v0, Lorg/threeten/bp/chrono/HijrahDate;->MONTH_LENGTH:[I

    array-length v0, v0

    if-ge v4, v0, :cond_ee

    .line 343
    sget-object v0, Lorg/threeten/bp/chrono/HijrahDate;->DEFAULT_MONTH_LENGTHS:[Ljava/lang/Integer;

    new-instance v1, Ljava/lang/Integer;

    sget-object v2, Lorg/threeten/bp/chrono/HijrahDate;->MONTH_LENGTH:[I

    aget v2, v2, v4

    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    aput-object v1, v0, v4

    .line 342
    add-int/lit8 v4, v4, 0x1

    goto :goto_d9

    .line 346
    .end local v4    # "i":I
    :cond_ee
    sget-object v0, Lorg/threeten/bp/chrono/HijrahDate;->LEAP_MONTH_LENGTH:[I

    array-length v0, v0

    new-array v0, v0, [Ljava/lang/Integer;

    sput-object v0, Lorg/threeten/bp/chrono/HijrahDate;->DEFAULT_LEAP_MONTH_LENGTHS:[Ljava/lang/Integer;

    .line 347
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_f6
    sget-object v0, Lorg/threeten/bp/chrono/HijrahDate;->LEAP_MONTH_LENGTH:[I

    array-length v0, v0

    if-ge v4, v0, :cond_10b

    .line 348
    sget-object v0, Lorg/threeten/bp/chrono/HijrahDate;->DEFAULT_LEAP_MONTH_LENGTHS:[Ljava/lang/Integer;

    new-instance v1, Ljava/lang/Integer;

    sget-object v2, Lorg/threeten/bp/chrono/HijrahDate;->LEAP_MONTH_LENGTH:[I

    aget v2, v2, v4

    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    aput-object v1, v0, v4

    .line 347
    add-int/lit8 v4, v4, 0x1

    goto :goto_f6

    .line 351
    .end local v4    # "i":I
    :cond_10b
    sget-object v0, Lorg/threeten/bp/chrono/HijrahDate;->CYCLEYEAR_START_DATE:[I

    array-length v0, v0

    new-array v0, v0, [Ljava/lang/Integer;

    sput-object v0, Lorg/threeten/bp/chrono/HijrahDate;->DEFAULT_CYCLE_YEARS:[Ljava/lang/Integer;

    .line 352
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_113
    sget-object v0, Lorg/threeten/bp/chrono/HijrahDate;->CYCLEYEAR_START_DATE:[I

    array-length v0, v0

    if-ge v4, v0, :cond_128

    .line 353
    sget-object v0, Lorg/threeten/bp/chrono/HijrahDate;->DEFAULT_CYCLE_YEARS:[Ljava/lang/Integer;

    new-instance v1, Ljava/lang/Integer;

    sget-object v2, Lorg/threeten/bp/chrono/HijrahDate;->CYCLEYEAR_START_DATE:[I

    aget v2, v2, v4

    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    aput-object v1, v0, v4

    .line 352
    add-int/lit8 v4, v4, 0x1

    goto :goto_113

    .line 356
    .end local v4    # "i":I
    :cond_128
    const/16 v0, 0x14e

    new-array v0, v0, [Ljava/lang/Long;

    sput-object v0, Lorg/threeten/bp/chrono/HijrahDate;->ADJUSTED_CYCLES:[Ljava/lang/Long;

    .line 357
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_12f
    sget-object v0, Lorg/threeten/bp/chrono/HijrahDate;->ADJUSTED_CYCLES:[Ljava/lang/Long;

    array-length v0, v0

    if-ge v4, v0, :cond_143

    .line 358
    sget-object v0, Lorg/threeten/bp/chrono/HijrahDate;->ADJUSTED_CYCLES:[Ljava/lang/Long;

    new-instance v1, Ljava/lang/Long;

    mul-int/lit16 v2, v4, 0x2987

    int-to-long v2, v2

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    aput-object v1, v0, v4

    .line 357
    add-int/lit8 v4, v4, 0x1

    goto :goto_12f

    .line 361
    .end local v4    # "i":I
    :cond_143
    sget-object v0, Lorg/threeten/bp/chrono/HijrahDate;->MIN_VALUES:[I

    array-length v0, v0

    new-array v0, v0, [Ljava/lang/Integer;

    sput-object v0, Lorg/threeten/bp/chrono/HijrahDate;->ADJUSTED_MIN_VALUES:[Ljava/lang/Integer;

    .line 362
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_14b
    sget-object v0, Lorg/threeten/bp/chrono/HijrahDate;->MIN_VALUES:[I

    array-length v0, v0

    if-ge v4, v0, :cond_160

    .line 363
    sget-object v0, Lorg/threeten/bp/chrono/HijrahDate;->ADJUSTED_MIN_VALUES:[Ljava/lang/Integer;

    new-instance v1, Ljava/lang/Integer;

    sget-object v2, Lorg/threeten/bp/chrono/HijrahDate;->MIN_VALUES:[I

    aget v2, v2, v4

    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    aput-object v1, v0, v4

    .line 362
    add-int/lit8 v4, v4, 0x1

    goto :goto_14b

    .line 365
    .end local v4    # "i":I
    :cond_160
    sget-object v0, Lorg/threeten/bp/chrono/HijrahDate;->LEAST_MAX_VALUES:[I

    array-length v0, v0

    new-array v0, v0, [Ljava/lang/Integer;

    sput-object v0, Lorg/threeten/bp/chrono/HijrahDate;->ADJUSTED_LEAST_MAX_VALUES:[Ljava/lang/Integer;

    .line 366
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_168
    sget-object v0, Lorg/threeten/bp/chrono/HijrahDate;->LEAST_MAX_VALUES:[I

    array-length v0, v0

    if-ge v4, v0, :cond_17d

    .line 367
    sget-object v0, Lorg/threeten/bp/chrono/HijrahDate;->ADJUSTED_LEAST_MAX_VALUES:[Ljava/lang/Integer;

    new-instance v1, Ljava/lang/Integer;

    sget-object v2, Lorg/threeten/bp/chrono/HijrahDate;->LEAST_MAX_VALUES:[I

    aget v2, v2, v4

    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    aput-object v1, v0, v4

    .line 366
    add-int/lit8 v4, v4, 0x1

    goto :goto_168

    .line 369
    .end local v4    # "i":I
    :cond_17d
    sget-object v0, Lorg/threeten/bp/chrono/HijrahDate;->MAX_VALUES:[I

    array-length v0, v0

    new-array v0, v0, [Ljava/lang/Integer;

    sput-object v0, Lorg/threeten/bp/chrono/HijrahDate;->ADJUSTED_MAX_VALUES:[Ljava/lang/Integer;

    .line 370
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_185
    sget-object v0, Lorg/threeten/bp/chrono/HijrahDate;->MAX_VALUES:[I

    array-length v0, v0

    if-ge v4, v0, :cond_19a

    .line 371
    sget-object v0, Lorg/threeten/bp/chrono/HijrahDate;->ADJUSTED_MAX_VALUES:[Ljava/lang/Integer;

    new-instance v1, Ljava/lang/Integer;

    sget-object v2, Lorg/threeten/bp/chrono/HijrahDate;->MAX_VALUES:[I

    aget v2, v2, v4

    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    aput-object v1, v0, v4

    .line 370
    add-int/lit8 v4, v4, 0x1

    goto :goto_185

    .line 374
    .end local v4    # "i":I
    :cond_19a
    :try_start_19a
    invoke-static {}, Lorg/threeten/bp/chrono/HijrahDate;->readDeviationConfig()V
    :try_end_19d
    .catch Ljava/io/IOException; {:try_start_19a .. :try_end_19d} :catch_19e
    .catch Ljava/text/ParseException; {:try_start_19a .. :try_end_19d} :catch_1a0

    .line 381
    goto :goto_1a1

    .line 375
    :catch_19e
    move-exception v4

    .line 381
    goto :goto_1a1

    .line 378
    :catch_1a0
    move-exception v4

    .line 382
    :goto_1a1
    return-void

    :array_1a2
    .array-data 4
        0x0
        0x1e
        0x3b
        0x59
        0x76
        0x94
        0xb1
        0xcf
        0xec
        0x10a
        0x127
        0x145
    .end array-data

    :array_1be
    .array-data 4
        0x0
        0x1e
        0x3b
        0x59
        0x76
        0x94
        0xb1
        0xcf
        0xec
        0x10a
        0x127
        0x145
    .end array-data

    :array_1da
    .array-data 4
        0x1e
        0x1d
        0x1e
        0x1d
        0x1e
        0x1d
        0x1e
        0x1d
        0x1e
        0x1d
        0x1e
        0x1d
    .end array-data

    :array_1f6
    .array-data 4
        0x1e
        0x1d
        0x1e
        0x1d
        0x1e
        0x1d
        0x1e
        0x1d
        0x1e
        0x1d
        0x1e
        0x1e
    .end array-data

    :array_212
    .array-data 4
        0x0
        0x1
        0x0
        0x1
        0x0
        0x1
        0x1
    .end array-data

    :array_224
    .array-data 4
        0x1
        0x270f
        0xb
        0x33
        0x5
        0x1d
        0x162
    .end array-data

    :array_236
    .array-data 4
        0x1
        0x270f
        0xb
        0x34
        0x6
        0x1e
        0x163
    .end array-data

    :array_248
    .array-data 4
        0x0
        0x162
        0x2c5
        0x427
        0x589
        0x6ec
        0x84e
        0x9b1
        0xb13
        0xc75
        0xdd8
        0xf3a
        0x109c
        0x11ff
        0x1361
        0x14c3
        0x1626
        0x1788
        0x18eb
        0x1a4d
        0x1baf
        0x1d12
        0x1e74
        0x1fd6
        0x2139
        0x229b
        0x23fe
        0x2560
        0x26c2
        0x2825
    .end array-data
.end method

.method private constructor <init>(J)V
    .registers 6
    .param p1, "gregorianDay"    # J

    .line 588
    invoke-direct {p0}, Lorg/threeten/bp/chrono/ChronoDateImpl;-><init>()V

    .line 589
    invoke-static {p1, p2}, Lorg/threeten/bp/chrono/HijrahDate;->getHijrahDateInfo(J)[I

    move-result-object v2

    .line 591
    .local v2, "dateInfo":[I
    const/4 v0, 0x1

    aget v0, v2, v0

    invoke-static {v0}, Lorg/threeten/bp/chrono/HijrahDate;->checkValidYearOfEra(I)V

    .line 592
    const/4 v0, 0x2

    aget v0, v2, v0

    invoke-static {v0}, Lorg/threeten/bp/chrono/HijrahDate;->checkValidMonth(I)V

    .line 593
    const/4 v0, 0x3

    aget v0, v2, v0

    invoke-static {v0}, Lorg/threeten/bp/chrono/HijrahDate;->checkValidDayOfMonth(I)V

    .line 594
    const/4 v0, 0x4

    aget v0, v2, v0

    invoke-static {v0}, Lorg/threeten/bp/chrono/HijrahDate;->checkValidDayOfYear(I)V

    .line 596
    const/4 v0, 0x0

    aget v0, v2, v0

    invoke-static {v0}, Lorg/threeten/bp/chrono/HijrahEra;->of(I)Lorg/threeten/bp/chrono/HijrahEra;

    move-result-object v0

    iput-object v0, p0, Lorg/threeten/bp/chrono/HijrahDate;->era:Lorg/threeten/bp/chrono/HijrahEra;

    .line 597
    const/4 v0, 0x1

    aget v0, v2, v0

    iput v0, p0, Lorg/threeten/bp/chrono/HijrahDate;->yearOfEra:I

    .line 598
    const/4 v0, 0x2

    aget v0, v2, v0

    iput v0, p0, Lorg/threeten/bp/chrono/HijrahDate;->monthOfYear:I

    .line 599
    const/4 v0, 0x3

    aget v0, v2, v0

    iput v0, p0, Lorg/threeten/bp/chrono/HijrahDate;->dayOfMonth:I

    .line 600
    const/4 v0, 0x4

    aget v0, v2, v0

    iput v0, p0, Lorg/threeten/bp/chrono/HijrahDate;->dayOfYear:I

    .line 601
    const/4 v0, 0x5

    aget v0, v2, v0

    invoke-static {v0}, Lorg/threeten/bp/DayOfWeek;->of(I)Lorg/threeten/bp/DayOfWeek;

    move-result-object v0

    iput-object v0, p0, Lorg/threeten/bp/chrono/HijrahDate;->dayOfWeek:Lorg/threeten/bp/DayOfWeek;

    .line 602
    iput-wide p1, p0, Lorg/threeten/bp/chrono/HijrahDate;->gregorianEpochDay:J

    .line 603
    iget v0, p0, Lorg/threeten/bp/chrono/HijrahDate;->yearOfEra:I

    int-to-long v0, v0

    invoke-static {v0, v1}, Lorg/threeten/bp/chrono/HijrahDate;->isLeapYear(J)Z

    move-result v0

    iput-boolean v0, p0, Lorg/threeten/bp/chrono/HijrahDate;->isLeapYear:Z

    .line 604
    return-void
.end method

.method private static addDeviationAsHijrah(IIIII)V
    .registers 33
    .param p0, "startYear"    # I
    .param p1, "startMonth"    # I
    .param p2, "endYear"    # I
    .param p3, "endMonth"    # I
    .param p4, "offset"    # I

    .line 1236
    move/from16 v0, p0

    const/4 v1, 0x1

    if-ge v0, v1, :cond_d

    .line 1237
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "startYear < 1"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1239
    :cond_d
    move/from16 v0, p2

    const/4 v1, 0x1

    if-ge v0, v1, :cond_1a

    .line 1240
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "endYear < 1"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1242
    :cond_1a
    if-ltz p1, :cond_22

    move/from16 v0, p1

    const/16 v1, 0xb

    if-le v0, v1, :cond_2a

    .line 1243
    :cond_22
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "startMonth < 0 || startMonth > 11"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1246
    :cond_2a
    if-ltz p3, :cond_32

    move/from16 v0, p3

    const/16 v1, 0xb

    if-le v0, v1, :cond_3a

    .line 1247
    :cond_32
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "endMonth < 0 || endMonth > 11"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1249
    :cond_3a
    move/from16 v0, p2

    const/16 v1, 0x270f

    if-le v0, v1, :cond_48

    .line 1250
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "endYear > 9999"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1252
    :cond_48
    move/from16 v0, p2

    move/from16 v1, p0

    if-ge v0, v1, :cond_56

    .line 1253
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "startYear > endYear"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1255
    :cond_56
    move/from16 v0, p2

    move/from16 v1, p0

    if-ne v0, v1, :cond_6a

    move/from16 v0, p3

    move/from16 v1, p1

    if-ge v0, v1, :cond_6a

    .line 1256
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "startYear == endYear && endMonth < startMonth"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1261
    :cond_6a
    move/from16 v0, p0

    int-to-long v0, v0

    invoke-static {v0, v1}, Lorg/threeten/bp/chrono/HijrahDate;->isLeapYear(J)Z

    move-result v6

    .line 1264
    .local v6, "isStartYLeap":Z
    sget-object v0, Lorg/threeten/bp/chrono/HijrahDate;->ADJUSTED_MONTH_DAYS:Ljava/util/HashMap;

    new-instance v1, Ljava/lang/Integer;

    move/from16 v2, p0

    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, [Ljava/lang/Integer;

    .line 1266
    .local v7, "orgStartMonthNums":[Ljava/lang/Integer;
    if-nez v7, :cond_b8

    .line 1267
    if-eqz v6, :cond_9f

    .line 1268
    sget-object v0, Lorg/threeten/bp/chrono/HijrahDate;->LEAP_NUM_DAYS:[I

    array-length v0, v0

    new-array v7, v0, [Ljava/lang/Integer;

    .line 1269
    const/4 v8, 0x0

    .local v8, "l":I
    :goto_8b
    sget-object v0, Lorg/threeten/bp/chrono/HijrahDate;->LEAP_NUM_DAYS:[I

    array-length v0, v0

    if-ge v8, v0, :cond_9e

    .line 1270
    new-instance v0, Ljava/lang/Integer;

    sget-object v1, Lorg/threeten/bp/chrono/HijrahDate;->LEAP_NUM_DAYS:[I

    aget v1, v1, v8

    invoke-direct {v0, v1}, Ljava/lang/Integer;-><init>(I)V

    aput-object v0, v7, v8

    .line 1269
    add-int/lit8 v8, v8, 0x1

    goto :goto_8b

    .end local v8    # "l":I
    :cond_9e
    goto :goto_b8

    .line 1273
    :cond_9f
    sget-object v0, Lorg/threeten/bp/chrono/HijrahDate;->NUM_DAYS:[I

    array-length v0, v0

    new-array v7, v0, [Ljava/lang/Integer;

    .line 1274
    const/4 v8, 0x0

    .local v8, "l":I
    :goto_a5
    sget-object v0, Lorg/threeten/bp/chrono/HijrahDate;->NUM_DAYS:[I

    array-length v0, v0

    if-ge v8, v0, :cond_b8

    .line 1275
    new-instance v0, Ljava/lang/Integer;

    sget-object v1, Lorg/threeten/bp/chrono/HijrahDate;->NUM_DAYS:[I

    aget v1, v1, v8

    invoke-direct {v0, v1}, Ljava/lang/Integer;-><init>(I)V

    aput-object v0, v7, v8

    .line 1274
    add-int/lit8 v8, v8, 0x1

    goto :goto_a5

    .line 1280
    .end local v8    # "l":I
    :cond_b8
    :goto_b8
    array-length v0, v7

    new-array v8, v0, [Ljava/lang/Integer;

    .line 1282
    .local v8, "newStartMonthNums":[Ljava/lang/Integer;
    const/4 v9, 0x0

    .local v9, "month":I
    :goto_bc
    const/16 v0, 0xc

    if-ge v9, v0, :cond_e4

    .line 1283
    move/from16 v0, p1

    if-le v9, v0, :cond_d4

    .line 1284
    new-instance v0, Ljava/lang/Integer;

    aget-object v1, v7, v9

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    sub-int v1, v1, p4

    invoke-direct {v0, v1}, Ljava/lang/Integer;-><init>(I)V

    aput-object v0, v8, v9

    goto :goto_e1

    .line 1288
    :cond_d4
    new-instance v0, Ljava/lang/Integer;

    aget-object v1, v7, v9

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/lang/Integer;-><init>(I)V

    aput-object v0, v8, v9

    .line 1282
    :goto_e1
    add-int/lit8 v9, v9, 0x1

    goto :goto_bc

    .line 1293
    .end local v9    # "month":I
    :cond_e4
    sget-object v0, Lorg/threeten/bp/chrono/HijrahDate;->ADJUSTED_MONTH_DAYS:Ljava/util/HashMap;

    new-instance v1, Ljava/lang/Integer;

    move/from16 v2, p0

    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0, v1, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1297
    sget-object v0, Lorg/threeten/bp/chrono/HijrahDate;->ADJUSTED_MONTH_LENGTHS:Ljava/util/HashMap;

    new-instance v1, Ljava/lang/Integer;

    move/from16 v2, p0

    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, [Ljava/lang/Integer;

    .line 1299
    .local v9, "orgStartMonthLengths":[Ljava/lang/Integer;
    if-nez v9, :cond_137

    .line 1300
    if-eqz v6, :cond_11e

    .line 1301
    sget-object v0, Lorg/threeten/bp/chrono/HijrahDate;->LEAP_MONTH_LENGTH:[I

    array-length v0, v0

    new-array v9, v0, [Ljava/lang/Integer;

    .line 1302
    const/4 v10, 0x0

    .local v10, "l":I
    :goto_10a
    sget-object v0, Lorg/threeten/bp/chrono/HijrahDate;->LEAP_MONTH_LENGTH:[I

    array-length v0, v0

    if-ge v10, v0, :cond_11d

    .line 1303
    new-instance v0, Ljava/lang/Integer;

    sget-object v1, Lorg/threeten/bp/chrono/HijrahDate;->LEAP_MONTH_LENGTH:[I

    aget v1, v1, v10

    invoke-direct {v0, v1}, Ljava/lang/Integer;-><init>(I)V

    aput-object v0, v9, v10

    .line 1302
    add-int/lit8 v10, v10, 0x1

    goto :goto_10a

    .end local v10    # "l":I
    :cond_11d
    goto :goto_137

    .line 1306
    :cond_11e
    sget-object v0, Lorg/threeten/bp/chrono/HijrahDate;->MONTH_LENGTH:[I

    array-length v0, v0

    new-array v9, v0, [Ljava/lang/Integer;

    .line 1307
    const/4 v10, 0x0

    .local v10, "l":I
    :goto_124
    sget-object v0, Lorg/threeten/bp/chrono/HijrahDate;->MONTH_LENGTH:[I

    array-length v0, v0

    if-ge v10, v0, :cond_137

    .line 1308
    new-instance v0, Ljava/lang/Integer;

    sget-object v1, Lorg/threeten/bp/chrono/HijrahDate;->MONTH_LENGTH:[I

    aget v1, v1, v10

    invoke-direct {v0, v1}, Ljava/lang/Integer;-><init>(I)V

    aput-object v0, v9, v10

    .line 1307
    add-int/lit8 v10, v10, 0x1

    goto :goto_124

    .line 1313
    .end local v10    # "l":I
    :cond_137
    :goto_137
    array-length v0, v9

    new-array v10, v0, [Ljava/lang/Integer;

    .line 1315
    .local v10, "newStartMonthLengths":[Ljava/lang/Integer;
    const/4 v11, 0x0

    .local v11, "month":I
    :goto_13b
    const/16 v0, 0xc

    if-ge v11, v0, :cond_163

    .line 1316
    move/from16 v0, p1

    if-ne v11, v0, :cond_153

    .line 1317
    new-instance v0, Ljava/lang/Integer;

    aget-object v1, v9, v11

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    sub-int v1, v1, p4

    invoke-direct {v0, v1}, Ljava/lang/Integer;-><init>(I)V

    aput-object v0, v10, v11

    goto :goto_160

    .line 1320
    :cond_153
    new-instance v0, Ljava/lang/Integer;

    aget-object v1, v9, v11

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/lang/Integer;-><init>(I)V

    aput-object v0, v10, v11

    .line 1315
    :goto_160
    add-int/lit8 v11, v11, 0x1

    goto :goto_13b

    .line 1325
    .end local v11    # "month":I
    :cond_163
    sget-object v0, Lorg/threeten/bp/chrono/HijrahDate;->ADJUSTED_MONTH_LENGTHS:Ljava/util/HashMap;

    new-instance v1, Ljava/lang/Integer;

    move/from16 v2, p0

    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0, v1, v10}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1327
    move/from16 v0, p0

    move/from16 v1, p2

    if-eq v0, v1, :cond_275

    .line 1330
    add-int/lit8 v0, p0, -0x1

    div-int/lit8 v11, v0, 0x1e

    .line 1331
    .local v11, "sCycleNumber":I
    add-int/lit8 v0, p0, -0x1

    rem-int/lit8 v12, v0, 0x1e

    .line 1332
    .local v12, "sYearInCycle":I
    sget-object v0, Lorg/threeten/bp/chrono/HijrahDate;->ADJUSTED_CYCLE_YEARS:Ljava/util/HashMap;

    new-instance v1, Ljava/lang/Integer;

    invoke-direct {v1, v11}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v13, v0

    check-cast v13, [Ljava/lang/Integer;

    .line 1334
    .local v13, "startCycles":[Ljava/lang/Integer;
    if-nez v13, :cond_1a4

    .line 1335
    sget-object v0, Lorg/threeten/bp/chrono/HijrahDate;->CYCLEYEAR_START_DATE:[I

    array-length v0, v0

    new-array v13, v0, [Ljava/lang/Integer;

    .line 1336
    const/4 v14, 0x0

    .local v14, "j":I
    :goto_193
    array-length v0, v13

    if-ge v14, v0, :cond_1a4

    .line 1337
    new-instance v0, Ljava/lang/Integer;

    sget-object v1, Lorg/threeten/bp/chrono/HijrahDate;->CYCLEYEAR_START_DATE:[I

    aget v1, v1, v14

    invoke-direct {v0, v1}, Ljava/lang/Integer;-><init>(I)V

    aput-object v0, v13, v14

    .line 1336
    add-int/lit8 v14, v14, 0x1

    goto :goto_193

    .line 1341
    .end local v14    # "j":I
    :cond_1a4
    add-int/lit8 v14, v12, 0x1

    .local v14, "j":I
    :goto_1a6
    sget-object v0, Lorg/threeten/bp/chrono/HijrahDate;->CYCLEYEAR_START_DATE:[I

    array-length v0, v0

    if-ge v14, v0, :cond_1bd

    .line 1342
    new-instance v0, Ljava/lang/Integer;

    aget-object v1, v13, v14

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    sub-int v1, v1, p4

    invoke-direct {v0, v1}, Ljava/lang/Integer;-><init>(I)V

    aput-object v0, v13, v14

    .line 1341
    add-int/lit8 v14, v14, 0x1

    goto :goto_1a6

    .line 1346
    .end local v14    # "j":I
    :cond_1bd
    sget-object v0, Lorg/threeten/bp/chrono/HijrahDate;->ADJUSTED_CYCLE_YEARS:Ljava/util/HashMap;

    new-instance v1, Ljava/lang/Integer;

    invoke-direct {v1, v11}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0, v1, v13}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1348
    add-int/lit8 v0, p0, -0x1

    div-int/lit8 v14, v0, 0x1e

    .line 1349
    .local v14, "sYearInMaxY":I
    add-int/lit8 v0, p2, -0x1

    div-int/lit8 v15, v0, 0x1e

    .line 1351
    .local v15, "sEndInMaxY":I
    if-eq v14, v15, :cond_213

    .line 1356
    add-int/lit8 v16, v14, 0x1

    .local v16, "j":I
    :goto_1d3
    sget-object v0, Lorg/threeten/bp/chrono/HijrahDate;->ADJUSTED_CYCLES:[Ljava/lang/Long;

    array-length v0, v0

    move/from16 v1, v16

    if-ge v1, v0, :cond_1f2

    .line 1357
    sget-object v0, Lorg/threeten/bp/chrono/HijrahDate;->ADJUSTED_CYCLES:[Ljava/lang/Long;

    new-instance v1, Ljava/lang/Long;

    sget-object v2, Lorg/threeten/bp/chrono/HijrahDate;->ADJUSTED_CYCLES:[Ljava/lang/Long;

    aget-object v2, v2, v16

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    move/from16 v4, p4

    int-to-long v4, v4

    sub-long/2addr v2, v4

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    aput-object v1, v0, v16

    .line 1356
    add-int/lit8 v16, v16, 0x1

    goto :goto_1d3

    .line 1362
    .end local v16    # "j":I
    :cond_1f2
    add-int/lit8 v16, v15, 0x1

    .local v16, "j":I
    :goto_1f4
    sget-object v0, Lorg/threeten/bp/chrono/HijrahDate;->ADJUSTED_CYCLES:[Ljava/lang/Long;

    array-length v0, v0

    move/from16 v1, v16

    if-ge v1, v0, :cond_213

    .line 1363
    sget-object v0, Lorg/threeten/bp/chrono/HijrahDate;->ADJUSTED_CYCLES:[Ljava/lang/Long;

    new-instance v1, Ljava/lang/Long;

    sget-object v2, Lorg/threeten/bp/chrono/HijrahDate;->ADJUSTED_CYCLES:[Ljava/lang/Long;

    aget-object v2, v2, v16

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    move/from16 v4, p4

    int-to-long v4, v4

    add-long/2addr v2, v4

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    aput-object v1, v0, v16

    .line 1362
    add-int/lit8 v16, v16, 0x1

    goto :goto_1f4

    .line 1369
    .end local v16    # "j":I
    :cond_213
    add-int/lit8 v0, p2, -0x1

    div-int/lit8 v16, v0, 0x1e

    .line 1370
    .local v16, "eCycleNumber":I
    add-int/lit8 v0, p2, -0x1

    rem-int/lit8 v17, v0, 0x1e

    .line 1371
    .local v17, "sEndInCycle":I
    sget-object v0, Lorg/threeten/bp/chrono/HijrahDate;->ADJUSTED_CYCLE_YEARS:Ljava/util/HashMap;

    new-instance v1, Ljava/lang/Integer;

    move/from16 v2, v16

    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object/from16 v18, v0

    check-cast v18, [Ljava/lang/Integer;

    .line 1373
    .local v18, "endCycles":[Ljava/lang/Integer;
    if-nez v18, :cond_24c

    .line 1374
    sget-object v0, Lorg/threeten/bp/chrono/HijrahDate;->CYCLEYEAR_START_DATE:[I

    array-length v0, v0

    new-array v1, v0, [Ljava/lang/Integer;

    move-object/from16 v18, v1

    .line 1375
    const/16 v19, 0x0

    .local v19, "j":I
    :goto_237
    move-object/from16 v0, v18

    array-length v0, v0

    move/from16 v1, v19

    if-ge v1, v0, :cond_24c

    .line 1376
    new-instance v0, Ljava/lang/Integer;

    sget-object v1, Lorg/threeten/bp/chrono/HijrahDate;->CYCLEYEAR_START_DATE:[I

    aget v1, v1, v19

    invoke-direct {v0, v1}, Ljava/lang/Integer;-><init>(I)V

    aput-object v0, v18, v19

    .line 1375
    add-int/lit8 v19, v19, 0x1

    goto :goto_237

    .line 1379
    .end local v19    # "j":I
    :cond_24c
    add-int/lit8 v19, v17, 0x1

    .local v19, "j":I
    :goto_24e
    sget-object v0, Lorg/threeten/bp/chrono/HijrahDate;->CYCLEYEAR_START_DATE:[I

    array-length v0, v0

    move/from16 v1, v19

    if-ge v1, v0, :cond_267

    .line 1380
    new-instance v0, Ljava/lang/Integer;

    aget-object v1, v18, v19

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    add-int v1, v1, p4

    invoke-direct {v0, v1}, Ljava/lang/Integer;-><init>(I)V

    aput-object v0, v18, v19

    .line 1379
    add-int/lit8 v19, v19, 0x1

    goto :goto_24e

    .line 1382
    .end local v19    # "j":I
    :cond_267
    sget-object v0, Lorg/threeten/bp/chrono/HijrahDate;->ADJUSTED_CYCLE_YEARS:Ljava/util/HashMap;

    new-instance v1, Ljava/lang/Integer;

    move/from16 v2, v16

    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    move-object/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1386
    .end local v11    # "sCycleNumber":I
    .end local v12    # "sYearInCycle":I
    .end local v13    # "startCycles":[Ljava/lang/Integer;
    .end local v14    # "sYearInMaxY":I
    .end local v15    # "sEndInMaxY":I
    .end local v16    # "eCycleNumber":I
    .end local v17    # "sEndInCycle":I
    .end local v18    # "endCycles":[Ljava/lang/Integer;
    :cond_275
    move/from16 v0, p2

    int-to-long v0, v0

    invoke-static {v0, v1}, Lorg/threeten/bp/chrono/HijrahDate;->isLeapYear(J)Z

    move-result v11

    .line 1388
    .local v11, "isEndYLeap":Z
    sget-object v0, Lorg/threeten/bp/chrono/HijrahDate;->ADJUSTED_MONTH_DAYS:Ljava/util/HashMap;

    new-instance v1, Ljava/lang/Integer;

    move/from16 v2, p2

    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v12, v0

    check-cast v12, [Ljava/lang/Integer;

    .line 1390
    .local v12, "orgEndMonthDays":[Ljava/lang/Integer;
    if-nez v12, :cond_2c3

    .line 1391
    if-eqz v11, :cond_2aa

    .line 1392
    sget-object v0, Lorg/threeten/bp/chrono/HijrahDate;->LEAP_NUM_DAYS:[I

    array-length v0, v0

    new-array v12, v0, [Ljava/lang/Integer;

    .line 1393
    const/4 v13, 0x0

    .local v13, "l":I
    :goto_296
    sget-object v0, Lorg/threeten/bp/chrono/HijrahDate;->LEAP_NUM_DAYS:[I

    array-length v0, v0

    if-ge v13, v0, :cond_2a9

    .line 1394
    new-instance v0, Ljava/lang/Integer;

    sget-object v1, Lorg/threeten/bp/chrono/HijrahDate;->LEAP_NUM_DAYS:[I

    aget v1, v1, v13

    invoke-direct {v0, v1}, Ljava/lang/Integer;-><init>(I)V

    aput-object v0, v12, v13

    .line 1393
    add-int/lit8 v13, v13, 0x1

    goto :goto_296

    .end local v13    # "l":I
    :cond_2a9
    goto :goto_2c3

    .line 1397
    :cond_2aa
    sget-object v0, Lorg/threeten/bp/chrono/HijrahDate;->NUM_DAYS:[I

    array-length v0, v0

    new-array v12, v0, [Ljava/lang/Integer;

    .line 1398
    const/4 v13, 0x0

    .local v13, "l":I
    :goto_2b0
    sget-object v0, Lorg/threeten/bp/chrono/HijrahDate;->NUM_DAYS:[I

    array-length v0, v0

    if-ge v13, v0, :cond_2c3

    .line 1399
    new-instance v0, Ljava/lang/Integer;

    sget-object v1, Lorg/threeten/bp/chrono/HijrahDate;->NUM_DAYS:[I

    aget v1, v1, v13

    invoke-direct {v0, v1}, Ljava/lang/Integer;-><init>(I)V

    aput-object v0, v12, v13

    .line 1398
    add-int/lit8 v13, v13, 0x1

    goto :goto_2b0

    .line 1404
    .end local v13    # "l":I
    :cond_2c3
    :goto_2c3
    array-length v0, v12

    new-array v13, v0, [Ljava/lang/Integer;

    .line 1406
    .local v13, "newEndMonthDays":[Ljava/lang/Integer;
    const/4 v14, 0x0

    .local v14, "month":I
    :goto_2c7
    const/16 v0, 0xc

    if-ge v14, v0, :cond_2ef

    .line 1407
    move/from16 v0, p3

    if-le v14, v0, :cond_2df

    .line 1408
    new-instance v0, Ljava/lang/Integer;

    aget-object v1, v12, v14

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    add-int v1, v1, p4

    invoke-direct {v0, v1}, Ljava/lang/Integer;-><init>(I)V

    aput-object v0, v13, v14

    goto :goto_2ec

    .line 1412
    :cond_2df
    new-instance v0, Ljava/lang/Integer;

    aget-object v1, v12, v14

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/lang/Integer;-><init>(I)V

    aput-object v0, v13, v14

    .line 1406
    :goto_2ec
    add-int/lit8 v14, v14, 0x1

    goto :goto_2c7

    .line 1417
    .end local v14    # "month":I
    :cond_2ef
    sget-object v0, Lorg/threeten/bp/chrono/HijrahDate;->ADJUSTED_MONTH_DAYS:Ljava/util/HashMap;

    new-instance v1, Ljava/lang/Integer;

    move/from16 v2, p2

    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0, v1, v13}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1420
    sget-object v0, Lorg/threeten/bp/chrono/HijrahDate;->ADJUSTED_MONTH_LENGTHS:Ljava/util/HashMap;

    new-instance v1, Ljava/lang/Integer;

    move/from16 v2, p2

    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v14, v0

    check-cast v14, [Ljava/lang/Integer;

    .line 1423
    .local v14, "orgEndMonthLengths":[Ljava/lang/Integer;
    if-nez v14, :cond_342

    .line 1424
    if-eqz v11, :cond_329

    .line 1425
    sget-object v0, Lorg/threeten/bp/chrono/HijrahDate;->LEAP_MONTH_LENGTH:[I

    array-length v0, v0

    new-array v14, v0, [Ljava/lang/Integer;

    .line 1426
    const/4 v15, 0x0

    .local v15, "l":I
    :goto_315
    sget-object v0, Lorg/threeten/bp/chrono/HijrahDate;->LEAP_MONTH_LENGTH:[I

    array-length v0, v0

    if-ge v15, v0, :cond_328

    .line 1427
    new-instance v0, Ljava/lang/Integer;

    sget-object v1, Lorg/threeten/bp/chrono/HijrahDate;->LEAP_MONTH_LENGTH:[I

    aget v1, v1, v15

    invoke-direct {v0, v1}, Ljava/lang/Integer;-><init>(I)V

    aput-object v0, v14, v15

    .line 1426
    add-int/lit8 v15, v15, 0x1

    goto :goto_315

    .end local v15    # "l":I
    :cond_328
    goto :goto_342

    .line 1430
    :cond_329
    sget-object v0, Lorg/threeten/bp/chrono/HijrahDate;->MONTH_LENGTH:[I

    array-length v0, v0

    new-array v14, v0, [Ljava/lang/Integer;

    .line 1431
    const/4 v15, 0x0

    .local v15, "l":I
    :goto_32f
    sget-object v0, Lorg/threeten/bp/chrono/HijrahDate;->MONTH_LENGTH:[I

    array-length v0, v0

    if-ge v15, v0, :cond_342

    .line 1432
    new-instance v0, Ljava/lang/Integer;

    sget-object v1, Lorg/threeten/bp/chrono/HijrahDate;->MONTH_LENGTH:[I

    aget v1, v1, v15

    invoke-direct {v0, v1}, Ljava/lang/Integer;-><init>(I)V

    aput-object v0, v14, v15

    .line 1431
    add-int/lit8 v15, v15, 0x1

    goto :goto_32f

    .line 1437
    .end local v15    # "l":I
    :cond_342
    :goto_342
    array-length v0, v14

    new-array v15, v0, [Ljava/lang/Integer;

    .line 1439
    .local v15, "newEndMonthLengths":[Ljava/lang/Integer;
    const/16 v16, 0x0

    .local v16, "month":I
    :goto_347
    move/from16 v0, v16

    const/16 v1, 0xc

    if-ge v0, v1, :cond_373

    .line 1440
    move/from16 v0, v16

    move/from16 v1, p3

    if-ne v0, v1, :cond_363

    .line 1441
    new-instance v0, Ljava/lang/Integer;

    aget-object v1, v14, v16

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    add-int v1, v1, p4

    invoke-direct {v0, v1}, Ljava/lang/Integer;-><init>(I)V

    aput-object v0, v15, v16

    goto :goto_370

    .line 1444
    :cond_363
    new-instance v0, Ljava/lang/Integer;

    aget-object v1, v14, v16

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/lang/Integer;-><init>(I)V

    aput-object v0, v15, v16

    .line 1439
    :goto_370
    add-int/lit8 v16, v16, 0x1

    goto :goto_347

    .line 1449
    .end local v16    # "month":I
    :cond_373
    sget-object v0, Lorg/threeten/bp/chrono/HijrahDate;->ADJUSTED_MONTH_LENGTHS:Ljava/util/HashMap;

    new-instance v1, Ljava/lang/Integer;

    move/from16 v2, p2

    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0, v1, v15}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1451
    sget-object v0, Lorg/threeten/bp/chrono/HijrahDate;->ADJUSTED_MONTH_LENGTHS:Ljava/util/HashMap;

    new-instance v1, Ljava/lang/Integer;

    move/from16 v2, p0

    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object/from16 v16, v0

    check-cast v16, [Ljava/lang/Integer;

    .line 1453
    .local v16, "startMonthLengths":[Ljava/lang/Integer;
    sget-object v0, Lorg/threeten/bp/chrono/HijrahDate;->ADJUSTED_MONTH_LENGTHS:Ljava/util/HashMap;

    new-instance v1, Ljava/lang/Integer;

    move/from16 v2, p2

    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object/from16 v17, v0

    check-cast v17, [Ljava/lang/Integer;

    .line 1455
    .local v17, "endMonthLengths":[Ljava/lang/Integer;
    sget-object v0, Lorg/threeten/bp/chrono/HijrahDate;->ADJUSTED_MONTH_DAYS:Ljava/util/HashMap;

    new-instance v1, Ljava/lang/Integer;

    move/from16 v2, p0

    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object/from16 v18, v0

    check-cast v18, [Ljava/lang/Integer;

    .line 1457
    .local v18, "startMonthDays":[Ljava/lang/Integer;
    sget-object v0, Lorg/threeten/bp/chrono/HijrahDate;->ADJUSTED_MONTH_DAYS:Ljava/util/HashMap;

    new-instance v1, Ljava/lang/Integer;

    move/from16 v2, p2

    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object/from16 v19, v0

    check-cast v19, [Ljava/lang/Integer;

    .line 1459
    .local v19, "endMonthDays":[Ljava/lang/Integer;
    aget-object v0, v16, p1

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v20

    .line 1460
    .local v20, "startMonthLength":I
    aget-object v0, v17, p3

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v21

    .line 1461
    .local v21, "endMonthLength":I
    const/16 v0, 0xb

    aget-object v0, v18, v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/16 v1, 0xb

    aget-object v1, v16, v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    add-int v22, v0, v1

    .line 1463
    .local v22, "startMonthDay":I
    const/16 v0, 0xb

    aget-object v0, v19, v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/16 v1, 0xb

    aget-object v1, v17, v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    add-int v23, v0, v1

    .line 1466
    .local v23, "endMonthDay":I
    sget-object v0, Lorg/threeten/bp/chrono/HijrahDate;->ADJUSTED_MAX_VALUES:[Ljava/lang/Integer;

    const/4 v1, 0x5

    aget-object v0, v0, v1

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v24

    .line 1468
    .local v24, "maxMonthLength":I
    sget-object v0, Lorg/threeten/bp/chrono/HijrahDate;->ADJUSTED_LEAST_MAX_VALUES:[Ljava/lang/Integer;

    const/4 v1, 0x5

    aget-object v0, v0, v1

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v25

    .line 1471
    .local v25, "leastMaxMonthLength":I
    move/from16 v0, v24

    move/from16 v1, v20

    if-ge v0, v1, :cond_40d

    .line 1472
    move/from16 v24, v20

    .line 1474
    :cond_40d
    move/from16 v0, v24

    move/from16 v1, v21

    if-ge v0, v1, :cond_415

    .line 1475
    move/from16 v24, v21

    .line 1477
    :cond_415
    sget-object v0, Lorg/threeten/bp/chrono/HijrahDate;->ADJUSTED_MAX_VALUES:[Ljava/lang/Integer;

    new-instance v1, Ljava/lang/Integer;

    move/from16 v2, v24

    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    const/4 v2, 0x5

    aput-object v1, v0, v2

    .line 1479
    move/from16 v0, v25

    move/from16 v1, v20

    if-le v0, v1, :cond_429

    .line 1480
    move/from16 v25, v20

    .line 1482
    :cond_429
    move/from16 v0, v25

    move/from16 v1, v21

    if-le v0, v1, :cond_431

    .line 1483
    move/from16 v25, v21

    .line 1485
    :cond_431
    sget-object v0, Lorg/threeten/bp/chrono/HijrahDate;->ADJUSTED_LEAST_MAX_VALUES:[Ljava/lang/Integer;

    new-instance v1, Ljava/lang/Integer;

    move/from16 v2, v25

    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    const/4 v2, 0x5

    aput-object v1, v0, v2

    .line 1488
    sget-object v0, Lorg/threeten/bp/chrono/HijrahDate;->ADJUSTED_MAX_VALUES:[Ljava/lang/Integer;

    const/4 v1, 0x6

    aget-object v0, v0, v1

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v26

    .line 1489
    .local v26, "maxMonthDay":I
    sget-object v0, Lorg/threeten/bp/chrono/HijrahDate;->ADJUSTED_LEAST_MAX_VALUES:[Ljava/lang/Integer;

    const/4 v1, 0x6

    aget-object v0, v0, v1

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v27

    .line 1492
    .local v27, "leastMaxMonthDay":I
    move/from16 v0, v26

    move/from16 v1, v22

    if-ge v0, v1, :cond_457

    .line 1493
    move/from16 v26, v22

    .line 1495
    :cond_457
    move/from16 v0, v26

    move/from16 v1, v23

    if-ge v0, v1, :cond_45f

    .line 1496
    move/from16 v26, v23

    .line 1499
    :cond_45f
    sget-object v0, Lorg/threeten/bp/chrono/HijrahDate;->ADJUSTED_MAX_VALUES:[Ljava/lang/Integer;

    new-instance v1, Ljava/lang/Integer;

    move/from16 v2, v26

    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    const/4 v2, 0x6

    aput-object v1, v0, v2

    .line 1501
    move/from16 v0, v27

    move/from16 v1, v22

    if-le v0, v1, :cond_473

    .line 1502
    move/from16 v27, v22

    .line 1504
    :cond_473
    move/from16 v0, v27

    move/from16 v1, v23

    if-le v0, v1, :cond_47b

    .line 1505
    move/from16 v27, v23

    .line 1507
    :cond_47b
    sget-object v0, Lorg/threeten/bp/chrono/HijrahDate;->ADJUSTED_LEAST_MAX_VALUES:[Ljava/lang/Integer;

    new-instance v1, Ljava/lang/Integer;

    move/from16 v2, v27

    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    const/4 v2, 0x6

    aput-object v1, v0, v2

    .line 1509
    return-void
.end method

.method private static checkValidDayOfMonth(I)V
    .registers 4
    .param p0, "dayOfMonth"    # I

    .line 538
    const/4 v0, 0x1

    if-lt p0, v0, :cond_9

    invoke-static {}, Lorg/threeten/bp/chrono/HijrahDate;->getMaximumDayOfMonth()I

    move-result v0

    if-le p0, v0, :cond_36

    .line 540
    :cond_9
    new-instance v0, Lorg/threeten/bp/DateTimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid day of month of Hijrah date, day "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " greater than "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lorg/threeten/bp/chrono/HijrahDate;->getMaximumDayOfMonth()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " or less than 1"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/threeten/bp/DateTimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 543
    :cond_36
    return-void
.end method

.method private static checkValidDayOfYear(I)V
    .registers 3
    .param p0, "dayOfYear"    # I

    .line 525
    const/4 v0, 0x1

    if-lt p0, v0, :cond_9

    invoke-static {}, Lorg/threeten/bp/chrono/HijrahDate;->getMaximumDayOfYear()I

    move-result v0

    if-le p0, v0, :cond_11

    .line 527
    :cond_9
    new-instance v0, Lorg/threeten/bp/DateTimeException;

    const-string v1, "Invalid day of year of Hijrah date"

    invoke-direct {v0, v1}, Lorg/threeten/bp/DateTimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 529
    :cond_11
    return-void
.end method

.method private static checkValidMonth(I)V
    .registers 3
    .param p0, "month"    # I

    .line 532
    const/4 v0, 0x1

    if-lt p0, v0, :cond_7

    const/16 v0, 0xc

    if-le p0, v0, :cond_f

    .line 533
    :cond_7
    new-instance v0, Lorg/threeten/bp/DateTimeException;

    const-string v1, "Invalid month of Hijrah date"

    invoke-direct {v0, v1}, Lorg/threeten/bp/DateTimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 535
    :cond_f
    return-void
.end method

.method private static checkValidYearOfEra(I)V
    .registers 3
    .param p0, "yearOfEra"    # I

    .line 518
    const/4 v0, 0x1

    if-lt p0, v0, :cond_7

    const/16 v0, 0x270f

    if-le p0, v0, :cond_f

    .line 520
    :cond_7
    new-instance v0, Lorg/threeten/bp/DateTimeException;

    const-string v1, "Invalid year of Hijrah Era"

    invoke-direct {v0, v1}, Lorg/threeten/bp/DateTimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 522
    :cond_f
    return-void
.end method

.method private static getAdjustedCycle(I)[Ljava/lang/Integer;
    .registers 5
    .param p0, "cycleNumber"    # I

    .line 974
    :try_start_0
    sget-object v0, Lorg/threeten/bp/chrono/HijrahDate;->ADJUSTED_CYCLE_YEARS:Ljava/util/HashMap;

    new-instance v1, Ljava/lang/Integer;

    invoke-direct {v1, p0}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, [Ljava/lang/Integer;
    :try_end_e
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_e} :catch_f

    .line 977
    .local v2, "cycles":[Ljava/lang/Integer;
    goto :goto_11

    .line 975
    :catch_f
    move-exception v3

    .line 976
    .local v3, "e":Ljava/lang/ArrayIndexOutOfBoundsException;
    const/4 v2, 0x0

    .line 978
    .end local v3    # "e":Ljava/lang/ArrayIndexOutOfBoundsException;
    :goto_11
    if-nez v2, :cond_15

    .line 979
    sget-object v2, Lorg/threeten/bp/chrono/HijrahDate;->DEFAULT_CYCLE_YEARS:[Ljava/lang/Integer;

    .line 981
    :cond_15
    return-object v2
.end method

.method private static getAdjustedMonthDays(I)[Ljava/lang/Integer;
    .registers 5
    .param p0, "year"    # I

    .line 993
    :try_start_0
    sget-object v0, Lorg/threeten/bp/chrono/HijrahDate;->ADJUSTED_MONTH_DAYS:Ljava/util/HashMap;

    new-instance v1, Ljava/lang/Integer;

    invoke-direct {v1, p0}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, [Ljava/lang/Integer;
    :try_end_e
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_e} :catch_f

    .line 996
    .local v2, "newMonths":[Ljava/lang/Integer;
    goto :goto_11

    .line 994
    :catch_f
    move-exception v3

    .line 995
    .local v3, "e":Ljava/lang/ArrayIndexOutOfBoundsException;
    const/4 v2, 0x0

    .line 997
    .end local v3    # "e":Ljava/lang/ArrayIndexOutOfBoundsException;
    :goto_11
    if-nez v2, :cond_1f

    .line 998
    int-to-long v0, p0

    invoke-static {v0, v1}, Lorg/threeten/bp/chrono/HijrahDate;->isLeapYear(J)Z

    move-result v0

    if-eqz v0, :cond_1d

    .line 999
    sget-object v2, Lorg/threeten/bp/chrono/HijrahDate;->DEFAULT_LEAP_MONTH_DAYS:[Ljava/lang/Integer;

    goto :goto_1f

    .line 1001
    :cond_1d
    sget-object v2, Lorg/threeten/bp/chrono/HijrahDate;->DEFAULT_MONTH_DAYS:[Ljava/lang/Integer;

    .line 1004
    :cond_1f
    :goto_1f
    return-object v2
.end method

.method private static getAdjustedMonthLength(I)[Ljava/lang/Integer;
    .registers 5
    .param p0, "year"    # I

    .line 1016
    :try_start_0
    sget-object v0, Lorg/threeten/bp/chrono/HijrahDate;->ADJUSTED_MONTH_LENGTHS:Ljava/util/HashMap;

    new-instance v1, Ljava/lang/Integer;

    invoke-direct {v1, p0}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, [Ljava/lang/Integer;
    :try_end_e
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_e} :catch_f

    .line 1019
    .local v2, "newMonths":[Ljava/lang/Integer;
    goto :goto_11

    .line 1017
    :catch_f
    move-exception v3

    .line 1018
    .local v3, "e":Ljava/lang/ArrayIndexOutOfBoundsException;
    const/4 v2, 0x0

    .line 1020
    .end local v3    # "e":Ljava/lang/ArrayIndexOutOfBoundsException;
    :goto_11
    if-nez v2, :cond_1f

    .line 1021
    int-to-long v0, p0

    invoke-static {v0, v1}, Lorg/threeten/bp/chrono/HijrahDate;->isLeapYear(J)Z

    move-result v0

    if-eqz v0, :cond_1d

    .line 1022
    sget-object v2, Lorg/threeten/bp/chrono/HijrahDate;->DEFAULT_LEAP_MONTH_LENGTHS:[Ljava/lang/Integer;

    goto :goto_1f

    .line 1024
    :cond_1d
    sget-object v2, Lorg/threeten/bp/chrono/HijrahDate;->DEFAULT_MONTH_LENGTHS:[Ljava/lang/Integer;

    .line 1027
    :cond_1f
    :goto_1f
    return-object v2
.end method

.method private static getConfigFileInputStream()Ljava/io/InputStream;
    .registers 13
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1677
    const-string v0, "org.threeten.bp.i18n.HijrahDate.deviationConfigFile"

    invoke-static {v0}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1680
    .local v3, "fileName":Ljava/lang/String;
    if-nez v3, :cond_a

    .line 1681
    const-string v3, "hijrah_deviation.cfg"

    .line 1684
    :cond_a
    const-string v0, "org.threeten.bp.i18n.HijrahDate.deviationConfigDir"

    invoke-static {v0}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1687
    .local v4, "dir":Ljava/lang/String;
    if-eqz v4, :cond_67

    .line 1688
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_24

    const-string v0, "file.separator"

    invoke-static {v0}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3b

    .line 1690
    :cond_24
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "file.separator"

    invoke-static {v1}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1692
    :cond_3b
    new-instance v5, Ljava/io/File;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-char v1, Lorg/threeten/bp/chrono/HijrahDate;->FILE_SEP:C

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v5, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1693
    .local v5, "file":Ljava/io/File;
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_65

    .line 1695
    :try_start_5d
    new-instance v0, Ljava/io/FileInputStream;

    invoke-direct {v0, v5}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_62
    .catch Ljava/io/IOException; {:try_start_5d .. :try_end_62} :catch_63

    return-object v0

    .line 1696
    :catch_63
    move-exception v6

    .line 1697
    .local v6, "ioe":Ljava/io/IOException;
    throw v6

    .line 1700
    .end local v6    # "ioe":Ljava/io/IOException;
    :cond_65
    const/4 v0, 0x0

    return-object v0

    .line 1703
    .end local v5    # "file":Ljava/io/File;
    :cond_67
    const-string v0, "java.class.path"

    invoke-static {v0}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 1704
    .local v5, "classPath":Ljava/lang/String;
    new-instance v6, Ljava/util/StringTokenizer;

    sget-object v0, Lorg/threeten/bp/chrono/HijrahDate;->PATH_SEP:Ljava/lang/String;

    invoke-direct {v6, v5, v0}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1705
    .local v6, "st":Ljava/util/StringTokenizer;
    :goto_74
    invoke-virtual {v6}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v0

    if-eqz v0, :cond_135

    .line 1706
    invoke-virtual {v6}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v7

    .line 1707
    .local v7, "path":Ljava/lang/String;
    new-instance v8, Ljava/io/File;

    invoke-direct {v8, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1708
    .local v8, "file":Ljava/io/File;
    invoke-virtual {v8}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_133

    .line 1709
    invoke-virtual {v8}, Ljava/io/File;->isDirectory()Z

    move-result v0

    if-eqz v0, :cond_e0

    .line 1710
    new-instance v9, Ljava/io/File;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-char v1, Lorg/threeten/bp/chrono/HijrahDate;->FILE_SEP:C

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lorg/threeten/bp/chrono/HijrahDate;->DEFAULT_CONFIG_PATH:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v9, v0, v3}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1712
    .local v9, "f":Ljava/io/File;
    invoke-virtual {v9}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_de

    .line 1714
    :try_start_b3
    new-instance v0, Ljava/io/FileInputStream;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-char v2, Lorg/threeten/bp/chrono/HijrahDate;->FILE_SEP:C

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lorg/threeten/bp/chrono/HijrahDate;->DEFAULT_CONFIG_PATH:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-char v2, Lorg/threeten/bp/chrono/HijrahDate;->FILE_SEP:C

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_db
    .catch Ljava/io/IOException; {:try_start_b3 .. :try_end_db} :catch_dc

    return-object v0

    .line 1717
    :catch_dc
    move-exception v10

    .line 1718
    .local v10, "ioe":Ljava/io/IOException;
    throw v10

    .line 1721
    .end local v9    # "f":Ljava/io/File;
    .end local v10    # "ioe":Ljava/io/IOException;
    :cond_de
    goto/16 :goto_133

    .line 1724
    :cond_e0
    :try_start_e0
    new-instance v9, Ljava/util/zip/ZipFile;

    invoke-direct {v9, v8}, Ljava/util/zip/ZipFile;-><init>(Ljava/io/File;)V
    :try_end_e5
    .catch Ljava/io/IOException; {:try_start_e0 .. :try_end_e5} :catch_e6

    .line 1727
    .local v9, "zip":Ljava/util/zip/ZipFile;
    goto :goto_e8

    .line 1725
    :catch_e6
    move-exception v10

    .line 1726
    .local v10, "ioe":Ljava/io/IOException;
    const/4 v9, 0x0

    .line 1729
    .end local v10    # "ioe":Ljava/io/IOException;
    :goto_e8
    if-eqz v9, :cond_133

    .line 1730
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lorg/threeten/bp/chrono/HijrahDate;->DEFAULT_CONFIG_PATH:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-char v1, Lorg/threeten/bp/chrono/HijrahDate;->FILE_SEP:C

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 1732
    .local v10, "targetFile":Ljava/lang/String;
    invoke-virtual {v9, v10}, Ljava/util/zip/ZipFile;->getEntry(Ljava/lang/String;)Ljava/util/zip/ZipEntry;

    move-result-object v11

    .line 1734
    .local v11, "entry":Ljava/util/zip/ZipEntry;
    if-nez v11, :cond_12a

    .line 1735
    sget-char v0, Lorg/threeten/bp/chrono/HijrahDate;->FILE_SEP:C

    const/16 v1, 0x2f

    if-ne v0, v1, :cond_118

    .line 1736
    const/16 v0, 0x2f

    const/16 v1, 0x5c

    invoke-virtual {v10, v0, v1}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v10

    goto :goto_126

    .line 1737
    :cond_118
    sget-char v0, Lorg/threeten/bp/chrono/HijrahDate;->FILE_SEP:C

    const/16 v1, 0x5c

    if-ne v0, v1, :cond_126

    .line 1738
    const/16 v0, 0x5c

    const/16 v1, 0x2f

    invoke-virtual {v10, v0, v1}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v10

    .line 1740
    :cond_126
    :goto_126
    invoke-virtual {v9, v10}, Ljava/util/zip/ZipFile;->getEntry(Ljava/lang/String;)Ljava/util/zip/ZipEntry;

    move-result-object v11

    .line 1743
    :cond_12a
    if-eqz v11, :cond_133

    .line 1745
    :try_start_12c
    invoke-virtual {v9, v11}, Ljava/util/zip/ZipFile;->getInputStream(Ljava/util/zip/ZipEntry;)Ljava/io/InputStream;
    :try_end_12f
    .catch Ljava/io/IOException; {:try_start_12c .. :try_end_12f} :catch_131

    move-result-object v0

    return-object v0

    .line 1746
    :catch_131
    move-exception v12

    .line 1747
    .local v12, "ioe":Ljava/io/IOException;
    throw v12

    .line 1753
    .end local v7    # "path":Ljava/lang/String;
    .end local v8    # "file":Ljava/io/File;
    .end local v9    # "zip":Ljava/util/zip/ZipFile;
    .end local v10    # "targetFile":Ljava/lang/String;
    .end local v11    # "entry":Ljava/util/zip/ZipEntry;
    .end local v12    # "ioe":Ljava/io/IOException;
    :cond_133
    :goto_133
    goto/16 :goto_74

    .line 1754
    :cond_135
    const/4 v0, 0x0

    return-object v0
.end method

.method private static getCycleNumber(J)I
    .registers 7
    .param p0, "epochDay"    # J

    .line 897
    sget-object v2, Lorg/threeten/bp/chrono/HijrahDate;->ADJUSTED_CYCLES:[Ljava/lang/Long;

    .line 900
    .local v2, "days":[Ljava/lang/Long;
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_3
    :try_start_3
    array-length v0, v2

    if-ge v4, v0, :cond_16

    .line 901
    aget-object v0, v2, v4

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J
    :try_end_b
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_3 .. :try_end_b} :catch_1a

    move-result-wide v0

    cmp-long v0, p0, v0

    if-gez v0, :cond_13

    .line 902
    add-int/lit8 v0, v4, -0x1

    return v0

    .line 900
    :cond_13
    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    .line 905
    .end local v4    # "i":I
    :cond_16
    long-to-int v0, p0

    :try_start_17
    div-int/lit16 v3, v0, 0x2987
    :try_end_19
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_17 .. :try_end_19} :catch_1a

    .line 908
    .local v3, "cycleNumber":I
    goto :goto_1e

    .line 906
    :catch_1a
    move-exception v4

    .line 907
    .local v4, "e":Ljava/lang/ArrayIndexOutOfBoundsException;
    long-to-int v0, p0

    div-int/lit16 v3, v0, 0x2987

    .line 909
    .end local v4    # "e":Ljava/lang/ArrayIndexOutOfBoundsException;
    :goto_1e
    return v3
.end method

.method private static getDayOfCycle(JI)I
    .registers 7
    .param p0, "epochDay"    # J
    .param p2, "cycleNumber"    # I

    .line 923
    :try_start_0
    sget-object v0, Lorg/threeten/bp/chrono/HijrahDate;->ADJUSTED_CYCLES:[Ljava/lang/Long;

    aget-object v2, v0, p2
    :try_end_4
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_4} :catch_5

    .line 926
    .local v2, "day":Ljava/lang/Long;
    goto :goto_7

    .line 924
    :catch_5
    move-exception v3

    .line 925
    .local v3, "e":Ljava/lang/ArrayIndexOutOfBoundsException;
    const/4 v2, 0x0

    .line 927
    .end local v3    # "e":Ljava/lang/ArrayIndexOutOfBoundsException;
    :goto_7
    if-nez v2, :cond_11

    .line 928
    new-instance v2, Ljava/lang/Long;

    mul-int/lit16 v0, p2, 0x2987

    int-to-long v0, v0

    invoke-direct {v2, v0, v1}, Ljava/lang/Long;-><init>(J)V

    .line 930
    :cond_11
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    sub-long v0, p0, v0

    long-to-int v0, v0

    return v0
.end method

.method private static getDayOfMonth(III)I
    .registers 6
    .param p0, "dayOfYear"    # I
    .param p1, "month"    # I
    .param p2, "year"    # I

    .line 1088
    invoke-static {p2}, Lorg/threeten/bp/chrono/HijrahDate;->getAdjustedMonthDays(I)[Ljava/lang/Integer;

    move-result-object v2

    .line 1090
    .local v2, "newMonths":[Ljava/lang/Integer;
    if-ltz p0, :cond_12

    .line 1091
    if-lez p1, :cond_11

    .line 1092
    aget-object v0, v2, p1

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    sub-int v0, p0, v0

    return v0

    .line 1094
    :cond_11
    return p0

    .line 1097
    :cond_12
    int-to-long v0, p2

    invoke-static {v0, v1}, Lorg/threeten/bp/chrono/HijrahDate;->isLeapYear(J)Z

    move-result v0

    if-eqz v0, :cond_1c

    add-int/lit16 p0, p0, 0x163

    goto :goto_1e

    :cond_1c
    add-int/lit16 p0, p0, 0x162

    .line 1099
    :goto_1e
    if-lez p1, :cond_29

    .line 1100
    aget-object v0, v2, p1

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    sub-int v0, p0, v0

    return v0

    .line 1102
    :cond_29
    return p0
.end method

.method private static getDayOfYear(III)I
    .registers 5
    .param p0, "cycleNumber"    # I
    .param p1, "dayOfCycle"    # I
    .param p2, "yearInCycle"    # I

    .line 1039
    invoke-static {p0}, Lorg/threeten/bp/chrono/HijrahDate;->getAdjustedCycle(I)[Ljava/lang/Integer;

    move-result-object v1

    .line 1041
    .local v1, "cycles":[Ljava/lang/Integer;
    if-lez p1, :cond_f

    .line 1042
    aget-object v0, v1, p2

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    sub-int v0, p1, v0

    return v0

    .line 1044
    :cond_f
    aget-object v0, v1, p2

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    add-int/2addr v0, p1

    return v0
.end method

.method private static getGregorianEpochDay(III)J
    .registers 7
    .param p0, "prolepticYear"    # I
    .param p1, "monthOfYear"    # I
    .param p2, "dayOfMonth"    # I

    .line 852
    invoke-static {p0}, Lorg/threeten/bp/chrono/HijrahDate;->yearToGregorianEpochDay(I)J

    move-result-wide v2

    .line 853
    .local v2, "day":J
    add-int/lit8 v0, p1, -0x1

    invoke-static {v0, p0}, Lorg/threeten/bp/chrono/HijrahDate;->getMonthDays(II)I

    move-result v0

    int-to-long v0, v0

    add-long/2addr v2, v0

    .line 854
    int-to-long v0, p2

    add-long/2addr v2, v0

    .line 855
    return-wide v2
.end method

.method private static getHijrahDateInfo(J)[I
    .registers 18
    .param p0, "gregorianDays"    # J

    .line 798
    const-wide/32 v0, -0x78274

    sub-long v13, p0, v0

    .line 800
    .local v13, "epochDay":J
    const-wide/16 v0, 0x0

    cmp-long v0, v13, v0

    if-ltz v0, :cond_32

    .line 801
    invoke-static {v13, v14}, Lorg/threeten/bp/chrono/HijrahDate;->getCycleNumber(J)I

    move-result v10

    .line 802
    .local v10, "cycleNumber":I
    invoke-static {v13, v14, v10}, Lorg/threeten/bp/chrono/HijrahDate;->getDayOfCycle(JI)I

    move-result v12

    .line 803
    .local v12, "dayOfCycle":I
    int-to-long v0, v12

    invoke-static {v10, v0, v1}, Lorg/threeten/bp/chrono/HijrahDate;->getYearInCycle(IJ)I

    move-result v11

    .line 804
    .local v11, "yearInCycle":I
    invoke-static {v10, v12, v11}, Lorg/threeten/bp/chrono/HijrahDate;->getDayOfYear(III)I

    move-result v9

    .line 806
    .local v9, "dayOfYear":I
    mul-int/lit8 v0, v10, 0x1e

    add-int/2addr v0, v11

    add-int/lit8 v5, v0, 0x1

    .line 807
    .local v5, "year":I
    invoke-static {v9, v5}, Lorg/threeten/bp/chrono/HijrahDate;->getMonthOfYear(II)I

    move-result v6

    .line 808
    .local v6, "month":I
    invoke-static {v9, v6, v5}, Lorg/threeten/bp/chrono/HijrahDate;->getDayOfMonth(III)I

    move-result v7

    .line 809
    .local v7, "date":I
    add-int/lit8 v7, v7, 0x1

    .line 810
    sget-object v0, Lorg/threeten/bp/chrono/HijrahEra;->AH:Lorg/threeten/bp/chrono/HijrahEra;

    invoke-virtual {v0}, Lorg/threeten/bp/chrono/HijrahEra;->getValue()I

    move-result v4

    .local v4, "era":I
    goto :goto_69

    .line 812
    .end local v4    # "era":I
    .end local v5    # "year":I
    .end local v6    # "month":I
    .end local v7    # "date":I
    .end local v9    # "dayOfYear":I
    .end local v10    # "cycleNumber":I
    .end local v11    # "yearInCycle":I
    .end local v12    # "dayOfCycle":I
    :cond_32
    long-to-int v0, v13

    div-int/lit16 v10, v0, 0x2987

    .line 813
    .local v10, "cycleNumber":I
    long-to-int v0, v13

    rem-int/lit16 v12, v0, 0x2987

    .line 814
    .local v12, "dayOfCycle":I
    if-nez v12, :cond_3e

    .line 815
    const/16 v12, -0x2987

    .line 816
    add-int/lit8 v10, v10, 0x1

    .line 818
    :cond_3e
    int-to-long v0, v12

    invoke-static {v10, v0, v1}, Lorg/threeten/bp/chrono/HijrahDate;->getYearInCycle(IJ)I

    move-result v11

    .line 819
    .local v11, "yearInCycle":I
    invoke-static {v10, v12, v11}, Lorg/threeten/bp/chrono/HijrahDate;->getDayOfYear(III)I

    move-result v9

    .line 820
    .local v9, "dayOfYear":I
    mul-int/lit8 v0, v10, 0x1e

    sub-int v5, v0, v11

    .line 821
    .local v5, "year":I
    rsub-int/lit8 v5, v5, 0x1

    .line 822
    int-to-long v0, v5

    invoke-static {v0, v1}, Lorg/threeten/bp/chrono/HijrahDate;->isLeapYear(J)Z

    move-result v0

    if-eqz v0, :cond_57

    add-int/lit16 v9, v9, 0x163

    goto :goto_59

    :cond_57
    add-int/lit16 v9, v9, 0x162

    .line 824
    :goto_59
    invoke-static {v9, v5}, Lorg/threeten/bp/chrono/HijrahDate;->getMonthOfYear(II)I

    move-result v6

    .line 825
    .local v6, "month":I
    invoke-static {v9, v6, v5}, Lorg/threeten/bp/chrono/HijrahDate;->getDayOfMonth(III)I

    move-result v7

    .line 826
    .local v7, "date":I
    add-int/lit8 v7, v7, 0x1

    .line 827
    sget-object v0, Lorg/threeten/bp/chrono/HijrahEra;->BEFORE_AH:Lorg/threeten/bp/chrono/HijrahEra;

    invoke-virtual {v0}, Lorg/threeten/bp/chrono/HijrahEra;->getValue()I

    move-result v4

    .line 830
    .local v4, "era":I
    :goto_69
    const-wide/16 v0, 0x5

    add-long/2addr v0, v13

    const-wide/16 v2, 0x7

    rem-long/2addr v0, v2

    long-to-int v8, v0

    .line 831
    .local v8, "dayOfWeek":I
    if-gtz v8, :cond_74

    const/4 v0, 0x7

    goto :goto_75

    :cond_74
    const/4 v0, 0x0

    :goto_75
    add-int/2addr v8, v0

    .line 833
    const/4 v0, 0x6

    new-array v15, v0, [I

    .line 834
    .local v15, "dateInfo":[I
    const/4 v0, 0x0

    aput v4, v15, v0

    .line 835
    const/4 v0, 0x1

    aput v5, v15, v0

    .line 836
    add-int/lit8 v0, v6, 0x1

    const/4 v1, 0x2

    aput v0, v15, v1

    .line 837
    const/4 v0, 0x3

    aput v7, v15, v0

    .line 838
    add-int/lit8 v0, v9, 0x1

    const/4 v1, 0x4

    aput v0, v15, v1

    .line 839
    const/4 v0, 0x5

    aput v8, v15, v0

    .line 840
    return-object v15
.end method

.method static getMaximumDayOfMonth()I
    .registers 2

    .line 1186
    sget-object v0, Lorg/threeten/bp/chrono/HijrahDate;->ADJUSTED_MAX_VALUES:[Ljava/lang/Integer;

    const/4 v1, 0x5

    aget-object v0, v0, v1

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method static getMaximumDayOfYear()I
    .registers 2

    .line 1204
    sget-object v0, Lorg/threeten/bp/chrono/HijrahDate;->ADJUSTED_MAX_VALUES:[Ljava/lang/Integer;

    const/4 v1, 0x6

    aget-object v0, v0, v1

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method private static getMonthDays(II)I
    .registers 4
    .param p0, "month"    # I
    .param p1, "year"    # I

    .line 1125
    invoke-static {p1}, Lorg/threeten/bp/chrono/HijrahDate;->getAdjustedMonthDays(I)[Ljava/lang/Integer;

    move-result-object v1

    .line 1126
    .local v1, "newMonths":[Ljava/lang/Integer;
    aget-object v0, v1, p0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method static getMonthLength(II)I
    .registers 4
    .param p0, "month"    # I
    .param p1, "year"    # I

    .line 1137
    invoke-static {p1}, Lorg/threeten/bp/chrono/HijrahDate;->getAdjustedMonthLength(I)[Ljava/lang/Integer;

    move-result-object v1

    .line 1138
    .local v1, "newMonths":[Ljava/lang/Integer;
    aget-object v0, v1, p0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method private static getMonthOfYear(II)I
    .registers 6
    .param p0, "dayOfYear"    # I
    .param p1, "year"    # I

    .line 1057
    invoke-static {p1}, Lorg/threeten/bp/chrono/HijrahDate;->getAdjustedMonthDays(I)[Ljava/lang/Integer;

    move-result-object v2

    .line 1059
    .local v2, "newMonths":[Ljava/lang/Integer;
    if-ltz p0, :cond_1b

    .line 1060
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_7
    array-length v0, v2

    if-ge v3, v0, :cond_18

    .line 1061
    aget-object v0, v2, v3

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ge p0, v0, :cond_15

    .line 1062
    add-int/lit8 v0, v3, -0x1

    return v0

    .line 1060
    :cond_15
    add-int/lit8 v3, v3, 0x1

    goto :goto_7

    .line 1065
    .end local v3    # "i":I
    :cond_18
    const/16 v0, 0xb

    return v0

    .line 1067
    :cond_1b
    int-to-long v0, p1

    invoke-static {v0, v1}, Lorg/threeten/bp/chrono/HijrahDate;->isLeapYear(J)Z

    move-result v0

    if-eqz v0, :cond_25

    add-int/lit16 p0, p0, 0x163

    goto :goto_27

    :cond_25
    add-int/lit16 p0, p0, 0x162

    .line 1069
    :goto_27
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_28
    array-length v0, v2

    if-ge v3, v0, :cond_39

    .line 1070
    aget-object v0, v2, v3

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ge p0, v0, :cond_36

    .line 1071
    add-int/lit8 v0, v3, -0x1

    return v0

    .line 1069
    :cond_36
    add-int/lit8 v3, v3, 0x1

    goto :goto_28

    .line 1074
    .end local v3    # "i":I
    :cond_39
    const/16 v0, 0xb

    return v0
.end method

.method private static getYearInCycle(IJ)I
    .registers 7
    .param p0, "cycleNumber"    # I
    .param p1, "dayOfCycle"    # J

    .line 941
    invoke-static {p0}, Lorg/threeten/bp/chrono/HijrahDate;->getAdjustedCycle(I)[Ljava/lang/Integer;

    move-result-object v2

    .line 942
    .local v2, "cycles":[Ljava/lang/Integer;
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-nez v0, :cond_c

    .line 943
    const/4 v0, 0x0

    return v0

    .line 946
    :cond_c
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-lez v0, :cond_2a

    .line 947
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_13
    array-length v0, v2

    if-ge v3, v0, :cond_27

    .line 948
    aget-object v0, v2, v3

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-long v0, v0

    cmp-long v0, p1, v0

    if-gez v0, :cond_24

    .line 949
    add-int/lit8 v0, v3, -0x1

    return v0

    .line 947
    :cond_24
    add-int/lit8 v3, v3, 0x1

    goto :goto_13

    .line 952
    .end local v3    # "i":I
    :cond_27
    const/16 v0, 0x1d

    return v0

    .line 954
    :cond_2a
    neg-long p1, p1

    .line 955
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_2c
    array-length v0, v2

    if-ge v3, v0, :cond_40

    .line 956
    aget-object v0, v2, v3

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-long v0, v0

    cmp-long v0, p1, v0

    if-gtz v0, :cond_3d

    .line 957
    add-int/lit8 v0, v3, -0x1

    return v0

    .line 955
    :cond_3d
    add-int/lit8 v3, v3, 0x1

    goto :goto_2c

    .line 960
    .end local v3    # "i":I
    :cond_40
    const/16 v0, 0x1d

    return v0
.end method

.method static getYearLength(I)I
    .registers 6
    .param p0, "year"    # I

    .line 1154
    add-int/lit8 v0, p0, -0x1

    div-int/lit8 v2, v0, 0x1e

    .line 1157
    .local v2, "cycleNumber":I
    :try_start_4
    sget-object v0, Lorg/threeten/bp/chrono/HijrahDate;->ADJUSTED_CYCLE_YEARS:Ljava/util/HashMap;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, [Ljava/lang/Integer;
    :try_end_11
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_4 .. :try_end_11} :catch_12

    .line 1160
    .local v3, "cycleYears":[Ljava/lang/Integer;
    goto :goto_14

    .line 1158
    :catch_12
    move-exception v4

    .line 1159
    .local v4, "e":Ljava/lang/ArrayIndexOutOfBoundsException;
    const/4 v3, 0x0

    .line 1161
    .end local v4    # "e":Ljava/lang/ArrayIndexOutOfBoundsException;
    :goto_14
    if-eqz v3, :cond_49

    .line 1162
    add-int/lit8 v0, p0, -0x1

    rem-int/lit8 v4, v0, 0x1e

    .line 1163
    .local v4, "yearInCycle":I
    const/16 v0, 0x1d

    if-ne v4, v0, :cond_39

    .line 1164
    sget-object v0, Lorg/threeten/bp/chrono/HijrahDate;->ADJUSTED_CYCLES:[Ljava/lang/Long;

    add-int/lit8 v1, v2, 0x1

    aget-object v0, v0, v1

    invoke-virtual {v0}, Ljava/lang/Long;->intValue()I

    move-result v0

    sget-object v1, Lorg/threeten/bp/chrono/HijrahDate;->ADJUSTED_CYCLES:[Ljava/lang/Long;

    aget-object v1, v1, v2

    invoke-virtual {v1}, Ljava/lang/Long;->intValue()I

    move-result v1

    sub-int/2addr v0, v1

    aget-object v1, v3, v4

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    sub-int/2addr v0, v1

    return v0

    .line 1168
    :cond_39
    add-int/lit8 v0, v4, 0x1

    aget-object v0, v3, v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    aget-object v1, v3, v4

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    sub-int/2addr v0, v1

    return v0

    .line 1171
    .end local v4    # "yearInCycle":I
    :cond_49
    int-to-long v0, p0

    invoke-static {v0, v1}, Lorg/threeten/bp/chrono/HijrahDate;->isLeapYear(J)Z

    move-result v0

    if-eqz v0, :cond_53

    const/16 v0, 0x163

    goto :goto_55

    :cond_53
    const/16 v0, 0x162

    :goto_55
    return v0
.end method

.method static isLeapYear(J)Z
    .registers 6
    .param p0, "year"    # J

    .line 1114
    const-wide/16 v0, 0x0

    cmp-long v0, p0, v0

    if-lez v0, :cond_8

    move-wide v0, p0

    goto :goto_9

    :cond_8
    neg-long v0, p0

    :goto_9
    const-wide/16 v2, 0xb

    mul-long/2addr v0, v2

    const-wide/16 v2, 0xe

    add-long/2addr v0, v2

    const-wide/16 v2, 0x1e

    rem-long/2addr v0, v2

    const-wide/16 v2, 0xb

    cmp-long v0, v0, v2

    if-gez v0, :cond_1a

    const/4 v0, 0x1

    goto :goto_1b

    :cond_1a
    const/4 v0, 0x0

    :goto_1b
    return v0
.end method

.method public static of(III)Lorg/threeten/bp/chrono/HijrahDate;
    .registers 5
    .param p0, "prolepticYear"    # I
    .param p1, "monthOfYear"    # I
    .param p2, "dayOfMonth"    # I

    .line 487
    const/4 v0, 0x1

    if-lt p0, v0, :cond_a

    sget-object v0, Lorg/threeten/bp/chrono/HijrahEra;->AH:Lorg/threeten/bp/chrono/HijrahEra;

    invoke-static {v0, p0, p1, p2}, Lorg/threeten/bp/chrono/HijrahDate;->of(Lorg/threeten/bp/chrono/HijrahEra;III)Lorg/threeten/bp/chrono/HijrahDate;

    move-result-object v0

    goto :goto_12

    :cond_a
    sget-object v0, Lorg/threeten/bp/chrono/HijrahEra;->BEFORE_AH:Lorg/threeten/bp/chrono/HijrahEra;

    rsub-int/lit8 v1, p0, 0x1

    invoke-static {v0, v1, p1, p2}, Lorg/threeten/bp/chrono/HijrahDate;->of(Lorg/threeten/bp/chrono/HijrahEra;III)Lorg/threeten/bp/chrono/HijrahDate;

    move-result-object v0

    :goto_12
    return-object v0
.end method

.method static of(Lorg/threeten/bp/chrono/HijrahEra;III)Lorg/threeten/bp/chrono/HijrahDate;
    .registers 7
    .param p0, "era"    # Lorg/threeten/bp/chrono/HijrahEra;
    .param p1, "yearOfEra"    # I
    .param p2, "monthOfYear"    # I
    .param p3, "dayOfMonth"    # I

    .line 505
    const-string v0, "era"

    invoke-static {p0, v0}, Lorg/threeten/bp/jdk8/Jdk8Methods;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 506
    invoke-static {p1}, Lorg/threeten/bp/chrono/HijrahDate;->checkValidYearOfEra(I)V

    .line 507
    invoke-static {p2}, Lorg/threeten/bp/chrono/HijrahDate;->checkValidMonth(I)V

    .line 508
    invoke-static {p3}, Lorg/threeten/bp/chrono/HijrahDate;->checkValidDayOfMonth(I)V

    .line 509
    invoke-virtual {p0, p1}, Lorg/threeten/bp/chrono/HijrahEra;->prolepticYear(I)I

    move-result v0

    invoke-static {v0, p2, p3}, Lorg/threeten/bp/chrono/HijrahDate;->getGregorianEpochDay(III)J

    move-result-wide v1

    .line 510
    .local v1, "gregorianDays":J
    new-instance v0, Lorg/threeten/bp/chrono/HijrahDate;

    invoke-direct {v0, v1, v2}, Lorg/threeten/bp/chrono/HijrahDate;-><init>(J)V

    return-object v0
.end method

.method static ofEpochDay(J)Lorg/threeten/bp/chrono/HijrahDate;
    .registers 3
    .param p0, "epochDay"    # J

    .line 558
    new-instance v0, Lorg/threeten/bp/chrono/HijrahDate;

    invoke-direct {v0, p0, p1}, Lorg/threeten/bp/chrono/HijrahDate;-><init>(J)V

    return-object v0
.end method

.method private static parseLine(Ljava/lang/String;I)V
    .registers 22
    .param p0, "line"    # Ljava/lang/String;
    .param p1, "num"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/text/ParseException;
        }
    .end annotation

    .line 1558
    new-instance v3, Ljava/util/StringTokenizer;

    const-string v0, ";"

    move-object/from16 v1, p0

    invoke-direct {v3, v1, v0}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1559
    .local v3, "st":Ljava/util/StringTokenizer;
    :goto_9
    invoke-virtual {v3}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v0

    if-eqz v0, :cond_205

    .line 1560
    invoke-virtual {v3}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v4

    .line 1561
    .local v4, "deviationElement":Ljava/lang/String;
    const/16 v0, 0x3a

    invoke-virtual {v4, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v5

    .line 1562
    .local v5, "offsetIndex":I
    const/4 v0, -0x1

    if-eq v5, v0, :cond_1e0

    .line 1563
    add-int/lit8 v0, v5, 0x1

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {v4, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    .line 1567
    .local v6, "offsetString":Ljava/lang/String;
    :try_start_26
    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_29
    .catch Ljava/lang/NumberFormatException; {:try_start_26 .. :try_end_29} :catch_2b

    move-result v7

    .line 1572
    .local v7, "offset":I
    goto :goto_4f

    .line 1568
    :catch_2b
    move-exception v8

    .line 1569
    .local v8, "ex":Ljava/lang/NumberFormatException;
    new-instance v0, Ljava/text/ParseException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Offset is not properly set at line "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move/from16 v2, p1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    move/from16 v2, p1

    invoke-direct {v0, v1, v2}, Ljava/text/ParseException;-><init>(Ljava/lang/String;I)V

    throw v0

    .line 1573
    .end local v8    # "ex":Ljava/lang/NumberFormatException;
    :goto_4f
    const/16 v0, 0x2d

    invoke-virtual {v4, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v8

    .line 1574
    .local v8, "separatorIndex":I
    const/4 v0, -0x1

    if-eq v8, v0, :cond_1bc

    .line 1575
    const/4 v0, 0x0

    invoke-virtual {v4, v0, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    .line 1577
    .local v9, "startDateStg":Ljava/lang/String;
    add-int/lit8 v0, v8, 0x1

    invoke-virtual {v4, v0, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    .line 1579
    .local v10, "endDateStg":Ljava/lang/String;
    const/16 v0, 0x2f

    invoke-virtual {v9, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v11

    .line 1580
    .local v11, "startDateYearSepIndex":I
    const/16 v0, 0x2f

    invoke-virtual {v10, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v12

    .line 1581
    .local v12, "endDateYearSepIndex":I
    const/4 v13, -0x1

    .line 1582
    .local v13, "startYear":I
    const/4 v14, -0x1

    .line 1583
    .local v14, "endYear":I
    const/4 v15, -0x1

    .line 1584
    .local v15, "startMonth":I
    const/16 v16, -0x1

    .line 1585
    .local v16, "endMonth":I
    const/4 v0, -0x1

    if-eq v11, v0, :cond_d9

    .line 1586
    const/4 v0, 0x0

    invoke-virtual {v9, v0, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v17

    .line 1588
    .local v17, "startYearStg":Ljava/lang/String;
    add-int/lit8 v0, v11, 0x1

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {v9, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v18

    .line 1592
    .local v18, "startMonthStg":Ljava/lang/String;
    :try_start_86
    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_89
    .catch Ljava/lang/NumberFormatException; {:try_start_86 .. :try_end_89} :catch_8b

    move-result v13

    .line 1597
    goto :goto_af

    .line 1593
    :catch_8b
    move-exception v19

    .line 1594
    .local v19, "ex":Ljava/lang/NumberFormatException;
    new-instance v0, Ljava/text/ParseException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Start year is not properly set at line "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move/from16 v2, p1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    move/from16 v2, p1

    invoke-direct {v0, v1, v2}, Ljava/text/ParseException;-><init>(Ljava/lang/String;I)V

    throw v0

    .line 1599
    .end local v19    # "ex":Ljava/lang/NumberFormatException;
    :goto_af
    :try_start_af
    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_b2
    .catch Ljava/lang/NumberFormatException; {:try_start_af .. :try_end_b2} :catch_b4

    move-result v15

    .line 1604
    goto :goto_d8

    .line 1600
    :catch_b4
    move-exception v19

    .line 1601
    .local v19, "ex":Ljava/lang/NumberFormatException;
    new-instance v0, Ljava/text/ParseException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Start month is not properly set at line "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move/from16 v2, p1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    move/from16 v2, p1

    invoke-direct {v0, v1, v2}, Ljava/text/ParseException;-><init>(Ljava/lang/String;I)V

    throw v0

    .line 1605
    .end local v17    # "startYearStg":Ljava/lang/String;
    .end local v18    # "startMonthStg":Ljava/lang/String;
    .end local v19    # "ex":Ljava/lang/NumberFormatException;
    :goto_d8
    goto :goto_fc

    .line 1606
    :cond_d9
    new-instance v0, Ljava/text/ParseException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Start year/month has incorrect format at line "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move/from16 v2, p1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    move/from16 v2, p1

    invoke-direct {v0, v1, v2}, Ljava/text/ParseException;-><init>(Ljava/lang/String;I)V

    throw v0

    .line 1610
    :goto_fc
    const/4 v0, -0x1

    if-eq v12, v0, :cond_161

    .line 1611
    const/4 v0, 0x0

    invoke-virtual {v10, v0, v12}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v17

    .line 1613
    .local v17, "endYearStg":Ljava/lang/String;
    add-int/lit8 v0, v12, 0x1

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {v10, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v18

    .line 1616
    .local v18, "endMonthStg":Ljava/lang/String;
    :try_start_10e
    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_111
    .catch Ljava/lang/NumberFormatException; {:try_start_10e .. :try_end_111} :catch_113

    move-result v14

    .line 1621
    goto :goto_137

    .line 1617
    :catch_113
    move-exception v19

    .line 1618
    .local v19, "ex":Ljava/lang/NumberFormatException;
    new-instance v0, Ljava/text/ParseException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "End year is not properly set at line "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move/from16 v2, p1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    move/from16 v2, p1

    invoke-direct {v0, v1, v2}, Ljava/text/ParseException;-><init>(Ljava/lang/String;I)V

    throw v0

    .line 1623
    .end local v19    # "ex":Ljava/lang/NumberFormatException;
    :goto_137
    :try_start_137
    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_13a
    .catch Ljava/lang/NumberFormatException; {:try_start_137 .. :try_end_13a} :catch_13c

    move-result v16

    .line 1628
    goto :goto_160

    .line 1624
    :catch_13c
    move-exception v19

    .line 1625
    .local v19, "ex":Ljava/lang/NumberFormatException;
    new-instance v0, Ljava/text/ParseException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "End month is not properly set at line "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move/from16 v2, p1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    move/from16 v2, p1

    invoke-direct {v0, v1, v2}, Ljava/text/ParseException;-><init>(Ljava/lang/String;I)V

    throw v0

    .line 1629
    .end local v17    # "endYearStg":Ljava/lang/String;
    .end local v18    # "endMonthStg":Ljava/lang/String;
    .end local v19    # "ex":Ljava/lang/NumberFormatException;
    :goto_160
    goto :goto_184

    .line 1630
    :cond_161
    new-instance v0, Ljava/text/ParseException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "End year/month has incorrect format at line "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move/from16 v2, p1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    move/from16 v2, p1

    invoke-direct {v0, v1, v2}, Ljava/text/ParseException;-><init>(Ljava/lang/String;I)V

    throw v0

    .line 1634
    :goto_184
    const/4 v0, -0x1

    if-eq v13, v0, :cond_198

    const/4 v0, -0x1

    if-eq v15, v0, :cond_198

    const/4 v0, -0x1

    if-eq v14, v0, :cond_198

    move/from16 v0, v16

    const/4 v1, -0x1

    if-eq v0, v1, :cond_198

    .line 1636
    move/from16 v0, v16

    invoke-static {v13, v15, v14, v0, v7}, Lorg/threeten/bp/chrono/HijrahDate;->addDeviationAsHijrah(IIIII)V

    goto :goto_1bb

    .line 1639
    :cond_198
    new-instance v0, Ljava/text/ParseException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown error at line "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move/from16 v2, p1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    move/from16 v2, p1

    invoke-direct {v0, v1, v2}, Ljava/text/ParseException;-><init>(Ljava/lang/String;I)V

    throw v0

    .line 1642
    .end local v9    # "startDateStg":Ljava/lang/String;
    .end local v10    # "endDateStg":Ljava/lang/String;
    .end local v11    # "startDateYearSepIndex":I
    .end local v12    # "endDateYearSepIndex":I
    .end local v13    # "startYear":I
    .end local v14    # "endYear":I
    .end local v15    # "startMonth":I
    .end local v16    # "endMonth":I
    :goto_1bb
    goto :goto_1df

    .line 1643
    :cond_1bc
    new-instance v0, Ljava/text/ParseException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Start and end year/month has incorrect format at line "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move/from16 v2, p1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    move/from16 v2, p1

    invoke-direct {v0, v1, v2}, Ljava/text/ParseException;-><init>(Ljava/lang/String;I)V

    throw v0

    .line 1647
    .end local v6    # "offsetString":Ljava/lang/String;
    .end local v7    # "offset":I
    .end local v8    # "separatorIndex":I
    :goto_1df
    goto :goto_203

    .line 1648
    :cond_1e0
    new-instance v0, Ljava/text/ParseException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Offset has incorrect format at line "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move/from16 v2, p1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    move/from16 v2, p1

    invoke-direct {v0, v1, v2}, Ljava/text/ParseException;-><init>(Ljava/lang/String;I)V

    throw v0

    .line 1651
    .end local v4    # "deviationElement":Ljava/lang/String;
    .end local v5    # "offsetIndex":I
    :goto_203
    goto/16 :goto_9

    .line 1652
    :cond_205
    return-void
.end method

.method private static readDeviationConfig()V
    .registers 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/text/ParseException;
        }
    .end annotation

    .line 1530
    invoke-static {}, Lorg/threeten/bp/chrono/HijrahDate;->getConfigFileInputStream()Ljava/io/InputStream;

    move-result-object v2

    .line 1531
    .local v2, "is":Ljava/io/InputStream;
    if-eqz v2, :cond_32

    .line 1532
    const/4 v3, 0x0

    .line 1534
    .local v3, "br":Ljava/io/BufferedReader;
    :try_start_7
    new-instance v0, Ljava/io/BufferedReader;

    new-instance v1, Ljava/io/InputStreamReader;

    invoke-direct {v1, v2}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v0, v1}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    move-object v3, v0

    .line 1535
    const-string v4, ""

    .line 1536
    .local v4, "line":Ljava/lang/String;
    const/4 v5, 0x0

    .line 1537
    .local v5, "num":I
    :goto_15
    invoke-virtual {v3}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_25

    .line 1538
    add-int/lit8 v5, v5, 0x1

    .line 1539
    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    .line 1540
    invoke-static {v4, v5}, Lorg/threeten/bp/chrono/HijrahDate;->parseLine(Ljava/lang/String;I)V
    :try_end_24
    .catchall {:try_start_7 .. :try_end_24} :catchall_2b

    goto :goto_15

    .line 1543
    .end local v4    # "line":Ljava/lang/String;
    .end local v5    # "num":I
    :cond_25
    if-eqz v3, :cond_32

    .line 1544
    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V

    goto :goto_32

    .line 1543
    :catchall_2b
    move-exception v6

    if-eqz v3, :cond_31

    .line 1544
    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V

    :cond_31
    throw v6

    .line 1548
    .end local v3    # "br":Ljava/io/BufferedReader;
    :cond_32
    :goto_32
    return-void
.end method

.method static readExternal(Ljava/io/DataInput;)Lorg/threeten/bp/chrono/ChronoLocalDate;
    .registers 5
    .param p0, "in"    # Ljava/io/DataInput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1770
    invoke-interface {p0}, Ljava/io/DataInput;->readInt()I

    move-result v1

    .line 1771
    .local v1, "year":I
    invoke-interface {p0}, Ljava/io/DataInput;->readByte()B

    move-result v2

    .line 1772
    .local v2, "month":I
    invoke-interface {p0}, Ljava/io/DataInput;->readByte()B

    move-result v3

    .line 1773
    .local v3, "dayOfMonth":I
    sget-object v0, Lorg/threeten/bp/chrono/HijrahChronology;->INSTANCE:Lorg/threeten/bp/chrono/HijrahChronology;

    invoke-virtual {v0, v1, v2, v3}, Lorg/threeten/bp/chrono/HijrahChronology;->date(III)Lorg/threeten/bp/chrono/HijrahDate;

    move-result-object v0

    return-object v0
.end method

.method private readResolve()Ljava/lang/Object;
    .registers 4

    .line 612
    new-instance v0, Lorg/threeten/bp/chrono/HijrahDate;

    iget-wide v1, p0, Lorg/threeten/bp/chrono/HijrahDate;->gregorianEpochDay:J

    invoke-direct {v0, v1, v2}, Lorg/threeten/bp/chrono/HijrahDate;-><init>(J)V

    return-object v0
.end method

.method private static resolvePreviousValid(III)Lorg/threeten/bp/chrono/HijrahDate;
    .registers 5
    .param p0, "yearOfEra"    # I
    .param p1, "month"    # I
    .param p2, "day"    # I

    .line 698
    add-int/lit8 v0, p1, -0x1

    invoke-static {v0, p0}, Lorg/threeten/bp/chrono/HijrahDate;->getMonthDays(II)I

    move-result v1

    .line 699
    .local v1, "monthDays":I
    if-le p2, v1, :cond_9

    .line 700
    move p2, v1

    .line 702
    :cond_9
    invoke-static {p0, p1, p2}, Lorg/threeten/bp/chrono/HijrahDate;->of(III)Lorg/threeten/bp/chrono/HijrahDate;

    move-result-object v0

    return-object v0
.end method

.method private writeReplace()Ljava/lang/Object;
    .registers 3

    .line 1759
    new-instance v0, Lorg/threeten/bp/chrono/Ser;

    const/4 v1, 0x3

    invoke-direct {v0, v1, p0}, Lorg/threeten/bp/chrono/Ser;-><init>(BLjava/lang/Object;)V

    return-object v0
.end method

.method private static yearToGregorianEpochDay(I)J
    .registers 10
    .param p0, "prolepticYear"    # I

    .line 865
    add-int/lit8 v0, p0, -0x1

    div-int/lit8 v4, v0, 0x1e

    .line 866
    .local v4, "cycleNumber":I
    add-int/lit8 v0, p0, -0x1

    rem-int/lit8 v5, v0, 0x1e

    .line 868
    .local v5, "yearInCycle":I
    invoke-static {v4}, Lorg/threeten/bp/chrono/HijrahDate;->getAdjustedCycle(I)[Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v5}, Ljava/lang/Math;->abs(I)I

    move-result v1

    aget-object v0, v0, v1

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v6

    .line 871
    .local v6, "dayInCycle":I
    if-gez v5, :cond_19

    .line 872
    neg-int v6, v6

    .line 878
    :cond_19
    :try_start_19
    sget-object v0, Lorg/threeten/bp/chrono/HijrahDate;->ADJUSTED_CYCLES:[Ljava/lang/Long;

    aget-object v7, v0, v4
    :try_end_1d
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_19 .. :try_end_1d} :catch_1e

    .line 881
    .local v7, "cycleDays":Ljava/lang/Long;
    goto :goto_20

    .line 879
    :catch_1e
    move-exception v8

    .line 880
    .local v8, "e":Ljava/lang/ArrayIndexOutOfBoundsException;
    const/4 v7, 0x0

    .line 883
    .end local v8    # "e":Ljava/lang/ArrayIndexOutOfBoundsException;
    :goto_20
    if-nez v7, :cond_2a

    .line 884
    new-instance v7, Ljava/lang/Long;

    mul-int/lit16 v0, v4, 0x2987

    int-to-long v0, v0

    invoke-direct {v7, v0, v1}, Ljava/lang/Long;-><init>(J)V

    .line 887
    :cond_2a
    invoke-virtual {v7}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    int-to-long v2, v6

    add-long/2addr v0, v2

    const-wide/32 v2, -0x78274

    add-long/2addr v0, v2

    const-wide/16 v2, 0x1

    sub-long/2addr v0, v2

    return-wide v0
.end method


# virtual methods
.method public final atTime(Lorg/threeten/bp/LocalTime;)Lorg/threeten/bp/chrono/ChronoLocalDateTime;
    .registers 3
    .param p1, "localTime"    # Lorg/threeten/bp/LocalTime;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Lorg/threeten/bp/LocalTime;)Lorg/threeten/bp/chrono/ChronoLocalDateTime<Lorg/threeten/bp/chrono/HijrahDate;>;"
        }
    .end annotation

    .line 729
    invoke-super {p0, p1}, Lorg/threeten/bp/chrono/ChronoDateImpl;->atTime(Lorg/threeten/bp/LocalTime;)Lorg/threeten/bp/chrono/ChronoLocalDateTime;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getChronology()Lorg/threeten/bp/chrono/Chronology;
    .registers 2

    .line 111
    invoke-virtual {p0}, Lorg/threeten/bp/chrono/HijrahDate;->getChronology()Lorg/threeten/bp/chrono/HijrahChronology;

    move-result-object v0

    return-object v0
.end method

.method public getChronology()Lorg/threeten/bp/chrono/HijrahChronology;
    .registers 2

    .line 618
    sget-object v0, Lorg/threeten/bp/chrono/HijrahChronology;->INSTANCE:Lorg/threeten/bp/chrono/HijrahChronology;

    return-object v0
.end method

.method public bridge synthetic getEra()Lorg/threeten/bp/chrono/Era;
    .registers 2

    .line 111
    invoke-virtual {p0}, Lorg/threeten/bp/chrono/HijrahDate;->getEra()Lorg/threeten/bp/chrono/HijrahEra;

    move-result-object v0

    return-object v0
.end method

.method public getEra()Lorg/threeten/bp/chrono/HijrahEra;
    .registers 2

    .line 623
    iget-object v0, p0, Lorg/threeten/bp/chrono/HijrahDate;->era:Lorg/threeten/bp/chrono/HijrahEra;

    return-object v0
.end method

.method public getLong(Lorg/threeten/bp/temporal/TemporalField;)J
    .registers 5
    .param p1, "field"    # Lorg/threeten/bp/temporal/TemporalField;

    .line 646
    instance-of v0, p1, Lorg/threeten/bp/temporal/ChronoField;

    if-eqz v0, :cond_82

    .line 647
    sget-object v0, Lorg/threeten/bp/chrono/HijrahDate$1;->$SwitchMap$org$threeten$bp$temporal$ChronoField:[I

    move-object v1, p1

    check-cast v1, Lorg/threeten/bp/temporal/ChronoField;

    invoke-virtual {v1}, Lorg/threeten/bp/temporal/ChronoField;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_88

    goto/16 :goto_69

    .line 648
    :pswitch_14
    iget-object v0, p0, Lorg/threeten/bp/chrono/HijrahDate;->dayOfWeek:Lorg/threeten/bp/DayOfWeek;

    invoke-virtual {v0}, Lorg/threeten/bp/DayOfWeek;->getValue()I

    move-result v0

    int-to-long v0, v0

    return-wide v0

    .line 649
    :pswitch_1c
    iget-object v0, p0, Lorg/threeten/bp/chrono/HijrahDate;->dayOfWeek:Lorg/threeten/bp/DayOfWeek;

    invoke-virtual {v0}, Lorg/threeten/bp/DayOfWeek;->getValue()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    rem-int/lit8 v0, v0, 0x7

    add-int/lit8 v0, v0, 0x1

    int-to-long v0, v0

    return-wide v0

    .line 650
    :pswitch_2a
    iget v0, p0, Lorg/threeten/bp/chrono/HijrahDate;->dayOfYear:I

    add-int/lit8 v0, v0, -0x1

    rem-int/lit8 v0, v0, 0x7

    add-int/lit8 v0, v0, 0x1

    int-to-long v0, v0

    return-wide v0

    .line 651
    :pswitch_34
    iget v0, p0, Lorg/threeten/bp/chrono/HijrahDate;->dayOfMonth:I

    int-to-long v0, v0

    return-wide v0

    .line 652
    :pswitch_38
    iget v0, p0, Lorg/threeten/bp/chrono/HijrahDate;->dayOfYear:I

    int-to-long v0, v0

    return-wide v0

    .line 653
    :pswitch_3c
    invoke-virtual {p0}, Lorg/threeten/bp/chrono/HijrahDate;->toEpochDay()J

    move-result-wide v0

    return-wide v0

    .line 654
    :pswitch_41
    iget v0, p0, Lorg/threeten/bp/chrono/HijrahDate;->dayOfMonth:I

    add-int/lit8 v0, v0, -0x1

    div-int/lit8 v0, v0, 0x7

    add-int/lit8 v0, v0, 0x1

    int-to-long v0, v0

    return-wide v0

    .line 655
    :pswitch_4b
    iget v0, p0, Lorg/threeten/bp/chrono/HijrahDate;->dayOfYear:I

    add-int/lit8 v0, v0, -0x1

    div-int/lit8 v0, v0, 0x7

    add-int/lit8 v0, v0, 0x1

    int-to-long v0, v0

    return-wide v0

    .line 656
    :pswitch_55
    iget v0, p0, Lorg/threeten/bp/chrono/HijrahDate;->monthOfYear:I

    int-to-long v0, v0

    return-wide v0

    .line 657
    :pswitch_59
    iget v0, p0, Lorg/threeten/bp/chrono/HijrahDate;->yearOfEra:I

    int-to-long v0, v0

    return-wide v0

    .line 658
    :pswitch_5d
    iget v0, p0, Lorg/threeten/bp/chrono/HijrahDate;->yearOfEra:I

    int-to-long v0, v0

    return-wide v0

    .line 659
    :pswitch_61
    iget-object v0, p0, Lorg/threeten/bp/chrono/HijrahDate;->era:Lorg/threeten/bp/chrono/HijrahEra;

    invoke-virtual {v0}, Lorg/threeten/bp/chrono/HijrahEra;->getValue()I

    move-result v0

    int-to-long v0, v0

    return-wide v0

    .line 661
    :goto_69
    new-instance v0, Lorg/threeten/bp/temporal/UnsupportedTemporalTypeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unsupported field: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/threeten/bp/temporal/UnsupportedTemporalTypeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 663
    :cond_82
    invoke-interface {p1, p0}, Lorg/threeten/bp/temporal/TemporalField;->getFrom(Lorg/threeten/bp/temporal/TemporalAccessor;)J

    move-result-wide v0

    return-wide v0

    nop

    :pswitch_data_88
    .packed-switch 0x1
        :pswitch_34
        :pswitch_38
        :pswitch_41
        :pswitch_59
        :pswitch_14
        :pswitch_1c
        :pswitch_2a
        :pswitch_3c
        :pswitch_4b
        :pswitch_55
        :pswitch_5d
        :pswitch_61
    .end packed-switch
.end method

.method public isLeapYear()Z
    .registers 2

    .line 745
    iget-boolean v0, p0, Lorg/threeten/bp/chrono/HijrahDate;->isLeapYear:Z

    return v0
.end method

.method public lengthOfMonth()I
    .registers 3

    .line 1143
    iget v0, p0, Lorg/threeten/bp/chrono/HijrahDate;->monthOfYear:I

    add-int/lit8 v0, v0, -0x1

    iget v1, p0, Lorg/threeten/bp/chrono/HijrahDate;->yearOfEra:I

    invoke-static {v0, v1}, Lorg/threeten/bp/chrono/HijrahDate;->getMonthLength(II)I

    move-result v0

    return v0
.end method

.method public lengthOfYear()I
    .registers 2

    .line 1177
    iget v0, p0, Lorg/threeten/bp/chrono/HijrahDate;->yearOfEra:I

    invoke-static {v0}, Lorg/threeten/bp/chrono/HijrahDate;->getYearLength(I)I

    move-result v0

    return v0
.end method

.method public bridge synthetic minus(JLorg/threeten/bp/temporal/TemporalUnit;)Lorg/threeten/bp/chrono/ChronoLocalDate;
    .registers 5
    .param p1, "x0"    # J
    .param p3, "x1"    # Lorg/threeten/bp/temporal/TemporalUnit;

    .line 111
    invoke-virtual {p0, p1, p2, p3}, Lorg/threeten/bp/chrono/HijrahDate;->minus(JLorg/threeten/bp/temporal/TemporalUnit;)Lorg/threeten/bp/chrono/HijrahDate;

    move-result-object v0

    return-object v0
.end method

.method public minus(JLorg/threeten/bp/temporal/TemporalUnit;)Lorg/threeten/bp/chrono/HijrahDate;
    .registers 5
    .param p1, "amountToAdd"    # J
    .param p3, "unit"    # Lorg/threeten/bp/temporal/TemporalUnit;

    .line 722
    invoke-super {p0, p1, p2, p3}, Lorg/threeten/bp/chrono/ChronoDateImpl;->minus(JLorg/threeten/bp/temporal/TemporalUnit;)Lorg/threeten/bp/chrono/ChronoLocalDate;

    move-result-object v0

    check-cast v0, Lorg/threeten/bp/chrono/HijrahDate;

    return-object v0
.end method

.method public bridge synthetic minus(JLorg/threeten/bp/temporal/TemporalUnit;)Lorg/threeten/bp/temporal/Temporal;
    .registers 5
    .param p1, "x0"    # J
    .param p3, "x1"    # Lorg/threeten/bp/temporal/TemporalUnit;

    .line 111
    invoke-virtual {p0, p1, p2, p3}, Lorg/threeten/bp/chrono/HijrahDate;->minus(JLorg/threeten/bp/temporal/TemporalUnit;)Lorg/threeten/bp/chrono/HijrahDate;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic plus(JLorg/threeten/bp/temporal/TemporalUnit;)Lorg/threeten/bp/chrono/ChronoDateImpl;
    .registers 5
    .param p1, "x0"    # J
    .param p3, "x1"    # Lorg/threeten/bp/temporal/TemporalUnit;

    .line 111
    invoke-virtual {p0, p1, p2, p3}, Lorg/threeten/bp/chrono/HijrahDate;->plus(JLorg/threeten/bp/temporal/TemporalUnit;)Lorg/threeten/bp/chrono/HijrahDate;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic plus(JLorg/threeten/bp/temporal/TemporalUnit;)Lorg/threeten/bp/chrono/ChronoLocalDate;
    .registers 5
    .param p1, "x0"    # J
    .param p3, "x1"    # Lorg/threeten/bp/temporal/TemporalUnit;

    .line 111
    invoke-virtual {p0, p1, p2, p3}, Lorg/threeten/bp/chrono/HijrahDate;->plus(JLorg/threeten/bp/temporal/TemporalUnit;)Lorg/threeten/bp/chrono/HijrahDate;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic plus(Lorg/threeten/bp/temporal/TemporalAmount;)Lorg/threeten/bp/chrono/ChronoLocalDate;
    .registers 3
    .param p1, "x0"    # Lorg/threeten/bp/temporal/TemporalAmount;

    .line 111
    invoke-virtual {p0, p1}, Lorg/threeten/bp/chrono/HijrahDate;->plus(Lorg/threeten/bp/temporal/TemporalAmount;)Lorg/threeten/bp/chrono/HijrahDate;

    move-result-object v0

    return-object v0
.end method

.method public plus(JLorg/threeten/bp/temporal/TemporalUnit;)Lorg/threeten/bp/chrono/HijrahDate;
    .registers 5
    .param p1, "amountToAdd"    # J
    .param p3, "unit"    # Lorg/threeten/bp/temporal/TemporalUnit;

    .line 712
    invoke-super {p0, p1, p2, p3}, Lorg/threeten/bp/chrono/ChronoDateImpl;->plus(JLorg/threeten/bp/temporal/TemporalUnit;)Lorg/threeten/bp/chrono/ChronoDateImpl;

    move-result-object v0

    check-cast v0, Lorg/threeten/bp/chrono/HijrahDate;

    return-object v0
.end method

.method public plus(Lorg/threeten/bp/temporal/TemporalAmount;)Lorg/threeten/bp/chrono/HijrahDate;
    .registers 3
    .param p1, "amount"    # Lorg/threeten/bp/temporal/TemporalAmount;

    .line 707
    invoke-super {p0, p1}, Lorg/threeten/bp/chrono/ChronoDateImpl;->plus(Lorg/threeten/bp/temporal/TemporalAmount;)Lorg/threeten/bp/chrono/ChronoLocalDate;

    move-result-object v0

    check-cast v0, Lorg/threeten/bp/chrono/HijrahDate;

    return-object v0
.end method

.method public bridge synthetic plus(JLorg/threeten/bp/temporal/TemporalUnit;)Lorg/threeten/bp/temporal/Temporal;
    .registers 5
    .param p1, "x0"    # J
    .param p3, "x1"    # Lorg/threeten/bp/temporal/TemporalUnit;

    .line 111
    invoke-virtual {p0, p1, p2, p3}, Lorg/threeten/bp/chrono/HijrahDate;->plus(JLorg/threeten/bp/temporal/TemporalUnit;)Lorg/threeten/bp/chrono/HijrahDate;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic plus(Lorg/threeten/bp/temporal/TemporalAmount;)Lorg/threeten/bp/temporal/Temporal;
    .registers 3
    .param p1, "x0"    # Lorg/threeten/bp/temporal/TemporalAmount;

    .line 111
    invoke-virtual {p0, p1}, Lorg/threeten/bp/chrono/HijrahDate;->plus(Lorg/threeten/bp/temporal/TemporalAmount;)Lorg/threeten/bp/chrono/HijrahDate;

    move-result-object v0

    return-object v0
.end method

.method bridge synthetic plusDays(J)Lorg/threeten/bp/chrono/ChronoDateImpl;
    .registers 4
    .param p1, "x0"    # J

    .line 111
    invoke-virtual {p0, p1, p2}, Lorg/threeten/bp/chrono/HijrahDate;->plusDays(J)Lorg/threeten/bp/chrono/HijrahDate;

    move-result-object v0

    return-object v0
.end method

.method plusDays(J)Lorg/threeten/bp/chrono/HijrahDate;
    .registers 6
    .param p1, "days"    # J

    .line 777
    new-instance v0, Lorg/threeten/bp/chrono/HijrahDate;

    iget-wide v1, p0, Lorg/threeten/bp/chrono/HijrahDate;->gregorianEpochDay:J

    add-long/2addr v1, p1

    invoke-direct {v0, v1, v2}, Lorg/threeten/bp/chrono/HijrahDate;-><init>(J)V

    return-object v0
.end method

.method bridge synthetic plusMonths(J)Lorg/threeten/bp/chrono/ChronoDateImpl;
    .registers 4
    .param p1, "x0"    # J

    .line 111
    invoke-virtual {p0, p1, p2}, Lorg/threeten/bp/chrono/HijrahDate;->plusMonths(J)Lorg/threeten/bp/chrono/HijrahDate;

    move-result-object v0

    return-object v0
.end method

.method plusMonths(J)Lorg/threeten/bp/chrono/HijrahDate;
    .registers 9
    .param p1, "months"    # J

    .line 760
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-nez v0, :cond_7

    .line 761
    return-object p0

    .line 763
    :cond_7
    iget v0, p0, Lorg/threeten/bp/chrono/HijrahDate;->monthOfYear:I

    add-int/lit8 v3, v0, -0x1

    .line 764
    .local v3, "newMonth":I
    long-to-int v0, p1

    add-int/2addr v3, v0

    .line 765
    div-int/lit8 v4, v3, 0xc

    .line 766
    .local v4, "years":I
    rem-int/lit8 v3, v3, 0xc

    .line 767
    :goto_11
    if-gez v3, :cond_1b

    .line 768
    add-int/lit8 v3, v3, 0xc

    .line 769
    const/4 v0, 0x1

    invoke-static {v4, v0}, Lorg/threeten/bp/jdk8/Jdk8Methods;->safeSubtract(II)I

    move-result v4

    goto :goto_11

    .line 771
    :cond_1b
    iget v0, p0, Lorg/threeten/bp/chrono/HijrahDate;->yearOfEra:I

    invoke-static {v0, v4}, Lorg/threeten/bp/jdk8/Jdk8Methods;->safeAdd(II)I

    move-result v5

    .line 772
    .local v5, "newYear":I
    iget-object v0, p0, Lorg/threeten/bp/chrono/HijrahDate;->era:Lorg/threeten/bp/chrono/HijrahEra;

    add-int/lit8 v1, v3, 0x1

    iget v2, p0, Lorg/threeten/bp/chrono/HijrahDate;->dayOfMonth:I

    invoke-static {v0, v5, v1, v2}, Lorg/threeten/bp/chrono/HijrahDate;->of(Lorg/threeten/bp/chrono/HijrahEra;III)Lorg/threeten/bp/chrono/HijrahDate;

    move-result-object v0

    return-object v0
.end method

.method bridge synthetic plusYears(J)Lorg/threeten/bp/chrono/ChronoDateImpl;
    .registers 4
    .param p1, "x0"    # J

    .line 111
    invoke-virtual {p0, p1, p2}, Lorg/threeten/bp/chrono/HijrahDate;->plusYears(J)Lorg/threeten/bp/chrono/HijrahDate;

    move-result-object v0

    return-object v0
.end method

.method plusYears(J)Lorg/threeten/bp/chrono/HijrahDate;
    .registers 7
    .param p1, "years"    # J

    .line 751
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-nez v0, :cond_7

    .line 752
    return-object p0

    .line 754
    :cond_7
    iget v0, p0, Lorg/threeten/bp/chrono/HijrahDate;->yearOfEra:I

    long-to-int v1, p1

    invoke-static {v0, v1}, Lorg/threeten/bp/jdk8/Jdk8Methods;->safeAdd(II)I

    move-result v3

    .line 755
    .local v3, "newYear":I
    iget-object v0, p0, Lorg/threeten/bp/chrono/HijrahDate;->era:Lorg/threeten/bp/chrono/HijrahEra;

    iget v1, p0, Lorg/threeten/bp/chrono/HijrahDate;->monthOfYear:I

    iget v2, p0, Lorg/threeten/bp/chrono/HijrahDate;->dayOfMonth:I

    invoke-static {v0, v3, v1, v2}, Lorg/threeten/bp/chrono/HijrahDate;->of(Lorg/threeten/bp/chrono/HijrahEra;III)Lorg/threeten/bp/chrono/HijrahDate;

    move-result-object v0

    return-object v0
.end method

.method public range(Lorg/threeten/bp/temporal/TemporalField;)Lorg/threeten/bp/temporal/ValueRange;
    .registers 7
    .param p1, "field"    # Lorg/threeten/bp/temporal/TemporalField;

    .line 628
    instance-of v0, p1, Lorg/threeten/bp/temporal/ChronoField;

    if-eqz v0, :cond_65

    .line 629
    invoke-virtual {p0, p1}, Lorg/threeten/bp/chrono/HijrahDate;->isSupported(Lorg/threeten/bp/temporal/TemporalField;)Z

    move-result v0

    if-eqz v0, :cond_4c

    .line 630
    move-object v4, p1

    check-cast v4, Lorg/threeten/bp/temporal/ChronoField;

    .line 631
    .local v4, "f":Lorg/threeten/bp/temporal/ChronoField;
    sget-object v0, Lorg/threeten/bp/chrono/HijrahDate$1;->$SwitchMap$org$threeten$bp$temporal$ChronoField:[I

    invoke-virtual {v4}, Lorg/threeten/bp/temporal/ChronoField;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_6a

    goto :goto_43

    .line 632
    :pswitch_19
    invoke-virtual {p0}, Lorg/threeten/bp/chrono/HijrahDate;->lengthOfMonth()I

    move-result v0

    int-to-long v0, v0

    const-wide/16 v2, 0x1

    invoke-static {v2, v3, v0, v1}, Lorg/threeten/bp/temporal/ValueRange;->of(JJ)Lorg/threeten/bp/temporal/ValueRange;

    move-result-object v0

    return-object v0

    .line 633
    :pswitch_25
    invoke-virtual {p0}, Lorg/threeten/bp/chrono/HijrahDate;->lengthOfYear()I

    move-result v0

    int-to-long v0, v0

    const-wide/16 v2, 0x1

    invoke-static {v2, v3, v0, v1}, Lorg/threeten/bp/temporal/ValueRange;->of(JJ)Lorg/threeten/bp/temporal/ValueRange;

    move-result-object v0

    return-object v0

    .line 634
    :pswitch_31
    const-wide/16 v0, 0x1

    const-wide/16 v2, 0x5

    invoke-static {v0, v1, v2, v3}, Lorg/threeten/bp/temporal/ValueRange;->of(JJ)Lorg/threeten/bp/temporal/ValueRange;

    move-result-object v0

    return-object v0

    .line 635
    :pswitch_3a
    const-wide/16 v0, 0x1

    const-wide/16 v2, 0x3e8

    invoke-static {v0, v1, v2, v3}, Lorg/threeten/bp/temporal/ValueRange;->of(JJ)Lorg/threeten/bp/temporal/ValueRange;

    move-result-object v0

    return-object v0

    .line 637
    :goto_43
    invoke-virtual {p0}, Lorg/threeten/bp/chrono/HijrahDate;->getChronology()Lorg/threeten/bp/chrono/HijrahChronology;

    move-result-object v0

    invoke-virtual {v0, v4}, Lorg/threeten/bp/chrono/HijrahChronology;->range(Lorg/threeten/bp/temporal/ChronoField;)Lorg/threeten/bp/temporal/ValueRange;

    move-result-object v0

    return-object v0

    .line 639
    .end local v4    # "f":Lorg/threeten/bp/temporal/ChronoField;
    :cond_4c
    new-instance v0, Lorg/threeten/bp/temporal/UnsupportedTemporalTypeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unsupported field: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/threeten/bp/temporal/UnsupportedTemporalTypeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 641
    :cond_65
    invoke-interface {p1, p0}, Lorg/threeten/bp/temporal/TemporalField;->rangeRefinedBy(Lorg/threeten/bp/temporal/TemporalAccessor;)Lorg/threeten/bp/temporal/ValueRange;

    move-result-object v0

    return-object v0

    :pswitch_data_6a
    .packed-switch 0x1
        :pswitch_19
        :pswitch_25
        :pswitch_31
        :pswitch_3a
    .end packed-switch
.end method

.method public toEpochDay()J
    .registers 4

    .line 734
    iget v0, p0, Lorg/threeten/bp/chrono/HijrahDate;->yearOfEra:I

    iget v1, p0, Lorg/threeten/bp/chrono/HijrahDate;->monthOfYear:I

    iget v2, p0, Lorg/threeten/bp/chrono/HijrahDate;->dayOfMonth:I

    invoke-static {v0, v1, v2}, Lorg/threeten/bp/chrono/HijrahDate;->getGregorianEpochDay(III)J

    move-result-wide v0

    return-wide v0
.end method

.method public bridge synthetic with(Lorg/threeten/bp/temporal/TemporalAdjuster;)Lorg/threeten/bp/chrono/ChronoLocalDate;
    .registers 3
    .param p1, "x0"    # Lorg/threeten/bp/temporal/TemporalAdjuster;

    .line 111
    invoke-virtual {p0, p1}, Lorg/threeten/bp/chrono/HijrahDate;->with(Lorg/threeten/bp/temporal/TemporalAdjuster;)Lorg/threeten/bp/chrono/HijrahDate;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic with(Lorg/threeten/bp/temporal/TemporalField;J)Lorg/threeten/bp/chrono/ChronoLocalDate;
    .registers 5
    .param p1, "x0"    # Lorg/threeten/bp/temporal/TemporalField;
    .param p2, "x1"    # J

    .line 111
    invoke-virtual {p0, p1, p2, p3}, Lorg/threeten/bp/chrono/HijrahDate;->with(Lorg/threeten/bp/temporal/TemporalField;J)Lorg/threeten/bp/chrono/HijrahDate;

    move-result-object v0

    return-object v0
.end method

.method public with(Lorg/threeten/bp/temporal/TemporalAdjuster;)Lorg/threeten/bp/chrono/HijrahDate;
    .registers 3
    .param p1, "adjuster"    # Lorg/threeten/bp/temporal/TemporalAdjuster;

    .line 669
    invoke-super {p0, p1}, Lorg/threeten/bp/chrono/ChronoDateImpl;->with(Lorg/threeten/bp/temporal/TemporalAdjuster;)Lorg/threeten/bp/chrono/ChronoLocalDate;

    move-result-object v0

    check-cast v0, Lorg/threeten/bp/chrono/HijrahDate;

    return-object v0
.end method

.method public with(Lorg/threeten/bp/temporal/TemporalField;J)Lorg/threeten/bp/chrono/HijrahDate;
    .registers 10
    .param p1, "field"    # Lorg/threeten/bp/temporal/TemporalField;
    .param p2, "newValue"    # J

    .line 674
    instance-of v0, p1, Lorg/threeten/bp/temporal/ChronoField;

    if-eqz v0, :cond_cd

    .line 675
    move-object v4, p1

    check-cast v4, Lorg/threeten/bp/temporal/ChronoField;

    .line 676
    .local v4, "f":Lorg/threeten/bp/temporal/ChronoField;
    invoke-virtual {v4, p2, p3}, Lorg/threeten/bp/temporal/ChronoField;->checkValidValue(J)J

    .line 677
    long-to-int v5, p2

    .line 678
    .local v5, "nvalue":I
    sget-object v0, Lorg/threeten/bp/chrono/HijrahDate$1;->$SwitchMap$org$threeten$bp$temporal$ChronoField:[I

    invoke-virtual {v4}, Lorg/threeten/bp/temporal/ChronoField;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_d4

    goto/16 :goto_b4

    .line 679
    :pswitch_18
    iget-object v0, p0, Lorg/threeten/bp/chrono/HijrahDate;->dayOfWeek:Lorg/threeten/bp/DayOfWeek;

    invoke-virtual {v0}, Lorg/threeten/bp/DayOfWeek;->getValue()I

    move-result v0

    int-to-long v0, v0

    sub-long v0, p2, v0

    invoke-virtual {p0, v0, v1}, Lorg/threeten/bp/chrono/HijrahDate;->plusDays(J)Lorg/threeten/bp/chrono/HijrahDate;

    move-result-object v0

    return-object v0

    .line 680
    :pswitch_26
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->ALIGNED_DAY_OF_WEEK_IN_MONTH:Lorg/threeten/bp/temporal/ChronoField;

    invoke-virtual {p0, v0}, Lorg/threeten/bp/chrono/HijrahDate;->getLong(Lorg/threeten/bp/temporal/TemporalField;)J

    move-result-wide v0

    sub-long v0, p2, v0

    invoke-virtual {p0, v0, v1}, Lorg/threeten/bp/chrono/HijrahDate;->plusDays(J)Lorg/threeten/bp/chrono/HijrahDate;

    move-result-object v0

    return-object v0

    .line 681
    :pswitch_33
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->ALIGNED_DAY_OF_WEEK_IN_YEAR:Lorg/threeten/bp/temporal/ChronoField;

    invoke-virtual {p0, v0}, Lorg/threeten/bp/chrono/HijrahDate;->getLong(Lorg/threeten/bp/temporal/TemporalField;)J

    move-result-wide v0

    sub-long v0, p2, v0

    invoke-virtual {p0, v0, v1}, Lorg/threeten/bp/chrono/HijrahDate;->plusDays(J)Lorg/threeten/bp/chrono/HijrahDate;

    move-result-object v0

    return-object v0

    .line 682
    :pswitch_40
    iget v0, p0, Lorg/threeten/bp/chrono/HijrahDate;->yearOfEra:I

    iget v1, p0, Lorg/threeten/bp/chrono/HijrahDate;->monthOfYear:I

    invoke-static {v0, v1, v5}, Lorg/threeten/bp/chrono/HijrahDate;->resolvePreviousValid(III)Lorg/threeten/bp/chrono/HijrahDate;

    move-result-object v0

    return-object v0

    .line 683
    :pswitch_49
    iget v0, p0, Lorg/threeten/bp/chrono/HijrahDate;->yearOfEra:I

    add-int/lit8 v1, v5, -0x1

    div-int/lit8 v1, v1, 0x1e

    add-int/lit8 v1, v1, 0x1

    add-int/lit8 v2, v5, -0x1

    rem-int/lit8 v2, v2, 0x1e

    add-int/lit8 v2, v2, 0x1

    invoke-static {v0, v1, v2}, Lorg/threeten/bp/chrono/HijrahDate;->resolvePreviousValid(III)Lorg/threeten/bp/chrono/HijrahDate;

    move-result-object v0

    return-object v0

    .line 684
    :pswitch_5c
    new-instance v0, Lorg/threeten/bp/chrono/HijrahDate;

    int-to-long v1, v5

    invoke-direct {v0, v1, v2}, Lorg/threeten/bp/chrono/HijrahDate;-><init>(J)V

    return-object v0

    .line 685
    :pswitch_63
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->ALIGNED_WEEK_OF_MONTH:Lorg/threeten/bp/temporal/ChronoField;

    invoke-virtual {p0, v0}, Lorg/threeten/bp/chrono/HijrahDate;->getLong(Lorg/threeten/bp/temporal/TemporalField;)J

    move-result-wide v0

    sub-long v0, p2, v0

    const-wide/16 v2, 0x7

    mul-long/2addr v0, v2

    invoke-virtual {p0, v0, v1}, Lorg/threeten/bp/chrono/HijrahDate;->plusDays(J)Lorg/threeten/bp/chrono/HijrahDate;

    move-result-object v0

    return-object v0

    .line 686
    :pswitch_73
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->ALIGNED_WEEK_OF_YEAR:Lorg/threeten/bp/temporal/ChronoField;

    invoke-virtual {p0, v0}, Lorg/threeten/bp/chrono/HijrahDate;->getLong(Lorg/threeten/bp/temporal/TemporalField;)J

    move-result-wide v0

    sub-long v0, p2, v0

    const-wide/16 v2, 0x7

    mul-long/2addr v0, v2

    invoke-virtual {p0, v0, v1}, Lorg/threeten/bp/chrono/HijrahDate;->plusDays(J)Lorg/threeten/bp/chrono/HijrahDate;

    move-result-object v0

    return-object v0

    .line 687
    :pswitch_83
    iget v0, p0, Lorg/threeten/bp/chrono/HijrahDate;->yearOfEra:I

    iget v1, p0, Lorg/threeten/bp/chrono/HijrahDate;->dayOfMonth:I

    invoke-static {v0, v5, v1}, Lorg/threeten/bp/chrono/HijrahDate;->resolvePreviousValid(III)Lorg/threeten/bp/chrono/HijrahDate;

    move-result-object v0

    return-object v0

    .line 688
    :pswitch_8c
    iget v0, p0, Lorg/threeten/bp/chrono/HijrahDate;->yearOfEra:I

    const/4 v1, 0x1

    if-lt v0, v1, :cond_93

    move v0, v5

    goto :goto_95

    :cond_93
    rsub-int/lit8 v0, v5, 0x1

    :goto_95
    iget v1, p0, Lorg/threeten/bp/chrono/HijrahDate;->monthOfYear:I

    iget v2, p0, Lorg/threeten/bp/chrono/HijrahDate;->dayOfMonth:I

    invoke-static {v0, v1, v2}, Lorg/threeten/bp/chrono/HijrahDate;->resolvePreviousValid(III)Lorg/threeten/bp/chrono/HijrahDate;

    move-result-object v0

    return-object v0

    .line 689
    :pswitch_9e
    iget v0, p0, Lorg/threeten/bp/chrono/HijrahDate;->monthOfYear:I

    iget v1, p0, Lorg/threeten/bp/chrono/HijrahDate;->dayOfMonth:I

    invoke-static {v5, v0, v1}, Lorg/threeten/bp/chrono/HijrahDate;->resolvePreviousValid(III)Lorg/threeten/bp/chrono/HijrahDate;

    move-result-object v0

    return-object v0

    .line 690
    :pswitch_a7
    iget v0, p0, Lorg/threeten/bp/chrono/HijrahDate;->yearOfEra:I

    rsub-int/lit8 v0, v0, 0x1

    iget v1, p0, Lorg/threeten/bp/chrono/HijrahDate;->monthOfYear:I

    iget v2, p0, Lorg/threeten/bp/chrono/HijrahDate;->dayOfMonth:I

    invoke-static {v0, v1, v2}, Lorg/threeten/bp/chrono/HijrahDate;->resolvePreviousValid(III)Lorg/threeten/bp/chrono/HijrahDate;

    move-result-object v0

    return-object v0

    .line 692
    :goto_b4
    new-instance v0, Lorg/threeten/bp/temporal/UnsupportedTemporalTypeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unsupported field: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/threeten/bp/temporal/UnsupportedTemporalTypeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 694
    .end local v4    # "f":Lorg/threeten/bp/temporal/ChronoField;
    .end local v5    # "nvalue":I
    :cond_cd
    invoke-interface {p1, p0, p2, p3}, Lorg/threeten/bp/temporal/TemporalField;->adjustInto(Lorg/threeten/bp/temporal/Temporal;J)Lorg/threeten/bp/temporal/Temporal;

    move-result-object v0

    check-cast v0, Lorg/threeten/bp/chrono/HijrahDate;

    return-object v0

    :pswitch_data_d4
    .packed-switch 0x1
        :pswitch_40
        :pswitch_49
        :pswitch_63
        :pswitch_8c
        :pswitch_18
        :pswitch_26
        :pswitch_33
        :pswitch_5c
        :pswitch_73
        :pswitch_83
        :pswitch_9e
        :pswitch_a7
    .end packed-switch
.end method

.method public bridge synthetic with(Lorg/threeten/bp/temporal/TemporalAdjuster;)Lorg/threeten/bp/temporal/Temporal;
    .registers 3
    .param p1, "x0"    # Lorg/threeten/bp/temporal/TemporalAdjuster;

    .line 111
    invoke-virtual {p0, p1}, Lorg/threeten/bp/chrono/HijrahDate;->with(Lorg/threeten/bp/temporal/TemporalAdjuster;)Lorg/threeten/bp/chrono/HijrahDate;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic with(Lorg/threeten/bp/temporal/TemporalField;J)Lorg/threeten/bp/temporal/Temporal;
    .registers 5
    .param p1, "x0"    # Lorg/threeten/bp/temporal/TemporalField;
    .param p2, "x1"    # J

    .line 111
    invoke-virtual {p0, p1, p2, p3}, Lorg/threeten/bp/chrono/HijrahDate;->with(Lorg/threeten/bp/temporal/TemporalField;J)Lorg/threeten/bp/chrono/HijrahDate;

    move-result-object v0

    return-object v0
.end method

.method writeExternal(Ljava/io/DataOutput;)V
    .registers 3
    .param p1, "out"    # Ljava/io/DataOutput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1764
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->YEAR:Lorg/threeten/bp/temporal/ChronoField;

    invoke-virtual {p0, v0}, Lorg/threeten/bp/chrono/HijrahDate;->get(Lorg/threeten/bp/temporal/TemporalField;)I

    move-result v0

    invoke-interface {p1, v0}, Ljava/io/DataOutput;->writeInt(I)V

    .line 1765
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->MONTH_OF_YEAR:Lorg/threeten/bp/temporal/ChronoField;

    invoke-virtual {p0, v0}, Lorg/threeten/bp/chrono/HijrahDate;->get(Lorg/threeten/bp/temporal/TemporalField;)I

    move-result v0

    invoke-interface {p1, v0}, Ljava/io/DataOutput;->writeByte(I)V

    .line 1766
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->DAY_OF_MONTH:Lorg/threeten/bp/temporal/ChronoField;

    invoke-virtual {p0, v0}, Lorg/threeten/bp/chrono/HijrahDate;->get(Lorg/threeten/bp/temporal/TemporalField;)I

    move-result v0

    invoke-interface {p1, v0}, Ljava/io/DataOutput;->writeByte(I)V

    .line 1767
    return-void
.end method

.class public Lcom/itau/empresas/feature/recarga/efetivacao/model/ValorRecargaVO;
.super Ljava/lang/Object;
.source "ValorRecargaVO.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field valorBonus:Ljava/lang/Integer;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "valor_bonus"
    .end annotation
.end field

.field valorRecarga:Ljava/lang/Integer;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "valor_recarga"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getValorRecarga()Ljava/lang/Integer;
    .registers 2

    .line 15
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/model/ValorRecargaVO;->valorRecarga:Ljava/lang/Integer;

    return-object v0
.end method

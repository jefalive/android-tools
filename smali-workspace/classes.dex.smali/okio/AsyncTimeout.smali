.class public Lokio/AsyncTimeout;
.super Lokio/Timeout;
.source "AsyncTimeout.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lokio/AsyncTimeout$Watchdog;
    }
.end annotation


# static fields
.field private static head:Lokio/AsyncTimeout;


# instance fields
.field private inQueue:Z

.field private next:Lokio/AsyncTimeout;

.field private timeoutAt:J


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 42
    invoke-direct {p0}, Lokio/Timeout;-><init>()V

    return-void
.end method

.method static declared-synchronized awaitTimeout()Lokio/AsyncTimeout;
    .registers 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    const-class v7, Lokio/AsyncTimeout;

    monitor-enter v7

    .line 334
    :try_start_3
    sget-object v0, Lokio/AsyncTimeout;->head:Lokio/AsyncTimeout;

    iget-object v2, v0, Lokio/AsyncTimeout;->next:Lokio/AsyncTimeout;

    .line 337
    .local v2, "node":Lokio/AsyncTimeout;
    if-nez v2, :cond_11

    .line 338
    const-class v0, Lokio/AsyncTimeout;

    invoke-virtual {v0}, Ljava/lang/Object;->wait()V

    .line 339
    monitor-exit v7

    const/4 v0, 0x0

    return-object v0

    .line 342
    :cond_11
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    invoke-direct {v2, v0, v1}, Lokio/AsyncTimeout;->remainingNanos(J)J

    move-result-wide v3

    .line 345
    .local v3, "waitNanos":J
    const-wide/16 v0, 0x0

    cmp-long v0, v3, v0

    if-lez v0, :cond_32

    .line 348
    const-wide/32 v0, 0xf4240

    div-long v5, v3, v0

    .line 349
    .local v5, "waitMillis":J
    const-wide/32 v0, 0xf4240

    mul-long/2addr v0, v5

    sub-long/2addr v3, v0

    .line 350
    const-class v0, Lokio/AsyncTimeout;

    long-to-int v1, v3

    invoke-virtual {v0, v5, v6, v1}, Ljava/lang/Object;->wait(JI)V

    .line 351
    monitor-exit v7

    const/4 v0, 0x0

    return-object v0

    .line 355
    .end local v5    # "waitMillis":J
    :cond_32
    sget-object v0, Lokio/AsyncTimeout;->head:Lokio/AsyncTimeout;

    iget-object v1, v2, Lokio/AsyncTimeout;->next:Lokio/AsyncTimeout;

    iput-object v1, v0, Lokio/AsyncTimeout;->next:Lokio/AsyncTimeout;

    .line 356
    const/4 v0, 0x0

    iput-object v0, v2, Lokio/AsyncTimeout;->next:Lokio/AsyncTimeout;
    :try_end_3b
    .catchall {:try_start_3 .. :try_end_3b} :catchall_3d

    .line 357
    monitor-exit v7

    return-object v2

    .end local v2    # "node":Lokio/AsyncTimeout;
    .end local v3    # "waitNanos":J
    :catchall_3d
    move-exception v2

    monitor-exit v7

    throw v2
.end method

.method private static declared-synchronized cancelScheduledTimeout(Lokio/AsyncTimeout;)Z
    .registers 4
    .param p0, "node"    # Lokio/AsyncTimeout;

    const-class v2, Lokio/AsyncTimeout;

    monitor-enter v2

    .line 127
    :try_start_3
    sget-object v1, Lokio/AsyncTimeout;->head:Lokio/AsyncTimeout;

    .local v1, "prev":Lokio/AsyncTimeout;
    :goto_5
    if-eqz v1, :cond_18

    .line 128
    iget-object v0, v1, Lokio/AsyncTimeout;->next:Lokio/AsyncTimeout;

    if-ne v0, p0, :cond_15

    .line 129
    iget-object v0, p0, Lokio/AsyncTimeout;->next:Lokio/AsyncTimeout;

    iput-object v0, v1, Lokio/AsyncTimeout;->next:Lokio/AsyncTimeout;

    .line 130
    const/4 v0, 0x0

    iput-object v0, p0, Lokio/AsyncTimeout;->next:Lokio/AsyncTimeout;

    .line 131
    monitor-exit v2

    const/4 v0, 0x0

    return v0

    .line 127
    :cond_15
    iget-object v1, v1, Lokio/AsyncTimeout;->next:Lokio/AsyncTimeout;
    :try_end_17
    .catchall {:try_start_3 .. :try_end_17} :catchall_1b

    goto :goto_5

    .line 136
    .end local v1    # "prev":Lokio/AsyncTimeout;
    :cond_18
    monitor-exit v2

    const/4 v0, 0x1

    return v0

    :catchall_1b
    move-exception p0

    monitor-exit v2

    throw p0
.end method

.method private remainingNanos(J)J
    .registers 5
    .param p1, "now"    # J

    .line 144
    iget-wide v0, p0, Lokio/AsyncTimeout;->timeoutAt:J

    sub-long/2addr v0, p1

    return-wide v0
.end method

.method private static declared-synchronized scheduleTimeout(Lokio/AsyncTimeout;JZ)V
    .registers 12
    .param p0, "node"    # Lokio/AsyncTimeout;
    .param p1, "timeoutNanos"    # J
    .param p3, "hasDeadline"    # Z

    const-class v7, Lokio/AsyncTimeout;

    monitor-enter v7

    .line 85
    :try_start_3
    sget-object v0, Lokio/AsyncTimeout;->head:Lokio/AsyncTimeout;

    if-nez v0, :cond_16

    .line 86
    new-instance v0, Lokio/AsyncTimeout;

    invoke-direct {v0}, Lokio/AsyncTimeout;-><init>()V

    sput-object v0, Lokio/AsyncTimeout;->head:Lokio/AsyncTimeout;

    .line 87
    new-instance v0, Lokio/AsyncTimeout$Watchdog;

    invoke-direct {v0}, Lokio/AsyncTimeout$Watchdog;-><init>()V

    invoke-virtual {v0}, Lokio/AsyncTimeout$Watchdog;->start()V

    .line 90
    :cond_16
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v2

    .line 91
    .local v2, "now":J
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-eqz v0, :cond_2f

    if-eqz p3, :cond_2f

    .line 94
    invoke-virtual {p0}, Lokio/AsyncTimeout;->deadlineNanoTime()J

    move-result-wide v0

    sub-long/2addr v0, v2

    invoke-static {p1, p2, v0, v1}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    add-long/2addr v0, v2

    iput-wide v0, p0, Lokio/AsyncTimeout;->timeoutAt:J

    goto :goto_49

    .line 95
    :cond_2f
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-eqz v0, :cond_3a

    .line 96
    add-long v0, v2, p1

    iput-wide v0, p0, Lokio/AsyncTimeout;->timeoutAt:J

    goto :goto_49

    .line 97
    :cond_3a
    if-eqz p3, :cond_43

    .line 98
    invoke-virtual {p0}, Lokio/AsyncTimeout;->deadlineNanoTime()J

    move-result-wide v0

    iput-wide v0, p0, Lokio/AsyncTimeout;->timeoutAt:J

    goto :goto_49

    .line 100
    :cond_43
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 104
    :goto_49
    invoke-direct {p0, v2, v3}, Lokio/AsyncTimeout;->remainingNanos(J)J

    move-result-wide v4

    .line 105
    .local v4, "remainingNanos":J
    sget-object v6, Lokio/AsyncTimeout;->head:Lokio/AsyncTimeout;

    .line 106
    .local v6, "prev":Lokio/AsyncTimeout;
    :goto_4f
    iget-object v0, v6, Lokio/AsyncTimeout;->next:Lokio/AsyncTimeout;

    if-eqz v0, :cond_5d

    iget-object v0, v6, Lokio/AsyncTimeout;->next:Lokio/AsyncTimeout;

    invoke-direct {v0, v2, v3}, Lokio/AsyncTimeout;->remainingNanos(J)J

    move-result-wide v0

    cmp-long v0, v4, v0

    if-gez v0, :cond_6d

    .line 107
    :cond_5d
    iget-object v0, v6, Lokio/AsyncTimeout;->next:Lokio/AsyncTimeout;

    iput-object v0, p0, Lokio/AsyncTimeout;->next:Lokio/AsyncTimeout;

    .line 108
    iput-object p0, v6, Lokio/AsyncTimeout;->next:Lokio/AsyncTimeout;

    .line 109
    sget-object v0, Lokio/AsyncTimeout;->head:Lokio/AsyncTimeout;

    if-ne v6, v0, :cond_70

    .line 110
    const-class v0, Lokio/AsyncTimeout;

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    goto :goto_70

    .line 105
    :cond_6d
    iget-object v6, v6, Lokio/AsyncTimeout;->next:Lokio/AsyncTimeout;
    :try_end_6f
    .catchall {:try_start_3 .. :try_end_6f} :catchall_72

    goto :goto_4f

    .line 115
    .end local v6    # "prev":Lokio/AsyncTimeout;
    :cond_70
    :goto_70
    monitor-exit v7

    return-void

    .end local v2    # "now":J
    .end local v4    # "remainingNanos":J
    :catchall_72
    move-exception p0

    monitor-exit v7

    throw p0
.end method


# virtual methods
.method public final enter()V
    .registers 6

    .line 72
    iget-boolean v0, p0, Lokio/AsyncTimeout;->inQueue:Z

    if-eqz v0, :cond_c

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Unbalanced enter/exit"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 73
    :cond_c
    invoke-virtual {p0}, Lokio/AsyncTimeout;->timeoutNanos()J

    move-result-wide v2

    .line 74
    .local v2, "timeoutNanos":J
    invoke-virtual {p0}, Lokio/AsyncTimeout;->hasDeadline()Z

    move-result v4

    .line 75
    .local v4, "hasDeadline":Z
    const-wide/16 v0, 0x0

    cmp-long v0, v2, v0

    if-nez v0, :cond_1d

    if-nez v4, :cond_1d

    .line 76
    return-void

    .line 78
    :cond_1d
    const/4 v0, 0x1

    iput-boolean v0, p0, Lokio/AsyncTimeout;->inQueue:Z

    .line 79
    invoke-static {p0, v2, v3, v4}, Lokio/AsyncTimeout;->scheduleTimeout(Lokio/AsyncTimeout;JZ)V

    .line 80
    return-void
.end method

.method final exit(Ljava/io/IOException;)Ljava/io/IOException;
    .registers 3
    .param p1, "cause"    # Ljava/io/IOException;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 287
    invoke-virtual {p0}, Lokio/AsyncTimeout;->exit()Z

    move-result v0

    if-nez v0, :cond_7

    return-object p1

    .line 288
    :cond_7
    invoke-virtual {p0, p1}, Lokio/AsyncTimeout;->newTimeoutException(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    return-object v0
.end method

.method final exit(Z)V
    .registers 4
    .param p1, "throwOnTimeout"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 276
    invoke-virtual {p0}, Lokio/AsyncTimeout;->exit()Z

    move-result v1

    .line 277
    .local v1, "timedOut":Z
    if-eqz v1, :cond_e

    if-eqz p1, :cond_e

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lokio/AsyncTimeout;->newTimeoutException(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    .line 278
    :cond_e
    return-void
.end method

.method public final exit()Z
    .registers 2

    .line 119
    iget-boolean v0, p0, Lokio/AsyncTimeout;->inQueue:Z

    if-nez v0, :cond_6

    const/4 v0, 0x0

    return v0

    .line 120
    :cond_6
    const/4 v0, 0x0

    iput-boolean v0, p0, Lokio/AsyncTimeout;->inQueue:Z

    .line 121
    invoke-static {p0}, Lokio/AsyncTimeout;->cancelScheduledTimeout(Lokio/AsyncTimeout;)Z

    move-result v0

    return v0
.end method

.method protected newTimeoutException(Ljava/io/IOException;)Ljava/io/IOException;
    .registers 4
    .param p1, "cause"    # Ljava/io/IOException;

    .line 297
    new-instance v1, Ljava/io/InterruptedIOException;

    const-string v0, "timeout"

    invoke-direct {v1, v0}, Ljava/io/InterruptedIOException;-><init>(Ljava/lang/String;)V

    .line 298
    .local v1, "e":Ljava/io/InterruptedIOException;
    if-eqz p1, :cond_c

    .line 299
    invoke-virtual {v1, p1}, Ljava/io/InterruptedIOException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    .line 301
    :cond_c
    return-object v1
.end method

.method public final sink(Lokio/Sink;)Lokio/Sink;
    .registers 3
    .param p1, "sink"    # Lokio/Sink;

    .line 160
    new-instance v0, Lokio/AsyncTimeout$1;

    invoke-direct {v0, p0, p1}, Lokio/AsyncTimeout$1;-><init>(Lokio/AsyncTimeout;Lokio/Sink;)V

    return-object v0
.end method

.method public final source(Lokio/Source;)Lokio/Source;
    .registers 3
    .param p1, "source"    # Lokio/Source;

    .line 233
    new-instance v0, Lokio/AsyncTimeout$2;

    invoke-direct {v0, p0, p1}, Lokio/AsyncTimeout$2;-><init>(Lokio/AsyncTimeout;Lokio/Source;)V

    return-object v0
.end method

.method protected timedOut()V
    .registers 1

    .line 152
    return-void
.end method

.class public Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioConfiguracoesActivity_$IntentBuilder_;
.super Lorg/androidannotations/api/builder/ActivityIntentBuilder;
.source "AreaUsuarioConfiguracoesActivity_.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioConfiguracoesActivity_;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "IntentBuilder_"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/androidannotations/api/builder/ActivityIntentBuilder<Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioConfiguracoesActivity_$IntentBuilder_;>;"
    }
.end annotation


# instance fields
.field private fragmentSupport_:Landroid/support/v4/app/Fragment;

.field private fragment_:Landroid/app/Fragment;


# direct methods
.method public constructor <init>(Landroid/support/v4/app/Fragment;)V
    .registers 4
    .param p1, "fragment"    # Landroid/support/v4/app/Fragment;

    .line 133
    invoke-virtual {p1}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const-class v1, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioConfiguracoesActivity_;

    invoke-direct {p0, v0, v1}, Lorg/androidannotations/api/builder/ActivityIntentBuilder;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 134
    iput-object p1, p0, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioConfiguracoesActivity_$IntentBuilder_;->fragmentSupport_:Landroid/support/v4/app/Fragment;

    .line 135
    return-void
.end method


# virtual methods
.method public startForResult(I)Lorg/androidannotations/api/builder/PostActivityStarter;
    .registers 6
    .param p1, "requestCode"    # I

    .line 139
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioConfiguracoesActivity_$IntentBuilder_;->fragmentSupport_:Landroid/support/v4/app/Fragment;

    if-eqz v0, :cond_c

    .line 140
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioConfiguracoesActivity_$IntentBuilder_;->fragmentSupport_:Landroid/support/v4/app/Fragment;

    iget-object v1, p0, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioConfiguracoesActivity_$IntentBuilder_;->intent:Landroid/content/Intent;

    invoke-virtual {v0, v1, p1}, Landroid/support/v4/app/Fragment;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_52

    .line 142
    :cond_c
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioConfiguracoesActivity_$IntentBuilder_;->fragment_:Landroid/app/Fragment;

    if-eqz v0, :cond_28

    .line 143
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_20

    .line 144
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioConfiguracoesActivity_$IntentBuilder_;->fragment_:Landroid/app/Fragment;

    iget-object v1, p0, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioConfiguracoesActivity_$IntentBuilder_;->intent:Landroid/content/Intent;

    iget-object v2, p0, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioConfiguracoesActivity_$IntentBuilder_;->lastOptions:Landroid/os/Bundle;

    invoke-virtual {v0, v1, p1, v2}, Landroid/app/Fragment;->startActivityForResult(Landroid/content/Intent;ILandroid/os/Bundle;)V

    goto :goto_52

    .line 146
    :cond_20
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioConfiguracoesActivity_$IntentBuilder_;->fragment_:Landroid/app/Fragment;

    iget-object v1, p0, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioConfiguracoesActivity_$IntentBuilder_;->intent:Landroid/content/Intent;

    invoke-virtual {v0, v1, p1}, Landroid/app/Fragment;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_52

    .line 149
    :cond_28
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioConfiguracoesActivity_$IntentBuilder_;->context:Landroid/content/Context;

    instance-of v0, v0, Landroid/app/Activity;

    if-eqz v0, :cond_3b

    .line 150
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioConfiguracoesActivity_$IntentBuilder_;->context:Landroid/content/Context;

    move-object v3, v0

    check-cast v3, Landroid/app/Activity;

    .line 151
    .local v3, "activity":Landroid/app/Activity;
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioConfiguracoesActivity_$IntentBuilder_;->intent:Landroid/content/Intent;

    iget-object v1, p0, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioConfiguracoesActivity_$IntentBuilder_;->lastOptions:Landroid/os/Bundle;

    invoke-static {v3, v0, p1, v1}, Landroid/support/v4/app/ActivityCompat;->startActivityForResult(Landroid/app/Activity;Landroid/content/Intent;ILandroid/os/Bundle;)V

    .line 152
    .end local v3    # "activity":Landroid/app/Activity;
    goto :goto_52

    .line 153
    :cond_3b
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_4b

    .line 154
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioConfiguracoesActivity_$IntentBuilder_;->context:Landroid/content/Context;

    iget-object v1, p0, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioConfiguracoesActivity_$IntentBuilder_;->intent:Landroid/content/Intent;

    iget-object v2, p0, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioConfiguracoesActivity_$IntentBuilder_;->lastOptions:Landroid/os/Bundle;

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;Landroid/os/Bundle;)V

    goto :goto_52

    .line 156
    :cond_4b
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioConfiguracoesActivity_$IntentBuilder_;->context:Landroid/content/Context;

    iget-object v1, p0, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioConfiguracoesActivity_$IntentBuilder_;->intent:Landroid/content/Intent;

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 161
    :goto_52
    new-instance v0, Lorg/androidannotations/api/builder/PostActivityStarter;

    iget-object v1, p0, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioConfiguracoesActivity_$IntentBuilder_;->context:Landroid/content/Context;

    invoke-direct {v0, v1}, Lorg/androidannotations/api/builder/PostActivityStarter;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.class public Lcom/google/android/gms/tagmanager/TagManager;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/tagmanager/TagManager$4;,
        Lcom/google/android/gms/tagmanager/TagManager$zza;
    }
.end annotation


# static fields
.field private static zzblm:Lcom/google/android/gms/tagmanager/TagManager;


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final zzbhN:Lcom/google/android/gms/tagmanager/DataLayer;

.field private final zzbkh:Lcom/google/android/gms/tagmanager/zzs;

.field private final zzblj:Lcom/google/android/gms/tagmanager/TagManager$zza;

.field private final zzblk:Lcom/google/android/gms/tagmanager/zzct;

.field private final zzbll:Ljava/util/concurrent/ConcurrentMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentMap<Lcom/google/android/gms/tagmanager/zzo;Ljava/lang/Boolean;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/google/android/gms/tagmanager/TagManager$zza;Lcom/google/android/gms/tagmanager/DataLayer;Lcom/google/android/gms/tagmanager/zzct;)V
    .registers 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "containerHolderLoaderProvider"    # Lcom/google/android/gms/tagmanager/TagManager$zza;
    .param p3, "dataLayer"    # Lcom/google/android/gms/tagmanager/DataLayer;
    .param p4, "serviceManager"    # Lcom/google/android/gms/tagmanager/zzct;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-nez p1, :cond_d

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "context cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_d
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/tagmanager/TagManager;->mContext:Landroid/content/Context;

    iput-object p4, p0, Lcom/google/android/gms/tagmanager/TagManager;->zzblk:Lcom/google/android/gms/tagmanager/zzct;

    iput-object p2, p0, Lcom/google/android/gms/tagmanager/TagManager;->zzblj:Lcom/google/android/gms/tagmanager/TagManager$zza;

    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/tagmanager/TagManager;->zzbll:Ljava/util/concurrent/ConcurrentMap;

    iput-object p3, p0, Lcom/google/android/gms/tagmanager/TagManager;->zzbhN:Lcom/google/android/gms/tagmanager/DataLayer;

    iget-object v0, p0, Lcom/google/android/gms/tagmanager/TagManager;->zzbhN:Lcom/google/android/gms/tagmanager/DataLayer;

    new-instance v1, Lcom/google/android/gms/tagmanager/TagManager$1;

    invoke-direct {v1, p0}, Lcom/google/android/gms/tagmanager/TagManager$1;-><init>(Lcom/google/android/gms/tagmanager/TagManager;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/tagmanager/DataLayer;->zza(Lcom/google/android/gms/tagmanager/DataLayer$zzb;)V

    iget-object v0, p0, Lcom/google/android/gms/tagmanager/TagManager;->zzbhN:Lcom/google/android/gms/tagmanager/DataLayer;

    new-instance v1, Lcom/google/android/gms/tagmanager/zzd;

    iget-object v2, p0, Lcom/google/android/gms/tagmanager/TagManager;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/google/android/gms/tagmanager/zzd;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/tagmanager/DataLayer;->zza(Lcom/google/android/gms/tagmanager/DataLayer$zzb;)V

    new-instance v0, Lcom/google/android/gms/tagmanager/zzs;

    invoke-direct {v0}, Lcom/google/android/gms/tagmanager/zzs;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/tagmanager/TagManager;->zzbkh:Lcom/google/android/gms/tagmanager/zzs;

    invoke-direct {p0}, Lcom/google/android/gms/tagmanager/TagManager;->zzHt()V

    return-void
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/google/android/gms/tagmanager/TagManager;
    .registers 8
    .param p0, "context"    # Landroid/content/Context;

    const-class v3, Lcom/google/android/gms/tagmanager/TagManager;

    monitor-enter v3

    :try_start_3
    sget-object v0, Lcom/google/android/gms/tagmanager/TagManager;->zzblm:Lcom/google/android/gms/tagmanager/TagManager;

    if-nez v0, :cond_2e

    if-nez p0, :cond_14

    const-string v0, "TagManager.getInstance requires non-null context."

    invoke-static {v0}, Lcom/google/android/gms/tagmanager/zzbg;->e(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_14
    new-instance v4, Lcom/google/android/gms/tagmanager/TagManager$2;

    invoke-direct {v4}, Lcom/google/android/gms/tagmanager/TagManager$2;-><init>()V

    new-instance v5, Lcom/google/android/gms/tagmanager/zzw;

    invoke-direct {v5, p0}, Lcom/google/android/gms/tagmanager/zzw;-><init>(Landroid/content/Context;)V

    new-instance v0, Lcom/google/android/gms/tagmanager/TagManager;

    new-instance v1, Lcom/google/android/gms/tagmanager/DataLayer;

    invoke-direct {v1, v5}, Lcom/google/android/gms/tagmanager/DataLayer;-><init>(Lcom/google/android/gms/tagmanager/DataLayer$zzc;)V

    invoke-static {}, Lcom/google/android/gms/tagmanager/zzcu;->zzHo()Lcom/google/android/gms/tagmanager/zzcu;

    move-result-object v2

    invoke-direct {v0, p0, v4, v1, v2}, Lcom/google/android/gms/tagmanager/TagManager;-><init>(Landroid/content/Context;Lcom/google/android/gms/tagmanager/TagManager$zza;Lcom/google/android/gms/tagmanager/DataLayer;Lcom/google/android/gms/tagmanager/zzct;)V

    sput-object v0, Lcom/google/android/gms/tagmanager/TagManager;->zzblm:Lcom/google/android/gms/tagmanager/TagManager;

    :cond_2e
    sget-object v0, Lcom/google/android/gms/tagmanager/TagManager;->zzblm:Lcom/google/android/gms/tagmanager/TagManager;
    :try_end_30
    .catchall {:try_start_3 .. :try_end_30} :catchall_32

    monitor-exit v3

    return-object v0

    :catchall_32
    move-exception v6

    monitor-exit v3

    throw v6
.end method

.method private zzHt()V
    .registers 3
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_10

    iget-object v0, p0, Lcom/google/android/gms/tagmanager/TagManager;->mContext:Landroid/content/Context;

    new-instance v1, Lcom/google/android/gms/tagmanager/TagManager$3;

    invoke-direct {v1, p0}, Lcom/google/android/gms/tagmanager/TagManager$3;-><init>(Lcom/google/android/gms/tagmanager/TagManager;)V

    invoke-virtual {v0, v1}, Landroid/content/Context;->registerComponentCallbacks(Landroid/content/ComponentCallbacks;)V

    :cond_10
    return-void
.end method

.method static synthetic zza(Lcom/google/android/gms/tagmanager/TagManager;Ljava/lang/String;)V
    .registers 2

    invoke-direct {p0, p1}, Lcom/google/android/gms/tagmanager/TagManager;->zzgp(Ljava/lang/String;)V

    return-void
.end method

.method private zzgp(Ljava/lang/String;)V
    .registers 5

    iget-object v0, p0, Lcom/google/android/gms/tagmanager/TagManager;->zzbll:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0}, Ljava/util/concurrent/ConcurrentMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_a
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1b

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/google/android/gms/tagmanager/zzo;

    invoke-virtual {v2, p1}, Lcom/google/android/gms/tagmanager/zzo;->zzfR(Ljava/lang/String;)V

    goto :goto_a

    :cond_1b
    return-void
.end method


# virtual methods
.method public dispatch()V
    .registers 2

    iget-object v0, p0, Lcom/google/android/gms/tagmanager/TagManager;->zzblk:Lcom/google/android/gms/tagmanager/zzct;

    invoke-virtual {v0}, Lcom/google/android/gms/tagmanager/zzct;->dispatch()V

    return-void
.end method

.method public zzb(Lcom/google/android/gms/tagmanager/zzo;)Z
    .registers 3

    iget-object v0, p0, Lcom/google/android/gms/tagmanager/TagManager;->zzbll:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0, p1}, Ljava/util/concurrent/ConcurrentMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_a

    const/4 v0, 0x1

    goto :goto_b

    :cond_a
    const/4 v0, 0x0

    :goto_b
    return v0
.end method

.method declared-synchronized zzp(Landroid/net/Uri;)Z
    .registers 8

    monitor-enter p0

    :try_start_1
    invoke-static {}, Lcom/google/android/gms/tagmanager/zzcb;->zzGU()Lcom/google/android/gms/tagmanager/zzcb;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/google/android/gms/tagmanager/zzcb;->zzp(Landroid/net/Uri;)Z

    move-result v0

    if-eqz v0, :cond_87

    invoke-virtual {v2}, Lcom/google/android/gms/tagmanager/zzcb;->getContainerId()Ljava/lang/String;

    move-result-object v3

    sget-object v0, Lcom/google/android/gms/tagmanager/TagManager$4;->zzblo:[I

    invoke-virtual {v2}, Lcom/google/android/gms/tagmanager/zzcb;->zzGV()Lcom/google/android/gms/tagmanager/zzcb$zza;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/tagmanager/zzcb$zza;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_8a

    goto/16 :goto_84

    :pswitch_20
    iget-object v0, p0, Lcom/google/android/gms/tagmanager/TagManager;->zzbll:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0}, Ljava/util/concurrent/ConcurrentMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_2a
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_49

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/google/android/gms/tagmanager/zzo;

    invoke-virtual {v5}, Lcom/google/android/gms/tagmanager/zzo;->getContainerId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_48

    const/4 v0, 0x0

    invoke-virtual {v5, v0}, Lcom/google/android/gms/tagmanager/zzo;->zzfT(Ljava/lang/String;)V

    invoke-virtual {v5}, Lcom/google/android/gms/tagmanager/zzo;->refresh()V

    :cond_48
    goto :goto_2a

    :cond_49
    goto :goto_84

    :pswitch_4a
    iget-object v0, p0, Lcom/google/android/gms/tagmanager/TagManager;->zzbll:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0}, Ljava/util/concurrent/ConcurrentMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_54
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_84

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/google/android/gms/tagmanager/zzo;

    invoke-virtual {v5}, Lcom/google/android/gms/tagmanager/zzo;->getContainerId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_76

    invoke-virtual {v2}, Lcom/google/android/gms/tagmanager/zzcb;->zzGW()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Lcom/google/android/gms/tagmanager/zzo;->zzfT(Ljava/lang/String;)V

    invoke-virtual {v5}, Lcom/google/android/gms/tagmanager/zzo;->refresh()V

    goto :goto_83

    :cond_76
    invoke-virtual {v5}, Lcom/google/android/gms/tagmanager/zzo;->zzGd()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_83

    const/4 v0, 0x0

    invoke-virtual {v5, v0}, Lcom/google/android/gms/tagmanager/zzo;->zzfT(Ljava/lang/String;)V

    invoke-virtual {v5}, Lcom/google/android/gms/tagmanager/zzo;->refresh()V
    :try_end_83
    .catchall {:try_start_1 .. :try_end_83} :catchall_94

    :cond_83
    :goto_83
    goto :goto_54

    :cond_84
    :goto_84
    monitor-exit p0

    const/4 v0, 0x1

    return v0

    :cond_87
    monitor-exit p0

    const/4 v0, 0x0

    return v0

    :pswitch_data_8a
    .packed-switch 0x1
        :pswitch_20
        :pswitch_4a
        :pswitch_4a
    .end packed-switch

    :catchall_94
    move-exception p1

    monitor-exit p0

    throw p1
.end method

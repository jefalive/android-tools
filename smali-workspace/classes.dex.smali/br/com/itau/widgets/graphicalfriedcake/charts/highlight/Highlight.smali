.class public Lbr/com/itau/widgets/graphicalfriedcake/charts/highlight/Highlight;
.super Ljava/lang/Object;
.source "Highlight.java"


# instance fields
.field private mDataSetIndex:I

.field private mRange:Landroid/util/Range;

.field private mStackIndex:I

.field private mXIndex:I


# direct methods
.method public constructor <init>(II)V
    .registers 4
    .param p1, "x"    # I
    .param p2, "dataSet"    # I

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    const/4 v0, -0x1

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/highlight/Highlight;->mStackIndex:I

    .line 36
    iput p1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/highlight/Highlight;->mXIndex:I

    .line 37
    iput p2, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/highlight/Highlight;->mDataSetIndex:I

    .line 38
    return-void
.end method

.method public constructor <init>(III)V
    .registers 4
    .param p1, "x"    # I
    .param p2, "dataSet"    # I
    .param p3, "stackIndex"    # I

    .line 50
    invoke-direct {p0, p1, p2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/highlight/Highlight;-><init>(II)V

    .line 51
    iput p3, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/highlight/Highlight;->mStackIndex:I

    .line 52
    return-void
.end method

.method public constructor <init>(IIILandroid/util/Range;)V
    .registers 5
    .param p1, "x"    # I
    .param p2, "dataSet"    # I
    .param p3, "stackIndex"    # I
    .param p4, "range"    # Landroid/util/Range;

    .line 65
    invoke-direct {p0, p1, p2, p3}, Lbr/com/itau/widgets/graphicalfriedcake/charts/highlight/Highlight;-><init>(III)V

    .line 66
    iput-object p4, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/highlight/Highlight;->mRange:Landroid/util/Range;

    .line 67
    return-void
.end method


# virtual methods
.method public equalTo(Lbr/com/itau/widgets/graphicalfriedcake/charts/highlight/Highlight;)Z
    .registers 4
    .param p1, "h"    # Lbr/com/itau/widgets/graphicalfriedcake/charts/highlight/Highlight;

    .line 120
    if-nez p1, :cond_4

    .line 121
    const/4 v0, 0x0

    return v0

    .line 123
    :cond_4
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/highlight/Highlight;->mDataSetIndex:I

    iget v1, p1, Lbr/com/itau/widgets/graphicalfriedcake/charts/highlight/Highlight;->mDataSetIndex:I

    if-ne v0, v1, :cond_18

    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/highlight/Highlight;->mXIndex:I

    iget v1, p1, Lbr/com/itau/widgets/graphicalfriedcake/charts/highlight/Highlight;->mXIndex:I

    if-ne v0, v1, :cond_18

    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/highlight/Highlight;->mStackIndex:I

    iget v1, p1, Lbr/com/itau/widgets/graphicalfriedcake/charts/highlight/Highlight;->mStackIndex:I

    if-ne v0, v1, :cond_18

    .line 125
    const/4 v0, 0x1

    return v0

    .line 127
    :cond_18
    const/4 v0, 0x0

    return v0
.end method

.method public getDataSetIndex()I
    .registers 2

    .line 76
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/highlight/Highlight;->mDataSetIndex:I

    return v0
.end method

.method public getRange()Landroid/util/Range;
    .registers 2

    .line 106
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/highlight/Highlight;->mRange:Landroid/util/Range;

    return-object v0
.end method

.method public getStackIndex()I
    .registers 2

    .line 97
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/highlight/Highlight;->mStackIndex:I

    return v0
.end method

.method public getXIndex()I
    .registers 2

    .line 86
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/highlight/Highlight;->mXIndex:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    .line 134
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Highlight, xIndex: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/highlight/Highlight;->mXIndex:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", dataSetIndex: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/highlight/Highlight;->mDataSetIndex:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", stackIndex (only stacked barentry): "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/highlight/Highlight;->mStackIndex:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

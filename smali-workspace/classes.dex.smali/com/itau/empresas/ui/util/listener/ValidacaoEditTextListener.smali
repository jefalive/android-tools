.class public interface abstract Lcom/itau/empresas/ui/util/listener/ValidacaoEditTextListener;
.super Ljava/lang/Object;
.source "ValidacaoEditTextListener.java"


# virtual methods
.method public abstract campoInvalido(Landroid/widget/EditText;)V
.end method

.method public abstract campoInvalido(Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<Landroid/widget/TextView;>;)V"
        }
    .end annotation
.end method

.method public abstract campoValido()V
.end method

.method public abstract campoValido(Landroid/widget/EditText;)V
.end method

.class Lnet/hockeyapp/android/FeedbackActivity$2;
.super Landroid/os/Handler;
.source "FeedbackActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lnet/hockeyapp/android/FeedbackActivity;->initFeedbackHandler()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/hockeyapp/android/FeedbackActivity;


# direct methods
.method constructor <init>(Lnet/hockeyapp/android/FeedbackActivity;)V
    .registers 2
    .param p1, "this$0"    # Lnet/hockeyapp/android/FeedbackActivity;

    .line 537
    iput-object p1, p0, Lnet/hockeyapp/android/FeedbackActivity$2;->this$0:Lnet/hockeyapp/android/FeedbackActivity;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 9
    .param p1, "msg"    # Landroid/os/Message;

    .line 540
    const/4 v2, 0x0

    .line 541
    .local v2, "success":Z
    iget-object v0, p0, Lnet/hockeyapp/android/FeedbackActivity$2;->this$0:Lnet/hockeyapp/android/FeedbackActivity;

    new-instance v1, Lnet/hockeyapp/android/objects/ErrorObject;

    invoke-direct {v1}, Lnet/hockeyapp/android/objects/ErrorObject;-><init>()V

    # setter for: Lnet/hockeyapp/android/FeedbackActivity;->error:Lnet/hockeyapp/android/objects/ErrorObject;
    invoke-static {v0, v1}, Lnet/hockeyapp/android/FeedbackActivity;->access$002(Lnet/hockeyapp/android/FeedbackActivity;Lnet/hockeyapp/android/objects/ErrorObject;)Lnet/hockeyapp/android/objects/ErrorObject;

    .line 543
    if-eqz p1, :cond_85

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_85

    .line 544
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v3

    .line 545
    .local v3, "bundle":Landroid/os/Bundle;
    const-string v0, "feedback_response"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 546
    .local v4, "responseString":Ljava/lang/String;
    const-string v0, "feedback_status"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 547
    .local v5, "statusCode":Ljava/lang/String;
    const-string v0, "request_type"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 548
    .local v6, "requestType":Ljava/lang/String;
    const-string v0, "send"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4b

    if-eqz v4, :cond_3b

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    const/16 v1, 0xc9

    if-eq v0, v1, :cond_4b

    .line 550
    :cond_3b
    iget-object v0, p0, Lnet/hockeyapp/android/FeedbackActivity$2;->this$0:Lnet/hockeyapp/android/FeedbackActivity;

    # getter for: Lnet/hockeyapp/android/FeedbackActivity;->error:Lnet/hockeyapp/android/objects/ErrorObject;
    invoke-static {v0}, Lnet/hockeyapp/android/FeedbackActivity;->access$000(Lnet/hockeyapp/android/FeedbackActivity;)Lnet/hockeyapp/android/objects/ErrorObject;

    move-result-object v0

    const/16 v1, 0x40c

    invoke-static {v1}, Lnet/hockeyapp/android/Strings;->get(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lnet/hockeyapp/android/objects/ErrorObject;->setMessage(Ljava/lang/String;)V

    goto :goto_84

    .line 552
    :cond_4b
    const-string v0, "fetch"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6c

    if-eqz v5, :cond_6c

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    const/16 v1, 0x194

    if-eq v0, v1, :cond_65

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    const/16 v1, 0x1a6

    if-ne v0, v1, :cond_6c

    .line 554
    :cond_65
    iget-object v0, p0, Lnet/hockeyapp/android/FeedbackActivity$2;->this$0:Lnet/hockeyapp/android/FeedbackActivity;

    # invokes: Lnet/hockeyapp/android/FeedbackActivity;->resetFeedbackView()V
    invoke-static {v0}, Lnet/hockeyapp/android/FeedbackActivity;->access$100(Lnet/hockeyapp/android/FeedbackActivity;)V

    .line 555
    const/4 v2, 0x1

    goto :goto_84

    .line 557
    :cond_6c
    if-eqz v4, :cond_75

    .line 558
    iget-object v0, p0, Lnet/hockeyapp/android/FeedbackActivity$2;->this$0:Lnet/hockeyapp/android/FeedbackActivity;

    # invokes: Lnet/hockeyapp/android/FeedbackActivity;->startParseFeedbackTask(Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v0, v4, v6}, Lnet/hockeyapp/android/FeedbackActivity;->access$200(Lnet/hockeyapp/android/FeedbackActivity;Ljava/lang/String;Ljava/lang/String;)V

    .line 559
    const/4 v2, 0x1

    goto :goto_84

    .line 562
    :cond_75
    iget-object v0, p0, Lnet/hockeyapp/android/FeedbackActivity$2;->this$0:Lnet/hockeyapp/android/FeedbackActivity;

    # getter for: Lnet/hockeyapp/android/FeedbackActivity;->error:Lnet/hockeyapp/android/objects/ErrorObject;
    invoke-static {v0}, Lnet/hockeyapp/android/FeedbackActivity;->access$000(Lnet/hockeyapp/android/FeedbackActivity;)Lnet/hockeyapp/android/objects/ErrorObject;

    move-result-object v0

    const/16 v1, 0x40d

    invoke-static {v1}, Lnet/hockeyapp/android/Strings;->get(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lnet/hockeyapp/android/objects/ErrorObject;->setMessage(Ljava/lang/String;)V

    .line 564
    .end local v3    # "bundle":Landroid/os/Bundle;
    .end local v4    # "responseString":Ljava/lang/String;
    .end local v5    # "statusCode":Ljava/lang/String;
    .end local v6    # "requestType":Ljava/lang/String;
    :goto_84
    goto :goto_94

    .line 566
    :cond_85
    iget-object v0, p0, Lnet/hockeyapp/android/FeedbackActivity$2;->this$0:Lnet/hockeyapp/android/FeedbackActivity;

    # getter for: Lnet/hockeyapp/android/FeedbackActivity;->error:Lnet/hockeyapp/android/objects/ErrorObject;
    invoke-static {v0}, Lnet/hockeyapp/android/FeedbackActivity;->access$000(Lnet/hockeyapp/android/FeedbackActivity;)Lnet/hockeyapp/android/objects/ErrorObject;

    move-result-object v0

    const/16 v1, 0x40c

    invoke-static {v1}, Lnet/hockeyapp/android/Strings;->get(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lnet/hockeyapp/android/objects/ErrorObject;->setMessage(Ljava/lang/String;)V

    .line 569
    :goto_94
    if-nez v2, :cond_a0

    .line 570
    iget-object v0, p0, Lnet/hockeyapp/android/FeedbackActivity$2;->this$0:Lnet/hockeyapp/android/FeedbackActivity;

    new-instance v1, Lnet/hockeyapp/android/FeedbackActivity$2$1;

    invoke-direct {v1, p0}, Lnet/hockeyapp/android/FeedbackActivity$2$1;-><init>(Lnet/hockeyapp/android/FeedbackActivity$2;)V

    invoke-virtual {v0, v1}, Lnet/hockeyapp/android/FeedbackActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 580
    :cond_a0
    return-void
.end method

.class public final enum Lbr/com/itau/textovoz/util/ApiUtils$Segment;
.super Ljava/lang/Enum;
.source "ApiUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lbr/com/itau/textovoz/util/ApiUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Segment"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<Lbr/com/itau/textovoz/util/ApiUtils$Segment;>;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lbr/com/itau/textovoz/util/ApiUtils$Segment;

.field public static final enum DEFAULT:Lbr/com/itau/textovoz/util/ApiUtils$Segment;


# instance fields
.field private item:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 4

    .line 54
    new-instance v0, Lbr/com/itau/textovoz/util/ApiUtils$Segment;

    const-string v1, "DEFAULT"

    const-string v2, "0"

    const/4 v3, 0x0

    invoke-direct {v0, v1, v3, v2}, Lbr/com/itau/textovoz/util/ApiUtils$Segment;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lbr/com/itau/textovoz/util/ApiUtils$Segment;->DEFAULT:Lbr/com/itau/textovoz/util/ApiUtils$Segment;

    .line 53
    const/4 v0, 0x1

    new-array v0, v0, [Lbr/com/itau/textovoz/util/ApiUtils$Segment;

    sget-object v1, Lbr/com/itau/textovoz/util/ApiUtils$Segment;->DEFAULT:Lbr/com/itau/textovoz/util/ApiUtils$Segment;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sput-object v0, Lbr/com/itau/textovoz/util/ApiUtils$Segment;->$VALUES:[Lbr/com/itau/textovoz/util/ApiUtils$Segment;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .registers 4
    .param p3, "item"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/lang/String;)V"
        }
    .end annotation

    .line 57
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 58
    iput-object p3, p0, Lbr/com/itau/textovoz/util/ApiUtils$Segment;->item:Ljava/lang/String;

    .line 59
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lbr/com/itau/textovoz/util/ApiUtils$Segment;
    .registers 2
    .param p0, "name"    # Ljava/lang/String;

    .line 53
    const-class v0, Lbr/com/itau/textovoz/util/ApiUtils$Segment;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lbr/com/itau/textovoz/util/ApiUtils$Segment;

    return-object v0
.end method

.method public static values()[Lbr/com/itau/textovoz/util/ApiUtils$Segment;
    .registers 1

    .line 53
    sget-object v0, Lbr/com/itau/textovoz/util/ApiUtils$Segment;->$VALUES:[Lbr/com/itau/textovoz/util/ApiUtils$Segment;

    invoke-virtual {v0}, [Lbr/com/itau/textovoz/util/ApiUtils$Segment;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lbr/com/itau/textovoz/util/ApiUtils$Segment;

    return-object v0
.end method


# virtual methods
.method public getItem()Ljava/lang/String;
    .registers 2

    .line 62
    iget-object v0, p0, Lbr/com/itau/textovoz/util/ApiUtils$Segment;->item:Ljava/lang/String;

    return-object v0
.end method

.class public final Lcom/google/android/gms/maps/model/BitmapDescriptorFactory;
.super Ljava/lang/Object;


# static fields
.field private static zzaSW:Lcom/google/android/gms/maps/model/internal/zza;


# direct methods
.method public static fromResource(I)Lcom/google/android/gms/maps/model/BitmapDescriptor;
    .registers 4
    .param p0, "resourceId"    # I

    :try_start_0
    new-instance v0, Lcom/google/android/gms/maps/model/BitmapDescriptor;

    invoke-static {}, Lcom/google/android/gms/maps/model/BitmapDescriptorFactory;->zzAi()Lcom/google/android/gms/maps/model/internal/zza;

    move-result-object v1

    invoke-interface {v1, p0}, Lcom/google/android/gms/maps/model/internal/zza;->zziz(I)Lcom/google/android/gms/dynamic/zzd;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/maps/model/BitmapDescriptor;-><init>(Lcom/google/android/gms/dynamic/zzd;)V
    :try_end_d
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_d} :catch_e

    return-object v0

    :catch_e
    move-exception v2

    new-instance v0, Lcom/google/android/gms/maps/model/RuntimeRemoteException;

    invoke-direct {v0, v2}, Lcom/google/android/gms/maps/model/RuntimeRemoteException;-><init>(Landroid/os/RemoteException;)V

    throw v0
.end method

.method private static zzAi()Lcom/google/android/gms/maps/model/internal/zza;
    .registers 2

    sget-object v0, Lcom/google/android/gms/maps/model/BitmapDescriptorFactory;->zzaSW:Lcom/google/android/gms/maps/model/internal/zza;

    const-string v1, "IBitmapDescriptorFactory is not initialized"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/zzx;->zzb(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/maps/model/internal/zza;

    return-object v0
.end method

.method public static zza(Lcom/google/android/gms/maps/model/internal/zza;)V
    .registers 2

    sget-object v0, Lcom/google/android/gms/maps/model/BitmapDescriptorFactory;->zzaSW:Lcom/google/android/gms/maps/model/internal/zza;

    if-eqz v0, :cond_5

    return-void

    :cond_5
    invoke-static {p0}, Lcom/google/android/gms/common/internal/zzx;->zzz(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/maps/model/internal/zza;

    sput-object v0, Lcom/google/android/gms/maps/model/BitmapDescriptorFactory;->zzaSW:Lcom/google/android/gms/maps/model/internal/zza;

    return-void
.end method

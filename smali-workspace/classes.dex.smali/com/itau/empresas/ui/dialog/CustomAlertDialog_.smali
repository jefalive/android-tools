.class public final Lcom/itau/empresas/ui/dialog/CustomAlertDialog_;
.super Lcom/itau/empresas/ui/dialog/CustomAlertDialog;
.source "CustomAlertDialog_.java"

# interfaces
.implements Lorg/androidannotations/api/view/HasViews;
.implements Lorg/androidannotations/api/view/OnViewChangedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/itau/empresas/ui/dialog/CustomAlertDialog_$FragmentBuilder_;
    }
.end annotation


# instance fields
.field private contentView_:Landroid/view/View;

.field private final onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;


# direct methods
.method public constructor <init>()V
    .registers 2

    .line 26
    invoke-direct {p0}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog;-><init>()V

    .line 30
    new-instance v0, Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-direct {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/ui/dialog/CustomAlertDialog_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    return-void
.end method

.method public static builder()Lcom/itau/empresas/ui/dialog/CustomAlertDialog_$FragmentBuilder_;
    .registers 1

    .line 88
    new-instance v0, Lcom/itau/empresas/ui/dialog/CustomAlertDialog_$FragmentBuilder_;

    invoke-direct {v0}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog_$FragmentBuilder_;-><init>()V

    return-object v0
.end method

.method private init_(Landroid/os/Bundle;)V
    .registers 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 76
    invoke-static {p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->registerOnViewChangedListener(Lorg/androidannotations/api/view/OnViewChangedListener;)V

    .line 77
    invoke-direct {p0}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog_;->injectFragmentArguments_()V

    .line 78
    invoke-static {}, Lcom/itau/empresas/CustomApplication_;->getInstance()Lcom/itau/empresas/CustomApplication;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/ui/dialog/CustomAlertDialog_;->application:Lcom/itau/empresas/CustomApplication;

    .line 79
    return-void
.end method

.method private injectFragmentArguments_()V
    .registers 3

    .line 133
    invoke-virtual {p0}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog_;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    .line 134
    .local v1, "args_":Landroid/os/Bundle;
    if-eqz v1, :cond_58

    .line 135
    const-string v0, "mensagem"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_16

    .line 136
    const-string v0, "mensagem"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/ui/dialog/CustomAlertDialog_;->mensagem:Ljava/lang/String;

    .line 138
    :cond_16
    const-string v0, "textoBotaoPositivo"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_26

    .line 139
    const-string v0, "textoBotaoPositivo"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/ui/dialog/CustomAlertDialog_;->textoBotaoPositivo:Ljava/lang/String;

    .line 141
    :cond_26
    const-string v0, "textoBotaoNegativo"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_36

    .line 142
    const-string v0, "textoBotaoNegativo"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/ui/dialog/CustomAlertDialog_;->textoBotaoNegativo:Ljava/lang/String;

    .line 144
    :cond_36
    const-string v0, "textoBotaoNeutro"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_46

    .line 145
    const-string v0, "textoBotaoNeutro"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/ui/dialog/CustomAlertDialog_;->textoBotaoNeutro:Ljava/lang/String;

    .line 147
    :cond_46
    const-string v0, "estadoArg"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_58

    .line 148
    const-string v0, "estadoArg"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Estados;

    iput-object v0, p0, Lcom/itau/empresas/ui/dialog/CustomAlertDialog_;->estadoArg:Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Estados;

    .line 151
    :cond_58
    return-void
.end method


# virtual methods
.method public findViewById(I)Landroid/view/View;
    .registers 3
    .param p1, "id"    # I

    .line 48
    iget-object v0, p0, Lcom/itau/empresas/ui/dialog/CustomAlertDialog_;->contentView_:Landroid/view/View;

    if-nez v0, :cond_6

    .line 49
    const/4 v0, 0x0

    return-object v0

    .line 51
    :cond_6
    iget-object v0, p0, Lcom/itau/empresas/ui/dialog/CustomAlertDialog_;->contentView_:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 40
    iget-object v0, p0, Lcom/itau/empresas/ui/dialog/CustomAlertDialog_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-static {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    move-result-object v1

    .line 41
    .local v1, "previousNotifier":Lorg/androidannotations/api/view/OnViewChangedNotifier;
    invoke-direct {p0, p1}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog_;->init_(Landroid/os/Bundle;)V

    .line 42
    invoke-super {p0, p1}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog;->onCreate(Landroid/os/Bundle;)V

    .line 43
    invoke-static {v1}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 44
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .registers 6
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .line 56
    invoke-super {p0, p1, p2, p3}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/ui/dialog/CustomAlertDialog_;->contentView_:Landroid/view/View;

    .line 57
    iget-object v0, p0, Lcom/itau/empresas/ui/dialog/CustomAlertDialog_;->contentView_:Landroid/view/View;

    if-nez v0, :cond_14

    .line 58
    const v0, 0x7f030085

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/ui/dialog/CustomAlertDialog_;->contentView_:Landroid/view/View;

    .line 60
    :cond_14
    iget-object v0, p0, Lcom/itau/empresas/ui/dialog/CustomAlertDialog_;->contentView_:Landroid/view/View;

    return-object v0
.end method

.method public onDestroyView()V
    .registers 2

    .line 65
    invoke-super {p0}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog;->onDestroyView()V

    .line 66
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/itau/empresas/ui/dialog/CustomAlertDialog_;->contentView_:Landroid/view/View;

    .line 67
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/itau/empresas/ui/dialog/CustomAlertDialog_;->textoMensagem:Landroid/widget/TextView;

    .line 68
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/itau/empresas/ui/dialog/CustomAlertDialog_;->botaoNeutro:Landroid/widget/Button;

    .line 69
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/itau/empresas/ui/dialog/CustomAlertDialog_;->botaoPositivo:Landroid/widget/Button;

    .line 70
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/itau/empresas/ui/dialog/CustomAlertDialog_;->botaoNegativo:Landroid/widget/Button;

    .line 71
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/itau/empresas/ui/dialog/CustomAlertDialog_;->rlDialogBotoesSimNao:Landroid/widget/RelativeLayout;

    .line 72
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/itau/empresas/ui/dialog/CustomAlertDialog_;->rlDialogUmBotao:Landroid/widget/RelativeLayout;

    .line 73
    return-void
.end method

.method public onViewChanged(Lorg/androidannotations/api/view/HasViews;)V
    .registers 4
    .param p1, "hasViews"    # Lorg/androidannotations/api/view/HasViews;

    .line 93
    const v0, 0x7f0e02a4

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/ui/dialog/CustomAlertDialog_;->textoMensagem:Landroid/widget/TextView;

    .line 94
    const v0, 0x7f0e038b

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/itau/empresas/ui/dialog/CustomAlertDialog_;->botaoNeutro:Landroid/widget/Button;

    .line 95
    const v0, 0x7f0e037a

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/itau/empresas/ui/dialog/CustomAlertDialog_;->botaoPositivo:Landroid/widget/Button;

    .line 96
    const v0, 0x7f0e037c

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/itau/empresas/ui/dialog/CustomAlertDialog_;->botaoNegativo:Landroid/widget/Button;

    .line 97
    const v0, 0x7f0e0379

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/itau/empresas/ui/dialog/CustomAlertDialog_;->rlDialogBotoesSimNao:Landroid/widget/RelativeLayout;

    .line 98
    const v0, 0x7f0e038a

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/itau/empresas/ui/dialog/CustomAlertDialog_;->rlDialogUmBotao:Landroid/widget/RelativeLayout;

    .line 99
    iget-object v0, p0, Lcom/itau/empresas/ui/dialog/CustomAlertDialog_;->botaoPositivo:Landroid/widget/Button;

    if-eqz v0, :cond_50

    .line 100
    iget-object v0, p0, Lcom/itau/empresas/ui/dialog/CustomAlertDialog_;->botaoPositivo:Landroid/widget/Button;

    new-instance v1, Lcom/itau/empresas/ui/dialog/CustomAlertDialog_$1;

    invoke-direct {v1, p0}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog_$1;-><init>(Lcom/itau/empresas/ui/dialog/CustomAlertDialog_;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 109
    :cond_50
    iget-object v0, p0, Lcom/itau/empresas/ui/dialog/CustomAlertDialog_;->botaoNegativo:Landroid/widget/Button;

    if-eqz v0, :cond_5e

    .line 110
    iget-object v0, p0, Lcom/itau/empresas/ui/dialog/CustomAlertDialog_;->botaoNegativo:Landroid/widget/Button;

    new-instance v1, Lcom/itau/empresas/ui/dialog/CustomAlertDialog_$2;

    invoke-direct {v1, p0}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog_$2;-><init>(Lcom/itau/empresas/ui/dialog/CustomAlertDialog_;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 119
    :cond_5e
    iget-object v0, p0, Lcom/itau/empresas/ui/dialog/CustomAlertDialog_;->botaoNeutro:Landroid/widget/Button;

    if-eqz v0, :cond_6c

    .line 120
    iget-object v0, p0, Lcom/itau/empresas/ui/dialog/CustomAlertDialog_;->botaoNeutro:Landroid/widget/Button;

    new-instance v1, Lcom/itau/empresas/ui/dialog/CustomAlertDialog_$3;

    invoke-direct {v1, p0}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog_$3;-><init>(Lcom/itau/empresas/ui/dialog/CustomAlertDialog_;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 129
    :cond_6c
    invoke-virtual {p0}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog_;->aoInicializar()V

    .line 130
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .registers 4
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;

    .line 83
    invoke-super {p0, p1, p2}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 84
    iget-object v0, p0, Lcom/itau/empresas/ui/dialog/CustomAlertDialog_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 85
    return-void
.end method

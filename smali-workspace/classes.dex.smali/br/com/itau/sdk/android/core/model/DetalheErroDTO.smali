.class public final Lbr/com/itau/sdk/android/core/model/DetalheErroDTO;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private campo:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "campo"
    .end annotation
.end field

.field private mensagem:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "mensagem"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getCampo()Ljava/lang/String;
    .registers 2

    .line 15
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/model/DetalheErroDTO;->campo:Ljava/lang/String;

    return-object v0
.end method

.method public getMensagem()Ljava/lang/String;
    .registers 2

    .line 23
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/model/DetalheErroDTO;->mensagem:Ljava/lang/String;

    return-object v0
.end method

.method public setCampo(Ljava/lang/String;)V
    .registers 2

    .line 19
    iput-object p1, p0, Lbr/com/itau/sdk/android/core/model/DetalheErroDTO;->campo:Ljava/lang/String;

    .line 20
    return-void
.end method

.method public setMensagem(Ljava/lang/String;)V
    .registers 2

    .line 27
    iput-object p1, p0, Lbr/com/itau/sdk/android/core/model/DetalheErroDTO;->mensagem:Ljava/lang/String;

    .line 28
    return-void
.end method

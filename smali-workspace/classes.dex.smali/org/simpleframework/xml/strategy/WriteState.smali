.class Lorg/simpleframework/xml/strategy/WriteState;
.super Lorg/simpleframework/xml/util/WeakCache;
.source "WriteState.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/simpleframework/xml/util/WeakCache<Lorg/simpleframework/xml/strategy/WriteGraph;>;"
    }
.end annotation


# instance fields
.field private contract:Lorg/simpleframework/xml/strategy/Contract;


# direct methods
.method public constructor <init>(Lorg/simpleframework/xml/strategy/Contract;)V
    .registers 2
    .param p1, "contract"    # Lorg/simpleframework/xml/strategy/Contract;

    .line 51
    invoke-direct {p0}, Lorg/simpleframework/xml/util/WeakCache;-><init>()V

    .line 52
    iput-object p1, p0, Lorg/simpleframework/xml/strategy/WriteState;->contract:Lorg/simpleframework/xml/strategy/Contract;

    .line 53
    return-void
.end method


# virtual methods
.method public find(Ljava/lang/Object;)Lorg/simpleframework/xml/strategy/WriteGraph;
    .registers 4
    .param p1, "map"    # Ljava/lang/Object;

    .line 66
    invoke-virtual {p0, p1}, Lorg/simpleframework/xml/strategy/WriteState;->fetch(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lorg/simpleframework/xml/strategy/WriteGraph;

    .line 68
    .local v1, "write":Lorg/simpleframework/xml/strategy/WriteGraph;
    if-nez v1, :cond_13

    .line 69
    new-instance v1, Lorg/simpleframework/xml/strategy/WriteGraph;

    iget-object v0, p0, Lorg/simpleframework/xml/strategy/WriteState;->contract:Lorg/simpleframework/xml/strategy/Contract;

    invoke-direct {v1, v0}, Lorg/simpleframework/xml/strategy/WriteGraph;-><init>(Lorg/simpleframework/xml/strategy/Contract;)V

    .line 70
    invoke-virtual {p0, p1, v1}, Lorg/simpleframework/xml/strategy/WriteState;->cache(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 72
    :cond_13
    return-object v1
.end method

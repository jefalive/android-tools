.class public final Lcom/itau/empresas/ui/view/AplicativoItemView_;
.super Lcom/itau/empresas/ui/view/AplicativoItemView;
.source "AplicativoItemView_.java"

# interfaces
.implements Lorg/androidannotations/api/view/HasViews;
.implements Lorg/androidannotations/api/view/OnViewChangedListener;


# instance fields
.field private alreadyInflated_:Z

.field private final onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;


# direct methods
.method public constructor <init>(Lcom/itau/empresas/ui/activity/BaseActivity;ILjava/lang/String;Ljava/lang/String;Z)V
    .registers 7
    .param p1, "activity"    # Lcom/itau/empresas/ui/activity/BaseActivity;
    .param p2, "imagemAplicativo"    # I
    .param p3, "nomeAplicativo"    # Ljava/lang/String;
    .param p4, "pacote"    # Ljava/lang/String;
    .param p5, "divisorVisivel"    # Z

    .line 37
    invoke-direct/range {p0 .. p5}, Lcom/itau/empresas/ui/view/AplicativoItemView;-><init>(Lcom/itau/empresas/ui/activity/BaseActivity;ILjava/lang/String;Ljava/lang/String;Z)V

    .line 33
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/itau/empresas/ui/view/AplicativoItemView_;->alreadyInflated_:Z

    .line 34
    new-instance v0, Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-direct {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/ui/view/AplicativoItemView_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 38
    invoke-direct {p0}, Lcom/itau/empresas/ui/view/AplicativoItemView_;->init_()V

    .line 39
    return-void
.end method

.method public static build(Lcom/itau/empresas/ui/activity/BaseActivity;ILjava/lang/String;Ljava/lang/String;Z)Lcom/itau/empresas/ui/view/AplicativoItemView;
    .registers 12
    .param p0, "activity"    # Lcom/itau/empresas/ui/activity/BaseActivity;
    .param p1, "imagemAplicativo"    # I
    .param p2, "nomeAplicativo"    # Ljava/lang/String;
    .param p3, "pacote"    # Ljava/lang/String;
    .param p4, "divisorVisivel"    # Z

    .line 42
    new-instance v0, Lcom/itau/empresas/ui/view/AplicativoItemView_;

    move-object v1, p0

    move v2, p1

    move-object v3, p2

    move-object v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/itau/empresas/ui/view/AplicativoItemView_;-><init>(Lcom/itau/empresas/ui/activity/BaseActivity;ILjava/lang/String;Ljava/lang/String;Z)V

    move-object v6, v0

    .line 43
    .local v6, "instance":Lcom/itau/empresas/ui/view/AplicativoItemView_;
    invoke-virtual {v6}, Lcom/itau/empresas/ui/view/AplicativoItemView_;->onFinishInflate()V

    .line 44
    return-object v6
.end method

.method private init_()V
    .registers 3

    .line 64
    iget-object v0, p0, Lcom/itau/empresas/ui/view/AplicativoItemView_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-static {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    move-result-object v1

    .line 65
    .local v1, "previousNotifier":Lorg/androidannotations/api/view/OnViewChangedNotifier;
    invoke-static {p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->registerOnViewChangedListener(Lorg/androidannotations/api/view/OnViewChangedListener;)V

    .line 66
    invoke-static {}, Lcom/itau/empresas/CustomApplication_;->getInstance()Lcom/itau/empresas/CustomApplication;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/ui/view/AplicativoItemView_;->application:Lcom/itau/empresas/CustomApplication;

    .line 67
    invoke-static {v1}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 68
    return-void
.end method


# virtual methods
.method public onFinishInflate()V
    .registers 3

    .line 55
    iget-boolean v0, p0, Lcom/itau/empresas/ui/view/AplicativoItemView_;->alreadyInflated_:Z

    if-nez v0, :cond_16

    .line 56
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/itau/empresas/ui/view/AplicativoItemView_;->alreadyInflated_:Z

    .line 57
    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/AplicativoItemView_;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0300d9

    invoke-static {v0, v1, p0}, Lcom/itau/empresas/ui/view/AplicativoItemView_;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 58
    iget-object v0, p0, Lcom/itau/empresas/ui/view/AplicativoItemView_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 60
    :cond_16
    invoke-super {p0}, Lcom/itau/empresas/ui/view/AplicativoItemView;->onFinishInflate()V

    .line 61
    return-void
.end method

.method public onViewChanged(Lorg/androidannotations/api/view/HasViews;)V
    .registers 4
    .param p1, "hasViews"    # Lorg/androidannotations/api/view/HasViews;

    .line 72
    const v0, 0x7f0e04e3

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/itau/empresas/ui/view/AplicativoItemView_;->imgItemAplicativo:Landroid/widget/ImageView;

    .line 73
    const v0, 0x7f0e04e5

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/ui/view/AplicativoItemView_;->textoItemAplicativoNome:Landroid/widget/TextView;

    .line 74
    const v0, 0x7f0e04e6

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/ui/view/AplicativoItemView_;->textoItemAplicativoStatus:Landroid/widget/TextView;

    .line 75
    const v0, 0x7f0e04e4

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/ui/view/AplicativoItemView_;->textoAplicativoLabel:Landroid/widget/TextView;

    .line 76
    const v0, 0x7f0e04e2

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/ui/view/AplicativoItemView_;->itemAplicativoDivisor:Landroid/view/View;

    .line 77
    const v0, 0x7f0e04e1

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 79
    .local v1, "view_ll_item_aplicativo":Landroid/view/View;
    if-eqz v1, :cond_46

    .line 80
    new-instance v0, Lcom/itau/empresas/ui/view/AplicativoItemView_$1;

    invoke-direct {v0, p0}, Lcom/itau/empresas/ui/view/AplicativoItemView_$1;-><init>(Lcom/itau/empresas/ui/view/AplicativoItemView_;)V

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 89
    :cond_46
    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/AplicativoItemView_;->inicializarViews()V

    .line 90
    return-void
.end method

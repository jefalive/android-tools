.class public Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosComprovanteContratacaoActivity;
.super Lcom/itau/empresas/ui/activity/BaseActivity;
.source "EmprestimosParceladosComprovanteContratacaoActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field devedorSolidario:Z

.field efetivacaoVO:Lcom/itau/empresas/feature/credito/giro/model/ResultadoEfetivacaoEmprestimosParceladosVO;

.field simulacaoVO:Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoSaidaVO;

.field temSeguro:Z

.field toolbar:Landroid/support/v7/widget/Toolbar;

.field viewCreditoDetalhadoComprovanteContratacao:Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 35
    invoke-direct {p0}, Lcom/itau/empresas/ui/activity/BaseActivity;-><init>()V

    return-void
.end method

.method private compartilhaPrintTelaComprovante(Landroid/view/View;)V
    .registers 7
    .param p1, "view"    # Landroid/view/View;

    .line 101
    const v0, 0x7f0e0626

    invoke-static {p1, v0}, Lcom/itau/empresas/ui/util/ViewUtils;->escondeView(Landroid/view/View;I)V

    .line 103
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 104
    .local v2, "views":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    invoke-interface {v2, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 106
    const/4 v3, 0x0

    .line 108
    .local v3, "file":Ljava/io/File;
    const-string v0, "credito_detalhado_contratacao_comprovante"

    .line 109
    :try_start_11
    invoke-static {p0, v2, v0}, Lcom/itau/empresas/ui/util/BitmapUtils;->salvarViewsPrint(Landroid/content/Context;Ljava/util/List;Ljava/lang/String;)Ljava/io/File;
    :try_end_14
    .catch Ljava/io/IOException; {:try_start_11 .. :try_end_14} :catch_17

    move-result-object v0

    move-object v3, v0

    .line 112
    goto :goto_27

    .line 110
    :catch_17
    move-exception v4

    .line 111
    .local v4, "e":Ljava/io/IOException;
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v4}, Lbr/com/itau/textovoz/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 114
    .end local v4    # "e":Ljava/io/IOException;
    :goto_27
    if-eqz v3, :cond_2c

    .line 115
    invoke-static {p0, v3}, Lcom/itau/empresas/ui/util/CompartilhamentoUtils;->compartilharArquivoImagem(Landroid/app/Activity;Ljava/io/File;)V

    .line 117
    :cond_2c
    return-void
.end method

.method private constroiToolbar()V
    .registers 3

    .line 73
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosComprovanteContratacaoActivity;->toolbar:Landroid/support/v7/widget/Toolbar;

    invoke-static {v0, p0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom;->Builder(Landroid/support/v7/widget/Toolbar;Landroid/support/v7/app/AppCompatActivity;)Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    move-result-object v0

    .line 75
    const v1, 0x7f07053d

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->comTitulo(I)Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    move-result-object v0

    .line 76
    invoke-virtual {v0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->comClickListenerFinish()Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    move-result-object v0

    .line 77
    invoke-virtual {v0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->build()Lcom/itau/empresas/ui/util/listener/ToolbarCustom;

    .line 78
    return-void
.end method

.method private verificaPermissoes()V
    .registers 4

    .line 55
    .line 56
    invoke-static {}, Lcom/itau/empresas/ui/util/PermissoesUtil;->getPermissionWriteExternalStorage()[Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/itau/empresas/ui/util/PermissoesUtil;->hasSelfPermissions(Landroid/content/Context;[Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_19

    .line 57
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x17

    if-lt v0, v1, :cond_2d

    .line 58
    invoke-static {}, Lcom/itau/empresas/ui/util/PermissoesUtil;->getPermissionWriteExternalStorage()[Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosComprovanteContratacaoActivity;->requestPermissions([Ljava/lang/String;I)V

    goto :goto_2d

    .line 63
    :cond_19
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosComprovanteContratacaoActivity;->viewCreditoDetalhadoComprovanteContratacao:Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;

    iget-object v0, v0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->svEmprestimosParceladosSimulacao:Landroid/widget/ScrollView;

    .line 64
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 65
    .local v2, "view":Landroid/view/View;
    invoke-direct {p0, v2}, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosComprovanteContratacaoActivity;->compartilhaPrintTelaComprovante(Landroid/view/View;)V

    .line 67
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosComprovanteContratacaoActivity;->viewCreditoDetalhadoComprovanteContratacao:Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;

    iget-object v0, v0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->botaoCreditoDetalhadoCompartilhar:Landroid/widget/Button;

    .line 68
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 70
    .end local v2    # "view":Landroid/view/View;
    :cond_2d
    :goto_2d
    return-void
.end method


# virtual methods
.method aoCarregarElementosDeTela()V
    .registers 9

    .line 48
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosComprovanteContratacaoActivity;->constroiToolbar()V

    .line 49
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosComprovanteContratacaoActivity;->viewCreditoDetalhadoComprovanteContratacao:Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosComprovanteContratacaoActivity;->simulacaoVO:Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoSaidaVO;

    sget-object v2, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView$EnumTipoExibicao;->SIMULACAO_COMPROVANTE_EFETIVACAO:Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView$EnumTipoExibicao;

    iget-boolean v3, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosComprovanteContratacaoActivity;->temSeguro:Z

    iget-boolean v4, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosComprovanteContratacaoActivity;->devedorSolidario:Z

    iget-object v5, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosComprovanteContratacaoActivity;->efetivacaoVO:Lcom/itau/empresas/feature/credito/giro/model/ResultadoEfetivacaoEmprestimosParceladosVO;

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->bind(Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoSaidaVO;Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView$EnumTipoExibicao;ZZLcom/itau/empresas/feature/credito/giro/model/ResultadoEfetivacaoEmprestimosParceladosVO;ZZ)V

    .line 52
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .registers 2
    .param p1, "v"    # Landroid/view/View;

    .line 124
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosComprovanteContratacaoActivity;->onBackPressed()V

    .line 125
    return-void
.end method

.method public onEventMainThread(Lcom/itau/empresas/Evento$EventoSolicitarVerificacaoPermissaoCompartilhar;)V
    .registers 2
    .param p1, "evento"    # Lcom/itau/empresas/Evento$EventoSolicitarVerificacaoPermissaoCompartilhar;

    .line 82
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosComprovanteContratacaoActivity;->verificaPermissoes()V

    .line 83
    return-void
.end method

.method public onRequestPermissionsResult(I[Ljava/lang/String;[I)V
    .registers 7
    .param p1, "requestCode"    # I
    .param p2, "permissions"    # [Ljava/lang/String;
    .param p3, "grantResults"    # [I

    .line 87
    const/4 v0, 0x4

    if-ne p1, v0, :cond_28

    array-length v0, p3

    if-lez v0, :cond_28

    const/4 v0, 0x0

    aget v0, p3, v0

    if-nez v0, :cond_28

    .line 89
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosComprovanteContratacaoActivity;->viewCreditoDetalhadoComprovanteContratacao:Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;

    iget-object v0, v0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->botaoCreditoDetalhadoCompartilhar:Landroid/widget/Button;

    .line 90
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 91
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosComprovanteContratacaoActivity;->viewCreditoDetalhadoComprovanteContratacao:Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;

    iget-object v0, v0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->svEmprestimosParceladosSimulacao:Landroid/widget/ScrollView;

    .line 92
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 93
    .local v2, "view":Landroid/view/View;
    invoke-direct {p0, v2}, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosComprovanteContratacaoActivity;->compartilhaPrintTelaComprovante(Landroid/view/View;)V

    .line 94
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosComprovanteContratacaoActivity;->viewCreditoDetalhadoComprovanteContratacao:Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;

    iget-object v0, v0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->botaoCreditoDetalhadoCompartilhar:Landroid/widget/Button;

    .line 95
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 97
    .end local v2    # "view":Landroid/view/View;
    :cond_28
    return-void
.end method

.method public opkeysConsumidas()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Ljava/lang/String;>;"
        }
    .end annotation

    .line 120
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

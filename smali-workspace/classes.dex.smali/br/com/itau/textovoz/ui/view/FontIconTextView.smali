.class public Lbr/com/itau/textovoz/ui/view/FontIconTextView;
.super Landroid/widget/TextView;
.source "FontIconTextView.java"


# static fields
.field private static sTypefaceCache:Landroid/support/v4/util/LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/support/v4/util/LruCache<Ljava/lang/String;Landroid/graphics/Typeface;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .line 12
    new-instance v0, Landroid/support/v4/util/LruCache;

    const/16 v1, 0xc

    invoke-direct {v0, v1}, Landroid/support/v4/util/LruCache;-><init>(I)V

    sput-object v0, Lbr/com/itau/textovoz/ui/view/FontIconTextView;->sTypefaceCache:Landroid/support/v4/util/LruCache;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .param p1, "context"    # Landroid/content/Context;

    .line 15
    invoke-direct {p0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 16
    invoke-virtual {p0}, Lbr/com/itau/textovoz/ui/view/FontIconTextView;->isInEditMode()Z

    move-result v0

    if-nez v0, :cond_c

    .line 17
    invoke-virtual {p0}, Lbr/com/itau/textovoz/ui/view/FontIconTextView;->init()V

    .line 19
    :cond_c
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .line 29
    invoke-direct {p0, p1, p2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 30
    invoke-virtual {p0}, Lbr/com/itau/textovoz/ui/view/FontIconTextView;->isInEditMode()Z

    move-result v0

    if-nez v0, :cond_c

    .line 31
    invoke-virtual {p0}, Lbr/com/itau/textovoz/ui/view/FontIconTextView;->init()V

    .line 33
    :cond_c
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .line 22
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 23
    invoke-virtual {p0}, Lbr/com/itau/textovoz/ui/view/FontIconTextView;->isInEditMode()Z

    move-result v0

    if-nez v0, :cond_c

    .line 24
    invoke-virtual {p0}, Lbr/com/itau/textovoz/ui/view/FontIconTextView;->init()V

    .line 26
    :cond_c
    return-void
.end method


# virtual methods
.method public init()V
    .registers 5

    .line 36
    sget-object v0, Lbr/com/itau/textovoz/ui/view/FontIconTextView;->sTypefaceCache:Landroid/support/v4/util/LruCache;

    invoke-virtual {p0}, Lbr/com/itau/textovoz/ui/view/FontIconTextView;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lbr/com/itau/textovoz/R$string;->font_name_ttf_itau_name:I

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Landroid/graphics/Typeface;

    .line 38
    .local v3, "typeface":Landroid/graphics/Typeface;
    if-nez v3, :cond_28

    .line 39
    invoke-static {}, Lbr/com/itau/textovoz/ui/view/FontIconTypefaceHolder;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v3

    .line 40
    sget-object v0, Lbr/com/itau/textovoz/ui/view/FontIconTextView;->sTypefaceCache:Landroid/support/v4/util/LruCache;

    invoke-virtual {p0}, Lbr/com/itau/textovoz/ui/view/FontIconTextView;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lbr/com/itau/textovoz/R$string;->font_name_ttf_itau_name:I

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v3}, Landroid/support/v4/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 42
    :cond_28
    invoke-virtual {p0, v3}, Lbr/com/itau/textovoz/ui/view/FontIconTextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 43
    return-void
.end method

.class abstract Lcom/google/android/gms/analytics/internal/zzq;
.super Lcom/google/android/gms/analytics/internal/zzc;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/analytics/internal/zzq$zza;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::Lcom/google/android/gms/analytics/internal/zzp;>Lcom/google/android/gms/analytics/internal/zzc;"
    }
.end annotation


# instance fields
.field zzRx:Lcom/google/android/gms/analytics/internal/zzq$zza;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/analytics/internal/zzq$zza<TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/android/gms/analytics/internal/zzf;Lcom/google/android/gms/analytics/internal/zzq$zza;)V
    .registers 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Lcom/google/android/gms/analytics/internal/zzf;Lcom/google/android/gms/analytics/internal/zzq$zza<TT;>;)V"
        }
    .end annotation

    invoke-direct {p0, p1}, Lcom/google/android/gms/analytics/internal/zzc;-><init>(Lcom/google/android/gms/analytics/internal/zzf;)V

    iput-object p2, p0, Lcom/google/android/gms/analytics/internal/zzq;->zzRx:Lcom/google/android/gms/analytics/internal/zzq$zza;

    return-void
.end method

.method private zza(Landroid/content/res/XmlResourceParser;)Lcom/google/android/gms/analytics/internal/zzp;
    .registers 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Landroid/content/res/XmlResourceParser;)TT;"
        }
    .end annotation

    :try_start_0
    invoke-interface {p1}, Landroid/content/res/XmlResourceParser;->next()I

    invoke-interface {p1}, Landroid/content/res/XmlResourceParser;->getEventType()I

    move-result v2

    :goto_7
    const/4 v0, 0x1

    if-eq v2, v0, :cond_d6

    invoke-interface {p1}, Landroid/content/res/XmlResourceParser;->getEventType()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_d0

    invoke-interface {p1}, Landroid/content/res/XmlResourceParser;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3

    const-string v0, "screenname"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_43

    const-string v0, "name"

    const/4 v1, 0x0

    invoke-interface {p1, v1, v0}, Landroid/content/res/XmlResourceParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {p1}, Landroid/content/res/XmlResourceParser;->nextText()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_41

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_41

    iget-object v0, p0, Lcom/google/android/gms/analytics/internal/zzq;->zzRx:Lcom/google/android/gms/analytics/internal/zzq$zza;

    invoke-interface {v0, v4, v5}, Lcom/google/android/gms/analytics/internal/zzq$zza;->zzj(Ljava/lang/String;Ljava/lang/String;)V

    :cond_41
    goto/16 :goto_d0

    :cond_43
    const-string v0, "string"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_69

    const-string v0, "name"

    const/4 v1, 0x0

    invoke-interface {p1, v1, v0}, Landroid/content/res/XmlResourceParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {p1}, Landroid/content/res/XmlResourceParser;->nextText()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_67

    if-eqz v5, :cond_67

    iget-object v0, p0, Lcom/google/android/gms/analytics/internal/zzq;->zzRx:Lcom/google/android/gms/analytics/internal/zzq$zza;

    invoke-interface {v0, v4, v5}, Lcom/google/android/gms/analytics/internal/zzq$zza;->zzk(Ljava/lang/String;Ljava/lang/String;)V

    :cond_67
    goto/16 :goto_d0

    :cond_69
    const-string v0, "bool"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9d

    const-string v0, "name"

    const/4 v1, 0x0

    invoke-interface {p1, v1, v0}, Landroid/content/res/XmlResourceParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {p1}, Landroid/content/res/XmlResourceParser;->nextText()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_9c

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z
    :try_end_89
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_89} :catch_d7
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_89} :catch_de

    move-result v0

    if-nez v0, :cond_9c

    :try_start_8c
    invoke-static {v5}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v6

    iget-object v0, p0, Lcom/google/android/gms/analytics/internal/zzq;->zzRx:Lcom/google/android/gms/analytics/internal/zzq$zza;

    invoke-interface {v0, v4, v6}, Lcom/google/android/gms/analytics/internal/zzq$zza;->zzf(Ljava/lang/String;Z)V
    :try_end_95
    .catch Ljava/lang/NumberFormatException; {:try_start_8c .. :try_end_95} :catch_96
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_8c .. :try_end_95} :catch_d7
    .catch Ljava/io/IOException; {:try_start_8c .. :try_end_95} :catch_de

    goto :goto_9c

    :catch_96
    move-exception v6

    const-string v0, "Error parsing bool configuration value"

    :try_start_99
    invoke-virtual {p0, v0, v5, v6}, Lcom/google/android/gms/analytics/internal/zzq;->zzc(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    :cond_9c
    :goto_9c
    goto :goto_d0

    :cond_9d
    const-string v0, "integer"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d0

    const-string v0, "name"

    const/4 v1, 0x0

    invoke-interface {p1, v1, v0}, Landroid/content/res/XmlResourceParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {p1}, Landroid/content/res/XmlResourceParser;->nextText()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_d0

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z
    :try_end_bd
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_99 .. :try_end_bd} :catch_d7
    .catch Ljava/io/IOException; {:try_start_99 .. :try_end_bd} :catch_de

    move-result v0

    if-nez v0, :cond_d0

    :try_start_c0
    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    iget-object v0, p0, Lcom/google/android/gms/analytics/internal/zzq;->zzRx:Lcom/google/android/gms/analytics/internal/zzq$zza;

    invoke-interface {v0, v4, v6}, Lcom/google/android/gms/analytics/internal/zzq$zza;->zzc(Ljava/lang/String;I)V
    :try_end_c9
    .catch Ljava/lang/NumberFormatException; {:try_start_c0 .. :try_end_c9} :catch_ca
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_c0 .. :try_end_c9} :catch_d7
    .catch Ljava/io/IOException; {:try_start_c0 .. :try_end_c9} :catch_de

    goto :goto_d0

    :catch_ca
    move-exception v6

    const-string v0, "Error parsing int configuration value"

    :try_start_cd
    invoke-virtual {p0, v0, v5, v6}, Lcom/google/android/gms/analytics/internal/zzq;->zzc(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    :cond_d0
    :goto_d0
    invoke-interface {p1}, Landroid/content/res/XmlResourceParser;->next()I
    :try_end_d3
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_cd .. :try_end_d3} :catch_d7
    .catch Ljava/io/IOException; {:try_start_cd .. :try_end_d3} :catch_de

    move-result v2

    goto/16 :goto_7

    :cond_d6
    goto :goto_e4

    :catch_d7
    move-exception v2

    const-string v0, "Error parsing tracker configuration file"

    invoke-virtual {p0, v0, v2}, Lcom/google/android/gms/analytics/internal/zzq;->zze(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_e4

    :catch_de
    move-exception v2

    const-string v0, "Error parsing tracker configuration file"

    invoke-virtual {p0, v0, v2}, Lcom/google/android/gms/analytics/internal/zzq;->zze(Ljava/lang/String;Ljava/lang/Object;)V

    :goto_e4
    iget-object v0, p0, Lcom/google/android/gms/analytics/internal/zzq;->zzRx:Lcom/google/android/gms/analytics/internal/zzq$zza;

    invoke-interface {v0}, Lcom/google/android/gms/analytics/internal/zzq$zza;->zzkq()Lcom/google/android/gms/analytics/internal/zzp;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public zzah(I)Lcom/google/android/gms/analytics/internal/zzp;
    .registers 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TT;"
        }
    .end annotation

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/analytics/internal/zzq;->zzji()Lcom/google/android/gms/analytics/internal/zzf;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/analytics/internal/zzf;->zzjx()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getXml(I)Landroid/content/res/XmlResourceParser;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/analytics/internal/zzq;->zza(Landroid/content/res/XmlResourceParser;)Lcom/google/android/gms/analytics/internal/zzp;
    :try_end_13
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_13} :catch_15

    move-result-object v0

    return-object v0

    :catch_15
    move-exception v1

    const-string v0, "inflate() called with unknown resourceId"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/analytics/internal/zzq;->zzd(Ljava/lang/String;Ljava/lang/Object;)V

    const/4 v0, 0x0

    return-object v0
.end method

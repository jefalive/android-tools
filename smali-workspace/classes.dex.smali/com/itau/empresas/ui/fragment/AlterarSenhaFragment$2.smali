.class Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment$2;
.super Ljava/lang/Object;
.source "AlterarSenhaFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment;->onEventMainThread(Lcom/itau/empresas/api/model/ResultadoCadastroSenhaEletronicaVO;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment;

.field final synthetic val$dialog:Lcom/itau/empresas/ui/dialog/CustomAlertDialog;


# direct methods
.method constructor <init>(Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment;Lcom/itau/empresas/ui/dialog/CustomAlertDialog;)V
    .registers 3
    .param p1, "this$0"    # Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment;

    .line 169
    iput-object p1, p0, Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment$2;->this$0:Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment;

    iput-object p2, p0, Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment$2;->val$dialog:Lcom/itau/empresas/ui/dialog/CustomAlertDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .registers 4
    .param p1, "view"    # Landroid/view/View;

    .line 172
    iget-object v0, p0, Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment$2;->val$dialog:Lcom/itau/empresas/ui/dialog/CustomAlertDialog;

    invoke-virtual {v0}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog;->dismiss()V

    .line 173
    iget-object v0, p0, Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment$2;->this$0:Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment;

    iget-object v0, v0, Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment;->trocaObrigatoria:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_19

    iget-object v0, p0, Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment$2;->this$0:Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment;

    iget-object v0, v0, Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment;->primeiroAcesso:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_22

    .line 175
    :cond_19
    iget-object v0, p0, Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment$2;->this$0:Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment;

    invoke-static {v0}, Lcom/itau/empresas/feature/home/HomeLogadaActivity;->intent(Landroid/support/v4/app/Fragment;)Lcom/itau/empresas/feature/home/HomeLogadaActivity_$IntentBuilder_;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/feature/home/HomeLogadaActivity_$IntentBuilder_;->start()Lorg/androidannotations/api/builder/PostActivityStarter;

    .line 177
    :cond_22
    iget-object v0, p0, Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment$2;->this$0:Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment;

    invoke-virtual {v0}, Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    .line 178
    .local v1, "act":Landroid/app/Activity;
    if-eqz v1, :cond_2d

    .line 179
    invoke-virtual {v1}, Landroid/app/Activity;->finish()V

    .line 181
    :cond_2d
    return-void
.end method

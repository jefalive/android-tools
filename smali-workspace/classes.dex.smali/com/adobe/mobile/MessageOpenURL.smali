.class final Lcom/adobe/mobile/MessageOpenURL;
.super Lcom/adobe/mobile/MessageTemplateCallback;
.source "MessageOpenURL.java"


# direct methods
.method constructor <init>()V
    .registers 1

    .line 26
    invoke-direct {p0}, Lcom/adobe/mobile/MessageTemplateCallback;-><init>()V

    return-void
.end method


# virtual methods
.method protected logPrefix()Ljava/lang/String;
    .registers 2

    .line 63
    const-string v0, "OpenURL"

    return-object v0
.end method

.method protected show()V
    .registers 8

    .line 34
    :try_start_0
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getCurrentActivity()Landroid/app/Activity;
    :try_end_3
    .catch Lcom/adobe/mobile/StaticMethods$NullActivityException; {:try_start_0 .. :try_end_3} :catch_5

    move-result-object v4

    .line 39
    .local v4, "currentActivity":Landroid/app/Activity;
    goto :goto_11

    .line 36
    .end local v4    # "currentActivity":Landroid/app/Activity;
    :catch_5
    move-exception v5

    .line 37
    .local v5, "ex":Lcom/adobe/mobile/StaticMethods$NullActivityException;
    invoke-virtual {v5}, Lcom/adobe/mobile/StaticMethods$NullActivityException;->getMessage()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logErrorFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 38
    return-void

    .line 42
    .local v4, "currentActivity":Landroid/app/Activity;
    .end local v5    # "ex":Lcom/adobe/mobile/StaticMethods$NullActivityException;
    :goto_11
    iget-object v0, p0, Lcom/adobe/mobile/MessageOpenURL;->showRule:Lcom/adobe/mobile/Messages$MessageShowRule;

    sget-object v1, Lcom/adobe/mobile/Messages$MessageShowRule;->MESSAGE_SHOW_RULE_ONCE:Lcom/adobe/mobile/Messages$MessageShowRule;

    if-ne v0, v1, :cond_1a

    .line 43
    invoke-virtual {p0}, Lcom/adobe/mobile/MessageOpenURL;->blacklist()V

    .line 46
    :cond_1a
    invoke-virtual {p0}, Lcom/adobe/mobile/MessageOpenURL;->getExpandedUrl()Ljava/lang/String;

    move-result-object v5

    .line 48
    .local v5, "expandedUrl":Ljava/lang/String;
    const-string v0, "%s - Creating intent with uri: %s"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/adobe/mobile/MessageOpenURL;->logPrefix()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const/4 v2, 0x1

    aput-object v5, v1, v2

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logDebugFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 51
    :try_start_30
    new-instance v6, Landroid/content/Intent;

    const-string v0, "android.intent.action.VIEW"

    invoke-direct {v6, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 52
    .local v6, "intent":Landroid/content/Intent;
    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v6, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 53
    invoke-virtual {v4, v6}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V
    :try_end_41
    .catch Ljava/lang/Exception; {:try_start_30 .. :try_end_41} :catch_42

    .line 57
    .end local v6    # "intent":Landroid/content/Intent;
    goto :goto_59

    .line 55
    :catch_42
    move-exception v6

    .line 56
    .local v6, "ex":Ljava/lang/Exception;
    const-string v0, "%s - Could not load intent for message (%s)"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/adobe/mobile/MessageOpenURL;->logPrefix()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-virtual {v6}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logDebugFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 59
    .end local v6    # "ex":Ljava/lang/Exception;
    :goto_59
    return-void
.end method

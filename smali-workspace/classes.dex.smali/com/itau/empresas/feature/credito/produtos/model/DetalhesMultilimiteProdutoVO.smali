.class public Lcom/itau/empresas/feature/credito/produtos/model/DetalhesMultilimiteProdutoVO;
.super Ljava/lang/Object;
.source "DetalhesMultilimiteProdutoVO.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private atributos:Ljava/util/List;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "atributos"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Lcom/itau/empresas/feature/credito/produtos/model/DetalhesMultilimiteAtributoVO;>;"
        }
    .end annotation
.end field

.field private titulo:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "titulo"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getAtributos()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Lcom/itau/empresas/feature/credito/produtos/model/DetalhesMultilimiteAtributoVO;>;"
        }
    .end annotation

    .line 24
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/model/DetalhesMultilimiteProdutoVO;->atributos:Ljava/util/List;

    return-object v0
.end method

.method public getTitulo()Ljava/lang/String;
    .registers 2

    .line 16
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/model/DetalhesMultilimiteProdutoVO;->titulo:Ljava/lang/String;

    return-object v0
.end method

.method public setAtributos(Ljava/util/List;)V
    .registers 2
    .param p1, "atributos"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<Lcom/itau/empresas/feature/credito/produtos/model/DetalhesMultilimiteAtributoVO;>;)V"
        }
    .end annotation

    .line 28
    iput-object p1, p0, Lcom/itau/empresas/feature/credito/produtos/model/DetalhesMultilimiteProdutoVO;->atributos:Ljava/util/List;

    .line 29
    return-void
.end method

.method public setTitulo(Ljava/lang/String;)V
    .registers 2
    .param p1, "titulo"    # Ljava/lang/String;

    .line 20
    iput-object p1, p0, Lcom/itau/empresas/feature/credito/produtos/model/DetalhesMultilimiteProdutoVO;->titulo:Ljava/lang/String;

    .line 21
    return-void
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    .line 34
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "DetalhesMultilimiteProdutoVO{titulo=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/produtos/model/DetalhesMultilimiteProdutoVO;->titulo:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", atributos="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/produtos/model/DetalhesMultilimiteProdutoVO;->atributos:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.class public abstract Lbr/com/itau/widgets/graphicalfriedcake/charts/listener/ChartTouchListener;
.super Landroid/view/GestureDetector$SimpleOnGestureListener;
.source "ChartTouchListener.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart<*>;>Landroid/view/GestureDetector$SimpleOnGestureListener;Landroid/view/View$OnTouchListener;"
    }
.end annotation


# static fields
.field protected static final DRAG:I = 0x1

.field protected static final NONE:I = 0x0

.field protected static final PINCH_ZOOM:I = 0x4

.field protected static final POST_ZOOM:I = 0x5

.field protected static final ROTATE:I = 0x6

.field protected static final X_ZOOM:I = 0x2

.field protected static final Y_ZOOM:I = 0x3


# instance fields
.field protected mChart:Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field protected mGestureDetector:Landroid/view/GestureDetector;

.field protected mLastHighlighted:Lbr/com/itau/widgets/graphicalfriedcake/charts/highlight/Highlight;

.field protected mTouchMode:I


# direct methods
.method public constructor <init>(Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;)V
    .registers 4
    .param p1, "chart"    # Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .line 42
    invoke-direct {p0}, Landroid/view/GestureDetector$SimpleOnGestureListener;-><init>()V

    .line 27
    const/4 v0, 0x0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/listener/ChartTouchListener;->mTouchMode:I

    .line 43
    iput-object p1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/listener/ChartTouchListener;->mChart:Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;

    .line 46
    new-instance v0, Landroid/view/GestureDetector;

    invoke-virtual {p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/listener/ChartTouchListener;->mGestureDetector:Landroid/view/GestureDetector;

    .line 47
    return-void
.end method

.method protected static distance(FFFF)F
    .registers 8
    .param p0, "eventX"    # F
    .param p1, "startX"    # F
    .param p2, "eventY"    # F
    .param p3, "startY"    # F

    .line 79
    sub-float v2, p0, p1

    .line 80
    .local v2, "dx":F
    sub-float v3, p2, p3

    .line 81
    .local v3, "dy":F
    mul-float v0, v2, v2

    mul-float v1, v3, v3

    add-float/2addr v0, v1

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    double-to-float v0, v0

    return v0
.end method


# virtual methods
.method public getTouchMode()I
    .registers 2

    .line 65
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/listener/ChartTouchListener;->mTouchMode:I

    return v0
.end method

.method public setLastHighlighted(Lbr/com/itau/widgets/graphicalfriedcake/charts/highlight/Highlight;)V
    .registers 2
    .param p1, "high"    # Lbr/com/itau/widgets/graphicalfriedcake/charts/highlight/Highlight;

    .line 55
    iput-object p1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/listener/ChartTouchListener;->mLastHighlighted:Lbr/com/itau/widgets/graphicalfriedcake/charts/highlight/Highlight;

    .line 56
    return-void
.end method

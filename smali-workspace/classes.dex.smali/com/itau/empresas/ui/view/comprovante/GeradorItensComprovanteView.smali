.class Lcom/itau/empresas/ui/view/comprovante/GeradorItensComprovanteView;
.super Ljava/lang/Object;
.source "GeradorItensComprovanteView.java"


# static fields
.field private static final ZERO:I


# instance fields
.field private final context:Landroid/content/Context;

.field private final itensComprovante:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Lcom/itau/empresas/ui/view/comprovante/ItensComprovanteView;>;"
        }
    .end annotation
.end field

.field private final layoutParamTop:I

.field private final targetLayout:Landroid/widget/LinearLayout;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .line 16
    sget-object v0, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    invoke-virtual {v0}, Ljava/math/BigDecimal;->intValue()I

    move-result v0

    sput v0, Lcom/itau/empresas/ui/view/comprovante/GeradorItensComprovanteView;->ZERO:I

    return-void
.end method

.method constructor <init>(Ljava/util/List;Landroid/content/Context;Landroid/widget/LinearLayout;)V
    .registers 6
    .param p1, "itensComprovante"    # Ljava/util/List;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "targetLayout"    # Landroid/widget/LinearLayout;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<Lcom/itau/empresas/ui/view/comprovante/ItensComprovanteView;>;Landroid/content/Context;Landroid/widget/LinearLayout;)V"
        }
    .end annotation

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/itau/empresas/ui/view/comprovante/GeradorItensComprovanteView;->itensComprovante:Ljava/util/List;

    .line 25
    iput-object p2, p0, Lcom/itau/empresas/ui/view/comprovante/GeradorItensComprovanteView;->context:Landroid/content/Context;

    .line 26
    iput-object p3, p0, Lcom/itau/empresas/ui/view/comprovante/GeradorItensComprovanteView;->targetLayout:Landroid/widget/LinearLayout;

    .line 27
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 28
    const v1, 0x7f0801e4

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/itau/empresas/ui/view/comprovante/GeradorItensComprovanteView;->layoutParamTop:I

    .line 29
    return-void
.end method

.method private buildItem(Lcom/itau/empresas/ui/view/comprovante/ItensComprovanteObservacaoView;)Landroid/view/View;
    .registers 9
    .param p1, "itemComprovante"    # Lcom/itau/empresas/ui/view/comprovante/ItensComprovanteObservacaoView;

    .line 62
    iget-object v0, p0, Lcom/itau/empresas/ui/view/comprovante/GeradorItensComprovanteView;->context:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f03017e

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 64
    .local v3, "item":Landroid/view/View;
    const v0, 0x7f0e067c

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Landroid/widget/TextView;

    .line 65
    .local v4, "titulo":Landroid/widget/TextView;
    invoke-virtual {p1}, Lcom/itau/empresas/ui/view/comprovante/ItensComprovanteObservacaoView;->getTitulo()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 67
    const v0, 0x7f0e067d

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Landroid/widget/TextView;

    .line 68
    .local v5, "desc":Landroid/widget/TextView;
    invoke-virtual {p1}, Lcom/itau/empresas/ui/view/comprovante/ItensComprovanteObservacaoView;->getDescricao()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 70
    const v0, 0x7f0e067e

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Landroid/widget/TextView;

    .line 71
    .local v6, "observacao":Landroid/widget/TextView;
    invoke-virtual {p1}, Lcom/itau/empresas/ui/view/comprovante/ItensComprovanteObservacaoView;->getObservacao()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 73
    return-object v3
.end method

.method private buildItem(Lcom/itau/empresas/ui/view/comprovante/ItensComprovanteView;)Landroid/view/View;
    .registers 9
    .param p1, "itemComprovante"    # Lcom/itau/empresas/ui/view/comprovante/ItensComprovanteView;

    .line 47
    iget-object v0, p0, Lcom/itau/empresas/ui/view/comprovante/GeradorItensComprovanteView;->context:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f03017e

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 49
    .local v3, "item":Landroid/view/View;
    const v0, 0x7f0e067c

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Landroid/widget/TextView;

    .line 50
    .local v4, "titulo":Landroid/widget/TextView;
    invoke-virtual {p1}, Lcom/itau/empresas/ui/view/comprovante/ItensComprovanteView;->getTitulo()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 52
    const v0, 0x7f0e067d

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Landroid/widget/TextView;

    .line 53
    .local v5, "desc":Landroid/widget/TextView;
    invoke-virtual {p1}, Lcom/itau/empresas/ui/view/comprovante/ItensComprovanteView;->getDescricao()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 55
    const v0, 0x7f0e067e

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Landroid/widget/TextView;

    .line 56
    .local v6, "observacao":Landroid/widget/TextView;
    const/16 v0, 0x8

    invoke-virtual {v6, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 58
    return-object v3
.end method

.method private getLayoutParams()Landroid/widget/LinearLayout$LayoutParams;
    .registers 6

    .line 77
    new-instance v4, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v0, -0x1

    const/4 v1, -0x2

    invoke-direct {v4, v0, v1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 80
    .local v4, "layoutParams":Landroid/widget/LinearLayout$LayoutParams;
    sget v0, Lcom/itau/empresas/ui/view/comprovante/GeradorItensComprovanteView;->ZERO:I

    iget v1, p0, Lcom/itau/empresas/ui/view/comprovante/GeradorItensComprovanteView;->layoutParamTop:I

    sget v2, Lcom/itau/empresas/ui/view/comprovante/GeradorItensComprovanteView;->ZERO:I

    sget v3, Lcom/itau/empresas/ui/view/comprovante/GeradorItensComprovanteView;->ZERO:I

    invoke-virtual {v4, v0, v1, v2, v3}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 82
    return-object v4
.end method


# virtual methods
.method init()V
    .registers 6

    .line 32
    iget-object v0, p0, Lcom/itau/empresas/ui/view/comprovante/GeradorItensComprovanteView;->itensComprovante:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_6
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_36

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/itau/empresas/ui/view/comprovante/ItensComprovanteView;

    .line 34
    .local v4, "itemComprovante":Lcom/itau/empresas/ui/view/comprovante/ItensComprovanteView;
    instance-of v0, v4, Lcom/itau/empresas/ui/view/comprovante/ItensComprovanteObservacaoView;

    if-eqz v0, :cond_28

    .line 36
    iget-object v0, p0, Lcom/itau/empresas/ui/view/comprovante/GeradorItensComprovanteView;->targetLayout:Landroid/widget/LinearLayout;

    move-object v1, v4

    check-cast v1, Lcom/itau/empresas/ui/view/comprovante/ItensComprovanteObservacaoView;

    .line 37
    invoke-direct {p0, v1}, Lcom/itau/empresas/ui/view/comprovante/GeradorItensComprovanteView;->buildItem(Lcom/itau/empresas/ui/view/comprovante/ItensComprovanteObservacaoView;)Landroid/view/View;

    move-result-object v1

    .line 38
    invoke-direct {p0}, Lcom/itau/empresas/ui/view/comprovante/GeradorItensComprovanteView;->getLayoutParams()Landroid/widget/LinearLayout$LayoutParams;

    move-result-object v2

    .line 37
    invoke-virtual {v0, v1, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_35

    .line 41
    :cond_28
    iget-object v0, p0, Lcom/itau/empresas/ui/view/comprovante/GeradorItensComprovanteView;->targetLayout:Landroid/widget/LinearLayout;

    invoke-direct {p0, v4}, Lcom/itau/empresas/ui/view/comprovante/GeradorItensComprovanteView;->buildItem(Lcom/itau/empresas/ui/view/comprovante/ItensComprovanteView;)Landroid/view/View;

    move-result-object v1

    invoke-direct {p0}, Lcom/itau/empresas/ui/view/comprovante/GeradorItensComprovanteView;->getLayoutParams()Landroid/widget/LinearLayout$LayoutParams;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 43
    .end local v4    # "itemComprovante":Lcom/itau/empresas/ui/view/comprovante/ItensComprovanteView;
    :goto_35
    goto :goto_6

    .line 44
    :cond_36
    return-void
.end method

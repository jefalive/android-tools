.class public Lbr/com/itau/sdk/android/core/exception/BackendException;
.super Ljava/lang/RuntimeException;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;
    }
.end annotation


# static fields
.field private static final $:[B

.field private static $$:I


# instance fields
.field private final backendResponse:Lbr/com/itau/sdk/android/core/endpoint/b;

.field private final body:Lbr/com/itau/sdk/android/core/model/ErrorDTO;
    .annotation runtime Lcom/google/gson/annotations/Expose;
    .end annotation
.end field

.field private final kind:Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;
    .annotation runtime Lcom/google/gson/annotations/Expose;
    .end annotation
.end field

.field private opKey:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/Expose;
    .end annotation
.end field

.field private final statusCode:I
    .annotation runtime Lcom/google/gson/annotations/Expose;
    .end annotation
.end field


# direct methods
.method private static $(III)Ljava/lang/String;
    .registers 9

    rsub-int/lit8 p1, p1, 0x3c

    new-instance v0, Ljava/lang/String;

    rsub-int/lit8 p0, p0, 0x43

    const/4 v5, 0x0

    add-int/lit8 p2, p2, 0x1

    sget-object v4, Lbr/com/itau/sdk/android/core/exception/BackendException;->$:[B

    new-array v1, p2, [B

    add-int/lit8 p2, p2, -0x1

    if-nez v4, :cond_18

    move v2, p1

    move v3, p2

    :goto_13
    neg-int v3, v3

    add-int p0, v2, v3

    add-int/lit8 p1, p1, 0x1

    :cond_18
    int-to-byte v2, p0

    aput-byte v2, v1, v5

    if-ne v5, p2, :cond_22

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Ljava/lang/String;-><init>([BI)V

    return-object v0

    :cond_22
    add-int/lit8 v5, v5, 0x1

    move v2, p0

    aget-byte v3, v4, p1

    goto :goto_13
.end method

.method static constructor <clinit>()V
    .registers 3

    const/16 v0, 0x64

    new-array v0, v0, [B

    fill-array-data v0, :array_e

    sput-object v0, Lbr/com/itau/sdk/android/core/exception/BackendException;->$:[B

    const/16 v0, 0xe1

    sput v0, Lbr/com/itau/sdk/android/core/exception/BackendException;->$$:I

    return-void

    :array_e
    .array-data 1
        0x55t
        -0x3t
        0x65t
        0x29t
        0xct
        -0x53t
        -0x1t
        0x13t
        -0x13t
        -0x1t
        0x2t
        0x30t
        -0x2ct
        0xbt
        -0x1t
        0x28t
        0xct
        -0x42t
        0x1t
        -0x2t
        -0x8t
        0x6t
        -0x9t
        0xat
        0x12t
        -0x13t
        -0xet
        0x3t
        0x1t
        0x1t
        -0x5t
        0xet
        0x28t
        -0x1ft
        -0x2t
        -0x8t
        0x6t
        -0x9t
        0xat
        0x1ft
        -0x33t
        0x15t
        -0x2t
        -0xbt
        -0x4t
        0xbt
        -0x6t
        0x1t
        -0xdt
        0x19t
        -0xdt
        0xbt
        -0x15t
        0x3ct
        0xct
        -0x4bt
        0x2t
        -0x5t
        0xat
        0x27t
        -0x2ct
        -0x6t
        0x9t
        0x8t
        -0xat
        0x47t
        -0x4dt
        0x54t
        -0x43t
        -0xct
        0x1t
        -0x8t
        0x11t
        -0xdt
        -0x2t
        0x54t
        -0x52t
        0xdt
        -0xet
        0x3t
        0x1t
        0x1t
        -0x5t
        0xet
        0x45t
        -0x54t
        0x5t
        0x4ft
        -0x4dt
        0xct
        -0xft
        0x0t
        0xbt
        0x1t
        0x44t
        -0x54t
        -0x5t
        0x9t
        0xbt
        0x37t
    .end array-data
.end method

.method constructor <init>(Ljava/lang/String;Lbr/com/itau/sdk/android/core/endpoint/b;Lokhttp3/ResponseBody;Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;Ljava/lang/Throwable;)V
    .registers 7

    .line 33
    invoke-direct {p0, p1, p5}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 34
    iput-object p4, p0, Lbr/com/itau/sdk/android/core/exception/BackendException;->kind:Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;

    .line 35
    iput-object p2, p0, Lbr/com/itau/sdk/android/core/exception/BackendException;->backendResponse:Lbr/com/itau/sdk/android/core/endpoint/b;

    .line 36
    invoke-direct {p0, p3}, Lbr/com/itau/sdk/android/core/exception/BackendException;->readBody(Lokhttp3/ResponseBody;)Lbr/com/itau/sdk/android/core/model/ErrorDTO;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/sdk/android/core/exception/BackendException;->body:Lbr/com/itau/sdk/android/core/model/ErrorDTO;

    .line 38
    if-eqz p2, :cond_16

    .line 39
    invoke-virtual {p2}, Lbr/com/itau/sdk/android/core/endpoint/b;->c()I

    move-result v0

    iput v0, p0, Lbr/com/itau/sdk/android/core/exception/BackendException;->statusCode:I

    goto :goto_27

    .line 40
    :cond_16
    instance-of v0, p5, Lbr/com/itau/sdk/android/core/exception/e;

    if-eqz v0, :cond_24

    .line 41
    move-object v0, p5

    check-cast v0, Lbr/com/itau/sdk/android/core/exception/e;

    invoke-virtual {v0}, Lbr/com/itau/sdk/android/core/exception/e;->a()I

    move-result v0

    iput v0, p0, Lbr/com/itau/sdk/android/core/exception/BackendException;->statusCode:I

    goto :goto_27

    .line 43
    :cond_24
    const/4 v0, -0x1

    iput v0, p0, Lbr/com/itau/sdk/android/core/exception/BackendException;->statusCode:I

    .line 44
    :goto_27
    return-void
.end method

.method public static conversionError(Lbr/com/itau/sdk/android/core/exception/b;)Lbr/com/itau/sdk/android/core/exception/BackendException;
    .registers 7

    .line 51
    new-instance v0, Lbr/com/itau/sdk/android/core/exception/BackendException;

    invoke-virtual {p0}, Lbr/com/itau/sdk/android/core/exception/b;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lbr/com/itau/sdk/android/core/exception/b;->a()Lbr/com/itau/sdk/android/core/endpoint/b;

    move-result-object v2

    sget-object v4, Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;->CONVERSION:Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;

    const/4 v3, 0x0

    move-object v5, p0

    invoke-direct/range {v0 .. v5}, Lbr/com/itau/sdk/android/core/exception/BackendException;-><init>(Ljava/lang/String;Lbr/com/itau/sdk/android/core/endpoint/b;Lokhttp3/ResponseBody;Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;Ljava/lang/Throwable;)V

    return-object v0
.end method

.method public static httpError(Lbr/com/itau/sdk/android/core/endpoint/b;Lokhttp3/ResponseBody;)Lbr/com/itau/sdk/android/core/exception/BackendException;
    .registers 9

    .line 55
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lbr/com/itau/sdk/android/core/endpoint/b;->c()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    sget v1, Lbr/com/itau/sdk/android/core/exception/BackendException;->$$:I

    ushr-int/lit8 v1, v1, 0x2

    sget-object v2, Lbr/com/itau/sdk/android/core/exception/BackendException;->$:[B

    const/16 v3, 0x5b

    aget-byte v2, v2, v3

    const/16 v3, 0x23

    invoke-static {v3, v1, v2}, Lbr/com/itau/sdk/android/core/exception/BackendException;->$(III)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lbr/com/itau/sdk/android/core/endpoint/b;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 56
    new-instance v0, Lbr/com/itau/sdk/android/core/exception/BackendException;

    sget-object v4, Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;->HTTP:Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;

    move-object v1, v6

    move-object v2, p0

    move-object v3, p1

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lbr/com/itau/sdk/android/core/exception/BackendException;-><init>(Ljava/lang/String;Lbr/com/itau/sdk/android/core/endpoint/b;Lokhttp3/ResponseBody;Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;Ljava/lang/Throwable;)V

    return-object v0
.end method

.method public static httpError(Lokhttp3/Response;Ljava/lang/String;)Lbr/com/itau/sdk/android/core/exception/BackendException;
    .registers 10

    .line 60
    new-instance v6, Lbr/com/itau/sdk/android/core/endpoint/b;

    const/4 v0, 0x0

    invoke-direct {v6, p0, p1, v0}, Lbr/com/itau/sdk/android/core/endpoint/b;-><init>(Lokhttp3/Response;Ljava/lang/Object;Lokhttp3/ResponseBody;)V

    .line 61
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lokhttp3/Response;->code()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    sget v1, Lbr/com/itau/sdk/android/core/exception/BackendException;->$$:I

    ushr-int/lit8 v1, v1, 0x2

    sget-object v2, Lbr/com/itau/sdk/android/core/exception/BackendException;->$:[B

    const/16 v3, 0x5b

    aget-byte v2, v2, v3

    const/16 v3, 0x23

    invoke-static {v3, v1, v2}, Lbr/com/itau/sdk/android/core/exception/BackendException;->$(III)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lokhttp3/Response;->message()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 62
    new-instance v0, Lbr/com/itau/sdk/android/core/exception/BackendException;

    sget-object v4, Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;->HTTP:Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;

    move-object v1, v7

    move-object v2, v6

    const/4 v3, 0x0

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lbr/com/itau/sdk/android/core/exception/BackendException;-><init>(Ljava/lang/String;Lbr/com/itau/sdk/android/core/endpoint/b;Lokhttp3/ResponseBody;Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;Ljava/lang/Throwable;)V

    return-object v0
.end method

.method public static networkError(Ljava/io/IOException;)Lbr/com/itau/sdk/android/core/exception/BackendException;
    .registers 7

    .line 47
    new-instance v0, Lbr/com/itau/sdk/android/core/exception/BackendException;

    invoke-virtual {p0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v1

    sget-object v4, Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;->NETWORK:Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;

    const/4 v2, 0x0

    const/4 v3, 0x0

    move-object v5, p0

    invoke-direct/range {v0 .. v5}, Lbr/com/itau/sdk/android/core/exception/BackendException;-><init>(Ljava/lang/String;Lbr/com/itau/sdk/android/core/endpoint/b;Lokhttp3/ResponseBody;Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;Ljava/lang/Throwable;)V

    return-object v0
.end method

.method private readBody(Lokhttp3/ResponseBody;)Lbr/com/itau/sdk/android/core/model/ErrorDTO;
    .registers 7

    .line 168
    if-nez p1, :cond_4

    .line 169
    const/4 v0, 0x0

    return-object v0

    .line 172
    :cond_4
    :try_start_4
    sget-object v0, Lbr/com/itau/sdk/android/core/n;->b:Lbr/com/itau/sdk/android/core/endpoint/i;

    const-class v1, Lbr/com/itau/sdk/android/core/model/ErrorDTO;

    invoke-virtual {v0, p1, v1}, Lbr/com/itau/sdk/android/core/endpoint/i;->a(Lokhttp3/ResponseBody;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbr/com/itau/sdk/android/core/model/ErrorDTO;
    :try_end_e
    .catch Lcom/google/gson/JsonSyntaxException; {:try_start_4 .. :try_end_e} :catch_f
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_e} :catch_28

    return-object v0

    .line 174
    :catch_f
    move-exception v4

    .line 175
    new-instance v0, Lbr/com/itau/sdk/android/core/exception/b;

    sget-object v1, Lbr/com/itau/sdk/android/core/exception/BackendException;->$:[B

    const/16 v2, 0x5b

    aget-byte v1, v1, v2

    sget-object v2, Lbr/com/itau/sdk/android/core/exception/BackendException;->$:[B

    const/16 v3, 0xf

    aget-byte v2, v2, v3

    invoke-static {v1, v1, v2}, Lbr/com/itau/sdk/android/core/exception/BackendException;->$(III)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lbr/com/itau/sdk/android/core/exception/BackendException;->backendResponse:Lbr/com/itau/sdk/android/core/endpoint/b;

    invoke-direct {v0, v1, v2, v4}, Lbr/com/itau/sdk/android/core/exception/b;-><init>(Ljava/lang/String;Lbr/com/itau/sdk/android/core/endpoint/b;Lcom/google/gson/JsonSyntaxException;)V

    throw v0

    .line 177
    :catch_28
    move-exception v4

    .line 178
    const/4 v0, 0x0

    return-object v0
.end method

.method public static routerException(Lbr/com/itau/sdk/android/core/exception/e;)Lbr/com/itau/sdk/android/core/exception/BackendException;
    .registers 7

    .line 66
    new-instance v0, Lbr/com/itau/sdk/android/core/exception/BackendException;

    invoke-virtual {p0}, Lbr/com/itau/sdk/android/core/exception/e;->getMessage()Ljava/lang/String;

    move-result-object v1

    sget-object v4, Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;->ROUTER:Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;

    const/4 v2, 0x0

    const/4 v3, 0x0

    move-object v5, p0

    invoke-direct/range {v0 .. v5}, Lbr/com/itau/sdk/android/core/exception/BackendException;-><init>(Ljava/lang/String;Lbr/com/itau/sdk/android/core/endpoint/b;Lokhttp3/ResponseBody;Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;Ljava/lang/Throwable;)V

    return-object v0
.end method

.method public static sessionException(Lbr/com/itau/sdk/android/core/exception/g;)Lbr/com/itau/sdk/android/core/exception/BackendException;
    .registers 7

    .line 78
    new-instance v0, Lbr/com/itau/sdk/android/core/exception/BackendException;

    invoke-virtual {p0}, Lbr/com/itau/sdk/android/core/exception/g;->getMessage()Ljava/lang/String;

    move-result-object v1

    sget-object v4, Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;->SESSION:Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;

    const/4 v2, 0x0

    const/4 v3, 0x0

    move-object v5, p0

    invoke-direct/range {v0 .. v5}, Lbr/com/itau/sdk/android/core/exception/BackendException;-><init>(Ljava/lang/String;Lbr/com/itau/sdk/android/core/endpoint/b;Lokhttp3/ResponseBody;Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;Ljava/lang/Throwable;)V

    return-object v0
.end method

.method public static simultaneousSessionException(Lbr/com/itau/sdk/android/core/exception/h;)Lbr/com/itau/sdk/android/core/exception/BackendException;
    .registers 7

    .line 82
    new-instance v0, Lbr/com/itau/sdk/android/core/exception/BackendException;

    invoke-virtual {p0}, Lbr/com/itau/sdk/android/core/exception/h;->getMessage()Ljava/lang/String;

    move-result-object v1

    sget-object v4, Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;->SIMULTANEOUS_SESSION:Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;

    const/4 v2, 0x0

    const/4 v3, 0x0

    move-object v5, p0

    invoke-direct/range {v0 .. v5}, Lbr/com/itau/sdk/android/core/exception/BackendException;-><init>(Ljava/lang/String;Lbr/com/itau/sdk/android/core/endpoint/b;Lokhttp3/ResponseBody;Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;Ljava/lang/Throwable;)V

    return-object v0
.end method

.method public static tokenException(Lbr/com/itau/sdk/android/core/exception/TokenException;)Lbr/com/itau/sdk/android/core/exception/BackendException;
    .registers 7

    .line 74
    new-instance v0, Lbr/com/itau/sdk/android/core/exception/BackendException;

    invoke-virtual {p0}, Lbr/com/itau/sdk/android/core/exception/TokenException;->getMessage()Ljava/lang/String;

    move-result-object v1

    sget-object v4, Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;->TOKEN:Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;

    const/4 v2, 0x0

    const/4 v3, 0x0

    move-object v5, p0

    invoke-direct/range {v0 .. v5}, Lbr/com/itau/sdk/android/core/exception/BackendException;-><init>(Ljava/lang/String;Lbr/com/itau/sdk/android/core/endpoint/b;Lokhttp3/ResponseBody;Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;Ljava/lang/Throwable;)V

    return-object v0
.end method

.method public static unexpectedError(Ljava/lang/Throwable;)Lbr/com/itau/sdk/android/core/exception/BackendException;
    .registers 7

    .line 70
    new-instance v0, Lbr/com/itau/sdk/android/core/exception/BackendException;

    invoke-virtual {p0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v1

    sget-object v4, Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;->UNEXPECTED:Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;

    const/4 v2, 0x0

    const/4 v3, 0x0

    move-object v5, p0

    invoke-direct/range {v0 .. v5}, Lbr/com/itau/sdk/android/core/exception/BackendException;-><init>(Ljava/lang/String;Lbr/com/itau/sdk/android/core/endpoint/b;Lokhttp3/ResponseBody;Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;Ljava/lang/Throwable;)V

    return-object v0
.end method

.method public static versionControlException(Lbr/com/itau/sdk/android/core/exception/i;)Lbr/com/itau/sdk/android/core/exception/BackendException;
    .registers 7

    .line 86
    new-instance v0, Lbr/com/itau/sdk/android/core/exception/BackendException;

    invoke-virtual {p0}, Lbr/com/itau/sdk/android/core/exception/i;->getMessage()Ljava/lang/String;

    move-result-object v1

    sget-object v4, Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;->VERSION_CONTROL:Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;

    const/4 v2, 0x0

    const/4 v3, 0x0

    move-object v5, p0

    invoke-direct/range {v0 .. v5}, Lbr/com/itau/sdk/android/core/exception/BackendException;-><init>(Ljava/lang/String;Lbr/com/itau/sdk/android/core/endpoint/b;Lokhttp3/ResponseBody;Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;Ljava/lang/Throwable;)V

    return-object v0
.end method


# virtual methods
.method public getBackendResponse()Lbr/com/itau/sdk/android/core/endpoint/b;
    .registers 2

    .line 159
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/exception/BackendException;->backendResponse:Lbr/com/itau/sdk/android/core/endpoint/b;

    return-object v0
.end method

.method public getError()Lbr/com/itau/sdk/android/core/model/ErrorDTO;
    .registers 2

    .line 163
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/exception/BackendException;->body:Lbr/com/itau/sdk/android/core/model/ErrorDTO;

    return-object v0
.end method

.method public getKind()Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;
    .registers 2

    .line 151
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/exception/BackendException;->kind:Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;

    return-object v0
.end method

.method public getOpKey()Ljava/lang/String;
    .registers 2

    .line 140
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/exception/BackendException;->opKey:Ljava/lang/String;

    return-object v0
.end method

.method public getStatusCode()I
    .registers 2

    .line 155
    iget v0, p0, Lbr/com/itau/sdk/android/core/exception/BackendException;->statusCode:I

    return v0
.end method

.method public setOpKey(Ljava/lang/String;)V
    .registers 2

    .line 144
    iput-object p1, p0, Lbr/com/itau/sdk/android/core/exception/BackendException;->opKey:Ljava/lang/String;

    .line 145
    return-void
.end method

.method public toString()Ljava/lang/String;
    .registers 6

    .line 184
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lbr/com/itau/sdk/android/core/exception/BackendException;->$:[B

    const/16 v2, 0x12

    aget-byte v1, v1, v2

    or-int/lit8 v2, v1, 0x1a

    sget-object v3, Lbr/com/itau/sdk/android/core/exception/BackendException;->$:[B

    const/16 v4, 0x29

    aget-byte v3, v3, v4

    invoke-static {v1, v2, v3}, Lbr/com/itau/sdk/android/core/exception/BackendException;->$(III)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lbr/com/itau/sdk/android/core/exception/BackendException;->body:Lbr/com/itau/sdk/android/core/model/ErrorDTO;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lbr/com/itau/sdk/android/core/exception/BackendException;->$:[B

    const/16 v2, 0x15

    aget-byte v1, v1, v2

    const/16 v2, 0x17

    invoke-static {v2, v1, v1}, Lbr/com/itau/sdk/android/core/exception/BackendException;->$(III)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lbr/com/itau/sdk/android/core/exception/BackendException;->kind:Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget v1, Lbr/com/itau/sdk/android/core/exception/BackendException;->$$:I

    ushr-int/lit8 v1, v1, 0x2

    sget-object v2, Lbr/com/itau/sdk/android/core/exception/BackendException;->$:[B

    const/4 v3, 0x4

    aget-byte v2, v2, v3

    const/16 v3, 0x17

    invoke-static {v3, v1, v2}, Lbr/com/itau/sdk/android/core/exception/BackendException;->$(III)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lbr/com/itau/sdk/android/core/exception/BackendException;->statusCode:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lbr/com/itau/sdk/android/core/exception/BackendException;->$:[B

    const/16 v2, 0xc

    aget-byte v1, v1, v2

    neg-int v1, v1

    sget-object v2, Lbr/com/itau/sdk/android/core/exception/BackendException;->$:[B

    const/16 v3, 0x48

    aget-byte v2, v2, v3

    const/16 v3, 0x17

    invoke-static {v3, v1, v2}, Lbr/com/itau/sdk/android/core/exception/BackendException;->$(III)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lbr/com/itau/sdk/android/core/exception/BackendException;->backendResponse:Lbr/com/itau/sdk/android/core/endpoint/b;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.class final Lio/fabric/sdk/android/services/common/AdvertisingInfoServiceStrategy$AdvertisingInterface;
.super Ljava/lang/Object;
.source "AdvertisingInfoServiceStrategy.java"

# interfaces
.implements Landroid/os/IInterface;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/fabric/sdk/android/services/common/AdvertisingInfoServiceStrategy;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "AdvertisingInterface"
.end annotation


# instance fields
.field private final binder:Landroid/os/IBinder;


# direct methods
.method public constructor <init>(Landroid/os/IBinder;)V
    .registers 2
    .param p1, "binder"    # Landroid/os/IBinder;

    .line 161
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 162
    iput-object p1, p0, Lio/fabric/sdk/android/services/common/AdvertisingInfoServiceStrategy$AdvertisingInterface;->binder:Landroid/os/IBinder;

    .line 163
    return-void
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 2

    .line 167
    iget-object v0, p0, Lio/fabric/sdk/android/services/common/AdvertisingInfoServiceStrategy$AdvertisingInterface;->binder:Landroid/os/IBinder;

    return-object v0
.end method

.method public getId()Ljava/lang/String;
    .registers 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 171
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v3

    .line 172
    .local v3, "data":Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v4

    .line 173
    .local v4, "reply":Landroid/os/Parcel;
    const/4 v5, 0x0

    .line 175
    .local v5, "id":Ljava/lang/String;
    const-string v0, "com.google.android.gms.ads.identifier.internal.IAdvertisingIdService"

    :try_start_b
    invoke-virtual {v3, v0}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 176
    iget-object v0, p0, Lio/fabric/sdk/android/services/common/AdvertisingInfoServiceStrategy$AdvertisingInterface;->binder:Landroid/os/IBinder;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-interface {v0, v1, v3, v4, v2}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 177
    invoke-virtual {v4}, Landroid/os/Parcel;->readException()V

    .line 178
    invoke-virtual {v4}, Landroid/os/Parcel;->readString()Ljava/lang/String;
    :try_end_1b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_1b} :catch_24
    .catchall {:try_start_b .. :try_end_1b} :catchall_37

    move-result-object v0

    move-object v5, v0

    .line 183
    invoke-virtual {v4}, Landroid/os/Parcel;->recycle()V

    .line 184
    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    .line 185
    goto :goto_3f

    .line 179
    :catch_24
    move-exception v6

    .line 180
    .local v6, "e":Ljava/lang/Exception;
    :try_start_25
    invoke-static {}, Lio/fabric/sdk/android/Fabric;->getLogger()Lio/fabric/sdk/android/Logger;

    move-result-object v0

    const-string v1, "Fabric"

    const-string v2, "Could not get parcel from Google Play Service to capture AdvertisingId"

    invoke-interface {v0, v1, v2}, Lio/fabric/sdk/android/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_30
    .catchall {:try_start_25 .. :try_end_30} :catchall_37

    .line 183
    .end local v6    # "e":Ljava/lang/Exception;
    invoke-virtual {v4}, Landroid/os/Parcel;->recycle()V

    .line 184
    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    .line 185
    goto :goto_3f

    .line 183
    :catchall_37
    move-exception v7

    invoke-virtual {v4}, Landroid/os/Parcel;->recycle()V

    .line 184
    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    throw v7

    .line 186
    :goto_3f
    return-object v5
.end method

.method public isLimitAdTrackingEnabled()Z
    .registers 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 190
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v3

    .line 191
    .local v3, "data":Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v4

    .line 192
    .local v4, "reply":Landroid/os/Parcel;
    const/4 v5, 0x0

    .line 194
    .local v5, "limitAdTracking":Z
    const-string v0, "com.google.android.gms.ads.identifier.internal.IAdvertisingIdService"

    :try_start_b
    invoke-virtual {v3, v0}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 195
    const/4 v0, 0x1

    invoke-virtual {v3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 196
    iget-object v0, p0, Lio/fabric/sdk/android/services/common/AdvertisingInfoServiceStrategy$AdvertisingInterface;->binder:Landroid/os/IBinder;

    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-interface {v0, v1, v3, v4, v2}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 197
    invoke-virtual {v4}, Landroid/os/Parcel;->readException()V

    .line 198
    invoke-virtual {v4}, Landroid/os/Parcel;->readInt()I
    :try_end_1f
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_1f} :catch_2e
    .catchall {:try_start_b .. :try_end_1f} :catchall_41

    move-result v0

    const/4 v1, 0x0

    if-eq v1, v0, :cond_25

    const/4 v0, 0x1

    goto :goto_26

    :cond_25
    const/4 v0, 0x0

    :goto_26
    move v5, v0

    .line 204
    invoke-virtual {v4}, Landroid/os/Parcel;->recycle()V

    .line 205
    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    .line 206
    goto :goto_49

    .line 199
    :catch_2e
    move-exception v6

    .line 200
    .local v6, "e":Ljava/lang/Exception;
    :try_start_2f
    invoke-static {}, Lio/fabric/sdk/android/Fabric;->getLogger()Lio/fabric/sdk/android/Logger;

    move-result-object v0

    const-string v1, "Fabric"

    const-string v2, "Could not get parcel from Google Play Service to capture Advertising limitAdTracking"

    invoke-interface {v0, v1, v2}, Lio/fabric/sdk/android/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3a
    .catchall {:try_start_2f .. :try_end_3a} :catchall_41

    .line 204
    .end local v6    # "e":Ljava/lang/Exception;
    invoke-virtual {v4}, Landroid/os/Parcel;->recycle()V

    .line 205
    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    .line 206
    goto :goto_49

    .line 204
    :catchall_41
    move-exception v7

    invoke-virtual {v4}, Landroid/os/Parcel;->recycle()V

    .line 205
    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    throw v7

    .line 207
    :goto_49
    return v5
.end method

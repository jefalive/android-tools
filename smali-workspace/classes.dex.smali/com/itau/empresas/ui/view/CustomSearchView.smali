.class public Lcom/itau/empresas/ui/view/CustomSearchView;
.super Landroid/support/v7/widget/SearchView;
.source "CustomSearchView.java"


# instance fields
.field private searchViewIcon:Landroid/widget/ImageView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 2
    .param p1, "context"    # Landroid/content/Context;

    .line 17
    invoke-direct {p0, p1}, Landroid/support/v7/widget/SearchView;-><init>(Landroid/content/Context;)V

    .line 18
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .line 21
    invoke-direct {p0, p1, p2}, Landroid/support/v7/widget/SearchView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 23
    .line 24
    const v0, 0x7f0e00ac

    invoke-virtual {p0, v0}, Lcom/itau/empresas/ui/view/CustomSearchView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/itau/empresas/ui/view/CustomSearchView;->searchViewIcon:Landroid/widget/ImageView;

    .line 26
    .line 27
    const v0, 0x7f0e00af

    invoke-virtual {p0, v0}, Lcom/itau/empresas/ui/view/CustomSearchView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Landroid/widget/ImageView;

    .line 29
    .local v1, "searchCloseIcon":Landroid/widget/ImageView;
    invoke-direct {p0}, Lcom/itau/empresas/ui/view/CustomSearchView;->removeSearchIconFromLeftContainer()V

    .line 31
    invoke-direct {p0, v1}, Lcom/itau/empresas/ui/view/CustomSearchView;->addSearchIconAtRightContainer(Landroid/widget/ImageView;)V

    .line 32
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .line 35
    invoke-direct {p0, p1, p2, p3}, Landroid/support/v7/widget/SearchView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 36
    return-void
.end method

.method private addSearchIconAtRightContainer(Landroid/widget/ImageView;)V
    .registers 4
    .param p1, "searchCloseIcon"    # Landroid/widget/ImageView;

    .line 57
    invoke-virtual {p1}, Landroid/widget/ImageView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Landroid/view/ViewGroup;

    .line 58
    .local v1, "closeIconParent":Landroid/view/ViewGroup;
    iget-object v0, p0, Lcom/itau/empresas/ui/view/CustomSearchView;->searchViewIcon:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 59
    return-void
.end method

.method private removeSearchIconFromLeftContainer()V
    .registers 3

    .line 46
    iget-object v0, p0, Lcom/itau/empresas/ui/view/CustomSearchView;->searchViewIcon:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Landroid/view/ViewGroup;

    .line 47
    .local v1, "searchIconParent":Landroid/view/ViewGroup;
    iget-object v0, p0, Lcom/itau/empresas/ui/view/CustomSearchView;->searchViewIcon:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 48
    return-void
.end method

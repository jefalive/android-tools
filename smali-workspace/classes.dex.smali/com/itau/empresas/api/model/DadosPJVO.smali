.class public Lcom/itau/empresas/api/model/DadosPJVO;
.super Ljava/lang/Object;
.source "DadosPJVO.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private autorizacao:Z
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "autorizacao"
    .end annotation
.end field

.field private codigoOperador:Ljava/lang/Integer;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "codigo_operador"
    .end annotation
.end field

.field private informacoesPagador:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "informacoes_pagador"
    .end annotation
.end field

.field private produto:Ljava/lang/String;

.field private referenciaEmpresa:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "referencia_empresa"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 2

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    const-string v0, "SISPAG"

    iput-object v0, p0, Lcom/itau/empresas/api/model/DadosPJVO;->produto:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public setAutorizacao(Z)V
    .registers 2
    .param p1, "autorizacao"    # Z

    .line 47
    iput-boolean p1, p0, Lcom/itau/empresas/api/model/DadosPJVO;->autorizacao:Z

    .line 48
    return-void
.end method

.method public setCodigoOperador(Ljava/lang/String;)V
    .registers 4
    .param p1, "codigoOperador"    # Ljava/lang/String;

    .line 40
    :try_start_0
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/api/model/DadosPJVO;->codigoOperador:Ljava/lang/Integer;
    :try_end_a
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_a} :catch_b

    .line 43
    goto :goto_f

    .line 41
    :catch_b
    move-exception v1

    .line 42
    .local v1, "ex":Ljava/lang/NumberFormatException;
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/itau/empresas/api/model/DadosPJVO;->codigoOperador:Ljava/lang/Integer;

    .line 44
    .end local v1    # "ex":Ljava/lang/NumberFormatException;
    :goto_f
    return-void
.end method

.method public setProduto(Ljava/lang/String;)V
    .registers 2
    .param p1, "produto"    # Ljava/lang/String;

    .line 26
    iput-object p1, p0, Lcom/itau/empresas/api/model/DadosPJVO;->produto:Ljava/lang/String;

    .line 27
    return-void
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    .line 52
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "DadosPJVO{produto=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/api/model/DadosPJVO;->produto:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", codigoOperador=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/api/model/DadosPJVO;->codigoOperador:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", referenciaEmpresa=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/api/model/DadosPJVO;->referenciaEmpresa:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", informacoesPagador=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/api/model/DadosPJVO;->informacoesPagador:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", autorizacao="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/itau/empresas/api/model/DadosPJVO;->autorizacao:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

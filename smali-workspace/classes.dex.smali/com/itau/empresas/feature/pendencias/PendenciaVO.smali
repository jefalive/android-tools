.class public Lcom/itau/empresas/feature/pendencias/PendenciaVO;
.super Ljava/lang/Object;
.source "PendenciaVO.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private canalParaResolverPendencia:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "canal_para_resolver_pendencia"
    .end annotation
.end field

.field private chaveMenuMobile:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "chave_menu_mobile"
    .end annotation
.end field

.field private dataValidade:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "data_validade_tarefa"
    .end annotation
.end field

.field private descricaoTarefa:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "descricao_tarefa"
    .end annotation
.end field

.field private idTarefa:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "id_tarefa"
    .end annotation
.end field

.field private quantidadePendencias:I
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "quantidade_pendencias"
    .end annotation
.end field

.field private tipoTarefa:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "tipo_tarefa"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getCanalParaResolverPendencia()Ljava/lang/String;
    .registers 2

    .line 39
    iget-object v0, p0, Lcom/itau/empresas/feature/pendencias/PendenciaVO;->canalParaResolverPendencia:Ljava/lang/String;

    return-object v0
.end method

.method public getChaveMenuMobile()Ljava/lang/String;
    .registers 2

    .line 43
    iget-object v0, p0, Lcom/itau/empresas/feature/pendencias/PendenciaVO;->chaveMenuMobile:Ljava/lang/String;

    return-object v0
.end method

.method public getDescricaoTarefa()Ljava/lang/String;
    .registers 2

    .line 35
    iget-object v0, p0, Lcom/itau/empresas/feature/pendencias/PendenciaVO;->descricaoTarefa:Ljava/lang/String;

    return-object v0
.end method

.method public getQuantidadePendencias()I
    .registers 2

    .line 27
    iget v0, p0, Lcom/itau/empresas/feature/pendencias/PendenciaVO;->quantidadePendencias:I

    return v0
.end method

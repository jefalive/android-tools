.class final Lde/greenrobot/event/BackgroundPoster;
.super Ljava/lang/Object;
.source "BackgroundPoster.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private final eventBus:Lde/greenrobot/event/EventBus;

.field private volatile executorRunning:Z

.field private final queue:Lde/greenrobot/event/PendingPostQueue;


# direct methods
.method constructor <init>(Lde/greenrobot/event/EventBus;)V
    .registers 3
    .param p1, "eventBus"    # Lde/greenrobot/event/EventBus;

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object p1, p0, Lde/greenrobot/event/BackgroundPoster;->eventBus:Lde/greenrobot/event/EventBus;

    .line 34
    new-instance v0, Lde/greenrobot/event/PendingPostQueue;

    invoke-direct {v0}, Lde/greenrobot/event/PendingPostQueue;-><init>()V

    iput-object v0, p0, Lde/greenrobot/event/BackgroundPoster;->queue:Lde/greenrobot/event/PendingPostQueue;

    .line 35
    return-void
.end method


# virtual methods
.method public enqueue(Lde/greenrobot/event/Subscription;Ljava/lang/Object;)V
    .registers 7
    .param p1, "subscription"    # Lde/greenrobot/event/Subscription;
    .param p2, "event"    # Ljava/lang/Object;

    .line 38
    invoke-static {p1, p2}, Lde/greenrobot/event/PendingPost;->obtainPendingPost(Lde/greenrobot/event/Subscription;Ljava/lang/Object;)Lde/greenrobot/event/PendingPost;

    move-result-object v1

    .line 39
    .local v1, "pendingPost":Lde/greenrobot/event/PendingPost;
    move-object v2, p0

    monitor-enter v2

    .line 40
    :try_start_6
    iget-object v0, p0, Lde/greenrobot/event/BackgroundPoster;->queue:Lde/greenrobot/event/PendingPostQueue;

    invoke-virtual {v0, v1}, Lde/greenrobot/event/PendingPostQueue;->enqueue(Lde/greenrobot/event/PendingPost;)V

    .line 41
    iget-boolean v0, p0, Lde/greenrobot/event/BackgroundPoster;->executorRunning:Z

    if-nez v0, :cond_1b

    .line 42
    const/4 v0, 0x1

    iput-boolean v0, p0, Lde/greenrobot/event/BackgroundPoster;->executorRunning:Z

    .line 43
    iget-object v0, p0, Lde/greenrobot/event/BackgroundPoster;->eventBus:Lde/greenrobot/event/EventBus;

    invoke-virtual {v0}, Lde/greenrobot/event/EventBus;->getExecutorService()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    invoke-interface {v0, p0}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V
    :try_end_1b
    .catchall {:try_start_6 .. :try_end_1b} :catchall_1d

    .line 45
    :cond_1b
    monitor-exit v2

    goto :goto_20

    :catchall_1d
    move-exception v3

    monitor-exit v2

    throw v3

    .line 46
    :goto_20
    return-void
.end method

.method public run()V
    .registers 8

    .line 53
    :goto_0
    :try_start_0
    iget-object v0, p0, Lde/greenrobot/event/BackgroundPoster;->queue:Lde/greenrobot/event/PendingPostQueue;

    const/16 v1, 0x3e8

    invoke-virtual {v0, v1}, Lde/greenrobot/event/PendingPostQueue;->poll(I)Lde/greenrobot/event/PendingPost;

    move-result-object v3

    .line 54
    .local v3, "pendingPost":Lde/greenrobot/event/PendingPost;
    if-nez v3, :cond_21

    .line 55
    move-object v4, p0

    monitor-enter v4
    :try_end_c
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_c} :catch_27
    .catchall {:try_start_0 .. :try_end_c} :catchall_4c

    .line 57
    :try_start_c
    iget-object v0, p0, Lde/greenrobot/event/BackgroundPoster;->queue:Lde/greenrobot/event/PendingPostQueue;

    invoke-virtual {v0}, Lde/greenrobot/event/PendingPostQueue;->poll()Lde/greenrobot/event/PendingPost;

    move-result-object v3

    .line 58
    if-nez v3, :cond_1c

    .line 59
    const/4 v0, 0x0

    iput-boolean v0, p0, Lde/greenrobot/event/BackgroundPoster;->executorRunning:Z
    :try_end_17
    .catchall {:try_start_c .. :try_end_17} :catchall_1e

    .line 60
    monitor-exit v4

    .line 70
    const/4 v0, 0x0

    iput-boolean v0, p0, Lde/greenrobot/event/BackgroundPoster;->executorRunning:Z

    .line 60
    return-void

    .line 62
    :cond_1c
    monitor-exit v4

    goto :goto_21

    :catchall_1e
    move-exception v5

    monitor-exit v4

    :try_start_20
    throw v5

    .line 64
    :cond_21
    :goto_21
    iget-object v0, p0, Lde/greenrobot/event/BackgroundPoster;->eventBus:Lde/greenrobot/event/EventBus;

    invoke-virtual {v0, v3}, Lde/greenrobot/event/EventBus;->invokeSubscriber(Lde/greenrobot/event/PendingPost;)V
    :try_end_26
    .catch Ljava/lang/InterruptedException; {:try_start_20 .. :try_end_26} :catch_27
    .catchall {:try_start_20 .. :try_end_26} :catchall_4c

    .line 65
    .end local v3    # "pendingPost":Lde/greenrobot/event/PendingPost;
    goto :goto_0

    .line 66
    :catch_27
    move-exception v3

    .line 67
    .local v3, "e":Ljava/lang/InterruptedException;
    const-string v0, "Event"

    :try_start_2a
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " was interruppted"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_48
    .catchall {:try_start_2a .. :try_end_48} :catchall_4c

    .line 70
    .end local v3    # "e":Ljava/lang/InterruptedException;
    const/4 v0, 0x0

    iput-boolean v0, p0, Lde/greenrobot/event/BackgroundPoster;->executorRunning:Z

    .line 71
    goto :goto_51

    .line 70
    :catchall_4c
    move-exception v6

    const/4 v0, 0x0

    iput-boolean v0, p0, Lde/greenrobot/event/BackgroundPoster;->executorRunning:Z

    throw v6

    .line 72
    :goto_51
    return-void
.end method

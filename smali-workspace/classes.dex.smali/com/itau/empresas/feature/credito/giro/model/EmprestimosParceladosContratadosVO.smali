.class public Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosContratadosVO;
.super Ljava/lang/Object;
.source "EmprestimosParceladosContratadosVO.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private camposComprovante:Ljava/util/List;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "campos_comprovante"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Lcom/itau/empresas/feature/credito/giro/model/CamposComprovanteVO;>;"
        }
    .end annotation
.end field

.field private idComprovante:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "id_comprovante"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getCamposComprovate()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Lcom/itau/empresas/feature/credito/giro/model/CamposComprovanteVO;>;"
        }
    .end annotation

    .line 69
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosContratadosVO;->camposComprovante:Ljava/util/List;

    return-object v0
.end method

.method public getIdComprovante()Ljava/lang/String;
    .registers 2

    .line 73
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosContratadosVO;->idComprovante:Ljava/lang/String;

    return-object v0
.end method

.class public final Lcom/sothree/slidinguppanel/library/R$styleable;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sothree/slidinguppanel/library/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "styleable"
.end annotation


# static fields
.field public static final SlidingUpPanelLayout:[I

.field public static final SlidingUpPanelLayout_umanoAnchorPoint:I = 0x9

.field public static final SlidingUpPanelLayout_umanoClipPanel:I = 0x8

.field public static final SlidingUpPanelLayout_umanoDragView:I = 0x5

.field public static final SlidingUpPanelLayout_umanoFadeColor:I = 0x3

.field public static final SlidingUpPanelLayout_umanoFlingVelocity:I = 0x4

.field public static final SlidingUpPanelLayout_umanoInitialState:I = 0xa

.field public static final SlidingUpPanelLayout_umanoOverlay:I = 0x7

.field public static final SlidingUpPanelLayout_umanoPanelHeight:I = 0x0

.field public static final SlidingUpPanelLayout_umanoParalaxOffset:I = 0x2

.field public static final SlidingUpPanelLayout_umanoScrollableView:I = 0x6

.field public static final SlidingUpPanelLayout_umanoShadowHeight:I = 0x1


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .line 34
    const/16 v0, 0xb

    new-array v0, v0, [I

    fill-array-data v0, :array_a

    sput-object v0, Lcom/sothree/slidinguppanel/library/R$styleable;->SlidingUpPanelLayout:[I

    return-void

    :array_a
    .array-data 4
        0x7f0101ed
        0x7f0101ee
        0x7f0101ef
        0x7f0101f0
        0x7f0101f1
        0x7f0101f2
        0x7f0101f3
        0x7f0101f4
        0x7f0101f5
        0x7f0101f6
        0x7f0101f7
    .end array-data
.end method

.method public constructor <init>()V
    .registers 1

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.class public Lcom/itau/empresas/ui/activity/ContatosActivity;
.super Lcom/itau/empresas/ui/activity/BaseActivity;
.source "ContatosActivity.java"


# instance fields
.field contatosExpandableListAdapter:Landroid/widget/ExpandableListAdapter;

.field listaContatos:Landroid/widget/ExpandableListView;

.field private listaDescricaoContatos:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Ljava/lang/String;>;"
        }
    .end annotation
.end field

.field private listaItensContatos:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<Ljava/lang/String;Ljava/util/List<Lcom/itau/empresas/ui/widgets/ContatosChildItem;>;>;"
        }
    .end annotation
.end field

.field private listaTituloContatos:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Ljava/lang/String;>;"
        }
    .end annotation
.end field

.field private tel:Ljava/lang/String;

.field telefonesUteis:Lcom/itau/empresas/ui/widgets/ContatosChildItem;

.field toolBar:Landroid/support/v7/widget/Toolbar;


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 41
    invoke-direct {p0}, Lcom/itau/empresas/ui/activity/BaseActivity;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/itau/empresas/ui/activity/ContatosActivity;)Ljava/util/List;
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/ui/activity/ContatosActivity;

    .line 41
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/ContatosActivity;->listaTituloContatos:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$100(Lcom/itau/empresas/ui/activity/ContatosActivity;Ljava/lang/String;Z)V
    .registers 3
    .param p0, "x0"    # Lcom/itau/empresas/ui/activity/ContatosActivity;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Z

    .line 41
    invoke-direct {p0, p1, p2}, Lcom/itau/empresas/ui/activity/ContatosActivity;->enviarEventoGroup(Ljava/lang/String;Z)V

    return-void
.end method

.method static synthetic access$200(Lcom/itau/empresas/ui/activity/ContatosActivity;)Ljava/util/HashMap;
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/ui/activity/ContatosActivity;

    .line 41
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/ContatosActivity;->listaItensContatos:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic access$302(Lcom/itau/empresas/ui/activity/ContatosActivity;Ljava/lang/String;)Ljava/lang/String;
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/ui/activity/ContatosActivity;
    .param p1, "x1"    # Ljava/lang/String;

    .line 41
    iput-object p1, p0, Lcom/itau/empresas/ui/activity/ContatosActivity;->tel:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$400(Lcom/itau/empresas/ui/activity/ContatosActivity;)V
    .registers 1
    .param p0, "x0"    # Lcom/itau/empresas/ui/activity/ContatosActivity;

    .line 41
    invoke-direct {p0}, Lcom/itau/empresas/ui/activity/ContatosActivity;->ligar()V

    return-void
.end method

.method private constroiToolbar()V
    .registers 3

    .line 277
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/ContatosActivity;->toolBar:Landroid/support/v7/widget/Toolbar;

    invoke-static {v0, p0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom;->Builder(Landroid/support/v7/widget/Toolbar;Landroid/support/v7/app/AppCompatActivity;)Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    move-result-object v0

    .line 279
    const v1, 0x7f070698

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->comTitulo(I)Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    move-result-object v0

    .line 280
    invoke-virtual {v0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->comClickListenerFinish()Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    move-result-object v0

    .line 281
    invoke-virtual {v0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->build()Lcom/itau/empresas/ui/util/listener/ToolbarCustom;

    .line 282
    return-void
.end method

.method private enviarEventoGroup(Ljava/lang/String;Z)V
    .registers 4
    .param p1, "nomeGrupoContato"    # Ljava/lang/String;
    .param p2, "expandido"    # Z

    .line 156
    if-eqz p2, :cond_a

    .line 157
    const v0, 0x7f070290

    invoke-virtual {p0, v0}, Lcom/itau/empresas/ui/activity/ContatosActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_11

    :cond_a
    const v0, 0x7f0702af

    invoke-virtual {p0, v0}, Lcom/itau/empresas/ui/activity/ContatosActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 156
    :goto_11
    invoke-virtual {p0, v0, p1}, Lcom/itau/empresas/ui/activity/ContatosActivity;->analyticsHit(Ljava/lang/String;Ljava/lang/String;)V

    .line 159
    return-void
.end method

.method public static intent(Landroid/support/v4/app/Fragment;)Lcom/itau/empresas/ui/activity/ContatosActivity_$IntentBuilder_;
    .registers 2
    .param p0, "fragment"    # Landroid/support/v4/app/Fragment;

    .line 320
    new-instance v0, Lcom/itau/empresas/ui/activity/ContatosActivity_$IntentBuilder_;

    invoke-direct {v0, p0}, Lcom/itau/empresas/ui/activity/ContatosActivity_$IntentBuilder_;-><init>(Landroid/support/v4/app/Fragment;)V

    return-object v0
.end method

.method private ligar()V
    .registers 5

    .line 127
    invoke-static {}, Lcom/itau/empresas/ui/util/PermissoesUtil;->getPermissionCallphone()[Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/itau/empresas/ui/util/PermissoesUtil;->hasSelfPermissions(Landroid/content/Context;[Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_44

    .line 128
    const-string v0, "android.permission.CALL_PHONE"

    invoke-static {p0, v0}, Landroid/support/v4/app/ActivityCompat;->checkSelfPermission(Landroid/content/Context;Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_20

    .line 130
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x17

    if-lt v0, v1, :cond_20

    .line 131
    invoke-static {}, Lcom/itau/empresas/ui/util/PermissoesUtil;->getPermissionCallphone()[Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/itau/empresas/ui/activity/ContatosActivity;->requestPermissions([Ljava/lang/String;I)V

    .line 134
    :cond_20
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.CALL"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "tel:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/itau/empresas/ui/activity/ContatosActivity;->tel:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {p0, v0}, Lcom/itau/empresas/ui/activity/ContatosActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_52

    .line 136
    :cond_44
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x17

    if-lt v0, v1, :cond_52

    .line 137
    invoke-static {}, Lcom/itau/empresas/ui/util/PermissoesUtil;->getPermissionCallphone()[Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/itau/empresas/ui/activity/ContatosActivity;->requestPermissions([Ljava/lang/String;I)V

    .line 141
    :cond_52
    :goto_52
    return-void
.end method

.method private preparaListContatos()V
    .registers 11

    .line 162
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/ui/activity/ContatosActivity;->listaTituloContatos:Ljava/util/List;

    .line 163
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/ui/activity/ContatosActivity;->listaDescricaoContatos:Ljava/util/List;

    .line 164
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/ui/activity/ContatosActivity;->listaItensContatos:Ljava/util/HashMap;

    .line 166
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/ContatosActivity;->listaTituloContatos:Ljava/util/List;

    const v1, 0x7f0706ae

    invoke-virtual {p0, v1}, Lcom/itau/empresas/ui/activity/ContatosActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 167
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/ContatosActivity;->listaDescricaoContatos:Ljava/util/List;

    const v1, 0x7f0701d6

    invoke-virtual {p0, v1}, Lcom/itau/empresas/ui/activity/ContatosActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 169
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/ContatosActivity;->listaTituloContatos:Ljava/util/List;

    const v1, 0x7f0706af

    invoke-virtual {p0, v1}, Lcom/itau/empresas/ui/activity/ContatosActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 170
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/ContatosActivity;->listaDescricaoContatos:Ljava/util/List;

    const v1, 0x7f0701d7

    invoke-virtual {p0, v1}, Lcom/itau/empresas/ui/activity/ContatosActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 172
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/ContatosActivity;->listaTituloContatos:Ljava/util/List;

    const v1, 0x7f0706ac

    invoke-virtual {p0, v1}, Lcom/itau/empresas/ui/activity/ContatosActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 173
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/ContatosActivity;->listaDescricaoContatos:Ljava/util/List;

    const v1, 0x7f0701d4

    invoke-virtual {p0, v1}, Lcom/itau/empresas/ui/activity/ContatosActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 175
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/ContatosActivity;->listaTituloContatos:Ljava/util/List;

    const v1, 0x7f0706ab

    invoke-virtual {p0, v1}, Lcom/itau/empresas/ui/activity/ContatosActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 176
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/ContatosActivity;->listaDescricaoContatos:Ljava/util/List;

    const v1, 0x7f0701d3

    invoke-virtual {p0, v1}, Lcom/itau/empresas/ui/activity/ContatosActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 178
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/ContatosActivity;->listaTituloContatos:Ljava/util/List;

    const v1, 0x7f0706ad

    invoke-virtual {p0, v1}, Lcom/itau/empresas/ui/activity/ContatosActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 179
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/ContatosActivity;->listaDescricaoContatos:Ljava/util/List;

    const v1, 0x7f0701d5

    invoke-virtual {p0, v1}, Lcom/itau/empresas/ui/activity/ContatosActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 181
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/ContatosActivity;->listaTituloContatos:Ljava/util/List;

    const v1, 0x7f0706bc

    invoke-virtual {p0, v1}, Lcom/itau/empresas/ui/activity/ContatosActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 182
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/ContatosActivity;->listaDescricaoContatos:Ljava/util/List;

    const v1, 0x7f0701da

    invoke-virtual {p0, v1}, Lcom/itau/empresas/ui/activity/ContatosActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 184
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/ContatosActivity;->listaTituloContatos:Ljava/util/List;

    const v1, 0x7f0706b5

    invoke-virtual {p0, v1}, Lcom/itau/empresas/ui/activity/ContatosActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 185
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/ContatosActivity;->listaDescricaoContatos:Ljava/util/List;

    const v1, 0x7f0701d9

    invoke-virtual {p0, v1}, Lcom/itau/empresas/ui/activity/ContatosActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 187
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 188
    .local v3, "itauEmpresa":Ljava/util/List;, "Ljava/util/List<Lcom/itau/empresas/ui/widgets/ContatosChildItem;>;"
    new-instance v0, Lcom/itau/empresas/ui/widgets/ContatosChildItem;

    invoke-direct {v0}, Lcom/itau/empresas/ui/widgets/ContatosChildItem;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/ui/activity/ContatosActivity;->telefonesUteis:Lcom/itau/empresas/ui/widgets/ContatosChildItem;

    .line 189
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/ContatosActivity;->telefonesUteis:Lcom/itau/empresas/ui/widgets/ContatosChildItem;

    const v1, 0x7f0703e1

    invoke-virtual {p0, v1}, Lcom/itau/empresas/ui/activity/ContatosActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/widgets/ContatosChildItem;->setNumeroTelefone(Ljava/lang/String;)V

    .line 190
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/ContatosActivity;->telefonesUteis:Lcom/itau/empresas/ui/widgets/ContatosChildItem;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 191
    new-instance v0, Lcom/itau/empresas/ui/widgets/ContatosChildItem;

    invoke-direct {v0}, Lcom/itau/empresas/ui/widgets/ContatosChildItem;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/ui/activity/ContatosActivity;->telefonesUteis:Lcom/itau/empresas/ui/widgets/ContatosChildItem;

    .line 192
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/ContatosActivity;->telefonesUteis:Lcom/itau/empresas/ui/widgets/ContatosChildItem;

    const v1, 0x7f0703e2

    invoke-virtual {p0, v1}, Lcom/itau/empresas/ui/activity/ContatosActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/widgets/ContatosChildItem;->setHorarioDeAtendimento(Ljava/lang/String;)V

    .line 193
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/ContatosActivity;->telefonesUteis:Lcom/itau/empresas/ui/widgets/ContatosChildItem;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 195
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 196
    .local v4, "itauTelefone":Ljava/util/List;, "Ljava/util/List<Lcom/itau/empresas/ui/widgets/ContatosChildItem;>;"
    new-instance v0, Lcom/itau/empresas/ui/widgets/ContatosChildItem;

    invoke-direct {v0}, Lcom/itau/empresas/ui/widgets/ContatosChildItem;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/ui/activity/ContatosActivity;->telefonesUteis:Lcom/itau/empresas/ui/widgets/ContatosChildItem;

    .line 197
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/ContatosActivity;->telefonesUteis:Lcom/itau/empresas/ui/widgets/ContatosChildItem;

    const v1, 0x7f0703e3

    invoke-virtual {p0, v1}, Lcom/itau/empresas/ui/activity/ContatosActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/widgets/ContatosChildItem;->setNumeroTelefone(Ljava/lang/String;)V

    .line 198
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/ContatosActivity;->telefonesUteis:Lcom/itau/empresas/ui/widgets/ContatosChildItem;

    const v1, 0x7f0703e4

    invoke-virtual {p0, v1}, Lcom/itau/empresas/ui/activity/ContatosActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/widgets/ContatosChildItem;->setInformacoes(Ljava/lang/String;)V

    .line 199
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/ContatosActivity;->telefonesUteis:Lcom/itau/empresas/ui/widgets/ContatosChildItem;

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 200
    new-instance v0, Lcom/itau/empresas/ui/widgets/ContatosChildItem;

    invoke-direct {v0}, Lcom/itau/empresas/ui/widgets/ContatosChildItem;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/ui/activity/ContatosActivity;->telefonesUteis:Lcom/itau/empresas/ui/widgets/ContatosChildItem;

    .line 201
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/ContatosActivity;->telefonesUteis:Lcom/itau/empresas/ui/widgets/ContatosChildItem;

    const v1, 0x7f0703e5

    invoke-virtual {p0, v1}, Lcom/itau/empresas/ui/activity/ContatosActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/widgets/ContatosChildItem;->setNumeroTelefone(Ljava/lang/String;)V

    .line 202
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/ContatosActivity;->telefonesUteis:Lcom/itau/empresas/ui/widgets/ContatosChildItem;

    const v1, 0x7f0703e6

    invoke-virtual {p0, v1}, Lcom/itau/empresas/ui/activity/ContatosActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/widgets/ContatosChildItem;->setInformacoes(Ljava/lang/String;)V

    .line 203
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/ContatosActivity;->telefonesUteis:Lcom/itau/empresas/ui/widgets/ContatosChildItem;

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 204
    new-instance v0, Lcom/itau/empresas/ui/widgets/ContatosChildItem;

    invoke-direct {v0}, Lcom/itau/empresas/ui/widgets/ContatosChildItem;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/ui/activity/ContatosActivity;->telefonesUteis:Lcom/itau/empresas/ui/widgets/ContatosChildItem;

    .line 205
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/ContatosActivity;->telefonesUteis:Lcom/itau/empresas/ui/widgets/ContatosChildItem;

    const v1, 0x7f0703e7

    invoke-virtual {p0, v1}, Lcom/itau/empresas/ui/activity/ContatosActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/widgets/ContatosChildItem;->setHorarioDeAtendimento(Ljava/lang/String;)V

    .line 206
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/ContatosActivity;->telefonesUteis:Lcom/itau/empresas/ui/widgets/ContatosChildItem;

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 208
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 209
    .local v5, "itauCard":Ljava/util/List;, "Ljava/util/List<Lcom/itau/empresas/ui/widgets/ContatosChildItem;>;"
    new-instance v0, Lcom/itau/empresas/ui/widgets/ContatosChildItem;

    invoke-direct {v0}, Lcom/itau/empresas/ui/widgets/ContatosChildItem;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/ui/activity/ContatosActivity;->telefonesUteis:Lcom/itau/empresas/ui/widgets/ContatosChildItem;

    .line 210
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/ContatosActivity;->telefonesUteis:Lcom/itau/empresas/ui/widgets/ContatosChildItem;

    const v1, 0x7f0703d7

    invoke-virtual {p0, v1}, Lcom/itau/empresas/ui/activity/ContatosActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/widgets/ContatosChildItem;->setNumeroTelefone(Ljava/lang/String;)V

    .line 211
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/ContatosActivity;->telefonesUteis:Lcom/itau/empresas/ui/widgets/ContatosChildItem;

    const v1, 0x7f0703d8

    invoke-virtual {p0, v1}, Lcom/itau/empresas/ui/activity/ContatosActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/widgets/ContatosChildItem;->setInformacoes(Ljava/lang/String;)V

    .line 212
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/ContatosActivity;->telefonesUteis:Lcom/itau/empresas/ui/widgets/ContatosChildItem;

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 213
    new-instance v0, Lcom/itau/empresas/ui/widgets/ContatosChildItem;

    invoke-direct {v0}, Lcom/itau/empresas/ui/widgets/ContatosChildItem;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/ui/activity/ContatosActivity;->telefonesUteis:Lcom/itau/empresas/ui/widgets/ContatosChildItem;

    .line 214
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/ContatosActivity;->telefonesUteis:Lcom/itau/empresas/ui/widgets/ContatosChildItem;

    const v1, 0x7f0703d9

    invoke-virtual {p0, v1}, Lcom/itau/empresas/ui/activity/ContatosActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/widgets/ContatosChildItem;->setNumeroTelefone(Ljava/lang/String;)V

    .line 215
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/ContatosActivity;->telefonesUteis:Lcom/itau/empresas/ui/widgets/ContatosChildItem;

    const v1, 0x7f0703da

    invoke-virtual {p0, v1}, Lcom/itau/empresas/ui/activity/ContatosActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/widgets/ContatosChildItem;->setInformacoes(Ljava/lang/String;)V

    .line 216
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/ContatosActivity;->telefonesUteis:Lcom/itau/empresas/ui/widgets/ContatosChildItem;

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 217
    new-instance v0, Lcom/itau/empresas/ui/widgets/ContatosChildItem;

    invoke-direct {v0}, Lcom/itau/empresas/ui/widgets/ContatosChildItem;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/ui/activity/ContatosActivity;->telefonesUteis:Lcom/itau/empresas/ui/widgets/ContatosChildItem;

    .line 218
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/ContatosActivity;->telefonesUteis:Lcom/itau/empresas/ui/widgets/ContatosChildItem;

    const v1, 0x7f0703db

    invoke-virtual {p0, v1}, Lcom/itau/empresas/ui/activity/ContatosActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/widgets/ContatosChildItem;->setHorarioDeAtendimento(Ljava/lang/String;)V

    .line 219
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/ContatosActivity;->telefonesUteis:Lcom/itau/empresas/ui/widgets/ContatosChildItem;

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 221
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 222
    .local v6, "investFone":Ljava/util/List;, "Ljava/util/List<Lcom/itau/empresas/ui/widgets/ContatosChildItem;>;"
    new-instance v0, Lcom/itau/empresas/ui/widgets/ContatosChildItem;

    invoke-direct {v0}, Lcom/itau/empresas/ui/widgets/ContatosChildItem;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/ui/activity/ContatosActivity;->telefonesUteis:Lcom/itau/empresas/ui/widgets/ContatosChildItem;

    .line 223
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/ContatosActivity;->telefonesUteis:Lcom/itau/empresas/ui/widgets/ContatosChildItem;

    const v1, 0x7f0703d2

    invoke-virtual {p0, v1}, Lcom/itau/empresas/ui/activity/ContatosActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/widgets/ContatosChildItem;->setNumeroTelefone(Ljava/lang/String;)V

    .line 224
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/ContatosActivity;->telefonesUteis:Lcom/itau/empresas/ui/widgets/ContatosChildItem;

    const v1, 0x7f0703d3

    invoke-virtual {p0, v1}, Lcom/itau/empresas/ui/activity/ContatosActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/widgets/ContatosChildItem;->setInformacoes(Ljava/lang/String;)V

    .line 225
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/ContatosActivity;->telefonesUteis:Lcom/itau/empresas/ui/widgets/ContatosChildItem;

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 226
    new-instance v0, Lcom/itau/empresas/ui/widgets/ContatosChildItem;

    invoke-direct {v0}, Lcom/itau/empresas/ui/widgets/ContatosChildItem;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/ui/activity/ContatosActivity;->telefonesUteis:Lcom/itau/empresas/ui/widgets/ContatosChildItem;

    .line 227
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/ContatosActivity;->telefonesUteis:Lcom/itau/empresas/ui/widgets/ContatosChildItem;

    const v1, 0x7f0703d4

    invoke-virtual {p0, v1}, Lcom/itau/empresas/ui/activity/ContatosActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/widgets/ContatosChildItem;->setNumeroTelefone(Ljava/lang/String;)V

    .line 228
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/ContatosActivity;->telefonesUteis:Lcom/itau/empresas/ui/widgets/ContatosChildItem;

    const v1, 0x7f0703d5

    invoke-virtual {p0, v1}, Lcom/itau/empresas/ui/activity/ContatosActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/widgets/ContatosChildItem;->setInformacoes(Ljava/lang/String;)V

    .line 229
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/ContatosActivity;->telefonesUteis:Lcom/itau/empresas/ui/widgets/ContatosChildItem;

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 230
    new-instance v0, Lcom/itau/empresas/ui/widgets/ContatosChildItem;

    invoke-direct {v0}, Lcom/itau/empresas/ui/widgets/ContatosChildItem;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/ui/activity/ContatosActivity;->telefonesUteis:Lcom/itau/empresas/ui/widgets/ContatosChildItem;

    .line 231
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/ContatosActivity;->telefonesUteis:Lcom/itau/empresas/ui/widgets/ContatosChildItem;

    const v1, 0x7f0703d6

    invoke-virtual {p0, v1}, Lcom/itau/empresas/ui/activity/ContatosActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/widgets/ContatosChildItem;->setHorarioDeAtendimento(Ljava/lang/String;)V

    .line 232
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/ContatosActivity;->telefonesUteis:Lcom/itau/empresas/ui/widgets/ContatosChildItem;

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 234
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 235
    .local v7, "itauCorretora":Ljava/util/List;, "Ljava/util/List<Lcom/itau/empresas/ui/widgets/ContatosChildItem;>;"
    new-instance v0, Lcom/itau/empresas/ui/widgets/ContatosChildItem;

    invoke-direct {v0}, Lcom/itau/empresas/ui/widgets/ContatosChildItem;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/ui/activity/ContatosActivity;->telefonesUteis:Lcom/itau/empresas/ui/widgets/ContatosChildItem;

    .line 236
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/ContatosActivity;->telefonesUteis:Lcom/itau/empresas/ui/widgets/ContatosChildItem;

    const v1, 0x7f0703dc

    invoke-virtual {p0, v1}, Lcom/itau/empresas/ui/activity/ContatosActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/widgets/ContatosChildItem;->setNumeroTelefone(Ljava/lang/String;)V

    .line 237
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/ContatosActivity;->telefonesUteis:Lcom/itau/empresas/ui/widgets/ContatosChildItem;

    const v1, 0x7f0703dd

    invoke-virtual {p0, v1}, Lcom/itau/empresas/ui/activity/ContatosActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/widgets/ContatosChildItem;->setInformacoes(Ljava/lang/String;)V

    .line 238
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/ContatosActivity;->telefonesUteis:Lcom/itau/empresas/ui/widgets/ContatosChildItem;

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 239
    new-instance v0, Lcom/itau/empresas/ui/widgets/ContatosChildItem;

    invoke-direct {v0}, Lcom/itau/empresas/ui/widgets/ContatosChildItem;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/ui/activity/ContatosActivity;->telefonesUteis:Lcom/itau/empresas/ui/widgets/ContatosChildItem;

    .line 240
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/ContatosActivity;->telefonesUteis:Lcom/itau/empresas/ui/widgets/ContatosChildItem;

    const v1, 0x7f0703de

    invoke-virtual {p0, v1}, Lcom/itau/empresas/ui/activity/ContatosActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/widgets/ContatosChildItem;->setNumeroTelefone(Ljava/lang/String;)V

    .line 241
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/ContatosActivity;->telefonesUteis:Lcom/itau/empresas/ui/widgets/ContatosChildItem;

    const v1, 0x7f0703df

    invoke-virtual {p0, v1}, Lcom/itau/empresas/ui/activity/ContatosActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/widgets/ContatosChildItem;->setInformacoes(Ljava/lang/String;)V

    .line 242
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/ContatosActivity;->telefonesUteis:Lcom/itau/empresas/ui/widgets/ContatosChildItem;

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 243
    new-instance v0, Lcom/itau/empresas/ui/widgets/ContatosChildItem;

    invoke-direct {v0}, Lcom/itau/empresas/ui/widgets/ContatosChildItem;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/ui/activity/ContatosActivity;->telefonesUteis:Lcom/itau/empresas/ui/widgets/ContatosChildItem;

    .line 244
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/ContatosActivity;->telefonesUteis:Lcom/itau/empresas/ui/widgets/ContatosChildItem;

    const v1, 0x7f0703e0

    invoke-virtual {p0, v1}, Lcom/itau/empresas/ui/activity/ContatosActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/widgets/ContatosChildItem;->setHorarioDeAtendimento(Ljava/lang/String;)V

    .line 245
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/ContatosActivity;->telefonesUteis:Lcom/itau/empresas/ui/widgets/ContatosChildItem;

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 247
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 248
    .local v8, "sacItau":Ljava/util/List;, "Ljava/util/List<Lcom/itau/empresas/ui/widgets/ContatosChildItem;>;"
    new-instance v0, Lcom/itau/empresas/ui/widgets/ContatosChildItem;

    invoke-direct {v0}, Lcom/itau/empresas/ui/widgets/ContatosChildItem;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/ui/activity/ContatosActivity;->telefonesUteis:Lcom/itau/empresas/ui/widgets/ContatosChildItem;

    .line 249
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/ContatosActivity;->telefonesUteis:Lcom/itau/empresas/ui/widgets/ContatosChildItem;

    const v1, 0x7f0703ec

    invoke-virtual {p0, v1}, Lcom/itau/empresas/ui/activity/ContatosActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/widgets/ContatosChildItem;->setNumeroTelefone(Ljava/lang/String;)V

    .line 250
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/ContatosActivity;->telefonesUteis:Lcom/itau/empresas/ui/widgets/ContatosChildItem;

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 251
    new-instance v0, Lcom/itau/empresas/ui/widgets/ContatosChildItem;

    invoke-direct {v0}, Lcom/itau/empresas/ui/widgets/ContatosChildItem;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/ui/activity/ContatosActivity;->telefonesUteis:Lcom/itau/empresas/ui/widgets/ContatosChildItem;

    .line 252
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/ContatosActivity;->telefonesUteis:Lcom/itau/empresas/ui/widgets/ContatosChildItem;

    const v1, 0x7f0703ed

    invoke-virtual {p0, v1}, Lcom/itau/empresas/ui/activity/ContatosActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/widgets/ContatosChildItem;->setHorarioDeAtendimento(Ljava/lang/String;)V

    .line 253
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/ContatosActivity;->telefonesUteis:Lcom/itau/empresas/ui/widgets/ContatosChildItem;

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 255
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 256
    .local v9, "ouvidoriaItau":Ljava/util/List;, "Ljava/util/List<Lcom/itau/empresas/ui/widgets/ContatosChildItem;>;"
    new-instance v0, Lcom/itau/empresas/ui/widgets/ContatosChildItem;

    invoke-direct {v0}, Lcom/itau/empresas/ui/widgets/ContatosChildItem;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/ui/activity/ContatosActivity;->telefonesUteis:Lcom/itau/empresas/ui/widgets/ContatosChildItem;

    .line 257
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/ContatosActivity;->telefonesUteis:Lcom/itau/empresas/ui/widgets/ContatosChildItem;

    const v1, 0x7f0703e8

    invoke-virtual {p0, v1}, Lcom/itau/empresas/ui/activity/ContatosActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/widgets/ContatosChildItem;->setNumeroTelefone(Ljava/lang/String;)V

    .line 258
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/ContatosActivity;->telefonesUteis:Lcom/itau/empresas/ui/widgets/ContatosChildItem;

    invoke-interface {v9, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 259
    new-instance v0, Lcom/itau/empresas/ui/widgets/ContatosChildItem;

    invoke-direct {v0}, Lcom/itau/empresas/ui/widgets/ContatosChildItem;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/ui/activity/ContatosActivity;->telefonesUteis:Lcom/itau/empresas/ui/widgets/ContatosChildItem;

    .line 260
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/ContatosActivity;->telefonesUteis:Lcom/itau/empresas/ui/widgets/ContatosChildItem;

    const v1, 0x7f0703e9

    invoke-virtual {p0, v1}, Lcom/itau/empresas/ui/activity/ContatosActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/widgets/ContatosChildItem;->setNumeroTelefone(Ljava/lang/String;)V

    .line 261
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/ContatosActivity;->telefonesUteis:Lcom/itau/empresas/ui/widgets/ContatosChildItem;

    const v1, 0x7f0703ea

    invoke-virtual {p0, v1}, Lcom/itau/empresas/ui/activity/ContatosActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/widgets/ContatosChildItem;->setInformacoes(Ljava/lang/String;)V

    .line 262
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/ContatosActivity;->telefonesUteis:Lcom/itau/empresas/ui/widgets/ContatosChildItem;

    invoke-interface {v9, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 263
    new-instance v0, Lcom/itau/empresas/ui/widgets/ContatosChildItem;

    invoke-direct {v0}, Lcom/itau/empresas/ui/widgets/ContatosChildItem;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/ui/activity/ContatosActivity;->telefonesUteis:Lcom/itau/empresas/ui/widgets/ContatosChildItem;

    .line 264
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/ContatosActivity;->telefonesUteis:Lcom/itau/empresas/ui/widgets/ContatosChildItem;

    const v1, 0x7f0703eb

    invoke-virtual {p0, v1}, Lcom/itau/empresas/ui/activity/ContatosActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/widgets/ContatosChildItem;->setHorarioDeAtendimento(Ljava/lang/String;)V

    .line 265
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/ContatosActivity;->telefonesUteis:Lcom/itau/empresas/ui/widgets/ContatosChildItem;

    invoke-interface {v9, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 267
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/ContatosActivity;->listaItensContatos:Ljava/util/HashMap;

    iget-object v1, p0, Lcom/itau/empresas/ui/activity/ContatosActivity;->listaTituloContatos:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 268
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/ContatosActivity;->listaItensContatos:Ljava/util/HashMap;

    iget-object v1, p0, Lcom/itau/empresas/ui/activity/ContatosActivity;->listaTituloContatos:Ljava/util/List;

    const/4 v2, 0x1

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 269
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/ContatosActivity;->listaItensContatos:Ljava/util/HashMap;

    iget-object v1, p0, Lcom/itau/empresas/ui/activity/ContatosActivity;->listaTituloContatos:Ljava/util/List;

    const/4 v2, 0x2

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 270
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/ContatosActivity;->listaItensContatos:Ljava/util/HashMap;

    iget-object v1, p0, Lcom/itau/empresas/ui/activity/ContatosActivity;->listaTituloContatos:Ljava/util/List;

    const/4 v2, 0x3

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 271
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/ContatosActivity;->listaItensContatos:Ljava/util/HashMap;

    iget-object v1, p0, Lcom/itau/empresas/ui/activity/ContatosActivity;->listaTituloContatos:Ljava/util/List;

    const/4 v2, 0x4

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 272
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/ContatosActivity;->listaItensContatos:Ljava/util/HashMap;

    iget-object v1, p0, Lcom/itau/empresas/ui/activity/ContatosActivity;->listaTituloContatos:Ljava/util/List;

    const/4 v2, 0x5

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 273
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/ContatosActivity;->listaItensContatos:Ljava/util/HashMap;

    iget-object v1, p0, Lcom/itau/empresas/ui/activity/ContatosActivity;->listaTituloContatos:Ljava/util/List;

    const/4 v2, 0x6

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1, v9}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 274
    return-void
.end method


# virtual methods
.method aoIniciarActivity()V
    .registers 7

    .line 57
    invoke-direct {p0}, Lcom/itau/empresas/ui/activity/ContatosActivity;->preparaListContatos()V

    .line 58
    invoke-direct {p0}, Lcom/itau/empresas/ui/activity/ContatosActivity;->constroiToolbar()V

    .line 60
    invoke-static {}, Lcom/itau/empresas/ui/util/PoyntUtils;->isPoynt()Z

    move-result v0

    if-eqz v0, :cond_1e

    .line 62
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/ContatosActivity;->toolBar:Landroid/support/v7/widget/Toolbar;

    const v1, 0x7f020165

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->setNavigationIcon(I)V

    .line 63
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/ContatosActivity;->toolBar:Landroid/support/v7/widget/Toolbar;

    new-instance v1, Lcom/itau/empresas/ui/activity/ContatosActivity$1;

    invoke-direct {v1, p0}, Lcom/itau/empresas/ui/activity/ContatosActivity$1;-><init>(Lcom/itau/empresas/ui/activity/ContatosActivity;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->setNavigationOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 71
    :cond_1e
    new-instance v0, Lcom/itau/empresas/adapter/ContatosExpandableListAdapter;

    iget-object v1, p0, Lcom/itau/empresas/ui/activity/ContatosActivity;->listaTituloContatos:Ljava/util/List;

    iget-object v2, p0, Lcom/itau/empresas/ui/activity/ContatosActivity;->listaItensContatos:Ljava/util/HashMap;

    iget-object v3, p0, Lcom/itau/empresas/ui/activity/ContatosActivity;->listaDescricaoContatos:Ljava/util/List;

    invoke-direct {v0, p0, v1, v2, v3}, Lcom/itau/empresas/adapter/ContatosExpandableListAdapter;-><init>(Landroid/content/Context;Ljava/util/List;Ljava/util/HashMap;Ljava/util/List;)V

    iput-object v0, p0, Lcom/itau/empresas/ui/activity/ContatosActivity;->contatosExpandableListAdapter:Landroid/widget/ExpandableListAdapter;

    .line 74
    new-instance v4, Landroid/util/DisplayMetrics;

    invoke-direct {v4}, Landroid/util/DisplayMetrics;-><init>()V

    .line 75
    .local v4, "metrics":Landroid/util/DisplayMetrics;
    invoke-virtual {p0}, Lcom/itau/empresas/ui/activity/ContatosActivity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 76
    iget v5, v4, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 77
    .local v5, "width":I
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x12

    if-ge v0, v1, :cond_59

    .line 78
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/ContatosActivity;->listaContatos:Landroid/widget/ExpandableListView;

    .line 79
    const/high16 v1, 0x420c0000    # 35.0f

    invoke-virtual {p0, v1}, Lcom/itau/empresas/ui/activity/ContatosActivity;->getDipsFromPixel(F)I

    move-result v1

    sub-int v1, v5, v1

    const/high16 v2, 0x40a00000    # 5.0f

    invoke-virtual {p0, v2}, Lcom/itau/empresas/ui/activity/ContatosActivity;->getDipsFromPixel(F)I

    move-result v2

    sub-int v2, v5, v2

    invoke-virtual {v0, v1, v2}, Landroid/widget/ExpandableListView;->setIndicatorBounds(II)V

    goto :goto_6e

    .line 81
    :cond_59
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/ContatosActivity;->listaContatos:Landroid/widget/ExpandableListView;

    .line 82
    const/high16 v1, 0x420c0000    # 35.0f

    invoke-virtual {p0, v1}, Lcom/itau/empresas/ui/activity/ContatosActivity;->getDipsFromPixel(F)I

    move-result v1

    sub-int v1, v5, v1

    .line 83
    const/high16 v2, 0x40a00000    # 5.0f

    invoke-virtual {p0, v2}, Lcom/itau/empresas/ui/activity/ContatosActivity;->getDipsFromPixel(F)I

    move-result v2

    sub-int v2, v5, v2

    .line 82
    invoke-virtual {v0, v1, v2}, Landroid/widget/ExpandableListView;->setIndicatorBoundsRelative(II)V

    .line 86
    :goto_6e
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/ContatosActivity;->listaContatos:Landroid/widget/ExpandableListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ExpandableListView;->setGroupIndicator(Landroid/graphics/drawable/Drawable;)V

    .line 88
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/ContatosActivity;->listaContatos:Landroid/widget/ExpandableListView;

    iget-object v1, p0, Lcom/itau/empresas/ui/activity/ContatosActivity;->contatosExpandableListAdapter:Landroid/widget/ExpandableListAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ExpandableListView;->setAdapter(Landroid/widget/ExpandableListAdapter;)V

    .line 90
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/ContatosActivity;->listaContatos:Landroid/widget/ExpandableListView;

    new-instance v1, Lcom/itau/empresas/ui/activity/ContatosActivity$2;

    invoke-direct {v1, p0}, Lcom/itau/empresas/ui/activity/ContatosActivity$2;-><init>(Lcom/itau/empresas/ui/activity/ContatosActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ExpandableListView;->setOnGroupExpandListener(Landroid/widget/ExpandableListView$OnGroupExpandListener;)V

    .line 97
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/ContatosActivity;->listaContatos:Landroid/widget/ExpandableListView;

    new-instance v1, Lcom/itau/empresas/ui/activity/ContatosActivity$3;

    invoke-direct {v1, p0}, Lcom/itau/empresas/ui/activity/ContatosActivity$3;-><init>(Lcom/itau/empresas/ui/activity/ContatosActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ExpandableListView;->setOnGroupCollapseListener(Landroid/widget/ExpandableListView$OnGroupCollapseListener;)V

    .line 104
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/ContatosActivity;->listaContatos:Landroid/widget/ExpandableListView;

    new-instance v1, Lcom/itau/empresas/ui/activity/ContatosActivity$4;

    invoke-direct {v1, p0}, Lcom/itau/empresas/ui/activity/ContatosActivity$4;-><init>(Lcom/itau/empresas/ui/activity/ContatosActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ExpandableListView;->setOnChildClickListener(Landroid/widget/ExpandableListView$OnChildClickListener;)V

    .line 124
    return-void
.end method

.method public getDipsFromPixel(F)I
    .registers 5
    .param p1, "pixels"    # F

    .line 285
    invoke-virtual {p0}, Lcom/itau/empresas/ui/activity/ContatosActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v2, v0, Landroid/util/DisplayMetrics;->density:F

    .line 286
    .local v2, "scale":F
    mul-float v0, p1, v2

    const/high16 v1, 0x3f000000    # 0.5f

    add-float/2addr v0, v1

    float-to-int v0, v0

    return v0
.end method

.method public hitAnalytics()Ljava/lang/String;
    .registers 2

    .line 291
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/ContatosActivity;->toolBar:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v0}, Landroid/support/v7/widget/Toolbar;->getTitle()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .registers 4
    .param p1, "menu"    # Landroid/view/Menu;

    .line 301
    invoke-virtual {p0}, Lcom/itau/empresas/ui/activity/ContatosActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f0f0007

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 302
    const/4 v0, 0x1

    return v0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .registers 4
    .param p1, "item"    # Landroid/view/MenuItem;

    .line 307
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x7f0e06d1

    if-ne v0, v1, :cond_e

    .line 309
    invoke-static {p0}, Lcom/itau/empresas/ui/util/ViewUtils;->sairDoApp(Landroid/content/Context;)V

    .line 310
    const/4 v0, 0x1

    return v0

    .line 312
    :cond_e
    invoke-super {p0, p1}, Lcom/itau/empresas/ui/activity/BaseActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0
.end method

.method public onRequestPermissionsResult(I[Ljava/lang/String;[I)V
    .registers 5
    .param p1, "requestCode"    # I
    .param p2, "permissions"    # [Ljava/lang/String;
    .param p3, "grantResults"    # [I

    .line 146
    invoke-super {p0, p1, p2, p3}, Lcom/itau/empresas/ui/activity/BaseActivity;->onRequestPermissionsResult(I[Ljava/lang/String;[I)V

    .line 147
    const/4 v0, 0x3

    if-ne p1, v0, :cond_13

    .line 148
    invoke-static {}, Lcom/itau/empresas/ui/util/PermissoesUtil;->getPermissionCallphone()[Ljava/lang/String;

    move-result-object v0

    .line 147
    invoke-static {p0, v0}, Lcom/itau/empresas/ui/util/PermissoesUtil;->hasSelfPermissions(Landroid/content/Context;[Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 149
    invoke-direct {p0}, Lcom/itau/empresas/ui/activity/ContatosActivity;->ligar()V

    .line 151
    :cond_13
    return-void
.end method

.method public opkeysConsumidas()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Ljava/lang/String;>;"
        }
    .end annotation

    .line 296
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

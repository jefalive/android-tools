.class public final enum Lcom/google/zxing/l;
.super Ljava/lang/Enum;


# static fields
.field public static final enum a:Lcom/google/zxing/l;

.field public static final enum b:Lcom/google/zxing/l;

.field public static final enum c:Lcom/google/zxing/l;

.field public static final enum d:Lcom/google/zxing/l;

.field public static final enum e:Lcom/google/zxing/l;

.field public static final enum f:Lcom/google/zxing/l;

.field public static final enum g:Lcom/google/zxing/l;

.field public static final enum h:Lcom/google/zxing/l;

.field public static final enum i:Lcom/google/zxing/l;

.field public static final enum j:Lcom/google/zxing/l;

.field public static final enum k:Lcom/google/zxing/l;

.field private static final synthetic l:[Lcom/google/zxing/l;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    new-instance v0, Lcom/google/zxing/l;

    const-string v1, "OTHER"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/google/zxing/l;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/zxing/l;->a:Lcom/google/zxing/l;

    new-instance v0, Lcom/google/zxing/l;

    const-string v1, "ORIENTATION"

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lcom/google/zxing/l;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/zxing/l;->b:Lcom/google/zxing/l;

    new-instance v0, Lcom/google/zxing/l;

    const-string v1, "BYTE_SEGMENTS"

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2}, Lcom/google/zxing/l;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/zxing/l;->c:Lcom/google/zxing/l;

    new-instance v0, Lcom/google/zxing/l;

    const-string v1, "ERROR_CORRECTION_LEVEL"

    const/4 v2, 0x3

    invoke-direct {v0, v1, v2}, Lcom/google/zxing/l;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/zxing/l;->d:Lcom/google/zxing/l;

    new-instance v0, Lcom/google/zxing/l;

    const-string v1, "ISSUE_NUMBER"

    const/4 v2, 0x4

    invoke-direct {v0, v1, v2}, Lcom/google/zxing/l;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/zxing/l;->e:Lcom/google/zxing/l;

    new-instance v0, Lcom/google/zxing/l;

    const-string v1, "SUGGESTED_PRICE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/google/zxing/l;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/zxing/l;->f:Lcom/google/zxing/l;

    new-instance v0, Lcom/google/zxing/l;

    const-string v1, "POSSIBLE_COUNTRY"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/google/zxing/l;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/zxing/l;->g:Lcom/google/zxing/l;

    new-instance v0, Lcom/google/zxing/l;

    const-string v1, "UPC_EAN_EXTENSION"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/google/zxing/l;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/zxing/l;->h:Lcom/google/zxing/l;

    new-instance v0, Lcom/google/zxing/l;

    const-string v1, "PDF417_EXTRA_METADATA"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/google/zxing/l;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/zxing/l;->i:Lcom/google/zxing/l;

    new-instance v0, Lcom/google/zxing/l;

    const-string v1, "STRUCTURED_APPEND_SEQUENCE"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/google/zxing/l;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/zxing/l;->j:Lcom/google/zxing/l;

    new-instance v0, Lcom/google/zxing/l;

    const-string v1, "STRUCTURED_APPEND_PARITY"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/google/zxing/l;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/zxing/l;->k:Lcom/google/zxing/l;

    const/16 v0, 0xb

    new-array v0, v0, [Lcom/google/zxing/l;

    sget-object v1, Lcom/google/zxing/l;->a:Lcom/google/zxing/l;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/zxing/l;->b:Lcom/google/zxing/l;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/zxing/l;->c:Lcom/google/zxing/l;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/zxing/l;->d:Lcom/google/zxing/l;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/zxing/l;->e:Lcom/google/zxing/l;

    const/4 v2, 0x4

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/zxing/l;->f:Lcom/google/zxing/l;

    const/4 v2, 0x5

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/zxing/l;->g:Lcom/google/zxing/l;

    const/4 v2, 0x6

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/zxing/l;->h:Lcom/google/zxing/l;

    const/4 v2, 0x7

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/zxing/l;->i:Lcom/google/zxing/l;

    const/16 v2, 0x8

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/zxing/l;->j:Lcom/google/zxing/l;

    const/16 v2, 0x9

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/zxing/l;->k:Lcom/google/zxing/l;

    const/16 v2, 0xa

    aput-object v1, v0, v2

    sput-object v0, Lcom/google/zxing/l;->l:[Lcom/google/zxing/l;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/zxing/l;
    .registers 2

    const-class v0, Lcom/google/zxing/l;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/zxing/l;

    return-object v0
.end method

.method public static values()[Lcom/google/zxing/l;
    .registers 1

    sget-object v0, Lcom/google/zxing/l;->l:[Lcom/google/zxing/l;

    invoke-virtual {v0}, [Lcom/google/zxing/l;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/zxing/l;

    return-object v0
.end method

.class final Lorg/threeten/bp/format/DateTimeFormatterBuilder$InstantPrinterParser;
.super Ljava/lang/Object;
.source "DateTimeFormatterBuilder.java"

# interfaces
.implements Lorg/threeten/bp/format/DateTimeFormatterBuilder$DateTimePrinterParser;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/threeten/bp/format/DateTimeFormatterBuilder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "InstantPrinterParser"
.end annotation


# instance fields
.field private final fractionalDigits:I


# direct methods
.method constructor <init>(I)V
    .registers 2
    .param p1, "fractionalDigits"    # I

    .line 2921
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2922
    iput p1, p0, Lorg/threeten/bp/format/DateTimeFormatterBuilder$InstantPrinterParser;->fractionalDigits:I

    .line 2923
    return-void
.end method


# virtual methods
.method public parse(Lorg/threeten/bp/format/DateTimeParseContext;Ljava/lang/CharSequence;I)I
    .registers 31
    .param p1, "context"    # Lorg/threeten/bp/format/DateTimeParseContext;
    .param p2, "text"    # Ljava/lang/CharSequence;
    .param p3, "position"    # I

    .line 3001
    invoke-virtual/range {p1 .. p1}, Lorg/threeten/bp/format/DateTimeParseContext;->copy()Lorg/threeten/bp/format/DateTimeParseContext;

    move-result-object v7

    .line 3002
    .local v7, "newContext":Lorg/threeten/bp/format/DateTimeParseContext;
    move-object/from16 v0, p0

    iget v0, v0, Lorg/threeten/bp/format/DateTimeFormatterBuilder$InstantPrinterParser;->fractionalDigits:I

    if-gez v0, :cond_c

    const/4 v8, 0x0

    goto :goto_10

    :cond_c
    move-object/from16 v0, p0

    iget v8, v0, Lorg/threeten/bp/format/DateTimeFormatterBuilder$InstantPrinterParser;->fractionalDigits:I

    .line 3003
    .local v8, "minDigits":I
    :goto_10
    move-object/from16 v0, p0

    iget v0, v0, Lorg/threeten/bp/format/DateTimeFormatterBuilder$InstantPrinterParser;->fractionalDigits:I

    if-gez v0, :cond_19

    const/16 v9, 0x9

    goto :goto_1d

    :cond_19
    move-object/from16 v0, p0

    iget v9, v0, Lorg/threeten/bp/format/DateTimeFormatterBuilder$InstantPrinterParser;->fractionalDigits:I

    .line 3004
    .local v9, "maxDigits":I
    :goto_1d
    new-instance v0, Lorg/threeten/bp/format/DateTimeFormatterBuilder;

    invoke-direct {v0}, Lorg/threeten/bp/format/DateTimeFormatterBuilder;-><init>()V

    sget-object v1, Lorg/threeten/bp/format/DateTimeFormatter;->ISO_LOCAL_DATE:Lorg/threeten/bp/format/DateTimeFormatter;

    invoke-virtual {v0, v1}, Lorg/threeten/bp/format/DateTimeFormatterBuilder;->append(Lorg/threeten/bp/format/DateTimeFormatter;)Lorg/threeten/bp/format/DateTimeFormatterBuilder;

    move-result-object v0

    const/16 v1, 0x54

    invoke-virtual {v0, v1}, Lorg/threeten/bp/format/DateTimeFormatterBuilder;->appendLiteral(C)Lorg/threeten/bp/format/DateTimeFormatterBuilder;

    move-result-object v0

    sget-object v1, Lorg/threeten/bp/temporal/ChronoField;->HOUR_OF_DAY:Lorg/threeten/bp/temporal/ChronoField;

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Lorg/threeten/bp/format/DateTimeFormatterBuilder;->appendValue(Lorg/threeten/bp/temporal/TemporalField;I)Lorg/threeten/bp/format/DateTimeFormatterBuilder;

    move-result-object v0

    const/16 v1, 0x3a

    invoke-virtual {v0, v1}, Lorg/threeten/bp/format/DateTimeFormatterBuilder;->appendLiteral(C)Lorg/threeten/bp/format/DateTimeFormatterBuilder;

    move-result-object v0

    sget-object v1, Lorg/threeten/bp/temporal/ChronoField;->MINUTE_OF_HOUR:Lorg/threeten/bp/temporal/ChronoField;

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Lorg/threeten/bp/format/DateTimeFormatterBuilder;->appendValue(Lorg/threeten/bp/temporal/TemporalField;I)Lorg/threeten/bp/format/DateTimeFormatterBuilder;

    move-result-object v0

    const/16 v1, 0x3a

    invoke-virtual {v0, v1}, Lorg/threeten/bp/format/DateTimeFormatterBuilder;->appendLiteral(C)Lorg/threeten/bp/format/DateTimeFormatterBuilder;

    move-result-object v0

    sget-object v1, Lorg/threeten/bp/temporal/ChronoField;->SECOND_OF_MINUTE:Lorg/threeten/bp/temporal/ChronoField;

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Lorg/threeten/bp/format/DateTimeFormatterBuilder;->appendValue(Lorg/threeten/bp/temporal/TemporalField;I)Lorg/threeten/bp/format/DateTimeFormatterBuilder;

    move-result-object v0

    sget-object v1, Lorg/threeten/bp/temporal/ChronoField;->NANO_OF_SECOND:Lorg/threeten/bp/temporal/ChronoField;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v8, v9, v2}, Lorg/threeten/bp/format/DateTimeFormatterBuilder;->appendFraction(Lorg/threeten/bp/temporal/TemporalField;IIZ)Lorg/threeten/bp/format/DateTimeFormatterBuilder;

    move-result-object v0

    const/16 v1, 0x5a

    invoke-virtual {v0, v1}, Lorg/threeten/bp/format/DateTimeFormatterBuilder;->appendLiteral(C)Lorg/threeten/bp/format/DateTimeFormatterBuilder;

    move-result-object v0

    invoke-virtual {v0}, Lorg/threeten/bp/format/DateTimeFormatterBuilder;->toFormatter()Lorg/threeten/bp/format/DateTimeFormatter;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lorg/threeten/bp/format/DateTimeFormatter;->toPrinterParser(Z)Lorg/threeten/bp/format/DateTimeFormatterBuilder$CompositePrinterParser;

    move-result-object v10

    .line 3009
    .local v10, "parser":Lorg/threeten/bp/format/DateTimeFormatterBuilder$CompositePrinterParser;
    move-object/from16 v0, p2

    move/from16 v1, p3

    invoke-virtual {v10, v7, v0, v1}, Lorg/threeten/bp/format/DateTimeFormatterBuilder$CompositePrinterParser;->parse(Lorg/threeten/bp/format/DateTimeParseContext;Ljava/lang/CharSequence;I)I

    move-result v11

    .line 3010
    .local v11, "pos":I
    if-gez v11, :cond_70

    .line 3011
    return v11

    .line 3015
    :cond_70
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->YEAR:Lorg/threeten/bp/temporal/ChronoField;

    invoke-virtual {v7, v0}, Lorg/threeten/bp/format/DateTimeParseContext;->getParsed(Lorg/threeten/bp/temporal/TemporalField;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v12

    .line 3016
    .local v12, "yearParsed":J
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->MONTH_OF_YEAR:Lorg/threeten/bp/temporal/ChronoField;

    invoke-virtual {v7, v0}, Lorg/threeten/bp/format/DateTimeParseContext;->getParsed(Lorg/threeten/bp/temporal/TemporalField;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->intValue()I

    move-result v14

    .line 3017
    .local v14, "month":I
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->DAY_OF_MONTH:Lorg/threeten/bp/temporal/ChronoField;

    invoke-virtual {v7, v0}, Lorg/threeten/bp/format/DateTimeParseContext;->getParsed(Lorg/threeten/bp/temporal/TemporalField;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->intValue()I

    move-result v15

    .line 3018
    .local v15, "day":I
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->HOUR_OF_DAY:Lorg/threeten/bp/temporal/ChronoField;

    invoke-virtual {v7, v0}, Lorg/threeten/bp/format/DateTimeParseContext;->getParsed(Lorg/threeten/bp/temporal/TemporalField;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->intValue()I

    move-result v16

    .line 3019
    .local v16, "hour":I
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->MINUTE_OF_HOUR:Lorg/threeten/bp/temporal/ChronoField;

    invoke-virtual {v7, v0}, Lorg/threeten/bp/format/DateTimeParseContext;->getParsed(Lorg/threeten/bp/temporal/TemporalField;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->intValue()I

    move-result v17

    .line 3020
    .local v17, "min":I
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->SECOND_OF_MINUTE:Lorg/threeten/bp/temporal/ChronoField;

    invoke-virtual {v7, v0}, Lorg/threeten/bp/format/DateTimeParseContext;->getParsed(Lorg/threeten/bp/temporal/TemporalField;)Ljava/lang/Long;

    move-result-object v18

    .line 3021
    .local v18, "secVal":Ljava/lang/Long;
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->NANO_OF_SECOND:Lorg/threeten/bp/temporal/ChronoField;

    invoke-virtual {v7, v0}, Lorg/threeten/bp/format/DateTimeParseContext;->getParsed(Lorg/threeten/bp/temporal/TemporalField;)Ljava/lang/Long;

    move-result-object v19

    .line 3022
    .local v19, "nanoVal":Ljava/lang/Long;
    if-eqz v18, :cond_b5

    invoke-virtual/range {v18 .. v18}, Ljava/lang/Long;->intValue()I

    move-result v20

    goto :goto_b7

    :cond_b5
    const/16 v20, 0x0

    .line 3023
    .local v20, "sec":I
    :goto_b7
    if-eqz v19, :cond_be

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Long;->intValue()I

    move-result v21

    goto :goto_c0

    :cond_be
    const/16 v21, 0x0

    .line 3024
    .local v21, "nano":I
    :goto_c0
    long-to-int v0, v12

    rem-int/lit16 v1, v0, 0x2710

    move/from16 v22, v1

    .line 3025
    .local v22, "year":I
    const/16 v23, 0x0

    .line 3026
    .local v23, "days":I
    move/from16 v0, v16

    const/16 v1, 0x18

    if-ne v0, v1, :cond_d8

    if-nez v17, :cond_d8

    if-nez v20, :cond_d8

    if-nez v21, :cond_d8

    .line 3027
    const/16 v16, 0x0

    .line 3028
    const/16 v23, 0x1

    goto :goto_ef

    .line 3029
    :cond_d8
    move/from16 v0, v16

    const/16 v1, 0x17

    if-ne v0, v1, :cond_ef

    move/from16 v0, v17

    const/16 v1, 0x3b

    if-ne v0, v1, :cond_ef

    move/from16 v0, v20

    const/16 v1, 0x3c

    if-ne v0, v1, :cond_ef

    .line 3030
    invoke-virtual/range {p1 .. p1}, Lorg/threeten/bp/format/DateTimeParseContext;->setParsedLeapSecond()V

    .line 3031
    const/16 v20, 0x3b

    .line 3035
    :cond_ef
    :goto_ef
    move/from16 v0, v22

    move v1, v14

    move v2, v15

    move/from16 v3, v16

    move/from16 v4, v17

    move/from16 v5, v20

    const/4 v6, 0x0

    :try_start_fa
    invoke-static/range {v0 .. v6}, Lorg/threeten/bp/LocalDateTime;->of(IIIIIII)Lorg/threeten/bp/LocalDateTime;

    move-result-object v0

    move/from16 v1, v23

    int-to-long v1, v1

    invoke-virtual {v0, v1, v2}, Lorg/threeten/bp/LocalDateTime;->plusDays(J)Lorg/threeten/bp/LocalDateTime;

    move-result-object v26

    .line 3036
    .local v26, "ldt":Lorg/threeten/bp/LocalDateTime;
    sget-object v0, Lorg/threeten/bp/ZoneOffset;->UTC:Lorg/threeten/bp/ZoneOffset;

    move-object/from16 v1, v26

    invoke-virtual {v1, v0}, Lorg/threeten/bp/LocalDateTime;->toEpochSecond(Lorg/threeten/bp/ZoneOffset;)J

    move-result-wide v24

    .line 3037
    .local v24, "instantSecs":J
    const-wide/16 v0, 0x2710

    div-long v0, v12, v0

    const-wide v2, 0x497968bd80L

    invoke-static {v0, v1, v2, v3}, Lorg/threeten/bp/jdk8/Jdk8Methods;->safeMultiply(JJ)J
    :try_end_119
    .catch Ljava/lang/RuntimeException; {:try_start_fa .. :try_end_119} :catch_11d

    move-result-wide v0

    add-long v24, v24, v0

    .line 3040
    .end local v26    # "ldt":Lorg/threeten/bp/LocalDateTime;
    goto :goto_121

    .line 3038
    :catch_11d
    move-exception v26

    .line 3039
    .local v26, "ex":Ljava/lang/RuntimeException;
    xor-int/lit8 v0, p3, -0x1

    return v0

    .line 3041
    .end local v26    # "ex":Ljava/lang/RuntimeException;
    :goto_121
    move/from16 v26, v11

    .line 3042
    .local v26, "successPos":I
    move-object/from16 v0, p1

    sget-object v1, Lorg/threeten/bp/temporal/ChronoField;->INSTANT_SECONDS:Lorg/threeten/bp/temporal/ChronoField;

    move-wide/from16 v2, v24

    move/from16 v4, p3

    move/from16 v5, v26

    invoke-virtual/range {v0 .. v5}, Lorg/threeten/bp/format/DateTimeParseContext;->setParsedField(Lorg/threeten/bp/temporal/TemporalField;JII)I

    move-result v26

    .line 3043
    move-object/from16 v0, p1

    sget-object v1, Lorg/threeten/bp/temporal/ChronoField;->NANO_OF_SECOND:Lorg/threeten/bp/temporal/ChronoField;

    move/from16 v2, v21

    int-to-long v2, v2

    move/from16 v4, p3

    move/from16 v5, v26

    invoke-virtual/range {v0 .. v5}, Lorg/threeten/bp/format/DateTimeParseContext;->setParsedField(Lorg/threeten/bp/temporal/TemporalField;JII)I

    move-result v0

    return v0
.end method

.method public print(Lorg/threeten/bp/format/DateTimePrintContext;Ljava/lang/StringBuilder;)Z
    .registers 20
    .param p1, "context"    # Lorg/threeten/bp/format/DateTimePrintContext;
    .param p2, "buf"    # Ljava/lang/StringBuilder;

    .line 2928
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->INSTANT_SECONDS:Lorg/threeten/bp/temporal/ChronoField;

    move-object/from16 v1, p1

    invoke-virtual {v1, v0}, Lorg/threeten/bp/format/DateTimePrintContext;->getValue(Lorg/threeten/bp/temporal/TemporalField;)Ljava/lang/Long;

    move-result-object v4

    .line 2929
    .local v4, "inSecs":Ljava/lang/Long;
    const-wide/16 v0, 0x0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    .line 2930
    .local v5, "inNanos":Ljava/lang/Long;
    invoke-virtual/range {p1 .. p1}, Lorg/threeten/bp/format/DateTimePrintContext;->getTemporal()Lorg/threeten/bp/temporal/TemporalAccessor;

    move-result-object v0

    sget-object v1, Lorg/threeten/bp/temporal/ChronoField;->NANO_OF_SECOND:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {v0, v1}, Lorg/threeten/bp/temporal/TemporalAccessor;->isSupported(Lorg/threeten/bp/temporal/TemporalField;)Z

    move-result v0

    if-eqz v0, :cond_28

    .line 2931
    invoke-virtual/range {p1 .. p1}, Lorg/threeten/bp/format/DateTimePrintContext;->getTemporal()Lorg/threeten/bp/temporal/TemporalAccessor;

    move-result-object v0

    sget-object v1, Lorg/threeten/bp/temporal/ChronoField;->NANO_OF_SECOND:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {v0, v1}, Lorg/threeten/bp/temporal/TemporalAccessor;->getLong(Lorg/threeten/bp/temporal/TemporalField;)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    .line 2933
    :cond_28
    if-nez v4, :cond_2c

    .line 2934
    const/4 v0, 0x0

    return v0

    .line 2936
    :cond_2c
    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    .line 2937
    .local v6, "inSec":J
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->NANO_OF_SECOND:Lorg/threeten/bp/temporal/ChronoField;

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lorg/threeten/bp/temporal/ChronoField;->checkValidIntValue(J)I

    move-result v8

    .line 2938
    .local v8, "inNano":I
    const-wide v0, -0xe79747c00L

    cmp-long v0, v6, v0

    if-ltz v0, :cond_9a

    .line 2940
    const-wide v0, 0x497968bd80L

    sub-long v0, v6, v0

    const-wide v2, 0xe79747c00L

    add-long v9, v0, v2

    .line 2941
    .local v9, "zeroSecs":J
    const-wide v0, 0x497968bd80L

    invoke-static {v9, v10, v0, v1}, Lorg/threeten/bp/jdk8/Jdk8Methods;->floorDiv(JJ)J

    move-result-wide v0

    const-wide/16 v2, 0x1

    add-long v11, v0, v2

    .line 2942
    .local v11, "hi":J
    const-wide v0, 0x497968bd80L

    invoke-static {v9, v10, v0, v1}, Lorg/threeten/bp/jdk8/Jdk8Methods;->floorMod(JJ)J

    move-result-wide v13

    .line 2943
    .local v13, "lo":J
    const-wide v0, 0xe79747c00L

    sub-long v0, v13, v0

    sget-object v2, Lorg/threeten/bp/ZoneOffset;->UTC:Lorg/threeten/bp/ZoneOffset;

    const/4 v3, 0x0

    invoke-static {v0, v1, v3, v2}, Lorg/threeten/bp/LocalDateTime;->ofEpochSecond(JILorg/threeten/bp/ZoneOffset;)Lorg/threeten/bp/LocalDateTime;

    move-result-object v15

    .line 2944
    .local v15, "ldt":Lorg/threeten/bp/LocalDateTime;
    const-wide/16 v0, 0x0

    cmp-long v0, v11, v0

    if-lez v0, :cond_86

    .line 2945
    move-object/from16 v0, p2

    const/16 v1, 0x2b

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v11, v12}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 2947
    :cond_86
    move-object/from16 v0, p2

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2948
    invoke-virtual {v15}, Lorg/threeten/bp/LocalDateTime;->getSecond()I

    move-result v0

    if-nez v0, :cond_98

    .line 2949
    const-string v0, ":00"

    move-object/from16 v1, p2

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2951
    .end local v9    # "zeroSecs":J
    .end local v11    # "hi":J
    .end local v13    # "lo":J
    .end local v15    # "ldt":Lorg/threeten/bp/LocalDateTime;
    :cond_98
    goto/16 :goto_10c

    .line 2953
    :cond_9a
    const-wide v0, 0xe79747c00L

    add-long v9, v6, v0

    .line 2954
    .local v9, "zeroSecs":J
    const-wide v0, 0x497968bd80L

    div-long v11, v9, v0

    .line 2955
    .local v11, "hi":J
    const-wide v0, 0x497968bd80L

    rem-long v13, v9, v0

    .line 2956
    .local v13, "lo":J
    const-wide v0, 0xe79747c00L

    sub-long v0, v13, v0

    sget-object v2, Lorg/threeten/bp/ZoneOffset;->UTC:Lorg/threeten/bp/ZoneOffset;

    const/4 v3, 0x0

    invoke-static {v0, v1, v3, v2}, Lorg/threeten/bp/LocalDateTime;->ofEpochSecond(JILorg/threeten/bp/ZoneOffset;)Lorg/threeten/bp/LocalDateTime;

    move-result-object v15

    .line 2957
    .local v15, "ldt":Lorg/threeten/bp/LocalDateTime;
    invoke-virtual/range {p2 .. p2}, Ljava/lang/StringBuilder;->length()I

    move-result v16

    .line 2958
    .local v16, "pos":I
    move-object/from16 v0, p2

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2959
    invoke-virtual {v15}, Lorg/threeten/bp/LocalDateTime;->getSecond()I

    move-result v0

    if-nez v0, :cond_d3

    .line 2960
    const-string v0, ":00"

    move-object/from16 v1, p2

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2962
    :cond_d3
    const-wide/16 v0, 0x0

    cmp-long v0, v11, v0

    if-gez v0, :cond_10c

    .line 2963
    invoke-virtual {v15}, Lorg/threeten/bp/LocalDateTime;->getYear()I

    move-result v0

    const/16 v1, -0x2710

    if-ne v0, v1, :cond_f3

    .line 2964
    add-int/lit8 v0, v16, 0x2

    const-wide/16 v1, 0x1

    sub-long v1, v11, v1

    invoke-static {v1, v2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    move-object/from16 v2, p2

    move/from16 v3, v16

    invoke-virtual {v2, v3, v0, v1}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_10c

    .line 2965
    :cond_f3
    const-wide/16 v0, 0x0

    cmp-long v0, v13, v0

    if-nez v0, :cond_101

    .line 2966
    move-object/from16 v0, p2

    move/from16 v1, v16

    invoke-virtual {v0, v1, v11, v12}, Ljava/lang/StringBuilder;->insert(IJ)Ljava/lang/StringBuilder;

    goto :goto_10c

    .line 2968
    :cond_101
    add-int/lit8 v0, v16, 0x1

    invoke-static {v11, v12}, Ljava/lang/Math;->abs(J)J

    move-result-wide v1

    move-object/from16 v3, p2

    invoke-virtual {v3, v0, v1, v2}, Ljava/lang/StringBuilder;->insert(IJ)Ljava/lang/StringBuilder;

    .line 2973
    .end local v9    # "zeroSecs":J
    .end local v11    # "hi":J
    .end local v13    # "lo":J
    .end local v15    # "ldt":Lorg/threeten/bp/LocalDateTime;
    .end local v16    # "pos":I
    :cond_10c
    :goto_10c
    move-object/from16 v0, p0

    iget v0, v0, Lorg/threeten/bp/format/DateTimeFormatterBuilder$InstantPrinterParser;->fractionalDigits:I

    const/4 v1, -0x2

    if-ne v0, v1, :cond_166

    .line 2974
    if-eqz v8, :cond_1a1

    .line 2975
    move-object/from16 v0, p2

    const/16 v1, 0x2e

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 2976
    const v0, 0xf4240

    rem-int v0, v8, v0

    if-nez v0, :cond_13a

    .line 2977
    const v0, 0xf4240

    div-int v0, v8, v0

    add-int/lit16 v0, v0, 0x3e8

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    move-object/from16 v1, p2

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_1a1

    .line 2978
    :cond_13a
    rem-int/lit16 v0, v8, 0x3e8

    if-nez v0, :cond_153

    .line 2979
    div-int/lit16 v0, v8, 0x3e8

    const v1, 0xf4240

    add-int/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    move-object/from16 v1, p2

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1a1

    .line 2981
    :cond_153
    const v0, 0x3b9aca00

    add-int/2addr v0, v8

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    move-object/from16 v1, p2

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1a1

    .line 2984
    :cond_166
    move-object/from16 v0, p0

    iget v0, v0, Lorg/threeten/bp/format/DateTimeFormatterBuilder$InstantPrinterParser;->fractionalDigits:I

    if-gtz v0, :cond_175

    move-object/from16 v0, p0

    iget v0, v0, Lorg/threeten/bp/format/DateTimeFormatterBuilder$InstantPrinterParser;->fractionalDigits:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_1a1

    if-lez v8, :cond_1a1

    .line 2985
    :cond_175
    move-object/from16 v0, p2

    const/16 v1, 0x2e

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 2986
    const v9, 0x5f5e100

    .line 2987
    .local v9, "div":I
    const/4 v10, 0x0

    .local v10, "i":I
    :goto_180
    move-object/from16 v0, p0

    iget v0, v0, Lorg/threeten/bp/format/DateTimeFormatterBuilder$InstantPrinterParser;->fractionalDigits:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_189

    if-gtz v8, :cond_18f

    :cond_189
    move-object/from16 v0, p0

    iget v0, v0, Lorg/threeten/bp/format/DateTimeFormatterBuilder$InstantPrinterParser;->fractionalDigits:I

    if-ge v10, v0, :cond_1a1

    .line 2988
    :cond_18f
    div-int v11, v8, v9

    .line 2989
    .local v11, "digit":I
    add-int/lit8 v0, v11, 0x30

    int-to-char v0, v0

    move-object/from16 v1, p2

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 2990
    mul-int v0, v11, v9

    sub-int/2addr v8, v0

    .line 2991
    div-int/lit8 v9, v9, 0xa

    .line 2987
    .end local v11    # "digit":I
    add-int/lit8 v10, v10, 0x1

    goto :goto_180

    .line 2994
    .end local v9    # "div":I
    .end local v10    # "i":I
    :cond_1a1
    :goto_1a1
    move-object/from16 v0, p2

    const/16 v1, 0x5a

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 2995
    const/4 v0, 0x1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .registers 2

    .line 3048
    const-string v0, "Instant()"

    return-object v0
.end method

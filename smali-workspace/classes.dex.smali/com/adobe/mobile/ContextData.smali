.class final Lcom/adobe/mobile/ContextData;
.super Ljava/lang/Object;
.source "ContextData.java"


# instance fields
.field protected contextData:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
        }
    .end annotation
.end field

.field protected value:Ljava/lang/Object;


# direct methods
.method constructor <init>()V
    .registers 2

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/adobe/mobile/ContextData;->value:Ljava/lang/Object;

    .line 25
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/adobe/mobile/ContextData;->contextData:Ljava/util/HashMap;

    return-void
.end method


# virtual methods
.method protected containsKey(Ljava/lang/String;)Z
    .registers 3
    .param p1, "key"    # Ljava/lang/String;

    .line 38
    iget-object v0, p0, Lcom/adobe/mobile/ContextData;->contextData:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method protected get(Ljava/lang/String;)Lcom/adobe/mobile/ContextData;
    .registers 3
    .param p1, "key"    # Ljava/lang/String;

    .line 46
    iget-object v0, p0, Lcom/adobe/mobile/ContextData;->contextData:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/adobe/mobile/ContextData;

    return-object v0
.end method

.method protected put(Ljava/lang/String;Lcom/adobe/mobile/ContextData;)V
    .registers 4
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Lcom/adobe/mobile/ContextData;

    .line 42
    iget-object v0, p0, Lcom/adobe/mobile/ContextData;->contextData:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 43
    return-void
.end method

.method public declared-synchronized toString()Ljava/lang/String;
    .registers 4

    monitor-enter p0

    .line 29
    const-string v2, ""

    .line 30
    .local v2, "addString":Ljava/lang/String;
    :try_start_3
    iget-object v0, p0, Lcom/adobe/mobile/ContextData;->value:Ljava/lang/Object;

    if-eqz v0, :cond_d

    .line 31
    iget-object v0, p0, Lcom/adobe/mobile/ContextData;->value:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    .line 34
    :cond_d
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-super {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_21
    .catchall {:try_start_3 .. :try_end_21} :catchall_24

    move-result-object v0

    monitor-exit p0

    return-object v0

    .end local v2    # "addString":Ljava/lang/String;
    :catchall_24
    move-exception v2

    monitor-exit p0

    throw v2
.end method

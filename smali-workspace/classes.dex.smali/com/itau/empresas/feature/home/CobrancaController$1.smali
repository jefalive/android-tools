.class Lcom/itau/empresas/feature/home/CobrancaController$1;
.super Ljava/lang/Object;
.source "CobrancaController.java"

# interfaces
.implements Lbr/com/itau/sdk/android/core/type/BackendCall;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/itau/empresas/feature/home/CobrancaController;->consultaWidget(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/itau/empresas/feature/home/CobrancaController;

.field final synthetic val$codigoCarreira:Ljava/lang/String;

.field final synthetic val$dataConsulta:Ljava/lang/String;

.field final synthetic val$reduzido:Z

.field final synthetic val$tipoCobranca:Ljava/lang/String;

.field final synthetic val$tipoVisao:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/itau/empresas/feature/home/CobrancaController;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 7
    .param p1, "this$0"    # Lcom/itau/empresas/feature/home/CobrancaController;

    .line 30
    iput-object p1, p0, Lcom/itau/empresas/feature/home/CobrancaController$1;->this$0:Lcom/itau/empresas/feature/home/CobrancaController;

    iput-boolean p2, p0, Lcom/itau/empresas/feature/home/CobrancaController$1;->val$reduzido:Z

    iput-object p3, p0, Lcom/itau/empresas/feature/home/CobrancaController$1;->val$tipoVisao:Ljava/lang/String;

    iput-object p4, p0, Lcom/itau/empresas/feature/home/CobrancaController$1;->val$tipoCobranca:Ljava/lang/String;

    iput-object p5, p0, Lcom/itau/empresas/feature/home/CobrancaController$1;->val$dataConsulta:Ljava/lang/String;

    iput-object p6, p0, Lcom/itau/empresas/feature/home/CobrancaController$1;->val$codigoCarreira:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public callBackend()V
    .registers 12

    .line 33
    iget-object v0, p0, Lcom/itau/empresas/feature/home/CobrancaController$1;->this$0:Lcom/itau/empresas/feature/home/CobrancaController;

    iget-object v1, p0, Lcom/itau/empresas/feature/home/CobrancaController$1;->this$0:Lcom/itau/empresas/feature/home/CobrancaController;

    .line 34
    # invokes: Lcom/itau/empresas/feature/home/CobrancaController;->api()Ljava/lang/Object;
    invoke-static {v1}, Lcom/itau/empresas/feature/home/CobrancaController;->access$000(Lcom/itau/empresas/feature/home/CobrancaController;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/itau/empresas/api/Api;

    iget-boolean v2, p0, Lcom/itau/empresas/feature/home/CobrancaController$1;->val$reduzido:Z

    iget-object v3, p0, Lcom/itau/empresas/feature/home/CobrancaController$1;->this$0:Lcom/itau/empresas/feature/home/CobrancaController;

    iget-object v3, v3, Lcom/itau/empresas/feature/home/CobrancaController;->app:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v3}, Lcom/itau/empresas/CustomApplication;->getContaSelecionada()Lcom/itau/empresas/api/model/ContaOperadorVO;

    move-result-object v3

    invoke-virtual {v3}, Lcom/itau/empresas/api/model/ContaOperadorVO;->getAgencia()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/itau/empresas/feature/home/CobrancaController$1;->this$0:Lcom/itau/empresas/feature/home/CobrancaController;

    iget-object v4, v4, Lcom/itau/empresas/feature/home/CobrancaController;->app:Lcom/itau/empresas/CustomApplication;

    .line 35
    invoke-virtual {v4}, Lcom/itau/empresas/CustomApplication;->getContaSelecionada()Lcom/itau/empresas/api/model/ContaOperadorVO;

    move-result-object v4

    invoke-virtual {v4}, Lcom/itau/empresas/api/model/ContaOperadorVO;->getConta()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/itau/empresas/feature/home/CobrancaController$1;->this$0:Lcom/itau/empresas/feature/home/CobrancaController;

    iget-object v5, v5, Lcom/itau/empresas/feature/home/CobrancaController;->app:Lcom/itau/empresas/CustomApplication;

    .line 36
    invoke-virtual {v5}, Lcom/itau/empresas/CustomApplication;->getContaSelecionada()Lcom/itau/empresas/api/model/ContaOperadorVO;

    move-result-object v5

    invoke-virtual {v5}, Lcom/itau/empresas/api/model/ContaOperadorVO;->getDac()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/itau/empresas/feature/home/CobrancaController$1;->this$0:Lcom/itau/empresas/feature/home/CobrancaController;

    iget-object v6, v6, Lcom/itau/empresas/feature/home/CobrancaController;->app:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v6}, Lcom/itau/empresas/CustomApplication;->getCodigoOperador()Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/itau/empresas/feature/home/CobrancaController$1;->val$tipoVisao:Ljava/lang/String;

    iget-object v8, p0, Lcom/itau/empresas/feature/home/CobrancaController$1;->val$tipoCobranca:Ljava/lang/String;

    iget-object v9, p0, Lcom/itau/empresas/feature/home/CobrancaController$1;->val$dataConsulta:Ljava/lang/String;

    iget-object v10, p0, Lcom/itau/empresas/feature/home/CobrancaController$1;->val$codigoCarreira:Ljava/lang/String;

    .line 34
    invoke-interface/range {v1 .. v10}, Lcom/itau/empresas/api/Api;->francesinha(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/itau/empresas/api/model/FrancesinhaVO;

    move-result-object v1

    .line 33
    # invokes: Lcom/itau/empresas/feature/home/CobrancaController;->TrataRetornoWidget(Lcom/itau/empresas/api/model/FrancesinhaVO;)V
    invoke-static {v0, v1}, Lcom/itau/empresas/feature/home/CobrancaController;->access$100(Lcom/itau/empresas/feature/home/CobrancaController;Lcom/itau/empresas/api/model/FrancesinhaVO;)V

    .line 41
    return-void
.end method

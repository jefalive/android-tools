.class Lcom/google/android/gms/internal/zzgu$1;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/gms/internal/zzgu;->zzgk()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic zzGB:Lcom/google/android/gms/internal/zzgu;

.field final synthetic zzqu:Ljava/util/concurrent/CountDownLatch;


# direct methods
.method constructor <init>(Lcom/google/android/gms/internal/zzgu;Ljava/util/concurrent/CountDownLatch;)V
    .registers 3

    iput-object p1, p0, Lcom/google/android/gms/internal/zzgu$1;->zzGB:Lcom/google/android/gms/internal/zzgu;

    iput-object p2, p0, Lcom/google/android/gms/internal/zzgu$1;->zzqu:Ljava/util/concurrent/CountDownLatch;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .registers 7

    iget-object v0, p0, Lcom/google/android/gms/internal/zzgu$1;->zzGB:Lcom/google/android/gms/internal/zzgu;

    iget-object v4, v0, Lcom/google/android/gms/internal/zzgu;->zzGg:Ljava/lang/Object;

    monitor-enter v4

    :try_start_5
    iget-object v0, p0, Lcom/google/android/gms/internal/zzgu$1;->zzGB:Lcom/google/android/gms/internal/zzgu;

    iget-object v1, p0, Lcom/google/android/gms/internal/zzgu$1;->zzGB:Lcom/google/android/gms/internal/zzgu;

    invoke-static {v1}, Lcom/google/android/gms/internal/zzgu;->zza(Lcom/google/android/gms/internal/zzgu;)Lcom/google/android/gms/internal/zzjp;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/internal/zzgu$1;->zzGB:Lcom/google/android/gms/internal/zzgu;

    iget-object v2, v2, Lcom/google/android/gms/internal/zzgu;->zzGz:Lcom/google/android/gms/internal/zzes;

    iget-object v3, p0, Lcom/google/android/gms/internal/zzgu$1;->zzqu:Ljava/util/concurrent/CountDownLatch;

    invoke-static {v1, v2, v3}, Lcom/google/android/gms/ads/internal/zzm;->zza(Lcom/google/android/gms/internal/zzjp;Lcom/google/android/gms/internal/zzes;Ljava/util/concurrent/CountDownLatch;)Z

    move-result v1

    invoke-static {v0, v1}, Lcom/google/android/gms/internal/zzgu;->zza(Lcom/google/android/gms/internal/zzgu;Z)Z
    :try_end_1a
    .catchall {:try_start_5 .. :try_end_1a} :catchall_1c

    monitor-exit v4

    goto :goto_1f

    :catchall_1c
    move-exception v5

    monitor-exit v4

    throw v5

    :goto_1f
    return-void
.end method

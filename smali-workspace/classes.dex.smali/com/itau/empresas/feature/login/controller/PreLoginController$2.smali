.class Lcom/itau/empresas/feature/login/controller/PreLoginController$2;
.super Ljava/lang/Object;
.source "PreLoginController.java"

# interfaces
.implements Lbr/com/itau/sdk/android/core/type/BackendCall;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/itau/empresas/feature/login/controller/PreLoginController;->buscaOperador(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/itau/empresas/feature/login/controller/PreLoginController;

.field final synthetic val$codigoOperador:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/itau/empresas/feature/login/controller/PreLoginController;Ljava/lang/String;)V
    .registers 3
    .param p1, "this$0"    # Lcom/itau/empresas/feature/login/controller/PreLoginController;

    .line 52
    iput-object p1, p0, Lcom/itau/empresas/feature/login/controller/PreLoginController$2;->this$0:Lcom/itau/empresas/feature/login/controller/PreLoginController;

    iput-object p2, p0, Lcom/itau/empresas/feature/login/controller/PreLoginController$2;->val$codigoOperador:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public callBackend()V
    .registers 10

    .line 55
    new-instance v0, Lcom/itau/empresas/feature/login/model/PreLoginUsuarioVO;

    iget-object v1, p0, Lcom/itau/empresas/feature/login/controller/PreLoginController$2;->val$codigoOperador:Ljava/lang/String;

    .line 56
    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_c

    const/4 v6, 0x0

    goto :goto_e

    :cond_c
    iget-object v6, p0, Lcom/itau/empresas/feature/login/controller/PreLoginController$2;->val$codigoOperador:Ljava/lang/String;

    :goto_e
    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v6}, Lcom/itau/empresas/feature/login/model/PreLoginUsuarioVO;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v7, v0

    .line 57
    .local v7, "usuario":Lcom/itau/empresas/feature/login/model/PreLoginUsuarioVO;
    iget-object v0, p0, Lcom/itau/empresas/feature/login/controller/PreLoginController$2;->this$0:Lcom/itau/empresas/feature/login/controller/PreLoginController;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/login/controller/PreLoginController;->api()Lcom/itau/empresas/feature/login/PreLoginAPI;

    move-result-object v0

    new-instance v1, Lcom/itau/empresas/feature/login/model/PreLoginVO;

    .line 59
    # invokes: Lcom/itau/empresas/feature/login/controller/PreLoginController;->enviaVerificacaoParaChaveDeAcesso()Z
    invoke-static {}, Lcom/itau/empresas/feature/login/controller/PreLoginController;->access$100()Z

    move-result v2

    invoke-direct {v1, v7, v2}, Lcom/itau/empresas/feature/login/model/PreLoginVO;-><init>(Lcom/itau/empresas/feature/login/model/PreLoginUsuarioVO;Z)V

    .line 58
    invoke-interface {v0, v1}, Lcom/itau/empresas/feature/login/PreLoginAPI;->buscaOperadoresAtivos(Lcom/itau/empresas/feature/login/model/PreLoginVO;)Ljava/util/List;

    move-result-object v8

    .line 60
    .local v8, "operadorVOs":Ljava/util/List;, "Ljava/util/List<Lcom/itau/empresas/api/model/OperadorVO;>;"
    iget-object v0, p0, Lcom/itau/empresas/feature/login/controller/PreLoginController$2;->this$0:Lcom/itau/empresas/feature/login/controller/PreLoginController;

    # getter for: Lcom/itau/empresas/feature/login/controller/PreLoginController;->listener:Lcom/itau/empresas/feature/login/strategy/PreLoginControllerListener;
    invoke-static {v0}, Lcom/itau/empresas/feature/login/controller/PreLoginController;->access$000(Lcom/itau/empresas/feature/login/controller/PreLoginController;)Lcom/itau/empresas/feature/login/strategy/PreLoginControllerListener;

    move-result-object v0

    invoke-interface {v0, v8}, Lcom/itau/empresas/feature/login/strategy/PreLoginControllerListener;->listaOperadores(Ljava/util/List;)V

    .line 61
    return-void
.end method

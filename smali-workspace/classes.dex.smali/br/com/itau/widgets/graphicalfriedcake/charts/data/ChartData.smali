.class public abstract Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;
.super Ljava/lang/Object;
.source "ChartData.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet<+Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;>;>Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field protected mDataSets:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<TT;>;"
        }
    .end annotation
.end field

.field protected mLastEnd:I

.field protected mLastStart:I

.field protected mLeftAxisMax:F

.field protected mLeftAxisMin:F

.field protected mRightAxisMax:F

.field protected mRightAxisMin:F

.field private mXValAverageLength:F

.field protected mXVals:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Ljava/lang/String;>;"
        }
    .end annotation
.end field

.field protected mYMax:F

.field protected mYMin:F

.field private mYValCount:I

.field private mYValueSum:F


# direct methods
.method public constructor <init>()V
    .registers 2

    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    const/4 v0, 0x0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mYMax:F

    .line 27
    const/4 v0, 0x0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mYMin:F

    .line 29
    const/4 v0, 0x0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mLeftAxisMax:F

    .line 31
    const/4 v0, 0x0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mLeftAxisMin:F

    .line 33
    const/4 v0, 0x0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mRightAxisMax:F

    .line 35
    const/4 v0, 0x0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mRightAxisMin:F

    .line 38
    const/4 v0, 0x0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mYValueSum:F

    .line 41
    const/4 v0, 0x0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mYValCount:I

    .line 44
    const/4 v0, 0x0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mLastStart:I

    .line 47
    const/4 v0, 0x0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mLastEnd:I

    .line 53
    const/4 v0, 0x0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mXValAverageLength:F

    .line 62
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mXVals:Ljava/util/List;

    .line 63
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mDataSets:Ljava/util/List;

    .line 64
    return-void
.end method

.method public constructor <init>(Ljava/util/List;)V
    .registers 3
    .param p1, "xVals"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<Ljava/lang/String;>;)V"
        }
    .end annotation

    .line 72
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    const/4 v0, 0x0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mYMax:F

    .line 27
    const/4 v0, 0x0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mYMin:F

    .line 29
    const/4 v0, 0x0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mLeftAxisMax:F

    .line 31
    const/4 v0, 0x0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mLeftAxisMin:F

    .line 33
    const/4 v0, 0x0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mRightAxisMax:F

    .line 35
    const/4 v0, 0x0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mRightAxisMin:F

    .line 38
    const/4 v0, 0x0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mYValueSum:F

    .line 41
    const/4 v0, 0x0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mYValCount:I

    .line 44
    const/4 v0, 0x0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mLastStart:I

    .line 47
    const/4 v0, 0x0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mLastEnd:I

    .line 53
    const/4 v0, 0x0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mXValAverageLength:F

    .line 73
    iput-object p1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mXVals:Ljava/util/List;

    .line 74
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mDataSets:Ljava/util/List;

    .line 75
    invoke-virtual {p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->init()V

    .line 76
    return-void
.end method

.method public constructor <init>(Ljava/util/List;Ljava/util/List;)V
    .registers 4
    .param p1, "xVals"    # Ljava/util/List;
    .param p2, "sets"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<Ljava/lang/String;>;Ljava/util/List<TT;>;)V"
        }
    .end annotation

    .line 98
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    const/4 v0, 0x0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mYMax:F

    .line 27
    const/4 v0, 0x0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mYMin:F

    .line 29
    const/4 v0, 0x0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mLeftAxisMax:F

    .line 31
    const/4 v0, 0x0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mLeftAxisMin:F

    .line 33
    const/4 v0, 0x0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mRightAxisMax:F

    .line 35
    const/4 v0, 0x0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mRightAxisMin:F

    .line 38
    const/4 v0, 0x0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mYValueSum:F

    .line 41
    const/4 v0, 0x0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mYValCount:I

    .line 44
    const/4 v0, 0x0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mLastStart:I

    .line 47
    const/4 v0, 0x0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mLastEnd:I

    .line 53
    const/4 v0, 0x0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mXValAverageLength:F

    .line 99
    iput-object p1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mXVals:Ljava/util/List;

    .line 100
    iput-object p2, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mDataSets:Ljava/util/List;

    .line 102
    invoke-virtual {p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->init()V

    .line 103
    return-void
.end method

.method public constructor <init>([Ljava/lang/String;)V
    .registers 3
    .param p1, "xVals"    # [Ljava/lang/String;

    .line 84
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    const/4 v0, 0x0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mYMax:F

    .line 27
    const/4 v0, 0x0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mYMin:F

    .line 29
    const/4 v0, 0x0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mLeftAxisMax:F

    .line 31
    const/4 v0, 0x0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mLeftAxisMin:F

    .line 33
    const/4 v0, 0x0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mRightAxisMax:F

    .line 35
    const/4 v0, 0x0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mRightAxisMin:F

    .line 38
    const/4 v0, 0x0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mYValueSum:F

    .line 41
    const/4 v0, 0x0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mYValCount:I

    .line 44
    const/4 v0, 0x0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mLastStart:I

    .line 47
    const/4 v0, 0x0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mLastEnd:I

    .line 53
    const/4 v0, 0x0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mXValAverageLength:F

    .line 85
    invoke-direct {p0, p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->arrayToList([Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mXVals:Ljava/util/List;

    .line 86
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mDataSets:Ljava/util/List;

    .line 87
    invoke-virtual {p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->init()V

    .line 88
    return-void
.end method

.method public constructor <init>([Ljava/lang/String;Ljava/util/List;)V
    .registers 4
    .param p1, "xVals"    # [Ljava/lang/String;
    .param p2, "sets"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([Ljava/lang/String;Ljava/util/List<TT;>;)V"
        }
    .end annotation

    .line 113
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    const/4 v0, 0x0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mYMax:F

    .line 27
    const/4 v0, 0x0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mYMin:F

    .line 29
    const/4 v0, 0x0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mLeftAxisMax:F

    .line 31
    const/4 v0, 0x0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mLeftAxisMin:F

    .line 33
    const/4 v0, 0x0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mRightAxisMax:F

    .line 35
    const/4 v0, 0x0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mRightAxisMin:F

    .line 38
    const/4 v0, 0x0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mYValueSum:F

    .line 41
    const/4 v0, 0x0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mYValCount:I

    .line 44
    const/4 v0, 0x0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mLastStart:I

    .line 47
    const/4 v0, 0x0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mLastEnd:I

    .line 53
    const/4 v0, 0x0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mXValAverageLength:F

    .line 114
    invoke-direct {p0, p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->arrayToList([Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mXVals:Ljava/util/List;

    .line 115
    iput-object p2, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mDataSets:Ljava/util/List;

    .line 117
    invoke-virtual {p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->init()V

    .line 118
    return-void
.end method

.method private arrayToList([Ljava/lang/String;)Ljava/util/List;
    .registers 3
    .param p1, "array"    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([Ljava/lang/String;)Ljava/util/List<Ljava/lang/String;>;"
        }
    .end annotation

    .line 127
    invoke-static {p1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method private calcXValAverageLength()V
    .registers 4

    .line 150
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mXVals:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-gtz v0, :cond_d

    .line 151
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mXValAverageLength:F

    .line 152
    return-void

    .line 155
    :cond_d
    const/high16 v1, 0x3f800000    # 1.0f

    .line 157
    .local v1, "sum":F
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_10
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mXVals:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_29

    .line 158
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mXVals:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    int-to-float v0, v0

    add-float/2addr v1, v0

    .line 157
    add-int/lit8 v2, v2, 0x1

    goto :goto_10

    .line 161
    .end local v2    # "i":I
    :cond_29
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mXVals:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    int-to-float v0, v0

    div-float v0, v1, v0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mXValAverageLength:F

    .line 162
    return-void
.end method

.method public static generateXVals(II)Ljava/util/List;
    .registers 6
    .param p0, "from"    # I
    .param p1, "to"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)Ljava/util/List<Ljava/lang/String;>;"
        }
    .end annotation

    .line 849
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 851
    .local v2, "xvals":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    move v3, p0

    .local v3, "i":I
    :goto_6
    if-ge v3, p1, :cond_21

    .line 852
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 851
    add-int/lit8 v3, v3, 0x1

    goto :goto_6

    .line 855
    .end local v3    # "i":I
    :cond_21
    return-object v2
.end method

.method private handleEmptyAxis(Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;)V
    .registers 4
    .param p1, "firstLeft"    # Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;
    .param p2, "firstRight"    # Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;TT;)V"
        }
    .end annotation

    .line 587
    if-nez p1, :cond_b

    .line 588
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mRightAxisMax:F

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mLeftAxisMax:F

    .line 589
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mRightAxisMin:F

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mLeftAxisMin:F

    goto :goto_15

    .line 590
    :cond_b
    if-nez p2, :cond_15

    .line 591
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mLeftAxisMax:F

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mRightAxisMax:F

    .line 592
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mLeftAxisMin:F

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mRightAxisMin:F

    .line 594
    :cond_15
    :goto_15
    return-void
.end method

.method private isLegal()V
    .registers 4

    .line 170
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mDataSets:Ljava/util/List;

    if-nez v0, :cond_5

    .line 171
    return-void

    .line 173
    :cond_5
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_6
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mDataSets:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_31

    .line 174
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mDataSets:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;

    .line 175
    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->getYVals()Ljava/util/List;

    move-result-object v0

    .line 176
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iget-object v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mXVals:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-le v0, v1, :cond_2e

    .line 177
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "One or more of the DataSet Entry arrays are longer than the x-values array of this ChartData object."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 173
    :cond_2e
    add-int/lit8 v2, v2, 0x1

    goto :goto_6

    .line 181
    .end local v2    # "i":I
    :cond_31
    return-void
.end method


# virtual methods
.method public addDataSet(Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;)V
    .registers 4
    .param p1, "d"    # Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .line 533
    if-nez p1, :cond_3

    .line 534
    return-void

    .line 536
    :cond_3
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mYValCount:I

    invoke-virtual {p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->getEntryCount()I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mYValCount:I

    .line 537
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mYValueSum:F

    invoke-virtual {p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->getYValueSum()F

    move-result v1

    add-float/2addr v0, v1

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mYValueSum:F

    .line 539
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mDataSets:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-gtz v0, :cond_4d

    .line 541
    invoke-virtual {p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->getYMax()F

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mYMax:F

    .line 542
    invoke-virtual {p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->getYMin()F

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mYMin:F

    .line 544
    invoke-virtual {p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->getAxisDependency()Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$AxisDependency;

    move-result-object v0

    sget-object v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$AxisDependency;->LEFT:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$AxisDependency;

    if-ne v0, v1, :cond_3f

    .line 546
    invoke-virtual {p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->getYMax()F

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mLeftAxisMax:F

    .line 547
    invoke-virtual {p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->getYMin()F

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mLeftAxisMin:F

    goto/16 :goto_b6

    .line 549
    :cond_3f
    invoke-virtual {p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->getYMax()F

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mRightAxisMax:F

    .line 550
    invoke-virtual {p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->getYMin()F

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mRightAxisMin:F

    goto/16 :goto_b6

    .line 554
    :cond_4d
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mYMax:F

    invoke-virtual {p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->getYMax()F

    move-result v1

    cmpg-float v0, v0, v1

    if-gez v0, :cond_5d

    .line 555
    invoke-virtual {p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->getYMax()F

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mYMax:F

    .line 556
    :cond_5d
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mYMin:F

    invoke-virtual {p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->getYMin()F

    move-result v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_6d

    .line 557
    invoke-virtual {p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->getYMin()F

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mYMin:F

    .line 559
    :cond_6d
    invoke-virtual {p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->getAxisDependency()Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$AxisDependency;

    move-result-object v0

    sget-object v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$AxisDependency;->LEFT:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$AxisDependency;

    if-ne v0, v1, :cond_96

    .line 561
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mLeftAxisMax:F

    invoke-virtual {p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->getYMax()F

    move-result v1

    cmpg-float v0, v0, v1

    if-gez v0, :cond_85

    .line 562
    invoke-virtual {p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->getYMax()F

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mLeftAxisMax:F

    .line 563
    :cond_85
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mLeftAxisMin:F

    invoke-virtual {p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->getYMin()F

    move-result v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_b6

    .line 564
    invoke-virtual {p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->getYMin()F

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mLeftAxisMin:F

    goto :goto_b6

    .line 566
    :cond_96
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mRightAxisMax:F

    invoke-virtual {p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->getYMax()F

    move-result v1

    cmpg-float v0, v0, v1

    if-gez v0, :cond_a6

    .line 567
    invoke-virtual {p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->getYMax()F

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mRightAxisMax:F

    .line 568
    :cond_a6
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mRightAxisMin:F

    invoke-virtual {p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->getYMin()F

    move-result v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_b6

    .line 569
    invoke-virtual {p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->getYMin()F

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mRightAxisMin:F

    .line 573
    :cond_b6
    :goto_b6
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mDataSets:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 575
    invoke-virtual {p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->getFirstLeft()Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;

    move-result-object v0

    invoke-virtual {p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->getFirstRight()Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->handleEmptyAxis(Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;)V

    .line 576
    return-void
.end method

.method public addEntry(Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;I)V
    .registers 7
    .param p1, "e"    # Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;
    .param p2, "dataSetIndex"    # I

    .line 647
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mDataSets:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-le v0, p2, :cond_b5

    if-ltz p2, :cond_b5

    .line 649
    invoke-virtual {p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;->getVal()F

    move-result v2

    .line 650
    .local v2, "val":F
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mDataSets:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;

    .line 652
    .local v3, "set":Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;, "TT;"
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mYValCount:I

    if-nez v0, :cond_42

    .line 653
    iput v2, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mYMin:F

    .line 654
    iput v2, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mYMax:F

    .line 656
    invoke-virtual {v3}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->getAxisDependency()Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$AxisDependency;

    move-result-object v0

    sget-object v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$AxisDependency;->LEFT:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$AxisDependency;

    if-ne v0, v1, :cond_35

    .line 658
    invoke-virtual {p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;->getVal()F

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mLeftAxisMax:F

    .line 659
    invoke-virtual {p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;->getVal()F

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mLeftAxisMin:F

    goto/16 :goto_9b

    .line 661
    :cond_35
    invoke-virtual {p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;->getVal()F

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mRightAxisMax:F

    .line 662
    invoke-virtual {p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;->getVal()F

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mRightAxisMin:F

    goto :goto_9b

    .line 666
    :cond_42
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mYMax:F

    cmpg-float v0, v0, v2

    if-gez v0, :cond_4a

    .line 667
    iput v2, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mYMax:F

    .line 668
    :cond_4a
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mYMin:F

    cmpl-float v0, v0, v2

    if-lez v0, :cond_52

    .line 669
    iput v2, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mYMin:F

    .line 671
    :cond_52
    invoke-virtual {v3}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->getAxisDependency()Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$AxisDependency;

    move-result-object v0

    sget-object v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$AxisDependency;->LEFT:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$AxisDependency;

    if-ne v0, v1, :cond_7b

    .line 673
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mLeftAxisMax:F

    invoke-virtual {p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;->getVal()F

    move-result v1

    cmpg-float v0, v0, v1

    if-gez v0, :cond_6a

    .line 674
    invoke-virtual {p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;->getVal()F

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mLeftAxisMax:F

    .line 675
    :cond_6a
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mLeftAxisMin:F

    invoke-virtual {p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;->getVal()F

    move-result v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_9b

    .line 676
    invoke-virtual {p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;->getVal()F

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mLeftAxisMin:F

    goto :goto_9b

    .line 678
    :cond_7b
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mRightAxisMax:F

    invoke-virtual {p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;->getVal()F

    move-result v1

    cmpg-float v0, v0, v1

    if-gez v0, :cond_8b

    .line 679
    invoke-virtual {p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;->getVal()F

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mRightAxisMax:F

    .line 680
    :cond_8b
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mRightAxisMin:F

    invoke-virtual {p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;->getVal()F

    move-result v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_9b

    .line 681
    invoke-virtual {p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;->getVal()F

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mRightAxisMin:F

    .line 685
    :cond_9b
    :goto_9b
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mYValCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mYValCount:I

    .line 686
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mYValueSum:F

    add-float/2addr v0, v2

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mYValueSum:F

    .line 688
    invoke-virtual {p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->getFirstLeft()Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;

    move-result-object v0

    invoke-virtual {p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->getFirstRight()Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->handleEmptyAxis(Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;)V

    .line 691
    invoke-virtual {v3, p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->addEntry(Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;)V

    .line 692
    .end local v2    # "val":F
    .end local v3    # "set":Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;, "TT;"
    .end local v3
    goto :goto_bc

    .line 693
    :cond_b5
    const-string v0, "addEntry"

    const-string v1, "Cannot add Entry because dataSetIndex too high or too low."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 695
    :goto_bc
    return-void
.end method

.method public addXValue(Ljava/lang/String;)V
    .registers 4
    .param p1, "xVal"    # Ljava/lang/String;

    .line 407
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mXValAverageLength:F

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    int-to-float v1, v1

    add-float/2addr v0, v1

    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr v0, v1

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mXValAverageLength:F

    .line 408
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mXVals:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 409
    return-void
.end method

.method public calcMinMax(II)V
    .registers 9
    .param p1, "start"    # I
    .param p2, "end"    # I

    .line 196
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mDataSets:Ljava/util/List;

    if-eqz v0, :cond_d

    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mDataSets:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ge v0, v1, :cond_15

    .line 198
    :cond_d
    const/4 v0, 0x0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mYMax:F

    .line 199
    const/4 v0, 0x0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mYMin:F

    goto/16 :goto_128

    .line 202
    :cond_15
    iput p1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mLastStart:I

    .line 203
    iput p2, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mLastEnd:I

    .line 205
    const v0, 0x7f7fffff    # Float.MAX_VALUE

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mYMin:F

    .line 206
    const v0, -0x800001

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mYMax:F

    .line 208
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_24
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mDataSets:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_7a

    .line 210
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mDataSets:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;

    invoke-virtual {v0, p1, p2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->calcMinMax(II)V

    .line 212
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mDataSets:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->getYMin()F

    move-result v0

    iget v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mYMin:F

    cmpg-float v0, v0, v1

    if-gez v0, :cond_57

    .line 213
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mDataSets:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->getYMin()F

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mYMin:F

    .line 215
    :cond_57
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mDataSets:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->getYMax()F

    move-result v0

    iget v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mYMax:F

    cmpl-float v0, v0, v1

    if-lez v0, :cond_77

    .line 216
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mDataSets:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->getYMax()F

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mYMax:F

    .line 208
    :cond_77
    add-int/lit8 v2, v2, 0x1

    goto :goto_24

    .line 219
    .end local v2    # "i":I
    :cond_7a
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mYMin:F

    const v1, 0x7f7fffff    # Float.MAX_VALUE

    cmpl-float v0, v0, v1

    if-nez v0, :cond_89

    .line 220
    const/4 v0, 0x0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mYMin:F

    .line 221
    const/4 v0, 0x0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mYMax:F

    .line 225
    :cond_89
    invoke-virtual {p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->getFirstLeft()Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;

    move-result-object v2

    .line 227
    .local v2, "firstLeft":Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;, "TT;"
    if-eqz v2, :cond_d7

    .line 229
    invoke-virtual {v2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->getYMax()F

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mLeftAxisMax:F

    .line 230
    invoke-virtual {v2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->getYMin()F

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mLeftAxisMin:F

    .line 232
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mDataSets:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_a1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_d7

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;

    .line 233
    .local v4, "dataSet":Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;, "Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet<*>;"
    invoke-virtual {v4}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->getAxisDependency()Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$AxisDependency;

    move-result-object v0

    sget-object v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$AxisDependency;->LEFT:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$AxisDependency;

    if-ne v0, v1, :cond_d6

    .line 234
    invoke-virtual {v4}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->getYMin()F

    move-result v0

    iget v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mLeftAxisMin:F

    cmpg-float v0, v0, v1

    if-gez v0, :cond_c6

    .line 235
    invoke-virtual {v4}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->getYMin()F

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mLeftAxisMin:F

    .line 237
    :cond_c6
    invoke-virtual {v4}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->getYMax()F

    move-result v0

    iget v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mLeftAxisMax:F

    cmpl-float v0, v0, v1

    if-lez v0, :cond_d6

    .line 238
    invoke-virtual {v4}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->getYMax()F

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mLeftAxisMax:F

    .line 240
    .end local v4    # "dataSet":Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;, "Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet<*>;"
    .end local v4
    :cond_d6
    goto :goto_a1

    .line 244
    :cond_d7
    invoke-virtual {p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->getFirstRight()Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;

    move-result-object v3

    .line 246
    .local v3, "firstRight":Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;, "TT;"
    if-eqz v3, :cond_125

    .line 248
    invoke-virtual {v3}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->getYMax()F

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mRightAxisMax:F

    .line 249
    invoke-virtual {v3}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->getYMin()F

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mRightAxisMin:F

    .line 251
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mDataSets:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_ef
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_125

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;

    .line 252
    .local v5, "dataSet":Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;, "Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet<*>;"
    invoke-virtual {v5}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->getAxisDependency()Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$AxisDependency;

    move-result-object v0

    sget-object v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$AxisDependency;->RIGHT:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$AxisDependency;

    if-ne v0, v1, :cond_124

    .line 253
    invoke-virtual {v5}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->getYMin()F

    move-result v0

    iget v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mRightAxisMin:F

    cmpg-float v0, v0, v1

    if-gez v0, :cond_114

    .line 254
    invoke-virtual {v5}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->getYMin()F

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mRightAxisMin:F

    .line 256
    :cond_114
    invoke-virtual {v5}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->getYMax()F

    move-result v0

    iget v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mRightAxisMax:F

    cmpl-float v0, v0, v1

    if-lez v0, :cond_124

    .line 257
    invoke-virtual {v5}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->getYMax()F

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mRightAxisMax:F

    .line 259
    .end local v5    # "dataSet":Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;, "Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet<*>;"
    .end local v5
    :cond_124
    goto :goto_ef

    .line 263
    :cond_125
    invoke-direct {p0, v2, v3}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->handleEmptyAxis(Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;)V

    .line 265
    .end local v2    # "firstLeft":Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;, "TT;"
    .end local v2
    .end local v3    # "firstRight":Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;, "TT;"
    .end local v3
    :goto_128
    return-void
.end method

.method protected calcYValueCount()V
    .registers 4

    .line 290
    const/4 v0, 0x0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mYValCount:I

    .line 292
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mDataSets:Ljava/util/List;

    if-nez v0, :cond_8

    .line 293
    return-void

    .line 295
    :cond_8
    const/4 v1, 0x0

    .line 297
    .local v1, "count":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_a
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mDataSets:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_22

    .line 298
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mDataSets:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->getEntryCount()I

    move-result v0

    add-int/2addr v1, v0

    .line 297
    add-int/lit8 v2, v2, 0x1

    goto :goto_a

    .line 301
    .end local v2    # "i":I
    :cond_22
    iput v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mYValCount:I

    .line 302
    return-void
.end method

.method protected calcYValueSum()V
    .registers 4

    .line 272
    const/4 v0, 0x0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mYValueSum:F

    .line 274
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mDataSets:Ljava/util/List;

    if-nez v0, :cond_8

    .line 275
    return-void

    .line 277
    :cond_8
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_9
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mDataSets:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_29

    .line 278
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mYValueSum:F

    iget-object v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mDataSets:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;

    invoke-virtual {v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->getYValueSum()F

    move-result v1

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    add-float/2addr v0, v1

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mYValueSum:F

    .line 277
    add-int/lit8 v2, v2, 0x1

    goto :goto_9

    .line 280
    .end local v2    # "i":I
    :cond_29
    return-void
.end method

.method public clearValues()V
    .registers 2

    .line 950
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mDataSets:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 951
    invoke-virtual {p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->notifyDataChanged()V

    .line 952
    return-void
.end method

.method public contains(Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;)Z
    .registers 5
    .param p1, "dataSet"    # Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)Z"
        }
    .end annotation

    .line 981
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mDataSets:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1c

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;

    .line 982
    .local v2, "set":Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;, "TT;"
    invoke-virtual {v2, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1b

    .line 983
    const/4 v0, 0x1

    return v0

    .line 984
    .end local v2    # "set":Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;, "TT;"
    .end local v2
    :cond_1b
    goto :goto_6

    .line 986
    :cond_1c
    const/4 v0, 0x0

    return v0
.end method

.method public contains(Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;)Z
    .registers 5
    .param p1, "e"    # Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;

    .line 964
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mDataSets:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1c

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;

    .line 965
    .local v2, "set":Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;, "TT;"
    invoke-virtual {v2, p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->contains(Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;)Z

    move-result v0

    if-eqz v0, :cond_1b

    .line 966
    const/4 v0, 0x1

    return v0

    .line 967
    .end local v2    # "set":Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;, "TT;"
    .end local v2
    :cond_1b
    goto :goto_6

    .line 969
    :cond_1c
    const/4 v0, 0x0

    return v0
.end method

.method public getColors()[I
    .registers 9

    .line 781
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mDataSets:Ljava/util/List;

    if-nez v0, :cond_6

    .line 782
    const/4 v0, 0x0

    return-object v0

    .line 784
    :cond_6
    const/4 v1, 0x0

    .line 786
    .local v1, "clrcnt":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_8
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mDataSets:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_24

    .line 787
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mDataSets:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->getColors()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/2addr v1, v0

    .line 786
    add-int/lit8 v2, v2, 0x1

    goto :goto_8

    .line 790
    .end local v2    # "i":I
    :cond_24
    new-array v2, v1, [I

    .line 791
    .local v2, "colors":[I
    const/4 v3, 0x0

    .line 793
    .local v3, "cnt":I
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_28
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mDataSets:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v4, v0, :cond_59

    .line 795
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mDataSets:Ljava/util/List;

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->getColors()Ljava/util/List;

    move-result-object v5

    .line 797
    .local v5, "clrs":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_40
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_56

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Ljava/lang/Integer;

    .line 798
    .local v7, "clr":Ljava/lang/Integer;
    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v0

    aput v0, v2, v3

    .line 799
    add-int/lit8 v3, v3, 0x1

    .line 800
    .end local v7    # "clr":Ljava/lang/Integer;
    goto :goto_40

    .line 793
    .end local v5    # "clrs":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    .end local v5
    :cond_56
    add-int/lit8 v4, v4, 0x1

    goto :goto_28

    .line 803
    .end local v4    # "i":I
    :cond_59
    return-object v2
.end method

.method public getDataSetByIndex(I)Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;
    .registers 3
    .param p1, "index"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TT;"
        }
    .end annotation

    .line 520
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mDataSets:Ljava/util/List;

    if-eqz v0, :cond_e

    if-ltz p1, :cond_e

    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mDataSets:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lt p1, v0, :cond_10

    .line 521
    :cond_e
    const/4 v0, 0x0

    return-object v0

    .line 523
    :cond_10
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mDataSets:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;

    return-object v0
.end method

.method public getDataSetByLabel(Ljava/lang/String;Z)Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;
    .registers 5
    .param p1, "label"    # Ljava/lang/String;
    .param p2, "ignorecase"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/lang/String;Z)TT;"
        }
    .end annotation

    .line 504
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mDataSets:Ljava/util/List;

    invoke-virtual {p0, v0, p1, p2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->getDataSetIndexByLabel(Ljava/util/List;Ljava/lang/String;Z)I

    move-result v1

    .line 506
    .local v1, "index":I
    if-ltz v1, :cond_10

    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mDataSets:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lt v1, v0, :cond_12

    .line 507
    :cond_10
    const/4 v0, 0x0

    return-object v0

    .line 509
    :cond_12
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mDataSets:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;

    return-object v0
.end method

.method public getDataSetCount()I
    .registers 2

    .line 312
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mDataSets:Ljava/util/List;

    if-nez v0, :cond_6

    .line 313
    const/4 v0, 0x0

    return v0

    .line 314
    :cond_6
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mDataSets:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getDataSetForEntry(Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;)Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;
    .registers 6
    .param p1, "e"    # Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;)TT;"
        }
    .end annotation

    .line 757
    if-nez p1, :cond_4

    .line 758
    const/4 v0, 0x0

    return-object v0

    .line 760
    :cond_4
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_5
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mDataSets:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_32

    .line 762
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mDataSets:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;

    .line 764
    .local v2, "set":Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;, "TT;"
    const/4 v3, 0x0

    .local v3, "j":I
    :goto_17
    invoke-virtual {v2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->getEntryCount()I

    move-result v0

    if-ge v3, v0, :cond_2f

    .line 765
    invoke-virtual {p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;->getXIndex()I

    move-result v0

    invoke-virtual {v2, v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->getEntryForXIndex(I)Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;

    move-result-object v0

    invoke-virtual {p1, v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;->equalTo(Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;)Z

    move-result v0

    if-eqz v0, :cond_2c

    .line 766
    return-object v2

    .line 764
    :cond_2c
    add-int/lit8 v3, v3, 0x1

    goto :goto_17

    .line 760
    .end local v2    # "set":Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;, "TT;"
    .end local v2
    .end local v3    # "j":I
    :cond_2f
    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    .line 770
    .end local v1    # "i":I
    :cond_32
    const/4 v0, 0x0

    return-object v0
.end method

.method protected getDataSetIndexByLabel(Ljava/util/List;Ljava/lang/String;Z)I
    .registers 6
    .param p1, "dataSets"    # Ljava/util/List;
    .param p2, "label"    # Ljava/lang/String;
    .param p3, "ignorecase"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<TT;>;Ljava/lang/String;Z)I"
        }
    .end annotation

    .line 443
    if-eqz p3, :cond_1e

    .line 444
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_3
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1d

    .line 445
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->getLabel()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 446
    return v1

    .line 444
    :cond_1a
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .end local v1    # "i":I
    :cond_1d
    goto :goto_39

    .line 448
    :cond_1e
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1f
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_39

    .line 449
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->getLabel()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_36

    .line 450
    return v1

    .line 448
    :cond_36
    add-int/lit8 v1, v1, 0x1

    goto :goto_1f

    .line 453
    .end local v1    # "i":I
    :cond_39
    :goto_39
    const/4 v0, -0x1

    return v0
.end method

.method protected getDataSetLabels()[Ljava/lang/String;
    .registers 4

    .line 473
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mDataSets:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    new-array v1, v0, [Ljava/lang/String;

    .line 475
    .local v1, "types":[Ljava/lang/String;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_9
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mDataSets:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_22

    .line 476
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mDataSets:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->getLabel()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v1, v2

    .line 475
    add-int/lit8 v2, v2, 0x1

    goto :goto_9

    .line 479
    .end local v2    # "i":I
    :cond_22
    return-object v1
.end method

.method public getDataSets()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<TT;>;"
        }
    .end annotation

    .line 426
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mDataSets:Ljava/util/List;

    return-object v0
.end method

.method public getEntryForHighlight(Lbr/com/itau/widgets/graphicalfriedcake/charts/highlight/Highlight;)Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;
    .registers 4
    .param p1, "highlight"    # Lbr/com/itau/widgets/graphicalfriedcake/charts/highlight/Highlight;

    .line 489
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mDataSets:Ljava/util/List;

    invoke-virtual {p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/highlight/Highlight;->getDataSetIndex()I

    move-result v1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;

    .line 490
    invoke-virtual {p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/highlight/Highlight;->getXIndex()I

    move-result v1

    .line 489
    invoke-virtual {v0, v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->getEntryForXIndex(I)Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;

    move-result-object v0

    return-object v0
.end method

.method public getFirstLeft()Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;
    .registers 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .line 824
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mDataSets:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_6
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1d

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;

    .line 825
    .local v3, "dataSet":Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;, "TT;"
    invoke-virtual {v3}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->getAxisDependency()Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$AxisDependency;

    move-result-object v0

    sget-object v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$AxisDependency;->LEFT:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$AxisDependency;

    if-ne v0, v1, :cond_1c

    .line 826
    return-object v3

    .line 827
    .end local v3    # "dataSet":Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;, "TT;"
    .end local v3
    :cond_1c
    goto :goto_6

    .line 829
    :cond_1d
    const/4 v0, 0x0

    return-object v0
.end method

.method public getFirstRight()Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;
    .registers 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .line 833
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mDataSets:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_6
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1d

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;

    .line 834
    .local v3, "dataSet":Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;, "TT;"
    invoke-virtual {v3}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->getAxisDependency()Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$AxisDependency;

    move-result-object v0

    sget-object v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$AxisDependency;->RIGHT:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$AxisDependency;

    if-ne v0, v1, :cond_1c

    .line 835
    return-object v3

    .line 836
    .end local v3    # "dataSet":Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;, "TT;"
    .end local v3
    :cond_1c
    goto :goto_6

    .line 838
    :cond_1d
    const/4 v0, 0x0

    return-object v0
.end method

.method public getIndexOfDataSet(Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;)I
    .registers 4
    .param p1, "dataSet"    # Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)I"
        }
    .end annotation

    .line 815
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mDataSets:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_15

    .line 816
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mDataSets:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-ne v0, p1, :cond_12

    .line 817
    return v1

    .line 815
    :cond_12
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 820
    .end local v1    # "i":I
    :cond_15
    const/4 v0, -0x1

    return v0
.end method

.method public getXValAverageLength()F
    .registers 2

    .line 368
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mXValAverageLength:F

    return v0
.end method

.method public getXValCount()I
    .registers 2

    .line 463
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mXVals:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getXVals()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Ljava/lang/String;>;"
        }
    .end annotation

    .line 397
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mXVals:Ljava/util/List;

    return-object v0
.end method

.method public getYMax()F
    .registers 2

    .line 345
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mYMax:F

    return v0
.end method

.method public getYMax(Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$AxisDependency;)F
    .registers 3
    .param p1, "axis"    # Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$AxisDependency;

    .line 355
    sget-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$AxisDependency;->LEFT:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$AxisDependency;

    if-ne p1, v0, :cond_7

    .line 356
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mLeftAxisMax:F

    return v0

    .line 358
    :cond_7
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mRightAxisMax:F

    return v0
.end method

.method public getYMin()F
    .registers 2

    .line 323
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mYMin:F

    return v0
.end method

.method public getYMin(Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$AxisDependency;)F
    .registers 3
    .param p1, "axis"    # Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$AxisDependency;

    .line 333
    sget-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$AxisDependency;->LEFT:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$AxisDependency;

    if-ne p1, v0, :cond_7

    .line 334
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mLeftAxisMin:F

    return v0

    .line 336
    :cond_7
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mRightAxisMin:F

    return v0
.end method

.method public getYValCount()I
    .registers 2

    .line 388
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mYValCount:I

    return v0
.end method

.method public getYValueSum()F
    .registers 2

    .line 378
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mYValueSum:F

    return v0
.end method

.method protected init()V
    .registers 3

    .line 136
    invoke-direct {p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->isLegal()V

    .line 138
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mLastStart:I

    iget v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mLastEnd:I

    invoke-virtual {p0, v0, v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->calcMinMax(II)V

    .line 139
    invoke-virtual {p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->calcYValueSum()V

    .line 140
    invoke-virtual {p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->calcYValueCount()V

    .line 142
    invoke-direct {p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->calcXValAverageLength()V

    .line 143
    return-void
.end method

.method public isHighlightEnabled()Z
    .registers 4

    .line 938
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mDataSets:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1c

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;

    .line 939
    .local v2, "set":Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;, "Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet<*>;"
    invoke-virtual {v2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->isHighlightEnabled()Z

    move-result v0

    if-nez v0, :cond_1b

    .line 940
    const/4 v0, 0x0

    return v0

    .line 941
    .end local v2    # "set":Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;, "Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet<*>;"
    .end local v2
    :cond_1b
    goto :goto_6

    .line 942
    :cond_1c
    const/4 v0, 0x1

    return v0
.end method

.method public notifyDataChanged()V
    .registers 1

    .line 188
    invoke-virtual {p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->init()V

    .line 189
    return-void
.end method

.method public removeDataSet(I)Z
    .registers 4
    .param p1, "index"    # I

    .line 631
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mDataSets:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge p1, v0, :cond_a

    if-gez p1, :cond_c

    .line 632
    :cond_a
    const/4 v0, 0x0

    return v0

    .line 634
    :cond_c
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mDataSets:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;

    .line 635
    .local v1, "set":Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;, "TT;"
    invoke-virtual {p0, v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->removeDataSet(Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;)Z

    move-result v0

    return v0
.end method

.method public removeDataSet(Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;)Z
    .registers 5
    .param p1, "d"    # Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)Z"
        }
    .end annotation

    .line 605
    if-nez p1, :cond_4

    .line 606
    const/4 v0, 0x0

    return v0

    .line 608
    :cond_4
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mDataSets:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    move-result v2

    .line 611
    .local v2, "removed":Z
    if-eqz v2, :cond_25

    .line 613
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mYValCount:I

    invoke-virtual {p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->getEntryCount()I

    move-result v1

    sub-int/2addr v0, v1

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mYValCount:I

    .line 614
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mYValueSum:F

    invoke-virtual {p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->getYValueSum()F

    move-result v1

    sub-float/2addr v0, v1

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mYValueSum:F

    .line 616
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mLastStart:I

    iget v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mLastEnd:I

    invoke-virtual {p0, v0, v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->calcMinMax(II)V

    .line 619
    :cond_25
    return v2
.end method

.method public removeEntry(II)Z
    .registers 6
    .param p1, "xIndex"    # I
    .param p2, "dataSetIndex"    # I

    .line 736
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mDataSets:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lt p2, v0, :cond_a

    .line 737
    const/4 v0, 0x0

    return v0

    .line 739
    :cond_a
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mDataSets:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;

    .line 740
    .local v1, "dataSet":Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;, "TT;"
    invoke-virtual {v1, p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->getEntryForXIndex(I)Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;

    move-result-object v2

    .line 742
    .local v2, "e":Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;
    if-eqz v2, :cond_1f

    invoke-virtual {v2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;->getXIndex()I

    move-result v0

    if-eq v0, p1, :cond_21

    .line 743
    :cond_1f
    const/4 v0, 0x0

    return v0

    .line 745
    :cond_21
    invoke-virtual {p0, v2, p2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->removeEntry(Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;I)Z

    move-result v0

    return v0
.end method

.method public removeEntry(Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;I)Z
    .registers 7
    .param p1, "e"    # Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;
    .param p2, "dataSetIndex"    # I

    .line 706
    if-eqz p1, :cond_a

    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mDataSets:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lt p2, v0, :cond_c

    .line 707
    :cond_a
    const/4 v0, 0x0

    return v0

    .line 710
    :cond_c
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mDataSets:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;

    invoke-virtual {p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;->getXIndex()I

    move-result v1

    invoke-virtual {v0, v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->removeEntry(I)Z

    move-result v2

    .line 712
    .local v2, "removed":Z
    if-eqz v2, :cond_34

    .line 714
    invoke-virtual {p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;->getVal()F

    move-result v3

    .line 716
    .local v3, "val":F
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mYValCount:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mYValCount:I

    .line 717
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mYValueSum:F

    sub-float/2addr v0, v3

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mYValueSum:F

    .line 719
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mLastStart:I

    iget v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mLastEnd:I

    invoke-virtual {p0, v0, v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->calcMinMax(II)V

    .line 722
    .end local v3    # "val":F
    :cond_34
    return v2
.end method

.method public removeXValue(I)V
    .registers 3
    .param p1, "index"    # I

    .line 417
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mXVals:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 418
    return-void
.end method

.method public setDrawValues(Z)V
    .registers 5
    .param p1, "enabled"    # Z

    .line 916
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mDataSets:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_17

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;

    .line 917
    .local v2, "set":Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;, "Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet<*>;"
    invoke-virtual {v2, p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->setDrawValues(Z)V

    .line 918
    .end local v2    # "set":Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;, "Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet<*>;"
    .end local v2
    goto :goto_6

    .line 919
    :cond_17
    return-void
.end method

.method public setHighlightEnabled(Z)V
    .registers 5
    .param p1, "enabled"    # Z

    .line 926
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mDataSets:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_17

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;

    .line 927
    .local v2, "set":Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;, "Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet<*>;"
    invoke-virtual {v2, p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->setHighlightEnabled(Z)V

    .line 928
    .end local v2    # "set":Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;, "Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet<*>;"
    .end local v2
    goto :goto_6

    .line 929
    :cond_17
    return-void
.end method

.method public setValueFormatter(Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ValueFormatter;)V
    .registers 5
    .param p1, "f"    # Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ValueFormatter;

    .line 864
    if-nez p1, :cond_3

    .line 865
    return-void

    .line 867
    :cond_3
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mDataSets:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_9
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1a

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;

    .line 868
    .local v2, "set":Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;, "Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet<*>;"
    invoke-virtual {v2, p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->setValueFormatter(Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ValueFormatter;)V

    .line 869
    .end local v2    # "set":Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;, "Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet<*>;"
    .end local v2
    goto :goto_9

    .line 871
    :cond_1a
    return-void
.end method

.method public setValueTextColor(I)V
    .registers 5
    .param p1, "color"    # I

    .line 880
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mDataSets:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_17

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;

    .line 881
    .local v2, "set":Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;, "Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet<*>;"
    invoke-virtual {v2, p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->setValueTextColor(I)V

    .line 882
    .end local v2    # "set":Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;, "Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet<*>;"
    .end local v2
    goto :goto_6

    .line 883
    :cond_17
    return-void
.end method

.method public setValueTextSize(F)V
    .registers 5
    .param p1, "size"    # F

    .line 904
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mDataSets:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_17

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;

    .line 905
    .local v2, "set":Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;, "Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet<*>;"
    invoke-virtual {v2, p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->setValueTextSize(F)V

    .line 906
    .end local v2    # "set":Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;, "Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet<*>;"
    .end local v2
    goto :goto_6

    .line 907
    :cond_17
    return-void
.end method

.method public setValueTypeface(Landroid/graphics/Typeface;)V
    .registers 5
    .param p1, "tf"    # Landroid/graphics/Typeface;

    .line 892
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->mDataSets:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_17

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;

    .line 893
    .local v2, "set":Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;, "Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet<*>;"
    invoke-virtual {v2, p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->setValueTypeface(Landroid/graphics/Typeface;)V

    .line 894
    .end local v2    # "set":Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;, "Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet<*>;"
    .end local v2
    goto :goto_6

    .line 895
    :cond_17
    return-void
.end method

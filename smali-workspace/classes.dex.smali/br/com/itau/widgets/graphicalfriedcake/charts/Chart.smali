.class public abstract Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;
.super Landroid/view/ViewGroup;
.source "Chart.java"

# interfaces
.implements Lbr/com/itau/widgets/graphicalfriedcake/charts/interfaces/ChartInterface;


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "NewApi"
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData<+Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet<+Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;>;>;>Landroid/view/ViewGroup;Lbr/com/itau/widgets/graphicalfriedcake/charts/interfaces/ChartInterface;"
    }
.end annotation


# static fields
.field public static final LOG_TAG:Ljava/lang/String; = "MPAndroidChart"

.field public static final PAINT_CENTER_TEXT:I = 0xe

.field public static final PAINT_DESCRIPTION:I = 0xb

.field public static final PAINT_GRID_BACKGROUND:I = 0x4

.field public static final PAINT_HOLE:I = 0xd

.field public static final PAINT_INFO:I = 0x7

.field public static final PAINT_LEGEND_LABEL:I = 0x12


# instance fields
.field protected mAnimator:Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/ChartAnimator;

.field protected mChartTouchListener:Lbr/com/itau/widgets/graphicalfriedcake/charts/listener/ChartTouchListener;

.field protected mData:Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field protected mDataNotSet:Z

.field protected mDefaultFormatter:Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ValueFormatter;

.field protected mDeltaX:F

.field protected mDescPaint:Landroid/graphics/Paint;

.field protected mDescription:Ljava/lang/String;

.field private mDescriptionPosition:Landroid/graphics/PointF;

.field private mDragDecelerationEnabled:Z

.field private mDragDecelerationFrictionCoef:F

.field protected mDrawBitmap:Landroid/graphics/Bitmap;

.field protected mDrawMarkerViews:Z

.field protected mDrawPaint:Landroid/graphics/Paint;

.field protected mDrawUnitInChart:Z

.field private mExtraBottomOffset:F

.field private mExtraLeftOffset:F

.field private mExtraRightOffset:F

.field private mExtraTopOffset:F

.field private mGestureListener:Lbr/com/itau/widgets/graphicalfriedcake/charts/listener/OnChartGestureListener;

.field protected mHighlighter:Lbr/com/itau/widgets/graphicalfriedcake/charts/highlight/ChartHighlighter;

.field protected mIndicesToHightlight:[Lbr/com/itau/widgets/graphicalfriedcake/charts/highlight/Highlight;

.field protected mInfoPaint:Landroid/graphics/Paint;

.field protected mJobs:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<Ljava/lang/Runnable;>;"
        }
    .end annotation
.end field

.field protected mLegend:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;

.field protected mLegendRenderer:Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/LegendRenderer;

.field protected mLogEnabled:Z

.field protected mMarkerView:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/MarkerView;

.field private mNoDataText:Ljava/lang/String;

.field private mNoDataTextDescription:Ljava/lang/String;

.field private mOffsetsCalculated:Z

.field protected mRenderer:Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/DataRenderer;

.field protected mSelectionListener:Lbr/com/itau/widgets/graphicalfriedcake/charts/listener/OnChartValueSelectedListener;

.field protected mTouchEnabled:Z

.field protected mViewPortHandler:Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;

.field protected mXChartMax:F

.field protected mXChartMin:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .param p1, "context"    # Landroid/content/Context;

    .line 164
    invoke-direct {p0, p1}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    .line 69
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mLogEnabled:Z

    .line 75
    const/4 v0, 0x0

    iput-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mData:Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;

    .line 78
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mDragDecelerationEnabled:Z

    .line 86
    const v0, 0x3f666666    # 0.9f

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mDragDecelerationFrictionCoef:F

    .line 104
    const-string v0, "Description"

    iput-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mDescription:Ljava/lang/String;

    .line 107
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mDataNotSet:Z

    .line 110
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mDrawUnitInChart:Z

    .line 113
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mDeltaX:F

    .line 115
    const/4 v0, 0x0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mXChartMin:F

    .line 116
    const/4 v0, 0x0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mXChartMax:F

    .line 119
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mTouchEnabled:Z

    .line 130
    const-string v0, "No chart data available."

    iput-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mNoDataText:Ljava/lang/String;

    .line 157
    const/4 v0, 0x0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mExtraTopOffset:F

    const/4 v0, 0x0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mExtraRightOffset:F

    const/4 v0, 0x0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mExtraBottomOffset:F

    const/4 v0, 0x0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mExtraLeftOffset:F

    .line 371
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mOffsetsCalculated:Z

    .line 448
    const/4 v0, 0x0

    new-array v0, v0, [Lbr/com/itau/widgets/graphicalfriedcake/charts/highlight/Highlight;

    iput-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mIndicesToHightlight:[Lbr/com/itau/widgets/graphicalfriedcake/charts/highlight/Highlight;

    .line 574
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mDrawMarkerViews:Z

    .line 1540
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mJobs:Ljava/util/ArrayList;

    .line 165
    invoke-virtual {p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->init()V

    .line 166
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .line 170
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 69
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mLogEnabled:Z

    .line 75
    const/4 v0, 0x0

    iput-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mData:Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;

    .line 78
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mDragDecelerationEnabled:Z

    .line 86
    const v0, 0x3f666666    # 0.9f

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mDragDecelerationFrictionCoef:F

    .line 104
    const-string v0, "Description"

    iput-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mDescription:Ljava/lang/String;

    .line 107
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mDataNotSet:Z

    .line 110
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mDrawUnitInChart:Z

    .line 113
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mDeltaX:F

    .line 115
    const/4 v0, 0x0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mXChartMin:F

    .line 116
    const/4 v0, 0x0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mXChartMax:F

    .line 119
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mTouchEnabled:Z

    .line 130
    const-string v0, "No chart data available."

    iput-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mNoDataText:Ljava/lang/String;

    .line 157
    const/4 v0, 0x0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mExtraTopOffset:F

    const/4 v0, 0x0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mExtraRightOffset:F

    const/4 v0, 0x0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mExtraBottomOffset:F

    const/4 v0, 0x0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mExtraLeftOffset:F

    .line 371
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mOffsetsCalculated:Z

    .line 448
    const/4 v0, 0x0

    new-array v0, v0, [Lbr/com/itau/widgets/graphicalfriedcake/charts/highlight/Highlight;

    iput-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mIndicesToHightlight:[Lbr/com/itau/widgets/graphicalfriedcake/charts/highlight/Highlight;

    .line 574
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mDrawMarkerViews:Z

    .line 1540
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mJobs:Ljava/util/ArrayList;

    .line 171
    invoke-virtual {p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->init()V

    .line 172
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .line 176
    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 69
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mLogEnabled:Z

    .line 75
    const/4 v0, 0x0

    iput-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mData:Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;

    .line 78
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mDragDecelerationEnabled:Z

    .line 86
    const v0, 0x3f666666    # 0.9f

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mDragDecelerationFrictionCoef:F

    .line 104
    const-string v0, "Description"

    iput-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mDescription:Ljava/lang/String;

    .line 107
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mDataNotSet:Z

    .line 110
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mDrawUnitInChart:Z

    .line 113
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mDeltaX:F

    .line 115
    const/4 v0, 0x0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mXChartMin:F

    .line 116
    const/4 v0, 0x0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mXChartMax:F

    .line 119
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mTouchEnabled:Z

    .line 130
    const-string v0, "No chart data available."

    iput-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mNoDataText:Ljava/lang/String;

    .line 157
    const/4 v0, 0x0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mExtraTopOffset:F

    const/4 v0, 0x0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mExtraRightOffset:F

    const/4 v0, 0x0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mExtraBottomOffset:F

    const/4 v0, 0x0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mExtraLeftOffset:F

    .line 371
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mOffsetsCalculated:Z

    .line 448
    const/4 v0, 0x0

    new-array v0, v0, [Lbr/com/itau/widgets/graphicalfriedcake/charts/highlight/Highlight;

    iput-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mIndicesToHightlight:[Lbr/com/itau/widgets/graphicalfriedcake/charts/highlight/Highlight;

    .line 574
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mDrawMarkerViews:Z

    .line 1540
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mJobs:Ljava/util/ArrayList;

    .line 177
    invoke-virtual {p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->init()V

    .line 178
    return-void
.end method


# virtual methods
.method public addJob(Ljava/lang/Runnable;)V
    .registers 3
    .param p1, "job"    # Ljava/lang/Runnable;

    .line 1549
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mJobs:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1550
    return-void
.end method

.method public animateX(I)V
    .registers 3
    .param p1, "durationMillis"    # I

    .line 813
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mAnimator:Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/ChartAnimator;

    invoke-virtual {v0, p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/ChartAnimator;->animateX(I)V

    .line 814
    return-void
.end method

.method public animateX(ILbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;)V
    .registers 4
    .param p1, "durationMillis"    # I
    .param p2, "easing"    # Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;

    .line 782
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mAnimator:Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/ChartAnimator;

    invoke-virtual {v0, p1, p2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/ChartAnimator;->animateX(ILbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;)V

    .line 783
    return-void
.end method

.method public animateX(ILbr/com/itau/widgets/graphicalfriedcake/charts/animation/EasingFunction;)V
    .registers 4
    .param p1, "durationMillis"    # I
    .param p2, "easing"    # Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/EasingFunction;

    .line 734
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mAnimator:Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/ChartAnimator;

    invoke-virtual {v0, p1, p2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/ChartAnimator;->animateX(ILbr/com/itau/widgets/graphicalfriedcake/charts/animation/EasingFunction;)V

    .line 735
    return-void
.end method

.method public animateXY(II)V
    .registers 4
    .param p1, "durationMillisX"    # I
    .param p2, "durationMillisY"    # I

    .line 838
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mAnimator:Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/ChartAnimator;

    invoke-virtual {v0, p1, p2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/ChartAnimator;->animateXY(II)V

    .line 839
    return-void
.end method

.method public animateXY(IILbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;)V
    .registers 6
    .param p1, "durationMillisX"    # I
    .param p2, "durationMillisY"    # I
    .param p3, "easingX"    # Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;
    .param p4, "easingY"    # Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;

    .line 769
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mAnimator:Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/ChartAnimator;

    invoke-virtual {v0, p1, p2, p3, p4}, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/ChartAnimator;->animateXY(IILbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;)V

    .line 770
    return-void
.end method

.method public animateXY(IILbr/com/itau/widgets/graphicalfriedcake/charts/animation/EasingFunction;Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/EasingFunction;)V
    .registers 6
    .param p1, "durationMillisX"    # I
    .param p2, "durationMillisY"    # I
    .param p3, "easingX"    # Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/EasingFunction;
    .param p4, "easingY"    # Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/EasingFunction;

    .line 721
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mAnimator:Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/ChartAnimator;

    invoke-virtual {v0, p1, p2, p3, p4}, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/ChartAnimator;->animateXY(IILbr/com/itau/widgets/graphicalfriedcake/charts/animation/EasingFunction;Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/EasingFunction;)V

    .line 722
    return-void
.end method

.method public animateY(I)V
    .registers 3
    .param p1, "durationMillis"    # I

    .line 825
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mAnimator:Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/ChartAnimator;

    invoke-virtual {v0, p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/ChartAnimator;->animateY(I)V

    .line 826
    return-void
.end method

.method public animateY(ILbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;)V
    .registers 4
    .param p1, "durationMillis"    # I
    .param p2, "easing"    # Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;

    .line 795
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mAnimator:Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/ChartAnimator;

    invoke-virtual {v0, p1, p2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/ChartAnimator;->animateY(ILbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;)V

    .line 796
    return-void
.end method

.method public animateY(ILbr/com/itau/widgets/graphicalfriedcake/charts/animation/EasingFunction;)V
    .registers 4
    .param p1, "durationMillis"    # I
    .param p2, "easing"    # Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/EasingFunction;

    .line 747
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mAnimator:Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/ChartAnimator;

    invoke-virtual {v0, p1, p2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/ChartAnimator;->animateY(ILbr/com/itau/widgets/graphicalfriedcake/charts/animation/EasingFunction;)V

    .line 748
    return-void
.end method

.method protected abstract calcMinMax()V
.end method

.method protected calculateFormatter(FF)V
    .registers 7
    .param p1, "min"    # F
    .param p2, "max"    # F

    .line 357
    const/4 v2, 0x0

    .line 359
    .local v2, "reference":F
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mData:Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;

    if-eqz v0, :cond_e

    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mData:Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->getXValCount()I

    move-result v0

    const/4 v1, 0x2

    if-ge v0, v1, :cond_1b

    .line 361
    :cond_e
    invoke-static {p1}, Ljava/lang/Math;->abs(F)F

    move-result v0

    invoke-static {p2}, Ljava/lang/Math;->abs(F)F

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v2

    goto :goto_21

    .line 363
    :cond_1b
    sub-float v0, p2, p1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v2

    .line 366
    :goto_21
    invoke-static {v2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Utils;->getDecimals(F)I

    move-result v3

    .line 367
    .local v3, "digits":I
    new-instance v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/DefaultValueFormatter;

    invoke-direct {v0, v3}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/DefaultValueFormatter;-><init>(I)V

    iput-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mDefaultFormatter:Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ValueFormatter;

    .line 368
    return-void
.end method

.method protected abstract calculateOffsets()V
.end method

.method public clear()V
    .registers 2

    .line 300
    const/4 v0, 0x0

    iput-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mData:Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;

    .line 301
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mDataNotSet:Z

    .line 302
    invoke-virtual {p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->invalidate()V

    .line 303
    return-void
.end method

.method public clearAllJobs()V
    .registers 2

    .line 1557
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mJobs:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 1558
    return-void
.end method

.method public clearValues()V
    .registers 2

    .line 310
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mData:Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->clearValues()V

    .line 311
    invoke-virtual {p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->invalidate()V

    .line 312
    return-void
.end method

.method public disableScroll()V
    .registers 3

    .line 1230
    invoke-virtual {p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    .line 1231
    .local v1, "parent":Landroid/view/ViewParent;
    if-eqz v1, :cond_a

    .line 1232
    const/4 v0, 0x1

    invoke-interface {v1, v0}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    .line 1233
    :cond_a
    return-void
.end method

.method protected drawDescription(Landroid/graphics/Canvas;)V
    .registers 6
    .param p1, "c"    # Landroid/graphics/Canvas;

    .line 426
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mDescription:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_43

    .line 428
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mDescriptionPosition:Landroid/graphics/PointF;

    if-nez v0, :cond_34

    .line 430
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mDescription:Ljava/lang/String;

    invoke-virtual {p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->getWidth()I

    move-result v1

    int-to-float v1, v1

    iget-object v2, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mViewPortHandler:Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;

    invoke-virtual {v2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->offsetRight()F

    move-result v2

    sub-float/2addr v1, v2

    const/high16 v2, 0x41200000    # 10.0f

    sub-float/2addr v1, v2

    .line 431
    invoke-virtual {p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->getHeight()I

    move-result v2

    int-to-float v2, v2

    iget-object v3, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mViewPortHandler:Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;

    invoke-virtual {v3}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->offsetBottom()F

    move-result v3

    sub-float/2addr v2, v3

    const/high16 v3, 0x41200000    # 10.0f

    sub-float/2addr v2, v3

    iget-object v3, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mDescPaint:Landroid/graphics/Paint;

    .line 430
    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    goto :goto_43

    .line 434
    :cond_34
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mDescription:Ljava/lang/String;

    iget-object v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mDescriptionPosition:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->x:F

    iget-object v2, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mDescriptionPosition:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->y:F

    iget-object v3, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mDescPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 437
    :cond_43
    :goto_43
    return-void
.end method

.method protected drawMarkers(Landroid/graphics/Canvas;)V
    .registers 14
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .line 585
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mMarkerView:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/MarkerView;

    if-eqz v0, :cond_e

    iget-boolean v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mDrawMarkerViews:Z

    if-eqz v0, :cond_e

    invoke-virtual {p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->valuesToHighlight()Z

    move-result v0

    if-nez v0, :cond_f

    .line 586
    :cond_e
    return-void

    .line 588
    :cond_f
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_10
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mIndicesToHightlight:[Lbr/com/itau/widgets/graphicalfriedcake/charts/highlight/Highlight;

    array-length v0, v0

    if-ge v5, v0, :cond_c6

    .line 590
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mIndicesToHightlight:[Lbr/com/itau/widgets/graphicalfriedcake/charts/highlight/Highlight;

    aget-object v6, v0, v5

    .line 591
    .local v6, "highlight":Lbr/com/itau/widgets/graphicalfriedcake/charts/highlight/Highlight;
    invoke-virtual {v6}, Lbr/com/itau/widgets/graphicalfriedcake/charts/highlight/Highlight;->getXIndex()I

    move-result v7

    .line 592
    .local v7, "xIndex":I
    invoke-virtual {v6}, Lbr/com/itau/widgets/graphicalfriedcake/charts/highlight/Highlight;->getDataSetIndex()I

    move-result v8

    .line 594
    .local v8, "dataSetIndex":I
    int-to-float v0, v7

    iget v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mDeltaX:F

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_c2

    int-to-float v0, v7

    iget v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mDeltaX:F

    iget-object v2, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mAnimator:Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/ChartAnimator;

    invoke-virtual {v2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/ChartAnimator;->getPhaseX()F

    move-result v2

    mul-float/2addr v1, v2

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_c2

    .line 596
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mData:Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;

    iget-object v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mIndicesToHightlight:[Lbr/com/itau/widgets/graphicalfriedcake/charts/highlight/Highlight;

    aget-object v1, v1, v5

    invoke-virtual {v0, v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->getEntryForHighlight(Lbr/com/itau/widgets/graphicalfriedcake/charts/highlight/Highlight;)Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;

    move-result-object v9

    .line 599
    .local v9, "e":Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;
    if-eqz v9, :cond_c2

    invoke-virtual {v9}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;->getXIndex()I

    move-result v0

    iget-object v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mIndicesToHightlight:[Lbr/com/itau/widgets/graphicalfriedcake/charts/highlight/Highlight;

    aget-object v1, v1, v5

    invoke-virtual {v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/highlight/Highlight;->getXIndex()I

    move-result v1

    if-eq v0, v1, :cond_52

    .line 600
    goto/16 :goto_c2

    .line 602
    :cond_52
    invoke-virtual {p0, v9, v6}, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->getMarkerPosition(Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;Lbr/com/itau/widgets/graphicalfriedcake/charts/highlight/Highlight;)[F

    move-result-object v10

    .line 605
    .local v10, "pos":[F
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mViewPortHandler:Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;

    const/4 v1, 0x0

    aget v1, v10, v1

    const/4 v2, 0x1

    aget v2, v10, v2

    invoke-virtual {v0, v1, v2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->isInBounds(FF)Z

    move-result v0

    if-nez v0, :cond_65

    .line 606
    goto :goto_c2

    .line 609
    :cond_65
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mMarkerView:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/MarkerView;

    invoke-virtual {v0, v9, v6}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/MarkerView;->refreshContent(Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;Lbr/com/itau/widgets/graphicalfriedcake/charts/highlight/Highlight;)V

    .line 618
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mMarkerView:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/MarkerView;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-static {v1, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 619
    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-static {v2, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 618
    invoke-virtual {v0, v1, v2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/MarkerView;->measure(II)V

    .line 620
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mMarkerView:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/MarkerView;

    iget-object v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mMarkerView:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/MarkerView;

    invoke-virtual {v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/MarkerView;->getMeasuredWidth()I

    move-result v1

    iget-object v2, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mMarkerView:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/MarkerView;

    .line 621
    invoke-virtual {v2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/MarkerView;->getMeasuredHeight()I

    move-result v2

    .line 620
    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4, v1, v2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/MarkerView;->layout(IIII)V

    .line 623
    const/4 v0, 0x1

    aget v0, v10, v0

    iget-object v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mMarkerView:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/MarkerView;

    invoke-virtual {v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/MarkerView;->getHeight()I

    move-result v1

    int-to-float v1, v1

    sub-float/2addr v0, v1

    const/4 v1, 0x0

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_b7

    .line 624
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mMarkerView:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/MarkerView;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/MarkerView;->getHeight()I

    move-result v0

    int-to-float v0, v0

    const/4 v1, 0x1

    aget v1, v10, v1

    sub-float v11, v0, v1

    .line 625
    .local v11, "y":F
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mMarkerView:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/MarkerView;

    const/4 v1, 0x0

    aget v1, v10, v1

    const/4 v2, 0x1

    aget v2, v10, v2

    add-float/2addr v2, v11

    invoke-virtual {v0, p1, v1, v2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/MarkerView;->draw(Landroid/graphics/Canvas;FF)V

    .line 626
    .end local v11    # "y":F
    goto :goto_c2

    .line 627
    :cond_b7
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mMarkerView:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/MarkerView;

    const/4 v1, 0x0

    aget v1, v10, v1

    const/4 v2, 0x1

    aget v2, v10, v2

    invoke-virtual {v0, p1, v1, v2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/MarkerView;->draw(Landroid/graphics/Canvas;FF)V

    .line 588
    .end local v6    # "highlight":Lbr/com/itau/widgets/graphicalfriedcake/charts/highlight/Highlight;
    .end local v7    # "xIndex":I
    .end local v8    # "dataSetIndex":I
    .end local v9    # "e":Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;
    .end local v10    # "pos":[F
    :cond_c2
    :goto_c2
    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_10

    .line 631
    .end local v5    # "i":I
    :cond_c6
    return-void
.end method

.method public enableScroll()V
    .registers 3

    .line 1239
    invoke-virtual {p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    .line 1240
    .local v1, "parent":Landroid/view/ViewParent;
    if-eqz v1, :cond_a

    .line 1241
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    .line 1242
    :cond_a
    return-void
.end method

.method public getAnimator()Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/ChartAnimator;
    .registers 2

    .line 655
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mAnimator:Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/ChartAnimator;

    return-object v0
.end method

.method public getAverage()F
    .registers 3

    .line 953
    invoke-virtual {p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->getYValueSum()F

    move-result v0

    iget-object v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mData:Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;

    invoke-virtual {v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->getYValCount()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v0, v1

    return v0
.end method

.method public getAverage(Ljava/lang/String;)F
    .registers 5
    .param p1, "dataSetLabel"    # Ljava/lang/String;

    .line 965
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mData:Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->getDataSetByLabel(Ljava/lang/String;Z)Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;

    move-result-object v2

    .line 967
    .local v2, "ds":Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;, "Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet<+Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;>;"
    invoke-virtual {v2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->getYValueSum()F

    move-result v0

    .line 968
    invoke-virtual {v2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->getEntryCount()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v0, v1

    return v0
.end method

.method public getCenter()Landroid/graphics/PointF;
    .registers 5

    .line 986
    new-instance v0, Landroid/graphics/PointF;

    invoke-virtual {p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->getWidth()I

    move-result v1

    int-to-float v1, v1

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    invoke-virtual {p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->getHeight()I

    move-result v2

    int-to-float v2, v2

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v2, v3

    invoke-direct {v0, v1, v2}, Landroid/graphics/PointF;-><init>(FF)V

    return-object v0
.end method

.method public getCenterOfView()Landroid/graphics/PointF;
    .registers 2

    .line 1413
    invoke-virtual {p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->getCenter()Landroid/graphics/PointF;

    move-result-object v0

    return-object v0
.end method

.method public getCenterOffsets()Landroid/graphics/PointF;
    .registers 2

    .line 997
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mViewPortHandler:Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->getContentCenter()Landroid/graphics/PointF;

    move-result-object v0

    return-object v0
.end method

.method public getChartBitmap()Landroid/graphics/Bitmap;
    .registers 7

    .line 1423
    invoke-virtual {p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->getHeight()I

    move-result v1

    sget-object v2, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 1425
    .local v3, "returnedBitmap":Landroid/graphics/Bitmap;
    new-instance v4, Landroid/graphics/Canvas;

    invoke-direct {v4, v3}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 1427
    .local v4, "canvas":Landroid/graphics/Canvas;
    invoke-virtual {p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v5

    .line 1428
    .local v5, "bgDrawable":Landroid/graphics/drawable/Drawable;
    if-eqz v5, :cond_1d

    .line 1430
    invoke-virtual {v5, v4}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    goto :goto_21

    .line 1434
    :cond_1d
    const/4 v0, -0x1

    invoke-virtual {v4, v0}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 1436
    :goto_21
    invoke-virtual {p0, v4}, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->draw(Landroid/graphics/Canvas;)V

    .line 1438
    return-object v3
.end method

.method public getContentRect()Landroid/graphics/RectF;
    .registers 2

    .line 1223
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mViewPortHandler:Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->getContentRect()Landroid/graphics/RectF;

    move-result-object v0

    return-object v0
.end method

.method public getData()Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .line 1368
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mData:Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;

    return-object v0
.end method

.method public getDefaultValueFormatter()Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ValueFormatter;
    .registers 2

    .line 853
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mDefaultFormatter:Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ValueFormatter;

    return-object v0
.end method

.method public getDragDecelerationFrictionCoef()F
    .registers 2

    .line 680
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mDragDecelerationFrictionCoef:F

    return v0
.end method

.method public getEntriesAtIndex(I)Ljava/util/List;
    .registers 7
    .param p1, "xIndex"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)Ljava/util/List<Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;>;"
        }
    .end annotation

    .line 1346
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1348
    .local v1, "vals":Ljava/util/List;, "Ljava/util/List<Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;>;"
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_6
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mData:Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->getDataSetCount()I

    move-result v0

    if-ge v2, v0, :cond_20

    .line 1350
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mData:Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;

    invoke-virtual {v0, v2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->getDataSetByIndex(I)Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;

    move-result-object v3

    .line 1352
    .local v3, "set":Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;, "Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet<+Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;>;"
    invoke-virtual {v3, p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->getEntryForXIndex(I)Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;

    move-result-object v4

    .line 1354
    .local v4, "e":Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;
    if-eqz v4, :cond_1d

    .line 1355
    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1348
    .end local v3    # "set":Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;, "Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet<+Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;>;"
    .end local v3
    .end local v4    # "e":Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;
    :cond_1d
    add-int/lit8 v2, v2, 0x1

    goto :goto_6

    .line 1359
    .end local v2    # "i":I
    :cond_20
    return-object v1
.end method

.method public getExtraBottomOffset()F
    .registers 2

    .line 1110
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mExtraBottomOffset:F

    return v0
.end method

.method public getExtraLeftOffset()F
    .registers 2

    .line 1124
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mExtraLeftOffset:F

    return v0
.end method

.method public getExtraRightOffset()F
    .registers 2

    .line 1096
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mExtraRightOffset:F

    return v0
.end method

.method public getExtraTopOffset()F
    .registers 2

    .line 1082
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mExtraTopOffset:F

    return v0
.end method

.method public getHighlighted()[Lbr/com/itau/widgets/graphicalfriedcake/charts/highlight/Highlight;
    .registers 2

    .line 457
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mIndicesToHightlight:[Lbr/com/itau/widgets/graphicalfriedcake/charts/highlight/Highlight;

    return-object v0
.end method

.method public getJobs()Ljava/util/ArrayList;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/ArrayList<Ljava/lang/Runnable;>;"
        }
    .end annotation

    .line 1567
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mJobs:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getLegend()Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;
    .registers 2

    .line 1202
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mLegend:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;

    return-object v0
.end method

.method public getLegendRenderer()Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/LegendRenderer;
    .registers 2

    .line 1212
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mLegendRenderer:Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/LegendRenderer;

    return-object v0
.end method

.method protected abstract getMarkerPosition(Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;Lbr/com/itau/widgets/graphicalfriedcake/charts/highlight/Highlight;)[F
.end method

.method public getMarkerView()Lbr/com/itau/widgets/graphicalfriedcake/charts/components/MarkerView;
    .registers 2

    .line 1191
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mMarkerView:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/MarkerView;

    return-object v0
.end method

.method public getOnChartGestureListener()Lbr/com/itau/widgets/graphicalfriedcake/charts/listener/OnChartGestureListener;
    .registers 2

    .line 881
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mGestureListener:Lbr/com/itau/widgets/graphicalfriedcake/charts/listener/OnChartGestureListener;

    return-object v0
.end method

.method public getPaint(I)Landroid/graphics/Paint;
    .registers 3
    .param p1, "which"    # I

    .line 1292
    sparse-switch p1, :sswitch_data_c

    goto :goto_a

    .line 1294
    :sswitch_4
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mInfoPaint:Landroid/graphics/Paint;

    return-object v0

    .line 1296
    :sswitch_7
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mDescPaint:Landroid/graphics/Paint;

    return-object v0

    .line 1299
    :goto_a
    const/4 v0, 0x0

    return-object v0

    :sswitch_data_c
    .sparse-switch
        0x7 -> :sswitch_4
        0xb -> :sswitch_7
    .end sparse-switch
.end method

.method public getPercentOfTotal(F)F
    .registers 4
    .param p1, "val"    # F

    .line 1378
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mData:Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->getYValueSum()F

    move-result v0

    div-float v0, p1, v0

    const/high16 v1, 0x42c80000    # 100.0f

    mul-float/2addr v0, v1

    return v0
.end method

.method public getRenderer()Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/DataRenderer;
    .registers 2

    .line 1397
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mRenderer:Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/DataRenderer;

    return-object v0
.end method

.method public getValueCount()I
    .registers 2

    .line 977
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mData:Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->getYValCount()I

    move-result v0

    return v0
.end method

.method public getViewPortHandler()Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;
    .registers 2

    .line 1388
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mViewPortHandler:Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;

    return-object v0
.end method

.method public getXChartMax()F
    .registers 2

    .line 934
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mXChartMax:F

    return v0
.end method

.method public getXChartMin()F
    .registers 2

    .line 939
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mXChartMin:F

    return v0
.end method

.method public getXValCount()I
    .registers 2

    .line 944
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mData:Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->getXValCount()I

    move-result v0

    return v0
.end method

.method public getXValue(I)Ljava/lang/String;
    .registers 3
    .param p1, "index"    # I

    .line 1330
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mData:Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;

    if-eqz v0, :cond_c

    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mData:Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->getXValCount()I

    move-result v0

    if-gt v0, p1, :cond_e

    .line 1331
    :cond_c
    const/4 v0, 0x0

    return-object v0

    .line 1333
    :cond_e
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mData:Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->getXVals()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getYMax()F
    .registers 2

    .line 920
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mData:Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->getYMax()F

    move-result v0

    return v0
.end method

.method public getYMin()F
    .registers 2

    .line 929
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mData:Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->getYMin()F

    move-result v0

    return v0
.end method

.method public getYValueSum()F
    .registers 2

    .line 911
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mData:Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->getYValueSum()F

    move-result v0

    return v0
.end method

.method public highlightTouch(Lbr/com/itau/widgets/graphicalfriedcake/charts/highlight/Highlight;)V
    .registers 6
    .param p1, "high"    # Lbr/com/itau/widgets/graphicalfriedcake/charts/highlight/Highlight;

    .line 522
    const/4 v3, 0x0

    .line 524
    .local v3, "e":Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;
    if-nez p1, :cond_7

    .line 525
    const/4 v0, 0x0

    iput-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mIndicesToHightlight:[Lbr/com/itau/widgets/graphicalfriedcake/charts/highlight/Highlight;

    goto :goto_46

    .line 528
    :cond_7
    iget-boolean v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mLogEnabled:Z

    if-eqz v0, :cond_27

    .line 529
    const-string v0, "MPAndroidChart"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Highlighted: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/highlight/Highlight;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 531
    :cond_27
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mData:Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;

    invoke-virtual {v0, p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->getEntryForHighlight(Lbr/com/itau/widgets/graphicalfriedcake/charts/highlight/Highlight;)Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;

    move-result-object v3

    .line 532
    if-eqz v3, :cond_39

    invoke-virtual {v3}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;->getXIndex()I

    move-result v0

    invoke-virtual {p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/highlight/Highlight;->getXIndex()I

    move-result v1

    if-eq v0, v1, :cond_3e

    .line 533
    :cond_39
    const/4 v0, 0x0

    iput-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mIndicesToHightlight:[Lbr/com/itau/widgets/graphicalfriedcake/charts/highlight/Highlight;

    .line 534
    const/4 p1, 0x0

    goto :goto_46

    .line 538
    :cond_3e
    const/4 v0, 0x1

    new-array v0, v0, [Lbr/com/itau/widgets/graphicalfriedcake/charts/highlight/Highlight;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    iput-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mIndicesToHightlight:[Lbr/com/itau/widgets/graphicalfriedcake/charts/highlight/Highlight;

    .line 545
    :goto_46
    invoke-virtual {p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->invalidate()V

    .line 547
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mSelectionListener:Lbr/com/itau/widgets/graphicalfriedcake/charts/listener/OnChartValueSelectedListener;

    if-eqz v0, :cond_62

    .line 549
    invoke-virtual {p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->valuesToHighlight()Z

    move-result v0

    if-nez v0, :cond_59

    .line 550
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mSelectionListener:Lbr/com/itau/widgets/graphicalfriedcake/charts/listener/OnChartValueSelectedListener;

    invoke-interface {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/listener/OnChartValueSelectedListener;->onNothingSelected()V

    goto :goto_62

    .line 553
    :cond_59
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mSelectionListener:Lbr/com/itau/widgets/graphicalfriedcake/charts/listener/OnChartValueSelectedListener;

    invoke-virtual {p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/highlight/Highlight;->getDataSetIndex()I

    move-result v1

    invoke-interface {v0, v3, v1, p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/listener/OnChartValueSelectedListener;->onValueSelected(Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;ILbr/com/itau/widgets/graphicalfriedcake/charts/highlight/Highlight;)V

    .line 556
    :cond_62
    :goto_62
    return-void
.end method

.method public highlightValue(II)V
    .registers 6
    .param p1, "xIndex"    # I
    .param p2, "dataSetIndex"    # I

    .line 502
    if-ltz p1, :cond_14

    if-ltz p2, :cond_14

    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mData:Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->getXValCount()I

    move-result v0

    if-ge p1, v0, :cond_14

    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mData:Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;

    .line 503
    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->getDataSetCount()I

    move-result v0

    if-lt p2, v0, :cond_19

    .line 505
    :cond_14
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->highlightValues([Lbr/com/itau/widgets/graphicalfriedcake/charts/highlight/Highlight;)V

    goto :goto_27

    .line 507
    :cond_19
    const/4 v0, 0x1

    new-array v0, v0, [Lbr/com/itau/widgets/graphicalfriedcake/charts/highlight/Highlight;

    new-instance v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/highlight/Highlight;

    invoke-direct {v1, p1, p2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/highlight/Highlight;-><init>(II)V

    const/4 v2, 0x0

    aput-object v1, v0, v2

    invoke-virtual {p0, v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->highlightValues([Lbr/com/itau/widgets/graphicalfriedcake/charts/highlight/Highlight;)V

    .line 511
    :goto_27
    return-void
.end method

.method public highlightValues([Lbr/com/itau/widgets/graphicalfriedcake/charts/highlight/Highlight;)V
    .registers 4
    .param p1, "highs"    # [Lbr/com/itau/widgets/graphicalfriedcake/charts/highlight/Highlight;

    .line 484
    iput-object p1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mIndicesToHightlight:[Lbr/com/itau/widgets/graphicalfriedcake/charts/highlight/Highlight;

    .line 486
    if-eqz p1, :cond_7

    array-length v0, p1

    if-nez v0, :cond_d

    .line 487
    :cond_7
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mChartTouchListener:Lbr/com/itau/widgets/graphicalfriedcake/charts/listener/ChartTouchListener;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/listener/ChartTouchListener;->setLastHighlighted(Lbr/com/itau/widgets/graphicalfriedcake/charts/highlight/Highlight;)V

    .line 490
    :cond_d
    invoke-virtual {p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->invalidate()V

    .line 491
    return-void
.end method

.method protected init()V
    .registers 5

    .line 185
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->setWillNotDraw(Z)V

    .line 188
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-ge v0, v1, :cond_12

    .line 189
    new-instance v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/ChartAnimator;

    invoke-direct {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/ChartAnimator;-><init>()V

    iput-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mAnimator:Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/ChartAnimator;

    goto :goto_1e

    .line 191
    :cond_12
    new-instance v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/ChartAnimator;

    new-instance v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart$1;

    invoke-direct {v1, p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart$1;-><init>(Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;)V

    invoke-direct {v0, v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/ChartAnimator;-><init>(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    iput-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mAnimator:Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/ChartAnimator;

    .line 201
    :goto_1e
    invoke-virtual {p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Utils;->init(Landroid/content/Context;)V

    .line 203
    new-instance v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/DefaultValueFormatter;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/DefaultValueFormatter;-><init>(I)V

    iput-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mDefaultFormatter:Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ValueFormatter;

    .line 205
    new-instance v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;

    invoke-direct {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;-><init>()V

    iput-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mViewPortHandler:Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;

    .line 207
    new-instance v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;

    invoke-direct {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;-><init>()V

    iput-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mLegend:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;

    .line 209
    new-instance v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/LegendRenderer;

    iget-object v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mViewPortHandler:Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;

    iget-object v2, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mLegend:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;

    invoke-direct {v0, v1, v2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/LegendRenderer;-><init>(Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;)V

    iput-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mLegendRenderer:Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/LegendRenderer;

    .line 211
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mDescPaint:Landroid/graphics/Paint;

    .line 212
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mDescPaint:Landroid/graphics/Paint;

    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 213
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mDescPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Align;->RIGHT:Landroid/graphics/Paint$Align;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 214
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mDescPaint:Landroid/graphics/Paint;

    const/high16 v1, 0x41100000    # 9.0f

    invoke-static {v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Utils;->convertDpToPixel(F)F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 216
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mInfoPaint:Landroid/graphics/Paint;

    .line 217
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mInfoPaint:Landroid/graphics/Paint;

    const/16 v1, 0xf7

    const/16 v2, 0xbd

    const/16 v3, 0x33

    invoke-static {v1, v2, v3}, Landroid/graphics/Color;->rgb(III)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 218
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mInfoPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 219
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mInfoPaint:Landroid/graphics/Paint;

    const/high16 v1, 0x41400000    # 12.0f

    invoke-static {v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Utils;->convertDpToPixel(F)F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 221
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mDrawPaint:Landroid/graphics/Paint;

    .line 223
    iget-boolean v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mLogEnabled:Z

    if-eqz v0, :cond_a3

    .line 224
    const-string v0, ""

    const-string v1, "Chart.init()"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 225
    :cond_a3
    return-void
.end method

.method public isDragDecelerationEnabled()Z
    .registers 2

    .line 662
    iget-boolean v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mDragDecelerationEnabled:Z

    return v0
.end method

.method public isDrawMarkerViewEnabled()Z
    .registers 2

    .line 1309
    iget-boolean v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mDrawMarkerViews:Z

    return v0
.end method

.method public isEmpty()Z
    .registers 2

    .line 322
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mData:Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;

    if-nez v0, :cond_6

    .line 323
    const/4 v0, 0x1

    return v0

    .line 326
    :cond_6
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mData:Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->getYValCount()I

    move-result v0

    if-gtz v0, :cond_10

    .line 327
    const/4 v0, 0x1

    return v0

    .line 329
    :cond_10
    const/4 v0, 0x0

    return v0
.end method

.method public isHighlightEnabled()Z
    .registers 2

    .line 902
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mData:Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;

    if-nez v0, :cond_6

    const/4 v0, 0x1

    goto :goto_c

    :cond_6
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mData:Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->isHighlightEnabled()Z

    move-result v0

    :goto_c
    return v0
.end method

.method public isLogEnabled()Z
    .registers 2

    .line 1143
    iget-boolean v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mLogEnabled:Z

    return v0
.end method

.method public abstract notifyDataSetChanged()V
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .registers 7
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .line 387
    iget-boolean v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mDataNotSet:Z

    if-nez v0, :cond_10

    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mData:Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;

    if-eqz v0, :cond_10

    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mData:Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->getYValCount()I

    move-result v0

    if-gtz v0, :cond_53

    .line 394
    :cond_10
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mNoDataText:Ljava/lang/String;

    invoke-virtual {p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->getWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    invoke-virtual {p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->getHeight()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    iget-object v3, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mInfoPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 396
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mNoDataTextDescription:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_52

    .line 397
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mInfoPaint:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->ascent()F

    move-result v0

    neg-float v0, v0

    iget-object v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mInfoPaint:Landroid/graphics/Paint;

    invoke-virtual {v1}, Landroid/graphics/Paint;->descent()F

    move-result v1

    add-float v4, v0, v1

    .line 398
    .local v4, "textOffset":F
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mNoDataTextDescription:Ljava/lang/String;

    invoke-virtual {p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->getWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    invoke-virtual {p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->getHeight()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    add-float/2addr v2, v4

    iget-object v3, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mInfoPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 401
    .end local v4    # "textOffset":F
    :cond_52
    return-void

    .line 404
    :cond_53
    iget-boolean v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mOffsetsCalculated:Z

    if-nez v0, :cond_5d

    .line 406
    invoke-virtual {p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->calculateOffsets()V

    .line 407
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mOffsetsCalculated:Z

    .line 416
    :cond_5d
    return-void
.end method

.method protected onLayout(ZIIII)V
    .registers 8
    .param p1, "changed"    # Z
    .param p2, "left"    # I
    .param p3, "top"    # I
    .param p4, "right"    # I
    .param p5, "bottom"    # I

    .line 1573
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    invoke-virtual {p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->getChildCount()I

    move-result v0

    if-ge v1, v0, :cond_11

    .line 1574
    invoke-virtual {p0, v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p2, p3, p4, p5}, Landroid/view/View;->layout(IIII)V

    .line 1573
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1576
    .end local v1    # "i":I
    :cond_11
    return-void
.end method

.method protected onMeasure(II)V
    .registers 7
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .line 1580
    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->onMeasure(II)V

    .line 1581
    const/high16 v0, 0x42480000    # 50.0f

    invoke-static {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Utils;->convertDpToPixel(F)F

    move-result v0

    float-to-int v3, v0

    .line 1582
    .line 1583
    .local v3, "size":I
    invoke-virtual {p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->getSuggestedMinimumWidth()I

    move-result v0

    .line 1584
    invoke-static {v3, p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->resolveSize(II)I

    move-result v1

    .line 1583
    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 1586
    invoke-virtual {p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->getSuggestedMinimumHeight()I

    move-result v1

    .line 1587
    invoke-static {v3, p2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->resolveSize(II)I

    move-result v2

    .line 1586
    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 1582
    invoke-virtual {p0, v0, v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->setMeasuredDimension(II)V

    .line 1589
    return-void
.end method

.method protected onSizeChanged(IIII)V
    .registers 10
    .param p1, "w"    # I
    .param p2, "h"    # I
    .param p3, "oldw"    # I
    .param p4, "oldh"    # I

    .line 1593
    iget-boolean v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mLogEnabled:Z

    if-eqz v0, :cond_b

    .line 1594
    const-string v0, "MPAndroidChart"

    const-string v1, "OnSizeChanged()"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1596
    :cond_b
    if-lez p1, :cond_71

    if-lez p2, :cond_71

    const/16 v0, 0x2710

    if-ge p1, v0, :cond_71

    const/16 v0, 0x2710

    if-ge p2, v0, :cond_71

    .line 1599
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mDrawBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_20

    .line 1600
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mDrawBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 1602
    :cond_20
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_4444:Landroid/graphics/Bitmap$Config;

    invoke-static {p1, p2, v0}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mDrawBitmap:Landroid/graphics/Bitmap;

    .line 1603
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mViewPortHandler:Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;

    int-to-float v1, p1

    int-to-float v2, p2

    invoke-virtual {v0, v1, v2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->setChartDimens(FF)V

    .line 1605
    iget-boolean v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mLogEnabled:Z

    if-eqz v0, :cond_55

    .line 1606
    const-string v0, "MPAndroidChart"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Setting chart dimens, width: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", height: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1608
    :cond_55
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mJobs:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_5b
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6c

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Ljava/lang/Runnable;

    .line 1609
    .local v4, "r":Ljava/lang/Runnable;
    invoke-virtual {p0, v4}, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->post(Ljava/lang/Runnable;)Z

    .line 1610
    .end local v4    # "r":Ljava/lang/Runnable;
    goto :goto_5b

    .line 1612
    :cond_6c
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mJobs:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 1615
    :cond_71
    invoke-virtual {p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->notifyDataSetChanged()V

    .line 1617
    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/ViewGroup;->onSizeChanged(IIII)V

    .line 1618
    return-void
.end method

.method public removeJob(Ljava/lang/Runnable;)V
    .registers 3
    .param p1, "job"    # Ljava/lang/Runnable;

    .line 1553
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mJobs:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 1554
    return-void
.end method

.method public saveToGallery(Ljava/lang/String;I)Z
    .registers 14
    .param p1, "fileName"    # Ljava/lang/String;
    .param p2, "quality"    # I

    .line 1488
    if-ltz p2, :cond_6

    const/16 v0, 0x64

    if-le p2, v0, :cond_8

    .line 1489
    :cond_6
    const/16 p2, 0x32

    .line 1491
    :cond_8
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 1493
    .local v2, "currentTime":J
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v4

    .line 1494
    .local v4, "extBaseDir":Ljava/io/File;
    new-instance v5, Ljava/io/File;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/DCIM"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v5, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1495
    .local v5, "file":Ljava/io/File;
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_3a

    .line 1496
    invoke-virtual {v5}, Ljava/io/File;->mkdirs()Z

    move-result v0

    if-nez v0, :cond_3a

    .line 1497
    const/4 v0, 0x0

    return v0

    .line 1501
    :cond_3a
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 1502
    .local v6, "filePath":Ljava/lang/String;
    const/4 v7, 0x0

    .line 1504
    .local v7, "out":Ljava/io/FileOutputStream;
    :try_start_56
    new-instance v7, Ljava/io/FileOutputStream;

    invoke-direct {v7, v6}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    .line 1506
    invoke-virtual {p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->getChartBitmap()Landroid/graphics/Bitmap;

    move-result-object v8

    .line 1508
    .local v8, "b":Landroid/graphics/Bitmap;
    sget-object v0, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    invoke-virtual {v8, v0, p2, v7}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 1512
    invoke-virtual {v7}, Ljava/io/FileOutputStream;->flush()V

    .line 1513
    invoke-virtual {v7}, Ljava/io/FileOutputStream;->close()V
    :try_end_6a
    .catch Ljava/io/IOException; {:try_start_56 .. :try_end_6a} :catch_6b

    .line 1519
    .end local v8    # "b":Landroid/graphics/Bitmap;
    goto :goto_71

    .line 1515
    :catch_6b
    move-exception v8

    .line 1516
    .local v8, "e":Ljava/io/IOException;
    invoke-virtual {v8}, Ljava/io/IOException;->printStackTrace()V

    .line 1518
    const/4 v0, 0x0

    return v0

    .line 1521
    .end local v8    # "e":Ljava/io/IOException;
    :goto_71
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v8

    .line 1523
    .local v8, "size":J
    new-instance v10, Landroid/content/ContentValues;

    const/16 v0, 0x8

    invoke-direct {v10, v0}, Landroid/content/ContentValues;-><init>(I)V

    .line 1526
    .local v10, "values":Landroid/content/ContentValues;
    const-string v0, "title"

    invoke-virtual {v10, v0, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1527
    const-string v0, "_display_name"

    invoke-virtual {v10, v0, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1528
    const-string v0, "date_added"

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v10, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1529
    const-string v0, "mime_type"

    const-string v1, "image/jpeg"

    invoke-virtual {v10, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1530
    const-string v0, "description"

    const-string v1, "MPAndroidChart-Library Save"

    invoke-virtual {v10, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1531
    const-string v0, "orientation"

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v10, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1532
    const-string v0, "_data"

    invoke-virtual {v10, v0, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1533
    const-string v0, "_size"

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v10, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1535
    invoke-virtual {p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1, v10}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v0

    if-nez v0, :cond_cc

    const/4 v0, 0x0

    goto :goto_cd

    :cond_cc
    const/4 v0, 0x1

    :goto_cd
    return v0
.end method

.method public saveToPath(Ljava/lang/String;Ljava/lang/String;)Z
    .registers 8
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "pathOnSD"    # Ljava/lang/String;

    .line 1453
    invoke-virtual {p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->getChartBitmap()Landroid/graphics/Bitmap;

    move-result-object v2

    .line 1455
    .local v2, "b":Landroid/graphics/Bitmap;
    const/4 v3, 0x0

    .line 1457
    .local v3, "stream":Ljava/io/OutputStream;
    :try_start_5
    new-instance v3, Ljava/io/FileOutputStream;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".png"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    .line 1465
    sget-object v0, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v1, 0x28

    invoke-virtual {v2, v0, v1, v3}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 1467
    invoke-virtual {v3}, Ljava/io/OutputStream;->close()V
    :try_end_3d
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_3d} :catch_3e

    .line 1471
    goto :goto_44

    .line 1468
    :catch_3e
    move-exception v4

    .line 1469
    .local v4, "e":Ljava/lang/Exception;
    invoke-virtual {v4}, Ljava/lang/Exception;->printStackTrace()V

    .line 1470
    const/4 v0, 0x0

    return v0

    .line 1473
    .end local v4    # "e":Ljava/lang/Exception;
    :goto_44
    const/4 v0, 0x1

    return v0
.end method

.method public setData(Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;)V
    .registers 6
    .param p1, "data"    # Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .line 269
    if-nez p1, :cond_a

    .line 270
    const-string v0, "MPAndroidChart"

    const-string v1, "Cannot set data for chart. Provided data object is null."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 272
    return-void

    .line 276
    :cond_a
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mDataNotSet:Z

    .line 277
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mOffsetsCalculated:Z

    .line 278
    iput-object p1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mData:Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;

    .line 281
    invoke-virtual {p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->getYMin()F

    move-result v0

    invoke-virtual {p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->getYMax()F

    move-result v1

    invoke-virtual {p0, v0, v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->calculateFormatter(FF)V

    .line 283
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mData:Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->getDataSets()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_27
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_40

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;

    .line 284
    .local v3, "set":Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;, "Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet<*>;"
    invoke-virtual {v3}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->needsDefaultFormatter()Z

    move-result v0

    if-eqz v0, :cond_3f

    .line 285
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mDefaultFormatter:Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ValueFormatter;

    invoke-virtual {v3, v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->setValueFormatter(Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ValueFormatter;)V

    .line 286
    .end local v3    # "set":Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;, "Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet<*>;"
    .end local v3
    :cond_3f
    goto :goto_27

    .line 289
    :cond_40
    invoke-virtual {p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->notifyDataSetChanged()V

    .line 291
    iget-boolean v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mLogEnabled:Z

    if-eqz v0, :cond_4e

    .line 292
    const-string v0, "MPAndroidChart"

    const-string v1, "Data is set."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 293
    :cond_4e
    return-void
.end method

.method public setDescription(Ljava/lang/String;)V
    .registers 2
    .param p1, "desc"    # Ljava/lang/String;

    .line 1007
    if-nez p1, :cond_4

    .line 1008
    const-string p1, ""

    .line 1009
    :cond_4
    iput-object p1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mDescription:Ljava/lang/String;

    .line 1010
    return-void
.end method

.method public setDescriptionColor(I)V
    .registers 3
    .param p1, "color"    # I

    .line 1052
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mDescPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 1053
    return-void
.end method

.method public setDescriptionPosition(FF)V
    .registers 4
    .param p1, "x"    # F
    .param p2, "y"    # F

    .line 1019
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0, p1, p2}, Landroid/graphics/PointF;-><init>(FF)V

    iput-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mDescriptionPosition:Landroid/graphics/PointF;

    .line 1020
    return-void
.end method

.method public setDescriptionTextSize(F)V
    .registers 4
    .param p1, "size"    # F

    .line 1038
    const/high16 v0, 0x41800000    # 16.0f

    cmpl-float v0, p1, v0

    if-lez v0, :cond_8

    .line 1039
    const/high16 p1, 0x41800000    # 16.0f

    .line 1040
    :cond_8
    const/high16 v0, 0x40c00000    # 6.0f

    cmpg-float v0, p1, v0

    if-gez v0, :cond_10

    .line 1041
    const/high16 p1, 0x40c00000    # 6.0f

    .line 1043
    :cond_10
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mDescPaint:Landroid/graphics/Paint;

    invoke-static {p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Utils;->convertDpToPixel(F)F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 1044
    return-void
.end method

.method public setDescriptionTypeface(Landroid/graphics/Typeface;)V
    .registers 3
    .param p1, "t"    # Landroid/graphics/Typeface;

    .line 1028
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mDescPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 1029
    return-void
.end method

.method public setDragDecelerationEnabled(Z)V
    .registers 2
    .param p1, "enabled"    # Z

    .line 671
    iput-boolean p1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mDragDecelerationEnabled:Z

    .line 672
    return-void
.end method

.method public setDragDecelerationFrictionCoef(F)V
    .registers 3
    .param p1, "newValue"    # F

    .line 693
    const/4 v0, 0x0

    cmpg-float v0, p1, v0

    if-gez v0, :cond_6

    .line 694
    const/4 p1, 0x0

    .line 696
    :cond_6
    const/high16 v0, 0x3f800000    # 1.0f

    cmpl-float v0, p1, v0

    if-ltz v0, :cond_f

    .line 697
    const p1, 0x3f7fbe77    # 0.999f

    .line 699
    :cond_f
    iput p1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mDragDecelerationFrictionCoef:F

    .line 700
    return-void
.end method

.method public setDrawMarkerViews(Z)V
    .registers 2
    .param p1, "enabled"    # Z

    .line 1320
    iput-boolean p1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mDrawMarkerViews:Z

    .line 1321
    return-void
.end method

.method public setExtraBottomOffset(F)V
    .registers 3
    .param p1, "offset"    # F

    .line 1103
    invoke-static {p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Utils;->convertDpToPixel(F)F

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mExtraBottomOffset:F

    .line 1104
    return-void
.end method

.method public setExtraLeftOffset(F)V
    .registers 3
    .param p1, "offset"    # F

    .line 1117
    invoke-static {p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Utils;->convertDpToPixel(F)F

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mExtraLeftOffset:F

    .line 1118
    return-void
.end method

.method public setExtraOffsets(FFFF)V
    .registers 5
    .param p1, "left"    # F
    .param p2, "top"    # F
    .param p3, "right"    # F
    .param p4, "bottom"    # F

    .line 1065
    invoke-virtual {p0, p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->setExtraLeftOffset(F)V

    .line 1066
    invoke-virtual {p0, p2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->setExtraTopOffset(F)V

    .line 1067
    invoke-virtual {p0, p3}, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->setExtraRightOffset(F)V

    .line 1068
    invoke-virtual {p0, p4}, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->setExtraBottomOffset(F)V

    .line 1069
    return-void
.end method

.method public setExtraRightOffset(F)V
    .registers 3
    .param p1, "offset"    # F

    .line 1089
    invoke-static {p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Utils;->convertDpToPixel(F)F

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mExtraRightOffset:F

    .line 1090
    return-void
.end method

.method public setExtraTopOffset(F)V
    .registers 3
    .param p1, "offset"    # F

    .line 1075
    invoke-static {p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Utils;->convertDpToPixel(F)F

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mExtraTopOffset:F

    .line 1076
    return-void
.end method

.method public setHardwareAccelerationEnabled(Z)V
    .registers 4
    .param p1, "enabled"    # Z

    .line 1628
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_14

    .line 1630
    if-eqz p1, :cond_e

    .line 1631
    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->setLayerType(ILandroid/graphics/Paint;)V

    goto :goto_1b

    .line 1633
    :cond_e
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->setLayerType(ILandroid/graphics/Paint;)V

    goto :goto_1b

    .line 1635
    :cond_14
    const-string v0, "MPAndroidChart"

    const-string v1, "Cannot enable/disable hardware acceleration for devices below API level 11."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1638
    :goto_1b
    return-void
.end method

.method public setHighlightEnabled(Z)V
    .registers 3
    .param p1, "enabled"    # Z

    .line 892
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mData:Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;

    if-eqz v0, :cond_9

    .line 893
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mData:Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;

    invoke-virtual {v0, p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->setHighlightEnabled(Z)V

    .line 894
    :cond_9
    return-void
.end method

.method public setLogEnabled(Z)V
    .registers 2
    .param p1, "enabled"    # Z

    .line 1134
    iput-boolean p1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mLogEnabled:Z

    .line 1135
    return-void
.end method

.method public setMarkerView(Lbr/com/itau/widgets/graphicalfriedcake/charts/components/MarkerView;)V
    .registers 2
    .param p1, "v"    # Lbr/com/itau/widgets/graphicalfriedcake/charts/components/MarkerView;

    .line 1182
    iput-object p1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mMarkerView:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/MarkerView;

    .line 1183
    return-void
.end method

.method public setNoDataText(Ljava/lang/String;)V
    .registers 2
    .param p1, "text"    # Ljava/lang/String;

    .line 1153
    iput-object p1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mNoDataText:Ljava/lang/String;

    .line 1154
    return-void
.end method

.method public setNoDataTextDescription(Ljava/lang/String;)V
    .registers 2
    .param p1, "text"    # Ljava/lang/String;

    .line 1163
    iput-object p1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mNoDataTextDescription:Ljava/lang/String;

    .line 1164
    return-void
.end method

.method public setOnChartGestureListener(Lbr/com/itau/widgets/graphicalfriedcake/charts/listener/OnChartGestureListener;)V
    .registers 2
    .param p1, "l"    # Lbr/com/itau/widgets/graphicalfriedcake/charts/listener/OnChartGestureListener;

    .line 872
    iput-object p1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mGestureListener:Lbr/com/itau/widgets/graphicalfriedcake/charts/listener/OnChartGestureListener;

    .line 873
    return-void
.end method

.method public setOnChartValueSelectedListener(Lbr/com/itau/widgets/graphicalfriedcake/charts/listener/OnChartValueSelectedListener;)V
    .registers 2
    .param p1, "l"    # Lbr/com/itau/widgets/graphicalfriedcake/charts/listener/OnChartValueSelectedListener;

    .line 862
    iput-object p1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mSelectionListener:Lbr/com/itau/widgets/graphicalfriedcake/charts/listener/OnChartValueSelectedListener;

    .line 863
    return-void
.end method

.method public setOnTouchListener(Lbr/com/itau/widgets/graphicalfriedcake/charts/listener/ChartTouchListener;)V
    .registers 2
    .param p1, "l"    # Lbr/com/itau/widgets/graphicalfriedcake/charts/listener/ChartTouchListener;

    .line 565
    iput-object p1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mChartTouchListener:Lbr/com/itau/widgets/graphicalfriedcake/charts/listener/ChartTouchListener;

    .line 566
    return-void
.end method

.method public setPaint(Landroid/graphics/Paint;I)V
    .registers 3
    .param p1, "p"    # Landroid/graphics/Paint;
    .param p2, "which"    # I

    .line 1275
    sparse-switch p2, :sswitch_data_a

    goto :goto_9

    .line 1277
    :sswitch_4
    iput-object p1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mInfoPaint:Landroid/graphics/Paint;

    .line 1278
    goto :goto_9

    .line 1280
    :sswitch_7
    iput-object p1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mDescPaint:Landroid/graphics/Paint;

    .line 1283
    :goto_9
    return-void

    :sswitch_data_a
    .sparse-switch
        0x7 -> :sswitch_4
        0xb -> :sswitch_7
    .end sparse-switch
.end method

.method public setRenderer(Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/DataRenderer;)V
    .registers 2
    .param p1, "renderer"    # Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/DataRenderer;

    .line 1407
    if-eqz p1, :cond_4

    .line 1408
    iput-object p1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mRenderer:Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/DataRenderer;

    .line 1409
    :cond_4
    return-void
.end method

.method public setTouchEnabled(Z)V
    .registers 2
    .param p1, "enabled"    # Z

    .line 1173
    iput-boolean p1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mTouchEnabled:Z

    .line 1174
    return-void
.end method

.method public valuesToHighlight()Z
    .registers 3

    .line 468
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mIndicesToHightlight:[Lbr/com/itau/widgets/graphicalfriedcake/charts/highlight/Highlight;

    if-eqz v0, :cond_10

    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mIndicesToHightlight:[Lbr/com/itau/widgets/graphicalfriedcake/charts/highlight/Highlight;

    array-length v0, v0

    if-lez v0, :cond_10

    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->mIndicesToHightlight:[Lbr/com/itau/widgets/graphicalfriedcake/charts/highlight/Highlight;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    if-nez v0, :cond_12

    :cond_10
    const/4 v0, 0x0

    goto :goto_13

    :cond_12
    const/4 v0, 0x1

    :goto_13
    return v0
.end method

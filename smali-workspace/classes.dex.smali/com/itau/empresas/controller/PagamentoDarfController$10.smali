.class Lcom/itau/empresas/controller/PagamentoDarfController$10;
.super Ljava/lang/Object;
.source "PagamentoDarfController.java"

# interfaces
.implements Lbr/com/itau/sdk/android/core/type/BackendCall;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/itau/empresas/controller/PagamentoDarfController;->consultaAlcadas(Ljava/lang/String;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/itau/empresas/controller/PagamentoDarfController;

.field final synthetic val$conta:Ljava/lang/String;

.field final synthetic val$operador:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/itau/empresas/controller/PagamentoDarfController;Ljava/lang/String;Ljava/lang/String;)V
    .registers 4
    .param p1, "this$0"    # Lcom/itau/empresas/controller/PagamentoDarfController;

    .line 145
    iput-object p1, p0, Lcom/itau/empresas/controller/PagamentoDarfController$10;->this$0:Lcom/itau/empresas/controller/PagamentoDarfController;

    iput-object p2, p0, Lcom/itau/empresas/controller/PagamentoDarfController$10;->val$operador:Ljava/lang/String;

    iput-object p3, p0, Lcom/itau/empresas/controller/PagamentoDarfController$10;->val$conta:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public callBackend()V
    .registers 4

    .line 148
    iget-object v0, p0, Lcom/itau/empresas/controller/PagamentoDarfController$10;->this$0:Lcom/itau/empresas/controller/PagamentoDarfController;

    invoke-virtual {v0}, Lcom/itau/empresas/controller/PagamentoDarfController;->api()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/api/Api;

    iget-object v1, p0, Lcom/itau/empresas/controller/PagamentoDarfController$10;->val$operador:Ljava/lang/String;

    iget-object v2, p0, Lcom/itau/empresas/controller/PagamentoDarfController$10;->val$conta:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lcom/itau/empresas/api/Api;->listaAlcada(Ljava/lang/String;Ljava/lang/String;)Lcom/itau/empresas/api/model/AlcadaVO;

    move-result-object v0

    invoke-static {v0}, Lcom/itau/empresas/controller/BaseController;->post(Ljava/lang/Object;)V

    .line 149
    return-void
.end method

.class public interface abstract Lbr/com/itau/textovoz/api/Api;
.super Ljava/lang/Object;
.source "Api.java"


# virtual methods
.method public abstract getSearch(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZZLjava/lang/String;Ljava/lang/String;ZZ)Lbr/com/itau/textovoz/model/response/SearchResponse;
    .param p1    # Ljava/lang/String;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Header;
            value = "itau-chave"
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Query;
            value = "termoBuscado"
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Query;
            value = "agencia"
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Query;
            value = "conta"
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Query;
            value = "digito_verificador_conta"
        .end annotation
    .end param
    .param p6    # Ljava/lang/String;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Query;
            value = "operador"
        .end annotation
    .end param
    .param p7    # Ljava/lang/String;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Query;
            value = "versao_os"
        .end annotation
    .end param
    .param p8    # Ljava/lang/String;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Query;
            value = "plataforma"
        .end annotation
    .end param
    .param p9    # Ljava/lang/String;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Query;
            value = "modelo"
        .end annotation
    .end param
    .param p10    # Z
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Query;
            value = "flag_voz"
        .end annotation
    .end param
    .param p11    # Z
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Query;
            value = "flag_menu"
        .end annotation
    .end param
    .param p12    # Z
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Query;
            value = "flag_faq"
        .end annotation
    .end param
    .param p13    # Ljava/lang/String;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Query;
            value = "flag_card"
        .end annotation
    .end param
    .param p14    # Ljava/lang/String;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Query;
            value = "flag_transacao"
        .end annotation
    .end param
    .param p15    # Z
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Query;
            value = "flag_transbordos"
        .end annotation
    .end param
    .param p16    # Z
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Query;
            value = "flag_comprovantes"
        .end annotation
    .end param
    .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/GET;
        opKey = "atendimentosBuscas"
    .end annotation

    .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Headers;
        value = {
            "Accept: application/vnd.itau.v1+json"
        }
    .end annotation
.end method

.class Lbr/com/itau/textovoz/controller/SearchController$1;
.super Ljava/lang/Object;
.source "SearchController.java"

# interfaces
.implements Lbr/com/itau/sdk/android/core/type/BackendCall;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lbr/com/itau/textovoz/controller/SearchController;->doGetSearch(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZZLjava/lang/String;Ljava/lang/String;ZZ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lbr/com/itau/textovoz/controller/SearchController;

.field final synthetic val$account:Ljava/lang/String;

.field final synthetic val$agency:Ljava/lang/String;

.field final synthetic val$digitChecker:Ljava/lang/String;

.field final synthetic val$flagCard:Ljava/lang/String;

.field final synthetic val$flagFaq:Z

.field final synthetic val$flagMenu:Z

.field final synthetic val$flagOverflow:Z

.field final synthetic val$flagReceipts:Z

.field final synthetic val$flagTrans:Ljava/lang/String;

.field final synthetic val$flagVoice:Z

.field final synthetic val$model:Ljava/lang/String;

.field final synthetic val$operator:Ljava/lang/String;

.field final synthetic val$opkKey:Ljava/lang/String;

.field final synthetic val$plataform:Ljava/lang/String;

.field final synthetic val$searchTerm:Ljava/lang/String;

.field final synthetic val$versionOS:Ljava/lang/String;


# direct methods
.method constructor <init>(Lbr/com/itau/textovoz/controller/SearchController;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZZLjava/lang/String;Ljava/lang/String;ZZ)V
    .registers 19
    .param p1, "this$0"    # Lbr/com/itau/textovoz/controller/SearchController;

    .line 18
    iput-object p1, p0, Lbr/com/itau/textovoz/controller/SearchController$1;->this$0:Lbr/com/itau/textovoz/controller/SearchController;

    iput-object p2, p0, Lbr/com/itau/textovoz/controller/SearchController$1;->val$opkKey:Ljava/lang/String;

    iput-object p3, p0, Lbr/com/itau/textovoz/controller/SearchController$1;->val$searchTerm:Ljava/lang/String;

    iput-object p4, p0, Lbr/com/itau/textovoz/controller/SearchController$1;->val$agency:Ljava/lang/String;

    iput-object p5, p0, Lbr/com/itau/textovoz/controller/SearchController$1;->val$account:Ljava/lang/String;

    iput-object p6, p0, Lbr/com/itau/textovoz/controller/SearchController$1;->val$digitChecker:Ljava/lang/String;

    iput-object p7, p0, Lbr/com/itau/textovoz/controller/SearchController$1;->val$operator:Ljava/lang/String;

    iput-object p8, p0, Lbr/com/itau/textovoz/controller/SearchController$1;->val$versionOS:Ljava/lang/String;

    iput-object p9, p0, Lbr/com/itau/textovoz/controller/SearchController$1;->val$plataform:Ljava/lang/String;

    iput-object p10, p0, Lbr/com/itau/textovoz/controller/SearchController$1;->val$model:Ljava/lang/String;

    iput-boolean p11, p0, Lbr/com/itau/textovoz/controller/SearchController$1;->val$flagVoice:Z

    iput-boolean p12, p0, Lbr/com/itau/textovoz/controller/SearchController$1;->val$flagMenu:Z

    iput-boolean p13, p0, Lbr/com/itau/textovoz/controller/SearchController$1;->val$flagFaq:Z

    iput-object p14, p0, Lbr/com/itau/textovoz/controller/SearchController$1;->val$flagCard:Ljava/lang/String;

    move-object/from16 v0, p15

    iput-object v0, p0, Lbr/com/itau/textovoz/controller/SearchController$1;->val$flagTrans:Ljava/lang/String;

    move/from16 v0, p16

    iput-boolean v0, p0, Lbr/com/itau/textovoz/controller/SearchController$1;->val$flagOverflow:Z

    move/from16 v0, p17

    iput-boolean v0, p0, Lbr/com/itau/textovoz/controller/SearchController$1;->val$flagReceipts:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public callBackend()V
    .registers 21

    .line 21
    invoke-static {}, Lde/greenrobot/event/EventBus;->getDefault()Lde/greenrobot/event/EventBus;

    move-result-object v1

    new-instance v2, Lbr/com/itau/textovoz/api/event/EventClientSearchResponse;

    invoke-static {}, Lbr/com/itau/textovoz/SearchItau;->getBackendClientApi()Lbr/com/itau/textovoz/api/Api;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lbr/com/itau/textovoz/controller/SearchController$1;->val$opkKey:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v5, v0, Lbr/com/itau/textovoz/controller/SearchController$1;->val$searchTerm:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v6, v0, Lbr/com/itau/textovoz/controller/SearchController$1;->val$agency:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v7, v0, Lbr/com/itau/textovoz/controller/SearchController$1;->val$account:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v8, v0, Lbr/com/itau/textovoz/controller/SearchController$1;->val$digitChecker:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v9, v0, Lbr/com/itau/textovoz/controller/SearchController$1;->val$operator:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v10, v0, Lbr/com/itau/textovoz/controller/SearchController$1;->val$versionOS:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v11, v0, Lbr/com/itau/textovoz/controller/SearchController$1;->val$plataform:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v12, v0, Lbr/com/itau/textovoz/controller/SearchController$1;->val$model:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-boolean v13, v0, Lbr/com/itau/textovoz/controller/SearchController$1;->val$flagVoice:Z

    move-object/from16 v0, p0

    iget-boolean v14, v0, Lbr/com/itau/textovoz/controller/SearchController$1;->val$flagMenu:Z

    move-object/from16 v0, p0

    iget-boolean v15, v0, Lbr/com/itau/textovoz/controller/SearchController$1;->val$flagFaq:Z

    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/textovoz/controller/SearchController$1;->val$flagCard:Ljava/lang/String;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/textovoz/controller/SearchController$1;->val$flagTrans:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lbr/com/itau/textovoz/controller/SearchController$1;->val$flagOverflow:Z

    move/from16 v18, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lbr/com/itau/textovoz/controller/SearchController$1;->val$flagReceipts:Z

    move/from16 v19, v0

    .line 22
    invoke-interface/range {v3 .. v19}, Lbr/com/itau/textovoz/api/Api;->getSearch(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZZLjava/lang/String;Ljava/lang/String;ZZ)Lbr/com/itau/textovoz/model/response/SearchResponse;

    move-result-object v0

    invoke-direct {v2, v0}, Lbr/com/itau/textovoz/api/event/EventClientSearchResponse;-><init>(Lbr/com/itau/textovoz/model/response/SearchResponse;)V

    .line 21
    invoke-virtual {v1, v2}, Lde/greenrobot/event/EventBus;->post(Ljava/lang/Object;)V

    .line 25
    return-void
.end method

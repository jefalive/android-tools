.class public Lcom/google/android/gms/internal/zzdy;
.super Ljava/lang/Object;


# annotations
.annotation runtime Lcom/google/android/gms/internal/zzhb;
.end annotation


# instance fields
.field private final zzAx:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<Lcom/google/android/gms/internal/zzdz;Lcom/google/android/gms/internal/zzea;>;"
        }
    .end annotation
.end field

.field private final zzAy:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList<Lcom/google/android/gms/internal/zzdz;>;"
        }
    .end annotation
.end field

.field private zzAz:Lcom/google/android/gms/internal/zzdv;


# direct methods
.method public constructor <init>()V
    .registers 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/internal/zzdy;->zzAx:Ljava/util/Map;

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/internal/zzdy;->zzAy:Ljava/util/LinkedList;

    return-void
.end method

.method private zzY(Ljava/lang/String;)[Ljava/lang/String;
    .registers 7

    const-string v0, "\u0000"

    :try_start_2
    invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    :goto_7
    array-length v0, v3

    if-ge v4, v0, :cond_1d

    new-instance v0, Ljava/lang/String;

    aget-object v1, v3, v4

    const/4 v2, 0x0

    invoke-static {v1, v2}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v1

    const-string v2, "UTF-8"

    invoke-direct {v0, v1, v2}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    aput-object v0, v3, v4
    :try_end_1a
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_2 .. :try_end_1a} :catch_1e

    add-int/lit8 v4, v4, 0x1

    goto :goto_7

    :cond_1d
    return-object v3

    :catch_1e
    move-exception v3

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/String;

    return-object v0
.end method

.method private static zza(Ljava/lang/String;Lcom/google/android/gms/internal/zzdz;)V
    .registers 4

    const/4 v0, 0x2

    invoke-static {v0}, Lcom/google/android/gms/internal/zzin;->zzQ(I)Z

    move-result v0

    if-eqz v0, :cond_14

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    invoke-static {p0, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/internal/zzin;->v(Ljava/lang/String;)V

    :cond_14
    return-void
.end method

.method private zzef()Ljava/lang/String;
    .registers 6

    :try_start_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v0, p0, Lcom/google/android/gms/internal/zzdy;->zzAy:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_b
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_35

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/internal/zzdz;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/zzdz;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v0, "UTF-8"

    invoke-virtual {v4, v0}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_34

    const-string v0, "\u0000"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_34
    goto :goto_b

    :cond_35
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_38
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_38} :catch_3a

    move-result-object v0

    return-object v0

    :catch_3a
    move-exception v2

    const-string v0, ""

    return-object v0
.end method


# virtual methods
.method flush()V
    .registers 5

    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/internal/zzdy;->zzAy:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    if-lez v0, :cond_35

    iget-object v0, p0, Lcom/google/android/gms/internal/zzdy;->zzAy:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->remove()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/google/android/gms/internal/zzdz;

    iget-object v0, p0, Lcom/google/android/gms/internal/zzdy;->zzAx:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/google/android/gms/internal/zzea;

    const-string v0, "Flushing interstitial queue for %s."

    invoke-static {v0, v1}, Lcom/google/android/gms/internal/zzdy;->zza(Ljava/lang/String;Lcom/google/android/gms/internal/zzdz;)V

    :goto_1f
    invoke-virtual {v2}, Lcom/google/android/gms/internal/zzea;->size()I

    move-result v0

    if-lez v0, :cond_2f

    invoke-virtual {v2}, Lcom/google/android/gms/internal/zzea;->zzej()Lcom/google/android/gms/internal/zzea$zza;

    move-result-object v3

    iget-object v0, v3, Lcom/google/android/gms/internal/zzea$zza;->zzAD:Lcom/google/android/gms/ads/internal/zzk;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/internal/zzk;->zzbp()V

    goto :goto_1f

    :cond_2f
    iget-object v0, p0, Lcom/google/android/gms/internal/zzdy;->zzAx:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_35
    return-void
.end method

.method restore()V
    .registers 14

    iget-object v0, p0, Lcom/google/android/gms/internal/zzdy;->zzAz:Lcom/google/android/gms/internal/zzdv;

    if-nez v0, :cond_5

    return-void

    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/internal/zzdy;->zzAz:Lcom/google/android/gms/internal/zzdv;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/zzdv;->zzed()Landroid/content/MutableContextWrapper;

    move-result-object v3

    const-string v0, "com.google.android.gms.ads.internal.interstitial.InterstitialAdPool"

    const/4 v1, 0x0

    invoke-virtual {v3, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v4

    invoke-virtual {p0}, Lcom/google/android/gms/internal/zzdy;->flush()V

    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    invoke-interface {v4}, Landroid/content/SharedPreferences;->getAll()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_26
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_87

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Ljava/util/Map$Entry;

    :try_start_33
    invoke-interface {v7}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Ljava/lang/String;

    const-string v0, "PoolKeys"

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_3f
    .catch Ljava/io/IOException; {:try_start_33 .. :try_end_3f} :catch_7f
    .catch Ljava/lang/ClassCastException; {:try_start_33 .. :try_end_3f} :catch_7f

    move-result v0

    if-eqz v0, :cond_43

    goto :goto_26

    :cond_43
    :try_start_43
    invoke-interface {v7}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Ljava/lang/String;

    new-instance v10, Lcom/google/android/gms/internal/zzec;

    invoke-direct {v10, v9}, Lcom/google/android/gms/internal/zzec;-><init>(Ljava/lang/String;)V

    new-instance v11, Lcom/google/android/gms/internal/zzdz;

    iget-object v0, v10, Lcom/google/android/gms/internal/zzec;->zzqH:Lcom/google/android/gms/ads/internal/client/AdRequestParcel;

    iget-object v1, v10, Lcom/google/android/gms/internal/zzec;->zzpS:Ljava/lang/String;

    iget v2, v10, Lcom/google/android/gms/internal/zzec;->zzAC:I

    invoke-direct {v11, v0, v1, v2}, Lcom/google/android/gms/internal/zzdz;-><init>(Lcom/google/android/gms/ads/internal/client/AdRequestParcel;Ljava/lang/String;I)V

    iget-object v0, p0, Lcom/google/android/gms/internal/zzdy;->zzAx:Ljava/util/Map;

    invoke-interface {v0, v11}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7e

    new-instance v12, Lcom/google/android/gms/internal/zzea;

    iget-object v0, v10, Lcom/google/android/gms/internal/zzec;->zzqH:Lcom/google/android/gms/ads/internal/client/AdRequestParcel;

    iget-object v1, v10, Lcom/google/android/gms/internal/zzec;->zzpS:Ljava/lang/String;

    iget v2, v10, Lcom/google/android/gms/internal/zzec;->zzAC:I

    invoke-direct {v12, v0, v1, v2}, Lcom/google/android/gms/internal/zzea;-><init>(Lcom/google/android/gms/ads/internal/client/AdRequestParcel;Ljava/lang/String;I)V

    iget-object v0, p0, Lcom/google/android/gms/internal/zzdy;->zzAx:Ljava/util/Map;

    invoke-interface {v0, v11, v12}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v11}, Lcom/google/android/gms/internal/zzdz;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v5, v0, v11}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "Restored interstitial queue for %s."

    invoke-static {v0, v11}, Lcom/google/android/gms/internal/zzdy;->zza(Ljava/lang/String;Lcom/google/android/gms/internal/zzdz;)V
    :try_end_7e
    .catch Ljava/io/IOException; {:try_start_43 .. :try_end_7e} :catch_7f
    .catch Ljava/lang/ClassCastException; {:try_start_43 .. :try_end_7e} :catch_7f

    :cond_7e
    goto :goto_85

    :catch_7f
    move-exception v8

    const-string v0, "Malformed preferences value for InterstitialAdPool."

    invoke-static {v0, v8}, Lcom/google/android/gms/internal/zzin;->zzd(Ljava/lang/String;Ljava/lang/Throwable;)V

    :goto_85
    goto/16 :goto_26

    :cond_87
    const-string v0, "PoolKeys"

    const-string v1, ""

    invoke-interface {v4, v0, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/internal/zzdy;->zzY(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    move-object v7, v6

    array-length v8, v7

    const/4 v9, 0x0

    :goto_96
    if-ge v9, v8, :cond_b1

    aget-object v10, v7, v9

    invoke-interface {v5, v10}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v11, v0

    check-cast v11, Lcom/google/android/gms/internal/zzdz;

    iget-object v0, p0, Lcom/google/android/gms/internal/zzdy;->zzAx:Ljava/util/Map;

    invoke-interface {v0, v11}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_ae

    iget-object v0, p0, Lcom/google/android/gms/internal/zzdy;->zzAy:Ljava/util/LinkedList;

    invoke-virtual {v0, v11}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    :cond_ae
    add-int/lit8 v9, v9, 0x1

    goto :goto_96

    :cond_b1
    return-void
.end method

.method save()V
    .registers 13

    iget-object v0, p0, Lcom/google/android/gms/internal/zzdy;->zzAz:Lcom/google/android/gms/internal/zzdv;

    if-nez v0, :cond_5

    return-void

    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/internal/zzdy;->zzAz:Lcom/google/android/gms/internal/zzdv;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/zzdv;->zzed()Landroid/content/MutableContextWrapper;

    move-result-object v2

    const-string v0, "com.google.android.gms.ads.internal.interstitial.InterstitialAdPool"

    const/4 v1, 0x0

    invoke-virtual {v2, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v3

    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    invoke-interface {v4}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    iget-object v0, p0, Lcom/google/android/gms/internal/zzdy;->zzAx:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_23
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5a

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Ljava/util/Map$Entry;

    invoke-interface {v6}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/google/android/gms/internal/zzdz;

    invoke-virtual {v7}, Lcom/google/android/gms/internal/zzdz;->zzeh()Z

    move-result v0

    if-eqz v0, :cond_59

    invoke-interface {v6}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/google/android/gms/internal/zzea;

    new-instance v9, Lcom/google/android/gms/internal/zzec;

    invoke-direct {v9, v8}, Lcom/google/android/gms/internal/zzec;-><init>(Lcom/google/android/gms/internal/zzea;)V

    invoke-virtual {v9}, Lcom/google/android/gms/internal/zzec;->zzem()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v7}, Lcom/google/android/gms/internal/zzdz;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-interface {v4, v11, v10}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    const-string v0, "Saved interstitial queue for %s."

    invoke-static {v0, v7}, Lcom/google/android/gms/internal/zzdy;->zza(Ljava/lang/String;Lcom/google/android/gms/internal/zzdz;)V

    :cond_59
    goto :goto_23

    :cond_5a
    const-string v0, "PoolKeys"

    invoke-direct {p0}, Lcom/google/android/gms/internal/zzdy;->zzef()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v4, v0, v1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v4}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return-void
.end method

.method zza(Lcom/google/android/gms/ads/internal/client/AdRequestParcel;Ljava/lang/String;)Lcom/google/android/gms/internal/zzea$zza;
    .registers 15

    iget-object v0, p0, Lcom/google/android/gms/internal/zzdy;->zzAz:Lcom/google/android/gms/internal/zzdv;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/zzdv;->zzed()Landroid/content/MutableContextWrapper;

    move-result-object v4

    new-instance v0, Lcom/google/android/gms/internal/zzhj$zza;

    invoke-direct {v0, v4}, Lcom/google/android/gms/internal/zzhj$zza;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Lcom/google/android/gms/internal/zzhj$zza;->zzgI()Lcom/google/android/gms/internal/zzhj;

    move-result-object v5

    iget v6, v5, Lcom/google/android/gms/internal/zzhj;->zzKc:I

    new-instance v7, Lcom/google/android/gms/internal/zzdz;

    invoke-direct {v7, p1, p2, v6}, Lcom/google/android/gms/internal/zzdz;-><init>(Lcom/google/android/gms/ads/internal/client/AdRequestParcel;Ljava/lang/String;I)V

    iget-object v0, p0, Lcom/google/android/gms/internal/zzdy;->zzAx:Ljava/util/Map;

    invoke-interface {v0, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/google/android/gms/internal/zzea;

    if-nez v8, :cond_30

    const-string v0, "Interstitial pool created at %s."

    invoke-static {v0, v7}, Lcom/google/android/gms/internal/zzdy;->zza(Ljava/lang/String;Lcom/google/android/gms/internal/zzdz;)V

    new-instance v8, Lcom/google/android/gms/internal/zzea;

    invoke-direct {v8, p1, p2, v6}, Lcom/google/android/gms/internal/zzea;-><init>(Lcom/google/android/gms/ads/internal/client/AdRequestParcel;Ljava/lang/String;I)V

    iget-object v0, p0, Lcom/google/android/gms/internal/zzdy;->zzAx:Ljava/util/Map;

    invoke-interface {v0, v7, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_30
    iget-object v0, p0, Lcom/google/android/gms/internal/zzdy;->zzAy:Ljava/util/LinkedList;

    invoke-virtual {v0, v7}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/gms/internal/zzdy;->zzAy:Ljava/util/LinkedList;

    invoke-virtual {v0, v7}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v7}, Lcom/google/android/gms/internal/zzdz;->zzeg()V

    :goto_3d
    iget-object v0, p0, Lcom/google/android/gms/internal/zzdy;->zzAy:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    sget-object v1, Lcom/google/android/gms/internal/zzbt;->zzwG:Lcom/google/android/gms/internal/zzbp;

    invoke-virtual {v1}, Lcom/google/android/gms/internal/zzbp;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-le v0, v1, :cond_7e

    iget-object v0, p0, Lcom/google/android/gms/internal/zzdy;->zzAy:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->remove()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lcom/google/android/gms/internal/zzdz;

    iget-object v0, p0, Lcom/google/android/gms/internal/zzdy;->zzAx:Ljava/util/Map;

    invoke-interface {v0, v9}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Lcom/google/android/gms/internal/zzea;

    const-string v0, "Evicting interstitial queue for %s."

    invoke-static {v0, v9}, Lcom/google/android/gms/internal/zzdy;->zza(Ljava/lang/String;Lcom/google/android/gms/internal/zzdz;)V

    :goto_68
    invoke-virtual {v10}, Lcom/google/android/gms/internal/zzea;->size()I

    move-result v0

    if-lez v0, :cond_78

    invoke-virtual {v10}, Lcom/google/android/gms/internal/zzea;->zzej()Lcom/google/android/gms/internal/zzea$zza;

    move-result-object v11

    iget-object v0, v11, Lcom/google/android/gms/internal/zzea$zza;->zzAD:Lcom/google/android/gms/ads/internal/zzk;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/internal/zzk;->zzbp()V

    goto :goto_68

    :cond_78
    iget-object v0, p0, Lcom/google/android/gms/internal/zzdy;->zzAx:Ljava/util/Map;

    invoke-interface {v0, v9}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_3d

    :cond_7e
    :goto_7e
    invoke-virtual {v8}, Lcom/google/android/gms/internal/zzea;->size()I

    move-result v0

    if-lez v0, :cond_b8

    invoke-virtual {v8}, Lcom/google/android/gms/internal/zzea;->zzej()Lcom/google/android/gms/internal/zzea$zza;

    move-result-object v9

    iget-boolean v0, v9, Lcom/google/android/gms/internal/zzea$zza;->zzAG:Z

    if-eqz v0, :cond_b2

    invoke-static {}, Lcom/google/android/gms/ads/internal/zzr;->zzbG()Lcom/google/android/gms/internal/zzmq;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/internal/zzmq;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, v9, Lcom/google/android/gms/internal/zzea$zza;->zzAF:J

    sub-long v10, v0, v2

    sget-object v0, Lcom/google/android/gms/internal/zzbt;->zzwI:Lcom/google/android/gms/internal/zzbp;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/zzbp;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-long v0, v0

    const-wide/16 v2, 0x3e8

    mul-long/2addr v0, v2

    cmp-long v0, v10, v0

    if-lez v0, :cond_b2

    const-string v0, "Expired interstitial at %s."

    invoke-static {v0, v7}, Lcom/google/android/gms/internal/zzdy;->zza(Ljava/lang/String;Lcom/google/android/gms/internal/zzdz;)V

    goto :goto_7e

    :cond_b2
    const-string v0, "Pooled interstitial returned at %s."

    invoke-static {v0, v7}, Lcom/google/android/gms/internal/zzdy;->zza(Ljava/lang/String;Lcom/google/android/gms/internal/zzdz;)V

    return-object v9

    :cond_b8
    const/4 v0, 0x0

    return-object v0
.end method

.method zza(Lcom/google/android/gms/internal/zzdv;)V
    .registers 3

    iget-object v0, p0, Lcom/google/android/gms/internal/zzdy;->zzAz:Lcom/google/android/gms/internal/zzdv;

    if-nez v0, :cond_9

    iput-object p1, p0, Lcom/google/android/gms/internal/zzdy;->zzAz:Lcom/google/android/gms/internal/zzdv;

    invoke-virtual {p0}, Lcom/google/android/gms/internal/zzdy;->restore()V

    :cond_9
    return-void
.end method

.method zzee()V
    .registers 7

    iget-object v0, p0, Lcom/google/android/gms/internal/zzdy;->zzAz:Lcom/google/android/gms/internal/zzdv;

    if-nez v0, :cond_5

    return-void

    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/internal/zzdy;->zzAx:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_f
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_48

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Ljava/util/Map$Entry;

    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/google/android/gms/internal/zzdz;

    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/google/android/gms/internal/zzea;

    :goto_2a
    invoke-virtual {v5}, Lcom/google/android/gms/internal/zzea;->size()I

    move-result v0

    sget-object v1, Lcom/google/android/gms/internal/zzbt;->zzwH:Lcom/google/android/gms/internal/zzbp;

    invoke-virtual {v1}, Lcom/google/android/gms/internal/zzbp;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-ge v0, v1, :cond_47

    const-string v0, "Pooling one interstitial for %s."

    invoke-static {v0, v4}, Lcom/google/android/gms/internal/zzdy;->zza(Ljava/lang/String;Lcom/google/android/gms/internal/zzdz;)V

    iget-object v0, p0, Lcom/google/android/gms/internal/zzdy;->zzAz:Lcom/google/android/gms/internal/zzdv;

    invoke-virtual {v5, v0}, Lcom/google/android/gms/internal/zzea;->zzb(Lcom/google/android/gms/internal/zzdv;)V

    goto :goto_2a

    :cond_47
    goto :goto_f

    :cond_48
    invoke-virtual {p0}, Lcom/google/android/gms/internal/zzdy;->save()V

    return-void
.end method

.class public Lcom/itau/empresas/feature/atendimento/activity/AtendimentoActivity;
.super Lcom/itau/empresas/ui/activity/BaseActivity;
.source "AtendimentoActivity.java"


# static fields
.field private static final limit:Ljava/lang/Integer;


# instance fields
.field controller:Lcom/itau/empresas/feature/atendimento/controller/AtendimentoController;

.field private faqs:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<Lcom/itau/empresas/feature/atendimento/model/AtendimentoFAQ;>;"
        }
    .end annotation
.end field

.field private filtro:Ljava/lang/String;

.field linearCartaoAlerta:Landroid/widget/LinearLayout;

.field listaFaqs:Lcom/itau/empresas/ui/widgets/NonScrollListView;

.field logoutHelper:Lcom/itau/empresas/feature/logout/LogoutHelper;

.field mensagemAlertaView:Lcom/itau/empresas/ui/view/MensagemAlertaView;

.field root:Landroid/widget/LinearLayout;

.field toolBar:Landroid/support/v7/widget/Toolbar;

.field private topSnackbar:Lbr/com/itau/topsnackbar/TopSnackbar;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .line 62
    const/4 v0, 0x3

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoActivity;->limit:Ljava/lang/Integer;

    return-void
.end method

.method public constructor <init>()V
    .registers 2

    .line 57
    invoke-direct {p0}, Lcom/itau/empresas/ui/activity/BaseActivity;-><init>()V

    .line 64
    const-string v0, ""

    iput-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoActivity;->filtro:Ljava/lang/String;

    return-void
.end method

.method private abreLink(Ljava/lang/String;)V
    .registers 8
    .param p1, "chaveMobile"    # Ljava/lang/String;

    .line 355
    new-instance v3, Lcom/itau/empresas/api/model/MenuVO;

    invoke-direct {v3}, Lcom/itau/empresas/api/model/MenuVO;-><init>()V

    .line 356
    .local v3, "item":Lcom/itau/empresas/api/model/MenuVO;
    invoke-virtual {v3, p1}, Lcom/itau/empresas/api/model/MenuVO;->setChaveMobile(Ljava/lang/String;)V

    .line 357
    invoke-virtual {v3}, Lcom/itau/empresas/api/model/MenuVO;->getChaveMobile()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/itau/empresas/ui/util/menu/InformacaoMenu;->getInstance(Ljava/lang/String;)Lcom/itau/empresas/ui/util/menu/InformacaoMenu;

    move-result-object v4

    .line 358
    .local v4, "informacaoMenu":Lcom/itau/empresas/ui/util/menu/InformacaoMenu;
    if-nez v4, :cond_1b

    .line 359
    const-string v0, "AtendimentoActivity"

    const-string v1, "InformacaoMenu est\u00e1 nulo"

    const/4 v2, 0x6

    invoke-static {v2, v0, v1}, Lcom/crashlytics/android/Crashlytics;->log(ILjava/lang/String;Ljava/lang/String;)V

    .line 360
    return-void

    .line 362
    :cond_1b
    invoke-virtual {v4}, Lcom/itau/empresas/ui/util/menu/InformacaoMenu;->getCommand()Lcom/itau/empresas/ui/util/menu/Command;

    move-result-object v5

    .line 363
    .local v5, "command":Lcom/itau/empresas/ui/util/menu/Command;
    if-eqz v5, :cond_24

    .line 364
    invoke-interface {v5, p0, v3}, Lcom/itau/empresas/ui/util/menu/Command;->executar(Landroid/support/v7/app/AppCompatActivity;Lcom/itau/empresas/api/model/MenuVO;)V

    .line 366
    :cond_24
    return-void
.end method

.method static synthetic access$000(Lcom/itau/empresas/feature/atendimento/activity/AtendimentoActivity;)Lcom/itau/empresas/CustomApplication;
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/feature/atendimento/activity/AtendimentoActivity;

    .line 57
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoActivity;->application:Lcom/itau/empresas/CustomApplication;

    return-object v0
.end method

.method static synthetic access$100(Lcom/itau/empresas/feature/atendimento/activity/AtendimentoActivity;Ljava/lang/String;)V
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/feature/atendimento/activity/AtendimentoActivity;
    .param p1, "x1"    # Ljava/lang/String;

    .line 57
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoActivity;->abreLink(Ljava/lang/String;)V

    return-void
.end method

.method private carregaAdapterFaqs(Ljava/util/ArrayList;)V
    .registers 4
    .param p1, "faqs"    # Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/ArrayList<Lcom/itau/empresas/feature/atendimento/model/AtendimentoFAQ;>;)V"
        }
    .end annotation

    .line 169
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoActivity;->listaFaqs:Lcom/itau/empresas/ui/widgets/NonScrollListView;

    new-instance v1, Lcom/itau/empresas/feature/atendimento/adapter/AtendimentoFaqsAdapter;

    invoke-direct {v1, p0, p1}, Lcom/itau/empresas/feature/atendimento/adapter/AtendimentoFaqsAdapter;-><init>(Landroid/content/Context;Ljava/util/ArrayList;)V

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/widgets/NonScrollListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 170
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoActivity;->listaFaqs:Lcom/itau/empresas/ui/widgets/NonScrollListView;

    new-instance v1, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoActivity$1;

    invoke-direct {v1, p0, p1}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoActivity$1;-><init>(Lcom/itau/empresas/feature/atendimento/activity/AtendimentoActivity;Ljava/util/ArrayList;)V

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/widgets/NonScrollListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 183
    return-void
.end method

.method private carregaAlertas()V
    .registers 2

    .line 120
    invoke-direct {p0}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoActivity;->carregarListaAlertas()V

    .line 121
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoActivity;->toolBar:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoActivity;->setSupportActionBar(Landroid/support/v7/widget/Toolbar;)V

    .line 122
    return-void
.end method

.method private carregaFaqs()V
    .registers 3

    .line 234
    sget-object v0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoActivity;->limit:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoActivity;->carregarListaPerguntas(II)V

    .line 235
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoActivity;->toolBar:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoActivity;->setSupportActionBar(Landroid/support/v7/widget/Toolbar;)V

    .line 236
    return-void
.end method

.method private carregarListaAlertas()V
    .registers 3

    .line 115
    invoke-virtual {p0}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoActivity;->mostraDialogoDeProgresso()V

    .line 116
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoActivity;->controller:Lcom/itau/empresas/feature/atendimento/controller/AtendimentoController;

    const v1, 0x7f07036f

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/atendimento/controller/AtendimentoController;->buscaAlertas(Ljava/lang/String;)V

    .line 117
    return-void
.end method

.method private carregarListaPerguntas(II)V
    .registers 9
    .param p1, "top"    # I
    .param p2, "offset"    # I

    .line 125
    invoke-virtual {p0}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoActivity;->mostraDialogoDeProgresso()V

    .line 126
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoActivity;->controller:Lcom/itau/empresas/feature/atendimento/controller/AtendimentoController;

    iget-object v2, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoActivity;->filtro:Ljava/lang/String;

    move v3, p1

    move v4, p2

    const-string v5, "true"

    const/4 v1, 0x0

    invoke-virtual/range {v0 .. v5}, Lcom/itau/empresas/feature/atendimento/controller/AtendimentoController;->buscaFaqs(Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;)V

    .line 127
    return-void
.end method

.method private constroiToolbar()V
    .registers 3

    .line 96
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoActivity;->toolBar:Landroid/support/v7/widget/Toolbar;

    invoke-static {v0, p0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom;->Builder(Landroid/support/v7/widget/Toolbar;Landroid/support/v7/app/AppCompatActivity;)Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    move-result-object v0

    .line 98
    const v1, 0x7f0706f9

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->comTitulo(I)Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    move-result-object v0

    .line 99
    invoke-virtual {v0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->comClickListenerFinish()Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    move-result-object v0

    .line 100
    invoke-virtual {v0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->build()Lcom/itau/empresas/ui/util/listener/ToolbarCustom;

    .line 101
    return-void
.end method

.method private irParaAtendimento()V
    .registers 2

    .line 347
    const-string v0, "tabbar_ajuda"

    invoke-direct {p0, v0}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoActivity;->abreLink(Ljava/lang/String;)V

    .line 348
    return-void
.end method

.method private irParaContaCorrente()V
    .registers 2

    .line 351
    const-string v0, "tabbar_extrato"

    invoke-direct {p0, v0}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoActivity;->abreLink(Ljava/lang/String;)V

    .line 352
    return-void
.end method

.method private irParaFaqs(Lbr/com/itau/textovoz/model/Faq;)V
    .registers 8
    .param p1, "faq"    # Lbr/com/itau/textovoz/model/Faq;

    .line 369
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 370
    .local v2, "channels":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/itau/empresas/feature/atendimento/model/AtendimentoCanal;>;"
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 371
    .local v3, "links":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/itau/empresas/feature/atendimento/model/AtendimentoLink;>;"
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 373
    .local v4, "relacionadas":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/itau/empresas/feature/atendimento/model/AtendimentoRelacionadas;>;"
    invoke-static {p1, v2}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoActivity;->preparaCanais(Lbr/com/itau/textovoz/model/Faq;Ljava/util/ArrayList;)V

    .line 374
    invoke-static {p1, v3}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoActivity;->preparaLinks(Lbr/com/itau/textovoz/model/Faq;Ljava/util/ArrayList;)V

    .line 375
    invoke-static {p1, v2, v3, v4}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoActivity;->preparaFaq(Lbr/com/itau/textovoz/model/Faq;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;)Lcom/itau/empresas/feature/atendimento/model/AtendimentoFAQ;

    move-result-object v5

    .line 377
    .local v5, "atendimentoFAQ":Lcom/itau/empresas/feature/atendimento/model/AtendimentoFAQ;
    invoke-static {p0}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity;->intent(Landroid/content/Context;)Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity_$IntentBuilder_;

    move-result-object v0

    const-string v1, "FAQ"

    .line 378
    invoke-virtual {v0, v1, v5}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity_$IntentBuilder_;->extra(Ljava/lang/String;Ljava/io/Serializable;)Lorg/androidannotations/api/builder/IntentBuilder;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity_$IntentBuilder_;

    .line 379
    invoke-virtual {v0}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity_$IntentBuilder_;->start()Lorg/androidannotations/api/builder/PostActivityStarter;

    .line 380
    return-void
.end method

.method private obterMenuAcesso(Ljava/util/List;)Ljava/util/ArrayList;
    .registers 7
    .param p1, "menuVOListApplication"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<Lcom/itau/empresas/api/model/MenuVO;>;)Ljava/util/ArrayList<Lbr/com/itau/textovoz/model/Menu;>;"
        }
    .end annotation

    .line 275
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 277
    .local v2, "listaMenuComponente":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lbr/com/itau/textovoz/model/Menu;>;"
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_6
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v3, v0, :cond_72

    .line 278
    const-string v0, "in\u00edcio"

    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/itau/empresas/api/model/MenuVO;

    invoke-virtual {v1}, Lcom/itau/empresas/api/model/MenuVO;->getNome()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_6e

    .line 279
    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/api/model/MenuVO;

    invoke-virtual {v0}, Lcom/itau/empresas/api/model/MenuVO;->getFilhos()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_4c

    .line 280
    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/api/model/MenuVO;

    invoke-virtual {v0}, Lcom/itau/empresas/api/model/MenuVO;->getFilhos()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_4c

    .line 282
    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/api/model/MenuVO;

    invoke-virtual {v0}, Lcom/itau/empresas/api/model/MenuVO;->getFilhos()Ljava/util/List;

    move-result-object v4

    .line 283
    .local v4, "menuFilhos":Ljava/util/List;, "Ljava/util/List<Lcom/itau/empresas/api/model/MenuVO;>;"
    invoke-direct {p0, v4}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoActivity;->retornaMenuFilhos(Ljava/util/List;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 284
    .end local v4    # "menuFilhos":Ljava/util/List;, "Ljava/util/List<Lcom/itau/empresas/api/model/MenuVO;>;"
    .end local v4
    goto :goto_6e

    .line 286
    :cond_4c
    new-instance v4, Lbr/com/itau/textovoz/model/Menu;

    invoke-direct {v4}, Lbr/com/itau/textovoz/model/Menu;-><init>()V

    .line 287
    .local v4, "menu":Lbr/com/itau/textovoz/model/Menu;
    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/api/model/MenuVO;

    invoke-virtual {v0}, Lcom/itau/empresas/api/model/MenuVO;->getNome()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Lbr/com/itau/textovoz/model/Menu;->setName(Ljava/lang/String;)V

    .line 288
    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/api/model/MenuVO;

    invoke-virtual {v0}, Lcom/itau/empresas/api/model/MenuVO;->getChaveMobile()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Lbr/com/itau/textovoz/model/Menu;->setOpKey(Ljava/lang/String;)V

    .line 289
    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 277
    .end local v4    # "menu":Lbr/com/itau/textovoz/model/Menu;
    :cond_6e
    :goto_6e
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_6

    .line 293
    .end local v3    # "i":I
    :cond_72
    return-object v2
.end method

.method private static preparaCanais(Lbr/com/itau/textovoz/model/Faq;Ljava/util/ArrayList;)V
    .registers 6
    .param p0, "faq"    # Lbr/com/itau/textovoz/model/Faq;
    .param p1, "channels"    # Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Lbr/com/itau/textovoz/model/Faq;Ljava/util/ArrayList<Lcom/itau/empresas/feature/atendimento/model/AtendimentoCanal;>;)V"
        }
    .end annotation

    .line 394
    invoke-virtual {p0}, Lbr/com/itau/textovoz/model/Faq;->getChannels()Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_4a

    invoke-virtual {p0}, Lbr/com/itau/textovoz/model/Faq;->getChannels()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_4a

    .line 395
    invoke-virtual {p0}, Lbr/com/itau/textovoz/model/Faq;->getChannels()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_18
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4a

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lbr/com/itau/textovoz/model/Channel;

    .line 396
    .local v2, "channel":Lbr/com/itau/textovoz/model/Channel;
    new-instance v3, Lcom/itau/empresas/feature/atendimento/model/AtendimentoCanal;

    invoke-direct {v3}, Lcom/itau/empresas/feature/atendimento/model/AtendimentoCanal;-><init>()V

    .line 397
    .local v3, "ch":Lcom/itau/empresas/feature/atendimento/model/AtendimentoCanal;
    invoke-virtual {v2}, Lbr/com/itau/textovoz/model/Channel;->getChannel()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/itau/empresas/feature/atendimento/model/AtendimentoCanal;->setCanal(Ljava/lang/String;)V

    .line 398
    invoke-virtual {v2}, Lbr/com/itau/textovoz/model/Channel;->getExplanation()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/itau/empresas/feature/atendimento/model/AtendimentoCanal;->setTextoExplicativo(Ljava/lang/String;)V

    .line 399
    invoke-virtual {v2}, Lbr/com/itau/textovoz/model/Channel;->getUrl()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/itau/empresas/feature/atendimento/model/AtendimentoCanal;->setUrl(Ljava/lang/String;)V

    .line 400
    invoke-virtual {v2}, Lbr/com/itau/textovoz/model/Channel;->getIcon()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/itau/empresas/feature/atendimento/model/AtendimentoCanal;->setIcone(Ljava/lang/String;)V

    .line 401
    invoke-virtual {p1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 402
    .end local v2    # "channel":Lbr/com/itau/textovoz/model/Channel;
    .end local v3    # "ch":Lcom/itau/empresas/feature/atendimento/model/AtendimentoCanal;
    goto :goto_18

    .line 404
    :cond_4a
    return-void
.end method

.method private static preparaFaq(Lbr/com/itau/textovoz/model/Faq;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;)Lcom/itau/empresas/feature/atendimento/model/AtendimentoFAQ;
    .registers 6
    .param p0, "faq"    # Lbr/com/itau/textovoz/model/Faq;
    .param p1, "channels"    # Ljava/util/ArrayList;
    .param p2, "links"    # Ljava/util/ArrayList;
    .param p3, "relacionadas"    # Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Lbr/com/itau/textovoz/model/Faq;Ljava/util/ArrayList<Lcom/itau/empresas/feature/atendimento/model/AtendimentoCanal;>;Ljava/util/ArrayList<Lcom/itau/empresas/feature/atendimento/model/AtendimentoLink;>;Ljava/util/ArrayList<Lcom/itau/empresas/feature/atendimento/model/AtendimentoRelacionadas;>;)Lcom/itau/empresas/feature/atendimento/model/AtendimentoFAQ;"
        }
    .end annotation

    .line 410
    new-instance v1, Lcom/itau/empresas/feature/atendimento/model/AtendimentoFAQ;

    invoke-direct {v1}, Lcom/itau/empresas/feature/atendimento/model/AtendimentoFAQ;-><init>()V

    .line 411
    .local v1, "atendimentoFAQ":Lcom/itau/empresas/feature/atendimento/model/AtendimentoFAQ;
    invoke-virtual {p0}, Lbr/com/itau/textovoz/model/Faq;->getQuestion()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/itau/empresas/feature/atendimento/model/AtendimentoFAQ;->setPergunta(Ljava/lang/String;)V

    .line 412
    invoke-virtual {p0}, Lbr/com/itau/textovoz/model/Faq;->getAnswer()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/itau/empresas/feature/atendimento/model/AtendimentoFAQ;->setResposta(Ljava/lang/String;)V

    .line 413
    invoke-virtual {p0}, Lbr/com/itau/textovoz/model/Faq;->getProduct()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/itau/empresas/feature/atendimento/model/AtendimentoFAQ;->setAssunto(Ljava/lang/String;)V

    .line 414
    invoke-virtual {v1, p1}, Lcom/itau/empresas/feature/atendimento/model/AtendimentoFAQ;->setCanaisDisponiveis(Ljava/util/ArrayList;)V

    .line 415
    invoke-virtual {v1, p2}, Lcom/itau/empresas/feature/atendimento/model/AtendimentoFAQ;->setLinks(Ljava/util/ArrayList;)V

    .line 416
    invoke-virtual {v1, p3}, Lcom/itau/empresas/feature/atendimento/model/AtendimentoFAQ;->setMobileRelacionadas(Ljava/util/ArrayList;)V

    .line 417
    return-object v1
.end method

.method private static preparaLinks(Lbr/com/itau/textovoz/model/Faq;Ljava/util/ArrayList;)V
    .registers 6
    .param p0, "faq"    # Lbr/com/itau/textovoz/model/Faq;
    .param p1, "links"    # Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Lbr/com/itau/textovoz/model/Faq;Ljava/util/ArrayList<Lcom/itau/empresas/feature/atendimento/model/AtendimentoLink;>;)V"
        }
    .end annotation

    .line 383
    invoke-virtual {p0}, Lbr/com/itau/textovoz/model/Faq;->getLinks()Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_3c

    invoke-virtual {p0}, Lbr/com/itau/textovoz/model/Faq;->getLinks()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_3c

    .line 384
    invoke-virtual {p0}, Lbr/com/itau/textovoz/model/Faq;->getLinks()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_18
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3c

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lbr/com/itau/textovoz/model/Link;

    .line 385
    .local v2, "link":Lbr/com/itau/textovoz/model/Link;
    new-instance v3, Lcom/itau/empresas/feature/atendimento/model/AtendimentoLink;

    invoke-direct {v3}, Lcom/itau/empresas/feature/atendimento/model/AtendimentoLink;-><init>()V

    .line 386
    .local v3, "ch":Lcom/itau/empresas/feature/atendimento/model/AtendimentoLink;
    invoke-virtual {v2}, Lbr/com/itau/textovoz/model/Link;->get_ref()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/itau/empresas/feature/atendimento/model/AtendimentoLink;->setRef(Ljava/lang/String;)V

    .line 387
    invoke-virtual {v2}, Lbr/com/itau/textovoz/model/Link;->getText()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/itau/empresas/feature/atendimento/model/AtendimentoLink;->setTexto(Ljava/lang/String;)V

    .line 388
    invoke-virtual {p1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 389
    .end local v2    # "link":Lbr/com/itau/textovoz/model/Link;
    .end local v3    # "ch":Lcom/itau/empresas/feature/atendimento/model/AtendimentoLink;
    goto :goto_18

    .line 391
    :cond_3c
    return-void
.end method

.method private retornaMenuFilhos(Ljava/util/List;)Ljava/util/ArrayList;
    .registers 6
    .param p1, "menuList"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<Lcom/itau/empresas/api/model/MenuVO;>;)Ljava/util/ArrayList<Lbr/com/itau/textovoz/model/Menu;>;"
        }
    .end annotation

    .line 297
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 298
    .local v1, "retorno":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lbr/com/itau/textovoz/model/Menu;>;"
    const/4 v2, 0x0

    .local v2, "j":I
    :goto_6
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_31

    .line 299
    new-instance v3, Lbr/com/itau/textovoz/model/Menu;

    invoke-direct {v3}, Lbr/com/itau/textovoz/model/Menu;-><init>()V

    .line 300
    .local v3, "menu":Lbr/com/itau/textovoz/model/Menu;
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/api/model/MenuVO;

    invoke-virtual {v0}, Lcom/itau/empresas/api/model/MenuVO;->getNome()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lbr/com/itau/textovoz/model/Menu;->setName(Ljava/lang/String;)V

    .line 301
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/api/model/MenuVO;

    invoke-virtual {v0}, Lcom/itau/empresas/api/model/MenuVO;->getChaveMobile()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lbr/com/itau/textovoz/model/Menu;->setOpKey(Ljava/lang/String;)V

    .line 302
    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 298
    .end local v3    # "menu":Lbr/com/itau/textovoz/model/Menu;
    add-int/lit8 v2, v2, 0x1

    goto :goto_6

    .line 304
    .end local v2    # "j":I
    :cond_31
    return-object v1
.end method

.method private setListaFaqs(Ljava/util/ArrayList;)V
    .registers 2
    .param p1, "retornoFaqs"    # Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/ArrayList<Lcom/itau/empresas/feature/atendimento/model/AtendimentoFAQ;>;)V"
        }
    .end annotation

    .line 130
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoActivity;->carregaAdapterFaqs(Ljava/util/ArrayList;)V

    .line 131
    return-void
.end method

.method private validaSegmento()V
    .registers 2

    .line 110
    invoke-virtual {p0}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoActivity;->mostraDialogoDeProgresso()V

    .line 111
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoActivity;->controller:Lcom/itau/empresas/feature/atendimento/controller/AtendimentoController;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/atendimento/controller/AtendimentoController;->buscaSegmento()V

    .line 112
    return-void
.end method


# virtual methods
.method abreDuvidasPorAssunto(Landroid/view/View;)V
    .registers 7
    .param p1, "view"    # Landroid/view/View;

    .line 136
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoActivity;->application:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getEventoTracker()Lcom/itau/empresas/ui/util/analytics/TrackerEvento;

    move-result-object v0

    new-instance v1, Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;

    .line 137
    const v2, 0x7f0702db

    invoke-virtual {p0, v2}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 138
    const v3, 0x7f0702bb

    invoke-virtual {p0, v3}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 139
    const v4, 0x7f07032d

    invoke-virtual {p0, v4}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 136
    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/analytics/TrackerEvento;->enviarHitEvento(Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;)V

    .line 141
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoAssuntoActivity;->intent(Landroid/content/Context;)Lcom/itau/empresas/feature/atendimento/activity/AtendimentoAssuntoActivity_$IntentBuilder_;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoAssuntoActivity_$IntentBuilder_;->start()Lorg/androidannotations/api/builder/PostActivityStarter;

    .line 142
    return-void
.end method

.method carregandoElementosDeTela()V
    .registers 6

    .line 81
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoActivity;->application:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getEventoTracker()Lcom/itau/empresas/ui/util/analytics/TrackerEvento;

    move-result-object v0

    new-instance v1, Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;

    .line 82
    const v2, 0x7f0702db

    invoke-virtual {p0, v2}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 83
    const v3, 0x7f070294

    invoke-virtual {p0, v3}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 84
    const v4, 0x7f0702d5

    invoke-virtual {p0, v4}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 81
    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/analytics/TrackerEvento;->enviarHitEvento(Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;)V

    .line 86
    invoke-direct {p0}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoActivity;->validaSegmento()V

    .line 87
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoActivity;->application:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getContextoAtendimento()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_31

    const-string v0, ""

    goto :goto_37

    :cond_31
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoActivity;->application:Lcom/itau/empresas/CustomApplication;

    .line 89
    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getContextoAtendimento()Ljava/lang/String;

    move-result-object v0

    :goto_37
    iput-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoActivity;->filtro:Ljava/lang/String;

    .line 90
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoActivity;->faqs:Ljava/util/ArrayList;

    .line 92
    invoke-direct {p0}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoActivity;->constroiToolbar()V

    .line 93
    return-void
.end method

.method direcionaWebView()V
    .registers 3

    .line 104
    const-string v0, "NET_ATENDIMENTO_PJ"

    .line 105
    const v1, 0x7f0706f9

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 104
    invoke-static {p0, v0, v1}, Lcom/itau/empresas/ui/activity/WebViewActivity;->iniciarWebViewActivity(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 106
    invoke-virtual {p0}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoActivity;->escondeDialogoDeProgresso()V

    .line 107
    return-void
.end method

.method public hitAnalytics()Ljava/lang/String;
    .registers 2

    .line 309
    const v0, 0x7f0706f9

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected irParaBusca()V
    .registers 8

    .line 147
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoActivity;->application:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getMenuVO()Lcom/itau/empresas/api/model/MenuVO;

    move-result-object v0

    if-nez v0, :cond_e

    .line 148
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoActivity;->logoutHelper:Lcom/itau/empresas/feature/logout/LogoutHelper;

    invoke-virtual {v0, p0}, Lcom/itau/empresas/feature/logout/LogoutHelper;->trataSessaoExpirada(Landroid/app/Activity;)V

    .line 149
    return-void

    .line 152
    :cond_e
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoActivity;->application:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getEventoTracker()Lcom/itau/empresas/ui/util/analytics/TrackerEvento;

    move-result-object v0

    new-instance v1, Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;

    .line 153
    const v2, 0x7f0702db

    invoke-virtual {p0, v2}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 154
    const v3, 0x7f070299

    invoke-virtual {p0, v3}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 155
    const v4, 0x7f0702d6

    invoke-virtual {p0, v4}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 152
    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/analytics/TrackerEvento;->enviarHitEvento(Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;)V

    .line 157
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoActivity;->application:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getMenuVO()Lcom/itau/empresas/api/model/MenuVO;

    move-result-object v0

    .line 158
    invoke-virtual {v0}, Lcom/itau/empresas/api/model/MenuVO;->getFilhos()Ljava/util/List;

    move-result-object v0

    .line 157
    invoke-direct {p0, v0}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoActivity;->obterMenuAcesso(Ljava/util/List;)Ljava/util/ArrayList;

    move-result-object v5

    .line 160
    .local v5, "listaMenuComponente":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lbr/com/itau/textovoz/model/Menu;>;"
    new-instance v6, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;

    invoke-direct {v6, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 161
    .local v6, "intent":Landroid/content/Intent;
    const-string v0, "operator"

    iget-object v1, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoActivity;->application:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v1}, Lcom/itau/empresas/CustomApplication;->getCodigoOperador()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 162
    const-string v0, "menu"

    invoke-virtual {v6, v0, v5}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 163
    const-string v0, "type"

    const-string v1, "0"

    invoke-virtual {v6, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 164
    const/16 v0, 0x64

    invoke-virtual {p0, v6, v0}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 165
    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .registers 10
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .line 319
    invoke-super {p0, p1, p2, p3}, Lcom/itau/empresas/ui/activity/BaseActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 321
    const/16 v0, 0x64

    if-ne p1, v0, :cond_59

    const/4 v0, -0x1

    if-ne p2, v0, :cond_59

    .line 322
    const v0, 0x7f070209

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p3, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    .line 324
    .local v2, "objectExtract":Z
    if-eqz v2, :cond_1c

    .line 325
    invoke-direct {p0}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoActivity;->irParaContaCorrente()V

    .line 326
    return-void

    .line 328
    :cond_1c
    const v0, 0x7f070126

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p3, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    .line 330
    .local v3, "objectCardViewTel":Z
    if-eqz v3, :cond_2e

    .line 331
    invoke-direct {p0}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoActivity;->irParaAtendimento()V

    .line 332
    return-void

    .line 334
    :cond_2e
    const v0, 0x7f070475

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lbr/com/itau/textovoz/model/Menu;

    .line 335
    .local v4, "menu":Lbr/com/itau/textovoz/model/Menu;
    if-eqz v4, :cond_46

    .line 336
    invoke-virtual {v4}, Lbr/com/itau/textovoz/model/Menu;->getOpKey()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoActivity;->abreLink(Ljava/lang/String;)V

    .line 337
    return-void

    .line 339
    :cond_46
    const v0, 0x7f070211

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lbr/com/itau/textovoz/model/Faq;

    .line 340
    .local v5, "faq":Lbr/com/itau/textovoz/model/Faq;
    if-eqz v5, :cond_59

    .line 341
    invoke-direct {p0, v5}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoActivity;->irParaFaqs(Lbr/com/itau/textovoz/model/Faq;)V

    .line 344
    .end local v2    # "objectExtract":Z
    .end local v3    # "objectCardViewTel":Z
    .end local v4    # "menu":Lbr/com/itau/textovoz/model/Menu;
    .end local v5    # "faq":Lbr/com/itau/textovoz/model/Faq;
    :cond_59
    return-void
.end method

.method public onEventMainThread(Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent$Conversion;)V
    .registers 5
    .param p1, "e"    # Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent$Conversion;

    .line 246
    invoke-static {p1, p0}, Lcom/itau/empresas/api/ApiHelper;->devoTratarEvento(Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;Lcom/itau/empresas/api/ApiConsumidor;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 247
    return-void

    .line 248
    :cond_7
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoActivity;->listaFaqs:Lcom/itau/empresas/ui/widgets/NonScrollListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/widgets/NonScrollListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 249
    invoke-virtual {p0}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoActivity;->escondeDialogoDeProgresso()V

    .line 250
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoActivity;->root:Landroid/widget/LinearLayout;

    const v1, 0x7f0701f2

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lbr/com/itau/topsnackbar/TopSnackbar;->make(Landroid/view/ViewGroup;II)Lbr/com/itau/topsnackbar/TopSnackbar;

    move-result-object v0

    .line 251
    invoke-virtual {v0}, Lbr/com/itau/topsnackbar/TopSnackbar;->show()Lbr/com/itau/topsnackbar/TopSnackbar;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoActivity;->topSnackbar:Lbr/com/itau/topsnackbar/TopSnackbar;

    .line 252
    return-void
.end method

.method public onEventMainThread(Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent$Unexpected;)V
    .registers 5
    .param p1, "e"    # Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent$Unexpected;

    .line 255
    invoke-static {p1, p0}, Lcom/itau/empresas/api/ApiHelper;->devoTratarEvento(Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;Lcom/itau/empresas/api/ApiConsumidor;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 256
    return-void

    .line 257
    :cond_7
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoActivity;->listaFaqs:Lcom/itau/empresas/ui/widgets/NonScrollListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/widgets/NonScrollListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 258
    invoke-virtual {p0}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoActivity;->escondeDialogoDeProgresso()V

    .line 259
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoActivity;->root:Landroid/widget/LinearLayout;

    const v1, 0x7f0701ef

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lbr/com/itau/topsnackbar/TopSnackbar;->make(Landroid/view/ViewGroup;II)Lbr/com/itau/topsnackbar/TopSnackbar;

    move-result-object v0

    .line 260
    invoke-virtual {v0}, Lbr/com/itau/topsnackbar/TopSnackbar;->show()Lbr/com/itau/topsnackbar/TopSnackbar;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoActivity;->topSnackbar:Lbr/com/itau/topsnackbar/TopSnackbar;

    .line 261
    return-void
.end method

.method public onEventMainThread(Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;)V
    .registers 5
    .param p1, "e"    # Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;

    .line 265
    invoke-static {p1, p0}, Lcom/itau/empresas/api/ApiHelper;->devoTratarEvento(Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;Lcom/itau/empresas/api/ApiConsumidor;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 266
    return-void

    .line 268
    :cond_7
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoActivity;->listaFaqs:Lcom/itau/empresas/ui/widgets/NonScrollListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/widgets/NonScrollListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 269
    invoke-virtual {p0}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoActivity;->escondeDialogoDeProgresso()V

    .line 270
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoActivity;->root:Landroid/widget/LinearLayout;

    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;->getException()Lbr/com/itau/sdk/android/core/exception/BackendException;

    move-result-object v1

    invoke-virtual {v1}, Lbr/com/itau/sdk/android/core/exception/BackendException;->getError()Lbr/com/itau/sdk/android/core/model/ErrorDTO;

    move-result-object v1

    invoke-virtual {v1}, Lbr/com/itau/sdk/android/core/model/ErrorDTO;->getMensagem()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lbr/com/itau/topsnackbar/TopSnackbar;->make(Landroid/view/ViewGroup;Ljava/lang/CharSequence;I)Lbr/com/itau/topsnackbar/TopSnackbar;

    move-result-object v0

    .line 271
    invoke-virtual {v0}, Lbr/com/itau/topsnackbar/TopSnackbar;->show()Lbr/com/itau/topsnackbar/TopSnackbar;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoActivity;->topSnackbar:Lbr/com/itau/topsnackbar/TopSnackbar;

    .line 272
    return-void
.end method

.method public onEventMainThread(Lcom/itau/empresas/api/model/DadosClienteEmpresaVO;)V
    .registers 4
    .param p1, "dadosClienteEmpresaVO"    # Lcom/itau/empresas/api/model/DadosClienteEmpresaVO;

    .line 190
    invoke-virtual {p1}, Lcom/itau/empresas/api/model/DadosClienteEmpresaVO;->getSegmento()Ljava/lang/String;

    move-result-object v0

    const-string v1, "EMP2"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_28

    .line 191
    invoke-virtual {p1}, Lcom/itau/empresas/api/model/DadosClienteEmpresaVO;->getSegmento()Ljava/lang/String;

    move-result-object v0

    const-string v1, "EMP3"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_28

    .line 192
    invoke-virtual {p1}, Lcom/itau/empresas/api/model/DadosClienteEmpresaVO;->getSegmento()Ljava/lang/String;

    move-result-object v0

    const-string v1, "EMP4"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_28

    .line 193
    invoke-virtual {p0}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoActivity;->direcionaWebView()V

    goto :goto_2e

    .line 196
    :cond_28
    invoke-direct {p0}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoActivity;->carregaAlertas()V

    .line 197
    invoke-direct {p0}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoActivity;->carregaFaqs()V

    .line 199
    :goto_2e
    return-void
.end method

.method public onEventMainThread(Lcom/itau/empresas/feature/atendimento/model/AlertaVO;)V
    .registers 6
    .param p1, "alerta"    # Lcom/itau/empresas/feature/atendimento/model/AlertaVO;

    .line 202
    invoke-virtual {p1}, Lcom/itau/empresas/feature/atendimento/model/AlertaVO;->getAlertas()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_56

    invoke-virtual {p1}, Lcom/itau/empresas/feature/atendimento/model/AlertaVO;->getAlertas()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_56

    .line 204
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoActivity;->mensagemAlertaView:Lcom/itau/empresas/ui/view/MensagemAlertaView;

    invoke-virtual {p1}, Lcom/itau/empresas/feature/atendimento/model/AlertaVO;->getAlertas()Ljava/util/List;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/itau/empresas/feature/atendimento/model/AlertaFAQ;

    invoke-virtual {v1}, Lcom/itau/empresas/feature/atendimento/model/AlertaFAQ;->getMensagem_resumida()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/view/MensagemAlertaView;->setMensagem(Ljava/lang/String;)V

    .line 205
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoActivity;->mensagemAlertaView:Lcom/itau/empresas/ui/view/MensagemAlertaView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/view/MensagemAlertaView;->exibeWarning(Z)V

    .line 207
    invoke-virtual {p1}, Lcom/itau/empresas/feature/atendimento/model/AlertaVO;->getAlertas()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/feature/atendimento/model/AlertaFAQ;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/atendimento/model/AlertaFAQ;->getLink_botao_mobile()Ljava/lang/String;

    move-result-object v3

    .line 209
    .local v3, "link":Ljava/lang/String;
    if-eqz v3, :cond_4c

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_4c

    .line 210
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoActivity;->linearCartaoAlerta:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoActivity$2;

    invoke-direct {v1, p0, v3}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoActivity$2;-><init>(Lcom/itau/empresas/feature/atendimento/activity/AtendimentoActivity;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_56

    .line 218
    :cond_4c
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoActivity;->linearCartaoAlerta:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoActivity$3;

    invoke-direct {v1, p0, p1}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoActivity$3;-><init>(Lcom/itau/empresas/feature/atendimento/activity/AtendimentoActivity;Lcom/itau/empresas/feature/atendimento/model/AlertaVO;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 231
    .end local v3    # "link":Ljava/lang/String;
    :cond_56
    :goto_56
    return-void
.end method

.method public onEventMainThread(Lcom/itau/empresas/feature/atendimento/model/AtendimentoVO;)V
    .registers 3
    .param p1, "atendimento"    # Lcom/itau/empresas/feature/atendimento/model/AtendimentoVO;

    .line 239
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoActivity;->topSnackbar:Lbr/com/itau/topsnackbar/TopSnackbar;

    if-eqz v0, :cond_9

    .line 240
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoActivity;->topSnackbar:Lbr/com/itau/topsnackbar/TopSnackbar;

    invoke-virtual {v0}, Lbr/com/itau/topsnackbar/TopSnackbar;->dismiss()V

    .line 241
    :cond_9
    invoke-virtual {p0}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoActivity;->escondeDialogoDeProgresso()V

    .line 242
    invoke-virtual {p1}, Lcom/itau/empresas/feature/atendimento/model/AtendimentoVO;->getFaqs()Ljava/util/ArrayList;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoActivity;->setListaFaqs(Ljava/util/ArrayList;)V

    .line 243
    return-void
.end method

.method public opkeysConsumidas()Ljava/util/List;
    .registers 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Ljava/lang/String;>;"
        }
    .end annotation

    .line 314
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "atendimentosFaqsPJ"

    const/4 v2, 0x0

    aput-object v1, v0, v2

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

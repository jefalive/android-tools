.class public Lcom/itau/empresas/feature/credito/produtos/activity/ProdutosActivity;
.super Lcom/itau/empresas/ui/activity/BaseActivity;
.source "ProdutosActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/itau/empresas/feature/credito/produtos/activity/ProdutosActivity$SectionsPagerAdapter;
    }
.end annotation


# instance fields
.field multiLimiteVO:Lcom/itau/empresas/feature/credito/produtos/model/MultiLimiteVO;

.field toolBar:Landroid/support/v7/widget/Toolbar;

.field widgetProdutosTablayout:Landroid/support/design/widget/TabLayout;

.field widgetProdutosViewpager:Lcom/itau/empresas/ui/util/CustomViewPager;


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 25
    invoke-direct {p0}, Lcom/itau/empresas/ui/activity/BaseActivity;-><init>()V

    return-void
.end method


# virtual methods
.method carregandoElementosDeTela()V
    .registers 5

    .line 41
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/activity/ProdutosActivity;->toolBar:Landroid/support/v7/widget/Toolbar;

    const v1, 0x7f0e05c2

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Landroid/widget/TextView;

    .line 42
    .local v3, "tituloToolbar":Landroid/widget/TextView;
    const v0, 0x7f070729

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(I)V

    .line 43
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/activity/ProdutosActivity;->widgetProdutosViewpager:Lcom/itau/empresas/ui/util/CustomViewPager;

    new-instance v1, Lcom/itau/empresas/feature/credito/produtos/activity/ProdutosActivity$SectionsPagerAdapter;

    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/produtos/activity/ProdutosActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lcom/itau/empresas/feature/credito/produtos/activity/ProdutosActivity$SectionsPagerAdapter;-><init>(Lcom/itau/empresas/feature/credito/produtos/activity/ProdutosActivity;Landroid/support/v4/app/FragmentManager;)V

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/CustomViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 44
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/activity/ProdutosActivity;->widgetProdutosTablayout:Landroid/support/design/widget/TabLayout;

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/produtos/activity/ProdutosActivity;->widgetProdutosViewpager:Lcom/itau/empresas/ui/util/CustomViewPager;

    invoke-virtual {v0, v1}, Landroid/support/design/widget/TabLayout;->setupWithViewPager(Landroid/support/v4/view/ViewPager;)V

    .line 45
    return-void
.end method

.method public opkeysConsumidas()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Ljava/lang/String;>;"
        }
    .end annotation

    .line 49
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    return-object v0
.end method

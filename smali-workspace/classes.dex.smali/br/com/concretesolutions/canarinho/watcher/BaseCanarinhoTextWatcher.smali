.class public abstract Lbr/com/concretesolutions/canarinho/watcher/BaseCanarinhoTextWatcher;
.super Ljava/lang/Object;
.source "BaseCanarinhoTextWatcher.java"

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field private eventoDeValidacao:Lbr/com/concretesolutions/canarinho/watcher/evento/EventoDeValidacao;

.field private mudancaInterna:Z

.field private tamanhoAnterior:I


# direct methods
.method public constructor <init>()V
    .registers 2

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbr/com/concretesolutions/canarinho/watcher/BaseCanarinhoTextWatcher;->mudancaInterna:Z

    .line 19
    const/4 v0, 0x0

    iput v0, p0, Lbr/com/concretesolutions/canarinho/watcher/BaseCanarinhoTextWatcher;->tamanhoAnterior:I

    return-void
.end method

.method private carregarMascara(Ljava/lang/String;[C)Ljava/lang/StringBuilder;
    .registers 10
    .param p1, "s"    # Ljava/lang/String;
    .param p2, "mascara"    # [C

    .line 147
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 148
    .local v2, "builder":Ljava/lang/StringBuilder;
    sget-object v0, Lbr/com/concretesolutions/canarinho/formatador/Formatador$Padroes;->PADRAO_SOMENTE_NUMEROS:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/util/regex/Matcher;->replaceAll(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 151
    .local v3, "str":Ljava/lang/String;
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_39

    .line 152
    const/4 v4, 0x0

    .line 155
    .local v4, "j":I
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_19
    array-length v0, p2

    if-ge v5, v0, :cond_39

    .line 157
    aget-char v6, p2, v5

    .line 159
    .local v6, "charMascara":C
    const/16 v0, 0x23

    if-eq v6, v0, :cond_26

    .line 160
    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 161
    goto :goto_36

    .line 164
    :cond_26
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v0

    if-lt v4, v0, :cond_2d

    .line 165
    goto :goto_39

    .line 168
    :cond_2d
    invoke-virtual {v3, v4}, Ljava/lang/String;->charAt(I)C

    move-result v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 169
    add-int/lit8 v4, v4, 0x1

    .line 155
    .end local v6    # "charMascara":C
    :goto_36
    add-int/lit8 v5, v5, 0x1

    goto :goto_19

    .line 173
    .end local v4    # "j":I
    .end local v5    # "i":I
    :cond_39
    :goto_39
    return-object v2
.end method

.method private trataAdicaoDeCaracter(Landroid/text/Editable;[C)Ljava/lang/StringBuilder;
    .registers 4
    .param p1, "s"    # Landroid/text/Editable;
    .param p2, "mascara"    # [C

    .line 114
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Lbr/com/concretesolutions/canarinho/watcher/BaseCanarinhoTextWatcher;->carregarMascara(Ljava/lang/String;[C)Ljava/lang/StringBuilder;

    move-result-object v0

    return-object v0
.end method

.method private trataRemocaoDeCaracter(Landroid/text/Editable;[C)Ljava/lang/StringBuilder;
    .registers 9
    .param p1, "s"    # Landroid/text/Editable;
    .param p2, "mascara"    # [C

    .line 119
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, p1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 122
    .local v2, "builder":Ljava/lang/StringBuilder;
    array-length v0, p2

    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v1

    if-le v0, v1, :cond_11

    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v3

    goto :goto_14

    :cond_11
    array-length v0, p2

    add-int/lit8 v3, v0, -0x1

    .line 125
    .local v3, "posicaoUltimoCaracter":I
    :goto_14
    aget-char v0, p2, v3

    const/16 v1, 0x23

    if-eq v0, v1, :cond_1c

    const/4 v4, 0x1

    goto :goto_1d

    :cond_1c
    const/4 v4, 0x0

    .line 129
    .local v4, "ultimoCaracterEraMascara":Z
    :goto_1d
    if-eqz v4, :cond_41

    .line 130
    const/4 v5, 0x0

    .line 131
    .local v5, "encontrouCaracterValido":Z
    :goto_20
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-lez v0, :cond_41

    if-nez v5, :cond_41

    .line 132
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    aget-char v0, p2, v0

    const/16 v1, 0x23

    if-ne v0, v1, :cond_36

    const/4 v5, 0x1

    goto :goto_37

    :cond_36
    const/4 v5, 0x0

    .line 133
    :goto_37
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->deleteCharAt(I)Ljava/lang/StringBuilder;

    goto :goto_20

    .line 139
    .end local v5    # "encontrouCaracterValido":Z
    :cond_41
    :goto_41
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-lez v0, :cond_5d

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    aget-char v0, p2, v0

    const/16 v1, 0x23

    if-eq v0, v1, :cond_5d

    .line 140
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->deleteCharAt(I)Ljava/lang/StringBuilder;

    goto :goto_41

    .line 143
    :cond_5d
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Lbr/com/concretesolutions/canarinho/watcher/BaseCanarinhoTextWatcher;->carregarMascara(Ljava/lang/String;[C)Ljava/lang/StringBuilder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method protected atualizaTexto(Lbr/com/concretesolutions/canarinho/validator/Validador;Lbr/com/concretesolutions/canarinho/validator/Validador$ResultadoParcial;Landroid/text/Editable;Ljava/lang/StringBuilder;)V
    .registers 11
    .param p1, "validador"    # Lbr/com/concretesolutions/canarinho/validator/Validador;
    .param p2, "resultadoParcial"    # Lbr/com/concretesolutions/canarinho/validator/Validador$ResultadoParcial;
    .param p3, "s"    # Landroid/text/Editable;
    .param p4, "builder"    # Ljava/lang/StringBuilder;

    .line 57
    invoke-virtual {p4}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    iput v0, p0, Lbr/com/concretesolutions/canarinho/watcher/BaseCanarinhoTextWatcher;->tamanhoAnterior:I

    .line 58
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbr/com/concretesolutions/canarinho/watcher/BaseCanarinhoTextWatcher;->mudancaInterna:Z

    .line 59
    move-object v0, p3

    invoke-interface {p3}, Landroid/text/Editable;->length()I

    move-result v2

    move-object v3, p4

    invoke-virtual {p4}, Ljava/lang/StringBuilder;->length()I

    move-result v5

    const/4 v1, 0x0

    const/4 v4, 0x0

    invoke-interface/range {v0 .. v5}, Landroid/text/Editable;->replace(IILjava/lang/CharSequence;II)Landroid/text/Editable;

    .line 61
    invoke-virtual {p4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2d

    .line 63
    invoke-virtual {p4}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    invoke-static {p3, v0}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;I)V

    .line 66
    :cond_2d
    invoke-virtual {p0, p1, p2, p3}, Lbr/com/concretesolutions/canarinho/watcher/BaseCanarinhoTextWatcher;->efetuaValidacao(Lbr/com/concretesolutions/canarinho/validator/Validador;Lbr/com/concretesolutions/canarinho/validator/Validador$ResultadoParcial;Landroid/text/Editable;)V

    .line 67
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbr/com/concretesolutions/canarinho/watcher/BaseCanarinhoTextWatcher;->mudancaInterna:Z

    .line 68
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .registers 5
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "count"    # I
    .param p4, "after"    # I

    .line 25
    return-void
.end method

.method protected efetuaValidacao(Lbr/com/concretesolutions/canarinho/validator/Validador;Lbr/com/concretesolutions/canarinho/validator/Validador$ResultadoParcial;Landroid/text/Editable;)V
    .registers 7
    .param p1, "validador"    # Lbr/com/concretesolutions/canarinho/validator/Validador;
    .param p2, "resultadoParcial"    # Lbr/com/concretesolutions/canarinho/validator/Validador$ResultadoParcial;
    .param p3, "s"    # Landroid/text/Editable;

    .line 80
    if-nez p1, :cond_3

    .line 81
    return-void

    .line 84
    :cond_3
    iget-object v0, p0, Lbr/com/concretesolutions/canarinho/watcher/BaseCanarinhoTextWatcher;->eventoDeValidacao:Lbr/com/concretesolutions/canarinho/watcher/evento/EventoDeValidacao;

    if-nez v0, :cond_f

    .line 85
    invoke-virtual {p3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lbr/com/concretesolutions/canarinho/validator/Validador;->ehValido(Ljava/lang/String;)Z

    .line 86
    return-void

    .line 89
    :cond_f
    invoke-interface {p1, p3, p2}, Lbr/com/concretesolutions/canarinho/validator/Validador;->ehValido(Landroid/text/Editable;Lbr/com/concretesolutions/canarinho/validator/Validador$ResultadoParcial;)Lbr/com/concretesolutions/canarinho/validator/Validador$ResultadoParcial;

    .line 91
    invoke-virtual {p2}, Lbr/com/concretesolutions/canarinho/validator/Validador$ResultadoParcial;->isParcialmenteValido()Z

    move-result v0

    if-nez v0, :cond_26

    .line 92
    iget-object v0, p0, Lbr/com/concretesolutions/canarinho/watcher/BaseCanarinhoTextWatcher;->eventoDeValidacao:Lbr/com/concretesolutions/canarinho/watcher/evento/EventoDeValidacao;

    invoke-virtual {p3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2}, Lbr/com/concretesolutions/canarinho/validator/Validador$ResultadoParcial;->getMensagem()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lbr/com/concretesolutions/canarinho/watcher/evento/EventoDeValidacao;->invalido(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3f

    .line 93
    :cond_26
    invoke-virtual {p2}, Lbr/com/concretesolutions/canarinho/validator/Validador$ResultadoParcial;->isValido()Z

    move-result v0

    if-nez v0, :cond_36

    .line 94
    iget-object v0, p0, Lbr/com/concretesolutions/canarinho/watcher/BaseCanarinhoTextWatcher;->eventoDeValidacao:Lbr/com/concretesolutions/canarinho/watcher/evento/EventoDeValidacao;

    invoke-virtual {p3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lbr/com/concretesolutions/canarinho/watcher/evento/EventoDeValidacao;->parcialmenteValido(Ljava/lang/String;)V

    goto :goto_3f

    .line 96
    :cond_36
    iget-object v0, p0, Lbr/com/concretesolutions/canarinho/watcher/BaseCanarinhoTextWatcher;->eventoDeValidacao:Lbr/com/concretesolutions/canarinho/watcher/evento/EventoDeValidacao;

    invoke-virtual {p3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lbr/com/concretesolutions/canarinho/watcher/evento/EventoDeValidacao;->totalmenteValido(Ljava/lang/String;)V

    .line 98
    :goto_3f
    return-void
.end method

.method public getEventoDeValidacao()Lbr/com/concretesolutions/canarinho/watcher/evento/EventoDeValidacao;
    .registers 2

    .line 181
    iget-object v0, p0, Lbr/com/concretesolutions/canarinho/watcher/BaseCanarinhoTextWatcher;->eventoDeValidacao:Lbr/com/concretesolutions/canarinho/watcher/evento/EventoDeValidacao;

    return-object v0
.end method

.method protected isApagouCaracter(Landroid/text/Editable;)Z
    .registers 4
    .param p1, "s"    # Landroid/text/Editable;

    .line 40
    iget v0, p0, Lbr/com/concretesolutions/canarinho/watcher/BaseCanarinhoTextWatcher;->tamanhoAnterior:I

    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v1

    if-le v0, v1, :cond_a

    const/4 v0, 0x1

    goto :goto_b

    :cond_a
    const/4 v0, 0x0

    :goto_b
    return v0
.end method

.method public isMudancaInterna()Z
    .registers 2

    .line 177
    iget-boolean v0, p0, Lbr/com/concretesolutions/canarinho/watcher/BaseCanarinhoTextWatcher;->mudancaInterna:Z

    return v0
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .registers 5
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "before"    # I
    .param p4, "count"    # I

    .line 30
    return-void
.end method

.method public setEventoDeValidacao(Lbr/com/concretesolutions/canarinho/watcher/evento/EventoDeValidacao;)V
    .registers 2
    .param p1, "eventoDeValidacao"    # Lbr/com/concretesolutions/canarinho/watcher/evento/EventoDeValidacao;

    .line 185
    iput-object p1, p0, Lbr/com/concretesolutions/canarinho/watcher/BaseCanarinhoTextWatcher;->eventoDeValidacao:Lbr/com/concretesolutions/canarinho/watcher/evento/EventoDeValidacao;

    .line 186
    return-void
.end method

.method protected trataAdicaoRemocaoDeCaracter(Landroid/text/Editable;[C)Ljava/lang/StringBuilder;
    .registers 4
    .param p1, "s"    # Landroid/text/Editable;
    .param p2, "mascara"    # [C

    .line 108
    invoke-virtual {p0, p1}, Lbr/com/concretesolutions/canarinho/watcher/BaseCanarinhoTextWatcher;->isApagouCaracter(Landroid/text/Editable;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 109
    invoke-direct {p0, p1, p2}, Lbr/com/concretesolutions/canarinho/watcher/BaseCanarinhoTextWatcher;->trataRemocaoDeCaracter(Landroid/text/Editable;[C)Ljava/lang/StringBuilder;

    move-result-object v0

    goto :goto_f

    .line 110
    :cond_b
    invoke-direct {p0, p1, p2}, Lbr/com/concretesolutions/canarinho/watcher/BaseCanarinhoTextWatcher;->trataAdicaoDeCaracter(Landroid/text/Editable;[C)Ljava/lang/StringBuilder;

    move-result-object v0

    :goto_f
    return-object v0
.end method

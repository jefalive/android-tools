.class public Lcom/itau/empresas/api/model/AlcadaVO;
.super Ljava/lang/Object;
.source "AlcadaVO.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private contasAPagarVO:Lcom/itau/empresas/api/model/ContasAPagarVO;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "contas_a_pagar"
    .end annotation
.end field

.field private pagamentoExpressoVO:Lcom/itau/empresas/api/model/PagamentoExpressoVO;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "pagamento_expresso"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getContasAPagarVO()Lcom/itau/empresas/api/model/ContasAPagarVO;
    .registers 2

    .line 25
    iget-object v0, p0, Lcom/itau/empresas/api/model/AlcadaVO;->contasAPagarVO:Lcom/itau/empresas/api/model/ContasAPagarVO;

    return-object v0
.end method

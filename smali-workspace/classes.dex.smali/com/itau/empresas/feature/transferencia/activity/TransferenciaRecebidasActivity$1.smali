.class Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity$1;
.super Ljava/lang/Object;
.source "TransferenciaRecebidasActivity.java"

# interfaces
.implements Landroid/widget/RadioGroup$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;->carregaElementosDaTela()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;


# direct methods
.method constructor <init>(Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;)V
    .registers 2
    .param p1, "this$0"    # Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;

    .line 111
    iput-object p1, p0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity$1;->this$0:Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/RadioGroup;I)V
    .registers 7
    .param p1, "group"    # Landroid/widget/RadioGroup;
    .param p2, "checkedId"    # I

    .line 113
    iget-object v0, p0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity$1;->this$0:Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;

    invoke-virtual {v0, p2}, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Landroid/widget/RadioButton;

    .line 114
    .local v3, "rb":Landroid/widget/RadioButton;
    invoke-virtual {v3}, Landroid/widget/RadioButton;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity$1;->this$0:Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;

    const v2, 0x7f070502

    invoke-virtual {v1, v2}, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3a

    .line 115
    iget-object v0, p0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity$1;->this$0:Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;

    iget-object v0, v0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;->textoEscolhaData:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 116
    iget-object v0, p0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity$1;->this$0:Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;

    iget-object v0, v0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;->llTransferenciaRecebidasFiltroData:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 117
    iget-object v0, p0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity$1;->this$0:Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;

    iget-object v0, v0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;->rlTransferenciasRecebidasFiltroDias:Landroid/widget/RelativeLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto :goto_54

    .line 119
    :cond_3a
    iget-object v0, p0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity$1;->this$0:Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;

    iget-object v0, v0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;->textoEscolhaData:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 120
    iget-object v0, p0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity$1;->this$0:Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;

    iget-object v0, v0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;->llTransferenciaRecebidasFiltroData:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 121
    iget-object v0, p0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity$1;->this$0:Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;

    iget-object v0, v0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;->rlTransferenciasRecebidasFiltroDias:Landroid/widget/RelativeLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 124
    :goto_54
    iget-object v0, p0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity$1;->this$0:Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;

    # invokes: Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;->carregarLista()V
    invoke-static {v0}, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;->access$000(Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;)V

    .line 125
    return-void
.end method

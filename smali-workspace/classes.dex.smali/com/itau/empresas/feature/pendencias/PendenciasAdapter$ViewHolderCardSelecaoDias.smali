.class Lcom/itau/empresas/feature/pendencias/PendenciasAdapter$ViewHolderCardSelecaoDias;
.super Landroid/support/v7/widget/RecyclerView$ViewHolder;
.source "PendenciasAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/itau/empresas/feature/pendencias/PendenciasAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ViewHolderCardSelecaoDias"
.end annotation


# instance fields
.field final dias:[Ljava/lang/String;

.field private podeFazerRequest:Z

.field private selecaoDias:Landroid/widget/Spinner;

.field final synthetic this$0:Lcom/itau/empresas/feature/pendencias/PendenciasAdapter;


# direct methods
.method public constructor <init>(Lcom/itau/empresas/feature/pendencias/PendenciasAdapter;Landroid/view/View;)V
    .registers 7
    .param p2, "itemView"    # Landroid/view/View;

    .line 186
    iput-object p1, p0, Lcom/itau/empresas/feature/pendencias/PendenciasAdapter$ViewHolderCardSelecaoDias;->this$0:Lcom/itau/empresas/feature/pendencias/PendenciasAdapter;

    .line 187
    invoke-direct {p0, p2}, Landroid/support/v7/widget/RecyclerView$ViewHolder;-><init>(Landroid/view/View;)V

    .line 173
    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "hoje"

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const-string v1, "1 dia"

    const/4 v2, 0x1

    aput-object v1, v0, v2

    const-string v1, "2 dias"

    const/4 v2, 0x2

    aput-object v1, v0, v2

    const-string v1, "3 dias"

    const/4 v2, 0x3

    aput-object v1, v0, v2

    const-string v1, "4 dias"

    const/4 v2, 0x4

    aput-object v1, v0, v2

    const-string v1, "5 dias"

    const/4 v2, 0x5

    aput-object v1, v0, v2

    const-string v1, "6 dias"

    const/4 v2, 0x6

    aput-object v1, v0, v2

    const-string v1, "7 dias"

    const/4 v2, 0x7

    aput-object v1, v0, v2

    iput-object v0, p0, Lcom/itau/empresas/feature/pendencias/PendenciasAdapter$ViewHolderCardSelecaoDias;->dias:[Ljava/lang/String;

    .line 184
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/itau/empresas/feature/pendencias/PendenciasAdapter$ViewHolderCardSelecaoDias;->podeFazerRequest:Z

    .line 188
    const v0, 0x7f0e0500

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/itau/empresas/feature/pendencias/PendenciasAdapter$ViewHolderCardSelecaoDias;->selecaoDias:Landroid/widget/Spinner;

    .line 190
    new-instance v3, Landroid/widget/ArrayAdapter;

    .line 191
    invoke-virtual {p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/pendencias/PendenciasAdapter$ViewHolderCardSelecaoDias;->dias:[Ljava/lang/String;

    const v2, 0x1090009

    invoke-direct {v3, v0, v2, v1}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    .line 195
    .local v3, "adapter":Landroid/widget/SpinnerAdapter;
    iget-object v0, p0, Lcom/itau/empresas/feature/pendencias/PendenciasAdapter$ViewHolderCardSelecaoDias;->selecaoDias:Landroid/widget/Spinner;

    invoke-virtual {v0, v3}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 196
    iget-object v0, p0, Lcom/itau/empresas/feature/pendencias/PendenciasAdapter$ViewHolderCardSelecaoDias;->selecaoDias:Landroid/widget/Spinner;

    new-instance v1, Lcom/itau/empresas/feature/pendencias/PendenciasAdapter$ViewHolderCardSelecaoDias$1;

    invoke-direct {v1, p0, p1}, Lcom/itau/empresas/feature/pendencias/PendenciasAdapter$ViewHolderCardSelecaoDias$1;-><init>(Lcom/itau/empresas/feature/pendencias/PendenciasAdapter$ViewHolderCardSelecaoDias;Lcom/itau/empresas/feature/pendencias/PendenciasAdapter;)V

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 212
    return-void
.end method

.method static synthetic access$000(Lcom/itau/empresas/feature/pendencias/PendenciasAdapter$ViewHolderCardSelecaoDias;)Z
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/feature/pendencias/PendenciasAdapter$ViewHolderCardSelecaoDias;

    .line 171
    iget-boolean v0, p0, Lcom/itau/empresas/feature/pendencias/PendenciasAdapter$ViewHolderCardSelecaoDias;->podeFazerRequest:Z

    return v0
.end method

.method static synthetic access$002(Lcom/itau/empresas/feature/pendencias/PendenciasAdapter$ViewHolderCardSelecaoDias;Z)Z
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/feature/pendencias/PendenciasAdapter$ViewHolderCardSelecaoDias;
    .param p1, "x1"    # Z

    .line 171
    iput-boolean p1, p0, Lcom/itau/empresas/feature/pendencias/PendenciasAdapter$ViewHolderCardSelecaoDias;->podeFazerRequest:Z

    return p1
.end method


# virtual methods
.method publicaEvento(Ljava/lang/Object;)V
    .registers 3
    .param p1, "evento"    # Ljava/lang/Object;

    .line 215
    invoke-static {}, Lde/greenrobot/event/EventBus;->getDefault()Lde/greenrobot/event/EventBus;

    move-result-object v0

    invoke-virtual {v0, p1}, Lde/greenrobot/event/EventBus;->post(Ljava/lang/Object;)V

    .line 216
    return-void
.end method

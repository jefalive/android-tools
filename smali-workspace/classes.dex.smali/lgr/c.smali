.class public final Llgr/c;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:[B

.field private static final b:I

.field private static final c:I

.field private static final d:[B

.field private static e:I


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .line 14
    const/16 v0, 0x25

    new-array v0, v0, [B

    fill-array-data v0, :array_5a

    sput-object v0, Llgr/c;->d:[B

    const/16 v0, 0x62

    sput v0, Llgr/c;->e:I

    sget-object v0, Llgr/c;->d:[B

    const/16 v1, 0x1c

    aget-byte v0, v0, v1

    add-int/lit8 v0, v0, 0x1

    int-to-byte v0, v0

    or-int/lit8 v1, v0, 0x8

    int-to-byte v1, v1

    const/16 v2, 0x19

    invoke-static {v2, v0, v1}, Llgr/c;->a(IBS)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    sput-object v0, Llgr/c;->a:[B

    .line 15
    sget-object v0, Llgr/c;->d:[B

    const/16 v1, 0x13

    aget-byte v0, v0, v1

    add-int/lit8 v0, v0, -0x1

    int-to-byte v0, v0

    const/16 v1, 0xa

    const/16 v2, 0x38

    invoke-static {v1, v2, v0}, Llgr/c;->a(IBS)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    sput v0, Llgr/c;->b:I

    .line 16
    sget-object v0, Llgr/c;->d:[B

    const/4 v1, 0x2

    aget-byte v0, v0, v1

    neg-int v0, v0

    int-to-byte v0, v0

    sget-object v1, Llgr/c;->d:[B

    const/16 v2, 0x13

    aget-byte v1, v1, v2

    add-int/lit8 v1, v1, -0x1

    int-to-byte v1, v1

    const/16 v2, 0xa

    invoke-static {v2, v0, v1}, Llgr/c;->a(IBS)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    sput v0, Llgr/c;->c:I

    return-void

    nop

    :array_5a
    .array-data 1
        0x4ct
        0x53t
        -0x39t
        0x16t
        -0x4t
        -0x4t
        -0x1t
        0x2et
        -0x3at
        0x2ft
        -0x36t
        -0x1t
        0x25t
        0x3t
        -0x8t
        -0x30t
        -0x4t
        0x27t
        -0x5t
        0x1t
        -0x4t
        -0x9t
        -0x3t
        -0x14t
        0x16t
        -0x1t
        0x2t
        -0x9t
        0x6t
        -0x5t
        -0x17t
        -0x9t
        0x6t
        -0x5t
        -0x17t
        0x3at
        0x16t
    .end array-data
.end method

.method constructor <init>()V
    .registers 1

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(IBS)Ljava/lang/String;
    .registers 9

    const/4 v5, -0x1

    sget-object v4, Llgr/c;->d:[B

    rsub-int/lit8 p0, p0, 0x1f

    add-int/lit8 p2, p2, 0x1

    rsub-int/lit8 p1, p1, 0x6c

    new-instance v0, Ljava/lang/String;

    new-array v1, p2, [B

    add-int/lit8 p2, p2, -0x1

    if-nez v4, :cond_19

    move v2, p2

    move v3, p0

    :goto_13
    neg-int v3, v3

    add-int/2addr v2, v3

    add-int/lit8 p1, v2, -0x4

    add-int/lit8 p0, p0, 0x1

    :cond_19
    add-int/lit8 v5, v5, 0x1

    int-to-byte v2, p1

    aput-byte v2, v1, v5

    if-ne v5, p2, :cond_29

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Ljava/lang/String;-><init>([BI)V

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_29
    move v2, p1

    aget-byte v3, v4, p0

    goto :goto_13
.end method

.method public static a([Ljava/lang/String;Ljava/lang/String;[B)Ljava/lang/String;
    .registers 16

    .line 20
    invoke-static {p2}, Lbr/com/itau/security/commons/binary/ByteUtils;->to([B)Ljava/lang/String;

    move-result-object v5

    .line 1052
    sget v6, Llgr/c;->b:I

    .line 1053
    sget v7, Llgr/c;->c:I

    .line 1055
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    .line 1057
    invoke-static {v5}, Lbr/com/itau/security/commons/binary/ByteUtils;->hexStr2Bytes(Ljava/lang/String;)[B

    move-result-object v0

    invoke-static {v0}, Lbr/com/itau/security/commons/binary/ByteUtils;->to([B)Ljava/lang/String;

    move-result-object v0

    .line 1058
    invoke-static {v0}, Lbr/com/itau/security/commons/binary/ByteUtils;->hexStr2Bytes(Ljava/lang/String;)[B

    move-result-object v0

    invoke-static {v0}, Lbr/com/itau/security/commons/binary/ByteUtils;->to([B)Ljava/lang/String;

    move-result-object v0

    .line 1059
    invoke-static {v0}, Lbr/com/itau/security/commons/binary/ByteUtils;->from(Ljava/lang/String;)[B

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/util/Base64;->decode([BI)[B

    move-result-object v0

    .line 1060
    invoke-static {v0}, Lbr/com/itau/security/commons/binary/ByteUtils;->to([B)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object p2

    .line 1063
    const/4 v9, 0x0

    :goto_2f
    array-length v0, p2

    if-ge v9, v0, :cond_85

    .line 1064
    aget-char v5, p2, v9

    .line 1065
    sget-object v0, Llgr/c;->d:[B

    const/16 v1, 0x13

    aget-byte v0, v0, v1

    add-int/lit8 v0, v0, -0x1

    int-to-byte v0, v0

    const/16 v1, 0x1b

    const/16 v2, 0x3c

    invoke-static {v1, v2, v0}, Llgr/c;->a(IBS)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v10

    .line 1066
    rem-int v0, v9, v6

    if-ne v0, v10, :cond_7e

    .line 1067
    mul-int v0, v9, v7

    add-int v11, v0, v6

    .line 1068
    sget-object v0, Llgr/c;->d:[B

    const/16 v1, 0x23

    aget-byte v0, v0, v1

    int-to-byte v0, v0

    sget-object v1, Llgr/c;->d:[B

    const/16 v2, 0x13

    aget-byte v1, v1, v2

    add-int/lit8 v1, v1, -0x1

    int-to-byte v1, v1

    const/16 v2, 0x19

    invoke-static {v2, v0, v1}, Llgr/c;->a(IBS)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v12

    .line 1069
    rem-int v0, v7, v12

    if-ne v0, v10, :cond_71

    const/4 v0, -0x1

    goto :goto_72

    :cond_71
    const/4 v0, 0x1

    :goto_72
    add-int/2addr v7, v0

    .line 1070
    rem-int v0, v6, v12

    if-ne v0, v10, :cond_79

    const/4 v0, -0x1

    goto :goto_7a

    :cond_79
    const/4 v0, 0x1

    :goto_7a
    add-int/2addr v6, v0

    .line 1072
    sub-int v0, v5, v11

    int-to-char v5, v0

    .line 1074
    :cond_7e
    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1063
    add-int/lit8 v9, v9, 0x1

    goto/16 :goto_2f

    .line 1077
    :cond_85
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    .line 20
    .line 21
    sget-object v5, Llgr/c;->a:[B

    move-object v6, p1

    .line 1081
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v5}, Lbr/com/itau/security/commons/binary/ByteUtils;->to([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1082
    invoke-static {v0}, Lbr/com/itau/security/commons/binary/ByteUtils;->from(Ljava/lang/String;)[B

    move-result-object v0

    invoke-static {v0}, Lbr/com/itau/security/commons/Crypto;->sha1([B)[B

    move-result-object v8

    .line 1083
    sget-object v0, Llgr/c;->d:[B

    const/16 v1, 0x13

    aget-byte v0, v0, v1

    int-to-byte v0, v0

    const/16 v1, 0xa

    const/16 v2, 0x3b

    invoke-static {v1, v2, v0}, Llgr/c;->a(IBS)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    new-array v5, v0, [B

    .line 1084
    array-length v0, v5

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-static {v8, v1, v5, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 21
    .line 1085
    move-object p1, v5

    .line 22
    move-object v6, p1

    move-object v5, p2

    .line 2042
    invoke-static {v6}, Lbr/com/itau/security/commons/Crypto;->sha1([B)[B

    move-result-object v7

    .line 2043
    array-length v0, v6

    new-array v8, v0, [B

    .line 2044
    array-length v0, v8

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-static {v7, v1, v8, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2045
    const/4 v0, 0x0

    invoke-static {v5, v0}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v0

    .line 2047
    invoke-static {v0, v6, v8}, Lbr/com/itau/security/commons/Crypto;->pkcs5PaddingDecrypt([B[B[B)[B

    move-result-object v0

    .line 23
    invoke-static {v0}, Lbr/com/itau/security/commons/binary/ByteUtils;->to([B)Ljava/lang/String;

    move-result-object p1

    .line 25
    sget-object v0, Llgr/c;->d:[B

    const/16 v1, 0x1a

    aget-byte v0, v0, v1

    int-to-byte v0, v0

    const/16 v1, 0x1b

    const/16 v2, 0x2b

    invoke-static {v1, v2, v0}, Llgr/c;->a(IBS)Ljava/lang/String;

    .line 26
    new-instance v0, Lbr/com/itau/security/commons/json/JsonHelper;

    invoke-direct {v0, p1}, Lbr/com/itau/security/commons/json/JsonHelper;-><init>(Ljava/lang/String;)V

    .line 27
    move-object p1, v0

    sget-object v1, Llgr/c;->d:[B

    const/16 v2, 0x1c

    aget-byte v1, v1, v2

    int-to-byte v1, v1

    sget-object v2, Llgr/c;->d:[B

    const/16 v3, 0x13

    aget-byte v2, v2, v3

    add-int/lit8 v2, v2, -0x1

    int-to-byte v2, v2

    sget-object v3, Llgr/c;->d:[B

    const/16 v4, 0x1a

    aget-byte v3, v3, v4

    int-to-byte v3, v3

    invoke-static {v1, v2, v3}, Llgr/c;->a(IBS)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbr/com/itau/security/commons/json/JsonHelper;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    .line 29
    sget-object v0, Llgr/c;->d:[B

    const/16 v1, 0x13

    aget-byte v0, v0, v1

    int-to-byte v0, v0

    const/16 v1, 0xa

    const/16 v2, 0x3b

    invoke-static {v1, v2, v0}, Llgr/c;->a(IBS)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    .line 30
    const/4 v6, 0x0

    const/4 v7, 0x0

    :goto_129
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v0

    if-ge v6, v0, :cond_17e

    .line 31
    add-int v0, v6, v5

    invoke-virtual {p2, v6, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    aput-object v0, p0, v7

    .line 32
    sget-object v0, Llgr/c;->d:[B

    const/16 v1, 0x15

    aget-byte v0, v0, v1

    neg-int v0, v0

    int-to-byte v0, v0

    sget-object v1, Llgr/c;->d:[B

    const/16 v2, 0x11

    aget-byte v1, v1, v2

    add-int/lit8 v1, v1, 0x1

    int-to-byte v1, v1

    sget-object v2, Llgr/c;->d:[B

    const/16 v3, 0x1a

    aget-byte v2, v2, v3

    int-to-byte v2, v2

    invoke-static {v0, v1, v2}, Llgr/c;->a(IBS)Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Llgr/c;->d:[B

    const/16 v2, 0x1c

    aget-byte v1, v1, v2

    add-int/lit8 v1, v1, 0x1

    int-to-byte v1, v1

    sget-object v2, Llgr/c;->d:[B

    const/16 v3, 0x13

    aget-byte v2, v2, v3

    int-to-byte v2, v2

    const/16 v3, 0x32

    invoke-static {v1, v3, v2}, Llgr/c;->a(IBS)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    aget-object v1, p0, v7

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 30
    add-int/2addr v6, v5

    add-int/lit8 v7, v7, 0x1

    goto :goto_129

    .line 35
    :cond_17e
    sget-object v0, Llgr/c;->d:[B

    const/4 v1, 0x4

    aget-byte v0, v0, v1

    neg-int v0, v0

    int-to-byte v0, v0

    sget-object v1, Llgr/c;->d:[B

    const/16 v2, 0xd

    aget-byte v1, v1, v2

    int-to-byte v1, v1

    sget-object v2, Llgr/c;->d:[B

    const/4 v3, 0x4

    aget-byte v2, v2, v3

    neg-int v2, v2

    int-to-byte v2, v2

    invoke-static {v0, v1, v2}, Llgr/c;->a(IBS)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lbr/com/itau/security/commons/json/JsonHelper;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 36
    sget-object v0, Llgr/c;->d:[B

    const/16 v1, 0x15

    aget-byte v0, v0, v1

    neg-int v0, v0

    int-to-byte v0, v0

    sget-object v1, Llgr/c;->d:[B

    const/16 v2, 0x11

    aget-byte v1, v1, v2

    add-int/lit8 v1, v1, 0x1

    int-to-byte v1, v1

    sget-object v2, Llgr/c;->d:[B

    const/16 v3, 0x1a

    aget-byte v2, v2, v3

    int-to-byte v2, v2

    invoke-static {v0, v1, v2}, Llgr/c;->a(IBS)Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Llgr/c;->d:[B

    const/16 v2, 0x13

    aget-byte v1, v1, v2

    add-int/lit8 v1, v1, -0x1

    int-to-byte v1, v1

    sget-object v2, Llgr/c;->d:[B

    const/16 v3, 0xd

    aget-byte v2, v2, v3

    int-to-byte v2, v2

    sget-object v3, Llgr/c;->d:[B

    const/16 v4, 0x1c

    aget-byte v3, v3, v4

    int-to-byte v3, v3

    invoke-static {v1, v2, v3}, Llgr/c;->a(IBS)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 37
    return-object v6
.end method

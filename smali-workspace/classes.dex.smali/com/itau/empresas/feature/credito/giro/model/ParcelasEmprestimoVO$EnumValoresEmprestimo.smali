.class public final enum Lcom/itau/empresas/feature/credito/giro/model/ParcelasEmprestimoVO$EnumValoresEmprestimo;
.super Ljava/lang/Enum;
.source "ParcelasEmprestimoVO.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/itau/empresas/feature/credito/giro/model/ParcelasEmprestimoVO;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "EnumValoresEmprestimo"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<Lcom/itau/empresas/feature/credito/giro/model/ParcelasEmprestimoVO$EnumValoresEmprestimo;>;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/itau/empresas/feature/credito/giro/model/ParcelasEmprestimoVO$EnumValoresEmprestimo;

.field public static final enum CARREGADO_DA_API:Lcom/itau/empresas/feature/credito/giro/model/ParcelasEmprestimoVO$EnumValoresEmprestimo;

.field public static final enum CARREGADO_MANUALMENTE:Lcom/itau/empresas/feature/credito/giro/model/ParcelasEmprestimoVO$EnumValoresEmprestimo;

.field public static final enum OUTRO_PLANO:Lcom/itau/empresas/feature/credito/giro/model/ParcelasEmprestimoVO$EnumValoresEmprestimo;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .line 109
    new-instance v0, Lcom/itau/empresas/feature/credito/giro/model/ParcelasEmprestimoVO$EnumValoresEmprestimo;

    const-string v1, "CARREGADO_DA_API"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/itau/empresas/feature/credito/giro/model/ParcelasEmprestimoVO$EnumValoresEmprestimo;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/itau/empresas/feature/credito/giro/model/ParcelasEmprestimoVO$EnumValoresEmprestimo;->CARREGADO_DA_API:Lcom/itau/empresas/feature/credito/giro/model/ParcelasEmprestimoVO$EnumValoresEmprestimo;

    .line 110
    new-instance v0, Lcom/itau/empresas/feature/credito/giro/model/ParcelasEmprestimoVO$EnumValoresEmprestimo;

    const-string v1, "CARREGADO_MANUALMENTE"

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lcom/itau/empresas/feature/credito/giro/model/ParcelasEmprestimoVO$EnumValoresEmprestimo;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/itau/empresas/feature/credito/giro/model/ParcelasEmprestimoVO$EnumValoresEmprestimo;->CARREGADO_MANUALMENTE:Lcom/itau/empresas/feature/credito/giro/model/ParcelasEmprestimoVO$EnumValoresEmprestimo;

    .line 111
    new-instance v0, Lcom/itau/empresas/feature/credito/giro/model/ParcelasEmprestimoVO$EnumValoresEmprestimo;

    const-string v1, "OUTRO_PLANO"

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2}, Lcom/itau/empresas/feature/credito/giro/model/ParcelasEmprestimoVO$EnumValoresEmprestimo;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/itau/empresas/feature/credito/giro/model/ParcelasEmprestimoVO$EnumValoresEmprestimo;->OUTRO_PLANO:Lcom/itau/empresas/feature/credito/giro/model/ParcelasEmprestimoVO$EnumValoresEmprestimo;

    .line 108
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/itau/empresas/feature/credito/giro/model/ParcelasEmprestimoVO$EnumValoresEmprestimo;

    sget-object v1, Lcom/itau/empresas/feature/credito/giro/model/ParcelasEmprestimoVO$EnumValoresEmprestimo;->CARREGADO_DA_API:Lcom/itau/empresas/feature/credito/giro/model/ParcelasEmprestimoVO$EnumValoresEmprestimo;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sget-object v1, Lcom/itau/empresas/feature/credito/giro/model/ParcelasEmprestimoVO$EnumValoresEmprestimo;->CARREGADO_MANUALMENTE:Lcom/itau/empresas/feature/credito/giro/model/ParcelasEmprestimoVO$EnumValoresEmprestimo;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    sget-object v1, Lcom/itau/empresas/feature/credito/giro/model/ParcelasEmprestimoVO$EnumValoresEmprestimo;->OUTRO_PLANO:Lcom/itau/empresas/feature/credito/giro/model/ParcelasEmprestimoVO$EnumValoresEmprestimo;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    sput-object v0, Lcom/itau/empresas/feature/credito/giro/model/ParcelasEmprestimoVO$EnumValoresEmprestimo;->$VALUES:[Lcom/itau/empresas/feature/credito/giro/model/ParcelasEmprestimoVO$EnumValoresEmprestimo;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 108
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/itau/empresas/feature/credito/giro/model/ParcelasEmprestimoVO$EnumValoresEmprestimo;
    .registers 2
    .param p0, "name"    # Ljava/lang/String;

    .line 108
    const-class v0, Lcom/itau/empresas/feature/credito/giro/model/ParcelasEmprestimoVO$EnumValoresEmprestimo;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/feature/credito/giro/model/ParcelasEmprestimoVO$EnumValoresEmprestimo;

    return-object v0
.end method

.method public static values()[Lcom/itau/empresas/feature/credito/giro/model/ParcelasEmprestimoVO$EnumValoresEmprestimo;
    .registers 1

    .line 108
    sget-object v0, Lcom/itau/empresas/feature/credito/giro/model/ParcelasEmprestimoVO$EnumValoresEmprestimo;->$VALUES:[Lcom/itau/empresas/feature/credito/giro/model/ParcelasEmprestimoVO$EnumValoresEmprestimo;

    invoke-virtual {v0}, [Lcom/itau/empresas/feature/credito/giro/model/ParcelasEmprestimoVO$EnumValoresEmprestimo;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/itau/empresas/feature/credito/giro/model/ParcelasEmprestimoVO$EnumValoresEmprestimo;

    return-object v0
.end method

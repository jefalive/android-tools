.class public abstract Lcom/google/android/gms/internal/zzau;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;
.implements Landroid/view/ViewTreeObserver$OnScrollChangedListener;


# annotations
.annotation runtime Lcom/google/android/gms/internal/zzhb;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/internal/zzau$zzb;,
        Lcom/google/android/gms/internal/zzau$zza;,
        Lcom/google/android/gms/internal/zzau$zzc;,
        Lcom/google/android/gms/internal/zzau$zzd;
    }
.end annotation


# instance fields
.field protected final zzpV:Ljava/lang/Object;

.field private zzqJ:Z

.field private zzrQ:Lcom/google/android/gms/internal/zziz;

.field private final zzrW:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<Lcom/google/android/gms/internal/zzif;>;"
        }
    .end annotation
.end field

.field private zzrX:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<Landroid/view/ViewTreeObserver;>;"
        }
    .end annotation
.end field

.field private final zzrY:Lcom/google/android/gms/internal/zzbb;

.field protected final zzrZ:Lcom/google/android/gms/internal/zzaw;

.field private final zzsa:Landroid/content/Context;

.field private final zzsb:Landroid/view/WindowManager;

.field private final zzsc:Landroid/os/PowerManager;

.field private final zzsd:Landroid/app/KeyguardManager;

.field private zzse:Lcom/google/android/gms/internal/zzay;

.field private zzsf:Z

.field private zzsg:Z

.field private zzsh:Z

.field private zzsi:Z

.field private zzsj:Z

.field zzsk:Landroid/content/BroadcastReceiver;

.field private final zzsl:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet<Lcom/google/android/gms/internal/zzav;>;"
        }
    .end annotation
.end field

.field private final zzsm:Lcom/google/android/gms/internal/zzdf;

.field private final zzsn:Lcom/google/android/gms/internal/zzdf;

.field private final zzso:Lcom/google/android/gms/internal/zzdf;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/ads/internal/client/AdSizeParcel;Lcom/google/android/gms/internal/zzif;Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;Lcom/google/android/gms/internal/zzbb;)V
    .registers 13

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/internal/zzau;->zzpV:Ljava/lang/Object;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/internal/zzau;->zzqJ:Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/internal/zzau;->zzsg:Z

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/internal/zzau;->zzsl:Ljava/util/HashSet;

    new-instance v0, Lcom/google/android/gms/internal/zzau$2;

    invoke-direct {v0, p0}, Lcom/google/android/gms/internal/zzau$2;-><init>(Lcom/google/android/gms/internal/zzau;)V

    iput-object v0, p0, Lcom/google/android/gms/internal/zzau;->zzsm:Lcom/google/android/gms/internal/zzdf;

    new-instance v0, Lcom/google/android/gms/internal/zzau$3;

    invoke-direct {v0, p0}, Lcom/google/android/gms/internal/zzau$3;-><init>(Lcom/google/android/gms/internal/zzau;)V

    iput-object v0, p0, Lcom/google/android/gms/internal/zzau;->zzsn:Lcom/google/android/gms/internal/zzdf;

    new-instance v0, Lcom/google/android/gms/internal/zzau$4;

    invoke-direct {v0, p0}, Lcom/google/android/gms/internal/zzau$4;-><init>(Lcom/google/android/gms/internal/zzau;)V

    iput-object v0, p0, Lcom/google/android/gms/internal/zzau;->zzso:Lcom/google/android/gms/internal/zzdf;

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p3}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/google/android/gms/internal/zzau;->zzrW:Ljava/lang/ref/WeakReference;

    iput-object p5, p0, Lcom/google/android/gms/internal/zzau;->zzrY:Lcom/google/android/gms/internal/zzbb;

    new-instance v0, Ljava/lang/ref/WeakReference;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/google/android/gms/internal/zzau;->zzrX:Ljava/lang/ref/WeakReference;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/internal/zzau;->zzsh:Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/internal/zzau;->zzsj:Z

    new-instance v0, Lcom/google/android/gms/internal/zziz;

    const-wide/16 v1, 0xc8

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/internal/zziz;-><init>(J)V

    iput-object v0, p0, Lcom/google/android/gms/internal/zzau;->zzrQ:Lcom/google/android/gms/internal/zziz;

    new-instance v0, Lcom/google/android/gms/internal/zzaw;

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v3, p2, Lcom/google/android/gms/ads/internal/client/AdSizeParcel;->zzuh:Ljava/lang/String;

    iget-object v4, p3, Lcom/google/android/gms/internal/zzif;->zzKT:Lorg/json/JSONObject;

    invoke-virtual {p3}, Lcom/google/android/gms/internal/zzif;->zzcv()Z

    move-result v5

    iget-boolean v6, p2, Lcom/google/android/gms/ads/internal/client/AdSizeParcel;->zzuk:Z

    move-object v2, p4

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/internal/zzaw;-><init>(Ljava/lang/String;Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;Ljava/lang/String;Lorg/json/JSONObject;ZZ)V

    iput-object v0, p0, Lcom/google/android/gms/internal/zzau;->zzrZ:Lcom/google/android/gms/internal/zzaw;

    const-string v0, "window"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    iput-object v0, p0, Lcom/google/android/gms/internal/zzau;->zzsb:Landroid/view/WindowManager;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "power"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    iput-object v0, p0, Lcom/google/android/gms/internal/zzau;->zzsc:Landroid/os/PowerManager;

    const-string v0, "keyguard"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/KeyguardManager;

    iput-object v0, p0, Lcom/google/android/gms/internal/zzau;->zzsd:Landroid/app/KeyguardManager;

    iput-object p1, p0, Lcom/google/android/gms/internal/zzau;->zzsa:Landroid/content/Context;

    return-void
.end method


# virtual methods
.method protected destroy()V
    .registers 4

    iget-object v1, p0, Lcom/google/android/gms/internal/zzau;->zzpV:Ljava/lang/Object;

    monitor-enter v1

    :try_start_3
    invoke-virtual {p0}, Lcom/google/android/gms/internal/zzau;->zzcj()V

    invoke-virtual {p0}, Lcom/google/android/gms/internal/zzau;->zzce()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/internal/zzau;->zzsh:Z

    invoke-virtual {p0}, Lcom/google/android/gms/internal/zzau;->zzcg()V
    :try_end_f
    .catchall {:try_start_3 .. :try_end_f} :catchall_11

    monitor-exit v1

    goto :goto_14

    :catchall_11
    move-exception v2

    monitor-exit v1

    throw v2

    :goto_14
    return-void
.end method

.method isScreenOn()Z
    .registers 2

    iget-object v0, p0, Lcom/google/android/gms/internal/zzau;->zzsc:Landroid/os/PowerManager;

    invoke-virtual {v0}, Landroid/os/PowerManager;->isScreenOn()Z

    move-result v0

    return v0
.end method

.method public onGlobalLayout()V
    .registers 2

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/internal/zzau;->zzh(Z)V

    return-void
.end method

.method public onScrollChanged()V
    .registers 2

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/internal/zzau;->zzh(Z)V

    return-void
.end method

.method public pause()V
    .registers 4

    iget-object v1, p0, Lcom/google/android/gms/internal/zzau;->zzpV:Ljava/lang/Object;

    monitor-enter v1

    const/4 v0, 0x1

    :try_start_4
    iput-boolean v0, p0, Lcom/google/android/gms/internal/zzau;->zzqJ:Z

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/internal/zzau;->zzh(Z)V
    :try_end_a
    .catchall {:try_start_4 .. :try_end_a} :catchall_c

    monitor-exit v1

    goto :goto_f

    :catchall_c
    move-exception v2

    monitor-exit v1

    throw v2

    :goto_f
    return-void
.end method

.method public resume()V
    .registers 4

    iget-object v1, p0, Lcom/google/android/gms/internal/zzau;->zzpV:Ljava/lang/Object;

    monitor-enter v1

    const/4 v0, 0x0

    :try_start_4
    iput-boolean v0, p0, Lcom/google/android/gms/internal/zzau;->zzqJ:Z

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/internal/zzau;->zzh(Z)V
    :try_end_a
    .catchall {:try_start_4 .. :try_end_a} :catchall_c

    monitor-exit v1

    goto :goto_f

    :catchall_c
    move-exception v2

    monitor-exit v1

    throw v2

    :goto_f
    return-void
.end method

.method public stop()V
    .registers 4

    iget-object v1, p0, Lcom/google/android/gms/internal/zzau;->zzpV:Ljava/lang/Object;

    monitor-enter v1

    const/4 v0, 0x1

    :try_start_4
    iput-boolean v0, p0, Lcom/google/android/gms/internal/zzau;->zzsg:Z

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/internal/zzau;->zzh(Z)V
    :try_end_a
    .catchall {:try_start_4 .. :try_end_a} :catchall_c

    monitor-exit v1

    goto :goto_f

    :catchall_c
    move-exception v2

    monitor-exit v1

    throw v2

    :goto_f
    return-void
.end method

.method protected zza(ILandroid/util/DisplayMetrics;)I
    .registers 5

    iget v1, p2, Landroid/util/DisplayMetrics;->density:F

    int-to-float v0, p1

    div-float/2addr v0, v1

    float-to-int v0, v0

    return v0
.end method

.method protected zza(Landroid/view/View;Ljava/util/Map;)V
    .registers 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Landroid/view/View;Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;)V"
        }
    .end annotation

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/internal/zzau;->zzh(Z)V

    return-void
.end method

.method public zza(Lcom/google/android/gms/internal/zzav;)V
    .registers 3

    iget-object v0, p0, Lcom/google/android/gms/internal/zzau;->zzsl:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public zza(Lcom/google/android/gms/internal/zzay;)V
    .registers 4

    iget-object v0, p0, Lcom/google/android/gms/internal/zzau;->zzpV:Ljava/lang/Object;

    monitor-enter v0

    :try_start_3
    iput-object p1, p0, Lcom/google/android/gms/internal/zzau;->zzse:Lcom/google/android/gms/internal/zzay;
    :try_end_5
    .catchall {:try_start_3 .. :try_end_5} :catchall_7

    monitor-exit v0

    goto :goto_a

    :catchall_7
    move-exception v1

    monitor-exit v0

    throw v1

    :goto_a
    return-void
.end method

.method protected zza(Lorg/json/JSONObject;)V
    .registers 5

    :try_start_0
    new-instance v1, Lorg/json/JSONArray;

    invoke-direct {v1}, Lorg/json/JSONArray;-><init>()V

    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2}, Lorg/json/JSONObject;-><init>()V

    invoke-virtual {v1, p1}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    const-string v0, "units"

    invoke-virtual {v2, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    invoke-virtual {p0, v2}, Lcom/google/android/gms/internal/zzau;->zzb(Lorg/json/JSONObject;)V
    :try_end_15
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_15} :catch_16

    goto :goto_1c

    :catch_16
    move-exception v1

    const-string v0, "Skipping active view message."

    invoke-static {v0, v1}, Lcom/google/android/gms/internal/zzin;->zzb(Ljava/lang/String;Ljava/lang/Throwable;)V

    :goto_1c
    return-void
.end method

.method protected zzb(Lcom/google/android/gms/internal/zzeh;)V
    .registers 4

    const-string v0, "/updateActiveView"

    iget-object v1, p0, Lcom/google/android/gms/internal/zzau;->zzsm:Lcom/google/android/gms/internal/zzdf;

    invoke-interface {p1, v0, v1}, Lcom/google/android/gms/internal/zzeh;->zza(Ljava/lang/String;Lcom/google/android/gms/internal/zzdf;)V

    const-string v0, "/untrackActiveViewUnit"

    iget-object v1, p0, Lcom/google/android/gms/internal/zzau;->zzsn:Lcom/google/android/gms/internal/zzdf;

    invoke-interface {p1, v0, v1}, Lcom/google/android/gms/internal/zzeh;->zza(Ljava/lang/String;Lcom/google/android/gms/internal/zzdf;)V

    const-string v0, "/visibilityChanged"

    iget-object v1, p0, Lcom/google/android/gms/internal/zzau;->zzso:Lcom/google/android/gms/internal/zzdf;

    invoke-interface {p1, v0, v1}, Lcom/google/android/gms/internal/zzeh;->zza(Ljava/lang/String;Lcom/google/android/gms/internal/zzdf;)V

    return-void
.end method

.method protected abstract zzb(Lorg/json/JSONObject;)V
.end method

.method protected zzb(Ljava/util/Map;)Z
    .registers 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;)Z"
        }
    .end annotation

    if-nez p1, :cond_4

    const/4 v0, 0x0

    return v0

    :cond_4
    const-string v0, "hashCode"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_21

    iget-object v0, p0, Lcom/google/android/gms/internal/zzau;->zzrZ:Lcom/google/android/gms/internal/zzaw;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/zzaw;->zzcu()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_21

    const/4 v0, 0x1

    goto :goto_22

    :cond_21
    const/4 v0, 0x0

    :goto_22
    return v0
.end method

.method protected zzc(Lcom/google/android/gms/internal/zzeh;)V
    .registers 4

    const-string v0, "/visibilityChanged"

    iget-object v1, p0, Lcom/google/android/gms/internal/zzau;->zzso:Lcom/google/android/gms/internal/zzdf;

    invoke-interface {p1, v0, v1}, Lcom/google/android/gms/internal/zzeh;->zzb(Ljava/lang/String;Lcom/google/android/gms/internal/zzdf;)V

    const-string v0, "/untrackActiveViewUnit"

    iget-object v1, p0, Lcom/google/android/gms/internal/zzau;->zzsn:Lcom/google/android/gms/internal/zzdf;

    invoke-interface {p1, v0, v1}, Lcom/google/android/gms/internal/zzeh;->zzb(Ljava/lang/String;Lcom/google/android/gms/internal/zzdf;)V

    const-string v0, "/updateActiveView"

    iget-object v1, p0, Lcom/google/android/gms/internal/zzau;->zzsm:Lcom/google/android/gms/internal/zzdf;

    invoke-interface {p1, v0, v1}, Lcom/google/android/gms/internal/zzeh;->zzb(Ljava/lang/String;Lcom/google/android/gms/internal/zzdf;)V

    return-void
.end method

.method protected zzcd()V
    .registers 6

    iget-object v2, p0, Lcom/google/android/gms/internal/zzau;->zzpV:Ljava/lang/Object;

    monitor-enter v2

    :try_start_3
    iget-object v0, p0, Lcom/google/android/gms/internal/zzau;->zzsk:Landroid/content/BroadcastReceiver;
    :try_end_5
    .catchall {:try_start_3 .. :try_end_5} :catchall_28

    if-eqz v0, :cond_9

    monitor-exit v2

    return-void

    :cond_9
    :try_start_9
    new-instance v3, Landroid/content/IntentFilter;

    invoke-direct {v3}, Landroid/content/IntentFilter;-><init>()V

    const-string v0, "android.intent.action.SCREEN_ON"

    invoke-virtual {v3, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v0, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v3, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    new-instance v0, Lcom/google/android/gms/internal/zzau$1;

    invoke-direct {v0, p0}, Lcom/google/android/gms/internal/zzau$1;-><init>(Lcom/google/android/gms/internal/zzau;)V

    iput-object v0, p0, Lcom/google/android/gms/internal/zzau;->zzsk:Landroid/content/BroadcastReceiver;

    iget-object v0, p0, Lcom/google/android/gms/internal/zzau;->zzsa:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/gms/internal/zzau;->zzsk:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1, v3}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;
    :try_end_26
    .catchall {:try_start_9 .. :try_end_26} :catchall_28

    monitor-exit v2

    goto :goto_2b

    :catchall_28
    move-exception v4

    monitor-exit v2

    throw v4

    :goto_2b
    return-void
.end method

.method protected zzce()V
    .registers 6

    iget-object v2, p0, Lcom/google/android/gms/internal/zzau;->zzpV:Ljava/lang/Object;

    monitor-enter v2

    :try_start_3
    iget-object v0, p0, Lcom/google/android/gms/internal/zzau;->zzsk:Landroid/content/BroadcastReceiver;
    :try_end_5
    .catchall {:try_start_3 .. :try_end_5} :catchall_24

    if-eqz v0, :cond_22

    :try_start_7
    iget-object v0, p0, Lcom/google/android/gms/internal/zzau;->zzsa:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/gms/internal/zzau;->zzsk:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_e
    .catch Ljava/lang/IllegalStateException; {:try_start_7 .. :try_end_e} :catch_f
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_e} :catch_16
    .catchall {:try_start_7 .. :try_end_e} :catchall_24

    goto :goto_1f

    :catch_f
    move-exception v3

    const-string v0, "Failed trying to unregister the receiver"

    :try_start_12
    invoke-static {v0, v3}, Lcom/google/android/gms/internal/zzin;->zzb(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1f

    :catch_16
    move-exception v3

    invoke-static {}, Lcom/google/android/gms/ads/internal/zzr;->zzbF()Lcom/google/android/gms/internal/zzih;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v3, v1}, Lcom/google/android/gms/internal/zzih;->zzb(Ljava/lang/Throwable;Z)V

    :goto_1f
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/internal/zzau;->zzsk:Landroid/content/BroadcastReceiver;
    :try_end_22
    .catchall {:try_start_12 .. :try_end_22} :catchall_24

    :cond_22
    monitor-exit v2

    goto :goto_27

    :catchall_24
    move-exception v4

    monitor-exit v2

    throw v4

    :goto_27
    return-void
.end method

.method public zzcf()V
    .registers 6

    iget-object v2, p0, Lcom/google/android/gms/internal/zzau;->zzpV:Ljava/lang/Object;

    monitor-enter v2

    :try_start_3
    iget-boolean v0, p0, Lcom/google/android/gms/internal/zzau;->zzsh:Z

    if-eqz v0, :cond_3b

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/internal/zzau;->zzsi:Z
    :try_end_a
    .catchall {:try_start_3 .. :try_end_a} :catchall_3d

    :try_start_a
    invoke-virtual {p0}, Lcom/google/android/gms/internal/zzau;->zzcn()Lorg/json/JSONObject;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/google/android/gms/internal/zzau;->zza(Lorg/json/JSONObject;)V
    :try_end_11
    .catch Lorg/json/JSONException; {:try_start_a .. :try_end_11} :catch_12
    .catch Ljava/lang/RuntimeException; {:try_start_a .. :try_end_11} :catch_19
    .catchall {:try_start_a .. :try_end_11} :catchall_3d

    goto :goto_1f

    :catch_12
    move-exception v3

    const-string v0, "JSON failure while processing active view data."

    :try_start_15
    invoke-static {v0, v3}, Lcom/google/android/gms/internal/zzin;->zzb(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1f

    :catch_19
    move-exception v3

    const-string v0, "Failure while processing active view data."

    invoke-static {v0, v3}, Lcom/google/android/gms/internal/zzin;->zzb(Ljava/lang/String;Ljava/lang/Throwable;)V

    :goto_1f
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Untracking ad unit: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/internal/zzau;->zzrZ:Lcom/google/android/gms/internal/zzaw;

    invoke-virtual {v1}, Lcom/google/android/gms/internal/zzaw;->zzcu()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/internal/zzin;->zzaI(Ljava/lang/String;)V
    :try_end_3b
    .catchall {:try_start_15 .. :try_end_3b} :catchall_3d

    :cond_3b
    monitor-exit v2

    goto :goto_40

    :catchall_3d
    move-exception v4

    monitor-exit v2

    throw v4

    :goto_40
    return-void
.end method

.method protected zzcg()V
    .registers 2

    iget-object v0, p0, Lcom/google/android/gms/internal/zzau;->zzse:Lcom/google/android/gms/internal/zzay;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/google/android/gms/internal/zzau;->zzse:Lcom/google/android/gms/internal/zzay;

    invoke-interface {v0, p0}, Lcom/google/android/gms/internal/zzay;->zza(Lcom/google/android/gms/internal/zzau;)V

    :cond_9
    return-void
.end method

.method public zzch()Z
    .registers 4

    iget-object v1, p0, Lcom/google/android/gms/internal/zzau;->zzpV:Ljava/lang/Object;

    monitor-enter v1

    :try_start_3
    iget-boolean v0, p0, Lcom/google/android/gms/internal/zzau;->zzsh:Z
    :try_end_5
    .catchall {:try_start_3 .. :try_end_5} :catchall_7

    monitor-exit v1

    return v0

    :catchall_7
    move-exception v2

    monitor-exit v1

    throw v2
.end method

.method protected zzci()V
    .registers 6

    iget-object v0, p0, Lcom/google/android/gms/internal/zzau;->zzrY:Lcom/google/android/gms/internal/zzbb;

    invoke-interface {v0}, Lcom/google/android/gms/internal/zzbb;->zzcq()Lcom/google/android/gms/internal/zzbb;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/gms/internal/zzbb;->zzco()Landroid/view/View;

    move-result-object v2

    if-nez v2, :cond_d

    return-void

    :cond_d
    iget-object v0, p0, Lcom/google/android/gms/internal/zzau;->zzrX:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Landroid/view/ViewTreeObserver;

    invoke-virtual {v2}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v4

    if-ne v4, v3, :cond_1d

    return-void

    :cond_1d
    invoke-virtual {p0}, Lcom/google/android/gms/internal/zzau;->zzcj()V

    iget-boolean v0, p0, Lcom/google/android/gms/internal/zzau;->zzsf:Z

    if-eqz v0, :cond_2c

    if-eqz v3, :cond_35

    invoke-virtual {v3}, Landroid/view/ViewTreeObserver;->isAlive()Z

    move-result v0

    if-eqz v0, :cond_35

    :cond_2c
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/internal/zzau;->zzsf:Z

    invoke-virtual {v4, p0}, Landroid/view/ViewTreeObserver;->addOnScrollChangedListener(Landroid/view/ViewTreeObserver$OnScrollChangedListener;)V

    invoke-virtual {v4, p0}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    :cond_35
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, v4}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/google/android/gms/internal/zzau;->zzrX:Ljava/lang/ref/WeakReference;

    return-void
.end method

.method protected zzcj()V
    .registers 3

    iget-object v0, p0, Lcom/google/android/gms/internal/zzau;->zzrX:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Landroid/view/ViewTreeObserver;

    if-eqz v1, :cond_11

    invoke-virtual {v1}, Landroid/view/ViewTreeObserver;->isAlive()Z

    move-result v0

    if-nez v0, :cond_12

    :cond_11
    return-void

    :cond_12
    invoke-virtual {v1, p0}, Landroid/view/ViewTreeObserver;->removeOnScrollChangedListener(Landroid/view/ViewTreeObserver$OnScrollChangedListener;)V

    invoke-virtual {v1, p0}, Landroid/view/ViewTreeObserver;->removeGlobalOnLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    return-void
.end method

.method protected zzck()Lorg/json/JSONObject;
    .registers 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    new-instance v4, Lorg/json/JSONObject;

    invoke-direct {v4}, Lorg/json/JSONObject;-><init>()V

    const-string v0, "afmaVersion"

    iget-object v1, p0, Lcom/google/android/gms/internal/zzau;->zzrZ:Lcom/google/android/gms/internal/zzaw;

    invoke-virtual {v1}, Lcom/google/android/gms/internal/zzaw;->zzcs()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "activeViewJSON"

    iget-object v2, p0, Lcom/google/android/gms/internal/zzau;->zzrZ:Lcom/google/android/gms/internal/zzaw;

    invoke-virtual {v2}, Lcom/google/android/gms/internal/zzaw;->zzct()Lorg/json/JSONObject;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "timestamp"

    invoke-static {}, Lcom/google/android/gms/ads/internal/zzr;->zzbG()Lcom/google/android/gms/internal/zzmq;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/gms/internal/zzmq;->elapsedRealtime()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "adFormat"

    iget-object v2, p0, Lcom/google/android/gms/internal/zzau;->zzrZ:Lcom/google/android/gms/internal/zzaw;

    invoke-virtual {v2}, Lcom/google/android/gms/internal/zzaw;->zzcr()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "hashCode"

    iget-object v2, p0, Lcom/google/android/gms/internal/zzau;->zzrZ:Lcom/google/android/gms/internal/zzaw;

    invoke-virtual {v2}, Lcom/google/android/gms/internal/zzaw;->zzcu()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "isMraid"

    iget-object v2, p0, Lcom/google/android/gms/internal/zzau;->zzrZ:Lcom/google/android/gms/internal/zzaw;

    invoke-virtual {v2}, Lcom/google/android/gms/internal/zzaw;->zzcv()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "isStopped"

    iget-boolean v2, p0, Lcom/google/android/gms/internal/zzau;->zzsg:Z

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "isPaused"

    iget-boolean v2, p0, Lcom/google/android/gms/internal/zzau;->zzqJ:Z

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "isScreenOn"

    invoke-virtual {p0}, Lcom/google/android/gms/internal/zzau;->isScreenOn()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "isNative"

    iget-object v2, p0, Lcom/google/android/gms/internal/zzau;->zzrZ:Lcom/google/android/gms/internal/zzaw;

    invoke-virtual {v2}, Lcom/google/android/gms/internal/zzaw;->zzcw()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    return-object v4
.end method

.method protected abstract zzcl()Z
.end method

.method protected zzcm()Lorg/json/JSONObject;
    .registers 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/gms/internal/zzau;->zzck()Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "isAttachedToWindow"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "isScreenOn"

    invoke-virtual {p0}, Lcom/google/android/gms/internal/zzau;->isScreenOn()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "isVisible"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    move-result-object v0

    return-object v0
.end method

.method protected zzcn()Lorg/json/JSONObject;
    .registers 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/gms/internal/zzau;->zzck()Lorg/json/JSONObject;

    move-result-object v2

    const-string v0, "doneReasonCode"

    const-string v1, "u"

    invoke-virtual {v2, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    return-object v2
.end method

.method protected zzd(Landroid/view/View;)Lorg/json/JSONObject;
    .registers 20
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    if-nez p1, :cond_7

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/internal/zzau;->zzcm()Lorg/json/JSONObject;

    move-result-object v0

    return-object v0

    :cond_7
    invoke-static {}, Lcom/google/android/gms/ads/internal/zzr;->zzbE()Lcom/google/android/gms/internal/zzis;

    move-result-object v0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/internal/zzis;->isAttachedToWindow(Landroid/view/View;)Z

    move-result v6

    const/4 v0, 0x2

    new-array v7, v0, [I

    const/4 v0, 0x2

    new-array v8, v0, [I

    move-object/from16 v0, p1

    :try_start_19
    invoke-virtual {v0, v7}, Landroid/view/View;->getLocationOnScreen([I)V

    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, Landroid/view/View;->getLocationInWindow([I)V
    :try_end_21
    .catch Ljava/lang/Exception; {:try_start_19 .. :try_end_21} :catch_22

    goto :goto_28

    :catch_22
    move-exception v9

    const-string v0, "Failure getting view location."

    invoke-static {v0, v9}, Lcom/google/android/gms/internal/zzin;->zzb(Ljava/lang/String;Ljava/lang/Throwable;)V

    :goto_28
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v9

    new-instance v10, Landroid/graphics/Rect;

    invoke-direct {v10}, Landroid/graphics/Rect;-><init>()V

    const/4 v0, 0x0

    aget v0, v7, v0

    iput v0, v10, Landroid/graphics/Rect;->left:I

    const/4 v0, 0x1

    aget v0, v7, v0

    iput v0, v10, Landroid/graphics/Rect;->top:I

    iget v0, v10, Landroid/graphics/Rect;->left:I

    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getWidth()I

    move-result v1

    add-int/2addr v0, v1

    iput v0, v10, Landroid/graphics/Rect;->right:I

    iget v0, v10, Landroid/graphics/Rect;->top:I

    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getHeight()I

    move-result v1

    add-int/2addr v0, v1

    iput v0, v10, Landroid/graphics/Rect;->bottom:I

    new-instance v11, Landroid/graphics/Rect;

    invoke-direct {v11}, Landroid/graphics/Rect;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/internal/zzau;->zzsb:Landroid/view/WindowManager;

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Display;->getWidth()I

    move-result v0

    iput v0, v11, Landroid/graphics/Rect;->right:I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/internal/zzau;->zzsb:Landroid/view/WindowManager;

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Display;->getHeight()I

    move-result v0

    iput v0, v11, Landroid/graphics/Rect;->bottom:I

    new-instance v12, Landroid/graphics/Rect;

    invoke-direct {v12}, Landroid/graphics/Rect;-><init>()V

    move-object/from16 v0, p1

    const/4 v1, 0x0

    invoke-virtual {v0, v12, v1}, Landroid/view/View;->getGlobalVisibleRect(Landroid/graphics/Rect;Landroid/graphics/Point;)Z

    move-result v13

    new-instance v14, Landroid/graphics/Rect;

    invoke-direct {v14}, Landroid/graphics/Rect;-><init>()V

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Landroid/view/View;->getLocalVisibleRect(Landroid/graphics/Rect;)Z

    move-result v15

    new-instance v16, Landroid/graphics/Rect;

    invoke-direct/range {v16 .. v16}, Landroid/graphics/Rect;-><init>()V

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Landroid/view/View;->getHitRect(Landroid/graphics/Rect;)V

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/internal/zzau;->zzck()Lorg/json/JSONObject;

    move-result-object v17

    const-string v0, "windowVisibility"

    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getWindowVisibility()I

    move-result v1

    move-object/from16 v2, v17

    invoke-virtual {v2, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "isAttachedToWindow"

    invoke-virtual {v0, v1, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "viewBox"

    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2}, Lorg/json/JSONObject;-><init>()V

    const-string v3, "top"

    iget v4, v11, Landroid/graphics/Rect;->top:I

    move-object/from16 v5, p0

    invoke-virtual {v5, v4, v9}, Lcom/google/android/gms/internal/zzau;->zza(ILandroid/util/DisplayMetrics;)I

    move-result v4

    invoke-virtual {v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    move-result-object v2

    const-string v3, "bottom"

    iget v4, v11, Landroid/graphics/Rect;->bottom:I

    move-object/from16 v5, p0

    invoke-virtual {v5, v4, v9}, Lcom/google/android/gms/internal/zzau;->zza(ILandroid/util/DisplayMetrics;)I

    move-result v4

    invoke-virtual {v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    move-result-object v2

    const-string v3, "left"

    iget v4, v11, Landroid/graphics/Rect;->left:I

    move-object/from16 v5, p0

    invoke-virtual {v5, v4, v9}, Lcom/google/android/gms/internal/zzau;->zza(ILandroid/util/DisplayMetrics;)I

    move-result v4

    invoke-virtual {v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    move-result-object v2

    const-string v3, "right"

    iget v4, v11, Landroid/graphics/Rect;->right:I

    move-object/from16 v5, p0

    invoke-virtual {v5, v4, v9}, Lcom/google/android/gms/internal/zzau;->zza(ILandroid/util/DisplayMetrics;)I

    move-result v4

    invoke-virtual {v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "adBox"

    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2}, Lorg/json/JSONObject;-><init>()V

    const-string v3, "top"

    iget v4, v10, Landroid/graphics/Rect;->top:I

    move-object/from16 v5, p0

    invoke-virtual {v5, v4, v9}, Lcom/google/android/gms/internal/zzau;->zza(ILandroid/util/DisplayMetrics;)I

    move-result v4

    invoke-virtual {v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    move-result-object v2

    const-string v3, "bottom"

    iget v4, v10, Landroid/graphics/Rect;->bottom:I

    move-object/from16 v5, p0

    invoke-virtual {v5, v4, v9}, Lcom/google/android/gms/internal/zzau;->zza(ILandroid/util/DisplayMetrics;)I

    move-result v4

    invoke-virtual {v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    move-result-object v2

    const-string v3, "left"

    iget v4, v10, Landroid/graphics/Rect;->left:I

    move-object/from16 v5, p0

    invoke-virtual {v5, v4, v9}, Lcom/google/android/gms/internal/zzau;->zza(ILandroid/util/DisplayMetrics;)I

    move-result v4

    invoke-virtual {v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    move-result-object v2

    const-string v3, "right"

    iget v4, v10, Landroid/graphics/Rect;->right:I

    move-object/from16 v5, p0

    invoke-virtual {v5, v4, v9}, Lcom/google/android/gms/internal/zzau;->zza(ILandroid/util/DisplayMetrics;)I

    move-result v4

    invoke-virtual {v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "globalVisibleBox"

    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2}, Lorg/json/JSONObject;-><init>()V

    const-string v3, "top"

    iget v4, v12, Landroid/graphics/Rect;->top:I

    move-object/from16 v5, p0

    invoke-virtual {v5, v4, v9}, Lcom/google/android/gms/internal/zzau;->zza(ILandroid/util/DisplayMetrics;)I

    move-result v4

    invoke-virtual {v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    move-result-object v2

    const-string v3, "bottom"

    iget v4, v12, Landroid/graphics/Rect;->bottom:I

    move-object/from16 v5, p0

    invoke-virtual {v5, v4, v9}, Lcom/google/android/gms/internal/zzau;->zza(ILandroid/util/DisplayMetrics;)I

    move-result v4

    invoke-virtual {v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    move-result-object v2

    const-string v3, "left"

    iget v4, v12, Landroid/graphics/Rect;->left:I

    move-object/from16 v5, p0

    invoke-virtual {v5, v4, v9}, Lcom/google/android/gms/internal/zzau;->zza(ILandroid/util/DisplayMetrics;)I

    move-result v4

    invoke-virtual {v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    move-result-object v2

    const-string v3, "right"

    iget v4, v12, Landroid/graphics/Rect;->right:I

    move-object/from16 v5, p0

    invoke-virtual {v5, v4, v9}, Lcom/google/android/gms/internal/zzau;->zza(ILandroid/util/DisplayMetrics;)I

    move-result v4

    invoke-virtual {v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "globalVisibleBoxVisible"

    invoke-virtual {v0, v1, v13}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "localVisibleBox"

    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2}, Lorg/json/JSONObject;-><init>()V

    const-string v3, "top"

    iget v4, v14, Landroid/graphics/Rect;->top:I

    move-object/from16 v5, p0

    invoke-virtual {v5, v4, v9}, Lcom/google/android/gms/internal/zzau;->zza(ILandroid/util/DisplayMetrics;)I

    move-result v4

    invoke-virtual {v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    move-result-object v2

    const-string v3, "bottom"

    iget v4, v14, Landroid/graphics/Rect;->bottom:I

    move-object/from16 v5, p0

    invoke-virtual {v5, v4, v9}, Lcom/google/android/gms/internal/zzau;->zza(ILandroid/util/DisplayMetrics;)I

    move-result v4

    invoke-virtual {v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    move-result-object v2

    const-string v3, "left"

    iget v4, v14, Landroid/graphics/Rect;->left:I

    move-object/from16 v5, p0

    invoke-virtual {v5, v4, v9}, Lcom/google/android/gms/internal/zzau;->zza(ILandroid/util/DisplayMetrics;)I

    move-result v4

    invoke-virtual {v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    move-result-object v2

    const-string v3, "right"

    iget v4, v14, Landroid/graphics/Rect;->right:I

    move-object/from16 v5, p0

    invoke-virtual {v5, v4, v9}, Lcom/google/android/gms/internal/zzau;->zza(ILandroid/util/DisplayMetrics;)I

    move-result v4

    invoke-virtual {v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "localVisibleBoxVisible"

    invoke-virtual {v0, v1, v15}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "hitBox"

    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2}, Lorg/json/JSONObject;-><init>()V

    const-string v3, "top"

    move-object/from16 v4, v16

    iget v4, v4, Landroid/graphics/Rect;->top:I

    move-object/from16 v5, p0

    invoke-virtual {v5, v4, v9}, Lcom/google/android/gms/internal/zzau;->zza(ILandroid/util/DisplayMetrics;)I

    move-result v4

    invoke-virtual {v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    move-result-object v2

    const-string v3, "bottom"

    move-object/from16 v4, v16

    iget v4, v4, Landroid/graphics/Rect;->bottom:I

    move-object/from16 v5, p0

    invoke-virtual {v5, v4, v9}, Lcom/google/android/gms/internal/zzau;->zza(ILandroid/util/DisplayMetrics;)I

    move-result v4

    invoke-virtual {v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    move-result-object v2

    const-string v3, "left"

    move-object/from16 v4, v16

    iget v4, v4, Landroid/graphics/Rect;->left:I

    move-object/from16 v5, p0

    invoke-virtual {v5, v4, v9}, Lcom/google/android/gms/internal/zzau;->zza(ILandroid/util/DisplayMetrics;)I

    move-result v4

    invoke-virtual {v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    move-result-object v2

    const-string v3, "right"

    move-object/from16 v4, v16

    iget v4, v4, Landroid/graphics/Rect;->right:I

    move-object/from16 v5, p0

    invoke-virtual {v5, v4, v9}, Lcom/google/android/gms/internal/zzau;->zza(ILandroid/util/DisplayMetrics;)I

    move-result v4

    invoke-virtual {v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "screenDensity"

    iget v2, v9, Landroid/util/DisplayMetrics;->density:F

    float-to-double v2, v2

    invoke-virtual {v0, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "isVisible"

    invoke-static {}, Lcom/google/android/gms/ads/internal/zzr;->zzbC()Lcom/google/android/gms/internal/zzir;

    move-result-object v2

    move-object/from16 v3, p0

    iget-object v3, v3, Lcom/google/android/gms/internal/zzau;->zzsc:Landroid/os/PowerManager;

    move-object/from16 v4, p0

    iget-object v4, v4, Lcom/google/android/gms/internal/zzau;->zzsd:Landroid/app/KeyguardManager;

    move-object/from16 v5, p1

    invoke-virtual {v2, v5, v3, v4}, Lcom/google/android/gms/internal/zzir;->zza(Landroid/view/View;Landroid/os/PowerManager;Landroid/app/KeyguardManager;)Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    return-object v17
.end method

.method protected zzg(Z)V
    .registers 5

    iget-object v0, p0, Lcom/google/android/gms/internal/zzau;->zzsl:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_17

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/google/android/gms/internal/zzav;

    invoke-interface {v2, p0, p1}, Lcom/google/android/gms/internal/zzav;->zza(Lcom/google/android/gms/internal/zzau;Z)V

    goto :goto_6

    :cond_17
    return-void
.end method

.method protected zzh(Z)V
    .registers 11

    iget-object v3, p0, Lcom/google/android/gms/internal/zzau;->zzpV:Ljava/lang/Object;

    monitor-enter v3

    :try_start_3
    invoke-virtual {p0}, Lcom/google/android/gms/internal/zzau;->zzcl()Z

    move-result v0

    if-eqz v0, :cond_d

    iget-boolean v0, p0, Lcom/google/android/gms/internal/zzau;->zzsh:Z
    :try_end_b
    .catchall {:try_start_3 .. :try_end_b} :catchall_69

    if-nez v0, :cond_f

    :cond_d
    monitor-exit v3

    return-void

    :cond_f
    :try_start_f
    iget-object v0, p0, Lcom/google/android/gms/internal/zzau;->zzrY:Lcom/google/android/gms/internal/zzbb;

    invoke-interface {v0}, Lcom/google/android/gms/internal/zzbb;->zzco()Landroid/view/View;

    move-result-object v4

    if-eqz v4, :cond_33

    invoke-static {}, Lcom/google/android/gms/ads/internal/zzr;->zzbC()Lcom/google/android/gms/internal/zzir;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/internal/zzau;->zzsc:Landroid/os/PowerManager;

    iget-object v2, p0, Lcom/google/android/gms/internal/zzau;->zzsd:Landroid/app/KeyguardManager;

    invoke-virtual {v0, v4, v1, v2}, Lcom/google/android/gms/internal/zzir;->zza(Landroid/view/View;Landroid/os/PowerManager;Landroid/app/KeyguardManager;)Z

    move-result v0

    if-eqz v0, :cond_33

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    const/4 v1, 0x0

    invoke-virtual {v4, v0, v1}, Landroid/view/View;->getGlobalVisibleRect(Landroid/graphics/Rect;Landroid/graphics/Point;)Z

    move-result v0

    if-eqz v0, :cond_33

    const/4 v5, 0x1

    goto :goto_34

    :cond_33
    const/4 v5, 0x0

    :goto_34
    if-eqz p1, :cond_44

    iget-object v0, p0, Lcom/google/android/gms/internal/zzau;->zzrQ:Lcom/google/android/gms/internal/zziz;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/zziz;->tryAcquire()Z

    move-result v0

    if-nez v0, :cond_44

    iget-boolean v0, p0, Lcom/google/android/gms/internal/zzau;->zzsj:Z
    :try_end_40
    .catchall {:try_start_f .. :try_end_40} :catchall_69

    if-ne v5, v0, :cond_44

    monitor-exit v3

    return-void

    :cond_44
    :try_start_44
    iput-boolean v5, p0, Lcom/google/android/gms/internal/zzau;->zzsj:Z

    iget-object v0, p0, Lcom/google/android/gms/internal/zzau;->zzrY:Lcom/google/android/gms/internal/zzbb;

    invoke-interface {v0}, Lcom/google/android/gms/internal/zzbb;->zzcp()Z

    move-result v6

    if-eqz v6, :cond_53

    invoke-virtual {p0}, Lcom/google/android/gms/internal/zzau;->zzcf()V
    :try_end_51
    .catchall {:try_start_44 .. :try_end_51} :catchall_69

    monitor-exit v3

    return-void

    :cond_53
    :try_start_53
    invoke-virtual {p0, v4}, Lcom/google/android/gms/internal/zzau;->zzd(Landroid/view/View;)Lorg/json/JSONObject;

    move-result-object v7

    invoke-virtual {p0, v7}, Lcom/google/android/gms/internal/zzau;->zza(Lorg/json/JSONObject;)V
    :try_end_5a
    .catch Lorg/json/JSONException; {:try_start_53 .. :try_end_5a} :catch_5b
    .catch Ljava/lang/RuntimeException; {:try_start_53 .. :try_end_5a} :catch_5b
    .catchall {:try_start_53 .. :try_end_5a} :catchall_69

    goto :goto_61

    :catch_5b
    move-exception v7

    const-string v0, "Active view update failed."

    :try_start_5e
    invoke-static {v0, v7}, Lcom/google/android/gms/internal/zzin;->zza(Ljava/lang/String;Ljava/lang/Throwable;)V

    :goto_61
    invoke-virtual {p0}, Lcom/google/android/gms/internal/zzau;->zzci()V

    invoke-virtual {p0}, Lcom/google/android/gms/internal/zzau;->zzcg()V
    :try_end_67
    .catchall {:try_start_5e .. :try_end_67} :catchall_69

    monitor-exit v3

    goto :goto_6c

    :catchall_69
    move-exception v8

    monitor-exit v3

    throw v8

    :goto_6c
    return-void
.end method

.class final Lorg/threeten/bp/chrono/Ser;
.super Ljava/lang/Object;
.source "Ser.java"

# interfaces
.implements Ljava/io/Externalizable;


# static fields
.field private static final serialVersionUID:J = 0x6d0b833274ca0096L


# instance fields
.field private object:Ljava/lang/Object;

.field private type:B


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 93
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 94
    return-void
.end method

.method constructor <init>(BLjava/lang/Object;)V
    .registers 3
    .param p1, "type"    # B
    .param p2, "object"    # Ljava/lang/Object;

    .line 102
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 103
    iput-byte p1, p0, Lorg/threeten/bp/chrono/Ser;->type:B

    .line 104
    iput-object p2, p0, Lorg/threeten/bp/chrono/Ser;->object:Ljava/lang/Object;

    .line 105
    return-void
.end method

.method private static readInternal(BLjava/io/ObjectInput;)Ljava/lang/Object;
    .registers 4
    .param p0, "type"    # B
    .param p1, "in"    # Ljava/io/ObjectInput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .line 177
    packed-switch p0, :pswitch_data_44

    goto :goto_3b

    .line 178
    :pswitch_4
    invoke-static {p1}, Lorg/threeten/bp/chrono/JapaneseDate;->readExternal(Ljava/io/DataInput;)Lorg/threeten/bp/chrono/ChronoLocalDate;

    move-result-object v0

    return-object v0

    .line 179
    :pswitch_9
    invoke-static {p1}, Lorg/threeten/bp/chrono/JapaneseEra;->readExternal(Ljava/io/DataInput;)Lorg/threeten/bp/chrono/JapaneseEra;

    move-result-object v0

    return-object v0

    .line 180
    :pswitch_e
    invoke-static {p1}, Lorg/threeten/bp/chrono/HijrahDate;->readExternal(Ljava/io/DataInput;)Lorg/threeten/bp/chrono/ChronoLocalDate;

    move-result-object v0

    return-object v0

    .line 181
    :pswitch_13
    invoke-static {p1}, Lorg/threeten/bp/chrono/HijrahEra;->readExternal(Ljava/io/DataInput;)Lorg/threeten/bp/chrono/HijrahEra;

    move-result-object v0

    return-object v0

    .line 182
    :pswitch_18
    invoke-static {p1}, Lorg/threeten/bp/chrono/MinguoDate;->readExternal(Ljava/io/DataInput;)Lorg/threeten/bp/chrono/ChronoLocalDate;

    move-result-object v0

    return-object v0

    .line 183
    :pswitch_1d
    invoke-static {p1}, Lorg/threeten/bp/chrono/MinguoEra;->readExternal(Ljava/io/DataInput;)Lorg/threeten/bp/chrono/MinguoEra;

    move-result-object v0

    return-object v0

    .line 184
    :pswitch_22
    invoke-static {p1}, Lorg/threeten/bp/chrono/ThaiBuddhistDate;->readExternal(Ljava/io/DataInput;)Lorg/threeten/bp/chrono/ChronoLocalDate;

    move-result-object v0

    return-object v0

    .line 185
    :pswitch_27
    invoke-static {p1}, Lorg/threeten/bp/chrono/ThaiBuddhistEra;->readExternal(Ljava/io/DataInput;)Lorg/threeten/bp/chrono/ThaiBuddhistEra;

    move-result-object v0

    return-object v0

    .line 186
    :pswitch_2c
    invoke-static {p1}, Lorg/threeten/bp/chrono/Chronology;->readExternal(Ljava/io/DataInput;)Lorg/threeten/bp/chrono/Chronology;

    move-result-object v0

    return-object v0

    .line 187
    :pswitch_31
    invoke-static {p1}, Lorg/threeten/bp/chrono/ChronoLocalDateTimeImpl;->readExternal(Ljava/io/ObjectInput;)Lorg/threeten/bp/chrono/ChronoLocalDateTime;

    move-result-object v0

    return-object v0

    .line 188
    :pswitch_36
    invoke-static {p1}, Lorg/threeten/bp/chrono/ChronoZonedDateTimeImpl;->readExternal(Ljava/io/ObjectInput;)Lorg/threeten/bp/chrono/ChronoZonedDateTime;

    move-result-object v0

    return-object v0

    .line 190
    :goto_3b
    :pswitch_3b
    new-instance v0, Ljava/io/StreamCorruptedException;

    const-string v1, "Unknown serialized type"

    invoke-direct {v0, v1}, Ljava/io/StreamCorruptedException;-><init>(Ljava/lang/String;)V

    throw v0

    nop

    :pswitch_data_44
    .packed-switch 0x1
        :pswitch_4
        :pswitch_9
        :pswitch_e
        :pswitch_13
        :pswitch_18
        :pswitch_1d
        :pswitch_22
        :pswitch_27
        :pswitch_3b
        :pswitch_3b
        :pswitch_2c
        :pswitch_31
        :pswitch_36
    .end packed-switch
.end method

.method private readResolve()Ljava/lang/Object;
    .registers 2

    .line 200
    iget-object v0, p0, Lorg/threeten/bp/chrono/Ser;->object:Ljava/lang/Object;

    return-object v0
.end method

.method private static writeInternal(BLjava/lang/Object;Ljava/io/ObjectOutput;)V
    .registers 5
    .param p0, "type"    # B
    .param p1, "object"    # Ljava/lang/Object;
    .param p2, "out"    # Ljava/io/ObjectOutput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 119
    invoke-interface {p2, p0}, Ljava/io/ObjectOutput;->writeByte(I)V

    .line 120
    packed-switch p0, :pswitch_data_5e

    goto/16 :goto_55

    .line 122
    :pswitch_8
    move-object v0, p1

    check-cast v0, Lorg/threeten/bp/chrono/JapaneseDate;

    invoke-virtual {v0, p2}, Lorg/threeten/bp/chrono/JapaneseDate;->writeExternal(Ljava/io/DataOutput;)V

    .line 123
    goto :goto_5d

    .line 125
    :pswitch_f
    move-object v0, p1

    check-cast v0, Lorg/threeten/bp/chrono/JapaneseEra;

    invoke-virtual {v0, p2}, Lorg/threeten/bp/chrono/JapaneseEra;->writeExternal(Ljava/io/DataOutput;)V

    .line 126
    goto :goto_5d

    .line 128
    :pswitch_16
    move-object v0, p1

    check-cast v0, Lorg/threeten/bp/chrono/HijrahDate;

    invoke-virtual {v0, p2}, Lorg/threeten/bp/chrono/HijrahDate;->writeExternal(Ljava/io/DataOutput;)V

    .line 129
    goto :goto_5d

    .line 131
    :pswitch_1d
    move-object v0, p1

    check-cast v0, Lorg/threeten/bp/chrono/HijrahEra;

    invoke-virtual {v0, p2}, Lorg/threeten/bp/chrono/HijrahEra;->writeExternal(Ljava/io/DataOutput;)V

    .line 132
    goto :goto_5d

    .line 134
    :pswitch_24
    move-object v0, p1

    check-cast v0, Lorg/threeten/bp/chrono/MinguoDate;

    invoke-virtual {v0, p2}, Lorg/threeten/bp/chrono/MinguoDate;->writeExternal(Ljava/io/DataOutput;)V

    .line 135
    goto :goto_5d

    .line 137
    :pswitch_2b
    move-object v0, p1

    check-cast v0, Lorg/threeten/bp/chrono/MinguoEra;

    invoke-virtual {v0, p2}, Lorg/threeten/bp/chrono/MinguoEra;->writeExternal(Ljava/io/DataOutput;)V

    .line 138
    goto :goto_5d

    .line 140
    :pswitch_32
    move-object v0, p1

    check-cast v0, Lorg/threeten/bp/chrono/ThaiBuddhistDate;

    invoke-virtual {v0, p2}, Lorg/threeten/bp/chrono/ThaiBuddhistDate;->writeExternal(Ljava/io/DataOutput;)V

    .line 141
    goto :goto_5d

    .line 143
    :pswitch_39
    move-object v0, p1

    check-cast v0, Lorg/threeten/bp/chrono/ThaiBuddhistEra;

    invoke-virtual {v0, p2}, Lorg/threeten/bp/chrono/ThaiBuddhistEra;->writeExternal(Ljava/io/DataOutput;)V

    .line 144
    goto :goto_5d

    .line 146
    :pswitch_40
    move-object v0, p1

    check-cast v0, Lorg/threeten/bp/chrono/Chronology;

    invoke-virtual {v0, p2}, Lorg/threeten/bp/chrono/Chronology;->writeExternal(Ljava/io/DataOutput;)V

    .line 147
    goto :goto_5d

    .line 149
    :pswitch_47
    move-object v0, p1

    check-cast v0, Lorg/threeten/bp/chrono/ChronoLocalDateTimeImpl;

    invoke-virtual {v0, p2}, Lorg/threeten/bp/chrono/ChronoLocalDateTimeImpl;->writeExternal(Ljava/io/ObjectOutput;)V

    .line 150
    goto :goto_5d

    .line 152
    :pswitch_4e
    move-object v0, p1

    check-cast v0, Lorg/threeten/bp/chrono/ChronoZonedDateTimeImpl;

    invoke-virtual {v0, p2}, Lorg/threeten/bp/chrono/ChronoZonedDateTimeImpl;->writeExternal(Ljava/io/ObjectOutput;)V

    .line 153
    goto :goto_5d

    .line 155
    :goto_55
    :pswitch_55
    new-instance v0, Ljava/io/InvalidClassException;

    const-string v1, "Unknown serialized type"

    invoke-direct {v0, v1}, Ljava/io/InvalidClassException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 157
    :goto_5d
    return-void

    :pswitch_data_5e
    .packed-switch 0x1
        :pswitch_8
        :pswitch_f
        :pswitch_16
        :pswitch_1d
        :pswitch_24
        :pswitch_2b
        :pswitch_32
        :pswitch_39
        :pswitch_55
        :pswitch_55
        :pswitch_40
        :pswitch_47
        :pswitch_4e
    .end packed-switch
.end method


# virtual methods
.method public readExternal(Ljava/io/ObjectInput;)V
    .registers 3
    .param p1, "in"    # Ljava/io/ObjectInput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .line 167
    invoke-interface {p1}, Ljava/io/ObjectInput;->readByte()B

    move-result v0

    iput-byte v0, p0, Lorg/threeten/bp/chrono/Ser;->type:B

    .line 168
    iget-byte v0, p0, Lorg/threeten/bp/chrono/Ser;->type:B

    invoke-static {v0, p1}, Lorg/threeten/bp/chrono/Ser;->readInternal(BLjava/io/ObjectInput;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lorg/threeten/bp/chrono/Ser;->object:Ljava/lang/Object;

    .line 169
    return-void
.end method

.method public writeExternal(Ljava/io/ObjectOutput;)V
    .registers 4
    .param p1, "out"    # Ljava/io/ObjectOutput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 115
    iget-byte v0, p0, Lorg/threeten/bp/chrono/Ser;->type:B

    iget-object v1, p0, Lorg/threeten/bp/chrono/Ser;->object:Ljava/lang/Object;

    invoke-static {v0, v1, p1}, Lorg/threeten/bp/chrono/Ser;->writeInternal(BLjava/lang/Object;Ljava/io/ObjectOutput;)V

    .line 116
    return-void
.end method

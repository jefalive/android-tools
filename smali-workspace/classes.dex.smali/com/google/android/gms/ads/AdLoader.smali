.class public Lcom/google/android/gms/ads/AdLoader;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/ads/AdLoader$Builder;
    }
.end annotation


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final zzoB:Lcom/google/android/gms/ads/internal/client/zzh;

.field private final zzoC:Lcom/google/android/gms/ads/internal/client/zzr;


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/google/android/gms/ads/internal/client/zzr;)V
    .registers 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "adLoader"    # Lcom/google/android/gms/ads/internal/client/zzr;

    invoke-static {}, Lcom/google/android/gms/ads/internal/client/zzh;->zzcO()Lcom/google/android/gms/ads/internal/client/zzh;

    move-result-object v0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/gms/ads/AdLoader;-><init>(Landroid/content/Context;Lcom/google/android/gms/ads/internal/client/zzr;Lcom/google/android/gms/ads/internal/client/zzh;)V

    return-void
.end method

.method constructor <init>(Landroid/content/Context;Lcom/google/android/gms/ads/internal/client/zzr;Lcom/google/android/gms/ads/internal/client/zzh;)V
    .registers 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "adLoader"    # Lcom/google/android/gms/ads/internal/client/zzr;
    .param p3, "parcelFactory"    # Lcom/google/android/gms/ads/internal/client/zzh;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/gms/ads/AdLoader;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/android/gms/ads/AdLoader;->zzoC:Lcom/google/android/gms/ads/internal/client/zzr;

    iput-object p3, p0, Lcom/google/android/gms/ads/AdLoader;->zzoB:Lcom/google/android/gms/ads/internal/client/zzh;

    return-void
.end method

.method private zza(Lcom/google/android/gms/ads/internal/client/zzaa;)V
    .registers 6

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/ads/AdLoader;->zzoC:Lcom/google/android/gms/ads/internal/client/zzr;

    iget-object v1, p0, Lcom/google/android/gms/ads/AdLoader;->zzoB:Lcom/google/android/gms/ads/internal/client/zzh;

    iget-object v2, p0, Lcom/google/android/gms/ads/AdLoader;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v2, p1}, Lcom/google/android/gms/ads/internal/client/zzh;->zza(Landroid/content/Context;Lcom/google/android/gms/ads/internal/client/zzaa;)Lcom/google/android/gms/ads/internal/client/AdRequestParcel;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/gms/ads/internal/client/zzr;->zzf(Lcom/google/android/gms/ads/internal/client/AdRequestParcel;)V
    :try_end_d
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_d} :catch_e

    goto :goto_14

    :catch_e
    move-exception v3

    const-string v0, "Failed to load ad."

    invoke-static {v0, v3}, Lcom/google/android/gms/ads/internal/util/client/zzb;->zzb(Ljava/lang/String;Ljava/lang/Throwable;)V

    :goto_14
    return-void
.end method


# virtual methods
.method public loadAd(Lcom/google/android/gms/ads/AdRequest;)V
    .registers 3
    .param p1, "adRequest"    # Lcom/google/android/gms/ads/AdRequest;

    invoke-virtual {p1}, Lcom/google/android/gms/ads/AdRequest;->zzaE()Lcom/google/android/gms/ads/internal/client/zzaa;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/ads/AdLoader;->zza(Lcom/google/android/gms/ads/internal/client/zzaa;)V

    return-void
.end method

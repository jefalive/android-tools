.class public Lbr/com/itau/textovoz/ui/fragment/FaqListFragment;
.super Landroid/support/v4/app/Fragment;
.source "FaqListFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lbr/com/itau/textovoz/ui/fragment/FaqListFragment$FaqAdapter;,
        Lbr/com/itau/textovoz/ui/fragment/FaqListFragment$FaqHolder;,
        Lbr/com/itau/textovoz/ui/fragment/FaqListFragment$FaqListListener;
    }
.end annotation


# static fields
.field private static ARG_KEY_FAQS:Ljava/lang/String;

.field private static ARG_KEY_SEGMENT:Ljava/lang/String;

.field private static ARG_KEY_USE_CARD:Ljava/lang/String;


# instance fields
.field private faqAdapter:Lbr/com/itau/textovoz/ui/fragment/FaqListFragment$FaqAdapter;

.field private faqs:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Lbr/com/itau/textovoz/model/Faq;>;"
        }
    .end annotation
.end field

.field private faqsSubList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Lbr/com/itau/textovoz/model/Faq;>;"
        }
    .end annotation
.end field

.field private listener:Lbr/com/itau/textovoz/ui/fragment/FaqListFragment$FaqListListener;

.field private moreItemsLinear:Landroid/widget/LinearLayout;

.field private moreItemsText:Lbr/com/itau/textovoz/ui/view/custom/CustomTextView;

.field private segment:Ljava/lang/String;

.field private useCardView:Z


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .line 30
    const-string v0, "faqs_key"

    sput-object v0, Lbr/com/itau/textovoz/ui/fragment/FaqListFragment;->ARG_KEY_FAQS:Ljava/lang/String;

    .line 31
    const-string v0, "segment_key"

    sput-object v0, Lbr/com/itau/textovoz/ui/fragment/FaqListFragment;->ARG_KEY_SEGMENT:Ljava/lang/String;

    .line 32
    const-string v0, "use_cardview_key"

    sput-object v0, Lbr/com/itau/textovoz/ui/fragment/FaqListFragment;->ARG_KEY_USE_CARD:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .registers 1

    .line 29
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lbr/com/itau/textovoz/ui/fragment/FaqListFragment;)Ljava/util/List;
    .registers 2
    .param p0, "x0"    # Lbr/com/itau/textovoz/ui/fragment/FaqListFragment;

    .line 29
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/fragment/FaqListFragment;->faqs:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$100(Lbr/com/itau/textovoz/ui/fragment/FaqListFragment;)Lbr/com/itau/textovoz/ui/fragment/FaqListFragment$FaqAdapter;
    .registers 2
    .param p0, "x0"    # Lbr/com/itau/textovoz/ui/fragment/FaqListFragment;

    .line 29
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/fragment/FaqListFragment;->faqAdapter:Lbr/com/itau/textovoz/ui/fragment/FaqListFragment$FaqAdapter;

    return-object v0
.end method

.method static synthetic access$200(Lbr/com/itau/textovoz/ui/fragment/FaqListFragment;)Z
    .registers 2
    .param p0, "x0"    # Lbr/com/itau/textovoz/ui/fragment/FaqListFragment;

    .line 29
    iget-boolean v0, p0, Lbr/com/itau/textovoz/ui/fragment/FaqListFragment;->useCardView:Z

    return v0
.end method

.method static synthetic access$300(Lbr/com/itau/textovoz/ui/fragment/FaqListFragment;)Ljava/lang/String;
    .registers 2
    .param p0, "x0"    # Lbr/com/itau/textovoz/ui/fragment/FaqListFragment;

    .line 29
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/fragment/FaqListFragment;->segment:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$400(Lbr/com/itau/textovoz/ui/fragment/FaqListFragment;)Lbr/com/itau/textovoz/ui/fragment/FaqListFragment$FaqListListener;
    .registers 2
    .param p0, "x0"    # Lbr/com/itau/textovoz/ui/fragment/FaqListFragment;

    .line 29
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/fragment/FaqListFragment;->listener:Lbr/com/itau/textovoz/ui/fragment/FaqListFragment$FaqListListener;

    return-object v0
.end method

.method public static newInstance(Ljava/util/ArrayList;Ljava/lang/String;Z)Lbr/com/itau/textovoz/ui/fragment/FaqListFragment;
    .registers 6
    .param p0, "faqs"    # Ljava/util/ArrayList;
    .param p1, "segment"    # Ljava/lang/String;
    .param p2, "useCardView"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/ArrayList<Lbr/com/itau/textovoz/model/Faq;>;Ljava/lang/String;Z)Lbr/com/itau/textovoz/ui/fragment/FaqListFragment;"
        }
    .end annotation

    .line 55
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 56
    .local v1, "bundle":Landroid/os/Bundle;
    sget-object v0, Lbr/com/itau/textovoz/ui/fragment/FaqListFragment;->ARG_KEY_FAQS:Ljava/lang/String;

    invoke-virtual {v1, v0, p0}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 57
    sget-object v0, Lbr/com/itau/textovoz/ui/fragment/FaqListFragment;->ARG_KEY_SEGMENT:Ljava/lang/String;

    invoke-virtual {v1, v0, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 58
    sget-object v0, Lbr/com/itau/textovoz/ui/fragment/FaqListFragment;->ARG_KEY_USE_CARD:Ljava/lang/String;

    invoke-virtual {v1, v0, p2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 60
    new-instance v2, Lbr/com/itau/textovoz/ui/fragment/FaqListFragment;

    invoke-direct {v2}, Lbr/com/itau/textovoz/ui/fragment/FaqListFragment;-><init>()V

    .line 61
    .local v2, "f":Lbr/com/itau/textovoz/ui/fragment/FaqListFragment;
    invoke-virtual {v2, v1}, Lbr/com/itau/textovoz/ui/fragment/FaqListFragment;->setArguments(Landroid/os/Bundle;)V

    .line 62
    return-object v2
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .registers 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 67
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 69
    invoke-virtual {p0}, Lbr/com/itau/textovoz/ui/fragment/FaqListFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    sget-object v1, Lbr/com/itau/textovoz/ui/fragment/FaqListFragment;->ARG_KEY_FAQS:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/textovoz/ui/fragment/FaqListFragment;->faqs:Ljava/util/List;

    .line 70
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lbr/com/itau/textovoz/ui/fragment/FaqListFragment;->faqsSubList:Ljava/util/List;

    .line 71
    invoke-virtual {p0}, Lbr/com/itau/textovoz/ui/fragment/FaqListFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    sget-object v1, Lbr/com/itau/textovoz/ui/fragment/FaqListFragment;->ARG_KEY_SEGMENT:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/textovoz/ui/fragment/FaqListFragment;->segment:Ljava/lang/String;

    .line 72
    invoke-virtual {p0}, Lbr/com/itau/textovoz/ui/fragment/FaqListFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    sget-object v1, Lbr/com/itau/textovoz/ui/fragment/FaqListFragment;->ARG_KEY_USE_CARD:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lbr/com/itau/textovoz/ui/fragment/FaqListFragment;->useCardView:Z

    .line 74
    iget-boolean v0, p0, Lbr/com/itau/textovoz/ui/fragment/FaqListFragment;->useCardView:Z

    if-eqz v0, :cond_45

    .line 75
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/fragment/FaqListFragment;->faqs:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x2

    if-le v0, v1, :cond_45

    .line 76
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/fragment/FaqListFragment;->faqs:Ljava/util/List;

    const/4 v1, 0x0

    const/4 v2, 0x2

    invoke-interface {v0, v1, v2}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/textovoz/ui/fragment/FaqListFragment;->faqsSubList:Ljava/util/List;

    .line 79
    :cond_45
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .registers 11
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .line 84
    sget v0, Lbr/com/itau/textovoz/R$layout;->fragment_faq_list:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    .line 85
    .local v3, "v":Landroid/view/View;
    sget v0, Lbr/com/itau/textovoz/R$id;->text_name_title_list:I

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lbr/com/itau/textovoz/ui/view/custom/CustomTextView;

    .line 86
    .local v4, "headerTitleTextView":Lbr/com/itau/textovoz/ui/view/custom/CustomTextView;
    sget v0, Lbr/com/itau/textovoz/R$string;->view_item_search_freq:I

    invoke-virtual {p0, v0}, Lbr/com/itau/textovoz/ui/fragment/FaqListFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Lbr/com/itau/textovoz/ui/view/custom/CustomTextView;->setText(Ljava/lang/CharSequence;)V

    .line 88
    sget v0, Lbr/com/itau/textovoz/R$id;->recycler_view_faq_search:I

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Landroid/support/v7/widget/RecyclerView;

    .line 89
    .local v5, "recyclerView":Landroid/support/v7/widget/RecyclerView;
    const/4 v0, 0x0

    invoke-virtual {v5, v0}, Landroid/support/v7/widget/RecyclerView;->setNestedScrollingEnabled(Z)V

    .line 90
    new-instance v0, Landroid/support/v7/widget/LinearLayoutManager;

    invoke-virtual {p0}, Lbr/com/itau/textovoz/ui/fragment/FaqListFragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v7/widget/LinearLayoutManager;-><init>(Landroid/content/Context;)V

    invoke-virtual {v5, v0}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(Landroid/support/v7/widget/RecyclerView$LayoutManager;)V

    .line 91
    new-instance v0, Landroid/support/v7/widget/DefaultItemAnimator;

    invoke-direct {v0}, Landroid/support/v7/widget/DefaultItemAnimator;-><init>()V

    invoke-virtual {v5, v0}, Landroid/support/v7/widget/RecyclerView;->setItemAnimator(Landroid/support/v7/widget/RecyclerView$ItemAnimator;)V

    .line 93
    sget v0, Lbr/com/itau/textovoz/R$id;->more_items_btn:I

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lbr/com/itau/textovoz/ui/fragment/FaqListFragment;->moreItemsLinear:Landroid/widget/LinearLayout;

    .line 94
    sget v0, Lbr/com/itau/textovoz/R$id;->edit_query_more:I

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lbr/com/itau/textovoz/ui/view/custom/CustomTextView;

    iput-object v0, p0, Lbr/com/itau/textovoz/ui/fragment/FaqListFragment;->moreItemsText:Lbr/com/itau/textovoz/ui/view/custom/CustomTextView;

    .line 95
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/fragment/FaqListFragment;->moreItemsText:Lbr/com/itau/textovoz/ui/view/custom/CustomTextView;

    sget v1, Lbr/com/itau/textovoz/R$string;->query_common:I

    invoke-virtual {p0, v1}, Lbr/com/itau/textovoz/ui/fragment/FaqListFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbr/com/itau/textovoz/ui/view/custom/CustomTextView;->setText(Ljava/lang/CharSequence;)V

    .line 97
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/fragment/FaqListFragment;->moreItemsLinear:Landroid/widget/LinearLayout;

    new-instance v1, Lbr/com/itau/textovoz/ui/fragment/FaqListFragment$1;

    invoke-direct {v1, p0}, Lbr/com/itau/textovoz/ui/fragment/FaqListFragment$1;-><init>(Lbr/com/itau/textovoz/ui/fragment/FaqListFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 106
    iget-boolean v0, p0, Lbr/com/itau/textovoz/ui/fragment/FaqListFragment;->useCardView:Z

    if-nez v0, :cond_85

    .line 107
    invoke-virtual {p0}, Lbr/com/itau/textovoz/ui/fragment/FaqListFragment;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lbr/com/itau/textovoz/R$drawable;->divider_search_list:I

    invoke-static {v0, v1}, Landroid/support/v4/content/ContextCompat;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    .line 108
    .local v6, "divider":Landroid/graphics/drawable/Drawable;
    new-instance v0, Lbr/com/itau/textovoz/ui/view/DividerItemDecoration;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-direct {v0, v6, v1, v2}, Lbr/com/itau/textovoz/ui/view/DividerItemDecoration;-><init>(Landroid/graphics/drawable/Drawable;ZZ)V

    invoke-virtual {v5, v0}, Landroid/support/v7/widget/RecyclerView;->addItemDecoration(Landroid/support/v7/widget/RecyclerView$ItemDecoration;)V

    .line 110
    new-instance v0, Lbr/com/itau/textovoz/ui/fragment/FaqListFragment$FaqAdapter;

    iget-object v1, p0, Lbr/com/itau/textovoz/ui/fragment/FaqListFragment;->faqs:Ljava/util/List;

    invoke-direct {v0, p0, v1}, Lbr/com/itau/textovoz/ui/fragment/FaqListFragment$FaqAdapter;-><init>(Lbr/com/itau/textovoz/ui/fragment/FaqListFragment;Ljava/util/List;)V

    iput-object v0, p0, Lbr/com/itau/textovoz/ui/fragment/FaqListFragment;->faqAdapter:Lbr/com/itau/textovoz/ui/fragment/FaqListFragment$FaqAdapter;

    .line 112
    .end local v6    # "divider":Landroid/graphics/drawable/Drawable;
    goto :goto_a6

    .line 113
    :cond_85
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/fragment/FaqListFragment;->faqsSubList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_9d

    .line 114
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/fragment/FaqListFragment;->moreItemsLinear:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 116
    new-instance v0, Lbr/com/itau/textovoz/ui/fragment/FaqListFragment$FaqAdapter;

    iget-object v1, p0, Lbr/com/itau/textovoz/ui/fragment/FaqListFragment;->faqsSubList:Ljava/util/List;

    invoke-direct {v0, p0, v1}, Lbr/com/itau/textovoz/ui/fragment/FaqListFragment$FaqAdapter;-><init>(Lbr/com/itau/textovoz/ui/fragment/FaqListFragment;Ljava/util/List;)V

    iput-object v0, p0, Lbr/com/itau/textovoz/ui/fragment/FaqListFragment;->faqAdapter:Lbr/com/itau/textovoz/ui/fragment/FaqListFragment$FaqAdapter;

    goto :goto_a6

    .line 118
    :cond_9d
    new-instance v0, Lbr/com/itau/textovoz/ui/fragment/FaqListFragment$FaqAdapter;

    iget-object v1, p0, Lbr/com/itau/textovoz/ui/fragment/FaqListFragment;->faqs:Ljava/util/List;

    invoke-direct {v0, p0, v1}, Lbr/com/itau/textovoz/ui/fragment/FaqListFragment$FaqAdapter;-><init>(Lbr/com/itau/textovoz/ui/fragment/FaqListFragment;Ljava/util/List;)V

    iput-object v0, p0, Lbr/com/itau/textovoz/ui/fragment/FaqListFragment;->faqAdapter:Lbr/com/itau/textovoz/ui/fragment/FaqListFragment$FaqAdapter;

    .line 122
    :goto_a6
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/fragment/FaqListFragment;->faqAdapter:Lbr/com/itau/textovoz/ui/fragment/FaqListFragment$FaqAdapter;

    invoke-virtual {v5, v0}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 124
    return-object v3
.end method

.method public setFaqListListener(Lbr/com/itau/textovoz/ui/fragment/FaqListFragment$FaqListListener;)V
    .registers 2
    .param p1, "listener"    # Lbr/com/itau/textovoz/ui/fragment/FaqListFragment$FaqListListener;

    .line 51
    iput-object p1, p0, Lbr/com/itau/textovoz/ui/fragment/FaqListFragment;->listener:Lbr/com/itau/textovoz/ui/fragment/FaqListFragment$FaqListListener;

    .line 52
    return-void
.end method

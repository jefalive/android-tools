.class public final Lorg/threeten/bp/jdk8/Jdk8Methods;
.super Ljava/lang/Object;
.source "Jdk8Methods.java"


# direct methods
.method public static compareInts(II)I
    .registers 3
    .param p0, "a"    # I
    .param p1, "b"    # I

    .line 110
    if-ge p0, p1, :cond_4

    .line 111
    const/4 v0, -0x1

    return v0

    .line 113
    :cond_4
    if-le p0, p1, :cond_8

    .line 114
    const/4 v0, 0x1

    return v0

    .line 116
    :cond_8
    const/4 v0, 0x0

    return v0
.end method

.method public static compareLongs(JJ)I
    .registers 5
    .param p0, "a"    # J
    .param p2, "b"    # J

    .line 127
    cmp-long v0, p0, p2

    if-gez v0, :cond_6

    .line 128
    const/4 v0, -0x1

    return v0

    .line 130
    :cond_6
    cmp-long v0, p0, p2

    if-lez v0, :cond_c

    .line 131
    const/4 v0, 0x1

    return v0

    .line 133
    :cond_c
    const/4 v0, 0x0

    return v0
.end method

.method public static equals(Ljava/lang/Object;Ljava/lang/Object;)Z
    .registers 3
    .param p0, "a"    # Ljava/lang/Object;
    .param p1, "b"    # Ljava/lang/Object;

    .line 93
    if-nez p0, :cond_8

    .line 94
    if-nez p1, :cond_6

    const/4 v0, 0x1

    goto :goto_7

    :cond_6
    const/4 v0, 0x0

    :goto_7
    return v0

    .line 96
    :cond_8
    if-nez p1, :cond_c

    .line 97
    const/4 v0, 0x0

    return v0

    .line 99
    :cond_c
    invoke-virtual {p0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public static floorDiv(JJ)J
    .registers 8
    .param p0, "a"    # J
    .param p2, "b"    # J

    .line 306
    const-wide/16 v0, 0x0

    cmp-long v0, p0, v0

    if-ltz v0, :cond_9

    div-long v0, p0, p2

    goto :goto_10

    :cond_9
    const-wide/16 v0, 0x1

    add-long/2addr v0, p0

    div-long/2addr v0, p2

    const-wide/16 v2, 0x1

    sub-long/2addr v0, v2

    :goto_10
    return-wide v0
.end method

.method public static floorMod(II)I
    .registers 3
    .param p0, "a"    # I
    .param p1, "b"    # I

    .line 381
    rem-int v0, p0, p1

    add-int/2addr v0, p1

    rem-int/2addr v0, p1

    return v0
.end method

.method public static floorMod(JI)I
    .registers 7
    .param p0, "a"    # J
    .param p2, "b"    # I

    .line 341
    int-to-long v0, p2

    rem-long v0, p0, v0

    int-to-long v2, p2

    add-long/2addr v0, v2

    int-to-long v2, p2

    rem-long/2addr v0, v2

    long-to-int v0, v0

    return v0
.end method

.method public static floorMod(JJ)J
    .registers 6
    .param p0, "a"    # J
    .param p2, "b"    # J

    .line 323
    rem-long v0, p0, p2

    add-long/2addr v0, p2

    rem-long/2addr v0, p2

    return-wide v0
.end method

.method public static requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;
    .registers 5
    .param p0, "value"    # Ljava/lang/Object;
    .param p1, "parameterName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:Ljava/lang/Object;>(TT;Ljava/lang/String;)TT;"
        }
    .end annotation

    .line 78
    if-nez p0, :cond_1b

    .line 79
    new-instance v0, Ljava/lang/NullPointerException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " must not be null"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 81
    :cond_1b
    return-object p0
.end method

.method public static safeAdd(II)I
    .registers 6
    .param p0, "a"    # I
    .param p1, "b"    # I

    .line 146
    add-int v3, p0, p1

    .line 148
    .local v3, "sum":I
    xor-int v0, p0, v3

    if-gez v0, :cond_2d

    xor-int v0, p0, p1

    if-ltz v0, :cond_2d

    .line 149
    new-instance v0, Ljava/lang/ArithmeticException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Addition overflows an int: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " + "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ArithmeticException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 151
    :cond_2d
    return v3
.end method

.method public static safeAdd(JJ)J
    .registers 10
    .param p0, "a"    # J
    .param p2, "b"    # J

    .line 163
    add-long v4, p0, p2

    .line 165
    .local v4, "sum":J
    xor-long v0, p0, v4

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-gez v0, :cond_35

    xor-long v0, p0, p2

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-ltz v0, :cond_35

    .line 166
    new-instance v0, Ljava/lang/ArithmeticException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Addition overflows a long: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " + "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ArithmeticException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 168
    :cond_35
    return-wide v4
.end method

.method public static safeMultiply(JI)J
    .registers 8
    .param p0, "a"    # J
    .param p2, "b"    # I

    .line 232
    packed-switch p2, :pswitch_data_62

    goto :goto_33

    .line 234
    :pswitch_4
    const-wide/high16 v0, -0x8000000000000000L

    cmp-long v0, p0, v0

    if-nez v0, :cond_2d

    .line 235
    new-instance v0, Ljava/lang/ArithmeticException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Multiplication overflows a long: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " * "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ArithmeticException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 237
    :cond_2d
    neg-long v0, p0

    return-wide v0

    .line 239
    :pswitch_2f
    const-wide/16 v0, 0x0

    return-wide v0

    .line 241
    :pswitch_32
    return-wide p0

    .line 243
    :goto_33
    int-to-long v0, p2

    mul-long v3, p0, v0

    .line 244
    .local v3, "total":J
    int-to-long v0, p2

    div-long v0, v3, v0

    cmp-long v0, v0, p0

    if-eqz v0, :cond_60

    .line 245
    new-instance v0, Ljava/lang/ArithmeticException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Multiplication overflows a long: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " * "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ArithmeticException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 247
    :cond_60
    return-wide v3

    nop

    :pswitch_data_62
    .packed-switch -0x1
        :pswitch_4
        :pswitch_2f
        :pswitch_32
    .end packed-switch
.end method

.method public static safeMultiply(JJ)J
    .registers 9
    .param p0, "a"    # J
    .param p2, "b"    # J

    .line 259
    const-wide/16 v0, 0x1

    cmp-long v0, p2, v0

    if-nez v0, :cond_7

    .line 260
    return-wide p0

    .line 262
    :cond_7
    const-wide/16 v0, 0x1

    cmp-long v0, p0, v0

    if-nez v0, :cond_e

    .line 263
    return-wide p2

    .line 265
    :cond_e
    const-wide/16 v0, 0x0

    cmp-long v0, p0, v0

    if-eqz v0, :cond_1a

    const-wide/16 v0, 0x0

    cmp-long v0, p2, v0

    if-nez v0, :cond_1d

    .line 266
    :cond_1a
    const-wide/16 v0, 0x0

    return-wide v0

    .line 268
    :cond_1d
    mul-long v3, p0, p2

    .line 269
    .local v3, "total":J
    div-long v0, v3, p2

    cmp-long v0, v0, p0

    if-nez v0, :cond_3d

    const-wide/high16 v0, -0x8000000000000000L

    cmp-long v0, p0, v0

    if-nez v0, :cond_31

    const-wide/16 v0, -0x1

    cmp-long v0, p2, v0

    if-eqz v0, :cond_3d

    :cond_31
    const-wide/high16 v0, -0x8000000000000000L

    cmp-long v0, p2, v0

    if-nez v0, :cond_60

    const-wide/16 v0, -0x1

    cmp-long v0, p0, v0

    if-nez v0, :cond_60

    .line 270
    :cond_3d
    new-instance v0, Ljava/lang/ArithmeticException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Multiplication overflows a long: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " * "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ArithmeticException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 272
    :cond_60
    return-wide v3
.end method

.method public static safeSubtract(II)I
    .registers 6
    .param p0, "a"    # I
    .param p1, "b"    # I

    .line 181
    sub-int v3, p0, p1

    .line 183
    .local v3, "result":I
    xor-int v0, p0, v3

    if-gez v0, :cond_2d

    xor-int v0, p0, p1

    if-gez v0, :cond_2d

    .line 184
    new-instance v0, Ljava/lang/ArithmeticException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Subtraction overflows an int: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ArithmeticException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 186
    :cond_2d
    return v3
.end method

.method public static safeSubtract(JJ)J
    .registers 10
    .param p0, "a"    # J
    .param p2, "b"    # J

    .line 198
    sub-long v4, p0, p2

    .line 200
    .local v4, "result":J
    xor-long v0, p0, v4

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-gez v0, :cond_35

    xor-long v0, p0, p2

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-gez v0, :cond_35

    .line 201
    new-instance v0, Ljava/lang/ArithmeticException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Subtraction overflows a long: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ArithmeticException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 203
    :cond_35
    return-wide v4
.end method

.method public static safeToInt(J)I
    .registers 5
    .param p0, "value"    # J

    .line 284
    const-wide/32 v0, 0x7fffffff

    cmp-long v0, p0, v0

    if-gtz v0, :cond_e

    const-wide/32 v0, -0x80000000

    cmp-long v0, p0, v0

    if-gez v0, :cond_27

    .line 285
    :cond_e
    new-instance v0, Ljava/lang/ArithmeticException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Calculation overflows an int: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ArithmeticException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 287
    :cond_27
    long-to-int v0, p0

    return v0
.end method

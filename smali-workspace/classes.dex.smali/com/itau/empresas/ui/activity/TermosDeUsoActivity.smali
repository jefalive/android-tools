.class public Lcom/itau/empresas/ui/activity/TermosDeUsoActivity;
.super Lcom/itau/empresas/ui/activity/BaseActivity;
.source "TermosDeUsoActivity.java"


# instance fields
.field toolBar:Landroid/support/v7/widget/Toolbar;

.field webView:Landroid/webkit/WebView;


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 24
    invoke-direct {p0}, Lcom/itau/empresas/ui/activity/BaseActivity;-><init>()V

    return-void
.end method

.method private constroiToolbar()V
    .registers 3

    .line 67
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/TermosDeUsoActivity;->toolBar:Landroid/support/v7/widget/Toolbar;

    invoke-static {v0, p0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom;->Builder(Landroid/support/v7/widget/Toolbar;Landroid/support/v7/app/AppCompatActivity;)Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    move-result-object v0

    .line 68
    const v1, 0x7f07069c

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->comTitulo(I)Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    move-result-object v0

    .line 69
    invoke-virtual {v0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->comClickListenerFinish()Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    move-result-object v0

    .line 70
    invoke-virtual {v0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->build()Lcom/itau/empresas/ui/util/listener/ToolbarCustom;

    .line 71
    return-void
.end method

.method public static intent(Landroid/support/v4/app/Fragment;)Lcom/itau/empresas/ui/activity/TermosDeUsoActivity_$IntentBuilder_;
    .registers 2
    .param p0, "fragment"    # Landroid/support/v4/app/Fragment;

    .line 79
    new-instance v0, Lcom/itau/empresas/ui/activity/TermosDeUsoActivity_$IntentBuilder_;

    invoke-direct {v0, p0}, Lcom/itau/empresas/ui/activity/TermosDeUsoActivity_$IntentBuilder_;-><init>(Landroid/support/v4/app/Fragment;)V

    return-object v0
.end method


# virtual methods
.method aoIniciarActivity()V
    .registers 3

    .line 31
    invoke-direct {p0}, Lcom/itau/empresas/ui/activity/TermosDeUsoActivity;->constroiToolbar()V

    .line 33
    invoke-static {}, Lcom/itau/empresas/ui/util/PoyntUtils;->isPoynt()Z

    move-result v0

    if-eqz v0, :cond_1b

    .line 34
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/TermosDeUsoActivity;->toolBar:Landroid/support/v7/widget/Toolbar;

    const v1, 0x7f020165

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->setNavigationIcon(I)V

    .line 35
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/TermosDeUsoActivity;->toolBar:Landroid/support/v7/widget/Toolbar;

    new-instance v1, Lcom/itau/empresas/ui/activity/TermosDeUsoActivity$1;

    invoke-direct {v1, p0}, Lcom/itau/empresas/ui/activity/TermosDeUsoActivity$1;-><init>(Lcom/itau/empresas/ui/activity/TermosDeUsoActivity;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->setNavigationOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 43
    :cond_1b
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/TermosDeUsoActivity;->webView:Landroid/webkit/WebView;

    const v1, 0x7f0706cb

    invoke-virtual {p0, v1}, Lcom/itau/empresas/ui/activity/TermosDeUsoActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 44
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .registers 4
    .param p1, "menu"    # Landroid/view/Menu;

    .line 53
    invoke-virtual {p0}, Lcom/itau/empresas/ui/activity/TermosDeUsoActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f0f0007

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 54
    const/4 v0, 0x1

    return v0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .registers 4
    .param p1, "item"    # Landroid/view/MenuItem;

    .line 59
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x7f0e06d1

    if-ne v0, v1, :cond_e

    .line 60
    invoke-static {p0}, Lcom/itau/empresas/ui/util/ViewUtils;->sairDoApp(Landroid/content/Context;)V

    .line 61
    const/4 v0, 0x1

    return v0

    .line 63
    :cond_e
    invoke-super {p0, p1}, Lcom/itau/empresas/ui/activity/BaseActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0
.end method

.method public opkeysConsumidas()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Ljava/lang/String;>;"
        }
    .end annotation

    .line 48
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

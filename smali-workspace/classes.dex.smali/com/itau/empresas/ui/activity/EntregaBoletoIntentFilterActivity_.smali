.class public final Lcom/itau/empresas/ui/activity/EntregaBoletoIntentFilterActivity_;
.super Lcom/itau/empresas/ui/activity/EntregaBoletoIntentFilterActivity;
.source "EntregaBoletoIntentFilterActivity_.java"

# interfaces
.implements Lorg/androidannotations/api/view/HasViews;


# instance fields
.field private final onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;


# direct methods
.method public constructor <init>()V
    .registers 2

    .line 27
    invoke-direct {p0}, Lcom/itau/empresas/ui/activity/EntregaBoletoIntentFilterActivity;-><init>()V

    .line 31
    new-instance v0, Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-direct {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/ui/activity/EntregaBoletoIntentFilterActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    return-void
.end method

.method static synthetic access$001(Lcom/itau/empresas/ui/activity/EntregaBoletoIntentFilterActivity_;)V
    .registers 1
    .param p0, "x0"    # Lcom/itau/empresas/ui/activity/EntregaBoletoIntentFilterActivity_;

    .line 27
    invoke-super {p0}, Lcom/itau/empresas/ui/activity/EntregaBoletoIntentFilterActivity;->mostraDialogoDeProgresso()V

    return-void
.end method

.method static synthetic access$101(Lcom/itau/empresas/ui/activity/EntregaBoletoIntentFilterActivity_;)V
    .registers 1
    .param p0, "x0"    # Lcom/itau/empresas/ui/activity/EntregaBoletoIntentFilterActivity_;

    .line 27
    invoke-super {p0}, Lcom/itau/empresas/ui/activity/EntregaBoletoIntentFilterActivity;->escondeDialogoDeProgresso()V

    return-void
.end method

.method private init_(Landroid/os/Bundle;)V
    .registers 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 42
    new-instance v0, Lcom/itau/empresas/Preferences_;

    invoke-direct {v0, p0}, Lcom/itau/empresas/Preferences_;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/itau/empresas/ui/activity/EntregaBoletoIntentFilterActivity_;->prefs:Lcom/itau/empresas/Preferences_;

    .line 43
    invoke-static {}, Lcom/itau/empresas/CustomApplication_;->getInstance()Lcom/itau/empresas/CustomApplication;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/ui/activity/EntregaBoletoIntentFilterActivity_;->application:Lcom/itau/empresas/CustomApplication;

    .line 44
    invoke-virtual {p0}, Lcom/itau/empresas/ui/activity/EntregaBoletoIntentFilterActivity_;->afterInject()V

    .line 45
    return-void
.end method


# virtual methods
.method public escondeDialogoDeProgresso()V
    .registers 5

    .line 94
    const-string v0, ""

    new-instance v1, Lcom/itau/empresas/ui/activity/EntregaBoletoIntentFilterActivity_$2;

    invoke-direct {v1, p0}, Lcom/itau/empresas/ui/activity/EntregaBoletoIntentFilterActivity_$2;-><init>(Lcom/itau/empresas/ui/activity/EntregaBoletoIntentFilterActivity_;)V

    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3}, Lorg/androidannotations/api/UiThreadExecutor;->runTask(Ljava/lang/String;Ljava/lang/Runnable;J)V

    .line 102
    return-void
.end method

.method public mostraDialogoDeProgresso()V
    .registers 5
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "CommitTransaction"
        }
    .end annotation

    .line 82
    const-string v0, ""

    new-instance v1, Lcom/itau/empresas/ui/activity/EntregaBoletoIntentFilterActivity_$1;

    invoke-direct {v1, p0}, Lcom/itau/empresas/ui/activity/EntregaBoletoIntentFilterActivity_$1;-><init>(Lcom/itau/empresas/ui/activity/EntregaBoletoIntentFilterActivity_;)V

    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3}, Lorg/androidannotations/api/UiThreadExecutor;->runTask(Ljava/lang/String;Ljava/lang/Runnable;J)V

    .line 90
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 35
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/EntregaBoletoIntentFilterActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-static {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    move-result-object v1

    .line 36
    .local v1, "previousNotifier":Lorg/androidannotations/api/view/OnViewChangedNotifier;
    invoke-direct {p0, p1}, Lcom/itau/empresas/ui/activity/EntregaBoletoIntentFilterActivity_;->init_(Landroid/os/Bundle;)V

    .line 37
    invoke-super {p0, p1}, Lcom/itau/empresas/ui/activity/EntregaBoletoIntentFilterActivity;->onCreate(Landroid/os/Bundle;)V

    .line 38
    invoke-static {v1}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 39
    return-void
.end method

.method public setContentView(I)V
    .registers 3
    .param p1, "layoutResID"    # I

    .line 49
    invoke-super {p0, p1}, Lcom/itau/empresas/ui/activity/EntregaBoletoIntentFilterActivity;->setContentView(I)V

    .line 50
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/EntregaBoletoIntentFilterActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 51
    return-void
.end method

.method public setContentView(Landroid/view/View;)V
    .registers 3
    .param p1, "view"    # Landroid/view/View;

    .line 61
    invoke-super {p0, p1}, Lcom/itau/empresas/ui/activity/EntregaBoletoIntentFilterActivity;->setContentView(Landroid/view/View;)V

    .line 62
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/EntregaBoletoIntentFilterActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 63
    return-void
.end method

.method public setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .registers 4
    .param p1, "view"    # Landroid/view/View;
    .param p2, "params"    # Landroid/view/ViewGroup$LayoutParams;

    .line 55
    invoke-super {p0, p1, p2}, Lcom/itau/empresas/ui/activity/EntregaBoletoIntentFilterActivity;->setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 56
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/EntregaBoletoIntentFilterActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 57
    return-void
.end method

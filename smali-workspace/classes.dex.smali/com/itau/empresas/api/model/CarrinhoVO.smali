.class public Lcom/itau/empresas/api/model/CarrinhoVO;
.super Ljava/lang/Object;
.source "CarrinhoVO.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private codProduto:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "codigo_produto"
    .end annotation
.end field

.field private dataSimulacao:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "data_simulacao"
    .end annotation
.end field

.field private descritivo:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "descritivo"
    .end annotation
.end field

.field private opcao1:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "opcao_1"
    .end annotation
.end field

.field private opcao2:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "opcao_2"
    .end annotation
.end field

.field private opcao3:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "opcao_3"
    .end annotation
.end field

.field private tipoProduto:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "tipo_produto"
    .end annotation
.end field

.field private valor1:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "valor_1"
    .end annotation
.end field

.field private valor10:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "valor_10"
    .end annotation
.end field

.field private valor2:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "valor_2"
    .end annotation
.end field

.field private valor3:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "valor_3"
    .end annotation
.end field

.field private valor4:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "valor_4"
    .end annotation
.end field

.field private valor5:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "valor_5"
    .end annotation
.end field

.field private valor6:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "valor_6"
    .end annotation
.end field

.field private valor7:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "valor_7"
    .end annotation
.end field

.field private valor8:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "valor_8"
    .end annotation
.end field

.field private valor9:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "valor_9"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

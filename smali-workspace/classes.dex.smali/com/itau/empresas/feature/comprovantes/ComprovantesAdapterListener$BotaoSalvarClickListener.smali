.class Lcom/itau/empresas/feature/comprovantes/ComprovantesAdapterListener$BotaoSalvarClickListener;
.super Ljava/lang/Object;
.source "ComprovantesAdapterListener.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/itau/empresas/feature/comprovantes/ComprovantesAdapterListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "BotaoSalvarClickListener"
.end annotation


# instance fields
.field private final salvarClickListener:Landroid/view/View$OnClickListener;


# direct methods
.method constructor <init>(Landroid/view/View$OnClickListener;)V
    .registers 2
    .param p1, "salvarClickListener"    # Landroid/view/View$OnClickListener;

    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    iput-object p1, p0, Lcom/itau/empresas/feature/comprovantes/ComprovantesAdapterListener$BotaoSalvarClickListener;->salvarClickListener:Landroid/view/View$OnClickListener;

    .line 47
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .registers 3
    .param p1, "v"    # Landroid/view/View;

    .line 51
    iget-object v0, p0, Lcom/itau/empresas/feature/comprovantes/ComprovantesAdapterListener$BotaoSalvarClickListener;->salvarClickListener:Landroid/view/View$OnClickListener;

    if-eqz v0, :cond_9

    .line 52
    iget-object v0, p0, Lcom/itau/empresas/feature/comprovantes/ComprovantesAdapterListener$BotaoSalvarClickListener;->salvarClickListener:Landroid/view/View$OnClickListener;

    invoke-interface {v0, p1}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    .line 54
    :cond_9
    return-void
.end method

.class public Lcom/itau/empresas/feature/home/HomeFragment_$FragmentBuilder_;
.super Lorg/androidannotations/api/builder/FragmentBuilder;
.source "HomeFragment_.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/itau/empresas/feature/home/HomeFragment_;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "FragmentBuilder_"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/androidannotations/api/builder/FragmentBuilder<Lcom/itau/empresas/feature/home/HomeFragment_$FragmentBuilder_;Lcom/itau/empresas/feature/home/HomeFragment;>;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 86
    invoke-direct {p0}, Lorg/androidannotations/api/builder/FragmentBuilder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/itau/empresas/feature/home/HomeFragment;
    .registers 3

    .line 92
    new-instance v1, Lcom/itau/empresas/feature/home/HomeFragment_;

    invoke-direct {v1}, Lcom/itau/empresas/feature/home/HomeFragment_;-><init>()V

    .line 93
    .local v1, "fragment_":Lcom/itau/empresas/feature/home/HomeFragment_;
    iget-object v0, p0, Lcom/itau/empresas/feature/home/HomeFragment_$FragmentBuilder_;->args:Landroid/os/Bundle;

    invoke-virtual {v1, v0}, Lcom/itau/empresas/feature/home/HomeFragment_;->setArguments(Landroid/os/Bundle;)V

    .line 94
    return-object v1
.end method

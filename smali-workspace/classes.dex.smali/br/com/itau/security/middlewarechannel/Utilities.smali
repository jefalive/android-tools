.class public final Lbr/com/itau/security/middlewarechannel/Utilities;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static a:Ljava/lang/String;

.field private static d:J

.field private static final e:[B

.field private static f:I


# direct methods
.method static constructor <clinit>()V
    .registers 4

    .line 19
    const/16 v0, 0xbb

    new-array v0, v0, [B

    fill-array-data v0, :array_2c

    sput-object v0, Lbr/com/itau/security/middlewarechannel/Utilities;->e:[B

    const/16 v0, 0xf2

    sput v0, Lbr/com/itau/security/middlewarechannel/Utilities;->f:I

    sget-object v0, Lbr/com/itau/security/middlewarechannel/Utilities;->e:[B

    const/16 v1, 0x13

    aget-byte v0, v0, v1

    neg-int v0, v0

    int-to-byte v0, v0

    sget-object v1, Lbr/com/itau/security/middlewarechannel/Utilities;->e:[B

    const/16 v2, 0x14

    aget-byte v1, v1, v2

    int-to-byte v1, v1

    sget v2, Lbr/com/itau/security/middlewarechannel/Utilities;->f:I

    and-int/lit16 v2, v2, 0x3af

    int-to-short v2, v2

    invoke-static {v0, v1, v2}, Lbr/com/itau/security/middlewarechannel/Utilities;->a(ISI)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    sput-wide v0, Lbr/com/itau/security/middlewarechannel/Utilities;->d:J

    return-void

    :array_2c
    .array-data 1
        0x17t
        0x5ct
        -0x10t
        -0x4t
        0xft
        0x3t
        -0x1t
        0x6t
        -0x36t
        -0x8t
        0x3t
        0x36t
        0x2t
        0x10t
        0x0t
        0x4t
        0x0t
        0x8t
        -0x6t
        -0x34t
        0x3et
        0xet
        -0x10t
        0x17t
        -0x44t
        0x38t
        0xft
        0x1t
        -0x3ct
        0x37t
        0x13t
        -0x40t
        0x4at
        0x0t
        -0x2t
        -0x6t
        0x12t
        -0x42t
        0x4bt
        -0x13t
        0x12t
        -0x3bt
        0x0t
        0x41t
        0x5t
        -0xat
        0xat
        0x6t
        -0x4t
        0x16t
        -0x8t
        0x2t
        -0x3bt
        0x36t
        0x15t
        0x0t
        0x2ft
        0x9t
        -0x6t
        -0x5t
        -0x41t
        0x51t
        0x4t
        0x8t
        -0x51t
        0x4ft
        0x6t
        -0xbt
        0x6t
        -0x41t
        0x46t
        0xft
        0x2t
        0x9t
        -0xct
        0xct
        0x9t
        0xft
        0x3t
        -0x1t
        0x6t
        -0x36t
        -0x8t
        0x3t
        -0xbt
        0x10t
        0x7t
        -0xat
        -0x2t
        0xet
        0x6t
        -0x32t
        -0x18t
        0x9t
        0x9t
        -0x1t
        0x18t
        -0x13t
        -0x4t
        0x3at
        -0xbt
        0x10t
        0x7t
        -0xat
        -0x2t
        0xet
        0x6t
        -0x31t
        -0x13t
        0x9t
        -0x1t
        0x18t
        -0x13t
        0x16t
        -0xat
        0x47t
        -0xbt
        0x10t
        0x7t
        -0xat
        -0x2t
        0xet
        0x6t
        -0x31t
        0x47t
        -0xet
        0xbt
        -0x38t
        0x3et
        0xet
        -0x10t
        0x17t
        -0x43t
        0x1bt
        0xet
        -0x6t
        0xat
        0x1t
        -0x6t
        0x12t
        -0x22t
        0x36t
        0xct
        0x4t
        -0x26t
        0x2ft
        0x1t
        -0x3ct
        0x39t
        0xbt
        0x3t
        0x2bt
        0x1t
        0x4t
        -0x51t
        0x46t
        0x1t
        0xet
        0x3t
        -0x49t
        0x4ct
        0x8t
        -0x2t
        0xet
        -0x49t
        0x4t
        0x30t
        0x8t
        -0x5t
        0xct
        -0x4ct
        0xbt
        0x9t
        -0x1t
        0x18t
        -0x13t
        0x9t
        0x12t
        0x10t
        -0x7t
        0x11t
        0x0t
        -0x3t
        -0x2t
        -0x2t
        0xdt
        -0x2t
    .end array-data
.end method

.method public constructor <init>()V
    .registers 1

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(ISI)Ljava/lang/String;
    .registers 9

    rsub-int/lit8 p1, p1, 0x73

    const/4 v4, 0x0

    rsub-int/lit8 p0, p0, 0x35

    new-instance v0, Ljava/lang/String;

    sget-object v5, Lbr/com/itau/security/middlewarechannel/Utilities;->e:[B

    add-int/lit8 p2, p2, 0x4

    new-array v1, p0, [B

    add-int/lit8 p0, p0, -0x1

    if-nez v5, :cond_18

    move v2, p2

    move v3, p0

    :goto_13
    add-int/lit8 p2, p2, 0x1

    add-int/2addr v2, v3

    add-int/lit8 p1, v2, -0x3

    :cond_18
    int-to-byte v2, p1

    aput-byte v2, v1, v4

    if-ne v4, p0, :cond_26

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Ljava/lang/String;-><init>([BI)V

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_26
    add-int/lit8 v4, v4, 0x1

    move v2, p1

    aget-byte v3, v5, p2

    goto :goto_13
.end method

.method public static getDeviceID()Ljava/lang/String;
    .registers 4

    .line 26
    sget-object v0, Lbr/com/itau/security/middlewarechannel/Utilities;->a:Ljava/lang/String;

    if-nez v0, :cond_1a

    .line 27
    new-instance v0, Lbr/com/itau/security/commons/exception/CommonSecurityException;

    sget-object v1, Lbr/com/itau/security/middlewarechannel/Utilities;->e:[B

    const/16 v2, 0x90

    aget-byte v1, v1, v2

    neg-int v1, v1

    int-to-byte v1, v1

    const/16 v2, 0x25

    const/16 v3, 0x93

    invoke-static {v2, v1, v3}, Lbr/com/itau/security/middlewarechannel/Utilities;->a(ISI)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lbr/com/itau/security/commons/exception/CommonSecurityException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 29
    :cond_1a
    sget-object v0, Lbr/com/itau/security/middlewarechannel/Utilities;->a:Ljava/lang/String;

    return-object v0
.end method

.method public static init(Landroid/content/Context;)V
    .registers 5

    .line 33
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lbr/com/itau/security/middlewarechannel/Utilities;->e:[B

    const/16 v2, 0x97

    aget-byte v1, v1, v2

    int-to-byte v1, v1

    sget-object v2, Lbr/com/itau/security/middlewarechannel/Utilities;->e:[B

    const/16 v3, 0x24

    aget-byte v2, v2, v3

    int-to-byte v2, v2

    const/16 v3, 0xae

    invoke-static {v1, v2, v3}, Lbr/com/itau/security/middlewarechannel/Utilities;->a(ISI)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 34
    invoke-static {v0}, Lbr/com/itau/security/commons/binary/ByteUtils;->b64Encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lbr/com/itau/security/middlewarechannel/Utilities;->a:Ljava/lang/String;

    .line 35
    return-void
.end method

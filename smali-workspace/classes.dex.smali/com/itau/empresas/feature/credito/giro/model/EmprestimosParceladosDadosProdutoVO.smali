.class public Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosDadosProdutoVO;
.super Ljava/lang/Object;
.source "EmprestimosParceladosDadosProdutoVO.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private codigoProduto:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "codigo_produto"
    .end annotation
.end field

.field private descricaoProduto:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "descricao_produto"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public setCodigoProduto(Ljava/lang/String;)V
    .registers 2
    .param p1, "codigoProduto"    # Ljava/lang/String;

    .line 21
    iput-object p1, p0, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosDadosProdutoVO;->codigoProduto:Ljava/lang/String;

    .line 22
    return-void
.end method

.method public setDescricaoProduto(Ljava/lang/String;)V
    .registers 2
    .param p1, "descricaoProduto"    # Ljava/lang/String;

    .line 29
    iput-object p1, p0, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosDadosProdutoVO;->descricaoProduto:Ljava/lang/String;

    .line 30
    return-void
.end method

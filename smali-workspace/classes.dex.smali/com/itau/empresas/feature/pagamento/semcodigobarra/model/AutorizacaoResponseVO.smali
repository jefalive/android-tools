.class public Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/AutorizacaoResponseVO;
.super Ljava/lang/Object;
.source "AutorizacaoResponseVO.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field public autorizacoesList:Ljava/util/List;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "autorizacoes"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Lcom/itau/empresas/api/model/AutorizacoesVO;>;"
        }
    .end annotation
.end field

.field private numeroCarrinho:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "numero_carrinho"
    .end annotation
.end field

.field private quantidadePagamentos:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "quantidade_pagamentos"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getAutorizacoesList()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Lcom/itau/empresas/api/model/AutorizacoesVO;>;"
        }
    .end annotation

    .line 40
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/AutorizacaoResponseVO;->autorizacoesList:Ljava/util/List;

    return-object v0
.end method

.method public getNumeroCarrinho()Ljava/lang/String;
    .registers 2

    .line 24
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/AutorizacaoResponseVO;->numeroCarrinho:Ljava/lang/String;

    return-object v0
.end method

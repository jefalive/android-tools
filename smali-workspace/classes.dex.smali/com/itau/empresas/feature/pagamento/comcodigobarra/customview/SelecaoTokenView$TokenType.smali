.class public final enum Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView$TokenType;
.super Ljava/lang/Enum;
.source "SelecaoTokenView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "TokenType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView$TokenType;>;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView$TokenType;

.field public static final enum APLICATIVO:Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView$TokenType;

.field public static final enum CHAVEIRO:Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView$TokenType;

.field public static final enum SMS:Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView$TokenType;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .line 33
    new-instance v0, Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView$TokenType;

    const-string v1, "CHAVEIRO"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView$TokenType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView$TokenType;->CHAVEIRO:Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView$TokenType;

    new-instance v0, Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView$TokenType;

    const-string v1, "SMS"

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView$TokenType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView$TokenType;->SMS:Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView$TokenType;

    new-instance v0, Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView$TokenType;

    const-string v1, "APLICATIVO"

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2}, Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView$TokenType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView$TokenType;->APLICATIVO:Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView$TokenType;

    .line 32
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView$TokenType;

    sget-object v1, Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView$TokenType;->CHAVEIRO:Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView$TokenType;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sget-object v1, Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView$TokenType;->SMS:Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView$TokenType;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    sget-object v1, Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView$TokenType;->APLICATIVO:Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView$TokenType;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    sput-object v0, Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView$TokenType;->$VALUES:[Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView$TokenType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 32
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView$TokenType;
    .registers 2
    .param p0, "name"    # Ljava/lang/String;

    .line 32
    const-class v0, Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView$TokenType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView$TokenType;

    return-object v0
.end method

.method public static values()[Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView$TokenType;
    .registers 1

    .line 32
    sget-object v0, Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView$TokenType;->$VALUES:[Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView$TokenType;

    invoke-virtual {v0}, [Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView$TokenType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView$TokenType;

    return-object v0
.end method

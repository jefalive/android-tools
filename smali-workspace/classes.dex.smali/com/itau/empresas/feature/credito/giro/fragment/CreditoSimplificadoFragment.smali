.class public Lcom/itau/empresas/feature/credito/giro/fragment/CreditoSimplificadoFragment;
.super Lcom/itau/empresas/ui/fragment/BaseFragment;
.source "CreditoSimplificadoFragment.java"


# instance fields
.field controller:Lcom/itau/empresas/feature/credito/giro/controller/EmprestimosParceladosController;

.field itemEmprestimoParcelado:Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView;

.field llActivityEmprestimosParceladosEstadoVazio:Landroid/widget/LinearLayout;

.field mock:I

.field textCreditoSimplificadoEmptyView:Landroid/widget/TextView;

.field viewCards:Landroid/widget/LinearLayout;

.field viewIrPara:Lcom/itau/empresas/ui/view/IrParaView;


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 33
    invoke-direct {p0}, Lcom/itau/empresas/ui/fragment/BaseFragment;-><init>()V

    return-void
.end method

.method private consultaOfertasEspeciais()V
    .registers 6

    .line 67
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/giro/fragment/CreditoSimplificadoFragment;->ocultarMensagemErro()V

    .line 69
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/giro/fragment/CreditoSimplificadoFragment;->mostraDialogoDeProgresso()V

    .line 71
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/fragment/CreditoSimplificadoFragment;->controller:Lcom/itau/empresas/feature/credito/giro/controller/EmprestimosParceladosController;

    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/giro/fragment/CreditoSimplificadoFragment;->getDadosUsuario()Lcom/itau/empresas/api/model/ContaOperadorVO;

    move-result-object v1

    invoke-virtual {v1}, Lcom/itau/empresas/api/model/ContaOperadorVO;->getAgencia()Ljava/lang/String;

    move-result-object v1

    .line 72
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/giro/fragment/CreditoSimplificadoFragment;->getDadosUsuario()Lcom/itau/empresas/api/model/ContaOperadorVO;

    move-result-object v2

    invoke-virtual {v2}, Lcom/itau/empresas/api/model/ContaOperadorVO;->getConta()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x7

    invoke-static {v3, v2}, Lcom/itau/empresas/ui/util/ViewUtils;->formataAdicionandoZeros(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 73
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/giro/fragment/CreditoSimplificadoFragment;->getDadosUsuario()Lcom/itau/empresas/api/model/ContaOperadorVO;

    move-result-object v3

    invoke-virtual {v3}, Lcom/itau/empresas/api/model/ContaOperadorVO;->getDac()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/itau/empresas/feature/credito/giro/fragment/CreditoSimplificadoFragment;->application:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v4}, Lcom/itau/empresas/CustomApplication;->getDadosOperador()Lcom/itau/empresas/api/model/DadosOperadorVO;

    move-result-object v4

    invoke-virtual {v4}, Lcom/itau/empresas/api/model/DadosOperadorVO;->getNumeroDocumento()Ljava/lang/String;

    move-result-object v4

    .line 71
    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/itau/empresas/feature/credito/giro/controller/EmprestimosParceladosController;->consultaOfertas(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 74
    return-void
.end method

.method private getDadosUsuario()Lcom/itau/empresas/api/model/ContaOperadorVO;
    .registers 2

    .line 62
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/fragment/CreditoSimplificadoFragment;->application:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getContaSelecionada()Lcom/itau/empresas/api/model/ContaOperadorVO;

    move-result-object v0

    return-object v0
.end method

.method private retornaOfertaPeloSufixoDoTipoOferta(Ljava/util/List;Ljava/lang/String;)Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosVO;
    .registers 7
    .param p1, "lista"    # Ljava/util/List;
    .param p2, "sufixo"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosVO;>;Ljava/lang/String;)Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosVO;"
        }
    .end annotation

    .line 180
    const/4 v1, 0x0

    .line 182
    .local v1, "ofertaProdutoParcelado":Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosVO;
    if-eqz p1, :cond_2d

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_2d

    .line 183
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_d
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2d

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosVO;

    .line 185
    .local v3, "oferta":Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosVO;
    invoke-virtual {v3}, Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosVO;->getTipoOferta()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2c

    invoke-virtual {v3}, Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosVO;->getTipoOferta()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2c

    .line 186
    move-object v1, v3

    .line 187
    goto :goto_2d

    .line 189
    .end local v3    # "oferta":Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosVO;
    :cond_2c
    goto :goto_d

    .line 192
    :cond_2d
    :goto_2d
    return-object v1
.end method

.method private setupCard(Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosSaidaVO;Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView$TipoOfertaEspecial;)V
    .registers 4
    .param p1, "saidaVO"    # Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosSaidaVO;
    .param p2, "tipoOferta"    # Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView$TipoOfertaEspecial;

    .line 79
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/fragment/CreditoSimplificadoFragment;->itemEmprestimoParcelado:Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView;

    invoke-virtual {v0, p1, p2}, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView;->bind(Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosSaidaVO;Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView$TipoOfertaEspecial;)V

    .line 80
    return-void
.end method

.method private temTransbordo(Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosVO;)Z
    .registers 3
    .param p1, "oferta"    # Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosVO;

    .line 161
    if-nez p1, :cond_4

    .line 162
    const/4 v0, 0x0

    return v0

    .line 165
    :cond_4
    invoke-virtual {p1}, Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosVO;->getIndicadorTransbordo()Z

    move-result v0

    return v0
.end method


# virtual methods
.method aoIniciarElementosDeTela()V
    .registers 1

    .line 58
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/giro/fragment/CreditoSimplificadoFragment;->consultaOfertasEspeciais()V

    .line 59
    return-void
.end method

.method public mostraEstadoVazio(Ljava/lang/String;)V
    .registers 4
    .param p1, "textoEmptyState"    # Ljava/lang/String;

    .line 203
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/fragment/CreditoSimplificadoFragment;->textCreditoSimplificadoEmptyView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 204
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/fragment/CreditoSimplificadoFragment;->viewCards:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 205
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/fragment/CreditoSimplificadoFragment;->llActivityEmprestimosParceladosEstadoVazio:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 207
    return-void
.end method

.method public mostrarCard()V
    .registers 3

    .line 197
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/fragment/CreditoSimplificadoFragment;->llActivityEmprestimosParceladosEstadoVazio:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 198
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/fragment/CreditoSimplificadoFragment;->viewCards:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 199
    return-void
.end method

.method public onEventMainThread(Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;)V
    .registers 3
    .param p1, "e"    # Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;

    .line 216
    invoke-static {p1, p0}, Lcom/itau/empresas/api/ApiHelper;->devoTratarEvento(Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;Lcom/itau/empresas/api/ApiConsumidor;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 217
    return-void

    .line 220
    :cond_7
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/giro/fragment/CreditoSimplificadoFragment;->escondeDialogoDeProgresso()V

    .line 221
    return-void
.end method

.method public onEventMainThread(Lcom/itau/empresas/Evento$EventoEmprestimosParcelados;)V
    .registers 9
    .param p1, "eventoEmprestimosParcelados"    # Lcom/itau/empresas/Evento$EventoEmprestimosParcelados;

    .line 90
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/giro/fragment/CreditoSimplificadoFragment;->escondeDialogoDeProgresso()V

    .line 97
    invoke-virtual {p1}, Lcom/itau/empresas/Evento$EventoEmprestimosParcelados;->getOfertas()Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosSaidaVO;

    move-result-object v0

    .line 98
    invoke-virtual {v0}, Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosSaidaVO;->getOfertaEspecialProdutosParceladosVO()Lcom/itau/empresas/feature/credito/giro/model/OfertaEspecialProdutosParceladosVO;

    move-result-object v5

    .line 100
    .local v5, "ofertaEspecial":Lcom/itau/empresas/feature/credito/giro/model/OfertaEspecialProdutosParceladosVO;
    invoke-virtual {v5}, Lcom/itau/empresas/feature/credito/giro/model/OfertaEspecialProdutosParceladosVO;->getTipoOferta()Ljava/lang/String;

    move-result-object v2

    .line 102
    .local v2, "tipoOferta":Ljava/lang/String;
    if-nez v2, :cond_1c

    .line 103
    const v0, 0x7f07055f

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/credito/giro/fragment/CreditoSimplificadoFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/credito/giro/fragment/CreditoSimplificadoFragment;->mostraEstadoVazio(Ljava/lang/String;)V

    .line 104
    return-void

    .line 107
    :cond_1c
    const-string v0, "giro_cartoes"

    invoke-virtual {v2, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2c

    const-string v0, "000000000"

    .line 108
    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5f

    .line 116
    .line 117
    :cond_2c
    invoke-virtual {p1}, Lcom/itau/empresas/Evento$EventoEmprestimosParcelados;->getOfertas()Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosSaidaVO;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosSaidaVO;->getListaOfertaProdutosParcelados()Ljava/util/List;

    move-result-object v0

    const-string v1, "giro_devedor_solidario"

    .line 116
    invoke-direct {p0, v0, v1}, Lcom/itau/empresas/feature/credito/giro/fragment/CreditoSimplificadoFragment;->retornaOfertaPeloSufixoDoTipoOferta(Ljava/util/List;Ljava/lang/String;)Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosVO;

    move-result-object v6

    .line 120
    .local v6, "oferta":Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosVO;
    if-nez v6, :cond_3e

    const/4 v4, 0x0

    goto :goto_3f

    :cond_3e
    const/4 v4, 0x1

    .line 122
    .local v4, "temGiroAval":Z
    :goto_3f
    invoke-direct {p0, v6}, Lcom/itau/empresas/feature/credito/giro/fragment/CreditoSimplificadoFragment;->temTransbordo(Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosVO;)Z

    move-result v3

    .line 125
    .local v3, "temTransbordo":Z
    if-eqz v4, :cond_54

    if-nez v3, :cond_54

    .line 127
    invoke-virtual {p1}, Lcom/itau/empresas/Evento$EventoEmprestimosParcelados;->getOfertas()Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosSaidaVO;

    move-result-object v0

    sget-object v1, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView$TipoOfertaEspecial;->GIRO_CARTOES_COM_GIRO_AVAL:Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView$TipoOfertaEspecial;

    invoke-direct {p0, v0, v1}, Lcom/itau/empresas/feature/credito/giro/fragment/CreditoSimplificadoFragment;->setupCard(Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosSaidaVO;Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView$TipoOfertaEspecial;)V

    .line 129
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/giro/fragment/CreditoSimplificadoFragment;->mostrarCard()V

    goto :goto_5e

    .line 132
    :cond_54
    const v0, 0x7f07055e

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/credito/giro/fragment/CreditoSimplificadoFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/credito/giro/fragment/CreditoSimplificadoFragment;->mostraEstadoVazio(Ljava/lang/String;)V

    .line 135
    .end local v6    # "oferta":Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosVO;
    :goto_5e
    goto :goto_9d

    .end local v3    # "temTransbordo":Z
    .end local v4    # "temGiroAval":Z
    :cond_5f
    const-string v0, "giro_devedor_solidario"

    invoke-virtual {v2, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_93

    .line 137
    .line 138
    invoke-virtual {p1}, Lcom/itau/empresas/Evento$EventoEmprestimosParcelados;->getOfertas()Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosSaidaVO;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosSaidaVO;->getListaOfertaProdutosParcelados()Ljava/util/List;

    move-result-object v0

    const-string v1, "giro_devedor_solidario"

    .line 137
    invoke-direct {p0, v0, v1}, Lcom/itau/empresas/feature/credito/giro/fragment/CreditoSimplificadoFragment;->retornaOfertaPeloSufixoDoTipoOferta(Ljava/util/List;Ljava/lang/String;)Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosVO;

    move-result-object v6

    .line 141
    .local v6, "oferta":Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosVO;
    invoke-direct {p0, v6}, Lcom/itau/empresas/feature/credito/giro/fragment/CreditoSimplificadoFragment;->temTransbordo(Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosVO;)Z

    move-result v3

    .line 143
    .local v3, "temTransbordo":Z
    if-nez v3, :cond_88

    .line 145
    invoke-virtual {p1}, Lcom/itau/empresas/Evento$EventoEmprestimosParcelados;->getOfertas()Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosSaidaVO;

    move-result-object v0

    sget-object v1, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView$TipoOfertaEspecial;->GIRO_AVAL:Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView$TipoOfertaEspecial;

    invoke-direct {p0, v0, v1}, Lcom/itau/empresas/feature/credito/giro/fragment/CreditoSimplificadoFragment;->setupCard(Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosSaidaVO;Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView$TipoOfertaEspecial;)V

    .line 147
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/giro/fragment/CreditoSimplificadoFragment;->mostrarCard()V

    goto :goto_92

    .line 150
    :cond_88
    const v0, 0x7f07055e

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/credito/giro/fragment/CreditoSimplificadoFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/credito/giro/fragment/CreditoSimplificadoFragment;->mostraEstadoVazio(Ljava/lang/String;)V

    .line 152
    .end local v6    # "oferta":Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosVO;
    :goto_92
    goto :goto_9d

    .line 154
    .end local v3    # "temTransbordo":Z
    :cond_93
    const v0, 0x7f07055f

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/credito/giro/fragment/CreditoSimplificadoFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/credito/giro/fragment/CreditoSimplificadoFragment;->mostraEstadoVazio(Ljava/lang/String;)V

    .line 157
    :goto_9d
    return-void
.end method

.method public opkeysConsumidas()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Ljava/lang/String;>;"
        }
    .end annotation

    .line 212
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

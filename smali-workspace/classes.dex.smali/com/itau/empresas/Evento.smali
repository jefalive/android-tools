.class public final Lcom/itau/empresas/Evento;
.super Ljava/lang/Object;
.source "Evento.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/itau/empresas/Evento$EventoLisConsulta;,
        Lcom/itau/empresas/Evento$EventoSelecionaDataDaPendencia;,
        Lcom/itau/empresas/Evento$EventoLoginComSenha;,
        Lcom/itau/empresas/Evento$EventoErroLoginFingerPrint;,
        Lcom/itau/empresas/Evento$EventoTrocaTipoSimulacao;,
        Lcom/itau/empresas/Evento$EventoBuscaSimulacaoEspecialOuPersonalizada;,
        Lcom/itau/empresas/Evento$EventoGiroEfetivacao;,
        Lcom/itau/empresas/Evento$EventoGiroOutroValorSelecionado;,
        Lcom/itau/empresas/Evento$EventoGiroOutroPlanoSelecionado;,
        Lcom/itau/empresas/Evento$EventoTomadaDecisaoSeguro;,
        Lcom/itau/empresas/Evento$EventoSolicitarVerificacaoPermissaoCompartilhar;,
        Lcom/itau/empresas/Evento$EventoEmprestimosParcelados;,
        Lcom/itau/empresas/Evento$EventoSairPoynt;,
        Lcom/itau/empresas/Evento$PermissaoLigacao;,
        Lcom/itau/empresas/Evento$PermissaoContatos;,
        Lcom/itau/empresas/Evento$PermissaoLocalizacao;,
        Lcom/itau/empresas/Evento$PermissaoCamera;,
        Lcom/itau/empresas/Evento$ValorSelecionado;,
        Lcom/itau/empresas/Evento$EventoLoginFinalizado;,
        Lcom/itau/empresas/Evento$EventoWidgetCobranca;,
        Lcom/itau/empresas/Evento$EventoSair;,
        Lcom/itau/empresas/Evento$EventoDetalhesMultilimiteErro;,
        Lcom/itau/empresas/Evento$EventoPrimeiroAcesso;,
        Lcom/itau/empresas/Evento$EventoTrocaDeSenha;,
        Lcom/itau/empresas/Evento$EventoListaMapas;,
        Lcom/itau/empresas/Evento$EventoBuscaContatosRecarga;,
        Lcom/itau/empresas/Evento$EventoContatoRecargaExcluido;,
        Lcom/itau/empresas/Evento$EventoAtendimento;,
        Lcom/itau/empresas/Evento$EventoApelidoContatoRecargaAlterado;,
        Lcom/itau/empresas/Evento$EventoContatoRecargaInserido;,
        Lcom/itau/empresas/Evento$ReExecutaRetriable;,
        Lcom/itau/empresas/Evento$ErroInesperadoApi;,
        Lcom/itau/empresas/Evento$MostraDialogoDeErro;
    }
.end annotation

.class Lcom/facebook/login/LoginManager$LoginLoggerHolder;
.super Ljava/lang/Object;
.source "LoginManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/facebook/login/LoginManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "LoginLoggerHolder"
.end annotation


# static fields
.field private static volatile logger:Lcom/facebook/login/LoginLogger;


# direct methods
.method static synthetic access$000(Landroid/content/Context;)Lcom/facebook/login/LoginLogger;
    .registers 2
    .param p0, "x0"    # Landroid/content/Context;

    .line 624
    invoke-static {p0}, Lcom/facebook/login/LoginManager$LoginLoggerHolder;->getLogger(Landroid/content/Context;)Lcom/facebook/login/LoginLogger;

    move-result-object v0

    return-object v0
.end method

.method private static declared-synchronized getLogger(Landroid/content/Context;)Lcom/facebook/login/LoginLogger;
    .registers 4
    .param p0, "context"    # Landroid/content/Context;

    const-class v2, Lcom/facebook/login/LoginManager$LoginLoggerHolder;

    monitor-enter v2

    .line 628
    if-eqz p0, :cond_6

    goto :goto_a

    :cond_6
    :try_start_6
    invoke-static {}, Lcom/facebook/FacebookSdk;->getApplicationContext()Landroid/content/Context;

    move-result-object p0

    .line 629
    :goto_a
    if-nez p0, :cond_f

    .line 630
    monitor-exit v2

    const/4 v0, 0x0

    return-object v0

    .line 632
    :cond_f
    sget-object v0, Lcom/facebook/login/LoginManager$LoginLoggerHolder;->logger:Lcom/facebook/login/LoginLogger;

    if-nez v0, :cond_1e

    .line 633
    new-instance v0, Lcom/facebook/login/LoginLogger;

    invoke-static {}, Lcom/facebook/FacebookSdk;->getApplicationId()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/facebook/login/LoginLogger;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    sput-object v0, Lcom/facebook/login/LoginManager$LoginLoggerHolder;->logger:Lcom/facebook/login/LoginLogger;

    .line 635
    :cond_1e
    sget-object v0, Lcom/facebook/login/LoginManager$LoginLoggerHolder;->logger:Lcom/facebook/login/LoginLogger;
    :try_end_20
    .catchall {:try_start_6 .. :try_end_20} :catchall_22

    monitor-exit v2

    return-object v0

    :catchall_22
    move-exception p0

    monitor-exit v2

    throw p0
.end method

.class public final Lcom/google/zxing/a/a;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Cloneable;


# instance fields
.field private a:[I

.field private b:I


# direct methods
.method public constructor <init>()V
    .registers 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/zxing/a/a;->b:I

    const/4 v0, 0x1

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/google/zxing/a/a;->a:[I

    return-void
.end method

.method public constructor <init>(I)V
    .registers 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/zxing/a/a;->b:I

    invoke-static {p1}, Lcom/google/zxing/a/a;->d(I)[I

    move-result-object v0

    iput-object v0, p0, Lcom/google/zxing/a/a;->a:[I

    return-void
.end method

.method constructor <init>([II)V
    .registers 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/zxing/a/a;->a:[I

    iput p2, p0, Lcom/google/zxing/a/a;->b:I

    return-void
.end method

.method private static d(I)[I
    .registers 3

    add-int/lit8 v0, p0, 0x1f

    div-int/lit8 v0, v0, 0x20

    new-array v0, v0, [I

    return-object v0
.end method


# virtual methods
.method public a()I
    .registers 2

    iget v0, p0, Lcom/google/zxing/a/a;->b:I

    return v0
.end method

.method public a(I)Z
    .registers 5

    iget-object v0, p0, Lcom/google/zxing/a/a;->a:[I

    div-int/lit8 v1, p1, 0x20

    aget v0, v0, v1

    and-int/lit8 v1, p1, 0x1f

    const/4 v2, 0x1

    shl-int v1, v2, v1

    and-int/2addr v0, v1

    if-eqz v0, :cond_10

    const/4 v0, 0x1

    goto :goto_11

    :cond_10
    const/4 v0, 0x0

    :goto_11
    return v0
.end method

.method public b()V
    .registers 5

    iget-object v0, p0, Lcom/google/zxing/a/a;->a:[I

    array-length v2, v0

    const/4 v3, 0x0

    :goto_4
    if-ge v3, v2, :cond_e

    iget-object v0, p0, Lcom/google/zxing/a/a;->a:[I

    const/4 v1, 0x0

    aput v1, v0, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_4

    :cond_e
    return-void
.end method

.method public b(I)V
    .registers 7

    iget-object v0, p0, Lcom/google/zxing/a/a;->a:[I

    div-int/lit8 v1, p1, 0x20

    aget v2, v0, v1

    and-int/lit8 v3, p1, 0x1f

    const/4 v4, 0x1

    shl-int v3, v4, v3

    or-int/2addr v2, v3

    aput v2, v0, v1

    return-void
.end method

.method public c(I)I
    .registers 7

    iget v0, p0, Lcom/google/zxing/a/a;->b:I

    if-lt p1, v0, :cond_7

    iget v0, p0, Lcom/google/zxing/a/a;->b:I

    return v0

    :cond_7
    div-int/lit8 v2, p1, 0x20

    iget-object v0, p0, Lcom/google/zxing/a/a;->a:[I

    aget v3, v0, v2

    and-int/lit8 v0, p1, 0x1f

    const/4 v1, 0x1

    shl-int v0, v1, v0

    add-int/lit8 v0, v0, -0x1

    xor-int/lit8 v0, v0, -0x1

    and-int/2addr v3, v0

    :goto_17
    if-nez v3, :cond_28

    add-int/lit8 v2, v2, 0x1

    iget-object v0, p0, Lcom/google/zxing/a/a;->a:[I

    array-length v0, v0

    if-ne v2, v0, :cond_23

    iget v0, p0, Lcom/google/zxing/a/a;->b:I

    return v0

    :cond_23
    iget-object v0, p0, Lcom/google/zxing/a/a;->a:[I

    aget v3, v0, v2

    goto :goto_17

    :cond_28
    mul-int/lit8 v0, v2, 0x20

    invoke-static {v3}, Ljava/lang/Integer;->numberOfTrailingZeros(I)I

    move-result v1

    add-int v4, v0, v1

    iget v0, p0, Lcom/google/zxing/a/a;->b:I

    if-le v4, v0, :cond_37

    iget v0, p0, Lcom/google/zxing/a/a;->b:I

    goto :goto_38

    :cond_37
    move v0, v4

    :goto_38
    return v0
.end method

.method public c()V
    .registers 14

    iget-object v0, p0, Lcom/google/zxing/a/a;->a:[I

    array-length v0, v0

    new-array v5, v0, [I

    iget v0, p0, Lcom/google/zxing/a/a;->b:I

    add-int/lit8 v0, v0, -0x1

    div-int/lit8 v6, v0, 0x20

    add-int/lit8 v7, v6, 0x1

    const/4 v8, 0x0

    :goto_e
    if-ge v8, v7, :cond_6c

    iget-object v0, p0, Lcom/google/zxing/a/a;->a:[I

    aget v0, v0, v8

    int-to-long v9, v0

    const/4 v0, 0x1

    shr-long v0, v9, v0

    const-wide/32 v2, 0x55555555

    and-long/2addr v0, v2

    const-wide/32 v2, 0x55555555

    and-long/2addr v2, v9

    const/4 v4, 0x1

    shl-long/2addr v2, v4

    or-long v9, v0, v2

    const/4 v0, 0x2

    shr-long v0, v9, v0

    const-wide/32 v2, 0x33333333

    and-long/2addr v0, v2

    const-wide/32 v2, 0x33333333

    and-long/2addr v2, v9

    const/4 v4, 0x2

    shl-long/2addr v2, v4

    or-long v9, v0, v2

    const/4 v0, 0x4

    shr-long v0, v9, v0

    const-wide/32 v2, 0xf0f0f0f

    and-long/2addr v0, v2

    const-wide/32 v2, 0xf0f0f0f

    and-long/2addr v2, v9

    const/4 v4, 0x4

    shl-long/2addr v2, v4

    or-long v9, v0, v2

    const/16 v0, 0x8

    shr-long v0, v9, v0

    const-wide/32 v2, 0xff00ff

    and-long/2addr v0, v2

    const-wide/32 v2, 0xff00ff

    and-long/2addr v2, v9

    const/16 v4, 0x8

    shl-long/2addr v2, v4

    or-long v9, v0, v2

    const/16 v0, 0x10

    shr-long v0, v9, v0

    const-wide/32 v2, 0xffff

    and-long/2addr v0, v2

    const-wide/32 v2, 0xffff

    and-long/2addr v2, v9

    const/16 v4, 0x10

    shl-long/2addr v2, v4

    or-long v9, v0, v2

    sub-int v0, v6, v8

    long-to-int v1, v9

    aput v1, v5, v0

    add-int/lit8 v8, v8, 0x1

    goto :goto_e

    :cond_6c
    iget v0, p0, Lcom/google/zxing/a/a;->b:I

    mul-int/lit8 v1, v7, 0x20

    if-eq v0, v1, :cond_a4

    mul-int/lit8 v0, v7, 0x20

    iget v1, p0, Lcom/google/zxing/a/a;->b:I

    sub-int v8, v0, v1

    const/4 v9, 0x1

    const/4 v10, 0x0

    :goto_7a
    rsub-int/lit8 v0, v8, 0x1f

    if-ge v10, v0, :cond_85

    shl-int/lit8 v0, v9, 0x1

    or-int/lit8 v9, v0, 0x1

    add-int/lit8 v10, v10, 0x1

    goto :goto_7a

    :cond_85
    const/4 v0, 0x0

    aget v0, v5, v0

    shr-int/2addr v0, v8

    and-int v10, v0, v9

    const/4 v11, 0x1

    :goto_8c
    if-ge v11, v7, :cond_a0

    aget v12, v5, v11

    rsub-int/lit8 v0, v8, 0x20

    shl-int v0, v12, v0

    or-int/2addr v10, v0

    add-int/lit8 v0, v11, -0x1

    aput v10, v5, v0

    shr-int v0, v12, v8

    and-int v10, v0, v9

    add-int/lit8 v11, v11, 0x1

    goto :goto_8c

    :cond_a0
    add-int/lit8 v0, v7, -0x1

    aput v10, v5, v0

    :cond_a4
    iput-object v5, p0, Lcom/google/zxing/a/a;->a:[I

    return-void
.end method

.method public synthetic clone()Ljava/lang/Object;
    .registers 2

    invoke-virtual {p0}, Lcom/google/zxing/a/a;->d()Lcom/google/zxing/a/a;

    move-result-object v0

    return-object v0
.end method

.method public d()Lcom/google/zxing/a/a;
    .registers 4

    new-instance v0, Lcom/google/zxing/a/a;

    iget-object v1, p0, Lcom/google/zxing/a/a;->a:[I

    invoke-virtual {v1}, [I->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [I

    iget v2, p0, Lcom/google/zxing/a/a;->b:I

    invoke-direct {v0, v1, v2}, Lcom/google/zxing/a/a;-><init>([II)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 5

    instance-of v0, p1, Lcom/google/zxing/a/a;

    if-nez v0, :cond_6

    const/4 v0, 0x0

    return v0

    :cond_6
    move-object v2, p1

    check-cast v2, Lcom/google/zxing/a/a;

    iget v0, p0, Lcom/google/zxing/a/a;->b:I

    iget v1, v2, Lcom/google/zxing/a/a;->b:I

    if-ne v0, v1, :cond_1b

    iget-object v0, p0, Lcom/google/zxing/a/a;->a:[I

    iget-object v1, v2, Lcom/google/zxing/a/a;->a:[I

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([I[I)Z

    move-result v0

    if-eqz v0, :cond_1b

    const/4 v0, 0x1

    goto :goto_1c

    :cond_1b
    const/4 v0, 0x0

    :goto_1c
    return v0
.end method

.method public hashCode()I
    .registers 3

    iget v0, p0, Lcom/google/zxing/a/a;->b:I

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/zxing/a/a;->a:[I

    invoke-static {v1}, Ljava/util/Arrays;->hashCode([I)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .registers 4

    new-instance v1, Ljava/lang/StringBuilder;

    iget v0, p0, Lcom/google/zxing/a/a;->b:I

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    const/4 v2, 0x0

    :goto_8
    iget v0, p0, Lcom/google/zxing/a/a;->b:I

    if-ge v2, v0, :cond_26

    and-int/lit8 v0, v2, 0x7

    if-nez v0, :cond_15

    const/16 v0, 0x20

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :cond_15
    invoke-virtual {p0, v2}, Lcom/google/zxing/a/a;->a(I)Z

    move-result v0

    if-eqz v0, :cond_1e

    const/16 v0, 0x58

    goto :goto_20

    :cond_1e
    const/16 v0, 0x2e

    :goto_20
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    add-int/lit8 v2, v2, 0x1

    goto :goto_8

    :cond_26
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

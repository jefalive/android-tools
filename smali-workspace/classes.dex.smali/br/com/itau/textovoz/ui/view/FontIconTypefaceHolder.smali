.class public Lbr/com/itau/textovoz/ui/view/FontIconTypefaceHolder;
.super Ljava/lang/Object;
.source "FontIconTypefaceHolder.java"


# static fields
.field private static sTypeface:Landroid/graphics/Typeface;


# direct methods
.method public static getTypeface()Landroid/graphics/Typeface;
    .registers 1

    .line 16
    sget-object v0, Lbr/com/itau/textovoz/ui/view/FontIconTypefaceHolder;->sTypeface:Landroid/graphics/Typeface;

    if-nez v0, :cond_a

    .line 17
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 19
    :cond_a
    sget-object v0, Lbr/com/itau/textovoz/ui/view/FontIconTypefaceHolder;->sTypeface:Landroid/graphics/Typeface;

    return-object v0
.end method

.method public static init(Landroid/content/res/AssetManager;Ljava/lang/String;)V
    .registers 3
    .param p0, "assets"    # Landroid/content/res/AssetManager;
    .param p1, "path"    # Ljava/lang/String;

    .line 27
    invoke-static {p0, p1}, Landroid/graphics/Typeface;->createFromAsset(Landroid/content/res/AssetManager;Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v0

    sput-object v0, Lbr/com/itau/textovoz/ui/view/FontIconTypefaceHolder;->sTypeface:Landroid/graphics/Typeface;

    .line 28
    return-void
.end method

.class public final Lcom/google/android/gms/internal/zzss;
.super Ljava/lang/Object;


# static fields
.field public static final zzbut:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/gms/internal/zzss;->zzbut:Ljava/lang/Object;

    return-void
.end method

.method public static equals([I[I)Z
    .registers 3
    .param p0, "field1"    # [I
    .param p1, "field2"    # [I

    if-eqz p0, :cond_5

    array-length v0, p0

    if-nez v0, :cond_e

    :cond_5
    if-eqz p1, :cond_a

    array-length v0, p1

    if-nez v0, :cond_c

    :cond_a
    const/4 v0, 0x1

    goto :goto_d

    :cond_c
    const/4 v0, 0x0

    :goto_d
    return v0

    :cond_e
    invoke-static {p0, p1}, Ljava/util/Arrays;->equals([I[I)Z

    move-result v0

    return v0
.end method

.method public static equals([J[J)Z
    .registers 3
    .param p0, "field1"    # [J
    .param p1, "field2"    # [J

    if-eqz p0, :cond_5

    array-length v0, p0

    if-nez v0, :cond_e

    :cond_5
    if-eqz p1, :cond_a

    array-length v0, p1

    if-nez v0, :cond_c

    :cond_a
    const/4 v0, 0x1

    goto :goto_d

    :cond_c
    const/4 v0, 0x0

    :goto_d
    return v0

    :cond_e
    invoke-static {p0, p1}, Ljava/util/Arrays;->equals([J[J)Z

    move-result v0

    return v0
.end method

.method public static equals([Ljava/lang/Object;[Ljava/lang/Object;)Z
    .registers 10
    .param p0, "field1"    # [Ljava/lang/Object;
    .param p1, "field2"    # [Ljava/lang/Object;

    const/4 v2, 0x0

    if-nez p0, :cond_5

    const/4 v3, 0x0

    goto :goto_6

    :cond_5
    array-length v3, p0

    :goto_6
    const/4 v4, 0x0

    if-nez p1, :cond_b

    const/4 v5, 0x0

    goto :goto_c

    :cond_b
    array-length v5, p1

    :goto_c
    if-ge v2, v3, :cond_15

    aget-object v0, p0, v2

    if-nez v0, :cond_15

    add-int/lit8 v2, v2, 0x1

    goto :goto_c

    :cond_15
    :goto_15
    if-ge v4, v5, :cond_1e

    aget-object v0, p1, v4

    if-nez v0, :cond_1e

    add-int/lit8 v4, v4, 0x1

    goto :goto_15

    :cond_1e
    if-lt v2, v3, :cond_22

    const/4 v6, 0x1

    goto :goto_23

    :cond_22
    const/4 v6, 0x0

    :goto_23
    if-lt v4, v5, :cond_27

    const/4 v7, 0x1

    goto :goto_28

    :cond_27
    const/4 v7, 0x0

    :goto_28
    if-eqz v6, :cond_2e

    if-eqz v7, :cond_2e

    const/4 v0, 0x1

    return v0

    :cond_2e
    if-eq v6, v7, :cond_32

    const/4 v0, 0x0

    return v0

    :cond_32
    aget-object v0, p0, v2

    aget-object v1, p1, v4

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3e

    const/4 v0, 0x0

    return v0

    :cond_3e
    add-int/lit8 v2, v2, 0x1

    add-int/lit8 v4, v4, 0x1

    goto :goto_c
.end method

.method public static hashCode([I)I
    .registers 2
    .param p0, "field"    # [I

    if-eqz p0, :cond_5

    array-length v0, p0

    if-nez v0, :cond_7

    :cond_5
    const/4 v0, 0x0

    goto :goto_b

    :cond_7
    invoke-static {p0}, Ljava/util/Arrays;->hashCode([I)I

    move-result v0

    :goto_b
    return v0
.end method

.method public static hashCode([J)I
    .registers 2
    .param p0, "field"    # [J

    if-eqz p0, :cond_5

    array-length v0, p0

    if-nez v0, :cond_7

    :cond_5
    const/4 v0, 0x0

    goto :goto_b

    :cond_7
    invoke-static {p0}, Ljava/util/Arrays;->hashCode([J)I

    move-result v0

    :goto_b
    return v0
.end method

.method public static hashCode([Ljava/lang/Object;)I
    .registers 7
    .param p0, "field"    # [Ljava/lang/Object;

    const/4 v2, 0x0

    const/4 v3, 0x0

    if-nez p0, :cond_6

    const/4 v4, 0x0

    goto :goto_7

    :cond_6
    array-length v4, p0

    :goto_7
    if-ge v3, v4, :cond_18

    aget-object v5, p0, v3

    if-eqz v5, :cond_15

    mul-int/lit8 v0, v2, 0x1f

    invoke-virtual {v5}, Ljava/lang/Object;->hashCode()I

    move-result v1

    add-int v2, v0, v1

    :cond_15
    add-int/lit8 v3, v3, 0x1

    goto :goto_7

    :cond_18
    return v2
.end method

.method public static zza([[B)I
    .registers 7

    const/4 v2, 0x0

    const/4 v3, 0x0

    if-nez p0, :cond_6

    const/4 v4, 0x0

    goto :goto_7

    :cond_6
    array-length v4, p0

    :goto_7
    if-ge v3, v4, :cond_18

    aget-object v5, p0, v3

    if-eqz v5, :cond_15

    mul-int/lit8 v0, v2, 0x1f

    invoke-static {v5}, Ljava/util/Arrays;->hashCode([B)I

    move-result v1

    add-int v2, v0, v1

    :cond_15
    add-int/lit8 v3, v3, 0x1

    goto :goto_7

    :cond_18
    return v2
.end method

.method public static zza(Lcom/google/android/gms/internal/zzso;Lcom/google/android/gms/internal/zzso;)V
    .registers 3

    iget-object v0, p0, Lcom/google/android/gms/internal/zzso;->zzbuj:Lcom/google/android/gms/internal/zzsq;

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/google/android/gms/internal/zzso;->zzbuj:Lcom/google/android/gms/internal/zzsq;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/zzsq;->zzJq()Lcom/google/android/gms/internal/zzsq;

    move-result-object v0

    iput-object v0, p1, Lcom/google/android/gms/internal/zzso;->zzbuj:Lcom/google/android/gms/internal/zzsq;

    :cond_c
    return-void
.end method

.method public static zza([[B[[B)Z
    .registers 10

    const/4 v2, 0x0

    if-nez p0, :cond_5

    const/4 v3, 0x0

    goto :goto_6

    :cond_5
    array-length v3, p0

    :goto_6
    const/4 v4, 0x0

    if-nez p1, :cond_b

    const/4 v5, 0x0

    goto :goto_c

    :cond_b
    array-length v5, p1

    :goto_c
    if-ge v2, v3, :cond_15

    aget-object v0, p0, v2

    if-nez v0, :cond_15

    add-int/lit8 v2, v2, 0x1

    goto :goto_c

    :cond_15
    :goto_15
    if-ge v4, v5, :cond_1e

    aget-object v0, p1, v4

    if-nez v0, :cond_1e

    add-int/lit8 v4, v4, 0x1

    goto :goto_15

    :cond_1e
    if-lt v2, v3, :cond_22

    const/4 v6, 0x1

    goto :goto_23

    :cond_22
    const/4 v6, 0x0

    :goto_23
    if-lt v4, v5, :cond_27

    const/4 v7, 0x1

    goto :goto_28

    :cond_27
    const/4 v7, 0x0

    :goto_28
    if-eqz v6, :cond_2e

    if-eqz v7, :cond_2e

    const/4 v0, 0x1

    return v0

    :cond_2e
    if-eq v6, v7, :cond_32

    const/4 v0, 0x0

    return v0

    :cond_32
    aget-object v0, p0, v2

    aget-object v1, p1, v4

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-nez v0, :cond_3e

    const/4 v0, 0x0

    return v0

    :cond_3e
    add-int/lit8 v2, v2, 0x1

    add-int/lit8 v4, v4, 0x1

    goto :goto_c
.end method

.class public final Lcom/itau/empresas/LogoutActivityListener_;
.super Lcom/itau/empresas/LogoutActivityListener;
.source "LogoutActivityListener_.java"


# instance fields
.field private context_:Landroid/content/Context;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .registers 2
    .param p1, "context"    # Landroid/content/Context;

    .line 19
    invoke-direct {p0}, Lcom/itau/empresas/LogoutActivityListener;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/itau/empresas/LogoutActivityListener_;->context_:Landroid/content/Context;

    .line 21
    invoke-direct {p0}, Lcom/itau/empresas/LogoutActivityListener_;->init_()V

    .line 22
    return-void
.end method

.method public static getInstance_(Landroid/content/Context;)Lcom/itau/empresas/LogoutActivityListener_;
    .registers 2
    .param p0, "context"    # Landroid/content/Context;

    .line 25
    new-instance v0, Lcom/itau/empresas/LogoutActivityListener_;

    invoke-direct {v0, p0}, Lcom/itau/empresas/LogoutActivityListener_;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method private init_()V
    .registers 2

    .line 29
    invoke-static {}, Lcom/itau/empresas/CustomApplication_;->getInstance()Lcom/itau/empresas/CustomApplication;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/LogoutActivityListener_;->customApplication:Lcom/itau/empresas/CustomApplication;

    .line 30
    iget-object v0, p0, Lcom/itau/empresas/LogoutActivityListener_;->context_:Landroid/content/Context;

    invoke-static {v0}, Lcom/itau/empresas/feature/login/controller/LoginController_;->getInstance_(Landroid/content/Context;)Lcom/itau/empresas/feature/login/controller/LoginController_;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/LogoutActivityListener_;->controller:Lcom/itau/empresas/feature/login/controller/LoginController;

    .line 31
    iget-object v0, p0, Lcom/itau/empresas/LogoutActivityListener_;->context_:Landroid/content/Context;

    invoke-static {v0}, Lcom/itau/empresas/feature/logout/LogoutHelper_;->getInstance_(Landroid/content/Context;)Lcom/itau/empresas/feature/logout/LogoutHelper_;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/LogoutActivityListener_;->logoutHelper:Lcom/itau/empresas/feature/logout/LogoutHelper;

    .line 32
    return-void
.end method


# virtual methods
.method public bridge synthetic onActivityCreated(Landroid/app/Activity;Landroid/os/Bundle;)V
    .registers 3

    .line 14
    invoke-super {p0, p1, p2}, Lcom/itau/empresas/LogoutActivityListener;->onActivityCreated(Landroid/app/Activity;Landroid/os/Bundle;)V

    return-void
.end method

.method public bridge synthetic onActivityDestroyed(Landroid/app/Activity;)V
    .registers 2

    .line 14
    invoke-super {p0, p1}, Lcom/itau/empresas/LogoutActivityListener;->onActivityDestroyed(Landroid/app/Activity;)V

    return-void
.end method

.method public bridge synthetic onActivityPaused(Landroid/app/Activity;)V
    .registers 2

    .line 14
    invoke-super {p0, p1}, Lcom/itau/empresas/LogoutActivityListener;->onActivityPaused(Landroid/app/Activity;)V

    return-void
.end method

.method public bridge synthetic onActivityResumed(Landroid/app/Activity;)V
    .registers 2

    .line 14
    invoke-super {p0, p1}, Lcom/itau/empresas/LogoutActivityListener;->onActivityResumed(Landroid/app/Activity;)V

    return-void
.end method

.method public bridge synthetic onActivitySaveInstanceState(Landroid/app/Activity;Landroid/os/Bundle;)V
    .registers 3

    .line 14
    invoke-super {p0, p1, p2}, Lcom/itau/empresas/LogoutActivityListener;->onActivitySaveInstanceState(Landroid/app/Activity;Landroid/os/Bundle;)V

    return-void
.end method

.method public bridge synthetic onActivityStarted(Landroid/app/Activity;)V
    .registers 2

    .line 14
    invoke-super {p0, p1}, Lcom/itau/empresas/LogoutActivityListener;->onActivityStarted(Landroid/app/Activity;)V

    return-void
.end method

.method public bridge synthetic onActivityStopped(Landroid/app/Activity;)V
    .registers 2

    .line 14
    invoke-super {p0, p1}, Lcom/itau/empresas/LogoutActivityListener;->onActivityStopped(Landroid/app/Activity;)V

    return-void
.end method

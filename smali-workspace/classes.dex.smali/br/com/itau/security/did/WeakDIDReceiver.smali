.class public final Lbr/com/itau/security/did/WeakDIDReceiver;
.super Landroid/content/BroadcastReceiver;
.source "SourceFile"


# static fields
.field public static a:Ljava/lang/String;

.field public static b:Ljava/lang/String;

.field private static c:Ljava/lang/String;

.field private static final d:[B

.field private static e:I


# direct methods
.method static constructor <clinit>()V
    .registers 4

    .line 13
    const/16 v0, 0x59

    new-array v0, v0, [B

    fill-array-data v0, :array_4c

    sput-object v0, Lbr/com/itau/security/did/WeakDIDReceiver;->d:[B

    const/16 v0, 0xba

    sput v0, Lbr/com/itau/security/did/WeakDIDReceiver;->e:I

    sget-object v0, Lbr/com/itau/security/did/WeakDIDReceiver;->d:[B

    const/16 v1, 0xf

    aget-byte v0, v0, v1

    int-to-byte v0, v0

    sget-object v1, Lbr/com/itau/security/did/WeakDIDReceiver;->d:[B

    const/16 v2, 0x54

    aget-byte v1, v1, v2

    neg-int v1, v1

    int-to-byte v1, v1

    sget-object v2, Lbr/com/itau/security/did/WeakDIDReceiver;->d:[B

    const/4 v3, 0x1

    aget-byte v2, v2, v3

    add-int/lit8 v2, v2, -0x1

    int-to-byte v2, v2

    invoke-static {v0, v1, v2}, Lbr/com/itau/security/did/WeakDIDReceiver;->a(SSB)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lbr/com/itau/security/did/WeakDIDReceiver;->a:Ljava/lang/String;

    .line 14
    sget-object v0, Lbr/com/itau/security/did/WeakDIDReceiver;->d:[B

    const/16 v1, 0x1c

    aget-byte v0, v0, v1

    int-to-byte v0, v0

    sget-object v1, Lbr/com/itau/security/did/WeakDIDReceiver;->d:[B

    const/16 v2, 0x54

    aget-byte v1, v1, v2

    neg-int v1, v1

    int-to-byte v1, v1

    add-int/lit8 v2, v1, 0x1

    int-to-byte v2, v2

    invoke-static {v0, v1, v2}, Lbr/com/itau/security/did/WeakDIDReceiver;->a(SSB)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lbr/com/itau/security/did/WeakDIDReceiver;->b:Ljava/lang/String;

    .line 16
    const-class v0, Lbr/com/itau/security/did/WeakDIDReceiver;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lbr/com/itau/security/did/WeakDIDReceiver;->c:Ljava/lang/String;

    return-void

    nop

    :array_4c
    .array-data 1
        0x35t
        0x12t
        -0x59t
        -0x78t
        -0x7t
        -0x22t
        0xdt
        -0x8t
        -0x4t
        -0x2t
        0x7t
        -0x17t
        -0x31t
        -0x20t
        0x23t
        0x0t
        -0xdt
        -0x10t
        -0x4at
        0x51t
        -0x19t
        -0x1t
        -0xbt
        -0x4at
        0x43t
        -0x1t
        -0x3at
        -0x20t
        0xat
        -0x4at
        0x2ft
        0x6t
        -0x8t
        -0x45t
        0x35t
        0x5t
        -0x19t
        0xet
        -0x4dt
        0x3ft
        -0x14t
        -0x8t
        0xct
        -0x9t
        -0xft
        0x5t
        -0x1t
        -0x51t
        0x23t
        -0x19t
        -0x1t
        -0xbt
        0xat
        -0x4at
        0x2ft
        0x6t
        -0x8t
        -0x45t
        0x35t
        0x5t
        -0x19t
        0xet
        -0x4dt
        0x3ft
        -0x14t
        -0x8t
        0xct
        -0x9t
        -0xft
        0x5t
        -0x1t
        -0x51t
        0x30t
        -0x1t
        -0xbt
        -0x3ct
        0x1et
        -0x13t
        0x6t
        -0x2t
        -0x16t
        0x8t
        -0x5t
        0x5t
        -0x1ct
        -0xbt
        -0x19t
        -0x1t
        -0xbt
    .end array-data
.end method

.method public constructor <init>()V
    .registers 1

    .line 11
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method private static a(SSB)Ljava/lang/String;
    .registers 9

    sget-object v4, Lbr/com/itau/security/did/WeakDIDReceiver;->d:[B

    const/4 v5, 0x0

    new-instance v0, Ljava/lang/String;

    rsub-int/lit8 p0, p0, 0x23

    mul-int/lit8 p2, p2, 0x2

    rsub-int/lit8 p2, p2, 0x55

    add-int/lit8 p1, p1, 0x46

    new-array v1, p0, [B

    add-int/lit8 p0, p0, -0x1

    if-nez v4, :cond_18

    move v2, p0

    move v3, p1

    :goto_15
    add-int/2addr v2, v3

    add-int/lit8 p1, v2, 0x6

    :cond_18
    int-to-byte v2, p1

    aput-byte v2, v1, v5

    if-ne v5, p0, :cond_26

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Ljava/lang/String;-><init>([BI)V

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_26
    add-int/lit8 v5, v5, 0x1

    move v2, p1

    add-int/lit8 p2, p2, 0x1

    aget-byte v3, v4, p2

    goto :goto_15
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 8

    .line 21
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object p2

    .line 22
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget v1, Lbr/com/itau/security/did/WeakDIDReceiver;->e:I

    and-int/lit8 v1, v1, 0x5c

    int-to-byte v1, v1

    const/16 v2, 0x29

    const/16 v3, 0x29

    invoke-static {v1, v2, v3}, Lbr/com/itau/security/did/WeakDIDReceiver;->a(SSB)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 24
    new-instance v0, Ldid/f;

    invoke-direct {v0, p1}, Ldid/f;-><init>(Landroid/content/Context;)V

    .line 25
    sget-object v1, Lbr/com/itau/security/did/DID;->a:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ldid/f;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    .line 27
    invoke-virtual {p2}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_31

    .line 28
    return-void

    .line 30
    :cond_31
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lbr/com/itau/security/did/WeakDIDReceiver;->d:[B

    const/16 v2, 0x28

    aget-byte v1, v1, v2

    neg-int v1, v1

    int-to-byte v1, v1

    sget-object v2, Lbr/com/itau/security/did/WeakDIDReceiver;->d:[B

    const/16 v3, 0xf

    aget-byte v2, v2, v3

    int-to-byte v2, v2

    or-int/lit8 v3, v2, 0x24

    int-to-byte v3, v3

    invoke-static {v1, v2, v3}, Lbr/com/itau/security/did/WeakDIDReceiver;->a(SSB)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v1

    iget-object v1, v1, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 31
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lbr/com/itau/security/did/WeakDIDReceiver;->getResultExtras(Z)Landroid/os/Bundle;

    move-result-object v0

    .line 32
    sget-object v1, Lbr/com/itau/security/did/WeakDIDReceiver;->d:[B

    const/16 v2, 0x4c

    aget-byte v1, v1, v2

    add-int/lit8 v1, v1, 0x1

    int-to-byte v1, v1

    sget-object v2, Lbr/com/itau/security/did/WeakDIDReceiver;->d:[B

    const/16 v3, 0xc

    aget-byte v2, v2, v3

    neg-int v2, v2

    int-to-byte v2, v2

    sget-object v3, Lbr/com/itau/security/did/WeakDIDReceiver;->d:[B

    const/16 v4, 0xf

    aget-byte v3, v3, v4

    int-to-byte v3, v3

    invoke-static {v1, v2, v3}, Lbr/com/itau/security/did/WeakDIDReceiver;->a(SSB)Ljava/lang/String;

    move-result-object v1

    invoke-static {p2}, Lbr/com/itau/security/commons/binary/ByteUtils;->from(Ljava/lang/String;)[B

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    .line 33
    invoke-virtual {p0}, Lbr/com/itau/security/did/WeakDIDReceiver;->abortBroadcast()V

    .line 34
    return-void
.end method

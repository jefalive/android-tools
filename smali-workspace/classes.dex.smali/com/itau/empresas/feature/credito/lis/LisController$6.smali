.class Lcom/itau/empresas/feature/credito/lis/LisController$6;
.super Ljava/lang/Object;
.source "LisController.java"

# interfaces
.implements Lbr/com/itau/sdk/android/core/type/BackendCall;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/itau/empresas/feature/credito/lis/LisController;->consultaContratadaComprovante(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/itau/empresas/feature/credito/lis/LisController;

.field final synthetic val$idComprovante:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/itau/empresas/feature/credito/lis/LisController;Ljava/lang/String;)V
    .registers 3
    .param p1, "this$0"    # Lcom/itau/empresas/feature/credito/lis/LisController;

    .line 102
    iput-object p1, p0, Lcom/itau/empresas/feature/credito/lis/LisController$6;->this$0:Lcom/itau/empresas/feature/credito/lis/LisController;

    iput-object p2, p0, Lcom/itau/empresas/feature/credito/lis/LisController$6;->val$idComprovante:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public callBackend()V
    .registers 7

    .line 105
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/LisController$6;->this$0:Lcom/itau/empresas/feature/credito/lis/LisController;

    iget-object v0, v0, Lcom/itau/empresas/feature/credito/lis/LisController;->app:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getContaSelecionada()Lcom/itau/empresas/api/model/ContaOperadorVO;

    move-result-object v5

    .line 106
    .local v5, "contaSelecionada":Lcom/itau/empresas/api/model/ContaOperadorVO;
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/LisController$6;->this$0:Lcom/itau/empresas/feature/credito/lis/LisController;

    # invokes: Lcom/itau/empresas/feature/credito/lis/LisController;->api()Ljava/lang/Object;
    invoke-static {v0}, Lcom/itau/empresas/feature/credito/lis/LisController;->access$1000(Lcom/itau/empresas/feature/credito/lis/LisController;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/api/Api;

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/lis/LisController$6;->val$idComprovante:Ljava/lang/String;

    invoke-virtual {v5}, Lcom/itau/empresas/api/model/ContaOperadorVO;->getAgencia()Ljava/lang/String;

    move-result-object v2

    .line 107
    invoke-virtual {v5}, Lcom/itau/empresas/api/model/ContaOperadorVO;->getConta()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x7

    invoke-static {v4, v3}, Lcom/itau/empresas/ui/util/ViewUtils;->formataAdicionandoZeros(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 108
    invoke-virtual {v5}, Lcom/itau/empresas/api/model/ContaOperadorVO;->getDac()Ljava/lang/String;

    move-result-object v4

    .line 106
    invoke-interface {v0, v1, v2, v3, v4}, Lcom/itau/empresas/api/Api;->buscaLisConsultaComprovante(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/itau/empresas/api/model/ComprovanteServiceVO;

    move-result-object v0

    # invokes: Lcom/itau/empresas/feature/credito/lis/LisController;->post(Ljava/lang/Object;)V
    invoke-static {v0}, Lcom/itau/empresas/feature/credito/lis/LisController;->access$1100(Ljava/lang/Object;)V

    .line 109
    return-void
.end method

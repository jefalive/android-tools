.class Landroid/support/v7/widget/RtlSpacingHelper;
.super Ljava/lang/Object;
.source "RtlSpacingHelper.java"


# instance fields
.field private mEnd:I

.field private mExplicitLeft:I

.field private mExplicitRight:I

.field private mIsRelative:Z

.field private mIsRtl:Z

.field private mLeft:I

.field private mRight:I

.field private mStart:I


# direct methods
.method constructor <init>()V
    .registers 2

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    const/4 v0, 0x0

    iput v0, p0, Landroid/support/v7/widget/RtlSpacingHelper;->mLeft:I

    .line 29
    const/4 v0, 0x0

    iput v0, p0, Landroid/support/v7/widget/RtlSpacingHelper;->mRight:I

    .line 30
    const/high16 v0, -0x80000000

    iput v0, p0, Landroid/support/v7/widget/RtlSpacingHelper;->mStart:I

    .line 31
    const/high16 v0, -0x80000000

    iput v0, p0, Landroid/support/v7/widget/RtlSpacingHelper;->mEnd:I

    .line 32
    const/4 v0, 0x0

    iput v0, p0, Landroid/support/v7/widget/RtlSpacingHelper;->mExplicitLeft:I

    .line 33
    const/4 v0, 0x0

    iput v0, p0, Landroid/support/v7/widget/RtlSpacingHelper;->mExplicitRight:I

    .line 35
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v7/widget/RtlSpacingHelper;->mIsRtl:Z

    .line 36
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v7/widget/RtlSpacingHelper;->mIsRelative:Z

    return-void
.end method


# virtual methods
.method public getEnd()I
    .registers 2

    .line 51
    iget-boolean v0, p0, Landroid/support/v7/widget/RtlSpacingHelper;->mIsRtl:Z

    if-eqz v0, :cond_7

    iget v0, p0, Landroid/support/v7/widget/RtlSpacingHelper;->mLeft:I

    goto :goto_9

    :cond_7
    iget v0, p0, Landroid/support/v7/widget/RtlSpacingHelper;->mRight:I

    :goto_9
    return v0
.end method

.method public getStart()I
    .registers 2

    .line 47
    iget-boolean v0, p0, Landroid/support/v7/widget/RtlSpacingHelper;->mIsRtl:Z

    if-eqz v0, :cond_7

    iget v0, p0, Landroid/support/v7/widget/RtlSpacingHelper;->mRight:I

    goto :goto_9

    :cond_7
    iget v0, p0, Landroid/support/v7/widget/RtlSpacingHelper;->mLeft:I

    :goto_9
    return v0
.end method

.method public setAbsolute(II)V
    .registers 4
    .param p1, "left"    # I
    .param p2, "right"    # I

    .line 68
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v7/widget/RtlSpacingHelper;->mIsRelative:Z

    .line 69
    const/high16 v0, -0x80000000

    if-eq p1, v0, :cond_b

    iput p1, p0, Landroid/support/v7/widget/RtlSpacingHelper;->mExplicitLeft:I

    iput p1, p0, Landroid/support/v7/widget/RtlSpacingHelper;->mLeft:I

    .line 70
    :cond_b
    const/high16 v0, -0x80000000

    if-eq p2, v0, :cond_13

    iput p2, p0, Landroid/support/v7/widget/RtlSpacingHelper;->mExplicitRight:I

    iput p2, p0, Landroid/support/v7/widget/RtlSpacingHelper;->mRight:I

    .line 71
    :cond_13
    return-void
.end method

.method public setDirection(Z)V
    .registers 4
    .param p1, "isRtl"    # Z

    .line 74
    iget-boolean v0, p0, Landroid/support/v7/widget/RtlSpacingHelper;->mIsRtl:Z

    if-ne p1, v0, :cond_5

    .line 75
    return-void

    .line 77
    :cond_5
    iput-boolean p1, p0, Landroid/support/v7/widget/RtlSpacingHelper;->mIsRtl:Z

    .line 78
    iget-boolean v0, p0, Landroid/support/v7/widget/RtlSpacingHelper;->mIsRelative:Z

    if-eqz v0, :cond_43

    .line 79
    if-eqz p1, :cond_28

    .line 80
    iget v0, p0, Landroid/support/v7/widget/RtlSpacingHelper;->mEnd:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_16

    iget v0, p0, Landroid/support/v7/widget/RtlSpacingHelper;->mEnd:I

    goto :goto_18

    :cond_16
    iget v0, p0, Landroid/support/v7/widget/RtlSpacingHelper;->mExplicitLeft:I

    :goto_18
    iput v0, p0, Landroid/support/v7/widget/RtlSpacingHelper;->mLeft:I

    .line 81
    iget v0, p0, Landroid/support/v7/widget/RtlSpacingHelper;->mStart:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_23

    iget v0, p0, Landroid/support/v7/widget/RtlSpacingHelper;->mStart:I

    goto :goto_25

    :cond_23
    iget v0, p0, Landroid/support/v7/widget/RtlSpacingHelper;->mExplicitRight:I

    :goto_25
    iput v0, p0, Landroid/support/v7/widget/RtlSpacingHelper;->mRight:I

    goto :goto_4b

    .line 83
    :cond_28
    iget v0, p0, Landroid/support/v7/widget/RtlSpacingHelper;->mStart:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_31

    iget v0, p0, Landroid/support/v7/widget/RtlSpacingHelper;->mStart:I

    goto :goto_33

    :cond_31
    iget v0, p0, Landroid/support/v7/widget/RtlSpacingHelper;->mExplicitLeft:I

    :goto_33
    iput v0, p0, Landroid/support/v7/widget/RtlSpacingHelper;->mLeft:I

    .line 84
    iget v0, p0, Landroid/support/v7/widget/RtlSpacingHelper;->mEnd:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_3e

    iget v0, p0, Landroid/support/v7/widget/RtlSpacingHelper;->mEnd:I

    goto :goto_40

    :cond_3e
    iget v0, p0, Landroid/support/v7/widget/RtlSpacingHelper;->mExplicitRight:I

    :goto_40
    iput v0, p0, Landroid/support/v7/widget/RtlSpacingHelper;->mRight:I

    goto :goto_4b

    .line 87
    :cond_43
    iget v0, p0, Landroid/support/v7/widget/RtlSpacingHelper;->mExplicitLeft:I

    iput v0, p0, Landroid/support/v7/widget/RtlSpacingHelper;->mLeft:I

    .line 88
    iget v0, p0, Landroid/support/v7/widget/RtlSpacingHelper;->mExplicitRight:I

    iput v0, p0, Landroid/support/v7/widget/RtlSpacingHelper;->mRight:I

    .line 90
    :goto_4b
    return-void
.end method

.method public setRelative(II)V
    .registers 4
    .param p1, "start"    # I
    .param p2, "end"    # I

    .line 55
    iput p1, p0, Landroid/support/v7/widget/RtlSpacingHelper;->mStart:I

    .line 56
    iput p2, p0, Landroid/support/v7/widget/RtlSpacingHelper;->mEnd:I

    .line 57
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v7/widget/RtlSpacingHelper;->mIsRelative:Z

    .line 58
    iget-boolean v0, p0, Landroid/support/v7/widget/RtlSpacingHelper;->mIsRtl:Z

    if-eqz v0, :cond_18

    .line 59
    const/high16 v0, -0x80000000

    if-eq p2, v0, :cond_11

    iput p2, p0, Landroid/support/v7/widget/RtlSpacingHelper;->mLeft:I

    .line 60
    :cond_11
    const/high16 v0, -0x80000000

    if-eq p1, v0, :cond_24

    iput p1, p0, Landroid/support/v7/widget/RtlSpacingHelper;->mRight:I

    goto :goto_24

    .line 62
    :cond_18
    const/high16 v0, -0x80000000

    if-eq p1, v0, :cond_1e

    iput p1, p0, Landroid/support/v7/widget/RtlSpacingHelper;->mLeft:I

    .line 63
    :cond_1e
    const/high16 v0, -0x80000000

    if-eq p2, v0, :cond_24

    iput p2, p0, Landroid/support/v7/widget/RtlSpacingHelper;->mRight:I

    .line 65
    :cond_24
    :goto_24
    return-void
.end method

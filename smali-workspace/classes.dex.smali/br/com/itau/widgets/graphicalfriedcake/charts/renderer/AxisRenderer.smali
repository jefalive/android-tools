.class public abstract Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/AxisRenderer;
.super Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/Renderer;
.source "AxisRenderer.java"


# instance fields
.field protected mAxisLabelPaint:Landroid/graphics/Paint;

.field protected mAxisLinePaint:Landroid/graphics/Paint;

.field protected mGridPaint:Landroid/graphics/Paint;

.field protected mLimitLinePaint:Landroid/graphics/Paint;

.field protected mTrans:Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Transformer;


# direct methods
.method public constructor <init>(Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Transformer;)V
    .registers 5
    .param p1, "viewPortHandler"    # Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;
    .param p2, "trans"    # Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Transformer;

    .line 41
    invoke-direct {p0, p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/Renderer;-><init>(Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;)V

    .line 44
    iput-object p2, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/AxisRenderer;->mTrans:Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Transformer;

    .line 47
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/AxisRenderer;->mAxisLabelPaint:Landroid/graphics/Paint;

    .line 50
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/AxisRenderer;->mGridPaint:Landroid/graphics/Paint;

    .line 51
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/AxisRenderer;->mGridPaint:Landroid/graphics/Paint;

    const v1, -0x777778

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 52
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/AxisRenderer;->mGridPaint:Landroid/graphics/Paint;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 53
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/AxisRenderer;->mGridPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 54
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/AxisRenderer;->mGridPaint:Landroid/graphics/Paint;

    const/16 v1, 0x5a

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 57
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/AxisRenderer;->mAxisLinePaint:Landroid/graphics/Paint;

    .line 58
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/AxisRenderer;->mAxisLinePaint:Landroid/graphics/Paint;

    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 59
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/AxisRenderer;->mAxisLinePaint:Landroid/graphics/Paint;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 60
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/AxisRenderer;->mAxisLinePaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 63
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/AxisRenderer;->mLimitLinePaint:Landroid/graphics/Paint;

    .line 64
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/AxisRenderer;->mLimitLinePaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 65
    return-void
.end method


# virtual methods
.method public getPaintAxisLabels()Landroid/graphics/Paint;
    .registers 2

    .line 74
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/AxisRenderer;->mAxisLabelPaint:Landroid/graphics/Paint;

    return-object v0
.end method

.method public getPaintAxisLine()Landroid/graphics/Paint;
    .registers 2

    .line 96
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/AxisRenderer;->mAxisLinePaint:Landroid/graphics/Paint;

    return-object v0
.end method

.method public getPaintGrid()Landroid/graphics/Paint;
    .registers 2

    .line 85
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/AxisRenderer;->mGridPaint:Landroid/graphics/Paint;

    return-object v0
.end method

.method public getTransformer()Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Transformer;
    .registers 2

    .line 106
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/AxisRenderer;->mTrans:Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Transformer;

    return-object v0
.end method

.method public abstract renderAxisLabels(Landroid/graphics/Canvas;)V
.end method

.method public abstract renderAxisLine(Landroid/graphics/Canvas;)V
.end method

.method public abstract renderGridLines(Landroid/graphics/Canvas;)V
.end method

.method public abstract renderLimitLines(Landroid/graphics/Canvas;)V
.end method

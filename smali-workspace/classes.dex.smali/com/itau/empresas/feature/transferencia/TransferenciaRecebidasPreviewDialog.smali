.class public Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasPreviewDialog;
.super Lcom/itau/empresas/ui/dialog/BaseDialog;
.source "TransferenciaRecebidasPreviewDialog.java"


# instance fields
.field campoTipoTransferencia:Landroid/widget/TextView;

.field campoValorTranferencia:Landroid/widget/TextView;

.field llBotoesTransferenciasRecebidas:Landroid/widget/LinearLayout;

.field textoCpfCnpj:Landroid/widget/TextView;

.field textoData:Landroid/widget/TextView;

.field textoRemetente:Landroid/widget/TextView;

.field transferenciaRecebidasVO:Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasVO;

.field viewDivisaoHeaderTransferenciaRecebida:Landroid/view/View;

.field viewDivisaoTransferenciaRecebida:Landroid/view/View;

.field warning:Lcom/itau/empresas/ui/view/WarningView;


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 26
    invoke-direct {p0}, Lcom/itau/empresas/ui/dialog/BaseDialog;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .registers 6
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 61
    invoke-super {p0, p1}, Lcom/itau/empresas/ui/dialog/BaseDialog;->onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;

    move-result-object v3

    .line 62
    .local v3, "dialog":Landroid/app/Dialog;
    invoke-virtual {v3}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/Window;->requestFeature(I)Z

    .line 63
    invoke-virtual {v3}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v0, v1}, Landroid/view/Window;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 65
    return-object v3
.end method

.method public onStart()V
    .registers 7

    .line 70
    invoke-super {p0}, Lcom/itau/empresas/ui/dialog/BaseDialog;->onStart()V

    .line 71
    invoke-virtual {p0}, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasPreviewDialog;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    if-nez v0, :cond_a

    .line 72
    return-void

    .line 75
    :cond_a
    invoke-virtual {p0}, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasPreviewDialog;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    int-to-double v0, v0

    const-wide v2, 0x3fe999999999999aL    # 0.8

    mul-double/2addr v0, v2

    double-to-int v4, v0

    .line 76
    .local v4, "width":I
    invoke-virtual {p0}, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasPreviewDialog;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    int-to-double v0, v0

    const-wide v2, 0x3fe3333333333333L    # 0.6

    mul-double/2addr v0, v2

    double-to-int v5, v0

    .line 78
    .local v5, "height":I
    invoke-virtual {p0}, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasPreviewDialog;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v4, v5}, Landroid/view/Window;->setLayout(II)V

    .line 79
    return-void
.end method

.method preencheCampos()V
    .registers 6

    .line 84
    iget-object v0, p0, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasPreviewDialog;->transferenciaRecebidasVO:Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasVO;

    if-eqz v0, :cond_b2

    .line 86
    iget-object v0, p0, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasPreviewDialog;->campoTipoTransferencia:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "valor do "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasPreviewDialog;->transferenciaRecebidasVO:Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasVO;

    invoke-virtual {v2}, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasVO;->getTipoPagamento()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 87
    iget-object v0, p0, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasPreviewDialog;->campoValorTranferencia:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasPreviewDialog;->transferenciaRecebidasVO:Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasVO;

    invoke-virtual {v1}, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasVO;->getValorPagamento()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/itau/empresas/ui/util/ValorMonetarioUtils;->formataValorDecimalParaMonetario(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 88
    iget-object v0, p0, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasPreviewDialog;->textoRemetente:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasPreviewDialog;->transferenciaRecebidasVO:Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasVO;

    invoke-virtual {v1}, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasVO;->getRemetente()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 89
    sget-object v0, Lbr/com/concretesolutions/canarinho/formatador/FormatadorCPFCNPJ;->CPF_CNPJ:Lbr/com/concretesolutions/canarinho/formatador/Formatador;

    iget-object v1, p0, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasPreviewDialog;->transferenciaRecebidasVO:Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasVO;

    invoke-virtual {v1}, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasVO;->getCpfCnpj()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lbr/com/concretesolutions/canarinho/formatador/Formatador;->podeSerFormatado(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_60

    .line 90
    iget-object v0, p0, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasPreviewDialog;->textoCpfCnpj:Landroid/widget/TextView;

    sget-object v1, Lbr/com/concretesolutions/canarinho/formatador/FormatadorCPFCNPJ;->CPF_CNPJ:Lbr/com/concretesolutions/canarinho/formatador/Formatador;

    iget-object v2, p0, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasPreviewDialog;->transferenciaRecebidasVO:Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasVO;

    invoke-virtual {v2}, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasVO;->getCpfCnpj()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lbr/com/concretesolutions/canarinho/formatador/Formatador;->formata(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 92
    :cond_60
    iget-object v0, p0, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasPreviewDialog;->textoData:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasPreviewDialog;->transferenciaRecebidasVO:Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasVO;

    invoke-virtual {v1}, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasVO;->getDataRecebimento()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/itau/empresas/ui/util/DataEHoraUtils;->formataDataComBarra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 94
    iget-object v0, p0, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasPreviewDialog;->warning:Lcom/itau/empresas/ui/view/WarningView;

    sget-object v1, Lcom/itau/empresas/ui/view/WarningView$Estado;->SUCESSO:Lcom/itau/empresas/ui/view/WarningView$Estado;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasPreviewDialog;->transferenciaRecebidasVO:Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasVO;

    .line 96
    invoke-virtual {v3}, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasVO;->getDataRecebimento()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/itau/empresas/ui/util/DataEHoraUtils;->formataDataComBarra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    aput-object v3, v2, v4

    .line 95
    const v3, 0x7f07067e

    invoke-virtual {p0, v3, v2}, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasPreviewDialog;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 94
    invoke-virtual {v0, v1, v2}, Lcom/itau/empresas/ui/view/WarningView;->setMensagem(Lcom/itau/empresas/ui/view/WarningView$Estado;Ljava/lang/String;)V

    .line 97
    iget-object v0, p0, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasPreviewDialog;->warning:Lcom/itau/empresas/ui/view/WarningView;

    invoke-virtual {p0}, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasPreviewDialog;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c00cd

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/view/WarningView;->setBackgroundColor(I)V

    .line 99
    iget-object v0, p0, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasPreviewDialog;->viewDivisaoTransferenciaRecebida:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 100
    iget-object v0, p0, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasPreviewDialog;->viewDivisaoHeaderTransferenciaRecebida:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 101
    iget-object v0, p0, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasPreviewDialog;->llBotoesTransferenciasRecebidas:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 103
    :cond_b2
    return-void
.end method

.class final Lcom/itau/empresas/ui/util/MascaraEditText$1;
.super Lcom/itau/empresas/ui/util/SimpleTextWatcher;
.source "MascaraEditText.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/itau/empresas/ui/util/MascaraEditText;->insere(Ljava/lang/String;Landroid/widget/EditText;)Landroid/text/TextWatcher;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field oldCount:I

.field final synthetic val$ediTxt:Landroid/widget/EditText;

.field final synthetic val$mask:Ljava/lang/String;


# direct methods
.method constructor <init>(Landroid/widget/EditText;Ljava/lang/String;)V
    .registers 4

    .line 18
    iput-object p1, p0, Lcom/itau/empresas/ui/util/MascaraEditText$1;->val$ediTxt:Landroid/widget/EditText;

    iput-object p2, p0, Lcom/itau/empresas/ui/util/MascaraEditText$1;->val$mask:Ljava/lang/String;

    invoke-direct {p0}, Lcom/itau/empresas/ui/util/SimpleTextWatcher;-><init>()V

    .line 20
    const/4 v0, 0x0

    iput v0, p0, Lcom/itau/empresas/ui/util/MascaraEditText$1;->oldCount:I

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .registers 13
    .param p1, "s"    # Landroid/text/Editable;

    .line 25
    iget-object v0, p0, Lcom/itau/empresas/ui/util/MascaraEditText$1;->val$ediTxt:Landroid/widget/EditText;

    invoke-virtual {v0, p0}, Landroid/widget/EditText;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 27
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/itau/empresas/ui/util/MascaraEditText;->removeMascara(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 29
    .local v3, "str":Ljava/lang/String;
    iget v0, p0, Lcom/itau/empresas/ui/util/MascaraEditText$1;->oldCount:I

    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v1

    if-le v0, v1, :cond_17

    const/4 v4, 0x1

    goto :goto_18

    :cond_17
    const/4 v4, 0x0

    .line 30
    .local v4, "apagou":Z
    :goto_18
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v0, ""

    invoke-direct {v5, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 32
    .local v5, "resultado":Ljava/lang/StringBuilder;
    const/4 v6, 0x0

    .line 34
    .local v6, "i":I
    iget-object v0, p0, Lcom/itau/empresas/ui/util/MascaraEditText$1;->val$mask:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v7

    array-length v8, v7

    const/4 v9, 0x0

    :goto_28
    if-ge v9, v8, :cond_47

    aget-char v10, v7, v9

    .line 35
    .local v10, "m":C
    const/16 v0, 0x23

    if-eq v10, v0, :cond_34

    .line 36
    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 37
    goto :goto_44

    .line 40
    :cond_34
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v0

    if-lt v6, v0, :cond_3b

    .line 41
    goto :goto_47

    .line 44
    :cond_3b
    invoke-virtual {v3, v6}, Ljava/lang/String;->charAt(I)C

    move-result v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 45
    add-int/lit8 v6, v6, 0x1

    .line 34
    .end local v10    # "m":C
    :goto_44
    add-int/lit8 v9, v9, 0x1

    goto :goto_28

    .line 48
    :cond_47
    :goto_47
    if-eqz v4, :cond_69

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-lez v0, :cond_69

    iget-object v0, p0, Lcom/itau/empresas/ui/util/MascaraEditText$1;->val$mask:Ljava/lang/String;

    .line 49
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    const/16 v1, 0x23

    if-eq v0, v1, :cond_69

    .line 50
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->deleteCharAt(I)Ljava/lang/StringBuilder;

    goto :goto_47

    .line 53
    :cond_69
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    iput v0, p0, Lcom/itau/empresas/ui/util/MascaraEditText$1;->oldCount:I

    .line 54
    iget-object v0, p0, Lcom/itau/empresas/ui/util/MascaraEditText$1;->val$ediTxt:Landroid/widget/EditText;

    invoke-virtual {v0, v5}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 55
    iget-object v0, p0, Lcom/itau/empresas/ui/util/MascaraEditText$1;->val$ediTxt:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/itau/empresas/ui/util/MascaraEditText$1;->val$ediTxt:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setSelection(I)V

    .line 56
    iget-object v0, p0, Lcom/itau/empresas/ui/util/MascaraEditText$1;->val$ediTxt:Landroid/widget/EditText;

    invoke-virtual {v0, p0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 57
    return-void
.end method

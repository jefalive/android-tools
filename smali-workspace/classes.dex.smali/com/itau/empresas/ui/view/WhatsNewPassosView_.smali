.class public final Lcom/itau/empresas/ui/view/WhatsNewPassosView_;
.super Lcom/itau/empresas/ui/view/WhatsNewPassosView;
.source "WhatsNewPassosView_.java"

# interfaces
.implements Lorg/androidannotations/api/view/HasViews;
.implements Lorg/androidannotations/api/view/OnViewChangedListener;


# instance fields
.field private alreadyInflated_:Z

.field private final onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .param p1, "context"    # Landroid/content/Context;

    .line 37
    invoke-direct {p0, p1}, Lcom/itau/empresas/ui/view/WhatsNewPassosView;-><init>(Landroid/content/Context;)V

    .line 33
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/itau/empresas/ui/view/WhatsNewPassosView_;->alreadyInflated_:Z

    .line 34
    new-instance v0, Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-direct {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/ui/view/WhatsNewPassosView_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 38
    invoke-direct {p0}, Lcom/itau/empresas/ui/view/WhatsNewPassosView_;->init_()V

    .line 39
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .line 42
    invoke-direct {p0, p1, p2}, Lcom/itau/empresas/ui/view/WhatsNewPassosView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 33
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/itau/empresas/ui/view/WhatsNewPassosView_;->alreadyInflated_:Z

    .line 34
    new-instance v0, Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-direct {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/ui/view/WhatsNewPassosView_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 43
    invoke-direct {p0}, Lcom/itau/empresas/ui/view/WhatsNewPassosView_;->init_()V

    .line 44
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .line 47
    invoke-direct {p0, p1, p2, p3}, Lcom/itau/empresas/ui/view/WhatsNewPassosView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 33
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/itau/empresas/ui/view/WhatsNewPassosView_;->alreadyInflated_:Z

    .line 34
    new-instance v0, Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-direct {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/ui/view/WhatsNewPassosView_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 48
    invoke-direct {p0}, Lcom/itau/empresas/ui/view/WhatsNewPassosView_;->init_()V

    .line 49
    return-void
.end method

.method private init_()V
    .registers 3

    .line 79
    iget-object v0, p0, Lcom/itau/empresas/ui/view/WhatsNewPassosView_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-static {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    move-result-object v1

    .line 80
    .local v1, "previousNotifier":Lorg/androidannotations/api/view/OnViewChangedNotifier;
    invoke-static {p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->registerOnViewChangedListener(Lorg/androidannotations/api/view/OnViewChangedListener;)V

    .line 81
    invoke-static {v1}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 82
    return-void
.end method


# virtual methods
.method public onFinishInflate()V
    .registers 3

    .line 70
    iget-boolean v0, p0, Lcom/itau/empresas/ui/view/WhatsNewPassosView_;->alreadyInflated_:Z

    if-nez v0, :cond_16

    .line 71
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/itau/empresas/ui/view/WhatsNewPassosView_;->alreadyInflated_:Z

    .line 72
    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/WhatsNewPassosView_;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f03018a

    invoke-static {v0, v1, p0}, Lcom/itau/empresas/ui/view/WhatsNewPassosView_;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 73
    iget-object v0, p0, Lcom/itau/empresas/ui/view/WhatsNewPassosView_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 75
    :cond_16
    invoke-super {p0}, Lcom/itau/empresas/ui/view/WhatsNewPassosView;->onFinishInflate()V

    .line 76
    return-void
.end method

.method public onViewChanged(Lorg/androidannotations/api/view/HasViews;)V
    .registers 4
    .param p1, "hasViews"    # Lorg/androidannotations/api/view/HasViews;

    .line 104
    const v0, 0x7f0e069e

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/cesards/cropimageview/CropImageView;

    iput-object v0, p0, Lcom/itau/empresas/ui/view/WhatsNewPassosView_;->imgPrincipal:Lcom/cesards/cropimageview/CropImageView;

    .line 105
    const v0, 0x7f0e06a1

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/ui/view/WhatsNewPassosView_;->textoConteudoInfo:Landroid/widget/TextView;

    .line 106
    const v0, 0x7f0e06a0

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/ui/view/WhatsNewPassosView_;->textoTituloInfo:Landroid/widget/TextView;

    .line 107
    const v0, 0x7f0e06a2

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/ui/view/PageIndicator;

    iput-object v0, p0, Lcom/itau/empresas/ui/view/WhatsNewPassosView_;->llPagerIndicator:Lcom/itau/empresas/ui/view/PageIndicator;

    .line 108
    const v0, 0x7f0e0489

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/ui/view/WhatsNewPassosView_;->botaoAnterior:Landroid/widget/TextView;

    .line 109
    const v0, 0x7f0e048b

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/ui/view/WhatsNewPassosView_;->botaoProximo:Landroid/widget/TextView;

    .line 110
    iget-object v0, p0, Lcom/itau/empresas/ui/view/WhatsNewPassosView_;->botaoAnterior:Landroid/widget/TextView;

    if-eqz v0, :cond_50

    .line 111
    iget-object v0, p0, Lcom/itau/empresas/ui/view/WhatsNewPassosView_;->botaoAnterior:Landroid/widget/TextView;

    new-instance v1, Lcom/itau/empresas/ui/view/WhatsNewPassosView_$1;

    invoke-direct {v1, p0}, Lcom/itau/empresas/ui/view/WhatsNewPassosView_$1;-><init>(Lcom/itau/empresas/ui/view/WhatsNewPassosView_;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 120
    :cond_50
    iget-object v0, p0, Lcom/itau/empresas/ui/view/WhatsNewPassosView_;->botaoProximo:Landroid/widget/TextView;

    if-eqz v0, :cond_5e

    .line 121
    iget-object v0, p0, Lcom/itau/empresas/ui/view/WhatsNewPassosView_;->botaoProximo:Landroid/widget/TextView;

    new-instance v1, Lcom/itau/empresas/ui/view/WhatsNewPassosView_$2;

    invoke-direct {v1, p0}, Lcom/itau/empresas/ui/view/WhatsNewPassosView_$2;-><init>(Lcom/itau/empresas/ui/view/WhatsNewPassosView_;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 130
    :cond_5e
    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/WhatsNewPassosView_;->inicializarViews()V

    .line 131
    return-void
.end method

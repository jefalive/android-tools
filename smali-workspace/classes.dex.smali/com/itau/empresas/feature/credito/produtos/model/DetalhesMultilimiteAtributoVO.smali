.class public Lcom/itau/empresas/feature/credito/produtos/model/DetalhesMultilimiteAtributoVO;
.super Ljava/lang/Object;
.source "DetalhesMultilimiteAtributoVO.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private literal:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "literal"
    .end annotation
.end field

.field private valor:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "valor"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getLiteral()Ljava/lang/String;
    .registers 2

    .line 15
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/model/DetalhesMultilimiteAtributoVO;->literal:Ljava/lang/String;

    return-object v0
.end method

.method public getValor()Ljava/lang/String;
    .registers 2

    .line 23
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/model/DetalhesMultilimiteAtributoVO;->valor:Ljava/lang/String;

    return-object v0
.end method

.method public setLiteral(Ljava/lang/String;)V
    .registers 2
    .param p1, "literal"    # Ljava/lang/String;

    .line 19
    iput-object p1, p0, Lcom/itau/empresas/feature/credito/produtos/model/DetalhesMultilimiteAtributoVO;->literal:Ljava/lang/String;

    .line 20
    return-void
.end method

.method public setValor(Ljava/lang/String;)V
    .registers 2
    .param p1, "valor"    # Ljava/lang/String;

    .line 27
    iput-object p1, p0, Lcom/itau/empresas/feature/credito/produtos/model/DetalhesMultilimiteAtributoVO;->valor:Ljava/lang/String;

    .line 28
    return-void
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    .line 32
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "DetalhesMultilimiteAtributoVO{literal=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/produtos/model/DetalhesMultilimiteAtributoVO;->literal:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", valor=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/produtos/model/DetalhesMultilimiteAtributoVO;->valor:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

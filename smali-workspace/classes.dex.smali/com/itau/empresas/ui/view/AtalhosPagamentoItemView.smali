.class public Lcom/itau/empresas/ui/view/AtalhosPagamentoItemView;
.super Landroid/widget/LinearLayout;
.source "AtalhosPagamentoItemView.java"


# instance fields
.field private divisorVisivel:Z

.field itemAtalhosDivisor:Landroid/view/View;

.field private nomeAplicativo:Ljava/lang/String;

.field textoItemAtalhosNome:Landroid/widget/TextView;


# virtual methods
.method protected aoClicarAbrirAtalhos()V
    .registers 1

    .line 44
    return-void
.end method

.method public inicializarViews()V
    .registers 3

    .line 38
    iget-object v0, p0, Lcom/itau/empresas/ui/view/AtalhosPagamentoItemView;->textoItemAtalhosNome:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/itau/empresas/ui/view/AtalhosPagamentoItemView;->nomeAplicativo:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 39
    iget-object v0, p0, Lcom/itau/empresas/ui/view/AtalhosPagamentoItemView;->itemAtalhosDivisor:Landroid/view/View;

    iget-boolean v1, p0, Lcom/itau/empresas/ui/view/AtalhosPagamentoItemView;->divisorVisivel:Z

    if-eqz v1, :cond_f

    const/4 v1, 0x0

    goto :goto_11

    :cond_f
    const/16 v1, 0x8

    :goto_11
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 40
    return-void
.end method

.class Lnet/hockeyapp/android/FeedbackActivity$4;
.super Ljava/lang/Object;
.source "FeedbackActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lnet/hockeyapp/android/FeedbackActivity;->loadFeedbackMessages(Lnet/hockeyapp/android/objects/FeedbackResponse;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/hockeyapp/android/FeedbackActivity;

.field final synthetic val$feedbackResponse:Lnet/hockeyapp/android/objects/FeedbackResponse;


# direct methods
.method constructor <init>(Lnet/hockeyapp/android/FeedbackActivity;Lnet/hockeyapp/android/objects/FeedbackResponse;)V
    .registers 3
    .param p1, "this$0"    # Lnet/hockeyapp/android/FeedbackActivity;

    .line 638
    iput-object p1, p0, Lnet/hockeyapp/android/FeedbackActivity$4;->this$0:Lnet/hockeyapp/android/FeedbackActivity;

    iput-object p2, p0, Lnet/hockeyapp/android/FeedbackActivity$4;->val$feedbackResponse:Lnet/hockeyapp/android/objects/FeedbackResponse;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .registers 11

    .line 642
    iget-object v0, p0, Lnet/hockeyapp/android/FeedbackActivity$4;->this$0:Lnet/hockeyapp/android/FeedbackActivity;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lnet/hockeyapp/android/FeedbackActivity;->configureFeedbackView(Z)V

    .line 644
    new-instance v5, Ljava/text/SimpleDateFormat;

    const-string v0, "yyyy-MM-dd\'T\'HH:mm:ss\'Z\'"

    invoke-direct {v5, v0}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 645
    .local v5, "format":Ljava/text/SimpleDateFormat;
    new-instance v6, Ljava/text/SimpleDateFormat;

    const-string v0, "d MMM h:mm a"

    invoke-direct {v6, v0}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 647
    .local v6, "formatNew":Ljava/text/SimpleDateFormat;
    const/4 v7, 0x0

    .line 648
    .local v7, "date":Ljava/util/Date;
    iget-object v0, p0, Lnet/hockeyapp/android/FeedbackActivity$4;->val$feedbackResponse:Lnet/hockeyapp/android/objects/FeedbackResponse;

    if-eqz v0, :cond_e9

    iget-object v0, p0, Lnet/hockeyapp/android/FeedbackActivity$4;->val$feedbackResponse:Lnet/hockeyapp/android/objects/FeedbackResponse;

    invoke-virtual {v0}, Lnet/hockeyapp/android/objects/FeedbackResponse;->getFeedback()Lnet/hockeyapp/android/objects/Feedback;

    move-result-object v0

    if-eqz v0, :cond_e9

    iget-object v0, p0, Lnet/hockeyapp/android/FeedbackActivity$4;->val$feedbackResponse:Lnet/hockeyapp/android/objects/FeedbackResponse;

    .line 649
    invoke-virtual {v0}, Lnet/hockeyapp/android/objects/FeedbackResponse;->getFeedback()Lnet/hockeyapp/android/objects/Feedback;

    move-result-object v0

    invoke-virtual {v0}, Lnet/hockeyapp/android/objects/Feedback;->getMessages()Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_e9

    iget-object v0, p0, Lnet/hockeyapp/android/FeedbackActivity$4;->val$feedbackResponse:Lnet/hockeyapp/android/objects/FeedbackResponse;

    .line 650
    invoke-virtual {v0}, Lnet/hockeyapp/android/objects/FeedbackResponse;->getFeedback()Lnet/hockeyapp/android/objects/Feedback;

    move-result-object v0

    invoke-virtual {v0}, Lnet/hockeyapp/android/objects/Feedback;->getMessages()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_e9

    .line 652
    iget-object v0, p0, Lnet/hockeyapp/android/FeedbackActivity$4;->this$0:Lnet/hockeyapp/android/FeedbackActivity;

    iget-object v1, p0, Lnet/hockeyapp/android/FeedbackActivity$4;->val$feedbackResponse:Lnet/hockeyapp/android/objects/FeedbackResponse;

    invoke-virtual {v1}, Lnet/hockeyapp/android/objects/FeedbackResponse;->getFeedback()Lnet/hockeyapp/android/objects/Feedback;

    move-result-object v1

    invoke-virtual {v1}, Lnet/hockeyapp/android/objects/Feedback;->getMessages()Ljava/util/ArrayList;

    move-result-object v1

    # setter for: Lnet/hockeyapp/android/FeedbackActivity;->feedbackMessages:Ljava/util/ArrayList;
    invoke-static {v0, v1}, Lnet/hockeyapp/android/FeedbackActivity;->access$602(Lnet/hockeyapp/android/FeedbackActivity;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    .line 654
    iget-object v0, p0, Lnet/hockeyapp/android/FeedbackActivity$4;->this$0:Lnet/hockeyapp/android/FeedbackActivity;

    # getter for: Lnet/hockeyapp/android/FeedbackActivity;->feedbackMessages:Ljava/util/ArrayList;
    invoke-static {v0}, Lnet/hockeyapp/android/FeedbackActivity;->access$600(Lnet/hockeyapp/android/FeedbackActivity;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->reverse(Ljava/util/List;)V

    .line 658
    :try_start_55
    iget-object v0, p0, Lnet/hockeyapp/android/FeedbackActivity$4;->this$0:Lnet/hockeyapp/android/FeedbackActivity;

    # getter for: Lnet/hockeyapp/android/FeedbackActivity;->feedbackMessages:Ljava/util/ArrayList;
    invoke-static {v0}, Lnet/hockeyapp/android/FeedbackActivity;->access$600(Lnet/hockeyapp/android/FeedbackActivity;)Ljava/util/ArrayList;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnet/hockeyapp/android/objects/FeedbackMessage;

    invoke-virtual {v0}, Lnet/hockeyapp/android/objects/FeedbackMessage;->getCreatedAt()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v7

    .line 659
    iget-object v0, p0, Lnet/hockeyapp/android/FeedbackActivity$4;->this$0:Lnet/hockeyapp/android/FeedbackActivity;

    # getter for: Lnet/hockeyapp/android/FeedbackActivity;->lastUpdatedTextView:Landroid/widget/TextView;
    invoke-static {v0}, Lnet/hockeyapp/android/FeedbackActivity;->access$700(Lnet/hockeyapp/android/FeedbackActivity;)Landroid/widget/TextView;

    move-result-object v0

    const-string v1, "Last Updated: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v6, v7}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_83
    .catch Ljava/text/ParseException; {:try_start_55 .. :try_end_83} :catch_84

    .line 663
    goto :goto_88

    .line 661
    :catch_84
    move-exception v8

    .line 662
    .local v8, "e1":Ljava/text/ParseException;
    invoke-virtual {v8}, Ljava/text/ParseException;->printStackTrace()V

    .line 665
    .end local v8    # "e1":Ljava/text/ParseException;
    :goto_88
    iget-object v0, p0, Lnet/hockeyapp/android/FeedbackActivity$4;->this$0:Lnet/hockeyapp/android/FeedbackActivity;

    # getter for: Lnet/hockeyapp/android/FeedbackActivity;->messagesAdapter:Lnet/hockeyapp/android/adapters/MessagesAdapter;
    invoke-static {v0}, Lnet/hockeyapp/android/FeedbackActivity;->access$800(Lnet/hockeyapp/android/FeedbackActivity;)Lnet/hockeyapp/android/adapters/MessagesAdapter;

    move-result-object v0

    if-nez v0, :cond_a7

    .line 666
    iget-object v0, p0, Lnet/hockeyapp/android/FeedbackActivity$4;->this$0:Lnet/hockeyapp/android/FeedbackActivity;

    new-instance v1, Lnet/hockeyapp/android/adapters/MessagesAdapter;

    iget-object v2, p0, Lnet/hockeyapp/android/FeedbackActivity$4;->this$0:Lnet/hockeyapp/android/FeedbackActivity;

    # getter for: Lnet/hockeyapp/android/FeedbackActivity;->context:Landroid/content/Context;
    invoke-static {v2}, Lnet/hockeyapp/android/FeedbackActivity;->access$300(Lnet/hockeyapp/android/FeedbackActivity;)Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lnet/hockeyapp/android/FeedbackActivity$4;->this$0:Lnet/hockeyapp/android/FeedbackActivity;

    # getter for: Lnet/hockeyapp/android/FeedbackActivity;->feedbackMessages:Ljava/util/ArrayList;
    invoke-static {v3}, Lnet/hockeyapp/android/FeedbackActivity;->access$600(Lnet/hockeyapp/android/FeedbackActivity;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lnet/hockeyapp/android/adapters/MessagesAdapter;-><init>(Landroid/content/Context;Ljava/util/ArrayList;)V

    # setter for: Lnet/hockeyapp/android/FeedbackActivity;->messagesAdapter:Lnet/hockeyapp/android/adapters/MessagesAdapter;
    invoke-static {v0, v1}, Lnet/hockeyapp/android/FeedbackActivity;->access$802(Lnet/hockeyapp/android/FeedbackActivity;Lnet/hockeyapp/android/adapters/MessagesAdapter;)Lnet/hockeyapp/android/adapters/MessagesAdapter;

    goto :goto_da

    .line 669
    :cond_a7
    iget-object v0, p0, Lnet/hockeyapp/android/FeedbackActivity$4;->this$0:Lnet/hockeyapp/android/FeedbackActivity;

    # getter for: Lnet/hockeyapp/android/FeedbackActivity;->messagesAdapter:Lnet/hockeyapp/android/adapters/MessagesAdapter;
    invoke-static {v0}, Lnet/hockeyapp/android/FeedbackActivity;->access$800(Lnet/hockeyapp/android/FeedbackActivity;)Lnet/hockeyapp/android/adapters/MessagesAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lnet/hockeyapp/android/adapters/MessagesAdapter;->clear()V

    .line 670
    iget-object v0, p0, Lnet/hockeyapp/android/FeedbackActivity$4;->this$0:Lnet/hockeyapp/android/FeedbackActivity;

    # getter for: Lnet/hockeyapp/android/FeedbackActivity;->feedbackMessages:Ljava/util/ArrayList;
    invoke-static {v0}, Lnet/hockeyapp/android/FeedbackActivity;->access$600(Lnet/hockeyapp/android/FeedbackActivity;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_ba
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_d1

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lnet/hockeyapp/android/objects/FeedbackMessage;

    .line 671
    .local v9, "message":Lnet/hockeyapp/android/objects/FeedbackMessage;
    iget-object v0, p0, Lnet/hockeyapp/android/FeedbackActivity$4;->this$0:Lnet/hockeyapp/android/FeedbackActivity;

    # getter for: Lnet/hockeyapp/android/FeedbackActivity;->messagesAdapter:Lnet/hockeyapp/android/adapters/MessagesAdapter;
    invoke-static {v0}, Lnet/hockeyapp/android/FeedbackActivity;->access$800(Lnet/hockeyapp/android/FeedbackActivity;)Lnet/hockeyapp/android/adapters/MessagesAdapter;

    move-result-object v0

    invoke-virtual {v0, v9}, Lnet/hockeyapp/android/adapters/MessagesAdapter;->add(Lnet/hockeyapp/android/objects/FeedbackMessage;)V

    .line 672
    .end local v9    # "message":Lnet/hockeyapp/android/objects/FeedbackMessage;
    goto :goto_ba

    .line 674
    :cond_d1
    iget-object v0, p0, Lnet/hockeyapp/android/FeedbackActivity$4;->this$0:Lnet/hockeyapp/android/FeedbackActivity;

    # getter for: Lnet/hockeyapp/android/FeedbackActivity;->messagesAdapter:Lnet/hockeyapp/android/adapters/MessagesAdapter;
    invoke-static {v0}, Lnet/hockeyapp/android/FeedbackActivity;->access$800(Lnet/hockeyapp/android/FeedbackActivity;)Lnet/hockeyapp/android/adapters/MessagesAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lnet/hockeyapp/android/adapters/MessagesAdapter;->notifyDataSetChanged()V

    .line 677
    :goto_da
    iget-object v0, p0, Lnet/hockeyapp/android/FeedbackActivity$4;->this$0:Lnet/hockeyapp/android/FeedbackActivity;

    # getter for: Lnet/hockeyapp/android/FeedbackActivity;->messagesListView:Landroid/widget/ListView;
    invoke-static {v0}, Lnet/hockeyapp/android/FeedbackActivity;->access$900(Lnet/hockeyapp/android/FeedbackActivity;)Landroid/widget/ListView;

    move-result-object v0

    iget-object v1, p0, Lnet/hockeyapp/android/FeedbackActivity$4;->this$0:Lnet/hockeyapp/android/FeedbackActivity;

    # getter for: Lnet/hockeyapp/android/FeedbackActivity;->messagesAdapter:Lnet/hockeyapp/android/adapters/MessagesAdapter;
    invoke-static {v1}, Lnet/hockeyapp/android/FeedbackActivity;->access$800(Lnet/hockeyapp/android/FeedbackActivity;)Lnet/hockeyapp/android/adapters/MessagesAdapter;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 679
    :cond_e9
    return-void
.end method

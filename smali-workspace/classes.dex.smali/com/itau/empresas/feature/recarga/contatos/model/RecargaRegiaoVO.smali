.class public Lcom/itau/empresas/feature/recarga/contatos/model/RecargaRegiaoVO;
.super Ljava/lang/Object;
.source "RecargaRegiaoVO.java"

# interfaces
.implements Ljava/io/Serializable;
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;Ljava/io/Serializable;Ljava/lang/Comparable<Lcom/itau/empresas/feature/recarga/contatos/model/RecargaRegiaoVO;>;"
    }
.end annotation


# instance fields
.field private regiao:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "regiao"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public compareTo(Lcom/itau/empresas/feature/recarga/contatos/model/RecargaRegiaoVO;)I
    .registers 4
    .param p1, "o"    # Lcom/itau/empresas/feature/recarga/contatos/model/RecargaRegiaoVO;

    .line 47
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/model/RecargaRegiaoVO;->regiao:Ljava/lang/String;

    iget-object v1, p1, Lcom/itau/empresas/feature/recarga/contatos/model/RecargaRegiaoVO;->regiao:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .registers 3

    .line 7
    move-object v0, p1

    check-cast v0, Lcom/itau/empresas/feature/recarga/contatos/model/RecargaRegiaoVO;

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/recarga/contatos/model/RecargaRegiaoVO;->compareTo(Lcom/itau/empresas/feature/recarga/contatos/model/RecargaRegiaoVO;)I

    move-result v0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 5
    .param p1, "o"    # Ljava/lang/Object;

    .line 22
    if-ne p0, p1, :cond_4

    .line 23
    const/4 v0, 0x1

    return v0

    .line 25
    :cond_4
    if-eqz p1, :cond_10

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    if-eq v0, v1, :cond_12

    .line 26
    :cond_10
    const/4 v0, 0x0

    return v0

    .line 29
    :cond_12
    move-object v2, p1

    check-cast v2, Lcom/itau/empresas/feature/recarga/contatos/model/RecargaRegiaoVO;

    .line 31
    .local v2, "regiaoVO":Lcom/itau/empresas/feature/recarga/contatos/model/RecargaRegiaoVO;
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/model/RecargaRegiaoVO;->regiao:Ljava/lang/String;

    if-eqz v0, :cond_24

    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/model/RecargaRegiaoVO;->regiao:Ljava/lang/String;

    iget-object v1, v2, Lcom/itau/empresas/feature/recarga/contatos/model/RecargaRegiaoVO;->regiao:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_28

    goto :goto_2a

    :cond_24
    iget-object v0, v2, Lcom/itau/empresas/feature/recarga/contatos/model/RecargaRegiaoVO;->regiao:Ljava/lang/String;

    if-nez v0, :cond_2a

    :cond_28
    const/4 v0, 0x1

    goto :goto_2b

    :cond_2a
    :goto_2a
    const/4 v0, 0x0

    :goto_2b
    return v0
.end method

.method public getRegiao()Ljava/lang/String;
    .registers 2

    .line 13
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/model/RecargaRegiaoVO;->regiao:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .registers 2

    .line 37
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/model/RecargaRegiaoVO;->regiao:Ljava/lang/String;

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/model/RecargaRegiaoVO;->regiao:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_c

    :cond_b
    const/4 v0, 0x0

    :goto_c
    return v0
.end method

.method public toString()Ljava/lang/String;
    .registers 2

    .line 42
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/model/RecargaRegiaoVO;->regiao:Ljava/lang/String;

    return-object v0
.end method

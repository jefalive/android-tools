.class public Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioMenuAbertoFragment;
.super Lcom/itau/empresas/ui/fragment/BaseFragment;
.source "AreaUsuarioMenuAbertoFragment.java"

# interfaces
.implements Lcom/itau/empresas/adapter/areausuario/AreaUsuarioMenuAbertoAdapter$OnConfiguracoesItemSelected;


# instance fields
.field controller:Lcom/itau/empresas/controller/SessaoController;

.field logoutHelper:Lcom/itau/empresas/feature/logout/LogoutHelper;

.field mAdapter:Lcom/itau/empresas/adapter/areausuario/AreaUsuarioMenuAbertoAdapter;

.field rvConfiguracoes:Landroid/support/v7/widget/RecyclerView;

.field textoCodigoOperador:Landroid/widget/TextView;

.field textoNomeOperador:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 35
    invoke-direct {p0}, Lcom/itau/empresas/ui/fragment/BaseFragment;-><init>()V

    return-void
.end method

.method private exibirOperadorSelecionado(Lcom/itau/empresas/api/model/DadosOperadorVO;)V
    .registers 6
    .param p1, "dadosOperador"    # Lcom/itau/empresas/api/model/DadosOperadorVO;

    .line 74
    if-eqz p1, :cond_3b

    .line 75
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioMenuAbertoFragment;->textoNomeOperador:Landroid/widget/TextView;

    .line 76
    invoke-virtual {p1}, Lcom/itau/empresas/api/model/DadosOperadorVO;->getNomeOperador()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/itau/empresas/ui/util/StringUtils;->converteTodaPrimeiraLetraParaMaiusculo(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 75
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 77
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioMenuAbertoFragment;->textoCodigoOperador:Landroid/widget/TextView;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    .line 78
    invoke-virtual {p1}, Lcom/itau/empresas/api/model/DadosOperadorVO;->getCodigoOperador()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const v2, 0x7f07018b

    invoke-virtual {p0, v2, v1}, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioMenuAbertoFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 77
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 79
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioMenuAbertoFragment;->textoCodigoOperador:Landroid/widget/TextView;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    .line 81
    invoke-virtual {p1}, Lcom/itau/empresas/api/model/DadosOperadorVO;->getCodigoOperador()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    .line 80
    const v2, 0x7f0700a9

    invoke-virtual {p0, v2, v1}, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioMenuAbertoFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 79
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 83
    :cond_3b
    return-void
.end method

.method private getListaDeConfiguracoes()Ljava/util/List;
    .registers 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Lcom/itau/empresas/api/universal/model/ItemConfiguracoes;>;"
        }
    .end annotation

    .line 103
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 105
    .local v4, "configuracoesList":Ljava/util/List;, "Ljava/util/List<Lcom/itau/empresas/api/universal/model/ItemConfiguracoes;>;"
    new-instance v0, Lcom/itau/empresas/api/universal/model/ItemConfiguracoes;

    .line 106
    const v1, 0x7f070264

    invoke-virtual {p0, v1}, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioMenuAbertoFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 107
    invoke-virtual {p0}, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioMenuAbertoFragment;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f07069f

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/itau/empresas/api/universal/model/ItemConfiguracoes;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Class;)V

    .line 105
    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 109
    new-instance v0, Lcom/itau/empresas/api/universal/model/ItemConfiguracoes;

    .line 110
    invoke-virtual {p0}, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioMenuAbertoFragment;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f070271

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 111
    invoke-virtual {p0}, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioMenuAbertoFragment;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f070577

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/itau/empresas/api/universal/model/ItemConfiguracoes;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Class;)V

    .line 109
    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 114
    new-instance v0, Lcom/itau/empresas/api/universal/model/ItemConfiguracoes;

    .line 115
    invoke-virtual {p0}, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioMenuAbertoFragment;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f070274

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 116
    invoke-virtual {p0}, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioMenuAbertoFragment;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f070576

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/itau/empresas/api/universal/model/ItemConfiguracoes;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Class;)V

    .line 114
    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 119
    new-instance v0, Lcom/itau/empresas/api/universal/model/ItemConfiguracoes;

    .line 120
    invoke-virtual {p0}, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioMenuAbertoFragment;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f07027e

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 121
    invoke-virtual {p0}, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioMenuAbertoFragment;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0706bd

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/itau/empresas/api/universal/model/ItemConfiguracoes;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Class;)V

    .line 119
    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 124
    return-object v4
.end method

.method private montarAdapter()V
    .registers 6

    .line 86
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioMenuAbertoFragment;->mAdapter:Lcom/itau/empresas/adapter/areausuario/AreaUsuarioMenuAbertoAdapter;

    invoke-direct {p0}, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioMenuAbertoFragment;->getListaDeConfiguracoes()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioMenuAbertoAdapter;->setListaItensConfiguracoes(Ljava/util/List;)V

    .line 88
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioMenuAbertoFragment;->mAdapter:Lcom/itau/empresas/adapter/areausuario/AreaUsuarioMenuAbertoAdapter;

    invoke-virtual {v0, p0}, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioMenuAbertoAdapter;->setListener(Lcom/itau/empresas/adapter/areausuario/AreaUsuarioMenuAbertoAdapter$OnConfiguracoesItemSelected;)V

    .line 90
    new-instance v4, Landroid/support/v7/widget/LinearLayoutManager;

    invoke-virtual {p0}, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioMenuAbertoFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {v4, v0, v1, v2}, Landroid/support/v7/widget/LinearLayoutManager;-><init>(Landroid/content/Context;IZ)V

    .line 93
    .local v4, "layoutManager":Landroid/support/v7/widget/LinearLayoutManager;
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioMenuAbertoFragment;->rvConfiguracoes:Landroid/support/v7/widget/RecyclerView;

    new-instance v1, Lcom/itau/empresas/ui/util/DividerItemDecoration;

    .line 95
    invoke-virtual {p0}, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioMenuAbertoFragment;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0201a1

    invoke-static {v2, v3}, Landroid/support/v4/content/ContextCompat;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    const/4 v3, 0x1

    invoke-direct {v1, v3, v2}, Lcom/itau/empresas/ui/util/DividerItemDecoration;-><init>(ILandroid/graphics/drawable/Drawable;)V

    .line 93
    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->addItemDecoration(Landroid/support/v7/widget/RecyclerView$ItemDecoration;)V

    .line 97
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioMenuAbertoFragment;->rvConfiguracoes:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, v4}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(Landroid/support/v7/widget/RecyclerView$LayoutManager;)V

    .line 99
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioMenuAbertoFragment;->rvConfiguracoes:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioMenuAbertoFragment;->mAdapter:Lcom/itau/empresas/adapter/areausuario/AreaUsuarioMenuAbertoAdapter;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 100
    return-void
.end method

.method public static novaInstancia(Lcom/itau/empresas/api/model/DadosOperadorVO;)Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioMenuAbertoFragment;
    .registers 4
    .param p0, "dadosOperador"    # Lcom/itau/empresas/api/model/DadosOperadorVO;

    .line 54
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 55
    .local v1, "bundle":Landroid/os/Bundle;
    const-string v0, "operadores"

    invoke-virtual {v1, v0, p0}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 57
    invoke-static {}, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioMenuAbertoFragment_;->builder()Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioMenuAbertoFragment_$FragmentBuilder_;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioMenuAbertoFragment_$FragmentBuilder_;->build()Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioMenuAbertoFragment;

    move-result-object v2

    .line 58
    .local v2, "fragment":Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioMenuAbertoFragment;
    invoke-virtual {v2, v1}, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioMenuAbertoFragment;->setArguments(Landroid/os/Bundle;)V

    .line 60
    return-object v2
.end method


# virtual methods
.method public afterViews()V
    .registers 4

    .line 65
    invoke-virtual {p0}, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioMenuAbertoFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "operadores"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/itau/empresas/api/model/DadosOperadorVO;

    .line 68
    .local v2, "dadosOperador":Lcom/itau/empresas/api/model/DadosOperadorVO;
    invoke-direct {p0, v2}, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioMenuAbertoFragment;->exibirOperadorSelecionado(Lcom/itau/empresas/api/model/DadosOperadorVO;)V

    .line 70
    invoke-direct {p0}, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioMenuAbertoFragment;->montarAdapter()V

    .line 71
    return-void
.end method

.method public onClick(I)V
    .registers 5
    .param p1, "opcaoSelecionada"    # I

    .line 129
    packed-switch p1, :pswitch_data_4a

    goto :goto_3d

    .line 131
    :pswitch_4
    invoke-static {p0}, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioConfiguracoesActivity;->intent(Landroid/support/v4/app/Fragment;)Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioConfiguracoesActivity_$IntentBuilder_;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioConfiguracoesActivity_$IntentBuilder_;->start()Lorg/androidannotations/api/builder/PostActivityStarter;

    .line 132
    goto :goto_48

    .line 134
    :pswitch_c
    invoke-static {}, Lcom/itau/empresas/feature/feedback/FeedbackDialog_;->builder()Lcom/itau/empresas/feature/feedback/FeedbackDialog_$FragmentBuilder_;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/feedback/FeedbackDialog_$FragmentBuilder_;->feedbackEspontaneo(Ljava/lang/Boolean;)Lcom/itau/empresas/feature/feedback/FeedbackDialog_$FragmentBuilder_;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/feature/feedback/FeedbackDialog_$FragmentBuilder_;->build()Lcom/itau/empresas/feature/feedback/FeedbackDialog;

    move-result-object v0

    .line 135
    invoke-virtual {p0}, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioMenuAbertoFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "sobre_app"

    .line 134
    invoke-virtual {v0, v1, v2}, Lcom/itau/empresas/feature/feedback/FeedbackDialog;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 136
    goto :goto_48

    .line 138
    :pswitch_2b
    invoke-virtual {p0}, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioMenuAbertoFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/itau/empresas/ui/activity/GenericaActivity;->intent(Landroid/content/Context;)Lcom/itau/empresas/ui/activity/GenericaActivity_$IntentBuilder_;

    move-result-object v0

    const-class v1, Lcom/itau/empresas/ui/fragment/OutrosFragment_;

    .line 139
    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/activity/GenericaActivity_$IntentBuilder_;->fragmentClass(Ljava/lang/Class;)Lcom/itau/empresas/ui/activity/GenericaActivity_$IntentBuilder_;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/ui/activity/GenericaActivity_$IntentBuilder_;->start()Lorg/androidannotations/api/builder/PostActivityStarter;

    .line 140
    goto :goto_48

    .line 143
    :goto_3d
    :pswitch_3d
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioMenuAbertoFragment;->logoutHelper:Lcom/itau/empresas/feature/logout/LogoutHelper;

    invoke-virtual {p0}, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioMenuAbertoFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    check-cast v1, Landroid/support/v7/app/AppCompatActivity;

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/logout/LogoutHelper;->sairDoApp(Landroid/support/v7/app/AppCompatActivity;)V

    .line 146
    :goto_48
    return-void

    nop

    :pswitch_data_4a
    .packed-switch 0x0
        :pswitch_4
        :pswitch_c
        :pswitch_2b
        :pswitch_3d
    .end packed-switch
.end method

.method public onEventMainThread(Lcom/itau/empresas/Evento$EventoSair;)V
    .registers 5
    .param p1, "e"    # Lcom/itau/empresas/Evento$EventoSair;

    .line 149
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioMenuAbertoFragment;->logoutHelper:Lcom/itau/empresas/feature/logout/LogoutHelper;

    invoke-virtual {p0}, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioMenuAbertoFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/itau/empresas/feature/logout/LogoutHelper;->efetuarLogout(ZLandroid/app/Activity;)V

    .line 150
    return-void
.end method

.method public onEventMainThread(Lcom/itau/empresas/Evento$EventoSairPoynt;)V
    .registers 5
    .param p1, "e"    # Lcom/itau/empresas/Evento$EventoSairPoynt;

    .line 153
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioMenuAbertoFragment;->logoutHelper:Lcom/itau/empresas/feature/logout/LogoutHelper;

    invoke-virtual {p0}, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioMenuAbertoFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/itau/empresas/feature/logout/LogoutHelper;->efetuarLogout(ZLandroid/app/Activity;)V

    .line 154
    return-void
.end method

.method public opkeysConsumidas()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Ljava/lang/String;>;"
        }
    .end annotation

    .line 50
    sget-object v0, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    return-object v0
.end method

.class Lcom/itau/empresas/ui/util/ViewUtils$CustomPasswordTransformationMethod$PassCharSequence;
.super Ljava/lang/Object;
.source "ViewUtils.java"

# interfaces
.implements Ljava/lang/CharSequence;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/itau/empresas/ui/util/ViewUtils$CustomPasswordTransformationMethod;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "PassCharSequence"
.end annotation


# instance fields
.field private final charSequence:Ljava/lang/CharSequence;

.field final synthetic this$0:Lcom/itau/empresas/ui/util/ViewUtils$CustomPasswordTransformationMethod;


# direct methods
.method public constructor <init>(Lcom/itau/empresas/ui/util/ViewUtils$CustomPasswordTransformationMethod;Ljava/lang/CharSequence;)V
    .registers 3
    .param p1, "this$0"    # Lcom/itau/empresas/ui/util/ViewUtils$CustomPasswordTransformationMethod;
    .param p2, "charSequence"    # Ljava/lang/CharSequence;

    .line 446
    iput-object p1, p0, Lcom/itau/empresas/ui/util/ViewUtils$CustomPasswordTransformationMethod$PassCharSequence;->this$0:Lcom/itau/empresas/ui/util/ViewUtils$CustomPasswordTransformationMethod;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 447
    iput-object p2, p0, Lcom/itau/empresas/ui/util/ViewUtils$CustomPasswordTransformationMethod$PassCharSequence;->charSequence:Ljava/lang/CharSequence;

    .line 448
    return-void
.end method


# virtual methods
.method public charAt(I)C
    .registers 3
    .param p1, "index"    # I

    .line 452
    iget-object v0, p0, Lcom/itau/empresas/ui/util/ViewUtils$CustomPasswordTransformationMethod$PassCharSequence;->this$0:Lcom/itau/empresas/ui/util/ViewUtils$CustomPasswordTransformationMethod;

    # getter for: Lcom/itau/empresas/ui/util/ViewUtils$CustomPasswordTransformationMethod;->DOT:C
    invoke-static {v0}, Lcom/itau/empresas/ui/util/ViewUtils$CustomPasswordTransformationMethod;->access$000(Lcom/itau/empresas/ui/util/ViewUtils$CustomPasswordTransformationMethod;)C

    move-result v0

    return v0
.end method

.method public length()I
    .registers 2

    .line 457
    iget-object v0, p0, Lcom/itau/empresas/ui/util/ViewUtils$CustomPasswordTransformationMethod$PassCharSequence;->charSequence:Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    return v0
.end method

.method public subSequence(II)Ljava/lang/CharSequence;
    .registers 6
    .param p1, "start"    # I
    .param p2, "end"    # I

    .line 462
    new-instance v0, Lcom/itau/empresas/ui/util/ViewUtils$CustomPasswordTransformationMethod$PassCharSequence;

    iget-object v1, p0, Lcom/itau/empresas/ui/util/ViewUtils$CustomPasswordTransformationMethod$PassCharSequence;->this$0:Lcom/itau/empresas/ui/util/ViewUtils$CustomPasswordTransformationMethod;

    iget-object v2, p0, Lcom/itau/empresas/ui/util/ViewUtils$CustomPasswordTransformationMethod$PassCharSequence;->charSequence:Ljava/lang/CharSequence;

    invoke-interface {v2, p1, p2}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/itau/empresas/ui/util/ViewUtils$CustomPasswordTransformationMethod$PassCharSequence;-><init>(Lcom/itau/empresas/ui/util/ViewUtils$CustomPasswordTransformationMethod;Ljava/lang/CharSequence;)V

    return-object v0
.end method

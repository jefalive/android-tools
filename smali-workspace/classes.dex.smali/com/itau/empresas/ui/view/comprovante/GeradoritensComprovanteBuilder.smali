.class public final Lcom/itau/empresas/ui/view/comprovante/GeradoritensComprovanteBuilder;
.super Ljava/lang/Object;
.source "GeradoritensComprovanteBuilder.java"


# instance fields
.field private builder:Lcom/itau/empresas/ui/view/comprovante/GeradoritensComprovanteBuilder;

.field private context:Landroid/content/Context;

.field private itensComprovante:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Lcom/itau/empresas/ui/view/comprovante/ItensComprovanteView;>;"
        }
    .end annotation
.end field

.field private targetLayout:Landroid/widget/LinearLayout;


# direct methods
.method private constructor <init>()V
    .registers 1

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    return-void
.end method

.method public static builder(Landroid/content/Context;)Lcom/itau/empresas/ui/view/comprovante/GeradoritensComprovanteBuilder;
    .registers 2
    .param p0, "context"    # Landroid/content/Context;

    .line 19
    new-instance v0, Lcom/itau/empresas/ui/view/comprovante/GeradoritensComprovanteBuilder;

    invoke-direct {v0}, Lcom/itau/empresas/ui/view/comprovante/GeradoritensComprovanteBuilder;-><init>()V

    .line 20
    .local v0, "builder":Lcom/itau/empresas/ui/view/comprovante/GeradoritensComprovanteBuilder;
    iput-object p0, v0, Lcom/itau/empresas/ui/view/comprovante/GeradoritensComprovanteBuilder;->context:Landroid/content/Context;

    .line 21
    iput-object v0, v0, Lcom/itau/empresas/ui/view/comprovante/GeradoritensComprovanteBuilder;->builder:Lcom/itau/empresas/ui/view/comprovante/GeradoritensComprovanteBuilder;

    .line 22
    return-object v0
.end method


# virtual methods
.method public build()V
    .registers 5

    .line 40
    new-instance v0, Lcom/itau/empresas/ui/view/comprovante/GeradorItensComprovanteView;

    iget-object v1, p0, Lcom/itau/empresas/ui/view/comprovante/GeradoritensComprovanteBuilder;->itensComprovante:Ljava/util/List;

    iget-object v2, p0, Lcom/itau/empresas/ui/view/comprovante/GeradoritensComprovanteBuilder;->context:Landroid/content/Context;

    iget-object v3, p0, Lcom/itau/empresas/ui/view/comprovante/GeradoritensComprovanteBuilder;->targetLayout:Landroid/widget/LinearLayout;

    invoke-direct {v0, v1, v2, v3}, Lcom/itau/empresas/ui/view/comprovante/GeradorItensComprovanteView;-><init>(Ljava/util/List;Landroid/content/Context;Landroid/widget/LinearLayout;)V

    .line 41
    invoke-virtual {v0}, Lcom/itau/empresas/ui/view/comprovante/GeradorItensComprovanteView;->init()V

    .line 42
    return-void
.end method

.method public itensComprovanteList(Ljava/util/List;)Lcom/itau/empresas/ui/view/comprovante/GeradoritensComprovanteBuilder;
    .registers 3
    .param p1, "itensComprovante"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<Lcom/itau/empresas/ui/view/comprovante/ComprovanteLabelWrapper;>;)Lcom/itau/empresas/ui/view/comprovante/GeradoritensComprovanteBuilder;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalAccessException;
        }
    .end annotation

    .line 27
    invoke-static {p1}, Lcom/itau/empresas/ui/view/comprovante/ComprovanteLabelProcessor;->parse(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/ui/view/comprovante/GeradoritensComprovanteBuilder;->itensComprovante:Ljava/util/List;

    .line 29
    iget-object v0, p0, Lcom/itau/empresas/ui/view/comprovante/GeradoritensComprovanteBuilder;->builder:Lcom/itau/empresas/ui/view/comprovante/GeradoritensComprovanteBuilder;

    return-object v0
.end method

.method public targetLayout(Landroid/widget/LinearLayout;)Lcom/itau/empresas/ui/view/comprovante/GeradoritensComprovanteBuilder;
    .registers 3
    .param p1, "targetLayout"    # Landroid/widget/LinearLayout;

    .line 34
    iput-object p1, p0, Lcom/itau/empresas/ui/view/comprovante/GeradoritensComprovanteBuilder;->targetLayout:Landroid/widget/LinearLayout;

    .line 36
    iget-object v0, p0, Lcom/itau/empresas/ui/view/comprovante/GeradoritensComprovanteBuilder;->builder:Lcom/itau/empresas/ui/view/comprovante/GeradoritensComprovanteBuilder;

    return-object v0
.end method

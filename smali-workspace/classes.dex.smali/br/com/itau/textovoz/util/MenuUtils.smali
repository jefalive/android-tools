.class public final Lbr/com/itau/textovoz/util/MenuUtils;
.super Ljava/lang/Object;
.source "MenuUtils.java"


# direct methods
.method public static getMenuDrawable(Landroid/content/Context;Lbr/com/itau/textovoz/model/Menu;)I
    .registers 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "menu"    # Lbr/com/itau/textovoz/model/Menu;

    .line 47
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {p1}, Lbr/com/itau/textovoz/model/Menu;->getIcon()Ljava/lang/String;

    move-result-object v1

    const-string v2, "attr"

    .line 48
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    .line 47
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v4

    .line 50
    .local v4, "id":I
    if-eqz v4, :cond_19

    .line 51
    invoke-static {p0, v4}, Lbr/com/itau/textovoz/util/ViewUtils;->getResourceFromAttr(Landroid/content/Context;I)I

    move-result v0

    return v0

    .line 54
    :cond_19
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {p1}, Lbr/com/itau/textovoz/model/Menu;->getIcon()Ljava/lang/String;

    move-result-object v1

    const-string v2, "drawable"

    .line 55
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    .line 54
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    return v0
.end method

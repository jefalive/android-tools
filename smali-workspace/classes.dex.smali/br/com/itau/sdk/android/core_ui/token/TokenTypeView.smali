.class abstract Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;
.super Landroid/widget/RelativeLayout;
.source "SourceFile"


# static fields
.field private static final A:[S

.field private static B:I


# instance fields
.field protected d:Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;

.field protected e:Landroid/widget/Button;

.field protected f:Landroid/widget/Button;

.field protected g:Landroid/widget/EditText;

.field protected h:Landroid/widget/EditText;

.field protected i:Landroid/widget/TextView;

.field protected j:Landroid/view/View;

.field protected l:Landroid/view/View;

.field protected m:Landroid/view/View;

.field protected n:Ljava/util/List;

.field protected o:Landroid/widget/RelativeLayout;

.field protected p:Landroid/view/View;

.field protected q:Landroid/widget/ImageView;

.field protected r:Landroid/widget/TextView;

.field protected s:Z

.field protected t:Landroid/widget/RelativeLayout;

.field private u:Landroid/widget/ImageView;

.field private v:Landroid/widget/TextView;

.field private x:Ljava/lang/String;

.field private y:Z

.field private z:Landroid/graphics/drawable/Drawable;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    const/16 v0, 0x3c

    new-array v0, v0, [S

    fill-array-data v0, :array_e

    sput-object v0, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->A:[S

    const/16 v0, 0x8

    sput v0, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->B:I

    return-void

    :array_e
    .array-data 2
        0x58s
        -0x7cs
        0x6cs
        0x79s
        -0x2s
        0x9s
        -0x4s
        0x8s
        -0x4s
        -0x9s
        0x3s
        0xes
        -0x2s
        -0xes
        0x7s
        0xas
        -0xbs
        0x0s
        0x0s
        0x0s
        0x0s
        0x0s
        -0x2s
        0x0s
        -0x2s
        -0xes
        0x0s
        0xas
        0x7s
        -0x7s
        -0x3s
        0x3s
        -0xbs
        -0x5s
        -0x1bs
        0x4s
        0x6s
        -0x9s
        0x4es
        -0x44s
        -0x1s
        -0x11s
        0x11s
        0x45s
        -0x43s
        -0xcs
        0x1s
        -0x6s
        0xfs
        -0xds
        0x52s
        -0x16s
        0x16s
        -0x4es
        -0x8cs
        0x8ds
        0x8s
        -0xds
        0x3s
        -0x4s
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 4

    .line 72
    sget v0, Lbr/com/itau/sdk/android/core_ui/R$attr;->itausdkcore_tokenTypeStyle:I

    const/4 v1, 0x0

    invoke-direct {p0, p1, v1, v0}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 57
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->s:Z

    .line 73
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->a(Landroid/util/AttributeSet;)V

    .line 74
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4

    .line 77
    sget v0, Lbr/com/itau/sdk/android/core_ui/R$attr;->itausdkcore_tokenTypeStyle:I

    invoke-direct {p0, p1, p2, v0}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 57
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->s:Z

    .line 78
    invoke-virtual {p0, p2}, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->a(Landroid/util/AttributeSet;)V

    .line 79
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 5

    .line 82
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 57
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->s:Z

    .line 83
    invoke-virtual {p0, p2}, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->a(Landroid/util/AttributeSet;)V

    .line 84
    return-void
.end method

.method private static a(III)Ljava/lang/String;
    .registers 9

    rsub-int/lit8 p1, p1, 0x1b

    const/4 v4, 0x0

    rsub-int/lit8 p2, p2, 0x73

    sget-object v5, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->A:[S

    new-instance v0, Ljava/lang/String;

    add-int/lit8 p0, p0, 0x4

    new-array v1, p1, [B

    if-nez v5, :cond_16

    move v2, p1

    move v3, p0

    :goto_11
    add-int/lit8 p0, p0, 0x1

    neg-int v3, v3

    add-int p2, v2, v3

    :cond_16
    move v2, v4

    add-int/lit8 v4, v4, 0x1

    int-to-byte v3, p2

    aput-byte v3, v1, v2

    if-ne v4, p1, :cond_23

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Ljava/lang/String;-><init>([BI)V

    return-object v0

    :cond_23
    move v2, p2

    aget-short v3, v5, p0

    goto :goto_11
.end method

.method private synthetic a(Landroid/view/View;)V
    .registers 2

    .line 239
    invoke-virtual {p0}, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->b()V

    return-void
.end method

.method private synthetic a(Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;Lbr/com/itau/sdk/android/core_ui/token/q;Landroid/view/View;)V
    .registers 8

    .line 99
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->h:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    .line 101
    invoke-interface {v3}, Landroid/text/Editable;->length()I

    move-result v0

    const/4 v1, 0x6

    if-eq v0, v1, :cond_21

    .line 102
    sget v0, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->B:I

    or-int/lit8 v0, v0, 0x16

    sget-object v1, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->A:[S

    const/16 v2, 0x11

    aget-short v1, v1, v2

    or-int/lit8 v2, v1, 0x1f

    invoke-static {v0, v1, v2}, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->a(III)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;->showError(Ljava/lang/String;)V

    .line 103
    return-void

    .line 106
    :cond_21
    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, p2, v0}, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->a(Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;Lbr/com/itau/sdk/android/core_ui/token/q;Ljava/lang/String;)V

    .line 107
    return-void
.end method

.method private a(Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;Lbr/com/itau/sdk/android/core_ui/token/q;Ljava/lang/String;)V
    .registers 8

    .line 313
    invoke-static {}, Lbr/com/itau/sdk/android/core/BackendClient;->getInstance()Lbr/com/itau/sdk/android/core/BackendClient;

    move-result-object v0

    sget-object v1, Lbr/com/itau/sdk/android/core/token/TokenEvent$Category;->TOKEN:Lbr/com/itau/sdk/android/core/token/TokenEvent$Category;

    invoke-virtual {p0}, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->getSendAction()Lbr/com/itau/sdk/android/core/token/TokenEvent$Action;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lbr/com/itau/sdk/android/core/BackendClient;->track(Lbr/com/itau/sdk/android/core/token/TokenEvent$Category;Lbr/com/itau/sdk/android/core/token/TokenEvent$Action;)V

    .line 314
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->g:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    .line 316
    invoke-interface {v3}, Landroid/text/Editable;->length()I

    move-result v0

    const/4 v1, 0x6

    if-eq v0, v1, :cond_2e

    .line 317
    sget v0, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->B:I

    or-int/lit8 v0, v0, 0x16

    sget-object v1, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->A:[S

    const/16 v2, 0x11

    aget-short v1, v1, v2

    or-int/lit8 v2, v1, 0x1f

    invoke-static {v0, v1, v2}, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->a(III)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;->showError(Ljava/lang/String;)V

    .line 318
    return-void

    .line 321
    :cond_2e
    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p2, v0, p3}, Lbr/com/itau/sdk/android/core_ui/token/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 322
    return-void
.end method

.method static synthetic a(Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;Landroid/view/View;)V
    .registers 2

    invoke-direct {p0, p1}, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->a(Landroid/view/View;)V

    return-void
.end method

.method static synthetic a(Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;Lbr/com/itau/sdk/android/core_ui/token/q;Landroid/view/View;)V
    .registers 4

    invoke-direct {p0, p1, p2, p3}, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->b(Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;Lbr/com/itau/sdk/android/core_ui/token/q;Landroid/view/View;)V

    return-void
.end method

.method private b(Landroid/util/AttributeSet;)V
    .registers 8

    .line 287
    if-nez p1, :cond_3

    .line 288
    return-void

    .line 290
    :cond_3
    invoke-virtual {p0}, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 291
    invoke-virtual {v0}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    sget-object v1, Lbr/com/itau/sdk/android/core_ui/R$styleable;->itausdkcore_TokenTypeView:[I

    .line 292
    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, p1, v1, v2, v3}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v4

    .line 298
    :try_start_13
    sget v0, Lbr/com/itau/sdk/android/core_ui/R$styleable;->itausdkcore_TokenTypeView_itausdkcore_tokenLabel:I

    invoke-virtual {v4, v0}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->x:Ljava/lang/String;

    .line 299
    sget v0, Lbr/com/itau/sdk/android/core_ui/R$styleable;->itausdkcore_TokenTypeView_itausdkcore_showSecondaryAction:I

    const/4 v1, 0x0

    invoke-virtual {v4, v0, v1}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    iput-boolean v0, p0, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->y:Z

    .line 301
    sget v0, Lbr/com/itau/sdk/android/core_ui/R$styleable;->itausdkcore_TokenTypeView_itausdkcore_tokenIcon:I

    invoke-virtual {v4, v0}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->z:Landroid/graphics/drawable/Drawable;
    :try_end_2c
    .catchall {:try_start_13 .. :try_end_2c} :catchall_30

    .line 304
    invoke-virtual {v4}, Landroid/content/res/TypedArray;->recycle()V

    .line 305
    goto :goto_35

    .line 304
    :catchall_30
    move-exception v5

    invoke-virtual {v4}, Landroid/content/res/TypedArray;->recycle()V

    throw v5

    .line 306
    :goto_35
    return-void
.end method

.method private synthetic b(Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;Lbr/com/itau/sdk/android/core_ui/token/q;Landroid/view/View;)V
    .registers 5

    .line 95
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->a(Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;Lbr/com/itau/sdk/android/core_ui/token/q;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic b(Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;Lbr/com/itau/sdk/android/core_ui/token/q;Landroid/view/View;)V
    .registers 4

    invoke-direct {p0, p1, p2, p3}, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->a(Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;Lbr/com/itau/sdk/android/core_ui/token/q;Landroid/view/View;)V

    return-void
.end method

.method private getIconResource()I
    .registers 3

    .line 256
    invoke-virtual {p0}, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->getType()I

    move-result v0

    packed-switch v0, :pswitch_data_1a

    goto :goto_17

    .line 258
    :pswitch_8
    invoke-static {}, Lbr/com/itau/sdk/android/core_ui/theme/b;->c()I

    move-result v1

    .line 259
    goto :goto_18

    .line 261
    :pswitch_d
    invoke-static {}, Lbr/com/itau/sdk/android/core_ui/theme/b;->d()I

    move-result v1

    .line 262
    goto :goto_18

    .line 264
    :pswitch_12
    invoke-static {}, Lbr/com/itau/sdk/android/core_ui/theme/b;->b()I

    move-result v1

    .line 265
    goto :goto_18

    .line 267
    :goto_17
    const/4 v1, 0x0

    .line 271
    :goto_18
    return v1

    nop

    :pswitch_data_1a
    .packed-switch 0x1
        :pswitch_12
        :pswitch_8
        :pswitch_d
    .end packed-switch
.end method


# virtual methods
.method a()I
    .registers 2

    .line 161
    sget v0, Lbr/com/itau/sdk/android/core_ui/R$layout;->itausdkcore_view_token_type:I

    return v0
.end method

.method a(Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;Lbr/com/itau/sdk/android/core_ui/token/q;)Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;
    .registers 5

    .line 93
    iput-object p1, p0, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->d:Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;

    .line 94
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->e:Landroid/widget/Button;

    invoke-static {p0, p1, p2}, Lbr/com/itau/sdk/android/core_ui/token/n;->a(Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;Lbr/com/itau/sdk/android/core_ui/token/q;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 97
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->m:Landroid/view/View;

    invoke-static {p0, p1, p2}, Lbr/com/itau/sdk/android/core_ui/token/o;->a(Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;Lbr/com/itau/sdk/android/core_ui/token/q;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 109
    return-object p0
.end method

.method a(Landroid/util/AttributeSet;)V
    .registers 13

    .line 165
    invoke-virtual {p0}, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p0}, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->a()I

    move-result v1

    invoke-static {v0, v1, p0}, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 166
    invoke-direct {p0, p1}, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->b(Landroid/util/AttributeSet;)V

    .line 168
    sget v0, Lbr/com/itau/sdk/android/core_ui/R$id;->token_type_container:I

    invoke-virtual {p0, v0}, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->t:Landroid/widget/RelativeLayout;

    .line 169
    sget v0, Lbr/com/itau/sdk/android/core_ui/R$id;->token_edit_container:I

    invoke-virtual {p0, v0}, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->o:Landroid/widget/RelativeLayout;

    .line 170
    sget v0, Lbr/com/itau/sdk/android/core_ui/R$id;->ic_seta:I

    invoke-virtual {p0, v0}, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->q:Landroid/widget/ImageView;

    .line 171
    sget v0, Lbr/com/itau/sdk/android/core_ui/R$id;->token_icon:I

    invoke-virtual {p0, v0}, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->u:Landroid/widget/ImageView;

    .line 172
    sget v0, Lbr/com/itau/sdk/android/core_ui/R$id;->token_label:I

    invoke-virtual {p0, v0}, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->v:Landroid/widget/TextView;

    .line 173
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->v:Landroid/widget/TextView;

    invoke-static {}, Lbr/com/itau/sdk/android/core_ui/theme/SDKThemeManager;->getInstance()Lbr/com/itau/sdk/android/core_ui/theme/SDKThemeManager;

    move-result-object v1

    invoke-virtual {p0}, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v2

    invoke-static {v1, v2}, Lbr/com/itau/sdk/android/core/util/TypefaceUtils;->getFontRegular(Lbr/com/itau/sdk/android/core/theme/BaseSDKThemeManager;Landroid/content/res/AssetManager;)Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 174
    sget v0, Lbr/com/itau/sdk/android/core_ui/R$id;->token_primary_action:I

    invoke-virtual {p0, v0}, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->e:Landroid/widget/Button;

    .line 175
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->e:Landroid/widget/Button;

    invoke-static {}, Lbr/com/itau/sdk/android/core_ui/theme/SDKThemeManager;->getInstance()Lbr/com/itau/sdk/android/core_ui/theme/SDKThemeManager;

    move-result-object v1

    invoke-virtual {p0}, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v2

    invoke-static {v1, v2}, Lbr/com/itau/sdk/android/core/util/TypefaceUtils;->getFontBold(Lbr/com/itau/sdk/android/core/theme/BaseSDKThemeManager;Landroid/content/res/AssetManager;)Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTypeface(Landroid/graphics/Typeface;)V

    .line 176
    sget v0, Lbr/com/itau/sdk/android/core_ui/R$id;->token_secondary_action:I

    invoke-virtual {p0, v0}, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->f:Landroid/widget/Button;

    .line 177
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->f:Landroid/widget/Button;

    invoke-static {}, Lbr/com/itau/sdk/android/core_ui/theme/SDKThemeManager;->getInstance()Lbr/com/itau/sdk/android/core_ui/theme/SDKThemeManager;

    move-result-object v1

    invoke-virtual {p0}, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v2

    invoke-static {v1, v2}, Lbr/com/itau/sdk/android/core/util/TypefaceUtils;->getFontBold(Lbr/com/itau/sdk/android/core/theme/BaseSDKThemeManager;Landroid/content/res/AssetManager;)Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTypeface(Landroid/graphics/Typeface;)V

    .line 178
    sget v0, Lbr/com/itau/sdk/android/core_ui/R$id;->token_number_edit:I

    invoke-virtual {p0, v0}, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->g:Landroid/widget/EditText;

    .line 179
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->g:Landroid/widget/EditText;

    invoke-static {}, Lbr/com/itau/sdk/android/core_ui/theme/SDKThemeManager;->getInstance()Lbr/com/itau/sdk/android/core_ui/theme/SDKThemeManager;

    move-result-object v1

    invoke-virtual {p0}, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v2

    invoke-static {v1, v2}, Lbr/com/itau/sdk/android/core/util/TypefaceUtils;->getFontRegular(Lbr/com/itau/sdk/android/core/theme/BaseSDKThemeManager;Landroid/content/res/AssetManager;)Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setTypeface(Landroid/graphics/Typeface;)V

    .line 180
    sget v0, Lbr/com/itau/sdk/android/core_ui/R$id;->divisor:I

    invoke-virtual {p0, v0}, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->p:Landroid/view/View;

    .line 181
    invoke-virtual {p0}, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->A:[S

    const/16 v2, 0x2a

    aget-short v1, v1, v2

    add-int/lit8 v1, v1, 0x1

    sget-object v2, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->A:[S

    const/16 v3, 0xb

    aget-short v2, v2, v3

    add-int/lit8 v3, v2, 0x4

    invoke-static {v1, v2, v3}, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->a(III)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Landroid/view/accessibility/AccessibilityManager;

    .line 183
    invoke-virtual {v6}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_e7

    .line 184
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->g:Landroid/widget/EditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setHint(Ljava/lang/CharSequence;)V

    goto :goto_106

    .line 186
    :cond_e7
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->g:Landroid/widget/EditText;

    sget-object v1, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->A:[S

    const/16 v2, 0x31

    aget-short v1, v1, v2

    neg-int v1, v1

    sget-object v2, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->A:[S

    const/16 v3, 0x34

    aget-short v2, v2, v3

    add-int/lit8 v2, v2, -0x1

    sget-object v3, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->A:[S

    const/16 v4, 0x2c

    aget-short v3, v3, v4

    neg-int v3, v3

    invoke-static {v1, v2, v3}, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->a(III)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setHint(Ljava/lang/CharSequence;)V

    .line 190
    :goto_106
    sget v0, Lbr/com/itau/sdk/android/core_ui/R$id;->itausdkcore_message_token_number:I

    invoke-virtual {p0, v0}, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->r:Landroid/widget/TextView;

    .line 191
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->r:Landroid/widget/TextView;

    if-eqz v0, :cond_129

    .line 192
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->r:Landroid/widget/TextView;

    invoke-static {}, Lbr/com/itau/sdk/android/core_ui/theme/SDKThemeManager;->getInstance()Lbr/com/itau/sdk/android/core_ui/theme/SDKThemeManager;

    move-result-object v1

    invoke-virtual {p0}, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v2

    invoke-static {v1, v2}, Lbr/com/itau/sdk/android/core/util/TypefaceUtils;->getFontRegular(Lbr/com/itau/sdk/android/core/theme/BaseSDKThemeManager;Landroid/content/res/AssetManager;)Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 194
    :cond_129
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->n:Ljava/util/List;

    if-nez v0, :cond_134

    .line 195
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->n:Ljava/util/List;

    .line 197
    :cond_134
    sget v0, Lbr/com/itau/sdk/android/core_ui/R$id;->first_token:I

    invoke-virtual {p0, v0}, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->j:Landroid/view/View;

    .line 198
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->j:Landroid/view/View;

    instance-of v0, v0, Landroid/widget/TextView;

    if-eqz v0, :cond_15f

    .line 199
    invoke-static {}, Lbr/com/itau/sdk/android/core_ui/theme/SDKThemeManager;->getInstance()Lbr/com/itau/sdk/android/core_ui/theme/SDKThemeManager;

    move-result-object v0

    iget-object v1, p0, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->j:Landroid/view/View;

    check-cast v1, Landroid/widget/TextView;

    sget-object v2, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->A:[S

    const/16 v3, 0x11

    aget-short v2, v2, v3

    or-int/lit8 v3, v2, 0x13

    sget-object v4, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->A:[S

    const/16 v5, 0xa

    aget-short v4, v4, v5

    invoke-static {v2, v3, v4}, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->a(III)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lbr/com/itau/sdk/android/core/util/TypefaceUtils;->applyBold(Lbr/com/itau/sdk/android/core/theme/BaseSDKThemeManager;Landroid/widget/TextView;Ljava/lang/String;)V

    .line 201
    :cond_15f
    sget v0, Lbr/com/itau/sdk/android/core_ui/R$id;->unlocking_warning:I

    invoke-virtual {p0, v0}, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->i:Landroid/widget/TextView;

    .line 202
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->i:Landroid/widget/TextView;

    invoke-static {}, Lbr/com/itau/sdk/android/core_ui/theme/SDKThemeManager;->getInstance()Lbr/com/itau/sdk/android/core_ui/theme/SDKThemeManager;

    move-result-object v1

    invoke-virtual {p0}, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v2

    invoke-static {v1, v2}, Lbr/com/itau/sdk/android/core/util/TypefaceUtils;->getFontRegular(Lbr/com/itau/sdk/android/core/theme/BaseSDKThemeManager;Landroid/content/res/AssetManager;)Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 203
    sget v0, Lbr/com/itau/sdk/android/core_ui/R$id;->second_token_number_text:I

    invoke-virtual {p0, v0}, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->l:Landroid/view/View;

    .line 204
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->l:Landroid/view/View;

    instance-of v0, v0, Landroid/widget/TextView;

    if-eqz v0, :cond_1a9

    .line 205
    invoke-static {}, Lbr/com/itau/sdk/android/core_ui/theme/SDKThemeManager;->getInstance()Lbr/com/itau/sdk/android/core_ui/theme/SDKThemeManager;

    move-result-object v0

    iget-object v1, p0, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->l:Landroid/view/View;

    check-cast v1, Landroid/widget/TextView;

    sget-object v2, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->A:[S

    const/16 v3, 0xe

    aget-short v2, v2, v3

    sget-object v3, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->A:[S

    const/16 v4, 0x11

    aget-short v3, v3, v4

    const/16 v4, 0x14

    invoke-static {v2, v4, v3}, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->a(III)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lbr/com/itau/sdk/android/core/util/TypefaceUtils;->applyBold(Lbr/com/itau/sdk/android/core/theme/BaseSDKThemeManager;Landroid/widget/TextView;Ljava/lang/String;)V

    .line 207
    :cond_1a9
    sget v0, Lbr/com/itau/sdk/android/core_ui/R$id;->second_token_number_edit:I

    invoke-virtual {p0, v0}, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->h:Landroid/widget/EditText;

    .line 208
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->h:Landroid/widget/EditText;

    invoke-static {}, Lbr/com/itau/sdk/android/core_ui/theme/SDKThemeManager;->getInstance()Lbr/com/itau/sdk/android/core_ui/theme/SDKThemeManager;

    move-result-object v1

    invoke-virtual {p0}, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v2

    invoke-static {v1, v2}, Lbr/com/itau/sdk/android/core/util/TypefaceUtils;->getFontRegular(Lbr/com/itau/sdk/android/core/theme/BaseSDKThemeManager;Landroid/content/res/AssetManager;)Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setTypeface(Landroid/graphics/Typeface;)V

    .line 210
    invoke-virtual {v6}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_1d6

    .line 211
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->h:Landroid/widget/EditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setHint(Ljava/lang/CharSequence;)V

    goto :goto_1f5

    .line 213
    :cond_1d6
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->h:Landroid/widget/EditText;

    sget-object v1, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->A:[S

    const/16 v2, 0x31

    aget-short v1, v1, v2

    neg-int v1, v1

    sget-object v2, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->A:[S

    const/16 v3, 0x34

    aget-short v2, v2, v3

    add-int/lit8 v2, v2, -0x1

    sget-object v3, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->A:[S

    const/16 v4, 0x2c

    aget-short v3, v3, v4

    neg-int v3, v3

    invoke-static {v1, v2, v3}, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->a(III)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setHint(Ljava/lang/CharSequence;)V

    .line 217
    :goto_1f5
    sget v0, Lbr/com/itau/sdk/android/core_ui/R$id;->button_ok_unlock:I

    invoke-virtual {p0, v0}, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->m:Landroid/view/View;

    .line 218
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->m:Landroid/view/View;

    instance-of v0, v0, Landroid/widget/TextView;

    if-eqz v0, :cond_21a

    .line 219
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->m:Landroid/view/View;

    check-cast v0, Landroid/widget/TextView;

    invoke-static {}, Lbr/com/itau/sdk/android/core_ui/theme/SDKThemeManager;->getInstance()Lbr/com/itau/sdk/android/core_ui/theme/SDKThemeManager;

    move-result-object v1

    invoke-virtual {p0}, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v2

    invoke-static {v1, v2}, Lbr/com/itau/sdk/android/core/util/TypefaceUtils;->getFontBold(Lbr/com/itau/sdk/android/core/theme/BaseSDKThemeManager;Landroid/content/res/AssetManager;)Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 222
    :cond_21a
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->n:Ljava/util/List;

    iget-object v1, p0, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->j:Landroid/view/View;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 223
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->n:Ljava/util/List;

    iget-object v1, p0, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->i:Landroid/widget/TextView;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 224
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->n:Ljava/util/List;

    iget-object v1, p0, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->l:Landroid/view/View;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 225
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->n:Ljava/util/List;

    iget-object v1, p0, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->h:Landroid/widget/EditText;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 226
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->n:Ljava/util/List;

    iget-object v1, p0, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->m:Landroid/view/View;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 228
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->g:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Landroid/widget/RelativeLayout$LayoutParams;

    .line 230
    iget-boolean v0, p0, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->y:Z

    if-nez v0, :cond_25c

    .line 231
    sget v0, Lbr/com/itau/sdk/android/core_ui/R$id;->token_primary_action:I

    const/4 v1, 0x5

    invoke-virtual {v7, v1, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 232
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->g:Landroid/widget/EditText;

    invoke-virtual {v0, v7}, Landroid/widget/EditText;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 233
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->f:Landroid/widget/Button;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 236
    :cond_25c
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->v:Landroid/widget/TextView;

    iget-object v1, p0, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->x:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 237
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->u:Landroid/widget/ImageView;

    iget-object v1, p0, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->z:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 239
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->t:Landroid/widget/RelativeLayout;

    invoke-static {p0}, Lbr/com/itau/sdk/android/core_ui/token/p;->a(Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 241
    invoke-static {}, Lbr/com/itau/sdk/android/core_ui/theme/b;->a()I

    move-result v8

    .line 242
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->e:Landroid/widget/Button;

    invoke-virtual {v0, v8}, Landroid/widget/Button;->setBackgroundColor(I)V

    .line 243
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->m:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Landroid/graphics/drawable/GradientDrawable;

    .line 244
    invoke-virtual {v9, v8}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    .line 246
    invoke-direct {p0}, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->getIconResource()I

    move-result v10

    .line 248
    if-eqz v10, :cond_297

    .line 249
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->u:Landroid/widget/ImageView;

    invoke-virtual {v0, v10}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 251
    :cond_297
    return-void
.end method

.method a(Lbr/com/itau/sdk/android/core/type/SecurityAssertResponseDTO;)V
    .registers 4

    .line 326
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->d:Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;

    invoke-virtual {v0, p1}, Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;->a(Lbr/com/itau/sdk/android/core/type/SecurityAssertResponseDTO;)Z

    move-result v0

    if-nez v0, :cond_e

    .line 327
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->d:Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;

    invoke-virtual {v0}, Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;->hideLoading()V

    .line 328
    return-void

    .line 331
    :cond_e
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->d:Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;->onAction(I)V

    .line 332
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->d:Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;

    invoke-virtual {v0}, Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;->dismissAllowingStateLoss()V

    .line 333
    return-void
.end method

.method a(Ljava/lang/String;)V
    .registers 4

    .line 133
    invoke-virtual {p0}, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->e()V

    .line 134
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->g:Landroid/widget/EditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 135
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->e:Landroid/widget/Button;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 136
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->q:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 138
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->r:Landroid/widget/TextView;

    if-eqz v0, :cond_23

    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->r:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 139
    :cond_23
    return-void
.end method

.method b()V
    .registers 5

    .line 142
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->o:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_c

    const/4 v3, 0x1

    goto :goto_d

    :cond_c
    const/4 v3, 0x0

    .line 144
    :goto_d
    if-eqz v3, :cond_38

    .line 145
    invoke-static {}, Lbr/com/itau/sdk/android/core/BackendClient;->getInstance()Lbr/com/itau/sdk/android/core/BackendClient;

    move-result-object v0

    sget-object v1, Lbr/com/itau/sdk/android/core/token/TokenEvent$Category;->TOKEN:Lbr/com/itau/sdk/android/core/token/TokenEvent$Category;

    .line 146
    invoke-virtual {p0}, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->getSelectionAction()Lbr/com/itau/sdk/android/core/token/TokenEvent$Action;

    move-result-object v2

    .line 145
    invoke-virtual {v0, v1, v2}, Lbr/com/itau/sdk/android/core/BackendClient;->track(Lbr/com/itau/sdk/android/core/token/TokenEvent$Category;Lbr/com/itau/sdk/android/core/token/TokenEvent$Action;)V

    .line 147
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->o:Landroid/widget/RelativeLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 148
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->p:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 149
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->q:Landroid/widget/ImageView;

    sget v1, Lbr/com/itau/sdk/android/core_ui/R$drawable;->itausdkcore_ic_seta_cima:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 151
    iget-boolean v0, p0, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->s:Z

    if-eqz v0, :cond_3b

    .line 152
    invoke-virtual {p0}, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->e()V

    goto :goto_3b

    .line 155
    :cond_38
    invoke-virtual {p0}, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->d()V

    .line 157
    :cond_3b
    :goto_3b
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->d:Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;

    invoke-virtual {v0, p0}, Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;->a(Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;)V

    .line 158
    return-void
.end method

.method c()V
    .registers 3

    .line 127
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->q:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 128
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->s:Z

    .line 129
    invoke-virtual {p0}, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->d()V

    .line 130
    return-void
.end method

.method d()V
    .registers 5

    .line 114
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->o:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_9

    .line 115
    return-void

    .line 117
    :cond_9
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->o:Landroid/widget/RelativeLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 118
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->p:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 119
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->q:Landroid/widget/ImageView;

    sget v1, Lbr/com/itau/sdk/android/core_ui/R$drawable;->ic_seta_baixo:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 121
    iget-boolean v0, p0, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->s:Z

    if-nez v0, :cond_3a

    .line 122
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->n:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_27
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3a

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Landroid/view/View;

    .line 123
    const/16 v0, 0x8

    invoke-virtual {v3, v0}, Landroid/view/View;->setVisibility(I)V

    goto :goto_27

    .line 124
    :cond_3a
    return-void
.end method

.method e()V
    .registers 4

    .line 276
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->n:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_18

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Landroid/view/View;

    .line 277
    const/4 v0, 0x0

    invoke-virtual {v2, v0}, Landroid/view/View;->setVisibility(I)V

    goto :goto_6

    .line 278
    :cond_18
    return-void
.end method

.method protected abstract getSelectionAction()Lbr/com/itau/sdk/android/core/token/TokenEvent$Action;
.end method

.method protected abstract getSendAction()Lbr/com/itau/sdk/android/core/token/TokenEvent$Action;
.end method

.method abstract getType()I
.end method

.method setToken(Ljava/lang/String;)V
    .registers 7

    .line 281
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->r:Landroid/widget/TextView;

    if-eqz v0, :cond_19

    .line 282
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->r:Landroid/widget/TextView;

    invoke-virtual {p0}, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lbr/com/itau/sdk/android/core_ui/R$string;->itausdkcore_token_number:I

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 283
    :cond_19
    return-void
.end method

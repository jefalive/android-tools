.class public abstract Lcom/google/zxing/g;
.super Ljava/lang/Object;


# instance fields
.field private final a:I

.field private final b:I


# direct methods
.method protected constructor <init>(II)V
    .registers 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/zxing/g;->a:I

    iput p2, p0, Lcom/google/zxing/g;->b:I

    return-void
.end method


# virtual methods
.method public abstract a()[B
.end method

.method public abstract a(I[B)[B
.end method

.method public final b()I
    .registers 2

    iget v0, p0, Lcom/google/zxing/g;->a:I

    return v0
.end method

.method public final c()I
    .registers 2

    iget v0, p0, Lcom/google/zxing/g;->b:I

    return v0
.end method

.method public d()Z
    .registers 2

    const/4 v0, 0x0

    return v0
.end method

.method public e()Lcom/google/zxing/g;
    .registers 3

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "This luminance source does not support rotation by 90 degrees."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final toString()Ljava/lang/String;
    .registers 10

    iget v0, p0, Lcom/google/zxing/g;->a:I

    new-array v3, v0, [B

    new-instance v4, Ljava/lang/StringBuilder;

    iget v0, p0, Lcom/google/zxing/g;->b:I

    iget v1, p0, Lcom/google/zxing/g;->a:I

    add-int/lit8 v1, v1, 0x1

    mul-int/2addr v0, v1

    invoke-direct {v4, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    const/4 v5, 0x0

    :goto_11
    iget v0, p0, Lcom/google/zxing/g;->b:I

    if-ge v5, v0, :cond_47

    invoke-virtual {p0, v5, v3}, Lcom/google/zxing/g;->a(I[B)[B

    move-result-object v3

    const/4 v6, 0x0

    :goto_1a
    iget v0, p0, Lcom/google/zxing/g;->a:I

    if-ge v6, v0, :cond_3f

    aget-byte v0, v3, v6

    and-int/lit16 v7, v0, 0xff

    const/16 v0, 0x40

    if-ge v7, v0, :cond_29

    const/16 v8, 0x23

    goto :goto_39

    :cond_29
    const/16 v0, 0x80

    if-ge v7, v0, :cond_30

    const/16 v8, 0x2b

    goto :goto_39

    :cond_30
    const/16 v0, 0xc0

    if-ge v7, v0, :cond_37

    const/16 v8, 0x2e

    goto :goto_39

    :cond_37
    const/16 v8, 0x20

    :goto_39
    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    add-int/lit8 v6, v6, 0x1

    goto :goto_1a

    :cond_3f
    const/16 v0, 0xa

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    add-int/lit8 v5, v5, 0x1

    goto :goto_11

    :cond_47
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

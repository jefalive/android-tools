.class public Lcom/google/android/gms/internal/zzix;
.super Ljava/lang/Object;


# annotations
.annotation runtime Lcom/google/android/gms/internal/zzhb;
.end annotation


# instance fields
.field private mHandler:Landroid/os/Handler;

.field private zzMG:Landroid/os/HandlerThread;

.field private zzMH:I

.field private final zzpV:Ljava/lang/Object;


# direct methods
.method public constructor <init>()V
    .registers 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/internal/zzix;->zzMG:Landroid/os/HandlerThread;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/internal/zzix;->mHandler:Landroid/os/Handler;

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/internal/zzix;->zzMH:I

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/internal/zzix;->zzpV:Ljava/lang/Object;

    return-void
.end method

.method static synthetic zza(Lcom/google/android/gms/internal/zzix;)Ljava/lang/Object;
    .registers 2

    iget-object v0, p0, Lcom/google/android/gms/internal/zzix;->zzpV:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic zzb(Lcom/google/android/gms/internal/zzix;)I
    .registers 2

    iget v0, p0, Lcom/google/android/gms/internal/zzix;->zzMH:I

    return v0
.end method


# virtual methods
.method public zzhC()Landroid/os/Looper;
    .registers 5

    iget-object v2, p0, Lcom/google/android/gms/internal/zzix;->zzpV:Ljava/lang/Object;

    monitor-enter v2

    :try_start_3
    iget v0, p0, Lcom/google/android/gms/internal/zzix;->zzMH:I

    if-nez v0, :cond_3c

    iget-object v0, p0, Lcom/google/android/gms/internal/zzix;->zzMG:Landroid/os/HandlerThread;

    if-nez v0, :cond_31

    const-string v0, "Starting the looper thread."

    invoke-static {v0}, Lcom/google/android/gms/internal/zzin;->v(Ljava/lang/String;)V

    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "LooperProvider"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/internal/zzix;->zzMG:Landroid/os/HandlerThread;

    iget-object v0, p0, Lcom/google/android/gms/internal/zzix;->zzMG:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    new-instance v0, Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/gms/internal/zzix;->zzMG:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/gms/internal/zzix;->mHandler:Landroid/os/Handler;

    const-string v0, "Looper thread started."

    invoke-static {v0}, Lcom/google/android/gms/internal/zzin;->v(Ljava/lang/String;)V

    goto :goto_43

    :cond_31
    const-string v0, "Resuming the looper thread"

    invoke-static {v0}, Lcom/google/android/gms/internal/zzin;->v(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/internal/zzix;->zzpV:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    goto :goto_43

    :cond_3c
    iget-object v0, p0, Lcom/google/android/gms/internal/zzix;->zzMG:Landroid/os/HandlerThread;

    const-string v1, "Invalid state: mHandlerThread should already been initialized."

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/zzx;->zzb(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_43
    iget v0, p0, Lcom/google/android/gms/internal/zzix;->zzMH:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/gms/internal/zzix;->zzMH:I

    iget-object v0, p0, Lcom/google/android/gms/internal/zzix;->zzMG:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;
    :try_end_4e
    .catchall {:try_start_3 .. :try_end_4e} :catchall_51

    move-result-object v0

    monitor-exit v2

    return-object v0

    :catchall_51
    move-exception v3

    monitor-exit v2

    throw v3
.end method

.method public zzhD()V
    .registers 5

    iget-object v2, p0, Lcom/google/android/gms/internal/zzix;->zzpV:Ljava/lang/Object;

    monitor-enter v2

    :try_start_3
    iget v0, p0, Lcom/google/android/gms/internal/zzix;->zzMH:I

    if-lez v0, :cond_9

    const/4 v0, 0x1

    goto :goto_a

    :cond_9
    const/4 v0, 0x0

    :goto_a
    const-string v1, "Invalid state: release() called more times than expected."

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/zzx;->zzb(ZLjava/lang/Object;)V

    iget v0, p0, Lcom/google/android/gms/internal/zzix;->zzMH:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/android/gms/internal/zzix;->zzMH:I

    if-nez v0, :cond_21

    iget-object v0, p0, Lcom/google/android/gms/internal/zzix;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/gms/internal/zzix$1;

    invoke-direct {v1, p0}, Lcom/google/android/gms/internal/zzix$1;-><init>(Lcom/google/android/gms/internal/zzix;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_21
    .catchall {:try_start_3 .. :try_end_21} :catchall_23

    :cond_21
    monitor-exit v2

    goto :goto_26

    :catchall_23
    move-exception v3

    monitor-exit v2

    throw v3

    :goto_26
    return-void
.end method

.class Lnet/hockeyapp/android/views/PaintView$1;
.super Landroid/os/AsyncTask;
.source "PaintView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lnet/hockeyapp/android/views/PaintView;-><init>(Landroid/content/Context;Landroid/net/Uri;II)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<Ljava/lang/Object;Ljava/lang/Void;Landroid/graphics/Bitmap;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lnet/hockeyapp/android/views/PaintView;


# direct methods
.method constructor <init>(Lnet/hockeyapp/android/views/PaintView;)V
    .registers 2
    .param p1, "this$0"    # Lnet/hockeyapp/android/views/PaintView;

    .line 164
    iput-object p1, p0, Lnet/hockeyapp/android/views/PaintView$1;->this$0:Lnet/hockeyapp/android/views/PaintView;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Object;)Landroid/graphics/Bitmap;
    .registers 10
    .param p1, "args"    # [Ljava/lang/Object;

    .line 174
    const/4 v0, 0x0

    aget-object v0, p1, v0

    move-object v3, v0

    check-cast v3, Landroid/content/Context;

    .line 175
    .local v3, "context":Landroid/content/Context;
    const/4 v0, 0x1

    aget-object v0, p1, v0

    move-object v4, v0

    check-cast v4, Landroid/net/Uri;

    .line 176
    .local v4, "imageUri":Landroid/net/Uri;
    const/4 v0, 0x2

    aget-object v0, p1, v0

    move-object v5, v0

    check-cast v5, Ljava/lang/Integer;

    .line 177
    .local v5, "displayWidth":Ljava/lang/Integer;
    const/4 v0, 0x3

    aget-object v0, p1, v0

    move-object v6, v0

    check-cast v6, Ljava/lang/Integer;

    .line 179
    .local v6, "displayHeight":Ljava/lang/Integer;
    :try_start_18
    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v2

    # invokes: Lnet/hockeyapp/android/views/PaintView;->decodeSampledBitmapFromResource(Landroid/content/ContentResolver;Landroid/net/Uri;II)Landroid/graphics/Bitmap;
    invoke-static {v0, v4, v1, v2}, Lnet/hockeyapp/android/views/PaintView;->access$000(Landroid/content/ContentResolver;Landroid/net/Uri;II)Landroid/graphics/Bitmap;
    :try_end_27
    .catch Ljava/lang/Exception; {:try_start_18 .. :try_end_27} :catch_29

    move-result-object v7

    .line 180
    .local v7, "bm":Landroid/graphics/Bitmap;
    return-object v7

    .line 181
    .end local v7    # "bm":Landroid/graphics/Bitmap;
    :catch_29
    move-exception v7

    .line 182
    .local v7, "e":Ljava/lang/Exception;
    const-string v0, "HockeyApp"

    const-string v1, "Could not load image into ImageView."

    invoke-static {v0, v1, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 184
    .end local v7    # "e":Ljava/lang/Exception;
    const/4 v0, 0x0

    return-object v0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3

    .line 164
    invoke-virtual {p0, p1}, Lnet/hockeyapp/android/views/PaintView$1;->doInBackground([Ljava/lang/Object;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Landroid/graphics/Bitmap;)V
    .registers 3
    .param p1, "bm"    # Landroid/graphics/Bitmap;

    .line 189
    if-nez p1, :cond_3

    .line 190
    return-void

    .line 191
    :cond_3
    iget-object v0, p0, Lnet/hockeyapp/android/views/PaintView$1;->this$0:Lnet/hockeyapp/android/views/PaintView;

    invoke-virtual {v0, p1}, Lnet/hockeyapp/android/views/PaintView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 192
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .registers 3

    .line 164
    move-object v0, p1

    check-cast v0, Landroid/graphics/Bitmap;

    invoke-virtual {p0, v0}, Lnet/hockeyapp/android/views/PaintView$1;->onPostExecute(Landroid/graphics/Bitmap;)V

    return-void
.end method

.method protected onPreExecute()V
    .registers 3

    .line 169
    iget-object v0, p0, Lnet/hockeyapp/android/views/PaintView$1;->this$0:Lnet/hockeyapp/android/views/PaintView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lnet/hockeyapp/android/views/PaintView;->setAdjustViewBounds(Z)V

    .line 170
    return-void
.end method

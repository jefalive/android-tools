.class public Lcom/itau/empresas/ui/util/VersaoUtils;
.super Ljava/lang/Object;
.source "VersaoUtils.java"


# static fields
.field private static final VERSAO_PATTERN:Ljava/util/regex/Pattern;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .line 10
    const-string v0, "(\\d+)([.]\\d+)?([.]\\d+)?"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/itau/empresas/ui/util/VersaoUtils;->VERSAO_PATTERN:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>()V
    .registers 1

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static versaoSemMinor()Ljava/lang/String;
    .registers 6

    .line 13
    sget-object v0, Lcom/itau/empresas/ui/util/VersaoUtils;->VERSAO_PATTERN:Ljava/util/regex/Pattern;

    const-string v1, "4.0.3"

    invoke-virtual {v0, v1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v2

    .line 14
    .local v2, "m":Ljava/util/regex/Matcher;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 16
    .local v3, "builder":Ljava/lang/StringBuilder;
    invoke-virtual {v2}, Ljava/util/regex/Matcher;->find()Z

    move-result v0

    if-eqz v0, :cond_27

    .line 17
    const/4 v0, 0x1

    invoke-virtual {v2, v0}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v4

    .line 18
    .local v4, "group1":Ljava/lang/String;
    const/4 v0, 0x2

    invoke-virtual {v2, v0}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v5

    .line 19
    .local v5, "group2":Ljava/lang/String;
    if-eqz v4, :cond_22

    .line 20
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 22
    :cond_22
    if-eqz v5, :cond_27

    .line 23
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 27
    .end local v4    # "group1":Ljava/lang/String;
    .end local v5    # "group2":Ljava/lang/String;
    :cond_27
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-eqz v0, :cond_32

    .line 28
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 31
    :cond_32
    const-string v0, "4.0.3"

    return-object v0
.end method

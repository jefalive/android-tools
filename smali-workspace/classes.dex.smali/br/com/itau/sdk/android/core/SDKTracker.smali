.class public interface abstract Lbr/com/itau/sdk/android/core/SDKTracker;
.super Ljava/lang/Object;
.source "SourceFile"


# virtual methods
.method public abstract onEnterScreen(Lbr/com/itau/sdk/android/core/token/TokenEvent$Screen;)V
.end method

.method public abstract onLeaveScreen(Lbr/com/itau/sdk/android/core/token/TokenEvent$Screen;)V
.end method

.method public abstract onTrackingEvent(Lbr/com/itau/sdk/android/core/token/TokenEvent$Category;Lbr/com/itau/sdk/android/core/token/TokenEvent$Action;)V
.end method

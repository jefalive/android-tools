.class public final Lcom/itau/empresas/ui/dialog/PedagioWebViewDialog_;
.super Lcom/itau/empresas/ui/dialog/PedagioWebViewDialog;
.source "PedagioWebViewDialog_.java"

# interfaces
.implements Lorg/androidannotations/api/view/HasViews;
.implements Lorg/androidannotations/api/view/OnViewChangedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/itau/empresas/ui/dialog/PedagioWebViewDialog_$FragmentBuilder_;
    }
.end annotation


# instance fields
.field private contentView_:Landroid/view/View;

.field private final onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;


# direct methods
.method public constructor <init>()V
    .registers 2

    .line 24
    invoke-direct {p0}, Lcom/itau/empresas/ui/dialog/PedagioWebViewDialog;-><init>()V

    .line 28
    new-instance v0, Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-direct {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/ui/dialog/PedagioWebViewDialog_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    return-void
.end method

.method public static builder()Lcom/itau/empresas/ui/dialog/PedagioWebViewDialog_$FragmentBuilder_;
    .registers 1

    .line 81
    new-instance v0, Lcom/itau/empresas/ui/dialog/PedagioWebViewDialog_$FragmentBuilder_;

    invoke-direct {v0}, Lcom/itau/empresas/ui/dialog/PedagioWebViewDialog_$FragmentBuilder_;-><init>()V

    return-object v0
.end method

.method private init_(Landroid/os/Bundle;)V
    .registers 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 67
    invoke-static {p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->registerOnViewChangedListener(Lorg/androidannotations/api/view/OnViewChangedListener;)V

    .line 68
    invoke-direct {p0}, Lcom/itau/empresas/ui/dialog/PedagioWebViewDialog_;->injectFragmentArguments_()V

    .line 69
    invoke-static {}, Lcom/itau/empresas/CustomApplication_;->getInstance()Lcom/itau/empresas/CustomApplication;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/ui/dialog/PedagioWebViewDialog_;->application:Lcom/itau/empresas/CustomApplication;

    .line 70
    invoke-static {}, Lcom/itau/empresas/CustomApplication_;->getInstance()Lcom/itau/empresas/CustomApplication;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/ui/dialog/PedagioWebViewDialog_;->application:Lcom/itau/empresas/CustomApplication;

    .line 71
    invoke-virtual {p0}, Lcom/itau/empresas/ui/dialog/PedagioWebViewDialog_;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/itau/empresas/adapter/ItemPedagioAdapter_;->getInstance_(Landroid/content/Context;)Lcom/itau/empresas/adapter/ItemPedagioAdapter_;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/ui/dialog/PedagioWebViewDialog_;->adapter:Lcom/itau/empresas/adapter/ItemPedagioAdapter;

    .line 72
    return-void
.end method

.method private injectFragmentArguments_()V
    .registers 3

    .line 91
    invoke-virtual {p0}, Lcom/itau/empresas/ui/dialog/PedagioWebViewDialog_;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    .line 92
    .local v1, "args_":Landroid/os/Bundle;
    if-eqz v1, :cond_36

    .line 93
    const-string v0, "categoriaAnalytics"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_16

    .line 94
    const-string v0, "categoriaAnalytics"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/ui/dialog/PedagioWebViewDialog_;->categoriaAnalytics:Ljava/lang/String;

    .line 96
    :cond_16
    const-string v0, "labels"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_26

    .line 97
    const-string v0, "labels"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/ui/dialog/PedagioWebViewDialog_;->labels:Ljava/util/ArrayList;

    .line 99
    :cond_26
    const-string v0, "chaves"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_36

    .line 100
    const-string v0, "chaves"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/ui/dialog/PedagioWebViewDialog_;->chaves:Ljava/util/ArrayList;

    .line 103
    :cond_36
    return-void
.end method


# virtual methods
.method public findViewById(I)Landroid/view/View;
    .registers 3
    .param p1, "id"    # I

    .line 44
    iget-object v0, p0, Lcom/itau/empresas/ui/dialog/PedagioWebViewDialog_;->contentView_:Landroid/view/View;

    if-nez v0, :cond_6

    .line 45
    const/4 v0, 0x0

    return-object v0

    .line 47
    :cond_6
    iget-object v0, p0, Lcom/itau/empresas/ui/dialog/PedagioWebViewDialog_;->contentView_:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 36
    iget-object v0, p0, Lcom/itau/empresas/ui/dialog/PedagioWebViewDialog_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-static {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    move-result-object v1

    .line 37
    .local v1, "previousNotifier":Lorg/androidannotations/api/view/OnViewChangedNotifier;
    invoke-direct {p0, p1}, Lcom/itau/empresas/ui/dialog/PedagioWebViewDialog_;->init_(Landroid/os/Bundle;)V

    .line 38
    invoke-super {p0, p1}, Lcom/itau/empresas/ui/dialog/PedagioWebViewDialog;->onCreate(Landroid/os/Bundle;)V

    .line 39
    invoke-static {v1}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 40
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .registers 6
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .line 52
    invoke-super {p0, p1, p2, p3}, Lcom/itau/empresas/ui/dialog/PedagioWebViewDialog;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/ui/dialog/PedagioWebViewDialog_;->contentView_:Landroid/view/View;

    .line 53
    iget-object v0, p0, Lcom/itau/empresas/ui/dialog/PedagioWebViewDialog_;->contentView_:Landroid/view/View;

    if-nez v0, :cond_14

    .line 54
    const v0, 0x7f030089

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/ui/dialog/PedagioWebViewDialog_;->contentView_:Landroid/view/View;

    .line 56
    :cond_14
    iget-object v0, p0, Lcom/itau/empresas/ui/dialog/PedagioWebViewDialog_;->contentView_:Landroid/view/View;

    return-object v0
.end method

.method public onDestroyView()V
    .registers 2

    .line 61
    invoke-super {p0}, Lcom/itau/empresas/ui/dialog/PedagioWebViewDialog;->onDestroyView()V

    .line 62
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/itau/empresas/ui/dialog/PedagioWebViewDialog_;->contentView_:Landroid/view/View;

    .line 63
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/itau/empresas/ui/dialog/PedagioWebViewDialog_;->listItemPopup:Landroid/widget/ListView;

    .line 64
    return-void
.end method

.method public onViewChanged(Lorg/androidannotations/api/view/HasViews;)V
    .registers 3
    .param p1, "hasViews"    # Lorg/androidannotations/api/view/HasViews;

    .line 86
    const v0, 0x7f0e03a3

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/itau/empresas/ui/dialog/PedagioWebViewDialog_;->listItemPopup:Landroid/widget/ListView;

    .line 87
    invoke-virtual {p0}, Lcom/itau/empresas/ui/dialog/PedagioWebViewDialog_;->aoInicializar()V

    .line 88
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .registers 4
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;

    .line 76
    invoke-super {p0, p1, p2}, Lcom/itau/empresas/ui/dialog/PedagioWebViewDialog;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 77
    iget-object v0, p0, Lcom/itau/empresas/ui/dialog/PedagioWebViewDialog_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 78
    return-void
.end method

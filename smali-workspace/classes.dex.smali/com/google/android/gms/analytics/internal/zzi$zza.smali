.class public Lcom/google/android/gms/analytics/internal/zzi$zza;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/analytics/internal/zzi;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "zza"
.end annotation


# instance fields
.field final synthetic zzQL:Lcom/google/android/gms/analytics/internal/zzi;

.field private volatile zzQM:Lcom/google/android/gms/analytics/internal/zzac;

.field private volatile zzQN:Z


# direct methods
.method protected constructor <init>(Lcom/google/android/gms/analytics/internal/zzi;)V
    .registers 2

    iput-object p1, p0, Lcom/google/android/gms/analytics/internal/zzi$zza;->zzQL:Lcom/google/android/gms/analytics/internal/zzi;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .registers 11
    .param p1, "name"    # Landroid/content/ComponentName;
    .param p2, "binder"    # Landroid/os/IBinder;

    const-string v0, "AnalyticsServiceConnection.onServiceConnected"

    invoke-static {v0}, Lcom/google/android/gms/common/internal/zzx;->zzcD(Ljava/lang/String;)V

    move-object v3, p0

    monitor-enter v3

    if-nez p2, :cond_15

    :try_start_9
    iget-object v0, p0, Lcom/google/android/gms/analytics/internal/zzi$zza;->zzQL:Lcom/google/android/gms/analytics/internal/zzi;

    const-string v1, "Service connected with null binder"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/analytics/internal/zzi;->zzbh(Ljava/lang/String;)V
    :try_end_10
    .catchall {:try_start_9 .. :try_end_10} :catchall_78

    :try_start_10
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V
    :try_end_13
    .catchall {:try_start_10 .. :try_end_13} :catchall_7f

    monitor-exit v3

    return-void

    :cond_15
    const/4 v4, 0x0

    :try_start_16
    invoke-interface {p2}, Landroid/os/IBinder;->getInterfaceDescriptor()Ljava/lang/String;

    move-result-object v5

    const-string v0, "com.google.android.gms.analytics.internal.IAnalyticsService"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2f

    invoke-static {p2}, Lcom/google/android/gms/analytics/internal/zzac$zza;->zzaf(Landroid/os/IBinder;)Lcom/google/android/gms/analytics/internal/zzac;

    move-result-object v0

    move-object v4, v0

    iget-object v0, p0, Lcom/google/android/gms/analytics/internal/zzi$zza;->zzQL:Lcom/google/android/gms/analytics/internal/zzi;

    const-string v1, "Bound to IAnalyticsService interface"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/analytics/internal/zzi;->zzbd(Ljava/lang/String;)V

    goto :goto_36

    :cond_2f
    iget-object v0, p0, Lcom/google/android/gms/analytics/internal/zzi$zza;->zzQL:Lcom/google/android/gms/analytics/internal/zzi;

    const-string v1, "Got binder with a wrong descriptor"

    invoke-virtual {v0, v1, v5}, Lcom/google/android/gms/analytics/internal/zzi;->zze(Ljava/lang/String;Ljava/lang/Object;)V
    :try_end_36
    .catch Landroid/os/RemoteException; {:try_start_16 .. :try_end_36} :catch_37
    .catchall {:try_start_16 .. :try_end_36} :catchall_78

    :goto_36
    goto :goto_3f

    :catch_37
    move-exception v5

    :try_start_38
    iget-object v0, p0, Lcom/google/android/gms/analytics/internal/zzi$zza;->zzQL:Lcom/google/android/gms/analytics/internal/zzi;

    const-string v1, "Service connect failed to get IAnalyticsService"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/analytics/internal/zzi;->zzbh(Ljava/lang/String;)V
    :try_end_3f
    .catchall {:try_start_38 .. :try_end_3f} :catchall_78

    :goto_3f
    if-nez v4, :cond_57

    :try_start_41
    invoke-static {}, Lcom/google/android/gms/common/stats/zzb;->zzrP()Lcom/google/android/gms/common/stats/zzb;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/analytics/internal/zzi$zza;->zzQL:Lcom/google/android/gms/analytics/internal/zzi;

    invoke-virtual {v1}, Lcom/google/android/gms/analytics/internal/zzi;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/analytics/internal/zzi$zza;->zzQL:Lcom/google/android/gms/analytics/internal/zzi;

    invoke-static {v2}, Lcom/google/android/gms/analytics/internal/zzi;->zza(Lcom/google/android/gms/analytics/internal/zzi;)Lcom/google/android/gms/analytics/internal/zzi$zza;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/stats/zzb;->zza(Landroid/content/Context;Landroid/content/ServiceConnection;)V
    :try_end_54
    .catch Ljava/lang/IllegalArgumentException; {:try_start_41 .. :try_end_54} :catch_55
    .catchall {:try_start_41 .. :try_end_54} :catchall_78

    goto :goto_74

    :catch_55
    move-exception v5

    goto :goto_74

    :cond_57
    :try_start_57
    iget-boolean v0, p0, Lcom/google/android/gms/analytics/internal/zzi$zza;->zzQN:Z

    if-nez v0, :cond_72

    iget-object v0, p0, Lcom/google/android/gms/analytics/internal/zzi$zza;->zzQL:Lcom/google/android/gms/analytics/internal/zzi;

    const-string v1, "onServiceConnected received after the timeout limit"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/analytics/internal/zzi;->zzbg(Ljava/lang/String;)V

    move-object v5, v4

    iget-object v0, p0, Lcom/google/android/gms/analytics/internal/zzi$zza;->zzQL:Lcom/google/android/gms/analytics/internal/zzi;

    invoke-virtual {v0}, Lcom/google/android/gms/analytics/internal/zzi;->zzjo()Lcom/google/android/gms/measurement/zzg;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/analytics/internal/zzi$zza$1;

    invoke-direct {v1, p0, v5}, Lcom/google/android/gms/analytics/internal/zzi$zza$1;-><init>(Lcom/google/android/gms/analytics/internal/zzi$zza;Lcom/google/android/gms/analytics/internal/zzac;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/measurement/zzg;->zzf(Ljava/lang/Runnable;)V

    goto :goto_74

    :cond_72
    iput-object v4, p0, Lcom/google/android/gms/analytics/internal/zzi$zza;->zzQM:Lcom/google/android/gms/analytics/internal/zzac;
    :try_end_74
    .catchall {:try_start_57 .. :try_end_74} :catchall_78

    :goto_74
    :try_start_74
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V
    :try_end_77
    .catchall {:try_start_74 .. :try_end_77} :catchall_7f

    goto :goto_7d

    :catchall_78
    move-exception v6

    :try_start_79
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    throw v6
    :try_end_7d
    .catchall {:try_start_79 .. :try_end_7d} :catchall_7f

    :goto_7d
    monitor-exit v3

    goto :goto_82

    :catchall_7f
    move-exception v7

    monitor-exit v3

    throw v7

    :goto_82
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .registers 4
    .param p1, "name"    # Landroid/content/ComponentName;

    const-string v0, "AnalyticsServiceConnection.onServiceDisconnected"

    invoke-static {v0}, Lcom/google/android/gms/common/internal/zzx;->zzcD(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/analytics/internal/zzi$zza;->zzQL:Lcom/google/android/gms/analytics/internal/zzi;

    invoke-virtual {v0}, Lcom/google/android/gms/analytics/internal/zzi;->zzjo()Lcom/google/android/gms/measurement/zzg;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/analytics/internal/zzi$zza$2;

    invoke-direct {v1, p0, p1}, Lcom/google/android/gms/analytics/internal/zzi$zza$2;-><init>(Lcom/google/android/gms/analytics/internal/zzi$zza;Landroid/content/ComponentName;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/measurement/zzg;->zzf(Ljava/lang/Runnable;)V

    return-void
.end method

.method public zzjK()Lcom/google/android/gms/analytics/internal/zzac;
    .registers 12

    iget-object v0, p0, Lcom/google/android/gms/analytics/internal/zzi$zza;->zzQL:Lcom/google/android/gms/analytics/internal/zzi;

    invoke-virtual {v0}, Lcom/google/android/gms/analytics/internal/zzi;->zzjk()V

    new-instance v3, Landroid/content/Intent;

    const-string v0, "com.google.android.gms.analytics.service.START"

    invoke-direct {v3, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    new-instance v0, Landroid/content/ComponentName;

    const-string v1, "com.google.android.gms"

    const-string v2, "com.google.android.gms.analytics.service.AnalyticsService"

    invoke-direct {v0, v1, v2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    iget-object v0, p0, Lcom/google/android/gms/analytics/internal/zzi$zza;->zzQL:Lcom/google/android/gms/analytics/internal/zzi;

    invoke-virtual {v0}, Lcom/google/android/gms/analytics/internal/zzi;->getContext()Landroid/content/Context;

    move-result-object v4

    const-string v0, "app_package_name"

    invoke-virtual {v4}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-static {}, Lcom/google/android/gms/common/stats/zzb;->zzrP()Lcom/google/android/gms/common/stats/zzb;

    move-result-object v5

    move-object v6, p0

    monitor-enter v6

    const/4 v0, 0x0

    :try_start_2e
    iput-object v0, p0, Lcom/google/android/gms/analytics/internal/zzi$zza;->zzQM:Lcom/google/android/gms/analytics/internal/zzac;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/analytics/internal/zzi$zza;->zzQN:Z

    iget-object v0, p0, Lcom/google/android/gms/analytics/internal/zzi$zza;->zzQL:Lcom/google/android/gms/analytics/internal/zzi;

    invoke-static {v0}, Lcom/google/android/gms/analytics/internal/zzi;->zza(Lcom/google/android/gms/analytics/internal/zzi;)Lcom/google/android/gms/analytics/internal/zzi$zza;

    move-result-object v0

    const/16 v1, 0x81

    invoke-virtual {v5, v4, v3, v0, v1}, Lcom/google/android/gms/common/stats/zzb;->zza(Landroid/content/Context;Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v7

    iget-object v0, p0, Lcom/google/android/gms/analytics/internal/zzi$zza;->zzQL:Lcom/google/android/gms/analytics/internal/zzi;

    const-string v1, "Bind to service requested"

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/analytics/internal/zzi;->zza(Ljava/lang/String;Ljava/lang/Object;)V

    if-nez v7, :cond_52

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/analytics/internal/zzi$zza;->zzQN:Z
    :try_end_4f
    .catchall {:try_start_2e .. :try_end_4f} :catchall_7b

    monitor-exit v6

    const/4 v0, 0x0

    return-object v0

    :cond_52
    :try_start_52
    iget-object v0, p0, Lcom/google/android/gms/analytics/internal/zzi$zza;->zzQL:Lcom/google/android/gms/analytics/internal/zzi;

    invoke-virtual {v0}, Lcom/google/android/gms/analytics/internal/zzi;->zzjn()Lcom/google/android/gms/analytics/internal/zzr;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/analytics/internal/zzr;->zzkN()J

    move-result-wide v8

    invoke-virtual {p0, v8, v9}, Ljava/lang/Object;->wait(J)V
    :try_end_5f
    .catch Ljava/lang/InterruptedException; {:try_start_52 .. :try_end_5f} :catch_60
    .catchall {:try_start_52 .. :try_end_5f} :catchall_7b

    goto :goto_68

    :catch_60
    move-exception v8

    :try_start_61
    iget-object v0, p0, Lcom/google/android/gms/analytics/internal/zzi$zza;->zzQL:Lcom/google/android/gms/analytics/internal/zzi;

    const-string v1, "Wait for service connect was interrupted"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/analytics/internal/zzi;->zzbg(Ljava/lang/String;)V

    :goto_68
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/analytics/internal/zzi$zza;->zzQN:Z

    iget-object v8, p0, Lcom/google/android/gms/analytics/internal/zzi$zza;->zzQM:Lcom/google/android/gms/analytics/internal/zzac;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/analytics/internal/zzi$zza;->zzQM:Lcom/google/android/gms/analytics/internal/zzac;

    if-nez v8, :cond_79

    iget-object v0, p0, Lcom/google/android/gms/analytics/internal/zzi$zza;->zzQL:Lcom/google/android/gms/analytics/internal/zzi;

    const-string v1, "Successfully bound to service but never got onServiceConnected callback"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/analytics/internal/zzi;->zzbh(Ljava/lang/String;)V
    :try_end_79
    .catchall {:try_start_61 .. :try_end_79} :catchall_7b

    :cond_79
    monitor-exit v6

    return-object v8

    :catchall_7b
    move-exception v10

    monitor-exit v6

    throw v10
.end method

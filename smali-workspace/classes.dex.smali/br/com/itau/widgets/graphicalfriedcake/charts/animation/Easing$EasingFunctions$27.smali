.class final Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingFunctions$27;
.super Ljava/lang/Object;
.source "Easing.java"

# interfaces
.implements Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/EasingFunction;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingFunctions;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .registers 1

    .line 647
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getInterpolation(F)F
    .registers 5
    .param p1, "input"    # F

    .line 674
    move v2, p1

    .line 675
    .local v2, "position":F
    const v0, 0x3eba2e8c

    cmpg-float v0, v2, v0

    if-gez v0, :cond_d

    .line 677
    const/high16 v0, 0x40f20000    # 7.5625f

    mul-float/2addr v0, v2

    mul-float/2addr v0, v2

    return v0

    .line 679
    :cond_d
    const v0, 0x3f3a2e8c

    cmpg-float v0, v2, v0

    if-gez v0, :cond_22

    .line 681
    const v0, 0x3f0ba2e9

    sub-float v0, v2, v0

    move v2, v0

    const/high16 v1, 0x40f20000    # 7.5625f

    mul-float/2addr v0, v1

    mul-float/2addr v0, v2

    const/high16 v1, 0x3f400000    # 0.75f

    add-float/2addr v0, v1

    return v0

    .line 683
    :cond_22
    const v0, 0x3f68ba2f

    cmpg-float v0, v2, v0

    if-gez v0, :cond_37

    .line 685
    const v0, 0x3f51745d

    sub-float v0, v2, v0

    move v2, v0

    const/high16 v1, 0x40f20000    # 7.5625f

    mul-float/2addr v0, v1

    mul-float/2addr v0, v2

    const/high16 v1, 0x3f700000    # 0.9375f

    add-float/2addr v0, v1

    return v0

    .line 689
    :cond_37
    const v0, 0x3f745d17

    sub-float v0, v2, v0

    move v2, v0

    const/high16 v1, 0x40f20000    # 7.5625f

    mul-float/2addr v0, v1

    mul-float/2addr v0, v2

    const/high16 v1, 0x3f7c0000    # 0.984375f

    add-float/2addr v0, v1

    return v0
.end method

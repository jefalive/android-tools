.class public final Lorg/threeten/bp/LocalTime;
.super Lorg/threeten/bp/jdk8/DefaultInterfaceTemporalAccessor;
.source "LocalTime.java"

# interfaces
.implements Lorg/threeten/bp/temporal/Temporal;
.implements Lorg/threeten/bp/temporal/TemporalAdjuster;
.implements Ljava/lang/Comparable;
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/threeten/bp/LocalTime$2;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/threeten/bp/jdk8/DefaultInterfaceTemporalAccessor;Lorg/threeten/bp/temporal/Temporal;Lorg/threeten/bp/temporal/TemporalAdjuster;Ljava/lang/Comparable<Lorg/threeten/bp/LocalTime;>;Ljava/io/Serializable;"
    }
.end annotation


# static fields
.field public static final FROM:Lorg/threeten/bp/temporal/TemporalQuery;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/threeten/bp/temporal/TemporalQuery<Lorg/threeten/bp/LocalTime;>;"
        }
    .end annotation
.end field

.field private static final HOURS:[Lorg/threeten/bp/LocalTime;

.field public static final MAX:Lorg/threeten/bp/LocalTime;

.field public static final MIDNIGHT:Lorg/threeten/bp/LocalTime;

.field public static final MIN:Lorg/threeten/bp/LocalTime;

.field public static final NOON:Lorg/threeten/bp/LocalTime;

.field private static final serialVersionUID:J = 0x5904a8b626e1a4f1L


# instance fields
.field private final hour:B

.field private final minute:B

.field private final nano:I

.field private final second:B


# direct methods
.method static constructor <clinit>()V
    .registers 6

    .line 113
    new-instance v0, Lorg/threeten/bp/LocalTime$1;

    invoke-direct {v0}, Lorg/threeten/bp/LocalTime$1;-><init>()V

    sput-object v0, Lorg/threeten/bp/LocalTime;->FROM:Lorg/threeten/bp/temporal/TemporalQuery;

    .line 122
    const/16 v0, 0x18

    new-array v0, v0, [Lorg/threeten/bp/LocalTime;

    sput-object v0, Lorg/threeten/bp/LocalTime;->HOURS:[Lorg/threeten/bp/LocalTime;

    .line 124
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_e
    sget-object v0, Lorg/threeten/bp/LocalTime;->HOURS:[Lorg/threeten/bp/LocalTime;

    array-length v0, v0

    if-ge v5, v0, :cond_22

    .line 125
    sget-object v0, Lorg/threeten/bp/LocalTime;->HOURS:[Lorg/threeten/bp/LocalTime;

    new-instance v1, Lorg/threeten/bp/LocalTime;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-direct {v1, v5, v2, v3, v4}, Lorg/threeten/bp/LocalTime;-><init>(IIII)V

    aput-object v1, v0, v5

    .line 124
    add-int/lit8 v5, v5, 0x1

    goto :goto_e

    .line 127
    .end local v5    # "i":I
    :cond_22
    sget-object v0, Lorg/threeten/bp/LocalTime;->HOURS:[Lorg/threeten/bp/LocalTime;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    sput-object v0, Lorg/threeten/bp/LocalTime;->MIDNIGHT:Lorg/threeten/bp/LocalTime;

    .line 128
    sget-object v0, Lorg/threeten/bp/LocalTime;->HOURS:[Lorg/threeten/bp/LocalTime;

    const/16 v1, 0xc

    aget-object v0, v0, v1

    sput-object v0, Lorg/threeten/bp/LocalTime;->NOON:Lorg/threeten/bp/LocalTime;

    .line 129
    sget-object v0, Lorg/threeten/bp/LocalTime;->HOURS:[Lorg/threeten/bp/LocalTime;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    sput-object v0, Lorg/threeten/bp/LocalTime;->MIN:Lorg/threeten/bp/LocalTime;

    .line 130
    new-instance v0, Lorg/threeten/bp/LocalTime;

    const/16 v1, 0x17

    const/16 v2, 0x3b

    const/16 v3, 0x3b

    const v4, 0x3b9ac9ff

    invoke-direct {v0, v1, v2, v3, v4}, Lorg/threeten/bp/LocalTime;-><init>(IIII)V

    sput-object v0, Lorg/threeten/bp/LocalTime;->MAX:Lorg/threeten/bp/LocalTime;

    .line 131
    return-void
.end method

.method private constructor <init>(IIII)V
    .registers 6
    .param p1, "hour"    # I
    .param p2, "minute"    # I
    .param p3, "second"    # I
    .param p4, "nanoOfSecond"    # I

    .line 467
    invoke-direct {p0}, Lorg/threeten/bp/jdk8/DefaultInterfaceTemporalAccessor;-><init>()V

    .line 468
    int-to-byte v0, p1

    iput-byte v0, p0, Lorg/threeten/bp/LocalTime;->hour:B

    .line 469
    int-to-byte v0, p2

    iput-byte v0, p0, Lorg/threeten/bp/LocalTime;->minute:B

    .line 470
    int-to-byte v0, p3

    iput-byte v0, p0, Lorg/threeten/bp/LocalTime;->second:B

    .line 471
    iput p4, p0, Lorg/threeten/bp/LocalTime;->nano:I

    .line 472
    return-void
.end method

.method private static create(IIII)Lorg/threeten/bp/LocalTime;
    .registers 5
    .param p0, "hour"    # I
    .param p1, "minute"    # I
    .param p2, "second"    # I
    .param p3, "nanoOfSecond"    # I

    .line 453
    or-int v0, p1, p2

    or-int/2addr v0, p3

    if-nez v0, :cond_a

    .line 454
    sget-object v0, Lorg/threeten/bp/LocalTime;->HOURS:[Lorg/threeten/bp/LocalTime;

    aget-object v0, v0, p0

    return-object v0

    .line 456
    :cond_a
    new-instance v0, Lorg/threeten/bp/LocalTime;

    invoke-direct {v0, p0, p1, p2, p3}, Lorg/threeten/bp/LocalTime;-><init>(IIII)V

    return-object v0
.end method

.method public static from(Lorg/threeten/bp/temporal/TemporalAccessor;)Lorg/threeten/bp/LocalTime;
    .registers 5
    .param p0, "temporal"    # Lorg/threeten/bp/temporal/TemporalAccessor;

    .line 402
    invoke-static {}, Lorg/threeten/bp/temporal/TemporalQueries;->localTime()Lorg/threeten/bp/temporal/TemporalQuery;

    move-result-object v0

    invoke-interface {p0, v0}, Lorg/threeten/bp/temporal/TemporalAccessor;->query(Lorg/threeten/bp/temporal/TemporalQuery;)Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lorg/threeten/bp/LocalTime;

    .line 403
    .local v3, "time":Lorg/threeten/bp/LocalTime;
    if-nez v3, :cond_38

    .line 404
    new-instance v0, Lorg/threeten/bp/DateTimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unable to obtain LocalTime from TemporalAccessor: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", type "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/threeten/bp/DateTimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 407
    :cond_38
    return-object v3
.end method

.method private get0(Lorg/threeten/bp/temporal/TemporalField;)I
    .registers 7
    .param p1, "field"    # Lorg/threeten/bp/temporal/TemporalField;

    .line 623
    sget-object v0, Lorg/threeten/bp/LocalTime$2;->$SwitchMap$org$threeten$bp$temporal$ChronoField:[I

    move-object v1, p1

    check-cast v1, Lorg/threeten/bp/temporal/ChronoField;

    invoke-virtual {v1}, Lorg/threeten/bp/temporal/ChronoField;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_ac

    goto/16 :goto_92

    .line 624
    :pswitch_10
    iget v0, p0, Lorg/threeten/bp/LocalTime;->nano:I

    return v0

    .line 625
    :pswitch_13
    new-instance v0, Lorg/threeten/bp/DateTimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Field too large for an int: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/threeten/bp/DateTimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 626
    :pswitch_2c
    iget v0, p0, Lorg/threeten/bp/LocalTime;->nano:I

    div-int/lit16 v0, v0, 0x3e8

    return v0

    .line 627
    :pswitch_31
    new-instance v0, Lorg/threeten/bp/DateTimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Field too large for an int: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/threeten/bp/DateTimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 628
    :pswitch_4a
    iget v0, p0, Lorg/threeten/bp/LocalTime;->nano:I

    const v1, 0xf4240

    div-int/2addr v0, v1

    return v0

    .line 629
    :pswitch_51
    invoke-virtual {p0}, Lorg/threeten/bp/LocalTime;->toNanoOfDay()J

    move-result-wide v0

    const-wide/32 v2, 0xf4240

    div-long/2addr v0, v2

    long-to-int v0, v0

    return v0

    .line 630
    :pswitch_5b
    iget-byte v0, p0, Lorg/threeten/bp/LocalTime;->second:B

    return v0

    .line 631
    :pswitch_5e
    invoke-virtual {p0}, Lorg/threeten/bp/LocalTime;->toSecondOfDay()I

    move-result v0

    return v0

    .line 632
    :pswitch_63
    iget-byte v0, p0, Lorg/threeten/bp/LocalTime;->minute:B

    return v0

    .line 633
    :pswitch_66
    iget-byte v0, p0, Lorg/threeten/bp/LocalTime;->hour:B

    mul-int/lit8 v0, v0, 0x3c

    iget-byte v1, p0, Lorg/threeten/bp/LocalTime;->minute:B

    add-int/2addr v0, v1

    return v0

    .line 634
    :pswitch_6e
    iget-byte v0, p0, Lorg/threeten/bp/LocalTime;->hour:B

    rem-int/lit8 v0, v0, 0xc

    return v0

    .line 635
    :pswitch_73
    iget-byte v0, p0, Lorg/threeten/bp/LocalTime;->hour:B

    rem-int/lit8 v4, v0, 0xc

    .local v4, "ham":I
    rem-int/lit8 v0, v4, 0xc

    if-nez v0, :cond_7e

    const/16 v0, 0xc

    goto :goto_7f

    :cond_7e
    move v0, v4

    :goto_7f
    return v0

    .line 636
    :pswitch_80
    iget-byte v0, p0, Lorg/threeten/bp/LocalTime;->hour:B

    return v0

    .line 637
    :pswitch_83
    iget-byte v0, p0, Lorg/threeten/bp/LocalTime;->hour:B

    if-nez v0, :cond_8a

    const/16 v0, 0x18

    goto :goto_8c

    :cond_8a
    iget-byte v0, p0, Lorg/threeten/bp/LocalTime;->hour:B

    :goto_8c
    return v0

    .line 638
    :pswitch_8d
    iget-byte v0, p0, Lorg/threeten/bp/LocalTime;->hour:B

    div-int/lit8 v0, v0, 0xc

    return v0

    .line 640
    .end local v4    # "ham":I
    :goto_92
    new-instance v0, Lorg/threeten/bp/temporal/UnsupportedTemporalTypeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unsupported field: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/threeten/bp/temporal/UnsupportedTemporalTypeException;-><init>(Ljava/lang/String;)V

    throw v0

    nop

    :pswitch_data_ac
    .packed-switch 0x1
        :pswitch_10
        :pswitch_13
        :pswitch_2c
        :pswitch_31
        :pswitch_4a
        :pswitch_51
        :pswitch_5b
        :pswitch_5e
        :pswitch_63
        :pswitch_66
        :pswitch_6e
        :pswitch_73
        :pswitch_80
        :pswitch_83
        :pswitch_8d
    .end packed-switch
.end method

.method public static now()Lorg/threeten/bp/LocalTime;
    .registers 1

    .line 217
    invoke-static {}, Lorg/threeten/bp/Clock;->systemDefaultZone()Lorg/threeten/bp/Clock;

    move-result-object v0

    invoke-static {v0}, Lorg/threeten/bp/LocalTime;->now(Lorg/threeten/bp/Clock;)Lorg/threeten/bp/LocalTime;

    move-result-object v0

    return-object v0
.end method

.method public static now(Lorg/threeten/bp/Clock;)Lorg/threeten/bp/LocalTime;
    .registers 9
    .param p0, "clock"    # Lorg/threeten/bp/Clock;

    .line 247
    const-string v0, "clock"

    invoke-static {p0, v0}, Lorg/threeten/bp/jdk8/Jdk8Methods;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 249
    invoke-virtual {p0}, Lorg/threeten/bp/Clock;->instant()Lorg/threeten/bp/Instant;

    move-result-object v4

    .line 250
    .local v4, "now":Lorg/threeten/bp/Instant;
    invoke-virtual {p0}, Lorg/threeten/bp/Clock;->getZone()Lorg/threeten/bp/ZoneId;

    move-result-object v0

    invoke-virtual {v0}, Lorg/threeten/bp/ZoneId;->getRules()Lorg/threeten/bp/zone/ZoneRules;

    move-result-object v0

    invoke-virtual {v0, v4}, Lorg/threeten/bp/zone/ZoneRules;->getOffset(Lorg/threeten/bp/Instant;)Lorg/threeten/bp/ZoneOffset;

    move-result-object v5

    .line 251
    .local v5, "offset":Lorg/threeten/bp/ZoneOffset;
    invoke-virtual {v4}, Lorg/threeten/bp/Instant;->getEpochSecond()J

    move-result-wide v0

    const-wide/32 v2, 0x15180

    rem-long v6, v0, v2

    .line 252
    .local v6, "secsOfDay":J
    invoke-virtual {v5}, Lorg/threeten/bp/ZoneOffset;->getTotalSeconds()I

    move-result v0

    int-to-long v0, v0

    add-long/2addr v0, v6

    const-wide/32 v2, 0x15180

    rem-long v6, v0, v2

    .line 253
    const-wide/16 v0, 0x0

    cmp-long v0, v6, v0

    if-gez v0, :cond_33

    .line 254
    const-wide/32 v0, 0x15180

    add-long/2addr v6, v0

    .line 256
    :cond_33
    invoke-virtual {v4}, Lorg/threeten/bp/Instant;->getNano()I

    move-result v0

    invoke-static {v6, v7, v0}, Lorg/threeten/bp/LocalTime;->ofSecondOfDay(JI)Lorg/threeten/bp/LocalTime;

    move-result-object v0

    return-object v0
.end method

.method public static of(II)Lorg/threeten/bp/LocalTime;
    .registers 5
    .param p0, "hour"    # I
    .param p1, "minute"    # I

    .line 273
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->HOUR_OF_DAY:Lorg/threeten/bp/temporal/ChronoField;

    int-to-long v1, p0

    invoke-virtual {v0, v1, v2}, Lorg/threeten/bp/temporal/ChronoField;->checkValidValue(J)J

    .line 274
    if-nez p1, :cond_d

    .line 275
    sget-object v0, Lorg/threeten/bp/LocalTime;->HOURS:[Lorg/threeten/bp/LocalTime;

    aget-object v0, v0, p0

    return-object v0

    .line 277
    :cond_d
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->MINUTE_OF_HOUR:Lorg/threeten/bp/temporal/ChronoField;

    int-to-long v1, p1

    invoke-virtual {v0, v1, v2}, Lorg/threeten/bp/temporal/ChronoField;->checkValidValue(J)J

    .line 278
    new-instance v0, Lorg/threeten/bp/LocalTime;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-direct {v0, p0, p1, v1, v2}, Lorg/threeten/bp/LocalTime;-><init>(IIII)V

    return-object v0
.end method

.method public static of(III)Lorg/threeten/bp/LocalTime;
    .registers 6
    .param p0, "hour"    # I
    .param p1, "minute"    # I
    .param p2, "second"    # I

    .line 295
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->HOUR_OF_DAY:Lorg/threeten/bp/temporal/ChronoField;

    int-to-long v1, p0

    invoke-virtual {v0, v1, v2}, Lorg/threeten/bp/temporal/ChronoField;->checkValidValue(J)J

    .line 296
    or-int v0, p1, p2

    if-nez v0, :cond_f

    .line 297
    sget-object v0, Lorg/threeten/bp/LocalTime;->HOURS:[Lorg/threeten/bp/LocalTime;

    aget-object v0, v0, p0

    return-object v0

    .line 299
    :cond_f
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->MINUTE_OF_HOUR:Lorg/threeten/bp/temporal/ChronoField;

    int-to-long v1, p1

    invoke-virtual {v0, v1, v2}, Lorg/threeten/bp/temporal/ChronoField;->checkValidValue(J)J

    .line 300
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->SECOND_OF_MINUTE:Lorg/threeten/bp/temporal/ChronoField;

    int-to-long v1, p2

    invoke-virtual {v0, v1, v2}, Lorg/threeten/bp/temporal/ChronoField;->checkValidValue(J)J

    .line 301
    new-instance v0, Lorg/threeten/bp/LocalTime;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, p2, v1}, Lorg/threeten/bp/LocalTime;-><init>(IIII)V

    return-object v0
.end method

.method public static of(IIII)Lorg/threeten/bp/LocalTime;
    .registers 7
    .param p0, "hour"    # I
    .param p1, "minute"    # I
    .param p2, "second"    # I
    .param p3, "nanoOfSecond"    # I

    .line 317
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->HOUR_OF_DAY:Lorg/threeten/bp/temporal/ChronoField;

    int-to-long v1, p0

    invoke-virtual {v0, v1, v2}, Lorg/threeten/bp/temporal/ChronoField;->checkValidValue(J)J

    .line 318
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->MINUTE_OF_HOUR:Lorg/threeten/bp/temporal/ChronoField;

    int-to-long v1, p1

    invoke-virtual {v0, v1, v2}, Lorg/threeten/bp/temporal/ChronoField;->checkValidValue(J)J

    .line 319
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->SECOND_OF_MINUTE:Lorg/threeten/bp/temporal/ChronoField;

    int-to-long v1, p2

    invoke-virtual {v0, v1, v2}, Lorg/threeten/bp/temporal/ChronoField;->checkValidValue(J)J

    .line 320
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->NANO_OF_SECOND:Lorg/threeten/bp/temporal/ChronoField;

    int-to-long v1, p3

    invoke-virtual {v0, v1, v2}, Lorg/threeten/bp/temporal/ChronoField;->checkValidValue(J)J

    .line 321
    invoke-static {p0, p1, p2, p3}, Lorg/threeten/bp/LocalTime;->create(IIII)Lorg/threeten/bp/LocalTime;

    move-result-object v0

    return-object v0
.end method

.method public static ofNanoOfDay(J)Lorg/threeten/bp/LocalTime;
    .registers 9
    .param p0, "nanoOfDay"    # J

    .line 374
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->NANO_OF_DAY:Lorg/threeten/bp/temporal/ChronoField;

    invoke-virtual {v0, p0, p1}, Lorg/threeten/bp/temporal/ChronoField;->checkValidValue(J)J

    .line 375
    const-wide v0, 0x34630b8a000L

    div-long v0, p0, v0

    long-to-int v4, v0

    .line 376
    .local v4, "hours":I
    int-to-long v0, v4

    const-wide v2, 0x34630b8a000L

    mul-long/2addr v0, v2

    sub-long/2addr p0, v0

    .line 377
    const-wide v0, 0xdf8475800L

    div-long v0, p0, v0

    long-to-int v5, v0

    .line 378
    .local v5, "minutes":I
    int-to-long v0, v5

    const-wide v2, 0xdf8475800L

    mul-long/2addr v0, v2

    sub-long/2addr p0, v0

    .line 379
    const-wide/32 v0, 0x3b9aca00

    div-long v0, p0, v0

    long-to-int v6, v0

    .line 380
    .local v6, "seconds":I
    int-to-long v0, v6

    const-wide/32 v2, 0x3b9aca00

    mul-long/2addr v0, v2

    sub-long/2addr p0, v0

    .line 381
    long-to-int v0, p0

    invoke-static {v4, v5, v6, v0}, Lorg/threeten/bp/LocalTime;->create(IIII)Lorg/threeten/bp/LocalTime;

    move-result-object v0

    return-object v0
.end method

.method public static ofSecondOfDay(J)Lorg/threeten/bp/LocalTime;
    .registers 6
    .param p0, "secondOfDay"    # J

    .line 335
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->SECOND_OF_DAY:Lorg/threeten/bp/temporal/ChronoField;

    invoke-virtual {v0, p0, p1}, Lorg/threeten/bp/temporal/ChronoField;->checkValidValue(J)J

    .line 336
    const-wide/16 v0, 0xe10

    div-long v0, p0, v0

    long-to-int v2, v0

    .line 337
    .local v2, "hours":I
    mul-int/lit16 v0, v2, 0xe10

    int-to-long v0, v0

    sub-long/2addr p0, v0

    .line 338
    const-wide/16 v0, 0x3c

    div-long v0, p0, v0

    long-to-int v3, v0

    .line 339
    .local v3, "minutes":I
    mul-int/lit8 v0, v3, 0x3c

    int-to-long v0, v0

    sub-long/2addr p0, v0

    .line 340
    long-to-int v0, p0

    const/4 v1, 0x0

    invoke-static {v2, v3, v0, v1}, Lorg/threeten/bp/LocalTime;->create(IIII)Lorg/threeten/bp/LocalTime;

    move-result-object v0

    return-object v0
.end method

.method static ofSecondOfDay(JI)Lorg/threeten/bp/LocalTime;
    .registers 8
    .param p0, "secondOfDay"    # J
    .param p2, "nanoOfSecond"    # I

    .line 355
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->SECOND_OF_DAY:Lorg/threeten/bp/temporal/ChronoField;

    invoke-virtual {v0, p0, p1}, Lorg/threeten/bp/temporal/ChronoField;->checkValidValue(J)J

    .line 356
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->NANO_OF_SECOND:Lorg/threeten/bp/temporal/ChronoField;

    int-to-long v1, p2

    invoke-virtual {v0, v1, v2}, Lorg/threeten/bp/temporal/ChronoField;->checkValidValue(J)J

    .line 357
    const-wide/16 v0, 0xe10

    div-long v0, p0, v0

    long-to-int v3, v0

    .line 358
    .local v3, "hours":I
    mul-int/lit16 v0, v3, 0xe10

    int-to-long v0, v0

    sub-long/2addr p0, v0

    .line 359
    const-wide/16 v0, 0x3c

    div-long v0, p0, v0

    long-to-int v4, v0

    .line 360
    .local v4, "minutes":I
    mul-int/lit8 v0, v4, 0x3c

    int-to-long v0, v0

    sub-long/2addr p0, v0

    .line 361
    long-to-int v0, p0

    invoke-static {v3, v4, v0, p2}, Lorg/threeten/bp/LocalTime;->create(IIII)Lorg/threeten/bp/LocalTime;

    move-result-object v0

    return-object v0
.end method

.method public static parse(Ljava/lang/CharSequence;Lorg/threeten/bp/format/DateTimeFormatter;)Lorg/threeten/bp/LocalTime;
    .registers 3
    .param p0, "text"    # Ljava/lang/CharSequence;
    .param p1, "formatter"    # Lorg/threeten/bp/format/DateTimeFormatter;

    .line 436
    const-string v0, "formatter"

    invoke-static {p1, v0}, Lorg/threeten/bp/jdk8/Jdk8Methods;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 437
    sget-object v0, Lorg/threeten/bp/LocalTime;->FROM:Lorg/threeten/bp/temporal/TemporalQuery;

    invoke-virtual {p1, p0, v0}, Lorg/threeten/bp/format/DateTimeFormatter;->parse(Ljava/lang/CharSequence;Lorg/threeten/bp/temporal/TemporalQuery;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/threeten/bp/LocalTime;

    return-object v0
.end method

.method static readExternal(Ljava/io/DataInput;)Lorg/threeten/bp/LocalTime;
    .registers 6
    .param p0, "in"    # Ljava/io/DataInput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1546
    invoke-interface {p0}, Ljava/io/DataInput;->readByte()B

    move-result v1

    .line 1547
    .local v1, "hour":I
    const/4 v2, 0x0

    .line 1548
    .local v2, "minute":I
    const/4 v3, 0x0

    .line 1549
    .local v3, "second":I
    const/4 v4, 0x0

    .line 1550
    .local v4, "nano":I
    if-gez v1, :cond_c

    .line 1551
    xor-int/lit8 v1, v1, -0x1

    goto :goto_22

    .line 1553
    :cond_c
    invoke-interface {p0}, Ljava/io/DataInput;->readByte()B

    move-result v2

    .line 1554
    if-gez v2, :cond_15

    .line 1555
    xor-int/lit8 v2, v2, -0x1

    goto :goto_22

    .line 1557
    :cond_15
    invoke-interface {p0}, Ljava/io/DataInput;->readByte()B

    move-result v3

    .line 1558
    if-gez v3, :cond_1e

    .line 1559
    xor-int/lit8 v3, v3, -0x1

    goto :goto_22

    .line 1561
    :cond_1e
    invoke-interface {p0}, Ljava/io/DataInput;->readInt()I

    move-result v4

    .line 1565
    :goto_22
    invoke-static {v1, v2, v3, v4}, Lorg/threeten/bp/LocalTime;->of(IIII)Lorg/threeten/bp/LocalTime;

    move-result-object v0

    return-object v0
.end method

.method private readResolve()Ljava/lang/Object;
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    .line 1520
    new-instance v0, Ljava/io/InvalidObjectException;

    const-string v1, "Deserialization via serialization delegate"

    invoke-direct {v0, v1}, Ljava/io/InvalidObjectException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private writeReplace()Ljava/lang/Object;
    .registers 3

    .line 1511
    new-instance v0, Lorg/threeten/bp/Ser;

    const/4 v1, 0x5

    invoke-direct {v0, v1, p0}, Lorg/threeten/bp/Ser;-><init>(BLjava/lang/Object;)V

    return-object v0
.end method


# virtual methods
.method public adjustInto(Lorg/threeten/bp/temporal/Temporal;)Lorg/threeten/bp/temporal/Temporal;
    .registers 5
    .param p1, "temporal"    # Lorg/threeten/bp/temporal/Temporal;

    .line 1243
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->NANO_OF_DAY:Lorg/threeten/bp/temporal/ChronoField;

    invoke-virtual {p0}, Lorg/threeten/bp/LocalTime;->toNanoOfDay()J

    move-result-wide v1

    invoke-interface {p1, v0, v1, v2}, Lorg/threeten/bp/temporal/Temporal;->with(Lorg/threeten/bp/temporal/TemporalField;J)Lorg/threeten/bp/temporal/Temporal;

    move-result-object v0

    return-object v0
.end method

.method public atOffset(Lorg/threeten/bp/ZoneOffset;)Lorg/threeten/bp/OffsetTime;
    .registers 3
    .param p1, "offset"    # Lorg/threeten/bp/ZoneOffset;

    .line 1332
    invoke-static {p0, p1}, Lorg/threeten/bp/OffsetTime;->of(Lorg/threeten/bp/LocalTime;Lorg/threeten/bp/ZoneOffset;)Lorg/threeten/bp/OffsetTime;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .registers 3
    .param p1, "x0"    # Ljava/lang/Object;

    .line 88
    move-object v0, p1

    check-cast v0, Lorg/threeten/bp/LocalTime;

    invoke-virtual {p0, v0}, Lorg/threeten/bp/LocalTime;->compareTo(Lorg/threeten/bp/LocalTime;)I

    move-result v0

    return v0
.end method

.method public compareTo(Lorg/threeten/bp/LocalTime;)I
    .registers 5
    .param p1, "other"    # Lorg/threeten/bp/LocalTime;

    .line 1376
    iget-byte v0, p0, Lorg/threeten/bp/LocalTime;->hour:B

    iget-byte v1, p1, Lorg/threeten/bp/LocalTime;->hour:B

    invoke-static {v0, v1}, Lorg/threeten/bp/jdk8/Jdk8Methods;->compareInts(II)I

    move-result v2

    .line 1377
    .local v2, "cmp":I
    if-nez v2, :cond_26

    .line 1378
    iget-byte v0, p0, Lorg/threeten/bp/LocalTime;->minute:B

    iget-byte v1, p1, Lorg/threeten/bp/LocalTime;->minute:B

    invoke-static {v0, v1}, Lorg/threeten/bp/jdk8/Jdk8Methods;->compareInts(II)I

    move-result v2

    .line 1379
    if-nez v2, :cond_26

    .line 1380
    iget-byte v0, p0, Lorg/threeten/bp/LocalTime;->second:B

    iget-byte v1, p1, Lorg/threeten/bp/LocalTime;->second:B

    invoke-static {v0, v1}, Lorg/threeten/bp/jdk8/Jdk8Methods;->compareInts(II)I

    move-result v2

    .line 1381
    if-nez v2, :cond_26

    .line 1382
    iget v0, p0, Lorg/threeten/bp/LocalTime;->nano:I

    iget v1, p1, Lorg/threeten/bp/LocalTime;->nano:I

    invoke-static {v0, v1}, Lorg/threeten/bp/jdk8/Jdk8Methods;->compareInts(II)I

    move-result v2

    .line 1386
    :cond_26
    return v2
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 5
    .param p1, "obj"    # Ljava/lang/Object;

    .line 1430
    if-ne p0, p1, :cond_4

    .line 1431
    const/4 v0, 0x1

    return v0

    .line 1433
    :cond_4
    instance-of v0, p1, Lorg/threeten/bp/LocalTime;

    if-eqz v0, :cond_27

    .line 1434
    move-object v2, p1

    check-cast v2, Lorg/threeten/bp/LocalTime;

    .line 1435
    .local v2, "other":Lorg/threeten/bp/LocalTime;
    iget-byte v0, p0, Lorg/threeten/bp/LocalTime;->hour:B

    iget-byte v1, v2, Lorg/threeten/bp/LocalTime;->hour:B

    if-ne v0, v1, :cond_25

    iget-byte v0, p0, Lorg/threeten/bp/LocalTime;->minute:B

    iget-byte v1, v2, Lorg/threeten/bp/LocalTime;->minute:B

    if-ne v0, v1, :cond_25

    iget-byte v0, p0, Lorg/threeten/bp/LocalTime;->second:B

    iget-byte v1, v2, Lorg/threeten/bp/LocalTime;->second:B

    if-ne v0, v1, :cond_25

    iget v0, p0, Lorg/threeten/bp/LocalTime;->nano:I

    iget v1, v2, Lorg/threeten/bp/LocalTime;->nano:I

    if-ne v0, v1, :cond_25

    const/4 v0, 0x1

    goto :goto_26

    :cond_25
    const/4 v0, 0x0

    :goto_26
    return v0

    .line 1438
    .end local v2    # "other":Lorg/threeten/bp/LocalTime;
    :cond_27
    const/4 v0, 0x0

    return v0
.end method

.method public format(Lorg/threeten/bp/format/DateTimeFormatter;)Ljava/lang/String;
    .registers 3
    .param p1, "formatter"    # Lorg/threeten/bp/format/DateTimeFormatter;

    .line 1505
    const-string v0, "formatter"

    invoke-static {p1, v0}, Lorg/threeten/bp/jdk8/Jdk8Methods;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 1506
    invoke-virtual {p1, p0}, Lorg/threeten/bp/format/DateTimeFormatter;->format(Lorg/threeten/bp/temporal/TemporalAccessor;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public get(Lorg/threeten/bp/temporal/TemporalField;)I
    .registers 3
    .param p1, "field"    # Lorg/threeten/bp/temporal/TemporalField;

    .line 580
    instance-of v0, p1, Lorg/threeten/bp/temporal/ChronoField;

    if-eqz v0, :cond_9

    .line 581
    invoke-direct {p0, p1}, Lorg/threeten/bp/LocalTime;->get0(Lorg/threeten/bp/temporal/TemporalField;)I

    move-result v0

    return v0

    .line 583
    :cond_9
    invoke-super {p0, p1}, Lorg/threeten/bp/jdk8/DefaultInterfaceTemporalAccessor;->get(Lorg/threeten/bp/temporal/TemporalField;)I

    move-result v0

    return v0
.end method

.method public getHour()I
    .registers 2

    .line 650
    iget-byte v0, p0, Lorg/threeten/bp/LocalTime;->hour:B

    return v0
.end method

.method public getLong(Lorg/threeten/bp/temporal/TemporalField;)J
    .registers 6
    .param p1, "field"    # Lorg/threeten/bp/temporal/TemporalField;

    .line 610
    instance-of v0, p1, Lorg/threeten/bp/temporal/ChronoField;

    if-eqz v0, :cond_1f

    .line 611
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->NANO_OF_DAY:Lorg/threeten/bp/temporal/ChronoField;

    if-ne p1, v0, :cond_d

    .line 612
    invoke-virtual {p0}, Lorg/threeten/bp/LocalTime;->toNanoOfDay()J

    move-result-wide v0

    return-wide v0

    .line 614
    :cond_d
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->MICRO_OF_DAY:Lorg/threeten/bp/temporal/ChronoField;

    if-ne p1, v0, :cond_19

    .line 615
    invoke-virtual {p0}, Lorg/threeten/bp/LocalTime;->toNanoOfDay()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    return-wide v0

    .line 617
    :cond_19
    invoke-direct {p0, p1}, Lorg/threeten/bp/LocalTime;->get0(Lorg/threeten/bp/temporal/TemporalField;)I

    move-result v0

    int-to-long v0, v0

    return-wide v0

    .line 619
    :cond_1f
    invoke-interface {p1, p0}, Lorg/threeten/bp/temporal/TemporalField;->getFrom(Lorg/threeten/bp/temporal/TemporalAccessor;)J

    move-result-wide v0

    return-wide v0
.end method

.method public getNano()I
    .registers 2

    .line 677
    iget v0, p0, Lorg/threeten/bp/LocalTime;->nano:I

    return v0
.end method

.method public getSecond()I
    .registers 2

    .line 668
    iget-byte v0, p0, Lorg/threeten/bp/LocalTime;->second:B

    return v0
.end method

.method public hashCode()I
    .registers 5

    .line 1448
    invoke-virtual {p0}, Lorg/threeten/bp/LocalTime;->toNanoOfDay()J

    move-result-wide v2

    .line 1449
    .local v2, "nod":J
    const/16 v0, 0x20

    ushr-long v0, v2, v0

    xor-long/2addr v0, v2

    long-to-int v0, v0

    return v0
.end method

.method public isSupported(Lorg/threeten/bp/temporal/TemporalField;)Z
    .registers 3
    .param p1, "field"    # Lorg/threeten/bp/temporal/TemporalField;

    .line 513
    instance-of v0, p1, Lorg/threeten/bp/temporal/ChronoField;

    if-eqz v0, :cond_9

    .line 514
    invoke-interface {p1}, Lorg/threeten/bp/temporal/TemporalField;->isTimeBased()Z

    move-result v0

    return v0

    .line 516
    :cond_9
    if-eqz p1, :cond_13

    invoke-interface {p1, p0}, Lorg/threeten/bp/temporal/TemporalField;->isSupportedBy(Lorg/threeten/bp/temporal/TemporalAccessor;)Z

    move-result v0

    if-eqz v0, :cond_13

    const/4 v0, 0x1

    goto :goto_14

    :cond_13
    const/4 v0, 0x0

    :goto_14
    return v0
.end method

.method public minus(JLorg/threeten/bp/temporal/TemporalUnit;)Lorg/threeten/bp/LocalTime;
    .registers 7
    .param p1, "amountToSubtract"    # J
    .param p3, "unit"    # Lorg/threeten/bp/temporal/TemporalUnit;

    .line 1117
    const-wide/high16 v0, -0x8000000000000000L

    cmp-long v0, p1, v0

    if-nez v0, :cond_16

    const-wide v0, 0x7fffffffffffffffL

    invoke-virtual {p0, v0, v1, p3}, Lorg/threeten/bp/LocalTime;->plus(JLorg/threeten/bp/temporal/TemporalUnit;)Lorg/threeten/bp/LocalTime;

    move-result-object v0

    const-wide/16 v1, 0x1

    invoke-virtual {v0, v1, v2, p3}, Lorg/threeten/bp/LocalTime;->plus(JLorg/threeten/bp/temporal/TemporalUnit;)Lorg/threeten/bp/LocalTime;

    move-result-object v0

    goto :goto_1b

    :cond_16
    neg-long v0, p1

    invoke-virtual {p0, v0, v1, p3}, Lorg/threeten/bp/LocalTime;->plus(JLorg/threeten/bp/temporal/TemporalUnit;)Lorg/threeten/bp/LocalTime;

    move-result-object v0

    :goto_1b
    return-object v0
.end method

.method public bridge synthetic minus(JLorg/threeten/bp/temporal/TemporalUnit;)Lorg/threeten/bp/temporal/Temporal;
    .registers 5
    .param p1, "x0"    # J
    .param p3, "x1"    # Lorg/threeten/bp/temporal/TemporalUnit;

    .line 88
    invoke-virtual {p0, p1, p2, p3}, Lorg/threeten/bp/LocalTime;->minus(JLorg/threeten/bp/temporal/TemporalUnit;)Lorg/threeten/bp/LocalTime;

    move-result-object v0

    return-object v0
.end method

.method public plus(JLorg/threeten/bp/temporal/TemporalUnit;)Lorg/threeten/bp/LocalTime;
    .registers 9
    .param p1, "amountToAdd"    # J
    .param p3, "unit"    # Lorg/threeten/bp/temporal/TemporalUnit;

    .line 963
    instance-of v0, p3, Lorg/threeten/bp/temporal/ChronoUnit;

    if-eqz v0, :cond_69

    .line 964
    move-object v4, p3

    check-cast v4, Lorg/threeten/bp/temporal/ChronoUnit;

    .line 965
    .local v4, "f":Lorg/threeten/bp/temporal/ChronoUnit;
    sget-object v0, Lorg/threeten/bp/LocalTime$2;->$SwitchMap$org$threeten$bp$temporal$ChronoUnit:[I

    invoke-virtual {v4}, Lorg/threeten/bp/temporal/ChronoUnit;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_70

    goto :goto_50

    .line 966
    :pswitch_13
    invoke-virtual {p0, p1, p2}, Lorg/threeten/bp/LocalTime;->plusNanos(J)Lorg/threeten/bp/LocalTime;

    move-result-object v0

    return-object v0

    .line 967
    :pswitch_18
    const-wide v0, 0x141dd76000L

    rem-long v0, p1, v0

    const-wide/16 v2, 0x3e8

    mul-long/2addr v0, v2

    invoke-virtual {p0, v0, v1}, Lorg/threeten/bp/LocalTime;->plusNanos(J)Lorg/threeten/bp/LocalTime;

    move-result-object v0

    return-object v0

    .line 968
    :pswitch_27
    const-wide/32 v0, 0x5265c00

    rem-long v0, p1, v0

    const-wide/32 v2, 0xf4240

    mul-long/2addr v0, v2

    invoke-virtual {p0, v0, v1}, Lorg/threeten/bp/LocalTime;->plusNanos(J)Lorg/threeten/bp/LocalTime;

    move-result-object v0

    return-object v0

    .line 969
    :pswitch_35
    invoke-virtual {p0, p1, p2}, Lorg/threeten/bp/LocalTime;->plusSeconds(J)Lorg/threeten/bp/LocalTime;

    move-result-object v0

    return-object v0

    .line 970
    :pswitch_3a
    invoke-virtual {p0, p1, p2}, Lorg/threeten/bp/LocalTime;->plusMinutes(J)Lorg/threeten/bp/LocalTime;

    move-result-object v0

    return-object v0

    .line 971
    :pswitch_3f
    invoke-virtual {p0, p1, p2}, Lorg/threeten/bp/LocalTime;->plusHours(J)Lorg/threeten/bp/LocalTime;

    move-result-object v0

    return-object v0

    .line 972
    :pswitch_44
    const-wide/16 v0, 0x2

    rem-long v0, p1, v0

    const-wide/16 v2, 0xc

    mul-long/2addr v0, v2

    invoke-virtual {p0, v0, v1}, Lorg/threeten/bp/LocalTime;->plusHours(J)Lorg/threeten/bp/LocalTime;

    move-result-object v0

    return-object v0

    .line 974
    :goto_50
    new-instance v0, Lorg/threeten/bp/temporal/UnsupportedTemporalTypeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unsupported unit: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/threeten/bp/temporal/UnsupportedTemporalTypeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 976
    .end local v4    # "f":Lorg/threeten/bp/temporal/ChronoUnit;
    :cond_69
    invoke-interface {p3, p0, p1, p2}, Lorg/threeten/bp/temporal/TemporalUnit;->addTo(Lorg/threeten/bp/temporal/Temporal;J)Lorg/threeten/bp/temporal/Temporal;

    move-result-object v0

    check-cast v0, Lorg/threeten/bp/LocalTime;

    return-object v0

    :pswitch_data_70
    .packed-switch 0x1
        :pswitch_13
        :pswitch_18
        :pswitch_27
        :pswitch_35
        :pswitch_3a
        :pswitch_3f
        :pswitch_44
    .end packed-switch
.end method

.method public bridge synthetic plus(JLorg/threeten/bp/temporal/TemporalUnit;)Lorg/threeten/bp/temporal/Temporal;
    .registers 5
    .param p1, "x0"    # J
    .param p3, "x1"    # Lorg/threeten/bp/temporal/TemporalUnit;

    .line 88
    invoke-virtual {p0, p1, p2, p3}, Lorg/threeten/bp/LocalTime;->plus(JLorg/threeten/bp/temporal/TemporalUnit;)Lorg/threeten/bp/LocalTime;

    move-result-object v0

    return-object v0
.end method

.method public plusHours(J)Lorg/threeten/bp/LocalTime;
    .registers 7
    .param p1, "hoursToAdd"    # J

    .line 992
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-nez v0, :cond_7

    .line 993
    return-object p0

    .line 995
    :cond_7
    const-wide/16 v0, 0x18

    rem-long v0, p1, v0

    long-to-int v0, v0

    iget-byte v1, p0, Lorg/threeten/bp/LocalTime;->hour:B

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x18

    rem-int/lit8 v3, v0, 0x18

    .line 996
    .local v3, "newHour":I
    iget-byte v0, p0, Lorg/threeten/bp/LocalTime;->minute:B

    iget-byte v1, p0, Lorg/threeten/bp/LocalTime;->second:B

    iget v2, p0, Lorg/threeten/bp/LocalTime;->nano:I

    invoke-static {v3, v0, v1, v2}, Lorg/threeten/bp/LocalTime;->create(IIII)Lorg/threeten/bp/LocalTime;

    move-result-object v0

    return-object v0
.end method

.method public plusMinutes(J)Lorg/threeten/bp/LocalTime;
    .registers 9
    .param p1, "minutesToAdd"    # J

    .line 1011
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-nez v0, :cond_7

    .line 1012
    return-object p0

    .line 1014
    :cond_7
    iget-byte v0, p0, Lorg/threeten/bp/LocalTime;->hour:B

    mul-int/lit8 v0, v0, 0x3c

    iget-byte v1, p0, Lorg/threeten/bp/LocalTime;->minute:B

    add-int v2, v0, v1

    .line 1015
    .local v2, "mofd":I
    const-wide/16 v0, 0x5a0

    rem-long v0, p1, v0

    long-to-int v0, v0

    add-int/2addr v0, v2

    add-int/lit16 v0, v0, 0x5a0

    rem-int/lit16 v3, v0, 0x5a0

    .line 1016
    .local v3, "newMofd":I
    if-ne v2, v3, :cond_1c

    .line 1017
    return-object p0

    .line 1019
    :cond_1c
    div-int/lit8 v4, v3, 0x3c

    .line 1020
    .local v4, "newHour":I
    rem-int/lit8 v5, v3, 0x3c

    .line 1021
    .local v5, "newMinute":I
    iget-byte v0, p0, Lorg/threeten/bp/LocalTime;->second:B

    iget v1, p0, Lorg/threeten/bp/LocalTime;->nano:I

    invoke-static {v4, v5, v0, v1}, Lorg/threeten/bp/LocalTime;->create(IIII)Lorg/threeten/bp/LocalTime;

    move-result-object v0

    return-object v0
.end method

.method public plusNanos(J)Lorg/threeten/bp/LocalTime;
    .registers 15
    .param p1, "nanosToAdd"    # J

    .line 1063
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-nez v0, :cond_7

    .line 1064
    return-object p0

    .line 1066
    :cond_7
    invoke-virtual {p0}, Lorg/threeten/bp/LocalTime;->toNanoOfDay()J

    move-result-wide v4

    .line 1067
    .local v4, "nofd":J
    const-wide v0, 0x4e94914f0000L

    rem-long v0, p1, v0

    add-long/2addr v0, v4

    const-wide v2, 0x4e94914f0000L

    add-long/2addr v0, v2

    const-wide v2, 0x4e94914f0000L

    rem-long v6, v0, v2

    .line 1068
    .local v6, "newNofd":J
    cmp-long v0, v4, v6

    if-nez v0, :cond_25

    .line 1069
    return-object p0

    .line 1071
    :cond_25
    const-wide v0, 0x34630b8a000L

    div-long v0, v6, v0

    long-to-int v8, v0

    .line 1072
    .local v8, "newHour":I
    const-wide v0, 0xdf8475800L

    div-long v0, v6, v0

    const-wide/16 v2, 0x3c

    rem-long/2addr v0, v2

    long-to-int v9, v0

    .line 1073
    .local v9, "newMinute":I
    const-wide/32 v0, 0x3b9aca00

    div-long v0, v6, v0

    const-wide/16 v2, 0x3c

    rem-long/2addr v0, v2

    long-to-int v10, v0

    .line 1074
    .local v10, "newSecond":I
    const-wide/32 v0, 0x3b9aca00

    rem-long v0, v6, v0

    long-to-int v11, v0

    .line 1075
    .local v11, "newNano":I
    invoke-static {v8, v9, v10, v11}, Lorg/threeten/bp/LocalTime;->create(IIII)Lorg/threeten/bp/LocalTime;

    move-result-object v0

    return-object v0
.end method

.method public plusSeconds(J)Lorg/threeten/bp/LocalTime;
    .registers 11
    .param p1, "secondstoAdd"    # J

    .line 1036
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-nez v0, :cond_7

    .line 1037
    return-object p0

    .line 1039
    :cond_7
    iget-byte v0, p0, Lorg/threeten/bp/LocalTime;->hour:B

    mul-int/lit16 v0, v0, 0xe10

    iget-byte v1, p0, Lorg/threeten/bp/LocalTime;->minute:B

    mul-int/lit8 v1, v1, 0x3c

    add-int/2addr v0, v1

    iget-byte v1, p0, Lorg/threeten/bp/LocalTime;->second:B

    add-int v3, v0, v1

    .line 1041
    .local v3, "sofd":I
    const-wide/32 v0, 0x15180

    rem-long v0, p1, v0

    long-to-int v0, v0

    add-int/2addr v0, v3

    const v1, 0x15180

    add-int/2addr v0, v1

    const v1, 0x15180

    rem-int v4, v0, v1

    .line 1042
    .local v4, "newSofd":I
    if-ne v3, v4, :cond_27

    .line 1043
    return-object p0

    .line 1045
    :cond_27
    div-int/lit16 v5, v4, 0xe10

    .line 1046
    .local v5, "newHour":I
    div-int/lit8 v0, v4, 0x3c

    rem-int/lit8 v6, v0, 0x3c

    .line 1047
    .local v6, "newMinute":I
    rem-int/lit8 v7, v4, 0x3c

    .line 1048
    .local v7, "newSecond":I
    iget v0, p0, Lorg/threeten/bp/LocalTime;->nano:I

    invoke-static {v5, v6, v7, v0}, Lorg/threeten/bp/LocalTime;->create(IIII)Lorg/threeten/bp/LocalTime;

    move-result-object v0

    return-object v0
.end method

.method public query(Lorg/threeten/bp/temporal/TemporalQuery;)Ljava/lang/Object;
    .registers 3
    .param p1, "query"    # Lorg/threeten/bp/temporal/TemporalQuery;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:Ljava/lang/Object;>(Lorg/threeten/bp/temporal/TemporalQuery<TR;>;)TR;"
        }
    .end annotation

    .line 1203
    invoke-static {}, Lorg/threeten/bp/temporal/TemporalQueries;->precision()Lorg/threeten/bp/temporal/TemporalQuery;

    move-result-object v0

    if-ne p1, v0, :cond_9

    .line 1204
    sget-object v0, Lorg/threeten/bp/temporal/ChronoUnit;->NANOS:Lorg/threeten/bp/temporal/ChronoUnit;

    return-object v0

    .line 1205
    :cond_9
    invoke-static {}, Lorg/threeten/bp/temporal/TemporalQueries;->localTime()Lorg/threeten/bp/temporal/TemporalQuery;

    move-result-object v0

    if-ne p1, v0, :cond_10

    .line 1206
    return-object p0

    .line 1209
    :cond_10
    invoke-static {}, Lorg/threeten/bp/temporal/TemporalQueries;->chronology()Lorg/threeten/bp/temporal/TemporalQuery;

    move-result-object v0

    if-eq p1, v0, :cond_2e

    invoke-static {}, Lorg/threeten/bp/temporal/TemporalQueries;->zoneId()Lorg/threeten/bp/temporal/TemporalQuery;

    move-result-object v0

    if-eq p1, v0, :cond_2e

    invoke-static {}, Lorg/threeten/bp/temporal/TemporalQueries;->zone()Lorg/threeten/bp/temporal/TemporalQuery;

    move-result-object v0

    if-eq p1, v0, :cond_2e

    invoke-static {}, Lorg/threeten/bp/temporal/TemporalQueries;->offset()Lorg/threeten/bp/temporal/TemporalQuery;

    move-result-object v0

    if-eq p1, v0, :cond_2e

    invoke-static {}, Lorg/threeten/bp/temporal/TemporalQueries;->localDate()Lorg/threeten/bp/temporal/TemporalQuery;

    move-result-object v0

    if-ne p1, v0, :cond_30

    .line 1212
    :cond_2e
    const/4 v0, 0x0

    return-object v0

    .line 1214
    :cond_30
    invoke-interface {p1, p0}, Lorg/threeten/bp/temporal/TemporalQuery;->queryFrom(Lorg/threeten/bp/temporal/TemporalAccessor;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public range(Lorg/threeten/bp/temporal/TemporalField;)Lorg/threeten/bp/temporal/ValueRange;
    .registers 3
    .param p1, "field"    # Lorg/threeten/bp/temporal/TemporalField;

    .line 551
    invoke-super {p0, p1}, Lorg/threeten/bp/jdk8/DefaultInterfaceTemporalAccessor;->range(Lorg/threeten/bp/temporal/TemporalField;)Lorg/threeten/bp/temporal/ValueRange;

    move-result-object v0

    return-object v0
.end method

.method public toNanoOfDay()J
    .registers 7

    .line 1356
    iget-byte v0, p0, Lorg/threeten/bp/LocalTime;->hour:B

    int-to-long v0, v0

    const-wide v2, 0x34630b8a000L

    mul-long v4, v0, v2

    .line 1357
    .local v4, "total":J
    iget-byte v0, p0, Lorg/threeten/bp/LocalTime;->minute:B

    int-to-long v0, v0

    const-wide v2, 0xdf8475800L

    mul-long/2addr v0, v2

    add-long/2addr v4, v0

    .line 1358
    iget-byte v0, p0, Lorg/threeten/bp/LocalTime;->second:B

    int-to-long v0, v0

    const-wide/32 v2, 0x3b9aca00

    mul-long/2addr v0, v2

    add-long/2addr v4, v0

    .line 1359
    iget v0, p0, Lorg/threeten/bp/LocalTime;->nano:I

    int-to-long v0, v0

    add-long/2addr v4, v0

    .line 1360
    return-wide v4
.end method

.method public toSecondOfDay()I
    .registers 4

    .line 1343
    iget-byte v0, p0, Lorg/threeten/bp/LocalTime;->hour:B

    mul-int/lit16 v2, v0, 0xe10

    .line 1344
    .local v2, "total":I
    iget-byte v0, p0, Lorg/threeten/bp/LocalTime;->minute:B

    mul-int/lit8 v0, v0, 0x3c

    add-int/2addr v2, v0

    .line 1345
    iget-byte v0, p0, Lorg/threeten/bp/LocalTime;->second:B

    add-int/2addr v2, v0

    .line 1346
    return v2
.end method

.method public toString()Ljava/lang/String;
    .registers 8

    .line 1471
    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v0, 0x12

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 1472
    .local v2, "buf":Ljava/lang/StringBuilder;
    iget-byte v3, p0, Lorg/threeten/bp/LocalTime;->hour:B

    .line 1473
    .local v3, "hourValue":I
    iget-byte v4, p0, Lorg/threeten/bp/LocalTime;->minute:B

    .line 1474
    .local v4, "minuteValue":I
    iget-byte v5, p0, Lorg/threeten/bp/LocalTime;->second:B

    .line 1475
    .local v5, "secondValue":I
    iget v6, p0, Lorg/threeten/bp/LocalTime;->nano:I

    .line 1476
    .local v6, "nanoValue":I
    const/16 v0, 0xa

    if-ge v3, v0, :cond_16

    const-string v0, "0"

    goto :goto_18

    :cond_16
    const-string v0, ""

    :goto_18
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0xa

    if-ge v4, v1, :cond_27

    const-string v1, ":0"

    goto :goto_29

    :cond_27
    const-string v1, ":"

    :goto_29
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 1478
    if-gtz v5, :cond_34

    if-lez v6, :cond_8d

    .line 1479
    :cond_34
    const/16 v0, 0xa

    if-ge v5, v0, :cond_3b

    const-string v0, ":0"

    goto :goto_3d

    :cond_3b
    const-string v0, ":"

    :goto_3d
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 1480
    if-lez v6, :cond_8d

    .line 1481
    const/16 v0, 0x2e

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1482
    const v0, 0xf4240

    rem-int v0, v6, v0

    if-nez v0, :cond_66

    .line 1483
    const v0, 0xf4240

    div-int v0, v6, v0

    add-int/lit16 v0, v0, 0x3e8

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_8d

    .line 1484
    :cond_66
    rem-int/lit16 v0, v6, 0x3e8

    if-nez v0, :cond_7d

    .line 1485
    div-int/lit16 v0, v6, 0x3e8

    const v1, 0xf4240

    add-int/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_8d

    .line 1487
    :cond_7d
    const v0, 0x3b9aca00

    add-int/2addr v0, v6

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1491
    :cond_8d
    :goto_8d
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public with(Lorg/threeten/bp/temporal/TemporalAdjuster;)Lorg/threeten/bp/LocalTime;
    .registers 3
    .param p1, "adjuster"    # Lorg/threeten/bp/temporal/TemporalAdjuster;

    .line 705
    instance-of v0, p1, Lorg/threeten/bp/LocalTime;

    if-eqz v0, :cond_8

    .line 706
    move-object v0, p1

    check-cast v0, Lorg/threeten/bp/LocalTime;

    return-object v0

    .line 708
    :cond_8
    invoke-interface {p1, p0}, Lorg/threeten/bp/temporal/TemporalAdjuster;->adjustInto(Lorg/threeten/bp/temporal/Temporal;)Lorg/threeten/bp/temporal/Temporal;

    move-result-object v0

    check-cast v0, Lorg/threeten/bp/LocalTime;

    return-object v0
.end method

.method public with(Lorg/threeten/bp/temporal/TemporalField;J)Lorg/threeten/bp/LocalTime;
    .registers 9
    .param p1, "field"    # Lorg/threeten/bp/temporal/TemporalField;
    .param p2, "newValue"    # J

    .line 794
    instance-of v0, p1, Lorg/threeten/bp/temporal/ChronoField;

    if-eqz v0, :cond_cb

    .line 795
    move-object v4, p1

    check-cast v4, Lorg/threeten/bp/temporal/ChronoField;

    .line 796
    .local v4, "f":Lorg/threeten/bp/temporal/ChronoField;
    invoke-virtual {v4, p2, p3}, Lorg/threeten/bp/temporal/ChronoField;->checkValidValue(J)J

    .line 797
    sget-object v0, Lorg/threeten/bp/LocalTime$2;->$SwitchMap$org$threeten$bp$temporal$ChronoField:[I

    invoke-virtual {v4}, Lorg/threeten/bp/temporal/ChronoField;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_d2

    goto/16 :goto_b2

    .line 798
    :pswitch_17
    long-to-int v0, p2

    invoke-virtual {p0, v0}, Lorg/threeten/bp/LocalTime;->withNano(I)Lorg/threeten/bp/LocalTime;

    move-result-object v0

    return-object v0

    .line 799
    :pswitch_1d
    invoke-static {p2, p3}, Lorg/threeten/bp/LocalTime;->ofNanoOfDay(J)Lorg/threeten/bp/LocalTime;

    move-result-object v0

    return-object v0

    .line 800
    :pswitch_22
    long-to-int v0, p2

    mul-int/lit16 v0, v0, 0x3e8

    invoke-virtual {p0, v0}, Lorg/threeten/bp/LocalTime;->withNano(I)Lorg/threeten/bp/LocalTime;

    move-result-object v0

    return-object v0

    .line 801
    :pswitch_2a
    const-wide/16 v0, 0x3e8

    mul-long/2addr v0, p2

    invoke-static {v0, v1}, Lorg/threeten/bp/LocalTime;->ofNanoOfDay(J)Lorg/threeten/bp/LocalTime;

    move-result-object v0

    return-object v0

    .line 802
    :pswitch_32
    long-to-int v0, p2

    const v1, 0xf4240

    mul-int/2addr v0, v1

    invoke-virtual {p0, v0}, Lorg/threeten/bp/LocalTime;->withNano(I)Lorg/threeten/bp/LocalTime;

    move-result-object v0

    return-object v0

    .line 803
    :pswitch_3c
    const-wide/32 v0, 0xf4240

    mul-long/2addr v0, p2

    invoke-static {v0, v1}, Lorg/threeten/bp/LocalTime;->ofNanoOfDay(J)Lorg/threeten/bp/LocalTime;

    move-result-object v0

    return-object v0

    .line 804
    :pswitch_45
    long-to-int v0, p2

    invoke-virtual {p0, v0}, Lorg/threeten/bp/LocalTime;->withSecond(I)Lorg/threeten/bp/LocalTime;

    move-result-object v0

    return-object v0

    .line 805
    :pswitch_4b
    invoke-virtual {p0}, Lorg/threeten/bp/LocalTime;->toSecondOfDay()I

    move-result v0

    int-to-long v0, v0

    sub-long v0, p2, v0

    invoke-virtual {p0, v0, v1}, Lorg/threeten/bp/LocalTime;->plusSeconds(J)Lorg/threeten/bp/LocalTime;

    move-result-object v0

    return-object v0

    .line 806
    :pswitch_57
    long-to-int v0, p2

    invoke-virtual {p0, v0}, Lorg/threeten/bp/LocalTime;->withMinute(I)Lorg/threeten/bp/LocalTime;

    move-result-object v0

    return-object v0

    .line 807
    :pswitch_5d
    iget-byte v0, p0, Lorg/threeten/bp/LocalTime;->hour:B

    mul-int/lit8 v0, v0, 0x3c

    iget-byte v1, p0, Lorg/threeten/bp/LocalTime;->minute:B

    add-int/2addr v0, v1

    int-to-long v0, v0

    sub-long v0, p2, v0

    invoke-virtual {p0, v0, v1}, Lorg/threeten/bp/LocalTime;->plusMinutes(J)Lorg/threeten/bp/LocalTime;

    move-result-object v0

    return-object v0

    .line 808
    :pswitch_6c
    iget-byte v0, p0, Lorg/threeten/bp/LocalTime;->hour:B

    rem-int/lit8 v0, v0, 0xc

    int-to-long v0, v0

    sub-long v0, p2, v0

    invoke-virtual {p0, v0, v1}, Lorg/threeten/bp/LocalTime;->plusHours(J)Lorg/threeten/bp/LocalTime;

    move-result-object v0

    return-object v0

    .line 809
    :pswitch_78
    const-wide/16 v0, 0xc

    cmp-long v0, p2, v0

    if-nez v0, :cond_81

    const-wide/16 v0, 0x0

    goto :goto_82

    :cond_81
    move-wide v0, p2

    :goto_82
    iget-byte v2, p0, Lorg/threeten/bp/LocalTime;->hour:B

    rem-int/lit8 v2, v2, 0xc

    int-to-long v2, v2

    sub-long/2addr v0, v2

    invoke-virtual {p0, v0, v1}, Lorg/threeten/bp/LocalTime;->plusHours(J)Lorg/threeten/bp/LocalTime;

    move-result-object v0

    return-object v0

    .line 810
    :pswitch_8d
    long-to-int v0, p2

    invoke-virtual {p0, v0}, Lorg/threeten/bp/LocalTime;->withHour(I)Lorg/threeten/bp/LocalTime;

    move-result-object v0

    return-object v0

    .line 811
    :pswitch_93
    const-wide/16 v0, 0x18

    cmp-long v0, p2, v0

    if-nez v0, :cond_9c

    const-wide/16 v0, 0x0

    goto :goto_9d

    :cond_9c
    move-wide v0, p2

    :goto_9d
    long-to-int v0, v0

    invoke-virtual {p0, v0}, Lorg/threeten/bp/LocalTime;->withHour(I)Lorg/threeten/bp/LocalTime;

    move-result-object v0

    return-object v0

    .line 812
    :pswitch_a3
    iget-byte v0, p0, Lorg/threeten/bp/LocalTime;->hour:B

    div-int/lit8 v0, v0, 0xc

    int-to-long v0, v0

    sub-long v0, p2, v0

    const-wide/16 v2, 0xc

    mul-long/2addr v0, v2

    invoke-virtual {p0, v0, v1}, Lorg/threeten/bp/LocalTime;->plusHours(J)Lorg/threeten/bp/LocalTime;

    move-result-object v0

    return-object v0

    .line 814
    :goto_b2
    new-instance v0, Lorg/threeten/bp/temporal/UnsupportedTemporalTypeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unsupported field: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/threeten/bp/temporal/UnsupportedTemporalTypeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 816
    .end local v4    # "f":Lorg/threeten/bp/temporal/ChronoField;
    :cond_cb
    invoke-interface {p1, p0, p2, p3}, Lorg/threeten/bp/temporal/TemporalField;->adjustInto(Lorg/threeten/bp/temporal/Temporal;J)Lorg/threeten/bp/temporal/Temporal;

    move-result-object v0

    check-cast v0, Lorg/threeten/bp/LocalTime;

    return-object v0

    :pswitch_data_d2
    .packed-switch 0x1
        :pswitch_17
        :pswitch_1d
        :pswitch_22
        :pswitch_2a
        :pswitch_32
        :pswitch_3c
        :pswitch_45
        :pswitch_4b
        :pswitch_57
        :pswitch_5d
        :pswitch_6c
        :pswitch_78
        :pswitch_8d
        :pswitch_93
        :pswitch_a3
    .end packed-switch
.end method

.method public bridge synthetic with(Lorg/threeten/bp/temporal/TemporalAdjuster;)Lorg/threeten/bp/temporal/Temporal;
    .registers 3
    .param p1, "x0"    # Lorg/threeten/bp/temporal/TemporalAdjuster;

    .line 88
    invoke-virtual {p0, p1}, Lorg/threeten/bp/LocalTime;->with(Lorg/threeten/bp/temporal/TemporalAdjuster;)Lorg/threeten/bp/LocalTime;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic with(Lorg/threeten/bp/temporal/TemporalField;J)Lorg/threeten/bp/temporal/Temporal;
    .registers 5
    .param p1, "x0"    # Lorg/threeten/bp/temporal/TemporalField;
    .param p2, "x1"    # J

    .line 88
    invoke-virtual {p0, p1, p2, p3}, Lorg/threeten/bp/LocalTime;->with(Lorg/threeten/bp/temporal/TemporalField;J)Lorg/threeten/bp/LocalTime;

    move-result-object v0

    return-object v0
.end method

.method public withHour(I)Lorg/threeten/bp/LocalTime;
    .registers 5
    .param p1, "hour"    # I

    .line 830
    iget-byte v0, p0, Lorg/threeten/bp/LocalTime;->hour:B

    if-ne v0, p1, :cond_5

    .line 831
    return-object p0

    .line 833
    :cond_5
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->HOUR_OF_DAY:Lorg/threeten/bp/temporal/ChronoField;

    int-to-long v1, p1

    invoke-virtual {v0, v1, v2}, Lorg/threeten/bp/temporal/ChronoField;->checkValidValue(J)J

    .line 834
    iget-byte v0, p0, Lorg/threeten/bp/LocalTime;->minute:B

    iget-byte v1, p0, Lorg/threeten/bp/LocalTime;->second:B

    iget v2, p0, Lorg/threeten/bp/LocalTime;->nano:I

    invoke-static {p1, v0, v1, v2}, Lorg/threeten/bp/LocalTime;->create(IIII)Lorg/threeten/bp/LocalTime;

    move-result-object v0

    return-object v0
.end method

.method public withMinute(I)Lorg/threeten/bp/LocalTime;
    .registers 5
    .param p1, "minute"    # I

    .line 847
    iget-byte v0, p0, Lorg/threeten/bp/LocalTime;->minute:B

    if-ne v0, p1, :cond_5

    .line 848
    return-object p0

    .line 850
    :cond_5
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->MINUTE_OF_HOUR:Lorg/threeten/bp/temporal/ChronoField;

    int-to-long v1, p1

    invoke-virtual {v0, v1, v2}, Lorg/threeten/bp/temporal/ChronoField;->checkValidValue(J)J

    .line 851
    iget-byte v0, p0, Lorg/threeten/bp/LocalTime;->hour:B

    iget-byte v1, p0, Lorg/threeten/bp/LocalTime;->second:B

    iget v2, p0, Lorg/threeten/bp/LocalTime;->nano:I

    invoke-static {v0, p1, v1, v2}, Lorg/threeten/bp/LocalTime;->create(IIII)Lorg/threeten/bp/LocalTime;

    move-result-object v0

    return-object v0
.end method

.method public withNano(I)Lorg/threeten/bp/LocalTime;
    .registers 5
    .param p1, "nanoOfSecond"    # I

    .line 881
    iget v0, p0, Lorg/threeten/bp/LocalTime;->nano:I

    if-ne v0, p1, :cond_5

    .line 882
    return-object p0

    .line 884
    :cond_5
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->NANO_OF_SECOND:Lorg/threeten/bp/temporal/ChronoField;

    int-to-long v1, p1

    invoke-virtual {v0, v1, v2}, Lorg/threeten/bp/temporal/ChronoField;->checkValidValue(J)J

    .line 885
    iget-byte v0, p0, Lorg/threeten/bp/LocalTime;->hour:B

    iget-byte v1, p0, Lorg/threeten/bp/LocalTime;->minute:B

    iget-byte v2, p0, Lorg/threeten/bp/LocalTime;->second:B

    invoke-static {v0, v1, v2, p1}, Lorg/threeten/bp/LocalTime;->create(IIII)Lorg/threeten/bp/LocalTime;

    move-result-object v0

    return-object v0
.end method

.method public withSecond(I)Lorg/threeten/bp/LocalTime;
    .registers 5
    .param p1, "second"    # I

    .line 864
    iget-byte v0, p0, Lorg/threeten/bp/LocalTime;->second:B

    if-ne v0, p1, :cond_5

    .line 865
    return-object p0

    .line 867
    :cond_5
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->SECOND_OF_MINUTE:Lorg/threeten/bp/temporal/ChronoField;

    int-to-long v1, p1

    invoke-virtual {v0, v1, v2}, Lorg/threeten/bp/temporal/ChronoField;->checkValidValue(J)J

    .line 868
    iget-byte v0, p0, Lorg/threeten/bp/LocalTime;->hour:B

    iget-byte v1, p0, Lorg/threeten/bp/LocalTime;->minute:B

    iget v2, p0, Lorg/threeten/bp/LocalTime;->nano:I

    invoke-static {v0, v1, p1, v2}, Lorg/threeten/bp/LocalTime;->create(IIII)Lorg/threeten/bp/LocalTime;

    move-result-object v0

    return-object v0
.end method

.method writeExternal(Ljava/io/DataOutput;)V
    .registers 4
    .param p1, "out"    # Ljava/io/DataOutput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1524
    iget v0, p0, Lorg/threeten/bp/LocalTime;->nano:I

    if-nez v0, :cond_33

    .line 1525
    iget-byte v0, p0, Lorg/threeten/bp/LocalTime;->second:B

    if-nez v0, :cond_21

    .line 1526
    iget-byte v0, p0, Lorg/threeten/bp/LocalTime;->minute:B

    if-nez v0, :cond_14

    .line 1527
    iget-byte v0, p0, Lorg/threeten/bp/LocalTime;->hour:B

    xor-int/lit8 v0, v0, -0x1

    invoke-interface {p1, v0}, Ljava/io/DataOutput;->writeByte(I)V

    goto :goto_47

    .line 1529
    :cond_14
    iget-byte v0, p0, Lorg/threeten/bp/LocalTime;->hour:B

    invoke-interface {p1, v0}, Ljava/io/DataOutput;->writeByte(I)V

    .line 1530
    iget-byte v0, p0, Lorg/threeten/bp/LocalTime;->minute:B

    xor-int/lit8 v0, v0, -0x1

    invoke-interface {p1, v0}, Ljava/io/DataOutput;->writeByte(I)V

    goto :goto_47

    .line 1533
    :cond_21
    iget-byte v0, p0, Lorg/threeten/bp/LocalTime;->hour:B

    invoke-interface {p1, v0}, Ljava/io/DataOutput;->writeByte(I)V

    .line 1534
    iget-byte v0, p0, Lorg/threeten/bp/LocalTime;->minute:B

    invoke-interface {p1, v0}, Ljava/io/DataOutput;->writeByte(I)V

    .line 1535
    iget-byte v0, p0, Lorg/threeten/bp/LocalTime;->second:B

    xor-int/lit8 v0, v0, -0x1

    invoke-interface {p1, v0}, Ljava/io/DataOutput;->writeByte(I)V

    goto :goto_47

    .line 1538
    :cond_33
    iget-byte v0, p0, Lorg/threeten/bp/LocalTime;->hour:B

    invoke-interface {p1, v0}, Ljava/io/DataOutput;->writeByte(I)V

    .line 1539
    iget-byte v0, p0, Lorg/threeten/bp/LocalTime;->minute:B

    invoke-interface {p1, v0}, Ljava/io/DataOutput;->writeByte(I)V

    .line 1540
    iget-byte v0, p0, Lorg/threeten/bp/LocalTime;->second:B

    invoke-interface {p1, v0}, Ljava/io/DataOutput;->writeByte(I)V

    .line 1541
    iget v0, p0, Lorg/threeten/bp/LocalTime;->nano:I

    invoke-interface {p1, v0}, Ljava/io/DataOutput;->writeInt(I)V

    .line 1543
    :goto_47
    return-void
.end method

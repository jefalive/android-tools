.class public interface abstract Lcom/google/android/gms/ads/internal/client/zzm;
.super Ljava/lang/Object;


# virtual methods
.method public abstract createAdLoaderBuilder(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/internal/zzew;Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;)Lcom/google/android/gms/ads/internal/client/zzs;
.end method

.method public abstract createAdOverlay(Landroid/app/Activity;)Lcom/google/android/gms/internal/zzfv;
.end method

.method public abstract createBannerAdManager(Landroid/content/Context;Lcom/google/android/gms/ads/internal/client/AdSizeParcel;Ljava/lang/String;Lcom/google/android/gms/internal/zzew;Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;)Lcom/google/android/gms/ads/internal/client/zzu;
.end method

.method public abstract createInAppPurchaseManager(Landroid/app/Activity;)Lcom/google/android/gms/internal/zzge;
.end method

.method public abstract createInterstitialAdManager(Landroid/content/Context;Lcom/google/android/gms/ads/internal/client/AdSizeParcel;Ljava/lang/String;Lcom/google/android/gms/internal/zzew;Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;)Lcom/google/android/gms/ads/internal/client/zzu;
.end method

.method public abstract createNativeAdViewDelegate(Landroid/widget/FrameLayout;Landroid/widget/FrameLayout;)Lcom/google/android/gms/internal/zzcj;
.end method

.class public Lcom/itau/empresas/feature/login/LoginFingerPrint;
.super Ljava/lang/Object;
.source "LoginFingerPrint.java"


# instance fields
.field private final application:Lcom/itau/empresas/CustomApplication;

.field private fingerprintDialog:Lcom/itau/empresas/ui/dialog/FingerprintDialog;

.field private final fingerprintManager:Lbr/com/itau/sdk/android/fingerprintcore/FingerprintManager;

.field private logarComTeclado:Z

.field private final loginActivity:Lcom/itau/empresas/feature/login/LoginActivity;


# direct methods
.method public constructor <init>(Lcom/itau/empresas/feature/login/LoginActivity;Lcom/itau/empresas/CustomApplication;)V
    .registers 4
    .param p1, "loginActivity"    # Lcom/itau/empresas/feature/login/LoginActivity;
    .param p2, "customApplication"    # Lcom/itau/empresas/CustomApplication;

    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    iput-object p1, p0, Lcom/itau/empresas/feature/login/LoginFingerPrint;->loginActivity:Lcom/itau/empresas/feature/login/LoginActivity;

    .line 41
    iput-object p2, p0, Lcom/itau/empresas/feature/login/LoginFingerPrint;->application:Lcom/itau/empresas/CustomApplication;

    .line 42
    invoke-static {p1}, Lcom/itau/empresas/feature/chaveacesso/FingerPrintManagerFactory;->criaFingerPrintManager(Landroid/content/Context;)Lbr/com/itau/sdk/android/fingerprintcore/FingerprintManager;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/login/LoginFingerPrint;->fingerprintManager:Lbr/com/itau/sdk/android/fingerprintcore/FingerprintManager;

    .line 43
    return-void
.end method

.method static synthetic access$000(Lcom/itau/empresas/feature/login/LoginFingerPrint;)V
    .registers 1
    .param p0, "x0"    # Lcom/itau/empresas/feature/login/LoginFingerPrint;

    .line 23
    invoke-direct {p0}, Lcom/itau/empresas/feature/login/LoginFingerPrint;->loginComFingerprint()V

    return-void
.end method

.method static synthetic access$100(Lcom/itau/empresas/feature/login/LoginFingerPrint;)Lcom/itau/empresas/feature/login/LoginActivity;
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/feature/login/LoginFingerPrint;

    .line 23
    iget-object v0, p0, Lcom/itau/empresas/feature/login/LoginFingerPrint;->loginActivity:Lcom/itau/empresas/feature/login/LoginActivity;

    return-object v0
.end method

.method static synthetic access$200(Lcom/itau/empresas/feature/login/LoginFingerPrint;)Lcom/itau/empresas/CustomApplication;
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/feature/login/LoginFingerPrint;

    .line 23
    iget-object v0, p0, Lcom/itau/empresas/feature/login/LoginFingerPrint;->application:Lcom/itau/empresas/CustomApplication;

    return-object v0
.end method

.method static synthetic access$300(Lcom/itau/empresas/feature/login/LoginFingerPrint;Ljava/lang/String;)V
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/feature/login/LoginFingerPrint;
    .param p1, "x1"    # Ljava/lang/String;

    .line 23
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/login/LoginFingerPrint;->exibeMensagemErroFingerPrint(Ljava/lang/String;)V

    return-void
.end method

.method private exibeMensagemErroFingerPrint(Ljava/lang/String;)V
    .registers 7
    .param p1, "mensagem"    # Ljava/lang/String;

    .line 122
    new-instance v0, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;

    iget-object v1, p0, Lcom/itau/empresas/feature/login/LoginFingerPrint;->loginActivity:Lcom/itau/empresas/feature/login/LoginActivity;

    invoke-direct {v0, v1}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget-object v1, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Estados;->UM_BOTAO:Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Estados;

    .line 123
    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;->estado(Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Estados;)Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/itau/empresas/feature/login/LoginFingerPrint;->application:Lcom/itau/empresas/CustomApplication;

    .line 124
    const v3, 0x7f07044b

    invoke-virtual {v2, v3}, Lcom/itau/empresas/CustomApplication;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;->mensagem(Ljava/lang/String;)Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;

    move-result-object v0

    .line 126
    const v1, 0x7f07048c

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;->textoNeutro(I)Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;->build()Lcom/itau/empresas/ui/dialog/CustomAlertDialog;

    move-result-object v4

    .line 128
    .local v4, "dialog":Lcom/itau/empresas/ui/dialog/CustomAlertDialog;
    new-instance v0, Lcom/itau/empresas/feature/login/LoginFingerPrint$3;

    invoke-direct {v0, p0, v4}, Lcom/itau/empresas/feature/login/LoginFingerPrint$3;-><init>(Lcom/itau/empresas/feature/login/LoginFingerPrint;Lcom/itau/empresas/ui/dialog/CustomAlertDialog;)V

    invoke-virtual {v4, v0}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog;->setNeutroListener(Landroid/view/View$OnClickListener;)V

    .line 138
    iget-object v0, p0, Lcom/itau/empresas/feature/login/LoginFingerPrint;->loginActivity:Lcom/itau/empresas/feature/login/LoginActivity;

    invoke-virtual {v4, v0}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog;->show(Landroid/support/v7/app/AppCompatActivity;)Lcom/itau/empresas/ui/dialog/BaseDialog;

    .line 139
    return-void
.end method

.method private loginComFingerprint()V
    .registers 3

    .line 92
    iget-object v0, p0, Lcom/itau/empresas/feature/login/LoginFingerPrint;->loginActivity:Lcom/itau/empresas/feature/login/LoginActivity;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/login/LoginActivity;->mostraDialogoDeProgresso()V

    .line 94
    new-instance v1, Ljava/lang/Thread;

    new-instance v0, Lcom/itau/empresas/feature/login/LoginFingerPrint$2;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/login/LoginFingerPrint$2;-><init>(Lcom/itau/empresas/feature/login/LoginFingerPrint;)V

    invoke-direct {v1, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 110
    .local v1, "thread":Ljava/lang/Thread;
    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    .line 112
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/itau/empresas/feature/login/LoginFingerPrint;->fingerprintDialog:Lcom/itau/empresas/ui/dialog/FingerprintDialog;

    .line 113
    return-void
.end method


# virtual methods
.method autenticarFingerPrint()V
    .registers 6

    .line 59
    :try_start_0
    iget-object v0, p0, Lcom/itau/empresas/feature/login/LoginFingerPrint;->fingerprintDialog:Lcom/itau/empresas/ui/dialog/FingerprintDialog;

    if-nez v0, :cond_b

    .line 60
    new-instance v0, Lcom/itau/empresas/ui/dialog/FingerprintDialog;

    invoke-direct {v0}, Lcom/itau/empresas/ui/dialog/FingerprintDialog;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/feature/login/LoginFingerPrint;->fingerprintDialog:Lcom/itau/empresas/ui/dialog/FingerprintDialog;

    .line 62
    :cond_b
    iget-object v0, p0, Lcom/itau/empresas/feature/login/LoginFingerPrint;->fingerprintDialog:Lcom/itau/empresas/ui/dialog/FingerprintDialog;

    iget-object v1, p0, Lcom/itau/empresas/feature/login/LoginFingerPrint;->loginActivity:Lcom/itau/empresas/feature/login/LoginActivity;

    iget-object v2, p0, Lcom/itau/empresas/feature/login/LoginFingerPrint;->loginActivity:Lcom/itau/empresas/feature/login/LoginActivity;

    .line 64
    const v3, 0x7f0706c5

    invoke-virtual {v2, v3}, Lcom/itau/empresas/feature/login/LoginActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 63
    invoke-virtual {v0, v1, v2}, Lcom/itau/empresas/ui/dialog/FingerprintDialog;->getBuilder(Landroid/content/Context;Ljava/lang/String;)Lcom/itau/empresas/ui/dialog/FingerprintDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/itau/empresas/feature/login/LoginFingerPrint$1;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/login/LoginFingerPrint$1;-><init>(Lcom/itau/empresas/feature/login/LoginFingerPrint;)V

    .line 65
    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/dialog/FingerprintDialog$Builder;->callback(Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintCallback;)Lcom/itau/empresas/ui/dialog/FingerprintDialog$Builder;

    move-result-object v0

    .line 83
    invoke-virtual {v0}, Lcom/itau/empresas/ui/dialog/FingerprintDialog$Builder;->show()V
    :try_end_28
    .catch Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintException; {:try_start_0 .. :try_end_28} :catch_29

    .line 87
    goto :goto_3f

    .line 84
    :catch_29
    move-exception v4

    .line 85
    .local v4, "e":Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintException;
    const-string v0, "LoginFingerPrint"

    iget-object v1, p0, Lcom/itau/empresas/feature/login/LoginFingerPrint;->application:Lcom/itau/empresas/CustomApplication;

    const v2, 0x7f070446

    invoke-virtual {v1, v2}, Lcom/itau/empresas/CustomApplication;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 86
    invoke-static {}, Lde/greenrobot/event/EventBus;->getDefault()Lde/greenrobot/event/EventBus;

    move-result-object v0

    invoke-virtual {v0, v4}, Lde/greenrobot/event/EventBus;->post(Ljava/lang/Object;)V

    .line 88
    .end local v4    # "e":Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintException;
    :goto_3f
    return-void
.end method

.method exibeMensagemFingerPrintEmOutroDipositivo()V
    .registers 3

    .line 116
    iget-object v0, p0, Lcom/itau/empresas/feature/login/LoginFingerPrint;->application:Lcom/itau/empresas/CustomApplication;

    .line 117
    const v1, 0x7f070448

    invoke-virtual {v0, v1}, Lcom/itau/empresas/CustomApplication;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 116
    invoke-direct {p0, v0}, Lcom/itau/empresas/feature/login/LoginFingerPrint;->exibeMensagemErroFingerPrint(Ljava/lang/String;)V

    .line 118
    return-void
.end method

.method protected getLogarComTeclado()Z
    .registers 2

    .line 151
    iget-boolean v0, p0, Lcom/itau/empresas/feature/login/LoginFingerPrint;->logarComTeclado:Z

    return v0
.end method

.method ofereceFingerPrint()Z
    .registers 4

    .line 51
    invoke-virtual {p0}, Lcom/itau/empresas/feature/login/LoginFingerPrint;->verificaFingerprintHabilitado()Z

    move-result v0

    if-eqz v0, :cond_23

    iget-object v0, p0, Lcom/itau/empresas/feature/login/LoginFingerPrint;->fingerprintManager:Lbr/com/itau/sdk/android/fingerprintcore/FingerprintManager;

    if-eqz v0, :cond_23

    iget-object v0, p0, Lcom/itau/empresas/feature/login/LoginFingerPrint;->fingerprintManager:Lbr/com/itau/sdk/android/fingerprintcore/FingerprintManager;

    .line 53
    invoke-interface {v0}, Lbr/com/itau/sdk/android/fingerprintcore/FingerprintManager;->isAvailable()Z

    move-result v0

    if-eqz v0, :cond_23

    iget-object v0, p0, Lcom/itau/empresas/feature/login/LoginFingerPrint;->application:Lcom/itau/empresas/CustomApplication;

    .line 54
    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "FP"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    if-nez v0, :cond_23

    const/4 v0, 0x1

    goto :goto_24

    :cond_23
    const/4 v0, 0x0

    .line 51
    :goto_24
    return v0
.end method

.method public onEventMainThread(Lcom/itau/empresas/Evento$EventoLoginComSenha;)V
    .registers 3
    .param p1, "login"    # Lcom/itau/empresas/Evento$EventoLoginComSenha;

    .line 142
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/itau/empresas/feature/login/LoginFingerPrint;->logarComTeclado:Z

    .line 143
    iget-object v0, p0, Lcom/itau/empresas/feature/login/LoginFingerPrint;->loginActivity:Lcom/itau/empresas/feature/login/LoginActivity;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/login/LoginActivity;->chamaAPITeclado()V

    .line 144
    return-void
.end method

.method protected setLogarComTeclado(Z)V
    .registers 2
    .param p1, "logarComTeclado"    # Z

    .line 147
    iput-boolean p1, p0, Lcom/itau/empresas/feature/login/LoginFingerPrint;->logarComTeclado:Z

    .line 148
    return-void
.end method

.method verificaFingerprintHabilitado()Z
    .registers 3

    .line 47
    invoke-static {}, Lcom/itau/empresas/ui/util/RemoteBundleUtils;->getInstance()Lcom/itau/empresas/ui/util/RemoteBundleUtils;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/login/LoginFingerPrint;->application:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/RemoteBundleUtils;->isFingerprint(Lcom/itau/empresas/CustomApplication;)Z

    move-result v0

    return v0
.end method

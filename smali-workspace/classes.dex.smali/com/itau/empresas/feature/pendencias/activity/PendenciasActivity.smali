.class public Lcom/itau/empresas/feature/pendencias/activity/PendenciasActivity;
.super Lcom/itau/empresas/ui/activity/BaseActivity;
.source "PendenciasActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/itau/empresas/feature/pendencias/activity/PendenciasActivity$EstadosPendencias;
    }
.end annotation


# instance fields
.field private adapter:Lcom/itau/empresas/feature/pendencias/PendenciasAdapter;

.field listaPendencias:Landroid/support/v7/widget/RecyclerView;

.field logoutHelper:Lcom/itau/empresas/feature/logout/LogoutHelper;

.field pendenciasController:Lcom/itau/empresas/feature/pendencias/PendenciasController;

.field rlPendencias:Landroid/widget/RelativeLayout;

.field toolBar:Landroid/support/v7/widget/Toolbar;


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 32
    invoke-direct {p0}, Lcom/itau/empresas/ui/activity/BaseActivity;-><init>()V

    return-void
.end method

.method private buscaPendenciasPorData(Ljava/lang/String;Ljava/lang/String;)V
    .registers 6
    .param p1, "dataHoje"    # Ljava/lang/String;
    .param p2, "dataSelecionada"    # Ljava/lang/String;

    .line 146
    iget-object v0, p0, Lcom/itau/empresas/feature/pendencias/activity/PendenciasActivity;->pendenciasController:Lcom/itau/empresas/feature/pendencias/PendenciasController;

    iget-object v1, p0, Lcom/itau/empresas/feature/pendencias/activity/PendenciasActivity;->application:Lcom/itau/empresas/CustomApplication;

    .line 147
    invoke-static {v1}, Lcom/itau/empresas/ui/util/ViewUtils;->getContaCompleta(Lcom/itau/empresas/CustomApplication;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/itau/empresas/feature/pendencias/activity/PendenciasActivity;->application:Lcom/itau/empresas/CustomApplication;

    .line 148
    invoke-virtual {v2}, Lcom/itau/empresas/CustomApplication;->getCodigoOperador()Ljava/lang/String;

    move-result-object v2

    .line 147
    invoke-virtual {v0, v1, v2, p1, p2}, Lcom/itau/empresas/feature/pendencias/PendenciasController;->buscaPendencias(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 151
    return-void
.end method

.method private constroiToolbar()V
    .registers 3

    .line 94
    iget-object v0, p0, Lcom/itau/empresas/feature/pendencias/activity/PendenciasActivity;->toolBar:Landroid/support/v7/widget/Toolbar;

    invoke-static {v0, p0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom;->Builder(Landroid/support/v7/widget/Toolbar;Landroid/support/v7/app/AppCompatActivity;)Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    move-result-object v0

    .line 95
    const v1, 0x7f07067d

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->comTitulo(I)Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    move-result-object v0

    .line 96
    const v1, 0x7f0704ec

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->comContentDescription(I)Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    move-result-object v0

    .line 97
    invoke-virtual {v0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->comClickListenerFinish()Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    move-result-object v0

    .line 98
    invoke-virtual {v0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->build()Lcom/itau/empresas/ui/util/listener/ToolbarCustom;

    .line 99
    return-void
.end method

.method public static intent(Landroid/content/Context;)Lcom/itau/empresas/feature/pendencias/activity/PendenciasActivity_$IntentBuilder_;
    .registers 2
    .param p0, "context"    # Landroid/content/Context;

    .line 160
    new-instance v0, Lcom/itau/empresas/feature/pendencias/activity/PendenciasActivity_$IntentBuilder_;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/pendencias/activity/PendenciasActivity_$IntentBuilder_;-><init>(Landroid/content/Context;)V

    return-object v0
.end method


# virtual methods
.method aoIniciar()V
    .registers 6

    .line 58
    invoke-direct {p0}, Lcom/itau/empresas/feature/pendencias/activity/PendenciasActivity;->constroiToolbar()V

    .line 60
    new-instance v4, Landroid/support/v7/widget/LinearLayoutManager;

    invoke-direct {v4, p0}, Landroid/support/v7/widget/LinearLayoutManager;-><init>(Landroid/content/Context;)V

    .line 62
    .local v4, "layoutManager":Landroid/support/v7/widget/RecyclerView$LayoutManager;
    iget-object v0, p0, Lcom/itau/empresas/feature/pendencias/activity/PendenciasActivity;->listaPendencias:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, v4}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(Landroid/support/v7/widget/RecyclerView$LayoutManager;)V

    .line 63
    iget-object v0, p0, Lcom/itau/empresas/feature/pendencias/activity/PendenciasActivity;->listaPendencias:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->invalidate()V

    .line 64
    iget-object v0, p0, Lcom/itau/empresas/feature/pendencias/activity/PendenciasActivity;->listaPendencias:Landroid/support/v7/widget/RecyclerView;

    new-instance v1, Lcom/itau/empresas/feature/pendencias/PendenciasAdapter;

    iget-object v2, p0, Lcom/itau/empresas/feature/pendencias/activity/PendenciasActivity;->application:Lcom/itau/empresas/CustomApplication;

    invoke-direct {v1, p0, v2}, Lcom/itau/empresas/feature/pendencias/PendenciasAdapter;-><init>(Landroid/content/Context;Lcom/itau/empresas/CustomApplication;)V

    iput-object v1, p0, Lcom/itau/empresas/feature/pendencias/activity/PendenciasActivity;->adapter:Lcom/itau/empresas/feature/pendencias/PendenciasAdapter;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 66
    iget-object v0, p0, Lcom/itau/empresas/feature/pendencias/activity/PendenciasActivity;->application:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getEventoTracker()Lcom/itau/empresas/ui/util/analytics/TrackerEvento;

    move-result-object v0

    new-instance v1, Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;

    .line 67
    const v2, 0x7f070329

    invoke-virtual {p0, v2}, Lcom/itau/empresas/feature/pendencias/activity/PendenciasActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 68
    const v3, 0x7f07032a

    invoke-virtual {p0, v3}, Lcom/itau/empresas/feature/pendencias/activity/PendenciasActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 66
    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/analytics/TrackerEvento;->enviarHitEvento(Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;)V

    .line 69
    return-void
.end method

.method public onEventMainThread(Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent$HTTP;)V
    .registers 6
    .param p1, "httpError"    # Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent$HTTP;

    .line 114
    invoke-static {p1, p0}, Lcom/itau/empresas/api/ApiHelper;->devoTratarEvento(Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;Lcom/itau/empresas/api/ApiConsumidor;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 115
    return-void

    .line 118
    :cond_7
    invoke-virtual {p0}, Lcom/itau/empresas/feature/pendencias/activity/PendenciasActivity;->escondeDialogoDeProgresso()V

    .line 119
    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent$HTTP;->getException()Lbr/com/itau/sdk/android/core/exception/BackendException;

    move-result-object v0

    invoke-virtual {v0}, Lbr/com/itau/sdk/android/core/exception/BackendException;->getStatusCode()I

    move-result v0

    const/16 v1, 0x1f7

    if-ne v0, v1, :cond_1f

    .line 120
    iget-object v0, p0, Lcom/itau/empresas/feature/pendencias/activity/PendenciasActivity;->adapter:Lcom/itau/empresas/feature/pendencias/PendenciasAdapter;

    sget-object v1, Lcom/itau/empresas/feature/pendencias/activity/PendenciasActivity$EstadosPendencias;->SEM_CONEXAO:Lcom/itau/empresas/feature/pendencias/activity/PendenciasActivity$EstadosPendencias;

    const/4 v2, 0x0

    invoke-virtual {v0, v2, v1}, Lcom/itau/empresas/feature/pendencias/PendenciasAdapter;->atualizaAdapter(Lcom/itau/empresas/feature/pendencias/ListaPendenciasVO;Lcom/itau/empresas/feature/pendencias/activity/PendenciasActivity$EstadosPendencias;)V

    .line 121
    return-void

    .line 124
    :cond_1f
    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent$HTTP;->getException()Lbr/com/itau/sdk/android/core/exception/BackendException;

    move-result-object v0

    invoke-virtual {v0}, Lbr/com/itau/sdk/android/core/exception/BackendException;->getError()Lbr/com/itau/sdk/android/core/model/ErrorDTO;

    move-result-object v3

    .line 126
    .local v3, "errorDTO":Lbr/com/itau/sdk/android/core/model/ErrorDTO;
    iget-object v0, p0, Lcom/itau/empresas/feature/pendencias/activity/PendenciasActivity;->rlPendencias:Landroid/widget/RelativeLayout;

    invoke-virtual {v3}, Lbr/com/itau/sdk/android/core/model/ErrorDTO;->getMensagem()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lbr/com/itau/topsnackbar/TopSnackbar;->make(Landroid/view/ViewGroup;Ljava/lang/CharSequence;I)Lbr/com/itau/topsnackbar/TopSnackbar;

    move-result-object v0

    .line 127
    invoke-virtual {v0}, Lbr/com/itau/topsnackbar/TopSnackbar;->show()Lbr/com/itau/topsnackbar/TopSnackbar;

    .line 128
    return-void
.end method

.method public onEventMainThread(Lcom/itau/empresas/Evento$EventoSelecionaDataDaPendencia;)V
    .registers 5
    .param p1, "dias"    # Lcom/itau/empresas/Evento$EventoSelecionaDataDaPendencia;

    .line 131
    invoke-virtual {p1}, Lcom/itau/empresas/Evento$EventoSelecionaDataDaPendencia;->getDias()I

    move-result v0

    .line 134
    .local v0, "numeroDias":I
    invoke-static {}, Lcom/itau/empresas/ui/util/DataEHoraUtils;->getDataAtual()Ljava/lang/String;

    move-result-object v2

    .line 136
    .local v2, "dataHoje":Ljava/lang/String;
    if-nez v0, :cond_c

    .line 137
    move-object v1, v2

    .local v1, "dataSelecionada":Ljava/lang/String;
    goto :goto_10

    .line 139
    .end local v1    # "dataSelecionada":Ljava/lang/String;
    :cond_c
    invoke-static {v0}, Lcom/itau/empresas/ui/util/DataEHoraUtils;->adicionaDiasHoje(I)Ljava/lang/String;

    move-result-object v1

    .line 142
    .local v1, "dataSelecionada":Ljava/lang/String;
    :goto_10
    invoke-direct {p0, v2, v1}, Lcom/itau/empresas/feature/pendencias/activity/PendenciasActivity;->buscaPendenciasPorData(Ljava/lang/String;Ljava/lang/String;)V

    .line 143
    return-void
.end method

.method public onEventMainThread(Lcom/itau/empresas/feature/pendencias/ListaPendenciasVO;)V
    .registers 5
    .param p1, "pendenciasVO"    # Lcom/itau/empresas/feature/pendencias/ListaPendenciasVO;

    .line 103
    invoke-virtual {p1}, Lcom/itau/empresas/feature/pendencias/ListaPendenciasVO;->getListaPendencias()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_13

    .line 104
    sget-object v2, Lcom/itau/empresas/feature/pendencias/activity/PendenciasActivity$EstadosPendencias;->NAO_POSSUI_PENDENCIAS:Lcom/itau/empresas/feature/pendencias/activity/PendenciasActivity$EstadosPendencias;

    .line 105
    .local v2, "estado":Lcom/itau/empresas/feature/pendencias/activity/PendenciasActivity$EstadosPendencias;
    iget-object v0, p0, Lcom/itau/empresas/feature/pendencias/activity/PendenciasActivity;->adapter:Lcom/itau/empresas/feature/pendencias/PendenciasAdapter;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/itau/empresas/feature/pendencias/PendenciasAdapter;->atualizaAdapter(Lcom/itau/empresas/feature/pendencias/ListaPendenciasVO;Lcom/itau/empresas/feature/pendencias/activity/PendenciasActivity$EstadosPendencias;)V

    goto :goto_1a

    .line 107
    .end local v2    # "estado":Lcom/itau/empresas/feature/pendencias/activity/PendenciasActivity$EstadosPendencias;
    :cond_13
    sget-object v2, Lcom/itau/empresas/feature/pendencias/activity/PendenciasActivity$EstadosPendencias;->POSSUI_PENDENCIAS:Lcom/itau/empresas/feature/pendencias/activity/PendenciasActivity$EstadosPendencias;

    .line 108
    .local v2, "estado":Lcom/itau/empresas/feature/pendencias/activity/PendenciasActivity$EstadosPendencias;
    iget-object v0, p0, Lcom/itau/empresas/feature/pendencias/activity/PendenciasActivity;->adapter:Lcom/itau/empresas/feature/pendencias/PendenciasAdapter;

    invoke-virtual {v0, p1, v2}, Lcom/itau/empresas/feature/pendencias/PendenciasAdapter;->atualizaAdapter(Lcom/itau/empresas/feature/pendencias/ListaPendenciasVO;Lcom/itau/empresas/feature/pendencias/activity/PendenciasActivity$EstadosPendencias;)V

    .line 110
    :goto_1a
    invoke-virtual {p0}, Lcom/itau/empresas/feature/pendencias/activity/PendenciasActivity;->escondeDialogoDeProgresso()V

    .line 111
    return-void
.end method

.method protected onResume()V
    .registers 4

    .line 73
    invoke-super {p0}, Lcom/itau/empresas/ui/activity/BaseActivity;->onResume()V

    .line 76
    iget-object v0, p0, Lcom/itau/empresas/feature/pendencias/activity/PendenciasActivity;->application:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getPendenciasVO()Lcom/itau/empresas/feature/pendencias/ListaPendenciasVO;

    move-result-object v0

    if-nez v0, :cond_16

    .line 77
    invoke-virtual {p0}, Lcom/itau/empresas/feature/pendencias/activity/PendenciasActivity;->mostraDialogoDeProgresso()V

    .line 78
    invoke-static {}, Lcom/itau/empresas/ui/util/DataEHoraUtils;->getDataAtual()Ljava/lang/String;

    move-result-object v2

    .line 79
    .local v2, "data":Ljava/lang/String;
    invoke-direct {p0, v2, v2}, Lcom/itau/empresas/feature/pendencias/activity/PendenciasActivity;->buscaPendenciasPorData(Ljava/lang/String;Ljava/lang/String;)V

    .line 80
    return-void

    .line 83
    .end local v2    # "data":Ljava/lang/String;
    :cond_16
    iget-object v0, p0, Lcom/itau/empresas/feature/pendencias/activity/PendenciasActivity;->application:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getPendenciasVO()Lcom/itau/empresas/feature/pendencias/ListaPendenciasVO;

    move-result-object v2

    .line 85
    .local v2, "pendenciasVOs":Lcom/itau/empresas/feature/pendencias/ListaPendenciasVO;
    invoke-virtual {v2}, Lcom/itau/empresas/feature/pendencias/ListaPendenciasVO;->getListaPendencias()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_32

    invoke-virtual {v2}, Lcom/itau/empresas/feature/pendencias/ListaPendenciasVO;->getListaPendencias()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_32

    .line 86
    invoke-virtual {v2}, Lcom/itau/empresas/feature/pendencias/ListaPendenciasVO;->getTotalPendencias()I

    move-result v0

    if-nez v0, :cond_3a

    .line 87
    :cond_32
    iget-object v0, p0, Lcom/itau/empresas/feature/pendencias/activity/PendenciasActivity;->adapter:Lcom/itau/empresas/feature/pendencias/PendenciasAdapter;

    sget-object v1, Lcom/itau/empresas/feature/pendencias/activity/PendenciasActivity$EstadosPendencias;->NAO_POSSUI_PENDENCIAS:Lcom/itau/empresas/feature/pendencias/activity/PendenciasActivity$EstadosPendencias;

    invoke-virtual {v0, v2, v1}, Lcom/itau/empresas/feature/pendencias/PendenciasAdapter;->atualizaAdapter(Lcom/itau/empresas/feature/pendencias/ListaPendenciasVO;Lcom/itau/empresas/feature/pendencias/activity/PendenciasActivity$EstadosPendencias;)V

    .line 88
    return-void

    .line 90
    :cond_3a
    iget-object v0, p0, Lcom/itau/empresas/feature/pendencias/activity/PendenciasActivity;->adapter:Lcom/itau/empresas/feature/pendencias/PendenciasAdapter;

    sget-object v1, Lcom/itau/empresas/feature/pendencias/activity/PendenciasActivity$EstadosPendencias;->POSSUI_PENDENCIAS:Lcom/itau/empresas/feature/pendencias/activity/PendenciasActivity$EstadosPendencias;

    invoke-virtual {v0, v2, v1}, Lcom/itau/empresas/feature/pendencias/PendenciasAdapter;->atualizaAdapter(Lcom/itau/empresas/feature/pendencias/ListaPendenciasVO;Lcom/itau/empresas/feature/pendencias/activity/PendenciasActivity$EstadosPendencias;)V

    .line 91
    return-void
.end method

.method public opkeysConsumidas()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Ljava/lang/String;>;"
        }
    .end annotation

    .line 53
    const-string v0, "listaPendenciasUsuario"

    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

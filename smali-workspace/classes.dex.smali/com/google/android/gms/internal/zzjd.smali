.class public Lcom/google/android/gms/internal/zzjd;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/internal/zzjg;


# annotations
.annotation runtime Lcom/google/android/gms/internal/zzhb;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:Ljava/lang/Object;>Ljava/lang/Object;Lcom/google/android/gms/internal/zzjg<TT;>;"
    }
.end annotation


# instance fields
.field private zzCy:Z

.field private zzNc:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field private zzNd:Z

.field private final zzNe:Lcom/google/android/gms/internal/zzjh;

.field private final zzpV:Ljava/lang/Object;


# direct methods
.method public constructor <init>()V
    .registers 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/internal/zzjd;->zzpV:Ljava/lang/Object;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/internal/zzjd;->zzNc:Ljava/lang/Object;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/internal/zzjd;->zzNd:Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/internal/zzjd;->zzCy:Z

    new-instance v0, Lcom/google/android/gms/internal/zzjh;

    invoke-direct {v0}, Lcom/google/android/gms/internal/zzjh;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/internal/zzjd;->zzNe:Lcom/google/android/gms/internal/zzjh;

    return-void
.end method


# virtual methods
.method public cancel(Z)Z
    .registers 5
    .param p1, "mayInterruptIfRunning"    # Z

    if-nez p1, :cond_4

    const/4 v0, 0x0

    return v0

    :cond_4
    iget-object v1, p0, Lcom/google/android/gms/internal/zzjd;->zzpV:Ljava/lang/Object;

    monitor-enter v1

    :try_start_7
    iget-boolean v0, p0, Lcom/google/android/gms/internal/zzjd;->zzNd:Z
    :try_end_9
    .catchall {:try_start_7 .. :try_end_9} :catchall_21

    if-eqz v0, :cond_e

    monitor-exit v1

    const/4 v0, 0x0

    return v0

    :cond_e
    const/4 v0, 0x1

    :try_start_f
    iput-boolean v0, p0, Lcom/google/android/gms/internal/zzjd;->zzCy:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/internal/zzjd;->zzNd:Z

    iget-object v0, p0, Lcom/google/android/gms/internal/zzjd;->zzpV:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    iget-object v0, p0, Lcom/google/android/gms/internal/zzjd;->zzNe:Lcom/google/android/gms/internal/zzjh;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/zzjh;->zzhK()V
    :try_end_1e
    .catchall {:try_start_f .. :try_end_1e} :catchall_21

    monitor-exit v1

    const/4 v0, 0x1

    return v0

    :catchall_21
    move-exception v2

    monitor-exit v1

    throw v2
.end method

.method public get()Ljava/lang/Object;
    .registers 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    iget-object v2, p0, Lcom/google/android/gms/internal/zzjd;->zzpV:Ljava/lang/Object;

    monitor-enter v2

    :try_start_3
    iget-boolean v0, p0, Lcom/google/android/gms/internal/zzjd;->zzNd:Z
    :try_end_5
    .catchall {:try_start_3 .. :try_end_5} :catchall_1e

    if-nez v0, :cond_e

    :try_start_7
    iget-object v0, p0, Lcom/google/android/gms/internal/zzjd;->zzpV:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->wait()V
    :try_end_c
    .catch Ljava/lang/InterruptedException; {:try_start_7 .. :try_end_c} :catch_d
    .catchall {:try_start_7 .. :try_end_c} :catchall_1e

    goto :goto_e

    :catch_d
    move-exception v3

    :cond_e
    :goto_e
    :try_start_e
    iget-boolean v0, p0, Lcom/google/android/gms/internal/zzjd;->zzCy:Z

    if-eqz v0, :cond_1a

    new-instance v0, Ljava/util/concurrent/CancellationException;

    const-string v1, "CallbackFuture was cancelled."

    invoke-direct {v0, v1}, Ljava/util/concurrent/CancellationException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1a
    iget-object v0, p0, Lcom/google/android/gms/internal/zzjd;->zzNc:Ljava/lang/Object;
    :try_end_1c
    .catchall {:try_start_e .. :try_end_1c} :catchall_1e

    monitor-exit v2

    return-object v0

    :catchall_1e
    move-exception v4

    monitor-exit v2

    throw v4
.end method

.method public get(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;
    .registers 10
    .param p1, "timeout"    # J
    .param p3, "unit"    # Ljava/util/concurrent/TimeUnit;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JLjava/util/concurrent/TimeUnit;)TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/util/concurrent/TimeoutException;
        }
    .end annotation

    iget-object v2, p0, Lcom/google/android/gms/internal/zzjd;->zzpV:Ljava/lang/Object;

    monitor-enter v2

    :try_start_3
    iget-boolean v0, p0, Lcom/google/android/gms/internal/zzjd;->zzNd:Z
    :try_end_5
    .catchall {:try_start_3 .. :try_end_5} :catchall_34

    if-nez v0, :cond_18

    :try_start_7
    invoke-virtual {p3, p1, p2}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v3

    const-wide/16 v0, 0x0

    cmp-long v0, v3, v0

    if-eqz v0, :cond_16

    iget-object v0, p0, Lcom/google/android/gms/internal/zzjd;->zzpV:Ljava/lang/Object;

    invoke-virtual {v0, v3, v4}, Ljava/lang/Object;->wait(J)V
    :try_end_16
    .catch Ljava/lang/InterruptedException; {:try_start_7 .. :try_end_16} :catch_17
    .catchall {:try_start_7 .. :try_end_16} :catchall_34

    :cond_16
    goto :goto_18

    :catch_17
    move-exception v3

    :cond_18
    :goto_18
    :try_start_18
    iget-boolean v0, p0, Lcom/google/android/gms/internal/zzjd;->zzNd:Z

    if-nez v0, :cond_24

    new-instance v0, Ljava/util/concurrent/TimeoutException;

    const-string v1, "CallbackFuture timed out."

    invoke-direct {v0, v1}, Ljava/util/concurrent/TimeoutException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_24
    iget-boolean v0, p0, Lcom/google/android/gms/internal/zzjd;->zzCy:Z

    if-eqz v0, :cond_30

    new-instance v0, Ljava/util/concurrent/CancellationException;

    const-string v1, "CallbackFuture was cancelled."

    invoke-direct {v0, v1}, Ljava/util/concurrent/CancellationException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_30
    iget-object v0, p0, Lcom/google/android/gms/internal/zzjd;->zzNc:Ljava/lang/Object;
    :try_end_32
    .catchall {:try_start_18 .. :try_end_32} :catchall_34

    monitor-exit v2

    return-object v0

    :catchall_34
    move-exception v5

    monitor-exit v2

    throw v5
.end method

.method public isCancelled()Z
    .registers 4

    iget-object v1, p0, Lcom/google/android/gms/internal/zzjd;->zzpV:Ljava/lang/Object;

    monitor-enter v1

    :try_start_3
    iget-boolean v0, p0, Lcom/google/android/gms/internal/zzjd;->zzCy:Z
    :try_end_5
    .catchall {:try_start_3 .. :try_end_5} :catchall_7

    monitor-exit v1

    return v0

    :catchall_7
    move-exception v2

    monitor-exit v1

    throw v2
.end method

.method public isDone()Z
    .registers 4

    iget-object v1, p0, Lcom/google/android/gms/internal/zzjd;->zzpV:Ljava/lang/Object;

    monitor-enter v1

    :try_start_3
    iget-boolean v0, p0, Lcom/google/android/gms/internal/zzjd;->zzNd:Z
    :try_end_5
    .catchall {:try_start_3 .. :try_end_5} :catchall_7

    monitor-exit v1

    return v0

    :catchall_7
    move-exception v2

    monitor-exit v1

    throw v2
.end method

.method public zzb(Ljava/lang/Runnable;)V
    .registers 3

    iget-object v0, p0, Lcom/google/android/gms/internal/zzjd;->zzNe:Lcom/google/android/gms/internal/zzjh;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/internal/zzjh;->zzb(Ljava/lang/Runnable;)V

    return-void
.end method

.method public zzc(Ljava/lang/Runnable;)V
    .registers 3

    iget-object v0, p0, Lcom/google/android/gms/internal/zzjd;->zzNe:Lcom/google/android/gms/internal/zzjh;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/internal/zzjh;->zzc(Ljava/lang/Runnable;)V

    return-void
.end method

.method public zzg(Ljava/lang/Object;)V
    .registers 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    iget-object v2, p0, Lcom/google/android/gms/internal/zzjd;->zzpV:Ljava/lang/Object;

    monitor-enter v2

    :try_start_3
    iget-boolean v0, p0, Lcom/google/android/gms/internal/zzjd;->zzCy:Z
    :try_end_5
    .catchall {:try_start_3 .. :try_end_5} :catchall_26

    if-eqz v0, :cond_9

    monitor-exit v2

    return-void

    :cond_9
    :try_start_9
    iget-boolean v0, p0, Lcom/google/android/gms/internal/zzjd;->zzNd:Z

    if-eqz v0, :cond_15

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Provided CallbackFuture with multiple values."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_15
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/internal/zzjd;->zzNd:Z

    iput-object p1, p0, Lcom/google/android/gms/internal/zzjd;->zzNc:Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/gms/internal/zzjd;->zzpV:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    iget-object v0, p0, Lcom/google/android/gms/internal/zzjd;->zzNe:Lcom/google/android/gms/internal/zzjh;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/zzjh;->zzhK()V
    :try_end_24
    .catchall {:try_start_9 .. :try_end_24} :catchall_26

    monitor-exit v2

    goto :goto_29

    :catchall_26
    move-exception v3

    monitor-exit v2

    throw v3

    :goto_29
    return-void
.end method

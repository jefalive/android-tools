.class public Lcom/itau/empresas/feature/itoken/fragment/ITokenInstalacaoFragment;
.super Lcom/itau/empresas/ui/fragment/BaseFragment;
.source "ITokenInstalacaoFragment.java"


# instance fields
.field controller:Lcom/itau/empresas/controller/ITokenController;


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 22
    invoke-direct {p0}, Lcom/itau/empresas/ui/fragment/BaseFragment;-><init>()V

    return-void
.end method


# virtual methods
.method aoClicarEmOrientacoesSeguranca()V
    .registers 6

    .line 55
    iget-object v0, p0, Lcom/itau/empresas/feature/itoken/fragment/ITokenInstalacaoFragment;->application:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getEventoTracker()Lcom/itau/empresas/ui/util/analytics/TrackerEvento;

    move-result-object v0

    new-instance v1, Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;

    .line 56
    const v2, 0x7f0702df

    invoke-virtual {p0, v2}, Lcom/itau/empresas/feature/itoken/fragment/ITokenInstalacaoFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 57
    const v3, 0x7f0702a0

    invoke-virtual {p0, v3}, Lcom/itau/empresas/feature/itoken/fragment/ITokenInstalacaoFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 58
    const v4, 0x7f07031c

    invoke-virtual {p0, v4}, Lcom/itau/empresas/feature/itoken/fragment/ITokenInstalacaoFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 55
    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/analytics/TrackerEvento;->enviarHitEvento(Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;)V

    .line 60
    invoke-static {p0}, Lcom/itau/empresas/feature/itoken/ITokenOrientacoesSegurancaActivity;->intent(Landroid/support/v4/app/Fragment;)Lcom/itau/empresas/feature/itoken/ITokenOrientacoesSegurancaActivity_$IntentBuilder_;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/feature/itoken/ITokenOrientacoesSegurancaActivity_$IntentBuilder_;->start()Lorg/androidannotations/api/builder/PostActivityStarter;

    .line 61
    return-void
.end method

.method aoClicarNoBotaoInstalar()V
    .registers 7

    .line 39
    iget-object v0, p0, Lcom/itau/empresas/feature/itoken/fragment/ITokenInstalacaoFragment;->application:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getEventoTracker()Lcom/itau/empresas/ui/util/analytics/TrackerEvento;

    move-result-object v0

    new-instance v1, Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;

    .line 40
    const v2, 0x7f0702df

    invoke-virtual {p0, v2}, Lcom/itau/empresas/feature/itoken/fragment/ITokenInstalacaoFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 41
    const v3, 0x7f0702a0

    invoke-virtual {p0, v3}, Lcom/itau/empresas/feature/itoken/fragment/ITokenInstalacaoFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 42
    const v4, 0x7f07030b

    invoke-virtual {p0, v4}, Lcom/itau/empresas/feature/itoken/fragment/ITokenInstalacaoFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 39
    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/analytics/TrackerEvento;->enviarHitEvento(Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;)V

    .line 44
    invoke-virtual {p0}, Lcom/itau/empresas/feature/itoken/fragment/ITokenInstalacaoFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    .line 46
    .local v5, "activity":Landroid/support/v4/app/FragmentActivity;
    instance-of v0, v5, Lcom/itau/empresas/ui/activity/BaseActivity;

    if-eqz v0, :cond_31

    .line 47
    move-object v0, v5

    check-cast v0, Lcom/itau/empresas/ui/activity/BaseActivity;

    invoke-virtual {v0}, Lcom/itau/empresas/ui/activity/BaseActivity;->mostraDialogoDeProgresso()V

    .line 48
    :cond_31
    invoke-static {v5}, Lcom/itau/empresas/ui/util/SnackbarUtils;->tentaDispensarSnackbarNo(Ljava/lang/Object;)V

    .line 50
    iget-object v0, p0, Lcom/itau/empresas/feature/itoken/fragment/ITokenInstalacaoFragment;->controller:Lcom/itau/empresas/controller/ITokenController;

    invoke-virtual {v0}, Lcom/itau/empresas/controller/ITokenController;->instalarTokenApp()V

    .line 51
    return-void
.end method

.method public aposCarregar()V
    .registers 4

    .line 30
    invoke-virtual {p0}, Lcom/itau/empresas/feature/itoken/fragment/ITokenInstalacaoFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/ui/activity/BaseActivity;

    invoke-virtual {v0}, Lcom/itau/empresas/ui/activity/BaseActivity;->escondeDialogoDeProgresso()V

    .line 32
    const v0, 0x7f0700f5

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/itoken/fragment/ITokenInstalacaoFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 33
    const v1, 0x7f0700cf

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/itoken/fragment/ITokenInstalacaoFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 34
    const v2, 0x7f0700c3

    invoke-virtual {p0, v2}, Lcom/itau/empresas/feature/itoken/fragment/ITokenInstalacaoFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 32
    invoke-virtual {p0, v0, v1, v2}, Lcom/itau/empresas/feature/itoken/fragment/ITokenInstalacaoFragment;->eventoAdobe(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 35
    return-void
.end method

.method public opkeysConsumidas()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Ljava/lang/String;>;"
        }
    .end annotation

    .line 65
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.class final Lcom/adobe/mobile/PiiQueue;
.super Lcom/adobe/mobile/ThirdPartyQueue;
.source "PiiQueue.java"


# static fields
.field private static _instance:Lcom/adobe/mobile/PiiQueue;

.field private static final _instanceMutex:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .line 30
    const/4 v0, 0x0

    sput-object v0, Lcom/adobe/mobile/PiiQueue;->_instance:Lcom/adobe/mobile/PiiQueue;

    .line 31
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/adobe/mobile/PiiQueue;->_instanceMutex:Ljava/lang/Object;

    return-void
.end method

.method protected constructor <init>()V
    .registers 1

    .line 12
    invoke-direct {p0}, Lcom/adobe/mobile/ThirdPartyQueue;-><init>()V

    .line 13
    return-void
.end method

.method protected static sharedInstance()Lcom/adobe/mobile/PiiQueue;
    .registers 3

    .line 34
    sget-object v1, Lcom/adobe/mobile/PiiQueue;->_instanceMutex:Ljava/lang/Object;

    monitor-enter v1

    .line 35
    :try_start_3
    sget-object v0, Lcom/adobe/mobile/PiiQueue;->_instance:Lcom/adobe/mobile/PiiQueue;

    if-nez v0, :cond_e

    .line 36
    new-instance v0, Lcom/adobe/mobile/PiiQueue;

    invoke-direct {v0}, Lcom/adobe/mobile/PiiQueue;-><init>()V

    sput-object v0, Lcom/adobe/mobile/PiiQueue;->_instance:Lcom/adobe/mobile/PiiQueue;

    .line 39
    :cond_e
    sget-object v0, Lcom/adobe/mobile/PiiQueue;->_instance:Lcom/adobe/mobile/PiiQueue;
    :try_end_10
    .catchall {:try_start_3 .. :try_end_10} :catchall_12

    monitor-exit v1

    return-object v0

    .line 40
    :catchall_12
    move-exception v2

    monitor-exit v1

    throw v2
.end method


# virtual methods
.method protected fileName()Ljava/lang/String;
    .registers 2

    .line 27
    const-string v0, "ADBMobilePIICache.sqlite"

    return-object v0
.end method

.method protected getWorker()Lcom/adobe/mobile/ThirdPartyQueue;
    .registers 2

    .line 45
    invoke-static {}, Lcom/adobe/mobile/PiiQueue;->sharedInstance()Lcom/adobe/mobile/PiiQueue;

    move-result-object v0

    return-object v0
.end method

.method protected logPrefix()Ljava/lang/String;
    .registers 2

    .line 22
    const-string v0, "PII"

    return-object v0
.end method

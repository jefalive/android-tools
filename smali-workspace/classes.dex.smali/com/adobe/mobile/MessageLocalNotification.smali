.class final Lcom/adobe/mobile/MessageLocalNotification;
.super Lcom/adobe/mobile/Message;
.source "MessageLocalNotification.java"


# instance fields
.field protected content:Ljava/lang/String;

.field protected deeplink:Ljava/lang/String;

.field protected localNotificationDelay:Ljava/lang/Integer;

.field protected userInfo:Ljava/lang/String;


# direct methods
.method constructor <init>()V
    .registers 1

    .line 34
    invoke-direct {p0}, Lcom/adobe/mobile/Message;-><init>()V

    return-void
.end method


# virtual methods
.method protected initWithPayloadObject(Lorg/json/JSONObject;)Z
    .registers 8
    .param p1, "dictionary"    # Lorg/json/JSONObject;

    .line 49
    if-eqz p1, :cond_8

    invoke-virtual {p1}, Lorg/json/JSONObject;->length()I

    move-result v0

    if-gtz v0, :cond_a

    .line 50
    :cond_8
    const/4 v0, 0x0

    return v0

    .line 54
    :cond_a
    invoke-super {p0, p1}, Lcom/adobe/mobile/Message;->initWithPayloadObject(Lorg/json/JSONObject;)Z

    move-result v0

    if-nez v0, :cond_12

    .line 55
    const/4 v0, 0x0

    return v0

    .line 58
    :cond_12
    const-string v0, "Messages -  Building Local Notification message."

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logDebugFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 62
    const-string v0, "payload"

    :try_start_1c
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v4

    .line 63
    .local v4, "jsonPayload":Lorg/json/JSONObject;
    invoke-virtual {v4}, Lorg/json/JSONObject;->length()I

    move-result v0

    if-gtz v0, :cond_35

    .line 64
    const-string v0, "Messages - Unable to create local notification message \"%s\", payload is empty"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/adobe/mobile/MessageLocalNotification;->messageId:Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logDebugFormat(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_33
    .catch Lorg/json/JSONException; {:try_start_1c .. :try_end_33} :catch_36

    .line 65
    const/4 v0, 0x0

    return v0

    .line 71
    :cond_35
    goto :goto_46

    .line 68
    .end local v4    # "jsonPayload":Lorg/json/JSONObject;
    :catch_36
    move-exception v5

    .line 69
    .local v5, "ex":Lorg/json/JSONException;
    const-string v0, "Messages - Unable to create local notification message \"%s\", payload is required"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/adobe/mobile/MessageLocalNotification;->messageId:Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logDebugFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 70
    const/4 v0, 0x0

    return v0

    .line 75
    .local v4, "jsonPayload":Lorg/json/JSONObject;
    .end local v5    # "ex":Lorg/json/JSONException;
    :goto_46
    const-string v0, "content"

    :try_start_48
    invoke-virtual {v4, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/adobe/mobile/MessageLocalNotification;->content:Ljava/lang/String;

    .line 76
    iget-object v0, p0, Lcom/adobe/mobile/MessageLocalNotification;->content:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-gtz v0, :cond_65

    .line 77
    const-string v0, "Messages - Unable to create local notification message \"%s\", content is empty"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/adobe/mobile/MessageLocalNotification;->messageId:Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logDebugFormat(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_63
    .catch Lorg/json/JSONException; {:try_start_48 .. :try_end_63} :catch_66

    .line 78
    const/4 v0, 0x0

    return v0

    .line 84
    :cond_65
    goto :goto_76

    .line 81
    :catch_66
    move-exception v5

    .line 82
    .local v5, "ex":Lorg/json/JSONException;
    const-string v0, "Messages - Unable to create local notification message \"%s\", content is required"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/adobe/mobile/MessageLocalNotification;->messageId:Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logDebugFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 83
    const/4 v0, 0x0

    return v0

    .line 87
    .end local v5    # "ex":Lorg/json/JSONException;
    :goto_76
    const-string v0, "wait"

    :try_start_78
    invoke-virtual {v4, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/adobe/mobile/MessageLocalNotification;->localNotificationDelay:Ljava/lang/Integer;
    :try_end_82
    .catch Lorg/json/JSONException; {:try_start_78 .. :try_end_82} :catch_83

    .line 92
    goto :goto_93

    .line 89
    :catch_83
    move-exception v5

    .line 90
    .local v5, "ex":Lorg/json/JSONException;
    const-string v0, "Messages - Unable to create local notification message \"%s\", localNotificationDelay is required"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/adobe/mobile/MessageLocalNotification;->messageId:Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logDebugFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 91
    const/4 v0, 0x0

    return v0

    .line 95
    .end local v5    # "ex":Lorg/json/JSONException;
    :goto_93
    const-string v0, "adb_deeplink"

    :try_start_95
    invoke-virtual {v4, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/adobe/mobile/MessageLocalNotification;->deeplink:Ljava/lang/String;
    :try_end_9b
    .catch Lorg/json/JSONException; {:try_start_95 .. :try_end_9b} :catch_9c

    .line 99
    goto :goto_a5

    .line 97
    :catch_9c
    move-exception v5

    .line 98
    .local v5, "ex":Lorg/json/JSONException;
    const-string v0, "Messages - Tried to read deeplink for local notification message but found none.  This is not a required field"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logDebugFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 102
    .end local v5    # "ex":Lorg/json/JSONException;
    :goto_a5
    const-string v0, "userData"

    :try_start_a7
    invoke-virtual {v4, v0}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/adobe/mobile/MessageLocalNotification;->userInfo:Ljava/lang/String;
    :try_end_b1
    .catch Lorg/json/JSONException; {:try_start_a7 .. :try_end_b1} :catch_b2
    .catch Ljava/lang/NullPointerException; {:try_start_a7 .. :try_end_b1} :catch_bc

    .line 108
    goto :goto_c5

    .line 104
    :catch_b2
    move-exception v5

    .line 105
    .local v5, "ex":Lorg/json/JSONException;
    const-string v0, "Messages - Tried to read userData for local notification message but found none.  This is not a required field"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logDebugFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 108
    .end local v5    # "ex":Lorg/json/JSONException;
    goto :goto_c5

    .line 106
    :catch_bc
    move-exception v5

    .line 107
    .local v5, "ex":Ljava/lang/NullPointerException;
    const-string v0, "Messages - Tried to read userData for local notification message but found none.  This is not a required field"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logDebugFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 110
    .end local v5    # "ex":Ljava/lang/NullPointerException;
    :goto_c5
    const-string v0, "Message created with: \n MessageID: \"%s\" \n Content: \"%s\" \n Delay: \"%d\" \n Deeplink: \"%s\" \n User Data: \"%s\""

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/adobe/mobile/MessageLocalNotification;->messageId:Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    iget-object v2, p0, Lcom/adobe/mobile/MessageLocalNotification;->content:Ljava/lang/String;

    const/4 v3, 0x1

    aput-object v2, v1, v3

    iget-object v2, p0, Lcom/adobe/mobile/MessageLocalNotification;->localNotificationDelay:Ljava/lang/Integer;

    const/4 v3, 0x2

    aput-object v2, v1, v3

    iget-object v2, p0, Lcom/adobe/mobile/MessageLocalNotification;->deeplink:Ljava/lang/String;

    const/4 v3, 0x3

    aput-object v2, v1, v3

    iget-object v2, p0, Lcom/adobe/mobile/MessageLocalNotification;->userInfo:Ljava/lang/String;

    const/4 v3, 0x4

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logDebugFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 112
    const/4 v0, 0x1

    return v0
.end method

.method protected show()V
    .registers 11

    .line 117
    invoke-super {p0}, Lcom/adobe/mobile/Message;->show()V

    .line 121
    :try_start_3
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getCurrentActivity()Landroid/app/Activity;
    :try_end_6
    .catch Lcom/adobe/mobile/StaticMethods$NullActivityException; {:try_start_3 .. :try_end_6} :catch_8

    move-result-object v4

    .line 126
    .local v4, "currentActivity":Landroid/app/Activity;
    goto :goto_14

    .line 123
    .end local v4    # "currentActivity":Landroid/app/Activity;
    :catch_8
    move-exception v5

    .line 124
    .local v5, "ex":Lcom/adobe/mobile/StaticMethods$NullActivityException;
    invoke-virtual {v5}, Lcom/adobe/mobile/StaticMethods$NullActivityException;->getMessage()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logErrorFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 125
    return-void

    .line 128
    .local v4, "currentActivity":Landroid/app/Activity;
    .end local v5    # "ex":Lcom/adobe/mobile/StaticMethods$NullActivityException;
    :goto_14
    new-instance v0, Ljava/security/SecureRandom;

    invoke-direct {v0}, Ljava/security/SecureRandom;-><init>()V

    invoke-virtual {v0}, Ljava/security/SecureRandom;->nextInt()I

    move-result v5

    .line 130
    .local v5, "requestCode":I
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v6

    .line 131
    .local v6, "calendar":Ljava/util/Calendar;
    iget-object v0, p0, Lcom/adobe/mobile/MessageLocalNotification;->localNotificationDelay:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/16 v1, 0xd

    invoke-virtual {v6, v1, v0}, Ljava/util/Calendar;->add(II)V

    .line 132
    new-instance v7, Landroid/content/Intent;

    const-string v0, "android.intent.action.VIEW"

    invoke-direct {v7, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 133
    .local v7, "intent":Landroid/content/Intent;
    const-class v0, Lcom/adobe/mobile/MessageNotificationHandler;

    invoke-virtual {v7, v4, v0}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 134
    const-string v0, "adbMessageCode"

    sget-object v1, Lcom/adobe/mobile/Messages;->MESSAGE_LOCAL_IDENTIFIER:Ljava/lang/Integer;

    invoke-virtual {v7, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 135
    const-string v0, "adb_m_l_id"

    iget-object v1, p0, Lcom/adobe/mobile/MessageLocalNotification;->messageId:Ljava/lang/String;

    invoke-virtual {v7, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 136
    const-string v0, "requestCode"

    invoke-virtual {v7, v0, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 137
    const-string v0, "userData"

    iget-object v1, p0, Lcom/adobe/mobile/MessageLocalNotification;->userInfo:Ljava/lang/String;

    invoke-virtual {v7, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 138
    const-string v0, "adb_deeplink"

    iget-object v1, p0, Lcom/adobe/mobile/MessageLocalNotification;->deeplink:Ljava/lang/String;

    invoke-virtual {v7, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 139
    const-string v0, "alarm_message"

    iget-object v1, p0, Lcom/adobe/mobile/MessageLocalNotification;->content:Ljava/lang/String;

    invoke-virtual {v7, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 142
    :try_start_60
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getSharedContext()Landroid/content/Context;

    move-result-object v0

    const/high16 v1, 0x8000000

    invoke-static {v0, v5, v7, v1}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v8

    .line 144
    .local v8, "sender":Landroid/app/PendingIntent;
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getSharedContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "alarm"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Landroid/app/AlarmManager;

    .line 145
    .local v9, "alarmManager":Landroid/app/AlarmManager;
    invoke-virtual {v6}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    const/4 v2, 0x0

    invoke-virtual {v9, v2, v0, v1, v8}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V
    :try_end_7f
    .catch Lcom/adobe/mobile/StaticMethods$NullContextException; {:try_start_60 .. :try_end_7f} :catch_80

    .line 148
    .end local v8    # "sender":Landroid/app/PendingIntent;
    .end local v9    # "alarmManager":Landroid/app/AlarmManager;
    goto :goto_90

    .line 146
    :catch_80
    move-exception v8

    .line 147
    .local v8, "e":Lcom/adobe/mobile/StaticMethods$NullContextException;
    const-string v0, "Messaging - Error scheduling local notification (%s)"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v8}, Lcom/adobe/mobile/StaticMethods$NullContextException;->getMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logErrorFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 149
    .end local v8    # "e":Lcom/adobe/mobile/StaticMethods$NullContextException;
    :goto_90
    return-void
.end method

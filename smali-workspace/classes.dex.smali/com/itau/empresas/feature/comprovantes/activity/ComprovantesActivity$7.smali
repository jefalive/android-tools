.class Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesActivity$7;
.super Ljava/lang/Object;
.source "ComprovantesActivity.java"

# interfaces
.implements Landroid/support/v7/widget/SearchView$OnQueryTextListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesActivity;->pesquisarSaldoExtrato()Landroid/support/v7/widget/SearchView$OnQueryTextListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesActivity;


# direct methods
.method constructor <init>(Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesActivity;)V
    .registers 2
    .param p1, "this$0"    # Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesActivity;

    .line 253
    iput-object p1, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesActivity$7;->this$0:Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onQueryTextChange(Ljava/lang/String;)Z
    .registers 3
    .param p1, "newText"    # Ljava/lang/String;

    .line 264
    iget-object v0, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesActivity$7;->this$0:Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesActivity;

    iget-object v0, v0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesActivity;->comprovantesAdapter:Lcom/itau/empresas/feature/comprovantes/ComprovantesAdapter;

    if-eqz v0, :cond_11

    .line 265
    iget-object v0, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesActivity$7;->this$0:Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesActivity;

    iget-object v0, v0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesActivity;->comprovantesAdapter:Lcom/itau/empresas/feature/comprovantes/ComprovantesAdapter;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/comprovantes/ComprovantesAdapter;->getFilter()Landroid/widget/Filter;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/Filter;->filter(Ljava/lang/CharSequence;)V

    .line 267
    :cond_11
    const/4 v0, 0x0

    return v0
.end method

.method public onQueryTextSubmit(Ljava/lang/String;)Z
    .registers 3
    .param p1, "query"    # Ljava/lang/String;

    .line 256
    iget-object v0, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesActivity$7;->this$0:Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesActivity;

    iget-object v0, v0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesActivity;->comprovantesAdapter:Lcom/itau/empresas/feature/comprovantes/ComprovantesAdapter;

    if-eqz v0, :cond_11

    .line 257
    iget-object v0, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesActivity$7;->this$0:Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesActivity;

    iget-object v0, v0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesActivity;->comprovantesAdapter:Lcom/itau/empresas/feature/comprovantes/ComprovantesAdapter;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/comprovantes/ComprovantesAdapter;->getFilter()Landroid/widget/Filter;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/Filter;->filter(Ljava/lang/CharSequence;)V

    .line 259
    :cond_11
    const/4 v0, 0x0

    return v0
.end method

.class Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView$1;
.super Ljava/lang/Object;
.source "EmprestimosParceladosDetalhadoView.java"

# interfaces
.implements Lcom/itau/empresas/ui/util/maquinaestado/Estado$OnChangeState;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->inicializaMaquinaDeEstado()Lcom/itau/empresas/ui/util/MaquinaDeEstado;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;


# direct methods
.method constructor <init>(Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;)V
    .registers 2
    .param p1, "this$0"    # Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;

    .line 252
    iput-object p1, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView$1;->this$0:Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onChangeState()V
    .registers 4

    .line 256
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView$1;->this$0:Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;

    # getter for: Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->temSeguro:Z
    invoke-static {v0}, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->access$000(Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;)Z

    move-result v0

    if-eqz v0, :cond_1d

    .line 258
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView$1;->this$0:Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;

    iget-object v0, v0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->botaoSimular:Landroid/widget/Button;

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView$1;->this$0:Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;

    .line 259
    invoke-virtual {v1}, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f070673

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto :goto_31

    .line 263
    :cond_1d
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView$1;->this$0:Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;

    iget-object v0, v0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->botaoSimular:Landroid/widget/Button;

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView$1;->this$0:Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;

    .line 264
    invoke-virtual {v1}, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f070672

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 269
    :goto_31
    return-void
.end method

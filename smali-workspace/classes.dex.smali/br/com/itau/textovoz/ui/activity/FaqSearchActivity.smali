.class public Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;
.super Lbr/com/itau/textovoz/ui/activity/BaseActivity;
.source "FaqSearchActivity.java"


# static fields
.field public static final TAG:Ljava/lang/String;

.field private static menuOptions:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<Ljava/lang/String;[Ljava/lang/String;>;"
        }
    .end annotation
.end field


# instance fields
.field channelsExpandableListAdapter:Landroid/widget/ExpandableListAdapter;

.field private linearLinksContainer:Landroid/widget/LinearLayout;

.field private listChannels:Landroid/widget/ExpandableListView;

.field onClickListener:Landroid/view/View$OnClickListener;

.field private operator:Ljava/lang/String;

.field private relativeAnswerFoot:Landroid/widget/LinearLayout;

.field private relativeAnswerYes:Landroid/widget/RelativeLayout;

.field private scrollAnswer:Landroid/support/v4/widget/NestedScrollView;

.field private segment:Ljava/lang/String;

.field private textAnswer:Landroid/widget/TextView;

.field private textFaq:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .line 39
    const-class v0, Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;->TAG:Ljava/lang/String;

    .line 53
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;->menuOptions:Ljava/util/HashMap;

    return-void
.end method

.method public constructor <init>()V
    .registers 2

    .line 37
    invoke-direct {p0}, Lbr/com/itau/textovoz/ui/activity/BaseActivity;-><init>()V

    .line 98
    new-instance v0, Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity$1;

    invoke-direct {v0, p0}, Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity$1;-><init>(Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;)V

    iput-object v0, p0, Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;->onClickListener:Landroid/view/View$OnClickListener;

    return-void
.end method

.method static synthetic access$000(Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;Ljava/lang/String;)V
    .registers 2
    .param p0, "x0"    # Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;
    .param p1, "x1"    # Ljava/lang/String;

    .line 37
    invoke-direct {p0, p1}, Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;->putGoogleAnalytic(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$100(Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;)Landroid/widget/RelativeLayout;
    .registers 2
    .param p0, "x0"    # Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;

    .line 37
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;->relativeAnswerYes:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$200(Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;)Ljava/lang/String;
    .registers 2
    .param p0, "x0"    # Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;

    .line 37
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;->operator:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300()Ljava/util/HashMap;
    .registers 1

    .line 37
    sget-object v0, Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;->menuOptions:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic access$400(Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;[Ljava/lang/String;)V
    .registers 2
    .param p0, "x0"    # Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;
    .param p1, "x1"    # [Ljava/lang/String;

    .line 37
    invoke-direct {p0, p1}, Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;->resultFaqLink([Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$500(Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;)Landroid/support/v4/widget/NestedScrollView;
    .registers 2
    .param p0, "x0"    # Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;

    .line 37
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;->scrollAnswer:Landroid/support/v4/widget/NestedScrollView;

    return-object v0
.end method

.method static synthetic access$600(Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;)Landroid/widget/ExpandableListView;
    .registers 2
    .param p0, "x0"    # Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;

    .line 37
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;->listChannels:Landroid/widget/ExpandableListView;

    return-object v0
.end method

.method private putGoogleAnalytic(Ljava/lang/String;)V
    .registers 4
    .param p1, "gaValue"    # Ljava/lang/String;

    .line 113
    sget v0, Lbr/com/itau/textovoz/R$string;->ga_csi_feedback:I

    invoke-virtual {p0, v0}, Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    sget v1, Lbr/com/itau/textovoz/R$string;->ga_faq:I

    invoke-virtual {p0, v1}, Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1, p1}, Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;->setEventTracker(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 114
    invoke-direct {p0}, Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;->setVisibilityFeedBack()V

    .line 115
    return-void
.end method

.method private resultFaqLink([Ljava/lang/String;)V
    .registers 4
    .param p1, "back"    # [Ljava/lang/String;

    .line 185
    invoke-virtual {p0}, Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 186
    .local v1, "intent":Landroid/content/Intent;
    sget v0, Lbr/com/itau/textovoz/R$string;->link_result:I

    invoke-virtual {p0, v0}, Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 187
    const/4 v0, -0x1

    invoke-virtual {p0, v0, v1}, Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;->setResult(ILandroid/content/Intent;)V

    .line 188
    invoke-virtual {p0}, Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;->finish()V

    .line 189
    return-void
.end method

.method private setHashMapLinks()V
    .registers 6

    .line 267
    sget-object v0, Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;->menuOptions:Ljava/util/HashMap;

    const-string v1, "ContaCorrente"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "conta_corrente"

    const/4 v4, 0x0

    aput-object v3, v2, v4

    const-string v3, "conta corrente"

    const/4 v4, 0x1

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 269
    sget-object v0, Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;->menuOptions:Ljava/util/HashMap;

    const-string v1, "PagamentoCodigoBarras"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "codigo_de_barras"

    const/4 v4, 0x0

    aput-object v3, v2, v4

    const-string v3, "pagamentos"

    const/4 v4, 0x1

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 271
    sget-object v0, Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;->menuOptions:Ljava/util/HashMap;

    const-string v1, "Transferencias"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "transferencias"

    const/4 v4, 0x0

    aput-object v3, v2, v4

    const-string v3, "tranf\u00earencias"

    const/4 v4, 0x1

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 273
    sget-object v0, Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;->menuOptions:Ljava/util/HashMap;

    const-string v1, "Recarga"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "recarga"

    const/4 v4, 0x0

    aput-object v3, v2, v4

    const-string v3, "recarga"

    const/4 v4, 0x1

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 275
    sget-object v0, Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;->menuOptions:Ljava/util/HashMap;

    const-string v1, "Comprovantes"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "comprovantes"

    const/4 v4, 0x0

    aput-object v3, v2, v4

    const-string v3, "comprovantes"

    const/4 v4, 0x1

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 277
    sget-object v0, Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;->menuOptions:Ljava/util/HashMap;

    const-string v1, "SegurancaITokenSMS"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "itoken_por_sms"

    const/4 v4, 0x0

    aput-object v3, v2, v4

    const-string v3, "iToken por SMS"

    const/4 v4, 0x1

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 279
    sget-object v0, Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;->menuOptions:Ljava/util/HashMap;

    const-string v1, "SegurancaIToKenAplicativo"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "itoken_no_aplicativo"

    const/4 v4, 0x0

    aput-object v3, v2, v4

    const-string v3, "iToken no aplicativo"

    const/4 v4, 0x1

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 281
    sget-object v0, Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;->menuOptions:Ljava/util/HashMap;

    const-string v1, "ViagemAvisoViagem"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "net_serv_viaj_sm"

    const/4 v4, 0x0

    aput-object v3, v2, v4

    const-string v3, "aviso viagem"

    const/4 v4, 0x1

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 283
    sget-object v0, Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;->menuOptions:Ljava/util/HashMap;

    const-string v1, "CartoesFaturas"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "pagto_cartao_new"

    const/4 v4, 0x0

    aput-object v3, v2, v4

    const-string v3, "pagamento de fatura"

    const/4 v4, 0x1

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 285
    sget-object v0, Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;->menuOptions:Ljava/util/HashMap;

    const-string v1, "PagamentoFaturaCartao"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "pagto_cartao_new"

    const/4 v4, 0x0

    aput-object v3, v2, v4

    const-string v3, "fatura cart\u00e3o"

    const/4 v4, 0x1

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 287
    sget-object v0, Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;->menuOptions:Ljava/util/HashMap;

    const-string v1, "PagamentoAutorizar"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "sisdeb_expresso"

    const/4 v4, 0x0

    aput-object v3, v2, v4

    const-string v3, "d\u00e9bitos e autorizar"

    const/4 v4, 0x1

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 289
    sget-object v0, Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;->menuOptions:Ljava/util/HashMap;

    const-string v1, "PagamentoDDA"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "net_dda_app"

    const/4 v4, 0x0

    aput-object v3, v2, v4

    const-string v3, "DDA"

    const/4 v4, 0x1

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 291
    sget-object v0, Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;->menuOptions:Ljava/util/HashMap;

    const-string v1, "CartoesLimites"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "net_dda_app"

    const/4 v4, 0x0

    aput-object v3, v2, v4

    const-string v3, "limite de cr\u00e9dito"

    const/4 v4, 0x1

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 293
    sget-object v0, Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;->menuOptions:Ljava/util/HashMap;

    const-string v1, "CartoesPagamentoFatura"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "pagto_cartao_new"

    const/4 v4, 0x0

    aput-object v3, v2, v4

    const-string v3, "pagamento de faturas"

    const/4 v4, 0x1

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 295
    sget-object v0, Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;->menuOptions:Ljava/util/HashMap;

    const-string v1, "CartoesAvisoViagem"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "net_serv_viaj_sm"

    const/4 v4, 0x0

    aput-object v3, v2, v4

    const-string v3, "aviso viagem"

    const/4 v4, 0x1

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 297
    sget-object v0, Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;->menuOptions:Ljava/util/HashMap;

    const-string v1, "CartoesSolucoesViagem"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "net_menu_cartao_prepago_app"

    const/4 v4, 0x0

    aput-object v3, v2, v4

    const-string v3, "solu\u00e7\u00f5es para viagem"

    const/4 v4, 0x1

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 299
    sget-object v0, Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;->menuOptions:Ljava/util/HashMap;

    const-string v1, "CartoesFaturaDigital"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "net_fatura_eletronica"

    const/4 v4, 0x0

    aput-object v3, v2, v4

    const-string v3, "fatura digital"

    const/4 v4, 0x1

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 301
    sget-object v0, Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;->menuOptions:Ljava/util/HashMap;

    const-string v1, "CartoesDesbloqueio"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "net_desbloqueio_cartao"

    const/4 v4, 0x0

    aput-object v3, v2, v4

    const-string v3, "desbloqueio de cart\u00f5es"

    const/4 v4, 0x1

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 303
    sget-object v0, Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;->menuOptions:Ljava/util/HashMap;

    const-string v1, "CartoesTrocaSenha"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "net_reemissao_senha_cartao"

    const/4 v4, 0x0

    aput-object v3, v2, v4

    const-string v3, "troca de senha"

    const/4 v4, 0x1

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 305
    sget-object v0, Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;->menuOptions:Ljava/util/HashMap;

    const-string v1, "PoupancaExtrato"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "sdo_poup_new"

    const/4 v4, 0x0

    aput-object v3, v2, v4

    const-string v3, "extrato"

    const/4 v4, 0x1

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 307
    sget-object v0, Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;->menuOptions:Ljava/util/HashMap;

    const-string v1, "PoupancaResgate"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "poupanca_resgate"

    const/4 v4, 0x0

    aput-object v3, v2, v4

    const-string v3, "resgate"

    const/4 v4, 0x1

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 309
    sget-object v0, Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;->menuOptions:Ljava/util/HashMap;

    const-string v1, "PoupancaAplicacao"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "poupanca_aplicacao"

    const/4 v4, 0x0

    aput-object v3, v2, v4

    const-string v3, "aplica\u00e7\u00e3o"

    const/4 v4, 0x1

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 311
    sget-object v0, Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;->menuOptions:Ljava/util/HashMap;

    const-string v1, "Emprestimos"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "net_new_emprestimo"

    const/4 v4, 0x0

    aput-object v3, v2, v4

    const-string v3, "cr\u00e9dito"

    const/4 v4, 0x1

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 313
    sget-object v0, Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;->menuOptions:Ljava/util/HashMap;

    const-string v1, "InvestimentosMeusInvestimentos"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "net_meus_invest_app"

    const/4 v4, 0x0

    aput-object v3, v2, v4

    const-string v3, "meus investimentos"

    const/4 v4, 0x1

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 315
    sget-object v0, Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;->menuOptions:Ljava/util/HashMap;

    const-string v1, "InvestimentosResgate"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "net_inv_resgate_app"

    const/4 v4, 0x0

    aput-object v3, v2, v4

    const-string v3, "resgate"

    const/4 v4, 0x1

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 317
    sget-object v0, Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;->menuOptions:Ljava/util/HashMap;

    const-string v1, "InvestimentosAplicacao"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "net_inv_aplicacao_app"

    const/4 v4, 0x0

    aput-object v3, v2, v4

    const-string v3, "aplica\u00e7\u00e3o"

    const/4 v4, 0x1

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 319
    sget-object v0, Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;->menuOptions:Ljava/util/HashMap;

    const-string v1, "InvestimentosAcoes"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "acoes_login_usabilidade"

    const/4 v4, 0x0

    aput-object v3, v2, v4

    const-string v3, "a\u00e7\u00f5es"

    const/4 v4, 0x1

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 321
    sget-object v0, Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;->menuOptions:Ljava/util/HashMap;

    const-string v1, "InvestimentosCatalogoFundos"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "net_invest_catalogo"

    const/4 v4, 0x0

    aput-object v3, v2, v4

    const-string v3, "cat\u00e1logo de fundos"

    const/4 v4, 0x1

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 323
    sget-object v0, Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;->menuOptions:Ljava/util/HashMap;

    const-string v1, "ViagemServicoViagem"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "net_checklist_viagem_int"

    const/4 v4, 0x0

    aput-object v3, v2, v4

    const-string v3, "servi\u00e7os para viagem"

    const/4 v4, 0x1

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 325
    sget-object v0, Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;->menuOptions:Ljava/util/HashMap;

    const-string v1, "ViagemTravelMoney"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "net_itau_cartao_prepago_app"

    const/4 v4, 0x0

    aput-object v3, v2, v4

    const-string v3, "Ita\u00fa Travel Money"

    const/4 v4, 0x1

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 327
    sget-object v0, Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;->menuOptions:Ljava/util/HashMap;

    const-string v1, "ViagemSeguro"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "net_seguroviagem2"

    const/4 v4, 0x0

    aput-object v3, v2, v4

    const-string v3, "seguro viagem"

    const/4 v4, 0x1

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 329
    sget-object v0, Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;->menuOptions:Ljava/util/HashMap;

    const-string v1, "ViagemGlobalTravelCard"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "net_globaltravel"

    const/4 v4, 0x0

    aput-object v3, v2, v4

    const-string v3, "Global Travel Card"

    const/4 v4, 0x1

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 331
    sget-object v0, Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;->menuOptions:Ljava/util/HashMap;

    const-string v1, "SegurancaAplicativoComputador"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "itoken_no_aplicativo"

    const/4 v4, 0x0

    aput-object v3, v2, v4

    const-string v3, "iToken no Aplicativo"

    const/4 v4, 0x1

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 333
    sget-object v0, Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;->menuOptions:Ljava/util/HashMap;

    const-string v1, "SegurancaTouchID"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "touchid"

    const/4 v4, 0x0

    aput-object v3, v2, v4

    const-string v3, "touche id"

    const/4 v4, 0x1

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 335
    sget-object v0, Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;->menuOptions:Ljava/util/HashMap;

    const-string v1, "Informacoes"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "ajuda_e_feedback"

    const/4 v4, 0x0

    aput-object v3, v2, v4

    const-string v3, "informa\u00e7\u00f5es"

    const/4 v4, 0x1

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 337
    return-void
.end method

.method public static setRequestFocus(Landroid/view/View;)V
    .registers 6
    .param p0, "view"    # Landroid/view/View;

    .line 133
    new-instance v3, Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity$2;

    invoke-direct {v3, p0}, Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity$2;-><init>(Landroid/view/View;)V

    .line 140
    .local v3, "task":Ljava/lang/Runnable;
    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadScheduledExecutor()Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v4

    .line 141
    .local v4, "worker":Ljava/util/concurrent/ScheduledExecutorService;
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v1, 0x1

    invoke-interface {v4, v3, v1, v2, v0}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    .line 142
    return-void
.end method

.method private setToolbar(Ljava/lang/String;)V
    .registers 7
    .param p1, "toolbarTitle"    # Ljava/lang/String;

    .line 223
    sget v0, Lbr/com/itau/textovoz/R$id;->toolbar_library:I

    invoke-virtual {p0, v0}, Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Landroid/support/v7/widget/Toolbar;

    .line 224
    .local v2, "toolbar":Landroid/support/v7/widget/Toolbar;
    invoke-virtual {p0, v2}, Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;->setSupportActionBar(Landroid/support/v7/widget/Toolbar;)V

    .line 226
    invoke-virtual {p0}, Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v3

    .line 227
    .local v3, "actionBar":Landroid/support/v7/app/ActionBar;
    if-eqz v3, :cond_43

    .line 228
    const/4 v0, 0x1

    invoke-virtual {v3, v0}, Landroid/support/v7/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 229
    const/4 v0, 0x1

    invoke-virtual {v3, v0}, Landroid/support/v7/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 230
    invoke-virtual {v3, p1}, Landroid/support/v7/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    .line 234
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;->segment:Ljava/lang/String;

    invoke-static {v0}, Lbr/com/itau/textovoz/util/CommonUtils;->getSegment(Ljava/lang/String;)I

    move-result v0

    const/4 v1, 0x4

    if-ne v1, v0, :cond_2d

    .line 235
    sget v0, Lbr/com/itau/textovoz/R$xml;->arrow_person:I

    invoke-static {p0, v0}, Lbr/com/itau/textovoz/ui/view/FontIconDrawable;->inflate(Landroid/content/Context;I)Lbr/com/itau/textovoz/ui/view/FontIconDrawable;

    move-result-object v4

    .local v4, "fontIconDrawable":Lbr/com/itau/textovoz/ui/view/FontIconDrawable;
    goto :goto_33

    .line 239
    .end local v4    # "fontIconDrawable":Lbr/com/itau/textovoz/ui/view/FontIconDrawable;
    :cond_2d
    sget v0, Lbr/com/itau/textovoz/R$xml;->arrow_itau:I

    invoke-static {p0, v0}, Lbr/com/itau/textovoz/ui/view/FontIconDrawable;->inflate(Landroid/content/Context;I)Lbr/com/itau/textovoz/ui/view/FontIconDrawable;

    move-result-object v4

    .line 243
    .local v4, "fontIconDrawable":Lbr/com/itau/textovoz/ui/view/FontIconDrawable;
    :goto_33
    invoke-virtual {p0}, Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lbr/com/itau/textovoz/R$string;->content_description_back:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/support/v7/app/ActionBar;->setHomeActionContentDescription(Ljava/lang/CharSequence;)V

    .line 244
    invoke-virtual {v3, v4}, Landroid/support/v7/app/ActionBar;->setHomeAsUpIndicator(Landroid/graphics/drawable/Drawable;)V

    .line 246
    .end local v4    # "fontIconDrawable":Lbr/com/itau/textovoz/ui/view/FontIconDrawable;
    :cond_43
    return-void
.end method

.method private setVisibilityFeedBack()V
    .registers 3

    .line 127
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;->relativeAnswerFoot:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 128
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;->relativeAnswerYes:Landroid/widget/RelativeLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 129
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;->relativeAnswerYes:Landroid/widget/RelativeLayout;

    invoke-static {v0}, Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;->setRequestFocus(Landroid/view/View;)V

    .line 130
    return-void
.end method

.method private showChannels(Ljava/util/List;)V
    .registers 4
    .param p1, "channels"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<Lbr/com/itau/textovoz/model/Channel;>;)V"
        }
    .end annotation

    .line 192
    new-instance v0, Lbr/com/itau/textovoz/adapter/AttendanceChannelViewAdapter;

    invoke-direct {v0, p0, p1}, Lbr/com/itau/textovoz/adapter/AttendanceChannelViewAdapter;-><init>(Landroid/content/Context;Ljava/util/List;)V

    iput-object v0, p0, Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;->channelsExpandableListAdapter:Landroid/widget/ExpandableListAdapter;

    .line 193
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;->listChannels:Landroid/widget/ExpandableListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ExpandableListView;->setGroupIndicator(Landroid/graphics/drawable/Drawable;)V

    .line 194
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;->listChannels:Landroid/widget/ExpandableListView;

    iget-object v1, p0, Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;->channelsExpandableListAdapter:Landroid/widget/ExpandableListAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ExpandableListView;->setAdapter(Landroid/widget/ExpandableListAdapter;)V

    .line 196
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;->listChannels:Landroid/widget/ExpandableListView;

    new-instance v1, Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity$4;

    invoke-direct {v1, p0}, Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity$4;-><init>(Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ExpandableListView;->setOnGroupClickListener(Landroid/widget/ExpandableListView$OnGroupClickListener;)V

    .line 210
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;->listChannels:Landroid/widget/ExpandableListView;

    new-instance v1, Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity$5;

    invoke-direct {v1, p0}, Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity$5;-><init>(Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ExpandableListView;->setOnGroupExpandListener(Landroid/widget/ExpandableListView$OnGroupExpandListener;)V

    .line 220
    return-void
.end method

.method private showFaq(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/util/List;)V
    .registers 7
    .param p1, "faq"    # Ljava/lang/String;
    .param p2, "answer"    # Ljava/lang/String;
    .param p3, "links"    # Ljava/util/List;
    .param p4, "product"    # Ljava/lang/String;
    .param p5, "channels"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/lang/String;Ljava/lang/String;Ljava/util/List<Lbr/com/itau/textovoz/model/Link;>;Ljava/lang/String;Ljava/util/List<Lbr/com/itau/textovoz/model/Channel;>;)V"
        }
    .end annotation

    .line 118
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;->textFaq:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 119
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;->textFaq:Landroid/widget/TextView;

    invoke-virtual {v0, p4}, Landroid/widget/TextView;->setTag(Ljava/lang/Object;)V

    .line 120
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;->textAnswer:Landroid/widget/TextView;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 122
    invoke-direct {p0, p3}, Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;->showLinks(Ljava/util/List;)V

    .line 123
    invoke-direct {p0, p5}, Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;->showChannels(Ljava/util/List;)V

    .line 124
    return-void
.end method

.method private showLinks(Ljava/util/List;)V
    .registers 13
    .param p1, "links"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<Lbr/com/itau/textovoz/model/Link;>;)V"
        }
    .end annotation

    .line 145
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_f

    .line 146
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;->linearLinksContainer:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto/16 :goto_73

    .line 148
    :cond_f
    sget v0, Lbr/com/itau/textovoz/R$id;->linear_links_container_library:I

    invoke-virtual {p0, v0}, Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Landroid/widget/LinearLayout;

    .line 150
    .local v4, "ll":Landroid/widget/LinearLayout;
    invoke-direct {p0}, Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;->setHashMapLinks()V

    .line 152
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1f
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_73

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lbr/com/itau/textovoz/model/Link;

    .line 153
    .local v6, "linkItem":Lbr/com/itau/textovoz/model/Link;
    invoke-virtual {p0}, Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lbr/com/itau/textovoz/R$layout;->view_link_itens:I

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v7

    .line 154
    .local v7, "inflatedLayout":Landroid/view/View;
    new-instance v8, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v0, -0x1

    const/16 v1, 0x28

    invoke-direct {v8, v0, v1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 156
    .local v8, "param":Landroid/widget/LinearLayout$LayoutParams;
    new-instance v9, Landroid/widget/RelativeLayout;

    invoke-direct {v9, p0}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 157
    .local v9, "v":Landroid/widget/RelativeLayout;
    invoke-virtual {v9, v8}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 158
    sget v0, Lbr/com/itau/textovoz/R$id;->txt_about_link:I

    invoke-virtual {v7, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Landroid/widget/TextView;

    .line 160
    .local v10, "link":Landroid/widget/TextView;
    invoke-virtual {v6}, Lbr/com/itau/textovoz/model/Link;->getText()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v10, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 161
    const/4 v0, 0x1

    invoke-virtual {v10, v0}, Landroid/widget/TextView;->setClickable(Z)V

    .line 162
    invoke-virtual {v6}, Lbr/com/itau/textovoz/model/Link;->get_ref()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v10, v0}, Landroid/widget/TextView;->setTag(Ljava/lang/Object;)V

    .line 163
    new-instance v0, Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity$3;

    invoke-direct {v0, p0, v6}, Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity$3;-><init>(Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;Lbr/com/itau/textovoz/model/Link;)V

    invoke-virtual {v10, v0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 178
    invoke-virtual {v4, v7}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 179
    invoke-virtual {v4, v9}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 180
    .end local v6    # "linkItem":Lbr/com/itau/textovoz/model/Link;
    .end local v7    # "inflatedLayout":Landroid/view/View;
    .end local v8    # "param":Landroid/widget/LinearLayout$LayoutParams;
    .end local v9    # "v":Landroid/widget/RelativeLayout;
    .end local v10    # "link":Landroid/widget/TextView;
    goto/16 :goto_1f

    .line 182
    .end local v4    # "ll":Landroid/widget/LinearLayout;
    :cond_73
    :goto_73
    return-void
.end method


# virtual methods
.method public onBackPressed()V
    .registers 3

    .line 261
    invoke-virtual {p0}, Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 262
    .local v1, "intent":Landroid/content/Intent;
    const/4 v0, 0x0

    invoke-virtual {p0, v0, v1}, Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;->setResult(ILandroid/content/Intent;)V

    .line 263
    invoke-virtual {p0}, Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;->finish()V

    .line 264
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .registers 17
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 57
    move-object/from16 v0, p1

    invoke-super {p0, v0}, Lbr/com/itau/textovoz/ui/activity/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 58
    sget v0, Lbr/com/itau/textovoz/R$layout;->activity_faq:I

    invoke-virtual {p0, v0}, Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;->setContentView(I)V

    .line 60
    sget v0, Lbr/com/itau/textovoz/R$id;->list_channels_library:I

    invoke-virtual {p0, v0}, Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ExpandableListView;

    iput-object v0, p0, Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;->listChannels:Landroid/widget/ExpandableListView;

    .line 61
    sget v0, Lbr/com/itau/textovoz/R$id;->scroll_answer_library:I

    invoke-virtual {p0, v0}, Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/widget/NestedScrollView;

    iput-object v0, p0, Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;->scrollAnswer:Landroid/support/v4/widget/NestedScrollView;

    .line 62
    sget v0, Lbr/com/itau/textovoz/R$id;->text_answer_library:I

    invoke-virtual {p0, v0}, Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;->textAnswer:Landroid/widget/TextView;

    .line 63
    sget v0, Lbr/com/itau/textovoz/R$id;->relative_answer_foot_library:I

    invoke-virtual {p0, v0}, Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;->relativeAnswerFoot:Landroid/widget/LinearLayout;

    .line 64
    sget v0, Lbr/com/itau/textovoz/R$id;->relative_answer_yes_library:I

    invoke-virtual {p0, v0}, Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;->relativeAnswerYes:Landroid/widget/RelativeLayout;

    .line 65
    sget v0, Lbr/com/itau/textovoz/R$id;->linear_links_container_library:I

    invoke-virtual {p0, v0}, Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;->linearLinksContainer:Landroid/widget/LinearLayout;

    .line 66
    sget v0, Lbr/com/itau/textovoz/R$id;->text_faq_library:I

    invoke-virtual {p0, v0}, Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;->textFaq:Landroid/widget/TextView;

    .line 67
    sget v0, Lbr/com/itau/textovoz/R$id;->btn_feed_sim_library:I

    invoke-virtual {p0, v0}, Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Landroid/widget/Button;

    .line 68
    .local v6, "buttonFeedYes":Landroid/widget/Button;
    sget v0, Lbr/com/itau/textovoz/R$id;->btn_feed_nao_library:I

    invoke-virtual {p0, v0}, Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Landroid/widget/Button;

    .line 69
    .local v7, "buttonFeedNo":Landroid/widget/Button;
    sget v0, Lbr/com/itau/textovoz/R$id;->img_close_yes_library:I

    invoke-virtual {p0, v0}, Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Landroid/widget/ImageView;

    .line 71
    .local v8, "imgCloseYes":Landroid/widget/ImageView;
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;->onClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v7, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 72
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;->onClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v6, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 73
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;->onClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v8, v0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 74
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;->relativeAnswerYes:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;->onClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 76
    invoke-virtual {p0}, Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v9

    .line 77
    .local v9, "extras":Landroid/os/Bundle;
    if-eqz v9, :cond_f5

    .line 78
    const-string v0, "type"

    invoke-virtual {v9, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;->segment:Ljava/lang/String;

    .line 79
    const-string v0, "operator"

    invoke-virtual {v9, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;->operator:Ljava/lang/String;

    .line 80
    sget v0, Lbr/com/itau/textovoz/R$string;->links_bundle_item:I

    invoke-virtual {p0, v0}, Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v0}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v10

    .line 81
    .local v10, "links":Ljava/util/List;, "Ljava/util/List<Lbr/com/itau/textovoz/model/Link;>;"
    sget v0, Lbr/com/itau/textovoz/R$string;->channel_bundle_item:I

    invoke-virtual {p0, v0}, Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v0}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v11

    .line 82
    .local v11, "channels":Ljava/util/List;, "Ljava/util/List<Lbr/com/itau/textovoz/model/Channel;>;"
    sget v0, Lbr/com/itau/textovoz/R$string;->faq_bundle_item:I

    invoke-virtual {p0, v0}, Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 84
    .local v12, "faq":Ljava/lang/String;
    sget v0, Lbr/com/itau/textovoz/R$string;->answer_bundle_item:I

    invoke-virtual {p0, v0}, Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    .line 85
    .local v13, "bundleAnswer":Ljava/lang/String;
    const-string v14, ""

    .line 86
    .local v14, "answer":Ljava/lang/String;
    invoke-static {v13}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_d3

    .line 87
    const-string v0, "\\n"

    const-string v1, "\n"

    invoke-virtual {v13, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v14

    .line 90
    :cond_d3
    sget v0, Lbr/com/itau/textovoz/R$string;->attendance_filter:I

    invoke-virtual {p0, v0}, Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;->setToolbar(Ljava/lang/String;)V

    .line 91
    move-object v0, p0

    move-object v1, v12

    move-object v2, v14

    move-object v3, v10

    sget v4, Lbr/com/itau/textovoz/R$string;->attendance_filter:I

    invoke-virtual {p0, v4}, Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object v5, v11

    invoke-direct/range {v0 .. v5}, Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;->showFaq(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/util/List;)V

    .line 93
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;->textAnswer:Landroid/widget/TextView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setFocusable(Z)V

    .line 94
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;->textAnswer:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->requestFocus()Z

    .line 96
    .end local v10    # "links":Ljava/util/List;, "Ljava/util/List<Lbr/com/itau/textovoz/model/Link;>;"
    .end local v10
    .end local v11    # "channels":Ljava/util/List;, "Ljava/util/List<Lbr/com/itau/textovoz/model/Channel;>;"
    .end local v11
    .end local v12    # "faq":Ljava/lang/String;
    .end local v13    # "bundleAnswer":Ljava/lang/String;
    .end local v14    # "answer":Ljava/lang/String;
    :cond_f5
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .registers 3
    .param p1, "item"    # Landroid/view/MenuItem;

    .line 250
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sparse-switch v0, :sswitch_data_12

    goto :goto_d

    .line 252
    :sswitch_8
    invoke-virtual {p0}, Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;->onBackPressed()V

    .line 253
    const/4 v0, 0x1

    return v0

    .line 255
    :goto_d
    invoke-super {p0, p1}, Lbr/com/itau/textovoz/ui/activity/BaseActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    :sswitch_data_12
    .sparse-switch
        0x102002c -> :sswitch_8
    .end sparse-switch
.end method

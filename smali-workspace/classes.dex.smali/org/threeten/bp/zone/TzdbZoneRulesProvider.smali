.class public final Lorg/threeten/bp/zone/TzdbZoneRulesProvider;
.super Lorg/threeten/bp/zone/ZoneRulesProvider;
.source "TzdbZoneRulesProvider.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/threeten/bp/zone/TzdbZoneRulesProvider$Version;
    }
.end annotation


# instance fields
.field private loadedUrls:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<Ljava/lang/String;>;"
        }
    .end annotation
.end field

.field private final regionIds:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<Ljava/lang/String;>;"
        }
    .end annotation
.end field

.field private final versions:Ljava/util/concurrent/ConcurrentNavigableMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentNavigableMap<Ljava/lang/String;Lorg/threeten/bp/zone/TzdbZoneRulesProvider$Version;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 3

    .line 86
    invoke-direct {p0}, Lorg/threeten/bp/zone/ZoneRulesProvider;-><init>()V

    .line 68
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;-><init>()V

    iput-object v0, p0, Lorg/threeten/bp/zone/TzdbZoneRulesProvider;->regionIds:Ljava/util/Set;

    .line 72
    new-instance v0, Ljava/util/concurrent/ConcurrentSkipListMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentSkipListMap;-><init>()V

    iput-object v0, p0, Lorg/threeten/bp/zone/TzdbZoneRulesProvider;->versions:Ljava/util/concurrent/ConcurrentNavigableMap;

    .line 77
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;-><init>()V

    iput-object v0, p0, Lorg/threeten/bp/zone/TzdbZoneRulesProvider;->loadedUrls:Ljava/util/Set;

    .line 87
    const-class v0, Lorg/threeten/bp/zone/ZoneRulesProvider;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/threeten/bp/zone/TzdbZoneRulesProvider;->load(Ljava/lang/ClassLoader;)Z

    move-result v0

    if-nez v0, :cond_2c

    .line 88
    new-instance v0, Lorg/threeten/bp/zone/ZoneRulesException;

    const-string v1, "No time-zone rules found for \'TZDB\'"

    invoke-direct {v0, v1}, Lorg/threeten/bp/zone/ZoneRulesException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 90
    :cond_2c
    return-void
.end method

.method public constructor <init>(Ljava/io/InputStream;)V
    .registers 5
    .param p1, "stream"    # Ljava/io/InputStream;

    .line 120
    invoke-direct {p0}, Lorg/threeten/bp/zone/ZoneRulesProvider;-><init>()V

    .line 68
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;-><init>()V

    iput-object v0, p0, Lorg/threeten/bp/zone/TzdbZoneRulesProvider;->regionIds:Ljava/util/Set;

    .line 72
    new-instance v0, Ljava/util/concurrent/ConcurrentSkipListMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentSkipListMap;-><init>()V

    iput-object v0, p0, Lorg/threeten/bp/zone/TzdbZoneRulesProvider;->versions:Ljava/util/concurrent/ConcurrentNavigableMap;

    .line 77
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;-><init>()V

    iput-object v0, p0, Lorg/threeten/bp/zone/TzdbZoneRulesProvider;->loadedUrls:Ljava/util/Set;

    .line 122
    :try_start_18
    invoke-direct {p0, p1}, Lorg/threeten/bp/zone/TzdbZoneRulesProvider;->load(Ljava/io/InputStream;)Z
    :try_end_1b
    .catch Ljava/lang/Exception; {:try_start_18 .. :try_end_1b} :catch_1c

    .line 125
    goto :goto_25

    .line 123
    :catch_1c
    move-exception v2

    .line 124
    .local v2, "ex":Ljava/lang/Exception;
    new-instance v0, Lorg/threeten/bp/zone/ZoneRulesException;

    const-string v1, "Unable to load TZDB time-zone rules"

    invoke-direct {v0, v1, v2}, Lorg/threeten/bp/zone/ZoneRulesException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0

    .line 126
    .end local v2    # "ex":Ljava/lang/Exception;
    :goto_25
    return-void
.end method

.method private load(Ljava/io/InputStream;)Z
    .registers 10
    .param p1, "in"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/io/StreamCorruptedException;
        }
    .end annotation

    .line 211
    const/4 v3, 0x0

    .line 212
    .local v3, "updated":Z
    invoke-direct {p0, p1}, Lorg/threeten/bp/zone/TzdbZoneRulesProvider;->loadData(Ljava/io/InputStream;)Ljava/lang/Iterable;

    move-result-object v4

    .line 213
    .local v4, "loadedVersions":Ljava/lang/Iterable;, "Ljava/lang/Iterable<Lorg/threeten/bp/zone/TzdbZoneRulesProvider$Version;>;"
    invoke-interface {v4}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .local v5, "i$":Ljava/util/Iterator;
    :goto_9
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_52

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lorg/threeten/bp/zone/TzdbZoneRulesProvider$Version;

    .line 216
    .local v6, "loadedVersion":Lorg/threeten/bp/zone/TzdbZoneRulesProvider$Version;
    iget-object v0, p0, Lorg/threeten/bp/zone/TzdbZoneRulesProvider;->versions:Ljava/util/concurrent/ConcurrentNavigableMap;

    # getter for: Lorg/threeten/bp/zone/TzdbZoneRulesProvider$Version;->versionId:Ljava/lang/String;
    invoke-static {v6}, Lorg/threeten/bp/zone/TzdbZoneRulesProvider$Version;->access$000(Lorg/threeten/bp/zone/TzdbZoneRulesProvider$Version;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, v6}, Ljava/util/concurrent/ConcurrentNavigableMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lorg/threeten/bp/zone/TzdbZoneRulesProvider$Version;

    .line 217
    .local v7, "existing":Lorg/threeten/bp/zone/TzdbZoneRulesProvider$Version;
    if-eqz v7, :cond_50

    # getter for: Lorg/threeten/bp/zone/TzdbZoneRulesProvider$Version;->versionId:Ljava/lang/String;
    invoke-static {v7}, Lorg/threeten/bp/zone/TzdbZoneRulesProvider$Version;->access$000(Lorg/threeten/bp/zone/TzdbZoneRulesProvider$Version;)Ljava/lang/String;

    move-result-object v0

    # getter for: Lorg/threeten/bp/zone/TzdbZoneRulesProvider$Version;->versionId:Ljava/lang/String;
    invoke-static {v6}, Lorg/threeten/bp/zone/TzdbZoneRulesProvider$Version;->access$000(Lorg/threeten/bp/zone/TzdbZoneRulesProvider$Version;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_50

    .line 218
    new-instance v0, Lorg/threeten/bp/zone/ZoneRulesException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Data already loaded for TZDB time-zone rules version: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    # getter for: Lorg/threeten/bp/zone/TzdbZoneRulesProvider$Version;->versionId:Ljava/lang/String;
    invoke-static {v6}, Lorg/threeten/bp/zone/TzdbZoneRulesProvider$Version;->access$000(Lorg/threeten/bp/zone/TzdbZoneRulesProvider$Version;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/threeten/bp/zone/ZoneRulesException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 220
    :cond_50
    const/4 v3, 0x1

    .line 221
    .end local v6    # "loadedVersion":Lorg/threeten/bp/zone/TzdbZoneRulesProvider$Version;
    .end local v7    # "existing":Lorg/threeten/bp/zone/TzdbZoneRulesProvider$Version;
    goto :goto_9

    .line 222
    .end local v5    # "i$":Ljava/util/Iterator;
    :cond_52
    return v3
.end method

.method private load(Ljava/lang/ClassLoader;)Z
    .registers 8
    .param p1, "classLoader"    # Ljava/lang/ClassLoader;

    .line 165
    const/4 v3, 0x0

    .line 166
    .local v3, "updated":Z
    const/4 v4, 0x0

    .line 168
    .local v4, "url":Ljava/net/URL;
    const-string v0, "org/threeten/bp/TZDB.dat"

    :try_start_4
    invoke-virtual {p1, v0}, Ljava/lang/ClassLoader;->getResources(Ljava/lang/String;)Ljava/util/Enumeration;

    move-result-object v5

    .line 169
    .local v5, "en":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/net/URL;>;"
    :goto_8
    invoke-interface {v5}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_1b

    .line 170
    invoke-interface {v5}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/net/URL;

    move-object v4, v0

    .line 171
    invoke-direct {p0, v4}, Lorg/threeten/bp/zone/TzdbZoneRulesProvider;->load(Ljava/net/URL;)Z
    :try_end_18
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_18} :catch_1c

    move-result v0

    or-int/2addr v3, v0

    goto :goto_8

    .line 175
    .end local v5    # "en":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/net/URL;>;"
    .end local v5
    :cond_1b
    goto :goto_36

    .line 173
    :catch_1c
    move-exception v5

    .line 174
    .local v5, "ex":Ljava/lang/Exception;
    new-instance v0, Lorg/threeten/bp/zone/ZoneRulesException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unable to load TZDB time-zone rules: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, v5}, Lorg/threeten/bp/zone/ZoneRulesException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0

    .line 176
    .end local v5    # "ex":Ljava/lang/Exception;
    :goto_36
    return v3
.end method

.method private load(Ljava/net/URL;)Z
    .registers 7
    .param p1, "url"    # Ljava/net/URL;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/ClassNotFoundException;,
            Ljava/io/IOException;,
            Lorg/threeten/bp/zone/ZoneRulesException;
        }
    .end annotation

    .line 189
    const/4 v2, 0x0

    .line 190
    .local v2, "updated":Z
    iget-object v0, p0, Lorg/threeten/bp/zone/TzdbZoneRulesProvider;->loadedUrls:Ljava/util/Set;

    invoke-virtual {p1}, Ljava/net/URL;->toExternalForm()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_26

    .line 191
    const/4 v3, 0x0

    .line 193
    .local v3, "in":Ljava/io/InputStream;
    :try_start_e
    invoke-virtual {p1}, Ljava/net/URL;->openStream()Ljava/io/InputStream;

    move-result-object v0

    move-object v3, v0

    .line 194
    invoke-direct {p0, v3}, Lorg/threeten/bp/zone/TzdbZoneRulesProvider;->load(Ljava/io/InputStream;)Z
    :try_end_16
    .catchall {:try_start_e .. :try_end_16} :catchall_1f

    move-result v0

    or-int/lit8 v2, v0, 0x0

    .line 196
    if-eqz v3, :cond_26

    .line 197
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V

    goto :goto_26

    .line 196
    :catchall_1f
    move-exception v4

    if-eqz v3, :cond_25

    .line 197
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V

    :cond_25
    throw v4

    .line 201
    .end local v3    # "in":Ljava/io/InputStream;
    :cond_26
    :goto_26
    return v2
.end method

.method private loadData(Ljava/io/InputStream;)Ljava/lang/Iterable;
    .registers 21
    .param p1, "in"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/io/InputStream;)Ljava/lang/Iterable<Lorg/threeten/bp/zone/TzdbZoneRulesProvider$Version;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/io/StreamCorruptedException;
        }
    .end annotation

    .line 232
    new-instance v4, Ljava/io/DataInputStream;

    move-object/from16 v0, p1

    invoke-direct {v4, v0}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    .line 233
    .local v4, "dis":Ljava/io/DataInputStream;
    invoke-virtual {v4}, Ljava/io/DataInputStream;->readByte()B

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_16

    .line 234
    new-instance v0, Ljava/io/StreamCorruptedException;

    const-string v1, "File format not recognised"

    invoke-direct {v0, v1}, Ljava/io/StreamCorruptedException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 237
    :cond_16
    invoke-virtual {v4}, Ljava/io/DataInputStream;->readUTF()Ljava/lang/String;

    move-result-object v5

    .line 238
    .local v5, "groupId":Ljava/lang/String;
    const-string v0, "TZDB"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2a

    .line 239
    new-instance v0, Ljava/io/StreamCorruptedException;

    const-string v1, "File format not recognised"

    invoke-direct {v0, v1}, Ljava/io/StreamCorruptedException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 242
    :cond_2a
    invoke-virtual {v4}, Ljava/io/DataInputStream;->readShort()S

    move-result v6

    .line 243
    .local v6, "versionCount":I
    new-array v7, v6, [Ljava/lang/String;

    .line 244
    .local v7, "versionArray":[Ljava/lang/String;
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_31
    if-ge v8, v6, :cond_3c

    .line 245
    invoke-virtual {v4}, Ljava/io/DataInputStream;->readUTF()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v7, v8

    .line 244
    add-int/lit8 v8, v8, 0x1

    goto :goto_31

    .line 248
    .end local v8    # "i":I
    :cond_3c
    invoke-virtual {v4}, Ljava/io/DataInputStream;->readShort()S

    move-result v8

    .line 249
    .local v8, "regionCount":I
    new-array v9, v8, [Ljava/lang/String;

    .line 250
    .local v9, "regionArray":[Ljava/lang/String;
    const/4 v10, 0x0

    .local v10, "i":I
    :goto_43
    if-ge v10, v8, :cond_4e

    .line 251
    invoke-virtual {v4}, Ljava/io/DataInputStream;->readUTF()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v9, v10

    .line 250
    add-int/lit8 v10, v10, 0x1

    goto :goto_43

    .line 253
    .end local v10    # "i":I
    :cond_4e
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/threeten/bp/zone/TzdbZoneRulesProvider;->regionIds:Ljava/util/Set;

    invoke-static {v9}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 255
    invoke-virtual {v4}, Ljava/io/DataInputStream;->readShort()S

    move-result v10

    .line 256
    .local v10, "ruleCount":I
    new-array v11, v10, [Ljava/lang/Object;

    .line 257
    .local v11, "ruleArray":[Ljava/lang/Object;
    const/4 v12, 0x0

    .local v12, "i":I
    :goto_60
    if-ge v12, v10, :cond_70

    .line 258
    invoke-virtual {v4}, Ljava/io/DataInputStream;->readShort()S

    move-result v0

    new-array v13, v0, [B

    .line 259
    .local v13, "bytes":[B
    invoke-virtual {v4, v13}, Ljava/io/DataInputStream;->readFully([B)V

    .line 260
    aput-object v13, v11, v12

    .line 257
    .end local v13    # "bytes":[B
    add-int/lit8 v12, v12, 0x1

    goto :goto_60

    .line 262
    .end local v12    # "i":I
    :cond_70
    new-instance v12, Ljava/util/concurrent/atomic/AtomicReferenceArray;

    invoke-direct {v12, v11}, Ljava/util/concurrent/atomic/AtomicReferenceArray;-><init>([Ljava/lang/Object;)V

    .line 264
    .local v12, "ruleData":Ljava/util/concurrent/atomic/AtomicReferenceArray;, "Ljava/util/concurrent/atomic/AtomicReferenceArray<Ljava/lang/Object;>;"
    new-instance v13, Ljava/util/HashSet;

    invoke-direct {v13, v6}, Ljava/util/HashSet;-><init>(I)V

    .line 265
    .local v13, "versionSet":Ljava/util/Set;, "Ljava/util/Set<Lorg/threeten/bp/zone/TzdbZoneRulesProvider$Version;>;"
    const/4 v14, 0x0

    .local v14, "i":I
    :goto_7b
    if-ge v14, v6, :cond_b1

    .line 266
    invoke-virtual {v4}, Ljava/io/DataInputStream;->readShort()S

    move-result v15

    .line 267
    .local v15, "versionRegionCount":I
    new-array v0, v15, [Ljava/lang/String;

    move-object/from16 v16, v0

    .line 268
    .local v16, "versionRegionArray":[Ljava/lang/String;
    new-array v0, v15, [S

    move-object/from16 v17, v0

    .line 269
    .local v17, "versionRulesArray":[S
    const/16 v18, 0x0

    .local v18, "j":I
    :goto_8b
    move/from16 v0, v18

    if-ge v0, v15, :cond_a0

    .line 270
    invoke-virtual {v4}, Ljava/io/DataInputStream;->readShort()S

    move-result v0

    aget-object v0, v9, v0

    aput-object v0, v16, v18

    .line 271
    invoke-virtual {v4}, Ljava/io/DataInputStream;->readShort()S

    move-result v0

    aput-short v0, v17, v18

    .line 269
    add-int/lit8 v18, v18, 0x1

    goto :goto_8b

    .line 273
    .end local v18    # "j":I
    :cond_a0
    new-instance v0, Lorg/threeten/bp/zone/TzdbZoneRulesProvider$Version;

    aget-object v1, v7, v14

    move-object/from16 v2, v16

    move-object/from16 v3, v17

    invoke-direct {v0, v1, v2, v3, v12}, Lorg/threeten/bp/zone/TzdbZoneRulesProvider$Version;-><init>(Ljava/lang/String;[Ljava/lang/String;[SLjava/util/concurrent/atomic/AtomicReferenceArray;)V

    invoke-interface {v13, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 265
    .end local v15    # "versionRegionCount":I
    .end local v16    # "versionRegionArray":[Ljava/lang/String;
    .end local v17    # "versionRulesArray":[S
    add-int/lit8 v14, v14, 0x1

    goto :goto_7b

    .line 275
    .end local v14    # "i":I
    :cond_b1
    return-object v13
.end method


# virtual methods
.method protected provideRules(Ljava/lang/String;Z)Lorg/threeten/bp/zone/ZoneRules;
    .registers 7
    .param p1, "zoneId"    # Ljava/lang/String;
    .param p2, "forCaching"    # Z

    .line 136
    const-string v0, "zoneId"

    invoke-static {p1, v0}, Lorg/threeten/bp/jdk8/Jdk8Methods;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 137
    iget-object v0, p0, Lorg/threeten/bp/zone/TzdbZoneRulesProvider;->versions:Ljava/util/concurrent/ConcurrentNavigableMap;

    invoke-interface {v0}, Ljava/util/concurrent/ConcurrentNavigableMap;->lastEntry()Ljava/util/Map$Entry;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/threeten/bp/zone/TzdbZoneRulesProvider$Version;

    invoke-virtual {v0, p1}, Lorg/threeten/bp/zone/TzdbZoneRulesProvider$Version;->getRules(Ljava/lang/String;)Lorg/threeten/bp/zone/ZoneRules;

    move-result-object v3

    .line 138
    .local v3, "rules":Lorg/threeten/bp/zone/ZoneRules;
    if-nez v3, :cond_30

    .line 139
    new-instance v0, Lorg/threeten/bp/zone/ZoneRulesException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown time-zone ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/threeten/bp/zone/ZoneRulesException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 141
    :cond_30
    return-object v3
.end method

.method protected provideZoneIds()Ljava/util/Set;
    .registers 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/Set<Ljava/lang/String;>;"
        }
    .end annotation

    .line 131
    new-instance v0, Ljava/util/HashSet;

    iget-object v1, p0, Lorg/threeten/bp/zone/TzdbZoneRulesProvider;->regionIds:Ljava/util/Set;

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .registers 2

    .line 280
    const-string v0, "TZDB"

    return-object v0
.end method

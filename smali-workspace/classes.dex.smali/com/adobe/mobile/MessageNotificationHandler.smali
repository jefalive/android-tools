.class public Lcom/adobe/mobile/MessageNotificationHandler;
.super Landroid/content/BroadcastReceiver;
.source "MessageNotificationHandler.java"


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 45
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method private getAppName()Ljava/lang/String;
    .registers 8

    .line 225
    const-string v4, ""

    .line 227
    .local v4, "appName":Ljava/lang/String;
    :try_start_2
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getSharedContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    .line 228
    .local v5, "packageManager":Landroid/content/pm/PackageManager;
    if-eqz v5, :cond_2b

    .line 229
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getSharedContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v5, v0, v1}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v6

    .line 230
    .local v6, "applicationInfo":Landroid/content/pm/ApplicationInfo;
    if-eqz v6, :cond_2b

    .line 231
    invoke-virtual {v5, v6}, Landroid/content/pm/PackageManager;->getApplicationLabel(Landroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;

    move-result-object v0

    if-eqz v0, :cond_28

    .line 232
    invoke-virtual {v5, v6}, Landroid/content/pm/PackageManager;->getApplicationLabel(Landroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Ljava/lang/String;
    :try_end_27
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_2 .. :try_end_27} :catch_2c
    .catch Lcom/adobe/mobile/StaticMethods$NullContextException; {:try_start_2 .. :try_end_27} :catch_3d

    goto :goto_2a

    :cond_28
    const-string v0, ""

    :goto_2a
    move-object v4, v0

    .line 241
    .end local v5    # "packageManager":Landroid/content/pm/PackageManager;
    .end local v6    # "applicationInfo":Landroid/content/pm/ApplicationInfo;
    :cond_2b
    goto :goto_4d

    .line 236
    :catch_2c
    move-exception v5

    .line 237
    .local v5, "ex":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v0, "Messages - unable to retrieve app name for local notification (%s)"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v5}, Landroid/content/pm/PackageManager$NameNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logDebugFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 241
    .end local v5    # "ex":Landroid/content/pm/PackageManager$NameNotFoundException;
    goto :goto_4d

    .line 238
    :catch_3d
    move-exception v5

    .line 239
    .local v5, "e":Lcom/adobe/mobile/StaticMethods$NullContextException;
    const-string v0, "Messages - unable to get app name (%s)"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v5}, Lcom/adobe/mobile/StaticMethods$NullContextException;->getMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logDebugFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 243
    .end local v5    # "e":Lcom/adobe/mobile/StaticMethods$NullContextException;
    :goto_4d
    return-object v4
.end method

.method private getLargeIcon()Landroid/graphics/Bitmap;
    .registers 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/ClassNotFoundException;,
            Ljava/lang/NoSuchMethodException;,
            Lcom/adobe/mobile/StaticMethods$NullContextException;,
            Ljava/lang/IllegalAccessException;,
            Ljava/lang/reflect/InvocationTargetException;
        }
    .end annotation

    .line 251
    const/4 v5, 0x0

    .line 254
    .local v5, "iconDrawable":Landroid/graphics/drawable/Drawable;
    invoke-static {}, Lcom/adobe/mobile/Messages;->getLargeIconResourceId()I

    move-result v6

    .line 255
    .local v6, "largeIconResourceId":I
    const/4 v0, -0x1

    if-eq v6, v0, :cond_6a

    .line 256
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getSharedContext()Landroid/content/Context;

    move-result-object v7

    .line 257
    .local v7, "ctx":Landroid/content/Context;
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x14

    if-le v0, v1, :cond_44

    .line 258
    const-class v0, Landroid/content/res/Resources;

    const-string v1, "getDrawable"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Class;

    sget-object v3, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    const/4 v4, 0x0

    aput-object v3, v2, v4

    const-class v3, Landroid/content/res/Resources$Theme;

    const/4 v4, 0x1

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v8

    .line 259
    .local v8, "__getDrawable":Ljava/lang/reflect/Method;
    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-virtual {v7}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v2

    const/4 v3, 0x1

    aput-object v2, v1, v3

    invoke-virtual {v8, v0, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Landroid/graphics/drawable/Drawable;

    .line 260
    .end local v8    # "__getDrawable":Ljava/lang/reflect/Method;
    goto :goto_69

    .line 261
    :cond_44
    const-class v0, Landroid/content/res/Resources;

    const-string v1, "getDrawable"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    sget-object v3, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    const/4 v4, 0x0

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v8

    .line 262
    .local v8, "__getDrawable":Ljava/lang/reflect/Method;
    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-virtual {v8, v0, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Landroid/graphics/drawable/Drawable;

    .line 264
    .end local v7    # "ctx":Landroid/content/Context;
    .end local v8    # "__getDrawable":Ljava/lang/reflect/Method;
    :goto_69
    goto :goto_82

    .line 267
    :cond_6a
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getSharedContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v7

    .line 268
    .local v7, "applicationInfo":Landroid/content/pm/ApplicationInfo;
    if-eqz v7, :cond_82

    .line 269
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getSharedContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v8

    .line 270
    .local v8, "packageManager":Landroid/content/pm/PackageManager;
    if-eqz v8, :cond_82

    .line 271
    invoke-virtual {v8, v7}, Landroid/content/pm/PackageManager;->getApplicationIcon(Landroid/content/pm/ApplicationInfo;)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    .line 276
    .end local v7    # "applicationInfo":Landroid/content/pm/ApplicationInfo;
    .end local v8    # "packageManager":Landroid/content/pm/PackageManager;
    :cond_82
    :goto_82
    const/4 v7, 0x0

    .line 277
    .local v7, "icon":Landroid/graphics/Bitmap;
    if-eqz v5, :cond_8c

    .line 278
    move-object v0, v5

    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v7

    .line 281
    :cond_8c
    return-object v7
.end method

.method private getSmallIcon()I
    .registers 3

    .line 247
    invoke-static {}, Lcom/adobe/mobile/Messages;->getSmallIconResourceId()I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_c

    invoke-static {}, Lcom/adobe/mobile/Messages;->getSmallIconResourceId()I

    move-result v0

    goto :goto_f

    :cond_c
    const v0, 0x1080093

    :goto_f
    return v0
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 33
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .line 49
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v5

    .line 51
    .local v5, "bundle":Landroid/os/Bundle;
    if-nez v5, :cond_f

    .line 52
    const-string v0, "Messages - unable to load extras from local notification intent"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logDebugFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 53
    return-void

    .line 64
    :cond_f
    const-string v0, "alarm_message"

    :try_start_11
    invoke-virtual {v5, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 65
    .local v6, "message":Ljava/lang/String;
    const-string v0, "adbMessageCode"

    invoke-virtual {v5, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v10

    .line 66
    .local v10, "requestCode":I
    const-string v0, "requestCode"

    invoke-virtual {v5, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v11

    .line 67
    .local v11, "otherRequestCode":I
    const-string v0, "adb_m_l_id"

    invoke-virtual {v5, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 68
    .local v7, "messageID":Ljava/lang/String;
    const-string v0, "adb_deeplink"

    invoke-virtual {v5, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 69
    .local v8, "deeplink":Ljava/lang/String;
    const-string v0, "userData"

    invoke-virtual {v5, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_32
    .catch Ljava/lang/Exception; {:try_start_11 .. :try_end_32} :catch_34

    move-result-object v9

    .line 74
    .local v9, "userInfoStringJSON":Ljava/lang/String;
    goto :goto_45

    .line 71
    .end local v6    # "message":Ljava/lang/String;
    .end local v7    # "messageID":Ljava/lang/String;
    .end local v8    # "deeplink":Ljava/lang/String;
    .end local v9    # "userInfoStringJSON":Ljava/lang/String;
    .end local v10    # "requestCode":I
    .end local v11    # "otherRequestCode":I
    :catch_34
    move-exception v12

    .line 72
    .local v12, "ex":Ljava/lang/Exception;
    const-string v0, "Messages - unable to load message from local notification (%s)"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v12}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logDebugFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 73
    return-void

    .line 77
    .local v6, "message":Ljava/lang/String;
    .local v7, "messageID":Ljava/lang/String;
    .local v8, "deeplink":Ljava/lang/String;
    .local v9, "userInfoStringJSON":Ljava/lang/String;
    .local v10, "requestCode":I
    .local v11, "otherRequestCode":I
    .end local v12    # "ex":Ljava/lang/Exception;
    :goto_45
    sget-object v0, Lcom/adobe/mobile/Messages;->MESSAGE_LOCAL_IDENTIFIER:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eq v10, v0, :cond_4e

    .line 78
    return-void

    .line 82
    :cond_4e
    if-nez v6, :cond_59

    .line 83
    const-string v0, "Messages - local notification message was empty "

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logDebugFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 84
    return-void

    .line 89
    :cond_59
    :try_start_59
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getCurrentActivity()Landroid/app/Activity;
    :try_end_5c
    .catch Lcom/adobe/mobile/StaticMethods$NullActivityException; {:try_start_59 .. :try_end_5c} :catch_5e

    move-result-object v12

    .line 94
    .local v12, "currentActivity":Landroid/app/Activity;
    goto :goto_6a

    .line 91
    .end local v12    # "currentActivity":Landroid/app/Activity;
    :catch_5e
    move-exception v13

    .line 92
    .local v13, "ex":Lcom/adobe/mobile/StaticMethods$NullActivityException;
    invoke-virtual {v13}, Lcom/adobe/mobile/StaticMethods$NullActivityException;->getMessage()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logErrorFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 93
    return-void

    .line 98
    .local v12, "currentActivity":Landroid/app/Activity;
    .end local v13    # "ex":Lcom/adobe/mobile/StaticMethods$NullActivityException;
    :goto_6a
    :try_start_6a
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getSharedContext()Landroid/content/Context;
    :try_end_6d
    .catch Lcom/adobe/mobile/StaticMethods$NullContextException; {:try_start_6a .. :try_end_6d} :catch_6f

    move-result-object v13

    .line 103
    .local v13, "sharedContext":Landroid/content/Context;
    goto :goto_7b

    .line 100
    .end local v13    # "sharedContext":Landroid/content/Context;
    :catch_6f
    move-exception v14

    .line 101
    .local v14, "ex":Lcom/adobe/mobile/StaticMethods$NullContextException;
    invoke-virtual {v14}, Lcom/adobe/mobile/StaticMethods$NullContextException;->getMessage()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logErrorFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 102
    return-void

    .line 105
    .local v13, "sharedContext":Landroid/content/Context;
    .end local v14    # "ex":Lcom/adobe/mobile/StaticMethods$NullContextException;
    :goto_7b
    const/4 v14, 0x0

    .line 107
    .local v14, "notificationActivity":Landroid/app/Activity;
    :try_start_7c
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getCurrentActivity()Landroid/app/Activity;
    :try_end_7f
    .catch Lcom/adobe/mobile/StaticMethods$NullActivityException; {:try_start_7c .. :try_end_7f} :catch_82

    move-result-object v0

    move-object v14, v0

    .line 111
    goto :goto_8b

    .line 109
    :catch_82
    move-exception v15

    .line 110
    .local v15, "ex":Lcom/adobe/mobile/StaticMethods$NullActivityException;
    const-string v0, "Messages - unable to find activity for your notification, using default"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logErrorFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 115
    .end local v15    # "ex":Lcom/adobe/mobile/StaticMethods$NullActivityException;
    :goto_8b
    if-eqz v8, :cond_a2

    invoke-virtual {v8}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_a2

    .line 116
    new-instance v15, Landroid/content/Intent;

    const-string v0, "android.intent.action.VIEW"

    invoke-direct {v15, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 117
    .local v15, "resumeIntent":Landroid/content/Intent;
    invoke-static {v8}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v15, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    goto :goto_ab

    .line 118
    .end local v15    # "resumeIntent":Landroid/content/Intent;
    :cond_a2
    if-eqz v14, :cond_a9

    .line 119
    invoke-virtual {v14}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v15

    .local v15, "resumeIntent":Landroid/content/Intent;
    goto :goto_ab

    .line 121
    .end local v15    # "resumeIntent":Landroid/content/Intent;
    :cond_a9
    move-object/from16 v15, p2

    .line 124
    .local v15, "resumeIntent":Landroid/content/Intent;
    :goto_ab
    const/high16 v0, 0x24000000

    invoke-virtual {v15, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 125
    const-string v0, "adb_m_l_id"

    invoke-virtual {v15, v0, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 126
    const-string v0, "userData"

    invoke-virtual {v15, v0, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 131
    sget v16, Landroid/os/Build$VERSION;->SDK_INT:I

    .line 134
    .local v16, "buildVersion":I
    const/high16 v0, 0x8000000

    :try_start_be
    invoke-static {v13, v11, v15, v0}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v17

    .line 141
    .local v17, "sender":Landroid/app/PendingIntent;
    if-nez v17, :cond_cd

    .line 142
    const-string v0, "Messages - could not retrieve sender from broadcast, unable to post notification"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logDebugFormat(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_cc
    .catch Ljava/lang/ClassNotFoundException; {:try_start_be .. :try_end_cc} :catch_279
    .catch Ljava/lang/NoSuchMethodException; {:try_start_be .. :try_end_cc} :catch_28a
    .catch Lcom/adobe/mobile/StaticMethods$NullContextException; {:try_start_be .. :try_end_cc} :catch_29b
    .catch Ljava/lang/Exception; {:try_start_be .. :try_end_cc} :catch_2ac

    .line 143
    return-void

    .line 147
    :cond_cd
    move/from16 v0, v16

    const/16 v1, 0xb

    if-lt v0, v1, :cond_1ff

    .line 148
    :try_start_d3
    const-class v0, Landroid/content/BroadcastReceiver;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v19

    .line 149
    .local v19, "classLoader":Ljava/lang/ClassLoader;
    const-string v0, "android.app.Notification$Builder"

    move-object/from16 v1, v19

    invoke-virtual {v1, v0}, Ljava/lang/ClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v20

    .line 151
    .local v20, "notificationBuilderClass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Class;

    const-class v1, Landroid/content/Context;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    move-object/from16 v1, v20

    invoke-virtual {v1, v0}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v21

    .line 152
    .local v21, "notificationConstructor":Ljava/lang/reflect/Constructor;, "Ljava/lang/reflect/Constructor<*>;"
    move-object/from16 v0, v21

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Constructor;->setAccessible(Z)V

    .line 153
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getSharedContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    move-object/from16 v1, v21

    invoke-virtual {v1, v0}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v22

    .line 157
    .local v22, "notificationBuilder":Ljava/lang/Object;
    const-string v0, "setSmallIcon"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Class;

    sget-object v2, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    move-object/from16 v2, v20

    invoke-virtual {v2, v0, v1}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v23

    .line 158
    .local v23, "setSmallIcon":Ljava/lang/reflect/Method;
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    invoke-direct/range {p0 .. p0}, Lcom/adobe/mobile/MessageNotificationHandler;->getSmallIcon()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    move-object/from16 v1, v23

    move-object/from16 v2, v22

    invoke-virtual {v1, v2, v0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 161
    const-string v0, "setLargeIcon"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Class;

    const-class v2, Landroid/graphics/Bitmap;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    move-object/from16 v2, v20

    invoke-virtual {v2, v0, v1}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v24

    .line 162
    .local v24, "setLargeIcon":Ljava/lang/reflect/Method;
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    invoke-direct/range {p0 .. p0}, Lcom/adobe/mobile/MessageNotificationHandler;->getLargeIcon()Landroid/graphics/Bitmap;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    move-object/from16 v1, v24

    move-object/from16 v2, v22

    invoke-virtual {v1, v2, v0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 165
    const-string v0, "setContentTitle"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Class;

    const-class v2, Ljava/lang/CharSequence;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    move-object/from16 v2, v20

    invoke-virtual {v2, v0, v1}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v25

    .line 166
    .local v25, "setContentTitle":Ljava/lang/reflect/Method;
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    invoke-direct/range {p0 .. p0}, Lcom/adobe/mobile/MessageNotificationHandler;->getAppName()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    move-object/from16 v1, v25

    move-object/from16 v2, v22

    invoke-virtual {v1, v2, v0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 169
    const-string v0, "setContentText"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Class;

    const-class v2, Ljava/lang/CharSequence;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    move-object/from16 v2, v20

    invoke-virtual {v2, v0, v1}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v26

    .line 170
    .local v26, "setContentText":Ljava/lang/reflect/Method;
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object v6, v0, v1

    move-object/from16 v1, v26

    move-object/from16 v2, v22

    invoke-virtual {v1, v2, v0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 173
    const-string v0, "setContentIntent"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Class;

    const-class v2, Landroid/app/PendingIntent;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    move-object/from16 v2, v20

    invoke-virtual {v2, v0, v1}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v27

    .line 174
    .local v27, "setContentIntent":Ljava/lang/reflect/Method;
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object v17, v0, v1

    move-object/from16 v1, v27

    move-object/from16 v2, v22

    invoke-virtual {v1, v2, v0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 178
    const-string v0, "setAutoCancel"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Class;

    sget-object v2, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    move-object/from16 v2, v20

    invoke-virtual {v2, v0, v1}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v28

    .line 179
    .local v28, "setAutoCancel":Ljava/lang/reflect/Method;
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    move-object/from16 v1, v28

    move-object/from16 v2, v22

    invoke-virtual {v1, v2, v0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 182
    move/from16 v0, v16

    const/16 v1, 0x10

    if-lt v0, v1, :cond_1e5

    .line 183
    const-string v0, "build"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Class;

    move-object/from16 v2, v20

    invoke-virtual {v2, v0, v1}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v29

    .line 184
    .local v29, "build":Ljava/lang/reflect/Method;
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v1, v29

    move-object/from16 v2, v22

    invoke-virtual {v1, v2, v0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v18

    .line 185
    .local v18, "notification":Ljava/lang/Object;
    .end local v29    # "build":Ljava/lang/reflect/Method;
    goto :goto_1fb

    .line 187
    .end local v18    # "notification":Ljava/lang/Object;
    :cond_1e5
    const-string v0, "getNotification"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Class;

    move-object/from16 v2, v20

    invoke-virtual {v2, v0, v1}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v29

    .line 188
    .local v29, "getNotification":Ljava/lang/reflect/Method;
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v1, v29

    move-object/from16 v2, v22

    invoke-virtual {v1, v2, v0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1fa
    .catch Ljava/lang/ClassNotFoundException; {:try_start_d3 .. :try_end_1fa} :catch_279
    .catch Ljava/lang/NoSuchMethodException; {:try_start_d3 .. :try_end_1fa} :catch_28a
    .catch Lcom/adobe/mobile/StaticMethods$NullContextException; {:try_start_d3 .. :try_end_1fa} :catch_29b
    .catch Ljava/lang/Exception; {:try_start_d3 .. :try_end_1fa} :catch_2ac

    move-result-object v18

    .line 191
    .local v18, "notification":Ljava/lang/Object;
    .end local v29    # "getNotification":Ljava/lang/reflect/Method;
    :goto_1fb
    if-nez v18, :cond_1fe

    .line 192
    return-void

    .line 194
    .end local v19    # "classLoader":Ljava/lang/ClassLoader;
    .end local v20    # "notificationBuilderClass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .end local v20
    .end local v21    # "notificationConstructor":Ljava/lang/reflect/Constructor;, "Ljava/lang/reflect/Constructor<*>;"
    .end local v21
    .end local v22    # "notificationBuilder":Ljava/lang/Object;
    .end local v23    # "setSmallIcon":Ljava/lang/reflect/Method;
    .end local v24    # "setLargeIcon":Ljava/lang/reflect/Method;
    .end local v25    # "setContentTitle":Ljava/lang/reflect/Method;
    .end local v26    # "setContentText":Ljava/lang/reflect/Method;
    .end local v27    # "setContentIntent":Ljava/lang/reflect/Method;
    .end local v28    # "setAutoCancel":Ljava/lang/reflect/Method;
    :cond_1fe
    goto :goto_25c

    .line 196
    .end local v18    # "notification":Ljava/lang/Object;
    :cond_1ff
    :try_start_1ff
    new-instance v19, Landroid/app/Notification;

    invoke-direct/range {v19 .. v19}, Landroid/app/Notification;-><init>()V

    .line 197
    .local v19, "tempNotification":Landroid/app/Notification;
    const-class v0, Landroid/app/Notification;

    const-string v1, "setLatestEventInfo"

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Class;

    const-class v3, Landroid/content/Context;

    const/4 v4, 0x0

    aput-object v3, v2, v4

    const-class v3, Ljava/lang/String;

    const/4 v4, 0x1

    aput-object v3, v2, v4

    const-class v3, Ljava/lang/String;

    const/4 v4, 0x2

    aput-object v3, v2, v4

    const-class v3, Landroid/app/PendingIntent;

    const/4 v4, 0x3

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v20

    .line 198
    .local v20, "__setLatestEventInfo":Ljava/lang/reflect/Method;
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object v13, v0, v1

    invoke-direct/range {p0 .. p0}, Lcom/adobe/mobile/MessageNotificationHandler;->getAppName()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    aput-object v1, v0, v2

    const/4 v1, 0x2

    aput-object v6, v0, v1

    const/4 v1, 0x3

    aput-object v17, v0, v1

    move-object/from16 v1, v20

    move-object/from16 v2, v19

    invoke-virtual {v1, v2, v0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 200
    const-class v0, Landroid/app/Notification;

    const-string v1, "icon"

    invoke-virtual {v0, v1}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v21

    .line 201
    .local v21, "__icon":Ljava/lang/reflect/Field;
    invoke-direct/range {p0 .. p0}, Lcom/adobe/mobile/MessageNotificationHandler;->getSmallIcon()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    move-object/from16 v1, v21

    move-object/from16 v2, v19

    invoke-virtual {v1, v2, v0}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 202
    const/16 v0, 0x10

    move-object/from16 v1, v19

    iput v0, v1, Landroid/app/Notification;->flags:I

    .line 204
    move-object/from16 v18, v19

    .line 207
    .local v18, "notification":Ljava/lang/Object;
    .end local v19    # "tempNotification":Landroid/app/Notification;
    .end local v20    # "__setLatestEventInfo":Ljava/lang/reflect/Method;
    .end local v21    # "__icon":Ljava/lang/reflect/Field;
    :goto_25c
    const-string v0, "notification"

    invoke-virtual {v12, v0}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    move-object/from16 v19, v0

    check-cast v19, Landroid/app/NotificationManager;

    .line 208
    .local v19, "notificationManager":Landroid/app/NotificationManager;
    new-instance v0, Ljava/security/SecureRandom;

    invoke-direct {v0}, Ljava/security/SecureRandom;-><init>()V

    invoke-virtual {v0}, Ljava/security/SecureRandom;->nextInt()I

    move-result v0

    move-object/from16 v1, v18

    check-cast v1, Landroid/app/Notification;

    move-object/from16 v2, v19

    invoke-virtual {v2, v0, v1}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V
    :try_end_278
    .catch Ljava/lang/ClassNotFoundException; {:try_start_1ff .. :try_end_278} :catch_279
    .catch Ljava/lang/NoSuchMethodException; {:try_start_1ff .. :try_end_278} :catch_28a
    .catch Lcom/adobe/mobile/StaticMethods$NullContextException; {:try_start_1ff .. :try_end_278} :catch_29b
    .catch Ljava/lang/Exception; {:try_start_1ff .. :try_end_278} :catch_2ac

    .line 221
    .end local v17    # "sender":Landroid/app/PendingIntent;
    .end local v18    # "notification":Ljava/lang/Object;
    .end local v19    # "notificationManager":Landroid/app/NotificationManager;
    goto :goto_2bc

    .line 210
    :catch_279
    move-exception v17

    .line 211
    .local v17, "ex":Ljava/lang/ClassNotFoundException;
    const-string v0, "Messages - error posting notification, class not found (%s)"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual/range {v17 .. v17}, Ljava/lang/ClassNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logErrorFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 221
    .end local v17    # "ex":Ljava/lang/ClassNotFoundException;
    goto :goto_2bc

    .line 213
    :catch_28a
    move-exception v17

    .line 214
    .local v17, "ex":Ljava/lang/NoSuchMethodException;
    const-string v0, "Messages - error posting notification, method not found (%s)"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual/range {v17 .. v17}, Ljava/lang/NoSuchMethodException;->getMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logErrorFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 221
    .end local v17    # "ex":Ljava/lang/NoSuchMethodException;
    goto :goto_2bc

    .line 216
    :catch_29b
    move-exception v17

    .line 217
    .local v17, "ex":Lcom/adobe/mobile/StaticMethods$NullContextException;
    const-string v0, "Messages - error posting notification (%s)"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual/range {v17 .. v17}, Lcom/adobe/mobile/StaticMethods$NullContextException;->getMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logErrorFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 221
    .end local v17    # "ex":Lcom/adobe/mobile/StaticMethods$NullContextException;
    goto :goto_2bc

    .line 219
    :catch_2ac
    move-exception v17

    .line 220
    .local v17, "ex":Ljava/lang/Exception;
    const-string v0, "Messages - unexpected error posting notification (%s)"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logErrorFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 222
    .end local v17    # "ex":Ljava/lang/Exception;
    :goto_2bc
    return-void
.end method

.class Lcom/itau/empresas/feature/extrato/fragment/FiltroExtratoDialog$3;
.super Ljava/lang/Object;
.source "FiltroExtratoDialog.java"

# interfaces
.implements Landroid/widget/RadioGroup$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/itau/empresas/feature/extrato/fragment/FiltroExtratoDialog;->configurarListenerOrdemExtrato()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/itau/empresas/feature/extrato/fragment/FiltroExtratoDialog;


# direct methods
.method constructor <init>(Lcom/itau/empresas/feature/extrato/fragment/FiltroExtratoDialog;)V
    .registers 2
    .param p1, "this$0"    # Lcom/itau/empresas/feature/extrato/fragment/FiltroExtratoDialog;

    .line 115
    iput-object p1, p0, Lcom/itau/empresas/feature/extrato/fragment/FiltroExtratoDialog$3;->this$0:Lcom/itau/empresas/feature/extrato/fragment/FiltroExtratoDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/RadioGroup;I)V
    .registers 11
    .param p1, "group"    # Landroid/widget/RadioGroup;
    .param p2, "checkedId"    # I

    .line 117
    invoke-virtual {p1, p2}, Landroid/widget/RadioGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Landroid/widget/RadioButton;

    .line 118
    .local v5, "rb":Landroid/widget/RadioButton;
    invoke-virtual {v5}, Landroid/widget/RadioButton;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v6

    .line 119
    .local v6, "ordemSelecionada":Ljava/lang/String;
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/FiltroExtratoDialog$3;->this$0:Lcom/itau/empresas/feature/extrato/fragment/FiltroExtratoDialog;

    const v1, 0x7f070227

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/extrato/fragment/FiltroExtratoDialog;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    .line 120
    .local v7, "antigo":Z
    if-eqz v7, :cond_25

    .line 121
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/FiltroExtratoDialog$3;->this$0:Lcom/itau/empresas/feature/extrato/fragment/FiltroExtratoDialog;

    const/4 v1, 0x2

    # setter for: Lcom/itau/empresas/feature/extrato/fragment/FiltroExtratoDialog;->ordem:I
    invoke-static {v0, v1}, Lcom/itau/empresas/feature/extrato/fragment/FiltroExtratoDialog;->access$302(Lcom/itau/empresas/feature/extrato/fragment/FiltroExtratoDialog;I)I

    goto :goto_2b

    .line 123
    :cond_25
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/FiltroExtratoDialog$3;->this$0:Lcom/itau/empresas/feature/extrato/fragment/FiltroExtratoDialog;

    const/4 v1, 0x1

    # setter for: Lcom/itau/empresas/feature/extrato/fragment/FiltroExtratoDialog;->ordem:I
    invoke-static {v0, v1}, Lcom/itau/empresas/feature/extrato/fragment/FiltroExtratoDialog;->access$302(Lcom/itau/empresas/feature/extrato/fragment/FiltroExtratoDialog;I)I

    .line 125
    :goto_2b
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/FiltroExtratoDialog$3;->this$0:Lcom/itau/empresas/feature/extrato/fragment/FiltroExtratoDialog;

    # getter for: Lcom/itau/empresas/feature/extrato/fragment/FiltroExtratoDialog;->application:Lcom/itau/empresas/CustomApplication;
    invoke-static {v0}, Lcom/itau/empresas/feature/extrato/fragment/FiltroExtratoDialog;->access$500(Lcom/itau/empresas/feature/extrato/fragment/FiltroExtratoDialog;)Lcom/itau/empresas/CustomApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/itau/empresas/feature/extrato/fragment/FiltroExtratoDialog$3;->this$0:Lcom/itau/empresas/feature/extrato/fragment/FiltroExtratoDialog;

    .line 126
    # getter for: Lcom/itau/empresas/feature/extrato/fragment/FiltroExtratoDialog;->application:Lcom/itau/empresas/CustomApplication;
    invoke-static {v2}, Lcom/itau/empresas/feature/extrato/fragment/FiltroExtratoDialog;->access$400(Lcom/itau/empresas/feature/extrato/fragment/FiltroExtratoDialog;)Lcom/itau/empresas/CustomApplication;

    move-result-object v2

    invoke-virtual {v2}, Lcom/itau/empresas/CustomApplication;->getCodigoOperador()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_ORDEMEXTRATO"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/itau/empresas/feature/extrato/fragment/FiltroExtratoDialog$3;->this$0:Lcom/itau/empresas/feature/extrato/fragment/FiltroExtratoDialog;

    # getter for: Lcom/itau/empresas/feature/extrato/fragment/FiltroExtratoDialog;->ordem:I
    invoke-static {v2}, Lcom/itau/empresas/feature/extrato/fragment/FiltroExtratoDialog;->access$300(Lcom/itau/empresas/feature/extrato/fragment/FiltroExtratoDialog;)I

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 127
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 128
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/FiltroExtratoDialog$3;->this$0:Lcom/itau/empresas/feature/extrato/fragment/FiltroExtratoDialog;

    iget-object v1, p0, Lcom/itau/empresas/feature/extrato/fragment/FiltroExtratoDialog$3;->this$0:Lcom/itau/empresas/feature/extrato/fragment/FiltroExtratoDialog;

    const v2, 0x7f0702b0

    invoke-virtual {v1, v2}, Lcom/itau/empresas/feature/extrato/fragment/FiltroExtratoDialog;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/itau/empresas/feature/extrato/fragment/FiltroExtratoDialog$3;->this$0:Lcom/itau/empresas/feature/extrato/fragment/FiltroExtratoDialog;

    .line 129
    const v3, 0x7f07031b

    invoke-virtual {v2, v3}, Lcom/itau/empresas/feature/extrato/fragment/FiltroExtratoDialog;->getString(I)Ljava/lang/String;

    move-result-object v2

    if-eqz v7, :cond_7c

    const-wide/16 v3, 0x0

    goto :goto_7e

    :cond_7c
    const-wide/16 v3, 0x1

    .line 130
    :goto_7e
    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    .line 128
    invoke-virtual {v0, v1, v2, v3}, Lcom/itau/empresas/feature/extrato/fragment/FiltroExtratoDialog;->analyticsHit(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    .line 131
    return-void
.end method

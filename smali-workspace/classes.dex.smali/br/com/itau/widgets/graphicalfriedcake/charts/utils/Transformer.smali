.class public Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Transformer;
.super Ljava/lang/Object;
.source "Transformer.java"


# instance fields
.field private mMBuffer1:Landroid/graphics/Matrix;

.field private mMBuffer2:Landroid/graphics/Matrix;

.field protected mMatrixOffset:Landroid/graphics/Matrix;

.field protected mMatrixValueToPx:Landroid/graphics/Matrix;

.field protected mViewPortHandler:Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;


# direct methods
.method public constructor <init>(Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;)V
    .registers 3
    .param p1, "viewPortHandler"    # Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Transformer;->mMatrixValueToPx:Landroid/graphics/Matrix;

    .line 25
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Transformer;->mMatrixOffset:Landroid/graphics/Matrix;

    .line 447
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Transformer;->mMBuffer1:Landroid/graphics/Matrix;

    .line 455
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Transformer;->mMBuffer2:Landroid/graphics/Matrix;

    .line 30
    iput-object p1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Transformer;->mViewPortHandler:Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;

    .line 31
    return-void
.end method


# virtual methods
.method public generateTransformedValuesBarChart(Ljava/util/List;ILbr/com/itau/widgets/graphicalfriedcake/charts/data/BarData;F)[F
    .registers 15
    .param p1, "entries"    # Ljava/util/List;
    .param p2, "dataSet"    # I
    .param p3, "bd"    # Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarData;
    .param p4, "phaseY"    # F
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<+Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;>;ILbr/com/itau/widgets/graphicalfriedcake/charts/data/BarData;F)[F"
        }
    .end annotation

    .line 217
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    mul-int/lit8 v0, v0, 0x2

    new-array v3, v0, [F

    .line 219
    .local v3, "valuePoints":[F
    invoke-virtual {p3}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarData;->getDataSetCount()I

    move-result v4

    .line 220
    .local v4, "setCount":I
    invoke-virtual {p3}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarData;->getGroupSpace()F

    move-result v5

    .line 222
    .local v5, "space":F
    const/4 v6, 0x0

    .local v6, "j":I
    :goto_11
    array-length v0, v3

    if-ge v6, v0, :cond_43

    .line 224
    div-int/lit8 v0, v6, 0x2

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;

    .line 227
    .local v7, "e":Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;
    invoke-virtual {v7}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;->getXIndex()I

    move-result v0

    div-int/lit8 v1, v6, 0x2

    add-int/lit8 v2, v4, -0x1

    mul-int/2addr v1, v2

    add-int/2addr v0, v1

    add-int/2addr v0, p2

    int-to-float v0, v0

    div-int/lit8 v1, v6, 0x2

    int-to-float v1, v1

    mul-float/2addr v1, v5

    add-float/2addr v0, v1

    const/high16 v1, 0x40000000    # 2.0f

    div-float v1, v5, v1

    add-float v8, v0, v1

    .line 229
    .local v8, "x":F
    invoke-virtual {v7}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;->getVal()F

    move-result v9

    .line 231
    .local v9, "y":F
    aput v8, v3, v6

    .line 232
    add-int/lit8 v0, v6, 0x1

    mul-float v1, v9, p4

    aput v1, v3, v0

    .line 222
    .end local v7    # "e":Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;
    .end local v8    # "x":F
    .end local v9    # "y":F
    add-int/lit8 v6, v6, 0x2

    goto :goto_11

    .line 235
    .end local v6    # "j":I
    :cond_43
    invoke-virtual {p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Transformer;->getValueToPixelMatrix()Landroid/graphics/Matrix;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 237
    return-object v3
.end method

.method public generateTransformedValuesBubble(Ljava/util/List;FFII)[F
    .registers 12
    .param p1, "entries"    # Ljava/util/List;
    .param p2, "phaseX"    # F
    .param p3, "phaseY"    # F
    .param p4, "from"    # I
    .param p5, "to"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<+Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;>;FFII)[F"
        }
    .end annotation

    .line 129
    sub-int v0, p5, p4

    int-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v0, v0

    mul-int/lit8 v2, v0, 0x2

    .line 131
    .local v2, "count":I
    new-array v3, v2, [F

    .line 133
    .local v3, "valuePoints":[F
    const/4 v4, 0x0

    .local v4, "j":I
    :goto_d
    if-ge v4, v2, :cond_32

    .line 135
    div-int/lit8 v0, v4, 0x2

    add-int/2addr v0, p4

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;

    .line 137
    .local v5, "e":Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;
    if-eqz v5, :cond_2f

    .line 138
    invoke-virtual {v5}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;->getXIndex()I

    move-result v0

    sub-int/2addr v0, p4

    int-to-float v0, v0

    mul-float/2addr v0, p2

    int-to-float v1, p4

    add-float/2addr v0, v1

    aput v0, v3, v4

    .line 139
    add-int/lit8 v0, v4, 0x1

    invoke-virtual {v5}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;->getVal()F

    move-result v1

    mul-float/2addr v1, p3

    aput v1, v3, v0

    .line 133
    .end local v5    # "e":Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;
    :cond_2f
    add-int/lit8 v4, v4, 0x2

    goto :goto_d

    .line 143
    .end local v4    # "j":I
    :cond_32
    invoke-virtual {p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Transformer;->getValueToPixelMatrix()Landroid/graphics/Matrix;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 145
    return-object v3
.end method

.method public generateTransformedValuesCandle(Ljava/util/List;FFII)[F
    .registers 12
    .param p1, "entries"    # Ljava/util/List;
    .param p2, "phaseX"    # F
    .param p3, "phaseY"    # F
    .param p4, "from"    # I
    .param p5, "to"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<Lbr/com/itau/widgets/graphicalfriedcake/charts/data/CandleEntry;>;FFII)[F"
        }
    .end annotation

    .line 187
    sub-int v0, p5, p4

    int-to-float v0, v0

    mul-float/2addr v0, p2

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v0, v0

    mul-int/lit8 v2, v0, 0x2

    .line 189
    .local v2, "count":I
    new-array v3, v2, [F

    .line 191
    .local v3, "valuePoints":[F
    const/4 v4, 0x0

    .local v4, "j":I
    :goto_f
    if-ge v4, v2, :cond_30

    .line 193
    div-int/lit8 v0, v4, 0x2

    add-int/2addr v0, p4

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/CandleEntry;

    .line 195
    .local v5, "e":Lbr/com/itau/widgets/graphicalfriedcake/charts/data/CandleEntry;
    if-eqz v5, :cond_2d

    .line 196
    invoke-virtual {v5}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/CandleEntry;->getXIndex()I

    move-result v0

    int-to-float v0, v0

    aput v0, v3, v4

    .line 197
    add-int/lit8 v0, v4, 0x1

    invoke-virtual {v5}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/CandleEntry;->getHigh()F

    move-result v1

    mul-float/2addr v1, p3

    aput v1, v3, v0

    .line 191
    .end local v5    # "e":Lbr/com/itau/widgets/graphicalfriedcake/charts/data/CandleEntry;
    :cond_2d
    add-int/lit8 v4, v4, 0x2

    goto :goto_f

    .line 201
    .end local v4    # "j":I
    :cond_30
    invoke-virtual {p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Transformer;->getValueToPixelMatrix()Landroid/graphics/Matrix;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 203
    return-object v3
.end method

.method public generateTransformedValuesHorizontalBarChart(Ljava/util/List;ILbr/com/itau/widgets/graphicalfriedcake/charts/data/BarData;F)[F
    .registers 15
    .param p1, "entries"    # Ljava/util/List;
    .param p2, "dataSet"    # I
    .param p3, "bd"    # Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarData;
    .param p4, "phaseY"    # F
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<+Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;>;ILbr/com/itau/widgets/graphicalfriedcake/charts/data/BarData;F)[F"
        }
    .end annotation

    .line 251
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    mul-int/lit8 v0, v0, 0x2

    new-array v3, v0, [F

    .line 253
    .local v3, "valuePoints":[F
    invoke-virtual {p3}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarData;->getDataSetCount()I

    move-result v4

    .line 254
    .local v4, "setCount":I
    invoke-virtual {p3}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarData;->getGroupSpace()F

    move-result v5

    .line 256
    .local v5, "space":F
    const/4 v6, 0x0

    .local v6, "j":I
    :goto_11
    array-length v0, v3

    if-ge v6, v0, :cond_43

    .line 258
    div-int/lit8 v0, v6, 0x2

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;

    .line 261
    .local v7, "e":Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;
    invoke-virtual {v7}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;->getXIndex()I

    move-result v0

    div-int/lit8 v1, v6, 0x2

    add-int/lit8 v2, v4, -0x1

    mul-int/2addr v1, v2

    add-int/2addr v0, v1

    add-int/2addr v0, p2

    int-to-float v0, v0

    div-int/lit8 v1, v6, 0x2

    int-to-float v1, v1

    mul-float/2addr v1, v5

    add-float/2addr v0, v1

    const/high16 v1, 0x40000000    # 2.0f

    div-float v1, v5, v1

    add-float v8, v0, v1

    .line 263
    .local v8, "x":F
    invoke-virtual {v7}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;->getVal()F

    move-result v9

    .line 265
    .local v9, "y":F
    mul-float v0, v9, p4

    aput v0, v3, v6

    .line 266
    add-int/lit8 v0, v6, 0x1

    aput v8, v3, v0

    .line 256
    .end local v7    # "e":Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;
    .end local v8    # "x":F
    .end local v9    # "y":F
    add-int/lit8 v6, v6, 0x2

    goto :goto_11

    .line 269
    .end local v6    # "j":I
    :cond_43
    invoke-virtual {p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Transformer;->getValueToPixelMatrix()Landroid/graphics/Matrix;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 271
    return-object v3
.end method

.method public generateTransformedValuesLine(Ljava/util/List;FFII)[F
    .registers 12
    .param p1, "entries"    # Ljava/util/List;
    .param p2, "phaseX"    # F
    .param p3, "phaseY"    # F
    .param p4, "from"    # I
    .param p5, "to"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<+Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;>;FFII)[F"
        }
    .end annotation

    .line 158
    sub-int v0, p5, p4

    int-to-float v0, v0

    mul-float/2addr v0, p2

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v0, v0

    mul-int/lit8 v2, v0, 0x2

    .line 160
    .local v2, "count":I
    new-array v3, v2, [F

    .line 162
    .local v3, "valuePoints":[F
    const/4 v4, 0x0

    .local v4, "j":I
    :goto_f
    if-ge v4, v2, :cond_30

    .line 164
    div-int/lit8 v0, v4, 0x2

    add-int/2addr v0, p4

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;

    .line 166
    .local v5, "e":Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;
    if-eqz v5, :cond_2d

    .line 167
    invoke-virtual {v5}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;->getXIndex()I

    move-result v0

    int-to-float v0, v0

    aput v0, v3, v4

    .line 168
    add-int/lit8 v0, v4, 0x1

    invoke-virtual {v5}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;->getVal()F

    move-result v1

    mul-float/2addr v1, p3

    aput v1, v3, v0

    .line 162
    .end local v5    # "e":Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;
    :cond_2d
    add-int/lit8 v4, v4, 0x2

    goto :goto_f

    .line 172
    .end local v4    # "j":I
    :cond_30
    invoke-virtual {p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Transformer;->getValueToPixelMatrix()Landroid/graphics/Matrix;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 174
    return-object v3
.end method

.method public generateTransformedValuesScatter(Ljava/util/List;F)[F
    .registers 8
    .param p1, "entries"    # Ljava/util/List;
    .param p2, "phaseY"    # F
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<+Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;>;F)[F"
        }
    .end annotation

    .line 102
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    mul-int/lit8 v0, v0, 0x2

    new-array v2, v0, [F

    .line 104
    .local v2, "valuePoints":[F
    const/4 v3, 0x0

    .local v3, "j":I
    :goto_9
    array-length v0, v2

    if-ge v3, v0, :cond_2a

    .line 106
    div-int/lit8 v0, v3, 0x2

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;

    .line 108
    .local v4, "e":Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;
    if-eqz v4, :cond_27

    .line 109
    invoke-virtual {v4}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;->getXIndex()I

    move-result v0

    int-to-float v0, v0

    aput v0, v2, v3

    .line 110
    add-int/lit8 v0, v3, 0x1

    invoke-virtual {v4}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;->getVal()F

    move-result v1

    mul-float/2addr v1, p2

    aput v1, v2, v0

    .line 104
    .end local v4    # "e":Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;
    :cond_27
    add-int/lit8 v3, v3, 0x2

    goto :goto_9

    .line 114
    .end local v3    # "j":I
    :cond_2a
    invoke-virtual {p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Transformer;->getValueToPixelMatrix()Landroid/graphics/Matrix;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 116
    return-object v2
.end method

.method public getOffsetMatrix()Landroid/graphics/Matrix;
    .registers 2

    .line 462
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Transformer;->mMatrixOffset:Landroid/graphics/Matrix;

    return-object v0
.end method

.method public getPixelToValueMatrix()Landroid/graphics/Matrix;
    .registers 3

    .line 457
    invoke-virtual {p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Transformer;->getValueToPixelMatrix()Landroid/graphics/Matrix;

    move-result-object v0

    iget-object v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Transformer;->mMBuffer2:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->invert(Landroid/graphics/Matrix;)Z

    .line 458
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Transformer;->mMBuffer2:Landroid/graphics/Matrix;

    return-object v0
.end method

.method public getValueMatrix()Landroid/graphics/Matrix;
    .registers 2

    .line 444
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Transformer;->mMatrixValueToPx:Landroid/graphics/Matrix;

    return-object v0
.end method

.method public getValueToPixelMatrix()Landroid/graphics/Matrix;
    .registers 3

    .line 449
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Transformer;->mMBuffer1:Landroid/graphics/Matrix;

    iget-object v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Transformer;->mMatrixValueToPx:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    .line 450
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Transformer;->mMBuffer1:Landroid/graphics/Matrix;

    iget-object v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Transformer;->mViewPortHandler:Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;

    iget-object v1, v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->mMatrixTouch:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->postConcat(Landroid/graphics/Matrix;)Z

    .line 451
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Transformer;->mMBuffer1:Landroid/graphics/Matrix;

    iget-object v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Transformer;->mMatrixOffset:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->postConcat(Landroid/graphics/Matrix;)Z

    .line 452
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Transformer;->mMBuffer1:Landroid/graphics/Matrix;

    return-object v0
.end method

.method public getValuesByTouchPoint(FF)Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/PointD;
    .registers 9
    .param p1, "x"    # F
    .param p2, "y"    # F

    .line 409
    const/4 v0, 0x2

    new-array v1, v0, [F

    .line 410
    .local v1, "pts":[F
    const/4 v0, 0x0

    aput p1, v1, v0

    .line 411
    const/4 v0, 0x1

    aput p2, v1, v0

    .line 413
    invoke-virtual {p0, v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Transformer;->pixelsToValue([F)V

    .line 415
    const/4 v0, 0x0

    aget v0, v1, v0

    float-to-double v2, v0

    .line 416
    .local v2, "xTouchVal":D
    const/4 v0, 0x1

    aget v0, v1, v0

    float-to-double v4, v0

    .line 418
    .local v4, "yTouchVal":D
    new-instance v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/PointD;

    invoke-direct {v0, v2, v3, v4, v5}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/PointD;-><init>(DD)V

    return-object v0
.end method

.method public pathValueToPixel(Landroid/graphics/Path;)V
    .registers 3
    .param p1, "path"    # Landroid/graphics/Path;

    .line 282
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Transformer;->mMatrixValueToPx:Landroid/graphics/Matrix;

    invoke-virtual {p1, v0}, Landroid/graphics/Path;->transform(Landroid/graphics/Matrix;)V

    .line 283
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Transformer;->mViewPortHandler:Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->getMatrixTouch()Landroid/graphics/Matrix;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/graphics/Path;->transform(Landroid/graphics/Matrix;)V

    .line 284
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Transformer;->mMatrixOffset:Landroid/graphics/Matrix;

    invoke-virtual {p1, v0}, Landroid/graphics/Path;->transform(Landroid/graphics/Matrix;)V

    .line 285
    return-void
.end method

.method public pathValuesToPixel(Ljava/util/List;)V
    .registers 4
    .param p1, "paths"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<Landroid/graphics/Path;>;)V"
        }
    .end annotation

    .line 294
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_13

    .line 295
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Path;

    invoke-virtual {p0, v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Transformer;->pathValueToPixel(Landroid/graphics/Path;)V

    .line 294
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 297
    .end local v1    # "i":I
    :cond_13
    return-void
.end method

.method public pixelsToValue([F)V
    .registers 4
    .param p1, "pixels"    # [F

    .line 383
    new-instance v1, Landroid/graphics/Matrix;

    invoke-direct {v1}, Landroid/graphics/Matrix;-><init>()V

    .line 386
    .local v1, "tmp":Landroid/graphics/Matrix;
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Transformer;->mMatrixOffset:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->invert(Landroid/graphics/Matrix;)Z

    .line 387
    invoke-virtual {v1, p1}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 389
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Transformer;->mViewPortHandler:Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->getMatrixTouch()Landroid/graphics/Matrix;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->invert(Landroid/graphics/Matrix;)Z

    .line 390
    invoke-virtual {v1, p1}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 392
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Transformer;->mMatrixValueToPx:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->invert(Landroid/graphics/Matrix;)Z

    .line 393
    invoke-virtual {v1, p1}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 394
    return-void
.end method

.method public pointValuesToPixel([F)V
    .registers 3
    .param p1, "pts"    # [F

    .line 307
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Transformer;->mMatrixValueToPx:Landroid/graphics/Matrix;

    invoke-virtual {v0, p1}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 308
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Transformer;->mViewPortHandler:Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->getMatrixTouch()Landroid/graphics/Matrix;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 309
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Transformer;->mMatrixOffset:Landroid/graphics/Matrix;

    invoke-virtual {v0, p1}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 310
    return-void
.end method

.method public prepareMatrixOffset(Z)V
    .registers 6
    .param p1, "inverted"    # Z

    .line 71
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Transformer;->mMatrixOffset:Landroid/graphics/Matrix;

    invoke-virtual {v0}, Landroid/graphics/Matrix;->reset()V

    .line 75
    if-nez p1, :cond_20

    .line 76
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Transformer;->mMatrixOffset:Landroid/graphics/Matrix;

    iget-object v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Transformer;->mViewPortHandler:Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;

    invoke-virtual {v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->offsetLeft()F

    move-result v1

    iget-object v2, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Transformer;->mViewPortHandler:Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;

    .line 77
    invoke-virtual {v2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->getChartHeight()F

    move-result v2

    iget-object v3, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Transformer;->mViewPortHandler:Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;

    invoke-virtual {v3}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->offsetBottom()F

    move-result v3

    sub-float/2addr v2, v3

    .line 76
    invoke-virtual {v0, v1, v2}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    goto :goto_3b

    .line 79
    :cond_20
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Transformer;->mMatrixOffset:Landroid/graphics/Matrix;

    iget-object v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Transformer;->mViewPortHandler:Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;

    .line 80
    invoke-virtual {v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->offsetLeft()F

    move-result v1

    iget-object v2, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Transformer;->mViewPortHandler:Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;

    invoke-virtual {v2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->offsetTop()F

    move-result v2

    neg-float v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Matrix;->setTranslate(FF)V

    .line 81
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Transformer;->mMatrixOffset:Landroid/graphics/Matrix;

    const/high16 v1, 0x3f800000    # 1.0f

    const/high16 v2, -0x40800000    # -1.0f

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 90
    :goto_3b
    return-void
.end method

.method public prepareMatrixValuePx(FFFF)V
    .registers 10
    .param p1, "xChartMin"    # F
    .param p2, "deltaX"    # F
    .param p3, "deltaY"    # F
    .param p4, "yChartMin"    # F

    .line 40
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Transformer;->mViewPortHandler:Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->contentWidth()F

    move-result v0

    div-float v3, v0, p2

    .line 41
    .local v3, "scaleX":F
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Transformer;->mViewPortHandler:Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->contentHeight()F

    move-result v0

    div-float v4, v0, p3

    .line 44
    .local v4, "scaleY":F
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Transformer;->mMatrixValueToPx:Landroid/graphics/Matrix;

    invoke-virtual {v0}, Landroid/graphics/Matrix;->reset()V

    .line 45
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Transformer;->mMatrixValueToPx:Landroid/graphics/Matrix;

    neg-float v1, p1

    neg-float v2, p4

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 46
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Transformer;->mMatrixValueToPx:Landroid/graphics/Matrix;

    neg-float v1, v4

    invoke-virtual {v0, v3, v1}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 47
    return-void
.end method

.method public rectValueToPixel(Landroid/graphics/RectF;)V
    .registers 3
    .param p1, "r"    # Landroid/graphics/RectF;

    .line 319
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Transformer;->mMatrixValueToPx:Landroid/graphics/Matrix;

    invoke-virtual {v0, p1}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    .line 320
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Transformer;->mViewPortHandler:Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->getMatrixTouch()Landroid/graphics/Matrix;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    .line 321
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Transformer;->mMatrixOffset:Landroid/graphics/Matrix;

    invoke-virtual {v0, p1}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    .line 322
    return-void
.end method

.method public rectValueToPixel(Landroid/graphics/RectF;F)V
    .registers 5
    .param p1, "r"    # Landroid/graphics/RectF;
    .param p2, "phaseY"    # F

    .line 333
    iget v0, p1, Landroid/graphics/RectF;->top:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-lez v0, :cond_d

    .line 334
    iget v0, p1, Landroid/graphics/RectF;->top:F

    mul-float/2addr v0, p2

    iput v0, p1, Landroid/graphics/RectF;->top:F

    goto :goto_12

    .line 336
    :cond_d
    iget v0, p1, Landroid/graphics/RectF;->bottom:F

    mul-float/2addr v0, p2

    iput v0, p1, Landroid/graphics/RectF;->bottom:F

    .line 338
    :goto_12
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Transformer;->mMatrixValueToPx:Landroid/graphics/Matrix;

    invoke-virtual {v0, p1}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    .line 339
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Transformer;->mViewPortHandler:Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->getMatrixTouch()Landroid/graphics/Matrix;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    .line 340
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Transformer;->mMatrixOffset:Landroid/graphics/Matrix;

    invoke-virtual {v0, p1}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    .line 341
    return-void
.end method

.method public rectValueToPixelHorizontal(Landroid/graphics/RectF;F)V
    .registers 5
    .param p1, "r"    # Landroid/graphics/RectF;
    .param p2, "phaseY"    # F

    .line 352
    iget v0, p1, Landroid/graphics/RectF;->left:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-lez v0, :cond_d

    .line 353
    iget v0, p1, Landroid/graphics/RectF;->left:F

    mul-float/2addr v0, p2

    iput v0, p1, Landroid/graphics/RectF;->left:F

    goto :goto_12

    .line 355
    :cond_d
    iget v0, p1, Landroid/graphics/RectF;->right:F

    mul-float/2addr v0, p2

    iput v0, p1, Landroid/graphics/RectF;->right:F

    .line 357
    :goto_12
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Transformer;->mMatrixValueToPx:Landroid/graphics/Matrix;

    invoke-virtual {v0, p1}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    .line 358
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Transformer;->mViewPortHandler:Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->getMatrixTouch()Landroid/graphics/Matrix;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    .line 359
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Transformer;->mMatrixOffset:Landroid/graphics/Matrix;

    invoke-virtual {v0, p1}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    .line 360
    return-void
.end method

.method public rectValuesToPixel(Ljava/util/List;)V
    .registers 5
    .param p1, "rects"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<Landroid/graphics/RectF;>;)V"
        }
    .end annotation

    .line 369
    invoke-virtual {p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Transformer;->getValueToPixelMatrix()Landroid/graphics/Matrix;

    move-result-object v1

    .line 371
    .local v1, "m":Landroid/graphics/Matrix;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_5
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_17

    .line 372
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/RectF;

    invoke-virtual {v1, v0}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    .line 371
    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    .line 373
    .end local v2    # "i":I
    :cond_17
    return-void
.end method

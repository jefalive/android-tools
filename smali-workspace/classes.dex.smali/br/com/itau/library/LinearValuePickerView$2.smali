.class Lbr/com/itau/library/LinearValuePickerView$2;
.super Ljava/lang/Object;
.source "LinearValuePickerView.java"

# interfaces
.implements Landroid/animation/Animator$AnimatorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lbr/com/itau/library/LinearValuePickerView;->startAnimation()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lbr/com/itau/library/LinearValuePickerView;


# direct methods
.method constructor <init>(Lbr/com/itau/library/LinearValuePickerView;)V
    .registers 2
    .param p1, "this$0"    # Lbr/com/itau/library/LinearValuePickerView;

    .line 254
    iput-object p1, p0, Lbr/com/itau/library/LinearValuePickerView$2;->this$0:Lbr/com/itau/library/LinearValuePickerView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationCancel(Landroid/animation/Animator;)V
    .registers 2
    .param p1, "animation"    # Landroid/animation/Animator;

    .line 273
    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .registers 7
    .param p1, "animation"    # Landroid/animation/Animator;

    .line 262
    const/4 v3, 0x0

    .line 263
    .local v3, "delay":I
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_2
    iget-object v0, p0, Lbr/com/itau/library/LinearValuePickerView$2;->this$0:Lbr/com/itau/library/LinearValuePickerView;

    # getter for: Lbr/com/itau/library/LinearValuePickerView;->radioButtonsList:Ljava/util/List;
    invoke-static {v0}, Lbr/com/itau/library/LinearValuePickerView;->access$000(Lbr/com/itau/library/LinearValuePickerView;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v4, v0, :cond_76

    .line 264
    add-int/lit8 v3, v3, 0x64

    .line 265
    iget-object v0, p0, Lbr/com/itau/library/LinearValuePickerView$2;->this$0:Lbr/com/itau/library/LinearValuePickerView;

    # getter for: Lbr/com/itau/library/LinearValuePickerView;->radioButtonsList:Ljava/util/List;
    invoke-static {v0}, Lbr/com/itau/library/LinearValuePickerView;->access$000(Lbr/com/itau/library/LinearValuePickerView;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    invoke-virtual {v0}, Landroid/widget/RadioButton;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->scaleX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->scaleY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, Landroid/view/animation/OvershootInterpolator;

    invoke-direct {v1}, Landroid/view/animation/OvershootInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v1, 0xfa

    invoke-virtual {v0, v1, v2}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    int-to-long v1, v3

    invoke-virtual {v0, v1, v2}, Landroid/view/ViewPropertyAnimator;->setStartDelay(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 266
    iget-object v0, p0, Lbr/com/itau/library/LinearValuePickerView$2;->this$0:Lbr/com/itau/library/LinearValuePickerView;

    # getter for: Lbr/com/itau/library/LinearValuePickerView;->textValueList:Ljava/util/List;
    invoke-static {v0}, Lbr/com/itau/library/LinearValuePickerView;->access$200(Lbr/com/itau/library/LinearValuePickerView;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v1, 0xc8

    invoke-virtual {v0, v1, v2}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iget-object v1, p0, Lbr/com/itau/library/LinearValuePickerView$2;->this$0:Lbr/com/itau/library/LinearValuePickerView;

    # getter for: Lbr/com/itau/library/LinearValuePickerView;->interpolator:Landroid/view/animation/Interpolator;
    invoke-static {v1}, Lbr/com/itau/library/LinearValuePickerView;->access$100(Lbr/com/itau/library/LinearValuePickerView;)Landroid/view/animation/Interpolator;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    add-int/lit16 v1, v3, 0x12c

    int-to-long v1, v1

    invoke-virtual {v0, v1, v2}, Landroid/view/ViewPropertyAnimator;->setStartDelay(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 263
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 268
    .end local v4    # "i":I
    :cond_76
    return-void
.end method

.method public onAnimationRepeat(Landroid/animation/Animator;)V
    .registers 2
    .param p1, "animation"    # Landroid/animation/Animator;

    .line 278
    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .registers 2
    .param p1, "animation"    # Landroid/animation/Animator;

    .line 258
    return-void
.end method

.class Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesActivity$4;
.super Ljava/lang/Object;
.source "ComprovantesActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesActivity;->onEventMainThread(Ljava/util/List;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesActivity;


# direct methods
.method constructor <init>(Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesActivity;)V
    .registers 2
    .param p1, "this$0"    # Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesActivity;

    .line 127
    iput-object p1, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesActivity$4;->this$0:Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .registers 6
    .param p1, "view"    # Landroid/view/View;

    .line 130
    new-instance v3, Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;

    iget-object v0, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesActivity$4;->this$0:Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesActivity;

    .line 131
    const v1, 0x7f0702da

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesActivity$4;->this$0:Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesActivity;

    .line 132
    const v2, 0x7f0702a0

    invoke-virtual {v1, v2}, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v3, v0, v1}, Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 133
    .local v3, "eventoAnalytics":Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;
    iget-object v0, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesActivity$4;->this$0:Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesActivity;

    # getter for: Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesActivity;->application:Lcom/itau/empresas/CustomApplication;
    invoke-static {v0}, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesActivity;->access$000(Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesActivity;)Lcom/itau/empresas/CustomApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getEventoTracker()Lcom/itau/empresas/ui/util/analytics/TrackerEvento;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/itau/empresas/ui/util/analytics/TrackerEvento;->enviarHitEvento(Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;)V

    .line 134
    return-void
.end method

.class public Lcom/itau/empresas/adapter/areausuario/AreaUsuarioConfiguracoesAdapter;
.super Landroid/support/v7/widget/RecyclerView$Adapter;
.source "AreaUsuarioConfiguracoesAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/itau/empresas/adapter/areausuario/AreaUsuarioConfiguracoesAdapter$ViewHolderFingerprint;,
        Lcom/itau/empresas/adapter/areausuario/AreaUsuarioConfiguracoesAdapter$ViewHolderLista;,
        Lcom/itau/empresas/adapter/areausuario/AreaUsuarioConfiguracoesAdapter$ViewHolderHeader;,
        Lcom/itau/empresas/adapter/areausuario/AreaUsuarioConfiguracoesAdapter$OnFingerprintChanged;,
        Lcom/itau/empresas/adapter/areausuario/AreaUsuarioConfiguracoesAdapter$OnConfiguracoesItemSelected;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/support/v7/widget/RecyclerView$Adapter<Landroid/support/v7/widget/RecyclerView$ViewHolder;>;"
    }
.end annotation


# static fields
.field private static viewTypeFingerprint:I

.field private static viewTypeHeaderGeral:I


# instance fields
.field private mFingerprintAtivado:Z

.field private mFingerprintListener:Lcom/itau/empresas/adapter/areausuario/AreaUsuarioConfiguracoesAdapter$OnFingerprintChanged;

.field private mListaDataset:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Lcom/itau/empresas/api/universal/model/ItemConfiguracoes;>;"
        }
    .end annotation
.end field

.field private mListaSeguranca:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Lcom/itau/empresas/api/universal/model/ItemConfiguracoes;>;"
        }
    .end annotation
.end field

.field private mListener:Lcom/itau/empresas/adapter/areausuario/AreaUsuarioConfiguracoesAdapter$OnConfiguracoesItemSelected;

.field private mPossuiFingerprint:Z


# direct methods
.method public constructor <init>(Ljava/util/List;Ljava/util/List;Z)V
    .registers 6
    .param p1, "listaSeguranca"    # Ljava/util/List;
    .param p2, "listaGeral"    # Ljava/util/List;
    .param p3, "possuiFingerprint"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<Lcom/itau/empresas/api/universal/model/ItemConfiguracoes;>;Ljava/util/List<Lcom/itau/empresas/api/universal/model/ItemConfiguracoes;>;Z)V"
        }
    .end annotation

    .line 47
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView$Adapter;-><init>()V

    .line 48
    iput-object p1, p0, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioConfiguracoesAdapter;->mListaSeguranca:Ljava/util/List;

    .line 50
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioConfiguracoesAdapter;->mListaDataset:Ljava/util/List;

    .line 52
    iget-object v0, p0, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioConfiguracoesAdapter;->mListaDataset:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 53
    iget-object v0, p0, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioConfiguracoesAdapter;->mListaDataset:Ljava/util/List;

    iget-object v1, p0, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioConfiguracoesAdapter;->mListaSeguranca:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 54
    iget-object v0, p0, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioConfiguracoesAdapter;->mListaDataset:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 55
    iget-object v0, p0, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioConfiguracoesAdapter;->mListaDataset:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 57
    iput-boolean p3, p0, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioConfiguracoesAdapter;->mPossuiFingerprint:Z

    .line 59
    iget-object v0, p0, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioConfiguracoesAdapter;->mListaSeguranca:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    sput v0, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioConfiguracoesAdapter;->viewTypeFingerprint:I

    .line 61
    if-eqz p3, :cond_36

    .line 62
    sget v0, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioConfiguracoesAdapter;->viewTypeFingerprint:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioConfiguracoesAdapter;->viewTypeFingerprint:I

    .line 65
    :cond_36
    sget v0, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioConfiguracoesAdapter;->viewTypeFingerprint:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioConfiguracoesAdapter;->viewTypeHeaderGeral:I

    .line 66
    return-void
.end method

.method static synthetic access$200(Lcom/itau/empresas/adapter/areausuario/AreaUsuarioConfiguracoesAdapter;)Lcom/itau/empresas/adapter/areausuario/AreaUsuarioConfiguracoesAdapter$OnFingerprintChanged;
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/adapter/areausuario/AreaUsuarioConfiguracoesAdapter;

    .line 21
    iget-object v0, p0, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioConfiguracoesAdapter;->mFingerprintListener:Lcom/itau/empresas/adapter/areausuario/AreaUsuarioConfiguracoesAdapter$OnFingerprintChanged;

    return-object v0
.end method


# virtual methods
.method public getItemCount()I
    .registers 2

    .line 164
    iget-object v0, p0, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioConfiguracoesAdapter;->mListaDataset:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItemViewType(I)I
    .registers 4
    .param p1, "position"    # I

    .line 85
    if-nez p1, :cond_4

    .line 86
    const/4 v0, 0x0

    return v0

    .line 87
    :cond_4
    iget-object v0, p0, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioConfiguracoesAdapter;->mListaSeguranca:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    if-ne p1, v0, :cond_11

    .line 88
    sget v0, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioConfiguracoesAdapter;->viewTypeHeaderGeral:I

    return v0

    .line 89
    :cond_11
    iget-boolean v0, p0, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioConfiguracoesAdapter;->mPossuiFingerprint:Z

    if-eqz v0, :cond_1b

    const/4 v0, 0x3

    if-ne p1, v0, :cond_1b

    .line 90
    sget v0, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioConfiguracoesAdapter;->viewTypeFingerprint:I

    return v0

    .line 92
    :cond_1b
    const/4 v0, 0x1

    return v0
.end method

.method public onBindViewHolder(Landroid/support/v7/widget/RecyclerView$ViewHolder;I)V
    .registers 7
    .param p1, "holder"    # Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .param p2, "position"    # I

    .line 126
    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView$ViewHolder;->getItemViewType()I

    move-result v0

    sget v1, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioConfiguracoesAdapter;->viewTypeHeaderGeral:I

    if-ne v0, v1, :cond_15

    .line 127
    move-object v2, p1

    check-cast v2, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioConfiguracoesAdapter$ViewHolderHeader;

    .line 129
    .local v2, "vh":Lcom/itau/empresas/adapter/areausuario/AreaUsuarioConfiguracoesAdapter$ViewHolderHeader;
    iget-object v0, v2, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioConfiguracoesAdapter$ViewHolderHeader;->titulo:Landroid/widget/TextView;

    const v1, 0x7f070120

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 130
    .end local v2    # "vh":Lcom/itau/empresas/adapter/areausuario/AreaUsuarioConfiguracoesAdapter$ViewHolderHeader;
    goto/16 :goto_84

    :cond_15
    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView$ViewHolder;->getItemViewType()I

    move-result v0

    if-nez v0, :cond_28

    .line 131
    move-object v2, p1

    check-cast v2, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioConfiguracoesAdapter$ViewHolderHeader;

    .line 133
    .local v2, "vh":Lcom/itau/empresas/adapter/areausuario/AreaUsuarioConfiguracoesAdapter$ViewHolderHeader;
    iget-object v0, v2, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioConfiguracoesAdapter$ViewHolderHeader;->titulo:Landroid/widget/TextView;

    const v1, 0x7f070121

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 134
    .end local v2    # "vh":Lcom/itau/empresas/adapter/areausuario/AreaUsuarioConfiguracoesAdapter$ViewHolderHeader;
    goto/16 :goto_84

    :cond_28
    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView$ViewHolder;->getItemViewType()I

    move-result v0

    sget v1, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioConfiguracoesAdapter;->viewTypeFingerprint:I

    if-ne v0, v1, :cond_66

    .line 135
    iget-object v0, p0, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioConfiguracoesAdapter;->mListaDataset:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/itau/empresas/api/universal/model/ItemConfiguracoes;

    .line 136
    .local v2, "item":Lcom/itau/empresas/api/universal/model/ItemConfiguracoes;
    move-object v3, p1

    check-cast v3, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioConfiguracoesAdapter$ViewHolderFingerprint;

    .line 138
    .local v3, "vh":Lcom/itau/empresas/adapter/areausuario/AreaUsuarioConfiguracoesAdapter$ViewHolderFingerprint;
    iget-object v0, v3, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioConfiguracoesAdapter$ViewHolderFingerprint;->icone:Lcom/itau/empresas/ui/view/TextViewIcon;

    invoke-virtual {v2}, Lcom/itau/empresas/api/universal/model/ItemConfiguracoes;->getIconeMenu()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/view/TextViewIcon;->setText(Ljava/lang/CharSequence;)V

    .line 139
    iget-object v0, v3, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioConfiguracoesAdapter$ViewHolderFingerprint;->titulo:Landroid/widget/TextView;

    invoke-virtual {v2}, Lcom/itau/empresas/api/universal/model/ItemConfiguracoes;->getItemMenu()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 141
    iget-object v0, v3, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioConfiguracoesAdapter$ViewHolderFingerprint;->switchFingerprint:Landroid/widget/Switch;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Switch;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 142
    iget-object v0, v3, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioConfiguracoesAdapter$ViewHolderFingerprint;->switchFingerprint:Landroid/widget/Switch;

    iget-boolean v1, p0, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioConfiguracoesAdapter;->mFingerprintAtivado:Z

    invoke-virtual {v0, v1}, Landroid/widget/Switch;->setChecked(Z)V

    .line 143
    iget-object v0, v3, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioConfiguracoesAdapter$ViewHolderFingerprint;->switchFingerprint:Landroid/widget/Switch;

    new-instance v1, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioConfiguracoesAdapter$1;

    invoke-direct {v1, p0}, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioConfiguracoesAdapter$1;-><init>(Lcom/itau/empresas/adapter/areausuario/AreaUsuarioConfiguracoesAdapter;)V

    invoke-virtual {v0, v1}, Landroid/widget/Switch;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 153
    .end local v2    # "item":Lcom/itau/empresas/api/universal/model/ItemConfiguracoes;
    .end local v3    # "vh":Lcom/itau/empresas/adapter/areausuario/AreaUsuarioConfiguracoesAdapter$ViewHolderFingerprint;
    goto :goto_84

    .line 154
    :cond_66
    iget-object v0, p0, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioConfiguracoesAdapter;->mListaDataset:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/itau/empresas/api/universal/model/ItemConfiguracoes;

    .line 155
    .local v2, "item":Lcom/itau/empresas/api/universal/model/ItemConfiguracoes;
    move-object v3, p1

    check-cast v3, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioConfiguracoesAdapter$ViewHolderLista;

    .line 157
    .local v3, "vh":Lcom/itau/empresas/adapter/areausuario/AreaUsuarioConfiguracoesAdapter$ViewHolderLista;
    iget-object v0, v3, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioConfiguracoesAdapter$ViewHolderLista;->icone:Lcom/itau/empresas/ui/view/TextViewIcon;

    invoke-virtual {v2}, Lcom/itau/empresas/api/universal/model/ItemConfiguracoes;->getIconeMenu()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/view/TextViewIcon;->setText(Ljava/lang/CharSequence;)V

    .line 158
    iget-object v0, v3, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioConfiguracoesAdapter$ViewHolderLista;->titulo:Landroid/widget/TextView;

    invoke-virtual {v2}, Lcom/itau/empresas/api/universal/model/ItemConfiguracoes;->getItemMenu()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 160
    .end local v2    # "item":Lcom/itau/empresas/api/universal/model/ItemConfiguracoes;
    .end local v3    # "vh":Lcom/itau/empresas/adapter/areausuario/AreaUsuarioConfiguracoesAdapter$ViewHolderLista;
    :goto_84
    return-void
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .registers 9
    .param p1, "parent"    # Landroid/view/ViewGroup;
    .param p2, "viewType"    # I

    .line 99
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v5

    .line 101
    .local v5, "inflater":Landroid/view/LayoutInflater;
    if-nez p2, :cond_18

    .line 102
    new-instance v4, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioConfiguracoesAdapter$ViewHolderHeader;

    .line 103
    const v0, 0x7f0300f7

    const/4 v1, 0x0

    invoke-virtual {v5, v0, p1, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    invoke-direct {v4, v0}, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioConfiguracoesAdapter$ViewHolderHeader;-><init>(Landroid/view/View;)V

    .local v4, "viewHolder":Landroid/support/v7/widget/RecyclerView$ViewHolder;
    goto :goto_50

    .line 105
    .end local v4    # "viewHolder":Landroid/support/v7/widget/RecyclerView$ViewHolder;
    :cond_18
    sget v0, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioConfiguracoesAdapter;->viewTypeHeaderGeral:I

    if-ne p2, v0, :cond_2a

    .line 106
    new-instance v4, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioConfiguracoesAdapter$ViewHolderHeader;

    .line 107
    const v0, 0x7f0300f7

    const/4 v1, 0x0

    invoke-virtual {v5, v0, p1, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    invoke-direct {v4, v0}, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioConfiguracoesAdapter$ViewHolderHeader;-><init>(Landroid/view/View;)V

    .local v4, "viewHolder":Landroid/support/v7/widget/RecyclerView$ViewHolder;
    goto :goto_50

    .line 109
    .end local v4    # "viewHolder":Landroid/support/v7/widget/RecyclerView$ViewHolder;
    :cond_2a
    const/4 v0, 0x1

    if-ne p2, v0, :cond_3e

    .line 110
    new-instance v4, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioConfiguracoesAdapter$ViewHolderLista;

    .line 111
    const v0, 0x7f0300f8

    const/4 v1, 0x0

    invoke-virtual {v5, v0, p1, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioConfiguracoesAdapter;->mListener:Lcom/itau/empresas/adapter/areausuario/AreaUsuarioConfiguracoesAdapter$OnConfiguracoesItemSelected;

    const/4 v2, 0x0

    invoke-direct {v4, v0, v1, v2}, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioConfiguracoesAdapter$ViewHolderLista;-><init>(Landroid/view/View;Lcom/itau/empresas/adapter/areausuario/AreaUsuarioConfiguracoesAdapter$OnConfiguracoesItemSelected;Lcom/itau/empresas/adapter/areausuario/AreaUsuarioConfiguracoesAdapter$1;)V

    .local v4, "viewHolder":Landroid/support/v7/widget/RecyclerView$ViewHolder;
    goto :goto_50

    .line 115
    .end local v4    # "viewHolder":Landroid/support/v7/widget/RecyclerView$ViewHolder;
    :cond_3e
    new-instance v4, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioConfiguracoesAdapter$ViewHolderFingerprint;

    .line 116
    const v0, 0x7f0300f6

    const/4 v1, 0x0

    invoke-virtual {v5, v0, p1, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iget-boolean v1, p0, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioConfiguracoesAdapter;->mPossuiFingerprint:Z

    iget-object v2, p0, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioConfiguracoesAdapter;->mFingerprintListener:Lcom/itau/empresas/adapter/areausuario/AreaUsuarioConfiguracoesAdapter$OnFingerprintChanged;

    const/4 v3, 0x0

    invoke-direct {v4, v0, v1, v2, v3}, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioConfiguracoesAdapter$ViewHolderFingerprint;-><init>(Landroid/view/View;ZLcom/itau/empresas/adapter/areausuario/AreaUsuarioConfiguracoesAdapter$OnFingerprintChanged;Lcom/itau/empresas/adapter/areausuario/AreaUsuarioConfiguracoesAdapter$1;)V

    .line 121
    .local v4, "viewHolder":Landroid/support/v7/widget/RecyclerView$ViewHolder;
    :goto_50
    return-object v4
.end method

.method public setClickListener(Lcom/itau/empresas/adapter/areausuario/AreaUsuarioConfiguracoesAdapter$OnConfiguracoesItemSelected;)V
    .registers 2
    .param p1, "listener"    # Lcom/itau/empresas/adapter/areausuario/AreaUsuarioConfiguracoesAdapter$OnConfiguracoesItemSelected;

    .line 69
    iput-object p1, p0, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioConfiguracoesAdapter;->mListener:Lcom/itau/empresas/adapter/areausuario/AreaUsuarioConfiguracoesAdapter$OnConfiguracoesItemSelected;

    .line 70
    return-void
.end method

.method public setFingerprintAtivado(Z)V
    .registers 3
    .param p1, "isAtivado"    # Z

    .line 77
    iput-boolean p1, p0, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioConfiguracoesAdapter;->mFingerprintAtivado:Z

    .line 79
    iget-boolean v0, p0, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioConfiguracoesAdapter;->mPossuiFingerprint:Z

    if-eqz v0, :cond_9

    .line 80
    invoke-virtual {p0}, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioConfiguracoesAdapter;->notifyDataSetChanged()V

    .line 81
    :cond_9
    return-void
.end method

.method public setFingerprintListerner(Lcom/itau/empresas/adapter/areausuario/AreaUsuarioConfiguracoesAdapter$OnFingerprintChanged;)V
    .registers 2
    .param p1, "listener"    # Lcom/itau/empresas/adapter/areausuario/AreaUsuarioConfiguracoesAdapter$OnFingerprintChanged;

    .line 73
    iput-object p1, p0, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioConfiguracoesAdapter;->mFingerprintListener:Lcom/itau/empresas/adapter/areausuario/AreaUsuarioConfiguracoesAdapter$OnFingerprintChanged;

    .line 74
    return-void
.end method

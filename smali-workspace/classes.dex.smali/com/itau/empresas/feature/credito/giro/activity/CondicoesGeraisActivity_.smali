.class public final Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity_;
.super Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity;
.source "CondicoesGeraisActivity_.java"

# interfaces
.implements Lorg/androidannotations/api/view/HasViews;
.implements Lorg/androidannotations/api/view/OnViewChangedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity_$IntentBuilder_;
    }
.end annotation


# instance fields
.field private final onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;


# direct methods
.method public constructor <init>()V
    .registers 2

    .line 36
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity;-><init>()V

    .line 40
    new-instance v0, Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-direct {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    return-void
.end method

.method static synthetic access$001(Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity_;)V
    .registers 1
    .param p0, "x0"    # Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity_;

    .line 36
    invoke-super {p0}, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity;->mostraDialogoDeProgresso()V

    return-void
.end method

.method static synthetic access$101(Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity_;)V
    .registers 1
    .param p0, "x0"    # Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity_;

    .line 36
    invoke-super {p0}, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity;->escondeDialogoDeProgresso()V

    return-void
.end method

.method private init_(Landroid/os/Bundle;)V
    .registers 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 57
    new-instance v0, Lcom/itau/empresas/Preferences_;

    invoke-direct {v0, p0}, Lcom/itau/empresas/Preferences_;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity_;->prefs:Lcom/itau/empresas/Preferences_;

    .line 58
    invoke-static {p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->registerOnViewChangedListener(Lorg/androidannotations/api/view/OnViewChangedListener;)V

    .line 59
    invoke-static {}, Lcom/itau/empresas/CustomApplication_;->getInstance()Lcom/itau/empresas/CustomApplication;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity_;->application:Lcom/itau/empresas/CustomApplication;

    .line 60
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity_;->injectExtras_()V

    .line 61
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity_;->afterInject()V

    .line 62
    return-void
.end method

.method private injectExtras_()V
    .registers 3

    .line 183
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity_;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 184
    .local v1, "extras_":Landroid/os/Bundle;
    if-eqz v1, :cond_5a

    .line 185
    const-string v0, "temSeguro"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 186
    const-string v0, "temSeguro"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity_;->temSeguro:Z

    .line 188
    :cond_1a
    const-string v0, "temDevedorSolidario"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2a

    .line 189
    const-string v0, "temDevedorSolidario"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity_;->temDevedorSolidario:Z

    .line 191
    :cond_2a
    const-string v0, "nome"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3a

    .line 192
    const-string v0, "nome"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity_;->nome:Ljava/lang/String;

    .line 194
    :cond_3a
    const-string v0, "tipoDoc"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4a

    .line 195
    const-string v0, "tipoDoc"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity_;->tipoDoc:Ljava/lang/String;

    .line 197
    :cond_4a
    const-string v0, "numeroDoc"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5a

    .line 198
    const-string v0, "numeroDoc"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity_;->numeroDoc:Ljava/lang/String;

    .line 201
    :cond_5a
    return-void
.end method


# virtual methods
.method public escondeDialogoDeProgresso()V
    .registers 5

    .line 226
    const-string v0, ""

    new-instance v1, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity_$8;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity_$8;-><init>(Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity_;)V

    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3}, Lorg/androidannotations/api/UiThreadExecutor;->runTask(Ljava/lang/String;Ljava/lang/Runnable;J)V

    .line 234
    return-void
.end method

.method public mostraDialogoDeProgresso()V
    .registers 5
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "CommitTransaction"
        }
    .end annotation

    .line 214
    const-string v0, ""

    new-instance v1, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity_$7;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity_$7;-><init>(Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity_;)V

    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3}, Lorg/androidannotations/api/UiThreadExecutor;->runTask(Ljava/lang/String;Ljava/lang/Runnable;J)V

    .line 222
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 49
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-static {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    move-result-object v1

    .line 50
    .local v1, "previousNotifier":Lorg/androidannotations/api/view/OnViewChangedNotifier;
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity_;->init_(Landroid/os/Bundle;)V

    .line 51
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity;->onCreate(Landroid/os/Bundle;)V

    .line 52
    invoke-static {v1}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 53
    const v0, 0x7f03002f

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity_;->setContentView(I)V

    .line 54
    return-void
.end method

.method public onViewChanged(Lorg/androidannotations/api/view/HasViews;)V
    .registers 4
    .param p1, "hasViews"    # Lorg/androidannotations/api/view/HasViews;

    .line 96
    const v0, 0x7f0e0161

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity_;->textoConsulteCondicoesGiro:Landroid/widget/TextView;

    .line 97
    const v0, 0x7f0e0163

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity_;->textoCondicoesGeraisSeguroCapitalGiro:Landroid/widget/TextView;

    .line 98
    const v0, 0x7f0e0503

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity_;->textoCondicoesGeraisContratacaoGiro:Landroid/widget/TextView;

    .line 99
    const v0, 0x7f0e0504

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity_;->textoCondicoesGeraisDevedorSolidario:Landroid/widget/TextView;

    .line 100
    const v0, 0x7f0e0162

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity_;->textoCondicoesGeraisLetrae:Landroid/widget/TextView;

    .line 101
    const v0, 0x7f0e0160

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity_;->textoConsulteCondicoesGerais:Landroid/widget/TextView;

    .line 102
    const v0, 0x7f0e014d

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity_;->setaDesceCondicoesGeraisDadosDoSeguro:Landroid/widget/ImageView;

    .line 103
    const v0, 0x7f0e0158

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity_;->setaDesceCondicoesGeraisContratacaoGiro:Landroid/widget/ImageView;

    .line 104
    const v0, 0x7f0e015d

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity_;->setaDesceCondicoesGeraisDevedorSolidario:Landroid/widget/ImageView;

    .line 105
    const v0, 0x7f0e0152

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity_;->setaDesceCondicoesGeraisContratacaoGiroComSeguro:Landroid/widget/ImageView;

    .line 106
    const v0, 0x7f0e014f

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity_;->llCondicoesGeraisDadosSeguro:Landroid/widget/LinearLayout;

    .line 107
    const v0, 0x7f0e015a

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity_;->llCondicoesGeraisDeclaracaoGiro:Landroid/widget/LinearLayout;

    .line 108
    const v0, 0x7f0e015f

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity_;->llCondicoesGeraisDevedorSolidario:Landroid/widget/LinearLayout;

    .line 109
    const v0, 0x7f0e0153

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity_;->llCondicoesGeraisDadosSeguroComSeguro:Landroid/widget/LinearLayout;

    .line 110
    const v0, 0x7f0e0156

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity_;->rlCondicoesGeraisContratacaoGiro:Landroid/widget/RelativeLayout;

    .line 111
    const v0, 0x7f0e015b

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity_;->rlCondicoesGeraisDevedorSolidario:Landroid/widget/RelativeLayout;

    .line 112
    const v0, 0x7f0e014b

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity_;->rlCondicoesGeraisDadosSeguro:Landroid/widget/RelativeLayout;

    .line 113
    const v0, 0x7f0e0150

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity_;->rlCondicoesGeraisContratacaoGiroComSeguro:Landroid/widget/RelativeLayout;

    .line 114
    const v0, 0x7f0e014e

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity_;->viewCondicoesGeraisDadosSeguro:Landroid/view/View;

    .line 115
    const v0, 0x7f0e0159

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity_;->viewCondicoesGeraisContratacaoGiro:Landroid/view/View;

    .line 116
    const v0, 0x7f0e015e

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity_;->viewCondicoesGeraisDevedorSolidario:Landroid/view/View;

    .line 117
    const v0, 0x7f0e0154

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity_;->viewCondicoesGeraisDadosSeguroComSeguro:Landroid/view/View;

    .line 118
    const v0, 0x7f0e00b6

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/Toolbar;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity_;->toolBar:Landroid/support/v7/widget/Toolbar;

    .line 119
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity_;->textoConsulteCondicoesGiro:Landroid/widget/TextView;

    if-eqz v0, :cond_103

    .line 120
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity_;->textoConsulteCondicoesGiro:Landroid/widget/TextView;

    new-instance v1, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity_$1;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity_$1;-><init>(Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity_;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 129
    :cond_103
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity_;->textoCondicoesGeraisSeguroCapitalGiro:Landroid/widget/TextView;

    if-eqz v0, :cond_111

    .line 130
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity_;->textoCondicoesGeraisSeguroCapitalGiro:Landroid/widget/TextView;

    new-instance v1, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity_$2;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity_$2;-><init>(Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity_;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 139
    :cond_111
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity_;->rlCondicoesGeraisContratacaoGiro:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_11f

    .line 140
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity_;->rlCondicoesGeraisContratacaoGiro:Landroid/widget/RelativeLayout;

    new-instance v1, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity_$3;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity_$3;-><init>(Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity_;)V

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 149
    :cond_11f
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity_;->rlCondicoesGeraisContratacaoGiroComSeguro:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_12d

    .line 150
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity_;->rlCondicoesGeraisContratacaoGiroComSeguro:Landroid/widget/RelativeLayout;

    new-instance v1, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity_$4;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity_$4;-><init>(Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity_;)V

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 159
    :cond_12d
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity_;->rlCondicoesGeraisDevedorSolidario:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_13b

    .line 160
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity_;->rlCondicoesGeraisDevedorSolidario:Landroid/widget/RelativeLayout;

    new-instance v1, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity_$5;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity_$5;-><init>(Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity_;)V

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 169
    :cond_13b
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity_;->rlCondicoesGeraisDadosSeguro:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_149

    .line 170
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity_;->rlCondicoesGeraisDadosSeguro:Landroid/widget/RelativeLayout;

    new-instance v1, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity_$6;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity_$6;-><init>(Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity_;)V

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 179
    :cond_149
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity_;->aoInicializar()V

    .line 180
    return-void
.end method

.method public setContentView(I)V
    .registers 3
    .param p1, "layoutResID"    # I

    .line 66
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity;->setContentView(I)V

    .line 67
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 68
    return-void
.end method

.method public setContentView(Landroid/view/View;)V
    .registers 3
    .param p1, "view"    # Landroid/view/View;

    .line 78
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity;->setContentView(Landroid/view/View;)V

    .line 79
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 80
    return-void
.end method

.method public setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .registers 4
    .param p1, "view"    # Landroid/view/View;
    .param p2, "params"    # Landroid/view/ViewGroup$LayoutParams;

    .line 72
    invoke-super {p0, p1, p2}, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity;->setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 73
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 74
    return-void
.end method

.method public setIntent(Landroid/content/Intent;)V
    .registers 2
    .param p1, "newIntent"    # Landroid/content/Intent;

    .line 205
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity;->setIntent(Landroid/content/Intent;)V

    .line 206
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity_;->injectExtras_()V

    .line 207
    return-void
.end method

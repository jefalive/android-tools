.class public final Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment_;
.super Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;
.source "WidgetSaldoMultiLimiteFragment_.java"

# interfaces
.implements Lorg/androidannotations/api/view/HasViews;
.implements Lorg/androidannotations/api/view/OnViewChangedListener;


# instance fields
.field private contentView_:Landroid/view/View;

.field private final onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;


# direct methods
.method public constructor <init>()V
    .registers 2

    .line 29
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;-><init>()V

    .line 33
    new-instance v0, Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-direct {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    return-void
.end method

.method private init_(Landroid/os/Bundle;)V
    .registers 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 75
    invoke-static {p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->registerOnViewChangedListener(Lorg/androidannotations/api/view/OnViewChangedListener;)V

    .line 76
    invoke-static {}, Lcom/itau/empresas/CustomApplication_;->getInstance()Lcom/itau/empresas/CustomApplication;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment_;->application:Lcom/itau/empresas/CustomApplication;

    .line 77
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment_;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/itau/empresas/feature/logout/LogoutHelper_;->getInstance_(Landroid/content/Context;)Lcom/itau/empresas/feature/logout/LogoutHelper_;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment_;->logoutHelper:Lcom/itau/empresas/feature/logout/LogoutHelper;

    .line 78
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment_;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/itau/empresas/PreferenciaUtils_;->getInstance_(Landroid/content/Context;)Lcom/itau/empresas/PreferenciaUtils_;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment_;->preferenciaWidget:Lcom/itau/empresas/PreferenciaUtils;

    .line 79
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment_;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/itau/empresas/controller/ExtratoController_;->getInstance_(Landroid/content/Context;)Lcom/itau/empresas/controller/ExtratoController_;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment_;->extratoController:Lcom/itau/empresas/controller/ExtratoController;

    .line 80
    return-void
.end method


# virtual methods
.method public findViewById(I)Landroid/view/View;
    .registers 3
    .param p1, "id"    # I

    .line 46
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment_;->contentView_:Landroid/view/View;

    if-nez v0, :cond_6

    .line 47
    const/4 v0, 0x0

    return-object v0

    .line 49
    :cond_6
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment_;->contentView_:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 38
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-static {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    move-result-object v1

    .line 39
    .local v1, "previousNotifier":Lorg/androidannotations/api/view/OnViewChangedNotifier;
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment_;->init_(Landroid/os/Bundle;)V

    .line 40
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->onCreate(Landroid/os/Bundle;)V

    .line 41
    invoke-static {v1}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 42
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .registers 6
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .line 54
    invoke-super {p0, p1, p2, p3}, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment_;->contentView_:Landroid/view/View;

    .line 55
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment_;->contentView_:Landroid/view/View;

    if-nez v0, :cond_14

    .line 56
    const v0, 0x7f0300c8

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment_;->contentView_:Landroid/view/View;

    .line 58
    :cond_14
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment_;->contentView_:Landroid/view/View;

    return-object v0
.end method

.method public onDestroyView()V
    .registers 2

    .line 63
    invoke-super {p0}, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->onDestroyView()V

    .line 64
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment_;->contentView_:Landroid/view/View;

    .line 65
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment_;->root:Landroid/widget/LinearLayout;

    .line 66
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment_;->llViewSaldoMultiLimite:Landroid/view/View;

    .line 67
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment_;->textoMostrarSaldo:Landroid/widget/TextView;

    .line 68
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment_;->textoSaldoNaoEncontrado:Landroid/widget/TextView;

    .line 69
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment_;->llMostrarSaldo:Landroid/widget/LinearLayout;

    .line 70
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment_;->progressMostrarSaldo:Landroid/widget/ProgressBar;

    .line 71
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment_;->elvSaldoMultiLimite:Landroid/widget/ExpandableListView;

    .line 72
    return-void
.end method

.method public onViewChanged(Lorg/androidannotations/api/view/HasViews;)V
    .registers 4
    .param p1, "hasViews"    # Lorg/androidannotations/api/view/HasViews;

    .line 94
    const v0, 0x7f0e049d

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment_;->root:Landroid/widget/LinearLayout;

    .line 95
    const v0, 0x7f0e049f

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment_;->textoMostrarSaldo:Landroid/widget/TextView;

    .line 96
    const v0, 0x7f0e04a1

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment_;->textoSaldoNaoEncontrado:Landroid/widget/TextView;

    .line 97
    const v0, 0x7f0e049e

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment_;->llMostrarSaldo:Landroid/widget/LinearLayout;

    .line 98
    const v0, 0x7f0e04a0

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment_;->progressMostrarSaldo:Landroid/widget/ProgressBar;

    .line 99
    const v0, 0x7f0e04a2

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ExpandableListView;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment_;->elvSaldoMultiLimite:Landroid/widget/ExpandableListView;

    .line 100
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment_;->root:Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment_;->llViewSaldoMultiLimite:Landroid/view/View;

    .line 101
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment_;->llMostrarSaldo:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_54

    .line 102
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment_;->llMostrarSaldo:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment_$1;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment_$1;-><init>(Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment_;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 111
    :cond_54
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment_;->carregandoElementosDeTela()V

    .line 112
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .registers 4
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;

    .line 84
    invoke-super {p0, p1, p2}, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 85
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 86
    return-void
.end method

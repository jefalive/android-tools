.class Lcom/itau/empresas/adapter/areausuario/AreaUsuarioConfiguracoesAdapter$ViewHolderFingerprint;
.super Lcom/itau/empresas/adapter/areausuario/AreaUsuarioConfiguracoesAdapter$ViewHolderLista;
.source "AreaUsuarioConfiguracoesAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/itau/empresas/adapter/areausuario/AreaUsuarioConfiguracoesAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ViewHolderFingerprint"
.end annotation


# instance fields
.field switchFingerprint:Landroid/widget/Switch;


# direct methods
.method private constructor <init>(Landroid/view/View;ZLcom/itau/empresas/adapter/areausuario/AreaUsuarioConfiguracoesAdapter$OnFingerprintChanged;)V
    .registers 6
    .param p1, "itemView"    # Landroid/view/View;
    .param p2, "possuiFingerprint"    # Z
    .param p3, "fingerprintListener"    # Lcom/itau/empresas/adapter/areausuario/AreaUsuarioConfiguracoesAdapter$OnFingerprintChanged;

    .line 203
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioConfiguracoesAdapter$ViewHolderLista;-><init>(Landroid/view/View;Lcom/itau/empresas/adapter/areausuario/AreaUsuarioConfiguracoesAdapter$OnConfiguracoesItemSelected;Lcom/itau/empresas/adapter/areausuario/AreaUsuarioConfiguracoesAdapter$1;)V

    .line 205
    const v0, 0x7f0e052f

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/ui/view/TextViewIcon;

    iput-object v0, p0, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioConfiguracoesAdapter$ViewHolderFingerprint;->icone:Lcom/itau/empresas/ui/view/TextViewIcon;

    .line 206
    const v0, 0x7f0e0530

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioConfiguracoesAdapter$ViewHolderFingerprint;->titulo:Landroid/widget/TextView;

    .line 207
    const v0, 0x7f0e0531

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Switch;

    iput-object v0, p0, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioConfiguracoesAdapter$ViewHolderFingerprint;->switchFingerprint:Landroid/widget/Switch;

    .line 209
    if-nez p2, :cond_2d

    .line 210
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 211
    :cond_2d
    return-void
.end method

.method synthetic constructor <init>(Landroid/view/View;ZLcom/itau/empresas/adapter/areausuario/AreaUsuarioConfiguracoesAdapter$OnFingerprintChanged;Lcom/itau/empresas/adapter/areausuario/AreaUsuarioConfiguracoesAdapter$1;)V
    .registers 5
    .param p1, "x0"    # Landroid/view/View;
    .param p2, "x1"    # Z
    .param p3, "x2"    # Lcom/itau/empresas/adapter/areausuario/AreaUsuarioConfiguracoesAdapter$OnFingerprintChanged;
    .param p4, "x3"    # Lcom/itau/empresas/adapter/areausuario/AreaUsuarioConfiguracoesAdapter$1;

    .line 198
    invoke-direct {p0, p1, p2, p3}, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioConfiguracoesAdapter$ViewHolderFingerprint;-><init>(Landroid/view/View;ZLcom/itau/empresas/adapter/areausuario/AreaUsuarioConfiguracoesAdapter$OnFingerprintChanged;)V

    return-void
.end method

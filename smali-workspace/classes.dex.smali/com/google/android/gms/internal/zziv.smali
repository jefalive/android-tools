.class public Lcom/google/android/gms/internal/zziv;
.super Ljava/lang/Object;


# annotations
.annotation runtime Lcom/google/android/gms/internal/zzhb;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/internal/zziv$1;,
        Lcom/google/android/gms/internal/zziv$zza;,
        Lcom/google/android/gms/internal/zziv$zzb;
    }
.end annotation


# instance fields
.field private final zzMn:[Ljava/lang/String;

.field private final zzMo:[D

.field private final zzMp:[D

.field private final zzMq:[I

.field private zzMr:I


# direct methods
.method private constructor <init>(Lcom/google/android/gms/internal/zziv$zzb;)V
    .registers 5

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/gms/internal/zziv$zzb;->zza(Lcom/google/android/gms/internal/zziv$zzb;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    invoke-static {p1}, Lcom/google/android/gms/internal/zziv$zzb;->zzb(Lcom/google/android/gms/internal/zziv$zzb;)Ljava/util/List;

    move-result-object v0

    new-array v1, v2, [Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/internal/zziv;->zzMn:[Ljava/lang/String;

    invoke-static {p1}, Lcom/google/android/gms/internal/zziv$zzb;->zza(Lcom/google/android/gms/internal/zziv$zzb;)Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/internal/zziv;->zzk(Ljava/util/List;)[D

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/internal/zziv;->zzMo:[D

    invoke-static {p1}, Lcom/google/android/gms/internal/zziv$zzb;->zzc(Lcom/google/android/gms/internal/zziv$zzb;)Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/internal/zziv;->zzk(Ljava/util/List;)[D

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/internal/zziv;->zzMp:[D

    new-array v0, v2, [I

    iput-object v0, p0, Lcom/google/android/gms/internal/zziv;->zzMq:[I

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/internal/zziv;->zzMr:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/internal/zziv$zzb;Lcom/google/android/gms/internal/zziv$1;)V
    .registers 3

    invoke-direct {p0, p1}, Lcom/google/android/gms/internal/zziv;-><init>(Lcom/google/android/gms/internal/zziv$zzb;)V

    return-void
.end method

.method private zzk(Ljava/util/List;)[D
    .registers 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<Ljava/lang/Double;>;)[D"
        }
    .end annotation

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    new-array v2, v0, [D

    const/4 v3, 0x0

    :goto_7
    array-length v0, v2

    if-ge v3, v0, :cond_19

    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    aput-wide v0, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_7

    :cond_19
    return-object v2
.end method


# virtual methods
.method public getBuckets()Ljava/util/List;
    .registers 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Lcom/google/android/gms/internal/zziv$zza;>;"
        }
    .end annotation

    new-instance v10, Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/google/android/gms/internal/zziv;->zzMn:[Ljava/lang/String;

    array-length v0, v0

    invoke-direct {v10, v0}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v11, 0x0

    :goto_9
    iget-object v0, p0, Lcom/google/android/gms/internal/zziv;->zzMn:[Ljava/lang/String;

    array-length v0, v0

    if-ge v11, v0, :cond_32

    new-instance v0, Lcom/google/android/gms/internal/zziv$zza;

    iget-object v1, p0, Lcom/google/android/gms/internal/zziv;->zzMn:[Ljava/lang/String;

    aget-object v1, v1, v11

    iget-object v2, p0, Lcom/google/android/gms/internal/zziv;->zzMp:[D

    aget-wide v2, v2, v11

    iget-object v4, p0, Lcom/google/android/gms/internal/zziv;->zzMo:[D

    aget-wide v4, v4, v11

    iget-object v6, p0, Lcom/google/android/gms/internal/zziv;->zzMq:[I

    aget v6, v6, v11

    int-to-double v6, v6

    iget v8, p0, Lcom/google/android/gms/internal/zziv;->zzMr:I

    int-to-double v8, v8

    div-double/2addr v6, v8

    iget-object v8, p0, Lcom/google/android/gms/internal/zziv;->zzMq:[I

    aget v8, v8, v11

    invoke-direct/range {v0 .. v8}, Lcom/google/android/gms/internal/zziv$zza;-><init>(Ljava/lang/String;DDDI)V

    invoke-interface {v10, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v11, v11, 0x1

    goto :goto_9

    :cond_32
    return-object v10
.end method

.method public zza(D)V
    .registers 7

    iget v0, p0, Lcom/google/android/gms/internal/zziv;->zzMr:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/gms/internal/zziv;->zzMr:I

    const/4 v3, 0x0

    :goto_7
    iget-object v0, p0, Lcom/google/android/gms/internal/zziv;->zzMp:[D

    array-length v0, v0

    if-ge v3, v0, :cond_30

    iget-object v0, p0, Lcom/google/android/gms/internal/zziv;->zzMp:[D

    aget-wide v0, v0, v3

    cmpg-double v0, v0, p1

    if-gtz v0, :cond_24

    iget-object v0, p0, Lcom/google/android/gms/internal/zziv;->zzMo:[D

    aget-wide v0, v0, v3

    cmpg-double v0, p1, v0

    if-gez v0, :cond_24

    iget-object v0, p0, Lcom/google/android/gms/internal/zziv;->zzMq:[I

    aget v1, v0, v3

    add-int/lit8 v1, v1, 0x1

    aput v1, v0, v3

    :cond_24
    iget-object v0, p0, Lcom/google/android/gms/internal/zziv;->zzMp:[D

    aget-wide v0, v0, v3

    cmpg-double v0, p1, v0

    if-gez v0, :cond_2d

    goto :goto_30

    :cond_2d
    add-int/lit8 v3, v3, 0x1

    goto :goto_7

    :cond_30
    :goto_30
    return-void
.end method

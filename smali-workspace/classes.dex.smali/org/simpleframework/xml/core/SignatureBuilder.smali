.class Lorg/simpleframework/xml/core/SignatureBuilder;
.super Ljava/lang/Object;
.source "SignatureBuilder.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/simpleframework/xml/core/SignatureBuilder$ParameterList;,
        Lorg/simpleframework/xml/core/SignatureBuilder$ParameterTable;
    }
.end annotation


# instance fields
.field private final factory:Ljava/lang/reflect/Constructor;

.field private final table:Lorg/simpleframework/xml/core/SignatureBuilder$ParameterTable;


# direct methods
.method public constructor <init>(Ljava/lang/reflect/Constructor;)V
    .registers 3
    .param p1, "factory"    # Ljava/lang/reflect/Constructor;

    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    new-instance v0, Lorg/simpleframework/xml/core/SignatureBuilder$ParameterTable;

    invoke-direct {v0}, Lorg/simpleframework/xml/core/SignatureBuilder$ParameterTable;-><init>()V

    iput-object v0, p0, Lorg/simpleframework/xml/core/SignatureBuilder;->table:Lorg/simpleframework/xml/core/SignatureBuilder$ParameterTable;

    .line 58
    iput-object p1, p0, Lorg/simpleframework/xml/core/SignatureBuilder;->factory:Ljava/lang/reflect/Constructor;

    .line 59
    return-void
.end method

.method private build(Lorg/simpleframework/xml/core/SignatureBuilder$ParameterTable;)Ljava/util/List;
    .registers 3
    .param p1, "matrix"    # Lorg/simpleframework/xml/core/SignatureBuilder$ParameterTable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Lorg/simpleframework/xml/core/SignatureBuilder$ParameterTable;)Ljava/util/List<Lorg/simpleframework/xml/core/Signature;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 111
    iget-object v0, p0, Lorg/simpleframework/xml/core/SignatureBuilder;->table:Lorg/simpleframework/xml/core/SignatureBuilder$ParameterTable;

    invoke-virtual {v0}, Lorg/simpleframework/xml/core/SignatureBuilder$ParameterTable;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 112
    invoke-direct {p0}, Lorg/simpleframework/xml/core/SignatureBuilder;->create()Ljava/util/List;

    move-result-object v0

    return-object v0

    .line 114
    :cond_d
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lorg/simpleframework/xml/core/SignatureBuilder;->build(Lorg/simpleframework/xml/core/SignatureBuilder$ParameterTable;I)V

    .line 115
    invoke-direct {p0, p1}, Lorg/simpleframework/xml/core/SignatureBuilder;->create(Lorg/simpleframework/xml/core/SignatureBuilder$ParameterTable;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method private build(Lorg/simpleframework/xml/core/SignatureBuilder$ParameterTable;I)V
    .registers 4
    .param p1, "matrix"    # Lorg/simpleframework/xml/core/SignatureBuilder$ParameterTable;
    .param p2, "index"    # I

    .line 178
    new-instance v0, Lorg/simpleframework/xml/core/SignatureBuilder$ParameterList;

    invoke-direct {v0}, Lorg/simpleframework/xml/core/SignatureBuilder$ParameterList;-><init>()V

    invoke-direct {p0, p1, v0, p2}, Lorg/simpleframework/xml/core/SignatureBuilder;->build(Lorg/simpleframework/xml/core/SignatureBuilder$ParameterTable;Lorg/simpleframework/xml/core/SignatureBuilder$ParameterList;I)V

    .line 179
    return-void
.end method

.method private build(Lorg/simpleframework/xml/core/SignatureBuilder$ParameterTable;Lorg/simpleframework/xml/core/SignatureBuilder$ParameterList;I)V
    .registers 11
    .param p1, "matrix"    # Lorg/simpleframework/xml/core/SignatureBuilder$ParameterTable;
    .param p2, "signature"    # Lorg/simpleframework/xml/core/SignatureBuilder$ParameterList;
    .param p3, "index"    # I

    .line 192
    iget-object v0, p0, Lorg/simpleframework/xml/core/SignatureBuilder;->table:Lorg/simpleframework/xml/core/SignatureBuilder$ParameterTable;

    invoke-virtual {v0, p3}, Lorg/simpleframework/xml/core/SignatureBuilder$ParameterTable;->get(I)Lorg/simpleframework/xml/core/SignatureBuilder$ParameterList;

    move-result-object v1

    .line 193
    .local v1, "column":Lorg/simpleframework/xml/core/SignatureBuilder$ParameterList;
    invoke-virtual {v1}, Lorg/simpleframework/xml/core/SignatureBuilder$ParameterList;->size()I

    move-result v2

    .line 194
    .local v2, "height":I
    iget-object v0, p0, Lorg/simpleframework/xml/core/SignatureBuilder;->table:Lorg/simpleframework/xml/core/SignatureBuilder$ParameterTable;

    # invokes: Lorg/simpleframework/xml/core/SignatureBuilder$ParameterTable;->width()I
    invoke-static {v0}, Lorg/simpleframework/xml/core/SignatureBuilder$ParameterTable;->access$000(Lorg/simpleframework/xml/core/SignatureBuilder$ParameterTable;)I

    move-result v3

    .line 196
    .local v3, "width":I
    add-int/lit8 v0, v3, -0x1

    if-le v0, p3, :cond_31

    .line 197
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_15
    if-ge v4, v2, :cond_30

    .line 198
    new-instance v5, Lorg/simpleframework/xml/core/SignatureBuilder$ParameterList;

    invoke-direct {v5, p2}, Lorg/simpleframework/xml/core/SignatureBuilder$ParameterList;-><init>(Lorg/simpleframework/xml/core/SignatureBuilder$ParameterList;)V

    .line 200
    .local v5, "extended":Lorg/simpleframework/xml/core/SignatureBuilder$ParameterList;
    if-eqz p2, :cond_2d

    .line 201
    invoke-virtual {v1, v4}, Lorg/simpleframework/xml/core/SignatureBuilder$ParameterList;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lorg/simpleframework/xml/core/Parameter;

    .line 203
    .local v6, "parameter":Lorg/simpleframework/xml/core/Parameter;
    invoke-virtual {v5, v6}, Lorg/simpleframework/xml/core/SignatureBuilder$ParameterList;->add(Ljava/lang/Object;)Z

    .line 204
    add-int/lit8 v0, p3, 0x1

    invoke-direct {p0, p1, v5, v0}, Lorg/simpleframework/xml/core/SignatureBuilder;->build(Lorg/simpleframework/xml/core/SignatureBuilder$ParameterTable;Lorg/simpleframework/xml/core/SignatureBuilder$ParameterList;I)V

    .line 197
    .end local v5    # "extended":Lorg/simpleframework/xml/core/SignatureBuilder$ParameterList;
    .end local v6    # "parameter":Lorg/simpleframework/xml/core/Parameter;
    :cond_2d
    add-int/lit8 v4, v4, 0x1

    goto :goto_15

    .end local v4    # "i":I
    :cond_30
    goto :goto_34

    .line 208
    :cond_31
    invoke-direct {p0, p1, p2, p3}, Lorg/simpleframework/xml/core/SignatureBuilder;->populate(Lorg/simpleframework/xml/core/SignatureBuilder$ParameterTable;Lorg/simpleframework/xml/core/SignatureBuilder$ParameterList;I)V

    .line 210
    :goto_34
    return-void
.end method

.method private create()Ljava/util/List;
    .registers 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Lorg/simpleframework/xml/core/Signature;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 126
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 127
    .local v1, "list":Ljava/util/List;, "Ljava/util/List<Lorg/simpleframework/xml/core/Signature;>;"
    new-instance v2, Lorg/simpleframework/xml/core/Signature;

    iget-object v0, p0, Lorg/simpleframework/xml/core/SignatureBuilder;->factory:Ljava/lang/reflect/Constructor;

    invoke-direct {v2, v0}, Lorg/simpleframework/xml/core/Signature;-><init>(Ljava/lang/reflect/Constructor;)V

    .line 129
    .local v2, "signature":Lorg/simpleframework/xml/core/Signature;
    invoke-virtual {p0}, Lorg/simpleframework/xml/core/SignatureBuilder;->isValid()Z

    move-result v0

    if-eqz v0, :cond_15

    .line 130
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 132
    :cond_15
    return-object v1
.end method

.method private create(Lorg/simpleframework/xml/core/SignatureBuilder$ParameterTable;)Ljava/util/List;
    .registers 16
    .param p1, "matrix"    # Lorg/simpleframework/xml/core/SignatureBuilder$ParameterTable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Lorg/simpleframework/xml/core/SignatureBuilder$ParameterTable;)Ljava/util/List<Lorg/simpleframework/xml/core/Signature;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 146
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 147
    .local v5, "list":Ljava/util/List;, "Ljava/util/List<Lorg/simpleframework/xml/core/Signature;>;"
    # invokes: Lorg/simpleframework/xml/core/SignatureBuilder$ParameterTable;->height()I
    invoke-static {p1}, Lorg/simpleframework/xml/core/SignatureBuilder$ParameterTable;->access$100(Lorg/simpleframework/xml/core/SignatureBuilder$ParameterTable;)I

    move-result v6

    .line 148
    .local v6, "height":I
    # invokes: Lorg/simpleframework/xml/core/SignatureBuilder$ParameterTable;->width()I
    invoke-static {p1}, Lorg/simpleframework/xml/core/SignatureBuilder$ParameterTable;->access$000(Lorg/simpleframework/xml/core/SignatureBuilder$ParameterTable;)I

    move-result v7

    .line 150
    .local v7, "width":I
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_e
    if-ge v8, v6, :cond_4b

    .line 151
    new-instance v9, Lorg/simpleframework/xml/core/Signature;

    iget-object v0, p0, Lorg/simpleframework/xml/core/SignatureBuilder;->factory:Ljava/lang/reflect/Constructor;

    invoke-direct {v9, v0}, Lorg/simpleframework/xml/core/Signature;-><init>(Ljava/lang/reflect/Constructor;)V

    .line 153
    .local v9, "signature":Lorg/simpleframework/xml/core/Signature;
    const/4 v10, 0x0

    .local v10, "j":I
    :goto_18
    if-ge v10, v7, :cond_45

    .line 154
    invoke-virtual {p1, v10, v8}, Lorg/simpleframework/xml/core/SignatureBuilder$ParameterTable;->get(II)Lorg/simpleframework/xml/core/Parameter;

    move-result-object v11

    .line 155
    .local v11, "parameter":Lorg/simpleframework/xml/core/Parameter;
    invoke-interface {v11}, Lorg/simpleframework/xml/core/Parameter;->getPath()Ljava/lang/String;

    move-result-object v12

    .line 156
    .local v12, "path":Ljava/lang/String;
    invoke-interface {v11}, Lorg/simpleframework/xml/core/Parameter;->getKey()Ljava/lang/Object;

    move-result-object v13

    .line 158
    .local v13, "key":Ljava/lang/Object;
    invoke-virtual {v9, v13}, Lorg/simpleframework/xml/core/Signature;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3f

    .line 159
    new-instance v0, Lorg/simpleframework/xml/core/ConstructorException;

    const-string v1, "Parameter \'%s\' is a duplicate in %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v12, v2, v3

    iget-object v3, p0, Lorg/simpleframework/xml/core/SignatureBuilder;->factory:Ljava/lang/reflect/Constructor;

    const/4 v4, 0x1

    aput-object v3, v2, v4

    invoke-direct {v0, v1, v2}, Lorg/simpleframework/xml/core/ConstructorException;-><init>(Ljava/lang/String;[Ljava/lang/Object;)V

    throw v0

    .line 161
    :cond_3f
    invoke-virtual {v9, v11}, Lorg/simpleframework/xml/core/Signature;->add(Lorg/simpleframework/xml/core/Parameter;)V

    .line 153
    .end local v11    # "parameter":Lorg/simpleframework/xml/core/Parameter;
    .end local v12    # "path":Ljava/lang/String;
    .end local v13    # "key":Ljava/lang/Object;
    add-int/lit8 v10, v10, 0x1

    goto :goto_18

    .line 163
    .end local v10    # "j":I
    :cond_45
    invoke-interface {v5, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 150
    .end local v9    # "signature":Lorg/simpleframework/xml/core/Signature;
    add-int/lit8 v8, v8, 0x1

    goto :goto_e

    .line 165
    .end local v8    # "i":I
    :cond_4b
    return-object v5
.end method

.method private populate(Lorg/simpleframework/xml/core/SignatureBuilder$ParameterTable;Lorg/simpleframework/xml/core/SignatureBuilder$ParameterList;I)V
    .registers 12
    .param p1, "matrix"    # Lorg/simpleframework/xml/core/SignatureBuilder$ParameterTable;
    .param p2, "signature"    # Lorg/simpleframework/xml/core/SignatureBuilder$ParameterList;
    .param p3, "index"    # I

    .line 223
    iget-object v0, p0, Lorg/simpleframework/xml/core/SignatureBuilder;->table:Lorg/simpleframework/xml/core/SignatureBuilder$ParameterTable;

    invoke-virtual {v0, p3}, Lorg/simpleframework/xml/core/SignatureBuilder$ParameterTable;->get(I)Lorg/simpleframework/xml/core/SignatureBuilder$ParameterList;

    move-result-object v1

    .line 224
    .local v1, "column":Lorg/simpleframework/xml/core/SignatureBuilder$ParameterList;
    invoke-virtual {p2}, Lorg/simpleframework/xml/core/SignatureBuilder$ParameterList;->size()I

    move-result v2

    .line 225
    .local v2, "width":I
    invoke-virtual {v1}, Lorg/simpleframework/xml/core/SignatureBuilder$ParameterList;->size()I

    move-result v3

    .line 227
    .local v3, "height":I
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_f
    if-ge v4, v3, :cond_36

    .line 228
    const/4 v5, 0x0

    .local v5, "j":I
    :goto_12
    if-ge v5, v2, :cond_25

    .line 229
    invoke-virtual {p1, v5}, Lorg/simpleframework/xml/core/SignatureBuilder$ParameterTable;->get(I)Lorg/simpleframework/xml/core/SignatureBuilder$ParameterList;

    move-result-object v6

    .line 230
    .local v6, "list":Lorg/simpleframework/xml/core/SignatureBuilder$ParameterList;
    invoke-virtual {p2, v5}, Lorg/simpleframework/xml/core/SignatureBuilder$ParameterList;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lorg/simpleframework/xml/core/Parameter;

    .line 232
    .local v7, "parameter":Lorg/simpleframework/xml/core/Parameter;
    invoke-virtual {v6, v7}, Lorg/simpleframework/xml/core/SignatureBuilder$ParameterList;->add(Ljava/lang/Object;)Z

    .line 228
    .end local v6    # "list":Lorg/simpleframework/xml/core/SignatureBuilder$ParameterList;
    .end local v7    # "parameter":Lorg/simpleframework/xml/core/Parameter;
    add-int/lit8 v5, v5, 0x1

    goto :goto_12

    .line 234
    .end local v5    # "j":I
    :cond_25
    invoke-virtual {p1, p3}, Lorg/simpleframework/xml/core/SignatureBuilder$ParameterTable;->get(I)Lorg/simpleframework/xml/core/SignatureBuilder$ParameterList;

    move-result-object v5

    .line 235
    .local v5, "list":Lorg/simpleframework/xml/core/SignatureBuilder$ParameterList;
    invoke-virtual {v1, v4}, Lorg/simpleframework/xml/core/SignatureBuilder$ParameterList;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lorg/simpleframework/xml/core/Parameter;

    .line 237
    .local v6, "parameter":Lorg/simpleframework/xml/core/Parameter;
    invoke-virtual {v5, v6}, Lorg/simpleframework/xml/core/SignatureBuilder$ParameterList;->add(Ljava/lang/Object;)Z

    .line 227
    .end local v5    # "list":Lorg/simpleframework/xml/core/SignatureBuilder$ParameterList;
    .end local v6    # "parameter":Lorg/simpleframework/xml/core/Parameter;
    add-int/lit8 v4, v4, 0x1

    goto :goto_f

    .line 239
    .end local v4    # "i":I
    :cond_36
    return-void
.end method


# virtual methods
.method public build()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Lorg/simpleframework/xml/core/Signature;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 97
    new-instance v0, Lorg/simpleframework/xml/core/SignatureBuilder$ParameterTable;

    invoke-direct {v0}, Lorg/simpleframework/xml/core/SignatureBuilder$ParameterTable;-><init>()V

    invoke-direct {p0, v0}, Lorg/simpleframework/xml/core/SignatureBuilder;->build(Lorg/simpleframework/xml/core/SignatureBuilder$ParameterTable;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public insert(Lorg/simpleframework/xml/core/Parameter;I)V
    .registers 4
    .param p1, "value"    # Lorg/simpleframework/xml/core/Parameter;
    .param p2, "index"    # I

    .line 85
    iget-object v0, p0, Lorg/simpleframework/xml/core/SignatureBuilder;->table:Lorg/simpleframework/xml/core/SignatureBuilder$ParameterTable;

    invoke-virtual {v0, p1, p2}, Lorg/simpleframework/xml/core/SignatureBuilder$ParameterTable;->insert(Lorg/simpleframework/xml/core/Parameter;I)V

    .line 86
    return-void
.end method

.method public isValid()Z
    .registers 4

    .line 70
    iget-object v0, p0, Lorg/simpleframework/xml/core/SignatureBuilder;->factory:Ljava/lang/reflect/Constructor;

    invoke-virtual {v0}, Ljava/lang/reflect/Constructor;->getParameterTypes()[Ljava/lang/Class;

    move-result-object v1

    .line 71
    .local v1, "types":[Ljava/lang/Class;
    iget-object v0, p0, Lorg/simpleframework/xml/core/SignatureBuilder;->table:Lorg/simpleframework/xml/core/SignatureBuilder$ParameterTable;

    # invokes: Lorg/simpleframework/xml/core/SignatureBuilder$ParameterTable;->width()I
    invoke-static {v0}, Lorg/simpleframework/xml/core/SignatureBuilder$ParameterTable;->access$000(Lorg/simpleframework/xml/core/SignatureBuilder$ParameterTable;)I

    move-result v2

    .line 73
    .local v2, "width":I
    array-length v0, v1

    if-ne v0, v2, :cond_11

    const/4 v0, 0x1

    goto :goto_12

    :cond_11
    const/4 v0, 0x0

    :goto_12
    return v0
.end method

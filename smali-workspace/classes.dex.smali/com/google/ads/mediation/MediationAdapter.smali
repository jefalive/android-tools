.class public interface abstract Lcom/google/ads/mediation/MediationAdapter;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<ADDITIONAL_PARAMETERS:Ljava/lang/Object;SERVER_PARAMETERS:Lcom/google/ads/mediation/MediationServerParameters;>Ljava/lang/Object;"
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# virtual methods
.method public abstract destroy()V
.end method

.method public abstract getAdditionalParametersType()Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/lang/Class<TADDITIONAL_PARAMETERS;>;"
        }
    .end annotation
.end method

.method public abstract getServerParametersType()Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/lang/Class<TSERVER_PARAMETERS;>;"
        }
    .end annotation
.end method

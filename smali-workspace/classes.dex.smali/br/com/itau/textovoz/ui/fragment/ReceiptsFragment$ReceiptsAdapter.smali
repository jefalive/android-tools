.class Lbr/com/itau/textovoz/ui/fragment/ReceiptsFragment$ReceiptsAdapter;
.super Landroid/support/v7/widget/RecyclerView$Adapter;
.source "ReceiptsFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lbr/com/itau/textovoz/ui/fragment/ReceiptsFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ReceiptsAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/support/v7/widget/RecyclerView$Adapter<Lbr/com/itau/textovoz/ui/fragment/ReceiptsFragment$ReceiptHolder;>;"
    }
.end annotation


# instance fields
.field private receipts:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Lbr/com/itau/textovoz/model/Receipt;>;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lbr/com/itau/textovoz/ui/fragment/ReceiptsFragment;


# direct methods
.method public constructor <init>(Lbr/com/itau/textovoz/ui/fragment/ReceiptsFragment;Ljava/util/List;)V
    .registers 3
    .param p2, "receipts"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<Lbr/com/itau/textovoz/model/Receipt;>;)V"
        }
    .end annotation

    .line 161
    iput-object p1, p0, Lbr/com/itau/textovoz/ui/fragment/ReceiptsFragment$ReceiptsAdapter;->this$0:Lbr/com/itau/textovoz/ui/fragment/ReceiptsFragment;

    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView$Adapter;-><init>()V

    .line 162
    iput-object p2, p0, Lbr/com/itau/textovoz/ui/fragment/ReceiptsFragment$ReceiptsAdapter;->receipts:Ljava/util/List;

    .line 163
    return-void
.end method


# virtual methods
.method public getItemCount()I
    .registers 2

    .line 185
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/fragment/ReceiptsFragment$ReceiptsAdapter;->receipts:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public bridge synthetic onBindViewHolder(Landroid/support/v7/widget/RecyclerView$ViewHolder;I)V
    .registers 4

    .line 158
    move-object v0, p1

    check-cast v0, Lbr/com/itau/textovoz/ui/fragment/ReceiptsFragment$ReceiptHolder;

    invoke-virtual {p0, v0, p2}, Lbr/com/itau/textovoz/ui/fragment/ReceiptsFragment$ReceiptsAdapter;->onBindViewHolder(Lbr/com/itau/textovoz/ui/fragment/ReceiptsFragment$ReceiptHolder;I)V

    return-void
.end method

.method public onBindViewHolder(Lbr/com/itau/textovoz/ui/fragment/ReceiptsFragment$ReceiptHolder;I)V
    .registers 5
    .param p1, "holder"    # Lbr/com/itau/textovoz/ui/fragment/ReceiptsFragment$ReceiptHolder;
    .param p2, "position"    # I

    .line 179
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/fragment/ReceiptsFragment$ReceiptsAdapter;->receipts:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lbr/com/itau/textovoz/model/Receipt;

    .line 180
    .local v1, "r":Lbr/com/itau/textovoz/model/Receipt;
    invoke-virtual {p1, v1}, Lbr/com/itau/textovoz/ui/fragment/ReceiptsFragment$ReceiptHolder;->bindReceipt(Lbr/com/itau/textovoz/model/Receipt;)V

    .line 181
    return-void
.end method

.method public bridge synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .registers 4

    .line 158
    invoke-virtual {p0, p1, p2}, Lbr/com/itau/textovoz/ui/fragment/ReceiptsFragment$ReceiptsAdapter;->onCreateViewHolder(Landroid/view/ViewGroup;I)Lbr/com/itau/textovoz/ui/fragment/ReceiptsFragment$ReceiptHolder;

    move-result-object v0

    return-object v0
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Lbr/com/itau/textovoz/ui/fragment/ReceiptsFragment$ReceiptHolder;
    .registers 7
    .param p1, "parent"    # Landroid/view/ViewGroup;
    .param p2, "viewType"    # I

    .line 171
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/fragment/ReceiptsFragment$ReceiptsAdapter;->this$0:Lbr/com/itau/textovoz/ui/fragment/ReceiptsFragment;

    invoke-virtual {v0}, Lbr/com/itau/textovoz/ui/fragment/ReceiptsFragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    .line 172
    .local v2, "inflater":Landroid/view/LayoutInflater;
    sget v0, Lbr/com/itau/textovoz/R$layout;->view_item_receipt:I

    const/4 v1, 0x0

    invoke-virtual {v2, v0, p1, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    .line 174
    .local v3, "v":Landroid/view/View;
    new-instance v0, Lbr/com/itau/textovoz/ui/fragment/ReceiptsFragment$ReceiptHolder;

    iget-object v1, p0, Lbr/com/itau/textovoz/ui/fragment/ReceiptsFragment$ReceiptsAdapter;->this$0:Lbr/com/itau/textovoz/ui/fragment/ReceiptsFragment;

    invoke-direct {v0, v1, v3}, Lbr/com/itau/textovoz/ui/fragment/ReceiptsFragment$ReceiptHolder;-><init>(Lbr/com/itau/textovoz/ui/fragment/ReceiptsFragment;Landroid/view/View;)V

    return-object v0
.end method

.method public setReceipts(Ljava/util/List;)V
    .registers 2
    .param p1, "receipts"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<Lbr/com/itau/textovoz/model/Receipt;>;)V"
        }
    .end annotation

    .line 166
    iput-object p1, p0, Lbr/com/itau/textovoz/ui/fragment/ReceiptsFragment$ReceiptsAdapter;->receipts:Ljava/util/List;

    .line 167
    return-void
.end method

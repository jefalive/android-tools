.class public abstract Lcom/google/android/gms/ads/internal/request/zzd;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/ads/internal/request/zzc$zza;
.implements Lcom/google/android/gms/internal/zzit;


# annotations
.annotation runtime Lcom/google/android/gms/internal/zzhb;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/ads/internal/request/zzd$zzb;,
        Lcom/google/android/gms/ads/internal/request/zzd$zza;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;Lcom/google/android/gms/ads/internal/request/zzc$zza;Lcom/google/android/gms/internal/zzit<Ljava/lang/Void;>;"
    }
.end annotation


# instance fields
.field private final zzHl:Lcom/google/android/gms/internal/zzji;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/zzji<Lcom/google/android/gms/ads/internal/request/AdRequestInfoParcel;>;"
        }
    .end annotation
.end field

.field private final zzHm:Lcom/google/android/gms/ads/internal/request/zzc$zza;

.field private final zzpV:Ljava/lang/Object;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/internal/zzji;Lcom/google/android/gms/ads/internal/request/zzc$zza;)V
    .registers 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Lcom/google/android/gms/internal/zzji<Lcom/google/android/gms/ads/internal/request/AdRequestInfoParcel;>;Lcom/google/android/gms/ads/internal/request/zzc$zza;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/ads/internal/request/zzd;->zzpV:Ljava/lang/Object;

    iput-object p1, p0, Lcom/google/android/gms/ads/internal/request/zzd;->zzHl:Lcom/google/android/gms/internal/zzji;

    iput-object p2, p0, Lcom/google/android/gms/ads/internal/request/zzd;->zzHm:Lcom/google/android/gms/ads/internal/request/zzc$zza;

    return-void
.end method


# virtual methods
.method public cancel()V
    .registers 1

    invoke-virtual {p0}, Lcom/google/android/gms/ads/internal/request/zzd;->zzgr()V

    return-void
.end method

.method zza(Lcom/google/android/gms/ads/internal/request/zzj;Lcom/google/android/gms/ads/internal/request/AdRequestInfoParcel;)Z
    .registers 7

    :try_start_0
    new-instance v0, Lcom/google/android/gms/ads/internal/request/zzg;

    invoke-direct {v0, p0}, Lcom/google/android/gms/ads/internal/request/zzg;-><init>(Lcom/google/android/gms/ads/internal/request/zzc$zza;)V

    invoke-interface {p1, p2, v0}, Lcom/google/android/gms/ads/internal/request/zzj;->zza(Lcom/google/android/gms/ads/internal/request/AdRequestInfoParcel;Lcom/google/android/gms/ads/internal/request/zzk;)V
    :try_end_8
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_8} :catch_a
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_8} :catch_19
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_8} :catch_28
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_8} :catch_37

    const/4 v0, 0x1

    return v0

    :catch_a
    move-exception v3

    const-string v0, "Could not fetch ad response from ad request service."

    invoke-static {v0, v3}, Lcom/google/android/gms/internal/zzin;->zzd(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-static {}, Lcom/google/android/gms/ads/internal/zzr;->zzbF()Lcom/google/android/gms/internal/zzih;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v3, v1}, Lcom/google/android/gms/internal/zzih;->zzb(Ljava/lang/Throwable;Z)V

    goto :goto_45

    :catch_19
    move-exception v3

    const-string v0, "Could not fetch ad response from ad request service due to an Exception."

    invoke-static {v0, v3}, Lcom/google/android/gms/internal/zzin;->zzd(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-static {}, Lcom/google/android/gms/ads/internal/zzr;->zzbF()Lcom/google/android/gms/internal/zzih;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v3, v1}, Lcom/google/android/gms/internal/zzih;->zzb(Ljava/lang/Throwable;Z)V

    goto :goto_45

    :catch_28
    move-exception v3

    const-string v0, "Could not fetch ad response from ad request service due to an Exception."

    invoke-static {v0, v3}, Lcom/google/android/gms/internal/zzin;->zzd(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-static {}, Lcom/google/android/gms/ads/internal/zzr;->zzbF()Lcom/google/android/gms/internal/zzih;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v3, v1}, Lcom/google/android/gms/internal/zzih;->zzb(Ljava/lang/Throwable;Z)V

    goto :goto_45

    :catch_37
    move-exception v3

    const-string v0, "Could not fetch ad response from ad request service due to an Exception."

    invoke-static {v0, v3}, Lcom/google/android/gms/internal/zzin;->zzd(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-static {}, Lcom/google/android/gms/ads/internal/zzr;->zzbF()Lcom/google/android/gms/internal/zzih;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v3, v1}, Lcom/google/android/gms/internal/zzih;->zzb(Ljava/lang/Throwable;Z)V

    :goto_45
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/request/zzd;->zzHm:Lcom/google/android/gms/ads/internal/request/zzc$zza;

    new-instance v1, Lcom/google/android/gms/ads/internal/request/AdResponseParcel;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/android/gms/ads/internal/request/AdResponseParcel;-><init>(I)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/ads/internal/request/zzc$zza;->zzb(Lcom/google/android/gms/ads/internal/request/AdResponseParcel;)V

    const/4 v0, 0x0

    return v0
.end method

.method public zzb(Lcom/google/android/gms/ads/internal/request/AdResponseParcel;)V
    .registers 5

    iget-object v1, p0, Lcom/google/android/gms/ads/internal/request/zzd;->zzpV:Ljava/lang/Object;

    monitor-enter v1

    :try_start_3
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/request/zzd;->zzHm:Lcom/google/android/gms/ads/internal/request/zzc$zza;

    invoke-interface {v0, p1}, Lcom/google/android/gms/ads/internal/request/zzc$zza;->zzb(Lcom/google/android/gms/ads/internal/request/AdResponseParcel;)V

    invoke-virtual {p0}, Lcom/google/android/gms/ads/internal/request/zzd;->zzgr()V
    :try_end_b
    .catchall {:try_start_3 .. :try_end_b} :catchall_d

    monitor-exit v1

    goto :goto_10

    :catchall_d
    move-exception v2

    monitor-exit v1

    throw v2

    :goto_10
    return-void
.end method

.method public zzga()Ljava/lang/Void;
    .registers 5

    invoke-virtual {p0}, Lcom/google/android/gms/ads/internal/request/zzd;->zzgs()Lcom/google/android/gms/ads/internal/request/zzj;

    move-result-object v3

    if-nez v3, :cond_16

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/request/zzd;->zzHm:Lcom/google/android/gms/ads/internal/request/zzc$zza;

    new-instance v1, Lcom/google/android/gms/ads/internal/request/AdResponseParcel;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/android/gms/ads/internal/request/AdResponseParcel;-><init>(I)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/ads/internal/request/zzc$zza;->zzb(Lcom/google/android/gms/ads/internal/request/AdResponseParcel;)V

    invoke-virtual {p0}, Lcom/google/android/gms/ads/internal/request/zzd;->zzgr()V

    const/4 v0, 0x0

    return-object v0

    :cond_16
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/request/zzd;->zzHl:Lcom/google/android/gms/internal/zzji;

    new-instance v1, Lcom/google/android/gms/ads/internal/request/zzd$1;

    invoke-direct {v1, p0, v3}, Lcom/google/android/gms/ads/internal/request/zzd$1;-><init>(Lcom/google/android/gms/ads/internal/request/zzd;Lcom/google/android/gms/ads/internal/request/zzj;)V

    new-instance v2, Lcom/google/android/gms/ads/internal/request/zzd$2;

    invoke-direct {v2, p0}, Lcom/google/android/gms/ads/internal/request/zzd$2;-><init>(Lcom/google/android/gms/ads/internal/request/zzd;)V

    invoke-interface {v0, v1, v2}, Lcom/google/android/gms/internal/zzji;->zza(Lcom/google/android/gms/internal/zzji$zzc;Lcom/google/android/gms/internal/zzji$zza;)V

    const/4 v0, 0x0

    return-object v0
.end method

.method public synthetic zzgd()Ljava/lang/Object;
    .registers 2

    invoke-virtual {p0}, Lcom/google/android/gms/ads/internal/request/zzd;->zzga()Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method public abstract zzgr()V
.end method

.method public abstract zzgs()Lcom/google/android/gms/ads/internal/request/zzj;
.end method

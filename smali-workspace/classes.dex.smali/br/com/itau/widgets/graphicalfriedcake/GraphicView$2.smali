.class Lbr/com/itau/widgets/graphicalfriedcake/GraphicView$2;
.super Landroid/support/v4/view/ViewPager$SimpleOnPageChangeListener;
.source "GraphicView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lbr/com/itau/widgets/graphicalfriedcake/GraphicView;->setDetailsPagerAdapter(Landroid/support/v4/view/PagerAdapter;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lbr/com/itau/widgets/graphicalfriedcake/GraphicView;


# direct methods
.method constructor <init>(Lbr/com/itau/widgets/graphicalfriedcake/GraphicView;)V
    .registers 2
    .param p1, "this$0"    # Lbr/com/itau/widgets/graphicalfriedcake/GraphicView;

    .line 184
    iput-object p1, p0, Lbr/com/itau/widgets/graphicalfriedcake/GraphicView$2;->this$0:Lbr/com/itau/widgets/graphicalfriedcake/GraphicView;

    invoke-direct {p0}, Landroid/support/v4/view/ViewPager$SimpleOnPageChangeListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onPageSelected(I)V
    .registers 5
    .param p1, "position"    # I

    .line 187
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/GraphicView$2;->this$0:Lbr/com/itau/widgets/graphicalfriedcake/GraphicView;

    # getter for: Lbr/com/itau/widgets/graphicalfriedcake/GraphicView;->items:Ljava/util/List;
    invoke-static {v0}, Lbr/com/itau/widgets/graphicalfriedcake/GraphicView;->access$100(Lbr/com/itau/widgets/graphicalfriedcake/GraphicView;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, p1, :cond_17

    .line 188
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/GraphicView$2;->this$0:Lbr/com/itau/widgets/graphicalfriedcake/GraphicView;

    # getter for: Lbr/com/itau/widgets/graphicalfriedcake/GraphicView;->pagerDetailsList:Landroid/support/v4/view/ViewPager;
    invoke-static {v0}, Lbr/com/itau/widgets/graphicalfriedcake/GraphicView;->access$000(Lbr/com/itau/widgets/graphicalfriedcake/GraphicView;)Landroid/support/v4/view/ViewPager;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/view/ViewPager;->setCurrentItem(IZ)V

    .line 191
    :cond_17
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/GraphicView$2;->this$0:Lbr/com/itau/widgets/graphicalfriedcake/GraphicView;

    # getter for: Lbr/com/itau/widgets/graphicalfriedcake/GraphicView;->items:Ljava/util/List;
    invoke-static {v0}, Lbr/com/itau/widgets/graphicalfriedcake/GraphicView;->access$100(Lbr/com/itau/widgets/graphicalfriedcake/GraphicView;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge p1, v0, :cond_42

    .line 192
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/GraphicView$2;->this$0:Lbr/com/itau/widgets/graphicalfriedcake/GraphicView;

    # invokes: Lbr/com/itau/widgets/graphicalfriedcake/GraphicView;->rotateChartToPosition(I)V
    invoke-static {v0, p1}, Lbr/com/itau/widgets/graphicalfriedcake/GraphicView;->access$200(Lbr/com/itau/widgets/graphicalfriedcake/GraphicView;I)V

    .line 193
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/GraphicView$2;->this$0:Lbr/com/itau/widgets/graphicalfriedcake/GraphicView;

    # getter for: Lbr/com/itau/widgets/graphicalfriedcake/GraphicView;->indicatorDetailsList:Lbr/com/itau/widgets/graphicalfriedcake/PageIndicator;
    invoke-static {v0}, Lbr/com/itau/widgets/graphicalfriedcake/GraphicView;->access$300(Lbr/com/itau/widgets/graphicalfriedcake/GraphicView;)Lbr/com/itau/widgets/graphicalfriedcake/PageIndicator;

    move-result-object v0

    iget-object v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/GraphicView$2;->this$0:Lbr/com/itau/widgets/graphicalfriedcake/GraphicView;

    # getter for: Lbr/com/itau/widgets/graphicalfriedcake/GraphicView;->items:Ljava/util/List;
    invoke-static {v1}, Lbr/com/itau/widgets/graphicalfriedcake/GraphicView;->access$100(Lbr/com/itau/widgets/graphicalfriedcake/GraphicView;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lbr/com/itau/widgets/graphicalfriedcake/model/ItemVO;

    invoke-virtual {v1}, Lbr/com/itau/widgets/graphicalfriedcake/model/ItemVO;->getColor()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbr/com/itau/widgets/graphicalfriedcake/PageIndicator;->refresh(Ljava/lang/String;)V

    goto :goto_63

    .line 199
    :cond_42
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/GraphicView$2;->this$0:Lbr/com/itau/widgets/graphicalfriedcake/GraphicView;

    const/16 v1, 0x168

    # invokes: Lbr/com/itau/widgets/graphicalfriedcake/GraphicView;->rotateChartToPosition(I)V
    invoke-static {v0, v1}, Lbr/com/itau/widgets/graphicalfriedcake/GraphicView;->access$200(Lbr/com/itau/widgets/graphicalfriedcake/GraphicView;I)V

    .line 200
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/GraphicView$2;->this$0:Lbr/com/itau/widgets/graphicalfriedcake/GraphicView;

    # getter for: Lbr/com/itau/widgets/graphicalfriedcake/GraphicView;->indicatorDetailsList:Lbr/com/itau/widgets/graphicalfriedcake/PageIndicator;
    invoke-static {v0}, Lbr/com/itau/widgets/graphicalfriedcake/GraphicView;->access$300(Lbr/com/itau/widgets/graphicalfriedcake/GraphicView;)Lbr/com/itau/widgets/graphicalfriedcake/PageIndicator;

    move-result-object v0

    iget-object v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/GraphicView$2;->this$0:Lbr/com/itau/widgets/graphicalfriedcake/GraphicView;

    # getter for: Lbr/com/itau/widgets/graphicalfriedcake/GraphicView;->items:Ljava/util/List;
    invoke-static {v1}, Lbr/com/itau/widgets/graphicalfriedcake/GraphicView;->access$100(Lbr/com/itau/widgets/graphicalfriedcake/GraphicView;)Ljava/util/List;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lbr/com/itau/widgets/graphicalfriedcake/model/ItemVO;

    invoke-virtual {v1}, Lbr/com/itau/widgets/graphicalfriedcake/model/ItemVO;->getColor()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbr/com/itau/widgets/graphicalfriedcake/PageIndicator;->refresh(Ljava/lang/String;)V

    .line 202
    :goto_63
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/GraphicView$2;->this$0:Lbr/com/itau/widgets/graphicalfriedcake/GraphicView;

    # setter for: Lbr/com/itau/widgets/graphicalfriedcake/GraphicView;->currentPosition:I
    invoke-static {v0, p1}, Lbr/com/itau/widgets/graphicalfriedcake/GraphicView;->access$402(Lbr/com/itau/widgets/graphicalfriedcake/GraphicView;I)I

    .line 203
    return-void
.end method

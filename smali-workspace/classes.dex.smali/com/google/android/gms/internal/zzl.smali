.class public Lcom/google/android/gms/internal/zzl;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/internal/zzl$zza;
    }
.end annotation


# instance fields
.field private zzY:Ljava/util/concurrent/atomic/AtomicInteger;

.field private final zzZ:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<Ljava/lang/String;Ljava/util/Queue<Lcom/google/android/gms/internal/zzk<*>;>;>;"
        }
    .end annotation
.end field

.field private final zzaa:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<Lcom/google/android/gms/internal/zzk<*>;>;"
        }
    .end annotation
.end field

.field private final zzab:Ljava/util/concurrent/PriorityBlockingQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/PriorityBlockingQueue<Lcom/google/android/gms/internal/zzk<*>;>;"
        }
    .end annotation
.end field

.field private final zzac:Ljava/util/concurrent/PriorityBlockingQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/PriorityBlockingQueue<Lcom/google/android/gms/internal/zzk<*>;>;"
        }
    .end annotation
.end field

.field private zzad:[Lcom/google/android/gms/internal/zzg;

.field private zzae:Lcom/google/android/gms/internal/zzc;

.field private zzaf:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Lcom/google/android/gms/internal/zzl$zza;>;"
        }
    .end annotation
.end field

.field private final zzj:Lcom/google/android/gms/internal/zzb;

.field private final zzk:Lcom/google/android/gms/internal/zzn;

.field private final zzz:Lcom/google/android/gms/internal/zzf;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/internal/zzb;Lcom/google/android/gms/internal/zzf;)V
    .registers 4

    const/4 v0, 0x4

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/gms/internal/zzl;-><init>(Lcom/google/android/gms/internal/zzb;Lcom/google/android/gms/internal/zzf;I)V

    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/internal/zzb;Lcom/google/android/gms/internal/zzf;I)V
    .registers 7

    new-instance v0, Lcom/google/android/gms/internal/zze;

    new-instance v1, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    invoke-direct {v0, v1}, Lcom/google/android/gms/internal/zze;-><init>(Landroid/os/Handler;)V

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/google/android/gms/internal/zzl;-><init>(Lcom/google/android/gms/internal/zzb;Lcom/google/android/gms/internal/zzf;ILcom/google/android/gms/internal/zzn;)V

    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/internal/zzb;Lcom/google/android/gms/internal/zzf;ILcom/google/android/gms/internal/zzn;)V
    .registers 6

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/internal/zzl;->zzY:Ljava/util/concurrent/atomic/AtomicInteger;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/internal/zzl;->zzZ:Ljava/util/Map;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/internal/zzl;->zzaa:Ljava/util/Set;

    new-instance v0, Ljava/util/concurrent/PriorityBlockingQueue;

    invoke-direct {v0}, Ljava/util/concurrent/PriorityBlockingQueue;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/internal/zzl;->zzab:Ljava/util/concurrent/PriorityBlockingQueue;

    new-instance v0, Ljava/util/concurrent/PriorityBlockingQueue;

    invoke-direct {v0}, Ljava/util/concurrent/PriorityBlockingQueue;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/internal/zzl;->zzac:Ljava/util/concurrent/PriorityBlockingQueue;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/internal/zzl;->zzaf:Ljava/util/List;

    iput-object p1, p0, Lcom/google/android/gms/internal/zzl;->zzj:Lcom/google/android/gms/internal/zzb;

    iput-object p2, p0, Lcom/google/android/gms/internal/zzl;->zzz:Lcom/google/android/gms/internal/zzf;

    new-array v0, p3, [Lcom/google/android/gms/internal/zzg;

    iput-object v0, p0, Lcom/google/android/gms/internal/zzl;->zzad:[Lcom/google/android/gms/internal/zzg;

    iput-object p4, p0, Lcom/google/android/gms/internal/zzl;->zzk:Lcom/google/android/gms/internal/zzn;

    return-void
.end method


# virtual methods
.method public getSequenceNumber()I
    .registers 2

    iget-object v0, p0, Lcom/google/android/gms/internal/zzl;->zzY:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    move-result v0

    return v0
.end method

.method public start()V
    .registers 8

    invoke-virtual {p0}, Lcom/google/android/gms/internal/zzl;->stop()V

    new-instance v0, Lcom/google/android/gms/internal/zzc;

    iget-object v1, p0, Lcom/google/android/gms/internal/zzl;->zzab:Ljava/util/concurrent/PriorityBlockingQueue;

    iget-object v2, p0, Lcom/google/android/gms/internal/zzl;->zzac:Ljava/util/concurrent/PriorityBlockingQueue;

    iget-object v3, p0, Lcom/google/android/gms/internal/zzl;->zzj:Lcom/google/android/gms/internal/zzb;

    iget-object v4, p0, Lcom/google/android/gms/internal/zzl;->zzk:Lcom/google/android/gms/internal/zzn;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/gms/internal/zzc;-><init>(Ljava/util/concurrent/BlockingQueue;Ljava/util/concurrent/BlockingQueue;Lcom/google/android/gms/internal/zzb;Lcom/google/android/gms/internal/zzn;)V

    iput-object v0, p0, Lcom/google/android/gms/internal/zzl;->zzae:Lcom/google/android/gms/internal/zzc;

    iget-object v0, p0, Lcom/google/android/gms/internal/zzl;->zzae:Lcom/google/android/gms/internal/zzc;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/zzc;->start()V

    const/4 v5, 0x0

    :goto_18
    iget-object v0, p0, Lcom/google/android/gms/internal/zzl;->zzad:[Lcom/google/android/gms/internal/zzg;

    array-length v0, v0

    if-ge v5, v0, :cond_34

    new-instance v6, Lcom/google/android/gms/internal/zzg;

    iget-object v0, p0, Lcom/google/android/gms/internal/zzl;->zzac:Ljava/util/concurrent/PriorityBlockingQueue;

    iget-object v1, p0, Lcom/google/android/gms/internal/zzl;->zzz:Lcom/google/android/gms/internal/zzf;

    iget-object v2, p0, Lcom/google/android/gms/internal/zzl;->zzj:Lcom/google/android/gms/internal/zzb;

    iget-object v3, p0, Lcom/google/android/gms/internal/zzl;->zzk:Lcom/google/android/gms/internal/zzn;

    invoke-direct {v6, v0, v1, v2, v3}, Lcom/google/android/gms/internal/zzg;-><init>(Ljava/util/concurrent/BlockingQueue;Lcom/google/android/gms/internal/zzf;Lcom/google/android/gms/internal/zzb;Lcom/google/android/gms/internal/zzn;)V

    iget-object v0, p0, Lcom/google/android/gms/internal/zzl;->zzad:[Lcom/google/android/gms/internal/zzg;

    aput-object v6, v0, v5

    invoke-virtual {v6}, Lcom/google/android/gms/internal/zzg;->start()V

    add-int/lit8 v5, v5, 0x1

    goto :goto_18

    :cond_34
    return-void
.end method

.method public stop()V
    .registers 3

    iget-object v0, p0, Lcom/google/android/gms/internal/zzl;->zzae:Lcom/google/android/gms/internal/zzc;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/google/android/gms/internal/zzl;->zzae:Lcom/google/android/gms/internal/zzc;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/zzc;->quit()V

    :cond_9
    const/4 v1, 0x0

    :goto_a
    iget-object v0, p0, Lcom/google/android/gms/internal/zzl;->zzad:[Lcom/google/android/gms/internal/zzg;

    array-length v0, v0

    if-ge v1, v0, :cond_1f

    iget-object v0, p0, Lcom/google/android/gms/internal/zzl;->zzad:[Lcom/google/android/gms/internal/zzg;

    aget-object v0, v0, v1

    if-eqz v0, :cond_1c

    iget-object v0, p0, Lcom/google/android/gms/internal/zzl;->zzad:[Lcom/google/android/gms/internal/zzg;

    aget-object v0, v0, v1

    invoke-virtual {v0}, Lcom/google/android/gms/internal/zzg;->quit()V

    :cond_1c
    add-int/lit8 v1, v1, 0x1

    goto :goto_a

    :cond_1f
    return-void
.end method

.method public zze(Lcom/google/android/gms/internal/zzk;)Lcom/google/android/gms/internal/zzk;
    .registers 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:Ljava/lang/Object;>(Lcom/google/android/gms/internal/zzk<TT;>;)Lcom/google/android/gms/internal/zzk<TT;>;"
        }
    .end annotation

    invoke-virtual {p1, p0}, Lcom/google/android/gms/internal/zzk;->zza(Lcom/google/android/gms/internal/zzl;)Lcom/google/android/gms/internal/zzk;

    iget-object v3, p0, Lcom/google/android/gms/internal/zzl;->zzaa:Ljava/util/Set;

    monitor-enter v3

    :try_start_6
    iget-object v0, p0, Lcom/google/android/gms/internal/zzl;->zzaa:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_b
    .catchall {:try_start_6 .. :try_end_b} :catchall_d

    monitor-exit v3

    goto :goto_10

    :catchall_d
    move-exception v4

    monitor-exit v3

    throw v4

    :goto_10
    invoke-virtual {p0}, Lcom/google/android/gms/internal/zzl;->getSequenceNumber()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/android/gms/internal/zzk;->zza(I)Lcom/google/android/gms/internal/zzk;

    const-string v0, "add-to-queue"

    invoke-virtual {p1, v0}, Lcom/google/android/gms/internal/zzk;->zzc(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/gms/internal/zzk;->zzr()Z

    move-result v0

    if-nez v0, :cond_28

    iget-object v0, p0, Lcom/google/android/gms/internal/zzl;->zzac:Ljava/util/concurrent/PriorityBlockingQueue;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/PriorityBlockingQueue;->add(Ljava/lang/Object;)Z

    return-object p1

    :cond_28
    iget-object v3, p0, Lcom/google/android/gms/internal/zzl;->zzZ:Ljava/util/Map;

    monitor-enter v3

    :try_start_2b
    invoke-virtual {p1}, Lcom/google/android/gms/internal/zzk;->zzh()Ljava/lang/String;

    move-result-object v4

    iget-object v0, p0, Lcom/google/android/gms/internal/zzl;->zzZ:Ljava/util/Map;

    invoke-interface {v0, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5f

    iget-object v0, p0, Lcom/google/android/gms/internal/zzl;->zzZ:Ljava/util/Map;

    invoke-interface {v0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Ljava/util/Queue;

    if-nez v5, :cond_47

    new-instance v5, Ljava/util/LinkedList;

    invoke-direct {v5}, Ljava/util/LinkedList;-><init>()V

    :cond_47
    invoke-interface {v5, p1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/gms/internal/zzl;->zzZ:Ljava/util/Map;

    invoke-interface {v0, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-boolean v0, Lcom/google/android/gms/internal/zzs;->DEBUG:Z

    if-eqz v0, :cond_5e

    const-string v0, "Request for cacheKey=%s is in flight, putting on hold."

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object v4, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/gms/internal/zzs;->zza(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_5e
    goto :goto_6a

    :cond_5f
    iget-object v0, p0, Lcom/google/android/gms/internal/zzl;->zzZ:Ljava/util/Map;

    const/4 v1, 0x0

    invoke-interface {v0, v4, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/gms/internal/zzl;->zzab:Ljava/util/concurrent/PriorityBlockingQueue;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/PriorityBlockingQueue;->add(Ljava/lang/Object;)Z
    :try_end_6a
    .catchall {:try_start_2b .. :try_end_6a} :catchall_6c

    :goto_6a
    monitor-exit v3

    return-object p1

    :catchall_6c
    move-exception v6

    monitor-exit v3

    throw v6
.end method

.method zzf(Lcom/google/android/gms/internal/zzk;)V
    .registers 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:Ljava/lang/Object;>(Lcom/google/android/gms/internal/zzk<TT;>;)V"
        }
    .end annotation

    iget-object v4, p0, Lcom/google/android/gms/internal/zzl;->zzaa:Ljava/util/Set;

    monitor-enter v4

    :try_start_3
    iget-object v0, p0, Lcom/google/android/gms/internal/zzl;->zzaa:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z
    :try_end_8
    .catchall {:try_start_3 .. :try_end_8} :catchall_a

    monitor-exit v4

    goto :goto_d

    :catchall_a
    move-exception v5

    monitor-exit v4

    throw v5

    :goto_d
    iget-object v4, p0, Lcom/google/android/gms/internal/zzl;->zzaf:Ljava/util/List;

    monitor-enter v4

    :try_start_10
    iget-object v0, p0, Lcom/google/android/gms/internal/zzl;->zzaf:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_16
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_27

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/google/android/gms/internal/zzl$zza;

    invoke-interface {v6, p1}, Lcom/google/android/gms/internal/zzl$zza;->zzg(Lcom/google/android/gms/internal/zzk;)V
    :try_end_26
    .catchall {:try_start_10 .. :try_end_26} :catchall_29

    goto :goto_16

    :cond_27
    monitor-exit v4

    goto :goto_2c

    :catchall_29
    move-exception v7

    monitor-exit v4

    throw v7

    :goto_2c
    invoke-virtual {p1}, Lcom/google/android/gms/internal/zzk;->zzr()Z

    move-result v0

    if-eqz v0, :cond_68

    iget-object v4, p0, Lcom/google/android/gms/internal/zzl;->zzZ:Ljava/util/Map;

    monitor-enter v4

    :try_start_35
    invoke-virtual {p1}, Lcom/google/android/gms/internal/zzk;->zzh()Ljava/lang/String;

    move-result-object v5

    iget-object v0, p0, Lcom/google/android/gms/internal/zzl;->zzZ:Ljava/util/Map;

    invoke-interface {v0, v5}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Ljava/util/Queue;

    if-eqz v6, :cond_63

    sget-boolean v0, Lcom/google/android/gms/internal/zzs;->DEBUG:Z

    if-eqz v0, :cond_5e

    const-string v0, "Releasing %d waiting requests for cacheKey=%s."

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    invoke-interface {v6}, Ljava/util/Queue;->size()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const/4 v2, 0x1

    aput-object v5, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/gms/internal/zzs;->zza(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_5e
    iget-object v0, p0, Lcom/google/android/gms/internal/zzl;->zzab:Ljava/util/concurrent/PriorityBlockingQueue;

    invoke-virtual {v0, v6}, Ljava/util/concurrent/PriorityBlockingQueue;->addAll(Ljava/util/Collection;)Z
    :try_end_63
    .catchall {:try_start_35 .. :try_end_63} :catchall_65

    :cond_63
    monitor-exit v4

    goto :goto_68

    :catchall_65
    move-exception v8

    monitor-exit v4

    throw v8

    :cond_68
    :goto_68
    return-void
.end method

.class Lcom/google/android/gms/internal/zzbh$zza;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/internal/zzbh;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "zza"
.end annotation


# instance fields
.field zztu:Ljava/io/ByteArrayOutputStream;

.field zztv:Landroid/util/Base64OutputStream;


# direct methods
.method public constructor <init>()V
    .registers 4

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/io/ByteArrayOutputStream;

    const/16 v1, 0x1000

    invoke-direct {v0, v1}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/gms/internal/zzbh$zza;->zztu:Ljava/io/ByteArrayOutputStream;

    new-instance v0, Landroid/util/Base64OutputStream;

    iget-object v1, p0, Lcom/google/android/gms/internal/zzbh$zza;->zztu:Ljava/io/ByteArrayOutputStream;

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Landroid/util/Base64OutputStream;-><init>(Ljava/io/OutputStream;I)V

    iput-object v0, p0, Lcom/google/android/gms/internal/zzbh$zza;->zztv:Landroid/util/Base64OutputStream;

    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .registers 5

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/internal/zzbh$zza;->zztv:Landroid/util/Base64OutputStream;

    invoke-virtual {v0}, Landroid/util/Base64OutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_5} :catch_6

    goto :goto_c

    :catch_6
    move-exception v1

    const-string v0, "HashManager: Unable to convert to Base64."

    invoke-static {v0, v1}, Lcom/google/android/gms/internal/zzin;->zzb(Ljava/lang/String;Ljava/lang/Throwable;)V

    :goto_c
    :try_start_c
    iget-object v0, p0, Lcom/google/android/gms/internal/zzbh$zza;->zztu:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->close()V

    iget-object v0, p0, Lcom/google/android/gms/internal/zzbh$zza;->zztu:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toString()Ljava/lang/String;
    :try_end_16
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_16} :catch_1e
    .catchall {:try_start_c .. :try_end_16} :catchall_2d

    move-result-object v1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/internal/zzbh$zza;->zztu:Ljava/io/ByteArrayOutputStream;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/internal/zzbh$zza;->zztv:Landroid/util/Base64OutputStream;

    return-object v1

    :catch_1e
    move-exception v1

    const-string v0, "HashManager: Unable to convert to Base64."

    :try_start_21
    invoke-static {v0, v1}, Lcom/google/android/gms/internal/zzin;->zzb(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_24
    .catchall {:try_start_21 .. :try_end_24} :catchall_2d

    const-string v2, ""

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/internal/zzbh$zza;->zztu:Ljava/io/ByteArrayOutputStream;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/internal/zzbh$zza;->zztv:Landroid/util/Base64OutputStream;

    return-object v2

    :catchall_2d
    move-exception v3

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/internal/zzbh$zza;->zztu:Ljava/io/ByteArrayOutputStream;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/internal/zzbh$zza;->zztv:Landroid/util/Base64OutputStream;

    throw v3
.end method

.method public write([B)V
    .registers 3
    .param p1, "data"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/gms/internal/zzbh$zza;->zztv:Landroid/util/Base64OutputStream;

    invoke-virtual {v0, p1}, Landroid/util/Base64OutputStream;->write([B)V

    return-void
.end method

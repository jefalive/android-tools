.class public Lcom/itau/empresas/ui/view/SeletorDataPagamentoView;
.super Landroid/widget/RelativeLayout;
.source "SeletorDataPagamentoView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/itau/empresas/ui/view/SeletorDataPagamentoView$TextChangedListener;
    }
.end annotation


# instance fields
.field botaoMais:Landroid/widget/Button;

.field botaoMenos:Landroid/widget/Button;

.field private dia:Ljava/lang/Integer;

.field diaSelecionado:Landroid/widget/EditText;

.field private valorMaximo:I

.field private valorMinimo:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 2
    .param p1, "context"    # Landroid/content/Context;

    .line 38
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 39
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .line 42
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 43
    invoke-direct {p0, p2}, Lcom/itau/empresas/ui/view/SeletorDataPagamentoView;->iniciaAtributos(Landroid/util/AttributeSet;)V

    .line 44
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .line 47
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 48
    invoke-direct {p0, p2}, Lcom/itau/empresas/ui/view/SeletorDataPagamentoView;->iniciaAtributos(Landroid/util/AttributeSet;)V

    .line 49
    return-void
.end method

.method static synthetic access$100(Lcom/itau/empresas/ui/view/SeletorDataPagamentoView;)V
    .registers 1
    .param p0, "x0"    # Lcom/itau/empresas/ui/view/SeletorDataPagamentoView;

    .line 22
    invoke-direct {p0}, Lcom/itau/empresas/ui/view/SeletorDataPagamentoView;->mostraHintError()V

    return-void
.end method

.method static synthetic access$200(Lcom/itau/empresas/ui/view/SeletorDataPagamentoView;)V
    .registers 1
    .param p0, "x0"    # Lcom/itau/empresas/ui/view/SeletorDataPagamentoView;

    .line 22
    invoke-direct {p0}, Lcom/itau/empresas/ui/view/SeletorDataPagamentoView;->configuraAcessibilidade()V

    return-void
.end method

.method private alterarValorTextView()V
    .registers 3

    .line 71
    iget-object v0, p0, Lcom/itau/empresas/ui/view/SeletorDataPagamentoView;->diaSelecionado:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/itau/empresas/ui/view/SeletorDataPagamentoView;->dia:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 72
    return-void
.end method

.method private configuraAcessibilidade()V
    .registers 6

    .line 165
    iget-object v0, p0, Lcom/itau/empresas/ui/view/SeletorDataPagamentoView;->botaoMenos:Landroid/widget/Button;

    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/SeletorDataPagamentoView;->getContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/itau/empresas/ui/view/SeletorDataPagamentoView;->dia:Ljava/lang/Integer;

    const/4 v4, 0x0

    aput-object v3, v2, v4

    const v3, 0x7f070416

    invoke-virtual {v1, v3, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 166
    iget-object v0, p0, Lcom/itau/empresas/ui/view/SeletorDataPagamentoView;->botaoMais:Landroid/widget/Button;

    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/SeletorDataPagamentoView;->getContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/itau/empresas/ui/view/SeletorDataPagamentoView;->dia:Ljava/lang/Integer;

    const/4 v4, 0x0

    aput-object v3, v2, v4

    const v3, 0x7f070415

    invoke-virtual {v1, v3, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 167
    return-void
.end method

.method private getDiaView()I
    .registers 2

    .line 67
    iget-object v0, p0, Lcom/itau/empresas/ui/view/SeletorDataPagamentoView;->diaSelecionado:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method private iniciaAtributos(Landroid/util/AttributeSet;)V
    .registers 7
    .param p1, "attrs"    # Landroid/util/AttributeSet;

    .line 156
    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/SeletorDataPagamentoView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    sget-object v1, Lcom/itau/empresas/R$styleable;->SeletorDataPagamento:[I

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, p1, v1, v2, v3}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v4

    .line 159
    .local v4, "typedArray":Landroid/content/res/TypedArray;
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {v4, v0, v1}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v0

    iput v0, p0, Lcom/itau/empresas/ui/view/SeletorDataPagamentoView;->valorMinimo:I

    .line 160
    const/4 v0, 0x1

    const/16 v1, 0x1e

    invoke-virtual {v4, v0, v1}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v0

    iput v0, p0, Lcom/itau/empresas/ui/view/SeletorDataPagamentoView;->valorMaximo:I

    .line 161
    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-virtual {v4, v0, v1}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/ui/view/SeletorDataPagamentoView;->dia:Ljava/lang/Integer;

    .line 162
    return-void
.end method

.method private mostraHintError()V
    .registers 6

    .line 59
    new-instance v4, Lbr/com/itau/widgets/hintview/Hint$Builder;

    iget-object v0, p0, Lcom/itau/empresas/ui/view/SeletorDataPagamentoView;->diaSelecionado:Landroid/widget/EditText;

    const v1, 0x7f09011d

    invoke-direct {v4, v0, v1}, Lbr/com/itau/widgets/hintview/Hint$Builder;-><init>(Landroid/view/View;I)V

    .line 60
    .local v4, "builder":Lbr/com/itau/widgets/hintview/Hint$Builder;
    const-string v0, "Valores fora do intervalo %1$s e %2$s"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/SeletorDataPagamentoView;->getValorMinimo()Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    .line 61
    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/SeletorDataPagamentoView;->getValorMaximo()Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x1

    aput-object v2, v1, v3

    .line 60
    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Lbr/com/itau/widgets/hintview/Hint$Builder;->setText(Ljava/lang/CharSequence;)Lbr/com/itau/widgets/hintview/Hint$Builder;

    .line 62
    const/4 v0, 0x1

    invoke-virtual {v4, v0}, Lbr/com/itau/widgets/hintview/Hint$Builder;->setCancelable(Z)Lbr/com/itau/widgets/hintview/Hint$Builder;

    .line 63
    invoke-virtual {v4}, Lbr/com/itau/widgets/hintview/Hint$Builder;->show()Lbr/com/itau/widgets/hintview/Hint;

    .line 64
    return-void
.end method


# virtual methods
.method aoCarregarTela()V
    .registers 4

    .line 53
    invoke-direct {p0}, Lcom/itau/empresas/ui/view/SeletorDataPagamentoView;->alterarValorTextView()V

    .line 54
    iget-object v0, p0, Lcom/itau/empresas/ui/view/SeletorDataPagamentoView;->diaSelecionado:Landroid/widget/EditText;

    new-instance v1, Lcom/itau/empresas/ui/view/SeletorDataPagamentoView$TextChangedListener;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/itau/empresas/ui/view/SeletorDataPagamentoView$TextChangedListener;-><init>(Lcom/itau/empresas/ui/view/SeletorDataPagamentoView;Lcom/itau/empresas/ui/view/SeletorDataPagamentoView$1;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 55
    invoke-direct {p0}, Lcom/itau/empresas/ui/view/SeletorDataPagamentoView;->configuraAcessibilidade()V

    .line 56
    return-void
.end method

.method decrementarDia()V
    .registers 5

    .line 86
    invoke-direct {p0}, Lcom/itau/empresas/ui/view/SeletorDataPagamentoView;->getDiaView()I

    move-result v0

    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/SeletorDataPagamentoView;->getValorMinimo()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-le v0, v1, :cond_21

    .line 87
    iget-object v2, p0, Lcom/itau/empresas/ui/view/SeletorDataPagamentoView;->dia:Ljava/lang/Integer;

    iget-object v0, p0, Lcom/itau/empresas/ui/view/SeletorDataPagamentoView;->dia:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, p0, Lcom/itau/empresas/ui/view/SeletorDataPagamentoView;->dia:Ljava/lang/Integer;

    .line 88
    invoke-direct {p0}, Lcom/itau/empresas/ui/view/SeletorDataPagamentoView;->alterarValorTextView()V

    .line 91
    :cond_21
    invoke-direct {p0}, Lcom/itau/empresas/ui/view/SeletorDataPagamentoView;->configuraAcessibilidade()V

    .line 92
    return-void
.end method

.method public getDia()Ljava/lang/Integer;
    .registers 2

    .line 111
    iget-object v0, p0, Lcom/itau/empresas/ui/view/SeletorDataPagamentoView;->dia:Ljava/lang/Integer;

    return-object v0
.end method

.method public getValorMaximo()Ljava/lang/Integer;
    .registers 2

    .line 95
    iget v0, p0, Lcom/itau/empresas/ui/view/SeletorDataPagamentoView;->valorMaximo:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public getValorMinimo()Ljava/lang/Integer;
    .registers 2

    .line 103
    iget v0, p0, Lcom/itau/empresas/ui/view/SeletorDataPagamentoView;->valorMinimo:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method incrementarDia()V
    .registers 5

    .line 76
    invoke-direct {p0}, Lcom/itau/empresas/ui/view/SeletorDataPagamentoView;->getDiaView()I

    move-result v0

    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/SeletorDataPagamentoView;->getValorMaximo()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-ge v0, v1, :cond_21

    .line 77
    iget-object v2, p0, Lcom/itau/empresas/ui/view/SeletorDataPagamentoView;->dia:Ljava/lang/Integer;

    iget-object v0, p0, Lcom/itau/empresas/ui/view/SeletorDataPagamentoView;->dia:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, p0, Lcom/itau/empresas/ui/view/SeletorDataPagamentoView;->dia:Ljava/lang/Integer;

    .line 78
    invoke-direct {p0}, Lcom/itau/empresas/ui/view/SeletorDataPagamentoView;->alterarValorTextView()V

    .line 81
    :cond_21
    invoke-direct {p0}, Lcom/itau/empresas/ui/view/SeletorDataPagamentoView;->configuraAcessibilidade()V

    .line 82
    return-void
.end method

.method public setValorMaximo(I)V
    .registers 2
    .param p1, "valorMaximo"    # I

    .line 99
    iput p1, p0, Lcom/itau/empresas/ui/view/SeletorDataPagamentoView;->valorMaximo:I

    .line 100
    return-void
.end method

.method public setValorMinimo(I)V
    .registers 2
    .param p1, "valorMinimo"    # I

    .line 107
    iput p1, p0, Lcom/itau/empresas/ui/view/SeletorDataPagamentoView;->valorMinimo:I

    .line 108
    return-void
.end method

.class public final Lbr/com/itau/sdk/android/core/model/CampoErroDTO;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private campo:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/Expose;
    .end annotation
.end field

.field private mensagem:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/Expose;
    .end annotation
.end field

.field private valor:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/Expose;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getCampo()Ljava/lang/String;
    .registers 2

    .line 17
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/model/CampoErroDTO;->campo:Ljava/lang/String;

    return-object v0
.end method

.method public getMensagem()Ljava/lang/String;
    .registers 2

    .line 25
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/model/CampoErroDTO;->mensagem:Ljava/lang/String;

    return-object v0
.end method

.method public getValor()Ljava/lang/String;
    .registers 2

    .line 33
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/model/CampoErroDTO;->valor:Ljava/lang/String;

    return-object v0
.end method

.method public setCampo(Ljava/lang/String;)V
    .registers 2

    .line 21
    iput-object p1, p0, Lbr/com/itau/sdk/android/core/model/CampoErroDTO;->campo:Ljava/lang/String;

    .line 22
    return-void
.end method

.method public setMensagem(Ljava/lang/String;)V
    .registers 2

    .line 29
    iput-object p1, p0, Lbr/com/itau/sdk/android/core/model/CampoErroDTO;->mensagem:Ljava/lang/String;

    .line 30
    return-void
.end method

.method public setValor(Ljava/lang/String;)V
    .registers 2

    .line 37
    iput-object p1, p0, Lbr/com/itau/sdk/android/core/model/CampoErroDTO;->valor:Ljava/lang/String;

    .line 38
    return-void
.end method

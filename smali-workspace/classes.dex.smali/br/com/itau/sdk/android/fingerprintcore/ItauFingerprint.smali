.class public final Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprint;
.super Ljava/lang/Object;
.source "ItauFingerprint.java"


# direct methods
.method public static getInstance(Landroid/content/Context;)Lbr/com/itau/sdk/android/fingerprintcore/FingerprintManager;
    .registers 4
    .param p0, "context"    # Landroid/content/Context;

    .line 14
    if-nez p0, :cond_a

    .line 15
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Context cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 18
    :cond_a
    invoke-static {p0}, Lbr/com/itau/sdk/android/fingerprintcore/android/AndroidFingerprintManager;->getInstance(Landroid/content/Context;)Lbr/com/itau/sdk/android/fingerprintcore/android/AndroidFingerprintManager;

    move-result-object v2

    .line 19
    .local v2, "manager":Lbr/com/itau/sdk/android/fingerprintcore/FingerprintManager;
    invoke-interface {v2}, Lbr/com/itau/sdk/android/fingerprintcore/FingerprintManager;->isAvailable()Z

    move-result v0

    if-eqz v0, :cond_15

    .line 20
    return-object v2

    .line 23
    :cond_15
    const/4 v0, 0x0

    return-object v0
.end method

.class public Lcom/itau/empresas/NotificacaoUtils;
.super Ljava/lang/Object;
.source "NotificacaoUtils.java"


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static configurarNotificacaoFimAno(Landroid/content/Context;)V
    .registers 15
    .param p0, "ctx"    # Landroid/content/Context;

    .line 103
    const/16 v5, 0x7e1

    .line 104
    .local v5, "ano":I
    const/16 v6, 0xb

    .line 105
    .local v6, "mes":I
    const/16 v7, 0xa

    .line 106
    .local v7, "hora":I
    const/4 v8, 0x0

    .line 108
    .local v8, "minuto":I
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v9

    .line 111
    .line 112
    .local v9, "dataAtual":Ljava/util/Calendar;
    const/16 v0, 0x15

    const/16 v1, 0xb

    const/16 v2, 0x7e1

    const/16 v3, 0xa

    const/4 v4, 0x0

    invoke-static {v0, v1, v2, v3, v4}, Lcom/itau/empresas/NotificacaoUtils;->obterDataHoraPersonalizada(IIIII)Ljava/util/Calendar;

    move-result-object v0

    .line 111
    invoke-static {v9, v0}, Lcom/itau/empresas/NotificacaoUtils;->obterPeriodo(Ljava/util/Calendar;Ljava/util/Calendar;)J

    move-result-wide v10

    .line 113
    .local v10, "periodoNatal":J
    const-wide/16 v0, 0x0

    cmp-long v0, v10, v0

    if-lez v0, :cond_26

    .line 114
    const/4 v0, 0x4

    invoke-static {p0, v0, v10, v11}, Lcom/itau/empresas/AlarmeUtils;->agendarAlarmeUnico(Landroid/content/Context;IJ)V

    .line 118
    .line 119
    :cond_26
    const/16 v0, 0xe

    const/16 v1, 0xb

    const/16 v2, 0x7e1

    const/16 v3, 0xa

    const/4 v4, 0x0

    invoke-static {v0, v1, v2, v3, v4}, Lcom/itau/empresas/NotificacaoUtils;->obterDataHoraPersonalizada(IIIII)Ljava/util/Calendar;

    move-result-object v0

    .line 118
    invoke-static {v9, v0}, Lcom/itau/empresas/NotificacaoUtils;->obterPeriodo(Ljava/util/Calendar;Ljava/util/Calendar;)J

    move-result-wide v12

    .line 120
    .local v12, "periodoAnoNovo":J
    const-wide/16 v0, 0x0

    cmp-long v0, v12, v0

    if-lez v0, :cond_41

    .line 121
    const/4 v0, 0x5

    invoke-static {p0, v0, v12, v13}, Lcom/itau/empresas/AlarmeUtils;->agendarAlarmeUnico(Landroid/content/Context;IJ)V

    .line 123
    :cond_41
    return-void
.end method

.method private static enviarNotificacao(Landroid/content/Context;Landroid/app/Notification;I)V
    .registers 4
    .param p0, "ctx"    # Landroid/content/Context;
    .param p1, "notification"    # Landroid/app/Notification;
    .param p2, "id"    # I

    .line 35
    invoke-static {p0}, Landroid/support/v4/app/NotificationManagerCompat;->from(Landroid/content/Context;)Landroid/support/v4/app/NotificationManagerCompat;

    move-result-object v0

    .line 36
    .local v0, "notificationManager":Landroid/support/v4/app/NotificationManagerCompat;
    invoke-virtual {v0, p2, p1}, Landroid/support/v4/app/NotificationManagerCompat;->notify(ILandroid/app/Notification;)V

    .line 37
    return-void
.end method

.method public static notificacaoSimples(Landroid/content/Context;ILjava/lang/String;Lcom/itau/empresas/CustomApplication;)V
    .registers 12
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "id"    # I
    .param p2, "mensagem"    # Ljava/lang/String;
    .param p3, "application"    # Lcom/itau/empresas/CustomApplication;

    .line 42
    invoke-static {p0}, Lcom/itau/empresas/feature/login/LoginActivity;->intent(Landroid/content/Context;)Lcom/itau/empresas/feature/login/LoginActivity_$IntentBuilder_;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/feature/login/LoginActivity_$IntentBuilder_;->get()Landroid/content/Intent;

    move-result-object v4

    .line 43
    .local v4, "viewIntent":Landroid/content/Intent;
    const v0, 0x8000

    invoke-virtual {v4, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 45
    invoke-static {p1, v4}, Lcom/itau/empresas/NotificacaoUtils;->salvarExtraNotificacao(ILandroid/content/Intent;)V

    .line 47
    .line 49
    const/high16 v0, 0x8000000

    invoke-static {p0, p1, v4, v0}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v5

    .line 51
    .local v5, "viewPendingIntent":Landroid/app/PendingIntent;
    new-instance v6, Landroid/support/v4/app/NotificationCompat$BigTextStyle;

    invoke-direct {v6}, Landroid/support/v4/app/NotificationCompat$BigTextStyle;-><init>()V

    .line 52
    .local v6, "bigText":Landroid/support/v4/app/NotificationCompat$BigTextStyle;
    invoke-virtual {v6, p2}, Landroid/support/v4/app/NotificationCompat$BigTextStyle;->bigText(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$BigTextStyle;

    .line 54
    .line 55
    const v0, 0x7f07043b

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0, p2}, Lcom/itau/empresas/NotificacaoUtils;->novaNotificacao(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v0

    .line 57
    invoke-virtual {v0, v5}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v0

    .line 58
    invoke-virtual {v0, v6}, Landroid/support/v4/app/NotificationCompat$Builder;->setStyle(Landroid/support/v4/app/NotificationCompat$Style;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v7

    .line 60
    .local v7, "notificationBuilder":Landroid/support/v4/app/NotificationCompat$Builder;
    invoke-virtual {v7}, Landroid/support/v4/app/NotificationCompat$Builder;->build()Landroid/app/Notification;

    move-result-object v0

    invoke-static {p0, v0, p1}, Lcom/itau/empresas/NotificacaoUtils;->enviarNotificacao(Landroid/content/Context;Landroid/app/Notification;I)V

    .line 61
    invoke-virtual {p3}, Lcom/itau/empresas/CustomApplication;->getEventoTracker()Lcom/itau/empresas/ui/util/analytics/TrackerEvento;

    move-result-object v0

    new-instance v1, Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;

    const-string v2, "local-notification"

    const-string v3, "ANALYTICS_HIT"

    .line 62
    invoke-virtual {v4, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 61
    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/analytics/TrackerEvento;->enviarHitEvento(Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;)V

    .line 64
    return-void
.end method

.method private static novaNotificacao(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/support/v4/app/NotificationCompat$Builder;
    .registers 5
    .param p0, "ctx"    # Landroid/content/Context;
    .param p1, "titulo"    # Ljava/lang/String;
    .param p2, "texto"    # Ljava/lang/String;

    .line 24
    new-instance v0, Landroid/support/v4/app/NotificationCompat$Builder;

    invoke-direct {v0, p0}, Landroid/support/v4/app/NotificationCompat$Builder;-><init>(Landroid/content/Context;)V

    .line 25
    const v1, 0x7f020158

    invoke-virtual {v0, v1}, Landroid/support/v4/app/NotificationCompat$Builder;->setSmallIcon(I)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v0

    .line 26
    invoke-virtual {v0, p1}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v0

    .line 27
    invoke-virtual {v0, p2}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v0

    .line 28
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/NotificationCompat$Builder;->setAutoCancel(Z)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v0

    .line 29
    const v1, 0x7f0c000c

    invoke-static {p0, v1}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/NotificationCompat$Builder;->setColor(I)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v0

    .line 30
    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/NotificationCompat$Builder;->setDefaults(I)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v0

    .line 24
    return-object v0
.end method

.method private static obterDataHoraPersonalizada(IIIII)Ljava/util/Calendar;
    .registers 7
    .param p0, "dia"    # I
    .param p1, "mes"    # I
    .param p2, "ano"    # I
    .param p3, "hora"    # I
    .param p4, "min"    # I

    .line 141
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    .line 143
    .local v1, "dataPersonalizada":Ljava/util/Calendar;
    const/4 v0, 0x5

    invoke-virtual {v1, v0, p0}, Ljava/util/Calendar;->set(II)V

    .line 144
    const/4 v0, 0x2

    invoke-virtual {v1, v0, p1}, Ljava/util/Calendar;->set(II)V

    .line 145
    const/4 v0, 0x1

    invoke-virtual {v1, v0, p2}, Ljava/util/Calendar;->set(II)V

    .line 146
    const/16 v0, 0xb

    invoke-virtual {v1, v0, p3}, Ljava/util/Calendar;->set(II)V

    .line 147
    const/16 v0, 0xc

    invoke-virtual {v1, v0, p4}, Ljava/util/Calendar;->set(II)V

    .line 149
    return-object v1
.end method

.method private static obterPeriodo(Ljava/util/Calendar;Ljava/util/Calendar;)J
    .registers 6
    .param p0, "dataAtual"    # Ljava/util/Calendar;
    .param p1, "dataPersonalizada"    # Ljava/util/Calendar;

    .line 126
    invoke-virtual {p1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    invoke-virtual {p0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    sub-long/2addr v0, v2

    return-wide v0
.end method

.method private static salvarExtraNotificacao(ILandroid/content/Intent;)V
    .registers 4
    .param p0, "id"    # I
    .param p1, "intent"    # Landroid/content/Intent;

    .line 69
    packed-switch p0, :pswitch_data_58

    goto/16 :goto_56

    .line 71
    :pswitch_5
    const-string v0, "WIDGET_ANDROID"

    const-string v1, "WIDGET_ANDROID_ITOKEN"

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 72
    const-string v0, "LOCAL_NOTIFICATION_CLICKED"

    const-string v1, "INSTALAR_TOKEN"

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 73
    const-string v0, "ANALYTICS_HIT"

    const-string v1, "oferta-token"

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 74
    goto :goto_56

    .line 77
    :pswitch_1b
    const-string v0, "WIDGET_ANDROID"

    const-string v1, "WIDGET_ANDROID_EXTRATO"

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 78
    const-string v0, "LOCAL_NOTIFICATION_CLICKED"

    const-string v1, "SALDO-EXTRATO"

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 79
    const-string v0, "ANALYTICS_HIT"

    const-string v1, "saldo-extrato"

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 80
    goto :goto_56

    .line 83
    :pswitch_31
    const-string v0, "ANALYTICS_HIT"

    const-string v1, "login"

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 84
    goto :goto_56

    .line 87
    :pswitch_39
    const-string v0, "WIDGET_ANDROID"

    const-string v1, "WIDGET_ANDROID_NATAL"

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 88
    const-string v0, "LOCAL_NOTIFICATION_CLICKED"

    const-string v1, "NOTIFICATION-NATAL"

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 89
    goto :goto_56

    .line 92
    :pswitch_48
    const-string v0, "WIDGET_ANDROID"

    const-string v1, "WIDGET_ANDROID_ANO_NOVO"

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 93
    const-string v0, "LOCAL_NOTIFICATION_CLICKED"

    const-string v1, "NOTIFICATION-ANO_NOVO"

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 94
    .line 99
    :goto_56
    return-void

    nop

    :pswitch_data_58
    .packed-switch 0x1
        :pswitch_5
        :pswitch_1b
        :pswitch_31
        :pswitch_39
        :pswitch_48
    .end packed-switch
.end method

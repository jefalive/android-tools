.class public Lcom/itau/empresas/api/model/RetornoAgenciasVO;
.super Ljava/lang/Object;
.source "RetornoAgenciasVO.java"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/itau/empresas/api/model/RetornoAgenciasVO$Dados;
    }
.end annotation


# instance fields
.field private codigo:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "Codigo"
    .end annotation
.end field

.field private data:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "Data"
    .end annotation
.end field

.field private ld:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "LD"
    .end annotation
.end field

.field private mensagem:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "Mensagem"
    .end annotation
.end field

.field private mensagemErro:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "MensagemErro"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getData()Ljava/util/List;
    .registers 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Ljava/util/List<Lcom/itau/empresas/api/model/AgenciaVO;>;>;"
        }
    .end annotation

    .line 23
    new-instance v2, Lcom/google/gson/Gson;

    invoke-direct {v2}, Lcom/google/gson/Gson;-><init>()V

    .line 26
    .local v2, "gson":Lcom/google/gson/Gson;
    :try_start_5
    iget-object v0, p0, Lcom/itau/empresas/api/model/RetornoAgenciasVO;->data:Ljava/lang/String;

    const-class v1, Lcom/itau/empresas/api/model/RetornoAgenciasVO$Dados;

    invoke-virtual {v2, v0, v1}, Lcom/google/gson/Gson;->fromJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/itau/empresas/api/model/RetornoAgenciasVO$Dados;

    .line 27
    .local v3, "retorno":Lcom/itau/empresas/api/model/RetornoAgenciasVO$Dados;
    invoke-virtual {v3}, Lcom/itau/empresas/api/model/RetornoAgenciasVO$Dados;->getD()Ljava/util/List;
    :try_end_13
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_13} :catch_15

    move-result-object v0

    return-object v0

    .line 29
    .end local v3    # "retorno":Lcom/itau/empresas/api/model/RetornoAgenciasVO$Dados;
    :catch_15
    move-exception v4

    .line 30
    .local v4, "e":Ljava/lang/Exception;
    invoke-virtual {v4}, Ljava/lang/Exception;->printStackTrace()V

    .line 32
    .end local v4    # "e":Ljava/lang/Exception;
    const/4 v0, 0x0

    return-object v0
.end method

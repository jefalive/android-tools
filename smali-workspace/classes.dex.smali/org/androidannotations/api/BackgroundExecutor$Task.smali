.class public abstract Lorg/androidannotations/api/BackgroundExecutor$Task;
.super Ljava/lang/Object;
.source "BackgroundExecutor.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/androidannotations/api/BackgroundExecutor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Task"
.end annotation


# instance fields
.field private executionAsked:Z

.field private future:Ljava/util/concurrent/Future;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/Future<*>;"
        }
    .end annotation
.end field

.field private id:Ljava/lang/String;

.field private managed:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private remainingDelay:J

.field private serial:Ljava/lang/String;

.field private targetTimeMillis:J


# direct methods
.method static synthetic access$000(Lorg/androidannotations/api/BackgroundExecutor$Task;)Ljava/lang/String;
    .registers 2
    .param p0, "x0"    # Lorg/androidannotations/api/BackgroundExecutor$Task;

    .line 358
    iget-object v0, p0, Lorg/androidannotations/api/BackgroundExecutor$Task;->id:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lorg/androidannotations/api/BackgroundExecutor$Task;)Ljava/lang/String;
    .registers 2
    .param p0, "x0"    # Lorg/androidannotations/api/BackgroundExecutor$Task;

    .line 358
    iget-object v0, p0, Lorg/androidannotations/api/BackgroundExecutor$Task;->serial:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lorg/androidannotations/api/BackgroundExecutor$Task;)Z
    .registers 2
    .param p0, "x0"    # Lorg/androidannotations/api/BackgroundExecutor$Task;

    .line 358
    iget-boolean v0, p0, Lorg/androidannotations/api/BackgroundExecutor$Task;->executionAsked:Z

    return v0
.end method

.method static synthetic access$202(Lorg/androidannotations/api/BackgroundExecutor$Task;Z)Z
    .registers 2
    .param p0, "x0"    # Lorg/androidannotations/api/BackgroundExecutor$Task;
    .param p1, "x1"    # Z

    .line 358
    iput-boolean p1, p0, Lorg/androidannotations/api/BackgroundExecutor$Task;->executionAsked:Z

    return p1
.end method

.method static synthetic access$300(Lorg/androidannotations/api/BackgroundExecutor$Task;)Ljava/util/concurrent/Future;
    .registers 2
    .param p0, "x0"    # Lorg/androidannotations/api/BackgroundExecutor$Task;

    .line 358
    iget-object v0, p0, Lorg/androidannotations/api/BackgroundExecutor$Task;->future:Ljava/util/concurrent/Future;

    return-object v0
.end method

.method static synthetic access$302(Lorg/androidannotations/api/BackgroundExecutor$Task;Ljava/util/concurrent/Future;)Ljava/util/concurrent/Future;
    .registers 2
    .param p0, "x0"    # Lorg/androidannotations/api/BackgroundExecutor$Task;
    .param p1, "x1"    # Ljava/util/concurrent/Future;

    .line 358
    iput-object p1, p0, Lorg/androidannotations/api/BackgroundExecutor$Task;->future:Ljava/util/concurrent/Future;

    return-object p1
.end method

.method static synthetic access$400(Lorg/androidannotations/api/BackgroundExecutor$Task;)J
    .registers 3
    .param p0, "x0"    # Lorg/androidannotations/api/BackgroundExecutor$Task;

    .line 358
    iget-wide v0, p0, Lorg/androidannotations/api/BackgroundExecutor$Task;->remainingDelay:J

    return-wide v0
.end method

.method static synthetic access$500(Lorg/androidannotations/api/BackgroundExecutor$Task;)Ljava/util/concurrent/atomic/AtomicBoolean;
    .registers 2
    .param p0, "x0"    # Lorg/androidannotations/api/BackgroundExecutor$Task;

    .line 358
    iget-object v0, p0, Lorg/androidannotations/api/BackgroundExecutor$Task;->managed:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-object v0
.end method

.method static synthetic access$600(Lorg/androidannotations/api/BackgroundExecutor$Task;)V
    .registers 1
    .param p0, "x0"    # Lorg/androidannotations/api/BackgroundExecutor$Task;

    .line 358
    invoke-direct {p0}, Lorg/androidannotations/api/BackgroundExecutor$Task;->postExecute()V

    return-void
.end method

.method private postExecute()V
    .registers 8

    .line 413
    iget-object v0, p0, Lorg/androidannotations/api/BackgroundExecutor$Task;->id:Ljava/lang/String;

    if-nez v0, :cond_9

    iget-object v0, p0, Lorg/androidannotations/api/BackgroundExecutor$Task;->serial:Ljava/lang/String;

    if-nez v0, :cond_9

    .line 415
    return-void

    .line 417
    :cond_9
    # getter for: Lorg/androidannotations/api/BackgroundExecutor;->CURRENT_SERIAL:Ljava/lang/ThreadLocal;
    invoke-static {}, Lorg/androidannotations/api/BackgroundExecutor;->access$700()Ljava/lang/ThreadLocal;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    .line 418
    const-class v4, Lorg/androidannotations/api/BackgroundExecutor;

    monitor-enter v4

    .line 420
    :try_start_14
    # getter for: Lorg/androidannotations/api/BackgroundExecutor;->TASKS:Ljava/util/List;
    invoke-static {}, Lorg/androidannotations/api/BackgroundExecutor;->access$800()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 422
    iget-object v0, p0, Lorg/androidannotations/api/BackgroundExecutor$Task;->serial:Ljava/lang/String;

    if-eqz v0, :cond_41

    .line 423
    iget-object v0, p0, Lorg/androidannotations/api/BackgroundExecutor$Task;->serial:Ljava/lang/String;

    # invokes: Lorg/androidannotations/api/BackgroundExecutor;->take(Ljava/lang/String;)Lorg/androidannotations/api/BackgroundExecutor$Task;
    invoke-static {v0}, Lorg/androidannotations/api/BackgroundExecutor;->access$900(Ljava/lang/String;)Lorg/androidannotations/api/BackgroundExecutor$Task;

    move-result-object v5

    .line 424
    .local v5, "next":Lorg/androidannotations/api/BackgroundExecutor$Task;
    if-eqz v5, :cond_41

    .line 425
    iget-wide v0, v5, Lorg/androidannotations/api/BackgroundExecutor$Task;->remainingDelay:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_3e

    .line 427
    iget-wide v0, v5, Lorg/androidannotations/api/BackgroundExecutor$Task;->targetTimeMillis:J

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x0

    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    iput-wide v0, v5, Lorg/androidannotations/api/BackgroundExecutor$Task;->remainingDelay:J

    .line 430
    :cond_3e
    invoke-static {v5}, Lorg/androidannotations/api/BackgroundExecutor;->execute(Lorg/androidannotations/api/BackgroundExecutor$Task;)V
    :try_end_41
    .catchall {:try_start_14 .. :try_end_41} :catchall_43

    .line 433
    .end local v5    # "next":Lorg/androidannotations/api/BackgroundExecutor$Task;
    :cond_41
    monitor-exit v4

    goto :goto_46

    :catchall_43
    move-exception v6

    monitor-exit v4

    throw v6

    .line 434
    :goto_46
    return-void
.end method


# virtual methods
.method public abstract execute()V
.end method

.method public run()V
    .registers 4

    .line 396
    iget-object v0, p0, Lorg/androidannotations/api/BackgroundExecutor$Task;->managed:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 398
    return-void

    .line 402
    :cond_a
    :try_start_a
    # getter for: Lorg/androidannotations/api/BackgroundExecutor;->CURRENT_SERIAL:Ljava/lang/ThreadLocal;
    invoke-static {}, Lorg/androidannotations/api/BackgroundExecutor;->access$700()Ljava/lang/ThreadLocal;

    move-result-object v0

    iget-object v1, p0, Lorg/androidannotations/api/BackgroundExecutor$Task;->serial:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    .line 403
    invoke-virtual {p0}, Lorg/androidannotations/api/BackgroundExecutor$Task;->execute()V
    :try_end_16
    .catchall {:try_start_a .. :try_end_16} :catchall_1a

    .line 406
    invoke-direct {p0}, Lorg/androidannotations/api/BackgroundExecutor$Task;->postExecute()V

    .line 407
    goto :goto_1f

    .line 406
    :catchall_1a
    move-exception v2

    invoke-direct {p0}, Lorg/androidannotations/api/BackgroundExecutor$Task;->postExecute()V

    throw v2

    .line 408
    :goto_1f
    return-void
.end method

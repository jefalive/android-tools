.class public final Lcom/itau/empresas/feature/home/HomeLogadaActivity_;
.super Lcom/itau/empresas/feature/home/HomeLogadaActivity;
.source "HomeLogadaActivity_.java"

# interfaces
.implements Lorg/androidannotations/api/view/HasViews;
.implements Lorg/androidannotations/api/view/OnViewChangedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/itau/empresas/feature/home/HomeLogadaActivity_$IntentBuilder_;
    }
.end annotation


# instance fields
.field private final onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;


# direct methods
.method public constructor <init>()V
    .registers 2

    .line 35
    invoke-direct {p0}, Lcom/itau/empresas/feature/home/HomeLogadaActivity;-><init>()V

    .line 39
    new-instance v0, Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-direct {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/feature/home/HomeLogadaActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    return-void
.end method

.method static synthetic access$001(Lcom/itau/empresas/feature/home/HomeLogadaActivity_;)V
    .registers 1
    .param p0, "x0"    # Lcom/itau/empresas/feature/home/HomeLogadaActivity_;

    .line 35
    invoke-super {p0}, Lcom/itau/empresas/feature/home/HomeLogadaActivity;->mostraDialogoDeProgresso()V

    return-void
.end method

.method static synthetic access$101(Lcom/itau/empresas/feature/home/HomeLogadaActivity_;)V
    .registers 1
    .param p0, "x0"    # Lcom/itau/empresas/feature/home/HomeLogadaActivity_;

    .line 35
    invoke-super {p0}, Lcom/itau/empresas/feature/home/HomeLogadaActivity;->escondeDialogoDeProgresso()V

    return-void
.end method

.method private init_(Landroid/os/Bundle;)V
    .registers 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 51
    new-instance v0, Lcom/itau/empresas/Preferences_;

    invoke-direct {v0, p0}, Lcom/itau/empresas/Preferences_;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/itau/empresas/feature/home/HomeLogadaActivity_;->prefs:Lcom/itau/empresas/Preferences_;

    .line 52
    invoke-static {p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->registerOnViewChangedListener(Lorg/androidannotations/api/view/OnViewChangedListener;)V

    .line 53
    invoke-static {}, Lcom/itau/empresas/CustomApplication_;->getInstance()Lcom/itau/empresas/CustomApplication;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/home/HomeLogadaActivity_;->application:Lcom/itau/empresas/CustomApplication;

    .line 54
    invoke-static {p0}, Lcom/itau/empresas/feature/logout/LogoutHelper_;->getInstance_(Landroid/content/Context;)Lcom/itau/empresas/feature/logout/LogoutHelper_;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/home/HomeLogadaActivity_;->logoutHelper:Lcom/itau/empresas/feature/logout/LogoutHelper;

    .line 55
    invoke-virtual {p0}, Lcom/itau/empresas/feature/home/HomeLogadaActivity_;->afterInject()V

    .line 56
    return-void
.end method


# virtual methods
.method public escondeDialogoDeProgresso()V
    .registers 5

    .line 118
    const-string v0, ""

    new-instance v1, Lcom/itau/empresas/feature/home/HomeLogadaActivity_$2;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/home/HomeLogadaActivity_$2;-><init>(Lcom/itau/empresas/feature/home/HomeLogadaActivity_;)V

    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3}, Lorg/androidannotations/api/UiThreadExecutor;->runTask(Ljava/lang/String;Ljava/lang/Runnable;J)V

    .line 126
    return-void
.end method

.method public mostraDialogoDeProgresso()V
    .registers 5
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "CommitTransaction"
        }
    .end annotation

    .line 106
    const-string v0, ""

    new-instance v1, Lcom/itau/empresas/feature/home/HomeLogadaActivity_$1;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/home/HomeLogadaActivity_$1;-><init>(Lcom/itau/empresas/feature/home/HomeLogadaActivity_;)V

    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3}, Lorg/androidannotations/api/UiThreadExecutor;->runTask(Ljava/lang/String;Ljava/lang/Runnable;J)V

    .line 114
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 43
    iget-object v0, p0, Lcom/itau/empresas/feature/home/HomeLogadaActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-static {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    move-result-object v1

    .line 44
    .local v1, "previousNotifier":Lorg/androidannotations/api/view/OnViewChangedNotifier;
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/home/HomeLogadaActivity_;->init_(Landroid/os/Bundle;)V

    .line 45
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/home/HomeLogadaActivity;->onCreate(Landroid/os/Bundle;)V

    .line 46
    invoke-static {v1}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 47
    const v0, 0x7f03003d

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/home/HomeLogadaActivity_;->setContentView(I)V

    .line 48
    return-void
.end method

.method public onViewChanged(Lorg/androidannotations/api/view/HasViews;)V
    .registers 3
    .param p1, "hasViews"    # Lorg/androidannotations/api/view/HasViews;

    .line 90
    const v0, 0x7f0e035b

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/itau/empresas/feature/home/HomeLogadaActivity_;->rlContainer:Landroid/widget/RelativeLayout;

    .line 91
    const v0, 0x7f0e0354

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/ui/view/TextViewIcon;

    iput-object v0, p0, Lcom/itau/empresas/feature/home/HomeLogadaActivity_;->imgLogo:Lcom/itau/empresas/ui/view/TextViewIcon;

    .line 92
    const v0, 0x7f0e00c9

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/home/HomeLogadaActivity_;->txtTitulo:Landroid/widget/TextView;

    .line 93
    const v0, 0x7f0e035d

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/home/HomeLogadaActivity_;->txtSubtitulo1:Landroid/widget/TextView;

    .line 94
    const v0, 0x7f0e035e

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/home/HomeLogadaActivity_;->txtSubtitulo2:Landroid/widget/TextView;

    .line 95
    const v0, 0x7f0e025a

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/Toolbar;

    iput-object v0, p0, Lcom/itau/empresas/feature/home/HomeLogadaActivity_;->toolbar:Landroid/support/v7/widget/Toolbar;

    .line 96
    const v0, 0x7f0e035c

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/itau/empresas/feature/home/HomeLogadaActivity_;->rlDadosContaEmOperacao:Landroid/widget/RelativeLayout;

    .line 97
    const v0, 0x7f0e01ba

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/BottomNavigationView;

    iput-object v0, p0, Lcom/itau/empresas/feature/home/HomeLogadaActivity_;->navigationView:Landroid/support/design/widget/BottomNavigationView;

    .line 98
    invoke-virtual {p0}, Lcom/itau/empresas/feature/home/HomeLogadaActivity_;->AfterViews()V

    .line 99
    return-void
.end method

.method public setContentView(I)V
    .registers 3
    .param p1, "layoutResID"    # I

    .line 60
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/home/HomeLogadaActivity;->setContentView(I)V

    .line 61
    iget-object v0, p0, Lcom/itau/empresas/feature/home/HomeLogadaActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 62
    return-void
.end method

.method public setContentView(Landroid/view/View;)V
    .registers 3
    .param p1, "view"    # Landroid/view/View;

    .line 72
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/home/HomeLogadaActivity;->setContentView(Landroid/view/View;)V

    .line 73
    iget-object v0, p0, Lcom/itau/empresas/feature/home/HomeLogadaActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 74
    return-void
.end method

.method public setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .registers 4
    .param p1, "view"    # Landroid/view/View;
    .param p2, "params"    # Landroid/view/ViewGroup$LayoutParams;

    .line 66
    invoke-super {p0, p1, p2}, Lcom/itau/empresas/feature/home/HomeLogadaActivity;->setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 67
    iget-object v0, p0, Lcom/itau/empresas/feature/home/HomeLogadaActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 68
    return-void
.end method

.class public final Lbr/com/itau/sdk/android/core/model/MiddlewareKeyValue;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private id:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "id"
    .end annotation
.end field

.field private idServ:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "ID_SERV"
    .end annotation
.end field

.field private op:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "OP"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 4

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lbr/com/itau/sdk/android/core/model/MiddlewareKeyValue;->idServ:Ljava/lang/String;

    .line 22
    iput-object p2, p0, Lbr/com/itau/sdk/android/core/model/MiddlewareKeyValue;->op:Ljava/lang/String;

    .line 23
    iput-object p3, p0, Lbr/com/itau/sdk/android/core/model/MiddlewareKeyValue;->id:Ljava/lang/String;

    .line 24
    return-void
.end method


# virtual methods
.method public getId()Ljava/lang/String;
    .registers 2

    .line 35
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/model/MiddlewareKeyValue;->id:Ljava/lang/String;

    return-object v0
.end method

.method public getIdServ()Ljava/lang/String;
    .registers 2

    .line 27
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/model/MiddlewareKeyValue;->idServ:Ljava/lang/String;

    return-object v0
.end method

.method public getOp()Ljava/lang/String;
    .registers 2

    .line 31
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/model/MiddlewareKeyValue;->op:Ljava/lang/String;

    return-object v0
.end method

.method public setId(Ljava/lang/String;)V
    .registers 2

    .line 39
    iput-object p1, p0, Lbr/com/itau/sdk/android/core/model/MiddlewareKeyValue;->id:Ljava/lang/String;

    .line 40
    return-void
.end method

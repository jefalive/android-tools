.class public Landroid/support/design/internal/BaselineLayout;
.super Landroid/view/ViewGroup;
.source "BaselineLayout.java"


# instance fields
.field private mBaseline:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 4
    .param p1, "context"    # Landroid/content/Context;

    .line 35
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 32
    const/4 v0, -0x1

    iput v0, p0, Landroid/support/design/internal/BaselineLayout;->mBaseline:I

    .line 36
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .line 39
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 32
    const/4 v0, -0x1

    iput v0, p0, Landroid/support/design/internal/BaselineLayout;->mBaseline:I

    .line 40
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .line 43
    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 32
    const/4 v0, -0x1

    iput v0, p0, Landroid/support/design/internal/BaselineLayout;->mBaseline:I

    .line 44
    return-void
.end method


# virtual methods
.method public getBaseline()I
    .registers 2

    .line 114
    iget v0, p0, Landroid/support/design/internal/BaselineLayout;->mBaseline:I

    return v0
.end method

.method protected onLayout(ZIIII)V
    .registers 19
    .param p1, "changed"    # Z
    .param p2, "left"    # I
    .param p3, "top"    # I
    .param p4, "right"    # I
    .param p5, "bottom"    # I

    .line 85
    invoke-virtual {p0}, Landroid/support/design/internal/BaselineLayout;->getChildCount()I

    move-result v2

    .line 86
    .local v2, "count":I
    invoke-virtual {p0}, Landroid/support/design/internal/BaselineLayout;->getPaddingLeft()I

    move-result v3

    .line 87
    .local v3, "parentLeft":I
    sub-int v0, p4, p2

    invoke-virtual {p0}, Landroid/support/design/internal/BaselineLayout;->getPaddingRight()I

    move-result v1

    sub-int v4, v0, v1

    .line 88
    .local v4, "parentRight":I
    sub-int v5, v4, v3

    .line 89
    .local v5, "parentContentWidth":I
    invoke-virtual {p0}, Landroid/support/design/internal/BaselineLayout;->getPaddingTop()I

    move-result v6

    .line 91
    .local v6, "parentTop":I
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_17
    if-ge v7, v2, :cond_55

    .line 92
    invoke-virtual {p0, v7}, Landroid/support/design/internal/BaselineLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v8

    .line 93
    .local v8, "child":Landroid/view/View;
    invoke-virtual {v8}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_26

    .line 94
    goto :goto_52

    .line 97
    :cond_26
    invoke-virtual {v8}, Landroid/view/View;->getMeasuredWidth()I

    move-result v9

    .line 98
    .local v9, "width":I
    invoke-virtual {v8}, Landroid/view/View;->getMeasuredHeight()I

    move-result v10

    .line 100
    .local v10, "height":I
    sub-int v0, v5, v9

    div-int/lit8 v0, v0, 0x2

    add-int v11, v3, v0

    .line 102
    .local v11, "childLeft":I
    iget v0, p0, Landroid/support/design/internal/BaselineLayout;->mBaseline:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_4a

    invoke-virtual {v8}, Landroid/view/View;->getBaseline()I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_4a

    .line 103
    iget v0, p0, Landroid/support/design/internal/BaselineLayout;->mBaseline:I

    add-int/2addr v0, v6

    invoke-virtual {v8}, Landroid/view/View;->getBaseline()I

    move-result v1

    sub-int v12, v0, v1

    .local v12, "childTop":I
    goto :goto_4b

    .line 105
    .end local v12    # "childTop":I
    :cond_4a
    move v12, v6

    .line 108
    .local v12, "childTop":I
    :goto_4b
    add-int v0, v11, v9

    add-int v1, v12, v10

    invoke-virtual {v8, v11, v12, v0, v1}, Landroid/view/View;->layout(IIII)V

    .line 91
    .end local v8    # "child":Landroid/view/View;
    .end local v9    # "width":I
    .end local v10    # "height":I
    .end local v11    # "childLeft":I
    .end local v12    # "childTop":I
    :goto_52
    add-int/lit8 v7, v7, 0x1

    goto :goto_17

    .line 110
    .end local v7    # "i":I
    :cond_55
    return-void
.end method

.method protected onMeasure(II)V
    .registers 14
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .line 47
    invoke-virtual {p0}, Landroid/support/design/internal/BaselineLayout;->getChildCount()I

    move-result v2

    .line 48
    .local v2, "count":I
    const/4 v3, 0x0

    .line 49
    .local v3, "maxWidth":I
    const/4 v4, 0x0

    .line 50
    .local v4, "maxHeight":I
    const/4 v5, -0x1

    .line 51
    .local v5, "maxChildBaseline":I
    const/4 v6, -0x1

    .line 52
    .local v6, "maxChildDescent":I
    const/4 v7, 0x0

    .line 54
    .local v7, "childState":I
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_a
    if-ge v8, v2, :cond_4b

    .line 55
    invoke-virtual {p0, v8}, Landroid/support/design/internal/BaselineLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v9

    .line 56
    .local v9, "child":Landroid/view/View;
    invoke-virtual {v9}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_19

    .line 57
    goto :goto_48

    .line 60
    :cond_19
    invoke-virtual {p0, v9, p1, p2}, Landroid/support/design/internal/BaselineLayout;->measureChild(Landroid/view/View;II)V

    .line 61
    invoke-virtual {v9}, Landroid/view/View;->getBaseline()I

    move-result v10

    .line 62
    .local v10, "baseline":I
    const/4 v0, -0x1

    if-eq v10, v0, :cond_30

    .line 63
    invoke-static {v5, v10}, Ljava/lang/Math;->max(II)I

    move-result v5

    .line 64
    invoke-virtual {v9}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    sub-int/2addr v0, v10

    invoke-static {v6, v0}, Ljava/lang/Math;->max(II)I

    move-result v6

    .line 66
    :cond_30
    invoke-virtual {v9}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    invoke-static {v3, v0}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 67
    invoke-virtual {v9}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    invoke-static {v4, v0}, Ljava/lang/Math;->max(II)I

    move-result v4

    .line 68
    .line 69
    invoke-static {v9}, Landroid/support/v4/view/ViewCompat;->getMeasuredState(Landroid/view/View;)I

    move-result v0

    .line 68
    invoke-static {v7, v0}, Landroid/support/v7/widget/ViewUtils;->combineMeasuredStates(II)I

    move-result v7

    .line 54
    .end local v9    # "child":Landroid/view/View;
    .end local v10    # "baseline":I
    :goto_48
    add-int/lit8 v8, v8, 0x1

    goto :goto_a

    .line 71
    .end local v8    # "i":I
    :cond_4b
    const/4 v0, -0x1

    if-eq v5, v0, :cond_56

    .line 72
    add-int v0, v5, v6

    invoke-static {v4, v0}, Ljava/lang/Math;->max(II)I

    move-result v4

    .line 73
    iput v5, p0, Landroid/support/design/internal/BaselineLayout;->mBaseline:I

    .line 75
    :cond_56
    invoke-virtual {p0}, Landroid/support/design/internal/BaselineLayout;->getSuggestedMinimumHeight()I

    move-result v0

    invoke-static {v4, v0}, Ljava/lang/Math;->max(II)I

    move-result v4

    .line 76
    invoke-virtual {p0}, Landroid/support/design/internal/BaselineLayout;->getSuggestedMinimumWidth()I

    move-result v0

    invoke-static {v3, v0}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 77
    .line 78
    invoke-static {v3, p1, v7}, Landroid/support/v4/view/ViewCompat;->resolveSizeAndState(III)I

    move-result v0

    shl-int/lit8 v1, v7, 0x10

    .line 79
    invoke-static {v4, p2, v1}, Landroid/support/v4/view/ViewCompat;->resolveSizeAndState(III)I

    move-result v1

    .line 77
    invoke-virtual {p0, v0, v1}, Landroid/support/design/internal/BaselineLayout;->setMeasuredDimension(II)V

    .line 81
    return-void
.end method

.class public final Lbr/com/concretesolutions/canarinho/validator/ValidadorCPF;
.super Ljava/lang/Object;
.source "ValidadorCPF.java"

# interfaces
.implements Lbr/com/concretesolutions/canarinho/validator/Validador;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lbr/com/concretesolutions/canarinho/validator/ValidadorCPF$SingletonHolder;
    }
.end annotation


# static fields
.field private static final DIGITO_PARA_CPF:Lbr/com/concretesolutions/canarinho/DigitoPara;


# direct methods
.method static constructor <clinit>()V
    .registers 5

    .line 15
    new-instance v0, Lbr/com/concretesolutions/canarinho/DigitoPara$Builder;

    invoke-direct {v0}, Lbr/com/concretesolutions/canarinho/DigitoPara$Builder;-><init>()V

    .line 16
    const/4 v1, 0x2

    const/16 v2, 0xb

    invoke-virtual {v0, v1, v2}, Lbr/com/concretesolutions/canarinho/DigitoPara$Builder;->comMultiplicadoresDeAte(II)Lbr/com/concretesolutions/canarinho/DigitoPara$Builder;

    move-result-object v0

    .line 17
    invoke-virtual {v0}, Lbr/com/concretesolutions/canarinho/DigitoPara$Builder;->complementarAoModulo()Lbr/com/concretesolutions/canarinho/DigitoPara$Builder;

    move-result-object v0

    const-string v1, "0"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Integer;

    .line 18
    const/16 v3, 0xa

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const/4 v4, 0x0

    aput-object v3, v2, v4

    const/16 v3, 0xb

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const/4 v4, 0x1

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Lbr/com/concretesolutions/canarinho/DigitoPara$Builder;->trocandoPorSeEncontrar(Ljava/lang/String;[Ljava/lang/Integer;)Lbr/com/concretesolutions/canarinho/DigitoPara$Builder;

    move-result-object v0

    .line 19
    invoke-virtual {v0}, Lbr/com/concretesolutions/canarinho/DigitoPara$Builder;->build()Lbr/com/concretesolutions/canarinho/DigitoPara;

    move-result-object v0

    sput-object v0, Lbr/com/concretesolutions/canarinho/validator/ValidadorCPF;->DIGITO_PARA_CPF:Lbr/com/concretesolutions/canarinho/DigitoPara;

    .line 15
    return-void
.end method

.method private constructor <init>()V
    .registers 1

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    return-void
.end method

.method synthetic constructor <init>(Lbr/com/concretesolutions/canarinho/validator/ValidadorCPF$1;)V
    .registers 2
    .param p1, "x0"    # Lbr/com/concretesolutions/canarinho/validator/ValidadorCPF$1;

    .line 13
    invoke-direct {p0}, Lbr/com/concretesolutions/canarinho/validator/ValidadorCPF;-><init>()V

    return-void
.end method

.method private estaNaListaNegra(Ljava/lang/String;)Z
    .registers 6
    .param p1, "valor"    # Ljava/lang/String;

    .line 82
    const/4 v2, 0x1

    .line 84
    .local v2, "igual":Z
    const/4 v3, 0x1

    .local v3, "i":I
    :goto_2
    const/16 v0, 0xb

    if-ge v3, v0, :cond_17

    if-eqz v2, :cond_17

    .line 85
    invoke-virtual {p1, v3}, Ljava/lang/String;->charAt(I)C

    move-result v0

    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Ljava/lang/String;->charAt(I)C

    move-result v1

    if-eq v0, v1, :cond_14

    .line 86
    const/4 v2, 0x0

    .line 84
    :cond_14
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 90
    .end local v3    # "i":I
    :cond_17
    if-nez v2, :cond_21

    const-string v0, "12345678909"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_23

    :cond_21
    const/4 v0, 0x1

    goto :goto_24

    :cond_23
    const/4 v0, 0x0

    :goto_24
    return v0
.end method

.method static getInstance()Lbr/com/concretesolutions/canarinho/validator/ValidadorCPF;
    .registers 1

    .line 26
    # getter for: Lbr/com/concretesolutions/canarinho/validator/ValidadorCPF$SingletonHolder;->INSTANCE:Lbr/com/concretesolutions/canarinho/validator/ValidadorCPF;
    invoke-static {}, Lbr/com/concretesolutions/canarinho/validator/ValidadorCPF$SingletonHolder;->access$000()Lbr/com/concretesolutions/canarinho/validator/ValidadorCPF;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public ehValido(Landroid/text/Editable;Lbr/com/concretesolutions/canarinho/validator/Validador$ResultadoParcial;)Lbr/com/concretesolutions/canarinho/validator/Validador$ResultadoParcial;
    .registers 6
    .param p1, "valor"    # Landroid/text/Editable;
    .param p2, "resultadoParcial"    # Lbr/com/concretesolutions/canarinho/validator/Validador$ResultadoParcial;

    .line 58
    if-eqz p2, :cond_4

    if-nez p1, :cond_c

    .line 59
    :cond_4
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Valores n\u00e3o podem ser nulos"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 62
    :cond_c
    sget-object v0, Lbr/com/concretesolutions/canarinho/formatador/Formatador$Padroes;->PADRAO_SOMENTE_NUMEROS:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/util/regex/Matcher;->replaceAll(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 64
    .local v2, "desformatado":Ljava/lang/String;
    invoke-virtual {p0, v2}, Lbr/com/concretesolutions/canarinho/validator/ValidadorCPF;->ehValido(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_39

    .line 65
    .line 66
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v1, 0xb

    if-ge v0, v1, :cond_28

    const/4 v0, 0x1

    goto :goto_29

    :cond_28
    const/4 v0, 0x0

    :goto_29
    invoke-virtual {p2, v0}, Lbr/com/concretesolutions/canarinho/validator/Validador$ResultadoParcial;->parcialmenteValido(Z)Lbr/com/concretesolutions/canarinho/validator/Validador$ResultadoParcial;

    move-result-object v0

    const-string v1, "CPF inv\u00e1lido"

    .line 67
    invoke-virtual {v0, v1}, Lbr/com/concretesolutions/canarinho/validator/Validador$ResultadoParcial;->mensagem(Ljava/lang/String;)Lbr/com/concretesolutions/canarinho/validator/Validador$ResultadoParcial;

    move-result-object v0

    .line 68
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lbr/com/concretesolutions/canarinho/validator/Validador$ResultadoParcial;->totalmenteValido(Z)Lbr/com/concretesolutions/canarinho/validator/Validador$ResultadoParcial;

    move-result-object v0

    return-object v0

    .line 71
    .line 72
    :cond_39
    const/4 v0, 0x1

    invoke-virtual {p2, v0}, Lbr/com/concretesolutions/canarinho/validator/Validador$ResultadoParcial;->parcialmenteValido(Z)Lbr/com/concretesolutions/canarinho/validator/Validador$ResultadoParcial;

    move-result-object v0

    .line 73
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lbr/com/concretesolutions/canarinho/validator/Validador$ResultadoParcial;->totalmenteValido(Z)Lbr/com/concretesolutions/canarinho/validator/Validador$ResultadoParcial;

    move-result-object v0

    return-object v0
.end method

.method public ehValido(Ljava/lang/String;)Z
    .registers 9
    .param p1, "value"    # Ljava/lang/String;

    .line 32
    if-eqz p1, :cond_a

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v1, 0xb

    if-ge v0, v1, :cond_c

    .line 33
    :cond_a
    const/4 v0, 0x0

    return v0

    .line 36
    :cond_c
    sget-object v0, Lbr/com/concretesolutions/canarinho/formatador/Formatador$Padroes;->PADRAO_SOMENTE_NUMEROS:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/util/regex/Matcher;->replaceAll(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 38
    .local v2, "desformatado":Ljava/lang/String;
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v1, 0xb

    if-eq v0, v1, :cond_22

    .line 39
    const/4 v0, 0x0

    return v0

    .line 42
    :cond_22
    invoke-direct {p0, v2}, Lbr/com/concretesolutions/canarinho/validator/ValidadorCPF;->estaNaListaNegra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2a

    .line 43
    const/4 v0, 0x0

    return v0

    .line 46
    :cond_2a
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x2

    const/4 v1, 0x0

    invoke-virtual {v2, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 47
    .local v3, "cpfSemDigito":Ljava/lang/String;
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x2

    invoke-virtual {v2, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    .line 49
    .local v4, "digitos":Ljava/lang/String;
    sget-object v0, Lbr/com/concretesolutions/canarinho/validator/ValidadorCPF;->DIGITO_PARA_CPF:Lbr/com/concretesolutions/canarinho/DigitoPara;

    invoke-virtual {v0, v3}, Lbr/com/concretesolutions/canarinho/DigitoPara;->calcula(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 50
    .local v5, "dig1":Ljava/lang/String;
    sget-object v0, Lbr/com/concretesolutions/canarinho/validator/ValidadorCPF;->DIGITO_PARA_CPF:Lbr/com/concretesolutions/canarinho/DigitoPara;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbr/com/concretesolutions/canarinho/DigitoPara;->calcula(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 52
    .local v6, "dig2":Ljava/lang/String;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.class Lio/fabric/sdk/android/services/concurrency/AsyncTask$3;
.super Ljava/util/concurrent/FutureTask;
.source "AsyncTask.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lio/fabric/sdk/android/services/concurrency/AsyncTask;-><init>()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/concurrent/FutureTask<TResult;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lio/fabric/sdk/android/services/concurrency/AsyncTask;


# direct methods
.method constructor <init>(Lio/fabric/sdk/android/services/concurrency/AsyncTask;Ljava/util/concurrent/Callable;)V
    .registers 3
    .param p2, "x0"    # Ljava/util/concurrent/Callable;

    .line 315
    iput-object p1, p0, Lio/fabric/sdk/android/services/concurrency/AsyncTask$3;->this$0:Lio/fabric/sdk/android/services/concurrency/AsyncTask;

    invoke-direct {p0, p2}, Ljava/util/concurrent/FutureTask;-><init>(Ljava/util/concurrent/Callable;)V

    return-void
.end method


# virtual methods
.method protected done()V
    .registers 5

    .line 319
    :try_start_0
    iget-object v0, p0, Lio/fabric/sdk/android/services/concurrency/AsyncTask$3;->this$0:Lio/fabric/sdk/android/services/concurrency/AsyncTask;

    invoke-virtual {p0}, Lio/fabric/sdk/android/services/concurrency/AsyncTask$3;->get()Ljava/lang/Object;

    move-result-object v1

    # invokes: Lio/fabric/sdk/android/services/concurrency/AsyncTask;->postResultIfNotInvoked(Ljava/lang/Object;)V
    invoke-static {v0, v1}, Lio/fabric/sdk/android/services/concurrency/AsyncTask;->access$400(Lio/fabric/sdk/android/services/concurrency/AsyncTask;Ljava/lang/Object;)V
    :try_end_9
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_9} :catch_a
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_9} :catch_11
    .catch Ljava/util/concurrent/CancellationException; {:try_start_0 .. :try_end_9} :catch_1e

    .line 327
    goto :goto_25

    .line 320
    :catch_a
    move-exception v3

    .line 321
    .local v3, "e":Ljava/lang/InterruptedException;
    const-string v0, "AsyncTask"

    invoke-static {v0, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 327
    .end local v3    # "e":Ljava/lang/InterruptedException;
    goto :goto_25

    .line 322
    :catch_11
    move-exception v3

    .line 323
    .local v3, "e":Ljava/util/concurrent/ExecutionException;
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "An error occured while executing doInBackground()"

    invoke-virtual {v3}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0

    .line 325
    .end local v3    # "e":Ljava/util/concurrent/ExecutionException;
    :catch_1e
    move-exception v3

    .line 326
    .local v3, "e":Ljava/util/concurrent/CancellationException;
    iget-object v0, p0, Lio/fabric/sdk/android/services/concurrency/AsyncTask$3;->this$0:Lio/fabric/sdk/android/services/concurrency/AsyncTask;

    const/4 v1, 0x0

    # invokes: Lio/fabric/sdk/android/services/concurrency/AsyncTask;->postResultIfNotInvoked(Ljava/lang/Object;)V
    invoke-static {v0, v1}, Lio/fabric/sdk/android/services/concurrency/AsyncTask;->access$400(Lio/fabric/sdk/android/services/concurrency/AsyncTask;Ljava/lang/Object;)V

    .line 328
    .end local v3    # "e":Ljava/util/concurrent/CancellationException;
    :goto_25
    return-void
.end method

.class Lcom/itau/empresas/feature/login/controller/OperadorController$1;
.super Ljava/lang/Object;
.source "OperadorController.java"

# interfaces
.implements Lbr/com/itau/sdk/android/core/type/BackendCall;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/itau/empresas/feature/login/controller/OperadorController;->buscaContasOperador()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/itau/empresas/feature/login/controller/OperadorController;


# direct methods
.method constructor <init>(Lcom/itau/empresas/feature/login/controller/OperadorController;)V
    .registers 2
    .param p1, "this$0"    # Lcom/itau/empresas/feature/login/controller/OperadorController;

    .line 22
    iput-object p1, p0, Lcom/itau/empresas/feature/login/controller/OperadorController$1;->this$0:Lcom/itau/empresas/feature/login/controller/OperadorController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public callBackend()V
    .registers 6

    .line 25
    iget-object v0, p0, Lcom/itau/empresas/feature/login/controller/OperadorController$1;->this$0:Lcom/itau/empresas/feature/login/controller/OperadorController;

    iget-object v0, v0, Lcom/itau/empresas/feature/login/controller/OperadorController;->app:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getEventoTracker()Lcom/itau/empresas/ui/util/analytics/TrackerEvento;

    move-result-object v0

    const-string v1, "listaContas"

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/analytics/TrackerEvento;->inicioMonitorarTempo(Ljava/lang/String;)V

    .line 26
    iget-object v0, p0, Lcom/itau/empresas/feature/login/controller/OperadorController$1;->this$0:Lcom/itau/empresas/feature/login/controller/OperadorController;

    iget-object v0, v0, Lcom/itau/empresas/feature/login/controller/OperadorController;->app:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getPerfilLogado()Lcom/itau/empresas/api/model/PerfilOperadorVO;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/login/controller/OperadorController$1;->this$0:Lcom/itau/empresas/feature/login/controller/OperadorController;

    .line 27
    # invokes: Lcom/itau/empresas/feature/login/controller/OperadorController;->api()Ljava/lang/Object;
    invoke-static {v1}, Lcom/itau/empresas/feature/login/controller/OperadorController;->access$000(Lcom/itau/empresas/feature/login/controller/OperadorController;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/itau/empresas/feature/login/LoginApi;

    iget-object v2, p0, Lcom/itau/empresas/feature/login/controller/OperadorController$1;->this$0:Lcom/itau/empresas/feature/login/controller/OperadorController;

    iget-object v2, v2, Lcom/itau/empresas/feature/login/controller/OperadorController;->app:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v2}, Lcom/itau/empresas/CustomApplication;->getCodigoOperador()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/itau/empresas/feature/login/LoginApi;->buscaContasOperador(Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/api/model/PerfilOperadorVO;->setContasOperador(Ljava/util/List;)V

    .line 28
    iget-object v0, p0, Lcom/itau/empresas/feature/login/controller/OperadorController$1;->this$0:Lcom/itau/empresas/feature/login/controller/OperadorController;

    iget-object v0, v0, Lcom/itau/empresas/feature/login/controller/OperadorController;->app:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getEventoTracker()Lcom/itau/empresas/ui/util/analytics/TrackerEvento;

    move-result-object v0

    const-string v1, "listaContas"

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/analytics/TrackerEvento;->fimMonitorarTempo(Ljava/lang/String;)V

    .line 30
    iget-object v0, p0, Lcom/itau/empresas/feature/login/controller/OperadorController$1;->this$0:Lcom/itau/empresas/feature/login/controller/OperadorController;

    iget-object v0, v0, Lcom/itau/empresas/feature/login/controller/OperadorController;->app:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getPerfilLogado()Lcom/itau/empresas/api/model/PerfilOperadorVO;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/api/model/PerfilOperadorVO;->getContasOperador()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_49
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_69

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/itau/empresas/api/model/ContaOperadorVO;

    .line 31
    .local v4, "contaOperadorVO":Lcom/itau/empresas/api/model/ContaOperadorVO;
    invoke-virtual {v4}, Lcom/itau/empresas/api/model/ContaOperadorVO;->getContaDefault()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_68

    .line 32
    iget-object v0, p0, Lcom/itau/empresas/feature/login/controller/OperadorController$1;->this$0:Lcom/itau/empresas/feature/login/controller/OperadorController;

    iget-object v0, v0, Lcom/itau/empresas/feature/login/controller/OperadorController;->app:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0, v4}, Lcom/itau/empresas/CustomApplication;->setContaSelecionada(Lcom/itau/empresas/api/model/ContaOperadorVO;)V

    .line 33
    return-void

    .line 35
    .end local v4    # "contaOperadorVO":Lcom/itau/empresas/api/model/ContaOperadorVO;
    :cond_68
    goto :goto_49

    .line 37
    :cond_69
    iget-object v0, p0, Lcom/itau/empresas/feature/login/controller/OperadorController$1;->this$0:Lcom/itau/empresas/feature/login/controller/OperadorController;

    iget-object v0, v0, Lcom/itau/empresas/feature/login/controller/OperadorController;->app:Lcom/itau/empresas/CustomApplication;

    iget-object v1, p0, Lcom/itau/empresas/feature/login/controller/OperadorController$1;->this$0:Lcom/itau/empresas/feature/login/controller/OperadorController;

    iget-object v1, v1, Lcom/itau/empresas/feature/login/controller/OperadorController;->app:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v1}, Lcom/itau/empresas/CustomApplication;->getPerfilLogado()Lcom/itau/empresas/api/model/PerfilOperadorVO;

    move-result-object v1

    invoke-virtual {v1}, Lcom/itau/empresas/api/model/PerfilOperadorVO;->getContasOperador()Ljava/util/List;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/itau/empresas/api/model/ContaOperadorVO;

    invoke-virtual {v0, v1}, Lcom/itau/empresas/CustomApplication;->setContaSelecionada(Lcom/itau/empresas/api/model/ContaOperadorVO;)V

    .line 38
    return-void
.end method

.class public Lcom/itau/empresas/ui/dialog/FingerprintDialog$Builder;
.super Ljava/lang/Object;
.source "FingerprintDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/itau/empresas/ui/dialog/FingerprintDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private callback:Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintCallback;

.field private final context:Landroid/content/Context;

.field private final texto:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .registers 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "texto"    # Ljava/lang/String;

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-object p1, p0, Lcom/itau/empresas/ui/dialog/FingerprintDialog$Builder;->context:Landroid/content/Context;

    .line 38
    iput-object p2, p0, Lcom/itau/empresas/ui/dialog/FingerprintDialog$Builder;->texto:Ljava/lang/String;

    .line 39
    return-void
.end method

.method private criarDialogExterno(Lcom/itau/empresas/ui/view/FingerprintValidacaoView;Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintCanceller;)Landroid/support/v7/app/AlertDialog;
    .registers 6
    .param p1, "view"    # Lcom/itau/empresas/ui/view/FingerprintValidacaoView;
    .param p2, "signal"    # Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintCanceller;

    .line 70
    new-instance v2, Lcom/itau/empresas/ui/dialog/FingerprintDialog$AbreLoginComSenhaOnClickListener;

    invoke-direct {v2, p2}, Lcom/itau/empresas/ui/dialog/FingerprintDialog$AbreLoginComSenhaOnClickListener;-><init>(Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintCanceller;)V

    .line 71
    .local v2, "listener":Lcom/itau/empresas/ui/dialog/FingerprintDialog$AbreLoginComSenhaOnClickListener;
    new-instance v0, Landroid/support/v7/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/itau/empresas/ui/dialog/FingerprintDialog$Builder;->context:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/support/v7/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 72
    invoke-virtual {v0, p1}, Landroid/support/v7/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v0

    .line 73
    const v1, 0x7f07022f

    invoke-virtual {v0, v1, v2}, Landroid/support/v7/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v0

    .line 74
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/AlertDialog$Builder;->setCancelable(Z)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v0

    .line 75
    invoke-virtual {v0, v2}, Landroid/support/v7/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v0

    .line 76
    invoke-virtual {v0}, Landroid/support/v7/app/AlertDialog$Builder;->create()Landroid/support/v7/app/AlertDialog;

    move-result-object v0

    .line 71
    return-object v0
.end method

.method private criarDialogInterno(Lcom/itau/empresas/ui/view/FingerprintValidacaoView;Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintCanceller;)Landroid/support/v7/app/AlertDialog;
    .registers 6
    .param p1, "view"    # Lcom/itau/empresas/ui/view/FingerprintValidacaoView;
    .param p2, "signal"    # Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintCanceller;

    .line 81
    new-instance v0, Landroid/support/v7/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/itau/empresas/ui/dialog/FingerprintDialog$Builder;->context:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/support/v7/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 82
    invoke-virtual {v0, p1}, Landroid/support/v7/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/itau/empresas/ui/dialog/FingerprintDialog$DismissOnClickListener;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/itau/empresas/ui/dialog/FingerprintDialog$DismissOnClickListener;-><init>(Lcom/itau/empresas/ui/dialog/FingerprintDialog$1;)V

    .line 83
    const v2, 0x7f070175

    invoke-virtual {v0, v2, v1}, Landroid/support/v7/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v0

    .line 84
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/AlertDialog$Builder;->setCancelable(Z)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/itau/empresas/ui/dialog/FingerprintDialog$AbreLoginComSenhaOnClickListener;

    invoke-direct {v1, p2}, Lcom/itau/empresas/ui/dialog/FingerprintDialog$AbreLoginComSenhaOnClickListener;-><init>(Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintCanceller;)V

    .line 85
    invoke-virtual {v0, v1}, Landroid/support/v7/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v0

    .line 86
    invoke-virtual {v0}, Landroid/support/v7/app/AlertDialog$Builder;->create()Landroid/support/v7/app/AlertDialog;

    move-result-object v0

    .line 81
    return-object v0
.end method

.method private isDialogoExterno()Z
    .registers 4

    .line 90
    iget-object v0, p0, Lcom/itau/empresas/ui/dialog/FingerprintDialog$Builder;->texto:Ljava/lang/String;

    iget-object v1, p0, Lcom/itau/empresas/ui/dialog/FingerprintDialog$Builder;->context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 91
    const v2, 0x7f0706c5

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 90
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public callback(Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintCallback;)Lcom/itau/empresas/ui/dialog/FingerprintDialog$Builder;
    .registers 2
    .param p1, "callback"    # Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintCallback;

    .line 42
    iput-object p1, p0, Lcom/itau/empresas/ui/dialog/FingerprintDialog$Builder;->callback:Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintCallback;

    .line 43
    return-object p0
.end method

.method public show()V
    .registers 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintException;
        }
    .end annotation

    .line 47
    iget-object v0, p0, Lcom/itau/empresas/ui/dialog/FingerprintDialog$Builder;->context:Landroid/content/Context;

    .line 48
    invoke-static {v0}, Lcom/itau/empresas/feature/chaveacesso/FingerPrintManagerFactory;->criaFingerPrintManager(Landroid/content/Context;)Lbr/com/itau/sdk/android/fingerprintcore/FingerprintManager;

    move-result-object v2

    .line 49
    .local v2, "manager":Lbr/com/itau/sdk/android/fingerprintcore/FingerprintManager;
    if-eqz v2, :cond_e

    invoke-interface {v2}, Lbr/com/itau/sdk/android/fingerprintcore/FingerprintManager;->isUnavailable()Z

    move-result v0

    if-eqz v0, :cond_16

    .line 50
    :cond_e
    new-instance v0, Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintException;

    const-string v1, "Fingerprint n\u00e3o dispon\u00edvel"

    invoke-direct {v0, v1}, Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 52
    :cond_16
    new-instance v3, Lcom/itau/empresas/ui/view/FingerprintValidacaoView;

    iget-object v0, p0, Lcom/itau/empresas/ui/dialog/FingerprintDialog$Builder;->context:Landroid/content/Context;

    invoke-direct {v3, v0}, Lcom/itau/empresas/ui/view/FingerprintValidacaoView;-><init>(Landroid/content/Context;)V

    .line 53
    .local v3, "view":Lcom/itau/empresas/ui/view/FingerprintValidacaoView;
    iget-object v0, p0, Lcom/itau/empresas/ui/dialog/FingerprintDialog$Builder;->texto:Ljava/lang/String;

    invoke-virtual {v3, v0}, Lcom/itau/empresas/ui/view/FingerprintValidacaoView;->mudaTextoInterno(Ljava/lang/String;)V

    .line 55
    .line 56
    invoke-static {v2}, Lbr/com/itau/sdk/android/fingerprintcore/DefaultFingerprintCanceller;->getDefaultInstance(Lbr/com/itau/sdk/android/fingerprintcore/FingerprintManager;)Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintCanceller;

    move-result-object v4

    .line 59
    .local v4, "signal":Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintCanceller;
    invoke-direct {p0}, Lcom/itau/empresas/ui/dialog/FingerprintDialog$Builder;->isDialogoExterno()Z

    move-result v0

    if-eqz v0, :cond_31

    .line 60
    invoke-direct {p0, v3, v4}, Lcom/itau/empresas/ui/dialog/FingerprintDialog$Builder;->criarDialogExterno(Lcom/itau/empresas/ui/view/FingerprintValidacaoView;Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintCanceller;)Landroid/support/v7/app/AlertDialog;

    move-result-object v5

    .local v5, "dialog":Landroid/support/v7/app/AlertDialog;
    goto :goto_35

    .line 62
    .end local v5    # "dialog":Landroid/support/v7/app/AlertDialog;
    :cond_31
    invoke-direct {p0, v3, v4}, Lcom/itau/empresas/ui/dialog/FingerprintDialog$Builder;->criarDialogInterno(Lcom/itau/empresas/ui/view/FingerprintValidacaoView;Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintCanceller;)Landroid/support/v7/app/AlertDialog;

    move-result-object v5

    .line 64
    .local v5, "dialog":Landroid/support/v7/app/AlertDialog;
    :goto_35
    invoke-virtual {v5}, Landroid/support/v7/app/AlertDialog;->show()V

    .line 65
    new-instance v0, Lcom/itau/empresas/ui/dialog/FingerprintDialogCallback;

    iget-object v1, p0, Lcom/itau/empresas/ui/dialog/FingerprintDialog$Builder;->callback:Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintCallback;

    invoke-direct {v0, v3, v1, v5}, Lcom/itau/empresas/ui/dialog/FingerprintDialogCallback;-><init>(Lcom/itau/empresas/ui/view/FingerprintValidacaoView;Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintCallback;Landroid/support/v7/app/AlertDialog;)V

    invoke-interface {v2, v0, v4}, Lbr/com/itau/sdk/android/fingerprintcore/FingerprintManager;->authenticate(Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintCallback;Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintCanceller;)Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintCanceller;

    .line 66
    return-void
.end method

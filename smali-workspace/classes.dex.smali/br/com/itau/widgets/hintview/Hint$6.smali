.class Lbr/com/itau/widgets/hintview/Hint$6;
.super Ljava/lang/Object;
.source "Hint.java"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lbr/com/itau/widgets/hintview/Hint;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lbr/com/itau/widgets/hintview/Hint;


# direct methods
.method constructor <init>(Lbr/com/itau/widgets/hintview/Hint;)V
    .registers 2
    .param p1, "this$0"    # Lbr/com/itau/widgets/hintview/Hint;

    .line 136
    iput-object p1, p0, Lbr/com/itau/widgets/hintview/Hint$6;->this$0:Lbr/com/itau/widgets/hintview/Hint;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onGlobalLayout()V
    .registers 7

    .line 139
    iget-object v0, p0, Lbr/com/itau/widgets/hintview/Hint$6;->this$0:Lbr/com/itau/widgets/hintview/Hint;

    # getter for: Lbr/com/itau/widgets/hintview/Hint;->mContentView:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lbr/com/itau/widgets/hintview/Hint;->access$400(Lbr/com/itau/widgets/hintview/Hint;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-static {v0, p0}, Lbr/com/itau/widgets/hintview/Util;->removeOnGlobalLayoutListener(Landroid/view/View;Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 141
    iget-object v0, p0, Lbr/com/itau/widgets/hintview/Hint$6;->this$0:Lbr/com/itau/widgets/hintview/Hint;

    # getter for: Lbr/com/itau/widgets/hintview/Hint;->mContentView:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lbr/com/itau/widgets/hintview/Hint;->access$400(Lbr/com/itau/widgets/hintview/Hint;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    iget-object v1, p0, Lbr/com/itau/widgets/hintview/Hint$6;->this$0:Lbr/com/itau/widgets/hintview/Hint;

    # getter for: Lbr/com/itau/widgets/hintview/Hint;->mArrowLayoutListener:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;
    invoke-static {v1}, Lbr/com/itau/widgets/hintview/Hint;->access$900(Lbr/com/itau/widgets/hintview/Hint;)Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 142
    iget-object v0, p0, Lbr/com/itau/widgets/hintview/Hint$6;->this$0:Lbr/com/itau/widgets/hintview/Hint;

    # invokes: Lbr/com/itau/widgets/hintview/Hint;->calculateLocation()Landroid/graphics/PointF;
    invoke-static {v0}, Lbr/com/itau/widgets/hintview/Hint;->access$1000(Lbr/com/itau/widgets/hintview/Hint;)Landroid/graphics/PointF;

    move-result-object v5

    .line 143
    .local v5, "location":Landroid/graphics/PointF;
    iget-object v0, p0, Lbr/com/itau/widgets/hintview/Hint$6;->this$0:Lbr/com/itau/widgets/hintview/Hint;

    # getter for: Lbr/com/itau/widgets/hintview/Hint;->mPopupWindow:Landroid/widget/PopupWindow;
    invoke-static {v0}, Lbr/com/itau/widgets/hintview/Hint;->access$1100(Lbr/com/itau/widgets/hintview/Hint;)Landroid/widget/PopupWindow;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setClippingEnabled(Z)V

    .line 144
    iget-object v0, p0, Lbr/com/itau/widgets/hintview/Hint$6;->this$0:Lbr/com/itau/widgets/hintview/Hint;

    # getter for: Lbr/com/itau/widgets/hintview/Hint;->mPopupWindow:Landroid/widget/PopupWindow;
    invoke-static {v0}, Lbr/com/itau/widgets/hintview/Hint;->access$1100(Lbr/com/itau/widgets/hintview/Hint;)Landroid/widget/PopupWindow;

    move-result-object v0

    iget v1, v5, Landroid/graphics/PointF;->x:F

    float-to-int v1, v1

    iget v2, v5, Landroid/graphics/PointF;->y:F

    float-to-int v2, v2

    iget-object v3, p0, Lbr/com/itau/widgets/hintview/Hint$6;->this$0:Lbr/com/itau/widgets/hintview/Hint;

    # getter for: Lbr/com/itau/widgets/hintview/Hint;->mPopupWindow:Landroid/widget/PopupWindow;
    invoke-static {v3}, Lbr/com/itau/widgets/hintview/Hint;->access$1100(Lbr/com/itau/widgets/hintview/Hint;)Landroid/widget/PopupWindow;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/PopupWindow;->getWidth()I

    move-result v3

    iget-object v4, p0, Lbr/com/itau/widgets/hintview/Hint$6;->this$0:Lbr/com/itau/widgets/hintview/Hint;

    # getter for: Lbr/com/itau/widgets/hintview/Hint;->mPopupWindow:Landroid/widget/PopupWindow;
    invoke-static {v4}, Lbr/com/itau/widgets/hintview/Hint;->access$1100(Lbr/com/itau/widgets/hintview/Hint;)Landroid/widget/PopupWindow;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/PopupWindow;->getHeight()I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/PopupWindow;->update(IIII)V

    .line 145
    return-void
.end method

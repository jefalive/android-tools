.class public final Lbr/com/itau/sdk/android/core/model/TokenDTO;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<Lbr/com/itau/sdk/android/core/model/TokenDTO;>;"
        }
    .end annotation
.end field


# instance fields
.field private serialNumber:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "numeroSerie"
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .line 12
    new-instance v0, Lbr/com/itau/sdk/android/core/model/TokenDTO$1;

    invoke-direct {v0}, Lbr/com/itau/sdk/android/core/model/TokenDTO$1;-><init>()V

    sput-object v0, Lbr/com/itau/sdk/android/core/model/TokenDTO;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method protected constructor <init>(Landroid/os/Parcel;)V
    .registers 3

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/sdk/android/core/model/TokenDTO;->serialNumber:Ljava/lang/String;

    .line 29
    return-void
.end method


# virtual methods
.method public describeContents()I
    .registers 2

    .line 41
    const/4 v0, 0x0

    return v0
.end method

.method public getSerialNumber()Ljava/lang/String;
    .registers 2

    .line 36
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/model/TokenDTO;->serialNumber:Ljava/lang/String;

    return-object v0
.end method

.method public setSerialNumber(Ljava/lang/String;)V
    .registers 2

    .line 32
    iput-object p1, p0, Lbr/com/itau/sdk/android/core/model/TokenDTO;->serialNumber:Ljava/lang/String;

    .line 33
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 4

    .line 46
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/model/TokenDTO;->serialNumber:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 47
    return-void
.end method

.class public final enum Lbr/com/itau/sdk/android/core/token/TokenEvent$Category;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lbr/com/itau/sdk/android/core/token/TokenEvent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Category"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<Lbr/com/itau/sdk/android/core/token/TokenEvent$Category;>;"
    }
.end annotation


# static fields
.field private static final $:[B

.field private static $$:I

.field private static final synthetic $VALUES:[Lbr/com/itau/sdk/android/core/token/TokenEvent$Category;

.field public static final enum PASSWORD:Lbr/com/itau/sdk/android/core/token/TokenEvent$Category;

.field public static final enum TOKEN:Lbr/com/itau/sdk/android/core/token/TokenEvent$Category;


# direct methods
.method private static $(III)Ljava/lang/String;
    .registers 9

    mul-int/lit8 p0, p0, 0x3

    add-int/lit8 p0, p0, 0x5

    new-instance v0, Ljava/lang/String;

    add-int/lit8 p1, p1, 0x4

    sget-object v4, Lbr/com/itau/sdk/android/core/token/TokenEvent$Category;->$:[B

    const/4 v5, 0x0

    mul-int/lit8 p2, p2, 0x4

    add-int/lit8 p2, p2, 0x50

    new-array v1, p0, [B

    add-int/lit8 p0, p0, -0x1

    if-nez v4, :cond_1b

    move v2, p2

    move v3, p1

    :goto_17
    neg-int v3, v3

    add-int/2addr v2, v3

    add-int/lit8 p2, v2, 0x1

    :cond_1b
    add-int/lit8 p1, p1, 0x1

    int-to-byte v2, p2

    aput-byte v2, v1, v5

    if-ne v5, p0, :cond_27

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Ljava/lang/String;-><init>([BI)V

    return-object v0

    :cond_27
    move v2, p2

    add-int/lit8 v5, v5, 0x1

    aget-byte v3, v4, p1

    goto :goto_17
.end method

.method static constructor <clinit>()V
    .registers 5

    .line 9
    const/16 v0, 0xf

    new-array v0, v0, [B

    fill-array-data v0, :array_5a

    sput-object v0, Lbr/com/itau/sdk/android/core/token/TokenEvent$Category;->$:[B

    const/16 v0, 0xd7

    sput v0, Lbr/com/itau/sdk/android/core/token/TokenEvent$Category;->$$:I

    new-instance v0, Lbr/com/itau/sdk/android/core/token/TokenEvent$Category;

    sget-object v1, Lbr/com/itau/sdk/android/core/token/TokenEvent$Category;->$:[B

    const/16 v2, 0xa

    aget-byte v1, v1, v2

    sget-object v2, Lbr/com/itau/sdk/android/core/token/TokenEvent$Category;->$:[B

    const/16 v3, 0xb

    aget-byte v2, v2, v3

    neg-int v2, v2

    add-int/lit8 v3, v2, -0x3

    invoke-static {v1, v2, v3}, Lbr/com/itau/sdk/android/core/token/TokenEvent$Category;->$(III)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lbr/com/itau/sdk/android/core/token/TokenEvent$Category;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbr/com/itau/sdk/android/core/token/TokenEvent$Category;->PASSWORD:Lbr/com/itau/sdk/android/core/token/TokenEvent$Category;

    .line 14
    new-instance v0, Lbr/com/itau/sdk/android/core/token/TokenEvent$Category;

    sget-object v1, Lbr/com/itau/sdk/android/core/token/TokenEvent$Category;->$:[B

    const/16 v2, 0xa

    aget-byte v1, v1, v2

    add-int/lit8 v1, v1, -0x1

    sget-object v2, Lbr/com/itau/sdk/android/core/token/TokenEvent$Category;->$:[B

    const/16 v3, 0xa

    aget-byte v2, v2, v3

    neg-int v2, v2

    sget-object v3, Lbr/com/itau/sdk/android/core/token/TokenEvent$Category;->$:[B

    const/16 v4, 0xa

    aget-byte v3, v3, v4

    invoke-static {v1, v2, v3}, Lbr/com/itau/sdk/android/core/token/TokenEvent$Category;->$(III)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lbr/com/itau/sdk/android/core/token/TokenEvent$Category;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbr/com/itau/sdk/android/core/token/TokenEvent$Category;->TOKEN:Lbr/com/itau/sdk/android/core/token/TokenEvent$Category;

    .line 5
    const/4 v0, 0x2

    new-array v0, v0, [Lbr/com/itau/sdk/android/core/token/TokenEvent$Category;

    sget-object v1, Lbr/com/itau/sdk/android/core/token/TokenEvent$Category;->PASSWORD:Lbr/com/itau/sdk/android/core/token/TokenEvent$Category;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sget-object v1, Lbr/com/itau/sdk/android/core/token/TokenEvent$Category;->TOKEN:Lbr/com/itau/sdk/android/core/token/TokenEvent$Category;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    sput-object v0, Lbr/com/itau/sdk/android/core/token/TokenEvent$Category;->$VALUES:[Lbr/com/itau/sdk/android/core/token/TokenEvent$Category;

    return-void

    nop

    :array_5a
    .array-data 1
        0x6et
        -0xft
        0x12t
        -0x71t
        0x6t
        0x5t
        0x7t
        -0x8t
        0x10t
        -0x11t
        0x1t
        -0x3t
        0x9t
        -0x2t
        0xft
    .end array-data
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 5
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lbr/com/itau/sdk/android/core/token/TokenEvent$Category;
    .registers 2

    .line 5
    const-class v0, Lbr/com/itau/sdk/android/core/token/TokenEvent$Category;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lbr/com/itau/sdk/android/core/token/TokenEvent$Category;

    return-object v0
.end method

.method public static values()[Lbr/com/itau/sdk/android/core/token/TokenEvent$Category;
    .registers 1

    .line 5
    sget-object v0, Lbr/com/itau/sdk/android/core/token/TokenEvent$Category;->$VALUES:[Lbr/com/itau/sdk/android/core/token/TokenEvent$Category;

    invoke-virtual {v0}, [Lbr/com/itau/sdk/android/core/token/TokenEvent$Category;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lbr/com/itau/sdk/android/core/token/TokenEvent$Category;

    return-object v0
.end method

.class public Lcom/google/android/gms/internal/zzaq;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/internal/zzaq$zza;
    }
.end annotation


# instance fields
.field private final zznF:Lcom/google/android/gms/internal/zzap;

.field private final zzox:Ljava/security/SecureRandom;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/internal/zzap;Ljava/security/SecureRandom;)V
    .registers 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/gms/internal/zzaq;->zznF:Lcom/google/android/gms/internal/zzap;

    iput-object p2, p0, Lcom/google/android/gms/internal/zzaq;->zzox:Ljava/security/SecureRandom;

    return-void
.end method

.method static zze([B)V
    .registers 4

    const/4 v2, 0x0

    :goto_1
    array-length v0, p0

    if-ge v2, v0, :cond_e

    aget-byte v0, p0, v2

    xor-int/lit8 v0, v0, 0x44

    int-to-byte v0, v0

    aput-byte v0, p0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_e
    return-void
.end method


# virtual methods
.method public zzc([BLjava/lang/String;)[B
    .registers 11
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/gms/internal/zzaq$zza;
        }
    .end annotation

    array-length v0, p1

    const/16 v1, 0x10

    if-eq v0, v1, :cond_b

    new-instance v0, Lcom/google/android/gms/internal/zzaq$zza;

    invoke-direct {v0, p0}, Lcom/google/android/gms/internal/zzaq$zza;-><init>(Lcom/google/android/gms/internal/zzaq;)V

    throw v0

    :cond_b
    :try_start_b
    iget-object v0, p0, Lcom/google/android/gms/internal/zzaq;->zznF:Lcom/google/android/gms/internal/zzap;

    const/4 v1, 0x0

    invoke-interface {v0, p2, v1}, Lcom/google/android/gms/internal/zzap;->zza(Ljava/lang/String;Z)[B

    move-result-object v2

    array-length v0, v2

    const/16 v1, 0x10

    if-gt v0, v1, :cond_1d

    new-instance v0, Lcom/google/android/gms/internal/zzaq$zza;

    invoke-direct {v0, p0}, Lcom/google/android/gms/internal/zzaq$zza;-><init>(Lcom/google/android/gms/internal/zzaq;)V

    throw v0

    :cond_1d
    array-length v0, v2

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    const/16 v0, 0x10

    new-array v4, v0, [B

    array-length v0, v2

    add-int/lit8 v0, v0, -0x10

    new-array v5, v0, [B

    invoke-virtual {v3, v4}, Ljava/nio/ByteBuffer;->get([B)Ljava/nio/ByteBuffer;

    invoke-virtual {v3, v5}, Ljava/nio/ByteBuffer;->get([B)Ljava/nio/ByteBuffer;

    new-instance v6, Ljavax/crypto/spec/SecretKeySpec;

    const-string v0, "AES"

    invoke-direct {v6, p1, v0}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    const-string v0, "AES/CBC/PKCS5Padding"

    invoke-static {v0}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;

    move-result-object v7

    new-instance v0, Ljavax/crypto/spec/IvParameterSpec;

    invoke-direct {v0, v4}, Ljavax/crypto/spec/IvParameterSpec;-><init>([B)V

    const/4 v1, 0x2

    invoke-virtual {v7, v1, v6, v0}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;Ljava/security/spec/AlgorithmParameterSpec;)V

    invoke-virtual {v7, v5}, Ljavax/crypto/Cipher;->doFinal([B)[B
    :try_end_50
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_b .. :try_end_50} :catch_52
    .catch Ljava/security/InvalidKeyException; {:try_start_b .. :try_end_50} :catch_59
    .catch Ljavax/crypto/IllegalBlockSizeException; {:try_start_b .. :try_end_50} :catch_60
    .catch Ljavax/crypto/NoSuchPaddingException; {:try_start_b .. :try_end_50} :catch_67
    .catch Ljavax/crypto/BadPaddingException; {:try_start_b .. :try_end_50} :catch_6e
    .catch Ljava/security/InvalidAlgorithmParameterException; {:try_start_b .. :try_end_50} :catch_75
    .catch Ljava/lang/IllegalArgumentException; {:try_start_b .. :try_end_50} :catch_7c

    move-result-object v0

    return-object v0

    :catch_52
    move-exception v2

    new-instance v0, Lcom/google/android/gms/internal/zzaq$zza;

    invoke-direct {v0, p0, v2}, Lcom/google/android/gms/internal/zzaq$zza;-><init>(Lcom/google/android/gms/internal/zzaq;Ljava/lang/Throwable;)V

    throw v0

    :catch_59
    move-exception v2

    new-instance v0, Lcom/google/android/gms/internal/zzaq$zza;

    invoke-direct {v0, p0, v2}, Lcom/google/android/gms/internal/zzaq$zza;-><init>(Lcom/google/android/gms/internal/zzaq;Ljava/lang/Throwable;)V

    throw v0

    :catch_60
    move-exception v2

    new-instance v0, Lcom/google/android/gms/internal/zzaq$zza;

    invoke-direct {v0, p0, v2}, Lcom/google/android/gms/internal/zzaq$zza;-><init>(Lcom/google/android/gms/internal/zzaq;Ljava/lang/Throwable;)V

    throw v0

    :catch_67
    move-exception v2

    new-instance v0, Lcom/google/android/gms/internal/zzaq$zza;

    invoke-direct {v0, p0, v2}, Lcom/google/android/gms/internal/zzaq$zza;-><init>(Lcom/google/android/gms/internal/zzaq;Ljava/lang/Throwable;)V

    throw v0

    :catch_6e
    move-exception v2

    new-instance v0, Lcom/google/android/gms/internal/zzaq$zza;

    invoke-direct {v0, p0, v2}, Lcom/google/android/gms/internal/zzaq$zza;-><init>(Lcom/google/android/gms/internal/zzaq;Ljava/lang/Throwable;)V

    throw v0

    :catch_75
    move-exception v2

    new-instance v0, Lcom/google/android/gms/internal/zzaq$zza;

    invoke-direct {v0, p0, v2}, Lcom/google/android/gms/internal/zzaq$zza;-><init>(Lcom/google/android/gms/internal/zzaq;Ljava/lang/Throwable;)V

    throw v0

    :catch_7c
    move-exception v2

    new-instance v0, Lcom/google/android/gms/internal/zzaq$zza;

    invoke-direct {v0, p0, v2}, Lcom/google/android/gms/internal/zzaq$zza;-><init>(Lcom/google/android/gms/internal/zzaq;Ljava/lang/Throwable;)V

    throw v0
.end method

.method public zzl(Ljava/lang/String;)[B
    .registers 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/gms/internal/zzaq$zza;
        }
    .end annotation

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/internal/zzaq;->zznF:Lcom/google/android/gms/internal/zzap;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, Lcom/google/android/gms/internal/zzap;->zza(Ljava/lang/String;Z)[B

    move-result-object v2

    array-length v0, v2

    const/16 v1, 0x20

    if-eq v0, v1, :cond_12

    new-instance v0, Lcom/google/android/gms/internal/zzaq$zza;

    invoke-direct {v0, p0}, Lcom/google/android/gms/internal/zzaq$zza;-><init>(Lcom/google/android/gms/internal/zzaq;)V

    throw v0

    :cond_12
    const/4 v0, 0x4

    const/16 v1, 0x10

    invoke-static {v2, v0, v1}, Ljava/nio/ByteBuffer;->wrap([BII)Ljava/nio/ByteBuffer;

    move-result-object v3

    const/16 v0, 0x10

    new-array v4, v0, [B

    invoke-virtual {v3, v4}, Ljava/nio/ByteBuffer;->get([B)Ljava/nio/ByteBuffer;

    invoke-static {v4}, Lcom/google/android/gms/internal/zzaq;->zze([B)V
    :try_end_23
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_23} :catch_24

    return-object v4

    :catch_24
    move-exception v2

    new-instance v0, Lcom/google/android/gms/internal/zzaq$zza;

    invoke-direct {v0, p0, v2}, Lcom/google/android/gms/internal/zzaq$zza;-><init>(Lcom/google/android/gms/internal/zzaq;Ljava/lang/Throwable;)V

    throw v0
.end method

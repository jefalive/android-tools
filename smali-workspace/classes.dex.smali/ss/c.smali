.class public final Lss/c;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/content/SharedPreferences$Editor;


# instance fields
.field final synthetic a:Lbr/com/itau/security/securestorage/SecureStorage;

.field private final b:Landroid/content/SharedPreferences$Editor;


# direct methods
.method private constructor <init>(Lbr/com/itau/security/securestorage/SecureStorage;)V
    .registers 3

    .line 210
    iput-object p1, p0, Lss/c;->a:Lbr/com/itau/security/securestorage/SecureStorage;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 212
    iget-object v0, p0, Lss/c;->a:Lbr/com/itau/security/securestorage/SecureStorage;

    invoke-static {v0}, Lbr/com/itau/security/securestorage/SecureStorage;->a(Lbr/com/itau/security/securestorage/SecureStorage;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    iput-object v0, p0, Lss/c;->b:Landroid/content/SharedPreferences$Editor;

    return-void
.end method

.method public synthetic constructor <init>(Lbr/com/itau/security/securestorage/SecureStorage;B)V
    .registers 3

    .line 210
    invoke-direct {p0, p1}, Lss/c;-><init>(Lbr/com/itau/security/securestorage/SecureStorage;)V

    return-void
.end method


# virtual methods
.method public final apply()V
    .registers 2

    .line 275
    iget-object v0, p0, Lss/c;->b:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 276
    return-void
.end method

.method public final clear()Landroid/content/SharedPreferences$Editor;
    .registers 2

    .line 264
    iget-object v0, p0, Lss/c;->b:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    .line 265
    return-object p0
.end method

.method public final commit()Z
    .registers 2

    .line 270
    iget-object v0, p0, Lss/c;->b:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    move-result v0

    return v0
.end method

.method public final putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;
    .registers 6

    .line 252
    iget-object v0, p0, Lss/c;->b:Landroid/content/SharedPreferences$Editor;

    invoke-static {p1}, Lbr/com/itau/security/securestorage/SecureStorage;->encrypt(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p2}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lbr/com/itau/security/securestorage/SecureStorage;->encrypt(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 253
    return-object p0
.end method

.method public final putFloat(Ljava/lang/String;F)Landroid/content/SharedPreferences$Editor;
    .registers 6

    .line 246
    iget-object v0, p0, Lss/c;->b:Landroid/content/SharedPreferences$Editor;

    invoke-static {p1}, Lbr/com/itau/security/securestorage/SecureStorage;->encrypt(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p2}, Ljava/lang/Float;->toString(F)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lbr/com/itau/security/securestorage/SecureStorage;->encrypt(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 247
    return-object p0
.end method

.method public final putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;
    .registers 6

    .line 234
    iget-object v0, p0, Lss/c;->b:Landroid/content/SharedPreferences$Editor;

    invoke-static {p1}, Lbr/com/itau/security/securestorage/SecureStorage;->encrypt(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lbr/com/itau/security/securestorage/SecureStorage;->encrypt(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 235
    return-object p0
.end method

.method public final putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;
    .registers 7

    .line 240
    iget-object v0, p0, Lss/c;->b:Landroid/content/SharedPreferences$Editor;

    invoke-static {p1}, Lbr/com/itau/security/securestorage/SecureStorage;->encrypt(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p2, p3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lbr/com/itau/security/securestorage/SecureStorage;->encrypt(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 241
    return-object p0
.end method

.method public final putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;
    .registers 6

    .line 216
    iget-object v0, p0, Lss/c;->b:Landroid/content/SharedPreferences$Editor;

    invoke-static {p1}, Lbr/com/itau/security/securestorage/SecureStorage;->encrypt(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p2}, Lbr/com/itau/security/securestorage/SecureStorage;->encrypt(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 217
    return-object p0
.end method

.method public final putStringSet(Ljava/lang/String;Ljava/util/Set;)Landroid/content/SharedPreferences$Editor;
    .registers 7

    .line 223
    new-instance v2, Ljava/util/HashSet;

    invoke-interface {p2}, Ljava/util/Set;->size()I

    move-result v0

    invoke-direct {v2, v0}, Ljava/util/HashSet;-><init>(I)V

    .line 225
    invoke-interface {p2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_d
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_22

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Ljava/lang/String;

    .line 226
    invoke-static {v3}, Lbr/com/itau/security/securestorage/SecureStorage;->encrypt(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_d

    .line 228
    :cond_22
    iget-object v0, p0, Lss/c;->b:Landroid/content/SharedPreferences$Editor;

    invoke-static {p1}, Lbr/com/itau/security/securestorage/SecureStorage;->encrypt(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putStringSet(Ljava/lang/String;Ljava/util/Set;)Landroid/content/SharedPreferences$Editor;

    .line 229
    return-object p0
.end method

.method public final remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;
    .registers 4

    .line 258
    iget-object v0, p0, Lss/c;->b:Landroid/content/SharedPreferences$Editor;

    invoke-static {p1}, Lbr/com/itau/security/securestorage/SecureStorage;->encrypt(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 259
    return-object p0
.end method

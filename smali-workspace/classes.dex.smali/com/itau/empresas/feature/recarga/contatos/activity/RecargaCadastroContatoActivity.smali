.class public Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity;
.super Lcom/itau/empresas/ui/activity/BaseActivity;
.source "RecargaCadastroContatoActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity$TIPOS;
    }
.end annotation


# instance fields
.field botaoAgenda:Landroid/widget/ImageView;

.field botaoExcluir:Landroid/widget/Button;

.field botaoFazerRecarga:Landroid/widget/Button;

.field botaoSalvar:Landroid/widget/Button;

.field campoApelido:Lbr/com/itau/widgets/material/MaterialEditText;

.field campoCelular:Lbr/com/itau/widgets/material/MaterialEditText;

.field contatoRecarga:Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;

.field contatoRecargaController:Lcom/itau/empresas/feature/recarga/contatos/controller/ContatoRecargaController;

.field llBotoesSalvarExcluir:Landroid/widget/LinearLayout;

.field recargaController:Lcom/itau/empresas/feature/recarga/efetivacao/controller/RecargaController;

.field root:Landroid/widget/LinearLayout;

.field spinnerOperadora:Lbr/com/itau/widgets/material/MaterialSpinner;

.field spinnerRegiao:Lbr/com/itau/widgets/material/MaterialSpinner;

.field textoMensagem:Landroid/widget/TextView;

.field tipo:Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity$TIPOS;

.field toolBar:Landroid/support/v7/widget/Toolbar;


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 67
    invoke-direct {p0}, Lcom/itau/empresas/ui/activity/BaseActivity;-><init>()V

    return-void
.end method

.method private constroiToolbar()V
    .registers 3

    .line 528
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity;->toolBar:Landroid/support/v7/widget/Toolbar;

    invoke-static {v0, p0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom;->Builder(Landroid/support/v7/widget/Toolbar;Landroid/support/v7/app/AppCompatActivity;)Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    move-result-object v0

    .line 530
    const v1, 0x7f0706b8

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->comTitulo(I)Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    move-result-object v0

    .line 531
    invoke-virtual {v0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->comClickListenerFinish()Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    move-result-object v0

    .line 532
    invoke-virtual {v0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->build()Lcom/itau/empresas/ui/util/listener/ToolbarCustom;

    .line 533
    return-void
.end method

.method private disparaEventoAnalytics()V
    .registers 5

    .line 132
    invoke-static {}, Lcom/itau/empresas/feature/analytics/AnalyticsHelper;->getInstance()Lcom/itau/empresas/feature/analytics/AnalyticsHelper;

    move-result-object v3

    .line 133
    .line 134
    .local v3, "analyticsHelper":Lcom/itau/empresas/feature/analytics/AnalyticsHelper;
    const v0, 0x7f0700ef

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 135
    const v1, 0x7f0700ce

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 136
    const v2, 0x7f0700c3

    invoke-virtual {p0, v2}, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 133
    invoke-virtual {v3, v0, v1, v2}, Lcom/itau/empresas/feature/analytics/AnalyticsHelper;->eventoAdobeState(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 138
    return-void
.end method

.method private hasRegiao()Z
    .registers 2

    .line 431
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity;->spinnerRegiao:Lbr/com/itau/widgets/material/MaterialSpinner;

    invoke-virtual {v0}, Lbr/com/itau/widgets/material/MaterialSpinner;->getVisibility()I

    move-result v0

    if-nez v0, :cond_a

    const/4 v0, 0x1

    goto :goto_b

    :cond_a
    const/4 v0, 0x0

    :goto_b
    return v0
.end method

.method public static intent(Landroid/content/Context;)Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity_$IntentBuilder_;
    .registers 2
    .param p0, "context"    # Landroid/content/Context;

    .line 536
    new-instance v0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity_$IntentBuilder_;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity_$IntentBuilder_;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method private irParaConfirmacao(ZLcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;)V
    .registers 6
    .param p1, "adicionado"    # Z
    .param p2, "contatoRecargaVO"    # Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;

    .line 453
    .line 454
    invoke-static {p0}, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaConfirmacaoCadastroContatoActivity_;->intent(Landroid/content/Context;)Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaConfirmacaoCadastroContatoActivity_$IntentBuilder_;

    move-result-object v0

    .line 455
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaConfirmacaoCadastroContatoActivity_$IntentBuilder_;->adicionado(Ljava/lang/Boolean;)Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaConfirmacaoCadastroContatoActivity_$IntentBuilder_;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity;->spinnerOperadora:Lbr/com/itau/widgets/material/MaterialSpinner;

    .line 456
    invoke-virtual {v1}, Lbr/com/itau/widgets/material/MaterialSpinner;->getSelectedItem()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/itau/empresas/feature/recarga/contatos/model/RecargaOperadoraVO;

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaConfirmacaoCadastroContatoActivity_$IntentBuilder_;->operadoraVO(Lcom/itau/empresas/feature/recarga/contatos/model/RecargaOperadoraVO;)Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaConfirmacaoCadastroContatoActivity_$IntentBuilder_;

    move-result-object v0

    .line 457
    invoke-virtual {v0, p2}, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaConfirmacaoCadastroContatoActivity_$IntentBuilder_;->contatoRecargaVO(Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;)Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaConfirmacaoCadastroContatoActivity_$IntentBuilder_;

    move-result-object v2

    .line 459
    .local v2, "builder":Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaConfirmacaoCadastroContatoActivity_$IntentBuilder_;
    invoke-direct {p0}, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity;->hasRegiao()Z

    move-result v0

    if-eqz v0, :cond_2d

    .line 460
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity;->spinnerRegiao:Lbr/com/itau/widgets/material/MaterialSpinner;

    invoke-virtual {v0}, Lbr/com/itau/widgets/material/MaterialSpinner;->getSelectedItem()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/feature/recarga/contatos/model/RecargaRegiaoVO;

    invoke-virtual {v2, v0}, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaConfirmacaoCadastroContatoActivity_$IntentBuilder_;->regiaoVO(Lcom/itau/empresas/feature/recarga/contatos/model/RecargaRegiaoVO;)Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaConfirmacaoCadastroContatoActivity_$IntentBuilder_;

    .line 463
    :cond_2d
    const/4 v0, 0x0

    invoke-virtual {v2, v0}, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaConfirmacaoCadastroContatoActivity_$IntentBuilder_;->startForResult(I)Lorg/androidannotations/api/builder/PostActivityStarter;

    .line 465
    return-void
.end method

.method private onSelecionarContatoResult(Landroid/content/Intent;)V
    .registers 12
    .param p1, "data"    # Landroid/content/Intent;

    .line 367
    const/4 v6, 0x0

    .line 369
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_1
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v7

    .line 370
    .local v7, "contactUri":Landroid/net/Uri;
    const/4 v0, 0x2

    new-array v8, v0, [Ljava/lang/String;

    const-string v0, "display_name"

    const/4 v1, 0x0

    aput-object v0, v8, v1

    const-string v0, "data1"

    const/4 v1, 0x1

    aput-object v0, v8, v1

    .line 374
    .local v8, "projection":[Ljava/lang/String;
    invoke-virtual {p0}, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    move-object v1, v7

    move-object v2, v8

    .line 375
    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    move-object v6, v0

    .line 376
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 377
    invoke-direct {p0, v6}, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity;->preencherContato(Landroid/database/Cursor;)V
    :try_end_26
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_26} :catch_2c
    .catchall {:try_start_1 .. :try_end_26} :catchall_3a

    .line 381
    .end local v7    # "contactUri":Landroid/net/Uri;
    .end local v8    # "projection":[Ljava/lang/String;
    if-eqz v6, :cond_41

    .line 382
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_41

    .line 378
    :catch_2c
    move-exception v7

    .line 379
    .local v7, "e":Ljava/lang/Exception;
    const-string v0, "RecargaCadastroContatoA"

    const-string v1, "onActivityResult: "

    :try_start_31
    invoke-static {v0, v1, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_34
    .catchall {:try_start_31 .. :try_end_34} :catchall_3a

    .line 381
    .end local v7    # "e":Ljava/lang/Exception;
    if-eqz v6, :cond_41

    .line 382
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_41

    .line 381
    :catchall_3a
    move-exception v9

    if-eqz v6, :cond_40

    .line 382
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_40
    throw v9

    .line 385
    :cond_41
    :goto_41
    return-void
.end method

.method private preencherContato(Landroid/database/Cursor;)V
    .registers 6
    .param p1, "cursor"    # Landroid/database/Cursor;

    .line 388
    const-string v0, "display_name"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    .line 389
    .local v1, "indexName":I
    const-string v0, "data1"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    .line 391
    .local v2, "indexNumber":I
    const/4 v0, -0x1

    if-eq v1, v0, :cond_18

    .line 392
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 393
    .local v3, "nomeContato":Ljava/lang/String;
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity;->campoApelido:Lbr/com/itau/widgets/material/MaterialEditText;

    invoke-virtual {v0, v3}, Lbr/com/itau/widgets/material/MaterialEditText;->setText(Ljava/lang/CharSequence;)V

    .line 396
    .end local v3    # "nomeContato":Ljava/lang/String;
    :cond_18
    const/4 v0, -0x1

    if-eq v2, v0, :cond_30

    .line 397
    const-string v0, "data1"

    .line 399
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 398
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 400
    .local v3, "numeroContato":Ljava/lang/String;
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity;->application:Lcom/itau/empresas/CustomApplication;

    invoke-static {v0, v3}, Lcom/itau/empresas/ui/util/ViewUtils;->ajustarNumeroAgenda(Lcom/itau/empresas/CustomApplication;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 402
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity;->campoCelular:Lbr/com/itau/widgets/material/MaterialEditText;

    invoke-virtual {v0, v3}, Lbr/com/itau/widgets/material/MaterialEditText;->setText(Ljava/lang/CharSequence;)V

    .line 404
    .end local v3    # "numeroContato":Ljava/lang/String;
    :cond_30
    return-void
.end method

.method private requestContato()V
    .registers 6

    .line 156
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity;->application:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getEventoTracker()Lcom/itau/empresas/ui/util/analytics/TrackerEvento;

    move-result-object v0

    new-instance v1, Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;

    .line 157
    const v2, 0x7f0702e7

    invoke-virtual {p0, v2}, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 158
    const v3, 0x7f07029a

    invoke-virtual {p0, v3}, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 156
    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/analytics/TrackerEvento;->enviarHitEvento(Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;)V

    .line 160
    new-instance v4, Landroid/content/Intent;

    const-string v0, "android.intent.action.PICK"

    sget-object v1, Landroid/provider/ContactsContract$Contacts;->CONTENT_URI:Landroid/net/Uri;

    invoke-direct {v4, v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 161
    .local v4, "intent":Landroid/content/Intent;
    const-string v0, "vnd.android.cursor.dir/phone_v2"

    invoke-virtual {v4, v0}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 162
    const/4 v0, 0x1

    invoke-virtual {p0, v4, v0}, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 163
    return-void
.end method

.method private validarCamposAdicionarContatos()Z
    .registers 4

    .line 407
    const/4 v2, 0x1

    .line 409
    .local v2, "retorno":Z
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity;->campoApelido:Lbr/com/itau/widgets/material/MaterialEditText;

    invoke-virtual {v0}, Lbr/com/itau/widgets/material/MaterialEditText;->validate()Z

    move-result v0

    if-nez v0, :cond_a

    .line 410
    const/4 v2, 0x0

    .line 412
    :cond_a
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity;->spinnerOperadora:Lbr/com/itau/widgets/material/MaterialSpinner;

    invoke-virtual {v0}, Lbr/com/itau/widgets/material/MaterialSpinner;->getSelectedItemPosition()I

    move-result v0

    const/4 v1, 0x1

    if-ge v0, v1, :cond_1c

    .line 413
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity;->spinnerOperadora:Lbr/com/itau/widgets/material/MaterialSpinner;

    const v1, 0x7f070653

    invoke-virtual {v0, v1}, Lbr/com/itau/widgets/material/MaterialSpinner;->setError(I)V

    .line 414
    const/4 v2, 0x0

    .line 418
    :cond_1c
    invoke-direct {p0}, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity;->hasRegiao()Z

    move-result v0

    if-eqz v0, :cond_33

    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity;->spinnerRegiao:Lbr/com/itau/widgets/material/MaterialSpinner;

    invoke-virtual {v0}, Lbr/com/itau/widgets/material/MaterialSpinner;->getSelectedItemPosition()I

    move-result v0

    if-nez v0, :cond_33

    .line 419
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity;->spinnerRegiao:Lbr/com/itau/widgets/material/MaterialSpinner;

    const v1, 0x7f070658

    invoke-virtual {v0, v1}, Lbr/com/itau/widgets/material/MaterialSpinner;->setError(I)V

    .line 420
    const/4 v2, 0x0

    .line 423
    :cond_33
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity;->campoCelular:Lbr/com/itau/widgets/material/MaterialEditText;

    invoke-virtual {v0}, Lbr/com/itau/widgets/material/MaterialEditText;->validate()Z

    move-result v0

    if-nez v0, :cond_3c

    .line 424
    const/4 v2, 0x0

    .line 427
    :cond_3c
    return v2
.end method


# virtual methods
.method aoAlterarNome()V
    .registers 3

    .line 177
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity;->botaoSalvar:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 178
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity;->botaoFazerRecarga:Landroid/widget/Button;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 179
    return-void
.end method

.method aoClicarAgenda()V
    .registers 3

    .line 143
    invoke-static {}, Lcom/itau/empresas/ui/util/PermissoesUtil;->getPermissionShowContatos()[Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/itau/empresas/ui/util/PermissoesUtil;->hasSelfPermissions(Landroid/content/Context;[Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 144
    invoke-direct {p0}, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity;->requestContato()V

    goto :goto_1c

    .line 147
    :cond_e
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x17

    if-lt v0, v1, :cond_1c

    .line 148
    invoke-static {}, Lcom/itau/empresas/ui/util/PermissoesUtil;->getPermissionShowContatos()[Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity;->requestPermissions([Ljava/lang/String;I)V

    .line 153
    :cond_1c
    :goto_1c
    return-void
.end method

.method aoClicarExcluir()V
    .registers 4

    .line 225
    new-instance v0, Landroid/support/v7/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/support/v7/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Tem certeza que deseja excluir "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity;->contatoRecarga:Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;

    .line 226
    invoke-virtual {v2}, Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;->getApelido()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "?"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v0

    const-string v1, "sim"

    new-instance v2, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity$1;

    invoke-direct {v2, p0}, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity$1;-><init>(Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity;)V

    .line 227
    invoke-virtual {v0, v1, v2}, Landroid/support/v7/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v0

    const-string v1, "n\u00e3o"

    .line 235
    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/support/v7/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v0

    .line 236
    invoke-virtual {v0}, Landroid/support/v7/app/AlertDialog$Builder;->show()Landroid/support/v7/app/AlertDialog;

    .line 237
    return-void
.end method

.method aoClicarFazerRecarga()V
    .registers 6

    .line 299
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity;->application:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getEventoTracker()Lcom/itau/empresas/ui/util/analytics/TrackerEvento;

    move-result-object v0

    new-instance v1, Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;

    .line 300
    const v2, 0x7f0702e7

    invoke-virtual {p0, v2}, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 301
    const v3, 0x7f0702bc

    invoke-virtual {p0, v3}, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 299
    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/analytics/TrackerEvento;->enviarHitEvento(Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;)V

    .line 303
    new-instance v4, Landroid/content/Intent;

    invoke-direct {v4}, Landroid/content/Intent;-><init>()V

    .line 304
    .local v4, "data":Landroid/content/Intent;
    const-string v0, "contato"

    iget-object v1, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity;->contatoRecarga:Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;

    invoke-virtual {v4, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 305
    const/4 v0, -0x1

    invoke-virtual {p0, v0, v4}, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity;->setResult(ILandroid/content/Intent;)V

    .line 306
    invoke-virtual {p0}, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity;->finish()V

    .line 308
    return-void
.end method

.method aoClicarSalvar()V
    .registers 10

    .line 242
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity;->tipo:Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity$TIPOS;

    sget-object v1, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity$TIPOS;->NOVO_CONTATO:Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity$TIPOS;

    if-ne v0, v1, :cond_c2

    .line 243
    invoke-direct {p0}, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity;->validarCamposAdicionarContatos()Z

    move-result v0

    if-eqz v0, :cond_11e

    .line 244
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity;->application:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getEventoTracker()Lcom/itau/empresas/ui/util/analytics/TrackerEvento;

    move-result-object v0

    new-instance v1, Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;

    .line 245
    const v2, 0x7f0702e7

    invoke-virtual {p0, v2}, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 246
    const v3, 0x7f070295

    invoke-virtual {p0, v3}, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 244
    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/analytics/TrackerEvento;->enviarHitEvento(Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;)V

    .line 248
    new-instance v4, Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaInputVO;

    invoke-direct {v4}, Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaInputVO;-><init>()V

    .line 249
    .local v4, "contatoRecargaInputVO":Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaInputVO;
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity;->spinnerOperadora:Lbr/com/itau/widgets/material/MaterialSpinner;

    .line 250
    invoke-virtual {v0}, Lbr/com/itau/widgets/material/MaterialSpinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity;->spinnerOperadora:Lbr/com/itau/widgets/material/MaterialSpinner;

    .line 251
    invoke-virtual {v1}, Lbr/com/itau/widgets/material/MaterialSpinner;->getSelectedItemPosition()I

    move-result v1

    invoke-interface {v0, v1}, Landroid/widget/SpinnerAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/itau/empresas/feature/recarga/contatos/model/RecargaOperadoraVO;

    .line 253
    .local v5, "recargaOperadoraVO":Lcom/itau/empresas/feature/recarga/contatos/model/RecargaOperadoraVO;
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity;->campoApelido:Lbr/com/itau/widgets/material/MaterialEditText;

    invoke-virtual {v0}, Lbr/com/itau/widgets/material/MaterialEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaInputVO;->setApelido(Ljava/lang/String;)V

    .line 255
    invoke-direct {p0}, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity;->hasRegiao()Z

    move-result v0

    if-eqz v0, :cond_6c

    .line 256
    .line 257
    invoke-virtual {v5}, Lcom/itau/empresas/feature/recarga/contatos/model/RecargaOperadoraVO;->getListaRegiao()Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity;->spinnerRegiao:Lbr/com/itau/widgets/material/MaterialSpinner;

    .line 258
    invoke-virtual {v1}, Lbr/com/itau/widgets/material/MaterialSpinner;->getSelectedItemPosition()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/feature/recarga/contatos/model/RecargaRegiaoVO;

    .line 259
    invoke-virtual {v0}, Lcom/itau/empresas/feature/recarga/contatos/model/RecargaRegiaoVO;->getRegiao()Ljava/lang/String;

    move-result-object v0

    .line 256
    invoke-virtual {v4, v0}, Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaInputVO;->setRegiao(Ljava/lang/String;)V

    .line 262
    :cond_6c
    invoke-virtual {v5}, Lcom/itau/empresas/feature/recarga/contatos/model/RecargaOperadoraVO;->getNomeOperadora()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaInputVO;->setNomeOperadora(Ljava/lang/String;)V

    .line 263
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity;->campoCelular:Lbr/com/itau/widgets/material/MaterialEditText;

    .line 264
    invoke-virtual {v0}, Lbr/com/itau/widgets/material/MaterialEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/itau/empresas/ui/util/MascaraEditText;->removeMascara(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 265
    .local v6, "celularSemMascara":Ljava/lang/String;
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {v6, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    .line 266
    .local v7, "ddd":Ljava/lang/String;
    const/4 v0, 0x2

    invoke-virtual {v6, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v8

    .line 268
    .local v8, "telefone":Ljava/lang/String;
    invoke-virtual {v4, v7}, Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaInputVO;->setDdd(Ljava/lang/String;)V

    .line 269
    invoke-virtual {v4, v8}, Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaInputVO;->setTelefone(Ljava/lang/String;)V

    .line 271
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity;->application:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getContaSelecionada()Lcom/itau/empresas/api/model/ContaOperadorVO;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/api/model/ContaOperadorVO;->getAgencia()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaInputVO;->setAgencia(Ljava/lang/String;)V

    .line 272
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity;->application:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getContaSelecionada()Lcom/itau/empresas/api/model/ContaOperadorVO;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/api/model/ContaOperadorVO;->getConta()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaInputVO;->setConta(Ljava/lang/String;)V

    .line 273
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity;->application:Lcom/itau/empresas/CustomApplication;

    .line 274
    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getContaSelecionada()Lcom/itau/empresas/api/model/ContaOperadorVO;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/api/model/ContaOperadorVO;->getDac()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaInputVO;->setDigitoVerificadorConta(Ljava/lang/String;)V

    .line 275
    invoke-virtual {p0}, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity;->mostraDialogoDeProgresso()V

    .line 276
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity;->contatoRecargaController:Lcom/itau/empresas/feature/recarga/contatos/controller/ContatoRecargaController;

    invoke-virtual {v0, v4}, Lcom/itau/empresas/feature/recarga/contatos/controller/ContatoRecargaController;->insereContatoRecarga(Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaInputVO;)V

    .line 277
    .end local v4    # "contatoRecargaInputVO":Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaInputVO;
    .end local v5    # "recargaOperadoraVO":Lcom/itau/empresas/feature/recarga/contatos/model/RecargaOperadoraVO;
    .end local v6    # "celularSemMascara":Ljava/lang/String;
    .end local v7    # "ddd":Ljava/lang/String;
    .end local v8    # "telefone":Ljava/lang/String;
    goto :goto_11e

    .line 279
    :cond_c2
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity;->tipo:Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity$TIPOS;

    sget-object v1, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity$TIPOS;->EDITAR_CONTATO:Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity$TIPOS;

    if-ne v0, v1, :cond_11e

    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity;->campoApelido:Lbr/com/itau/widgets/material/MaterialEditText;

    invoke-virtual {v0}, Lbr/com/itau/widgets/material/MaterialEditText;->validate()Z

    move-result v0

    if-eqz v0, :cond_11e

    .line 281
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity;->application:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getEventoTracker()Lcom/itau/empresas/ui/util/analytics/TrackerEvento;

    move-result-object v0

    new-instance v1, Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;

    .line 282
    const v2, 0x7f0702e7

    invoke-virtual {p0, v2}, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 283
    const v3, 0x7f0702a5

    invoke-virtual {p0, v3}, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 281
    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/analytics/TrackerEvento;->enviarHitEvento(Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;)V

    .line 285
    new-instance v4, Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaApelidoInputVO;

    invoke-direct {v4}, Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaApelidoInputVO;-><init>()V

    .line 286
    .local v4, "contatoRecargaApelidoInputVO":Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaApelidoInputVO;
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity;->campoApelido:Lbr/com/itau/widgets/material/MaterialEditText;

    invoke-virtual {v0}, Lbr/com/itau/widgets/material/MaterialEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaApelidoInputVO;->setApelido(Ljava/lang/String;)V

    .line 287
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity;->contatoRecarga:Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;->getNomeOperadora()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaApelidoInputVO;->setNomeOperadora(Ljava/lang/String;)V

    .line 288
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity;->contatoRecarga:Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;

    .line 289
    invoke-virtual {v0}, Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;->getEstadoOperadora()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaApelidoInputVO;->setEstadoOperadora(Ljava/lang/String;)V

    .line 290
    invoke-virtual {p0}, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity;->mostraDialogoDeProgresso()V

    .line 291
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity;->contatoRecargaController:Lcom/itau/empresas/feature/recarga/contatos/controller/ContatoRecargaController;

    iget-object v1, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity;->contatoRecarga:Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;

    invoke-virtual {v1}, Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;->getIdContato()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v4}, Lcom/itau/empresas/feature/recarga/contatos/controller/ContatoRecargaController;->alteraApelidoContatoRecarga(Ljava/lang/String;Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaApelidoInputVO;)V

    .line 295
    .end local v4    # "contatoRecargaApelidoInputVO":Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaApelidoInputVO;
    :cond_11e
    :goto_11e
    return-void
.end method

.method aoSelecionarOperadora(ZI)V
    .registers 8
    .param p1, "selected"    # Z
    .param p2, "index"    # I

    .line 184
    const-string v0, "input_method"

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Landroid/view/inputmethod/InputMethodManager;

    .line 186
    .local v2, "inputManager":Landroid/view/inputmethod/InputMethodManager;
    invoke-virtual {p0}, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity;->getCurrentFocus()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v2, v0, v1}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 189
    const/4 v0, -0x1

    if-le p2, v0, :cond_84

    if-eqz p1, :cond_84

    .line 190
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity;->spinnerOperadora:Lbr/com/itau/widgets/material/MaterialSpinner;

    invoke-virtual {v0}, Lbr/com/itau/widgets/material/MaterialSpinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    add-int/lit8 v1, p2, 0x1

    .line 191
    invoke-interface {v0, v1}, Landroid/widget/SpinnerAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/itau/empresas/feature/recarga/contatos/model/RecargaOperadoraVO;

    .line 193
    .local v3, "item":Lcom/itau/empresas/feature/recarga/contatos/model/RecargaOperadoraVO;
    invoke-virtual {v3}, Lcom/itau/empresas/feature/recarga/contatos/model/RecargaOperadoraVO;->getListaRegiao()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-gt v0, v1, :cond_3c

    .line 194
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity;->spinnerRegiao:Lbr/com/itau/widgets/material/MaterialSpinner;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lbr/com/itau/widgets/material/MaterialSpinner;->setVisibility(I)V

    .line 195
    return-void

    .line 198
    :cond_3c
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity;->spinnerRegiao:Lbr/com/itau/widgets/material/MaterialSpinner;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lbr/com/itau/widgets/material/MaterialSpinner;->setVisibility(I)V

    .line 199
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity;->spinnerRegiao:Lbr/com/itau/widgets/material/MaterialSpinner;

    invoke-virtual {v3}, Lcom/itau/empresas/feature/recarga/contatos/model/RecargaOperadoraVO;->getListaRegiao()Ljava/util/List;

    move-result-object v1

    invoke-static {p0, v0, v1}, Lcom/itau/empresas/ui/util/ViewUtils;->inicializarSpinnerOrdenado(Landroid/content/Context;Landroid/widget/Spinner;Ljava/util/List;)V

    .line 201
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity;->spinnerRegiao:Lbr/com/itau/widgets/material/MaterialSpinner;

    invoke-virtual {v0}, Lbr/com/itau/widgets/material/MaterialSpinner;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_54

    .line 202
    return-void

    .line 205
    :cond_54
    const/4 v4, 0x0

    .local v4, "count":I
    :goto_55
    invoke-virtual {v3}, Lcom/itau/empresas/feature/recarga/contatos/model/RecargaOperadoraVO;->getListaRegiao()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v4, v0, :cond_84

    .line 206
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity;->contatoRecarga:Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;->getEstadoOperadora()Ljava/lang/String;

    move-result-object v0

    .line 207
    invoke-virtual {v3}, Lcom/itau/empresas/feature/recarga/contatos/model/RecargaOperadoraVO;->getListaRegiao()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/itau/empresas/feature/recarga/contatos/model/RecargaRegiaoVO;

    invoke-virtual {v1}, Lcom/itau/empresas/feature/recarga/contatos/model/RecargaRegiaoVO;->getRegiao()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_81

    .line 208
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity;->spinnerRegiao:Lbr/com/itau/widgets/material/MaterialSpinner;

    add-int/lit8 v1, v4, 0x1

    invoke-virtual {v0, v1}, Lbr/com/itau/widgets/material/MaterialSpinner;->setSelection(I)V

    .line 209
    goto :goto_84

    .line 205
    :cond_81
    add-int/lit8 v4, v4, 0x1

    goto :goto_55

    .line 213
    .end local v3    # "item":Lcom/itau/empresas/feature/recarga/contatos/model/RecargaOperadoraVO;
    .end local v4    # "count":I
    :cond_84
    :goto_84
    return-void
.end method

.method aoSelecionarRegiao(ZI)V
    .registers 6
    .param p1, "selected"    # Z
    .param p2, "index"    # I

    .line 217
    const-string v0, "input_method"

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Landroid/view/inputmethod/InputMethodManager;

    .line 219
    .local v2, "inputManager":Landroid/view/inputmethod/InputMethodManager;
    invoke-virtual {p0}, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity;->getCurrentFocus()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v2, v0, v1}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 221
    return-void
.end method

.method carregaElementosDaTela()V
    .registers 7

    .line 93
    invoke-direct {p0}, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity;->constroiToolbar()V

    .line 94
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity;->recargaController:Lcom/itau/empresas/feature/recarga/efetivacao/controller/RecargaController;

    const-string v1, "recargas"

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/recarga/efetivacao/controller/RecargaController;->buscaOperadorasRecarga(Ljava/lang/String;)V

    .line 96
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity;->campoCelular:Lbr/com/itau/widgets/material/MaterialEditText;

    new-instance v1, Lbr/com/itau/widgets/material/validation/RegexpValidator;

    .line 97
    const v2, 0x7f070649

    invoke-virtual {p0, v2}, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "^\\(\\d{2}\\) \\d{4,5}-\\d{4}$"

    invoke-direct {v1, v2, v3}, Lbr/com/itau/widgets/material/validation/RegexpValidator;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 96
    invoke-virtual {v0, v1}, Lbr/com/itau/widgets/material/MaterialEditText;->addValidator(Lbr/com/itau/widgets/material/validation/METValidator;)Lbr/com/itau/widgets/material/MaterialEditText;

    .line 99
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity;->campoCelular:Lbr/com/itau/widgets/material/MaterialEditText;

    invoke-static {v0}, Lcom/itau/empresas/ui/util/MascaraCelular;->insere(Landroid/widget/EditText;)Landroid/text/TextWatcher;

    move-result-object v4

    .line 100
    .local v4, "mascara":Landroid/text/TextWatcher;
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity;->campoCelular:Lbr/com/itau/widgets/material/MaterialEditText;

    invoke-virtual {v0, v4}, Lbr/com/itau/widgets/material/MaterialEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 102
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity;->campoApelido:Lbr/com/itau/widgets/material/MaterialEditText;

    new-instance v1, Lbr/com/itau/widgets/material/validation/RegexpValidator;

    const-string v2, "\u00e9 necess\u00e1rio preencher o nome!"

    const-string v3, ".+"

    invoke-direct {v1, v2, v3}, Lbr/com/itau/widgets/material/validation/RegexpValidator;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lbr/com/itau/widgets/material/MaterialEditText;->addValidator(Lbr/com/itau/widgets/material/validation/METValidator;)Lbr/com/itau/widgets/material/MaterialEditText;

    .line 104
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity;->tipo:Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity$TIPOS;

    sget-object v1, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity$TIPOS;->EDITAR_CONTATO:Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity$TIPOS;

    if-ne v0, v1, :cond_4f

    .line 105
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity;->llBotoesSalvarExcluir:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 106
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity;->botaoExcluir:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 107
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity;->textoMensagem:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_7f

    .line 109
    :cond_4f
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity;->tipo:Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity$TIPOS;

    sget-object v1, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity$TIPOS;->NOVO_CONTATO:Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity$TIPOS;

    if-ne v0, v1, :cond_6a

    .line 110
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity;->llBotoesSalvarExcluir:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 111
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity;->botaoExcluir:Landroid/widget/Button;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 112
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity;->textoMensagem:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_7f

    .line 115
    :cond_6a
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity;->llBotoesSalvarExcluir:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 116
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity;->botaoExcluir:Landroid/widget/Button;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 117
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity;->textoMensagem:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 120
    :goto_7f
    const-string v0, "accessibility"

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Landroid/view/accessibility/AccessibilityManager;

    .line 123
    .local v5, "manager":Landroid/view/accessibility/AccessibilityManager;
    invoke-virtual {v5}, Landroid/view/accessibility/AccessibilityManager;->isTouchExplorationEnabled()Z

    move-result v0

    if-nez v0, :cond_94

    invoke-virtual {v5}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_a0

    .line 124
    :cond_94
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity;->campoCelular:Lbr/com/itau/widgets/material/MaterialEditText;

    const v1, 0x7f0704ed

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbr/com/itau/widgets/material/MaterialEditText;->setHint(Ljava/lang/CharSequence;)V

    .line 127
    :cond_a0
    invoke-direct {p0}, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity;->disparaEventoAnalytics()V

    .line 129
    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .registers 5
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .line 351
    const/4 v0, 0x1

    if-ne p1, v0, :cond_a

    const/4 v0, -0x1

    if-ne p2, v0, :cond_a

    .line 353
    invoke-direct {p0, p3}, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity;->onSelecionarContatoResult(Landroid/content/Intent;)V

    goto :goto_24

    .line 355
    :cond_a
    if-nez p1, :cond_21

    const/4 v0, -0x1

    if-ne p2, v0, :cond_21

    if-eqz p3, :cond_21

    const-string v0, "contato"

    .line 357
    invoke-virtual {p3, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_21

    .line 358
    const/4 v0, -0x1

    invoke-virtual {p0, v0, p3}, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity;->setResult(ILandroid/content/Intent;)V

    .line 359
    invoke-virtual {p0}, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity;->finish()V

    goto :goto_24

    .line 362
    :cond_21
    invoke-super {p0, p1, p2, p3}, Lcom/itau/empresas/ui/activity/BaseActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 364
    :goto_24
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .registers 6
    .param p1, "menu"    # Landroid/view/Menu;

    .line 505
    invoke-static {}, Lcom/itau/empresas/ui/util/PoyntUtils;->isPoynt()Z

    move-result v0

    if-eqz v0, :cond_26

    .line 506
    invoke-virtual {p0}, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f0f0005

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 508
    const v0, 0x7f0e06c6

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    .line 509
    .local v2, "itemMenuPesquisa":Landroid/view/MenuItem;
    const/4 v0, 0x0

    invoke-interface {v2, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 511
    const v0, 0x7f0e06c5

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v3

    .line 512
    .local v3, "itemMenuFiltro":Landroid/view/MenuItem;
    const/4 v0, 0x0

    invoke-interface {v3, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 515
    .end local v2    # "itemMenuPesquisa":Landroid/view/MenuItem;
    .end local v3    # "itemMenuFiltro":Landroid/view/MenuItem;
    :cond_26
    const/4 v0, 0x1

    return v0
.end method

.method public onEventMainThread(Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent$Conversion;)V
    .registers 5
    .param p1, "e"    # Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent$Conversion;

    .line 468
    invoke-static {p1, p0}, Lcom/itau/empresas/api/ApiHelper;->devoTratarEvento(Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;Lcom/itau/empresas/api/ApiConsumidor;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 469
    return-void

    .line 472
    :cond_7
    invoke-virtual {p0}, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity;->escondeDialogoDeProgresso()V

    .line 473
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity;->root:Landroid/widget/LinearLayout;

    const v1, 0x7f0701f2

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lbr/com/itau/topsnackbar/TopSnackbar;->make(Landroid/view/ViewGroup;II)Lbr/com/itau/topsnackbar/TopSnackbar;

    move-result-object v0

    .line 474
    invoke-virtual {v0}, Lbr/com/itau/topsnackbar/TopSnackbar;->show()Lbr/com/itau/topsnackbar/TopSnackbar;

    .line 475
    return-void
.end method

.method public onEventMainThread(Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;)V
    .registers 5
    .param p1, "e"    # Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;

    .line 479
    invoke-static {p1, p0}, Lcom/itau/empresas/api/ApiHelper;->devoTratarEvento(Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;Lcom/itau/empresas/api/ApiConsumidor;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 480
    return-void

    .line 483
    :cond_7
    invoke-virtual {p0}, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity;->escondeDialogoDeProgresso()V

    .line 484
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity;->root:Landroid/widget/LinearLayout;

    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;->getException()Lbr/com/itau/sdk/android/core/exception/BackendException;

    move-result-object v1

    invoke-virtual {v1}, Lbr/com/itau/sdk/android/core/exception/BackendException;->getError()Lbr/com/itau/sdk/android/core/model/ErrorDTO;

    move-result-object v1

    invoke-virtual {v1}, Lbr/com/itau/sdk/android/core/model/ErrorDTO;->getMensagem()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lbr/com/itau/topsnackbar/TopSnackbar;->make(Landroid/view/ViewGroup;Ljava/lang/CharSequence;I)Lbr/com/itau/topsnackbar/TopSnackbar;

    move-result-object v0

    .line 485
    invoke-virtual {v0}, Lbr/com/itau/topsnackbar/TopSnackbar;->show()Lbr/com/itau/topsnackbar/TopSnackbar;

    .line 486
    return-void
.end method

.method public onEventMainThread(Lcom/itau/empresas/Evento$EventoApelidoContatoRecargaAlterado;)V
    .registers 4
    .param p1, "eventoApelidoContatoRecargaAlterado"    # Lcom/itau/empresas/Evento$EventoApelidoContatoRecargaAlterado;

    .line 442
    invoke-virtual {p0}, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity;->escondeDialogoDeProgresso()V

    .line 443
    invoke-virtual {p1}, Lcom/itau/empresas/Evento$EventoApelidoContatoRecargaAlterado;->getContatoRecargaVO()Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, v1, v0}, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity;->irParaConfirmacao(ZLcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;)V

    .line 444
    return-void
.end method

.method public onEventMainThread(Lcom/itau/empresas/Evento$EventoContatoRecargaExcluido;)V
    .registers 3
    .param p1, "eventoContatoRecargaExcluido"    # Lcom/itau/empresas/Evento$EventoContatoRecargaExcluido;

    .line 448
    invoke-virtual {p0}, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity;->escondeDialogoDeProgresso()V

    .line 449
    invoke-static {p0}, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaContatosActivity;->intent(Landroid/content/Context;)Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaContatosActivity_$IntentBuilder_;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaContatosActivity_$IntentBuilder_;->start()Lorg/androidannotations/api/builder/PostActivityStarter;

    .line 450
    return-void
.end method

.method public onEventMainThread(Lcom/itau/empresas/Evento$EventoContatoRecargaInserido;)V
    .registers 4
    .param p1, "eventoContatoRecargaInserido"    # Lcom/itau/empresas/Evento$EventoContatoRecargaInserido;

    .line 436
    invoke-virtual {p0}, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity;->escondeDialogoDeProgresso()V

    .line 437
    invoke-virtual {p1}, Lcom/itau/empresas/Evento$EventoContatoRecargaInserido;->getContatoRecargaVO()Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;

    move-result-object v0

    const/4 v1, 0x1

    invoke-direct {p0, v1, v0}, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity;->irParaConfirmacao(ZLcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;)V

    .line 438
    return-void
.end method

.method public onEventMainThread(Ljava/util/List;)V
    .registers 6
    .param p1, "operadoras"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<Lcom/itau/empresas/feature/recarga/contatos/model/RecargaOperadoraVO;>;)V"
        }
    .end annotation

    .line 311
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity;->spinnerOperadora:Lbr/com/itau/widgets/material/MaterialSpinner;

    invoke-static {p0, v0, p1}, Lcom/itau/empresas/ui/util/ViewUtils;->inicializarSpinnerOrdenado(Landroid/content/Context;Landroid/widget/Spinner;Ljava/util/List;)V

    .line 313
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity;->contatoRecarga:Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;

    if-eqz v0, :cond_77

    .line 314
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity;->campoApelido:Lbr/com/itau/widgets/material/MaterialEditText;

    iget-object v1, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity;->contatoRecarga:Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;

    invoke-virtual {v1}, Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;->getApelido()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbr/com/itau/widgets/material/MaterialEditText;->setText(Ljava/lang/CharSequence;)V

    .line 315
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity;->campoCelular:Lbr/com/itau/widgets/material/MaterialEditText;

    iget-object v1, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity;->contatoRecarga:Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;

    .line 316
    invoke-virtual {v1}, Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;->getDdd()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity;->contatoRecarga:Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;

    .line 317
    invoke-virtual {v2}, Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;->getTelefone()Ljava/lang/String;

    move-result-object v2

    .line 316
    invoke-static {p0, v1, v2}, Lcom/itau/empresas/ui/util/ViewUtils;->formataNumeroTelefonico(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 315
    invoke-virtual {v0, v1}, Lbr/com/itau/widgets/material/MaterialEditText;->setText(Ljava/lang/CharSequence;)V

    .line 318
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity;->campoCelular:Lbr/com/itau/widgets/material/MaterialEditText;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lbr/com/itau/widgets/material/MaterialEditText;->setEnabled(Z)V

    .line 319
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity;->spinnerOperadora:Lbr/com/itau/widgets/material/MaterialSpinner;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lbr/com/itau/widgets/material/MaterialSpinner;->setEnabled(Z)V

    .line 320
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity;->spinnerRegiao:Lbr/com/itau/widgets/material/MaterialSpinner;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lbr/com/itau/widgets/material/MaterialSpinner;->setEnabled(Z)V

    .line 321
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity;->botaoAgenda:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 323
    const/4 v3, 0x0

    .local v3, "count":I
    :goto_42
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v3, v0, :cond_69

    .line 324
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity;->contatoRecarga:Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;->getNomeOperadora()Ljava/lang/String;

    move-result-object v0

    .line 325
    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/itau/empresas/feature/recarga/contatos/model/RecargaOperadoraVO;

    invoke-virtual {v1}, Lcom/itau/empresas/feature/recarga/contatos/model/RecargaOperadoraVO;->getNomeOperadora()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_66

    .line 326
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity;->spinnerOperadora:Lbr/com/itau/widgets/material/MaterialSpinner;

    add-int/lit8 v1, v3, 0x1

    invoke-virtual {v0, v1}, Lbr/com/itau/widgets/material/MaterialSpinner;->setSelection(I)V

    .line 327
    goto :goto_69

    .line 323
    :cond_66
    add-int/lit8 v3, v3, 0x1

    goto :goto_42

    .line 331
    .end local v3    # "count":I
    :cond_69
    :goto_69
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity;->botaoSalvar:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 332
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity;->botaoFazerRecarga:Landroid/widget/Button;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_a3

    .line 335
    :cond_77
    new-instance v0, Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;

    invoke-direct {v0}, Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity;->contatoRecarga:Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;

    .line 337
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity;->campoCelular:Lbr/com/itau/widgets/material/MaterialEditText;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lbr/com/itau/widgets/material/MaterialEditText;->setEnabled(Z)V

    .line 338
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity;->spinnerOperadora:Lbr/com/itau/widgets/material/MaterialSpinner;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lbr/com/itau/widgets/material/MaterialSpinner;->setEnabled(Z)V

    .line 339
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity;->spinnerRegiao:Lbr/com/itau/widgets/material/MaterialSpinner;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lbr/com/itau/widgets/material/MaterialSpinner;->setEnabled(Z)V

    .line 340
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity;->botaoAgenda:Landroid/widget/ImageView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 342
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity;->botaoSalvar:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 343
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity;->botaoFazerRecarga:Landroid/widget/Button;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 346
    :goto_a3
    invoke-virtual {p0}, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity;->escondeDialogoDeProgresso()V

    .line 347
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .registers 4
    .param p1, "item"    # Landroid/view/MenuItem;

    .line 520
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x7f0e06cf

    if-ne v0, v1, :cond_10

    .line 521
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity;->application:Lcom/itau/empresas/CustomApplication;

    invoke-static {v0, p0}, Lcom/itau/empresas/ui/util/ViewUtils;->aoClicarBotaoSairMenu(Lcom/itau/empresas/CustomApplication;Lcom/itau/empresas/ui/activity/BaseActivity;)V

    .line 522
    const/4 v0, 0x1

    return v0

    .line 524
    :cond_10
    invoke-super {p0, p1}, Lcom/itau/empresas/ui/activity/BaseActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0
.end method

.method public onRequestPermissionsResult(I[Ljava/lang/String;[I)V
    .registers 5
    .param p1, "requestCode"    # I
    .param p2, "permissions"    # [Ljava/lang/String;
    .param p3, "grantResults"    # [I

    .line 168
    invoke-super {p0, p1, p2, p3}, Lcom/itau/empresas/ui/activity/BaseActivity;->onRequestPermissionsResult(I[Ljava/lang/String;[I)V

    .line 169
    const/4 v0, 0x1

    if-ne p1, v0, :cond_13

    .line 170
    invoke-static {}, Lcom/itau/empresas/ui/util/PermissoesUtil;->getPermissionShowContatos()[Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/itau/empresas/ui/util/PermissoesUtil;->hasSelfPermissions(Landroid/content/Context;[Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 171
    invoke-direct {p0}, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity;->requestContato()V

    .line 173
    :cond_13
    return-void
.end method

.method public opkeysConsumidas()Ljava/util/List;
    .registers 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Ljava/lang/String;>;"
        }
    .end annotation

    .line 490
    const/16 v0, 0x9

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "listaOperadoras"

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const-string v1, "contatosRecarga"

    const/4 v2, 0x1

    aput-object v1, v0, v2

    const-string v1, "cadastrarContatoRecarga"

    const/4 v2, 0x2

    aput-object v1, v0, v2

    const-string v1, "alterarContatoRecarga"

    const/4 v2, 0x3

    aput-object v1, v0, v2

    const-string v1, "deletarContatoRecarga"

    const/4 v2, 0x4

    aput-object v1, v0, v2

    const-string v1, "valoresOperadora"

    const/4 v2, 0x5

    aput-object v1, v0, v2

    const-string v1, "simularRecarga"

    const/4 v2, 0x6

    aput-object v1, v0, v2

    const-string v1, "exibicao_recarga_autorizar"

    const/4 v2, 0x7

    aput-object v1, v0, v2

    const-string v1, "efetivarRecarga"

    const/16 v2, 0x8

    aput-object v1, v0, v2

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

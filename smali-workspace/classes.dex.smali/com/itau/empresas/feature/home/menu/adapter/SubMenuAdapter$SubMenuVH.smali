.class final Lcom/itau/empresas/feature/home/menu/adapter/SubMenuAdapter$SubMenuVH;
.super Landroid/support/v7/widget/RecyclerView$ViewHolder;
.source "SubMenuAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/itau/empresas/feature/home/menu/adapter/SubMenuAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x10
    name = "SubMenuVH"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/itau/empresas/feature/home/menu/adapter/SubMenuAdapter;

.field public final titulo:Landroid/widget/TextView;

.field private final viewHolderClickListener:Lcom/itau/empresas/feature/home/menu/adapter/ViewHolderClickListener;


# direct methods
.method private constructor <init>(Lcom/itau/empresas/feature/home/menu/adapter/SubMenuAdapter;Landroid/view/View;Lcom/itau/empresas/feature/home/menu/adapter/ViewHolderClickListener;)V
    .registers 5
    .param p1, "this$0"    # Lcom/itau/empresas/feature/home/menu/adapter/SubMenuAdapter;
    .param p2, "itemView"    # Landroid/view/View;
    .param p3, "listner"    # Lcom/itau/empresas/feature/home/menu/adapter/ViewHolderClickListener;

    .line 69
    iput-object p1, p0, Lcom/itau/empresas/feature/home/menu/adapter/SubMenuAdapter$SubMenuVH;->this$0:Lcom/itau/empresas/feature/home/menu/adapter/SubMenuAdapter;

    .line 70
    invoke-direct {p0, p2}, Landroid/support/v7/widget/RecyclerView$ViewHolder;-><init>(Landroid/view/View;)V

    .line 71
    iput-object p3, p0, Lcom/itau/empresas/feature/home/menu/adapter/SubMenuAdapter$SubMenuVH;->viewHolderClickListener:Lcom/itau/empresas/feature/home/menu/adapter/ViewHolderClickListener;

    .line 72
    invoke-virtual {p2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 73
    const v0, 0x7f0e0568

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/home/menu/adapter/SubMenuAdapter$SubMenuVH;->titulo:Landroid/widget/TextView;

    .line 74
    return-void
.end method

.method synthetic constructor <init>(Lcom/itau/empresas/feature/home/menu/adapter/SubMenuAdapter;Landroid/view/View;Lcom/itau/empresas/feature/home/menu/adapter/ViewHolderClickListener;Lcom/itau/empresas/feature/home/menu/adapter/SubMenuAdapter$1;)V
    .registers 5
    .param p1, "x0"    # Lcom/itau/empresas/feature/home/menu/adapter/SubMenuAdapter;
    .param p2, "x1"    # Landroid/view/View;
    .param p3, "x2"    # Lcom/itau/empresas/feature/home/menu/adapter/ViewHolderClickListener;
    .param p4, "x3"    # Lcom/itau/empresas/feature/home/menu/adapter/SubMenuAdapter$1;

    .line 64
    invoke-direct {p0, p1, p2, p3}, Lcom/itau/empresas/feature/home/menu/adapter/SubMenuAdapter$SubMenuVH;-><init>(Lcom/itau/empresas/feature/home/menu/adapter/SubMenuAdapter;Landroid/view/View;Lcom/itau/empresas/feature/home/menu/adapter/ViewHolderClickListener;)V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .registers 4
    .param p1, "v"    # Landroid/view/View;

    .line 78
    iget-object v0, p0, Lcom/itau/empresas/feature/home/menu/adapter/SubMenuAdapter$SubMenuVH;->viewHolderClickListener:Lcom/itau/empresas/feature/home/menu/adapter/ViewHolderClickListener;

    invoke-virtual {p0}, Lcom/itau/empresas/feature/home/menu/adapter/SubMenuAdapter$SubMenuVH;->getAdapterPosition()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/itau/empresas/feature/home/menu/adapter/ViewHolderClickListener;->clicadoNaPosicao(I)V

    .line 79
    return-void
.end method

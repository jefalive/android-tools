.class public Lbr/com/itau/sdk/android/core/model/VersionControlDTO;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private message:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "message"
    .end annotation
.end field

.field private type:Lbr/com/itau/sdk/android/core/model/VersionControlType;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "type"
    .end annotation
.end field

.field private url:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "url"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getMessage()Ljava/lang/String;
    .registers 2

    .line 22
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/model/VersionControlDTO;->message:Ljava/lang/String;

    return-object v0
.end method

.method public getType()Lbr/com/itau/sdk/android/core/model/VersionControlType;
    .registers 2

    .line 18
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/model/VersionControlDTO;->type:Lbr/com/itau/sdk/android/core/model/VersionControlType;

    return-object v0
.end method

.method public getUrl()Ljava/lang/String;
    .registers 2

    .line 26
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/model/VersionControlDTO;->url:Ljava/lang/String;

    return-object v0
.end method

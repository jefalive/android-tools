.class public final Lcom/itau/empresas/feature/feedback/FeedbackHelper;
.super Ljava/lang/Object;
.source "FeedbackHelper.java"


# static fields
.field private static final QUANDO_EXIBIR_DIALOG:[I


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .line 10
    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_a

    sput-object v0, Lcom/itau/empresas/feature/feedback/FeedbackHelper;->QUANDO_EXIBIR_DIALOG:[I

    return-void

    nop

    :array_a
    .array-data 4
        0x2
        0x7
    .end array-data
.end method

.method public static deveExibirFeedback(Landroid/content/SharedPreferences;)Z
    .registers 7
    .param p0, "sharedPreferences"    # Landroid/content/SharedPreferences;

    .line 34
    invoke-static {p0}, Lcom/itau/empresas/feature/feedback/FeedbackUtil;->getQuantidadeAcessos(Landroid/content/SharedPreferences;)I

    move-result v1

    .line 35
    .local v1, "quantidadeAcessos":I
    sget-object v2, Lcom/itau/empresas/feature/feedback/FeedbackHelper;->QUANDO_EXIBIR_DIALOG:[I

    array-length v3, v2

    const/4 v4, 0x0

    :goto_8
    if-ge v4, v3, :cond_13

    aget v5, v2, v4

    .line 36
    .local v5, "quantidadeParaExibir":I
    if-ne v5, v1, :cond_10

    .line 37
    const/4 v0, 0x1

    return v0

    .line 35
    .end local v5    # "quantidadeParaExibir":I
    :cond_10
    add-int/lit8 v4, v4, 0x1

    goto :goto_8

    .line 39
    :cond_13
    const/4 v0, 0x0

    return v0
.end method

.method public static exibirFeedback(Landroid/support/v7/app/AppCompatActivity;)V
    .registers 4
    .param p0, "activity"    # Landroid/support/v7/app/AppCompatActivity;

    .line 17
    invoke-virtual {p0}, Landroid/support/v7/app/AppCompatActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/itau/empresas/CustomApplication;

    .line 18
    .local v1, "application":Lcom/itau/empresas/CustomApplication;
    invoke-virtual {v1}, Lcom/itau/empresas/CustomApplication;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v2

    .line 19
    .local v2, "sharedPreferences":Landroid/content/SharedPreferences;
    invoke-static {v2}, Lcom/itau/empresas/feature/feedback/FeedbackUtil;->salvaQuantidadeOperacoesSucesso(Landroid/content/SharedPreferences;)V

    .line 21
    invoke-static {v2}, Lcom/itau/empresas/feature/feedback/FeedbackHelper;->deveExibirFeedback(Landroid/content/SharedPreferences;)Z

    move-result v0

    if-eqz v0, :cond_22

    .line 22
    invoke-static {v2}, Lcom/itau/empresas/feature/feedback/FeedbackHelper;->salvaPreferenciaFeedback(Landroid/content/SharedPreferences;)V

    .line 23
    invoke-static {}, Lcom/itau/empresas/feature/feedback/FeedbackDialog_;->builder()Lcom/itau/empresas/feature/feedback/FeedbackDialog_$FragmentBuilder_;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/feature/feedback/FeedbackDialog_$FragmentBuilder_;->build()Lcom/itau/empresas/feature/feedback/FeedbackDialog;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/itau/empresas/feature/feedback/FeedbackDialog;->show(Landroid/support/v7/app/AppCompatActivity;)Lcom/itau/empresas/ui/dialog/BaseDialog;

    .line 25
    :cond_22
    return-void
.end method

.method private static salvaPreferenciaFeedback(Landroid/content/SharedPreferences;)V
    .registers 4
    .param p0, "sharedPreferences"    # Landroid/content/SharedPreferences;

    .line 28
    invoke-interface {p0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    .line 29
    .local v2, "ed":Landroid/content/SharedPreferences$Editor;
    const-string v0, "feedback"

    sget-object v1, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-interface {v2, v0, v1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 30
    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 31
    return-void
.end method

.class public Lcom/itau/empresas/feature/credito/giro/activity/GiroValorActivity;
.super Lcom/itau/empresas/ui/activity/BaseActivity;
.source "GiroValorActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field botaoContinuar:Landroid/widget/Button;

.field campoValorDisponivel:Landroid/widget/TextView;

.field giroPersonalizadoValoresView:Lcom/itau/empresas/feature/credito/giro/customview/GiroPersonalizadoValoresView;

.field ofertaEspecialAceita:Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;

.field produtosParceladosSimulacaoPropostaSaidaVO:Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoPropostaSaidaVO;

.field root:Landroid/widget/RelativeLayout;

.field textoValorAuxiliar:Landroid/widget/TextView;

.field toolBar:Landroid/support/v7/widget/Toolbar;

.field private topSnackbar:Lbr/com/itau/topsnackbar/TopSnackbar;


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 35
    invoke-direct {p0}, Lcom/itau/empresas/ui/activity/BaseActivity;-><init>()V

    return-void
.end method

.method private alternarMensagemIndicandoSelecao(Z)V
    .registers 5
    .param p1, "exibir"    # Z

    .line 200
    if-eqz p1, :cond_17

    .line 201
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorActivity;->root:Landroid/widget/RelativeLayout;

    .line 202
    const v1, 0x7f07066b

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lbr/com/itau/topsnackbar/TopSnackbar;->make(Landroid/view/ViewGroup;Ljava/lang/CharSequence;I)Lbr/com/itau/topsnackbar/TopSnackbar;

    move-result-object v0

    .line 203
    invoke-virtual {v0}, Lbr/com/itau/topsnackbar/TopSnackbar;->show()Lbr/com/itau/topsnackbar/TopSnackbar;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorActivity;->topSnackbar:Lbr/com/itau/topsnackbar/TopSnackbar;

    goto :goto_20

    .line 205
    :cond_17
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorActivity;->topSnackbar:Lbr/com/itau/topsnackbar/TopSnackbar;

    if-eqz v0, :cond_20

    .line 206
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorActivity;->topSnackbar:Lbr/com/itau/topsnackbar/TopSnackbar;

    invoke-virtual {v0}, Lbr/com/itau/topsnackbar/TopSnackbar;->dismiss()V

    .line 208
    :cond_20
    :goto_20
    return-void
.end method

.method private carregarDadosNaTela()V
    .registers 6

    .line 166
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorActivity;->produtosParceladosSimulacaoPropostaSaidaVO:Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoPropostaSaidaVO;

    if-eqz v0, :cond_67

    .line 168
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorActivity;->campoValorDisponivel:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorActivity;->produtosParceladosSimulacaoPropostaSaidaVO:Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoPropostaSaidaVO;

    .line 170
    invoke-virtual {v1}, Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoPropostaSaidaVO;->getValorMaximoContratacao()F

    move-result v1

    .line 169
    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/itau/empresas/ui/util/ValorMonetarioUtils;->formataValorDecimalParaMonetario(FZ)Ljava/lang/String;

    move-result-object v1

    .line 168
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 173
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorActivity;->produtosParceladosSimulacaoPropostaSaidaVO:Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoPropostaSaidaVO;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoPropostaSaidaVO;->getValoresEmprestimos()Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_67

    .line 175
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorActivity;->giroPersonalizadoValoresView:Lcom/itau/empresas/feature/credito/giro/customview/GiroPersonalizadoValoresView;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/credito/giro/customview/GiroPersonalizadoValoresView;->clearData()V

    .line 177
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorActivity;->giroPersonalizadoValoresView:Lcom/itau/empresas/feature/credito/giro/customview/GiroPersonalizadoValoresView;

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorActivity;->produtosParceladosSimulacaoPropostaSaidaVO:Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoPropostaSaidaVO;

    .line 178
    invoke-virtual {v1}, Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoPropostaSaidaVO;->getValoresEmprestimos()Ljava/util/ArrayList;

    move-result-object v1

    iget-object v2, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorActivity;->produtosParceladosSimulacaoPropostaSaidaVO:Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoPropostaSaidaVO;

    .line 179
    invoke-virtual {v2}, Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoPropostaSaidaVO;->getValorMinimoContratacao()F

    move-result v2

    iget-object v3, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorActivity;->produtosParceladosSimulacaoPropostaSaidaVO:Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoPropostaSaidaVO;

    .line 181
    invoke-virtual {v3}, Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoPropostaSaidaVO;->getValorMaximoContratacao()F

    move-result v3

    .line 177
    invoke-virtual {v0, p0, v1, v2, v3}, Lcom/itau/empresas/feature/credito/giro/customview/GiroPersonalizadoValoresView;->setData(Landroid/content/Context;Ljava/util/List;FF)V

    .line 183
    const v0, 0x7f070686

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorActivity;->produtosParceladosSimulacaoPropostaSaidaVO:Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoPropostaSaidaVO;

    .line 187
    invoke-virtual {v2}, Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoPropostaSaidaVO;->getValorMinimoContratacao()F

    move-result v2

    .line 185
    const/4 v3, 0x1

    invoke-static {v2, v3}, Lcom/itau/empresas/ui/util/ValorMonetarioUtils;->formataValorDecimalParaMonetario(FZ)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    iget-object v2, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorActivity;->produtosParceladosSimulacaoPropostaSaidaVO:Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoPropostaSaidaVO;

    .line 192
    invoke-virtual {v2}, Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoPropostaSaidaVO;->getValorMaximoContratacao()F

    move-result v2

    .line 190
    const/4 v3, 0x1

    invoke-static {v2, v3}, Lcom/itau/empresas/ui/util/ValorMonetarioUtils;->formataValorDecimalParaMonetario(FZ)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    aput-object v2, v1, v3

    .line 183
    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 194
    .local v4, "auxiliar":Ljava/lang/String;
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorActivity;->textoValorAuxiliar:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 197
    .end local v4    # "auxiliar":Ljava/lang/String;
    :cond_67
    return-void
.end method

.method private constroiToolbar()V
    .registers 3

    .line 149
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorActivity;->produtosParceladosSimulacaoPropostaSaidaVO:Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoPropostaSaidaVO;

    if-eqz v0, :cond_1c

    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorActivity;->produtosParceladosSimulacaoPropostaSaidaVO:Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoPropostaSaidaVO;

    .line 150
    invoke-virtual {v0}, Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoPropostaSaidaVO;->getDadosSeguro()Lcom/itau/empresas/feature/credito/giro/model/SeguroProdutosParceladoSaidaVO;

    move-result-object v0

    if-eqz v0, :cond_1c

    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorActivity;->produtosParceladosSimulacaoPropostaSaidaVO:Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoPropostaSaidaVO;

    .line 151
    invoke-virtual {v0}, Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoPropostaSaidaVO;->getDadosSeguro()Lcom/itau/empresas/feature/credito/giro/model/SeguroProdutosParceladoSaidaVO;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/feature/credito/giro/model/SeguroProdutosParceladoSaidaVO;->getIndicadorSeguro()Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 152
    const v1, 0x7f070699

    .local v1, "id":I
    goto :goto_1f

    .line 154
    .end local v1    # "id":I
    :cond_1c
    const v1, 0x7f07069a

    .line 157
    .local v1, "id":I
    :goto_1f
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorActivity;->toolBar:Landroid/support/v7/widget/Toolbar;

    invoke-static {v0, p0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom;->Builder(Landroid/support/v7/widget/Toolbar;Landroid/support/v7/app/AppCompatActivity;)Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    move-result-object v0

    .line 159
    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->comTitulo(I)Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    move-result-object v0

    .line 160
    invoke-virtual {v0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->comClickListenerFinish()Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    move-result-object v0

    .line 161
    invoke-virtual {v0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->build()Lcom/itau/empresas/ui/util/listener/ToolbarCustom;

    .line 162
    return-void
.end method

.method public static intent(Landroid/content/Context;)Lcom/itau/empresas/feature/credito/giro/activity/GiroValorActivity_$IntentBuilder_;
    .registers 2
    .param p0, "context"    # Landroid/content/Context;

    .line 211
    new-instance v0, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorActivity_$IntentBuilder_;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorActivity_$IntentBuilder_;-><init>(Landroid/content/Context;)V

    return-object v0
.end method


# virtual methods
.method aoCarregarTela()V
    .registers 2

    .line 56
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorActivity;->constroiToolbar()V

    .line 57
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorActivity;->topSnackbar:Lbr/com/itau/topsnackbar/TopSnackbar;

    if-eqz v0, :cond_c

    .line 58
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorActivity;->topSnackbar:Lbr/com/itau/topsnackbar/TopSnackbar;

    invoke-virtual {v0}, Lbr/com/itau/topsnackbar/TopSnackbar;->dismiss()V

    .line 60
    :cond_c
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorActivity;->alternarMensagemIndicandoSelecao(Z)V

    .line 61
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorActivity;->carregarDadosNaTela()V

    .line 62
    return-void
.end method

.method aoClicarNoBotaoContinuar()V
    .registers 4

    .line 67
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorActivity;->topSnackbar:Lbr/com/itau/topsnackbar/TopSnackbar;

    if-eqz v0, :cond_9

    .line 68
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorActivity;->topSnackbar:Lbr/com/itau/topsnackbar/TopSnackbar;

    invoke-virtual {v0}, Lbr/com/itau/topsnackbar/TopSnackbar;->dismiss()V

    .line 70
    :cond_9
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorActivity;->giroPersonalizadoValoresView:Lcom/itau/empresas/feature/credito/giro/customview/GiroPersonalizadoValoresView;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/credito/giro/customview/GiroPersonalizadoValoresView;->getSelectedItem()Lcom/itau/empresas/feature/credito/giro/model/ValoresEmprestimoVO;

    move-result-object v2

    .line 72
    .local v2, "valoresEmprestimoVO":Lcom/itau/empresas/feature/credito/giro/model/ValoresEmprestimoVO;
    if-eqz v2, :cond_32

    .line 74
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorActivity;->alternarMensagemIndicandoSelecao(Z)V

    .line 77
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorActivity;->ofertaEspecialAceita:Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;

    invoke-virtual {v2}, Lcom/itau/empresas/feature/credito/giro/model/ValoresEmprestimoVO;->getValoresEmprestimo()Ljava/math/BigDecimal;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;->setValorEmprestimo(Ljava/math/BigDecimal;)V

    .line 79
    invoke-static {p0}, Lcom/itau/empresas/feature/credito/giro/activity/GiroDataActivity;->intent(Landroid/content/Context;)Lcom/itau/empresas/feature/credito/giro/activity/GiroDataActivity_$IntentBuilder_;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorActivity;->ofertaEspecialAceita:Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;

    .line 80
    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/credito/giro/activity/GiroDataActivity_$IntentBuilder_;->ofertaEspecialAceita(Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;)Lcom/itau/empresas/feature/credito/giro/activity/GiroDataActivity_$IntentBuilder_;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorActivity;->produtosParceladosSimulacaoPropostaSaidaVO:Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoPropostaSaidaVO;

    .line 81
    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/credito/giro/activity/GiroDataActivity_$IntentBuilder_;->simulacaoPropostaSaidaVO(Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoPropostaSaidaVO;)Lcom/itau/empresas/feature/credito/giro/activity/GiroDataActivity_$IntentBuilder_;

    move-result-object v0

    .line 82
    invoke-virtual {v0}, Lcom/itau/empresas/feature/credito/giro/activity/GiroDataActivity_$IntentBuilder_;->start()Lorg/androidannotations/api/builder/PostActivityStarter;

    goto :goto_36

    .line 85
    :cond_32
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorActivity;->alternarMensagemIndicandoSelecao(Z)V

    .line 87
    :goto_36
    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .registers 8
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .line 126
    const/4 v0, -0x1

    if-ne p2, v0, :cond_23

    if-nez p1, :cond_23

    const-string v0, "valor"

    .line 127
    invoke-virtual {p3, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_23

    .line 129
    const-string v0, "valor"

    const/4 v1, 0x0

    invoke-virtual {p3, v0, v1}, Landroid/content/Intent;->getFloatExtra(Ljava/lang/String;F)F

    move-result v2

    .line 131
    .local v2, "valor":F
    const/4 v0, 0x0

    cmpl-float v0, v2, v0

    if-lez v0, :cond_23

    .line 133
    float-to-double v0, v2

    invoke-static {v0, v1}, Ljava/math/BigDecimal;->valueOf(D)Ljava/math/BigDecimal;

    move-result-object v3

    .line 135
    .local v3, "valorBigDecimal":Ljava/math/BigDecimal;
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorActivity;->giroPersonalizadoValoresView:Lcom/itau/empresas/feature/credito/giro/customview/GiroPersonalizadoValoresView;

    invoke-virtual {v0, v3}, Lcom/itau/empresas/feature/credito/giro/customview/GiroPersonalizadoValoresView;->addItem(Ljava/math/BigDecimal;)V

    .line 139
    .end local v2    # "valor":F
    .end local v3    # "valorBigDecimal":Ljava/math/BigDecimal;
    :cond_23
    invoke-super {p0, p1, p2, p3}, Lcom/itau/empresas/ui/activity/BaseActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 140
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .registers 2
    .param p1, "v"    # Landroid/view/View;

    .line 143
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorActivity;->onBackPressed()V

    .line 144
    return-void
.end method

.method public onEventMainThread(Lcom/itau/empresas/Evento$EventoGiroOutroValorSelecionado;)V
    .registers 4
    .param p1, "eventoGiroOutroValorSelecionado"    # Lcom/itau/empresas/Evento$EventoGiroOutroValorSelecionado;

    .line 96
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorActivity;->alternarMensagemIndicandoSelecao(Z)V

    .line 98
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorActivity;->giroPersonalizadoValoresView:Lcom/itau/empresas/feature/credito/giro/customview/GiroPersonalizadoValoresView;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/credito/giro/customview/GiroPersonalizadoValoresView;->removeSelection()V

    .line 100
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorActivity;->produtosParceladosSimulacaoPropostaSaidaVO:Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoPropostaSaidaVO;

    if-eqz v0, :cond_52

    .line 102
    invoke-static {p0}, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity;->intent(Landroid/content/Context;)Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity_$IntentBuilder_;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorActivity;->produtosParceladosSimulacaoPropostaSaidaVO:Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoPropostaSaidaVO;

    .line 104
    invoke-virtual {v1}, Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoPropostaSaidaVO;->getValorMinimoContratacao()F

    move-result v1

    .line 103
    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity_$IntentBuilder_;->valorMinimo(F)Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity_$IntentBuilder_;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorActivity;->produtosParceladosSimulacaoPropostaSaidaVO:Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoPropostaSaidaVO;

    .line 106
    invoke-virtual {v1}, Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoPropostaSaidaVO;->getValorMaximoContratacao()F

    move-result v1

    .line 105
    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity_$IntentBuilder_;->valorMaximo(F)Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity_$IntentBuilder_;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorActivity;->produtosParceladosSimulacaoPropostaSaidaVO:Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoPropostaSaidaVO;

    .line 108
    invoke-virtual {v1}, Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoPropostaSaidaVO;->getDadosSeguro()Lcom/itau/empresas/feature/credito/giro/model/SeguroProdutosParceladoSaidaVO;

    move-result-object v1

    .line 109
    invoke-virtual {v1}, Lcom/itau/empresas/feature/credito/giro/model/SeguroProdutosParceladoSaidaVO;->getIndicadorSeguro()Z

    move-result v1

    .line 107
    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity_$IntentBuilder_;->temSeguro(Z)Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity_$IntentBuilder_;

    move-result-object v0

    sget-object v1, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity$Exibicao;->VALOR:Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity$Exibicao;

    .line 110
    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity_$IntentBuilder_;->exibicao(Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity$Exibicao;)Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity_$IntentBuilder_;

    move-result-object v0

    .line 111
    const v1, 0x8000

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity_$IntentBuilder_;->flags(I)Lorg/androidannotations/api/builder/IntentBuilder;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity_$IntentBuilder_;

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorActivity;->ofertaEspecialAceita:Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;

    .line 112
    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity_$IntentBuilder_;->ofertaEspecialAceita(Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;)Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity_$IntentBuilder_;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorActivity;->produtosParceladosSimulacaoPropostaSaidaVO:Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoPropostaSaidaVO;

    .line 113
    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity_$IntentBuilder_;->produtosParceladosSimulacaoPropostaSaidaVO(Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoPropostaSaidaVO;)Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity_$IntentBuilder_;

    move-result-object v0

    .line 115
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity_$IntentBuilder_;->startForResult(I)Lorg/androidannotations/api/builder/PostActivityStarter;

    .line 117
    :cond_52
    return-void
.end method

.method public opkeysConsumidas()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Ljava/lang/String;>;"
        }
    .end annotation

    .line 121
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

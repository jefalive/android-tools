.class public Lcom/itau/empresas/feature/pendencias/PendenciasAdapter;
.super Landroid/support/v7/widget/RecyclerView$Adapter;
.source "PendenciasAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/itau/empresas/feature/pendencias/PendenciasAdapter$ViewHolderCardSelecaoDias;,
        Lcom/itau/empresas/feature/pendencias/PendenciasAdapter$ViewHolderCardAlerta;,
        Lcom/itau/empresas/feature/pendencias/PendenciasAdapter$ViewHolderLista;,
        Lcom/itau/empresas/feature/pendencias/PendenciasAdapter$ViewHolderHeader;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/support/v7/widget/RecyclerView$Adapter<Landroid/support/v7/widget/RecyclerView$ViewHolder;>;"
    }
.end annotation


# instance fields
.field private estadosPendencias:Lcom/itau/empresas/feature/pendencias/activity/PendenciasActivity$EstadosPendencias;

.field private headerAppPosition:I

.field private headerWebPosition:I

.field private final listaApp:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Lcom/itau/empresas/feature/pendencias/PendenciaVO;>;"
        }
    .end annotation
.end field

.field private final listaWeb:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Lcom/itau/empresas/feature/pendencias/PendenciaVO;>;"
        }
    .end annotation
.end field

.field private mApplication:Lcom/itau/empresas/CustomApplication;

.field private mContext:Landroid/content/Context;

.field private quantidadePendencias:I

.field private final recyclerData:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Lcom/itau/empresas/feature/pendencias/PendenciaVO;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/itau/empresas/CustomApplication;)V
    .registers 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "application"    # Lcom/itau/empresas/CustomApplication;

    .line 54
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView$Adapter;-><init>()V

    .line 42
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/feature/pendencias/PendenciasAdapter;->recyclerData:Ljava/util/List;

    .line 44
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/feature/pendencias/PendenciasAdapter;->listaApp:Ljava/util/List;

    .line 45
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/feature/pendencias/PendenciasAdapter;->listaWeb:Ljava/util/List;

    .line 55
    iput-object p1, p0, Lcom/itau/empresas/feature/pendencias/PendenciasAdapter;->mContext:Landroid/content/Context;

    .line 56
    iput-object p2, p0, Lcom/itau/empresas/feature/pendencias/PendenciasAdapter;->mApplication:Lcom/itau/empresas/CustomApplication;

    .line 57
    return-void
.end method

.method static synthetic access$100(Lcom/itau/empresas/feature/pendencias/PendenciasAdapter;)Landroid/content/Context;
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/feature/pendencias/PendenciasAdapter;

    .line 33
    iget-object v0, p0, Lcom/itau/empresas/feature/pendencias/PendenciasAdapter;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$200(Lcom/itau/empresas/feature/pendencias/PendenciasAdapter;Ljava/lang/String;)V
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/feature/pendencias/PendenciasAdapter;
    .param p1, "x1"    # Ljava/lang/String;

    .line 33
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/pendencias/PendenciasAdapter;->disparaGA(Ljava/lang/String;)V

    return-void
.end method

.method private ajustaViewTypeLista(Lcom/itau/empresas/feature/pendencias/PendenciasAdapter$ViewHolderLista;I)V
    .registers 7
    .param p1, "viewHolder"    # Lcom/itau/empresas/feature/pendencias/PendenciasAdapter$ViewHolderLista;
    .param p2, "position"    # I

    .line 327
    iget-object v0, p0, Lcom/itau/empresas/feature/pendencias/PendenciasAdapter;->recyclerData:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/itau/empresas/feature/pendencias/PendenciaVO;

    .line 328
    .local v2, "pendenciasVO":Lcom/itau/empresas/feature/pendencias/PendenciaVO;
    move-object v3, p1

    .line 329
    .local v3, "viewHolderLista":Lcom/itau/empresas/feature/pendencias/PendenciasAdapter$ViewHolderLista;
    iget-object v0, v3, Lcom/itau/empresas/feature/pendencias/PendenciasAdapter$ViewHolderLista;->nomePendencia:Landroid/widget/TextView;

    invoke-virtual {v2}, Lcom/itau/empresas/feature/pendencias/PendenciaVO;->getDescricaoTarefa()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 330
    iget-object v0, v3, Lcom/itau/empresas/feature/pendencias/PendenciasAdapter$ViewHolderLista;->quantidadePendencia:Landroid/widget/TextView;

    .line 331
    invoke-virtual {v2}, Lcom/itau/empresas/feature/pendencias/PendenciaVO;->getQuantidadePendencias()I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    .line 330
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 332
    iget-object v0, v3, Lcom/itau/empresas/feature/pendencias/PendenciasAdapter$ViewHolderLista;->parentView:Landroid/view/View;

    new-instance v1, Lcom/itau/empresas/feature/pendencias/PendenciasAdapter$1;

    invoke-direct {v1, p0, v2, v3}, Lcom/itau/empresas/feature/pendencias/PendenciasAdapter$1;-><init>(Lcom/itau/empresas/feature/pendencias/PendenciasAdapter;Lcom/itau/empresas/feature/pendencias/PendenciaVO;Lcom/itau/empresas/feature/pendencias/PendenciasAdapter$ViewHolderLista;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 351
    return-void
.end method

.method private atualizaComEstado()V
    .registers 3

    .line 76
    invoke-direct {p0}, Lcom/itau/empresas/feature/pendencias/PendenciasAdapter;->limpaListas()V

    .line 77
    iget-object v0, p0, Lcom/itau/empresas/feature/pendencias/PendenciasAdapter;->recyclerData:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 78
    iget-object v0, p0, Lcom/itau/empresas/feature/pendencias/PendenciasAdapter;->recyclerData:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 79
    invoke-virtual {p0}, Lcom/itau/empresas/feature/pendencias/PendenciasAdapter;->notifyDataSetChanged()V

    .line 80
    return-void
.end method

.method private atualizaComLista(Ljava/util/List;)V
    .registers 4
    .param p1, "listaPendencias"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<Lcom/itau/empresas/feature/pendencias/PendenciaVO;>;)V"
        }
    .end annotation

    .line 83
    invoke-direct {p0}, Lcom/itau/empresas/feature/pendencias/PendenciasAdapter;->limpaListas()V

    .line 84
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/pendencias/PendenciasAdapter;->separaListas(Ljava/util/List;)V

    .line 86
    iget-object v0, p0, Lcom/itau/empresas/feature/pendencias/PendenciasAdapter;->listaApp:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_10

    const/4 v0, 0x2

    goto :goto_11

    :cond_10
    const/4 v0, -0x1

    :goto_11
    iput v0, p0, Lcom/itau/empresas/feature/pendencias/PendenciasAdapter;->headerAppPosition:I

    .line 88
    iget-object v0, p0, Lcom/itau/empresas/feature/pendencias/PendenciasAdapter;->listaWeb:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1f

    .line 89
    const/4 v0, -0x1

    iput v0, p0, Lcom/itau/empresas/feature/pendencias/PendenciasAdapter;->headerWebPosition:I

    goto :goto_34

    .line 90
    :cond_1f
    iget v0, p0, Lcom/itau/empresas/feature/pendencias/PendenciasAdapter;->headerAppPosition:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_28

    .line 91
    const/4 v0, 0x2

    iput v0, p0, Lcom/itau/empresas/feature/pendencias/PendenciasAdapter;->headerWebPosition:I

    goto :goto_34

    .line 93
    :cond_28
    iget-object v0, p0, Lcom/itau/empresas/feature/pendencias/PendenciasAdapter;->listaApp:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x2

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/itau/empresas/feature/pendencias/PendenciasAdapter;->headerWebPosition:I

    .line 96
    :goto_34
    iget-object v0, p0, Lcom/itau/empresas/feature/pendencias/PendenciasAdapter;->recyclerData:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 97
    iget-object v0, p0, Lcom/itau/empresas/feature/pendencias/PendenciasAdapter;->recyclerData:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 99
    iget-object v0, p0, Lcom/itau/empresas/feature/pendencias/PendenciasAdapter;->listaApp:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_55

    .line 100
    iget-object v0, p0, Lcom/itau/empresas/feature/pendencias/PendenciasAdapter;->recyclerData:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 101
    iget-object v0, p0, Lcom/itau/empresas/feature/pendencias/PendenciasAdapter;->recyclerData:Ljava/util/List;

    iget-object v1, p0, Lcom/itau/empresas/feature/pendencias/PendenciasAdapter;->listaApp:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 104
    :cond_55
    iget-object v0, p0, Lcom/itau/empresas/feature/pendencias/PendenciasAdapter;->listaWeb:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_6a

    .line 105
    iget-object v0, p0, Lcom/itau/empresas/feature/pendencias/PendenciasAdapter;->recyclerData:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 106
    iget-object v0, p0, Lcom/itau/empresas/feature/pendencias/PendenciasAdapter;->recyclerData:Ljava/util/List;

    iget-object v1, p0, Lcom/itau/empresas/feature/pendencias/PendenciasAdapter;->listaWeb:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 109
    :cond_6a
    invoke-virtual {p0}, Lcom/itau/empresas/feature/pendencias/PendenciasAdapter;->notifyDataSetChanged()V

    .line 110
    return-void
.end method

.method private disparaGA(Ljava/lang/String;)V
    .registers 7
    .param p1, "descricaoPendencia"    # Ljava/lang/String;

    .line 362
    if-eqz p1, :cond_29

    .line 363
    invoke-static {p1}, Lcom/itau/empresas/ui/util/StringUtils;->removeAcentos(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, " "

    const-string v2, "-"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v4

    .line 366
    .local v4, "acao":Ljava/lang/String;
    iget-object v0, p0, Lcom/itau/empresas/feature/pendencias/PendenciasAdapter;->mApplication:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getEventoTracker()Lcom/itau/empresas/ui/util/analytics/TrackerEvento;

    move-result-object v0

    new-instance v1, Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;

    iget-object v2, p0, Lcom/itau/empresas/feature/pendencias/PendenciasAdapter;->mContext:Landroid/content/Context;

    .line 367
    const v3, 0x7f070329

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v4}, Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 366
    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/analytics/TrackerEvento;->enviarHitEvento(Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;)V

    .line 369
    .end local v4    # "acao":Ljava/lang/String;
    :cond_29
    return-void
.end method

.method private getQuantidadePendencias()I
    .registers 2

    .line 354
    iget v0, p0, Lcom/itau/empresas/feature/pendencias/PendenciasAdapter;->quantidadePendencias:I

    return v0
.end method

.method private limpaListas()V
    .registers 2

    .line 128
    iget-object v0, p0, Lcom/itau/empresas/feature/pendencias/PendenciasAdapter;->recyclerData:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 129
    iget-object v0, p0, Lcom/itau/empresas/feature/pendencias/PendenciasAdapter;->listaApp:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 130
    iget-object v0, p0, Lcom/itau/empresas/feature/pendencias/PendenciasAdapter;->listaWeb:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 131
    return-void
.end method

.method private separaListas(Ljava/util/List;)V
    .registers 6
    .param p1, "resultadoApi"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<Lcom/itau/empresas/feature/pendencias/PendenciaVO;>;)V"
        }
    .end annotation

    .line 60
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3d

    .line 62
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_7
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_3d

    .line 63
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/feature/pendencias/PendenciaVO;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/pendencias/PendenciaVO;->getCanalParaResolverPendencia()Ljava/lang/String;

    move-result-object v3

    .line 65
    .local v3, "canal":Ljava/lang/String;
    const-string v0, "app_mobile"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_29

    .line 66
    iget-object v0, p0, Lcom/itau/empresas/feature/pendencias/PendenciasAdapter;->listaApp:Ljava/util/List;

    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3a

    .line 67
    :cond_29
    const-string v0, "internet"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3a

    .line 68
    iget-object v0, p0, Lcom/itau/empresas/feature/pendencias/PendenciasAdapter;->listaWeb:Ljava/util/List;

    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 62
    .end local v3    # "canal":Ljava/lang/String;
    :cond_3a
    :goto_3a
    add-int/lit8 v2, v2, 0x1

    goto :goto_7

    .line 72
    .end local v2    # "i":I
    :cond_3d
    return-void
.end method

.method private setQuantidadePendencias(I)V
    .registers 2
    .param p1, "quantidadePendencias"    # I

    .line 358
    iput p1, p0, Lcom/itau/empresas/feature/pendencias/PendenciasAdapter;->quantidadePendencias:I

    .line 359
    return-void
.end method


# virtual methods
.method public atualizaAdapter(Lcom/itau/empresas/feature/pendencias/ListaPendenciasVO;Lcom/itau/empresas/feature/pendencias/activity/PendenciasActivity$EstadosPendencias;)V
    .registers 4
    .param p1, "pendencias"    # Lcom/itau/empresas/feature/pendencias/ListaPendenciasVO;
    .param p2, "estadosPendencias"    # Lcom/itau/empresas/feature/pendencias/activity/PendenciasActivity$EstadosPendencias;

    .line 115
    iput-object p2, p0, Lcom/itau/empresas/feature/pendencias/PendenciasAdapter;->estadosPendencias:Lcom/itau/empresas/feature/pendencias/activity/PendenciasActivity$EstadosPendencias;

    .line 117
    sget-object v0, Lcom/itau/empresas/feature/pendencias/activity/PendenciasActivity$EstadosPendencias;->SEM_CONEXAO:Lcom/itau/empresas/feature/pendencias/activity/PendenciasActivity$EstadosPendencias;

    if-eq p2, v0, :cond_a

    sget-object v0, Lcom/itau/empresas/feature/pendencias/activity/PendenciasActivity$EstadosPendencias;->NAO_POSSUI_PENDENCIAS:Lcom/itau/empresas/feature/pendencias/activity/PendenciasActivity$EstadosPendencias;

    if-ne p2, v0, :cond_e

    .line 119
    :cond_a
    invoke-direct {p0}, Lcom/itau/empresas/feature/pendencias/PendenciasAdapter;->atualizaComEstado()V

    .line 120
    return-void

    .line 122
    :cond_e
    invoke-virtual {p1}, Lcom/itau/empresas/feature/pendencias/ListaPendenciasVO;->getListaPendencias()Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/itau/empresas/feature/pendencias/PendenciasAdapter;->atualizaComLista(Ljava/util/List;)V

    .line 123
    invoke-virtual {p1}, Lcom/itau/empresas/feature/pendencias/ListaPendenciasVO;->getTotalPendencias()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/itau/empresas/feature/pendencias/PendenciasAdapter;->setQuantidadePendencias(I)V

    .line 125
    return-void
.end method

.method public getItemCount()I
    .registers 2

    .line 221
    iget-object v0, p0, Lcom/itau/empresas/feature/pendencias/PendenciasAdapter;->recyclerData:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItemViewType(I)I
    .registers 3
    .param p1, "position"    # I

    .line 227
    if-nez p1, :cond_4

    .line 228
    const/4 v0, 0x3

    return v0

    .line 231
    :cond_4
    const/4 v0, 0x1

    if-ne p1, v0, :cond_9

    .line 232
    const/4 v0, 0x4

    return v0

    .line 235
    :cond_9
    iget v0, p0, Lcom/itau/empresas/feature/pendencias/PendenciasAdapter;->headerAppPosition:I

    if-ne p1, v0, :cond_f

    .line 236
    const/4 v0, 0x0

    return v0

    .line 239
    :cond_f
    iget v0, p0, Lcom/itau/empresas/feature/pendencias/PendenciasAdapter;->headerWebPosition:I

    if-ne p1, v0, :cond_15

    .line 240
    const/4 v0, 0x1

    return v0

    .line 244
    :cond_15
    const/4 v0, 0x2

    return v0
.end method

.method public onBindViewHolder(Landroid/support/v7/widget/RecyclerView$ViewHolder;I)V
    .registers 11
    .param p1, "viewHolder"    # Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .param p2, "position"    # I

    .line 283
    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView$ViewHolder;->getItemViewType()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_6c

    .line 284
    move-object v5, p1

    check-cast v5, Lcom/itau/empresas/feature/pendencias/PendenciasAdapter$ViewHolderCardAlerta;

    .line 287
    .local v5, "viewHolderCardAlerta":Lcom/itau/empresas/feature/pendencias/PendenciasAdapter$ViewHolderCardAlerta;
    iget-object v0, p0, Lcom/itau/empresas/feature/pendencias/PendenciasAdapter;->estadosPendencias:Lcom/itau/empresas/feature/pendencias/activity/PendenciasActivity$EstadosPendencias;

    sget-object v1, Lcom/itau/empresas/feature/pendencias/activity/PendenciasActivity$EstadosPendencias;->SEM_CONEXAO:Lcom/itau/empresas/feature/pendencias/activity/PendenciasActivity$EstadosPendencias;

    if-eq v0, v1, :cond_26

    iget-object v0, p0, Lcom/itau/empresas/feature/pendencias/PendenciasAdapter;->estadosPendencias:Lcom/itau/empresas/feature/pendencias/activity/PendenciasActivity$EstadosPendencias;

    sget-object v1, Lcom/itau/empresas/feature/pendencias/activity/PendenciasActivity$EstadosPendencias;->NAO_POSSUI_PENDENCIAS:Lcom/itau/empresas/feature/pendencias/activity/PendenciasActivity$EstadosPendencias;

    if-ne v0, v1, :cond_35

    iget-object v0, p0, Lcom/itau/empresas/feature/pendencias/PendenciasAdapter;->listaApp:Ljava/util/List;

    .line 289
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_26

    iget-object v0, p0, Lcom/itau/empresas/feature/pendencias/PendenciasAdapter;->listaWeb:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_35

    .line 290
    :cond_26
    iget-object v0, v5, Lcom/itau/empresas/feature/pendencias/PendenciasAdapter$ViewHolderCardAlerta;->parentView:Lcom/itau/empresas/ui/view/CardAlertaView;

    invoke-virtual {v0}, Lcom/itau/empresas/ui/view/CardAlertaView;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 291
    const v1, 0x7f070512

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 292
    .local v7, "textoCardAlerta":Ljava/lang/String;
    const/4 v6, -0x1

    .local v6, "itensPendentes":I
    goto :goto_57

    .line 294
    .end local v6    # "itensPendentes":I
    .end local v7    # "textoCardAlerta":Ljava/lang/String;
    :cond_35
    invoke-direct {p0}, Lcom/itau/empresas/feature/pendencias/PendenciasAdapter;->getQuantidadePendencias()I

    move-result v6

    .line 295
    .local v6, "itensPendentes":I
    const/4 v0, 0x1

    if-le v6, v0, :cond_4a

    .line 296
    iget-object v0, v5, Lcom/itau/empresas/feature/pendencias/PendenciasAdapter$ViewHolderCardAlerta;->parentView:Lcom/itau/empresas/ui/view/CardAlertaView;

    invoke-virtual {v0}, Lcom/itau/empresas/ui/view/CardAlertaView;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 297
    const v1, 0x7f070511

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    .local v7, "textoCardAlerta":Ljava/lang/String;
    goto :goto_57

    .line 299
    .end local v7    # "textoCardAlerta":Ljava/lang/String;
    :cond_4a
    iget-object v0, v5, Lcom/itau/empresas/feature/pendencias/PendenciasAdapter$ViewHolderCardAlerta;->parentView:Lcom/itau/empresas/ui/view/CardAlertaView;

    invoke-virtual {v0}, Lcom/itau/empresas/ui/view/CardAlertaView;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 300
    const v1, 0x7f07050e

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 303
    .local v7, "textoCardAlerta":Ljava/lang/String;
    :goto_57
    iget-object v0, v5, Lcom/itau/empresas/feature/pendencias/PendenciasAdapter$ViewHolderCardAlerta;->parentView:Lcom/itau/empresas/ui/view/CardAlertaView;

    iget-object v1, p0, Lcom/itau/empresas/feature/pendencias/PendenciasAdapter;->estadosPendencias:Lcom/itau/empresas/feature/pendencias/activity/PendenciasActivity$EstadosPendencias;

    invoke-virtual {v0, v6, v7, v1}, Lcom/itau/empresas/ui/view/CardAlertaView;->bind(ILjava/lang/String;Lcom/itau/empresas/feature/pendencias/activity/PendenciasActivity$EstadosPendencias;)V

    .line 305
    iget-object v0, v5, Lcom/itau/empresas/feature/pendencias/PendenciasAdapter$ViewHolderCardAlerta;->parentView:Lcom/itau/empresas/ui/view/CardAlertaView;

    const/16 v1, 0x1e

    const/16 v2, 0x1e

    const/16 v3, 0x1e

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/itau/empresas/ui/view/CardAlertaView;->configuraMargemWarning(IIII)V

    .line 307
    .end local v5    # "viewHolderCardAlerta":Lcom/itau/empresas/feature/pendencias/PendenciasAdapter$ViewHolderCardAlerta;
    .end local v6    # "itensPendentes":I
    .end local v7    # "textoCardAlerta":Ljava/lang/String;
    goto/16 :goto_d1

    :cond_6c
    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView$ViewHolder;->getItemViewType()I

    move-result v0

    if-nez v0, :cond_92

    .line 308
    move-object v5, p1

    check-cast v5, Lcom/itau/empresas/feature/pendencias/PendenciasAdapter$ViewHolderHeader;

    .line 309
    .local v5, "viewHolderHeader":Lcom/itau/empresas/feature/pendencias/PendenciasAdapter$ViewHolderHeader;
    move-object v0, p1

    check-cast v0, Lcom/itau/empresas/feature/pendencias/PendenciasAdapter$ViewHolderHeader;

    iget-object v0, v0, Lcom/itau/empresas/feature/pendencias/PendenciasAdapter$ViewHolderHeader;->titulo:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    move-result-object v6

    .line 310
    .local v6, "context":Landroid/content/Context;
    iget-object v0, v5, Lcom/itau/empresas/feature/pendencias/PendenciasAdapter$ViewHolderHeader;->titulo:Landroid/widget/TextView;

    .line 311
    const v1, 0x7f07057e

    invoke-virtual {v6, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 310
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 312
    iget-object v0, v5, Lcom/itau/empresas/feature/pendencias/PendenciasAdapter$ViewHolderHeader;->descricao:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 313
    .end local v5    # "viewHolderHeader":Lcom/itau/empresas/feature/pendencias/PendenciasAdapter$ViewHolderHeader;
    .end local v6    # "context":Landroid/content/Context;
    goto :goto_d1

    :cond_92
    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView$ViewHolder;->getItemViewType()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_c4

    .line 314
    move-object v5, p1

    check-cast v5, Lcom/itau/empresas/feature/pendencias/PendenciasAdapter$ViewHolderHeader;

    .line 315
    .local v5, "viewHolderHeader":Lcom/itau/empresas/feature/pendencias/PendenciasAdapter$ViewHolderHeader;
    move-object v0, p1

    check-cast v0, Lcom/itau/empresas/feature/pendencias/PendenciasAdapter$ViewHolderHeader;

    iget-object v0, v0, Lcom/itau/empresas/feature/pendencias/PendenciasAdapter$ViewHolderHeader;->titulo:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    move-result-object v6

    .line 316
    .local v6, "context":Landroid/content/Context;
    iget-object v0, v5, Lcom/itau/empresas/feature/pendencias/PendenciasAdapter$ViewHolderHeader;->titulo:Landroid/widget/TextView;

    .line 317
    const v1, 0x7f070580

    invoke-virtual {v6, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 316
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 318
    iget-object v0, v5, Lcom/itau/empresas/feature/pendencias/PendenciasAdapter$ViewHolderHeader;->descricao:Landroid/widget/TextView;

    .line 319
    const v1, 0x7f07057f

    invoke-virtual {v6, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 318
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 320
    iget-object v0, v5, Lcom/itau/empresas/feature/pendencias/PendenciasAdapter$ViewHolderHeader;->descricao:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 321
    .end local v5    # "viewHolderHeader":Lcom/itau/empresas/feature/pendencias/PendenciasAdapter$ViewHolderHeader;
    .end local v6    # "context":Landroid/content/Context;
    goto :goto_d1

    :cond_c4
    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView$ViewHolder;->getItemViewType()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_d1

    .line 322
    move-object v0, p1

    check-cast v0, Lcom/itau/empresas/feature/pendencias/PendenciasAdapter$ViewHolderLista;

    invoke-direct {p0, v0, p2}, Lcom/itau/empresas/feature/pendencias/PendenciasAdapter;->ajustaViewTypeLista(Lcom/itau/empresas/feature/pendencias/PendenciasAdapter$ViewHolderLista;I)V

    .line 324
    :cond_d1
    :goto_d1
    return-void
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .registers 7
    .param p1, "viewGroup"    # Landroid/view/ViewGroup;
    .param p2, "viewType"    # I

    .line 250
    const/4 v2, 0x0

    .line 251
    .local v2, "viewHolder":Landroid/support/v7/widget/RecyclerView$ViewHolder;
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    .line 253
    .local v3, "inflater":Landroid/view/LayoutInflater;
    packed-switch p2, :pswitch_data_54

    goto/16 :goto_53

    .line 255
    :pswitch_e
    new-instance v2, Lcom/itau/empresas/feature/pendencias/PendenciasAdapter$ViewHolderCardAlerta;

    .line 256
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/itau/empresas/ui/view/CardAlertaView_;->build(Landroid/content/Context;)Lcom/itau/empresas/ui/view/CardAlertaView;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/itau/empresas/feature/pendencias/PendenciasAdapter$ViewHolderCardAlerta;-><init>(Lcom/itau/empresas/ui/view/CardAlertaView;)V

    .line 257
    goto :goto_53

    .line 259
    :pswitch_1c
    new-instance v2, Lcom/itau/empresas/feature/pendencias/PendenciasAdapter$ViewHolderCardSelecaoDias;

    .line 260
    const v0, 0x7f0300e6

    const/4 v1, 0x0

    invoke-virtual {v3, v0, p1, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    invoke-direct {v2, p0, v0}, Lcom/itau/empresas/feature/pendencias/PendenciasAdapter$ViewHolderCardSelecaoDias;-><init>(Lcom/itau/empresas/feature/pendencias/PendenciasAdapter;Landroid/view/View;)V

    .line 261
    goto :goto_53

    .line 263
    :pswitch_2a
    new-instance v2, Lcom/itau/empresas/feature/pendencias/PendenciasAdapter$ViewHolderHeader;

    .line 264
    const v0, 0x7f03010d

    const/4 v1, 0x0

    invoke-virtual {v3, v0, p1, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/itau/empresas/feature/pendencias/PendenciasAdapter$ViewHolderHeader;-><init>(Landroid/view/View;)V

    .line 265
    goto :goto_53

    .line 267
    :pswitch_38
    new-instance v2, Lcom/itau/empresas/feature/pendencias/PendenciasAdapter$ViewHolderHeader;

    .line 268
    const v0, 0x7f03010d

    const/4 v1, 0x0

    invoke-virtual {v3, v0, p1, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/itau/empresas/feature/pendencias/PendenciasAdapter$ViewHolderHeader;-><init>(Landroid/view/View;)V

    .line 269
    goto :goto_53

    .line 271
    :pswitch_46
    new-instance v2, Lcom/itau/empresas/feature/pendencias/PendenciasAdapter$ViewHolderLista;

    .line 272
    const v0, 0x7f03010c

    const/4 v1, 0x0

    invoke-virtual {v3, v0, p1, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/itau/empresas/feature/pendencias/PendenciasAdapter$ViewHolderLista;-><init>(Landroid/view/View;)V

    .line 273
    .line 278
    :goto_53
    return-object v2

    :pswitch_data_54
    .packed-switch 0x0
        :pswitch_2a
        :pswitch_38
        :pswitch_46
        :pswitch_e
        :pswitch_1c
    .end packed-switch
.end method

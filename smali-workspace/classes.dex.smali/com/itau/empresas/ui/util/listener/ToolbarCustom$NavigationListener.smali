.class final Lcom/itau/empresas/ui/util/listener/ToolbarCustom$NavigationListener;
.super Ljava/lang/Object;
.source "ToolbarCustom.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/itau/empresas/ui/util/listener/ToolbarCustom;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "NavigationListener"
.end annotation


# instance fields
.field private final activity:Landroid/support/v7/app/AppCompatActivity;


# direct methods
.method private constructor <init>(Landroid/support/v7/app/AppCompatActivity;)V
    .registers 2
    .param p1, "activity"    # Landroid/support/v7/app/AppCompatActivity;

    .line 123
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 124
    iput-object p1, p0, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$NavigationListener;->activity:Landroid/support/v7/app/AppCompatActivity;

    .line 125
    return-void
.end method

.method synthetic constructor <init>(Landroid/support/v7/app/AppCompatActivity;Lcom/itau/empresas/ui/util/listener/ToolbarCustom$1;)V
    .registers 3
    .param p1, "x0"    # Landroid/support/v7/app/AppCompatActivity;
    .param p2, "x1"    # Lcom/itau/empresas/ui/util/listener/ToolbarCustom$1;

    .line 119
    invoke-direct {p0, p1}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$NavigationListener;-><init>(Landroid/support/v7/app/AppCompatActivity;)V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .registers 3
    .param p1, "v"    # Landroid/view/View;

    .line 129
    iget-object v0, p0, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$NavigationListener;->activity:Landroid/support/v7/app/AppCompatActivity;

    invoke-virtual {v0}, Landroid/support/v7/app/AppCompatActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->getBackStackEntryCount()I

    move-result v0

    if-lez v0, :cond_16

    .line 130
    iget-object v0, p0, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$NavigationListener;->activity:Landroid/support/v7/app/AppCompatActivity;

    invoke-virtual {v0}, Landroid/support/v7/app/AppCompatActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->popBackStack()V

    goto :goto_1b

    .line 132
    :cond_16
    iget-object v0, p0, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$NavigationListener;->activity:Landroid/support/v7/app/AppCompatActivity;

    invoke-virtual {v0}, Landroid/support/v7/app/AppCompatActivity;->onBackPressed()V

    .line 133
    :goto_1b
    return-void
.end method

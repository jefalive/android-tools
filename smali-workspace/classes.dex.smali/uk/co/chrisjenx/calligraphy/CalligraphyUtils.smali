.class public final Luk/co/chrisjenx/calligraphy/CalligraphyUtils;
.super Ljava/lang/Object;
.source "CalligraphyUtils.java"


# static fields
.field public static final ANDROID_ATTR_TEXT_APPEARANCE:[I

.field private static sAppCompatViewCheck:Ljava/lang/Boolean;

.field private static sToolbarCheck:Ljava/lang/Boolean;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .line 25
    const/4 v0, 0x1

    new-array v0, v0, [I

    fill-array-data v0, :array_10

    sput-object v0, Luk/co/chrisjenx/calligraphy/CalligraphyUtils;->ANDROID_ATTR_TEXT_APPEARANCE:[I

    .line 315
    const/4 v0, 0x0

    sput-object v0, Luk/co/chrisjenx/calligraphy/CalligraphyUtils;->sToolbarCheck:Ljava/lang/Boolean;

    .line 316
    const/4 v0, 0x0

    sput-object v0, Luk/co/chrisjenx/calligraphy/CalligraphyUtils;->sAppCompatViewCheck:Ljava/lang/Boolean;

    return-void

    nop

    :array_10
    .array-data 4
        0x1010034
    .end array-data
.end method

.method private constructor <init>()V
    .registers 1

    .line 352
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 353
    return-void
.end method

.method static applyFontToTextView(Landroid/content/Context;Landroid/widget/TextView;Luk/co/chrisjenx/calligraphy/CalligraphyConfig;)V
    .registers 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "textView"    # Landroid/widget/TextView;
    .param p2, "config"    # Luk/co/chrisjenx/calligraphy/CalligraphyConfig;

    .line 119
    const/4 v0, 0x0

    invoke-static {p0, p1, p2, v0}, Luk/co/chrisjenx/calligraphy/CalligraphyUtils;->applyFontToTextView(Landroid/content/Context;Landroid/widget/TextView;Luk/co/chrisjenx/calligraphy/CalligraphyConfig;Z)V

    .line 120
    return-void
.end method

.method public static applyFontToTextView(Landroid/content/Context;Landroid/widget/TextView;Luk/co/chrisjenx/calligraphy/CalligraphyConfig;Ljava/lang/String;)V
    .registers 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "textView"    # Landroid/widget/TextView;
    .param p2, "config"    # Luk/co/chrisjenx/calligraphy/CalligraphyConfig;
    .param p3, "textViewFont"    # Ljava/lang/String;

    .line 138
    const/4 v0, 0x0

    invoke-static {p0, p1, p2, p3, v0}, Luk/co/chrisjenx/calligraphy/CalligraphyUtils;->applyFontToTextView(Landroid/content/Context;Landroid/widget/TextView;Luk/co/chrisjenx/calligraphy/CalligraphyConfig;Ljava/lang/String;Z)V

    .line 139
    return-void
.end method

.method static applyFontToTextView(Landroid/content/Context;Landroid/widget/TextView;Luk/co/chrisjenx/calligraphy/CalligraphyConfig;Ljava/lang/String;Z)V
    .registers 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "textView"    # Landroid/widget/TextView;
    .param p2, "config"    # Luk/co/chrisjenx/calligraphy/CalligraphyConfig;
    .param p3, "textViewFont"    # Ljava/lang/String;
    .param p4, "deferred"    # Z

    .line 142
    if-eqz p0, :cond_6

    if-eqz p1, :cond_6

    if-nez p2, :cond_7

    :cond_6
    return-void

    .line 143
    :cond_7
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_14

    invoke-static {p0, p1, p3, p4}, Luk/co/chrisjenx/calligraphy/CalligraphyUtils;->applyFontToTextView(Landroid/content/Context;Landroid/widget/TextView;Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 144
    return-void

    .line 146
    :cond_14
    invoke-static {p0, p1, p2, p4}, Luk/co/chrisjenx/calligraphy/CalligraphyUtils;->applyFontToTextView(Landroid/content/Context;Landroid/widget/TextView;Luk/co/chrisjenx/calligraphy/CalligraphyConfig;Z)V

    .line 147
    return-void
.end method

.method static applyFontToTextView(Landroid/content/Context;Landroid/widget/TextView;Luk/co/chrisjenx/calligraphy/CalligraphyConfig;Z)V
    .registers 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "textView"    # Landroid/widget/TextView;
    .param p2, "config"    # Luk/co/chrisjenx/calligraphy/CalligraphyConfig;
    .param p3, "deferred"    # Z

    .line 123
    if-eqz p0, :cond_6

    if-eqz p1, :cond_6

    if-nez p2, :cond_7

    :cond_6
    return-void

    .line 124
    :cond_7
    invoke-virtual {p2}, Luk/co/chrisjenx/calligraphy/CalligraphyConfig;->isFontSet()Z

    move-result v0

    if-nez v0, :cond_e

    return-void

    .line 125
    :cond_e
    invoke-virtual {p2}, Luk/co/chrisjenx/calligraphy/CalligraphyConfig;->getFontPath()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, p1, v0, p3}, Luk/co/chrisjenx/calligraphy/CalligraphyUtils;->applyFontToTextView(Landroid/content/Context;Landroid/widget/TextView;Ljava/lang/String;Z)Z

    .line 126
    return-void
.end method

.method public static applyFontToTextView(Landroid/content/Context;Landroid/widget/TextView;Ljava/lang/String;)Z
    .registers 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "textView"    # Landroid/widget/TextView;
    .param p2, "filePath"    # Ljava/lang/String;

    .line 108
    const/4 v0, 0x0

    invoke-static {p0, p1, p2, v0}, Luk/co/chrisjenx/calligraphy/CalligraphyUtils;->applyFontToTextView(Landroid/content/Context;Landroid/widget/TextView;Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method static applyFontToTextView(Landroid/content/Context;Landroid/widget/TextView;Ljava/lang/String;Z)Z
    .registers 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "textView"    # Landroid/widget/TextView;
    .param p2, "filePath"    # Ljava/lang/String;
    .param p3, "deferred"    # Z

    .line 112
    if-eqz p1, :cond_4

    if-nez p0, :cond_6

    :cond_4
    const/4 v0, 0x0

    return v0

    .line 113
    :cond_6
    invoke-virtual {p0}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v1

    .line 114
    .local v1, "assetManager":Landroid/content/res/AssetManager;
    invoke-static {v1, p2}, Luk/co/chrisjenx/calligraphy/TypefaceUtils;->load(Landroid/content/res/AssetManager;Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v2

    .line 115
    .local v2, "typeface":Landroid/graphics/Typeface;
    invoke-static {p1, v2, p3}, Luk/co/chrisjenx/calligraphy/CalligraphyUtils;->applyFontToTextView(Landroid/widget/TextView;Landroid/graphics/Typeface;Z)Z

    move-result v0

    return v0
.end method

.method public static applyFontToTextView(Landroid/widget/TextView;Landroid/graphics/Typeface;)Z
    .registers 3
    .param p0, "textView"    # Landroid/widget/TextView;
    .param p1, "typeface"    # Landroid/graphics/Typeface;

    .line 56
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Luk/co/chrisjenx/calligraphy/CalligraphyUtils;->applyFontToTextView(Landroid/widget/TextView;Landroid/graphics/Typeface;Z)Z

    move-result v0

    return v0
.end method

.method public static applyFontToTextView(Landroid/widget/TextView;Landroid/graphics/Typeface;Z)Z
    .registers 5
    .param p0, "textView"    # Landroid/widget/TextView;
    .param p1, "typeface"    # Landroid/graphics/Typeface;
    .param p2, "deferred"    # Z

    .line 75
    if-eqz p0, :cond_4

    if-nez p1, :cond_6

    :cond_4
    const/4 v0, 0x0

    return v0

    .line 76
    :cond_6
    invoke-virtual {p0}, Landroid/widget/TextView;->getPaintFlags()I

    move-result v0

    or-int/lit16 v0, v0, 0x80

    or-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setPaintFlags(I)V

    .line 77
    invoke-virtual {p0, p1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 78
    if-eqz p2, :cond_2b

    .line 79
    invoke-virtual {p0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0, p1}, Luk/co/chrisjenx/calligraphy/CalligraphyUtils;->applyTypefaceSpan(Ljava/lang/CharSequence;Landroid/graphics/Typeface;)Ljava/lang/CharSequence;

    move-result-object v0

    sget-object v1, Landroid/widget/TextView$BufferType;->SPANNABLE:Landroid/widget/TextView$BufferType;

    invoke-virtual {p0, v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)V

    .line 80
    new-instance v0, Luk/co/chrisjenx/calligraphy/CalligraphyUtils$1;

    invoke-direct {v0, p1}, Luk/co/chrisjenx/calligraphy/CalligraphyUtils$1;-><init>(Landroid/graphics/Typeface;)V

    invoke-virtual {p0, v0}, Landroid/widget/TextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 95
    :cond_2b
    const/4 v0, 0x1

    return v0
.end method

.method public static applyTypefaceSpan(Ljava/lang/CharSequence;Landroid/graphics/Typeface;)Ljava/lang/CharSequence;
    .registers 7
    .param p0, "s"    # Ljava/lang/CharSequence;
    .param p1, "typeface"    # Landroid/graphics/Typeface;

    .line 35
    if-eqz p0, :cond_23

    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-lez v0, :cond_23

    .line 36
    instance-of v0, p0, Landroid/text/Spannable;

    if-nez v0, :cond_12

    .line 37
    new-instance v0, Landroid/text/SpannableString;

    invoke-direct {v0, p0}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    move-object p0, v0

    .line 39
    :cond_12
    move-object v0, p0

    check-cast v0, Landroid/text/Spannable;

    invoke-static {p1}, Luk/co/chrisjenx/calligraphy/TypefaceUtils;->getSpan(Landroid/graphics/Typeface;)Luk/co/chrisjenx/calligraphy/CalligraphyTypefaceSpan;

    move-result-object v1

    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    move-result v2

    const/4 v3, 0x0

    const/16 v4, 0x21

    invoke-interface {v0, v1, v3, v2, v4}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 41
    :cond_23
    return-object p0
.end method

.method static canAddV7AppCompatViews()Z
    .registers 2

    .line 341
    sget-object v0, Luk/co/chrisjenx/calligraphy/CalligraphyUtils;->sAppCompatViewCheck:Ljava/lang/Boolean;

    if-nez v0, :cond_13

    .line 343
    const-string v0, "android.support.v7.widget.AppCompatTextView"

    :try_start_6
    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    .line 344
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    sput-object v0, Luk/co/chrisjenx/calligraphy/CalligraphyUtils;->sAppCompatViewCheck:Ljava/lang/Boolean;
    :try_end_d
    .catch Ljava/lang/ClassNotFoundException; {:try_start_6 .. :try_end_d} :catch_e

    .line 347
    goto :goto_13

    .line 345
    :catch_e
    move-exception v1

    .line 346
    .local v1, "e":Ljava/lang/ClassNotFoundException;
    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    sput-object v0, Luk/co/chrisjenx/calligraphy/CalligraphyUtils;->sAppCompatViewCheck:Ljava/lang/Boolean;

    .line 349
    .end local v1    # "e":Ljava/lang/ClassNotFoundException;
    :cond_13
    :goto_13
    sget-object v0, Luk/co/chrisjenx/calligraphy/CalligraphyUtils;->sAppCompatViewCheck:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method static canCheckForV7Toolbar()Z
    .registers 2

    .line 324
    sget-object v0, Luk/co/chrisjenx/calligraphy/CalligraphyUtils;->sToolbarCheck:Ljava/lang/Boolean;

    if-nez v0, :cond_13

    .line 326
    const-string v0, "android.support.v7.widget.Toolbar"

    :try_start_6
    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    .line 327
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    sput-object v0, Luk/co/chrisjenx/calligraphy/CalligraphyUtils;->sToolbarCheck:Ljava/lang/Boolean;
    :try_end_d
    .catch Ljava/lang/ClassNotFoundException; {:try_start_6 .. :try_end_d} :catch_e

    .line 330
    goto :goto_13

    .line 328
    :catch_e
    move-exception v1

    .line 329
    .local v1, "e":Ljava/lang/ClassNotFoundException;
    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    sput-object v0, Luk/co/chrisjenx/calligraphy/CalligraphyUtils;->sToolbarCheck:Ljava/lang/Boolean;

    .line 332
    .end local v1    # "e":Ljava/lang/ClassNotFoundException;
    :cond_13
    :goto_13
    sget-object v0, Luk/co/chrisjenx/calligraphy/CalligraphyUtils;->sToolbarCheck:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method static pullFontPathFromStyle(Landroid/content/Context;Landroid/util/AttributeSet;[I)Ljava/lang/String;
    .registers 8
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "attrs"    # Landroid/util/AttributeSet;
    .param p2, "attributeId"    # [I

    .line 185
    if-eqz p2, :cond_4

    if-nez p1, :cond_6

    .line 186
    :cond_4
    const/4 v0, 0x0

    return-object v0

    .line 187
    :cond_6
    invoke-virtual {p0, p1, p2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 188
    .local v1, "typedArray":Landroid/content/res/TypedArray;
    if-eqz v1, :cond_2a

    .line 191
    const/4 v0, 0x0

    :try_start_d
    invoke-virtual {v1, v0}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 192
    .local v2, "fontFromAttribute":Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z
    :try_end_14
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_14} :catch_20
    .catchall {:try_start_d .. :try_end_14} :catchall_25

    move-result v0

    if-nez v0, :cond_1c

    .line 193
    move-object v3, v2

    .line 198
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    return-object v3

    .end local v2    # "fontFromAttribute":Ljava/lang/String;
    :cond_1c
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 199
    goto :goto_2a

    .line 195
    :catch_20
    move-exception v2

    .line 198
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 199
    goto :goto_2a

    .line 198
    :catchall_25
    move-exception v4

    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    throw v4

    .line 201
    :cond_2a
    :goto_2a
    const/4 v0, 0x0

    return-object v0
.end method

.method static pullFontPathFromTextAppearance(Landroid/content/Context;Landroid/util/AttributeSet;[I)Ljava/lang/String;
    .registers 11
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "attrs"    # Landroid/util/AttributeSet;
    .param p2, "attributeId"    # [I

    .line 213
    if-eqz p2, :cond_4

    if-nez p1, :cond_6

    .line 214
    :cond_4
    const/4 v0, 0x0

    return-object v0

    .line 217
    :cond_6
    const/4 v2, -0x1

    .line 218
    .local v2, "textAppearanceId":I
    sget-object v0, Luk/co/chrisjenx/calligraphy/CalligraphyUtils;->ANDROID_ATTR_TEXT_APPEARANCE:[I

    invoke-virtual {p0, p1, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v3

    .line 219
    .local v3, "typedArrayAttr":Landroid/content/res/TypedArray;
    if-eqz v3, :cond_24

    .line 221
    const/4 v0, 0x0

    const/4 v1, -0x1

    :try_start_11
    invoke-virtual {v3, v0, v1}, Landroid/content/res/TypedArray;->getResourceId(II)I
    :try_end_14
    .catch Ljava/lang/Exception; {:try_start_11 .. :try_end_14} :catch_19
    .catchall {:try_start_11 .. :try_end_14} :catchall_1f

    move-result v2

    .line 226
    invoke-virtual {v3}, Landroid/content/res/TypedArray;->recycle()V

    .line 227
    goto :goto_24

    .line 222
    :catch_19
    move-exception v4

    .line 224
    .local v4, "ignored":Ljava/lang/Exception;
    const/4 v5, 0x0

    .line 226
    invoke-virtual {v3}, Landroid/content/res/TypedArray;->recycle()V

    return-object v5

    .end local v4    # "ignored":Ljava/lang/Exception;
    :catchall_1f
    move-exception v6

    invoke-virtual {v3}, Landroid/content/res/TypedArray;->recycle()V

    throw v6

    .line 230
    :cond_24
    :goto_24
    invoke-virtual {p0, v2, p2}, Landroid/content/Context;->obtainStyledAttributes(I[I)Landroid/content/res/TypedArray;

    move-result-object v4

    .line 231
    .local v4, "textAppearanceAttrs":Landroid/content/res/TypedArray;
    if-eqz v4, :cond_3e

    .line 233
    const/4 v0, 0x0

    :try_start_2b
    invoke-virtual {v4, v0}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;
    :try_end_2e
    .catch Ljava/lang/Exception; {:try_start_2b .. :try_end_2e} :catch_33
    .catchall {:try_start_2b .. :try_end_2e} :catchall_39

    move-result-object v5

    .line 238
    invoke-virtual {v4}, Landroid/content/res/TypedArray;->recycle()V

    return-object v5

    .line 234
    :catch_33
    move-exception v5

    .line 236
    .local v5, "ignore":Ljava/lang/Exception;
    const/4 v6, 0x0

    .line 238
    invoke-virtual {v4}, Landroid/content/res/TypedArray;->recycle()V

    return-object v6

    .end local v5    # "ignore":Ljava/lang/Exception;
    :catchall_39
    move-exception v7

    invoke-virtual {v4}, Landroid/content/res/TypedArray;->recycle()V

    throw v7

    .line 241
    :cond_3e
    const/4 v0, 0x0

    return-object v0
.end method

.method static pullFontPathFromTheme(Landroid/content/Context;II[I)Ljava/lang/String;
    .registers 15
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "styleAttrId"    # I
    .param p2, "subStyleAttrId"    # I
    .param p3, "attributeId"    # [I

    .line 282
    const/4 v0, -0x1

    if-eq p1, v0, :cond_5

    if-nez p3, :cond_7

    .line 283
    :cond_5
    const/4 v0, 0x0

    return-object v0

    .line 285
    :cond_7
    invoke-virtual {p0}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v3

    .line 286
    .local v3, "theme":Landroid/content/res/Resources$Theme;
    new-instance v4, Landroid/util/TypedValue;

    invoke-direct {v4}, Landroid/util/TypedValue;-><init>()V

    .line 288
    .local v4, "value":Landroid/util/TypedValue;
    const/4 v0, 0x1

    invoke-virtual {v3, p1, v4, v0}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 289
    const/4 v5, -0x1

    .line 290
    .local v5, "subStyleResId":I
    iget v0, v4, Landroid/util/TypedValue;->resourceId:I

    const/4 v1, 0x1

    new-array v1, v1, [I

    const/4 v2, 0x0

    aput p2, v1, v2

    invoke-virtual {v3, v0, v1}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(I[I)Landroid/content/res/TypedArray;

    move-result-object v6

    .line 292
    .local v6, "parentTypedArray":Landroid/content/res/TypedArray;
    const/4 v0, 0x0

    const/4 v1, -0x1

    :try_start_23
    invoke-virtual {v6, v0, v1}, Landroid/content/res/TypedArray;->getResourceId(II)I
    :try_end_26
    .catch Ljava/lang/Exception; {:try_start_23 .. :try_end_26} :catch_2b
    .catchall {:try_start_23 .. :try_end_26} :catchall_31

    move-result v5

    .line 297
    invoke-virtual {v6}, Landroid/content/res/TypedArray;->recycle()V

    .line 298
    goto :goto_36

    .line 293
    :catch_2b
    move-exception v7

    .line 295
    .local v7, "ignore":Ljava/lang/Exception;
    const/4 v8, 0x0

    .line 297
    invoke-virtual {v6}, Landroid/content/res/TypedArray;->recycle()V

    return-object v8

    .end local v7    # "ignore":Ljava/lang/Exception;
    :catchall_31
    move-exception v9

    invoke-virtual {v6}, Landroid/content/res/TypedArray;->recycle()V

    throw v9

    .line 300
    :goto_36
    const/4 v0, -0x1

    if-ne v5, v0, :cond_3b

    const/4 v0, 0x0

    return-object v0

    .line 301
    :cond_3b
    invoke-virtual {p0, v5, p3}, Landroid/content/Context;->obtainStyledAttributes(I[I)Landroid/content/res/TypedArray;

    move-result-object v7

    .line 302
    .local v7, "subTypedArray":Landroid/content/res/TypedArray;
    if-eqz v7, :cond_55

    .line 304
    const/4 v0, 0x0

    :try_start_42
    invoke-virtual {v7, v0}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;
    :try_end_45
    .catch Ljava/lang/Exception; {:try_start_42 .. :try_end_45} :catch_4a
    .catchall {:try_start_42 .. :try_end_45} :catchall_50

    move-result-object v8

    .line 309
    invoke-virtual {v7}, Landroid/content/res/TypedArray;->recycle()V

    return-object v8

    .line 305
    :catch_4a
    move-exception v8

    .line 307
    .local v8, "ignore":Ljava/lang/Exception;
    const/4 v9, 0x0

    .line 309
    invoke-virtual {v7}, Landroid/content/res/TypedArray;->recycle()V

    return-object v9

    .end local v8    # "ignore":Ljava/lang/Exception;
    :catchall_50
    move-exception v10

    invoke-virtual {v7}, Landroid/content/res/TypedArray;->recycle()V

    throw v10

    .line 312
    :cond_55
    const/4 v0, 0x0

    return-object v0
.end method

.method static pullFontPathFromTheme(Landroid/content/Context;I[I)Ljava/lang/String;
    .registers 10
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "styleAttrId"    # I
    .param p2, "attributeId"    # [I

    .line 253
    const/4 v0, -0x1

    if-eq p1, v0, :cond_5

    if-nez p2, :cond_7

    .line 254
    :cond_5
    const/4 v0, 0x0

    return-object v0

    .line 256
    :cond_7
    invoke-virtual {p0}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v1

    .line 257
    .local v1, "theme":Landroid/content/res/Resources$Theme;
    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    .line 259
    .local v2, "value":Landroid/util/TypedValue;
    const/4 v0, 0x1

    invoke-virtual {v1, p1, v2, v0}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 260
    iget v0, v2, Landroid/util/TypedValue;->resourceId:I

    invoke-virtual {v1, v0, p2}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(I[I)Landroid/content/res/TypedArray;

    move-result-object v3

    .line 262
    .local v3, "typedArray":Landroid/content/res/TypedArray;
    const/4 v0, 0x0

    :try_start_1b
    invoke-virtual {v3, v0}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;
    :try_end_1e
    .catch Ljava/lang/Exception; {:try_start_1b .. :try_end_1e} :catch_24
    .catchall {:try_start_1b .. :try_end_1e} :catchall_2a

    move-result-object v4

    .line 263
    .local v4, "font":Ljava/lang/String;
    move-object v5, v4

    .line 268
    invoke-virtual {v3}, Landroid/content/res/TypedArray;->recycle()V

    return-object v5

    .line 264
    .end local v4    # "font":Ljava/lang/String;
    :catch_24
    move-exception v4

    .line 266
    .local v4, "ignore":Ljava/lang/Exception;
    const/4 v5, 0x0

    .line 268
    invoke-virtual {v3}, Landroid/content/res/TypedArray;->recycle()V

    return-object v5

    .end local v4    # "ignore":Ljava/lang/Exception;
    :catchall_2a
    move-exception v6

    invoke-virtual {v3}, Landroid/content/res/TypedArray;->recycle()V

    throw v6
.end method

.method static pullFontPathFromView(Landroid/content/Context;Landroid/util/AttributeSet;[I)Ljava/lang/String;
    .registers 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "attrs"    # Landroid/util/AttributeSet;
    .param p2, "attributeId"    # [I

    .line 158
    if-eqz p2, :cond_4

    if-nez p1, :cond_6

    .line 159
    :cond_4
    const/4 v0, 0x0

    return-object v0

    .line 163
    :cond_6
    :try_start_6
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/4 v1, 0x0

    aget v1, p2, v1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getResourceEntryName(I)Ljava/lang/String;
    :try_end_10
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_6 .. :try_end_10} :catch_12

    move-result-object v2

    .line 167
    .local v2, "attributeName":Ljava/lang/String;
    goto :goto_15

    .line 164
    .end local v2    # "attributeName":Ljava/lang/String;
    :catch_12
    move-exception v3

    .line 166
    .local v3, "e":Landroid/content/res/Resources$NotFoundException;
    const/4 v0, 0x0

    return-object v0

    .line 169
    .local v2, "attributeName":Ljava/lang/String;
    .end local v3    # "e":Landroid/content/res/Resources$NotFoundException;
    :goto_15
    const/4 v0, 0x0

    const/4 v1, -0x1

    invoke-interface {p1, v0, v2, v1}, Landroid/util/AttributeSet;->getAttributeResourceValue(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v3

    .line 170
    .local v3, "stringResourceId":I
    if-lez v3, :cond_22

    .line 171
    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_27

    .line 172
    :cond_22
    const/4 v0, 0x0

    invoke-interface {p1, v0, v2}, Landroid/util/AttributeSet;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_27
    return-object v0
.end method

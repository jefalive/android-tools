.class public abstract Lcom/google/android/gms/common/internal/zzj$zzc;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/common/internal/zzj;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x404
    name = "zzc"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<TListener:Ljava/lang/Object;>Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private mListener:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TTListener;"
        }
    .end annotation
.end field

.field final synthetic zzalL:Lcom/google/android/gms/common/internal/zzj;

.field private zzalM:Z


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/internal/zzj;Ljava/lang/Object;)V
    .registers 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TTListener;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/google/android/gms/common/internal/zzj$zzc;->zzalL:Lcom/google/android/gms/common/internal/zzj;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/google/android/gms/common/internal/zzj$zzc;->mListener:Ljava/lang/Object;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/common/internal/zzj$zzc;->zzalM:Z

    return-void
.end method


# virtual methods
.method public unregister()V
    .registers 4

    invoke-virtual {p0}, Lcom/google/android/gms/common/internal/zzj$zzc;->zzqO()V

    iget-object v0, p0, Lcom/google/android/gms/common/internal/zzj$zzc;->zzalL:Lcom/google/android/gms/common/internal/zzj;

    invoke-static {v0}, Lcom/google/android/gms/common/internal/zzj;->zzd(Lcom/google/android/gms/common/internal/zzj;)Ljava/util/ArrayList;

    move-result-object v1

    monitor-enter v1

    :try_start_a
    iget-object v0, p0, Lcom/google/android/gms/common/internal/zzj$zzc;->zzalL:Lcom/google/android/gms/common/internal/zzj;

    invoke-static {v0}, Lcom/google/android/gms/common/internal/zzj;->zzd(Lcom/google/android/gms/common/internal/zzj;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z
    :try_end_13
    .catchall {:try_start_a .. :try_end_13} :catchall_15

    monitor-exit v1

    goto :goto_18

    :catchall_15
    move-exception v2

    monitor-exit v1

    throw v2

    :goto_18
    return-void
.end method

.method protected abstract zzqM()V
.end method

.method public zzqN()V
    .registers 8

    move-object v4, p0

    monitor-enter v4

    :try_start_2
    iget-object v3, p0, Lcom/google/android/gms/common/internal/zzj$zzc;->mListener:Ljava/lang/Object;

    iget-boolean v0, p0, Lcom/google/android/gms/common/internal/zzj$zzc;->zzalM:Z

    if-eqz v0, :cond_26

    const-string v0, "GmsClient"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Callback proxy "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " being reused. This is not safe."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_26
    .catchall {:try_start_2 .. :try_end_26} :catchall_28

    :cond_26
    monitor-exit v4

    goto :goto_2b

    :catchall_28
    move-exception v5

    monitor-exit v4

    throw v5

    :goto_2b
    if-eqz v3, :cond_36

    :try_start_2d
    invoke-virtual {p0, v3}, Lcom/google/android/gms/common/internal/zzj$zzc;->zzw(Ljava/lang/Object;)V
    :try_end_30
    .catch Ljava/lang/RuntimeException; {:try_start_2d .. :try_end_30} :catch_31

    goto :goto_39

    :catch_31
    move-exception v4

    invoke-virtual {p0}, Lcom/google/android/gms/common/internal/zzj$zzc;->zzqM()V

    throw v4

    :cond_36
    invoke-virtual {p0}, Lcom/google/android/gms/common/internal/zzj$zzc;->zzqM()V

    :goto_39
    move-object v4, p0

    monitor-enter v4

    const/4 v0, 0x1

    :try_start_3c
    iput-boolean v0, p0, Lcom/google/android/gms/common/internal/zzj$zzc;->zzalM:Z
    :try_end_3e
    .catchall {:try_start_3c .. :try_end_3e} :catchall_40

    monitor-exit v4

    goto :goto_43

    :catchall_40
    move-exception v6

    monitor-exit v4

    throw v6

    :goto_43
    invoke-virtual {p0}, Lcom/google/android/gms/common/internal/zzj$zzc;->unregister()V

    return-void
.end method

.method public zzqO()V
    .registers 4

    move-object v1, p0

    monitor-enter v1

    const/4 v0, 0x0

    :try_start_3
    iput-object v0, p0, Lcom/google/android/gms/common/internal/zzj$zzc;->mListener:Ljava/lang/Object;
    :try_end_5
    .catchall {:try_start_3 .. :try_end_5} :catchall_7

    monitor-exit v1

    goto :goto_a

    :catchall_7
    move-exception v2

    monitor-exit v1

    throw v2

    :goto_a
    return-void
.end method

.method protected abstract zzw(Ljava/lang/Object;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TTListener;)V"
        }
    .end annotation
.end method

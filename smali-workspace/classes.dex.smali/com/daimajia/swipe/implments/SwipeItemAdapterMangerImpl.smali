.class public Lcom/daimajia/swipe/implments/SwipeItemAdapterMangerImpl;
.super Lcom/daimajia/swipe/implments/SwipeItemMangerImpl;
.source "SwipeItemAdapterMangerImpl.java"


# instance fields
.field protected mAdapter:Landroid/widget/BaseAdapter;


# direct methods
.method public constructor <init>(Landroid/widget/BaseAdapter;)V
    .registers 2
    .param p1, "adapter"    # Landroid/widget/BaseAdapter;

    .line 26
    invoke-direct {p0, p1}, Lcom/daimajia/swipe/implments/SwipeItemMangerImpl;-><init>(Landroid/widget/BaseAdapter;)V

    .line 27
    iput-object p1, p0, Lcom/daimajia/swipe/implments/SwipeItemAdapterMangerImpl;->mAdapter:Landroid/widget/BaseAdapter;

    .line 28
    return-void
.end method


# virtual methods
.method public initialize(Landroid/view/View;I)V
    .registers 9
    .param p1, "target"    # Landroid/view/View;
    .param p2, "position"    # I

    .line 32
    invoke-virtual {p0, p2}, Lcom/daimajia/swipe/implments/SwipeItemAdapterMangerImpl;->getSwipeLayoutId(I)I

    move-result v2

    .line 34
    .local v2, "resId":I
    new-instance v3, Lcom/daimajia/swipe/implments/SwipeItemMangerImpl$OnLayoutListener;

    invoke-direct {v3, p0, p2}, Lcom/daimajia/swipe/implments/SwipeItemMangerImpl$OnLayoutListener;-><init>(Lcom/daimajia/swipe/implments/SwipeItemMangerImpl;I)V

    .line 35
    .local v3, "onLayoutListener":Lcom/daimajia/swipe/implments/SwipeItemMangerImpl$OnLayoutListener;
    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/daimajia/swipe/SwipeLayout;

    .line 36
    .local v4, "swipeLayout":Lcom/daimajia/swipe/SwipeLayout;
    if-nez v4, :cond_1a

    .line 37
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "can not find SwipeLayout in target view"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 39
    :cond_1a
    new-instance v5, Lcom/daimajia/swipe/implments/SwipeItemMangerImpl$SwipeMemory;

    invoke-direct {v5, p0, p2}, Lcom/daimajia/swipe/implments/SwipeItemMangerImpl$SwipeMemory;-><init>(Lcom/daimajia/swipe/implments/SwipeItemMangerImpl;I)V

    .line 40
    .local v5, "swipeMemory":Lcom/daimajia/swipe/implments/SwipeItemMangerImpl$SwipeMemory;
    invoke-virtual {v4, v5}, Lcom/daimajia/swipe/SwipeLayout;->addSwipeListener(Lcom/daimajia/swipe/SwipeLayout$SwipeListener;)V

    .line 41
    invoke-virtual {v4, v3}, Lcom/daimajia/swipe/SwipeLayout;->addOnLayoutListener(Lcom/daimajia/swipe/SwipeLayout$OnLayout;)V

    .line 42
    new-instance v0, Lcom/daimajia/swipe/implments/SwipeItemMangerImpl$ValueBox;

    invoke-direct {v0, p0, p2, v5, v3}, Lcom/daimajia/swipe/implments/SwipeItemMangerImpl$ValueBox;-><init>(Lcom/daimajia/swipe/implments/SwipeItemMangerImpl;ILcom/daimajia/swipe/implments/SwipeItemMangerImpl$SwipeMemory;Lcom/daimajia/swipe/implments/SwipeItemMangerImpl$OnLayoutListener;)V

    invoke-virtual {v4, v2, v0}, Lcom/daimajia/swipe/SwipeLayout;->setTag(ILjava/lang/Object;)V

    .line 44
    iget-object v0, p0, Lcom/daimajia/swipe/implments/SwipeItemAdapterMangerImpl;->mShownLayouts:Ljava/util/Set;

    invoke-interface {v0, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 45
    return-void
.end method

.method public updateConvertView(Landroid/view/View;I)V
    .registers 8
    .param p1, "target"    # Landroid/view/View;
    .param p2, "position"    # I

    .line 49
    invoke-virtual {p0, p2}, Lcom/daimajia/swipe/implments/SwipeItemAdapterMangerImpl;->getSwipeLayoutId(I)I

    move-result v2

    .line 51
    .local v2, "resId":I
    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/daimajia/swipe/SwipeLayout;

    .line 52
    .local v3, "swipeLayout":Lcom/daimajia/swipe/SwipeLayout;
    if-nez v3, :cond_15

    .line 53
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "can not find SwipeLayout in target view"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 55
    :cond_15
    invoke-virtual {v3, v2}, Lcom/daimajia/swipe/SwipeLayout;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/daimajia/swipe/implments/SwipeItemMangerImpl$ValueBox;

    .line 56
    .local v4, "valueBox":Lcom/daimajia/swipe/implments/SwipeItemMangerImpl$ValueBox;
    iget-object v0, v4, Lcom/daimajia/swipe/implments/SwipeItemMangerImpl$ValueBox;->swipeMemory:Lcom/daimajia/swipe/implments/SwipeItemMangerImpl$SwipeMemory;

    invoke-virtual {v0, p2}, Lcom/daimajia/swipe/implments/SwipeItemMangerImpl$SwipeMemory;->setPosition(I)V

    .line 57
    iget-object v0, v4, Lcom/daimajia/swipe/implments/SwipeItemMangerImpl$ValueBox;->onLayoutListener:Lcom/daimajia/swipe/implments/SwipeItemMangerImpl$OnLayoutListener;

    invoke-virtual {v0, p2}, Lcom/daimajia/swipe/implments/SwipeItemMangerImpl$OnLayoutListener;->setPosition(I)V

    .line 58
    iput p2, v4, Lcom/daimajia/swipe/implments/SwipeItemMangerImpl$ValueBox;->position:I

    .line 59
    return-void
.end method

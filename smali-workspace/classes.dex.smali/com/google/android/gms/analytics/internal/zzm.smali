.class public final enum Lcom/google/android/gms/analytics/internal/zzm;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<Lcom/google/android/gms/analytics/internal/zzm;>;"
    }
.end annotation


# static fields
.field public static final enum zzRk:Lcom/google/android/gms/analytics/internal/zzm;

.field public static final enum zzRl:Lcom/google/android/gms/analytics/internal/zzm;

.field public static final enum zzRm:Lcom/google/android/gms/analytics/internal/zzm;

.field public static final enum zzRn:Lcom/google/android/gms/analytics/internal/zzm;

.field public static final enum zzRo:Lcom/google/android/gms/analytics/internal/zzm;

.field public static final enum zzRp:Lcom/google/android/gms/analytics/internal/zzm;

.field private static final synthetic zzRq:[Lcom/google/android/gms/analytics/internal/zzm;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    new-instance v0, Lcom/google/android/gms/analytics/internal/zzm;

    const-string v1, "NONE"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/analytics/internal/zzm;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/analytics/internal/zzm;->zzRk:Lcom/google/android/gms/analytics/internal/zzm;

    new-instance v0, Lcom/google/android/gms/analytics/internal/zzm;

    const-string v1, "BATCH_BY_SESSION"

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/analytics/internal/zzm;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/analytics/internal/zzm;->zzRl:Lcom/google/android/gms/analytics/internal/zzm;

    new-instance v0, Lcom/google/android/gms/analytics/internal/zzm;

    const-string v1, "BATCH_BY_TIME"

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/analytics/internal/zzm;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/analytics/internal/zzm;->zzRm:Lcom/google/android/gms/analytics/internal/zzm;

    new-instance v0, Lcom/google/android/gms/analytics/internal/zzm;

    const-string v1, "BATCH_BY_BRUTE_FORCE"

    const/4 v2, 0x3

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/analytics/internal/zzm;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/analytics/internal/zzm;->zzRn:Lcom/google/android/gms/analytics/internal/zzm;

    new-instance v0, Lcom/google/android/gms/analytics/internal/zzm;

    const-string v1, "BATCH_BY_COUNT"

    const/4 v2, 0x4

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/analytics/internal/zzm;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/analytics/internal/zzm;->zzRo:Lcom/google/android/gms/analytics/internal/zzm;

    new-instance v0, Lcom/google/android/gms/analytics/internal/zzm;

    const-string v1, "BATCH_BY_SIZE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/analytics/internal/zzm;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/analytics/internal/zzm;->zzRp:Lcom/google/android/gms/analytics/internal/zzm;

    const/4 v0, 0x6

    new-array v0, v0, [Lcom/google/android/gms/analytics/internal/zzm;

    sget-object v1, Lcom/google/android/gms/analytics/internal/zzm;->zzRk:Lcom/google/android/gms/analytics/internal/zzm;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/gms/analytics/internal/zzm;->zzRl:Lcom/google/android/gms/analytics/internal/zzm;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/gms/analytics/internal/zzm;->zzRm:Lcom/google/android/gms/analytics/internal/zzm;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/gms/analytics/internal/zzm;->zzRn:Lcom/google/android/gms/analytics/internal/zzm;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/gms/analytics/internal/zzm;->zzRo:Lcom/google/android/gms/analytics/internal/zzm;

    const/4 v2, 0x4

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/gms/analytics/internal/zzm;->zzRp:Lcom/google/android/gms/analytics/internal/zzm;

    const/4 v2, 0x5

    aput-object v1, v0, v2

    sput-object v0, Lcom/google/android/gms/analytics/internal/zzm;->zzRq:[Lcom/google/android/gms/analytics/internal/zzm;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/gms/analytics/internal/zzm;
    .registers 2
    .param p0, "name"    # Ljava/lang/String;

    const-class v0, Lcom/google/android/gms/analytics/internal/zzm;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/analytics/internal/zzm;

    return-object v0
.end method

.method public static values()[Lcom/google/android/gms/analytics/internal/zzm;
    .registers 1

    sget-object v0, Lcom/google/android/gms/analytics/internal/zzm;->zzRq:[Lcom/google/android/gms/analytics/internal/zzm;

    invoke-virtual {v0}, [Lcom/google/android/gms/analytics/internal/zzm;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/gms/analytics/internal/zzm;

    return-object v0
.end method

.method public static zzbm(Ljava/lang/String;)Lcom/google/android/gms/analytics/internal/zzm;
    .registers 2

    const-string v0, "BATCH_BY_SESSION"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b

    sget-object v0, Lcom/google/android/gms/analytics/internal/zzm;->zzRl:Lcom/google/android/gms/analytics/internal/zzm;

    return-object v0

    :cond_b
    const-string v0, "BATCH_BY_TIME"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_16

    sget-object v0, Lcom/google/android/gms/analytics/internal/zzm;->zzRm:Lcom/google/android/gms/analytics/internal/zzm;

    return-object v0

    :cond_16
    const-string v0, "BATCH_BY_BRUTE_FORCE"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_21

    sget-object v0, Lcom/google/android/gms/analytics/internal/zzm;->zzRn:Lcom/google/android/gms/analytics/internal/zzm;

    return-object v0

    :cond_21
    const-string v0, "BATCH_BY_COUNT"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2c

    sget-object v0, Lcom/google/android/gms/analytics/internal/zzm;->zzRo:Lcom/google/android/gms/analytics/internal/zzm;

    return-object v0

    :cond_2c
    const-string v0, "BATCH_BY_SIZE"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_37

    sget-object v0, Lcom/google/android/gms/analytics/internal/zzm;->zzRp:Lcom/google/android/gms/analytics/internal/zzm;

    return-object v0

    :cond_37
    sget-object v0, Lcom/google/android/gms/analytics/internal/zzm;->zzRk:Lcom/google/android/gms/analytics/internal/zzm;

    return-object v0
.end method

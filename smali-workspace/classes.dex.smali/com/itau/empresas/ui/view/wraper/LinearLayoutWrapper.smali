.class public Lcom/itau/empresas/ui/view/wraper/LinearLayoutWrapper;
.super Ljava/lang/Object;
.source "LinearLayoutWrapper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/itau/empresas/ui/view/wraper/LinearLayoutWrapper$ListViewAdapterObserver;,
        Lcom/itau/empresas/ui/view/wraper/LinearLayoutWrapper$OnDataSetChanged;,
        Lcom/itau/empresas/ui/view/wraper/LinearLayoutWrapper$ListViewAdapterInterator;,
        Lcom/itau/empresas/ui/view/wraper/LinearLayoutWrapper$AdapterInterator;
    }
.end annotation


# instance fields
.field private adapter:Landroid/widget/Adapter;

.field private final interator:Lcom/itau/empresas/ui/view/wraper/LinearLayoutWrapper$AdapterInterator;

.field private final layout:Landroid/widget/LinearLayout;

.field private final mainHandler:Landroid/os/Handler;

.field private register:Ljava/lang/Runnable;

.field private unregister:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Landroid/widget/LinearLayout;Landroid/widget/Adapter;)V
    .registers 5
    .param p1, "layout"    # Landroid/widget/LinearLayout;
    .param p2, "adapter"    # Landroid/widget/Adapter;

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/itau/empresas/ui/view/wraper/LinearLayoutWrapper;->mainHandler:Landroid/os/Handler;

    .line 27
    iput-object p1, p0, Lcom/itau/empresas/ui/view/wraper/LinearLayoutWrapper;->layout:Landroid/widget/LinearLayout;

    .line 28
    invoke-direct {p0}, Lcom/itau/empresas/ui/view/wraper/LinearLayoutWrapper;->removeBuiltViews()V

    .line 29
    new-instance v0, Lcom/itau/empresas/ui/view/wraper/LinearLayoutWrapper$ListViewAdapterInterator;

    invoke-direct {v0, p2}, Lcom/itau/empresas/ui/view/wraper/LinearLayoutWrapper$ListViewAdapterInterator;-><init>(Landroid/widget/Adapter;)V

    iput-object v0, p0, Lcom/itau/empresas/ui/view/wraper/LinearLayoutWrapper;->interator:Lcom/itau/empresas/ui/view/wraper/LinearLayoutWrapper$AdapterInterator;

    .line 30
    iput-object p2, p0, Lcom/itau/empresas/ui/view/wraper/LinearLayoutWrapper;->adapter:Landroid/widget/Adapter;

    .line 31
    return-void
.end method

.method static synthetic access$000(Lcom/itau/empresas/ui/view/wraper/LinearLayoutWrapper;)V
    .registers 1
    .param p0, "x0"    # Lcom/itau/empresas/ui/view/wraper/LinearLayoutWrapper;

    .line 14
    invoke-direct {p0}, Lcom/itau/empresas/ui/view/wraper/LinearLayoutWrapper;->cleanAndBuildViews()V

    return-void
.end method

.method static synthetic access$100(Lcom/itau/empresas/ui/view/wraper/LinearLayoutWrapper;)V
    .registers 1
    .param p0, "x0"    # Lcom/itau/empresas/ui/view/wraper/LinearLayoutWrapper;

    .line 14
    invoke-direct {p0}, Lcom/itau/empresas/ui/view/wraper/LinearLayoutWrapper;->removeBuiltViews()V

    return-void
.end method

.method static synthetic access$200(Lcom/itau/empresas/ui/view/wraper/LinearLayoutWrapper;)V
    .registers 1
    .param p0, "x0"    # Lcom/itau/empresas/ui/view/wraper/LinearLayoutWrapper;

    .line 14
    invoke-direct {p0}, Lcom/itau/empresas/ui/view/wraper/LinearLayoutWrapper;->tryBuildViews()V

    return-void
.end method

.method private addView(Landroid/view/View;)V
    .registers 3
    .param p1, "view"    # Landroid/view/View;

    .line 133
    iget-object v0, p0, Lcom/itau/empresas/ui/view/wraper/LinearLayoutWrapper;->layout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 134
    return-void
.end method

.method private buildViews()V
    .registers 4

    .line 127
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    iget-object v0, p0, Lcom/itau/empresas/ui/view/wraper/LinearLayoutWrapper;->interator:Lcom/itau/empresas/ui/view/wraper/LinearLayoutWrapper$AdapterInterator;

    invoke-interface {v0}, Lcom/itau/empresas/ui/view/wraper/LinearLayoutWrapper$AdapterInterator;->getCount()I

    move-result v0

    if-ge v2, v0, :cond_17

    .line 128
    iget-object v0, p0, Lcom/itau/empresas/ui/view/wraper/LinearLayoutWrapper;->interator:Lcom/itau/empresas/ui/view/wraper/LinearLayoutWrapper$AdapterInterator;

    iget-object v1, p0, Lcom/itau/empresas/ui/view/wraper/LinearLayoutWrapper;->layout:Landroid/widget/LinearLayout;

    invoke-interface {v0, v2, v1}, Lcom/itau/empresas/ui/view/wraper/LinearLayoutWrapper$AdapterInterator;->getView(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/itau/empresas/ui/view/wraper/LinearLayoutWrapper;->addView(Landroid/view/View;)V

    .line 127
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 129
    .end local v2    # "i":I
    :cond_17
    return-void
.end method

.method private cleanAndBuildViews()V
    .registers 3

    .line 107
    iget-object v0, p0, Lcom/itau/empresas/ui/view/wraper/LinearLayoutWrapper;->mainHandler:Landroid/os/Handler;

    new-instance v1, Lcom/itau/empresas/ui/view/wraper/LinearLayoutWrapper$7;

    invoke-direct {v1, p0}, Lcom/itau/empresas/ui/view/wraper/LinearLayoutWrapper$7;-><init>(Lcom/itau/empresas/ui/view/wraper/LinearLayoutWrapper;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 114
    return-void
.end method

.method private declared-synchronized removeBuiltViews()V
    .registers 3

    monitor-enter p0

    .line 139
    :try_start_1
    iget-object v0, p0, Lcom/itau/empresas/ui/view/wraper/LinearLayoutWrapper;->layout:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V
    :try_end_6
    .catchall {:try_start_1 .. :try_end_6} :catchall_8

    .line 140
    monitor-exit p0

    return-void

    :catchall_8
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method private setupAdapter(Landroid/widget/Adapter;)V
    .registers 4
    .param p1, "adapter"    # Landroid/widget/Adapter;

    .line 47
    new-instance v1, Lcom/itau/empresas/ui/view/wraper/LinearLayoutWrapper$ListViewAdapterObserver;

    new-instance v0, Lcom/itau/empresas/ui/view/wraper/LinearLayoutWrapper$1;

    invoke-direct {v0, p0}, Lcom/itau/empresas/ui/view/wraper/LinearLayoutWrapper$1;-><init>(Lcom/itau/empresas/ui/view/wraper/LinearLayoutWrapper;)V

    invoke-direct {v1, v0}, Lcom/itau/empresas/ui/view/wraper/LinearLayoutWrapper$ListViewAdapterObserver;-><init>(Lcom/itau/empresas/ui/view/wraper/LinearLayoutWrapper$OnDataSetChanged;)V

    .line 53
    .local v1, "observer":Landroid/database/DataSetObserver;
    invoke-direct {p0}, Lcom/itau/empresas/ui/view/wraper/LinearLayoutWrapper;->buildViews()V

    .line 54
    new-instance v0, Lcom/itau/empresas/ui/view/wraper/LinearLayoutWrapper$2;

    invoke-direct {v0, p0, p1, v1}, Lcom/itau/empresas/ui/view/wraper/LinearLayoutWrapper$2;-><init>(Lcom/itau/empresas/ui/view/wraper/LinearLayoutWrapper;Landroid/widget/Adapter;Landroid/database/DataSetObserver;)V

    iput-object v0, p0, Lcom/itau/empresas/ui/view/wraper/LinearLayoutWrapper;->register:Ljava/lang/Runnable;

    .line 64
    new-instance v0, Lcom/itau/empresas/ui/view/wraper/LinearLayoutWrapper$3;

    invoke-direct {v0, p0, p1, v1}, Lcom/itau/empresas/ui/view/wraper/LinearLayoutWrapper$3;-><init>(Lcom/itau/empresas/ui/view/wraper/LinearLayoutWrapper;Landroid/widget/Adapter;Landroid/database/DataSetObserver;)V

    iput-object v0, p0, Lcom/itau/empresas/ui/view/wraper/LinearLayoutWrapper;->unregister:Ljava/lang/Runnable;

    .line 74
    return-void
.end method

.method private tryBuildViews()V
    .registers 4

    .line 119
    :try_start_0
    invoke-direct {p0}, Lcom/itau/empresas/ui/view/wraper/LinearLayoutWrapper;->buildViews()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_3} :catch_4

    .line 122
    goto :goto_c

    .line 120
    :catch_4
    move-exception v2

    .line 121
    .local v2, "e":Ljava/lang/Exception;
    const-string v0, "LinearLayoutAdapter"

    const-string v1, "tryBuildViews: Hey guys, looks like we got one exception when build a new view"

    invoke-static {v0, v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 123
    .end local v2    # "e":Ljava/lang/Exception;
    :goto_c
    return-void
.end method


# virtual methods
.method public init()V
    .registers 2

    .line 42
    iget-object v0, p0, Lcom/itau/empresas/ui/view/wraper/LinearLayoutWrapper;->adapter:Landroid/widget/Adapter;

    invoke-direct {p0, v0}, Lcom/itau/empresas/ui/view/wraper/LinearLayoutWrapper;->setupAdapter(Landroid/widget/Adapter;)V

    .line 43
    iget-object v0, p0, Lcom/itau/empresas/ui/view/wraper/LinearLayoutWrapper;->register:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 44
    return-void
.end method

.method public unregister()V
    .registers 2

    .line 149
    iget-object v0, p0, Lcom/itau/empresas/ui/view/wraper/LinearLayoutWrapper;->unregister:Ljava/lang/Runnable;

    if-eqz v0, :cond_9

    .line 150
    iget-object v0, p0, Lcom/itau/empresas/ui/view/wraper/LinearLayoutWrapper;->unregister:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 152
    :cond_9
    return-void
.end method

.class Lcom/itau/empresas/feature/transferencia/TransferenciaController$1;
.super Ljava/lang/Object;
.source "TransferenciaController.java"

# interfaces
.implements Lbr/com/itau/sdk/android/core/type/BackendCall;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/itau/empresas/feature/transferencia/TransferenciaController;->buscaTransferenciaRecebidas(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/itau/empresas/feature/transferencia/TransferenciaController;

.field final synthetic val$dataFim:Ljava/lang/String;

.field final synthetic val$dataInicio:Ljava/lang/String;

.field final synthetic val$tipoPagamento:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/itau/empresas/feature/transferencia/TransferenciaController;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 5
    .param p1, "this$0"    # Lcom/itau/empresas/feature/transferencia/TransferenciaController;

    .line 21
    iput-object p1, p0, Lcom/itau/empresas/feature/transferencia/TransferenciaController$1;->this$0:Lcom/itau/empresas/feature/transferencia/TransferenciaController;

    iput-object p2, p0, Lcom/itau/empresas/feature/transferencia/TransferenciaController$1;->val$dataInicio:Ljava/lang/String;

    iput-object p3, p0, Lcom/itau/empresas/feature/transferencia/TransferenciaController$1;->val$dataFim:Ljava/lang/String;

    iput-object p4, p0, Lcom/itau/empresas/feature/transferencia/TransferenciaController$1;->val$tipoPagamento:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public callBackend()V
    .registers 6

    .line 24
    iget-object v0, p0, Lcom/itau/empresas/feature/transferencia/TransferenciaController$1;->this$0:Lcom/itau/empresas/feature/transferencia/TransferenciaController;

    # invokes: Lcom/itau/empresas/feature/transferencia/TransferenciaController;->api()Ljava/lang/Object;
    invoke-static {v0}, Lcom/itau/empresas/feature/transferencia/TransferenciaController;->access$000(Lcom/itau/empresas/feature/transferencia/TransferenciaController;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/api/Api;

    iget-object v1, p0, Lcom/itau/empresas/feature/transferencia/TransferenciaController$1;->this$0:Lcom/itau/empresas/feature/transferencia/TransferenciaController;

    iget-object v1, v1, Lcom/itau/empresas/feature/transferencia/TransferenciaController;->app:Lcom/itau/empresas/CustomApplication;

    invoke-static {v1}, Lcom/itau/empresas/ui/util/ViewUtils;->getContaCompleta(Lcom/itau/empresas/CustomApplication;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/itau/empresas/feature/transferencia/TransferenciaController$1;->val$dataInicio:Ljava/lang/String;

    iget-object v3, p0, Lcom/itau/empresas/feature/transferencia/TransferenciaController$1;->val$dataFim:Ljava/lang/String;

    iget-object v4, p0, Lcom/itau/empresas/feature/transferencia/TransferenciaController$1;->val$tipoPagamento:Ljava/lang/String;

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/itau/empresas/api/Api;->buscaTransferenciaRecebidas(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    # invokes: Lcom/itau/empresas/feature/transferencia/TransferenciaController;->post(Ljava/lang/Object;)V
    invoke-static {v0}, Lcom/itau/empresas/feature/transferencia/TransferenciaController;->access$100(Ljava/lang/Object;)V

    .line 28
    return-void
.end method

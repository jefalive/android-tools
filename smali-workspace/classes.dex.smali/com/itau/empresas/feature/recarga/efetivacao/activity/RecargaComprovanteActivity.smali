.class public Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity;
.super Lcom/itau/empresas/ui/activity/BaseActivity;
.source "RecargaComprovanteActivity.java"

# interfaces
.implements Lcom/itau/empresas/ui/util/SnackbarUtils$Gerenciador;


# instance fields
.field contatoRecargaController:Lcom/itau/empresas/feature/recarga/contatos/controller/ContatoRecargaController;

.field llSalvarContato:Landroid/widget/LinearLayout;

.field nome:Ljava/lang/String;

.field novo:Z

.field resultadoEfetivacaoRecargaVO:Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoEfetivacaoRecargaVO;

.field root:Landroid/widget/LinearLayout;

.field textoContatoLocal:Landroid/widget/TextView;

.field textoContatoNome:Landroid/widget/TextView;

.field textoContatoTelefone:Landroid/widget/TextView;

.field textoDataRecarga:Landroid/widget/TextView;

.field textoNome:Landroid/widget/TextView;

.field textoOperadoraRegiao:Landroid/widget/TextView;

.field textoValorRecarga:Landroid/widget/TextView;

.field toolBar:Landroid/support/v7/widget/Toolbar;

.field private topSnackbar:Lcom/itau/empresas/ui/util/SnackbarUtils$GerenciadorDelegate;

.field warning:Lcom/itau/empresas/ui/view/WarningView;


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 46
    invoke-direct {p0}, Lcom/itau/empresas/ui/activity/BaseActivity;-><init>()V

    return-void
.end method

.method private constroiToolbar()V
    .registers 3

    .line 88
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity;->toolBar:Landroid/support/v7/widget/Toolbar;

    invoke-static {v0, p0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom;->Builder(Landroid/support/v7/widget/Toolbar;Landroid/support/v7/app/AppCompatActivity;)Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    move-result-object v0

    .line 90
    const v1, 0x7f0706b8

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->comTitulo(I)Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    move-result-object v0

    .line 91
    invoke-virtual {v0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->comClickListenerFinish()Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    move-result-object v0

    .line 92
    invoke-virtual {v0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->build()Lcom/itau/empresas/ui/util/listener/ToolbarCustom;

    .line 93
    return-void
.end method

.method private disparaEventoAnalytics()V
    .registers 5

    .line 96
    invoke-static {}, Lcom/itau/empresas/feature/analytics/AnalyticsHelper;->getInstance()Lcom/itau/empresas/feature/analytics/AnalyticsHelper;

    move-result-object v3

    .line 97
    .line 98
    .local v3, "analyticsHelper":Lcom/itau/empresas/feature/analytics/AnalyticsHelper;
    const v0, 0x7f0700f3

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 99
    const v1, 0x7f0700ce

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 100
    const v2, 0x7f0700c3

    invoke-virtual {p0, v2}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 97
    invoke-virtual {v3, v0, v1, v2}, Lcom/itau/empresas/feature/analytics/AnalyticsHelper;->eventoAdobeState(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 102
    return-void
.end method

.method public static intent(Landroid/content/Context;)Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity_$IntentBuilder_;
    .registers 2
    .param p0, "context"    # Landroid/content/Context;

    .line 289
    new-instance v0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity_$IntentBuilder_;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity_$IntentBuilder_;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method private preencheDadosIniciais()V
    .registers 1

    .line 105
    invoke-direct {p0}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity;->preencherDadosWarning()V

    .line 106
    invoke-direct {p0}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity;->preencherDadosRecarga()V

    .line 107
    invoke-direct {p0}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity;->preencherDadosContato()V

    .line 108
    return-void
.end method

.method private preencherDadosContato()V
    .registers 7

    .line 167
    iget-boolean v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity;->novo:Z

    if-eqz v0, :cond_b

    .line 168
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity;->llSalvarContato:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_12

    .line 170
    :cond_b
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity;->llSalvarContato:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 173
    :goto_12
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity;->resultadoEfetivacaoRecargaVO:Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoEfetivacaoRecargaVO;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoEfetivacaoRecargaVO;->getRecarga()Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoRecargaVO;

    move-result-object v4

    .line 174
    .local v4, "recarga":Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoRecargaVO;
    invoke-virtual {v4}, Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoRecargaVO;->getNomeRegiao()Ljava/lang/String;

    move-result-object v5

    .line 176
    .local v5, "nomeRegiao":Ljava/lang/String;
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity;->nome:Ljava/lang/String;

    if-eqz v0, :cond_28

    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity;->nome:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_30

    .line 177
    :cond_28
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity;->textoContatoNome:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_37

    .line 179
    :cond_30
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity;->textoContatoNome:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity;->nome:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 182
    :goto_37
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity;->textoContatoTelefone:Landroid/widget/TextView;

    .line 183
    invoke-virtual {v4}, Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoRecargaVO;->getDddTelefone()Ljava/lang/String;

    move-result-object v1

    .line 184
    invoke-virtual {v4}, Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoRecargaVO;->getNumeroTelefone()Ljava/lang/String;

    move-result-object v2

    .line 183
    invoke-static {p0, v1, v2}, Lcom/itau/empresas/ui/util/ViewUtils;->formataNumeroTelefonico(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 182
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 186
    if-eqz v5, :cond_50

    invoke-virtual {v5}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_58

    .line 187
    :cond_50
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity;->textoContatoLocal:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_71

    .line 189
    :cond_58
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity;->textoContatoLocal:Landroid/widget/TextView;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    .line 190
    invoke-virtual {v4}, Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoRecargaVO;->getNomeOperadora()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const/4 v2, 0x1

    aput-object v5, v1, v2

    const v2, 0x7f070532

    invoke-virtual {p0, v2, v1}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 189
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 193
    :goto_71
    return-void
.end method

.method private preencherDadosRecarga()V
    .registers 8

    .line 133
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity;->resultadoEfetivacaoRecargaVO:Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoEfetivacaoRecargaVO;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoEfetivacaoRecargaVO;->getRecarga()Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoRecargaVO;

    move-result-object v3

    .line 135
    .local v3, "recarga":Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoRecargaVO;
    invoke-virtual {v3}, Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoRecargaVO;->getNomeOperadora()Ljava/lang/String;

    move-result-object v4

    .line 136
    .local v4, "nomeOperadora":Ljava/lang/String;
    invoke-virtual {v3}, Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoRecargaVO;->getNomeRegiao()Ljava/lang/String;

    move-result-object v5

    .line 138
    .local v5, "nomeRegiao":Ljava/lang/String;
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity;->nome:Ljava/lang/String;

    if-eqz v0, :cond_1a

    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity;->nome:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_22

    .line 139
    :cond_1a
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity;->textoNome:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_29

    .line 141
    :cond_22
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity;->textoNome:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity;->nome:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 144
    :goto_29
    const/4 v6, 0x0

    .line 146
    .local v6, "operadoraRegiao":Ljava/lang/String;
    if-eqz v4, :cond_2d

    .line 147
    move-object v6, v4

    .line 149
    :cond_2d
    if-eqz v5, :cond_4c

    invoke-virtual {v5}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4c

    .line 150
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " - "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 153
    :cond_4c
    if-eqz v6, :cond_54

    .line 154
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity;->textoOperadoraRegiao:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_5b

    .line 156
    :cond_54
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity;->textoOperadoraRegiao:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 159
    :goto_5b
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity;->textoDataRecarga:Landroid/widget/TextView;

    invoke-virtual {v3}, Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoRecargaVO;->getDataRecarga()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/itau/empresas/ui/util/DataEHoraUtils;->formataDataComBarra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 161
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity;->textoValorRecarga:Landroid/widget/TextView;

    .line 162
    invoke-virtual {v3}, Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoRecargaVO;->getValorRecarga()Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/itau/empresas/ui/util/ValorMonetarioUtils;->formataValorDecimalParaMonetario(FZ)Ljava/lang/String;

    move-result-object v1

    .line 161
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 163
    return-void
.end method

.method private preencherDadosWarning()V
    .registers 7

    .line 112
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity;->resultadoEfetivacaoRecargaVO:Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoEfetivacaoRecargaVO;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoEfetivacaoRecargaVO;->getComprovante()Lcom/itau/empresas/feature/recarga/efetivacao/model/RecargaComprovanteVO;

    move-result-object v5

    .line 114
    .local v5, "comprovanteVO":Lcom/itau/empresas/feature/recarga/efetivacao/model/RecargaComprovanteVO;
    if-eqz v5, :cond_58

    .line 115
    invoke-virtual {v5}, Lcom/itau/empresas/feature/recarga/efetivacao/model/RecargaComprovanteVO;->getStatusPagamento()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2a

    invoke-virtual {v5}, Lcom/itau/empresas/feature/recarga/efetivacao/model/RecargaComprovanteVO;->getStatusPagamento()Ljava/lang/String;

    move-result-object v0

    .line 116
    const v1, 0x7f0704bc

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2a

    .line 117
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity;->warning:Lcom/itau/empresas/ui/view/WarningView;

    sget-object v1, Lcom/itau/empresas/ui/view/WarningView$Estado;->AGUARDANDO:Lcom/itau/empresas/ui/view/WarningView$Estado;

    const v2, 0x7f070660

    invoke-virtual {v0, v1, v2}, Lcom/itau/empresas/ui/view/WarningView;->setMensagem(Lcom/itau/empresas/ui/view/WarningView$Estado;I)V

    goto :goto_51

    .line 120
    :cond_2a
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity;->warning:Lcom/itau/empresas/ui/view/WarningView;

    sget-object v1, Lcom/itau/empresas/ui/view/WarningView$Estado;->SUCESSO:Lcom/itau/empresas/ui/view/WarningView$Estado;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    .line 123
    invoke-virtual {v5}, Lcom/itau/empresas/feature/recarga/efetivacao/model/RecargaComprovanteVO;->getDataPagamento()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/itau/empresas/ui/util/DataEHoraUtils;->formataDataComBarra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    aput-object v3, v2, v4

    .line 124
    invoke-virtual {v5}, Lcom/itau/empresas/feature/recarga/efetivacao/model/RecargaComprovanteVO;->getHoraPagamento()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/itau/empresas/ui/util/DataEHoraUtils;->formataHorario(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    aput-object v3, v2, v4

    .line 121
    const v3, 0x7f07065f

    invoke-virtual {p0, v3, v2}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 120
    invoke-virtual {v0, v1, v2}, Lcom/itau/empresas/ui/view/WarningView;->setMensagem(Lcom/itau/empresas/ui/view/WarningView$Estado;Ljava/lang/String;)V

    .line 126
    :goto_51
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity;->warning:Lcom/itau/empresas/ui/view/WarningView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/view/WarningView;->setVisibility(I)V

    goto :goto_5f

    .line 128
    :cond_58
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity;->warning:Lcom/itau/empresas/ui/view/WarningView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/view/WarningView;->setVisibility(I)V

    .line 130
    :goto_5f
    return-void
.end method


# virtual methods
.method protected aoClicarEnviar()V
    .registers 5

    .line 208
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity;->application:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getEventoTracker()Lcom/itau/empresas/ui/util/analytics/TrackerEvento;

    move-result-object v0

    new-instance v1, Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;

    .line 209
    const v2, 0x7f0702e9

    invoke-virtual {p0, v2}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 210
    const v3, 0x7f0702a9

    invoke-virtual {p0, v3}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 208
    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/analytics/TrackerEvento;->enviarHitEvento(Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;)V

    .line 212
    invoke-static {p0}, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesEnviarActivity;->intent(Landroid/content/Context;)Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesEnviarActivity_$IntentBuilder_;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity;->resultadoEfetivacaoRecargaVO:Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoEfetivacaoRecargaVO;

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesEnviarActivity_$IntentBuilder_;->recargaVO(Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoEfetivacaoRecargaVO;)Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesEnviarActivity_$IntentBuilder_;

    move-result-object v0

    .line 213
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesEnviarActivity_$IntentBuilder_;->startForResult(I)Lorg/androidannotations/api/builder/PostActivityStarter;

    .line 214
    return-void
.end method

.method protected aoClicarExtrato()V
    .registers 5

    .line 227
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity;->application:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getEventoTracker()Lcom/itau/empresas/ui/util/analytics/TrackerEvento;

    move-result-object v0

    new-instance v1, Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;

    .line 228
    const v2, 0x7f0702e9

    invoke-virtual {p0, v2}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 229
    const v3, 0x7f0702ab

    invoke-virtual {p0, v3}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 227
    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/analytics/TrackerEvento;->enviarHitEvento(Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;)V

    .line 230
    .line 231
    invoke-static {p0}, Lcom/itau/empresas/feature/home/HomeLogadaActivity;->iniciaHomeLogadaExibindoExtrato(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    .line 230
    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity;->startActivity(Landroid/content/Intent;)V

    .line 232
    return-void
.end method

.method protected aoClicarInicio()V
    .registers 5

    .line 236
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity;->application:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getEventoTracker()Lcom/itau/empresas/ui/util/analytics/TrackerEvento;

    move-result-object v0

    new-instance v1, Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;

    .line 237
    const v2, 0x7f0702e9

    invoke-virtual {p0, v2}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 238
    const v3, 0x7f0702b5

    invoke-virtual {p0, v3}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 236
    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/analytics/TrackerEvento;->enviarHitEvento(Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;)V

    .line 239
    invoke-static {p0}, Lcom/itau/empresas/feature/home/HomeLogadaActivity;->intent(Landroid/content/Context;)Lcom/itau/empresas/feature/home/HomeLogadaActivity_$IntentBuilder_;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/feature/home/HomeLogadaActivity_$IntentBuilder_;->start()Lorg/androidannotations/api/builder/PostActivityStarter;

    .line 240
    return-void
.end method

.method protected aoClicarNovaRecarga()V
    .registers 5

    .line 218
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity;->application:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getEventoTracker()Lcom/itau/empresas/ui/util/analytics/TrackerEvento;

    move-result-object v0

    new-instance v1, Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;

    .line 219
    const v2, 0x7f0702e9

    invoke-virtual {p0, v2}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 220
    const v3, 0x7f0702b9

    invoke-virtual {p0, v3}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 218
    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/analytics/TrackerEvento;->enviarHitEvento(Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;)V

    .line 222
    invoke-static {p0}, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaContatosActivity;->intent(Landroid/content/Context;)Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaContatosActivity_$IntentBuilder_;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaContatosActivity_$IntentBuilder_;->start()Lorg/androidannotations/api/builder/PostActivityStarter;

    .line 223
    return-void
.end method

.method protected aoClicarVerComprovante()V
    .registers 5

    .line 197
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity;->application:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getEventoTracker()Lcom/itau/empresas/ui/util/analytics/TrackerEvento;

    move-result-object v0

    new-instance v1, Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;

    .line 198
    const v2, 0x7f0702e9

    invoke-virtual {p0, v2}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 199
    const v3, 0x7f0702d0

    invoke-virtual {p0, v3}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 197
    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/analytics/TrackerEvento;->enviarHitEvento(Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;)V

    .line 201
    invoke-static {p0}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaDetalheComprovanteActivity;->intent(Landroid/content/Context;)Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaDetalheComprovanteActivity_$IntentBuilder_;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity;->nome:Ljava/lang/String;

    .line 202
    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaDetalheComprovanteActivity_$IntentBuilder_;->nome(Ljava/lang/String;)Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaDetalheComprovanteActivity_$IntentBuilder_;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity;->resultadoEfetivacaoRecargaVO:Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoEfetivacaoRecargaVO;

    .line 203
    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaDetalheComprovanteActivity_$IntentBuilder_;->resultadoEfetivacaoRecargaVO(Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoEfetivacaoRecargaVO;)Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaDetalheComprovanteActivity_$IntentBuilder_;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaDetalheComprovanteActivity_$IntentBuilder_;->start()Lorg/androidannotations/api/builder/PostActivityStarter;

    .line 204
    return-void
.end method

.method protected aoInicializarViews()V
    .registers 3

    .line 75
    new-instance v0, Lcom/itau/empresas/ui/util/SnackbarUtils$GerenciadorDelegate;

    iget-object v1, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity;->root:Landroid/widget/LinearLayout;

    invoke-direct {v0, v1}, Lcom/itau/empresas/ui/util/SnackbarUtils$GerenciadorDelegate;-><init>(Landroid/view/ViewGroup;)V

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity;->topSnackbar:Lcom/itau/empresas/ui/util/SnackbarUtils$GerenciadorDelegate;

    .line 77
    invoke-direct {p0}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity;->preencheDadosIniciais()V

    .line 78
    invoke-direct {p0}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity;->preencherDadosWarning()V

    .line 79
    invoke-direct {p0}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity;->preencherDadosRecarga()V

    .line 80
    invoke-direct {p0}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity;->preencherDadosContato()V

    .line 81
    invoke-direct {p0}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity;->constroiToolbar()V

    .line 83
    invoke-static {p0}, Lcom/itau/empresas/feature/feedback/FeedbackHelper;->exibirFeedback(Landroid/support/v7/app/AppCompatActivity;)V

    .line 84
    invoke-direct {p0}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity;->disparaEventoAnalytics()V

    .line 85
    return-void
.end method

.method protected aoSalvarContato()V
    .registers 7

    .line 244
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity;->application:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getEventoTracker()Lcom/itau/empresas/ui/util/analytics/TrackerEvento;

    move-result-object v0

    new-instance v1, Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;

    .line 245
    const v2, 0x7f0702e9

    invoke-virtual {p0, v2}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 246
    const v3, 0x7f0702c3

    invoke-virtual {p0, v3}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 244
    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/analytics/TrackerEvento;->enviarHitEvento(Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;)V

    .line 248
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity;->application:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getEventoTracker()Lcom/itau/empresas/ui/util/analytics/TrackerEvento;

    move-result-object v0

    new-instance v1, Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;

    .line 249
    const v2, 0x7f0702e4

    invoke-virtual {p0, v2}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 250
    const v3, 0x7f0702bd

    invoke-virtual {p0, v3}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 248
    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/analytics/TrackerEvento;->enviarHitEvento(Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;)V

    .line 251
    new-instance v4, Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;

    invoke-direct {v4}, Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;-><init>()V

    .line 252
    .local v4, "contatoRecargaVO":Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity;->resultadoEfetivacaoRecargaVO:Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoEfetivacaoRecargaVO;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoEfetivacaoRecargaVO;->getRecarga()Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoRecargaVO;

    move-result-object v5

    .line 254
    .local v5, "recargaVO":Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoRecargaVO;
    invoke-virtual {v5}, Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoRecargaVO;->getDddTelefone()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;->setDdd(Ljava/lang/String;)V

    .line 255
    invoke-virtual {v5}, Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoRecargaVO;->getNumeroTelefone()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;->setTelefone(Ljava/lang/String;)V

    .line 256
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity;->nome:Ljava/lang/String;

    invoke-virtual {v4, v0}, Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;->setApelido(Ljava/lang/String;)V

    .line 257
    invoke-virtual {v5}, Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoRecargaVO;->getNomeOperadora()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;->setNomeOperadora(Ljava/lang/String;)V

    .line 258
    invoke-virtual {v5}, Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoRecargaVO;->getNomeRegiao()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;->setEstadoOperadora(Ljava/lang/String;)V

    .line 260
    invoke-static {p0}, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity;->intent(Landroid/content/Context;)Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity_$IntentBuilder_;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity_$IntentBuilder_;->contatoRecarga(Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;)Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity_$IntentBuilder_;

    move-result-object v0

    sget-object v1, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity$TIPOS;->NOVO_CONTATO:Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity$TIPOS;

    .line 261
    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity_$IntentBuilder_;->tipo(Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity$TIPOS;)Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity_$IntentBuilder_;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity_$IntentBuilder_;->start()Lorg/androidannotations/api/builder/PostActivityStarter;

    .line 262
    return-void
.end method

.method public dismissSnackbar()V
    .registers 2

    .line 281
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity;->topSnackbar:Lcom/itau/empresas/ui/util/SnackbarUtils$GerenciadorDelegate;

    invoke-virtual {v0}, Lcom/itau/empresas/ui/util/SnackbarUtils$GerenciadorDelegate;->dismissSnackbar()V

    .line 282
    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .registers 6
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .line 266
    if-nez p1, :cond_19

    const/4 v0, -0x1

    if-ne p2, v0, :cond_19

    .line 267
    new-instance v0, Landroid/support/v7/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/support/v7/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 268
    const v1, 0x7f070520

    invoke-virtual {v0, v1}, Landroid/support/v7/app/AlertDialog$Builder;->setMessage(I)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v0

    .line 269
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/AlertDialog$Builder;->setCancelable(Z)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v0

    .line 270
    invoke-virtual {v0}, Landroid/support/v7/app/AlertDialog$Builder;->show()Landroid/support/v7/app/AlertDialog;

    .line 272
    :cond_19
    invoke-super {p0, p1, p2, p3}, Lcom/itau/empresas/ui/activity/BaseActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 273
    return-void
.end method

.method public opkeysConsumidas()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Ljava/lang/String;>;"
        }
    .end annotation

    .line 277
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public showSnackbar(Ljava/lang/CharSequence;I)V
    .registers 4
    .param p1, "texto"    # Ljava/lang/CharSequence;
    .param p2, "duracao"    # I

    .line 285
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity;->topSnackbar:Lcom/itau/empresas/ui/util/SnackbarUtils$GerenciadorDelegate;

    invoke-virtual {v0, p1, p2}, Lcom/itau/empresas/ui/util/SnackbarUtils$GerenciadorDelegate;->showSnackbar(Ljava/lang/CharSequence;I)V

    .line 286
    return-void
.end method

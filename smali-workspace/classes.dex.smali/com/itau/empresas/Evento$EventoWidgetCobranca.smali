.class public Lcom/itau/empresas/Evento$EventoWidgetCobranca;
.super Ljava/lang/Object;
.source "Evento.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/itau/empresas/Evento;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "EventoWidgetCobranca"
.end annotation


# static fields
.field private static cobrancas:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Lbr/com/itau/widgets/graphicalfriedcake/model/ItemVO;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 131
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getCobrancas()Ljava/util/List;
    .registers 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Lbr/com/itau/widgets/graphicalfriedcake/model/ItemVO;>;"
        }
    .end annotation

    .line 136
    sget-object v0, Lcom/itau/empresas/Evento$EventoWidgetCobranca;->cobrancas:Ljava/util/List;

    return-object v0
.end method

.method public static setCobrancas(Ljava/util/List;)V
    .registers 1
    .param p0, "lista"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<Lbr/com/itau/widgets/graphicalfriedcake/model/ItemVO;>;)V"
        }
    .end annotation

    .line 140
    sput-object p0, Lcom/itau/empresas/Evento$EventoWidgetCobranca;->cobrancas:Ljava/util/List;

    .line 141
    return-void
.end method

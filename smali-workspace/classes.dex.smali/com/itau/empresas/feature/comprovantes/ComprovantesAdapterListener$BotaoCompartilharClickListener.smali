.class Lcom/itau/empresas/feature/comprovantes/ComprovantesAdapterListener$BotaoCompartilharClickListener;
.super Ljava/lang/Object;
.source "ComprovantesAdapterListener.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/itau/empresas/feature/comprovantes/ComprovantesAdapterListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "BotaoCompartilharClickListener"
.end annotation


# instance fields
.field private final compartilharClickListener:Landroid/view/View$OnClickListener;

.field private final comprovantesVO:Lcom/itau/empresas/feature/comprovantes/model/ComprovantesVO;

.field private final context:Landroid/content/Context;

.field private final swipeLayout:Lcom/daimajia/swipe/SwipeLayout;


# direct methods
.method constructor <init>(Lcom/itau/empresas/feature/comprovantes/model/ComprovantesVO;Landroid/content/Context;Landroid/view/View$OnClickListener;Lcom/daimajia/swipe/SwipeLayout;)V
    .registers 5
    .param p1, "comprovantesVO"    # Lcom/itau/empresas/feature/comprovantes/model/ComprovantesVO;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "compartilharClickListener"    # Landroid/view/View$OnClickListener;
    .param p4, "swipeLayout"    # Lcom/daimajia/swipe/SwipeLayout;

    .line 92
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 93
    iput-object p1, p0, Lcom/itau/empresas/feature/comprovantes/ComprovantesAdapterListener$BotaoCompartilharClickListener;->comprovantesVO:Lcom/itau/empresas/feature/comprovantes/model/ComprovantesVO;

    .line 94
    iput-object p2, p0, Lcom/itau/empresas/feature/comprovantes/ComprovantesAdapterListener$BotaoCompartilharClickListener;->context:Landroid/content/Context;

    .line 95
    iput-object p3, p0, Lcom/itau/empresas/feature/comprovantes/ComprovantesAdapterListener$BotaoCompartilharClickListener;->compartilharClickListener:Landroid/view/View$OnClickListener;

    .line 96
    iput-object p4, p0, Lcom/itau/empresas/feature/comprovantes/ComprovantesAdapterListener$BotaoCompartilharClickListener;->swipeLayout:Lcom/daimajia/swipe/SwipeLayout;

    .line 97
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .registers 4
    .param p1, "v"    # Landroid/view/View;

    .line 102
    iget-object v0, p0, Lcom/itau/empresas/feature/comprovantes/ComprovantesAdapterListener$BotaoCompartilharClickListener;->context:Landroid/content/Context;

    invoke-static {v0}, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesDetalheActivity;->intent(Landroid/content/Context;)Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesDetalheActivity_$IntentBuilder_;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/comprovantes/ComprovantesAdapterListener$BotaoCompartilharClickListener;->comprovantesVO:Lcom/itau/empresas/feature/comprovantes/model/ComprovantesVO;

    .line 103
    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesDetalheActivity_$IntentBuilder_;->comprovantesVO(Lcom/itau/empresas/feature/comprovantes/model/ComprovantesVO;)Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesDetalheActivity_$IntentBuilder_;

    move-result-object v0

    .line 104
    invoke-virtual {v0}, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesDetalheActivity_$IntentBuilder_;->start()Lorg/androidannotations/api/builder/PostActivityStarter;

    .line 106
    iget-object v0, p0, Lcom/itau/empresas/feature/comprovantes/ComprovantesAdapterListener$BotaoCompartilharClickListener;->swipeLayout:Lcom/daimajia/swipe/SwipeLayout;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/daimajia/swipe/SwipeLayout;->close(Z)V

    .line 108
    iget-object v0, p0, Lcom/itau/empresas/feature/comprovantes/ComprovantesAdapterListener$BotaoCompartilharClickListener;->compartilharClickListener:Landroid/view/View$OnClickListener;

    if-eqz v0, :cond_1e

    .line 109
    iget-object v0, p0, Lcom/itau/empresas/feature/comprovantes/ComprovantesAdapterListener$BotaoCompartilharClickListener;->compartilharClickListener:Landroid/view/View$OnClickListener;

    invoke-interface {v0, p1}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    .line 111
    :cond_1e
    return-void
.end method

.class public Lcom/google/ads/conversiontracking/b;
.super Lcom/google/ads/conversiontracking/GoogleConversionReporter;
.source "SourceFile"


# instance fields
.field private a:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 2

    .line 19
    invoke-direct {p0}, Lcom/google/ads/conversiontracking/GoogleConversionReporter;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/google/ads/conversiontracking/b;->a:Landroid/content/Context;

    .line 21
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;J)V
    .registers 11

    .line 35
    move-object v0, p0

    :try_start_1
    iget-object v1, p0, Lcom/google/ads/conversiontracking/b;->a:Landroid/content/Context;

    new-instance v2, Lcom/google/ads/conversiontracking/g$c;

    invoke-direct {v2}, Lcom/google/ads/conversiontracking/g$c;-><init>()V

    .line 38
    invoke-virtual {v2, p1}, Lcom/google/ads/conversiontracking/g$c;->a(Ljava/lang/String;)Lcom/google/ads/conversiontracking/g$c;

    move-result-object v2

    .line 39
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/google/ads/conversiontracking/g$c;->a(Z)Lcom/google/ads/conversiontracking/g$c;

    move-result-object v2

    .line 40
    invoke-virtual {v2}, Lcom/google/ads/conversiontracking/g$c;->b()Lcom/google/ads/conversiontracking/g$c;

    move-result-object v2

    .line 41
    invoke-virtual {v2, p2, p3}, Lcom/google/ads/conversiontracking/g$c;->a(J)Lcom/google/ads/conversiontracking/g$c;

    move-result-object v2

    .line 35
    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x1

    invoke-virtual/range {v0 .. v5}, Lcom/google/ads/conversiontracking/b;->a(Landroid/content/Context;Lcom/google/ads/conversiontracking/g$c;ZZZ)V
    :try_end_1f
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1f} :catch_20

    .line 47
    goto :goto_28

    .line 45
    :catch_20
    move-exception v6

    .line 46
    const-string v0, "GoogleConversionReporter"

    const-string v1, "Error sending ping"

    invoke-static {v0, v1, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 48
    :goto_28
    return-void
.end method

.method public report()V
    .registers 1

    .line 26
    return-void
.end method

.class Lcom/daimajia/swipe/SwipeLayout$SwipeDetector;
.super Landroid/view/GestureDetector$SimpleOnGestureListener;
.source "SwipeLayout.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/daimajia/swipe/SwipeLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "SwipeDetector"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/daimajia/swipe/SwipeLayout;


# direct methods
.method constructor <init>(Lcom/daimajia/swipe/SwipeLayout;)V
    .registers 2

    .line 1075
    iput-object p1, p0, Lcom/daimajia/swipe/SwipeLayout$SwipeDetector;->this$0:Lcom/daimajia/swipe/SwipeLayout;

    invoke-direct {p0}, Landroid/view/GestureDetector$SimpleOnGestureListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onDoubleTap(Landroid/view/MotionEvent;)Z
    .registers 8
    .param p1, "e"    # Landroid/view/MotionEvent;

    .line 1085
    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout$SwipeDetector;->this$0:Lcom/daimajia/swipe/SwipeLayout;

    # getter for: Lcom/daimajia/swipe/SwipeLayout;->mDoubleClickListener:Lcom/daimajia/swipe/SwipeLayout$DoubleClickListener;
    invoke-static {v0}, Lcom/daimajia/swipe/SwipeLayout;->access$900(Lcom/daimajia/swipe/SwipeLayout;)Lcom/daimajia/swipe/SwipeLayout$DoubleClickListener;

    move-result-object v0

    if-eqz v0, :cond_5d

    .line 1087
    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout$SwipeDetector;->this$0:Lcom/daimajia/swipe/SwipeLayout;

    invoke-virtual {v0}, Lcom/daimajia/swipe/SwipeLayout;->getCurrentBottomView()Landroid/view/View;

    move-result-object v4

    .line 1088
    .local v4, "bottom":Landroid/view/View;
    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout$SwipeDetector;->this$0:Lcom/daimajia/swipe/SwipeLayout;

    invoke-virtual {v0}, Lcom/daimajia/swipe/SwipeLayout;->getSurfaceView()Landroid/view/View;

    move-result-object v5

    .line 1089
    .local v5, "surface":Landroid/view/View;
    if-eqz v4, :cond_4c

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    invoke-virtual {v4}, Landroid/view/View;->getLeft()I

    move-result v1

    int-to-float v1, v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_4c

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    invoke-virtual {v4}, Landroid/view/View;->getRight()I

    move-result v1

    int-to-float v1, v1

    cmpg-float v0, v0, v1

    if-gez v0, :cond_4c

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    invoke-virtual {v4}, Landroid/view/View;->getTop()I

    move-result v1

    int-to-float v1, v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_4c

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    invoke-virtual {v4}, Landroid/view/View;->getBottom()I

    move-result v1

    int-to-float v1, v1

    cmpg-float v0, v0, v1

    if-gez v0, :cond_4c

    .line 1091
    move-object v3, v4

    .local v3, "target":Landroid/view/View;
    goto :goto_4d

    .line 1093
    .end local v3    # "target":Landroid/view/View;
    :cond_4c
    move-object v3, v5

    .line 1095
    .local v3, "target":Landroid/view/View;
    :goto_4d
    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout$SwipeDetector;->this$0:Lcom/daimajia/swipe/SwipeLayout;

    # getter for: Lcom/daimajia/swipe/SwipeLayout;->mDoubleClickListener:Lcom/daimajia/swipe/SwipeLayout$DoubleClickListener;
    invoke-static {v0}, Lcom/daimajia/swipe/SwipeLayout;->access$900(Lcom/daimajia/swipe/SwipeLayout;)Lcom/daimajia/swipe/SwipeLayout$DoubleClickListener;

    move-result-object v0

    iget-object v1, p0, Lcom/daimajia/swipe/SwipeLayout$SwipeDetector;->this$0:Lcom/daimajia/swipe/SwipeLayout;

    if-ne v3, v5, :cond_59

    const/4 v2, 0x1

    goto :goto_5a

    :cond_59
    const/4 v2, 0x0

    :goto_5a
    invoke-interface {v0, v1, v2}, Lcom/daimajia/swipe/SwipeLayout$DoubleClickListener;->onDoubleClick(Lcom/daimajia/swipe/SwipeLayout;Z)V

    .line 1097
    .end local v3    # "target":Landroid/view/View;
    .end local v4    # "bottom":Landroid/view/View;
    .end local v5    # "surface":Landroid/view/View;
    :cond_5d
    const/4 v0, 0x1

    return v0
.end method

.method public onSingleTapUp(Landroid/view/MotionEvent;)Z
    .registers 3
    .param p1, "e"    # Landroid/view/MotionEvent;

    .line 1078
    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout$SwipeDetector;->this$0:Lcom/daimajia/swipe/SwipeLayout;

    # getter for: Lcom/daimajia/swipe/SwipeLayout;->mClickToClose:Z
    invoke-static {v0}, Lcom/daimajia/swipe/SwipeLayout;->access$700(Lcom/daimajia/swipe/SwipeLayout;)Z

    move-result v0

    if-eqz v0, :cond_15

    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout$SwipeDetector;->this$0:Lcom/daimajia/swipe/SwipeLayout;

    # invokes: Lcom/daimajia/swipe/SwipeLayout;->isTouchOnSurface(Landroid/view/MotionEvent;)Z
    invoke-static {v0, p1}, Lcom/daimajia/swipe/SwipeLayout;->access$800(Lcom/daimajia/swipe/SwipeLayout;Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_15

    .line 1079
    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout$SwipeDetector;->this$0:Lcom/daimajia/swipe/SwipeLayout;

    invoke-virtual {v0}, Lcom/daimajia/swipe/SwipeLayout;->close()V

    .line 1081
    :cond_15
    invoke-super {p0, p1}, Landroid/view/GestureDetector$SimpleOnGestureListener;->onSingleTapUp(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

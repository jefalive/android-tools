.class public Lcom/itau/empresas/ui/util/PermissoesUtil;
.super Ljava/lang/Object;
.source "PermissoesUtil.java"


# direct methods
.method public static getPermissionCallphone()[Ljava/lang/String;
    .registers 3

    .line 40
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "android.permission.CALL_PHONE"

    const/4 v2, 0x0

    aput-object v1, v0, v2

    return-object v0
.end method

.method public static getPermissionShowCamera()[Ljava/lang/String;
    .registers 3

    .line 24
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "android.permission.CAMERA"

    const/4 v2, 0x0

    aput-object v1, v0, v2

    return-object v0
.end method

.method public static getPermissionShowContatos()[Ljava/lang/String;
    .registers 3

    .line 29
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "android.permission.READ_CONTACTS"

    const/4 v2, 0x0

    aput-object v1, v0, v2

    return-object v0
.end method

.method public static getPermissionShowLocation()[Ljava/lang/String;
    .registers 3

    .line 34
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "android.permission.ACCESS_COARSE_LOCATION"

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const-string v1, "android.permission.ACCESS_FINE_LOCATION"

    const/4 v2, 0x1

    aput-object v1, v0, v2

    return-object v0
.end method

.method public static getPermissionWriteExternalStorage()[Ljava/lang/String;
    .registers 3

    .line 45
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "android.permission.WRITE_EXTERNAL_STORAGE"

    const/4 v2, 0x0

    aput-object v1, v0, v2

    return-object v0
.end method

.method public static varargs hasSelfPermissions(Landroid/content/Context;[Ljava/lang/String;)Z
    .registers 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "permissions"    # [Ljava/lang/String;

    .line 74
    move-object v1, p1

    array-length v2, v1

    const/4 v3, 0x0

    :goto_3
    if-ge v3, v2, :cond_12

    aget-object v4, v1, v3

    .line 75
    .local v4, "permission":Ljava/lang/String;
    invoke-static {p0, v4}, Landroid/support/v4/content/PermissionChecker;->checkSelfPermission(Landroid/content/Context;Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_f

    .line 76
    const/4 v0, 0x0

    return v0

    .line 74
    .end local v4    # "permission":Ljava/lang/String;
    :cond_f
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    .line 79
    :cond_12
    const/4 v0, 0x1

    return v0
.end method

.method public static varargs shouldShowRequestPermissionRationale(Landroid/app/Activity;[Ljava/lang/String;)Z
    .registers 7
    .param p0, "activity"    # Landroid/app/Activity;
    .param p1, "permissions"    # [Ljava/lang/String;

    .line 91
    move-object v1, p1

    array-length v2, v1

    const/4 v3, 0x0

    :goto_3
    if-ge v3, v2, :cond_12

    aget-object v4, v1, v3

    .line 92
    .local v4, "permission":Ljava/lang/String;
    invoke-static {p0, v4}, Landroid/support/v4/app/ActivityCompat;->shouldShowRequestPermissionRationale(Landroid/app/Activity;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 93
    const/4 v0, 0x1

    return v0

    .line 91
    .end local v4    # "permission":Ljava/lang/String;
    :cond_f
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    .line 96
    :cond_12
    const/4 v0, 0x0

    return v0
.end method

.method public static varargs shouldShowRequestPermissionRationale(Landroid/support/v4/app/Fragment;[Ljava/lang/String;)Z
    .registers 7
    .param p0, "fragment"    # Landroid/support/v4/app/Fragment;
    .param p1, "permissions"    # [Ljava/lang/String;

    .line 108
    move-object v1, p1

    array-length v2, v1

    const/4 v3, 0x0

    :goto_3
    if-ge v3, v2, :cond_12

    aget-object v4, v1, v3

    .line 109
    .local v4, "permission":Ljava/lang/String;
    invoke-virtual {p0, v4}, Landroid/support/v4/app/Fragment;->shouldShowRequestPermissionRationale(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 110
    const/4 v0, 0x1

    return v0

    .line 108
    .end local v4    # "permission":Ljava/lang/String;
    :cond_f
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    .line 113
    :cond_12
    const/4 v0, 0x0

    return v0
.end method

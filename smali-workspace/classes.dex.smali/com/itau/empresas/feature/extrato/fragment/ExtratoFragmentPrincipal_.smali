.class public final Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal_;
.super Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;
.source "ExtratoFragmentPrincipal_.java"

# interfaces
.implements Lorg/androidannotations/api/view/HasViews;
.implements Lorg/androidannotations/api/view/OnViewChangedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal_$FragmentBuilder_;
    }
.end annotation


# instance fields
.field private contentView_:Landroid/view/View;

.field private final onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;


# direct methods
.method public constructor <init>()V
    .registers 2

    .line 28
    invoke-direct {p0}, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;-><init>()V

    .line 32
    new-instance v0, Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-direct {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    return-void
.end method

.method public static builder()Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal_$FragmentBuilder_;
    .registers 1

    .line 86
    new-instance v0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal_$FragmentBuilder_;

    invoke-direct {v0}, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal_$FragmentBuilder_;-><init>()V

    return-object v0
.end method

.method private init_(Landroid/os/Bundle;)V
    .registers 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 71
    invoke-static {p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->registerOnViewChangedListener(Lorg/androidannotations/api/view/OnViewChangedListener;)V

    .line 72
    invoke-static {}, Lcom/itau/empresas/CustomApplication_;->getInstance()Lcom/itau/empresas/CustomApplication;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal_;->application:Lcom/itau/empresas/CustomApplication;

    .line 73
    invoke-static {}, Lcom/itau/empresas/CustomApplication_;->getInstance()Lcom/itau/empresas/CustomApplication;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal_;->application:Lcom/itau/empresas/CustomApplication;

    .line 74
    invoke-virtual {p0}, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal_;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/itau/empresas/controller/ExtratoController_;->getInstance_(Landroid/content/Context;)Lcom/itau/empresas/controller/ExtratoController_;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal_;->extratoController:Lcom/itau/empresas/controller/ExtratoController;

    .line 75
    invoke-virtual {p0}, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal_;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/itau/empresas/feature/logout/LogoutHelper_;->getInstance_(Landroid/content/Context;)Lcom/itau/empresas/feature/logout/LogoutHelper_;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal_;->logoutHelper:Lcom/itau/empresas/feature/logout/LogoutHelper;

    .line 76
    invoke-virtual {p0}, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal_;->afterInject()V

    .line 77
    return-void
.end method


# virtual methods
.method public findViewById(I)Landroid/view/View;
    .registers 3
    .param p1, "id"    # I

    .line 45
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal_;->contentView_:Landroid/view/View;

    if-nez v0, :cond_6

    .line 46
    const/4 v0, 0x0

    return-object v0

    .line 48
    :cond_6
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal_;->contentView_:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 37
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-static {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    move-result-object v1

    .line 38
    .local v1, "previousNotifier":Lorg/androidannotations/api/view/OnViewChangedNotifier;
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal_;->init_(Landroid/os/Bundle;)V

    .line 39
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->onCreate(Landroid/os/Bundle;)V

    .line 40
    invoke-static {v1}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 41
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .registers 6
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .line 53
    invoke-super {p0, p1, p2, p3}, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal_;->contentView_:Landroid/view/View;

    .line 54
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal_;->contentView_:Landroid/view/View;

    if-nez v0, :cond_14

    .line 55
    const v0, 0x7f0300a1

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal_;->contentView_:Landroid/view/View;

    .line 57
    :cond_14
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal_;->contentView_:Landroid/view/View;

    return-object v0
.end method

.method public onDestroyView()V
    .registers 2

    .line 62
    invoke-super {p0}, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->onDestroyView()V

    .line 63
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal_;->contentView_:Landroid/view/View;

    .line 64
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal_;->textoSaldoConta:Landroid/widget/TextView;

    .line 65
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal_;->tabsExtrato:Landroid/support/design/widget/TabLayout;

    .line 66
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal_;->pagerExtrato:Landroid/support/v4/view/ViewPager;

    .line 67
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal_;->rlSaldoEmConta:Landroid/widget/RelativeLayout;

    .line 68
    return-void
.end method

.method public onViewChanged(Lorg/androidannotations/api/view/HasViews;)V
    .registers 4
    .param p1, "hasViews"    # Lorg/androidannotations/api/view/HasViews;

    .line 91
    const v0, 0x7f0e040c

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal_;->textoSaldoConta:Landroid/widget/TextView;

    .line 92
    const v0, 0x7f0e040e

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/TabLayout;

    iput-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal_;->tabsExtrato:Landroid/support/design/widget/TabLayout;

    .line 93
    const v0, 0x7f0e040f

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    iput-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal_;->pagerExtrato:Landroid/support/v4/view/ViewPager;

    .line 94
    const v0, 0x7f0e040a

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal_;->rlSaldoEmConta:Landroid/widget/RelativeLayout;

    .line 95
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal_;->rlSaldoEmConta:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_3a

    .line 96
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal_;->rlSaldoEmConta:Landroid/widget/RelativeLayout;

    new-instance v1, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal_$1;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal_$1;-><init>(Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal_;)V

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 105
    :cond_3a
    invoke-virtual {p0}, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal_;->afterViews()V

    .line 106
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .registers 4
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;

    .line 81
    invoke-super {p0, p1, p2}, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 82
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 83
    return-void
.end method

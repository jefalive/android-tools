.class public Lcom/itau/empresas/api/universal/model/ItemConfiguracoes;
.super Ljava/lang/Object;
.source "ItemConfiguracoes.java"


# instance fields
.field private activity:Ljava/lang/Class;

.field private iconeMenu:Ljava/lang/String;

.field private itemMenu:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Class;)V
    .registers 4
    .param p1, "iconeMenu"    # Ljava/lang/String;
    .param p2, "itemMenu"    # Ljava/lang/String;
    .param p3, "activity"    # Ljava/lang/Class;

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    iput-object p1, p0, Lcom/itau/empresas/api/universal/model/ItemConfiguracoes;->iconeMenu:Ljava/lang/String;

    .line 13
    iput-object p2, p0, Lcom/itau/empresas/api/universal/model/ItemConfiguracoes;->itemMenu:Ljava/lang/String;

    .line 14
    iput-object p3, p0, Lcom/itau/empresas/api/universal/model/ItemConfiguracoes;->activity:Ljava/lang/Class;

    .line 15
    return-void
.end method


# virtual methods
.method public getIconeMenu()Ljava/lang/String;
    .registers 2

    .line 18
    iget-object v0, p0, Lcom/itau/empresas/api/universal/model/ItemConfiguracoes;->iconeMenu:Ljava/lang/String;

    return-object v0
.end method

.method public getItemMenu()Ljava/lang/String;
    .registers 2

    .line 22
    iget-object v0, p0, Lcom/itau/empresas/api/universal/model/ItemConfiguracoes;->itemMenu:Ljava/lang/String;

    return-object v0
.end method

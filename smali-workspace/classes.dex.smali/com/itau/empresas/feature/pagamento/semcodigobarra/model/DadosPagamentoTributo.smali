.class public Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo;
.super Ljava/lang/Object;
.source "DadosPagamentoTributo.java"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo$Builder;
    }
.end annotation


# instance fields
.field private dataPagamento:Ljava/lang/String;

.field private detalhesAutorizacaoVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;

.field private identificacaoComprovante:Ljava/lang/String;

.field private inclusaoTributoResponseVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/InclusaoTributoResponseVO;

.field private isMostraComprovantes:Z

.field private isMostraDetalhesAutorizacao:Z

.field private mensagem:Ljava/lang/String;

.field private tipoPagamento:Ljava/lang/String;

.field private titulo:Ljava/lang/String;

.field private valorPagamento:Ljava/lang/String;


# direct methods
.method private constructor <init>(Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;)V
    .registers 4
    .param p1, "detalhesAutorizacaoVO"    # Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    invoke-virtual {p1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;->getDescricaoPagamento()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo;->tipoPagamento:Ljava/lang/String;

    .line 34
    invoke-virtual {p1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;->getValorPagamento()Ljava/math/BigDecimal;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/itau/empresas/ui/util/ValorMonetarioUtils;->formataValorDecimalParaMonetario(Ljava/math/BigDecimal;Z)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo;->valorPagamento:Ljava/lang/String;

    .line 35
    invoke-virtual {p1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;->getDataPagamento()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/itau/empresas/ui/util/DataEHoraUtils;->formataDataComBarra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo;->dataPagamento:Ljava/lang/String;

    .line 36
    invoke-virtual {p1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;->getIdentificacaoComprovante()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo;->identificacaoComprovante:Ljava/lang/String;

    .line 37
    iput-object p1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo;->detalhesAutorizacaoVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;

    .line 38
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo;->isMostraDetalhesAutorizacao:Z

    .line 40
    return-void
.end method

.method synthetic constructor <init>(Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo$1;)V
    .registers 3
    .param p1, "x0"    # Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;
    .param p2, "x1"    # Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo$1;

    .line 10
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo;-><init>(Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;)V

    return-void
.end method

.method private constructor <init>(Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/InclusaoTributoResponseVO;)V
    .registers 4
    .param p1, "inclusaoTributoResponseVO"    # Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/InclusaoTributoResponseVO;

    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    invoke-virtual {p1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/InclusaoTributoResponseVO;->getTipoPagamento()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo;->titulo:Ljava/lang/String;

    .line 44
    invoke-virtual {p1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/InclusaoTributoResponseVO;->getValorTotal()Ljava/math/BigDecimal;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/itau/empresas/ui/util/ValorMonetarioUtils;->formataValorDecimalParaMonetario(Ljava/math/BigDecimal;Z)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo;->valorPagamento:Ljava/lang/String;

    .line 45
    invoke-virtual {p1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/InclusaoTributoResponseVO;->getDataPagamento()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/itau/empresas/ui/util/DataEHoraUtils;->formataDataComBarra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo;->dataPagamento:Ljava/lang/String;

    .line 46
    invoke-virtual {p1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/InclusaoTributoResponseVO;->getIdentificadorPagamento()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo;->identificacaoComprovante:Ljava/lang/String;

    .line 47
    iput-object p1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo;->inclusaoTributoResponseVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/InclusaoTributoResponseVO;

    .line 48
    return-void
.end method

.method synthetic constructor <init>(Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/InclusaoTributoResponseVO;Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo$1;)V
    .registers 3
    .param p1, "x0"    # Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/InclusaoTributoResponseVO;
    .param p2, "x1"    # Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo$1;

    .line 10
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo;-><init>(Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/InclusaoTributoResponseVO;)V

    return-void
.end method

.method static synthetic access$102(Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo;Ljava/lang/String;)Ljava/lang/String;
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo;
    .param p1, "x1"    # Ljava/lang/String;

    .line 10
    iput-object p1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo;->mensagem:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$202(Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo;Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;)Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo;
    .param p1, "x1"    # Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;

    .line 10
    iput-object p1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo;->detalhesAutorizacaoVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;

    return-object p1
.end method

.method static synthetic access$302(Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo;Z)Z
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo;
    .param p1, "x1"    # Z

    .line 10
    iput-boolean p1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo;->isMostraComprovantes:Z

    return p1
.end method

.method static synthetic access$402(Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo;Z)Z
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo;
    .param p1, "x1"    # Z

    .line 10
    iput-boolean p1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo;->isMostraDetalhesAutorizacao:Z

    return p1
.end method

.method static synthetic access$502(Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo;Ljava/lang/String;)Ljava/lang/String;
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo;
    .param p1, "x1"    # Ljava/lang/String;

    .line 10
    iput-object p1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo;->tipoPagamento:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$702(Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo;Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/InclusaoTributoResponseVO;)Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/InclusaoTributoResponseVO;
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo;
    .param p1, "x1"    # Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/InclusaoTributoResponseVO;

    .line 10
    iput-object p1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo;->inclusaoTributoResponseVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/InclusaoTributoResponseVO;

    return-object p1
.end method


# virtual methods
.method public getDataPagamento()Ljava/lang/String;
    .registers 2

    .line 87
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo;->dataPagamento:Ljava/lang/String;

    return-object v0
.end method

.method public getDetalhesAutorizacaoVO()Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;
    .registers 2

    .line 55
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo;->detalhesAutorizacaoVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;

    return-object v0
.end method

.method public getIdentificacaoComprovante()Ljava/lang/String;
    .registers 2

    .line 91
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo;->identificacaoComprovante:Ljava/lang/String;

    return-object v0
.end method

.method public getInclusaoTributoResponseVO()Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/InclusaoTributoResponseVO;
    .registers 2

    .line 51
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo;->inclusaoTributoResponseVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/InclusaoTributoResponseVO;

    return-object v0
.end method

.method public getTipoPagamento()Ljava/lang/String;
    .registers 2

    .line 75
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo;->tipoPagamento:Ljava/lang/String;

    return-object v0
.end method

.method public getValorPagamento()Ljava/lang/String;
    .registers 2

    .line 83
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo;->valorPagamento:Ljava/lang/String;

    return-object v0
.end method

.method public isIdentificadorComprovante()Z
    .registers 2

    .line 59
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo;->identificacaoComprovante:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_a

    const/4 v0, 0x1

    goto :goto_b

    :cond_a
    const/4 v0, 0x0

    :goto_b
    return v0
.end method

.method public isMostraComprovantes()Z
    .registers 2

    .line 63
    iget-boolean v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo;->isMostraComprovantes:Z

    return v0
.end method

.method public isMostraDetalhesAutorizacao()Z
    .registers 2

    .line 67
    iget-boolean v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo;->isMostraDetalhesAutorizacao:Z

    return v0
.end method

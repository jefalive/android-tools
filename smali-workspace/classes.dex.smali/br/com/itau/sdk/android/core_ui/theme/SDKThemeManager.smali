.class public Lbr/com/itau/sdk/android/core_ui/theme/SDKThemeManager;
.super Lbr/com/itau/sdk/android/core/theme/BaseSDKThemeManager;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 15
    invoke-direct {p0}, Lbr/com/itau/sdk/android/core/theme/BaseSDKThemeManager;-><init>()V

    return-void
.end method

.method public static getInstance()Lbr/com/itau/sdk/android/core_ui/theme/SDKThemeManager;
    .registers 1

    .line 122
    invoke-static {}, Lbr/com/itau/sdk/android/core_ui/theme/a;->a()Lbr/com/itau/sdk/android/core_ui/theme/SDKThemeManager;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public checkBackgroundTheme(Landroid/view/View;)V
    .registers 2

    .line 107
    invoke-static {p1}, Lbr/com/itau/sdk/android/core_ui/theme/b;->a(Landroid/view/View;)V

    .line 108
    return-void
.end method

.method public checkDialogTheme(Landroid/app/Dialog;)Landroid/app/Dialog;
    .registers 2

    .line 95
    invoke-static {p1}, Lbr/com/itau/sdk/android/core_ui/theme/b;->a(Landroid/app/Dialog;)V

    .line 96
    return-object p1
.end method

.method public checkDialogTheme(Landroid/support/v7/app/AlertDialog;)Landroid/support/v7/app/AlertDialog;
    .registers 2

    .line 101
    invoke-static {p1}, Lbr/com/itau/sdk/android/core_ui/theme/b;->a(Landroid/support/v7/app/AlertDialog;)V

    .line 102
    return-object p1
.end method

.method public checkLoadingTheme(Landroid/view/View;)V
    .registers 2

    .line 112
    invoke-static {p1}, Lbr/com/itau/sdk/android/core_ui/theme/b;->b(Landroid/view/View;)V

    .line 113
    return-void
.end method

.method public getDialogTheme()I
    .registers 2

    .line 117
    sget v0, Lbr/com/itau/sdk/android/core_ui/R$style;->itausdkcore_TokenDialog:I

    return v0
.end method

.method public getFontBold()Ljava/lang/String;
    .registers 3

    .line 90
    sget v0, Lbr/com/itau/sdk/android/core_ui/R$attr;->itausdkcore_custom_font_bold_path:I

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lbr/com/itau/sdk/android/core_ui/theme/SDKThemeManager;->getValue(ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getFontRegular()Ljava/lang/String;
    .registers 3

    .line 85
    sget v0, Lbr/com/itau/sdk/android/core_ui/R$attr;->itausdkcore_custom_font_regular_path:I

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lbr/com/itau/sdk/android/core_ui/theme/SDKThemeManager;->getValue(ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

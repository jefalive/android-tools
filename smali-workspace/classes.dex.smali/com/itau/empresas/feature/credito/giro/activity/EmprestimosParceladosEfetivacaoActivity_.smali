.class public final Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosEfetivacaoActivity_;
.super Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosEfetivacaoActivity;
.source "EmprestimosParceladosEfetivacaoActivity_.java"

# interfaces
.implements Lorg/androidannotations/api/view/HasViews;
.implements Lorg/androidannotations/api/view/OnViewChangedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosEfetivacaoActivity_$IntentBuilder_;
    }
.end annotation


# instance fields
.field private final onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;


# direct methods
.method public constructor <init>()V
    .registers 2

    .line 39
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosEfetivacaoActivity;-><init>()V

    .line 43
    new-instance v0, Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-direct {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosEfetivacaoActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    return-void
.end method

.method static synthetic access$001(Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosEfetivacaoActivity_;)V
    .registers 1
    .param p0, "x0"    # Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosEfetivacaoActivity_;

    .line 39
    invoke-super {p0}, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosEfetivacaoActivity;->mostraDialogoDeProgresso()V

    return-void
.end method

.method static synthetic access$101(Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosEfetivacaoActivity_;)V
    .registers 1
    .param p0, "x0"    # Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosEfetivacaoActivity_;

    .line 39
    invoke-super {p0}, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosEfetivacaoActivity;->escondeDialogoDeProgresso()V

    return-void
.end method

.method private init_(Landroid/os/Bundle;)V
    .registers 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 60
    new-instance v0, Lcom/itau/empresas/Preferences_;

    invoke-direct {v0, p0}, Lcom/itau/empresas/Preferences_;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosEfetivacaoActivity_;->prefs:Lcom/itau/empresas/Preferences_;

    .line 61
    invoke-static {p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->registerOnViewChangedListener(Lorg/androidannotations/api/view/OnViewChangedListener;)V

    .line 62
    invoke-static {}, Lcom/itau/empresas/CustomApplication_;->getInstance()Lcom/itau/empresas/CustomApplication;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosEfetivacaoActivity_;->application:Lcom/itau/empresas/CustomApplication;

    .line 63
    invoke-static {p0}, Lcom/itau/empresas/controller/EmprestimosParceladosEfetivacaoController_;->getInstance_(Landroid/content/Context;)Lcom/itau/empresas/controller/EmprestimosParceladosEfetivacaoController_;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosEfetivacaoActivity_;->controller:Lcom/itau/empresas/controller/EmprestimosParceladosEfetivacaoController;

    .line 64
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosEfetivacaoActivity_;->injectExtras_()V

    .line 65
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosEfetivacaoActivity_;->afterInject()V

    .line 66
    return-void
.end method

.method private injectExtras_()V
    .registers 3

    .line 120
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosEfetivacaoActivity_;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 121
    .local v1, "extras_":Landroid/os/Bundle;
    if-eqz v1, :cond_60

    .line 122
    const-string v0, "efetivacaoVO"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 123
    const-string v0, "efetivacaoVO"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/feature/credito/giro/model/ResultadoEfetivacaoEmprestimosParceladosVO;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosEfetivacaoActivity_;->efetivacaoVO:Lcom/itau/empresas/feature/credito/giro/model/ResultadoEfetivacaoEmprestimosParceladosVO;

    .line 125
    :cond_1c
    const-string v0, "simulacaoVO"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2e

    .line 126
    const-string v0, "simulacaoVO"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoSaidaVO;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosEfetivacaoActivity_;->simulacaoVO:Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoSaidaVO;

    .line 128
    :cond_2e
    const-string v0, "temSeguro"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3e

    .line 129
    const-string v0, "temSeguro"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosEfetivacaoActivity_;->temSeguro:Z

    .line 131
    :cond_3e
    const-string v0, "devedorSolidario"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4e

    .line 132
    const-string v0, "devedorSolidario"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosEfetivacaoActivity_;->devedorSolidario:Z

    .line 134
    :cond_4e
    const-string v0, "arquivoComprovante"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_60

    .line 135
    const-string v0, "arquivoComprovante"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosEfetivacaoActivity_;->arquivoComprovante:Ljava/io/File;

    .line 138
    :cond_60
    return-void
.end method


# virtual methods
.method public escondeDialogoDeProgresso()V
    .registers 5

    .line 163
    const-string v0, ""

    new-instance v1, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosEfetivacaoActivity_$3;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosEfetivacaoActivity_$3;-><init>(Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosEfetivacaoActivity_;)V

    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3}, Lorg/androidannotations/api/UiThreadExecutor;->runTask(Ljava/lang/String;Ljava/lang/Runnable;J)V

    .line 171
    return-void
.end method

.method public mostraDialogoDeProgresso()V
    .registers 5
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "CommitTransaction"
        }
    .end annotation

    .line 151
    const-string v0, ""

    new-instance v1, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosEfetivacaoActivity_$2;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosEfetivacaoActivity_$2;-><init>(Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosEfetivacaoActivity_;)V

    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3}, Lorg/androidannotations/api/UiThreadExecutor;->runTask(Ljava/lang/String;Ljava/lang/Runnable;J)V

    .line 159
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 52
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosEfetivacaoActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-static {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    move-result-object v1

    .line 53
    .local v1, "previousNotifier":Lorg/androidannotations/api/view/OnViewChangedNotifier;
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosEfetivacaoActivity_;->init_(Landroid/os/Bundle;)V

    .line 54
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosEfetivacaoActivity;->onCreate(Landroid/os/Bundle;)V

    .line 55
    invoke-static {v1}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 56
    const v0, 0x7f030031

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosEfetivacaoActivity_;->setContentView(I)V

    .line 57
    return-void
.end method

.method public onViewChanged(Lorg/androidannotations/api/view/HasViews;)V
    .registers 4
    .param p1, "hasViews"    # Lorg/androidannotations/api/view/HasViews;

    .line 100
    const v0, 0x7f0e0168

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosEfetivacaoActivity_;->textoEfetivacaoEmprestimosParceladosValor:Landroid/widget/TextView;

    .line 101
    const v0, 0x7f0e0167

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosEfetivacaoActivity_;->textoCreditoEfetuadoEm:Landroid/widget/TextView;

    .line 102
    const v0, 0x7f0e0166

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosEfetivacaoActivity_;->llLabelEfetivacaoSucesso:Landroid/widget/LinearLayout;

    .line 103
    const v0, 0x7f0e00b6

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/Toolbar;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosEfetivacaoActivity_;->toolBar:Landroid/support/v7/widget/Toolbar;

    .line 104
    const v0, 0x7f0e0169

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 106
    .local v1, "view_botao_mais_detalhes_emprestimos_parcelados_efetivacao":Landroid/view/View;
    if-eqz v1, :cond_3d

    .line 107
    new-instance v0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosEfetivacaoActivity_$1;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosEfetivacaoActivity_$1;-><init>(Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosEfetivacaoActivity_;)V

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 116
    :cond_3d
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosEfetivacaoActivity_;->aoCarregarElementosDeTela()V

    .line 117
    return-void
.end method

.method public setContentView(I)V
    .registers 3
    .param p1, "layoutResID"    # I

    .line 70
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosEfetivacaoActivity;->setContentView(I)V

    .line 71
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosEfetivacaoActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 72
    return-void
.end method

.method public setContentView(Landroid/view/View;)V
    .registers 3
    .param p1, "view"    # Landroid/view/View;

    .line 82
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosEfetivacaoActivity;->setContentView(Landroid/view/View;)V

    .line 83
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosEfetivacaoActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 84
    return-void
.end method

.method public setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .registers 4
    .param p1, "view"    # Landroid/view/View;
    .param p2, "params"    # Landroid/view/ViewGroup$LayoutParams;

    .line 76
    invoke-super {p0, p1, p2}, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosEfetivacaoActivity;->setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 77
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosEfetivacaoActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 78
    return-void
.end method

.method public setIntent(Landroid/content/Intent;)V
    .registers 2
    .param p1, "newIntent"    # Landroid/content/Intent;

    .line 142
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosEfetivacaoActivity;->setIntent(Landroid/content/Intent;)V

    .line 143
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosEfetivacaoActivity_;->injectExtras_()V

    .line 144
    return-void
.end method

.class public final Lcom/itau/empresas/feature/home/menu/adapter/MenuItemDecoration;
.super Landroid/support/v7/widget/RecyclerView$ItemDecoration;
.source "MenuItemDecoration.java"


# instance fields
.field private final offset:I


# direct methods
.method public constructor <init>(I)V
    .registers 2
    .param p1, "offset"    # I

    .line 15
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView$ItemDecoration;-><init>()V

    .line 16
    iput p1, p0, Lcom/itau/empresas/feature/home/menu/adapter/MenuItemDecoration;->offset:I

    .line 17
    return-void
.end method


# virtual methods
.method public getItemOffsets(Landroid/graphics/Rect;Landroid/view/View;Landroid/support/v7/widget/RecyclerView;Landroid/support/v7/widget/RecyclerView$State;)V
    .registers 7
    .param p1, "outRect"    # Landroid/graphics/Rect;
    .param p2, "view"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/support/v7/widget/RecyclerView;
    .param p4, "state"    # Landroid/support/v7/widget/RecyclerView$State;

    .line 23
    iget v0, p0, Lcom/itau/empresas/feature/home/menu/adapter/MenuItemDecoration;->offset:I

    div-int/lit8 v0, v0, 0x2

    iput v0, p1, Landroid/graphics/Rect;->left:I

    .line 24
    iget v0, p0, Lcom/itau/empresas/feature/home/menu/adapter/MenuItemDecoration;->offset:I

    div-int/lit8 v0, v0, 0x2

    iput v0, p1, Landroid/graphics/Rect;->right:I

    .line 25
    iget v0, p0, Lcom/itau/empresas/feature/home/menu/adapter/MenuItemDecoration;->offset:I

    iput v0, p1, Landroid/graphics/Rect;->bottom:I

    .line 26
    return-void
.end method

.class public Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity;
.super Lcom/itau/empresas/ui/activity/BaseActivity;
.source "CondicoesGeraisActivity.java"


# instance fields
.field llCondicoesGeraisDadosSeguro:Landroid/widget/LinearLayout;

.field llCondicoesGeraisDadosSeguroComSeguro:Landroid/widget/LinearLayout;

.field llCondicoesGeraisDeclaracaoGiro:Landroid/widget/LinearLayout;

.field llCondicoesGeraisDevedorSolidario:Landroid/widget/LinearLayout;

.field nome:Ljava/lang/String;

.field numeroDoc:Ljava/lang/String;

.field rlCondicoesGeraisContratacaoGiro:Landroid/widget/RelativeLayout;

.field rlCondicoesGeraisContratacaoGiroComSeguro:Landroid/widget/RelativeLayout;

.field rlCondicoesGeraisDadosSeguro:Landroid/widget/RelativeLayout;

.field rlCondicoesGeraisDevedorSolidario:Landroid/widget/RelativeLayout;

.field setaDesceCondicoesGeraisContratacaoGiro:Landroid/widget/ImageView;

.field setaDesceCondicoesGeraisContratacaoGiroComSeguro:Landroid/widget/ImageView;

.field setaDesceCondicoesGeraisDadosDoSeguro:Landroid/widget/ImageView;

.field setaDesceCondicoesGeraisDevedorSolidario:Landroid/widget/ImageView;

.field temDevedorSolidario:Z

.field temSeguro:Z

.field textoCondicoesGeraisContratacaoGiro:Landroid/widget/TextView;

.field textoCondicoesGeraisDevedorSolidario:Landroid/widget/TextView;

.field textoCondicoesGeraisLetrae:Landroid/widget/TextView;

.field textoCondicoesGeraisSeguroCapitalGiro:Landroid/widget/TextView;

.field textoConsulteCondicoesGerais:Landroid/widget/TextView;

.field textoConsulteCondicoesGiro:Landroid/widget/TextView;

.field tipoDoc:Ljava/lang/String;

.field toolBar:Landroid/support/v7/widget/Toolbar;

.field viewCondicoesGeraisContratacaoGiro:Landroid/view/View;

.field viewCondicoesGeraisDadosSeguro:Landroid/view/View;

.field viewCondicoesGeraisDadosSeguroComSeguro:Landroid/view/View;

.field viewCondicoesGeraisDevedorSolidario:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 25
    invoke-direct {p0}, Lcom/itau/empresas/ui/activity/BaseActivity;-><init>()V

    return-void
.end method

.method private abreWebViewComTermo(Ljava/lang/String;)V
    .registers 3
    .param p1, "termo"    # Ljava/lang/String;

    .line 203
    invoke-static {p0}, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisTermosActivity;->intent(Landroid/content/Context;)Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisTermosActivity_$IntentBuilder_;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisTermosActivity_$IntentBuilder_;->termo(Ljava/lang/String;)Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisTermosActivity_$IntentBuilder_;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisTermosActivity_$IntentBuilder_;->start()Lorg/androidannotations/api/builder/PostActivityStarter;

    .line 204
    return-void
.end method

.method private constroiToolbar()V
    .registers 3

    .line 76
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity;->toolBar:Landroid/support/v7/widget/Toolbar;

    invoke-static {v0, p0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom;->Builder(Landroid/support/v7/widget/Toolbar;Landroid/support/v7/app/AppCompatActivity;)Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    move-result-object v0

    .line 78
    const v1, 0x7f070697

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->comTitulo(I)Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    move-result-object v0

    .line 79
    invoke-virtual {v0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->comClickListenerFinish()Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    move-result-object v0

    .line 80
    invoke-virtual {v0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->build()Lcom/itau/empresas/ui/util/listener/ToolbarCustom;

    .line 81
    return-void
.end method

.method public static intent(Landroid/content/Context;)Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity_$IntentBuilder_;
    .registers 2
    .param p0, "context"    # Landroid/content/Context;

    .line 64
    new-instance v0, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity_$IntentBuilder_;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity_$IntentBuilder_;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method private setaAcessibilidade()V
    .registers 4

    .line 84
    const-string v2, ", toque para saber mais"

    .line 85
    .local v2, "complementoContentDescription":Ljava/lang/String;
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity;->rlCondicoesGeraisDevedorSolidario:Landroid/widget/RelativeLayout;

    .line 86
    const v1, 0x7f070526

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 87
    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 86
    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 89
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity;->rlCondicoesGeraisContratacaoGiro:Landroid/widget/RelativeLayout;

    .line 90
    const v1, 0x7f070524

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 91
    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 90
    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 93
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity;->rlCondicoesGeraisDadosSeguro:Landroid/widget/RelativeLayout;

    .line 94
    const v1, 0x7f070523

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 95
    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 94
    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 97
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity;->rlCondicoesGeraisContratacaoGiroComSeguro:Landroid/widget/RelativeLayout;

    .line 98
    const v1, 0x7f070525

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 99
    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 98
    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 101
    return-void
.end method

.method private setaTextoDevedorSolidario()Ljava/lang/String;
    .registers 3

    .line 207
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const v1, 0x7f070555

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity;->nome:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 211
    const v1, 0x7f070556

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity;->tipoDoc:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity;->numeroDoc:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 216
    const v1, 0x7f070557

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 207
    return-object v0
.end method

.method private verificaSeMostraAbaDevedorSolidario()V
    .registers 3

    .line 126
    iget-boolean v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity;->temDevedorSolidario:Z

    if-eqz v0, :cond_b

    .line 127
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity;->rlCondicoesGeraisDevedorSolidario:Landroid/widget/RelativeLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto :goto_12

    .line 129
    :cond_b
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity;->rlCondicoesGeraisDevedorSolidario:Landroid/widget/RelativeLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 131
    :goto_12
    return-void
.end method

.method private verificaSeMostraAbaSeguro()V
    .registers 3

    .line 104
    iget-boolean v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity;->temSeguro:Z

    if-eqz v0, :cond_18

    .line 105
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity;->rlCondicoesGeraisDadosSeguro:Landroid/widget/RelativeLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 106
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity;->rlCondicoesGeraisContratacaoGiroComSeguro:Landroid/widget/RelativeLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 107
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity;->rlCondicoesGeraisContratacaoGiro:Landroid/widget/RelativeLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto :goto_2c

    .line 109
    :cond_18
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity;->rlCondicoesGeraisDadosSeguro:Landroid/widget/RelativeLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 110
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity;->rlCondicoesGeraisContratacaoGiroComSeguro:Landroid/widget/RelativeLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 111
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity;->rlCondicoesGeraisContratacaoGiro:Landroid/widget/RelativeLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 113
    :goto_2c
    return-void
.end method

.method private verificaSeMostraTextoSeguro()V
    .registers 3

    .line 116
    iget-boolean v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity;->temSeguro:Z

    if-eqz v0, :cond_11

    .line 117
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity;->textoCondicoesGeraisLetrae:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 118
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity;->textoCondicoesGeraisSeguroCapitalGiro:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1f

    .line 120
    :cond_11
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity;->textoCondicoesGeraisLetrae:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 121
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity;->textoCondicoesGeraisSeguroCapitalGiro:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 123
    :goto_1f
    return-void
.end method


# virtual methods
.method aoClicarEmCondicoes()V
    .registers 2

    .line 134
    const-string v0, "giro"

    invoke-direct {p0, v0}, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity;->abreWebViewComTermo(Ljava/lang/String;)V

    .line 135
    return-void
.end method

.method aoClicarEmContratacaoGiro()V
    .registers 3

    .line 144
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity;->llCondicoesGeraisDeclaracaoGiro:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1f

    .line 145
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity;->setaDesceCondicoesGeraisContratacaoGiro:Landroid/widget/ImageView;

    const v1, 0x7f0201ee

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 146
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity;->llCondicoesGeraisDeclaracaoGiro:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 147
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity;->viewCondicoesGeraisContratacaoGiro:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_33

    .line 149
    :cond_1f
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity;->setaDesceCondicoesGeraisContratacaoGiro:Landroid/widget/ImageView;

    const v1, 0x7f0201f0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 150
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity;->llCondicoesGeraisDeclaracaoGiro:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 151
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity;->viewCondicoesGeraisContratacaoGiro:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 153
    :goto_33
    return-void
.end method

.method aoClicarEmContratacaoGiroComSeguro()V
    .registers 3

    .line 159
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity;->llCondicoesGeraisDadosSeguroComSeguro:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1f

    .line 160
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity;->setaDesceCondicoesGeraisContratacaoGiroComSeguro:Landroid/widget/ImageView;

    .line 161
    const v1, 0x7f0201ee

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 162
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity;->llCondicoesGeraisDadosSeguroComSeguro:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 163
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity;->viewCondicoesGeraisDadosSeguroComSeguro:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_33

    .line 165
    :cond_1f
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity;->setaDesceCondicoesGeraisContratacaoGiroComSeguro:Landroid/widget/ImageView;

    .line 166
    const v1, 0x7f0201f0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 167
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity;->llCondicoesGeraisDadosSeguroComSeguro:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 168
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity;->viewCondicoesGeraisDadosSeguroComSeguro:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 170
    :goto_33
    return-void
.end method

.method aoClicarEmDadosDoSeguro()V
    .registers 3

    .line 189
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity;->llCondicoesGeraisDadosSeguro:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1f

    .line 190
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity;->setaDesceCondicoesGeraisDadosDoSeguro:Landroid/widget/ImageView;

    const v1, 0x7f0201ee

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 191
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity;->llCondicoesGeraisDadosSeguro:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 192
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity;->viewCondicoesGeraisDadosSeguro:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_33

    .line 195
    :cond_1f
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity;->setaDesceCondicoesGeraisDadosDoSeguro:Landroid/widget/ImageView;

    const v1, 0x7f0201f0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 196
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity;->llCondicoesGeraisDadosSeguro:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 197
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity;->viewCondicoesGeraisDadosSeguro:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 200
    :goto_33
    return-void
.end method

.method aoClicarEmDevedorSolidario()V
    .registers 3

    .line 174
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity;->llCondicoesGeraisDevedorSolidario:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1f

    .line 175
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity;->setaDesceCondicoesGeraisDevedorSolidario:Landroid/widget/ImageView;

    const v1, 0x7f0201ee

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 176
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity;->llCondicoesGeraisDevedorSolidario:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 177
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity;->viewCondicoesGeraisDevedorSolidario:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_3c

    .line 179
    :cond_1f
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity;->setaDesceCondicoesGeraisDevedorSolidario:Landroid/widget/ImageView;

    const v1, 0x7f0201f0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 180
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity;->llCondicoesGeraisDevedorSolidario:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 181
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity;->viewCondicoesGeraisDevedorSolidario:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 182
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity;->textoCondicoesGeraisDevedorSolidario:Landroid/widget/TextView;

    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity;->setaTextoDevedorSolidario()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 184
    :goto_3c
    return-void
.end method

.method aoClicarEmSeguroCapitalGiro()V
    .registers 2

    .line 138
    const-string v0, "seguro"

    invoke-direct {p0, v0}, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity;->abreWebViewComTermo(Ljava/lang/String;)V

    .line 139
    return-void
.end method

.method aoInicializar()V
    .registers 1

    .line 68
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity;->constroiToolbar()V

    .line 69
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity;->verificaSeMostraTextoSeguro()V

    .line 70
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity;->verificaSeMostraAbaDevedorSolidario()V

    .line 71
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity;->verificaSeMostraAbaSeguro()V

    .line 72
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity;->setaAcessibilidade()V

    .line 73
    return-void
.end method

.method public opkeysConsumidas()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Ljava/lang/String;>;"
        }
    .end annotation

    .line 221
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

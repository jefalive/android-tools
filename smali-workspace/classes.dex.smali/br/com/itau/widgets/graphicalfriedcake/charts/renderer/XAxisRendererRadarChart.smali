.class public Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/XAxisRendererRadarChart;
.super Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/XAxisRenderer;
.source "XAxisRendererRadarChart.java"


# instance fields
.field private mChart:Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;


# direct methods
.method public constructor <init>(Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis;Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;)V
    .registers 5
    .param p1, "viewPortHandler"    # Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;
    .param p2, "xAxis"    # Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis;
    .param p3, "chart"    # Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;

    .line 20
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/XAxisRenderer;-><init>(Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis;Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Transformer;)V

    .line 23
    iput-object p3, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/XAxisRendererRadarChart;->mChart:Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;

    .line 24
    return-void
.end method


# virtual methods
.method public renderAxisLabels(Landroid/graphics/Canvas;)V
    .registers 13
    .param p1, "c"    # Landroid/graphics/Canvas;

    .line 31
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/XAxisRendererRadarChart;->mXAxis:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_10

    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/XAxisRendererRadarChart;->mXAxis:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis;->isDrawLabelsEnabled()Z

    move-result v0

    if-nez v0, :cond_11

    .line 32
    :cond_10
    return-void

    .line 35
    :cond_11
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/XAxisRendererRadarChart;->mAxisLabelPaint:Landroid/graphics/Paint;

    iget-object v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/XAxisRendererRadarChart;->mXAxis:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis;

    invoke-virtual {v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 36
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/XAxisRendererRadarChart;->mAxisLabelPaint:Landroid/graphics/Paint;

    iget-object v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/XAxisRendererRadarChart;->mXAxis:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis;

    invoke-virtual {v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis;->getTextSize()F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 37
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/XAxisRendererRadarChart;->mAxisLabelPaint:Landroid/graphics/Paint;

    iget-object v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/XAxisRendererRadarChart;->mXAxis:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis;

    invoke-virtual {v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis;->getTextColor()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 40
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/XAxisRendererRadarChart;->mChart:Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->getSliceAngle()F

    move-result v4

    .line 45
    .local v4, "sliceangle":F
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/XAxisRendererRadarChart;->mChart:Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->getFactor()F

    move-result v5

    .line 48
    .local v5, "factor":F
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/XAxisRendererRadarChart;->mChart:Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->getCenterOffsets()Landroid/graphics/PointF;

    move-result-object v6

    .line 51
    .local v6, "center":Landroid/graphics/PointF;
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_45
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/XAxisRendererRadarChart;->mXAxis:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis;->getValues()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v7, v0, :cond_94

    .line 54
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/XAxisRendererRadarChart;->mXAxis:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis;->getValues()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Ljava/lang/String;

    .line 57
    .local v8, "text":Ljava/lang/String;
    int-to-float v0, v7

    mul-float/2addr v0, v4

    iget-object v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/XAxisRendererRadarChart;->mChart:Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;

    invoke-virtual {v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->getRotationAngle()F

    move-result v1

    add-float/2addr v0, v1

    const/high16 v1, 0x43b40000    # 360.0f

    rem-float v9, v0, v1

    .line 60
    .local v9, "angle":F
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/XAxisRendererRadarChart;->mChart:Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->getYRange()F

    move-result v0

    mul-float/2addr v0, v5

    iget-object v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/XAxisRendererRadarChart;->mXAxis:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis;

    iget v1, v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis;->mLabelWidth:I

    int-to-float v1, v1

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    add-float/2addr v0, v1

    invoke-static {v6, v0, v9}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Utils;->getPosition(Landroid/graphics/PointF;FF)Landroid/graphics/PointF;

    move-result-object v10

    .line 64
    .local v10, "p":Landroid/graphics/PointF;
    iget v0, v10, Landroid/graphics/PointF;->x:F

    iget v1, v10, Landroid/graphics/PointF;->y:F

    iget-object v2, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/XAxisRendererRadarChart;->mXAxis:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis;

    iget v2, v2, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis;->mLabelHeight:I

    int-to-float v2, v2

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v2, v3

    add-float/2addr v1, v2

    iget-object v2, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/XAxisRendererRadarChart;->mAxisLabelPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v8, v0, v1, v2}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 51
    .end local v8    # "text":Ljava/lang/String;
    .end local v9    # "angle":F
    .end local v10    # "p":Landroid/graphics/PointF;
    add-int/lit8 v7, v7, 0x1

    goto :goto_45

    .line 66
    .end local v7    # "i":I
    :cond_94
    return-void
.end method

.method public renderLimitLines(Landroid/graphics/Canvas;)V
    .registers 2
    .param p1, "c"    # Landroid/graphics/Canvas;

    .line 77
    return-void
.end method

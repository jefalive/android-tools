.class public Lbr/com/itau/textovoz/ui/activity/ErrorSystemActivity;
.super Lbr/com/itau/textovoz/ui/activity/BaseActivity;
.source "ErrorSystemActivity.java"


# instance fields
.field private closeAppLibrary:Z

.field private final onClickListener:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>()V
    .registers 2

    .line 14
    invoke-direct {p0}, Lbr/com/itau/textovoz/ui/activity/BaseActivity;-><init>()V

    .line 59
    new-instance v0, Lbr/com/itau/textovoz/ui/activity/ErrorSystemActivity$1;

    invoke-direct {v0, p0}, Lbr/com/itau/textovoz/ui/activity/ErrorSystemActivity$1;-><init>(Lbr/com/itau/textovoz/ui/activity/ErrorSystemActivity;)V

    iput-object v0, p0, Lbr/com/itau/textovoz/ui/activity/ErrorSystemActivity;->onClickListener:Landroid/view/View$OnClickListener;

    return-void
.end method

.method static synthetic access$000(Lbr/com/itau/textovoz/ui/activity/ErrorSystemActivity;)Z
    .registers 2
    .param p0, "x0"    # Lbr/com/itau/textovoz/ui/activity/ErrorSystemActivity;

    .line 14
    iget-boolean v0, p0, Lbr/com/itau/textovoz/ui/activity/ErrorSystemActivity;->closeAppLibrary:Z

    return v0
.end method


# virtual methods
.method public onBackPressed()V
    .registers 3

    .line 77
    invoke-virtual {p0}, Lbr/com/itau/textovoz/ui/activity/ErrorSystemActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 78
    .local v1, "intent":Landroid/content/Intent;
    const/4 v0, -0x1

    invoke-virtual {p0, v0, v1}, Lbr/com/itau/textovoz/ui/activity/ErrorSystemActivity;->setResult(ILandroid/content/Intent;)V

    .line 79
    invoke-virtual {p0}, Lbr/com/itau/textovoz/ui/activity/ErrorSystemActivity;->finish()V

    .line 80
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .registers 10
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 21
    invoke-super {p0, p1}, Lbr/com/itau/textovoz/ui/activity/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 22
    sget v0, Lbr/com/itau/textovoz/R$layout;->activity_error_system:I

    invoke-virtual {p0, v0}, Lbr/com/itau/textovoz/ui/activity/ErrorSystemActivity;->setContentView(I)V

    .line 24
    sget v0, Lbr/com/itau/textovoz/R$id;->txt_error_system_color:I

    invoke-virtual {p0, v0}, Lbr/com/itau/textovoz/ui/activity/ErrorSystemActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lbr/com/itau/textovoz/ui/view/custom/CustomTextView;

    .line 25
    .local v2, "appCompatTextView":Lbr/com/itau/textovoz/ui/view/custom/CustomTextView;
    sget v0, Lbr/com/itau/textovoz/R$id;->txt_error_system_search:I

    invoke-virtual {p0, v0}, Lbr/com/itau/textovoz/ui/activity/ErrorSystemActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lbr/com/itau/textovoz/ui/view/custom/CustomTextView;

    .line 26
    .local v3, "customTextViewType":Lbr/com/itau/textovoz/ui/view/custom/CustomTextView;
    sget v0, Lbr/com/itau/textovoz/R$id;->error_system_close:I

    invoke-virtual {p0, v0}, Lbr/com/itau/textovoz/ui/activity/ErrorSystemActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lbr/com/itau/textovoz/ui/view/FontIconTextView;

    .line 28
    .local v4, "fontIconTextView":Lbr/com/itau/textovoz/ui/view/FontIconTextView;
    invoke-virtual {p0}, Lbr/com/itau/textovoz/ui/activity/ErrorSystemActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v5

    .line 29
    .local v5, "extras":Landroid/os/Bundle;
    if-eqz v5, :cond_94

    .line 31
    const-string v0, "type"

    invoke-virtual {v5, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 32
    .local v6, "segment":Ljava/lang/String;
    const-string v0, "messageText"

    invoke-virtual {v5, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 33
    .local v7, "messageText":Ljava/lang/String;
    const-string v0, "closeAppLibrary"

    const/4 v1, 0x0

    invoke-virtual {v5, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lbr/com/itau/textovoz/ui/activity/ErrorSystemActivity;->closeAppLibrary:Z

    .line 35
    if-eqz v3, :cond_47

    .line 36
    invoke-virtual {v3, v7}, Lbr/com/itau/textovoz/ui/view/custom/CustomTextView;->setText(Ljava/lang/CharSequence;)V

    .line 39
    :cond_47
    if-eqz v2, :cond_6b

    .line 40
    invoke-static {v6}, Lbr/com/itau/textovoz/util/CommonUtils;->getSegment(Ljava/lang/String;)I

    move-result v0

    const/4 v1, 0x4

    if-ne v1, v0, :cond_5e

    .line 41
    invoke-virtual {p0}, Lbr/com/itau/textovoz/ui/activity/ErrorSystemActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lbr/com/itau/textovoz/R$color;->personnalite_theme_color:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v2, v0}, Lbr/com/itau/textovoz/ui/view/custom/CustomTextView;->setTextColor(I)V

    goto :goto_6b

    .line 43
    :cond_5e
    invoke-virtual {p0}, Lbr/com/itau/textovoz/ui/activity/ErrorSystemActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lbr/com/itau/textovoz/R$color;->itau_theme_color:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v2, v0}, Lbr/com/itau/textovoz/ui/view/custom/CustomTextView;->setTextColor(I)V

    .line 47
    :cond_6b
    :goto_6b
    if-eqz v4, :cond_94

    .line 48
    invoke-static {v6}, Lbr/com/itau/textovoz/util/CommonUtils;->getSegment(Ljava/lang/String;)I

    move-result v0

    const/4 v1, 0x4

    if-ne v1, v0, :cond_82

    .line 49
    invoke-virtual {p0}, Lbr/com/itau/textovoz/ui/activity/ErrorSystemActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lbr/com/itau/textovoz/R$color;->personnalite_theme_color:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v4, v0}, Lbr/com/itau/textovoz/ui/view/FontIconTextView;->setTextColor(I)V

    goto :goto_8f

    .line 51
    :cond_82
    invoke-virtual {p0}, Lbr/com/itau/textovoz/ui/activity/ErrorSystemActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lbr/com/itau/textovoz/R$color;->itau_theme_color:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v4, v0}, Lbr/com/itau/textovoz/ui/view/FontIconTextView;->setTextColor(I)V

    .line 54
    :goto_8f
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/activity/ErrorSystemActivity;->onClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v4, v0}, Lbr/com/itau/textovoz/ui/view/FontIconTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 57
    .end local v6    # "segment":Ljava/lang/String;
    .end local v7    # "messageText":Ljava/lang/String;
    :cond_94
    return-void
.end method

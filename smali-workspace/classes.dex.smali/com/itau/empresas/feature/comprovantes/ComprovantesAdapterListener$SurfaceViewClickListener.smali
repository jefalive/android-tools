.class Lcom/itau/empresas/feature/comprovantes/ComprovantesAdapterListener$SurfaceViewClickListener;
.super Ljava/lang/Object;
.source "ComprovantesAdapterListener.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/itau/empresas/feature/comprovantes/ComprovantesAdapterListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "SurfaceViewClickListener"
.end annotation


# instance fields
.field private final chooseDetailClickListner:Landroid/view/View$OnClickListener;

.field private final comprovantesVO:Lcom/itau/empresas/feature/comprovantes/model/ComprovantesVO;

.field private final context:Landroid/content/Context;


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/itau/empresas/feature/comprovantes/model/ComprovantesVO;Landroid/view/View$OnClickListener;)V
    .registers 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "comprovantesVO"    # Lcom/itau/empresas/feature/comprovantes/model/ComprovantesVO;
    .param p3, "chooseDetailClickListner"    # Landroid/view/View$OnClickListener;

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/itau/empresas/feature/comprovantes/ComprovantesAdapterListener$SurfaceViewClickListener;->context:Landroid/content/Context;

    .line 23
    iput-object p2, p0, Lcom/itau/empresas/feature/comprovantes/ComprovantesAdapterListener$SurfaceViewClickListener;->comprovantesVO:Lcom/itau/empresas/feature/comprovantes/model/ComprovantesVO;

    .line 24
    iput-object p3, p0, Lcom/itau/empresas/feature/comprovantes/ComprovantesAdapterListener$SurfaceViewClickListener;->chooseDetailClickListner:Landroid/view/View$OnClickListener;

    .line 25
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .registers 4
    .param p1, "view"    # Landroid/view/View;

    .line 30
    iget-object v0, p0, Lcom/itau/empresas/feature/comprovantes/ComprovantesAdapterListener$SurfaceViewClickListener;->context:Landroid/content/Context;

    .line 31
    invoke-static {v0}, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesDetalheActivity_;->intent(Landroid/content/Context;)Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesDetalheActivity_$IntentBuilder_;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/comprovantes/ComprovantesAdapterListener$SurfaceViewClickListener;->comprovantesVO:Lcom/itau/empresas/feature/comprovantes/model/ComprovantesVO;

    .line 32
    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesDetalheActivity_$IntentBuilder_;->comprovantesVO(Lcom/itau/empresas/feature/comprovantes/model/ComprovantesVO;)Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesDetalheActivity_$IntentBuilder_;

    move-result-object v0

    .line 33
    invoke-virtual {v0}, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesDetalheActivity_$IntentBuilder_;->start()Lorg/androidannotations/api/builder/PostActivityStarter;

    .line 35
    iget-object v0, p0, Lcom/itau/empresas/feature/comprovantes/ComprovantesAdapterListener$SurfaceViewClickListener;->chooseDetailClickListner:Landroid/view/View$OnClickListener;

    if-eqz v0, :cond_18

    .line 36
    iget-object v0, p0, Lcom/itau/empresas/feature/comprovantes/ComprovantesAdapterListener$SurfaceViewClickListener;->chooseDetailClickListner:Landroid/view/View$OnClickListener;

    invoke-interface {v0, p1}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    .line 38
    :cond_18
    return-void
.end method

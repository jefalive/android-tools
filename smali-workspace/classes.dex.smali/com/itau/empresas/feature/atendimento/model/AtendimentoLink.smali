.class public Lcom/itau/empresas/feature/atendimento/model/AtendimentoLink;
.super Ljava/lang/Object;
.source "AtendimentoLink.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private ref:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "ref"
    .end annotation
.end field

.field private texto:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "texto"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public setRef(Ljava/lang/String;)V
    .registers 2
    .param p1, "ref"    # Ljava/lang/String;

    .line 24
    iput-object p1, p0, Lcom/itau/empresas/feature/atendimento/model/AtendimentoLink;->ref:Ljava/lang/String;

    .line 25
    return-void
.end method

.method public setTexto(Ljava/lang/String;)V
    .registers 2
    .param p1, "texto"    # Ljava/lang/String;

    .line 20
    iput-object p1, p0, Lcom/itau/empresas/feature/atendimento/model/AtendimentoLink;->texto:Ljava/lang/String;

    .line 21
    return-void
.end method

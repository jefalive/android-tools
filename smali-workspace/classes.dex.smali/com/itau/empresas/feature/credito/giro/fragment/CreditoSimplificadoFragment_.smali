.class public final Lcom/itau/empresas/feature/credito/giro/fragment/CreditoSimplificadoFragment_;
.super Lcom/itau/empresas/feature/credito/giro/fragment/CreditoSimplificadoFragment;
.source "CreditoSimplificadoFragment_.java"

# interfaces
.implements Lorg/androidannotations/api/view/HasViews;
.implements Lorg/androidannotations/api/view/OnViewChangedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/itau/empresas/feature/credito/giro/fragment/CreditoSimplificadoFragment_$FragmentBuilder_;
    }
.end annotation


# instance fields
.field private contentView_:Landroid/view/View;

.field private final onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;


# direct methods
.method public constructor <init>()V
    .registers 2

    .line 26
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/giro/fragment/CreditoSimplificadoFragment;-><init>()V

    .line 30
    new-instance v0, Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-direct {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/fragment/CreditoSimplificadoFragment_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    return-void
.end method

.method public static builder()Lcom/itau/empresas/feature/credito/giro/fragment/CreditoSimplificadoFragment_$FragmentBuilder_;
    .registers 1

    .line 84
    new-instance v0, Lcom/itau/empresas/feature/credito/giro/fragment/CreditoSimplificadoFragment_$FragmentBuilder_;

    invoke-direct {v0}, Lcom/itau/empresas/feature/credito/giro/fragment/CreditoSimplificadoFragment_$FragmentBuilder_;-><init>()V

    return-object v0
.end method

.method private init_(Landroid/os/Bundle;)V
    .registers 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 71
    invoke-static {p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->registerOnViewChangedListener(Lorg/androidannotations/api/view/OnViewChangedListener;)V

    .line 72
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/giro/fragment/CreditoSimplificadoFragment_;->injectFragmentArguments_()V

    .line 73
    invoke-static {}, Lcom/itau/empresas/CustomApplication_;->getInstance()Lcom/itau/empresas/CustomApplication;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/fragment/CreditoSimplificadoFragment_;->application:Lcom/itau/empresas/CustomApplication;

    .line 74
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/giro/fragment/CreditoSimplificadoFragment_;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/itau/empresas/feature/credito/giro/controller/EmprestimosParceladosController_;->getInstance_(Landroid/content/Context;)Lcom/itau/empresas/feature/credito/giro/controller/EmprestimosParceladosController_;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/fragment/CreditoSimplificadoFragment_;->controller:Lcom/itau/empresas/feature/credito/giro/controller/EmprestimosParceladosController;

    .line 75
    return-void
.end method

.method private injectFragmentArguments_()V
    .registers 3

    .line 98
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/giro/fragment/CreditoSimplificadoFragment_;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    .line 99
    .local v1, "args_":Landroid/os/Bundle;
    if-eqz v1, :cond_16

    .line 100
    const-string v0, "mock"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_16

    .line 101
    const-string v0, "mock"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/itau/empresas/feature/credito/giro/fragment/CreditoSimplificadoFragment_;->mock:I

    .line 104
    :cond_16
    return-void
.end method


# virtual methods
.method public findViewById(I)Landroid/view/View;
    .registers 3
    .param p1, "id"    # I

    .line 44
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/fragment/CreditoSimplificadoFragment_;->contentView_:Landroid/view/View;

    if-nez v0, :cond_6

    .line 45
    const/4 v0, 0x0

    return-object v0

    .line 47
    :cond_6
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/fragment/CreditoSimplificadoFragment_;->contentView_:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 36
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/fragment/CreditoSimplificadoFragment_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-static {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    move-result-object v1

    .line 37
    .local v1, "previousNotifier":Lorg/androidannotations/api/view/OnViewChangedNotifier;
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/credito/giro/fragment/CreditoSimplificadoFragment_;->init_(Landroid/os/Bundle;)V

    .line 38
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/credito/giro/fragment/CreditoSimplificadoFragment;->onCreate(Landroid/os/Bundle;)V

    .line 39
    invoke-static {v1}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 40
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .registers 6
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .line 52
    invoke-super {p0, p1, p2, p3}, Lcom/itau/empresas/feature/credito/giro/fragment/CreditoSimplificadoFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/fragment/CreditoSimplificadoFragment_;->contentView_:Landroid/view/View;

    .line 53
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/fragment/CreditoSimplificadoFragment_;->contentView_:Landroid/view/View;

    if-nez v0, :cond_14

    .line 54
    const v0, 0x7f03009b

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/fragment/CreditoSimplificadoFragment_;->contentView_:Landroid/view/View;

    .line 56
    :cond_14
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/fragment/CreditoSimplificadoFragment_;->contentView_:Landroid/view/View;

    return-object v0
.end method

.method public onDestroyView()V
    .registers 2

    .line 61
    invoke-super {p0}, Lcom/itau/empresas/feature/credito/giro/fragment/CreditoSimplificadoFragment;->onDestroyView()V

    .line 62
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/fragment/CreditoSimplificadoFragment_;->contentView_:Landroid/view/View;

    .line 63
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/fragment/CreditoSimplificadoFragment_;->itemEmprestimoParcelado:Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView;

    .line 64
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/fragment/CreditoSimplificadoFragment_;->viewIrPara:Lcom/itau/empresas/ui/view/IrParaView;

    .line 65
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/fragment/CreditoSimplificadoFragment_;->viewCards:Landroid/widget/LinearLayout;

    .line 66
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/fragment/CreditoSimplificadoFragment_;->llActivityEmprestimosParceladosEstadoVazio:Landroid/widget/LinearLayout;

    .line 67
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/fragment/CreditoSimplificadoFragment_;->textCreditoSimplificadoEmptyView:Landroid/widget/TextView;

    .line 68
    return-void
.end method

.method public onViewChanged(Lorg/androidannotations/api/view/HasViews;)V
    .registers 3
    .param p1, "hasViews"    # Lorg/androidannotations/api/view/HasViews;

    .line 89
    const v0, 0x7f0e0401

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/fragment/CreditoSimplificadoFragment_;->itemEmprestimoParcelado:Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView;

    .line 90
    const v0, 0x7f0e016a

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/ui/view/IrParaView;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/fragment/CreditoSimplificadoFragment_;->viewIrPara:Lcom/itau/empresas/ui/view/IrParaView;

    .line 91
    const v0, 0x7f0e0400

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/fragment/CreditoSimplificadoFragment_;->viewCards:Landroid/widget/LinearLayout;

    .line 92
    const v0, 0x7f0e03fd

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/fragment/CreditoSimplificadoFragment_;->llActivityEmprestimosParceladosEstadoVazio:Landroid/widget/LinearLayout;

    .line 93
    const v0, 0x7f0e03ff

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/fragment/CreditoSimplificadoFragment_;->textCreditoSimplificadoEmptyView:Landroid/widget/TextView;

    .line 94
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/giro/fragment/CreditoSimplificadoFragment_;->aoIniciarElementosDeTela()V

    .line 95
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .registers 4
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;

    .line 79
    invoke-super {p0, p1, p2}, Lcom/itau/empresas/feature/credito/giro/fragment/CreditoSimplificadoFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 80
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/fragment/CreditoSimplificadoFragment_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 81
    return-void
.end method

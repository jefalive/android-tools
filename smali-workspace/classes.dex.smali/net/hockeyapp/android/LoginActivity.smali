.class public Lnet/hockeyapp/android/LoginActivity;
.super Landroid/app/Activity;
.source "LoginActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private loginHandler:Landroid/os/Handler;

.field private loginTask:Lnet/hockeyapp/android/tasks/LoginTask;

.field private mode:I

.field private secret:Ljava/lang/String;

.field private url:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 57
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method private configureView()V
    .registers 4

    .line 107
    iget v0, p0, Lnet/hockeyapp/android/LoginActivity;->mode:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_12

    .line 108
    const/16 v0, 0x3004

    invoke-virtual {p0, v0}, Lnet/hockeyapp/android/LoginActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Landroid/widget/EditText;

    .line 109
    .local v2, "passwordInput":Landroid/widget/EditText;
    const/4 v0, 0x4

    invoke-virtual {v2, v0}, Landroid/widget/EditText;->setVisibility(I)V

    .line 112
    .end local v2    # "passwordInput":Landroid/widget/EditText;
    :cond_12
    const/16 v0, 0x3005

    invoke-virtual {p0, v0}, Lnet/hockeyapp/android/LoginActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Landroid/widget/Button;

    .line 113
    .local v2, "loginButton":Landroid/widget/Button;
    invoke-virtual {v2, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 114
    return-void
.end method

.method private initLoginHandler()V
    .registers 2

    .line 117
    new-instance v0, Lnet/hockeyapp/android/LoginActivity$1;

    invoke-direct {v0, p0}, Lnet/hockeyapp/android/LoginActivity$1;-><init>(Lnet/hockeyapp/android/LoginActivity;)V

    iput-object v0, p0, Lnet/hockeyapp/android/LoginActivity;->loginHandler:Landroid/os/Handler;

    .line 135
    return-void
.end method

.method private performAuthentication()V
    .registers 11

    .line 138
    const/16 v0, 0x3003

    invoke-virtual {p0, v0}, Lnet/hockeyapp/android/LoginActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "performAuthentication"
    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    check-cast v0, Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    .line 139
    .local v6, "email":Ljava/lang/String;
    const/16 v0, 0x3004

    invoke-virtual {p0, v0}, Lnet/hockeyapp/android/LoginActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v7

    .line 141
    .local v7, "password":Ljava/lang/String;
    const/4 v8, 0x0

    .line 142
    .local v8, "ready":Z
    new-instance v9, Ljava/util/HashMap;

    invoke-direct {v9}, Ljava/util/HashMap;-><init>()V

    .line 144
    .local v9, "params":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    iget v0, p0, Lnet/hockeyapp/android/LoginActivity;->mode:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_56

    .line 145
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_33

    const/4 v8, 0x1

    goto :goto_34

    :cond_33
    const/4 v8, 0x0

    .line 146
    :goto_34
    const-string v0, "email"

    invoke-interface {v9, v0, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 147
    const-string v0, "authcode"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lnet/hockeyapp/android/LoginActivity;->secret:Ljava/lang/String;

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lnet/hockeyapp/android/LoginActivity;->md5(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v9, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_74

    .line 149
    :cond_56
    iget v0, p0, Lnet/hockeyapp/android/LoginActivity;->mode:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_74

    .line 150
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_69

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_69

    const/4 v8, 0x1

    goto :goto_6a

    :cond_69
    const/4 v8, 0x0

    .line 151
    :goto_6a
    const-string v0, "email"

    invoke-interface {v9, v0, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 152
    const-string v0, "password"

    invoke-interface {v9, v0, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 155
    :cond_74
    :goto_74
    if-eqz v8, :cond_8b

    .line 156
    new-instance v0, Lnet/hockeyapp/android/tasks/LoginTask;

    iget-object v2, p0, Lnet/hockeyapp/android/LoginActivity;->loginHandler:Landroid/os/Handler;

    iget-object v3, p0, Lnet/hockeyapp/android/LoginActivity;->url:Ljava/lang/String;

    iget v4, p0, Lnet/hockeyapp/android/LoginActivity;->mode:I

    move-object v1, p0

    move-object v5, v9

    invoke-direct/range {v0 .. v5}, Lnet/hockeyapp/android/tasks/LoginTask;-><init>(Landroid/content/Context;Landroid/os/Handler;Ljava/lang/String;ILjava/util/Map;)V

    iput-object v0, p0, Lnet/hockeyapp/android/LoginActivity;->loginTask:Lnet/hockeyapp/android/tasks/LoginTask;

    .line 157
    iget-object v0, p0, Lnet/hockeyapp/android/LoginActivity;->loginTask:Lnet/hockeyapp/android/tasks/LoginTask;

    invoke-static {v0}, Lnet/hockeyapp/android/utils/AsyncTaskUtils;->execute(Landroid/os/AsyncTask;)V

    goto :goto_9a

    .line 160
    :cond_8b
    const/16 v0, 0x501

    invoke-static {v0}, Lnet/hockeyapp/android/Strings;->get(I)Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0x3e8

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 162
    :goto_9a
    return-void
.end method


# virtual methods
.method public md5(Ljava/lang/String;)Ljava/lang/String;
    .registers 12
    .param p1, "s"    # Ljava/lang/String;

    .line 167
    const-string v0, "MD5"

    invoke-static {v0, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_2
    invoke-static {v0}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v2

    .line 168
    .local v2, "digest":Ljava/security/MessageDigest;
    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/security/MessageDigest;->update([B)V

    .line 169
    invoke-virtual {v2}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v3

    .line 172
    .local v3, "messageDigest":[B
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 173
    .local v4, "hexString":Ljava/lang/StringBuilder;
    move-object v5, v3

    array-length v6, v5

    const/4 v7, 0x0

    :goto_19
    if-ge v7, v6, :cond_44

    aget-byte v8, v5, v7

    .line 174
    .local v8, "aMessageDigest":B
    and-int/lit16 v0, v8, 0xff

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v9

    .line 175
    .local v9, "h":Ljava/lang/String;
    :goto_23
    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x2

    if-ge v0, v1, :cond_3e

    .line 176
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "0"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    goto :goto_23

    .line 177
    :cond_3e
    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 173
    .end local v8    # "aMessageDigest":B
    .end local v9    # "h":Ljava/lang/String;
    add-int/lit8 v7, v7, 0x1

    goto :goto_19

    .line 179
    :cond_44
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_47
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_2 .. :try_end_47} :catch_49

    move-result-object v0

    return-object v0

    .line 181
    .end local v2    # "digest":Ljava/security/MessageDigest;
    .end local v3    # "messageDigest":[B
    .end local v4    # "hexString":Ljava/lang/StringBuilder;
    :catch_49
    move-exception v2

    .line 182
    .local v2, "e":Ljava/security/NoSuchAlgorithmException;
    invoke-virtual {v2}, Ljava/security/NoSuchAlgorithmException;->printStackTrace()V

    .line 184
    .end local v2    # "e":Ljava/security/NoSuchAlgorithmException;
    const-string v0, ""

    return-object v0
.end method

.method public onClick(Landroid/view/View;)V
    .registers 3
    .param p1, "v"    # Landroid/view/View;

    .line 189
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const-string v1, "LogLegal"
    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    sparse-switch v0, :sswitch_data_c

    goto :goto_b

    .line 191
    :sswitch_8
    invoke-direct {p0}, Lnet/hockeyapp/android/LoginActivity;->performAuthentication()V

    .line 192
    .line 197
    :goto_b
    return-void

    :sswitch_data_c
    .sparse-switch
        0x3005 -> :sswitch_8
    .end sparse-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .registers 6
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 85
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 86
    new-instance v0, Lnet/hockeyapp/android/views/LoginView;

    invoke-direct {v0, p0}, Lnet/hockeyapp/android/views/LoginView;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0, v0}, Lnet/hockeyapp/android/LoginActivity;->setContentView(Landroid/view/View;)V

    .line 88
    invoke-virtual {p0}, Lnet/hockeyapp/android/LoginActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    .line 89
    .local v2, "extras":Landroid/os/Bundle;
    if-eqz v2, :cond_2d

    .line 90
    const-string v0, "url"

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnet/hockeyapp/android/LoginActivity;->url:Ljava/lang/String;

    .line 91
    const-string v0, "secret"

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnet/hockeyapp/android/LoginActivity;->secret:Ljava/lang/String;

    .line 92
    const-string v0, "mode"

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lnet/hockeyapp/android/LoginActivity;->mode:I

    .line 95
    :cond_2d
    invoke-direct {p0}, Lnet/hockeyapp/android/LoginActivity;->configureView()V

    .line 96
    invoke-direct {p0}, Lnet/hockeyapp/android/LoginActivity;->initLoginHandler()V

    .line 99
    invoke-virtual {p0}, Lnet/hockeyapp/android/LoginActivity;->getLastNonConfigurationInstance()Ljava/lang/Object;

    move-result-object v3

    .line 100
    .local v3, "object":Ljava/lang/Object;
    if-eqz v3, :cond_45

    .line 101
    move-object v0, v3

    check-cast v0, Lnet/hockeyapp/android/tasks/LoginTask;

    iput-object v0, p0, Lnet/hockeyapp/android/LoginActivity;->loginTask:Lnet/hockeyapp/android/tasks/LoginTask;

    .line 102
    iget-object v0, p0, Lnet/hockeyapp/android/LoginActivity;->loginTask:Lnet/hockeyapp/android/tasks/LoginTask;

    iget-object v1, p0, Lnet/hockeyapp/android/LoginActivity;->loginHandler:Landroid/os/Handler;

    invoke-virtual {v0, p0, v1}, Lnet/hockeyapp/android/tasks/LoginTask;->attach(Landroid/content/Context;Landroid/os/Handler;)V

    .line 104
    :cond_45
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .registers 6
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .line 201
    const/4 v0, 0x4

    if-ne p1, v0, :cond_24

    .line 202
    sget-object v0, Lnet/hockeyapp/android/LoginManager;->listener:Lnet/hockeyapp/android/LoginManagerListener;

    if-eqz v0, :cond_d

    .line 203
    sget-object v0, Lnet/hockeyapp/android/LoginManager;->listener:Lnet/hockeyapp/android/LoginManagerListener;

    invoke-virtual {v0}, Lnet/hockeyapp/android/LoginManagerListener;->onBack()V

    goto :goto_24

    .line 206
    :cond_d
    new-instance v2, Landroid/content/Intent;

    sget-object v0, Lnet/hockeyapp/android/LoginManager;->mainActivity:Ljava/lang/Class;

    invoke-direct {v2, p0, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 207
    .local v2, "intent":Landroid/content/Intent;
    const/high16 v0, 0x4000000

    invoke-virtual {v2, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 208
    const-string v0, "net.hockeyapp.android.EXIT"

    const/4 v1, 0x1

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 209
    invoke-virtual {p0, v2}, Lnet/hockeyapp/android/LoginActivity;->startActivity(Landroid/content/Intent;)V

    .line 210
    const/4 v0, 0x1

    return v0

    .line 214
    .end local v2    # "intent":Landroid/content/Intent;
    :cond_24
    :goto_24
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method public onRetainNonConfigurationInstance()Ljava/lang/Object;
    .registers 2

    .line 226
    iget-object v0, p0, Lnet/hockeyapp/android/LoginActivity;->loginTask:Lnet/hockeyapp/android/tasks/LoginTask;

    if-eqz v0, :cond_9

    .line 227
    iget-object v0, p0, Lnet/hockeyapp/android/LoginActivity;->loginTask:Lnet/hockeyapp/android/tasks/LoginTask;

    invoke-virtual {v0}, Lnet/hockeyapp/android/tasks/LoginTask;->detach()V

    .line 230
    :cond_9
    iget-object v0, p0, Lnet/hockeyapp/android/LoginActivity;->loginTask:Lnet/hockeyapp/android/tasks/LoginTask;

    return-object v0
.end method

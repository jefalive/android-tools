.class public Lcom/itau/empresas/ui/activity/DetalhesProdutosLisActivity;
.super Lcom/itau/empresas/ui/activity/BaseActivity;
.source "DetalhesProdutosLisActivity.java"


# instance fields
.field detalhesProdutosListAdapter:Lcom/itau/empresas/adapter/DetalhesLisAdapter;

.field private itens:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Lcom/itau/empresas/api/model/DetalheLisVO;>;"
        }
    .end annotation
.end field

.field lisVO:Lcom/itau/empresas/api/model/LisVO;

.field lista:Landroid/widget/ListView;

.field toolBar:Landroid/support/v7/widget/Toolbar;


# direct methods
.method public constructor <init>()V
    .registers 2

    .line 28
    invoke-direct {p0}, Lcom/itau/empresas/ui/activity/BaseActivity;-><init>()V

    .line 37
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/ui/activity/DetalhesProdutosLisActivity;->itens:Ljava/util/List;

    return-void
.end method

.method private adicionarItem(Ljava/lang/String;Ljava/lang/String;Lcom/itau/empresas/api/model/DetalheLisVO$Tipo;)V
    .registers 6
    .param p1, "titulo"    # Ljava/lang/String;
    .param p2, "valor"    # Ljava/lang/String;
    .param p3, "tipo"    # Lcom/itau/empresas/api/model/DetalheLisVO$Tipo;

    .line 78
    if-eqz p1, :cond_17

    if-eqz p2, :cond_17

    .line 79
    new-instance v1, Lcom/itau/empresas/api/model/DetalheLisVO;

    invoke-direct {v1}, Lcom/itau/empresas/api/model/DetalheLisVO;-><init>()V

    .line 80
    .local v1, "lis":Lcom/itau/empresas/api/model/DetalheLisVO;
    invoke-virtual {v1, p1}, Lcom/itau/empresas/api/model/DetalheLisVO;->setTitulo(Ljava/lang/String;)V

    .line 81
    invoke-virtual {v1, p2}, Lcom/itau/empresas/api/model/DetalheLisVO;->setValor(Ljava/lang/String;)V

    .line 82
    invoke-virtual {v1, p3}, Lcom/itau/empresas/api/model/DetalheLisVO;->setTipo(Lcom/itau/empresas/api/model/DetalheLisVO$Tipo;)V

    .line 83
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/DetalhesProdutosLisActivity;->itens:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 85
    .end local v1    # "lis":Lcom/itau/empresas/api/model/DetalheLisVO;
    :cond_17
    return-void
.end method

.method private constroiToolbar()V
    .registers 3

    .line 88
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/DetalhesProdutosLisActivity;->toolBar:Landroid/support/v7/widget/Toolbar;

    invoke-static {v0, p0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom;->Builder(Landroid/support/v7/widget/Toolbar;Landroid/support/v7/app/AppCompatActivity;)Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    move-result-object v0

    .line 89
    const v1, 0x7f0706a3

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->comTitulo(I)Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    move-result-object v0

    .line 90
    invoke-virtual {v0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->comClickListenerFinish()Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    move-result-object v0

    .line 91
    invoke-virtual {v0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->build()Lcom/itau/empresas/ui/util/listener/ToolbarCustom;

    .line 92
    return-void
.end method

.method public static intent(Landroid/content/Context;)Lcom/itau/empresas/ui/activity/DetalhesProdutosLisActivity_$IntentBuilder_;
    .registers 2
    .param p0, "context"    # Landroid/content/Context;

    .line 100
    new-instance v0, Lcom/itau/empresas/ui/activity/DetalhesProdutosLisActivity_$IntentBuilder_;

    invoke-direct {v0, p0}, Lcom/itau/empresas/ui/activity/DetalhesProdutosLisActivity_$IntentBuilder_;-><init>(Landroid/content/Context;)V

    return-object v0
.end method


# virtual methods
.method aoIniciarActivity()V
    .registers 4

    .line 41
    invoke-direct {p0}, Lcom/itau/empresas/ui/activity/DetalhesProdutosLisActivity;->constroiToolbar()V

    .line 43
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/DetalhesProdutosLisActivity;->lisVO:Lcom/itau/empresas/api/model/LisVO;

    if-eqz v0, :cond_111

    .line 44
    const v0, 0x7f070595

    invoke-virtual {p0, v0}, Lcom/itau/empresas/ui/activity/DetalhesProdutosLisActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/ui/activity/DetalhesProdutosLisActivity;->lisVO:Lcom/itau/empresas/api/model/LisVO;

    invoke-virtual {v1}, Lcom/itau/empresas/api/model/LisVO;->getLimiteTotal()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/itau/empresas/api/model/DetalheLisVO$Tipo;->DINHEIRO:Lcom/itau/empresas/api/model/DetalheLisVO$Tipo;

    invoke-direct {p0, v0, v1, v2}, Lcom/itau/empresas/ui/activity/DetalhesProdutosLisActivity;->adicionarItem(Ljava/lang/String;Ljava/lang/String;Lcom/itau/empresas/api/model/DetalheLisVO$Tipo;)V

    .line 45
    const v0, 0x7f070593

    invoke-virtual {p0, v0}, Lcom/itau/empresas/ui/activity/DetalhesProdutosLisActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/ui/activity/DetalhesProdutosLisActivity;->lisVO:Lcom/itau/empresas/api/model/LisVO;

    invoke-virtual {v1}, Lcom/itau/empresas/api/model/LisVO;->getLimiteDisponivel()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/itau/empresas/api/model/DetalheLisVO$Tipo;->DINHEIRO:Lcom/itau/empresas/api/model/DetalheLisVO$Tipo;

    invoke-direct {p0, v0, v1, v2}, Lcom/itau/empresas/ui/activity/DetalhesProdutosLisActivity;->adicionarItem(Ljava/lang/String;Ljava/lang/String;Lcom/itau/empresas/api/model/DetalheLisVO$Tipo;)V

    .line 47
    const v0, 0x7f070596

    invoke-virtual {p0, v0}, Lcom/itau/empresas/ui/activity/DetalhesProdutosLisActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/ui/activity/DetalhesProdutosLisActivity;->lisVO:Lcom/itau/empresas/api/model/LisVO;

    invoke-virtual {v1}, Lcom/itau/empresas/api/model/LisVO;->getLimiteUtilizado()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/itau/empresas/api/model/DetalheLisVO$Tipo;->DINHEIRO:Lcom/itau/empresas/api/model/DetalheLisVO$Tipo;

    invoke-direct {p0, v0, v1, v2}, Lcom/itau/empresas/ui/activity/DetalhesProdutosLisActivity;->adicionarItem(Ljava/lang/String;Ljava/lang/String;Lcom/itau/empresas/api/model/DetalheLisVO$Tipo;)V

    .line 49
    const v0, 0x7f070594

    invoke-virtual {p0, v0}, Lcom/itau/empresas/ui/activity/DetalhesProdutosLisActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/ui/activity/DetalhesProdutosLisActivity;->lisVO:Lcom/itau/empresas/api/model/LisVO;

    invoke-virtual {v1}, Lcom/itau/empresas/api/model/LisVO;->getLimiteLis()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/itau/empresas/api/model/DetalheLisVO$Tipo;->DINHEIRO:Lcom/itau/empresas/api/model/DetalheLisVO$Tipo;

    invoke-direct {p0, v0, v1, v2}, Lcom/itau/empresas/ui/activity/DetalhesProdutosLisActivity;->adicionarItem(Ljava/lang/String;Ljava/lang/String;Lcom/itau/empresas/api/model/DetalheLisVO$Tipo;)V

    .line 50
    const v0, 0x7f070592

    invoke-virtual {p0, v0}, Lcom/itau/empresas/ui/activity/DetalhesProdutosLisActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/ui/activity/DetalhesProdutosLisActivity;->lisVO:Lcom/itau/empresas/api/model/LisVO;

    invoke-virtual {v1}, Lcom/itau/empresas/api/model/LisVO;->getLimiteAdicional()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/itau/empresas/api/model/DetalheLisVO$Tipo;->DINHEIRO:Lcom/itau/empresas/api/model/DetalheLisVO$Tipo;

    invoke-direct {p0, v0, v1, v2}, Lcom/itau/empresas/ui/activity/DetalhesProdutosLisActivity;->adicionarItem(Ljava/lang/String;Ljava/lang/String;Lcom/itau/empresas/api/model/DetalheLisVO$Tipo;)V

    .line 52
    const v0, 0x7f070550

    invoke-virtual {p0, v0}, Lcom/itau/empresas/ui/activity/DetalhesProdutosLisActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/ui/activity/DetalhesProdutosLisActivity;->lisVO:Lcom/itau/empresas/api/model/LisVO;

    invoke-virtual {v1}, Lcom/itau/empresas/api/model/LisVO;->getDataVencimento()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/itau/empresas/api/model/DetalheLisVO$Tipo;->DATA:Lcom/itau/empresas/api/model/DetalheLisVO$Tipo;

    invoke-direct {p0, v0, v1, v2}, Lcom/itau/empresas/ui/activity/DetalhesProdutosLisActivity;->adicionarItem(Ljava/lang/String;Ljava/lang/String;Lcom/itau/empresas/api/model/DetalheLisVO$Tipo;)V

    .line 55
    const v0, 0x7f070558

    invoke-virtual {p0, v0}, Lcom/itau/empresas/ui/activity/DetalhesProdutosLisActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/ui/activity/DetalhesProdutosLisActivity;->lisVO:Lcom/itau/empresas/api/model/LisVO;

    .line 56
    invoke-virtual {v1}, Lcom/itau/empresas/api/model/LisVO;->getUtilizacao()Lcom/itau/empresas/api/model/UtilizacaoVO;

    move-result-object v1

    invoke-virtual {v1}, Lcom/itau/empresas/api/model/UtilizacaoVO;->getDiasUtilizados()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/itau/empresas/api/model/DetalheLisVO$Tipo;->STRING:Lcom/itau/empresas/api/model/DetalheLisVO$Tipo;

    .line 55
    invoke-direct {p0, v0, v1, v2}, Lcom/itau/empresas/ui/activity/DetalhesProdutosLisActivity;->adicionarItem(Ljava/lang/String;Ljava/lang/String;Lcom/itau/empresas/api/model/DetalheLisVO$Tipo;)V

    .line 57
    const v0, 0x7f070590

    invoke-virtual {p0, v0}, Lcom/itau/empresas/ui/activity/DetalhesProdutosLisActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/ui/activity/DetalhesProdutosLisActivity;->lisVO:Lcom/itau/empresas/api/model/LisVO;

    .line 58
    invoke-virtual {v1}, Lcom/itau/empresas/api/model/LisVO;->getUtilizacao()Lcom/itau/empresas/api/model/UtilizacaoVO;

    move-result-object v1

    invoke-virtual {v1}, Lcom/itau/empresas/api/model/UtilizacaoVO;->getJurosAcumulados()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/itau/empresas/api/model/DetalheLisVO$Tipo;->STRING:Lcom/itau/empresas/api/model/DetalheLisVO$Tipo;

    .line 57
    invoke-direct {p0, v0, v1, v2}, Lcom/itau/empresas/ui/activity/DetalhesProdutosLisActivity;->adicionarItem(Ljava/lang/String;Ljava/lang/String;Lcom/itau/empresas/api/model/DetalheLisVO$Tipo;)V

    .line 59
    const v0, 0x7f07054e

    invoke-virtual {p0, v0}, Lcom/itau/empresas/ui/activity/DetalhesProdutosLisActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/ui/activity/DetalhesProdutosLisActivity;->lisVO:Lcom/itau/empresas/api/model/LisVO;

    .line 60
    invoke-virtual {v1}, Lcom/itau/empresas/api/model/LisVO;->getUtilizacao()Lcom/itau/empresas/api/model/UtilizacaoVO;

    move-result-object v1

    invoke-virtual {v1}, Lcom/itau/empresas/api/model/UtilizacaoVO;->getDataJurosAcumulados()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/itau/empresas/api/model/DetalheLisVO$Tipo;->STRING:Lcom/itau/empresas/api/model/DetalheLisVO$Tipo;

    .line 59
    invoke-direct {p0, v0, v1, v2}, Lcom/itau/empresas/ui/activity/DetalhesProdutosLisActivity;->adicionarItem(Ljava/lang/String;Ljava/lang/String;Lcom/itau/empresas/api/model/DetalheLisVO$Tipo;)V

    .line 62
    const v0, 0x7f070677

    invoke-virtual {p0, v0}, Lcom/itau/empresas/ui/activity/DetalhesProdutosLisActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/ui/activity/DetalhesProdutosLisActivity;->lisVO:Lcom/itau/empresas/api/model/LisVO;

    .line 63
    invoke-virtual {v1}, Lcom/itau/empresas/api/model/LisVO;->getTaxas()Lcom/itau/empresas/api/model/TaxasLimiteVO;

    move-result-object v1

    invoke-virtual {v1}, Lcom/itau/empresas/api/model/TaxasLimiteVO;->getTaxaContratoMensal()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/itau/empresas/api/model/DetalheLisVO$Tipo;->DINHEIRO:Lcom/itau/empresas/api/model/DetalheLisVO$Tipo;

    .line 62
    invoke-direct {p0, v0, v1, v2}, Lcom/itau/empresas/ui/activity/DetalhesProdutosLisActivity;->adicionarItem(Ljava/lang/String;Ljava/lang/String;Lcom/itau/empresas/api/model/DetalheLisVO$Tipo;)V

    .line 64
    const v0, 0x7f070676

    invoke-virtual {p0, v0}, Lcom/itau/empresas/ui/activity/DetalhesProdutosLisActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/ui/activity/DetalhesProdutosLisActivity;->lisVO:Lcom/itau/empresas/api/model/LisVO;

    .line 65
    invoke-virtual {v1}, Lcom/itau/empresas/api/model/LisVO;->getTaxas()Lcom/itau/empresas/api/model/TaxasLimiteVO;

    move-result-object v1

    invoke-virtual {v1}, Lcom/itau/empresas/api/model/TaxasLimiteVO;->getTaxaContratoAnual()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/itau/empresas/api/model/DetalheLisVO$Tipo;->DINHEIRO:Lcom/itau/empresas/api/model/DetalheLisVO$Tipo;

    .line 64
    invoke-direct {p0, v0, v1, v2}, Lcom/itau/empresas/ui/activity/DetalhesProdutosLisActivity;->adicionarItem(Ljava/lang/String;Ljava/lang/String;Lcom/itau/empresas/api/model/DetalheLisVO$Tipo;)V

    .line 66
    const v0, 0x7f070516

    invoke-virtual {p0, v0}, Lcom/itau/empresas/ui/activity/DetalhesProdutosLisActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/ui/activity/DetalhesProdutosLisActivity;->lisVO:Lcom/itau/empresas/api/model/LisVO;

    invoke-virtual {v1}, Lcom/itau/empresas/api/model/LisVO;->getTaxas()Lcom/itau/empresas/api/model/TaxasLimiteVO;

    move-result-object v1

    invoke-virtual {v1}, Lcom/itau/empresas/api/model/TaxasLimiteVO;->getTaxaCetMensal()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/itau/empresas/api/model/DetalheLisVO$Tipo;->DINHEIRO:Lcom/itau/empresas/api/model/DetalheLisVO$Tipo;

    invoke-direct {p0, v0, v1, v2}, Lcom/itau/empresas/ui/activity/DetalhesProdutosLisActivity;->adicionarItem(Ljava/lang/String;Ljava/lang/String;Lcom/itau/empresas/api/model/DetalheLisVO$Tipo;)V

    .line 68
    const v0, 0x7f070515

    invoke-virtual {p0, v0}, Lcom/itau/empresas/ui/activity/DetalhesProdutosLisActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/ui/activity/DetalhesProdutosLisActivity;->lisVO:Lcom/itau/empresas/api/model/LisVO;

    invoke-virtual {v1}, Lcom/itau/empresas/api/model/LisVO;->getTaxas()Lcom/itau/empresas/api/model/TaxasLimiteVO;

    move-result-object v1

    invoke-virtual {v1}, Lcom/itau/empresas/api/model/TaxasLimiteVO;->getTaxaCetAnual()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/itau/empresas/api/model/DetalheLisVO$Tipo;->DINHEIRO:Lcom/itau/empresas/api/model/DetalheLisVO$Tipo;

    invoke-direct {p0, v0, v1, v2}, Lcom/itau/empresas/ui/activity/DetalhesProdutosLisActivity;->adicionarItem(Ljava/lang/String;Ljava/lang/String;Lcom/itau/empresas/api/model/DetalheLisVO$Tipo;)V

    .line 72
    :cond_111
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/DetalhesProdutosLisActivity;->detalhesProdutosListAdapter:Lcom/itau/empresas/adapter/DetalhesLisAdapter;

    invoke-virtual {v0, p0}, Lcom/itau/empresas/adapter/DetalhesLisAdapter;->setContext(Landroid/content/Context;)V

    .line 73
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/DetalhesProdutosLisActivity;->detalhesProdutosListAdapter:Lcom/itau/empresas/adapter/DetalhesLisAdapter;

    iget-object v1, p0, Lcom/itau/empresas/ui/activity/DetalhesProdutosLisActivity;->itens:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/itau/empresas/adapter/DetalhesLisAdapter;->setData(Ljava/util/List;)V

    .line 74
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/DetalhesProdutosLisActivity;->lista:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/itau/empresas/ui/activity/DetalhesProdutosLisActivity;->detalhesProdutosListAdapter:Lcom/itau/empresas/adapter/DetalhesLisAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 75
    return-void
.end method

.method public opkeysConsumidas()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Ljava/lang/String;>;"
        }
    .end annotation

    .line 96
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

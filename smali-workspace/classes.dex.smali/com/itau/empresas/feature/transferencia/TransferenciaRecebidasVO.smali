.class public Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasVO;
.super Ljava/lang/Object;
.source "TransferenciaRecebidasVO.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private agenciaOrigem:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "agencia_origem"
    .end annotation
.end field

.field private bancoOrigem:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "banco_origem"
    .end annotation
.end field

.field private codigoFinalidade:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "codigo_finalidade"
    .end annotation
.end field

.field private contaOrigem:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "conta_origem"
    .end annotation
.end field

.field private cpfCnpj:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "cpf_cnpj"
    .end annotation
.end field

.field private dataRecebimento:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "data_recebimento"
    .end annotation
.end field

.field private digitoVerificadorContaOrigem:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "digito_verificador_conta_origem"
    .end annotation
.end field

.field private finalidade:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "finalidade"
    .end annotation
.end field

.field private ispbBanco:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "ispb_banco"
    .end annotation
.end field

.field private numeroDoc:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "numero_doc"
    .end annotation
.end field

.field private remetente:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "remetente"
    .end annotation
.end field

.field private tipoPagamento:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "tipo_pagamento"
    .end annotation
.end field

.field private valorPagamento:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "valor_pagamento"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getCpfCnpj()Ljava/lang/String;
    .registers 2

    .line 41
    iget-object v0, p0, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasVO;->cpfCnpj:Ljava/lang/String;

    return-object v0
.end method

.method public getDataRecebimento()Ljava/lang/String;
    .registers 2

    .line 45
    iget-object v0, p0, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasVO;->dataRecebimento:Ljava/lang/String;

    return-object v0
.end method

.method public getRemetente()Ljava/lang/String;
    .registers 2

    .line 37
    iget-object v0, p0, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasVO;->remetente:Ljava/lang/String;

    return-object v0
.end method

.method public getTipoPagamento()Ljava/lang/String;
    .registers 2

    .line 54
    iget-object v0, p0, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasVO;->tipoPagamento:Ljava/lang/String;

    return-object v0
.end method

.method public getValorPagamento()Ljava/lang/String;
    .registers 2

    .line 50
    iget-object v0, p0, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasVO;->valorPagamento:Ljava/lang/String;

    return-object v0
.end method

.class public Lnet/hockeyapp/android/utils/VersionCache;
.super Ljava/lang/Object;
.source "VersionCache.java"


# static fields
.field private static VERSION_INFO_KEY:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .line 41
    const-string v0, "versionInfo"

    sput-object v0, Lnet/hockeyapp/android/utils/VersionCache;->VERSION_INFO_KEY:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .registers 1

    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getVersionInfo(Landroid/content/Context;)Ljava/lang/String;
    .registers 4
    .param p0, "context"    # Landroid/content/Context;

    .line 53
    if-eqz p0, :cond_12

    .line 54
    const-string v0, "HockeyApp"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 55
    .local v2, "preferences":Landroid/content/SharedPreferences;
    sget-object v0, Lnet/hockeyapp/android/utils/VersionCache;->VERSION_INFO_KEY:Ljava/lang/String;

    const-string v1, "[]"

    invoke-interface {v2, v0, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 58
    .end local v2    # "preferences":Landroid/content/SharedPreferences;
    :cond_12
    const-string v0, "[]"

    return-object v0
.end method

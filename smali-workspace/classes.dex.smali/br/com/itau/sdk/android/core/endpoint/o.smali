.class public Lbr/com/itau/sdk/android/core/endpoint/o;
.super Lokhttp3/RequestBody;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lbr/com/itau/sdk/android/core/endpoint/o$a;
    }
.end annotation


# instance fields
.field protected a:Lokhttp3/RequestBody;

.field protected b:Lbr/com/itau/sdk/android/core/SDKProgressListener;

.field protected c:Lbr/com/itau/sdk/android/core/endpoint/o$a;


# direct methods
.method public constructor <init>(Lokhttp3/RequestBody;Lbr/com/itau/sdk/android/core/SDKProgressListener;)V
    .registers 3

    .line 21
    invoke-direct {p0}, Lokhttp3/RequestBody;-><init>()V

    .line 22
    iput-object p1, p0, Lbr/com/itau/sdk/android/core/endpoint/o;->a:Lokhttp3/RequestBody;

    .line 23
    iput-object p2, p0, Lbr/com/itau/sdk/android/core/endpoint/o;->b:Lbr/com/itau/sdk/android/core/SDKProgressListener;

    .line 24
    return-void
.end method


# virtual methods
.method public contentLength()J
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 33
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/endpoint/o;->a:Lokhttp3/RequestBody;

    invoke-virtual {v0}, Lokhttp3/RequestBody;->contentLength()J

    move-result-wide v0

    return-wide v0
.end method

.method public contentType()Lokhttp3/MediaType;
    .registers 2

    .line 28
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/endpoint/o;->a:Lokhttp3/RequestBody;

    invoke-virtual {v0}, Lokhttp3/RequestBody;->contentType()Lokhttp3/MediaType;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lokio/BufferedSink;)V
    .registers 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 40
    new-instance v0, Lbr/com/itau/sdk/android/core/endpoint/o$a;

    invoke-direct {v0, p0, p1}, Lbr/com/itau/sdk/android/core/endpoint/o$a;-><init>(Lbr/com/itau/sdk/android/core/endpoint/o;Lokio/Sink;)V

    iput-object v0, p0, Lbr/com/itau/sdk/android/core/endpoint/o;->c:Lbr/com/itau/sdk/android/core/endpoint/o$a;

    .line 41
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/endpoint/o;->c:Lbr/com/itau/sdk/android/core/endpoint/o$a;

    invoke-static {v0}, Lokio/Okio;->buffer(Lokio/Sink;)Lokio/BufferedSink;

    move-result-object v1

    .line 43
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/endpoint/o;->a:Lokhttp3/RequestBody;

    invoke-virtual {v0, v1}, Lokhttp3/RequestBody;->writeTo(Lokio/BufferedSink;)V

    .line 45
    invoke-interface {v1}, Lokio/BufferedSink;->flush()V

    .line 46
    return-void
.end method

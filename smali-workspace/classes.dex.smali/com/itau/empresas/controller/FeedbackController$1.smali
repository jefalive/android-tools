.class Lcom/itau/empresas/controller/FeedbackController$1;
.super Ljava/lang/Object;
.source "FeedbackController.java"

# interfaces
.implements Lbr/com/itau/sdk/android/core/type/BackendCall;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/itau/empresas/controller/FeedbackController;->enviaFeedback(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/itau/empresas/controller/FeedbackController;

.field final synthetic val$comentario:Ljava/lang/String;

.field final synthetic val$email:Ljava/lang/String;

.field final synthetic val$idCategoria:I

.field final synthetic val$nota:I

.field final synthetic val$origem:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/itau/empresas/controller/FeedbackController;ILjava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
    .registers 7
    .param p1, "this$0"    # Lcom/itau/empresas/controller/FeedbackController;

    .line 29
    iput-object p1, p0, Lcom/itau/empresas/controller/FeedbackController$1;->this$0:Lcom/itau/empresas/controller/FeedbackController;

    iput p2, p0, Lcom/itau/empresas/controller/FeedbackController$1;->val$idCategoria:I

    iput-object p3, p0, Lcom/itau/empresas/controller/FeedbackController$1;->val$origem:Ljava/lang/String;

    iput p4, p0, Lcom/itau/empresas/controller/FeedbackController$1;->val$nota:I

    iput-object p5, p0, Lcom/itau/empresas/controller/FeedbackController$1;->val$email:Ljava/lang/String;

    iput-object p6, p0, Lcom/itau/empresas/controller/FeedbackController$1;->val$comentario:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public callBackend()V
    .registers 10

    .line 33
    # getter for: Lcom/itau/empresas/controller/FeedbackController;->CODIGO_APP:Ljava/lang/String;
    invoke-static {}, Lcom/itau/empresas/controller/FeedbackController;->access$000()Ljava/lang/String;

    move-result-object v0

    iget v1, p0, Lcom/itau/empresas/controller/FeedbackController$1;->val$idCategoria:I

    .line 34
    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    .line 35
    # getter for: Lcom/itau/empresas/controller/FeedbackController;->GUID:Ljava/lang/String;
    invoke-static {}, Lcom/itau/empresas/controller/FeedbackController;->access$100()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/itau/empresas/controller/FeedbackController$1;->this$0:Lcom/itau/empresas/controller/FeedbackController;

    iget-object v4, p0, Lcom/itau/empresas/controller/FeedbackController$1;->val$origem:Ljava/lang/String;

    iget v5, p0, Lcom/itau/empresas/controller/FeedbackController$1;->val$nota:I

    iget-object v6, p0, Lcom/itau/empresas/controller/FeedbackController$1;->val$email:Ljava/lang/String;

    iget-object v7, p0, Lcom/itau/empresas/controller/FeedbackController$1;->val$comentario:Ljava/lang/String;

    .line 36
    # invokes: Lcom/itau/empresas/controller/FeedbackController;->getConteudoFeedback(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    invoke-static {v3, v4, v5, v6, v7}, Lcom/itau/empresas/controller/FeedbackController;->access$200(Lcom/itau/empresas/controller/FeedbackController;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/itau/empresas/controller/FeedbackController$1;->this$0:Lcom/itau/empresas/controller/FeedbackController;

    .line 37
    # invokes: Lcom/itau/empresas/controller/FeedbackController;->getDadosCliente()Ljava/lang/String;
    invoke-static {v4}, Lcom/itau/empresas/controller/FeedbackController;->access$300(Lcom/itau/empresas/controller/FeedbackController;)Ljava/lang/String;

    move-result-object v4

    const-class v5, Lcom/itau/empresas/api/model/ResultadoFeedbackVO;

    .line 32
    invoke-static/range {v0 .. v5}, Lbr/com/itau/sdk/android/core/BackendClient;->sendFeedbackForApp(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/itau/empresas/api/model/ResultadoFeedbackVO;

    .line 40
    .local v8, "resultadoFeedbackVO":Lcom/itau/empresas/api/model/ResultadoFeedbackVO;
    invoke-static {v8}, Lcom/itau/empresas/controller/BaseController;->post(Ljava/lang/Object;)V

    .line 41
    return-void
.end method

.class public Lcom/itau/empresas/ui/util/analytics/EventoAnalyticsRes;
.super Ljava/lang/Object;
.source "EventoAnalyticsRes.java"


# instance fields
.field private acaoRes:I

.field private categoriaRes:I

.field private labelRes:I

.field private valor:Ljava/lang/Long;


# direct methods
.method public constructor <init>(III)V
    .registers 5
    .param p1, "categoria"    # I
    .param p2, "acao"    # I
    .param p3, "label"    # I

    .line 16
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/itau/empresas/ui/util/analytics/EventoAnalyticsRes;-><init>(IIILjava/lang/Long;)V

    .line 18
    return-void
.end method

.method public constructor <init>(IIILjava/lang/Long;)V
    .registers 5
    .param p1, "categoria"    # I
    .param p2, "acao"    # I
    .param p3, "label"    # I
    .param p4, "valor"    # Ljava/lang/Long;

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput p1, p0, Lcom/itau/empresas/ui/util/analytics/EventoAnalyticsRes;->categoriaRes:I

    .line 22
    iput p2, p0, Lcom/itau/empresas/ui/util/analytics/EventoAnalyticsRes;->acaoRes:I

    .line 23
    iput p3, p0, Lcom/itau/empresas/ui/util/analytics/EventoAnalyticsRes;->labelRes:I

    .line 24
    iput-object p4, p0, Lcom/itau/empresas/ui/util/analytics/EventoAnalyticsRes;->valor:Ljava/lang/Long;

    .line 25
    return-void
.end method


# virtual methods
.method public toEventoGA(Landroid/content/Context;)Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;
    .registers 7
    .param p1, "context"    # Landroid/content/Context;

    .line 28
    iget v0, p0, Lcom/itau/empresas/ui/util/analytics/EventoAnalyticsRes;->categoriaRes:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 29
    .local v2, "categoria":Ljava/lang/String;
    iget v0, p0, Lcom/itau/empresas/ui/util/analytics/EventoAnalyticsRes;->acaoRes:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 30
    .local v3, "acao":Ljava/lang/String;
    iget v0, p0, Lcom/itau/empresas/ui/util/analytics/EventoAnalyticsRes;->labelRes:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_18

    iget v0, p0, Lcom/itau/empresas/ui/util/analytics/EventoAnalyticsRes;->labelRes:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    goto :goto_19

    :cond_18
    const/4 v4, 0x0

    .line 32
    .local v4, "label":Ljava/lang/String;
    :goto_19
    new-instance v0, Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;

    iget-object v1, p0, Lcom/itau/empresas/ui/util/analytics/EventoAnalyticsRes;->valor:Ljava/lang/Long;

    invoke-direct {v0, v2, v3, v4, v1}, Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    return-object v0
.end method

.class public Lcom/itau/empresas/api/model/FrancesinhaVO$DadosPeriodo;
.super Ljava/lang/Object;
.source "FrancesinhaVO.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/itau/empresas/api/model/FrancesinhaVO;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "DadosPeriodo"
.end annotation


# instance fields
.field private dataTitulos:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "data_titulos"
    .end annotation
.end field

.field private literalTitulos:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "literal_titulos"
    .end annotation
.end field

.field private quantidadeTitulos:I
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "quantidade_titulos"
    .end annotation
.end field

.field private valorTitulos:Ljava/lang/Float;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "valor_titulos"
    .end annotation
.end field


# virtual methods
.method public getLiteralTitulos()Ljava/lang/String;
    .registers 2

    .line 127
    iget-object v0, p0, Lcom/itau/empresas/api/model/FrancesinhaVO$DadosPeriodo;->literalTitulos:Ljava/lang/String;

    return-object v0
.end method

.method public getQuantidadeTitulos()I
    .registers 2

    .line 135
    iget v0, p0, Lcom/itau/empresas/api/model/FrancesinhaVO$DadosPeriodo;->quantidadeTitulos:I

    return v0
.end method

.method public getValorTitulos()Ljava/lang/Float;
    .registers 2

    .line 131
    iget-object v0, p0, Lcom/itau/empresas/api/model/FrancesinhaVO$DadosPeriodo;->valorTitulos:Ljava/lang/Float;

    return-object v0
.end method

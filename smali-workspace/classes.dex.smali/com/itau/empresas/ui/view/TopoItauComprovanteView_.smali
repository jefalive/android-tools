.class public final Lcom/itau/empresas/ui/view/TopoItauComprovanteView_;
.super Lcom/itau/empresas/ui/view/TopoItauComprovanteView;
.source "TopoItauComprovanteView_.java"

# interfaces
.implements Lorg/androidannotations/api/view/HasViews;


# instance fields
.field private alreadyInflated_:Z

.field private final onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .param p1, "context"    # Landroid/content/Context;

    .line 32
    invoke-direct {p0, p1}, Lcom/itau/empresas/ui/view/TopoItauComprovanteView;-><init>(Landroid/content/Context;)V

    .line 28
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/itau/empresas/ui/view/TopoItauComprovanteView_;->alreadyInflated_:Z

    .line 29
    new-instance v0, Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-direct {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/ui/view/TopoItauComprovanteView_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 33
    invoke-direct {p0}, Lcom/itau/empresas/ui/view/TopoItauComprovanteView_;->init_()V

    .line 34
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .line 37
    invoke-direct {p0, p1, p2}, Lcom/itau/empresas/ui/view/TopoItauComprovanteView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 28
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/itau/empresas/ui/view/TopoItauComprovanteView_;->alreadyInflated_:Z

    .line 29
    new-instance v0, Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-direct {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/ui/view/TopoItauComprovanteView_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 38
    invoke-direct {p0}, Lcom/itau/empresas/ui/view/TopoItauComprovanteView_;->init_()V

    .line 39
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .line 42
    invoke-direct {p0, p1, p2, p3}, Lcom/itau/empresas/ui/view/TopoItauComprovanteView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 28
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/itau/empresas/ui/view/TopoItauComprovanteView_;->alreadyInflated_:Z

    .line 29
    new-instance v0, Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-direct {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/ui/view/TopoItauComprovanteView_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 43
    invoke-direct {p0}, Lcom/itau/empresas/ui/view/TopoItauComprovanteView_;->init_()V

    .line 44
    return-void
.end method

.method private init_()V
    .registers 3

    .line 69
    iget-object v0, p0, Lcom/itau/empresas/ui/view/TopoItauComprovanteView_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-static {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    move-result-object v1

    .line 70
    .local v1, "previousNotifier":Lorg/androidannotations/api/view/OnViewChangedNotifier;
    invoke-static {v1}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 71
    return-void
.end method


# virtual methods
.method public onFinishInflate()V
    .registers 3

    .line 60
    iget-boolean v0, p0, Lcom/itau/empresas/ui/view/TopoItauComprovanteView_;->alreadyInflated_:Z

    if-nez v0, :cond_16

    .line 61
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/itau/empresas/ui/view/TopoItauComprovanteView_;->alreadyInflated_:Z

    .line 62
    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/TopoItauComprovanteView_;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f030186

    invoke-static {v0, v1, p0}, Lcom/itau/empresas/ui/view/TopoItauComprovanteView_;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 63
    iget-object v0, p0, Lcom/itau/empresas/ui/view/TopoItauComprovanteView_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 65
    :cond_16
    invoke-super {p0}, Lcom/itau/empresas/ui/view/TopoItauComprovanteView;->onFinishInflate()V

    .line 66
    return-void
.end method

.class public Lbr/com/itau/library/LinearValuePickerView;
.super Landroid/widget/LinearLayout;
.source "LinearValuePickerView.java"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# instance fields
.field private action:Lbr/com/itau/library/br/com/itau/library/interfaces/IAction;

.field private alreadyInflated:Z

.field private animated:Z

.field private color:I

.field private fontBold:Landroid/graphics/Typeface;

.field private fontRegular:Landroid/graphics/Typeface;

.field private interpolator:Landroid/view/animation/Interpolator;

.field private linearValuepickerValueText:Landroid/widget/LinearLayout;

.field private normalDrawable:Landroid/graphics/drawable/LayerDrawable;

.field private radioButtonsList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Landroid/widget/RadioButton;>;"
        }
    .end annotation
.end field

.field private radiogroupValuepickerRadiogroup:Landroid/widget/RadioGroup;

.field private selectedDrawable:Landroid/graphics/drawable/LayerDrawable;

.field private selectedPosition:I

.field private textColor:I

.field private textValueList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Landroid/widget/TextView;>;"
        }
    .end annotation
.end field

.field private typefaceBold:Ljava/lang/String;

.field private typefaceRegular:Ljava/lang/String;

.field private values:[Ljava/lang/String;

.field private viewValuepickerLine:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .param p1, "context"    # Landroid/content/Context;

    .line 65
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 50
    const/4 v0, -0x1

    iput v0, p0, Lbr/com/itau/library/LinearValuePickerView;->selectedPosition:I

    .line 57
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbr/com/itau/library/LinearValuePickerView;->animated:Z

    .line 58
    new-instance v0, Landroid/support/v4/view/animation/FastOutSlowInInterpolator;

    invoke-direct {v0}, Landroid/support/v4/view/animation/FastOutSlowInInterpolator;-><init>()V

    iput-object v0, p0, Lbr/com/itau/library/LinearValuePickerView;->interpolator:Landroid/view/animation/Interpolator;

    .line 59
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbr/com/itau/library/LinearValuePickerView;->alreadyInflated:Z

    .line 60
    const-string v0, "#969696"

    invoke-static {v0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lbr/com/itau/library/LinearValuePickerView;->textColor:I

    .line 66
    invoke-direct {p0}, Lbr/com/itau/library/LinearValuePickerView;->init()V

    .line 67
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .line 70
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 50
    const/4 v0, -0x1

    iput v0, p0, Lbr/com/itau/library/LinearValuePickerView;->selectedPosition:I

    .line 57
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbr/com/itau/library/LinearValuePickerView;->animated:Z

    .line 58
    new-instance v0, Landroid/support/v4/view/animation/FastOutSlowInInterpolator;

    invoke-direct {v0}, Landroid/support/v4/view/animation/FastOutSlowInInterpolator;-><init>()V

    iput-object v0, p0, Lbr/com/itau/library/LinearValuePickerView;->interpolator:Landroid/view/animation/Interpolator;

    .line 59
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbr/com/itau/library/LinearValuePickerView;->alreadyInflated:Z

    .line 60
    const-string v0, "#969696"

    invoke-static {v0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lbr/com/itau/library/LinearValuePickerView;->textColor:I

    .line 71
    invoke-direct {p0, p1, p2}, Lbr/com/itau/library/LinearValuePickerView;->getAttrs(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 72
    invoke-direct {p0}, Lbr/com/itau/library/LinearValuePickerView;->init()V

    .line 73
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .line 76
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 50
    const/4 v0, -0x1

    iput v0, p0, Lbr/com/itau/library/LinearValuePickerView;->selectedPosition:I

    .line 57
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbr/com/itau/library/LinearValuePickerView;->animated:Z

    .line 58
    new-instance v0, Landroid/support/v4/view/animation/FastOutSlowInInterpolator;

    invoke-direct {v0}, Landroid/support/v4/view/animation/FastOutSlowInInterpolator;-><init>()V

    iput-object v0, p0, Lbr/com/itau/library/LinearValuePickerView;->interpolator:Landroid/view/animation/Interpolator;

    .line 59
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbr/com/itau/library/LinearValuePickerView;->alreadyInflated:Z

    .line 60
    const-string v0, "#969696"

    invoke-static {v0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lbr/com/itau/library/LinearValuePickerView;->textColor:I

    .line 77
    invoke-direct {p0, p1, p2}, Lbr/com/itau/library/LinearValuePickerView;->getAttrs(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 78
    invoke-direct {p0}, Lbr/com/itau/library/LinearValuePickerView;->init()V

    .line 79
    return-void
.end method

.method static synthetic access$000(Lbr/com/itau/library/LinearValuePickerView;)Ljava/util/List;
    .registers 2
    .param p0, "x0"    # Lbr/com/itau/library/LinearValuePickerView;

    .line 41
    iget-object v0, p0, Lbr/com/itau/library/LinearValuePickerView;->radioButtonsList:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$100(Lbr/com/itau/library/LinearValuePickerView;)Landroid/view/animation/Interpolator;
    .registers 2
    .param p0, "x0"    # Lbr/com/itau/library/LinearValuePickerView;

    .line 41
    iget-object v0, p0, Lbr/com/itau/library/LinearValuePickerView;->interpolator:Landroid/view/animation/Interpolator;

    return-object v0
.end method

.method static synthetic access$200(Lbr/com/itau/library/LinearValuePickerView;)Ljava/util/List;
    .registers 2
    .param p0, "x0"    # Lbr/com/itau/library/LinearValuePickerView;

    .line 41
    iget-object v0, p0, Lbr/com/itau/library/LinearValuePickerView;->textValueList:Ljava/util/List;

    return-object v0
.end method

.method private animateTextColorChange(Landroid/widget/TextView;I)V
    .registers 9
    .param p1, "text"    # Landroid/widget/TextView;
    .param p2, "newColor"    # I

    .line 227
    invoke-virtual {p1}, Landroid/widget/TextView;->getCurrentTextColor()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    .line 228
    .local v4, "colorFrom":Ljava/lang/Integer;
    new-instance v0, Landroid/animation/ArgbEvaluator;

    invoke-direct {v0}, Landroid/animation/ArgbEvaluator;-><init>()V

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object v4, v1, v2

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x1

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Landroid/animation/ValueAnimator;->ofObject(Landroid/animation/TypeEvaluator;[Ljava/lang/Object;)Landroid/animation/ValueAnimator;

    move-result-object v5

    .line 229
    .local v5, "colorAnimation":Landroid/animation/ValueAnimator;
    const-wide/16 v0, 0xc8

    invoke-virtual {v5, v0, v1}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 230
    new-instance v0, Lbr/com/itau/library/LinearValuePickerView$1;

    invoke-direct {v0, p0, p1}, Lbr/com/itau/library/LinearValuePickerView$1;-><init>(Lbr/com/itau/library/LinearValuePickerView;Landroid/widget/TextView;)V

    invoke-virtual {v5, v0}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 237
    invoke-virtual {v5}, Landroid/animation/ValueAnimator;->start()V

    .line 238
    return-void
.end method

.method private animateTextValue(IZ)V
    .registers 10
    .param p1, "itemPos"    # I
    .param p2, "isChecked"    # Z

    .line 208
    const/4 v3, 0x0

    .line 209
    .local v3, "translationY":I
    const/high16 v4, 0x3f800000    # 1.0f

    .line 210
    .local v4, "scale":F
    iget v5, p0, Lbr/com/itau/library/LinearValuePickerView;->textColor:I

    .line 211
    .local v5, "colorText":I
    iget-object v6, p0, Lbr/com/itau/library/LinearValuePickerView;->fontRegular:Landroid/graphics/Typeface;

    .line 213
    .local v6, "font":Landroid/graphics/Typeface;
    if-eqz p2, :cond_1b

    .line 214
    invoke-virtual {p0}, Lbr/com/itau/library/LinearValuePickerView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Litau/com/br/linearvaluepickerview/R$dimen;->value_picker_margin_text:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    mul-int/lit8 v3, v0, -0x1

    .line 215
    const/high16 v4, 0x3fc00000    # 1.5f

    .line 216
    iget v5, p0, Lbr/com/itau/library/LinearValuePickerView;->color:I

    .line 217
    iget-object v6, p0, Lbr/com/itau/library/LinearValuePickerView;->fontBold:Landroid/graphics/Typeface;

    .line 219
    :cond_1b
    iget-object v0, p0, Lbr/com/itau/library/LinearValuePickerView;->textValueList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    int-to-float v1, v3

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/ViewPropertyAnimator;->scaleX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/ViewPropertyAnimator;->scaleY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v1, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/ViewPropertyAnimator;->setStartDelay(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v1, 0x12c

    invoke-virtual {v0, v1, v2}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 220
    iget-object v0, p0, Lbr/com/itau/library/LinearValuePickerView;->textValueList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-direct {p0, v0, v5}, Lbr/com/itau/library/LinearValuePickerView;->animateTextColorChange(Landroid/widget/TextView;I)V

    .line 221
    if-eqz v6, :cond_5b

    .line 222
    iget-object v0, p0, Lbr/com/itau/library/LinearValuePickerView;->textValueList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 223
    :cond_5b
    return-void
.end method

.method private applyAttrs()V
    .registers 6

    .line 136
    iget-object v0, p0, Lbr/com/itau/library/LinearValuePickerView;->typefaceRegular:Ljava/lang/String;

    if-eqz v0, :cond_14

    .line 137
    invoke-virtual {p0}, Lbr/com/itau/library/LinearValuePickerView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    iget-object v1, p0, Lbr/com/itau/library/LinearValuePickerView;->typefaceRegular:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/graphics/Typeface;->createFromAsset(Landroid/content/res/AssetManager;Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/library/LinearValuePickerView;->fontRegular:Landroid/graphics/Typeface;

    .line 138
    :cond_14
    iget-object v0, p0, Lbr/com/itau/library/LinearValuePickerView;->typefaceBold:Ljava/lang/String;

    if-eqz v0, :cond_28

    .line 139
    invoke-virtual {p0}, Lbr/com/itau/library/LinearValuePickerView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    iget-object v1, p0, Lbr/com/itau/library/LinearValuePickerView;->typefaceBold:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/graphics/Typeface;->createFromAsset(Landroid/content/res/AssetManager;Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/library/LinearValuePickerView;->fontBold:Landroid/graphics/Typeface;

    .line 140
    :cond_28
    iget-object v0, p0, Lbr/com/itau/library/LinearValuePickerView;->viewValuepickerLine:Landroid/view/View;

    iget v1, p0, Lbr/com/itau/library/LinearValuePickerView;->color:I

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 142
    invoke-virtual {p0}, Lbr/com/itau/library/LinearValuePickerView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Litau/com/br/linearvaluepickerview/R$drawable;->circle_normal:I

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/support/v4/content/res/ResourcesCompat;->getDrawable(Landroid/content/res/Resources;ILandroid/content/res/Resources$Theme;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/LayerDrawable;

    iput-object v0, p0, Lbr/com/itau/library/LinearValuePickerView;->normalDrawable:Landroid/graphics/drawable/LayerDrawable;

    .line 143
    iget-object v0, p0, Lbr/com/itau/library/LinearValuePickerView;->normalDrawable:Landroid/graphics/drawable/LayerDrawable;

    sget v1, Litau/com/br/linearvaluepickerview/R$id;->shape_circle_normal:I

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/LayerDrawable;->findDrawableByLayerId(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/GradientDrawable;

    move-object v3, v0

    check-cast v3, Landroid/graphics/drawable/GradientDrawable;

    .line 144
    .local v3, "shapeNormal":Landroid/graphics/drawable/GradientDrawable;
    invoke-virtual {p0}, Lbr/com/itau/library/LinearValuePickerView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Litau/com/br/linearvaluepickerview/R$dimen;->value_picker_stroke_width:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iget v1, p0, Lbr/com/itau/library/LinearValuePickerView;->color:I

    invoke-virtual {v3, v0, v1}, Landroid/graphics/drawable/GradientDrawable;->setStroke(II)V

    .line 145
    const/4 v0, -0x1

    invoke-virtual {v3, v0}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    .line 147
    invoke-virtual {p0}, Lbr/com/itau/library/LinearValuePickerView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Litau/com/br/linearvaluepickerview/R$drawable;->circle_selected:I

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/support/v4/content/res/ResourcesCompat;->getDrawable(Landroid/content/res/Resources;ILandroid/content/res/Resources$Theme;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/LayerDrawable;

    iput-object v0, p0, Lbr/com/itau/library/LinearValuePickerView;->selectedDrawable:Landroid/graphics/drawable/LayerDrawable;

    .line 148
    iget-object v0, p0, Lbr/com/itau/library/LinearValuePickerView;->selectedDrawable:Landroid/graphics/drawable/LayerDrawable;

    sget v1, Litau/com/br/linearvaluepickerview/R$id;->shape_circle_selected:I

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/LayerDrawable;->findDrawableByLayerId(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/GradientDrawable;

    move-object v4, v0

    check-cast v4, Landroid/graphics/drawable/GradientDrawable;

    .line 149
    .local v4, "shapeSelected":Landroid/graphics/drawable/GradientDrawable;
    invoke-virtual {p0}, Lbr/com/itau/library/LinearValuePickerView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Litau/com/br/linearvaluepickerview/R$dimen;->value_picker_stroke_width:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    const/4 v1, -0x1

    invoke-virtual {v4, v0, v1}, Landroid/graphics/drawable/GradientDrawable;->setStroke(II)V

    .line 150
    iget v0, p0, Lbr/com/itau/library/LinearValuePickerView;->color:I

    invoke-virtual {v4, v0}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    .line 151
    return-void
.end method

.method private generateStateListDrawable()Landroid/graphics/drawable/StateListDrawable;
    .registers 5

    .line 300
    new-instance v3, Landroid/graphics/drawable/StateListDrawable;

    invoke-direct {v3}, Landroid/graphics/drawable/StateListDrawable;-><init>()V

    .line 301
    .local v3, "stateListDrawable":Landroid/graphics/drawable/StateListDrawable;
    const/4 v0, 0x1

    new-array v0, v0, [I

    fill-array-data v0, :array_18

    iget-object v1, p0, Lbr/com/itau/library/LinearValuePickerView;->selectedDrawable:Landroid/graphics/drawable/LayerDrawable;

    invoke-virtual {v3, v0, v1}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 302
    sget-object v0, Landroid/util/StateSet;->WILD_CARD:[I

    iget-object v1, p0, Lbr/com/itau/library/LinearValuePickerView;->normalDrawable:Landroid/graphics/drawable/LayerDrawable;

    invoke-virtual {v3, v0, v1}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 303
    return-object v3

    :array_18
    .array-data 4
        0x10100a0
    .end array-data
.end method

.method private generateViews()V
    .registers 10

    .line 164
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lbr/com/itau/library/LinearValuePickerView;->radioButtonsList:Ljava/util/List;

    .line 165
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lbr/com/itau/library/LinearValuePickerView;->textValueList:Ljava/util/List;

    .line 167
    iget-object v0, p0, Lbr/com/itau/library/LinearValuePickerView;->radiogroupValuepickerRadiogroup:Landroid/widget/RadioGroup;

    invoke-virtual {v0}, Landroid/widget/RadioGroup;->removeAllViews()V

    .line 168
    iget-object v0, p0, Lbr/com/itau/library/LinearValuePickerView;->linearValuepickerValueText:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 170
    new-instance v3, Landroid/widget/RadioGroup$LayoutParams;

    const/4 v0, -0x1

    const/16 v1, 0x64

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-direct {v3, v0, v1, v2}, Landroid/widget/RadioGroup$LayoutParams;-><init>(IIF)V

    .line 171
    .local v3, "spaceLayoutParams":Landroid/widget/RadioGroup$LayoutParams;
    new-instance v4, Landroid/widget/LinearLayout$LayoutParams;

    invoke-virtual {p0}, Lbr/com/itau/library/LinearValuePickerView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Litau/com/br/linearvaluepickerview/R$dimen;->value_picker_text_width:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    const/4 v1, -0x2

    invoke-direct {v4, v0, v1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 172
    .local v4, "textLayoutParams":Landroid/widget/LinearLayout$LayoutParams;
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_33
    iget-object v0, p0, Lbr/com/itau/library/LinearValuePickerView;->values:[Ljava/lang/String;

    array-length v0, v0

    if-ge v5, v0, :cond_e4

    .line 173
    new-instance v6, Landroid/support/v7/widget/AppCompatRadioButton;

    invoke-virtual {p0}, Lbr/com/itau/library/LinearValuePickerView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v6, v0}, Landroid/support/v7/widget/AppCompatRadioButton;-><init>(Landroid/content/Context;)V

    .line 174
    .local v6, "newRadioButton":Landroid/support/v7/widget/AppCompatRadioButton;
    invoke-direct {p0}, Lbr/com/itau/library/LinearValuePickerView;->generateStateListDrawable()Landroid/graphics/drawable/StateListDrawable;

    move-result-object v0

    invoke-virtual {v6, v0}, Landroid/support/v7/widget/AppCompatRadioButton;->setButtonDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 175
    const/4 v0, 0x0

    invoke-virtual {v6, v0}, Landroid/support/v7/widget/AppCompatRadioButton;->setBackgroundColor(I)V

    .line 176
    const/16 v0, 0x11

    invoke-virtual {v6, v0}, Landroid/support/v7/widget/AppCompatRadioButton;->setGravity(I)V

    .line 177
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v6, v0}, Landroid/support/v7/widget/AppCompatRadioButton;->setTag(Ljava/lang/Object;)V

    .line 178
    iget-object v0, p0, Lbr/com/itau/library/LinearValuePickerView;->values:[Ljava/lang/String;

    aget-object v0, v0, v5

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Landroid/support/v7/widget/AppCompatRadioButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 179
    invoke-virtual {v6, p0}, Landroid/support/v7/widget/AppCompatRadioButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 180
    iget-object v0, p0, Lbr/com/itau/library/LinearValuePickerView;->radiogroupValuepickerRadiogroup:Landroid/widget/RadioGroup;

    invoke-virtual {v0, v6}, Landroid/widget/RadioGroup;->addView(Landroid/view/View;)V

    .line 181
    iget-object v0, p0, Lbr/com/itau/library/LinearValuePickerView;->radioButtonsList:Ljava/util/List;

    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 182
    iget-object v0, p0, Lbr/com/itau/library/LinearValuePickerView;->values:[Ljava/lang/String;

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    if-ge v5, v0, :cond_88

    .line 183
    new-instance v7, Landroid/view/View;

    invoke-virtual {p0}, Lbr/com/itau/library/LinearValuePickerView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v7, v0}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 184
    .local v7, "spaceView":Landroid/view/View;
    invoke-virtual {v7, v3}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 185
    iget-object v0, p0, Lbr/com/itau/library/LinearValuePickerView;->radiogroupValuepickerRadiogroup:Landroid/widget/RadioGroup;

    invoke-virtual {v0, v7}, Landroid/widget/RadioGroup;->addView(Landroid/view/View;)V

    .line 188
    .end local v7    # "spaceView":Landroid/view/View;
    :cond_88
    new-instance v7, Landroid/widget/TextView;

    invoke-virtual {p0}, Lbr/com/itau/library/LinearValuePickerView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v7, v0}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 189
    .local v7, "newValueText":Landroid/widget/TextView;
    invoke-virtual {v7, v4}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 190
    iget v0, p0, Lbr/com/itau/library/LinearValuePickerView;->textColor:I

    invoke-virtual {v7, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 191
    const/4 v0, 0x2

    const/high16 v1, 0x41400000    # 12.0f

    invoke-virtual {v7, v0, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 192
    const/4 v0, 0x1

    invoke-virtual {v7, v0}, Landroid/widget/TextView;->setGravity(I)V

    .line 193
    iget-object v0, p0, Lbr/com/itau/library/LinearValuePickerView;->values:[Ljava/lang/String;

    aget-object v0, v0, v5

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 194
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v7, v0}, Landroid/widget/TextView;->setTag(Ljava/lang/Object;)V

    .line 195
    iget-object v0, p0, Lbr/com/itau/library/LinearValuePickerView;->fontRegular:Landroid/graphics/Typeface;

    if-eqz v0, :cond_be

    .line 196
    iget-object v0, p0, Lbr/com/itau/library/LinearValuePickerView;->fontRegular:Landroid/graphics/Typeface;

    invoke-virtual {v7, v0}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 197
    :cond_be
    iget-object v0, p0, Lbr/com/itau/library/LinearValuePickerView;->linearValuepickerValueText:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v7}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 198
    iget-object v0, p0, Lbr/com/itau/library/LinearValuePickerView;->textValueList:Ljava/util/List;

    invoke-interface {v0, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 199
    iget-object v0, p0, Lbr/com/itau/library/LinearValuePickerView;->values:[Ljava/lang/String;

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    if-ge v5, v0, :cond_e0

    .line 200
    new-instance v8, Landroid/view/View;

    invoke-virtual {p0}, Lbr/com/itau/library/LinearValuePickerView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v8, v0}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 201
    .local v8, "spaceViewText":Landroid/view/View;
    invoke-virtual {v8, v3}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 202
    iget-object v0, p0, Lbr/com/itau/library/LinearValuePickerView;->linearValuepickerValueText:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v8}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 172
    .end local v6    # "newRadioButton":Landroid/support/v7/widget/AppCompatRadioButton;
    .end local v7    # "newValueText":Landroid/widget/TextView;
    .end local v8    # "spaceViewText":Landroid/view/View;
    :cond_e0
    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_33

    .line 205
    .end local v5    # "i":I
    :cond_e4
    return-void
.end method

.method private getAttrs(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .line 98
    sget-object v0, Litau/com/br/linearvaluepickerview/R$styleable;->ValuePickerView:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v2

    .line 99
    .local v2, "values":Landroid/content/res/TypedArray;
    sget v0, Litau/com/br/linearvaluepickerview/R$styleable;->ValuePickerView_vp_color:I

    const v1, -0x777778

    invoke-virtual {v2, v0, v1}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    iput v0, p0, Lbr/com/itau/library/LinearValuePickerView;->color:I

    .line 100
    sget v0, Litau/com/br/linearvaluepickerview/R$styleable;->ValuePickerView_vp_text_color:I

    const-string v1, "#969696"

    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v2, v0, v1}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    iput v0, p0, Lbr/com/itau/library/LinearValuePickerView;->textColor:I

    .line 101
    sget v0, Litau/com/br/linearvaluepickerview/R$styleable;->ValuePickerView_vp_typefaceRegular:I

    invoke-virtual {v2, v0}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/library/LinearValuePickerView;->typefaceRegular:Ljava/lang/String;

    .line 102
    sget v0, Litau/com/br/linearvaluepickerview/R$styleable;->ValuePickerView_vp_typefaceBold:I

    invoke-virtual {v2, v0}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/library/LinearValuePickerView;->typefaceBold:Ljava/lang/String;

    .line 104
    invoke-virtual {v2}, Landroid/content/res/TypedArray;->recycle()V

    .line 105
    return-void
.end method

.method private init()V
    .registers 3

    .line 89
    invoke-virtual {p0}, Lbr/com/itau/library/LinearValuePickerView;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Litau/com/br/linearvaluepickerview/R$layout;->view_linear_value_picker:I

    invoke-static {v0, v1, p0}, Lbr/com/itau/library/LinearValuePickerView;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 90
    sget v0, Litau/com/br/linearvaluepickerview/R$id;->linear_valuepicker_value_text:I

    invoke-virtual {p0, v0}, Lbr/com/itau/library/LinearValuePickerView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lbr/com/itau/library/LinearValuePickerView;->linearValuepickerValueText:Landroid/widget/LinearLayout;

    .line 91
    sget v0, Litau/com/br/linearvaluepickerview/R$id;->view_valuepicker_line:I

    invoke-virtual {p0, v0}, Lbr/com/itau/library/LinearValuePickerView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/library/LinearValuePickerView;->viewValuepickerLine:Landroid/view/View;

    .line 92
    sget v0, Litau/com/br/linearvaluepickerview/R$id;->radiogroup_valuepicker_radiogroup:I

    invoke-virtual {p0, v0}, Lbr/com/itau/library/LinearValuePickerView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioGroup;

    iput-object v0, p0, Lbr/com/itau/library/LinearValuePickerView;->radiogroupValuepickerRadiogroup:Landroid/widget/RadioGroup;

    .line 93
    invoke-direct {p0}, Lbr/com/itau/library/LinearValuePickerView;->applyAttrs()V

    .line 95
    return-void
.end method

.method private initPicker()V
    .registers 3

    .line 154
    invoke-direct {p0}, Lbr/com/itau/library/LinearValuePickerView;->generateViews()V

    .line 155
    iget-boolean v0, p0, Lbr/com/itau/library/LinearValuePickerView;->animated:Z

    if-eqz v0, :cond_e

    .line 156
    invoke-direct {p0}, Lbr/com/itau/library/LinearValuePickerView;->prepareToAnimation()V

    .line 157
    invoke-direct {p0}, Lbr/com/itau/library/LinearValuePickerView;->startAnimation()V

    goto :goto_15

    .line 159
    :cond_e
    iget-object v0, p0, Lbr/com/itau/library/LinearValuePickerView;->viewValuepickerLine:Landroid/view/View;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/View;->setScaleX(F)V

    .line 161
    :goto_15
    return-void
.end method

.method private prepareToAnimation()V
    .registers 4

    .line 241
    iget-object v0, p0, Lbr/com/itau/library/LinearValuePickerView;->viewValuepickerLine:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setScaleX(F)V

    .line 242
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_7
    iget-object v0, p0, Lbr/com/itau/library/LinearValuePickerView;->radioButtonsList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_2a

    .line 243
    iget-object v0, p0, Lbr/com/itau/library/LinearValuePickerView;->radioButtonsList:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setScaleX(F)V

    .line 244
    iget-object v0, p0, Lbr/com/itau/library/LinearValuePickerView;->radioButtonsList:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setScaleY(F)V

    .line 242
    add-int/lit8 v2, v2, 0x1

    goto :goto_7

    .line 247
    .end local v2    # "i":I
    :cond_2a
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_2b
    iget-object v0, p0, Lbr/com/itau/library/LinearValuePickerView;->textValueList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_4e

    .line 248
    iget-object v0, p0, Lbr/com/itau/library/LinearValuePickerView;->textValueList:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setAlpha(F)V

    .line 249
    iget-object v0, p0, Lbr/com/itau/library/LinearValuePickerView;->textValueList:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setAlpha(F)V

    .line 247
    add-int/lit8 v2, v2, 0x1

    goto :goto_2b

    .line 251
    .end local v2    # "i":I
    :cond_4e
    return-void
.end method

.method private startAnimation()V
    .registers 4

    .line 254
    iget-object v0, p0, Lbr/com/itau/library/LinearValuePickerView;->viewValuepickerLine:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->scaleX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v1, 0x258

    invoke-virtual {v0, v1, v2}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iget-object v1, p0, Lbr/com/itau/library/LinearValuePickerView;->interpolator:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v1, 0x12c

    invoke-virtual {v0, v1, v2}, Landroid/view/ViewPropertyAnimator;->setStartDelay(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, Lbr/com/itau/library/LinearValuePickerView$2;

    invoke-direct {v1, p0}, Lbr/com/itau/library/LinearValuePickerView$2;-><init>(Lbr/com/itau/library/LinearValuePickerView;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 279
    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 281
    return-void
.end method


# virtual methods
.method public getSelectedPosition()I
    .registers 2

    .line 125
    iget v0, p0, Lbr/com/itau/library/LinearValuePickerView;->selectedPosition:I

    return v0
.end method

.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .registers 6
    .param p1, "button"    # Landroid/widget/CompoundButton;
    .param p2, "isChecked"    # Z

    .line 285
    if-eqz p2, :cond_31

    .line 286
    invoke-virtual {p1}, Landroid/widget/CompoundButton;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lbr/com/itau/library/LinearValuePickerView;->selectedPosition:I

    .line 287
    invoke-virtual {p1}, Landroid/widget/CompoundButton;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/high16 v1, 0x40000000    # 2.0f

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->scaleX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/high16 v1, 0x40000000    # 2.0f

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->scaleY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v1, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/ViewPropertyAnimator;->setStartDelay(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 289
    iget-object v0, p0, Lbr/com/itau/library/LinearValuePickerView;->action:Lbr/com/itau/library/br/com/itau/library/interfaces/IAction;

    if-eqz v0, :cond_4a

    .line 290
    iget-object v0, p0, Lbr/com/itau/library/LinearValuePickerView;->action:Lbr/com/itau/library/br/com/itau/library/interfaces/IAction;

    invoke-interface {v0}, Lbr/com/itau/library/br/com/itau/library/interfaces/IAction;->OnClickAction()V

    goto :goto_4a

    .line 294
    :cond_31
    invoke-virtual {p1}, Landroid/widget/CompoundButton;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->scaleX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->scaleY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v1, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/ViewPropertyAnimator;->setStartDelay(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 296
    :cond_4a
    :goto_4a
    invoke-virtual {p1}, Landroid/widget/CompoundButton;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {p0, v0, p2}, Lbr/com/itau/library/LinearValuePickerView;->animateTextValue(IZ)V

    .line 297
    return-void
.end method

.method public setAnimated(Z)V
    .registers 2
    .param p1, "animated"    # Z

    .line 108
    iput-boolean p1, p0, Lbr/com/itau/library/LinearValuePickerView;->animated:Z

    .line 109
    return-void
.end method

.method public setInterpolator(Landroid/view/animation/Interpolator;)V
    .registers 2
    .param p1, "interpolator"    # Landroid/view/animation/Interpolator;

    .line 116
    iput-object p1, p0, Lbr/com/itau/library/LinearValuePickerView;->interpolator:Landroid/view/animation/Interpolator;

    .line 117
    return-void
.end method

.method public setOnClickAction(Lbr/com/itau/library/br/com/itau/library/interfaces/IAction;)V
    .registers 2
    .param p1, "action"    # Lbr/com/itau/library/br/com/itau/library/interfaces/IAction;

    .line 112
    iput-object p1, p0, Lbr/com/itau/library/LinearValuePickerView;->action:Lbr/com/itau/library/br/com/itau/library/interfaces/IAction;

    .line 113
    return-void
.end method

.method public setSelectedPosition(I)V
    .registers 4
    .param p1, "selectedPosition"    # I

    .line 129
    iget-object v0, p0, Lbr/com/itau/library/LinearValuePickerView;->radioButtonsList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge p1, v0, :cond_16

    .line 130
    iput p1, p0, Lbr/com/itau/library/LinearValuePickerView;->selectedPosition:I

    .line 131
    iget-object v0, p0, Lbr/com/itau/library/LinearValuePickerView;->radioButtonsList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 133
    :cond_16
    return-void
.end method

.method public setValues([Ljava/lang/String;)V
    .registers 2
    .param p1, "values"    # [Ljava/lang/String;

    .line 120
    iput-object p1, p0, Lbr/com/itau/library/LinearValuePickerView;->values:[Ljava/lang/String;

    .line 121
    invoke-direct {p0}, Lbr/com/itau/library/LinearValuePickerView;->initPicker()V

    .line 122
    return-void
.end method

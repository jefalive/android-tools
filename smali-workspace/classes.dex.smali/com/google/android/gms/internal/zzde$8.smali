.class final Lcom/google/android/gms/internal/zzde$8;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/internal/zzdf;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/internal/zzde;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .registers 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private zzc(Lcom/google/android/gms/internal/zzjp;)V
    .registers 8

    const-string v0, "Received support message, responding."

    invoke-static {v0}, Lcom/google/android/gms/internal/zzin;->zzaJ(Ljava/lang/String;)V

    const/4 v2, 0x0

    invoke-interface {p1}, Lcom/google/android/gms/internal/zzjp;->zzhR()Lcom/google/android/gms/ads/internal/zzd;

    move-result-object v3

    if-eqz v3, :cond_14

    iget-object v4, v3, Lcom/google/android/gms/ads/internal/zzd;->zzpy:Lcom/google/android/gms/ads/internal/overlay/zzm;

    if-eqz v4, :cond_14

    invoke-interface {v4}, Lcom/google/android/gms/ads/internal/overlay/zzm;->zzfM()Z

    move-result v2

    :cond_14
    new-instance v4, Lorg/json/JSONObject;

    invoke-direct {v4}, Lorg/json/JSONObject;-><init>()V

    const-string v0, "event"

    const-string v1, "checkSupport"

    :try_start_1d
    invoke-virtual {v4, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "supports"

    invoke-virtual {v4, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    const-string v0, "appStreaming"

    invoke-interface {p1, v0, v4}, Lcom/google/android/gms/internal/zzjp;->zzb(Ljava/lang/String;Lorg/json/JSONObject;)V
    :try_end_2a
    .catch Ljava/lang/Throwable; {:try_start_1d .. :try_end_2a} :catch_2b

    goto :goto_2c

    :catch_2b
    move-exception v5

    :goto_2c
    return-void
.end method


# virtual methods
.method public zza(Lcom/google/android/gms/internal/zzjp;Ljava/util/Map;)V
    .registers 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Lcom/google/android/gms/internal/zzjp;Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;)V"
        }
    .end annotation

    const-string v0, "checkSupport"

    const-string v1, "action"

    invoke-interface {p2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_12

    invoke-direct {p0, p1}, Lcom/google/android/gms/internal/zzde$8;->zzc(Lcom/google/android/gms/internal/zzjp;)V

    return-void

    :cond_12
    invoke-interface {p1}, Lcom/google/android/gms/internal/zzjp;->zzhS()Lcom/google/android/gms/ads/internal/overlay/zzd;

    move-result-object v2

    if-eqz v2, :cond_1b

    invoke-virtual {v2, p1, p2}, Lcom/google/android/gms/ads/internal/overlay/zzd;->zzg(Lcom/google/android/gms/internal/zzjp;Ljava/util/Map;)V

    :cond_1b
    return-void
.end method

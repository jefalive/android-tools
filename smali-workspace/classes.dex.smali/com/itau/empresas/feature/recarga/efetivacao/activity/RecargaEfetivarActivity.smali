.class public Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity;
.super Lcom/itau/empresas/ui/activity/BaseActivity;
.source "RecargaEfetivarActivity.java"

# interfaces
.implements Lcom/itau/empresas/ui/util/SnackbarUtils$Gerenciador;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity$ErrorLimite;
    }
.end annotation


# instance fields
.field application:Lcom/itau/empresas/CustomApplication;

.field aviso:Ljava/lang/String;

.field botaoInterrogacao:Landroid/widget/ImageButton;

.field campoIdentificacaoComprovante:Landroid/widget/TextView;

.field limite:Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity$ErrorLimite;

.field llValor:Landroid/widget/LinearLayout;

.field llValorBonus:Landroid/widget/LinearLayout;

.field nome:Ljava/lang/String;

.field novo:Z

.field private popupIdentificacaoComprovante:Landroid/widget/PopupWindow;

.field recargaController:Lcom/itau/empresas/feature/recarga/efetivacao/controller/RecargaController;

.field recargaInputVO:Lcom/itau/empresas/feature/recarga/contatos/model/RecargaInputVO;

.field resultadoSimulacaoRecargaVO:Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoSimulacaoRecargaVO;

.field root:Landroid/widget/LinearLayout;

.field textoAgContaDebitada:Landroid/widget/TextView;

.field textoDestinoLocal:Landroid/widget/TextView;

.field textoDestinoNome:Landroid/widget/TextView;

.field textoDestinoTelefone:Landroid/widget/TextView;

.field textoOrigemAgencia:Landroid/widget/TextView;

.field textoOrigemConta:Landroid/widget/TextView;

.field textoOrigemNome:Landroid/widget/TextView;

.field textoOrigemOperador:Landroid/widget/TextView;

.field textoRecargaOperadora:Landroid/widget/TextView;

.field textoValor:Landroid/widget/TextView;

.field textoValorBonus:Landroid/widget/TextView;

.field toolBar:Landroid/support/v7/widget/Toolbar;

.field private topSnackbar:Lcom/itau/empresas/ui/util/SnackbarUtils$GerenciadorDelegate;

.field warning:Lcom/itau/empresas/ui/view/WarningView;


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 58
    invoke-direct {p0}, Lcom/itau/empresas/ui/activity/BaseActivity;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity;)Landroid/widget/PopupWindow;
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity;

    .line 58
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity;->popupIdentificacaoComprovante:Landroid/widget/PopupWindow;

    return-object v0
.end method

.method private alertaLimiteUltrapassado(Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity$ErrorLimite;)V
    .registers 6
    .param p1, "limite"    # Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity$ErrorLimite;

    .line 152
    sget-object v0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity$4;->$SwitchMap$com$itau$empresas$feature$recarga$efetivacao$activity$RecargaEfetivarActivity$ErrorLimite:[I

    invoke-virtual {p1}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity$ErrorLimite;->ordinal()I

    move-result v1

    aget v0, v0, v1

    sparse-switch v0, :sswitch_data_3e

    goto :goto_14

    .line 154
    :sswitch_c
    const v2, 0x7f0703fb

    .line 155
    .local v2, "mensagemErro":I
    goto :goto_17

    .line 157
    .end local v2    # "mensagemErro":I
    :sswitch_10
    const v2, 0x7f0703fa

    .line 158
    .local v2, "mensagemErro":I
    goto :goto_17

    .line 161
    .end local v2    # "mensagemErro":I
    :goto_14
    const v2, 0x7f0701f2

    .line 164
    .local v2, "mensagemErro":I
    :goto_17
    new-instance v0, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;

    invoke-direct {v0, p0}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 165
    const v1, 0x7f07048c

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;->textoNeutro(I)Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;

    move-result-object v0

    .line 166
    invoke-virtual {v0, v2}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;->mensagem(I)Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;

    move-result-object v0

    sget-object v1, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Estados;->UM_BOTAO:Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Estados;

    .line 167
    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;->estado(Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Estados;)Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;

    move-result-object v0

    .line 168
    invoke-virtual {v0}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;->build()Lcom/itau/empresas/ui/dialog/CustomAlertDialog;

    move-result-object v3

    .line 169
    .local v3, "alert":Lcom/itau/empresas/ui/dialog/CustomAlertDialog;
    new-instance v0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity$1;

    invoke-direct {v0, p0, v3}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity$1;-><init>(Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity;Lcom/itau/empresas/ui/dialog/CustomAlertDialog;)V

    invoke-virtual {v3, v0}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog;->setNeutroListener(Landroid/view/View$OnClickListener;)V

    .line 176
    invoke-virtual {v3, p0}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog;->show(Landroid/support/v7/app/AppCompatActivity;)Lcom/itau/empresas/ui/dialog/BaseDialog;

    .line 177
    return-void

    nop

    :sswitch_data_3e
    .sparse-switch
        0x1 -> :sswitch_c
        0x2 -> :sswitch_10
    .end sparse-switch
.end method

.method private constroiToolbar()V
    .registers 3

    .line 129
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity;->toolBar:Landroid/support/v7/widget/Toolbar;

    invoke-static {v0, p0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom;->Builder(Landroid/support/v7/widget/Toolbar;Landroid/support/v7/app/AppCompatActivity;)Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    move-result-object v0

    .line 130
    const v1, 0x7f0706b8

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->comTitulo(I)Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    move-result-object v0

    .line 131
    invoke-virtual {v0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->comClickListenerFinish()Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    move-result-object v0

    .line 132
    invoke-virtual {v0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->build()Lcom/itau/empresas/ui/util/listener/ToolbarCustom;

    .line 133
    return-void
.end method

.method private disparaEventoAnalytics()V
    .registers 5

    .line 120
    invoke-static {}, Lcom/itau/empresas/feature/analytics/AnalyticsHelper;->getInstance()Lcom/itau/empresas/feature/analytics/AnalyticsHelper;

    move-result-object v3

    .line 121
    .line 122
    .local v3, "analyticsHelper":Lcom/itau/empresas/feature/analytics/AnalyticsHelper;
    const v0, 0x7f0700f0

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 123
    const v1, 0x7f0700ce

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 124
    const v2, 0x7f0700c3

    invoke-virtual {p0, v2}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 121
    invoke-virtual {v3, v0, v1, v2}, Lcom/itau/empresas/feature/analytics/AnalyticsHelper;->eventoAdobeState(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 126
    return-void
.end method

.method private incluirAutorizar()V
    .registers 5

    .line 418
    invoke-virtual {p0}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity;->dismissSnackbar()V

    .line 419
    invoke-virtual {p0}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity;->mostraDialogoDeProgresso()V

    .line 421
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity;->recargaInputVO:Lcom/itau/empresas/feature/recarga/contatos/model/RecargaInputVO;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/recarga/contatos/model/RecargaInputVO;->getRecarga()Lcom/itau/empresas/feature/recarga/contatos/model/RecargaVO;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity;->resultadoSimulacaoRecargaVO:Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoSimulacaoRecargaVO;

    .line 423
    invoke-virtual {v1}, Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoSimulacaoRecargaVO;->getRecarga()Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoRecargaVO;

    move-result-object v1

    invoke-virtual {v1}, Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoRecargaVO;->getValorRecarga()Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Float;->intValue()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 422
    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/recarga/contatos/model/RecargaVO;->setValorRecarga(Ljava/lang/Integer;)V

    .line 424
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity;->recargaInputVO:Lcom/itau/empresas/feature/recarga/contatos/model/RecargaInputVO;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/recarga/contatos/model/RecargaInputVO;->getRecarga()Lcom/itau/empresas/feature/recarga/contatos/model/RecargaVO;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity;->resultadoSimulacaoRecargaVO:Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoSimulacaoRecargaVO;

    .line 425
    invoke-virtual {v1}, Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoSimulacaoRecargaVO;->getRecarga()Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoRecargaVO;

    move-result-object v1

    invoke-virtual {v1}, Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoRecargaVO;->getCodigoPagamento()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/recarga/contatos/model/RecargaVO;->setCodigoPagamento(Ljava/lang/String;)V

    .line 426
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity;->recargaInputVO:Lcom/itau/empresas/feature/recarga/contatos/model/RecargaInputVO;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/recarga/contatos/model/RecargaInputVO;->getRecarga()Lcom/itau/empresas/feature/recarga/contatos/model/RecargaVO;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity;->campoIdentificacaoComprovante:Landroid/widget/TextView;

    .line 427
    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/recarga/contatos/model/RecargaVO;->setIdentificacaoComprovante(Ljava/lang/String;)V

    .line 428
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity;->resultadoSimulacaoRecargaVO:Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoSimulacaoRecargaVO;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoSimulacaoRecargaVO;->getRecarga()Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoRecargaVO;

    move-result-object v3

    .line 429
    .local v3, "recargaVO":Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoRecargaVO;
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity;->recargaController:Lcom/itau/empresas/feature/recarga/efetivacao/controller/RecargaController;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 430
    invoke-virtual {v3}, Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoRecargaVO;->getDddTelefone()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v3}, Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoRecargaVO;->getNumeroTelefone()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity;->recargaInputVO:Lcom/itau/empresas/feature/recarga/contatos/model/RecargaInputVO;

    invoke-virtual {v0, v1, v2}, Lcom/itau/empresas/feature/recarga/efetivacao/controller/RecargaController;->efetuaRecarga(Ljava/lang/String;Lcom/itau/empresas/feature/recarga/contatos/model/RecargaInputVO;)V

    .line 432
    return-void
.end method

.method private inicializarPopupIdentificacaoComprovante()V
    .registers 7

    .line 258
    new-instance v0, Landroid/widget/PopupWindow;

    invoke-direct {v0, p0}, Landroid/widget/PopupWindow;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity;->popupIdentificacaoComprovante:Landroid/widget/PopupWindow;

    .line 259
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity;->popupIdentificacaoComprovante:Landroid/widget/PopupWindow;

    invoke-virtual {p0}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    .line 260
    const v2, 0x7f030060

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 259
    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setContentView(Landroid/view/View;)V

    .line 261
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity;->popupIdentificacaoComprovante:Landroid/widget/PopupWindow;

    const/4 v1, -0x2

    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setWidth(I)V

    .line 262
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity;->popupIdentificacaoComprovante:Landroid/widget/PopupWindow;

    const/4 v1, -0x2

    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setHeight(I)V

    .line 263
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity;->popupIdentificacaoComprovante:Landroid/widget/PopupWindow;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setFocusable(Z)V

    .line 264
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity;->popupIdentificacaoComprovante:Landroid/widget/PopupWindow;

    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 266
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity;->popupIdentificacaoComprovante:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->getContentView()Landroid/view/View;

    move-result-object v0

    .line 267
    const v1, 0x7f0e02db

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Landroid/widget/LinearLayout;

    .line 269
    .local v4, "container":Landroid/widget/LinearLayout;
    new-instance v5, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity$2;

    invoke-direct {v5, p0}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity$2;-><init>(Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity;)V

    .line 276
    .local v5, "popupFechar":Landroid/view/View$OnClickListener;
    const v0, 0x7f0e021e

    invoke-virtual {v4, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 277
    invoke-virtual {v4, v5}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 278
    return-void
.end method

.method public static intent(Landroid/content/Context;)Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity_$IntentBuilder_;
    .registers 2
    .param p0, "context"    # Landroid/content/Context;

    .line 472
    new-instance v0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity_$IntentBuilder_;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity_$IntentBuilder_;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method private static locateView(Landroid/view/View;)Landroid/graphics/Rect;
    .registers 5
    .param p0, "v"    # Landroid/view/View;

    .line 288
    const/4 v0, 0x2

    new-array v2, v0, [I

    .line 289
    .local v2, "locInt":[I
    if-nez p0, :cond_7

    .line 290
    const/4 v0, 0x0

    return-object v0

    .line 293
    :cond_7
    :try_start_7
    invoke-virtual {p0, v2}, Landroid/view/View;->getLocationOnScreen([I)V
    :try_end_a
    .catch Ljava/lang/NullPointerException; {:try_start_7 .. :try_end_a} :catch_b

    .line 298
    goto :goto_15

    .line 294
    :catch_b
    move-exception v3

    .line 296
    .local v3, "npe":Ljava/lang/NullPointerException;
    const-string v0, "RecargaEfetivarActivity"

    const-string v1, "View n\u00e3o est\u00e1 mais na tela"

    invoke-static {v0, v1, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 297
    const/4 v0, 0x0

    return-object v0

    .line 299
    .end local v3    # "npe":Ljava/lang/NullPointerException;
    :goto_15
    new-instance v3, Landroid/graphics/Rect;

    invoke-direct {v3}, Landroid/graphics/Rect;-><init>()V

    .line 300
    .local v3, "location":Landroid/graphics/Rect;
    const/4 v0, 0x0

    aget v0, v2, v0

    iput v0, v3, Landroid/graphics/Rect;->left:I

    .line 301
    const/4 v0, 0x1

    aget v0, v2, v0

    iput v0, v3, Landroid/graphics/Rect;->top:I

    .line 302
    iget v0, v3, Landroid/graphics/Rect;->left:I

    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v1

    add-int/2addr v0, v1

    iput v0, v3, Landroid/graphics/Rect;->right:I

    .line 303
    iget v0, v3, Landroid/graphics/Rect;->top:I

    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result v1

    add-int/2addr v0, v1

    iput v0, v3, Landroid/graphics/Rect;->bottom:I

    .line 304
    return-object v3
.end method

.method private mostraDialogErro(Ljava/lang/String;)V
    .registers 5
    .param p1, "mensagem"    # Ljava/lang/String;

    .line 395
    new-instance v0, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;

    invoke-direct {v0, p0}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 396
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;->cancelable(Z)Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;

    move-result-object v0

    .line 397
    invoke-virtual {v0, p1}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;->mensagem(Ljava/lang/String;)Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;

    move-result-object v0

    .line 398
    const v1, 0x7f0704ba

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;->textoNeutro(I)Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;

    move-result-object v0

    sget-object v1, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Estados;->UM_BOTAO:Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Estados;

    .line 399
    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;->estado(Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Estados;)Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;

    move-result-object v0

    .line 400
    invoke-virtual {v0}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;->build()Lcom/itau/empresas/ui/dialog/CustomAlertDialog;

    move-result-object v2

    .line 402
    .local v2, "dialog":Lcom/itau/empresas/ui/dialog/CustomAlertDialog;
    new-instance v0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity$3;

    invoke-direct {v0, p0, v2}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity$3;-><init>(Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity;Lcom/itau/empresas/ui/dialog/CustomAlertDialog;)V

    invoke-virtual {v2, v0}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog;->setNeutroListener(Landroid/view/View$OnClickListener;)V

    .line 411
    invoke-virtual {v2, p0}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog;->show(Landroid/support/v7/app/AppCompatActivity;)Lcom/itau/empresas/ui/dialog/BaseDialog;

    .line 412
    return-void
.end method

.method private mostrarAviso()V
    .registers 4

    .line 140
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity;->aviso:Ljava/lang/String;

    if-eqz v0, :cond_13

    .line 141
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity;->warning:Lcom/itau/empresas/ui/view/WarningView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/view/WarningView;->setVisibility(I)V

    .line 142
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity;->warning:Lcom/itau/empresas/ui/view/WarningView;

    sget-object v1, Lcom/itau/empresas/ui/view/WarningView$Estado;->ATENCAO:Lcom/itau/empresas/ui/view/WarningView$Estado;

    iget-object v2, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity;->aviso:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/itau/empresas/ui/view/WarningView;->setMensagem(Lcom/itau/empresas/ui/view/WarningView$Estado;Ljava/lang/String;)V

    .line 144
    :cond_13
    return-void
.end method

.method private preencherDadosDestino()V
    .registers 6

    .line 205
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity;->nome:Ljava/lang/String;

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity;->nome:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_14

    .line 206
    :cond_c
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity;->textoDestinoNome:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1b

    .line 208
    :cond_14
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity;->textoDestinoNome:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity;->nome:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 211
    :goto_1b
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity;->resultadoSimulacaoRecargaVO:Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoSimulacaoRecargaVO;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoSimulacaoRecargaVO;->getRecarga()Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoRecargaVO;

    move-result-object v3

    .line 212
    .local v3, "recargaVO":Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoRecargaVO;
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity;->textoDestinoTelefone:Landroid/widget/TextView;

    .line 213
    invoke-virtual {v3}, Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoRecargaVO;->getDddTelefone()Ljava/lang/String;

    move-result-object v1

    .line 214
    invoke-virtual {v3}, Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoRecargaVO;->getNumeroTelefone()Ljava/lang/String;

    move-result-object v2

    .line 213
    invoke-static {p0, v1, v2}, Lcom/itau/empresas/ui/util/ViewUtils;->formataNumeroTelefonico(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 212
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 216
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity;->resultadoSimulacaoRecargaVO:Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoSimulacaoRecargaVO;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoSimulacaoRecargaVO;->getRecarga()Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoRecargaVO;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoRecargaVO;->getNomeRegiao()Ljava/lang/String;

    move-result-object v4

    .line 217
    .local v4, "regiao":Ljava/lang/String;
    if-eqz v4, :cond_44

    invoke-virtual {v4}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4c

    .line 218
    :cond_44
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity;->textoDestinoLocal:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_5b

    .line 220
    :cond_4c
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity;->textoDestinoLocal:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity;->resultadoSimulacaoRecargaVO:Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoSimulacaoRecargaVO;

    invoke-virtual {v1}, Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoSimulacaoRecargaVO;->getRecarga()Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoRecargaVO;

    move-result-object v1

    invoke-virtual {v1}, Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoRecargaVO;->getNomeRegiao()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 222
    :goto_5b
    return-void
.end method

.method private preencherDadosOrigem()V
    .registers 6

    .line 183
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity;->application:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getContaSelecionada()Lcom/itau/empresas/api/model/ContaOperadorVO;

    move-result-object v4

    .line 185
    .local v4, "contaLogada":Lcom/itau/empresas/api/model/ContaOperadorVO;
    invoke-virtual {v4}, Lcom/itau/empresas/api/model/ContaOperadorVO;->getApelido()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_16

    invoke-virtual {v4}, Lcom/itau/empresas/api/model/ContaOperadorVO;->getApelido()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1e

    .line 186
    :cond_16
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity;->textoOrigemNome:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_27

    .line 188
    :cond_1e
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity;->textoOrigemNome:Landroid/widget/TextView;

    invoke-virtual {v4}, Lcom/itau/empresas/api/model/ContaOperadorVO;->getApelido()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 191
    :goto_27
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity;->textoOrigemAgencia:Landroid/widget/TextView;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    .line 192
    invoke-virtual {v4}, Lcom/itau/empresas/api/model/ContaOperadorVO;->getAgencia()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const v2, 0x7f07062b

    invoke-virtual {p0, v2, v1}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 193
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity;->textoOrigemConta:Landroid/widget/TextView;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    .line 194
    invoke-virtual {v4}, Lcom/itau/empresas/api/model/ContaOperadorVO;->getConta()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    .line 195
    invoke-virtual {v4}, Lcom/itau/empresas/api/model/ContaOperadorVO;->getDac()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    aput-object v2, v1, v3

    .line 194
    const v2, 0x7f07062c

    invoke-virtual {p0, v2, v1}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 196
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity;->textoOrigemOperador:Landroid/widget/TextView;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity;->application:Lcom/itau/empresas/CustomApplication;

    .line 198
    invoke-virtual {v2}, Lcom/itau/empresas/CustomApplication;->getCodigoOperador()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    .line 197
    const v2, 0x7f07062d

    invoke-virtual {p0, v2, v1}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 199
    return-void
.end method

.method private preencherValores()V
    .registers 7

    .line 228
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity;->resultadoSimulacaoRecargaVO:Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoSimulacaoRecargaVO;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoSimulacaoRecargaVO;->getRecarga()Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoRecargaVO;

    move-result-object v4

    .line 229
    .local v4, "recargaVO":Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoRecargaVO;
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity;->application:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getContaSelecionada()Lcom/itau/empresas/api/model/ContaOperadorVO;

    move-result-object v5

    .line 231
    .local v5, "contaLogada":Lcom/itau/empresas/api/model/ContaOperadorVO;
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity;->textoAgContaDebitada:Landroid/widget/TextView;

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    .line 232
    invoke-virtual {v5}, Lcom/itau/empresas/api/model/ContaOperadorVO;->getAgencia()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    .line 233
    invoke-virtual {v5}, Lcom/itau/empresas/api/model/ContaOperadorVO;->getConta()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    aput-object v2, v1, v3

    invoke-virtual {v5}, Lcom/itau/empresas/api/model/ContaOperadorVO;->getDac()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x2

    aput-object v2, v1, v3

    .line 232
    const v2, 0x7f070551

    invoke-virtual {p0, v2, v1}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 231
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 234
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity;->textoRecargaOperadora:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity;->resultadoSimulacaoRecargaVO:Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoSimulacaoRecargaVO;

    invoke-virtual {v1}, Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoSimulacaoRecargaVO;->getRecarga()Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoRecargaVO;

    move-result-object v1

    invoke-virtual {v1}, Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoRecargaVO;->getNomeOperadora()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 236
    invoke-virtual {v4}, Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoRecargaVO;->getValorRecarga()Ljava/lang/Float;

    move-result-object v0

    if-eqz v0, :cond_5e

    .line 237
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity;->textoValor:Landroid/widget/TextView;

    .line 239
    invoke-virtual {v4}, Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoRecargaVO;->getValorRecarga()Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/itau/empresas/ui/util/ValorMonetarioUtils;->formataValorDecimalParaMonetario(FZ)Ljava/lang/String;

    move-result-object v1

    .line 237
    const/high16 v2, 0x3f000000    # 0.5f

    invoke-static {v1, v2}, Lcom/itau/empresas/ui/util/ValorMonetarioUtils;->destacaValorMonetario(Ljava/lang/String;F)Landroid/text/SpannableString;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_65

    .line 242
    :cond_5e
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity;->llValorBonus:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 245
    :goto_65
    invoke-virtual {v4}, Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoRecargaVO;->getValorBonus()Ljava/lang/Float;

    move-result-object v0

    if-eqz v0, :cond_84

    .line 246
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity;->textoValorBonus:Landroid/widget/TextView;

    .line 247
    invoke-virtual {v4}, Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoRecargaVO;->getValorBonus()Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/itau/empresas/ui/util/ValorMonetarioUtils;->formataValorDecimalParaMonetario(FZ)Ljava/lang/String;

    move-result-object v1

    .line 246
    const/high16 v2, 0x3f000000    # 0.5f

    invoke-static {v1, v2}, Lcom/itau/empresas/ui/util/ValorMonetarioUtils;->destacaValorMonetario(Ljava/lang/String;F)Landroid/text/SpannableString;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_8b

    .line 250
    :cond_84
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity;->llValorBonus:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 252
    :goto_8b
    return-void
.end method


# virtual methods
.method protected aoClicarContinuar()V
    .registers 5

    .line 330
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity;->application:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getEventoTracker()Lcom/itau/empresas/ui/util/analytics/TrackerEvento;

    move-result-object v0

    new-instance v1, Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;

    .line 331
    const v2, 0x7f0702e5

    invoke-virtual {p0, v2}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 332
    const v3, 0x7f0702a2

    invoke-virtual {p0, v3}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 330
    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/analytics/TrackerEvento;->enviarHitEvento(Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;)V

    .line 333
    invoke-direct {p0}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity;->incluirAutorizar()V

    .line 334
    return-void
.end method

.method protected aoClicarInterrogacao()V
    .registers 6

    .line 312
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity;->botaoInterrogacao:Landroid/widget/ImageButton;

    invoke-static {v0}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity;->locateView(Landroid/view/View;)Landroid/graphics/Rect;

    move-result-object v4

    .line 313
    .local v4, "location":Landroid/graphics/Rect;
    if-nez v4, :cond_9

    .line 314
    return-void

    .line 317
    :cond_9
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity;->application:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getEventoTracker()Lcom/itau/empresas/ui/util/analytics/TrackerEvento;

    move-result-object v0

    new-instance v1, Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;

    .line 318
    const v2, 0x7f0702e5

    invoke-virtual {p0, v2}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 319
    const v3, 0x7f0702b4

    invoke-virtual {p0, v3}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 317
    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/analytics/TrackerEvento;->enviarHitEvento(Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;)V

    .line 320
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity;->popupIdentificacaoComprovante:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->getContentView()Landroid/view/View;

    move-result-object v0

    .line 321
    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/View;->measure(II)V

    .line 322
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity;->popupIdentificacaoComprovante:Landroid/widget/PopupWindow;

    iget-object v1, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity;->botaoInterrogacao:Landroid/widget/ImageButton;

    iget-object v2, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity;->botaoInterrogacao:Landroid/widget/ImageButton;

    .line 323
    invoke-virtual {v2}, Landroid/widget/ImageButton;->getHeight()I

    move-result v2

    iget-object v3, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity;->popupIdentificacaoComprovante:Landroid/widget/PopupWindow;

    invoke-virtual {v3}, Landroid/widget/PopupWindow;->getContentView()Landroid/view/View;

    move-result-object v3

    .line 324
    invoke-virtual {v3}, Landroid/view/View;->getMeasuredHeight()I

    move-result v3

    add-int/2addr v2, v3

    rsub-int/lit8 v2, v2, 0x0

    .line 322
    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3, v2}, Landroid/widget/PopupWindow;->showAsDropDown(Landroid/view/View;II)V

    .line 325
    return-void
.end method

.method carregaElementosDaTela()V
    .registers 3

    .line 99
    new-instance v0, Lcom/itau/empresas/ui/util/SnackbarUtils$GerenciadorDelegate;

    iget-object v1, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity;->root:Landroid/widget/LinearLayout;

    invoke-direct {v0, v1}, Lcom/itau/empresas/ui/util/SnackbarUtils$GerenciadorDelegate;-><init>(Landroid/view/ViewGroup;)V

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity;->topSnackbar:Lcom/itau/empresas/ui/util/SnackbarUtils$GerenciadorDelegate;

    .line 100
    invoke-direct {p0}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity;->constroiToolbar()V

    .line 102
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity;->limite:Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity$ErrorLimite;

    if-eqz v0, :cond_1d

    .line 103
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity;->limite:Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity$ErrorLimite;

    invoke-direct {p0, v0}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity;->alertaLimiteUltrapassado(Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity$ErrorLimite;)V

    .line 104
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity;->warning:Lcom/itau/empresas/ui/view/WarningView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/view/WarningView;->setVisibility(I)V

    .line 105
    return-void

    .line 108
    :cond_1d
    invoke-direct {p0}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity;->mostrarAviso()V

    .line 110
    invoke-direct {p0}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity;->inicializarPopupIdentificacaoComprovante()V

    .line 112
    invoke-direct {p0}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity;->preencherDadosOrigem()V

    .line 113
    invoke-direct {p0}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity;->preencherDadosDestino()V

    .line 114
    invoke-direct {p0}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity;->preencherValores()V

    .line 116
    invoke-direct {p0}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity;->disparaEventoAnalytics()V

    .line 117
    return-void
.end method

.method public dismissSnackbar()V
    .registers 2

    .line 448
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity;->topSnackbar:Lcom/itau/empresas/ui/util/SnackbarUtils$GerenciadorDelegate;

    invoke-virtual {v0}, Lcom/itau/empresas/ui/util/SnackbarUtils$GerenciadorDelegate;->dismissSnackbar()V

    .line 449
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .registers 3
    .param p1, "menu"    # Landroid/view/Menu;

    .line 461
    invoke-static {p0, p1}, Lcom/itau/empresas/ui/util/PoyntUtils;->onCreateOptionsMenu(Landroid/app/Activity;Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method public onEventMainThread(Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent$Conversion;)V
    .registers 4
    .param p1, "e"    # Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent$Conversion;

    .line 350
    invoke-static {p1, p0}, Lcom/itau/empresas/api/ApiHelper;->devoTratarEvento(Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;Lcom/itau/empresas/api/ApiConsumidor;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 351
    return-void

    .line 354
    :cond_7
    invoke-virtual {p0}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity;->escondeDialogoDeProgresso()V

    .line 355
    const v0, 0x7f0701f2

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity;->showSnackbar(Ljava/lang/CharSequence;I)V

    .line 357
    return-void
.end method

.method public onEventMainThread(Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent$HTTP;)V
    .registers 6
    .param p1, "http"    # Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent$HTTP;

    .line 361
    invoke-static {p1, p0}, Lcom/itau/empresas/api/ApiHelper;->devoTratarEvento(Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;Lcom/itau/empresas/api/ApiConsumidor;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 362
    return-void

    .line 365
    :cond_7
    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent$HTTP;->getException()Lbr/com/itau/sdk/android/core/exception/BackendException;

    move-result-object v2

    .line 366
    .local v2, "exception":Lbr/com/itau/sdk/android/core/exception/BackendException;
    invoke-virtual {v2}, Lbr/com/itau/sdk/android/core/exception/BackendException;->getError()Lbr/com/itau/sdk/android/core/model/ErrorDTO;

    move-result-object v3

    .line 368
    .local v3, "errorDTO":Lbr/com/itau/sdk/android/core/model/ErrorDTO;
    invoke-virtual {v2}, Lbr/com/itau/sdk/android/core/exception/BackendException;->getStatusCode()I

    move-result v0

    const/16 v1, 0x352

    if-ne v0, v1, :cond_1e

    .line 369
    invoke-virtual {v3}, Lbr/com/itau/sdk/android/core/model/ErrorDTO;->getMensagem()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity;->mostraDialogErro(Ljava/lang/String;)V

    .line 371
    :cond_1e
    return-void
.end method

.method public onEventMainThread(Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;)V
    .registers 6
    .param p1, "e"    # Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;

    .line 375
    invoke-static {p1, p0}, Lcom/itau/empresas/api/ApiHelper;->devoTratarEvento(Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;Lcom/itau/empresas/api/ApiConsumidor;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 376
    return-void

    .line 379
    :cond_7
    invoke-virtual {p0}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity;->escondeDialogoDeProgresso()V

    .line 381
    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;->getException()Lbr/com/itau/sdk/android/core/exception/BackendException;

    move-result-object v2

    .line 382
    .local v2, "exception":Lbr/com/itau/sdk/android/core/exception/BackendException;
    invoke-virtual {v2}, Lbr/com/itau/sdk/android/core/exception/BackendException;->getError()Lbr/com/itau/sdk/android/core/model/ErrorDTO;

    move-result-object v3

    .line 383
    .local v3, "error":Lbr/com/itau/sdk/android/core/model/ErrorDTO;
    if-eqz v3, :cond_24

    invoke-virtual {v2}, Lbr/com/itau/sdk/android/core/exception/BackendException;->getStatusCode()I

    move-result v0

    const/16 v1, 0x352

    if-eq v0, v1, :cond_24

    .line 384
    invoke-virtual {v3}, Lbr/com/itau/sdk/android/core/model/ErrorDTO;->getMensagem()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity;->showSnackbar(Ljava/lang/CharSequence;I)V

    .line 387
    :cond_24
    return-void
.end method

.method public onEventMainThread(Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoEfetivacaoRecargaVO;)V
    .registers 4
    .param p1, "resultadoEfetivacaoRecargaVO"    # Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoEfetivacaoRecargaVO;

    .line 340
    invoke-virtual {p0}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity;->escondeDialogoDeProgresso()V

    .line 342
    invoke-static {p0}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity;->intent(Landroid/content/Context;)Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity_$IntentBuilder_;

    move-result-object v0

    iget-boolean v1, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity;->novo:Z

    .line 343
    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity_$IntentBuilder_;->novo(Z)Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity_$IntentBuilder_;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity;->nome:Ljava/lang/String;

    .line 344
    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity_$IntentBuilder_;->nome(Ljava/lang/String;)Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity_$IntentBuilder_;

    move-result-object v0

    .line 345
    invoke-virtual {v0, p1}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity_$IntentBuilder_;->resultadoEfetivacaoRecargaVO(Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoEfetivacaoRecargaVO;)Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity_$IntentBuilder_;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity_$IntentBuilder_;->start()Lorg/androidannotations/api/builder/PostActivityStarter;

    .line 346
    invoke-virtual {p0}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity;->finish()V

    .line 347
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .registers 3
    .param p1, "item"    # Landroid/view/MenuItem;

    .line 466
    invoke-static {p0, p1}, Lcom/itau/empresas/ui/util/PoyntUtils;->onOptionsItemSelected(Lcom/itau/empresas/ui/activity/BaseActivity;Landroid/view/MenuItem;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 467
    const/4 v0, 0x1

    return v0

    .line 468
    :cond_8
    invoke-super {p0, p1}, Lcom/itau/empresas/ui/activity/BaseActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0
.end method

.method public opkeysConsumidas()Ljava/util/List;
    .registers 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Ljava/lang/String;>;"
        }
    .end annotation

    .line 436
    const/16 v0, 0x9

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "listaOperadoras"

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const-string v1, "contatosRecarga"

    const/4 v2, 0x1

    aput-object v1, v0, v2

    const-string v1, "cadastrarContatoRecarga"

    const/4 v2, 0x2

    aput-object v1, v0, v2

    const-string v1, "alterarContatoRecarga"

    const/4 v2, 0x3

    aput-object v1, v0, v2

    const-string v1, "deletarContatoRecarga"

    const/4 v2, 0x4

    aput-object v1, v0, v2

    const-string v1, "valoresOperadora"

    const/4 v2, 0x5

    aput-object v1, v0, v2

    const-string v1, "simularRecarga"

    const/4 v2, 0x6

    aput-object v1, v0, v2

    const-string v1, "exibicao_recarga_autorizar"

    const/4 v2, 0x7

    aput-object v1, v0, v2

    const-string v1, "efetivarRecarga"

    const/16 v2, 0x8

    aput-object v1, v0, v2

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public showSnackbar(Ljava/lang/CharSequence;I)V
    .registers 4
    .param p1, "texto"    # Ljava/lang/CharSequence;
    .param p2, "duracao"    # I

    .line 452
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity;->topSnackbar:Lcom/itau/empresas/ui/util/SnackbarUtils$GerenciadorDelegate;

    invoke-virtual {v0, p1, p2}, Lcom/itau/empresas/ui/util/SnackbarUtils$GerenciadorDelegate;->showSnackbar(Ljava/lang/CharSequence;I)V

    .line 453
    return-void
.end method

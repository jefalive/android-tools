.class public Lcom/itau/empresas/feature/credito/produtos/view/ProdutoMultilimiteView;
.super Landroid/widget/LinearLayout;
.source "ProdutoMultilimiteView.java"


# instance fields
.field public imagemSetaDireita:Landroid/widget/ImageView;

.field public textoTipo:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 2
    .param p1, "context"    # Landroid/content/Context;

    .line 24
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 25
    return-void
.end method


# virtual methods
.method public bind(Lcom/itau/empresas/feature/credito/produtos/model/DetalhesMultilimiteProdutoVO;)V
    .registers 7
    .param p1, "produtoVO"    # Lcom/itau/empresas/feature/credito/produtos/model/DetalhesMultilimiteProdutoVO;

    .line 28
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/view/ProdutoMultilimiteView;->textoTipo:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/itau/empresas/feature/credito/produtos/model/DetalhesMultilimiteProdutoVO;->getTitulo()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 29
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/view/ProdutoMultilimiteView;->textoTipo:Landroid/widget/TextView;

    .line 30
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/produtos/view/ProdutoMultilimiteView;->getContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/itau/empresas/feature/credito/produtos/view/ProdutoMultilimiteView;->textoTipo:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    const/4 v4, 0x0

    aput-object v3, v2, v4

    const v3, 0x7f070088

    invoke-virtual {v1, v3, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 29
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 31
    return-void
.end method

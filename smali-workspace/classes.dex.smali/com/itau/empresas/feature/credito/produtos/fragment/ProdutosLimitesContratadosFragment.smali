.class public Lcom/itau/empresas/feature/credito/produtos/fragment/ProdutosLimitesContratadosFragment;
.super Lcom/itau/empresas/ui/fragment/BaseFragment;
.source "ProdutosLimitesContratadosFragment.java"


# instance fields
.field controller:Lcom/itau/empresas/controller/ProdutosController;

.field private firstLoad:Z

.field limitesProdutosListAdapter:Lcom/itau/empresas/feature/credito/produtos/adapter/LimitesProdutosListAdapter;

.field listaProdutos:Landroid/widget/ListView;

.field private position:I

.field private retornoMultiLimite:Lcom/itau/empresas/feature/credito/produtos/model/MultiLimiteVO;

.field textoProdutoNaoEncontrado:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .registers 2

    .line 53
    invoke-direct {p0}, Lcom/itau/empresas/ui/fragment/BaseFragment;-><init>()V

    .line 49
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/ProdutosLimitesContratadosFragment;->firstLoad:Z

    .line 55
    return-void
.end method


# virtual methods
.method carregandoElementosDeTela()V
    .registers 2

    .line 71
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/produtos/fragment/ProdutosLimitesContratadosFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/CustomApplication;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/ProdutosLimitesContratadosFragment;->application:Lcom/itau/empresas/CustomApplication;

    .line 72
    iget-boolean v0, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/ProdutosLimitesContratadosFragment;->firstLoad:Z

    if-eqz v0, :cond_13

    .line 73
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/produtos/fragment/ProdutosLimitesContratadosFragment;->loadDados()V

    .line 75
    :cond_13
    return-void
.end method

.method public loadDados()V
    .registers 2

    .line 78
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/produtos/fragment/ProdutosLimitesContratadosFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/ui/activity/BaseActivity;

    invoke-virtual {v0}, Lcom/itau/empresas/ui/activity/BaseActivity;->mostraDialogoDeProgresso()V

    .line 79
    return-void
.end method

.method public onEventMainThread(Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent$Conversion;)V
    .registers 5
    .param p1, "e"    # Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent$Conversion;

    .line 121
    invoke-static {p1, p0}, Lcom/itau/empresas/api/ApiHelper;->devoTratarEvento(Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;Lcom/itau/empresas/api/ApiConsumidor;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 122
    return-void

    .line 125
    :cond_7
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/produtos/fragment/ProdutosLimitesContratadosFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const-string v0, "ItauEmpresas"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Erro na Chamanda: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 126
    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent$Conversion;->getException()Lbr/com/itau/sdk/android/core/exception/BackendException;

    move-result-object v2

    invoke-virtual {v2}, Lbr/com/itau/sdk/android/core/exception/BackendException;->getOpKey()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent$Conversion;->getException()Lbr/com/itau/sdk/android/core/exception/BackendException;

    move-result-object v2

    .line 125
    invoke-static {v0, v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 127
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/produtos/fragment/ProdutosLimitesContratadosFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/ui/activity/BaseActivity;

    invoke-virtual {v0}, Lcom/itau/empresas/ui/activity/BaseActivity;->escondeDialogoDeProgresso()V

    .line 128
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/ProdutosLimitesContratadosFragment;->listaProdutos:Landroid/widget/ListView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setVisibility(I)V

    .line 129
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/ProdutosLimitesContratadosFragment;->textoProdutoNaoEncontrado:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 130
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/ProdutosLimitesContratadosFragment;->textoProdutoNaoEncontrado:Landroid/widget/TextView;

    const v1, 0x7f0701f2

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 131
    return-void
.end method

.method public onEventMainThread(Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;)V
    .registers 5
    .param p1, "e"    # Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;

    .line 134
    invoke-static {p1, p0}, Lcom/itau/empresas/api/ApiHelper;->devoTratarEvento(Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;Lcom/itau/empresas/api/ApiConsumidor;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 135
    return-void

    .line 138
    :cond_7
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/produtos/fragment/ProdutosLimitesContratadosFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const-string v0, "ItauEmpresas"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Erro na Chamada:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 139
    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;->getException()Lbr/com/itau/sdk/android/core/exception/BackendException;

    move-result-object v2

    invoke-virtual {v2}, Lbr/com/itau/sdk/android/core/exception/BackendException;->getOpKey()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;->getException()Lbr/com/itau/sdk/android/core/exception/BackendException;

    move-result-object v2

    .line 138
    invoke-static {v0, v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 140
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/produtos/fragment/ProdutosLimitesContratadosFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/ui/activity/BaseActivity;

    invoke-virtual {v0}, Lcom/itau/empresas/ui/activity/BaseActivity;->escondeDialogoDeProgresso()V

    .line 141
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/ProdutosLimitesContratadosFragment;->listaProdutos:Landroid/widget/ListView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setVisibility(I)V

    .line 142
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/ProdutosLimitesContratadosFragment;->textoProdutoNaoEncontrado:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 143
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/ProdutosLimitesContratadosFragment;->textoProdutoNaoEncontrado:Landroid/widget/TextView;

    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;->getException()Lbr/com/itau/sdk/android/core/exception/BackendException;

    move-result-object v1

    invoke-virtual {v1}, Lbr/com/itau/sdk/android/core/exception/BackendException;->getError()Lbr/com/itau/sdk/android/core/model/ErrorDTO;

    move-result-object v1

    invoke-virtual {v1}, Lbr/com/itau/sdk/android/core/model/ErrorDTO;->getMensagem()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 144
    return-void
.end method

.method public onEventMainThread(Lcom/itau/empresas/feature/credito/produtos/model/ProdutosVO;)V
    .registers 8
    .param p1, "retornoProdutos"    # Lcom/itau/empresas/feature/credito/produtos/model/ProdutosVO;

    .line 82
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/produtos/fragment/ProdutosLimitesContratadosFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/ui/activity/BaseActivity;

    invoke-virtual {v0}, Lcom/itau/empresas/ui/activity/BaseActivity;->escondeDialogoDeProgresso()V

    .line 83
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 84
    .local v2, "itens":Ljava/util/List;, "Ljava/util/List<Lcom/itau/empresas/feature/credito/produtos/model/ProdutoChildItem;>;"
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/ProdutosLimitesContratadosFragment;->retornoMultiLimite:Lcom/itau/empresas/feature/credito/produtos/model/MultiLimiteVO;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/credito/produtos/model/MultiLimiteVO;->getDetalheProdutoMultiLimite()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_59

    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/ProdutosLimitesContratadosFragment;->retornoMultiLimite:Lcom/itau/empresas/feature/credito/produtos/model/MultiLimiteVO;

    .line 85
    invoke-virtual {v0}, Lcom/itau/empresas/feature/credito/produtos/model/MultiLimiteVO;->getDetalheProdutoMultiLimite()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_59

    .line 86
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/ProdutosLimitesContratadosFragment;->retornoMultiLimite:Lcom/itau/empresas/feature/credito/produtos/model/MultiLimiteVO;

    .line 87
    invoke-virtual {v0}, Lcom/itau/empresas/feature/credito/produtos/model/MultiLimiteVO;->getDetalheProdutoMultiLimite()Ljava/util/List;

    move-result-object v0

    .line 86
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2c
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_57

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/itau/empresas/feature/credito/produtos/model/MultiLimiteVO$DetalheProdutoMultiLimiteVO;

    .line 88
    .local v4, "detalheProdutoMultiLimiteVO":Lcom/itau/empresas/feature/credito/produtos/model/MultiLimiteVO$DetalheProdutoMultiLimiteVO;
    new-instance v5, Lcom/itau/empresas/feature/credito/produtos/model/ProdutoChildItem;

    invoke-direct {v5}, Lcom/itau/empresas/feature/credito/produtos/model/ProdutoChildItem;-><init>()V

    .line 89
    .local v5, "item":Lcom/itau/empresas/feature/credito/produtos/model/ProdutoChildItem;
    invoke-virtual {v4}, Lcom/itau/empresas/feature/credito/produtos/model/MultiLimiteVO$DetalheProdutoMultiLimiteVO;->getDescricaoProduto()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Lcom/itau/empresas/feature/credito/produtos/model/ProdutoChildItem;->setDescricao(Ljava/lang/String;)V

    .line 90
    invoke-virtual {v4}, Lcom/itau/empresas/feature/credito/produtos/model/MultiLimiteVO$DetalheProdutoMultiLimiteVO;->getLimiteDisponivel()Ljava/lang/Float;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Float;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Lcom/itau/empresas/feature/credito/produtos/model/ProdutoChildItem;->setValor(Ljava/lang/String;)V

    .line 91
    invoke-virtual {v5, v4}, Lcom/itau/empresas/feature/credito/produtos/model/ProdutoChildItem;->setDetalheProdutoMultiLimite(Lcom/itau/empresas/feature/credito/produtos/model/MultiLimiteVO$DetalheProdutoMultiLimiteVO;)V

    .line 92
    invoke-interface {v2, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 93
    .end local v4    # "detalheProdutoMultiLimiteVO":Lcom/itau/empresas/feature/credito/produtos/model/MultiLimiteVO$DetalheProdutoMultiLimiteVO;
    .end local v5    # "item":Lcom/itau/empresas/feature/credito/produtos/model/ProdutoChildItem;
    goto :goto_2c

    :cond_57
    goto/16 :goto_cf

    .line 95
    :cond_59
    invoke-virtual {p1}, Lcom/itau/empresas/feature/credito/produtos/model/ProdutosVO;->getListaProdutosRotativos()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_96

    .line 96
    invoke-virtual {p1}, Lcom/itau/empresas/feature/credito/produtos/model/ProdutosVO;->getListaProdutosRotativos()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_6b
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_96

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/itau/empresas/feature/credito/produtos/model/ProdutosRotativosVO;

    .line 97
    .local v4, "produto":Lcom/itau/empresas/feature/credito/produtos/model/ProdutosRotativosVO;
    new-instance v5, Lcom/itau/empresas/feature/credito/produtos/model/ProdutoChildItem;

    invoke-direct {v5}, Lcom/itau/empresas/feature/credito/produtos/model/ProdutoChildItem;-><init>()V

    .line 98
    .local v5, "item":Lcom/itau/empresas/feature/credito/produtos/model/ProdutoChildItem;
    invoke-virtual {v4}, Lcom/itau/empresas/feature/credito/produtos/model/ProdutosRotativosVO;->getNomeProduto()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Lcom/itau/empresas/feature/credito/produtos/model/ProdutoChildItem;->setDescricao(Ljava/lang/String;)V

    .line 99
    invoke-virtual {v4}, Lcom/itau/empresas/feature/credito/produtos/model/ProdutosRotativosVO;->getLimiteDisponivel()Ljava/lang/Float;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Float;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Lcom/itau/empresas/feature/credito/produtos/model/ProdutoChildItem;->setValor(Ljava/lang/String;)V

    .line 100
    invoke-virtual {v5, v4}, Lcom/itau/empresas/feature/credito/produtos/model/ProdutoChildItem;->setProdutosRotativos(Lcom/itau/empresas/feature/credito/produtos/model/ProdutosRotativosVO;)V

    .line 101
    invoke-interface {v2, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 102
    .end local v4    # "produto":Lcom/itau/empresas/feature/credito/produtos/model/ProdutosRotativosVO;
    .end local v5    # "item":Lcom/itau/empresas/feature/credito/produtos/model/ProdutoChildItem;
    goto :goto_6b

    .line 104
    :cond_96
    invoke-virtual {p1}, Lcom/itau/empresas/feature/credito/produtos/model/ProdutosVO;->getListaProdutosParcelados()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_cf

    .line 105
    invoke-virtual {p1}, Lcom/itau/empresas/feature/credito/produtos/model/ProdutosVO;->getListaProdutosParcelados()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_a8
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_cf

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/itau/empresas/feature/credito/produtos/model/ProdutosParceladosVO;

    .line 106
    .local v4, "produto":Lcom/itau/empresas/feature/credito/produtos/model/ProdutosParceladosVO;
    new-instance v5, Lcom/itau/empresas/feature/credito/produtos/model/ProdutoChildItem;

    invoke-direct {v5}, Lcom/itau/empresas/feature/credito/produtos/model/ProdutoChildItem;-><init>()V

    .line 107
    .local v5, "item":Lcom/itau/empresas/feature/credito/produtos/model/ProdutoChildItem;
    invoke-virtual {v4}, Lcom/itau/empresas/feature/credito/produtos/model/ProdutosParceladosVO;->getNomeProduto()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Lcom/itau/empresas/feature/credito/produtos/model/ProdutoChildItem;->setDescricao(Ljava/lang/String;)V

    .line 108
    invoke-virtual {v4}, Lcom/itau/empresas/feature/credito/produtos/model/ProdutosParceladosVO;->getValorDisponivelContratacao()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Lcom/itau/empresas/feature/credito/produtos/model/ProdutoChildItem;->setValor(Ljava/lang/String;)V

    .line 109
    invoke-virtual {v5, v4}, Lcom/itau/empresas/feature/credito/produtos/model/ProdutoChildItem;->setProdutosParcelados(Lcom/itau/empresas/feature/credito/produtos/model/ProdutosParceladosVO;)V

    .line 110
    invoke-interface {v2, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 111
    .end local v4    # "produto":Lcom/itau/empresas/feature/credito/produtos/model/ProdutosParceladosVO;
    .end local v5    # "item":Lcom/itau/empresas/feature/credito/produtos/model/ProdutoChildItem;
    goto :goto_a8

    .line 115
    :cond_cf
    :goto_cf
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/ProdutosLimitesContratadosFragment;->limitesProdutosListAdapter:Lcom/itau/empresas/feature/credito/produtos/adapter/LimitesProdutosListAdapter;

    invoke-virtual {v0, v2}, Lcom/itau/empresas/feature/credito/produtos/adapter/LimitesProdutosListAdapter;->setData(Ljava/util/List;)V

    .line 116
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/ProdutosLimitesContratadosFragment;->limitesProdutosListAdapter:Lcom/itau/empresas/feature/credito/produtos/adapter/LimitesProdutosListAdapter;

    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/produtos/fragment/ProdutosLimitesContratadosFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/credito/produtos/adapter/LimitesProdutosListAdapter;->setContext(Landroid/content/Context;)V

    .line 117
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/ProdutosLimitesContratadosFragment;->listaProdutos:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/ProdutosLimitesContratadosFragment;->limitesProdutosListAdapter:Lcom/itau/empresas/feature/credito/produtos/adapter/LimitesProdutosListAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 118
    return-void
.end method

.method public opkeysConsumidas()Ljava/util/List;
    .registers 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Ljava/lang/String;>;"
        }
    .end annotation

    .line 148
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "limitesProduto"

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const-string v1, "MULTILIMITE_PJ"

    const/4 v2, 0x1

    aput-object v1, v0, v2

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public setPosition(I)V
    .registers 2
    .param p1, "pos"    # I

    .line 62
    iput p1, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/ProdutosLimitesContratadosFragment;->position:I

    .line 63
    return-void
.end method

.method public setRetornoMultiLimite(Lcom/itau/empresas/feature/credito/produtos/model/MultiLimiteVO;)V
    .registers 2
    .param p1, "retornoMultiLimite"    # Lcom/itau/empresas/feature/credito/produtos/model/MultiLimiteVO;

    .line 58
    iput-object p1, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/ProdutosLimitesContratadosFragment;->retornoMultiLimite:Lcom/itau/empresas/feature/credito/produtos/model/MultiLimiteVO;

    .line 59
    return-void
.end method

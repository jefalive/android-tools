.class final Lcom/itau/empresas/feature/login/strategy/ValidadorStrategy;
.super Ljava/lang/Object;
.source "ValidadorStrategy.java"


# direct methods
.method static possuiSomenteUmOperador(Ljava/util/List;)Z
    .registers 3
    .param p0, "operadorVOs"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<Lcom/itau/empresas/api/model/OperadorVO;>;)Z"
        }
    .end annotation

    .line 20
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_9

    const/4 v0, 0x1

    goto :goto_a

    :cond_9
    const/4 v0, 0x0

    :goto_a
    return v0
.end method

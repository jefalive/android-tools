.class Lorg/simpleframework/xml/core/Qualifier;
.super Ljava/lang/Object;
.source "Qualifier.java"

# interfaces
.implements Lorg/simpleframework/xml/core/Decorator;


# instance fields
.field private decorator:Lorg/simpleframework/xml/core/NamespaceDecorator;


# direct methods
.method public constructor <init>(Lorg/simpleframework/xml/core/Contact;)V
    .registers 3
    .param p1, "contact"    # Lorg/simpleframework/xml/core/Contact;

    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    new-instance v0, Lorg/simpleframework/xml/core/NamespaceDecorator;

    invoke-direct {v0}, Lorg/simpleframework/xml/core/NamespaceDecorator;-><init>()V

    iput-object v0, p0, Lorg/simpleframework/xml/core/Qualifier;->decorator:Lorg/simpleframework/xml/core/NamespaceDecorator;

    .line 52
    invoke-direct {p0, p1}, Lorg/simpleframework/xml/core/Qualifier;->scan(Lorg/simpleframework/xml/core/Contact;)V

    .line 53
    return-void
.end method

.method private namespace(Lorg/simpleframework/xml/core/Contact;)V
    .registers 4
    .param p1, "contact"    # Lorg/simpleframework/xml/core/Contact;

    .line 105
    const-class v0, Lorg/simpleframework/xml/Namespace;

    invoke-interface {p1, v0}, Lorg/simpleframework/xml/core/Contact;->getAnnotation(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lorg/simpleframework/xml/Namespace;

    .line 107
    .local v1, "primary":Lorg/simpleframework/xml/Namespace;
    if-eqz v1, :cond_15

    .line 108
    iget-object v0, p0, Lorg/simpleframework/xml/core/Qualifier;->decorator:Lorg/simpleframework/xml/core/NamespaceDecorator;

    invoke-virtual {v0, v1}, Lorg/simpleframework/xml/core/NamespaceDecorator;->set(Lorg/simpleframework/xml/Namespace;)V

    .line 109
    iget-object v0, p0, Lorg/simpleframework/xml/core/Qualifier;->decorator:Lorg/simpleframework/xml/core/NamespaceDecorator;

    invoke-virtual {v0, v1}, Lorg/simpleframework/xml/core/NamespaceDecorator;->add(Lorg/simpleframework/xml/Namespace;)V

    .line 111
    :cond_15
    return-void
.end method

.method private scan(Lorg/simpleframework/xml/core/Contact;)V
    .registers 2
    .param p1, "contact"    # Lorg/simpleframework/xml/core/Contact;

    .line 92
    invoke-direct {p0, p1}, Lorg/simpleframework/xml/core/Qualifier;->namespace(Lorg/simpleframework/xml/core/Contact;)V

    .line 93
    invoke-direct {p0, p1}, Lorg/simpleframework/xml/core/Qualifier;->scope(Lorg/simpleframework/xml/core/Contact;)V

    .line 94
    return-void
.end method

.method private scope(Lorg/simpleframework/xml/core/Contact;)V
    .registers 8
    .param p1, "contact"    # Lorg/simpleframework/xml/core/Contact;

    .line 122
    const-class v0, Lorg/simpleframework/xml/NamespaceList;

    invoke-interface {p1, v0}, Lorg/simpleframework/xml/core/Contact;->getAnnotation(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lorg/simpleframework/xml/NamespaceList;

    .line 124
    .local v1, "scope":Lorg/simpleframework/xml/NamespaceList;
    if-eqz v1, :cond_1d

    .line 125
    invoke-interface {v1}, Lorg/simpleframework/xml/NamespaceList;->value()[Lorg/simpleframework/xml/Namespace;

    move-result-object v2

    .local v2, "arr$":[Lorg/simpleframework/xml/Namespace;
    array-length v3, v2

    .local v3, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_11
    if-ge v4, v3, :cond_1d

    aget-object v5, v2, v4

    .line 126
    .local v5, "name":Lorg/simpleframework/xml/Namespace;
    iget-object v0, p0, Lorg/simpleframework/xml/core/Qualifier;->decorator:Lorg/simpleframework/xml/core/NamespaceDecorator;

    invoke-virtual {v0, v5}, Lorg/simpleframework/xml/core/NamespaceDecorator;->add(Lorg/simpleframework/xml/Namespace;)V

    .line 125
    .end local v5    # "name":Lorg/simpleframework/xml/Namespace;
    add-int/lit8 v4, v4, 0x1

    goto :goto_11

    .line 129
    .end local v2    # "arr$":[Lorg/simpleframework/xml/Namespace;
    .end local v3    # "len$":I
    .end local v4    # "i$":I
    :cond_1d
    return-void
.end method


# virtual methods
.method public decorate(Lorg/simpleframework/xml/stream/OutputNode;)V
    .registers 3
    .param p1, "node"    # Lorg/simpleframework/xml/stream/OutputNode;

    .line 64
    iget-object v0, p0, Lorg/simpleframework/xml/core/Qualifier;->decorator:Lorg/simpleframework/xml/core/NamespaceDecorator;

    invoke-virtual {v0, p1}, Lorg/simpleframework/xml/core/NamespaceDecorator;->decorate(Lorg/simpleframework/xml/stream/OutputNode;)V

    .line 65
    return-void
.end method

.method public decorate(Lorg/simpleframework/xml/stream/OutputNode;Lorg/simpleframework/xml/core/Decorator;)V
    .registers 4
    .param p1, "node"    # Lorg/simpleframework/xml/stream/OutputNode;
    .param p2, "secondary"    # Lorg/simpleframework/xml/core/Decorator;

    .line 79
    iget-object v0, p0, Lorg/simpleframework/xml/core/Qualifier;->decorator:Lorg/simpleframework/xml/core/NamespaceDecorator;

    invoke-virtual {v0, p1, p2}, Lorg/simpleframework/xml/core/NamespaceDecorator;->decorate(Lorg/simpleframework/xml/stream/OutputNode;Lorg/simpleframework/xml/core/Decorator;)V

    .line 80
    return-void
.end method

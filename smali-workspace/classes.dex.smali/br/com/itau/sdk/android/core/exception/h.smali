.class public Lbr/com/itau/sdk/android/core/exception/h;
.super Ljava/lang/RuntimeException;
.source "SourceFile"


# static fields
.field private static final b:[S

.field private static c:I


# direct methods
.method static constructor <clinit>()V
    .registers 3

    const/16 v0, 0x6f

    new-array v0, v0, [S

    fill-array-data v0, :array_e

    sput-object v0, Lbr/com/itau/sdk/android/core/exception/h;->b:[S

    const/16 v0, 0xc9

    sput v0, Lbr/com/itau/sdk/android/core/exception/h;->c:I

    return-void

    :array_e
    .array-data 2
        0x6es
        -0xfs
        0x12s
        -0x71s
        0x86s
        -0x83s
        -0x5es
        0x37s
        -0x6s
        -0x15s
        -0x58s
        0x41s
        -0x10s
        -0xbs
        -0xfs
        0x6bs
        -0x86s
        -0x20s
        -0x8s
        -0x5bs
        0x34s
        -0x3s
        -0x11s
        -0xcs
        -0x13s
        -0x16s
        0x0s
        -0x22s
        0x2s
        -0x61s
        0x40s
        -0x5es
        0x32s
        -0xds
        -0xds
        -0x1s
        -0xfs
        -0x13s
        -0x52s
        -0x1bs
        0x41s
        -0x10s
        -0x15s
        -0x5s
        -0x62s
        0x36s
        0x4s
        -0x1es
        -0x5s
        -0xes
        -0x1es
        -0x54s
        0x46s
        -0x17s
        -0x1bs
        -0x50s
        0x44s
        -0x1ds
        -0x1s
        -0xfs
        0x61s
        -0x83s
        -0x5es
        0x32s
        0x4s
        -0x1as
        -0x2s
        -0x24s
        -0x42s
        -0x1ds
        0x12s
        0x17s
        -0x1s
        -0x23s
        0x2s
        -0x1ds
        -0xes
        -0x54s
        0x32s
        -0x4s
        -0x14s
        -0x1s
        -0x16s
        -0xas
        -0x62s
        0x3es
        -0x13s
        -0xas
        -0x8s
        -0x10s
        -0x14s
        -0xbs
        -0x62s
        0x36s
        -0x54s
        0x45s
        -0x1es
        -0x6s
        -0x9s
        -0x1es
        -0x54s
        0x3fs
        -0xes
        -0x8s
        -0x24s
        -0x3s
        -0x17s
        -0x6s
        -0x9s
        -0x1es
        -0x46s
    .end array-data
.end method

.method public constructor <init>()V
    .registers 3

    .line 9
    sget-object v0, Lbr/com/itau/sdk/android/core/exception/h;->b:[S

    const/16 v1, 0x1a

    aget-short v0, v0, v1

    invoke-static {v0, v0, v0}, Lbr/com/itau/sdk/android/core/exception/h;->a(III)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    .line 10
    return-void
.end method

.method private static a(III)Ljava/lang/String;
    .registers 9

    const/4 v5, 0x0

    new-instance v0, Ljava/lang/String;

    sget-object v4, Lbr/com/itau/sdk/android/core/exception/h;->b:[S

    mul-int/lit8 p0, p0, 0x2

    rsub-int/lit8 p0, p0, 0x4e

    mul-int/lit8 p2, p2, 0x3

    rsub-int/lit8 p2, p2, 0x3

    mul-int/lit8 p1, p1, 0x4

    rsub-int/lit8 p1, p1, 0x6c

    new-array v1, p1, [B

    add-int/lit8 p1, p1, -0x1

    if-nez v4, :cond_1c

    move v2, p0

    move v3, p1

    :goto_19
    add-int/2addr v2, v3

    add-int/lit8 p0, v2, 0xf

    :cond_1c
    add-int/lit8 p2, p2, 0x1

    int-to-byte v2, p0

    aput-byte v2, v1, v5

    if-ne v5, p1, :cond_28

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Ljava/lang/String;-><init>([BI)V

    return-object v0

    :cond_28
    move v2, p0

    add-int/lit8 v5, v5, 0x1

    aget-short v3, v4, p2

    goto :goto_19
.end method

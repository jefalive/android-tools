.class public Lcom/itau/empresas/adapter/DetalhesLisAdapter;
.super Landroid/widget/BaseAdapter;
.source "DetalhesLisAdapter.java"


# instance fields
.field private context:Landroid/content/Context;

.field private produtos:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Lcom/itau/empresas/api/model/DetalheLisVO;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 19
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public getCount()I
    .registers 2

    .line 34
    iget-object v0, p0, Lcom/itau/empresas/adapter/DetalhesLisAdapter;->produtos:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Lcom/itau/empresas/api/model/DetalheLisVO;
    .registers 3
    .param p1, "position"    # I

    .line 39
    iget-object v0, p0, Lcom/itau/empresas/adapter/DetalhesLisAdapter;->produtos:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/api/model/DetalheLisVO;

    return-object v0
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .registers 3

    .line 18
    invoke-virtual {p0, p1}, Lcom/itau/empresas/adapter/DetalhesLisAdapter;->getItem(I)Lcom/itau/empresas/api/model/DetalheLisVO;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .registers 4
    .param p1, "position"    # I

    .line 44
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 7
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;
    .annotation build Landroid/annotation/TargetApi;
        value = 0x13
    .end annotation

    .line 50
    iget-object v0, p0, Lcom/itau/empresas/adapter/DetalhesLisAdapter;->produtos:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/itau/empresas/api/model/DetalheLisVO;

    .line 52
    .local v1, "produto":Lcom/itau/empresas/api/model/DetalheLisVO;
    if-nez p2, :cond_11

    .line 53
    iget-object v0, p0, Lcom/itau/empresas/adapter/DetalhesLisAdapter;->context:Landroid/content/Context;

    invoke-static {v0}, Lcom/itau/empresas/ui/view/DetalheProdutoView_;->build(Landroid/content/Context;)Lcom/itau/empresas/ui/view/DetalheProdutoView;

    move-result-object p2

    .line 56
    :cond_11
    move-object v2, p2

    check-cast v2, Lcom/itau/empresas/ui/view/DetalheProdutoView;

    .line 57
    .local v2, "view":Lcom/itau/empresas/ui/view/DetalheProdutoView;
    invoke-virtual {v2, v1}, Lcom/itau/empresas/ui/view/DetalheProdutoView;->bind(Lcom/itau/empresas/api/model/DetalheLisVO;)V

    .line 59
    return-object p2
.end method

.method public notifyDataSetChanged()V
    .registers 1

    .line 64
    invoke-super {p0}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    .line 65
    return-void
.end method

.method public setContext(Landroid/content/Context;)V
    .registers 2
    .param p1, "context"    # Landroid/content/Context;

    .line 29
    iput-object p1, p0, Lcom/itau/empresas/adapter/DetalhesLisAdapter;->context:Landroid/content/Context;

    .line 30
    return-void
.end method

.method public setData(Ljava/util/List;)V
    .registers 2
    .param p1, "produtos"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<Lcom/itau/empresas/api/model/DetalheLisVO;>;)V"
        }
    .end annotation

    .line 25
    iput-object p1, p0, Lcom/itau/empresas/adapter/DetalhesLisAdapter;->produtos:Ljava/util/List;

    .line 26
    return-void
.end method

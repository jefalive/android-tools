.class public Lcom/google/android/gms/internal/zzax;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/internal/zzay;


# annotations
.annotation runtime Lcom/google/android/gms/internal/zzhb;
.end annotation


# instance fields
.field private final zzpT:Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;

.field private final zzpV:Ljava/lang/Object;

.field private final zzsB:Ljava/util/WeakHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/WeakHashMap<Lcom/google/android/gms/internal/zzif;Lcom/google/android/gms/internal/zzau;>;"
        }
    .end annotation
.end field

.field private final zzsC:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<Lcom/google/android/gms/internal/zzau;>;"
        }
    .end annotation
.end field

.field private final zzsD:Lcom/google/android/gms/internal/zzeg;

.field private final zzsa:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;Lcom/google/android/gms/internal/zzeg;)V
    .registers 5

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/internal/zzax;->zzpV:Ljava/lang/Object;

    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/internal/zzax;->zzsB:Ljava/util/WeakHashMap;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/internal/zzax;->zzsC:Ljava/util/ArrayList;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/internal/zzax;->zzsa:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/android/gms/internal/zzax;->zzpT:Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;

    iput-object p3, p0, Lcom/google/android/gms/internal/zzax;->zzsD:Lcom/google/android/gms/internal/zzeg;

    return-void
.end method


# virtual methods
.method public zza(Lcom/google/android/gms/ads/internal/client/AdSizeParcel;Lcom/google/android/gms/internal/zzif;)Lcom/google/android/gms/internal/zzau;
    .registers 4

    iget-object v0, p2, Lcom/google/android/gms/internal/zzif;->zzED:Lcom/google/android/gms/internal/zzjp;

    invoke-interface {v0}, Lcom/google/android/gms/internal/zzjp;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, p1, p2, v0}, Lcom/google/android/gms/internal/zzax;->zza(Lcom/google/android/gms/ads/internal/client/AdSizeParcel;Lcom/google/android/gms/internal/zzif;Landroid/view/View;)Lcom/google/android/gms/internal/zzau;

    move-result-object v0

    return-object v0
.end method

.method public zza(Lcom/google/android/gms/ads/internal/client/AdSizeParcel;Lcom/google/android/gms/internal/zzif;Landroid/view/View;)Lcom/google/android/gms/internal/zzau;
    .registers 6

    new-instance v0, Lcom/google/android/gms/internal/zzau$zzd;

    invoke-direct {v0, p3, p2}, Lcom/google/android/gms/internal/zzau$zzd;-><init>(Landroid/view/View;Lcom/google/android/gms/internal/zzif;)V

    const/4 v1, 0x0

    invoke-virtual {p0, p1, p2, v0, v1}, Lcom/google/android/gms/internal/zzax;->zza(Lcom/google/android/gms/ads/internal/client/AdSizeParcel;Lcom/google/android/gms/internal/zzif;Lcom/google/android/gms/internal/zzbb;Lcom/google/android/gms/internal/zzeh;)Lcom/google/android/gms/internal/zzau;

    move-result-object v0

    return-object v0
.end method

.method public zza(Lcom/google/android/gms/ads/internal/client/AdSizeParcel;Lcom/google/android/gms/internal/zzif;Landroid/view/View;Lcom/google/android/gms/internal/zzeh;)Lcom/google/android/gms/internal/zzau;
    .registers 6

    new-instance v0, Lcom/google/android/gms/internal/zzau$zzd;

    invoke-direct {v0, p3, p2}, Lcom/google/android/gms/internal/zzau$zzd;-><init>(Landroid/view/View;Lcom/google/android/gms/internal/zzif;)V

    invoke-virtual {p0, p1, p2, v0, p4}, Lcom/google/android/gms/internal/zzax;->zza(Lcom/google/android/gms/ads/internal/client/AdSizeParcel;Lcom/google/android/gms/internal/zzif;Lcom/google/android/gms/internal/zzbb;Lcom/google/android/gms/internal/zzeh;)Lcom/google/android/gms/internal/zzau;

    move-result-object v0

    return-object v0
.end method

.method public zza(Lcom/google/android/gms/ads/internal/client/AdSizeParcel;Lcom/google/android/gms/internal/zzif;Lcom/google/android/gms/ads/internal/formats/zzh;)Lcom/google/android/gms/internal/zzau;
    .registers 6

    new-instance v0, Lcom/google/android/gms/internal/zzau$zza;

    invoke-direct {v0, p3}, Lcom/google/android/gms/internal/zzau$zza;-><init>(Lcom/google/android/gms/ads/internal/formats/zzh;)V

    const/4 v1, 0x0

    invoke-virtual {p0, p1, p2, v0, v1}, Lcom/google/android/gms/internal/zzax;->zza(Lcom/google/android/gms/ads/internal/client/AdSizeParcel;Lcom/google/android/gms/internal/zzif;Lcom/google/android/gms/internal/zzbb;Lcom/google/android/gms/internal/zzeh;)Lcom/google/android/gms/internal/zzau;

    move-result-object v0

    return-object v0
.end method

.method public zza(Lcom/google/android/gms/ads/internal/client/AdSizeParcel;Lcom/google/android/gms/internal/zzif;Lcom/google/android/gms/internal/zzbb;Lcom/google/android/gms/internal/zzeh;)Lcom/google/android/gms/internal/zzau;
    .registers 15

    iget-object v7, p0, Lcom/google/android/gms/internal/zzax;->zzpV:Ljava/lang/Object;

    monitor-enter v7

    :try_start_3
    invoke-virtual {p0, p2}, Lcom/google/android/gms/internal/zzax;->zzh(Lcom/google/android/gms/internal/zzif;)Z

    move-result v0

    if-eqz v0, :cond_13

    iget-object v0, p0, Lcom/google/android/gms/internal/zzax;->zzsB:Ljava/util/WeakHashMap;

    invoke-virtual {v0, p2}, Ljava/util/WeakHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/internal/zzau;
    :try_end_11
    .catchall {:try_start_3 .. :try_end_11} :catchall_42

    monitor-exit v7

    return-object v0

    :cond_13
    if-eqz p4, :cond_24

    :try_start_15
    new-instance v0, Lcom/google/android/gms/internal/zzaz;

    iget-object v1, p0, Lcom/google/android/gms/internal/zzax;->zzsa:Landroid/content/Context;

    move-object v2, p1

    move-object v3, p2

    iget-object v4, p0, Lcom/google/android/gms/internal/zzax;->zzpT:Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;

    move-object v5, p3

    move-object v6, p4

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/internal/zzaz;-><init>(Landroid/content/Context;Lcom/google/android/gms/ads/internal/client/AdSizeParcel;Lcom/google/android/gms/internal/zzif;Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;Lcom/google/android/gms/internal/zzbb;Lcom/google/android/gms/internal/zzeh;)V

    move-object v8, v0

    goto :goto_33

    :cond_24
    new-instance v0, Lcom/google/android/gms/internal/zzba;

    iget-object v1, p0, Lcom/google/android/gms/internal/zzax;->zzsa:Landroid/content/Context;

    move-object v2, p1

    move-object v3, p2

    iget-object v4, p0, Lcom/google/android/gms/internal/zzax;->zzpT:Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;

    move-object v5, p3

    iget-object v6, p0, Lcom/google/android/gms/internal/zzax;->zzsD:Lcom/google/android/gms/internal/zzeg;

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/internal/zzba;-><init>(Landroid/content/Context;Lcom/google/android/gms/ads/internal/client/AdSizeParcel;Lcom/google/android/gms/internal/zzif;Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;Lcom/google/android/gms/internal/zzbb;Lcom/google/android/gms/internal/zzeg;)V

    move-object v8, v0

    :goto_33
    invoke-virtual {v8, p0}, Lcom/google/android/gms/internal/zzau;->zza(Lcom/google/android/gms/internal/zzay;)V

    iget-object v0, p0, Lcom/google/android/gms/internal/zzax;->zzsB:Ljava/util/WeakHashMap;

    invoke-virtual {v0, p2, v8}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/gms/internal/zzax;->zzsC:Ljava/util/ArrayList;

    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_40
    .catchall {:try_start_15 .. :try_end_40} :catchall_42

    monitor-exit v7

    return-object v8

    :catchall_42
    move-exception v9

    monitor-exit v7

    throw v9
.end method

.method public zza(Lcom/google/android/gms/internal/zzau;)V
    .registers 7

    iget-object v1, p0, Lcom/google/android/gms/internal/zzax;->zzpV:Ljava/lang/Object;

    monitor-enter v1

    :try_start_3
    invoke-virtual {p1}, Lcom/google/android/gms/internal/zzau;->zzch()Z

    move-result v0

    if-nez v0, :cond_2f

    iget-object v0, p0, Lcom/google/android/gms/internal/zzax;->zzsC:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/gms/internal/zzax;->zzsB:Ljava/util/WeakHashMap;

    invoke-virtual {v0}, Ljava/util/WeakHashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_18
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2f

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Ljava/util/Map$Entry;

    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    if-ne v0, p1, :cond_2e

    invoke-interface {v2}, Ljava/util/Iterator;->remove()V
    :try_end_2e
    .catchall {:try_start_3 .. :try_end_2e} :catchall_31

    :cond_2e
    goto :goto_18

    :cond_2f
    monitor-exit v1

    goto :goto_34

    :catchall_31
    move-exception v4

    monitor-exit v1

    throw v4

    :goto_34
    return-void
.end method

.method public zzh(Lcom/google/android/gms/internal/zzif;)Z
    .registers 6

    iget-object v1, p0, Lcom/google/android/gms/internal/zzax;->zzpV:Ljava/lang/Object;

    monitor-enter v1

    :try_start_3
    iget-object v0, p0, Lcom/google/android/gms/internal/zzax;->zzsB:Ljava/util/WeakHashMap;

    invoke-virtual {v0, p1}, Ljava/util/WeakHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/google/android/gms/internal/zzau;

    if-eqz v2, :cond_16

    invoke-virtual {v2}, Lcom/google/android/gms/internal/zzau;->zzch()Z
    :try_end_11
    .catchall {:try_start_3 .. :try_end_11} :catchall_19

    move-result v0

    if-eqz v0, :cond_16

    const/4 v0, 0x1

    goto :goto_17

    :cond_16
    const/4 v0, 0x0

    :goto_17
    monitor-exit v1

    return v0

    :catchall_19
    move-exception v3

    monitor-exit v1

    throw v3
.end method

.method public zzi(Lcom/google/android/gms/internal/zzif;)V
    .registers 6

    iget-object v1, p0, Lcom/google/android/gms/internal/zzax;->zzpV:Ljava/lang/Object;

    monitor-enter v1

    :try_start_3
    iget-object v0, p0, Lcom/google/android/gms/internal/zzax;->zzsB:Ljava/util/WeakHashMap;

    invoke-virtual {v0, p1}, Ljava/util/WeakHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/google/android/gms/internal/zzau;

    if-eqz v2, :cond_11

    invoke-virtual {v2}, Lcom/google/android/gms/internal/zzau;->zzcf()V
    :try_end_11
    .catchall {:try_start_3 .. :try_end_11} :catchall_13

    :cond_11
    monitor-exit v1

    goto :goto_16

    :catchall_13
    move-exception v3

    monitor-exit v1

    throw v3

    :goto_16
    return-void
.end method

.method public zzj(Lcom/google/android/gms/internal/zzif;)V
    .registers 6

    iget-object v1, p0, Lcom/google/android/gms/internal/zzax;->zzpV:Ljava/lang/Object;

    monitor-enter v1

    :try_start_3
    iget-object v0, p0, Lcom/google/android/gms/internal/zzax;->zzsB:Ljava/util/WeakHashMap;

    invoke-virtual {v0, p1}, Ljava/util/WeakHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/google/android/gms/internal/zzau;

    if-eqz v2, :cond_11

    invoke-virtual {v2}, Lcom/google/android/gms/internal/zzau;->stop()V
    :try_end_11
    .catchall {:try_start_3 .. :try_end_11} :catchall_13

    :cond_11
    monitor-exit v1

    goto :goto_16

    :catchall_13
    move-exception v3

    monitor-exit v1

    throw v3

    :goto_16
    return-void
.end method

.method public zzk(Lcom/google/android/gms/internal/zzif;)V
    .registers 6

    iget-object v1, p0, Lcom/google/android/gms/internal/zzax;->zzpV:Ljava/lang/Object;

    monitor-enter v1

    :try_start_3
    iget-object v0, p0, Lcom/google/android/gms/internal/zzax;->zzsB:Ljava/util/WeakHashMap;

    invoke-virtual {v0, p1}, Ljava/util/WeakHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/google/android/gms/internal/zzau;

    if-eqz v2, :cond_11

    invoke-virtual {v2}, Lcom/google/android/gms/internal/zzau;->pause()V
    :try_end_11
    .catchall {:try_start_3 .. :try_end_11} :catchall_13

    :cond_11
    monitor-exit v1

    goto :goto_16

    :catchall_13
    move-exception v3

    monitor-exit v1

    throw v3

    :goto_16
    return-void
.end method

.method public zzl(Lcom/google/android/gms/internal/zzif;)V
    .registers 6

    iget-object v1, p0, Lcom/google/android/gms/internal/zzax;->zzpV:Ljava/lang/Object;

    monitor-enter v1

    :try_start_3
    iget-object v0, p0, Lcom/google/android/gms/internal/zzax;->zzsB:Ljava/util/WeakHashMap;

    invoke-virtual {v0, p1}, Ljava/util/WeakHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/google/android/gms/internal/zzau;

    if-eqz v2, :cond_11

    invoke-virtual {v2}, Lcom/google/android/gms/internal/zzau;->resume()V
    :try_end_11
    .catchall {:try_start_3 .. :try_end_11} :catchall_13

    :cond_11
    monitor-exit v1

    goto :goto_16

    :catchall_13
    move-exception v3

    monitor-exit v1

    throw v3

    :goto_16
    return-void
.end method

.class public Lcom/itau/empresas/api/universal/model/DetalhesMultilimiteXMLVO$Dados;
.super Ljava/lang/Object;
.source "DetalhesMultilimiteXMLVO.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/itau/empresas/api/universal/model/DetalhesMultilimiteXMLVO;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Dados"
.end annotation


# instance fields
.field private produtoYW04:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Ljava/lang/Object;>;"
        }
    .end annotation

    .annotation runtime Lorg/simpleframework/xml/ElementList;
        inline = true
        name = "PRODUTOYW04"
        required = false
    .end annotation
.end field

.field private produtoYW10:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Ljava/lang/Object;>;"
        }
    .end annotation

    .annotation runtime Lorg/simpleframework/xml/ElementList;
        inline = true
        name = "PRODUTOYW10"
        required = false
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getProdutoYW04()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Ljava/lang/Object;>;"
        }
    .end annotation

    .line 44
    iget-object v0, p0, Lcom/itau/empresas/api/universal/model/DetalhesMultilimiteXMLVO$Dados;->produtoYW04:Ljava/util/List;

    return-object v0
.end method

.method public getProdutoYW10()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Ljava/lang/Object;>;"
        }
    .end annotation

    .line 48
    iget-object v0, p0, Lcom/itau/empresas/api/universal/model/DetalhesMultilimiteXMLVO$Dados;->produtoYW10:Ljava/util/List;

    return-object v0
.end method

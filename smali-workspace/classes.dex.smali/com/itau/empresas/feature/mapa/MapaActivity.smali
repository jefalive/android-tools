.class public Lcom/itau/empresas/feature/mapa/MapaActivity;
.super Lcom/itau/empresas/ui/activity/BaseActivity;
.source "MapaActivity.java"

# interfaces
.implements Lcom/itau/empresas/ui/util/SnackbarUtils$Gerenciador;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/itau/empresas/feature/mapa/MapaActivity$DrawerListener;
    }
.end annotation


# instance fields
.field controller:Lcom/itau/empresas/controller/MapasController;

.field public drawerLayoutFilter:Landroid/support/v4/widget/DrawerLayout;

.field private filtro:Lcom/itau/empresas/api/model/FiltroAgenciaVO;

.field lastPosition:Lcom/google/android/gms/maps/model/LatLng;

.field pagerMap:Landroid/support/v4/view/ViewPager;

.field root:Landroid/widget/RelativeLayout;

.field switchFiltroCaixaEletronico:Landroid/support/v7/widget/SwitchCompat;

.field switchFiltroEmpresas:Landroid/support/v7/widget/SwitchCompat;

.field switchFiltroItau:Landroid/support/v7/widget/SwitchCompat;

.field switchFiltroPersonnalite:Landroid/support/v7/widget/SwitchCompat;

.field private tabsAdapter:Lcom/itau/empresas/feature/mapa/MapaAdapter;

.field tabsMap:Landroid/support/design/widget/TabLayout;

.field tituloToolbar:Landroid/widget/TextView;

.field toolbar:Landroid/support/v7/widget/Toolbar;

.field private topSnackbar:Lcom/itau/empresas/ui/util/SnackbarUtils$Gerenciador;


# direct methods
.method public constructor <init>()V
    .registers 2

    .line 37
    invoke-direct {p0}, Lcom/itau/empresas/ui/activity/BaseActivity;-><init>()V

    .line 54
    new-instance v0, Lcom/itau/empresas/api/model/FiltroAgenciaVO;

    invoke-direct {v0}, Lcom/itau/empresas/api/model/FiltroAgenciaVO;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaActivity;->filtro:Lcom/itau/empresas/api/model/FiltroAgenciaVO;

    return-void
.end method

.method static synthetic access$100(Lcom/itau/empresas/feature/mapa/MapaActivity;)V
    .registers 1
    .param p0, "x0"    # Lcom/itau/empresas/feature/mapa/MapaActivity;

    .line 37
    invoke-direct {p0}, Lcom/itau/empresas/feature/mapa/MapaActivity;->getAgencias()V

    return-void
.end method

.method private atualizaSwitchs(Lcom/itau/empresas/api/model/FiltroAgenciaVO;)V
    .registers 4
    .param p1, "filtro"    # Lcom/itau/empresas/api/model/FiltroAgenciaVO;

    .line 130
    iget-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaActivity;->switchFiltroItau:Landroid/support/v7/widget/SwitchCompat;

    invoke-virtual {p1}, Lcom/itau/empresas/api/model/FiltroAgenciaVO;->getFiltroItau()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/SwitchCompat;->setChecked(Z)V

    .line 131
    iget-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaActivity;->switchFiltroPersonnalite:Landroid/support/v7/widget/SwitchCompat;

    invoke-virtual {p1}, Lcom/itau/empresas/api/model/FiltroAgenciaVO;->getFiltroPers()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/SwitchCompat;->setChecked(Z)V

    .line 132
    iget-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaActivity;->switchFiltroEmpresas:Landroid/support/v7/widget/SwitchCompat;

    invoke-virtual {p1}, Lcom/itau/empresas/api/model/FiltroAgenciaVO;->getFiltroEmpresas()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/SwitchCompat;->setChecked(Z)V

    .line 133
    iget-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaActivity;->switchFiltroCaixaEletronico:Landroid/support/v7/widget/SwitchCompat;

    invoke-virtual {p1}, Lcom/itau/empresas/api/model/FiltroAgenciaVO;->getFiltroCaixas()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/SwitchCompat;->setChecked(Z)V

    .line 134
    return-void
.end method

.method private constroiToolbar()V
    .registers 3

    .line 123
    iget-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaActivity;->toolbar:Landroid/support/v7/widget/Toolbar;

    invoke-static {v0, p0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom;->Builder(Landroid/support/v7/widget/Toolbar;Landroid/support/v7/app/AppCompatActivity;)Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    move-result-object v0

    .line 124
    const v1, 0x7f0706b3

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->comTitulo(I)Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    move-result-object v0

    .line 125
    invoke-virtual {v0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->comClickListenerFinish()Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    move-result-object v0

    .line 126
    invoke-virtual {v0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->build()Lcom/itau/empresas/ui/util/listener/ToolbarCustom;

    .line 127
    return-void
.end method

.method private getAgencias()V
    .registers 3

    .line 137
    invoke-virtual {p0}, Lcom/itau/empresas/feature/mapa/MapaActivity;->mostraDialogoDeProgresso()V

    .line 138
    invoke-virtual {p0}, Lcom/itau/empresas/feature/mapa/MapaActivity;->dismissSnackbar()V

    .line 140
    iget-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaActivity;->filtro:Lcom/itau/empresas/api/model/FiltroAgenciaVO;

    invoke-virtual {v0}, Lcom/itau/empresas/api/model/FiltroAgenciaVO;->getFiltroItau()Z

    move-result v0

    if-nez v0, :cond_32

    iget-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaActivity;->filtro:Lcom/itau/empresas/api/model/FiltroAgenciaVO;

    .line 141
    invoke-virtual {v0}, Lcom/itau/empresas/api/model/FiltroAgenciaVO;->getFiltroPers()Z

    move-result v0

    if-nez v0, :cond_32

    iget-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaActivity;->filtro:Lcom/itau/empresas/api/model/FiltroAgenciaVO;

    .line 142
    invoke-virtual {v0}, Lcom/itau/empresas/api/model/FiltroAgenciaVO;->getFiltroEmpresas()Z

    move-result v0

    if-nez v0, :cond_32

    iget-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaActivity;->filtro:Lcom/itau/empresas/api/model/FiltroAgenciaVO;

    .line 143
    invoke-virtual {v0}, Lcom/itau/empresas/api/model/FiltroAgenciaVO;->getFiltroCaixasExc()Z

    move-result v0

    if-nez v0, :cond_32

    .line 144
    const v0, 0x7f07045a

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/mapa/MapaActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/itau/empresas/feature/mapa/MapaActivity;->showSnackbar(Ljava/lang/CharSequence;I)V

    .line 146
    return-void

    .line 148
    :cond_32
    iget-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaActivity;->filtro:Lcom/itau/empresas/api/model/FiltroAgenciaVO;

    invoke-virtual {v0}, Lcom/itau/empresas/api/model/FiltroAgenciaVO;->getFiltroCaixasExc()Z

    move-result v0

    if-eqz v0, :cond_67

    iget-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaActivity;->filtro:Lcom/itau/empresas/api/model/FiltroAgenciaVO;

    invoke-virtual {v0}, Lcom/itau/empresas/api/model/FiltroAgenciaVO;->getFiltroItau()Z

    move-result v0

    if-nez v0, :cond_67

    iget-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaActivity;->filtro:Lcom/itau/empresas/api/model/FiltroAgenciaVO;

    invoke-virtual {v0}, Lcom/itau/empresas/api/model/FiltroAgenciaVO;->getFiltroEmpresas()Z

    move-result v0

    if-nez v0, :cond_67

    iget-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaActivity;->filtro:Lcom/itau/empresas/api/model/FiltroAgenciaVO;

    .line 149
    invoke-virtual {v0}, Lcom/itau/empresas/api/model/FiltroAgenciaVO;->getFiltroPers()Z

    move-result v0

    if-nez v0, :cond_67

    .line 150
    iget-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaActivity;->filtro:Lcom/itau/empresas/api/model/FiltroAgenciaVO;

    const-string v1, "true"

    invoke-virtual {v0, v1}, Lcom/itau/empresas/api/model/FiltroAgenciaVO;->setFiltroItau(Ljava/lang/String;)V

    .line 151
    iget-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaActivity;->filtro:Lcom/itau/empresas/api/model/FiltroAgenciaVO;

    const-string v1, "true"

    invoke-virtual {v0, v1}, Lcom/itau/empresas/api/model/FiltroAgenciaVO;->setFiltroEmpresas(Ljava/lang/String;)V

    .line 152
    iget-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaActivity;->filtro:Lcom/itau/empresas/api/model/FiltroAgenciaVO;

    const-string v1, "true"

    invoke-virtual {v0, v1}, Lcom/itau/empresas/api/model/FiltroAgenciaVO;->setFiltroPers(Ljava/lang/String;)V

    .line 155
    :cond_67
    iget-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaActivity;->controller:Lcom/itau/empresas/controller/MapasController;

    iget-object v1, p0, Lcom/itau/empresas/feature/mapa/MapaActivity;->filtro:Lcom/itau/empresas/api/model/FiltroAgenciaVO;

    invoke-virtual {v0, v1}, Lcom/itau/empresas/controller/MapasController;->buscaAgencias(Lcom/itau/empresas/api/model/FiltroAgenciaVO;)V

    .line 157
    return-void
.end method

.method public static intent(Landroid/support/v4/app/Fragment;)Lcom/itau/empresas/feature/mapa/MapaActivity_$IntentBuilder_;
    .registers 2
    .param p0, "fragment"    # Landroid/support/v4/app/Fragment;

    .line 228
    new-instance v0, Lcom/itau/empresas/feature/mapa/MapaActivity_$IntentBuilder_;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/mapa/MapaActivity_$IntentBuilder_;-><init>(Landroid/support/v4/app/Fragment;)V

    return-object v0
.end method


# virtual methods
.method afterViews()V
    .registers 4

    .line 61
    new-instance v0, Lcom/itau/empresas/ui/util/SnackbarUtils$GerenciadorDelegate;

    iget-object v1, p0, Lcom/itau/empresas/feature/mapa/MapaActivity;->root:Landroid/widget/RelativeLayout;

    invoke-direct {v0, v1}, Lcom/itau/empresas/ui/util/SnackbarUtils$GerenciadorDelegate;-><init>(Landroid/view/ViewGroup;)V

    iput-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaActivity;->topSnackbar:Lcom/itau/empresas/ui/util/SnackbarUtils$Gerenciador;

    .line 62
    new-instance v0, Lcom/itau/empresas/feature/mapa/MapaAdapter;

    invoke-virtual {p0}, Lcom/itau/empresas/feature/mapa/MapaActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Lcom/itau/empresas/feature/mapa/MapaAdapter;-><init>(Landroid/support/v4/app/FragmentManager;Landroid/app/Activity;)V

    iput-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaActivity;->tabsAdapter:Lcom/itau/empresas/feature/mapa/MapaAdapter;

    .line 63
    iget-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaActivity;->pagerMap:Landroid/support/v4/view/ViewPager;

    iget-object v1, p0, Lcom/itau/empresas/feature/mapa/MapaActivity;->tabsAdapter:Lcom/itau/empresas/feature/mapa/MapaAdapter;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 64
    iget-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaActivity;->tabsMap:Landroid/support/design/widget/TabLayout;

    iget-object v1, p0, Lcom/itau/empresas/feature/mapa/MapaActivity;->pagerMap:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, v1}, Landroid/support/design/widget/TabLayout;->setupWithViewPager(Landroid/support/v4/view/ViewPager;)V

    .line 65
    invoke-direct {p0}, Lcom/itau/empresas/feature/mapa/MapaActivity;->constroiToolbar()V

    .line 66
    iget-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaActivity;->filtro:Lcom/itau/empresas/api/model/FiltroAgenciaVO;

    invoke-direct {p0, v0}, Lcom/itau/empresas/feature/mapa/MapaActivity;->atualizaSwitchs(Lcom/itau/empresas/api/model/FiltroAgenciaVO;)V

    .line 68
    iget-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaActivity;->pagerMap:Landroid/support/v4/view/ViewPager;

    new-instance v1, Lcom/itau/empresas/feature/mapa/MapaActivity$1;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/mapa/MapaActivity$1;-><init>(Lcom/itau/empresas/feature/mapa/MapaActivity;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->addOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    .line 87
    iget-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaActivity;->drawerLayoutFilter:Landroid/support/v4/widget/DrawerLayout;

    new-instance v1, Lcom/itau/empresas/feature/mapa/MapaActivity$DrawerListener;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/itau/empresas/feature/mapa/MapaActivity$DrawerListener;-><init>(Lcom/itau/empresas/feature/mapa/MapaActivity;Lcom/itau/empresas/feature/mapa/MapaActivity$1;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/DrawerLayout;->setDrawerListener(Landroid/support/v4/widget/DrawerLayout$DrawerListener;)V

    .line 88
    return-void
.end method

.method public atualizaMapa(Lcom/google/android/gms/maps/model/LatLng;)V
    .registers 5
    .param p1, "lastPosition"    # Lcom/google/android/gms/maps/model/LatLng;

    .line 176
    if-eqz p1, :cond_1b

    .line 177
    iget-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaActivity;->filtro:Lcom/itau/empresas/api/model/FiltroAgenciaVO;

    iget-wide v1, p1, Lcom/google/android/gms/maps/model/LatLng;->latitude:D

    invoke-static {v1, v2}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/api/model/FiltroAgenciaVO;->setLati(Ljava/lang/String;)V

    .line 178
    iget-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaActivity;->filtro:Lcom/itau/empresas/api/model/FiltroAgenciaVO;

    iget-wide v1, p1, Lcom/google/android/gms/maps/model/LatLng;->longitude:D

    invoke-static {v1, v2}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/api/model/FiltroAgenciaVO;->setLongi(Ljava/lang/String;)V

    .line 179
    invoke-direct {p0}, Lcom/itau/empresas/feature/mapa/MapaActivity;->getAgencias()V

    .line 182
    :cond_1b
    return-void
.end method

.method public dismissSnackbar()V
    .registers 2

    .line 194
    iget-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaActivity;->topSnackbar:Lcom/itau/empresas/ui/util/SnackbarUtils$Gerenciador;

    invoke-interface {v0}, Lcom/itau/empresas/ui/util/SnackbarUtils$Gerenciador;->dismissSnackbar()V

    .line 195
    return-void
.end method

.method filtraCaixaEletronico(Z)V
    .registers 4
    .param p1, "isChecked"    # Z

    .line 116
    if-eqz p1, :cond_13

    .line 117
    const v0, 0x7f0702b0

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/mapa/MapaActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f07015e

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/mapa/MapaActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/itau/empresas/feature/mapa/MapaActivity;->analyticsHit(Ljava/lang/String;Ljava/lang/String;)V

    .line 119
    :cond_13
    iget-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaActivity;->filtro:Lcom/itau/empresas/api/model/FiltroAgenciaVO;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/api/model/FiltroAgenciaVO;->setFiltroCaixasExc(Ljava/lang/String;)V

    .line 120
    return-void
.end method

.method filtraEmpresas(Z)V
    .registers 4
    .param p1, "isChecked"    # Z

    .line 108
    if-eqz p1, :cond_13

    .line 109
    const v0, 0x7f0702b0

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/mapa/MapaActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f0701e9

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/mapa/MapaActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/itau/empresas/feature/mapa/MapaActivity;->analyticsHit(Ljava/lang/String;Ljava/lang/String;)V

    .line 111
    :cond_13
    iget-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaActivity;->filtro:Lcom/itau/empresas/api/model/FiltroAgenciaVO;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/api/model/FiltroAgenciaVO;->setFiltroEmpresas(Ljava/lang/String;)V

    .line 112
    return-void
.end method

.method filtraItau(Z)V
    .registers 4
    .param p1, "isChecked"    # Z

    .line 92
    if-eqz p1, :cond_13

    .line 93
    const v0, 0x7f0702b0

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/mapa/MapaActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f070378

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/mapa/MapaActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/itau/empresas/feature/mapa/MapaActivity;->analyticsHit(Ljava/lang/String;Ljava/lang/String;)V

    .line 95
    :cond_13
    iget-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaActivity;->filtro:Lcom/itau/empresas/api/model/FiltroAgenciaVO;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/api/model/FiltroAgenciaVO;->setFiltroItau(Ljava/lang/String;)V

    .line 96
    return-void
.end method

.method filtraPerson(Z)V
    .registers 4
    .param p1, "isChecked"    # Z

    .line 100
    if-eqz p1, :cond_13

    .line 101
    const v0, 0x7f0702b0

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/mapa/MapaActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f0704b3

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/mapa/MapaActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/itau/empresas/feature/mapa/MapaActivity;->analyticsHit(Ljava/lang/String;Ljava/lang/String;)V

    .line 103
    :cond_13
    iget-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaActivity;->filtro:Lcom/itau/empresas/api/model/FiltroAgenciaVO;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/api/model/FiltroAgenciaVO;->setFiltroPers(Ljava/lang/String;)V

    .line 104
    return-void
.end method

.method public hitAnalytics()Ljava/lang/String;
    .registers 2

    .line 185
    const v0, 0x7f0702e0

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/mapa/MapaActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public onEventMainThread(Lcom/itau/empresas/Evento$ErroInesperadoApi;)V
    .registers 4
    .param p1, "erroInesperadoApi"    # Lcom/itau/empresas/Evento$ErroInesperadoApi;

    .line 160
    invoke-virtual {p0}, Lcom/itau/empresas/feature/mapa/MapaActivity;->escondeDialogoDeProgresso()V

    .line 161
    const v0, 0x7f0701f0

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/mapa/MapaActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/itau/empresas/feature/mapa/MapaActivity;->showSnackbar(Ljava/lang/CharSequence;I)V

    .line 163
    return-void
.end method

.method public onEventMainThread(Ljava/lang/Exception;)V
    .registers 4
    .param p1, "ex"    # Ljava/lang/Exception;

    .line 166
    invoke-virtual {p0}, Lcom/itau/empresas/feature/mapa/MapaActivity;->escondeDialogoDeProgresso()V

    .line 167
    const v0, 0x7f0701f0

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/mapa/MapaActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/itau/empresas/feature/mapa/MapaActivity;->showSnackbar(Ljava/lang/CharSequence;I)V

    .line 169
    return-void
.end method

.method public opkeysConsumidas()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Ljava/lang/String;>;"
        }
    .end annotation

    .line 190
    const-string v0, "GetAgencias"

    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public setLatLong(Lcom/google/android/gms/maps/model/LatLng;)V
    .registers 2
    .param p1, "latLong"    # Lcom/google/android/gms/maps/model/LatLng;

    .line 172
    iput-object p1, p0, Lcom/itau/empresas/feature/mapa/MapaActivity;->lastPosition:Lcom/google/android/gms/maps/model/LatLng;

    .line 173
    return-void
.end method

.method public showSnackbar(Ljava/lang/CharSequence;I)V
    .registers 4
    .param p1, "texto"    # Ljava/lang/CharSequence;
    .param p2, "duracao"    # I

    .line 198
    iget-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaActivity;->topSnackbar:Lcom/itau/empresas/ui/util/SnackbarUtils$Gerenciador;

    invoke-interface {v0, p1, p2}, Lcom/itau/empresas/ui/util/SnackbarUtils$Gerenciador;->showSnackbar(Ljava/lang/CharSequence;I)V

    .line 199
    return-void
.end method

.class public Lbr/com/itau/textovoz/controller/SearchController;
.super Ljava/lang/Object;
.source "SearchController.java"


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public doGetSearch(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZZLjava/lang/String;Ljava/lang/String;ZZ)V
    .registers 35
    .param p1, "searchTerm"    # Ljava/lang/String;
    .param p2, "agency"    # Ljava/lang/String;
    .param p3, "account"    # Ljava/lang/String;
    .param p4, "digitChecker"    # Ljava/lang/String;
    .param p5, "operator"    # Ljava/lang/String;
    .param p6, "opkKey"    # Ljava/lang/String;
    .param p7, "versionOS"    # Ljava/lang/String;
    .param p8, "plataform"    # Ljava/lang/String;
    .param p9, "model"    # Ljava/lang/String;
    .param p10, "flagVoice"    # Z
    .param p11, "flagMenu"    # Z
    .param p12, "flagFaq"    # Z
    .param p13, "flagCard"    # Ljava/lang/String;
    .param p14, "flagTrans"    # Ljava/lang/String;
    .param p15, "flagOverflow"    # Z
    .param p16, "flagReceipts"    # Z

    .line 18
    new-instance v0, Lbr/com/itau/textovoz/controller/SearchController$1;

    move-object/from16 v1, p0

    move-object/from16 v2, p6

    move-object/from16 v3, p1

    move-object/from16 v4, p2

    move-object/from16 v5, p3

    move-object/from16 v6, p4

    move-object/from16 v7, p5

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move/from16 v11, p10

    move/from16 v12, p11

    move/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    move/from16 v16, p15

    move/from16 v17, p16

    invoke-direct/range {v0 .. v17}, Lbr/com/itau/textovoz/controller/SearchController$1;-><init>(Lbr/com/itau/textovoz/controller/SearchController;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZZLjava/lang/String;Ljava/lang/String;ZZ)V

    invoke-static {v0}, Lbr/com/itau/sdk/android/core/BackendClient;->callApi(Lbr/com/itau/sdk/android/core/type/BackendCall;)V

    .line 27
    return-void
.end method

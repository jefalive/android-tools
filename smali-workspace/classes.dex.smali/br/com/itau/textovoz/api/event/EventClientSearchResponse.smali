.class public Lbr/com/itau/textovoz/api/event/EventClientSearchResponse;
.super Ljava/lang/Object;
.source "EventClientSearchResponse.java"


# instance fields
.field private final clientSearchResponse:Lbr/com/itau/textovoz/model/response/SearchResponse;


# direct methods
.method public constructor <init>(Lbr/com/itau/textovoz/model/response/SearchResponse;)V
    .registers 2
    .param p1, "clientPushInstallationReponse"    # Lbr/com/itau/textovoz/model/response/SearchResponse;

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    iput-object p1, p0, Lbr/com/itau/textovoz/api/event/EventClientSearchResponse;->clientSearchResponse:Lbr/com/itau/textovoz/model/response/SearchResponse;

    .line 11
    return-void
.end method


# virtual methods
.method public getClientAttendanceResponse()Lbr/com/itau/textovoz/model/response/SearchResponse;
    .registers 2

    .line 14
    iget-object v0, p0, Lbr/com/itau/textovoz/api/event/EventClientSearchResponse;->clientSearchResponse:Lbr/com/itau/textovoz/model/response/SearchResponse;

    return-object v0
.end method

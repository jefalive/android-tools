.class public final Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/DetalhesAutorizacaoActivity_;
.super Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/DetalhesAutorizacaoActivity;
.source "DetalhesAutorizacaoActivity_.java"

# interfaces
.implements Lorg/androidannotations/api/view/HasViews;
.implements Lorg/androidannotations/api/view/OnViewChangedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/DetalhesAutorizacaoActivity_$IntentBuilder_;
    }
.end annotation


# instance fields
.field private final onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;


# direct methods
.method public constructor <init>()V
    .registers 2

    .line 37
    invoke-direct {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/DetalhesAutorizacaoActivity;-><init>()V

    .line 41
    new-instance v0, Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-direct {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/DetalhesAutorizacaoActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    return-void
.end method

.method static synthetic access$001(Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/DetalhesAutorizacaoActivity_;)V
    .registers 1
    .param p0, "x0"    # Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/DetalhesAutorizacaoActivity_;

    .line 37
    invoke-super {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/DetalhesAutorizacaoActivity;->mostraDialogoDeProgresso()V

    return-void
.end method

.method static synthetic access$101(Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/DetalhesAutorizacaoActivity_;)V
    .registers 1
    .param p0, "x0"    # Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/DetalhesAutorizacaoActivity_;

    .line 37
    invoke-super {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/DetalhesAutorizacaoActivity;->escondeDialogoDeProgresso()V

    return-void
.end method

.method private init_(Landroid/os/Bundle;)V
    .registers 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 54
    new-instance v0, Lcom/itau/empresas/Preferences_;

    invoke-direct {v0, p0}, Lcom/itau/empresas/Preferences_;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/DetalhesAutorizacaoActivity_;->prefs:Lcom/itau/empresas/Preferences_;

    .line 55
    invoke-static {p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->registerOnViewChangedListener(Lorg/androidannotations/api/view/OnViewChangedListener;)V

    .line 56
    invoke-static {}, Lcom/itau/empresas/CustomApplication_;->getInstance()Lcom/itau/empresas/CustomApplication;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/DetalhesAutorizacaoActivity_;->application:Lcom/itau/empresas/CustomApplication;

    .line 57
    invoke-static {}, Lcom/itau/empresas/CustomApplication_;->getInstance()Lcom/itau/empresas/CustomApplication;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/DetalhesAutorizacaoActivity_;->app:Lcom/itau/empresas/CustomApplication;

    .line 58
    invoke-static {p0}, Lcom/itau/empresas/controller/PagamentoDarfController_;->getInstance_(Landroid/content/Context;)Lcom/itau/empresas/controller/PagamentoDarfController_;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/DetalhesAutorizacaoActivity_;->pagamentoDarfController:Lcom/itau/empresas/controller/PagamentoDarfController;

    .line 59
    invoke-direct {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/DetalhesAutorizacaoActivity_;->injectExtras_()V

    .line 60
    invoke-virtual {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/DetalhesAutorizacaoActivity_;->afterInject()V

    .line 61
    return-void
.end method

.method private injectExtras_()V
    .registers 3

    .line 120
    invoke-virtual {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/DetalhesAutorizacaoActivity_;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 121
    .local v1, "extras_":Landroid/os/Bundle;
    if-eqz v1, :cond_1c

    .line 122
    const-string v0, "detalhesAutorizacaoVO"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 123
    const-string v0, "detalhesAutorizacaoVO"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/DetalhesAutorizacaoActivity_;->detalhesAutorizacaoVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;

    .line 126
    :cond_1c
    return-void
.end method


# virtual methods
.method public escondeDialogoDeProgresso()V
    .registers 5

    .line 151
    const-string v0, ""

    new-instance v1, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/DetalhesAutorizacaoActivity_$2;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/DetalhesAutorizacaoActivity_$2;-><init>(Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/DetalhesAutorizacaoActivity_;)V

    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3}, Lorg/androidannotations/api/UiThreadExecutor;->runTask(Ljava/lang/String;Ljava/lang/Runnable;J)V

    .line 159
    return-void
.end method

.method public mostraDialogoDeProgresso()V
    .registers 5
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "CommitTransaction"
        }
    .end annotation

    .line 139
    const-string v0, ""

    new-instance v1, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/DetalhesAutorizacaoActivity_$1;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/DetalhesAutorizacaoActivity_$1;-><init>(Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/DetalhesAutorizacaoActivity_;)V

    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3}, Lorg/androidannotations/api/UiThreadExecutor;->runTask(Ljava/lang/String;Ljava/lang/Runnable;J)V

    .line 147
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 46
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/DetalhesAutorizacaoActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-static {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    move-result-object v1

    .line 47
    .local v1, "previousNotifier":Lorg/androidannotations/api/view/OnViewChangedNotifier;
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/DetalhesAutorizacaoActivity_;->init_(Landroid/os/Bundle;)V

    .line 48
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/DetalhesAutorizacaoActivity;->onCreate(Landroid/os/Bundle;)V

    .line 49
    invoke-static {v1}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 50
    const v0, 0x7f03002d

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/DetalhesAutorizacaoActivity_;->setContentView(I)V

    .line 51
    return-void
.end method

.method public onViewChanged(Lorg/androidannotations/api/view/HasViews;)V
    .registers 3
    .param p1, "hasViews"    # Lorg/androidannotations/api/view/HasViews;

    .line 95
    const v0, 0x7f0e0138

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/DetalhesAutorizacaoActivity_;->campoAgenciaConta:Landroid/widget/TextView;

    .line 96
    const v0, 0x7f0e04a4

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/DetalhesAutorizacaoActivity_;->mensagemAlerta:Landroid/widget/TextView;

    .line 97
    const v0, 0x7f0e04a3

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/DetalhesAutorizacaoActivity_;->imagemAlerta:Landroid/widget/TextView;

    .line 98
    const v0, 0x7f0e04a5

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/DetalhesAutorizacaoActivity_;->motivo:Landroid/widget/TextView;

    .line 99
    const v0, 0x7f0e013e

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/DetalhesAutorizacaoActivity_;->campoNomeOperadorInclusao:Landroid/widget/TextView;

    .line 100
    const v0, 0x7f0e0142

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/DetalhesAutorizacaoActivity_;->campoHoraInclusao:Landroid/widget/TextView;

    .line 101
    const v0, 0x7f0e0141

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/DetalhesAutorizacaoActivity_;->campoDataInclusao:Landroid/widget/TextView;

    .line 102
    const v0, 0x7f0e013f

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/DetalhesAutorizacaoActivity_;->campoCpfOperadorInclusao:Landroid/widget/TextView;

    .line 103
    const v0, 0x7f0e0145

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/DetalhesAutorizacaoActivity_;->campoNomeOperadorAutorizador:Landroid/widget/TextView;

    .line 104
    const v0, 0x7f0e0146

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/DetalhesAutorizacaoActivity_;->campoCpfOperadorAutorizador:Landroid/widget/TextView;

    .line 105
    const v0, 0x7f0e0147

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/DetalhesAutorizacaoActivity_;->campoCodigoOperadorAutorizador:Landroid/widget/TextView;

    .line 106
    const v0, 0x7f0e0149

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/DetalhesAutorizacaoActivity_;->campoHoraAutorizacao:Landroid/widget/TextView;

    .line 107
    const v0, 0x7f0e0148

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/DetalhesAutorizacaoActivity_;->campoDataAutorizacao:Landroid/widget/TextView;

    .line 108
    const v0, 0x7f0e013d

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/DetalhesAutorizacaoActivity_;->labelInclusao:Landroid/widget/TextView;

    .line 109
    const v0, 0x7f0e0144

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/DetalhesAutorizacaoActivity_;->labelAutorizacao:Landroid/widget/TextView;

    .line 110
    const v0, 0x7f0e0137

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/DetalhesAutorizacaoActivity_;->campoNomeEmpresa:Landroid/widget/TextView;

    .line 111
    const v0, 0x7f0e0139

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/DetalhesAutorizacaoActivity_;->campoCnpjPagador:Landroid/widget/TextView;

    .line 112
    const v0, 0x7f0e0284

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/DetalhesAutorizacaoActivity_;->rlAlerta:Landroid/widget/RelativeLayout;

    .line 113
    const v0, 0x7f0e0143

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/DetalhesAutorizacaoActivity_;->rlAutorizacaoDetalhe:Landroid/widget/RelativeLayout;

    .line 114
    const v0, 0x7f0e013a

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/DetalhesAutorizacaoActivity_;->llDetalhesPagamento:Landroid/widget/LinearLayout;

    .line 115
    const v0, 0x7f0e00b6

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/Toolbar;

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/DetalhesAutorizacaoActivity_;->toolBar:Landroid/support/v7/widget/Toolbar;

    .line 116
    invoke-virtual {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/DetalhesAutorizacaoActivity_;->updateView()V

    .line 117
    return-void
.end method

.method public setContentView(I)V
    .registers 3
    .param p1, "layoutResID"    # I

    .line 65
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/DetalhesAutorizacaoActivity;->setContentView(I)V

    .line 66
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/DetalhesAutorizacaoActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 67
    return-void
.end method

.method public setContentView(Landroid/view/View;)V
    .registers 3
    .param p1, "view"    # Landroid/view/View;

    .line 77
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/DetalhesAutorizacaoActivity;->setContentView(Landroid/view/View;)V

    .line 78
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/DetalhesAutorizacaoActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 79
    return-void
.end method

.method public setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .registers 4
    .param p1, "view"    # Landroid/view/View;
    .param p2, "params"    # Landroid/view/ViewGroup$LayoutParams;

    .line 71
    invoke-super {p0, p1, p2}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/DetalhesAutorizacaoActivity;->setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 72
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/DetalhesAutorizacaoActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 73
    return-void
.end method

.method public setIntent(Landroid/content/Intent;)V
    .registers 2
    .param p1, "newIntent"    # Landroid/content/Intent;

    .line 130
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/DetalhesAutorizacaoActivity;->setIntent(Landroid/content/Intent;)V

    .line 131
    invoke-direct {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/DetalhesAutorizacaoActivity_;->injectExtras_()V

    .line 132
    return-void
.end method

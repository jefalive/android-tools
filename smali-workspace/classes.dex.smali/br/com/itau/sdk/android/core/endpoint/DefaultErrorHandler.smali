.class public Lbr/com/itau/sdk/android/core/endpoint/DefaultErrorHandler;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lbr/com/itau/sdk/android/core/endpoint/ErrorHandler;


# static fields
.field private static final $:[B

.field private static $$:I


# direct methods
.method private static $(III)Ljava/lang/String;
    .registers 9

    const/4 v5, 0x0

    add-int/lit8 p0, p0, 0x43

    rsub-int/lit8 p2, p2, 0x21

    sget-object v4, Lbr/com/itau/sdk/android/core/endpoint/DefaultErrorHandler;->$:[B

    new-instance v0, Ljava/lang/String;

    add-int/lit8 p1, p1, 0x4

    new-array v1, p2, [B

    add-int/lit8 p2, p2, -0x1

    if-nez v4, :cond_15

    move v2, p1

    move v3, p2

    :goto_13
    add-int p0, v2, v3

    :cond_15
    add-int/lit8 p1, p1, 0x1

    int-to-byte v2, p0

    aput-byte v2, v1, v5

    move v2, v5

    add-int/lit8 v5, v5, 0x1

    if-ne v2, p2, :cond_24

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Ljava/lang/String;-><init>([BI)V

    return-object v0

    :cond_24
    move v2, p0

    aget-byte v3, v4, p1

    goto :goto_13
.end method

.method static constructor <clinit>()V
    .registers 3

    const/16 v0, 0xc2

    new-array v0, v0, [B

    fill-array-data v0, :array_e

    sput-object v0, Lbr/com/itau/sdk/android/core/endpoint/DefaultErrorHandler;->$:[B

    const/16 v0, 0x5a

    sput v0, Lbr/com/itau/sdk/android/core/endpoint/DefaultErrorHandler;->$$:I

    return-void

    :array_e
    .array-data 1
        0x34t
        -0x24t
        -0x4dt
        -0x2bt
        0xct
        0x0t
        -0x4t
        -0x30t
        0x45t
        0x13t
        -0x15t
        0x2t
        0xbt
        0x4t
        -0xbt
        0x6t
        -0x1t
        -0x4et
        0x5t
        0x4et
        0x16t
        0x4t
        0x8t
        -0x9t
        0x8t
        -0x13t
        0xdt
        -0x9t
        0xat
        0x6t
        -0x2t
        -0x53t
        0x53t
        -0xet
        0xet
        0x0t
        -0xat
        0x6t
        -0x1t
        -0x4et
        0x45t
        0x13t
        -0x15t
        0x2t
        0xbt
        0x4t
        -0xbt
        0x6t
        -0x1t
        -0x4et
        0x5t
        0x4et
        0x19t
        -0x9t
        0x13t
        -0x8t
        -0xbt
        -0x2t
        0x11t
        -0xft
        -0x1t
        -0x44t
        0x43t
        -0x2t
        0x14t
        -0x2t
        -0xet
        0x12t
        0xet
        0x0t
        -0xat
        0x6t
        -0x1t
        -0x4et
        0x45t
        0x13t
        -0x15t
        0x2t
        0xbt
        0x4t
        -0xbt
        0x6t
        -0x1t
        -0x4et
        0x5t
        0x4et
        0x2ct
        -0x1t
        0x8t
        -0x11t
        0xdt
        0x1t
        -0xat
        0x6t
        -0x1t
        -0x4et
        0x45t
        0x13t
        -0x15t
        0x2t
        0xbt
        0x4t
        -0xbt
        0x6t
        -0x1t
        -0x4et
        0x5t
        0x4et
        0x1dt
        0x6t
        -0x1t
        -0xft
        0xdt
        -0x52t
        0x45t
        0x13t
        -0x15t
        0x2t
        0xbt
        0x4t
        -0xbt
        0x6t
        -0x1t
        -0x4et
        0x5t
        0x4et
        0x17t
        0xft
        0x3t
        -0x8t
        0x3t
        -0x7t
        -0x4bt
        0x45t
        0x13t
        -0x15t
        0x2t
        0xbt
        0x4t
        -0xbt
        0x6t
        -0x1t
        -0x4et
        0x5t
        0x4et
        0x19t
        -0x9t
        0x13t
        -0x8t
        -0xbt
        -0x2t
        0x11t
        -0xft
        -0x1t
        -0x44t
        0x45t
        0x13t
        -0x15t
        0x2t
        0xbt
        0x4t
        -0xbt
        0x6t
        -0x1t
        -0x4et
        0x5t
        0x4et
        0xft
        0xdt
        0x1t
        -0xat
        0x6t
        -0x1t
        -0x4et
        0x43t
        0xct
        -0x1t
        0x6t
        -0x2t
        -0x3t
        -0x3t
        -0x4ct
        0x45t
        0x13t
        -0x15t
        0x2t
        0xbt
        0x4t
        -0xbt
        0x6t
        -0x1t
        -0x4et
        0x5t
        0x4et
    .end array-data
.end method

.method public constructor <init>()V
    .registers 1

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public handleConversionException(Lbr/com/itau/sdk/android/core/exception/BackendException;)Z
    .registers 6

    .line 65
    sget-object v0, Lbr/com/itau/sdk/android/core/endpoint/DefaultErrorHandler;->$:[B

    const/4 v1, 0x5

    aget-byte v0, v0, v1

    or-int/lit8 v1, v0, 0x51

    sget-object v2, Lbr/com/itau/sdk/android/core/endpoint/DefaultErrorHandler;->$:[B

    const/16 v3, 0x1c

    aget-byte v2, v2, v3

    invoke-static {v0, v1, v2}, Lbr/com/itau/sdk/android/core/endpoint/DefaultErrorHandler;->$(III)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {p0, p1, v0, v1}, Lbr/com/itau/sdk/android/core/l;->a(Ljava/lang/Object;Ljava/lang/Exception;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 66
    const/4 v0, 0x0

    return v0
.end method

.method public handleError(Lbr/com/itau/sdk/android/core/exception/BackendException;)Ljava/lang/Throwable;
    .registers 6

    .line 13
    sget-object v0, Lbr/com/itau/sdk/android/core/endpoint/g;->a:[I

    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/exception/BackendException;->getKind()Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;

    move-result-object v1

    invoke-virtual {v1}, Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_4e

    goto/16 :goto_35

    .line 16
    :pswitch_11
    invoke-virtual {p0, p1}, Lbr/com/itau/sdk/android/core/endpoint/DefaultErrorHandler;->handleNetworkException(Lbr/com/itau/sdk/android/core/exception/BackendException;)Z

    .line 17
    goto :goto_4d

    .line 19
    :pswitch_15
    invoke-virtual {p0, p1}, Lbr/com/itau/sdk/android/core/endpoint/DefaultErrorHandler;->handleConversionException(Lbr/com/itau/sdk/android/core/exception/BackendException;)Z

    .line 20
    goto :goto_4d

    .line 22
    :pswitch_19
    invoke-virtual {p0, p1}, Lbr/com/itau/sdk/android/core/endpoint/DefaultErrorHandler;->handleHttpException(Lbr/com/itau/sdk/android/core/exception/BackendException;)Z

    .line 23
    goto :goto_4d

    .line 25
    :pswitch_1d
    invoke-virtual {p0, p1}, Lbr/com/itau/sdk/android/core/endpoint/DefaultErrorHandler;->handleUnexpected(Lbr/com/itau/sdk/android/core/exception/BackendException;)Z

    .line 26
    goto :goto_4d

    .line 28
    :pswitch_21
    invoke-virtual {p0, p1}, Lbr/com/itau/sdk/android/core/endpoint/DefaultErrorHandler;->handleRouter(Lbr/com/itau/sdk/android/core/exception/BackendException;)Z

    .line 29
    goto :goto_4d

    .line 31
    :pswitch_25
    invoke-virtual {p0, p1}, Lbr/com/itau/sdk/android/core/endpoint/DefaultErrorHandler;->handleToken(Lbr/com/itau/sdk/android/core/exception/BackendException;)Z

    .line 32
    goto :goto_4d

    .line 34
    :pswitch_29
    invoke-virtual {p0, p1}, Lbr/com/itau/sdk/android/core/endpoint/DefaultErrorHandler;->handleSession(Lbr/com/itau/sdk/android/core/exception/BackendException;)Z

    .line 35
    goto :goto_4d

    .line 37
    :pswitch_2d
    invoke-virtual {p0, p1}, Lbr/com/itau/sdk/android/core/endpoint/DefaultErrorHandler;->handleSimultaneousSession(Lbr/com/itau/sdk/android/core/exception/BackendException;)Z

    .line 38
    goto :goto_4d

    .line 40
    :pswitch_31
    invoke-virtual {p0, p1}, Lbr/com/itau/sdk/android/core/endpoint/DefaultErrorHandler;->handleVersionControl(Lbr/com/itau/sdk/android/core/exception/BackendException;)Z

    .line 41
    goto :goto_4d

    .line 43
    :goto_35
    new-instance v0, Ljava/lang/RuntimeException;

    sget-object v1, Lbr/com/itau/sdk/android/core/endpoint/DefaultErrorHandler;->$:[B

    const/16 v2, 0x43

    aget-byte v1, v1, v2

    sget-object v2, Lbr/com/itau/sdk/android/core/endpoint/DefaultErrorHandler;->$:[B

    const/16 v3, 0x3a

    aget-byte v2, v2, v3

    const/16 v3, 0x2f

    invoke-static {v1, v3, v2}, Lbr/com/itau/sdk/android/core/endpoint/DefaultErrorHandler;->$(III)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 46
    :goto_4d
    return-object p1

    :pswitch_data_4e
    .packed-switch 0x1
        :pswitch_11
        :pswitch_15
        :pswitch_19
        :pswitch_1d
        :pswitch_21
        :pswitch_25
        :pswitch_29
        :pswitch_2d
        :pswitch_31
    .end packed-switch
.end method

.method public handleHttpException(Lbr/com/itau/sdk/android/core/exception/BackendException;)Z
    .registers 5

    .line 70
    sget-object v0, Lbr/com/itau/sdk/android/core/endpoint/DefaultErrorHandler;->$:[B

    const/16 v1, 0x12

    aget-byte v0, v0, v1

    sget-object v1, Lbr/com/itau/sdk/android/core/endpoint/DefaultErrorHandler;->$:[B

    const/16 v2, 0x10

    aget-byte v1, v1, v2

    and-int/lit8 v2, v1, 0x10

    invoke-static {v0, v1, v2}, Lbr/com/itau/sdk/android/core/endpoint/DefaultErrorHandler;->$(III)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {p0, p1, v0, v1}, Lbr/com/itau/sdk/android/core/l;->a(Ljava/lang/Object;Ljava/lang/Exception;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 71
    const/4 v0, 0x0

    return v0
.end method

.method public handleNetworkException(Lbr/com/itau/sdk/android/core/exception/BackendException;)Z
    .registers 5

    .line 60
    sget-object v0, Lbr/com/itau/sdk/android/core/endpoint/DefaultErrorHandler;->$:[B

    const/16 v1, 0xc

    aget-byte v0, v0, v1

    sget-object v1, Lbr/com/itau/sdk/android/core/endpoint/DefaultErrorHandler;->$:[B

    const/16 v2, 0x1a

    aget-byte v1, v1, v2

    const/16 v2, 0x79

    invoke-static {v0, v2, v1}, Lbr/com/itau/sdk/android/core/endpoint/DefaultErrorHandler;->$(III)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {p0, p1, v0, v1}, Lbr/com/itau/sdk/android/core/l;->a(Ljava/lang/Object;Ljava/lang/Exception;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 61
    const/4 v0, 0x0

    return v0
.end method

.method public handleRouter(Lbr/com/itau/sdk/android/core/exception/BackendException;)Z
    .registers 5

    .line 55
    sget-object v0, Lbr/com/itau/sdk/android/core/endpoint/DefaultErrorHandler;->$:[B

    const/16 v1, 0x7f

    aget-byte v0, v0, v1

    sget-object v1, Lbr/com/itau/sdk/android/core/endpoint/DefaultErrorHandler;->$:[B

    const/16 v2, 0x22

    aget-byte v1, v1, v2

    const/16 v2, 0x67

    invoke-static {v0, v2, v1}, Lbr/com/itau/sdk/android/core/endpoint/DefaultErrorHandler;->$(III)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {p0, p1, v0, v1}, Lbr/com/itau/sdk/android/core/l;->a(Ljava/lang/Object;Ljava/lang/Exception;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 56
    const/4 v0, 0x0

    return v0
.end method

.method public handleSession(Lbr/com/itau/sdk/android/core/exception/BackendException;)Z
    .registers 6

    .line 80
    sget v0, Lbr/com/itau/sdk/android/core/endpoint/DefaultErrorHandler;->$$:I

    and-int/lit8 v0, v0, 0x34

    or-int/lit8 v1, v0, 0x2e

    sget-object v2, Lbr/com/itau/sdk/android/core/endpoint/DefaultErrorHandler;->$:[B

    const/16 v3, 0x1a

    aget-byte v2, v2, v3

    invoke-static {v0, v1, v2}, Lbr/com/itau/sdk/android/core/endpoint/DefaultErrorHandler;->$(III)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {p0, p1, v0, v1}, Lbr/com/itau/sdk/android/core/l;->a(Ljava/lang/Object;Ljava/lang/Exception;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 81
    const/4 v0, 0x0

    return v0
.end method

.method public handleSimultaneousSession(Lbr/com/itau/sdk/android/core/exception/BackendException;)Z
    .registers 6

    .line 85
    sget v0, Lbr/com/itau/sdk/android/core/endpoint/DefaultErrorHandler;->$$:I

    and-int/lit8 v0, v0, 0x34

    sget-object v1, Lbr/com/itau/sdk/android/core/endpoint/DefaultErrorHandler;->$:[B

    const/16 v2, 0x7f

    aget-byte v1, v1, v2

    sget-object v2, Lbr/com/itau/sdk/android/core/endpoint/DefaultErrorHandler;->$:[B

    const/4 v3, 0x5

    aget-byte v2, v2, v3

    invoke-static {v0, v1, v2}, Lbr/com/itau/sdk/android/core/endpoint/DefaultErrorHandler;->$(III)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {p0, p1, v0, v1}, Lbr/com/itau/sdk/android/core/l;->a(Ljava/lang/Object;Ljava/lang/Exception;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 86
    const/4 v0, 0x0

    return v0
.end method

.method public handleToken(Lbr/com/itau/sdk/android/core/exception/BackendException;)Z
    .registers 5

    .line 50
    sget-object v0, Lbr/com/itau/sdk/android/core/endpoint/DefaultErrorHandler;->$:[B

    const/16 v1, 0x7f

    aget-byte v0, v0, v1

    sget-object v1, Lbr/com/itau/sdk/android/core/endpoint/DefaultErrorHandler;->$:[B

    const/16 v2, 0x22

    aget-byte v1, v1, v2

    const/16 v2, 0x67

    invoke-static {v0, v2, v1}, Lbr/com/itau/sdk/android/core/endpoint/DefaultErrorHandler;->$(III)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {p0, p1, v0, v1}, Lbr/com/itau/sdk/android/core/l;->a(Ljava/lang/Object;Ljava/lang/Exception;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 51
    const/4 v0, 0x0

    return v0
.end method

.method public handleUnexpected(Lbr/com/itau/sdk/android/core/exception/BackendException;)Z
    .registers 5

    .line 75
    sget-object v0, Lbr/com/itau/sdk/android/core/endpoint/DefaultErrorHandler;->$:[B

    const/16 v1, 0x43

    aget-byte v0, v0, v1

    sget-object v1, Lbr/com/itau/sdk/android/core/endpoint/DefaultErrorHandler;->$:[B

    const/16 v2, 0x1c

    aget-byte v1, v1, v2

    const/16 v2, 0x8c

    invoke-static {v0, v2, v1}, Lbr/com/itau/sdk/android/core/endpoint/DefaultErrorHandler;->$(III)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {p0, p1, v0, v1}, Lbr/com/itau/sdk/android/core/l;->a(Ljava/lang/Object;Ljava/lang/Exception;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 76
    const/4 v0, 0x0

    return v0
.end method

.method public handleVersionControl(Lbr/com/itau/sdk/android/core/exception/BackendException;)Z
    .registers 5

    .line 90
    sget-object v0, Lbr/com/itau/sdk/android/core/endpoint/DefaultErrorHandler;->$:[B

    const/16 v1, 0x9

    aget-byte v0, v0, v1

    sget-object v1, Lbr/com/itau/sdk/android/core/endpoint/DefaultErrorHandler;->$:[B

    const/16 v2, 0x12

    aget-byte v1, v1, v2

    const/16 v2, 0xa2

    invoke-static {v0, v2, v1}, Lbr/com/itau/sdk/android/core/endpoint/DefaultErrorHandler;->$(III)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {p0, p1, v0, v1}, Lbr/com/itau/sdk/android/core/l;->a(Ljava/lang/Object;Ljava/lang/Exception;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 91
    const/4 v0, 0x0

    return v0
.end method

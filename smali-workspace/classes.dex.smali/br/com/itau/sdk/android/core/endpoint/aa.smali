.class public final Lbr/com/itau/sdk/android/core/endpoint/aa;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:[B

.field private static b:I


# direct methods
.method static constructor <clinit>()V
    .registers 3

    const/16 v0, 0x6b

    new-array v0, v0, [B

    fill-array-data v0, :array_e

    sput-object v0, Lbr/com/itau/sdk/android/core/endpoint/aa;->a:[B

    const/16 v0, 0xc2

    sput v0, Lbr/com/itau/sdk/android/core/endpoint/aa;->b:I

    return-void

    :array_e
    .array-data 1
        0x44t
        -0x26t
        -0x39t
        -0x6et
        0x22t
        0x1t
        0x10t
        -0x56t
        0x4ct
        0x8t
        0x9t
        -0xct
        0x10t
        -0x9t
        -0x2t
        0x5t
        0x5t
        -0x42t
        0x48t
        0xct
        -0x7t
        0xft
        0x2t
        -0x3t
        0x8t
        0x9t
        -0x51t
        0x47t
        0x4t
        0x4t
        0x6t
        0x8t
        -0x2t
        0xet
        -0x8t
        0x9t
        0x2t
        0x8t
        -0x50t
        0x44t
        0x14t
        -0xat
        -0x42t
        0x56t
        0x5t
        -0x2t
        0x3t
        0x2t
        0x6t
        0x5t
        -0xct
        0x2t
        -0x33t
        0x28t
        0x9t
        -0xct
        0x10t
        -0x9t
        -0x2t
        0x5t
        0x5t
        -0x42t
        0x47t
        0x4t
        0x4t
        0x6t
        0x8t
        -0x2t
        0xet
        -0x8t
        0x9t
        0x2t
        0x8t
        -0x50t
        0x50t
        0xbt
        0x1t
        0x4t
        -0x51t
        0x51t
        0x4t
        0x8t
        -0x51t
        0x48t
        0x16t
        -0x1t
        -0xct
        0xct
        -0x7t
        -0x41t
        0x52t
        0x8t
        -0x9t
        0x0t
        0x10t
        -0x4ft
        0x4ct
        0x8t
        0x9t
        -0xct
        0x10t
        -0x9t
        -0x2t
        0x5t
        0x5t
        0x11t
        -0x42t
    .end array-data
.end method

.method static a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;
    .registers 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:Ljava/lang/Object;>(TT;Ljava/lang/String;)TT;"
        }
    .end annotation

    .line 17
    if-nez p0, :cond_8

    .line 18
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0, p1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 20
    :cond_8
    return-object p0
.end method

.method private static a(III)Ljava/lang/String;
    .registers 9

    sget-object v4, Lbr/com/itau/sdk/android/core/endpoint/aa;->a:[B

    mul-int/lit8 p0, p0, 0x6

    rsub-int/lit8 p0, p0, 0x4f

    mul-int/lit8 p2, p2, 0x31

    add-int/lit8 p2, p2, 0x4

    const/4 v5, 0x0

    mul-int/lit8 p1, p1, 0x5

    add-int/lit8 p1, p1, 0x32

    new-instance v0, Ljava/lang/String;

    new-array v1, p1, [B

    if-nez v4, :cond_1c

    move v2, p2

    move v3, p0

    :goto_17
    add-int/2addr v2, v3

    add-int/lit8 p0, v2, -0x3

    add-int/lit8 p2, p2, 0x1

    :cond_1c
    move v2, v5

    add-int/lit8 v5, v5, 0x1

    int-to-byte v3, p0

    aput-byte v3, v1, v2

    if-ne v5, p1, :cond_29

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Ljava/lang/String;-><init>([BI)V

    return-object v0

    :cond_29
    move v2, p0

    aget-byte v3, v4, p2

    goto :goto_17
.end method

.method static a(Ljava/lang/Class;)V
    .registers 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:Ljava/lang/Object;>(Ljava/lang/Class<TT;>;)V"
        }
    .end annotation

    .line 43
    invoke-virtual {p0}, Ljava/lang/Class;->isInterface()Z

    move-result v0

    if-nez v0, :cond_16

    .line 44
    new-instance v0, Ljava/lang/IllegalArgumentException;

    sget-object v1, Lbr/com/itau/sdk/android/core/endpoint/aa;->a:[B

    const/16 v2, 0x5d

    aget-byte v1, v1, v2

    invoke-static {v1, v1, v1}, Lbr/com/itau/sdk/android/core/endpoint/aa;->a(III)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 49
    :cond_16
    invoke-virtual {p0}, Ljava/lang/Class;->getInterfaces()[Ljava/lang/Class;

    move-result-object v0

    array-length v0, v0

    if-lez v0, :cond_2c

    .line 50
    new-instance v0, Ljava/lang/IllegalArgumentException;

    sget-object v1, Lbr/com/itau/sdk/android/core/endpoint/aa;->a:[B

    const/4 v2, 0x5

    aget-byte v1, v1, v2

    invoke-static {v1, v1, v1}, Lbr/com/itau/sdk/android/core/endpoint/aa;->a(III)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 51
    :cond_2c
    return-void
.end method

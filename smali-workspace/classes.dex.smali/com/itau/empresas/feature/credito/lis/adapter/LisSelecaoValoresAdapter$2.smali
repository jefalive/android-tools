.class Lcom/itau/empresas/feature/credito/lis/adapter/LisSelecaoValoresAdapter$2;
.super Ljava/lang/Object;
.source "LisSelecaoValoresAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/itau/empresas/feature/credito/lis/adapter/LisSelecaoValoresAdapter;->bind(Lcom/itau/empresas/feature/credito/lis/adapter/LisSelecaoValoresAdapter$LisSelecaoValoresViewHolder;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/itau/empresas/feature/credito/lis/adapter/LisSelecaoValoresAdapter;

.field final synthetic val$limiteOfertado:Lcom/itau/empresas/feature/credito/lis/model/LisLimiteOfertadoVO;


# direct methods
.method constructor <init>(Lcom/itau/empresas/feature/credito/lis/adapter/LisSelecaoValoresAdapter;Lcom/itau/empresas/feature/credito/lis/model/LisLimiteOfertadoVO;)V
    .registers 3
    .param p1, "this$0"    # Lcom/itau/empresas/feature/credito/lis/adapter/LisSelecaoValoresAdapter;

    .line 107
    iput-object p1, p0, Lcom/itau/empresas/feature/credito/lis/adapter/LisSelecaoValoresAdapter$2;->this$0:Lcom/itau/empresas/feature/credito/lis/adapter/LisSelecaoValoresAdapter;

    iput-object p2, p0, Lcom/itau/empresas/feature/credito/lis/adapter/LisSelecaoValoresAdapter$2;->val$limiteOfertado:Lcom/itau/empresas/feature/credito/lis/model/LisLimiteOfertadoVO;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .registers 7
    .param p1, "v"    # Landroid/view/View;

    .line 110
    new-instance v4, Ljava/math/BigDecimal;

    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/adapter/LisSelecaoValoresAdapter$2;->val$limiteOfertado:Lcom/itau/empresas/feature/credito/lis/model/LisLimiteOfertadoVO;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/credito/lis/model/LisLimiteOfertadoVO;->getLimiteOfertado()F

    move-result v0

    float-to-double v0, v0

    invoke-direct {v4, v0, v1}, Ljava/math/BigDecimal;-><init>(D)V

    .line 111
    .local v4, "bdLimiteOfertado":Ljava/math/BigDecimal;
    const-string v0, "LisSelecaoAdapter"

    const-string v1, "Valor escolhido: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 112
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/adapter/LisSelecaoValoresAdapter$2;->this$0:Lcom/itau/empresas/feature/credito/lis/adapter/LisSelecaoValoresAdapter;

    .line 113
    # getter for: Lcom/itau/empresas/feature/credito/lis/adapter/LisSelecaoValoresAdapter;->context:Landroid/content/Context;
    invoke-static {v0}, Lcom/itau/empresas/feature/credito/lis/adapter/LisSelecaoValoresAdapter;->access$200(Lcom/itau/empresas/feature/credito/lis/adapter/LisSelecaoValoresAdapter;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/itau/empresas/feature/credito/lis/activity/LisDataSimulacaoActivity;->intent(Landroid/content/Context;)Lcom/itau/empresas/feature/credito/lis/activity/LisDataSimulacaoActivity_$IntentBuilder_;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/lis/adapter/LisSelecaoValoresAdapter$2;->this$0:Lcom/itau/empresas/feature/credito/lis/adapter/LisSelecaoValoresAdapter;

    .line 114
    # getter for: Lcom/itau/empresas/feature/credito/lis/adapter/LisSelecaoValoresAdapter;->ofertaContratacaoLisVo:Lcom/itau/empresas/feature/credito/lis/model/OfertaContratacaoLisVo;
    invoke-static {v1}, Lcom/itau/empresas/feature/credito/lis/adapter/LisSelecaoValoresAdapter;->access$100(Lcom/itau/empresas/feature/credito/lis/adapter/LisSelecaoValoresAdapter;)Lcom/itau/empresas/feature/credito/lis/model/OfertaContratacaoLisVo;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/credito/lis/activity/LisDataSimulacaoActivity_$IntentBuilder_;->ofertaContratacaoLisVo(Lcom/itau/empresas/feature/credito/lis/model/OfertaContratacaoLisVo;)Lcom/itau/empresas/feature/credito/lis/activity/LisDataSimulacaoActivity_$IntentBuilder_;

    move-result-object v0

    .line 115
    invoke-virtual {v0, v4}, Lcom/itau/empresas/feature/credito/lis/activity/LisDataSimulacaoActivity_$IntentBuilder_;->valorSelecionado(Ljava/math/BigDecimal;)Lcom/itau/empresas/feature/credito/lis/activity/LisDataSimulacaoActivity_$IntentBuilder_;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/lis/adapter/LisSelecaoValoresAdapter$2;->this$0:Lcom/itau/empresas/feature/credito/lis/adapter/LisSelecaoValoresAdapter;

    .line 116
    # getter for: Lcom/itau/empresas/feature/credito/lis/adapter/LisSelecaoValoresAdapter;->possuiLisAdicional:Z
    invoke-static {v1}, Lcom/itau/empresas/feature/credito/lis/adapter/LisSelecaoValoresAdapter;->access$000(Lcom/itau/empresas/feature/credito/lis/adapter/LisSelecaoValoresAdapter;)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/credito/lis/activity/LisDataSimulacaoActivity_$IntentBuilder_;->possuiProdutoLisAdicional(Z)Lcom/itau/empresas/feature/credito/lis/activity/LisDataSimulacaoActivity_$IntentBuilder_;

    move-result-object v0

    .line 117
    invoke-virtual {v0}, Lcom/itau/empresas/feature/credito/lis/activity/LisDataSimulacaoActivity_$IntentBuilder_;->start()Lorg/androidannotations/api/builder/PostActivityStarter;

    .line 118
    return-void
.end method

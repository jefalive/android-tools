.class public Lcom/itau/empresas/adapter/ItemPedagioAdapter;
.super Landroid/widget/BaseAdapter;
.source "ItemPedagioAdapter.java"


# instance fields
.field context:Landroid/content/Context;

.field private labels:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Ljava/lang/String;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 2
    .param p1, "context"    # Landroid/content/Context;

    .line 23
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/itau/empresas/adapter/ItemPedagioAdapter;->context:Landroid/content/Context;

    .line 25
    return-void
.end method


# virtual methods
.method public getCount()I
    .registers 2

    .line 38
    iget-object v0, p0, Lcom/itau/empresas/adapter/ItemPedagioAdapter;->labels:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .registers 3

    .line 16
    invoke-virtual {p0, p1}, Lcom/itau/empresas/adapter/ItemPedagioAdapter;->getItem(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getItem(I)Ljava/lang/String;
    .registers 3
    .param p1, "i"    # I

    .line 65
    iget-object v0, p0, Lcom/itau/empresas/adapter/ItemPedagioAdapter;->labels:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getItemId(I)J
    .registers 4
    .param p1, "i"    # I

    .line 33
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 10
    .param p1, "position"    # I
    .param p2, "view"    # Landroid/view/View;
    .param p3, "viewGroup"    # Landroid/view/ViewGroup;

    .line 43
    if-nez p2, :cond_10

    .line 44
    iget-object v0, p0, Lcom/itau/empresas/adapter/ItemPedagioAdapter;->context:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030118

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 47
    :cond_10
    const v0, 0x7f0e0572

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Landroid/widget/TextView;

    .line 48
    .local v3, "textoItemPopup":Landroid/widget/TextView;
    const v0, 0x7f0e0573

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    .line 50
    .local v4, "linha":Landroid/view/View;
    iget-object v0, p0, Lcom/itau/empresas/adapter/ItemPedagioAdapter;->labels:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 52
    iget-object v0, p0, Lcom/itau/empresas/adapter/ItemPedagioAdapter;->labels:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v5

    .line 53
    .local v5, "tamanho":I
    const/4 v0, 0x1

    if-le v5, v0, :cond_3e

    add-int/lit8 v0, v5, -0x1

    if-eq p1, v0, :cond_3e

    .line 54
    const/4 v0, 0x0

    invoke-virtual {v4, v0}, Landroid/view/View;->setVisibility(I)V

    goto :goto_42

    .line 56
    :cond_3e
    const/4 v0, 0x4

    invoke-virtual {v4, v0}, Landroid/view/View;->setVisibility(I)V

    .line 59
    :goto_42
    return-object p2
.end method

.method public inicializar(Ljava/util/List;)V
    .registers 2
    .param p1, "labels"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<Ljava/lang/String;>;)V"
        }
    .end annotation

    .line 28
    iput-object p1, p0, Lcom/itau/empresas/adapter/ItemPedagioAdapter;->labels:Ljava/util/List;

    .line 29
    return-void
.end method

.class public Lcom/itau/empresas/ui/util/listener/ToolbarCustom;
.super Ljava/lang/Object;
.source "ToolbarCustom.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/itau/empresas/ui/util/listener/ToolbarCustom$NavigationListener;,
        Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final activity:Landroid/support/v7/app/AppCompatActivity;

.field private final builder:Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .line 14
    const-class v0, Lcom/itau/empresas/ui/util/listener/ToolbarCustom;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_a

    const/4 v0, 0x1

    goto :goto_b

    :cond_a
    const/4 v0, 0x0

    :goto_b
    sput-boolean v0, Lcom/itau/empresas/ui/util/listener/ToolbarCustom;->$assertionsDisabled:Z

    return-void
.end method

.method private constructor <init>(Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;)V
    .registers 3
    .param p1, "builder"    # Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/itau/empresas/ui/util/listener/ToolbarCustom;->builder:Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    .line 21
    # getter for: Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->activity:Landroid/support/v7/app/AppCompatActivity;
    invoke-static {p1}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->access$000(Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;)Landroid/support/v7/app/AppCompatActivity;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/ui/util/listener/ToolbarCustom;->activity:Landroid/support/v7/app/AppCompatActivity;

    .line 22
    invoke-direct {p0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom;->customizaToolbar()V

    .line 23
    return-void
.end method

.method synthetic constructor <init>(Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;Lcom/itau/empresas/ui/util/listener/ToolbarCustom$1;)V
    .registers 3
    .param p1, "x0"    # Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;
    .param p2, "x1"    # Lcom/itau/empresas/ui/util/listener/ToolbarCustom$1;

    .line 14
    invoke-direct {p0, p1}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom;-><init>(Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;)V

    return-void
.end method

.method public static Builder(Landroid/support/v7/widget/Toolbar;Landroid/support/v7/app/AppCompatActivity;)Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;
    .registers 3
    .param p0, "toolbar"    # Landroid/support/v7/widget/Toolbar;
    .param p1, "activity"    # Landroid/support/v7/app/AppCompatActivity;

    .line 26
    new-instance v0, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    invoke-direct {v0, p0, p1}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;-><init>(Landroid/support/v7/widget/Toolbar;Landroid/support/v7/app/AppCompatActivity;)V

    return-object v0
.end method

.method private customizaToolbar()V
    .registers 3

    .line 30
    iget-object v0, p0, Lcom/itau/empresas/ui/util/listener/ToolbarCustom;->activity:Landroid/support/v7/app/AppCompatActivity;

    iget-object v1, p0, Lcom/itau/empresas/ui/util/listener/ToolbarCustom;->builder:Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    # getter for: Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->toolbar:Landroid/support/v7/widget/Toolbar;
    invoke-static {v1}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->access$100(Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;)Landroid/support/v7/widget/Toolbar;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/AppCompatActivity;->setSupportActionBar(Landroid/support/v7/widget/Toolbar;)V

    .line 31
    sget-boolean v0, Lcom/itau/empresas/ui/util/listener/ToolbarCustom;->$assertionsDisabled:Z

    if-nez v0, :cond_1d

    iget-object v0, p0, Lcom/itau/empresas/ui/util/listener/ToolbarCustom;->activity:Landroid/support/v7/app/AppCompatActivity;

    invoke-virtual {v0}, Landroid/support/v7/app/AppCompatActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    if-nez v0, :cond_1d

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 32
    :cond_1d
    iget-object v0, p0, Lcom/itau/empresas/ui/util/listener/ToolbarCustom;->activity:Landroid/support/v7/app/AppCompatActivity;

    invoke-virtual {v0}, Landroid/support/v7/app/AppCompatActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    .line 33
    iget-object v0, p0, Lcom/itau/empresas/ui/util/listener/ToolbarCustom;->activity:Landroid/support/v7/app/AppCompatActivity;

    invoke-virtual {v0}, Landroid/support/v7/app/AppCompatActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 34
    invoke-direct {p0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom;->setaDrawableFechar()V

    .line 35
    invoke-direct {p0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom;->setaTitulo()V

    .line 36
    invoke-direct {p0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom;->setaContentDescription()V

    .line 37
    invoke-direct {p0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom;->setaClickListener()V

    .line 38
    return-void
.end method

.method private setaClickListener()V
    .registers 5

    .line 59
    iget-object v0, p0, Lcom/itau/empresas/ui/util/listener/ToolbarCustom;->builder:Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    # getter for: Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->isFinishActivity:Z
    invoke-static {v0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->access$500(Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;)Z

    move-result v0

    if-eqz v0, :cond_19

    .line 60
    iget-object v0, p0, Lcom/itau/empresas/ui/util/listener/ToolbarCustom;->builder:Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    # getter for: Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->toolbar:Landroid/support/v7/widget/Toolbar;
    invoke-static {v0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->access$100(Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;)Landroid/support/v7/widget/Toolbar;

    move-result-object v0

    new-instance v1, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$NavigationListener;

    iget-object v2, p0, Lcom/itau/empresas/ui/util/listener/ToolbarCustom;->activity:Landroid/support/v7/app/AppCompatActivity;

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$NavigationListener;-><init>(Landroid/support/v7/app/AppCompatActivity;Lcom/itau/empresas/ui/util/listener/ToolbarCustom$1;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->setNavigationOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 61
    :cond_19
    return-void
.end method

.method private setaContentDescription()V
    .registers 4

    .line 41
    iget-object v0, p0, Lcom/itau/empresas/ui/util/listener/ToolbarCustom;->builder:Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    # getter for: Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->toolbar:Landroid/support/v7/widget/Toolbar;
    invoke-static {v0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->access$100(Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;)Landroid/support/v7/widget/Toolbar;

    move-result-object v0

    const v1, 0x7f0e05c2

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Landroid/widget/TextView;

    .line 42
    .local v2, "tituloToolbar":Landroid/widget/TextView;
    iget-object v0, p0, Lcom/itau/empresas/ui/util/listener/ToolbarCustom;->builder:Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    # getter for: Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->idTextoAcessibilidade:I
    invoke-static {v0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->access$200(Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;)I

    move-result v0

    if-nez v0, :cond_1e

    .line 43
    const-string v0, ""

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_2d

    .line 45
    :cond_1e
    iget-object v0, p0, Lcom/itau/empresas/ui/util/listener/ToolbarCustom;->activity:Landroid/support/v7/app/AppCompatActivity;

    iget-object v1, p0, Lcom/itau/empresas/ui/util/listener/ToolbarCustom;->builder:Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    # getter for: Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->idTextoAcessibilidade:I
    invoke-static {v1}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->access$200(Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/AppCompatActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 47
    :goto_2d
    return-void
.end method

.method private setaDrawableFechar()V
    .registers 3

    .line 64
    iget-object v0, p0, Lcom/itau/empresas/ui/util/listener/ToolbarCustom;->builder:Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    # getter for: Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->toolbar:Landroid/support/v7/widget/Toolbar;
    invoke-static {v0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->access$100(Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;)Landroid/support/v7/widget/Toolbar;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/ui/util/listener/ToolbarCustom;->builder:Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    # getter for: Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->idDrawable:I
    invoke-static {v1}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->access$700(Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->setNavigationIcon(I)V

    .line 65
    return-void
.end method

.method private setaTitulo()V
    .registers 4

    .line 50
    iget-object v0, p0, Lcom/itau/empresas/ui/util/listener/ToolbarCustom;->builder:Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    # getter for: Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->toolbar:Landroid/support/v7/widget/Toolbar;
    invoke-static {v0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->access$100(Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;)Landroid/support/v7/widget/Toolbar;

    move-result-object v0

    const v1, 0x7f0e05c2

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Landroid/widget/TextView;

    .line 51
    .local v2, "tituloToolbar":Landroid/widget/TextView;
    iget-object v0, p0, Lcom/itau/empresas/ui/util/listener/ToolbarCustom;->builder:Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    # getter for: Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->idTitulo:I
    invoke-static {v0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->access$300(Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;)I

    move-result v0

    if-eqz v0, :cond_22

    .line 52
    iget-object v0, p0, Lcom/itau/empresas/ui/util/listener/ToolbarCustom;->builder:Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    # getter for: Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->idTitulo:I
    invoke-static {v0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->access$300(Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;)I

    move-result v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(I)V

    goto :goto_33

    .line 53
    :cond_22
    iget-object v0, p0, Lcom/itau/empresas/ui/util/listener/ToolbarCustom;->builder:Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    # getter for: Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->titulo:Ljava/lang/String;
    invoke-static {v0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->access$400(Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_33

    .line 54
    iget-object v0, p0, Lcom/itau/empresas/ui/util/listener/ToolbarCustom;->builder:Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    # getter for: Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->titulo:Ljava/lang/String;
    invoke-static {v0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->access$400(Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 56
    :cond_33
    :goto_33
    return-void
.end method

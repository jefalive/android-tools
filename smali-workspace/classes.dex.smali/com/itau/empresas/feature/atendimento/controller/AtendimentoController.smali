.class public Lcom/itau/empresas/feature/atendimento/controller/AtendimentoController;
.super Lcom/itau/empresas/controller/BaseController;
.source "AtendimentoController.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/itau/empresas/controller/BaseController<Lcom/itau/empresas/api/Api;>;"
    }
.end annotation


# instance fields
.field app:Lcom/itau/empresas/CustomApplication;


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 17
    invoke-direct {p0}, Lcom/itau/empresas/controller/BaseController;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/itau/empresas/feature/atendimento/controller/AtendimentoController;)Ljava/lang/Object;
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/feature/atendimento/controller/AtendimentoController;

    .line 17
    invoke-virtual {p0}, Lcom/itau/empresas/feature/atendimento/controller/AtendimentoController;->api()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100(Ljava/lang/Object;)V
    .registers 1
    .param p0, "x0"    # Ljava/lang/Object;

    .line 17
    invoke-static {p0}, Lcom/itau/empresas/feature/atendimento/controller/AtendimentoController;->post(Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic access$200(Lcom/itau/empresas/feature/atendimento/controller/AtendimentoController;)Ljava/lang/Object;
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/feature/atendimento/controller/AtendimentoController;

    .line 17
    invoke-virtual {p0}, Lcom/itau/empresas/feature/atendimento/controller/AtendimentoController;->api()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$300(Ljava/lang/Object;)V
    .registers 1
    .param p0, "x0"    # Ljava/lang/Object;

    .line 17
    invoke-static {p0}, Lcom/itau/empresas/feature/atendimento/controller/AtendimentoController;->post(Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic access$600(Lcom/itau/empresas/feature/atendimento/controller/AtendimentoController;)Ljava/lang/Object;
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/feature/atendimento/controller/AtendimentoController;

    .line 17
    invoke-virtual {p0}, Lcom/itau/empresas/feature/atendimento/controller/AtendimentoController;->api()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$700(Ljava/lang/Object;)V
    .registers 1
    .param p0, "x0"    # Ljava/lang/Object;

    .line 17
    invoke-static {p0}, Lcom/itau/empresas/feature/atendimento/controller/AtendimentoController;->post(Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public buscaAlertas(Ljava/lang/String;)V
    .registers 3
    .param p1, "idAlerta"    # Ljava/lang/String;

    .line 57
    new-instance v0, Lcom/itau/empresas/feature/atendimento/controller/AtendimentoController$4;

    invoke-direct {v0, p0, p1}, Lcom/itau/empresas/feature/atendimento/controller/AtendimentoController$4;-><init>(Lcom/itau/empresas/feature/atendimento/controller/AtendimentoController;Ljava/lang/String;)V

    invoke-static {v0}, Lbr/com/itau/sdk/android/core/BackendClient;->callApi(Lbr/com/itau/sdk/android/core/type/BackendCall;)V

    .line 65
    return-void
.end method

.method public buscaFaqs(Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;)V
    .registers 13
    .param p1, "idFaq"    # Ljava/lang/String;
    .param p2, "filtro"    # Ljava/lang/String;
    .param p3, "limit"    # I
    .param p4, "offset"    # I
    .param p5, "retornoSemMatch"    # Ljava/lang/String;

    .line 24
    new-instance v0, Lcom/itau/empresas/feature/atendimento/controller/AtendimentoController$1;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/itau/empresas/feature/atendimento/controller/AtendimentoController$1;-><init>(Lcom/itau/empresas/feature/atendimento/controller/AtendimentoController;Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;)V

    invoke-static {v0}, Lbr/com/itau/sdk/android/core/BackendClient;->callApi(Lbr/com/itau/sdk/android/core/type/BackendCall;)V

    .line 34
    return-void
.end method

.method public buscaSegmento()V
    .registers 2

    .line 37
    new-instance v0, Lcom/itau/empresas/feature/atendimento/controller/AtendimentoController$2;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/atendimento/controller/AtendimentoController$2;-><init>(Lcom/itau/empresas/feature/atendimento/controller/AtendimentoController;)V

    invoke-static {v0}, Lbr/com/itau/sdk/android/core/BackendClient;->callApi(Lbr/com/itau/sdk/android/core/type/BackendCall;)V

    .line 45
    return-void
.end method

.class public Lbr/com/itau/sdk/android/core/RemoteBundle;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:[B

.field private static b:I


# direct methods
.method static constructor <clinit>()V
    .registers 3

    const/16 v0, 0x4c

    new-array v0, v0, [B

    fill-array-data v0, :array_e

    sput-object v0, Lbr/com/itau/sdk/android/core/RemoteBundle;->a:[B

    const/4 v0, 0x7

    sput v0, Lbr/com/itau/sdk/android/core/RemoteBundle;->b:I

    return-void

    nop

    :array_e
    .array-data 1
        0x7bt
        0x74t
        0x3bt
        -0x47t
        0x11t
        -0x43t
        0x36t
        0xdt
        -0x1t
        -0x3et
        0x3ct
        0xct
        -0x12t
        0x15t
        -0x46t
        0x46t
        -0xet
        0x8t
        -0x3ct
        0x34t
        0xet
        -0x9t
        0xft
        -0x2t
        -0x5t
        -0x4t
        -0x35t
        0x36t
        0xdt
        0x4t
        -0xct
        -0x36t
        0x17t
        -0x2t
        0x14t
        -0x12t
        0x1ft
        -0x13t
        -0x5t
        0x15t
        0x11t
        -0x43t
        0x36t
        0xdt
        -0x1t
        -0x3et
        0x3ct
        0xct
        -0x12t
        0x15t
        -0x46t
        0x46t
        -0xet
        0x8t
        -0x3ct
        0x34t
        0xet
        -0x9t
        0xft
        -0x2t
        -0x5t
        -0x4t
        -0x35t
        0x36t
        0xdt
        0x4t
        -0xct
        -0x36t
        0x1bt
        -0x6t
        0x13t
        -0xat
        0x18t
        -0x13t
        -0x5t
        0x15t
    .end array-data
.end method

.method private static a()Lcom/google/gson/JsonObject;
    .registers 5

    .line 128
    new-instance v3, Lbr/com/itau/security/securestorage/SecureStorage;

    invoke-static {}, Lbr/com/itau/sdk/android/core/BackendClient;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v3, v0}, Lbr/com/itau/security/securestorage/SecureStorage;-><init>(Landroid/content/Context;)V

    .line 129
    sget-object v0, Lbr/com/itau/sdk/android/core/RemoteBundle;->a:[B

    const/16 v1, 0x8

    aget-byte v0, v0, v1

    add-int/lit8 v0, v0, 0x1

    sget-object v1, Lbr/com/itau/sdk/android/core/RemoteBundle;->a:[B

    const/16 v2, 0x8

    aget-byte v1, v1, v2

    neg-int v1, v1

    invoke-static {v0, v0, v1}, Lbr/com/itau/sdk/android/core/RemoteBundle;->a(III)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v3, v0, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 131
    if-nez v4, :cond_25

    .line 132
    const/4 v0, 0x0

    return-object v0

    .line 135
    :cond_25
    sget-object v0, Lbr/com/itau/sdk/android/core/n;->a:Lcom/google/gson/Gson;

    const-class v1, Lcom/google/gson/JsonObject;

    invoke-virtual {v0, v4, v1}, Lcom/google/gson/Gson;->fromJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/gson/JsonObject;

    return-object v0
.end method

.method private static a(III)Ljava/lang/String;
    .registers 9

    mul-int/lit8 p0, p0, 0x4

    rsub-int/lit8 p0, p0, 0x62

    sget-object v4, Lbr/com/itau/sdk/android/core/RemoteBundle;->a:[B

    const/4 v5, 0x0

    mul-int/lit8 p1, p1, 0x4

    add-int/lit8 p1, p1, 0x25

    mul-int/lit8 p2, p2, 0x24

    rsub-int/lit8 p2, p2, 0x27

    new-instance v0, Ljava/lang/String;

    new-array v1, p1, [B

    add-int/lit8 p1, p1, -0x1

    if-nez v4, :cond_1c

    move v2, p0

    move v3, p2

    :goto_19
    add-int/2addr v2, v3

    add-int/lit8 p0, v2, -0x1

    :cond_1c
    int-to-byte v2, p0

    aput-byte v2, v1, v5

    move v2, v5

    add-int/lit8 p2, p2, 0x1

    add-int/lit8 v5, v5, 0x1

    if-ne v2, p1, :cond_2b

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Ljava/lang/String;-><init>([BI)V

    return-object v0

    :cond_2b
    move v2, p0

    aget-byte v3, v4, p2

    goto :goto_19
.end method

.method public static getInt(Ljava/lang/String;I)I
    .registers 4

    .line 111
    invoke-static {p0}, Lbr/com/itau/sdk/android/core/RemoteBundle;->getInt(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    .line 112
    if-eqz v1, :cond_b

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_c

    :cond_b
    move v0, p1

    :goto_c
    return v0
.end method

.method public static getInt(Ljava/lang/String;)Ljava/lang/Integer;
    .registers 3

    .line 106
    invoke-static {}, Lbr/com/itau/sdk/android/core/RemoteBundle;->a()Lcom/google/gson/JsonObject;

    move-result-object v1

    .line 107
    if-eqz v1, :cond_19

    invoke-virtual {v1, p0}, Lcom/google/gson/JsonObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_19

    invoke-virtual {v1, p0}, Lcom/google/gson/JsonObject;->get(Ljava/lang/String;)Lcom/google/gson/JsonElement;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/gson/JsonElement;->getAsInt()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_1a

    :cond_19
    const/4 v0, 0x0

    :goto_1a
    return-object v0
.end method

.method public static getString(Ljava/lang/String;)Ljava/lang/String;
    .registers 3

    .line 117
    invoke-static {}, Lbr/com/itau/sdk/android/core/RemoteBundle;->a()Lcom/google/gson/JsonObject;

    move-result-object v1

    .line 118
    if-eqz v1, :cond_15

    invoke-virtual {v1, p0}, Lcom/google/gson/JsonObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_15

    invoke-virtual {v1, p0}, Lcom/google/gson/JsonObject;->get(Ljava/lang/String;)Lcom/google/gson/JsonElement;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/gson/JsonElement;->getAsString()Ljava/lang/String;

    move-result-object v0

    goto :goto_16

    :cond_15
    const/4 v0, 0x0

    :goto_16
    return-object v0
.end method

.method public static getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .registers 4

    .line 122
    invoke-static {p0}, Lbr/com/itau/sdk/android/core/RemoteBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 123
    if-eqz v1, :cond_8

    move-object v0, v1

    goto :goto_9

    :cond_8
    move-object v0, p1

    :goto_9
    return-object v0
.end method

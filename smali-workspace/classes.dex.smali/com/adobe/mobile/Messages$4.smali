.class final Lcom/adobe/mobile/Messages$4;
.super Ljava/lang/Object;
.source "Messages.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/adobe/mobile/Messages;->checkForInAppMessage(Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$cdata:Ljava/util/Map;

.field final synthetic val$lifecycleData:Ljava/util/Map;

.field final synthetic val$vars:Ljava/util/Map;


# direct methods
.method constructor <init>(Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;)V
    .registers 4

    .line 176
    iput-object p1, p0, Lcom/adobe/mobile/Messages$4;->val$vars:Ljava/util/Map;

    iput-object p2, p0, Lcom/adobe/mobile/Messages$4;->val$cdata:Ljava/util/Map;

    iput-object p3, p0, Lcom/adobe/mobile/Messages$4;->val$lifecycleData:Ljava/util/Map;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .registers 8

    .line 179
    const/4 v2, 0x0

    .line 181
    .local v2, "messages":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/adobe/mobile/Message;>;"
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->isWearableApp()Z

    move-result v0

    if-nez v0, :cond_f

    .line 182
    invoke-static {}, Lcom/adobe/mobile/MobileConfig;->getInstance()Lcom/adobe/mobile/MobileConfig;

    move-result-object v0

    invoke-virtual {v0}, Lcom/adobe/mobile/MobileConfig;->getInAppMessages()Ljava/util/ArrayList;

    move-result-object v2

    .line 185
    :cond_f
    if-eqz v2, :cond_17

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-gtz v0, :cond_18

    .line 186
    :cond_17
    return-void

    .line 190
    :cond_18
    iget-object v0, p0, Lcom/adobe/mobile/Messages$4;->val$vars:Ljava/util/Map;

    if-eqz v0, :cond_3b

    iget-object v0, p0, Lcom/adobe/mobile/Messages$4;->val$vars:Ljava/util/Map;

    const-string v1, "pev2"

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3b

    iget-object v0, p0, Lcom/adobe/mobile/Messages$4;->val$vars:Ljava/util/Map;

    const-string v1, "pev2"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ADBINTERNAL:In-App Message"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3b

    .line 191
    return-void

    .line 196
    :cond_3b
    iget-object v0, p0, Lcom/adobe/mobile/Messages$4;->val$cdata:Ljava/util/Map;

    invoke-static {v0}, Lcom/adobe/mobile/Messages;->lowercaseKeysForMap(Ljava/util/Map;)Ljava/util/HashMap;

    move-result-object v3

    .line 197
    .local v3, "lowercaseContextData":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    iget-object v0, p0, Lcom/adobe/mobile/Messages$4;->val$vars:Ljava/util/Map;

    invoke-static {v0}, Lcom/adobe/mobile/Messages;->lowercaseKeysForMap(Ljava/util/Map;)Ljava/util/HashMap;

    move-result-object v4

    .line 199
    .local v4, "lowercaseVars":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_4b
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_65

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/adobe/mobile/Message;

    .line 200
    .local v6, "message":Lcom/adobe/mobile/Message;
    iget-object v0, p0, Lcom/adobe/mobile/Messages$4;->val$lifecycleData:Ljava/util/Map;

    invoke-virtual {v6, v4, v3, v0}, Lcom/adobe/mobile/Message;->shouldShowForVariables(Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;)Z

    move-result v0

    if-eqz v0, :cond_64

    .line 201
    invoke-virtual {v6}, Lcom/adobe/mobile/Message;->show()V

    .line 202
    goto :goto_65

    .line 204
    .end local v6    # "message":Lcom/adobe/mobile/Message;
    :cond_64
    goto :goto_4b

    .line 207
    :cond_65
    :goto_65
    return-void
.end method

.class public Lcom/cesards/cropimageview/ImageMathsFactory;
.super Ljava/lang/Object;
.source "ImageMathsFactory.java"


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getImageMaths(Lcom/cesards/cropimageview/CropImageView;)Lcom/cesards/cropimageview/ImageMaths;
    .registers 5
    .param p1, "imageView"    # Lcom/cesards/cropimageview/CropImageView;

    .line 9
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x12

    if-ge v0, v1, :cond_8

    const/4 v2, 0x1

    goto :goto_9

    :cond_8
    const/4 v2, 0x0

    .line 10
    .local v2, "preApi18":Z
    :goto_9
    if-eqz v2, :cond_11

    new-instance v0, Lcom/cesards/cropimageview/PreApi18CropImage;

    invoke-direct {v0, p1}, Lcom/cesards/cropimageview/PreApi18CropImage;-><init>(Lcom/cesards/cropimageview/CropImageView;)V

    goto :goto_16

    :cond_11
    new-instance v0, Lcom/cesards/cropimageview/API18Image;

    invoke-direct {v0, p1}, Lcom/cesards/cropimageview/API18Image;-><init>(Lcom/cesards/cropimageview/CropImageView;)V

    :goto_16
    check-cast v0, Lcom/cesards/cropimageview/ImageMaths;

    return-object v0
.end method

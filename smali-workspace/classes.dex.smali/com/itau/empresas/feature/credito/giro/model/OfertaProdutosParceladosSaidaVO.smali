.class public Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosSaidaVO;
.super Ljava/lang/Object;
.source "OfertaProdutosParceladosSaidaVO.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private agencia:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "agencia"
    .end annotation
.end field

.field private cnpjCliente:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "cnpj_cliente"
    .end annotation
.end field

.field private codigoPlanoFinanciamento:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "codigo_plano_financiamento"
    .end annotation
.end field

.field private conta:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "conta"
    .end annotation
.end field

.field private cpfOperador:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "cpf_operador"
    .end annotation
.end field

.field private cpfRepresentante:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "cpf_representante"
    .end annotation
.end field

.field private digitoConta:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "digito_conta"
    .end annotation
.end field

.field private hasIndicadorTravaDomicilio:Z
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "indicador_trava_domicilio"
    .end annotation
.end field

.field private indicadorOfertaEspecial:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "indicador_oferta_especial"
    .end annotation
.end field

.field private indicadorTransbordo:Z
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "indicador_transbordo"
    .end annotation
.end field

.field private listaBandeiraProdutosParceladosVO:Ljava/util/List;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "bandeiras"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Lcom/itau/empresas/api/model/BandeiraProdutosParceladosVO;>;"
        }
    .end annotation
.end field

.field private listaOfertaProdutosParcelados:Ljava/util/List;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "ofertas"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosVO;>;"
        }
    .end annotation
.end field

.field private nomeCliente:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "nome_cliente"
    .end annotation
.end field

.field private numeroConvenio:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "numero_convenio"
    .end annotation
.end field

.field private ofertaEspecialProdutosParceladosVO:Lcom/itau/empresas/feature/credito/giro/model/OfertaEspecialProdutosParceladosVO;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "oferta_especial"
    .end annotation
.end field

.field private prazoMaximoContratacao:J
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "prazo_maximo_contratacao"
    .end annotation
.end field

.field private prazoMinimoContratacao:J
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "prazo_minimo_contratacao"
    .end annotation
.end field

.field private solicitacaoTrava:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "solicitacao_trava"
    .end annotation
.end field

.field private taxaJurosMes:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "taxa_juros_mes"
    .end annotation
.end field

.field private valorTotalTrava:J
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "valor_total_trava"
    .end annotation
.end field

.field private versaoConvenio:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "versao_convenio"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getAgencia()Ljava/lang/String;
    .registers 2

    .line 165
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosSaidaVO;->agencia:Ljava/lang/String;

    return-object v0
.end method

.method public getCnpjCliente()Ljava/lang/String;
    .registers 2

    .line 100
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosSaidaVO;->cnpjCliente:Ljava/lang/String;

    return-object v0
.end method

.method public getConta()Ljava/lang/String;
    .registers 2

    .line 173
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosSaidaVO;->conta:Ljava/lang/String;

    return-object v0
.end method

.method public getCpfOperador()Ljava/lang/String;
    .registers 2

    .line 197
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosSaidaVO;->cpfOperador:Ljava/lang/String;

    return-object v0
.end method

.method public getDigitoConta()Ljava/lang/String;
    .registers 2

    .line 181
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosSaidaVO;->digitoConta:Ljava/lang/String;

    return-object v0
.end method

.method public getListaOfertaProdutosParcelados()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosVO;>;"
        }
    .end annotation

    .line 58
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosSaidaVO;->listaOfertaProdutosParcelados:Ljava/util/List;

    return-object v0
.end method

.method public getOfertaEspecialProdutosParceladosVO()Lcom/itau/empresas/feature/credito/giro/model/OfertaEspecialProdutosParceladosVO;
    .registers 2

    .line 75
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosSaidaVO;->ofertaEspecialProdutosParceladosVO:Lcom/itau/empresas/feature/credito/giro/model/OfertaEspecialProdutosParceladosVO;

    return-object v0
.end method

.method public getPrazoMaximoContratacao()J
    .registers 3

    .line 213
    iget-wide v0, p0, Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosSaidaVO;->prazoMaximoContratacao:J

    return-wide v0
.end method

.method public getPrazoMinimoContratacao()J
    .registers 3

    .line 205
    iget-wide v0, p0, Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosSaidaVO;->prazoMinimoContratacao:J

    return-wide v0
.end method

.method public getTaxaJurosMes()Ljava/lang/String;
    .registers 2

    .line 221
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosSaidaVO;->taxaJurosMes:Ljava/lang/String;

    return-object v0
.end method

.method public setPrazoMaximoContratacao(J)V
    .registers 3
    .param p1, "prazoMaximoContratacao"    # J

    .line 217
    iput-wide p1, p0, Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosSaidaVO;->prazoMaximoContratacao:J

    .line 218
    return-void
.end method

.method public setPrazoMinimoContratacao(J)V
    .registers 3
    .param p1, "prazoMinimoContratacao"    # J

    .line 209
    iput-wide p1, p0, Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosSaidaVO;->prazoMinimoContratacao:J

    .line 210
    return-void
.end method

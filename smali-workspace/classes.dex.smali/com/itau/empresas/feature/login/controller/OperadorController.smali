.class public Lcom/itau/empresas/feature/login/controller/OperadorController;
.super Lcom/itau/empresas/controller/BaseController;
.source "OperadorController.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/itau/empresas/controller/BaseController<Lcom/itau/empresas/feature/login/LoginApi;>;"
    }
.end annotation


# instance fields
.field app:Lcom/itau/empresas/CustomApplication;


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 17
    invoke-direct {p0}, Lcom/itau/empresas/controller/BaseController;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/itau/empresas/feature/login/controller/OperadorController;)Ljava/lang/Object;
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/feature/login/controller/OperadorController;

    .line 17
    invoke-virtual {p0}, Lcom/itau/empresas/feature/login/controller/OperadorController;->api()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public buscaContasOperador()V
    .registers 2

    .line 22
    new-instance v0, Lcom/itau/empresas/feature/login/controller/OperadorController$1;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/login/controller/OperadorController$1;-><init>(Lcom/itau/empresas/feature/login/controller/OperadorController;)V

    invoke-static {v0}, Lbr/com/itau/sdk/android/core/BackendClient;->callApi(Lbr/com/itau/sdk/android/core/type/BackendCall;)V

    .line 40
    return-void
.end method

.method public buscaDadosOperador()V
    .registers 4

    .line 44
    iget-object v0, p0, Lcom/itau/empresas/feature/login/controller/OperadorController;->app:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getEventoTracker()Lcom/itau/empresas/ui/util/analytics/TrackerEvento;

    move-result-object v0

    const-string v1, "operadorDetalhes"

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/analytics/TrackerEvento;->inicioMonitorarTempo(Ljava/lang/String;)V

    .line 45
    invoke-virtual {p0}, Lcom/itau/empresas/feature/login/controller/OperadorController;->api()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/feature/login/LoginApi;

    iget-object v1, p0, Lcom/itau/empresas/feature/login/controller/OperadorController;->app:Lcom/itau/empresas/CustomApplication;

    .line 46
    invoke-virtual {v1}, Lcom/itau/empresas/CustomApplication;->getCodigoOperador()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/itau/empresas/feature/login/LoginApi;->consultaDadosOperador(Ljava/lang/String;)Lcom/itau/empresas/api/model/DadosOperadorVO;

    move-result-object v2

    .line 47
    .local v2, "dadosOperadorVO":Lcom/itau/empresas/api/model/DadosOperadorVO;
    iget-object v0, p0, Lcom/itau/empresas/feature/login/controller/OperadorController;->app:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getEventoTracker()Lcom/itau/empresas/ui/util/analytics/TrackerEvento;

    move-result-object v0

    const-string v1, "operadorDetalhes"

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/analytics/TrackerEvento;->fimMonitorarTempo(Ljava/lang/String;)V

    .line 48
    iget-object v0, p0, Lcom/itau/empresas/feature/login/controller/OperadorController;->app:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0, v2}, Lcom/itau/empresas/CustomApplication;->setDadosOperador(Lcom/itau/empresas/api/model/DadosOperadorVO;)V

    .line 49
    return-void
.end method

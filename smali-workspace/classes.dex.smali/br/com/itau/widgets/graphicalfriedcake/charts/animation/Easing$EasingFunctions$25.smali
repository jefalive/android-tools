.class final Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingFunctions$25;
.super Ljava/lang/Object;
.source "Easing.java"

# interfaces
.implements Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/EasingFunction;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingFunctions;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .registers 1

    .line 603
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getInterpolation(F)F
    .registers 7
    .param p1, "input"    # F

    .line 620
    const v3, 0x3fd9cd60

    .line 621
    .local v3, "s":F
    const/high16 v0, 0x3f000000    # 0.5f

    div-float v4, p1, v0

    .line 622
    .local v4, "position":F
    const/high16 v0, 0x3f800000    # 1.0f

    cmpg-float v0, v4, v0

    if-gez v0, :cond_1c

    .line 624
    mul-float v0, v4, v4

    const v3, 0x402612ff

    const v1, 0x406612ff

    mul-float/2addr v1, v4

    sub-float/2addr v1, v3

    mul-float/2addr v0, v1

    const/high16 v1, 0x3f000000    # 0.5f

    mul-float/2addr v0, v1

    return v0

    .line 627
    :cond_1c
    const/high16 v0, 0x40000000    # 2.0f

    sub-float v0, v4, v0

    move v4, v0

    mul-float/2addr v0, v4

    const v3, 0x402612ff

    const v1, 0x406612ff

    mul-float/2addr v1, v4

    add-float/2addr v1, v3

    mul-float/2addr v0, v1

    const/high16 v1, 0x40000000    # 2.0f

    add-float/2addr v0, v1

    const/high16 v1, 0x3f000000    # 0.5f

    mul-float/2addr v0, v1

    return v0
.end method

.class public final Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ConfirmarAutorizacaoTributosActivity_;
.super Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ConfirmarAutorizacaoTributosActivity;
.source "ConfirmarAutorizacaoTributosActivity_.java"

# interfaces
.implements Lorg/androidannotations/api/view/HasViews;
.implements Lorg/androidannotations/api/view/OnViewChangedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ConfirmarAutorizacaoTributosActivity_$IntentBuilder_;
    }
.end annotation


# instance fields
.field private final onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;


# direct methods
.method public constructor <init>()V
    .registers 2

    .line 44
    invoke-direct {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ConfirmarAutorizacaoTributosActivity;-><init>()V

    .line 48
    new-instance v0, Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-direct {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ConfirmarAutorizacaoTributosActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    return-void
.end method

.method static synthetic access$001(Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ConfirmarAutorizacaoTributosActivity_;)V
    .registers 1
    .param p0, "x0"    # Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ConfirmarAutorizacaoTributosActivity_;

    .line 44
    invoke-super {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ConfirmarAutorizacaoTributosActivity;->mostraDialogoDeProgresso()V

    return-void
.end method

.method static synthetic access$101(Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ConfirmarAutorizacaoTributosActivity_;)V
    .registers 1
    .param p0, "x0"    # Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ConfirmarAutorizacaoTributosActivity_;

    .line 44
    invoke-super {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ConfirmarAutorizacaoTributosActivity;->escondeDialogoDeProgresso()V

    return-void
.end method

.method private init_(Landroid/os/Bundle;)V
    .registers 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 63
    new-instance v0, Lcom/itau/empresas/Preferences_;

    invoke-direct {v0, p0}, Lcom/itau/empresas/Preferences_;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ConfirmarAutorizacaoTributosActivity_;->prefs:Lcom/itau/empresas/Preferences_;

    .line 64
    invoke-static {p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->registerOnViewChangedListener(Lorg/androidannotations/api/view/OnViewChangedListener;)V

    .line 65
    invoke-static {}, Lcom/itau/empresas/CustomApplication_;->getInstance()Lcom/itau/empresas/CustomApplication;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ConfirmarAutorizacaoTributosActivity_;->application:Lcom/itau/empresas/CustomApplication;

    .line 66
    invoke-static {}, Lcom/itau/empresas/CustomApplication_;->getInstance()Lcom/itau/empresas/CustomApplication;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ConfirmarAutorizacaoTributosActivity_;->app:Lcom/itau/empresas/CustomApplication;

    .line 67
    invoke-static {p0}, Lcom/itau/empresas/controller/PagamentoDarfController_;->getInstance_(Landroid/content/Context;)Lcom/itau/empresas/controller/PagamentoDarfController_;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ConfirmarAutorizacaoTributosActivity_;->pagamentoDarfController:Lcom/itau/empresas/controller/PagamentoDarfController;

    .line 68
    invoke-static {p0}, Lcom/itau/empresas/controller/PagamentoGPSController_;->getInstance_(Landroid/content/Context;)Lcom/itau/empresas/controller/PagamentoGPSController_;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ConfirmarAutorizacaoTributosActivity_;->pagamentoGPSController:Lcom/itau/empresas/controller/PagamentoGPSController;

    .line 69
    invoke-direct {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ConfirmarAutorizacaoTributosActivity_;->injectExtras_()V

    .line 70
    invoke-virtual {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ConfirmarAutorizacaoTributosActivity_;->afterInject()V

    .line 71
    return-void
.end method

.method private injectExtras_()V
    .registers 3

    .line 127
    invoke-virtual {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ConfirmarAutorizacaoTributosActivity_;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 128
    .local v1, "extras_":Landroid/os/Bundle;
    if-eqz v1, :cond_40

    .line 129
    const-string v0, "darfInclusaoRequestVO"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 130
    const-string v0, "darfInclusaoRequestVO"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ConfirmarAutorizacaoTributosActivity_;->darfInclusaoRequestVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;

    .line 132
    :cond_1c
    const-string v0, "gpsRequestVO"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2e

    .line 133
    const-string v0, "gpsRequestVO"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/GpsRequestVO;

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ConfirmarAutorizacaoTributosActivity_;->gpsRequestVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/GpsRequestVO;

    .line 135
    :cond_2e
    const-string v0, "dadosConfirmacaoTributos"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_40

    .line 136
    const-string v0, "dadosConfirmacaoTributos"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ConfirmarAutorizacaoTributosActivity_;->dadosConfirmacaoTributos:Ljava/util/ArrayList;

    .line 139
    :cond_40
    return-void
.end method


# virtual methods
.method public escondeDialogoDeProgresso()V
    .registers 5

    .line 164
    const-string v0, ""

    new-instance v1, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ConfirmarAutorizacaoTributosActivity_$3;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ConfirmarAutorizacaoTributosActivity_$3;-><init>(Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ConfirmarAutorizacaoTributosActivity_;)V

    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3}, Lorg/androidannotations/api/UiThreadExecutor;->runTask(Ljava/lang/String;Ljava/lang/Runnable;J)V

    .line 172
    return-void
.end method

.method public mostraDialogoDeProgresso()V
    .registers 5
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "CommitTransaction"
        }
    .end annotation

    .line 152
    const-string v0, ""

    new-instance v1, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ConfirmarAutorizacaoTributosActivity_$2;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ConfirmarAutorizacaoTributosActivity_$2;-><init>(Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ConfirmarAutorizacaoTributosActivity_;)V

    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3}, Lorg/androidannotations/api/UiThreadExecutor;->runTask(Ljava/lang/String;Ljava/lang/Runnable;J)V

    .line 160
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 55
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ConfirmarAutorizacaoTributosActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-static {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    move-result-object v1

    .line 56
    .local v1, "previousNotifier":Lorg/androidannotations/api/view/OnViewChangedNotifier;
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ConfirmarAutorizacaoTributosActivity_;->init_(Landroid/os/Bundle;)V

    .line 57
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ConfirmarAutorizacaoTributosActivity;->onCreate(Landroid/os/Bundle;)V

    .line 58
    invoke-static {v1}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 59
    const v0, 0x7f03002a

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ConfirmarAutorizacaoTributosActivity_;->setContentView(I)V

    .line 60
    return-void
.end method

.method public onViewChanged(Lorg/androidannotations/api/view/HasViews;)V
    .registers 4
    .param p1, "hasViews"    # Lorg/androidannotations/api/view/HasViews;

    .line 105
    const v0, 0x7f0e0124

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ConfirmarAutorizacaoTributosActivity_;->root:Landroid/widget/LinearLayout;

    .line 106
    const v0, 0x7f0e012e

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ConfirmarAutorizacaoTributosActivity_;->llConfirmacaoPagamento:Landroid/widget/LinearLayout;

    .line 107
    const v0, 0x7f0e0138

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ConfirmarAutorizacaoTributosActivity_;->campoAgenciaConta:Landroid/widget/TextView;

    .line 108
    const v0, 0x7f0e05ab

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ConfirmarAutorizacaoTributosActivity_;->campoNomeDaEmpresa:Landroid/widget/TextView;

    .line 109
    const v0, 0x7f0e012f

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ConfirmarAutorizacaoTributosActivity_;->botaoAutorizarPagamentoTributos:Landroid/widget/TextView;

    .line 110
    const v0, 0x7f0e0139

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ConfirmarAutorizacaoTributosActivity_;->campoCnpjPagador:Landroid/widget/TextView;

    .line 111
    const v0, 0x7f0e00b6

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/Toolbar;

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ConfirmarAutorizacaoTributosActivity_;->toolBar:Landroid/support/v7/widget/Toolbar;

    .line 112
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ConfirmarAutorizacaoTributosActivity_;->botaoAutorizarPagamentoTributos:Landroid/widget/TextView;

    if-eqz v0, :cond_5b

    .line 113
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ConfirmarAutorizacaoTributosActivity_;->botaoAutorizarPagamentoTributos:Landroid/widget/TextView;

    new-instance v1, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ConfirmarAutorizacaoTributosActivity_$1;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ConfirmarAutorizacaoTributosActivity_$1;-><init>(Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ConfirmarAutorizacaoTributosActivity_;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 122
    :cond_5b
    invoke-virtual {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ConfirmarAutorizacaoTributosActivity_;->updateView()V

    .line 123
    return-void
.end method

.method public setContentView(I)V
    .registers 3
    .param p1, "layoutResID"    # I

    .line 75
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ConfirmarAutorizacaoTributosActivity;->setContentView(I)V

    .line 76
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ConfirmarAutorizacaoTributosActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 77
    return-void
.end method

.method public setContentView(Landroid/view/View;)V
    .registers 3
    .param p1, "view"    # Landroid/view/View;

    .line 87
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ConfirmarAutorizacaoTributosActivity;->setContentView(Landroid/view/View;)V

    .line 88
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ConfirmarAutorizacaoTributosActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 89
    return-void
.end method

.method public setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .registers 4
    .param p1, "view"    # Landroid/view/View;
    .param p2, "params"    # Landroid/view/ViewGroup$LayoutParams;

    .line 81
    invoke-super {p0, p1, p2}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ConfirmarAutorizacaoTributosActivity;->setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 82
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ConfirmarAutorizacaoTributosActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 83
    return-void
.end method

.method public setIntent(Landroid/content/Intent;)V
    .registers 2
    .param p1, "newIntent"    # Landroid/content/Intent;

    .line 143
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ConfirmarAutorizacaoTributosActivity;->setIntent(Landroid/content/Intent;)V

    .line 144
    invoke-direct {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ConfirmarAutorizacaoTributosActivity_;->injectExtras_()V

    .line 145
    return-void
.end method

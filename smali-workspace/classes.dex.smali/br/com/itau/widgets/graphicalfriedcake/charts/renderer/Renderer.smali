.class public abstract Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/Renderer;
.super Ljava/lang/Object;
.source "Renderer.java"


# instance fields
.field protected mMaxX:I

.field protected mMinX:I

.field protected mViewPortHandler:Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;


# direct methods
.method public constructor <init>(Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;)V
    .registers 3
    .param p1, "viewPortHandler"    # Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    const/4 v0, 0x0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/Renderer;->mMinX:I

    .line 26
    const/4 v0, 0x0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/Renderer;->mMaxX:I

    .line 30
    iput-object p1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/Renderer;->mViewPortHandler:Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;

    .line 31
    return-void
.end method


# virtual methods
.method public calcXBounds(Lbr/com/itau/widgets/graphicalfriedcake/charts/interfaces/BarLineScatterCandleBubbleDataProvider;I)V
    .registers 8
    .param p1, "chart"    # Lbr/com/itau/widgets/graphicalfriedcake/charts/interfaces/BarLineScatterCandleBubbleDataProvider;
    .param p2, "xAxisModulus"    # I

    .line 61
    invoke-interface {p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/interfaces/BarLineScatterCandleBubbleDataProvider;->getLowestVisibleXIndex()I

    move-result v2

    .line 62
    .local v2, "low":I
    invoke-interface {p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/interfaces/BarLineScatterCandleBubbleDataProvider;->getHighestVisibleXIndex()I

    move-result v3

    .line 64
    .local v3, "high":I
    rem-int v0, v2, p2

    if-nez v0, :cond_e

    move v4, p2

    goto :goto_f

    :cond_e
    const/4 v4, 0x0

    .line 66
    .local v4, "subLow":I
    :goto_f
    div-int v0, v2, p2

    mul-int/2addr v0, p2

    sub-int/2addr v0, v4

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/Renderer;->mMinX:I

    .line 67
    div-int v0, v3, p2

    mul-int/2addr v0, p2

    add-int/2addr v0, p2

    invoke-interface {p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/interfaces/BarLineScatterCandleBubbleDataProvider;->getXChartMax()F

    move-result v1

    float-to-int v1, v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/Renderer;->mMaxX:I

    .line 76
    return-void
.end method

.method protected fitsBounds(FFF)Z
    .registers 5
    .param p1, "val"    # F
    .param p2, "min"    # F
    .param p3, "max"    # F

    .line 46
    cmpg-float v0, p1, p2

    if-ltz v0, :cond_8

    cmpl-float v0, p1, p3

    if-lez v0, :cond_a

    .line 47
    :cond_8
    const/4 v0, 0x0

    return v0

    .line 49
    :cond_a
    const/4 v0, 0x1

    return v0
.end method

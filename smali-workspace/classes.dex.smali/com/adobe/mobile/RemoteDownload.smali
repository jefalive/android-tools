.class Lcom/adobe/mobile/RemoteDownload;
.super Ljava/lang/Object;
.source "RemoteDownload.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/adobe/mobile/RemoteDownload$DownloadFileTask;,
        Lcom/adobe/mobile/RemoteDownload$RemoteDownloadBlock;
    }
.end annotation


# direct methods
.method constructor <init>()V
    .registers 1

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$100()Ljava/text/SimpleDateFormat;
    .registers 1

    .line 13
    invoke-static {}, Lcom/adobe/mobile/RemoteDownload;->createRFC2822Formatter()Ljava/text/SimpleDateFormat;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$200(Ljava/lang/String;)Ljava/lang/String;
    .registers 2
    .param p0, "x0"    # Ljava/lang/String;

    .line 13
    invoke-static {p0}, Lcom/adobe/mobile/RemoteDownload;->getEtagOfFile(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$300(Ljava/lang/String;)J
    .registers 3
    .param p0, "x0"    # Ljava/lang/String;

    .line 13
    invoke-static {p0}, Lcom/adobe/mobile/RemoteDownload;->getLastModifiedOfFile(Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method static synthetic access$400(Ljava/lang/String;Ljava/util/Date;Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;
    .registers 5
    .param p0, "x0"    # Ljava/lang/String;
    .param p1, "x1"    # Ljava/util/Date;
    .param p2, "x2"    # Ljava/lang/String;
    .param p3, "x3"    # Ljava/lang/String;

    .line 13
    invoke-static {p0, p1, p2, p3}, Lcom/adobe/mobile/RemoteDownload;->getNewCachedFile(Ljava/lang/String;Ljava/util/Date;Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method private static createRFC2822Formatter()Ljava/text/SimpleDateFormat;
    .registers 4

    .line 283
    const-string v2, "EEE, dd MMM yyyy HH:mm:ss Z"

    .line 284
    .local v2, "pattern":Ljava/lang/String;
    new-instance v3, Ljava/text/SimpleDateFormat;

    const-string v0, "EEE, dd MMM yyyy HH:mm:ss Z"

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v3, v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 285
    .local v3, "rfc2822formatter":Ljava/text/SimpleDateFormat;
    const-string v0, "GMT"

    invoke-static {v0}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/text/SimpleDateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 287
    return-object v3
.end method

.method protected static deleteCachedDataForURL(Ljava/lang/String;Ljava/lang/String;)Z
    .registers 5
    .param p0, "url"    # Ljava/lang/String;
    .param p1, "directory"    # Ljava/lang/String;

    .line 214
    if-eqz p0, :cond_9

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x1

    if-ge v0, v1, :cond_13

    .line 215
    :cond_9
    const-string v0, "Cached File - tried to delete cached file, but file path was empty"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logWarningFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 216
    const/4 v0, 0x0

    return v0

    .line 219
    :cond_13
    invoke-static {p0, p1}, Lcom/adobe/mobile/RemoteDownload;->getFileForCachedURL(Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;

    move-result-object v2

    .line 221
    .local v2, "cachedFile":Ljava/io/File;
    if-eqz v2, :cond_21

    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    move-result v0

    if-eqz v0, :cond_21

    const/4 v0, 0x1

    goto :goto_22

    :cond_21
    const/4 v0, 0x0

    :goto_22
    return v0
.end method

.method protected static deleteFilesForDirectoryNotInList(Ljava/lang/String;Ljava/util/List;)V
    .registers 13
    .param p0, "directory"    # Ljava/lang/String;
    .param p1, "urls"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/lang/String;Ljava/util/List<Ljava/lang/String;>;)V"
        }
    .end annotation

    .line 113
    if-eqz p1, :cond_8

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-gtz v0, :cond_c

    .line 115
    :cond_8
    invoke-static {p0}, Lcom/adobe/mobile/RemoteDownload;->deleteFilesInDirectory(Ljava/lang/String;)V

    .line 116
    return-void

    .line 119
    :cond_c
    invoke-static {p0}, Lcom/adobe/mobile/RemoteDownload;->getDownloadCacheDirectory(Ljava/lang/String;)Ljava/io/File;

    move-result-object v2

    .line 120
    .local v2, "cacheDir":Ljava/io/File;
    if-nez v2, :cond_13

    .line 122
    return-void

    .line 125
    :cond_13
    invoke-virtual {v2}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v3

    .line 126
    .local v3, "cachedFiles":[Ljava/io/File;
    if-eqz v3, :cond_1c

    array-length v0, v3

    if-gtz v0, :cond_1d

    .line 128
    :cond_1c
    return-void

    .line 132
    :cond_1d
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 133
    .local v4, "hashedUrls":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_26
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3b

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Ljava/lang/String;

    .line 134
    .local v6, "url":Ljava/lang/String;
    invoke-static {v6}, Lcom/adobe/mobile/RemoteDownload;->md5hash(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 135
    .end local v6    # "url":Ljava/lang/String;
    goto :goto_26

    .line 137
    :cond_3b
    move-object v5, v3

    array-length v6, v5

    const/4 v7, 0x0

    :goto_3e
    if-ge v7, v6, :cond_71

    aget-object v8, v5, v7

    .line 138
    .local v8, "file":Ljava/io/File;
    invoke-virtual {v8}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v9

    .line 139
    .local v9, "fileName":Ljava/lang/String;
    const-string v0, "."

    invoke-virtual {v9, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    const/4 v1, 0x0

    invoke-virtual {v9, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    .line 140
    .local v10, "fileHash":Ljava/lang/String;
    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6e

    .line 141
    invoke-virtual {v8}, Ljava/io/File;->delete()Z

    move-result v0

    if-eqz v0, :cond_66

    .line 142
    const-string v0, "Cached File - Removed unused cache file"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logDebugFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_6e

    .line 145
    :cond_66
    const-string v0, "Cached File - Failed to remove unused cache file"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logWarningFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 137
    .end local v8    # "file":Ljava/io/File;
    .end local v9    # "fileName":Ljava/lang/String;
    .end local v10    # "fileHash":Ljava/lang/String;
    :cond_6e
    :goto_6e
    add-int/lit8 v7, v7, 0x1

    goto :goto_3e

    .line 149
    :cond_71
    return-void
.end method

.method protected static deleteFilesInDirectory(Ljava/lang/String;)V
    .registers 9
    .param p0, "directory"    # Ljava/lang/String;

    .line 152
    invoke-static {p0}, Lcom/adobe/mobile/RemoteDownload;->getDownloadCacheDirectory(Ljava/lang/String;)Ljava/io/File;

    move-result-object v2

    .line 153
    .local v2, "cacheDir":Ljava/io/File;
    if-nez v2, :cond_7

    .line 155
    return-void

    .line 158
    :cond_7
    invoke-virtual {v2}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v3

    .line 159
    .local v3, "cachedFiles":[Ljava/io/File;
    if-eqz v3, :cond_10

    array-length v0, v3

    if-gtz v0, :cond_11

    .line 161
    :cond_10
    return-void

    .line 164
    :cond_11
    move-object v4, v3

    array-length v5, v4

    const/4 v6, 0x0

    :goto_14
    if-ge v6, v5, :cond_32

    aget-object v7, v4, v6

    .line 165
    .local v7, "file":Ljava/io/File;
    invoke-virtual {v7}, Ljava/io/File;->delete()Z

    move-result v0

    if-eqz v0, :cond_27

    .line 166
    const-string v0, "Cached File - Removed unused cache file"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logDebugFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_2f

    .line 169
    :cond_27
    const-string v0, "Cached File - Failed to remove unused cache file"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logWarningFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 164
    .end local v7    # "file":Ljava/io/File;
    :goto_2f
    add-int/lit8 v6, v6, 0x1

    goto :goto_14

    .line 172
    :cond_32
    return-void
.end method

.method protected static getDownloadCacheDirectory(Ljava/lang/String;)Ljava/io/File;
    .registers 4
    .param p0, "directory"    # Ljava/lang/String;

    .line 204
    new-instance v2, Ljava/io/File;

    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getCacheDirectory()Ljava/io/File;

    move-result-object v0

    invoke-direct {v2, v0, p0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 205
    .local v2, "downloadCacheDirectory":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_1f

    invoke-virtual {v2}, Ljava/io/File;->mkdir()Z

    move-result v0

    if-nez v0, :cond_1f

    .line 206
    const-string v0, "Cached File - unable to open/make download cache directory"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logWarningFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 207
    const/4 v0, 0x0

    return-object v0

    .line 210
    :cond_1f
    return-object v2
.end method

.method private static getEtagOfFile(Ljava/lang/String;)Ljava/lang/String;
    .registers 4
    .param p0, "path"    # Ljava/lang/String;

    .line 242
    if-eqz p0, :cond_9

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x1

    if-ge v0, v1, :cond_13

    .line 243
    :cond_9
    const-string v0, "Cached File - Path was null or empty for Cache File"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logWarningFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 244
    const/4 v0, 0x0

    return-object v0

    .line 247
    :cond_13
    invoke-static {p0}, Lcom/adobe/mobile/RemoteDownload;->getPathExtension(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/adobe/mobile/RemoteDownload;->splitPathExtension(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 249
    .local v2, "splitExtension":[Ljava/lang/String;
    if-eqz v2, :cond_21

    array-length v0, v2

    const/4 v1, 0x2

    if-ge v0, v1, :cond_2b

    .line 250
    :cond_21
    const-string v0, "Cached File - No etag for file. Extension had no second value after split."

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logWarningFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 251
    const/4 v0, 0x0

    return-object v0

    .line 254
    :cond_2b
    const/4 v0, 0x1

    aget-object v0, v2, v0

    return-object v0
.end method

.method protected static getFileForCachedURL(Ljava/lang/String;)Ljava/io/File;
    .registers 2
    .param p0, "url"    # Ljava/lang/String;

    .line 80
    const-string v0, "adbdownloadcache"

    invoke-static {p0, v0}, Lcom/adobe/mobile/RemoteDownload;->getFileForCachedURL(Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method protected static getFileForCachedURL(Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;
    .registers 13
    .param p0, "url"    # Ljava/lang/String;
    .param p1, "directory"    # Ljava/lang/String;

    .line 85
    if-eqz p0, :cond_9

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x1

    if-ge v0, v1, :cond_b

    .line 86
    :cond_9
    const/4 v0, 0x0

    return-object v0

    .line 89
    :cond_b
    invoke-static {p1}, Lcom/adobe/mobile/RemoteDownload;->getDownloadCacheDirectory(Ljava/lang/String;)Ljava/io/File;

    move-result-object v4

    .line 90
    .local v4, "cacheDirectory":Ljava/io/File;
    if-nez v4, :cond_13

    .line 91
    const/4 v0, 0x0

    return-object v0

    .line 94
    :cond_13
    invoke-virtual {v4}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v5

    .line 95
    .local v5, "cachedFiles":[Ljava/lang/String;
    if-eqz v5, :cond_1d

    array-length v0, v5

    const/4 v1, 0x1

    if-ge v0, v1, :cond_2e

    .line 96
    :cond_1d
    const-string v0, "Cached Files - Directory is empty (%s)."

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logDebugFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 97
    const/4 v0, 0x0

    return-object v0

    .line 100
    :cond_2e
    invoke-static {p0}, Lcom/adobe/mobile/RemoteDownload;->md5hash(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 102
    .local v6, "hashedName":Ljava/lang/String;
    move-object v7, v5

    array-length v8, v7

    const/4 v9, 0x0

    :goto_35
    if-ge v9, v8, :cond_53

    aget-object v10, v7, v9

    .line 103
    .local v10, "fileName":Ljava/lang/String;
    const/16 v0, 0x2e

    invoke-virtual {v10, v0}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v0

    const/4 v1, 0x0

    invoke-virtual {v10, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_50

    .line 104
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v4, v10}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v0

    .line 102
    .end local v10    # "fileName":Ljava/lang/String;
    :cond_50
    add-int/lit8 v9, v9, 0x1

    goto :goto_35

    .line 108
    :cond_53
    const-string v0, "Cached Files - This file has not previously been cached (%s)."

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logDebugFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 109
    const/4 v0, 0x0

    return-object v0
.end method

.method private static getLastModifiedOfFile(Ljava/lang/String;)J
    .registers 4
    .param p0, "path"    # Ljava/lang/String;

    .line 226
    if-eqz p0, :cond_9

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x1

    if-ge v0, v1, :cond_14

    .line 227
    :cond_9
    const-string v0, "Cached File - Path was null or empty for Cache File. Could not get Last Modified Date."

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logWarningFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 228
    const-wide/16 v0, 0x0

    return-wide v0

    .line 231
    :cond_14
    invoke-static {p0}, Lcom/adobe/mobile/RemoteDownload;->getPathExtension(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/adobe/mobile/RemoteDownload;->splitPathExtension(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 232
    .local v2, "splitExtension":[Ljava/lang/String;
    if-eqz v2, :cond_22

    array-length v0, v2

    const/4 v1, 0x1

    if-ge v0, v1, :cond_2d

    .line 233
    :cond_22
    const-string v0, "Cached File - No last modified date for file. Extension had no values after split."

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logWarningFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 234
    const-wide/16 v0, 0x0

    return-wide v0

    .line 237
    :cond_2d
    const/4 v0, 0x0

    aget-object v0, v2, v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method private static getNewCachedFile(Ljava/lang/String;Ljava/util/Date;Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;
    .registers 10
    .param p0, "url"    # Ljava/lang/String;
    .param p1, "lastModified"    # Ljava/util/Date;
    .param p2, "etag"    # Ljava/lang/String;
    .param p3, "directory"    # Ljava/lang/String;

    .line 175
    if-eqz p0, :cond_9

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x1

    if-ge v0, v1, :cond_13

    .line 176
    :cond_9
    const-string v0, "Cached File - Invalid url parameter while attempting to create cache file. Could not save data."

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logWarningFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 177
    const/4 v0, 0x0

    return-object v0

    .line 180
    :cond_13
    if-nez p1, :cond_1f

    .line 181
    const-string v0, "Cached File - Invalid lastModified parameter while attempting to create cache file. Could not save data."

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logWarningFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 182
    const/4 v0, 0x0

    return-object v0

    .line 185
    :cond_1f
    if-eqz p2, :cond_28

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x1

    if-ge v0, v1, :cond_32

    .line 186
    :cond_28
    const-string v0, "Cached File - Invalid etag parameter while attempting to create cache file. Could not save data."

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logWarningFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 187
    const/4 v0, 0x0

    return-object v0

    .line 191
    :cond_32
    invoke-static {p3}, Lcom/adobe/mobile/RemoteDownload;->getDownloadCacheDirectory(Ljava/lang/String;)Ljava/io/File;

    move-result-object v4

    .line 192
    .local v4, "cacheDirectory":Ljava/io/File;
    if-nez v4, :cond_3a

    .line 193
    const/4 v0, 0x0

    return-object v0

    .line 196
    :cond_3a
    invoke-static {p0}, Lcom/adobe/mobile/RemoteDownload;->md5hash(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 197
    .local v5, "md5Hash":Ljava/lang/String;
    if-eqz v5, :cond_47

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x1

    if-ge v0, v1, :cond_49

    .line 198
    :cond_47
    const/4 v0, 0x0

    return-object v0

    .line 200
    :cond_49
    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p0}, Lcom/adobe/mobile/RemoteDownload;->md5hash(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method private static getPathExtension(Ljava/lang/String;)Ljava/lang/String;
    .registers 3
    .param p0, "path"    # Ljava/lang/String;

    .line 259
    if-eqz p0, :cond_9

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x1

    if-ge v0, v1, :cond_13

    .line 260
    :cond_9
    const-string v0, "Cached File - Path was null or empty for Cache File"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logWarningFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 261
    const/4 v0, 0x0

    return-object v0

    .line 264
    :cond_13
    const-string v0, "."

    invoke-virtual {p0, v0}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static md5hash(Ljava/lang/String;)Ljava/lang/String;
    .registers 13
    .param p0, "input"    # Ljava/lang/String;

    .line 293
    if-eqz p0, :cond_9

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x1

    if-ge v0, v1, :cond_b

    .line 294
    :cond_9
    const/4 v0, 0x0

    return-object v0

    .line 298
    :cond_b
    const-string v0, "MD5"

    :try_start_d
    invoke-static {v0}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v4

    .line 299
    .local v4, "messagedigest":Ljava/security/MessageDigest;
    const-string v0, "UTF-8"

    invoke-virtual {p0, v0}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/security/MessageDigest;->update([B)V

    .line 300
    invoke-virtual {v4}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v5

    .line 302
    .local v5, "messageDigest":[B
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 303
    .local v6, "md5HexBuilder":Ljava/lang/StringBuilder;
    move-object v7, v5

    array-length v8, v7

    const/4 v9, 0x0

    :goto_26
    if-ge v9, v8, :cond_51

    aget-byte v10, v7, v9

    .line 304
    .local v10, "aMessageDigest":B
    and-int/lit16 v0, v10, 0xff

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v11

    .line 305
    .local v11, "hexString":Ljava/lang/String;
    :goto_30
    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x2

    if-ge v0, v1, :cond_4b

    .line 306
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "0"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    goto :goto_30

    .line 308
    :cond_4b
    invoke-virtual {v6, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 303
    .end local v10    # "aMessageDigest":B
    .end local v11    # "hexString":Ljava/lang/String;
    add-int/lit8 v9, v9, 0x1

    goto :goto_26

    .line 310
    :cond_51
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_54
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_d .. :try_end_54} :catch_56
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_d .. :try_end_54} :catch_67

    move-result-object v0

    return-object v0

    .line 312
    .end local v4    # "messagedigest":Ljava/security/MessageDigest;
    .end local v5    # "messageDigest":[B
    .end local v6    # "md5HexBuilder":Ljava/lang/StringBuilder;
    :catch_56
    move-exception v4

    .line 313
    .local v4, "e":Ljava/security/NoSuchAlgorithmException;
    const-string v0, "Cached Files - unable to get md5 hash (%s)"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v4}, Ljava/security/NoSuchAlgorithmException;->getMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logErrorFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 316
    .end local v4    # "e":Ljava/security/NoSuchAlgorithmException;
    goto :goto_77

    .line 314
    :catch_67
    move-exception v4

    .line 315
    .local v4, "e":Ljava/io/UnsupportedEncodingException;
    const-string v0, "Cached Files - Unsupported Encoding: UTF-8 (%s)"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v4}, Ljava/io/UnsupportedEncodingException;->getMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logErrorFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 317
    .end local v4    # "e":Ljava/io/UnsupportedEncodingException;
    :goto_77
    const/4 v0, 0x0

    return-object v0
.end method

.method protected static remoteDownloadAsync(Ljava/lang/String;IILcom/adobe/mobile/RemoteDownload$RemoteDownloadBlock;Ljava/lang/String;)V
    .registers 13
    .param p0, "url"    # Ljava/lang/String;
    .param p1, "connectionTimeout"    # I
    .param p2, "readTimeout"    # I
    .param p3, "block"    # Lcom/adobe/mobile/RemoteDownload$RemoteDownloadBlock;
    .param p4, "directory"    # Ljava/lang/String;

    .line 39
    new-instance v7, Ljava/lang/Thread;

    new-instance v0, Lcom/adobe/mobile/RemoteDownload$DownloadFileTask;

    move-object v1, p0

    move-object v2, p3

    move v3, p1

    move v4, p2

    move-object v5, p4

    const/4 v6, 0x0

    invoke-direct/range {v0 .. v6}, Lcom/adobe/mobile/RemoteDownload$DownloadFileTask;-><init>(Ljava/lang/String;Lcom/adobe/mobile/RemoteDownload$RemoteDownloadBlock;IILjava/lang/String;Lcom/adobe/mobile/RemoteDownload$1;)V

    invoke-direct {v7, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 40
    .local v7, "thread":Ljava/lang/Thread;
    invoke-virtual {v7}, Ljava/lang/Thread;->start()V

    .line 41
    return-void
.end method

.method protected static remoteDownloadAsync(Ljava/lang/String;Lcom/adobe/mobile/RemoteDownload$RemoteDownloadBlock;)V
    .registers 5
    .param p0, "url"    # Ljava/lang/String;
    .param p1, "block"    # Lcom/adobe/mobile/RemoteDownload$RemoteDownloadBlock;

    .line 50
    const-string v0, "adbdownloadcache"

    const/16 v1, 0x2710

    const/16 v2, 0x2710

    invoke-static {p0, v1, v2, p1, v0}, Lcom/adobe/mobile/RemoteDownload;->remoteDownloadAsync(Ljava/lang/String;IILcom/adobe/mobile/RemoteDownload$RemoteDownloadBlock;Ljava/lang/String;)V

    .line 51
    return-void
.end method

.method protected static remoteDownloadSync(Ljava/lang/String;IILcom/adobe/mobile/RemoteDownload$RemoteDownloadBlock;Ljava/lang/String;)V
    .registers 13
    .param p0, "url"    # Ljava/lang/String;
    .param p1, "connectionTimeout"    # I
    .param p2, "readTimeout"    # I
    .param p3, "block"    # Lcom/adobe/mobile/RemoteDownload$RemoteDownloadBlock;
    .param p4, "directory"    # Ljava/lang/String;

    .line 60
    new-instance v0, Lcom/adobe/mobile/RemoteDownload$DownloadFileTask;

    move-object v1, p0

    move-object v2, p3

    move v3, p1

    move v4, p2

    move-object v5, p4

    const/4 v6, 0x0

    invoke-direct/range {v0 .. v6}, Lcom/adobe/mobile/RemoteDownload$DownloadFileTask;-><init>(Ljava/lang/String;Lcom/adobe/mobile/RemoteDownload$RemoteDownloadBlock;IILjava/lang/String;Lcom/adobe/mobile/RemoteDownload$1;)V

    move-object v7, v0

    .line 61
    .local v7, "r":Ljava/lang/Runnable;
    invoke-interface {v7}, Ljava/lang/Runnable;->run()V

    .line 62
    return-void
.end method

.method protected static remoteDownloadSync(Ljava/lang/String;Lcom/adobe/mobile/RemoteDownload$RemoteDownloadBlock;)V
    .registers 5
    .param p0, "url"    # Ljava/lang/String;
    .param p1, "block"    # Lcom/adobe/mobile/RemoteDownload$RemoteDownloadBlock;

    .line 71
    const-string v0, "adbdownloadcache"

    const/16 v1, 0x2710

    const/16 v2, 0x2710

    invoke-static {p0, v1, v2, p1, v0}, Lcom/adobe/mobile/RemoteDownload;->remoteDownloadSync(Ljava/lang/String;IILcom/adobe/mobile/RemoteDownload$RemoteDownloadBlock;Ljava/lang/String;)V

    .line 72
    return-void
.end method

.method private static splitPathExtension(Ljava/lang/String;)[Ljava/lang/String;
    .registers 5
    .param p0, "extension"    # Ljava/lang/String;

    .line 269
    if-eqz p0, :cond_9

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x1

    if-ge v0, v1, :cond_13

    .line 270
    :cond_9
    const-string v0, "Cached File - Extension was null or empty on Cache File"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logWarningFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 271
    const/4 v0, 0x0

    return-object v0

    .line 274
    :cond_13
    const-string v0, "_"

    invoke-virtual {p0, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 275
    .local v3, "separated":[Ljava/lang/String;
    array-length v0, v3

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2a

    .line 276
    const-string v0, "Cached File - Invalid Extension on Cache File (%s)"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logWarningFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 277
    const/4 v0, 0x0

    return-object v0

    .line 279
    :cond_2a
    return-object v3
.end method

.method protected static stringIsUrl(Ljava/lang/String;)Z
    .registers 3
    .param p0, "stringUrl"    # Ljava/lang/String;

    .line 24
    if-eqz p0, :cond_8

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    if-gtz v0, :cond_a

    .line 25
    :cond_8
    const/4 v0, 0x0

    return v0

    .line 29
    :cond_a
    :try_start_a
    new-instance v0, Ljava/net/URL;

    invoke-direct {v0, p0}, Ljava/net/URL;-><init>(Ljava/lang/String;)V
    :try_end_f
    .catch Ljava/net/MalformedURLException; {:try_start_a .. :try_end_f} :catch_11

    .line 30
    const/4 v0, 0x1

    return v0

    .line 32
    :catch_11
    move-exception v1

    .line 33
    .local v1, "ex":Ljava/net/MalformedURLException;
    const/4 v0, 0x0

    return v0
.end method

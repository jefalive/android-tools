.class public Lbr/com/itau/sdk/android/fingerprintcore/android/AndroidFingerprintCanceller;
.super Ljava/lang/Object;
.source "AndroidFingerprintCanceller.java"

# interfaces
.implements Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintCanceller;


# instance fields
.field private final delegate:Landroid/support/v4/os/CancellationSignal;


# direct methods
.method public constructor <init>()V
    .registers 2

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    new-instance v0, Landroid/support/v4/os/CancellationSignal;

    invoke-direct {v0}, Landroid/support/v4/os/CancellationSignal;-><init>()V

    iput-object v0, p0, Lbr/com/itau/sdk/android/fingerprintcore/android/AndroidFingerprintCanceller;->delegate:Landroid/support/v4/os/CancellationSignal;

    .line 13
    return-void
.end method


# virtual methods
.method public cancel()V
    .registers 2

    .line 17
    iget-object v0, p0, Lbr/com/itau/sdk/android/fingerprintcore/android/AndroidFingerprintCanceller;->delegate:Landroid/support/v4/os/CancellationSignal;

    invoke-virtual {v0}, Landroid/support/v4/os/CancellationSignal;->cancel()V

    .line 18
    return-void
.end method

.method public getCancellationSignal()Landroid/support/v4/os/CancellationSignal;
    .registers 2

    .line 21
    iget-object v0, p0, Lbr/com/itau/sdk/android/fingerprintcore/android/AndroidFingerprintCanceller;->delegate:Landroid/support/v4/os/CancellationSignal;

    return-object v0
.end method

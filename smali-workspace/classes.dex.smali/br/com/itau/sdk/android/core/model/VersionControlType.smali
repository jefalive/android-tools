.class public final enum Lbr/com/itau/sdk/android/core/model/VersionControlType;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<Lbr/com/itau/sdk/android/core/model/VersionControlType;>;"
    }
.end annotation


# static fields
.field private static final $:[B

.field private static $$:I

.field private static final synthetic $VALUES:[Lbr/com/itau/sdk/android/core/model/VersionControlType;

.field public static final enum OPTIONAL:Lbr/com/itau/sdk/android/core/model/VersionControlType;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "optional"
    .end annotation
.end field

.field public static final enum REQUIRED:Lbr/com/itau/sdk/android/core/model/VersionControlType;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "required"
    .end annotation
.end field

.field public static final enum UNAVAILABLE:Lbr/com/itau/sdk/android/core/model/VersionControlType;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "unavailable"
    .end annotation
.end field


# direct methods
.method private static $(III)Ljava/lang/String;
    .registers 9

    rsub-int/lit8 p0, p0, 0x15

    new-instance v0, Ljava/lang/String;

    mul-int/lit8 p1, p1, 0x3

    add-int/lit8 p1, p1, 0x4f

    mul-int/lit8 p2, p2, 0x3

    rsub-int/lit8 p2, p2, 0xb

    sget-object v5, Lbr/com/itau/sdk/android/core/model/VersionControlType;->$:[B

    const/4 v4, 0x0

    new-array v1, p2, [B

    add-int/lit8 p2, p2, -0x1

    if-nez v5, :cond_1c

    move v2, p2

    move v3, p0

    :goto_17
    neg-int v3, v3

    add-int p1, v2, v3

    add-int/lit8 p0, p0, 0x1

    :cond_1c
    int-to-byte v2, p1

    aput-byte v2, v1, v4

    move v2, v4

    add-int/lit8 v4, v4, 0x1

    if-ne v2, p2, :cond_29

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Ljava/lang/String;-><init>([BI)V

    return-object v0

    :cond_29
    move v2, p1

    aget-byte v3, v5, p0

    goto :goto_17
.end method

.method static constructor <clinit>()V
    .registers 4

    .line 13
    const/16 v0, 0x1c

    new-array v0, v0, [B

    fill-array-data v0, :array_70

    sput-object v0, Lbr/com/itau/sdk/android/core/model/VersionControlType;->$:[B

    const/16 v0, 0xa9

    sput v0, Lbr/com/itau/sdk/android/core/model/VersionControlType;->$$:I

    new-instance v0, Lbr/com/itau/sdk/android/core/model/VersionControlType;

    sget-object v1, Lbr/com/itau/sdk/android/core/model/VersionControlType;->$:[B

    const/16 v2, 0x13

    aget-byte v1, v1, v2

    neg-int v1, v1

    ushr-int/lit8 v2, v1, 0x2

    add-int/lit8 v3, v2, -0x2

    invoke-static {v1, v2, v3}, Lbr/com/itau/sdk/android/core/model/VersionControlType;->$(III)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lbr/com/itau/sdk/android/core/model/VersionControlType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbr/com/itau/sdk/android/core/model/VersionControlType;->UNAVAILABLE:Lbr/com/itau/sdk/android/core/model/VersionControlType;

    .line 19
    new-instance v0, Lbr/com/itau/sdk/android/core/model/VersionControlType;

    sget-object v1, Lbr/com/itau/sdk/android/core/model/VersionControlType;->$:[B

    const/16 v2, 0x8

    aget-byte v1, v1, v2

    add-int/lit8 v1, v1, -0x1

    sget-object v2, Lbr/com/itau/sdk/android/core/model/VersionControlType;->$:[B

    const/16 v3, 0x8

    aget-byte v2, v2, v3

    const/16 v3, 0x11

    invoke-static {v3, v1, v2}, Lbr/com/itau/sdk/android/core/model/VersionControlType;->$(III)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lbr/com/itau/sdk/android/core/model/VersionControlType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbr/com/itau/sdk/android/core/model/VersionControlType;->OPTIONAL:Lbr/com/itau/sdk/android/core/model/VersionControlType;

    .line 25
    new-instance v0, Lbr/com/itau/sdk/android/core/model/VersionControlType;

    sget-object v1, Lbr/com/itau/sdk/android/core/model/VersionControlType;->$:[B

    const/16 v2, 0x8

    aget-byte v1, v1, v2

    add-int/lit8 v1, v1, -0x1

    sget-object v2, Lbr/com/itau/sdk/android/core/model/VersionControlType;->$:[B

    const/16 v3, 0x8

    aget-byte v2, v2, v3

    invoke-static {v1, v2, v2}, Lbr/com/itau/sdk/android/core/model/VersionControlType;->$(III)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2}, Lbr/com/itau/sdk/android/core/model/VersionControlType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbr/com/itau/sdk/android/core/model/VersionControlType;->REQUIRED:Lbr/com/itau/sdk/android/core/model/VersionControlType;

    .line 8
    const/4 v0, 0x3

    new-array v0, v0, [Lbr/com/itau/sdk/android/core/model/VersionControlType;

    sget-object v1, Lbr/com/itau/sdk/android/core/model/VersionControlType;->UNAVAILABLE:Lbr/com/itau/sdk/android/core/model/VersionControlType;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sget-object v1, Lbr/com/itau/sdk/android/core/model/VersionControlType;->OPTIONAL:Lbr/com/itau/sdk/android/core/model/VersionControlType;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    sget-object v1, Lbr/com/itau/sdk/android/core/model/VersionControlType;->REQUIRED:Lbr/com/itau/sdk/android/core/model/VersionControlType;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    sput-object v0, Lbr/com/itau/sdk/android/core/model/VersionControlType;->$VALUES:[Lbr/com/itau/sdk/android/core/model/VersionControlType;

    return-void

    nop

    :array_70
    .array-data 1
        0x25t
        0x57t
        -0x4at
        0x71t
        -0x1t
        -0x4t
        0xbt
        -0x6t
        0x1t
        0xdt
        -0xbt
        0x7t
        0xdt
        -0x15t
        0x15t
        -0x8t
        -0x3t
        0xbt
        -0x1t
        -0xat
        0x7t
        0xdt
        -0xct
        -0x4t
        0xct
        -0x9t
        0xdt
        0x1t
    .end array-data
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 8
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lbr/com/itau/sdk/android/core/model/VersionControlType;
    .registers 2

    .line 8
    const-class v0, Lbr/com/itau/sdk/android/core/model/VersionControlType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lbr/com/itau/sdk/android/core/model/VersionControlType;

    return-object v0
.end method

.method public static values()[Lbr/com/itau/sdk/android/core/model/VersionControlType;
    .registers 1

    .line 8
    sget-object v0, Lbr/com/itau/sdk/android/core/model/VersionControlType;->$VALUES:[Lbr/com/itau/sdk/android/core/model/VersionControlType;

    invoke-virtual {v0}, [Lbr/com/itau/sdk/android/core/model/VersionControlType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lbr/com/itau/sdk/android/core/model/VersionControlType;

    return-object v0
.end method

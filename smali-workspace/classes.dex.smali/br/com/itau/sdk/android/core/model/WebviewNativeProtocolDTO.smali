.class public final Lbr/com/itau/sdk/android/core/model/WebviewNativeProtocolDTO;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private action:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/Expose;
    .end annotation
.end field

.field private category:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/Expose;
    .end annotation
.end field

.field private data:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/Expose;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getAction()Ljava/lang/String;
    .registers 2

    .line 24
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/model/WebviewNativeProtocolDTO;->action:Ljava/lang/String;

    return-object v0
.end method

.method public getCategory()Ljava/lang/String;
    .registers 2

    .line 28
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/model/WebviewNativeProtocolDTO;->category:Ljava/lang/String;

    return-object v0
.end method

.method public getData()Ljava/lang/String;
    .registers 2

    .line 20
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/model/WebviewNativeProtocolDTO;->data:Ljava/lang/String;

    return-object v0
.end method

.class public Lcom/itau/empresas/feature/pagamento/semcodigobarra/adapter/FiltroPagamentos;
.super Landroid/widget/Filter;
.source "FiltroPagamentos.java"


# instance fields
.field private adapter:Landroid/widget/BaseAdapter;

.field private cnpjPagamentos:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/CnpjVO;>;"
        }
    .end annotation
.end field

.field private cnpjVOList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/CnpjVO;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/List;Ljava/util/List;Landroid/widget/BaseAdapter;)V
    .registers 4
    .param p1, "cnpjPagamentos"    # Ljava/util/List;
    .param p2, "cnpjVOList"    # Ljava/util/List;
    .param p3, "adapter"    # Landroid/widget/BaseAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/CnpjVO;>;Ljava/util/List<Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/CnpjVO;>;Landroid/widget/BaseAdapter;)V"
        }
    .end annotation

    .line 19
    invoke-direct {p0}, Landroid/widget/Filter;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/adapter/FiltroPagamentos;->cnpjPagamentos:Ljava/util/List;

    .line 21
    iput-object p2, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/adapter/FiltroPagamentos;->cnpjVOList:Ljava/util/List;

    .line 22
    iput-object p3, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/adapter/FiltroPagamentos;->adapter:Landroid/widget/BaseAdapter;

    .line 23
    return-void
.end method


# virtual methods
.method protected performFiltering(Ljava/lang/CharSequence;)Landroid/widget/Filter$FilterResults;
    .registers 9
    .param p1, "busca"    # Ljava/lang/CharSequence;

    .line 27
    new-instance v2, Landroid/widget/Filter$FilterResults;

    invoke-direct {v2}, Landroid/widget/Filter$FilterResults;-><init>()V

    .line 28
    .local v2, "resultado":Landroid/widget/Filter$FilterResults;
    iget-object v3, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/adapter/FiltroPagamentos;->cnpjVOList:Ljava/util/List;

    .line 29
    .local v3, "cnpjVOs":Ljava/util/List;, "Ljava/util/List<Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/CnpjVO;>;"
    if-eqz p1, :cond_f

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-nez v0, :cond_18

    .line 30
    :cond_f
    iput-object v3, v2, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    .line 31
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    iput v0, v2, Landroid/widget/Filter$FilterResults;->count:I

    goto :goto_4a

    .line 33
    :cond_18
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 34
    .local v4, "listaBancosFiltrada":Ljava/util/List;, "Ljava/util/List<Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/CnpjVO;>;"
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/adapter/FiltroPagamentos;->cnpjVOList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_23
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_42

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/CnpjVO;

    .line 35
    .local v6, "j":Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/CnpjVO;
    invoke-virtual {v6}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/CnpjVO;->getCnpj()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_41

    .line 36
    invoke-interface {v4, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 38
    .end local v6    # "j":Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/CnpjVO;
    :cond_41
    goto :goto_23

    .line 39
    :cond_42
    iput-object v4, v2, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    .line 40
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    iput v0, v2, Landroid/widget/Filter$FilterResults;->count:I

    .line 42
    .end local v4    # "listaBancosFiltrada":Ljava/util/List;, "Ljava/util/List<Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/CnpjVO;>;"
    .end local v4
    :goto_4a
    return-object v2
.end method

.method protected publishResults(Ljava/lang/CharSequence;Landroid/widget/Filter$FilterResults;)V
    .registers 4
    .param p1, "constraint"    # Ljava/lang/CharSequence;
    .param p2, "resultado"    # Landroid/widget/Filter$FilterResults;

    .line 47
    iget v0, p2, Landroid/widget/Filter$FilterResults;->count:I

    if-lez v0, :cond_f

    .line 48
    iget-object v0, p2, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/adapter/FiltroPagamentos;->cnpjPagamentos:Ljava/util/List;

    .line 49
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/adapter/FiltroPagamentos;->adapter:Landroid/widget/BaseAdapter;

    invoke-virtual {v0}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    .line 51
    :cond_f
    return-void
.end method

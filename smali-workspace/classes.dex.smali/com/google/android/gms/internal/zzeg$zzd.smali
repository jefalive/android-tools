.class public Lcom/google/android/gms/internal/zzeg$zzd;
.super Lcom/google/android/gms/internal/zzjj;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/internal/zzeg;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "zzd"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/gms/internal/zzjj<Lcom/google/android/gms/internal/zzeh;>;"
    }
.end annotation


# instance fields
.field private final zzBo:Lcom/google/android/gms/internal/zzeg$zze;

.field private zzBp:Z

.field private final zzpV:Ljava/lang/Object;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/internal/zzeg$zze;)V
    .registers 3

    invoke-direct {p0}, Lcom/google/android/gms/internal/zzjj;-><init>()V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/internal/zzeg$zzd;->zzpV:Ljava/lang/Object;

    iput-object p1, p0, Lcom/google/android/gms/internal/zzeg$zzd;->zzBo:Lcom/google/android/gms/internal/zzeg$zze;

    return-void
.end method

.method static synthetic zza(Lcom/google/android/gms/internal/zzeg$zzd;)Lcom/google/android/gms/internal/zzeg$zze;
    .registers 2

    iget-object v0, p0, Lcom/google/android/gms/internal/zzeg$zzd;->zzBo:Lcom/google/android/gms/internal/zzeg$zze;

    return-object v0
.end method


# virtual methods
.method public release()V
    .registers 5

    iget-object v2, p0, Lcom/google/android/gms/internal/zzeg$zzd;->zzpV:Ljava/lang/Object;

    monitor-enter v2

    :try_start_3
    iget-boolean v0, p0, Lcom/google/android/gms/internal/zzeg$zzd;->zzBp:Z
    :try_end_5
    .catchall {:try_start_3 .. :try_end_5} :catchall_28

    if-eqz v0, :cond_9

    monitor-exit v2

    return-void

    :cond_9
    const/4 v0, 0x1

    :try_start_a
    iput-boolean v0, p0, Lcom/google/android/gms/internal/zzeg$zzd;->zzBp:Z

    new-instance v0, Lcom/google/android/gms/internal/zzeg$zzd$1;

    invoke-direct {v0, p0}, Lcom/google/android/gms/internal/zzeg$zzd$1;-><init>(Lcom/google/android/gms/internal/zzeg$zzd;)V

    new-instance v1, Lcom/google/android/gms/internal/zzji$zzb;

    invoke-direct {v1}, Lcom/google/android/gms/internal/zzji$zzb;-><init>()V

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/internal/zzeg$zzd;->zza(Lcom/google/android/gms/internal/zzji$zzc;Lcom/google/android/gms/internal/zzji$zza;)V

    new-instance v0, Lcom/google/android/gms/internal/zzeg$zzd$2;

    invoke-direct {v0, p0}, Lcom/google/android/gms/internal/zzeg$zzd$2;-><init>(Lcom/google/android/gms/internal/zzeg$zzd;)V

    new-instance v1, Lcom/google/android/gms/internal/zzeg$zzd$3;

    invoke-direct {v1, p0}, Lcom/google/android/gms/internal/zzeg$zzd$3;-><init>(Lcom/google/android/gms/internal/zzeg$zzd;)V

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/internal/zzeg$zzd;->zza(Lcom/google/android/gms/internal/zzji$zzc;Lcom/google/android/gms/internal/zzji$zza;)V
    :try_end_26
    .catchall {:try_start_a .. :try_end_26} :catchall_28

    monitor-exit v2

    goto :goto_2b

    :catchall_28
    move-exception v3

    monitor-exit v2

    throw v3

    :goto_2b
    return-void
.end method

.class public interface abstract Lcom/itau/empresas/api/Api;
.super Ljava/lang/Object;
.source "Api.java"


# virtual methods
.method public abstract alteraApelidoContatoRecarga(Ljava/lang/String;Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaApelidoInputVO;)Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;
    .param p1    # Ljava/lang/String;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Path;
            value = "id_contato"
        .end annotation
    .end param
    .param p2    # Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaApelidoInputVO;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Body;
        .end annotation
    .end param
    .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Headers;
        value = {
            "itau-chave: 38d6287a-9f93-4e70-8d03-dac48cdb5a09"
        }
    .end annotation

    .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/PATCH;
        opKey = "alterarContatoRecarga"
    .end annotation
.end method

.method public abstract buscaAgencias(Lcom/itau/empresas/api/model/FiltroAgenciaVO;)Ljava/lang/String;
    .param p1    # Lcom/itau/empresas/api/model/FiltroAgenciaVO;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Body;
        .end annotation
    .end param
    .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/legacy/Middleware;
        operation = "GetAgencias"
    .end annotation
.end method

.method public abstract buscaAlertas(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/itau/empresas/feature/atendimento/model/AlertaVO;
    .param p1    # Ljava/lang/String;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Query;
            value = "id_alerta"
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Query;
            value = "codigo_operador"
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Query;
            value = "canal"
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Query;
            value = "opcao_retorno"
        .end annotation
    .end param
    .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/GET;
        opKey = "atendimentosAlertasPJ"
    .end annotation

    .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Headers;
        value = {
            "Accept: application/vnd.itau.v1+json",
            "itau-chave: 38d6287a-9f93-4e70-8d03-dac48cdb5a09"
        }
    .end annotation
.end method

.method public abstract buscaComprovantes(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;
    .param p1    # Ljava/lang/String;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Query;
            value = "agencia"
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Query;
            value = "conta"
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Query;
            value = "digito_verificador_conta"
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Query;
            value = "data_inicio"
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Query;
            value = "data_fim"
        .end annotation
    .end param
    .param p6    # Ljava/lang/String;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Query;
            value = "quantidade_registros"
        .end annotation
    .end param
    .param p7    # Ljava/lang/String;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Query;
            value = "tipo"
        .end annotation
    .end param
    .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/GET;
        opKey = "comprovantes"
    .end annotation

    .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Headers;
        value = {
            "Accept: application/vnd.itau.v1+json",
            "itau-chave: 38d6287a-9f93-4e70-8d03-dac48cdb5a09"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/util/List<Lcom/itau/empresas/feature/comprovantes/model/ComprovantesVO;>;"
        }
    .end annotation
.end method

.method public abstract buscaConsultaDetalhadaLis(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/itau/empresas/feature/credito/lis/model/LisConsultaDetalhadaRespostaVO;
    .param p1    # Ljava/lang/String;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Query;
            value = "agencia"
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Query;
            value = "conta"
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Query;
            value = "digito_conta"
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Query;
            value = "agencia_contratual"
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Query;
            value = "conta_contratual"
        .end annotation
    .end param
    .param p6    # Ljava/lang/String;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Query;
            value = "digito_conta_contratual"
        .end annotation
    .end param
    .param p7    # Ljava/lang/String;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Path;
            value = "id_produto"
        .end annotation
    .end param
    .param p8    # Ljava/lang/String;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Query;
            value = "cpf_representante"
        .end annotation
    .end param
    .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/GET;
        opKey = "creditoRotativoChequeEspecialConsultar"
    .end annotation

    .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Headers;
        value = {
            "Accept: application/vnd.itau.v1+json",
            "itau-chave: 830e669e-9552-4dd5-8736-5925c80591b3"
        }
    .end annotation
.end method

.method public abstract buscaContatosRecarga(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;
    .param p1    # Ljava/lang/String;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Query;
            value = "agencia"
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Query;
            value = "conta"
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Query;
            value = "digito_verificador_conta"
        .end annotation
    .end param
    .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/GET;
        opKey = "contatosRecarga"
    .end annotation

    .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Headers;
        value = {
            "itau-chave: 38d6287a-9f93-4e70-8d03-dac48cdb5a09",
            "Accept: application/vnd.itau.v2+json"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/util/List<Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;>;"
        }
    .end annotation
.end method

.method public abstract buscaDetalheOperadoraRecarga(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/itau/empresas/feature/recarga/contatos/model/RecargaDetalheOperadoraVO;
    .param p1    # Ljava/lang/String;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Path;
            value = "id_operadora"
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Query;
            value = "nome_regiao"
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Query;
            value = "agencia"
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Query;
            value = "conta"
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Query;
            value = "digito_verificador_conta"
        .end annotation
    .end param
    .param p6    # Ljava/lang/String;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Query;
            value = "cpf"
        .end annotation
    .end param
    .param p7    # Ljava/lang/String;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Query;
            value = "codigo_operador"
        .end annotation
    .end param
    .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/GET;
        opKey = "valoresOperadora"
    .end annotation

    .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Headers;
        value = {
            "Accept: application/vnd.itau.v1+json",
            "itau-chave: 38d6287a-9f93-4e70-8d03-dac48cdb5a09"
        }
    .end annotation
.end method

.method public abstract buscaFaqs(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/itau/empresas/feature/atendimento/model/AtendimentoVO;
    .param p1    # Ljava/lang/String;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Query;
            value = "id_faq"
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Query;
            value = "codigo_operador"
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Query;
            value = "filtro"
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Query;
            value = "limit"
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Query;
            value = "offset"
        .end annotation
    .end param
    .param p6    # Ljava/lang/String;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Query;
            value = "retorno_sem_match"
        .end annotation
    .end param
    .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/GET;
        opKey = "atendimentosFaqsPJ"
    .end annotation

    .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Headers;
        value = {
            "Accept: application/vnd.itau.v1+json",
            "itau-chave: 38d6287a-9f93-4e70-8d03-dac48cdb5a09"
        }
    .end annotation
.end method

.method public abstract buscaLimitesProduto(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/itau/empresas/feature/credito/lis/model/LisConsultaDetalheRespostaVO$LisConsultaRespostaVO;
    .param p1    # Ljava/lang/String;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Query;
            value = "agencia"
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Query;
            value = "conta"
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Query;
            value = "digito_conta"
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Query;
            value = "tipo_pessoa"
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Query;
            value = "cpf_representante_legal"
        .end annotation
    .end param
    .param p6    # Ljava/lang/String;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Query;
            value = "familia"
        .end annotation
    .end param
    .param p7    # Ljava/lang/String;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Query;
            value = "nome_produto"
        .end annotation
    .end param
    .param p8    # Ljava/lang/String;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Query;
            value = "tipo_operacao"
        .end annotation
    .end param
    .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/GET;
        opKey = "limitesProduto"
    .end annotation

    .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Headers;
        value = {
            "Accept: application/vnd.itau.v1+json",
            "itau-chave: 830e669e-9552-4dd5-8736-5925c80591b3"
        }
    .end annotation
.end method

.method public abstract buscaLisConsultaComprovante(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/itau/empresas/api/model/ComprovanteServiceVO;
    .param p1    # Ljava/lang/String;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Path;
            value = "id_comprovante"
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Query;
            value = "agencia"
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Query;
            value = "conta"
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Query;
            value = "digito_verificador_conta"
        .end annotation
    .end param
    .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/GET;
        opKey = "comprovanteId"
    .end annotation

    .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Headers;
        value = {
            "Accept: application/vnd.itau.v1+json",
            "itau-chave: 38d6287a-9f93-4e70-8d03-dac48cdb5a09"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/itau/empresas/api/model/ComprovanteServiceVO<Lcom/itau/empresas/api/model/CampoComprovanteLisConsultaVO;>;"
        }
    .end annotation
.end method

.method public abstract buscaOfertaChequeEspecial(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/itau/empresas/feature/credito/lis/model/OfertaContratacaoLisVo;
    .param p1    # Ljava/lang/String;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Query;
            value = "agencia"
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Query;
            value = "conta"
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Query;
            value = "digito_conta"
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Query;
            value = "cpf_representante"
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Query;
            value = "codigo_produto"
        .end annotation
    .end param
    .param p6    # Ljava/lang/String;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Query;
            value = "nome_produto"
        .end annotation
    .end param
    .param p7    # Ljava/lang/String;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Query;
            value = "tipo_operacao"
        .end annotation
    .end param
    .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/GET;
        opKey = "creditoRotativoChequeEspecialOfertas"
    .end annotation

    .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Headers;
        value = {
            "Accept: application/vnd.itau.v1+json",
            "itau-chave: 830e669e-9552-4dd5-8736-5925c80591b3"
        }
    .end annotation
.end method

.method public abstract buscaOfertaResumo(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/itau/empresas/feature/credito/lis/model/OfertaResumoLisVO;
    .param p1    # Ljava/lang/String;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Query;
            value = "agencia"
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Query;
            value = "conta"
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Query;
            value = "digito_conta"
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Query;
            value = "cpf_representante"
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Query;
            value = "nome_produto"
        .end annotation
    .end param
    .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/GET;
        opKey = "creditoRotativoChequeEspecialOfertasResumo"
    .end annotation

    .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Headers;
        value = {
            "Accept: application/vnd.itau.v1+json",
            "itau-chave: 830e669e-9552-4dd5-8736-5925c80591b3"
        }
    .end annotation
.end method

.method public abstract buscaOperadorasRecarga(Ljava/lang/String;)Ljava/util/List;
    .param p1    # Ljava/lang/String;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Query;
            value = "finalidade"
        .end annotation
    .end param
    .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/GET;
        opKey = "listaOperadoras"
    .end annotation

    .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Headers;
        value = {
            "Accept: application/vnd.itau.v1+json",
            "itau-chave: 38d6287a-9f93-4e70-8d03-dac48cdb5a09"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/lang/String;)Ljava/util/List<Lcom/itau/empresas/feature/recarga/contatos/model/RecargaOperadoraVO;>;"
        }
    .end annotation
.end method

.method public abstract buscaPendencias(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/itau/empresas/feature/pendencias/ListaPendenciasVO;
    .param p1    # Ljava/lang/String;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Path;
            value = "id_conta"
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Query;
            value = "codigo_operador"
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Query;
            value = "tipo_consulta"
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Query;
            value = "data_inicio"
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Query;
            value = "data_fim"
        .end annotation
    .end param
    .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/GET;
        opKey = "listaPendenciasUsuario"
    .end annotation

    .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Headers;
        value = {
            "Accept: application/vnd.itau.v1+json",
            "itau-chave: 38d6287a-9f93-4e70-8d03-dac48cdb5a09"
        }
    .end annotation
.end method

.method public abstract buscaTransferenciaRecebidas(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;
    .param p1    # Ljava/lang/String;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Path;
            value = "id_conta"
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Query;
            value = "data_inicio"
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Query;
            value = "data_fim"
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Query;
            value = "tipo_pagamento"
        .end annotation
    .end param
    .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/GET;
        opKey = "transfRecebidas"
    .end annotation

    .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Headers;
        value = {
            "Accept: application/vnd.itau.v1+json",
            "itau-chave: 38d6287a-9f93-4e70-8d03-dac48cdb5a09"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/util/List<Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasVO;>;"
        }
    .end annotation
.end method

.method public abstract cadastroSenha(Ljava/lang/String;Lcom/itau/empresas/api/model/CadastroSenhaEletronicaVO;)Lcom/itau/empresas/api/model/ResultadoCadastroSenhaEletronicaVO;
    .param p1    # Ljava/lang/String;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Path;
            value = "codigo_operador"
        .end annotation
    .end param
    .param p2    # Lcom/itau/empresas/api/model/CadastroSenhaEletronicaVO;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Body;
        .end annotation
    .end param
    .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Headers;
        value = {
            "Accept: application/vnd.itau.v1+json",
            "itau-chave: 38d6287a-9f93-4e70-8d03-dac48cdb5a09"
        }
    .end annotation

    .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/POST;
        opKey = "operadorSenha"
    .end annotation
.end method

.method public abstract consultaCnpjPagadores(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;
    .param p1    # Ljava/lang/String;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Query;
            value = "codigo_operador"
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Query;
            value = "agencia"
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Query;
            value = "conta"
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Query;
            value = "digito_verificador_conta"
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Query;
            value = "finalidade"
        .end annotation
    .end param
    .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/GET;
        opKey = "pagamentosCNPJPagadores"
    .end annotation

    .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Headers;
        value = {
            "Accept: application/vnd.itau.v1+json",
            "itau-chave: 38d6287a-9f93-4e70-8d03-dac48cdb5a09"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/util/List<Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/CnpjVO;>;"
        }
    .end annotation
.end method

.method public abstract consultaDadosClienteEmpresa(Ljava/lang/String;)Lcom/itau/empresas/api/model/DadosClienteEmpresaVO;
    .param p1    # Ljava/lang/String;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Query;
            value = "codigo_operador"
        .end annotation
    .end param
    .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/GET;
        opKey = "clientesEmpresas"
    .end annotation

    .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Headers;
        value = {
            "Accept: application/vnd.itau.v1+json",
            "itau-chave: 38d6287a-9f93-4e70-8d03-dac48cdb5a09"
        }
    .end annotation
.end method

.method public abstract consultaDetalhesAutorizacaoDarf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;
    .param p1    # Ljava/lang/String;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Query;
            value = "codigo_operador"
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Query;
            value = "identificacao_funcionalidade"
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Query;
            value = "numero_carrinho"
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Query;
            value = "numero_lote"
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Query;
            value = "identificacao_pagamento"
        .end annotation
    .end param
    .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/GET;
        opKey = "detalhePagamentoAutorizado"
    .end annotation

    .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Headers;
        value = {
            "Accept: application/vnd.itau.v1+json",
            "itau-chave: 38d6287a-9f93-4e70-8d03-dac48cdb5a09"
        }
    .end annotation
.end method

.method public abstract consultaLancamentos(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;)Lcom/itau/empresas/api/model/ExtratoVO;
    .param p1    # Ljava/lang/String;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Path;
            value = "id_conta"
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Query;
            value = "data_inicial"
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Query;
            value = "data_final"
        .end annotation
    .end param
    .param p4    # Ljava/lang/Boolean;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Query;
            value = "reduzido"
        .end annotation
    .end param
    .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/GET;
        opKey = "extrato"
    .end annotation

    .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Headers;
        value = {
            "Accept: application/vnd.itau.v1+json",
            "itau-chave: 38d6287a-9f93-4e70-8d03-dac48cdb5a09"
        }
    .end annotation
.end method

.method public abstract consultaLimitesHorarioPagamento(Ljava/lang/String;)Ljava/util/List;
    .param p1    # Ljava/lang/String;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Query;
            value = "tipos_operacoes_sispag"
        .end annotation
    .end param
    .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/GET;
        opKey = "horariosLimiteSISPAG"
    .end annotation

    .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Headers;
        value = {
            "Accept: application/vnd.itau.v1+json",
            "itau-chave: 38d6287a-9f93-4e70-8d03-dac48cdb5a09"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/lang/String;)Ljava/util/List<Lcom/itau/empresas/api/model/HorarioLimiteTransacoesVO;>;"
        }
    .end annotation
.end method

.method public abstract consultaLis(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/itau/empresas/api/model/LisVO;
    .param p1    # Ljava/lang/String;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Query;
            value = "agencia"
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Query;
            value = "conta"
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Query;
            value = "digito_verificador_conta"
        .end annotation
    .end param
    .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/GET;
        opKey = "lis"
    .end annotation

    .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Headers;
        value = {
            "Accept: application/vnd.itau.v1+json",
            "itau-chave: 38d6287a-9f93-4e70-8d03-dac48cdb5a09"
        }
    .end annotation
.end method

.method public abstract consultaMultiLimite(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/itau/empresas/feature/credito/produtos/model/MultiLimiteVO;
    .param p1    # Ljava/lang/String;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Query;
            value = "agencia"
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Query;
            value = "conta"
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Query;
            value = "digito_verificador_conta"
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Query;
            value = "incluso"
        .end annotation
    .end param
    .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/GET;
        opKey = "multiLimites"
    .end annotation

    .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Headers;
        value = {
            "Accept: application/vnd.itau.v1+json",
            "itau-chave: 38d6287a-9f93-4e70-8d03-dac48cdb5a09"
        }
    .end annotation
.end method

.method public abstract consultaSaldo(Ljava/lang/String;)Lcom/itau/empresas/api/model/SaldoVO;
    .param p1    # Ljava/lang/String;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Path;
            value = "id_conta"
        .end annotation
    .end param
    .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/GET;
        opKey = "saldo"
    .end annotation

    .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Headers;
        value = {
            "Accept: application/vnd.itau.v1+json",
            "itau-chave: 38d6287a-9f93-4e70-8d03-dac48cdb5a09"
        }
    .end annotation
.end method

.method public abstract detalhesMultilimite(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/legacy/MiddlewareUniversal;
        opKey = "MULTILIMITE_PJ"
    .end annotation
.end method

.method public abstract efetuaContratacaoLis(Ljava/lang/String;Lcom/itau/empresas/feature/credito/lis/model/LisContratacaoEnvioVO;)Lcom/itau/empresas/feature/credito/lis/model/LisContratacaoRespostaVO;
    .param p1    # Ljava/lang/String;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Path;
            value = "id_produto"
        .end annotation
    .end param
    .param p2    # Lcom/itau/empresas/feature/credito/lis/model/LisContratacaoEnvioVO;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Body;
        .end annotation
    .end param
    .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Headers;
        value = {
            "Accept: application/vnd.itau.v1+json",
            "itau-chave: 830e669e-9552-4dd5-8736-5925c80591b3"
        }
    .end annotation

    .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/POST;
        opKey = "creditoRotativoChequeEspecialEfetivar"
    .end annotation
.end method

.method public abstract efetuaRecarga(Ljava/lang/String;Lcom/itau/empresas/feature/recarga/contatos/model/RecargaInputVO;)Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoEfetivacaoRecargaVO;
    .param p1    # Ljava/lang/String;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Path;
            value = "id_telefone"
        .end annotation
    .end param
    .param p2    # Lcom/itau/empresas/feature/recarga/contatos/model/RecargaInputVO;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Body;
        .end annotation
    .end param
    .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Headers;
        value = {
            "itau-chave: 38d6287a-9f93-4e70-8d03-dac48cdb5a09",
            "Accept: application/vnd.itau.v1+json"
        }
    .end annotation

    .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/POST;
        opKey = "efetivarRecarga"
    .end annotation
.end method

.method public abstract efetuaSimulacaoLis(Ljava/lang/String;Ljava/lang/String;Lcom/itau/empresas/feature/credito/lis/model/LisSimulacaoEnvioVO;)Lcom/itau/empresas/feature/credito/lis/model/LisSimulacaoRespostaVO;
    .param p1    # Ljava/lang/String;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Path;
            value = "id_produto"
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Query;
            value = "tipo_operacao"
        .end annotation
    .end param
    .param p3    # Lcom/itau/empresas/feature/credito/lis/model/LisSimulacaoEnvioVO;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Body;
        .end annotation
    .end param
    .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Headers;
        value = {
            "Accept: application/vnd.itau.v1+json",
            "itau-chave: 830e669e-9552-4dd5-8736-5925c80591b3"
        }
    .end annotation

    .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/POST;
        opKey = "creditoRotativoChequeEspecialSimular"
    .end annotation
.end method

.method public abstract enviarComprovantePorEmail(Ljava/lang/String;Lcom/itau/empresas/api/model/EnvioComprovanteVO;)Lcom/itau/empresas/api/model/ResultadoComprovanteEnviadoVO;
    .param p1    # Ljava/lang/String;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Path;
            value = "id_comprovante"
        .end annotation
    .end param
    .param p2    # Lcom/itau/empresas/api/model/EnvioComprovanteVO;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Body;
        .end annotation
    .end param
    .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Headers;
        value = {
            "Accept: application/vnd.itau.v1+json",
            "itau-chave: 38d6287a-9f93-4e70-8d03-dac48cdb5a09"
        }
    .end annotation

    .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/POST;
        opKey = "enviarComprovanteEmail"
    .end annotation
.end method

.method public abstract excluiContatoRecarga(Ljava/lang/String;)Ljava/util/List;
    .param p1    # Ljava/lang/String;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Path;
            value = "id_contato"
        .end annotation
    .end param
    .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/DELETE;
        opKey = "deletarContatoRecarga"
    .end annotation

    .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Headers;
        value = {
            "itau-chave: 38d6287a-9f93-4e70-8d03-dac48cdb5a09"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/lang/String;)Ljava/util/List<Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;>;"
        }
    .end annotation
.end method

.method public abstract francesinha(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/itau/empresas/api/model/FrancesinhaVO;
    .param p1    # Z
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Query;
            value = "reduzido"
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Query;
            value = "agencia"
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Query;
            value = "conta"
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Query;
            value = "digito_verificador_conta"
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Query;
            value = "codigo_operador"
        .end annotation
    .end param
    .param p6    # Ljava/lang/String;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Query;
            value = "tipo_visao"
        .end annotation
    .end param
    .param p7    # Ljava/lang/String;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Query;
            value = "tipo_cobranca"
        .end annotation
    .end param
    .param p8    # Ljava/lang/String;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Query;
            value = "data_consulta"
        .end annotation
    .end param
    .param p9    # Ljava/lang/String;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Query;
            value = "codigo_carteira"
        .end annotation
    .end param
    .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/GET;
        opKey = "francesinha"
    .end annotation

    .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Headers;
        value = {
            "Accept: application/vnd.itau.v1+json",
            "itau-chave: 38d6287a-9f93-4e70-8d03-dac48cdb5a09"
        }
    .end annotation
.end method

.method public abstract incluiDarf()Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/InclusaoTributoResponseVO;
    .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Headers;
        value = {
            "Accept: application/vnd.itau.v1+json",
            "itau-chave: 38d6287a-9f93-4e70-8d03-dac48cdb5a09"
        }
    .end annotation

    .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/POST;
        opKey = "pagamentoTributoDarfPJIncluirEfetivar"
    .end annotation
.end method

.method public abstract incluiGps()Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/InclusaoTributoResponseVO;
    .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Headers;
        value = {
            "Accept: application/vnd.itau.v1+json",
            "itau-chave: 38d6287a-9f93-4e70-8d03-dac48cdb5a09"
        }
    .end annotation

    .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/POST;
        opKey = "pagamentoTributoGpsPJIncluirEfetivar"
    .end annotation
.end method

.method public abstract incluirAutorizarDarf()Lcom/google/gson/JsonObject;
    .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Headers;
        value = {
            "Accept: application/vnd.itau.v1+json",
            "itau-chave: 38d6287a-9f93-4e70-8d03-dac48cdb5a09"
        }
    .end annotation

    .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/POST;
        opKey = "pagamentoTributoDarfPJIncluirEAutorizarEfetivar"
    .end annotation
.end method

.method public abstract incluirAutorizarGps()Lcom/google/gson/JsonObject;
    .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Headers;
        value = {
            "Accept: application/vnd.itau.v1+json",
            "itau-chave: 38d6287a-9f93-4e70-8d03-dac48cdb5a09"
        }
    .end annotation

    .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/POST;
        opKey = "pagamentoTributoGpsPJIncluirEAutorizarEfetivar"
    .end annotation
.end method

.method public abstract insereContatoRecarga(Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaInputVO;)Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;
    .param p1    # Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaInputVO;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Body;
        .end annotation
    .end param
    .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Headers;
        value = {
            "itau-chave: 38d6287a-9f93-4e70-8d03-dac48cdb5a09",
            "Accept: application/vnd.itau.v2+json"
        }
    .end annotation

    .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/POST;
        opKey = "cadastrarContatoRecarga"
    .end annotation
.end method

.method public abstract listaAlcada(Ljava/lang/String;Ljava/lang/String;)Lcom/itau/empresas/api/model/AlcadaVO;
    .param p1    # Ljava/lang/String;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Path;
            value = "operador"
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Path;
            value = "conta"
        .end annotation
    .end param
    .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/GET;
        opKey = "alcadas"
    .end annotation

    .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Headers;
        value = {
            "Accept: application/vnd.itau.v1+json",
            "itau-chave: 38d6287a-9f93-4e70-8d03-dac48cdb5a09"
        }
    .end annotation
.end method

.method public abstract simulaRecarga(Ljava/lang/String;Lcom/itau/empresas/feature/recarga/contatos/model/RecargaInputVO;)Lbr/com/itau/sdk/android/core/endpoint/http/RouterResponse;
    .param p1    # Ljava/lang/String;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Path;
            value = "id_telefone"
        .end annotation
    .end param
    .param p2    # Lcom/itau/empresas/feature/recarga/contatos/model/RecargaInputVO;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Body;
        .end annotation
    .end param
    .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Headers;
        value = {
            "itau-chave: 38d6287a-9f93-4e70-8d03-dac48cdb5a09",
            "Accept: application/vnd.itau.v1+json"
        }
    .end annotation

    .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/POST;
        opKey = "simularRecarga"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/lang/String;Lcom/itau/empresas/feature/recarga/contatos/model/RecargaInputVO;)Lbr/com/itau/sdk/android/core/endpoint/http/RouterResponse<Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoSimulacaoRecargaVO;>;"
        }
    .end annotation
.end method

.method public abstract simulaRecargaAutorizar(Ljava/lang/String;Lcom/itau/empresas/feature/recarga/contatos/model/RecargaInputVO;)Lbr/com/itau/sdk/android/core/endpoint/http/RouterResponse;
    .param p1    # Ljava/lang/String;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Path;
            value = "id_telefone"
        .end annotation
    .end param
    .param p2    # Lcom/itau/empresas/feature/recarga/contatos/model/RecargaInputVO;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Body;
        .end annotation
    .end param
    .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Headers;
        value = {
            "itau-chave: 38d6287a-9f93-4e70-8d03-dac48cdb5a09",
            "Accept: application/vnd.itau.v1+json"
        }
    .end annotation

    .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/POST;
        opKey = "exibicao_recarga_autorizar"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/lang/String;Lcom/itau/empresas/feature/recarga/contatos/model/RecargaInputVO;)Lbr/com/itau/sdk/android/core/endpoint/http/RouterResponse<Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoSimulacaoRecargaVO;>;"
        }
    .end annotation
.end method

.method public abstract simularDarf(Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;)Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/InclusaoTributoResponseVO;
    .param p1    # Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Body;
        .end annotation
    .end param
    .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Headers;
        value = {
            "Accept: application/vnd.itau.v1+json",
            "itau-chave: 38d6287a-9f93-4e70-8d03-dac48cdb5a09"
        }
    .end annotation

    .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/POST;
        opKey = "pagamentoTributoDarfPJIncluirSimular"
    .end annotation
.end method

.method public abstract simularDarfAutorizacao(Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;)Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/InclusaoTributoResponseVO;
    .param p1    # Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Body;
        .end annotation
    .end param
    .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Headers;
        value = {
            "Accept: application/vnd.itau.v1+json",
            "itau-chave: 38d6287a-9f93-4e70-8d03-dac48cdb5a09"
        }
    .end annotation

    .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/POST;
        opKey = "pagamentoTributoDarfPJIncluirEAutorizarSimular"
    .end annotation
.end method

.method public abstract simularGPSAutorizacao(Lcom/itau/empresas/api/model/DadosGpsRequest;)Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/InclusaoTributoResponseVO;
    .param p1    # Lcom/itau/empresas/api/model/DadosGpsRequest;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Body;
        .end annotation
    .end param
    .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Headers;
        value = {
            "Accept: application/vnd.itau.v1+json",
            "itau-chave: 38d6287a-9f93-4e70-8d03-dac48cdb5a09"
        }
    .end annotation

    .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/POST;
        opKey = "pagamentoTributoGpsPJIncluirEAutorizarSimular"
    .end annotation
.end method

.method public abstract simularInclusaoGPS(Lcom/itau/empresas/api/model/DadosGpsRequest;)Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/InclusaoTributoResponseVO;
    .param p1    # Lcom/itau/empresas/api/model/DadosGpsRequest;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Body;
        .end annotation
    .end param
    .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Headers;
        value = {
            "Accept: application/vnd.itau.v1+json",
            "itau-chave: 38d6287a-9f93-4e70-8d03-dac48cdb5a09"
        }
    .end annotation

    .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/POST;
        opKey = "pagamentoTributoGpsPJIncluirSimular"
    .end annotation
.end method

.method public abstract trocarContaSessao(Lcom/itau/empresas/api/model/TrocaContaVO;)Ljava/lang/Void;
    .param p1    # Lcom/itau/empresas/api/model/TrocaContaVO;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Body;
        .end annotation
    .end param
    .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Headers;
        value = {
            "itau-chave: 38d6287a-9f93-4e70-8d03-dac48cdb5a09"
        }
    .end annotation

    .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/POST;
        opKey = "trocaConta"
    .end annotation
.end method

.method public abstract trocarSenhaObrigatoria(Ljava/lang/String;Lcom/itau/empresas/api/model/CadastroSenhaEletronicaVO;)Lcom/itau/empresas/api/model/ResultadoCadastroSenhaEletronicaVO;
    .param p1    # Ljava/lang/String;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Path;
            value = "codigo_operador"
        .end annotation
    .end param
    .param p2    # Lcom/itau/empresas/api/model/CadastroSenhaEletronicaVO;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Body;
        .end annotation
    .end param
    .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Headers;
        value = {
            "Accept: application/vnd.itau.v1+json",
            "itau-chave: 38d6287a-9f93-4e70-8d03-dac48cdb5a09"
        }
    .end annotation

    .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/POST;
        opKey = "trocarSenhaObrigatoria"
    .end annotation
.end method

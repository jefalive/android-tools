.class public final Ldid/f;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/content/SharedPreferences;


# static fields
.field private static a:Ljava/lang/String;

.field private static b:Ljava/lang/String;

.field private static final d:[B

.field private static e:I


# instance fields
.field private final c:Landroid/content/SharedPreferences;


# direct methods
.method static constructor <clinit>()V
    .registers 4

    .line 17
    const/16 v0, 0x2b

    new-array v0, v0, [B

    fill-array-data v0, :array_46

    sput-object v0, Ldid/f;->d:[B

    const/16 v0, 0xf9

    sput v0, Ldid/f;->e:I

    sget-object v0, Ldid/f;->d:[B

    const/4 v1, 0x0

    aget-byte v0, v0, v1

    int-to-byte v0, v0

    int-to-byte v1, v0

    sget-object v2, Ldid/f;->d:[B

    const/16 v3, 0x1b

    aget-byte v2, v2, v3

    neg-int v2, v2

    int-to-byte v2, v2

    invoke-static {v0, v1, v2}, Ldid/f;->a(BBI)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Ldid/f;->a:Ljava/lang/String;

    .line 18
    sget-object v0, Ldid/f;->d:[B

    const/4 v1, 0x5

    aget-byte v0, v0, v1

    add-int/lit8 v0, v0, -0x1

    int-to-byte v0, v0

    sget-object v1, Ldid/f;->d:[B

    const/4 v2, 0x0

    aget-byte v1, v1, v2

    int-to-byte v1, v1

    sget-object v2, Ldid/f;->d:[B

    const/16 v3, 0x12

    aget-byte v2, v2, v3

    int-to-byte v2, v2

    invoke-static {v0, v1, v2}, Ldid/f;->a(BBI)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Ldid/f;->b:Ljava/lang/String;

    return-void

    :array_46
    .array-data 1
        0x0t
        -0x80t
        -0x70t
        -0x7at
        -0x45t
        0x10t
        -0x4t
        -0xat
        0x14t
        -0x13t
        0xat
        0x6t
        -0xat
        0x14t
        0xet
        0x7t
        0x11t
        -0x4t
        0x5t
        -0x2t
        -0x38t
        0x5t
        0x4t
        -0x4t
        0x8t
        0x35t
        -0x34t
        -0x9t
        -0xet
        0x1t
        0x28t
        0x7t
        -0x2dt
        0x40t
        -0x2t
        0x2t
        -0x4t
        0x2t
        -0x40t
        0x10t
        0x3t
        -0xdt
        0x8t
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 5

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    sget-object v0, Ldid/f;->d:[B

    const/16 v1, 0x1d

    aget-byte v0, v0, v1

    int-to-byte v0, v0

    sget-object v1, Ldid/f;->d:[B

    const/4 v2, 0x0

    aget-byte v1, v1, v2

    int-to-byte v1, v1

    const/16 v2, 0x17

    invoke-static {v2, v0, v1}, Ldid/f;->a(BBI)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Ldid/f;->c:Landroid/content/SharedPreferences;

    .line 24
    return-void
.end method

.method static synthetic a(Ldid/f;)Landroid/content/SharedPreferences;
    .registers 2

    .line 15
    iget-object v0, p0, Ldid/f;->c:Landroid/content/SharedPreferences;

    return-object v0
.end method

.method private static a(BBI)Ljava/lang/String;
    .registers 9

    new-instance v0, Ljava/lang/String;

    const/4 v4, 0x0

    mul-int/lit8 p2, p2, 0x2

    rsub-int/lit8 p2, p2, 0x73

    mul-int/lit8 p1, p1, 0x7

    rsub-int/lit8 p1, p1, 0x10

    sget-object v5, Ldid/f;->d:[B

    rsub-int/lit8 p0, p0, 0x1b

    new-array v1, p1, [B

    if-nez v5, :cond_19

    move v2, p1

    move v3, p2

    :goto_15
    neg-int v3, v3

    add-int/2addr v2, v3

    add-int/lit8 p2, v2, 0x1

    :cond_19
    int-to-byte v2, p2

    aput-byte v2, v1, v4

    add-int/lit8 v4, v4, 0x1

    add-int/lit8 p0, p0, 0x1

    if-ne v4, p1, :cond_27

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Ljava/lang/String;-><init>([BI)V

    return-object v0

    :cond_27
    move v2, p2

    aget-byte v3, v5, p0

    goto :goto_15
.end method

.method static synthetic a(Ljava/lang/String;)Ljava/lang/String;
    .registers 4

    .line 15
    .line 6117
    invoke-static {p0}, Lbr/com/itau/security/commons/binary/ByteUtils;->from(Ljava/lang/String;)[B

    move-result-object p0

    .line 6118
    sget-object v0, Ldid/f;->a:Ljava/lang/String;

    invoke-static {v0}, Lbr/com/itau/security/commons/binary/ByteUtils;->from(Ljava/lang/String;)[B

    move-result-object v1

    .line 6119
    sget-object v0, Ldid/f;->b:Ljava/lang/String;

    invoke-static {v0}, Lbr/com/itau/security/commons/binary/ByteUtils;->from(Ljava/lang/String;)[B

    move-result-object v2

    .line 6120
    invoke-static {p0, v1, v2}, Lbr/com/itau/security/commons/Crypto;->pkcs7PaddingEncrypt([B[B[B)[B

    move-result-object v0

    .line 6121
    invoke-static {v0}, Lbr/com/itau/security/commons/binary/ByteUtils;->b64Encode([B)Ljava/lang/String;

    move-result-object v0

    .line 15
    return-object v0
.end method


# virtual methods
.method public final contains(Ljava/lang/String;)Z
    .registers 6

    .line 98
    iget-object v0, p0, Ldid/f;->c:Landroid/content/SharedPreferences;

    .line 5117
    invoke-static {p1}, Lbr/com/itau/security/commons/binary/ByteUtils;->from(Ljava/lang/String;)[B

    move-result-object p1

    .line 5118
    sget-object v1, Ldid/f;->a:Ljava/lang/String;

    invoke-static {v1}, Lbr/com/itau/security/commons/binary/ByteUtils;->from(Ljava/lang/String;)[B

    move-result-object v2

    .line 5119
    sget-object v1, Ldid/f;->b:Ljava/lang/String;

    invoke-static {v1}, Lbr/com/itau/security/commons/binary/ByteUtils;->from(Ljava/lang/String;)[B

    move-result-object v3

    .line 5120
    invoke-static {p1, v2, v3}, Lbr/com/itau/security/commons/Crypto;->pkcs7PaddingEncrypt([B[B[B)[B

    move-result-object v1

    .line 5121
    invoke-static {v1}, Lbr/com/itau/security/commons/binary/ByteUtils;->b64Encode([B)Ljava/lang/String;

    move-result-object v1

    .line 98
    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public final edit()Landroid/content/SharedPreferences$Editor;
    .registers 3

    .line 103
    new-instance v0, Ldid/h;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Ldid/h;-><init>(Ldid/f;B)V

    return-object v0
.end method

.method public final getAll()Ljava/util/Map;
    .registers 10

    .line 29
    iget-object v0, p0, Ldid/f;->c:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->getAll()Ljava/util/Map;

    move-result-object v2

    .line 31
    if-nez v2, :cond_d

    .line 32
    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v0

    return-object v0

    .line 34
    :cond_d
    new-instance v3, Ljava/util/HashMap;

    .line 35
    invoke-interface {v2}, Ljava/util/Map;->size()I

    move-result v0

    invoke-direct {v3, v0}, Ljava/util/HashMap;-><init>(I)V

    .line 37
    invoke-interface {v2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1e
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_80

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Ljava/util/Map$Entry;

    .line 39
    :try_start_2b
    invoke-interface {v4}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v5

    .line 41
    if-eqz v5, :cond_6e

    .line 43
    invoke-interface {v4}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1125
    invoke-static {v0}, Lbr/com/itau/security/commons/binary/ByteUtils;->b64Decode(Ljava/lang/String;)[B

    move-result-object v6

    .line 1126
    sget-object v0, Ldid/f;->a:Ljava/lang/String;

    invoke-static {v0}, Lbr/com/itau/security/commons/binary/ByteUtils;->from(Ljava/lang/String;)[B

    move-result-object v7

    .line 1127
    sget-object v0, Ldid/f;->b:Ljava/lang/String;

    invoke-static {v0}, Lbr/com/itau/security/commons/binary/ByteUtils;->from(Ljava/lang/String;)[B

    move-result-object v8

    .line 1128
    invoke-static {v6, v7, v8}, Lbr/com/itau/security/commons/Crypto;->pkcs7PaddingDecrypt([B[B[B)[B

    move-result-object v0

    invoke-static {v0}, Lbr/com/itau/security/commons/binary/ByteUtils;->to([B)Ljava/lang/String;

    move-result-object v0

    .line 43
    .line 44
    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2125
    invoke-static {v1}, Lbr/com/itau/security/commons/binary/ByteUtils;->b64Decode(Ljava/lang/String;)[B

    move-result-object v6

    .line 2126
    sget-object v1, Ldid/f;->a:Ljava/lang/String;

    invoke-static {v1}, Lbr/com/itau/security/commons/binary/ByteUtils;->from(Ljava/lang/String;)[B

    move-result-object v7

    .line 2127
    sget-object v1, Ldid/f;->b:Ljava/lang/String;

    invoke-static {v1}, Lbr/com/itau/security/commons/binary/ByteUtils;->from(Ljava/lang/String;)[B

    move-result-object v8

    .line 2128
    invoke-static {v6, v7, v8}, Lbr/com/itau/security/commons/Crypto;->pkcs7PaddingDecrypt([B[B[B)[B

    move-result-object v1

    invoke-static {v1}, Lbr/com/itau/security/commons/binary/ByteUtils;->to([B)Ljava/lang/String;

    move-result-object v1

    .line 43
    invoke-interface {v3, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_6e
    .catch Ljava/lang/Exception; {:try_start_2b .. :try_end_6e} :catch_6f

    .line 51
    :cond_6e
    goto :goto_1e

    .line 47
    .line 49
    :catch_6f
    invoke-interface {v4}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    .line 50
    invoke-interface {v4}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 49
    invoke-interface {v3, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 52
    goto/16 :goto_1e

    .line 53
    :cond_80
    return-object v3
.end method

.method public final getBoolean(Ljava/lang/String;Z)Z
    .registers 4

    .line 93
    invoke-static {p2}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Ldid/f;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public final getFloat(Ljava/lang/String;F)F
    .registers 4

    .line 88
    invoke-static {p2}, Ljava/lang/Float;->toString(F)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Ldid/f;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v0

    return v0
.end method

.method public final getInt(Ljava/lang/String;I)I
    .registers 4

    .line 78
    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Ldid/f;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public final getLong(Ljava/lang/String;J)J
    .registers 6

    .line 83
    invoke-static {p2, p3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Ldid/f;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method public final getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .registers 7

    .line 58
    iget-object v0, p0, Ldid/f;->c:Landroid/content/SharedPreferences;

    .line 3117
    invoke-static {p1}, Lbr/com/itau/security/commons/binary/ByteUtils;->from(Ljava/lang/String;)[B

    move-result-object p1

    .line 3118
    sget-object v1, Ldid/f;->a:Ljava/lang/String;

    invoke-static {v1}, Lbr/com/itau/security/commons/binary/ByteUtils;->from(Ljava/lang/String;)[B

    move-result-object v2

    .line 3119
    sget-object v1, Ldid/f;->b:Ljava/lang/String;

    invoke-static {v1}, Lbr/com/itau/security/commons/binary/ByteUtils;->from(Ljava/lang/String;)[B

    move-result-object v3

    .line 3120
    invoke-static {p1, v2, v3}, Lbr/com/itau/security/commons/Crypto;->pkcs7PaddingEncrypt([B[B[B)[B

    move-result-object v1

    .line 3121
    invoke-static {v1}, Lbr/com/itau/security/commons/binary/ByteUtils;->b64Encode([B)Ljava/lang/String;

    move-result-object v1

    .line 58
    invoke-interface {v0, v1, p2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    #const-string p0, "HACKZINHO"

    #invoke-static {p0, p1}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    .line 59
    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2a

    return-object p2

    .line 3125
    :cond_2a
    invoke-static {p1}, Lbr/com/itau/security/commons/binary/ByteUtils;->b64Decode(Ljava/lang/String;)[B

    move-result-object p1

    .line 3126
    sget-object v0, Ldid/f;->a:Ljava/lang/String;

    invoke-static {v0}, Lbr/com/itau/security/commons/binary/ByteUtils;->from(Ljava/lang/String;)[B

    move-result-object p2

    .line 3127
    sget-object v0, Ldid/f;->b:Ljava/lang/String;

    invoke-static {v0}, Lbr/com/itau/security/commons/binary/ByteUtils;->from(Ljava/lang/String;)[B

    move-result-object v2

    .line 3128
    invoke-static {p1, p2, v2}, Lbr/com/itau/security/commons/Crypto;->pkcs7PaddingDecrypt([B[B[B)[B

    move-result-object v0

    #const-string p0, "HACKZINHO666"

    invoke-static {v0}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    move-result-object v3

    #invoke-static {p0, v3}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {v0}, Lbr/com/itau/security/commons/binary/ByteUtils;->to([B)Ljava/lang/String;

    move-result-object v0

    .line 59
    return-object v0
.end method

.method public final getStringSet(Ljava/lang/String;Ljava/util/Set;)Ljava/util/Set;
    .registers 8

    .line 64
    iget-object v0, p0, Ldid/f;->c:Landroid/content/SharedPreferences;

    .line 4117
    invoke-static {p1}, Lbr/com/itau/security/commons/binary/ByteUtils;->from(Ljava/lang/String;)[B

    move-result-object p1

    .line 4118
    sget-object v1, Ldid/f;->a:Ljava/lang/String;

    invoke-static {v1}, Lbr/com/itau/security/commons/binary/ByteUtils;->from(Ljava/lang/String;)[B

    move-result-object v2

    .line 4119
    sget-object v1, Ldid/f;->b:Ljava/lang/String;

    invoke-static {v1}, Lbr/com/itau/security/commons/binary/ByteUtils;->from(Ljava/lang/String;)[B

    move-result-object v3

    .line 4120
    invoke-static {p1, v2, v3}, Lbr/com/itau/security/commons/Crypto;->pkcs7PaddingEncrypt([B[B[B)[B

    move-result-object v1

    .line 4121
    invoke-static {v1}, Lbr/com/itau/security/commons/binary/ByteUtils;->b64Encode([B)Ljava/lang/String;

    move-result-object v1

    .line 64
    invoke-interface {v0, v1, p2}, Landroid/content/SharedPreferences;->getStringSet(Ljava/lang/String;Ljava/util/Set;)Ljava/util/Set;

    move-result-object p1

    .line 65
    invoke-interface {p1, p2}, Ljava/util/Set;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_25

    .line 66
    return-object p2

    .line 68
    :cond_25
    new-instance p2, Ljava/util/HashSet;

    invoke-direct {p2}, Ljava/util/HashSet;-><init>()V

    .line 70
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_2e
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_57

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Ljava/lang/String;

    .line 71
    .line 4125
    invoke-static {v2}, Lbr/com/itau/security/commons/binary/ByteUtils;->b64Decode(Ljava/lang/String;)[B

    move-result-object v2

    .line 4126
    sget-object v0, Ldid/f;->a:Ljava/lang/String;

    invoke-static {v0}, Lbr/com/itau/security/commons/binary/ByteUtils;->from(Ljava/lang/String;)[B

    move-result-object v3

    .line 4127
    sget-object v0, Ldid/f;->b:Ljava/lang/String;

    invoke-static {v0}, Lbr/com/itau/security/commons/binary/ByteUtils;->from(Ljava/lang/String;)[B

    move-result-object v4

    .line 4128
    invoke-static {v2, v3, v4}, Lbr/com/itau/security/commons/Crypto;->pkcs7PaddingDecrypt([B[B[B)[B

    move-result-object v0

    invoke-static {v0}, Lbr/com/itau/security/commons/binary/ByteUtils;->to([B)Ljava/lang/String;

    move-result-object v0

    .line 71
    invoke-interface {p2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_2e

    .line 73
    :cond_57
    return-object p2
.end method

.method public final registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V
    .registers 3

    .line 108
    iget-object v0, p0, Ldid/f;->c:Landroid/content/SharedPreferences;

    invoke-interface {v0, p1}, Landroid/content/SharedPreferences;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 109
    return-void
.end method

.method public final unregisterOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V
    .registers 3

    .line 113
    iget-object v0, p0, Ldid/f;->c:Landroid/content/SharedPreferences;

    invoke-interface {v0, p1}, Landroid/content/SharedPreferences;->unregisterOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 114
    return-void
.end method

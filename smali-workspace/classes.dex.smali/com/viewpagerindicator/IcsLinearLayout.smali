.class Lcom/viewpagerindicator/IcsLinearLayout;
.super Landroid/widget/LinearLayout;
.source "IcsLinearLayout.java"


# static fields
.field private static final LL:[I


# instance fields
.field private mDivider:Landroid/graphics/drawable/Drawable;

.field private mDividerHeight:I

.field private mDividerPadding:I

.field private mDividerWidth:I

.field private mShowDividers:I


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .line 18
    const/4 v0, 0x3

    new-array v0, v0, [I

    fill-array-data v0, :array_a

    sput-object v0, Lcom/viewpagerindicator/IcsLinearLayout;->LL:[I

    return-void

    nop

    :array_a
    .array-data 4
        0x1010129
        0x1010329
        0x101032a
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .registers 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "themeAttr"    # I

    .line 35
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 37
    sget-object v0, Lcom/viewpagerindicator/IcsLinearLayout;->LL:[I

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v0, p2, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v3

    .line 38
    .local v3, "a":Landroid/content/res/TypedArray;
    const/4 v0, 0x0

    invoke-virtual {v3, v0}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/viewpagerindicator/IcsLinearLayout;->setDividerDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 39
    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-virtual {v3, v0, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    iput v0, p0, Lcom/viewpagerindicator/IcsLinearLayout;->mDividerPadding:I

    .line 40
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {v3, v0, v1}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v0

    iput v0, p0, Lcom/viewpagerindicator/IcsLinearLayout;->mShowDividers:I

    .line 41
    invoke-virtual {v3}, Landroid/content/res/TypedArray;->recycle()V

    .line 42
    return-void
.end method

.method private drawDividersHorizontal(Landroid/graphics/Canvas;)V
    .registers 9
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .line 128
    invoke-virtual {p0}, Lcom/viewpagerindicator/IcsLinearLayout;->getChildCount()I

    move-result v2

    .line 129
    .local v2, "count":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_5
    if-ge v3, v2, :cond_30

    .line 130
    invoke-virtual {p0, v3}, Lcom/viewpagerindicator/IcsLinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 132
    .local v4, "child":Landroid/view/View;
    if-eqz v4, :cond_2d

    invoke-virtual {v4}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-eq v0, v1, :cond_2d

    .line 133
    invoke-direct {p0, v3}, Lcom/viewpagerindicator/IcsLinearLayout;->hasDividerBeforeChildAt(I)Z

    move-result v0

    if-eqz v0, :cond_2d

    .line 134
    invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Landroid/widget/LinearLayout$LayoutParams;

    .line 135
    .local v5, "lp":Landroid/widget/LinearLayout$LayoutParams;
    invoke-virtual {v4}, Landroid/view/View;->getLeft()I

    move-result v0

    iget v1, v5, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    sub-int v6, v0, v1

    .line 136
    .local v6, "left":I
    invoke-direct {p0, p1, v6}, Lcom/viewpagerindicator/IcsLinearLayout;->drawVerticalDivider(Landroid/graphics/Canvas;I)V

    .line 129
    .end local v4    # "child":Landroid/view/View;
    .end local v5    # "lp":Landroid/widget/LinearLayout$LayoutParams;
    .end local v6    # "left":I
    :cond_2d
    add-int/lit8 v3, v3, 0x1

    goto :goto_5

    .line 141
    .end local v3    # "i":I
    :cond_30
    invoke-direct {p0, v2}, Lcom/viewpagerindicator/IcsLinearLayout;->hasDividerBeforeChildAt(I)Z

    move-result v0

    if-eqz v0, :cond_54

    .line 142
    add-int/lit8 v0, v2, -0x1

    invoke-virtual {p0, v0}, Lcom/viewpagerindicator/IcsLinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 143
    .local v3, "child":Landroid/view/View;
    const/4 v4, 0x0

    .line 144
    .local v4, "right":I
    if-nez v3, :cond_4d

    .line 145
    invoke-virtual {p0}, Lcom/viewpagerindicator/IcsLinearLayout;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Lcom/viewpagerindicator/IcsLinearLayout;->getPaddingRight()I

    move-result v1

    sub-int/2addr v0, v1

    iget v1, p0, Lcom/viewpagerindicator/IcsLinearLayout;->mDividerWidth:I

    sub-int v4, v0, v1

    goto :goto_51

    .line 148
    :cond_4d
    invoke-virtual {v3}, Landroid/view/View;->getRight()I

    move-result v4

    .line 150
    :goto_51
    invoke-direct {p0, p1, v4}, Lcom/viewpagerindicator/IcsLinearLayout;->drawVerticalDivider(Landroid/graphics/Canvas;I)V

    .line 152
    .end local v3    # "child":Landroid/view/View;
    .end local v4    # "right":I
    :cond_54
    return-void
.end method

.method private drawDividersVertical(Landroid/graphics/Canvas;)V
    .registers 9
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .line 101
    invoke-virtual {p0}, Lcom/viewpagerindicator/IcsLinearLayout;->getChildCount()I

    move-result v2

    .line 102
    .local v2, "count":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_5
    if-ge v3, v2, :cond_30

    .line 103
    invoke-virtual {p0, v3}, Lcom/viewpagerindicator/IcsLinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 105
    .local v4, "child":Landroid/view/View;
    if-eqz v4, :cond_2d

    invoke-virtual {v4}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-eq v0, v1, :cond_2d

    .line 106
    invoke-direct {p0, v3}, Lcom/viewpagerindicator/IcsLinearLayout;->hasDividerBeforeChildAt(I)Z

    move-result v0

    if-eqz v0, :cond_2d

    .line 107
    invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Landroid/widget/LinearLayout$LayoutParams;

    .line 108
    .local v5, "lp":Landroid/widget/LinearLayout$LayoutParams;
    invoke-virtual {v4}, Landroid/view/View;->getTop()I

    move-result v0

    iget v1, v5, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    sub-int v6, v0, v1

    .line 109
    .local v6, "top":I
    invoke-direct {p0, p1, v6}, Lcom/viewpagerindicator/IcsLinearLayout;->drawHorizontalDivider(Landroid/graphics/Canvas;I)V

    .line 102
    .end local v4    # "child":Landroid/view/View;
    .end local v5    # "lp":Landroid/widget/LinearLayout$LayoutParams;
    .end local v6    # "top":I
    :cond_2d
    add-int/lit8 v3, v3, 0x1

    goto :goto_5

    .line 114
    .end local v3    # "i":I
    :cond_30
    invoke-direct {p0, v2}, Lcom/viewpagerindicator/IcsLinearLayout;->hasDividerBeforeChildAt(I)Z

    move-result v0

    if-eqz v0, :cond_54

    .line 115
    add-int/lit8 v0, v2, -0x1

    invoke-virtual {p0, v0}, Lcom/viewpagerindicator/IcsLinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 116
    .local v3, "child":Landroid/view/View;
    const/4 v4, 0x0

    .line 117
    .local v4, "bottom":I
    if-nez v3, :cond_4d

    .line 118
    invoke-virtual {p0}, Lcom/viewpagerindicator/IcsLinearLayout;->getHeight()I

    move-result v0

    invoke-virtual {p0}, Lcom/viewpagerindicator/IcsLinearLayout;->getPaddingBottom()I

    move-result v1

    sub-int/2addr v0, v1

    iget v1, p0, Lcom/viewpagerindicator/IcsLinearLayout;->mDividerHeight:I

    sub-int v4, v0, v1

    goto :goto_51

    .line 121
    :cond_4d
    invoke-virtual {v3}, Landroid/view/View;->getBottom()I

    move-result v4

    .line 123
    :goto_51
    invoke-direct {p0, p1, v4}, Lcom/viewpagerindicator/IcsLinearLayout;->drawHorizontalDivider(Landroid/graphics/Canvas;I)V

    .line 125
    .end local v3    # "child":Landroid/view/View;
    .end local v4    # "bottom":I
    :cond_54
    return-void
.end method

.method private drawHorizontalDivider(Landroid/graphics/Canvas;I)V
    .registers 7
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "top"    # I

    .line 155
    iget-object v0, p0, Lcom/viewpagerindicator/IcsLinearLayout;->mDivider:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Lcom/viewpagerindicator/IcsLinearLayout;->getPaddingLeft()I

    move-result v1

    iget v2, p0, Lcom/viewpagerindicator/IcsLinearLayout;->mDividerPadding:I

    add-int/2addr v1, v2

    .line 156
    invoke-virtual {p0}, Lcom/viewpagerindicator/IcsLinearLayout;->getWidth()I

    move-result v2

    invoke-virtual {p0}, Lcom/viewpagerindicator/IcsLinearLayout;->getPaddingRight()I

    move-result v3

    sub-int/2addr v2, v3

    iget v3, p0, Lcom/viewpagerindicator/IcsLinearLayout;->mDividerPadding:I

    sub-int/2addr v2, v3

    iget v3, p0, Lcom/viewpagerindicator/IcsLinearLayout;->mDividerHeight:I

    add-int/2addr v3, p2

    .line 155
    invoke-virtual {v0, v1, p2, v2, v3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 157
    iget-object v0, p0, Lcom/viewpagerindicator/IcsLinearLayout;->mDivider:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 158
    return-void
.end method

.method private drawVerticalDivider(Landroid/graphics/Canvas;I)V
    .registers 8
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "left"    # I

    .line 161
    iget-object v0, p0, Lcom/viewpagerindicator/IcsLinearLayout;->mDivider:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Lcom/viewpagerindicator/IcsLinearLayout;->getPaddingTop()I

    move-result v1

    iget v2, p0, Lcom/viewpagerindicator/IcsLinearLayout;->mDividerPadding:I

    add-int/2addr v1, v2

    iget v2, p0, Lcom/viewpagerindicator/IcsLinearLayout;->mDividerWidth:I

    add-int/2addr v2, p2

    .line 162
    invoke-virtual {p0}, Lcom/viewpagerindicator/IcsLinearLayout;->getHeight()I

    move-result v3

    invoke-virtual {p0}, Lcom/viewpagerindicator/IcsLinearLayout;->getPaddingBottom()I

    move-result v4

    sub-int/2addr v3, v4

    iget v4, p0, Lcom/viewpagerindicator/IcsLinearLayout;->mDividerPadding:I

    sub-int/2addr v3, v4

    .line 161
    invoke-virtual {v0, p2, v1, v2, v3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 163
    iget-object v0, p0, Lcom/viewpagerindicator/IcsLinearLayout;->mDivider:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 164
    return-void
.end method

.method private hasDividerBeforeChildAt(I)Z
    .registers 6
    .param p1, "childIndex"    # I

    .line 167
    if-eqz p1, :cond_8

    invoke-virtual {p0}, Lcom/viewpagerindicator/IcsLinearLayout;->getChildCount()I

    move-result v0

    if-ne p1, v0, :cond_a

    .line 168
    :cond_8
    const/4 v0, 0x0

    return v0

    .line 170
    :cond_a
    iget v0, p0, Lcom/viewpagerindicator/IcsLinearLayout;->mShowDividers:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_27

    .line 171
    const/4 v2, 0x0

    .line 172
    .local v2, "hasVisibleViewBefore":Z
    add-int/lit8 v3, p1, -0x1

    .local v3, "i":I
    :goto_13
    if-ltz v3, :cond_26

    .line 173
    invoke-virtual {p0, v3}, Lcom/viewpagerindicator/IcsLinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-eq v0, v1, :cond_23

    .line 174
    const/4 v2, 0x1

    .line 175
    goto :goto_26

    .line 172
    :cond_23
    add-int/lit8 v3, v3, -0x1

    goto :goto_13

    .line 178
    .end local v3    # "i":I
    :cond_26
    :goto_26
    return v2

    .line 180
    .end local v2    # "hasVisibleViewBefore":Z
    :cond_27
    const/4 v0, 0x0

    return v0
.end method


# virtual methods
.method protected measureChildWithMargins(Landroid/view/View;IIII)V
    .registers 11
    .param p1, "child"    # Landroid/view/View;
    .param p2, "parentWidthMeasureSpec"    # I
    .param p3, "widthUsed"    # I
    .param p4, "parentHeightMeasureSpec"    # I
    .param p5, "heightUsed"    # I

    .line 62
    invoke-virtual {p0, p1}, Lcom/viewpagerindicator/IcsLinearLayout;->indexOfChild(Landroid/view/View;)I

    move-result v1

    .line 63
    .local v1, "index":I
    invoke-virtual {p0}, Lcom/viewpagerindicator/IcsLinearLayout;->getOrientation()I

    move-result v2

    .line 64
    .local v2, "orientation":I
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Landroid/widget/LinearLayout$LayoutParams;

    .line 65
    .local v3, "params":Landroid/widget/LinearLayout$LayoutParams;
    invoke-direct {p0, v1}, Lcom/viewpagerindicator/IcsLinearLayout;->hasDividerBeforeChildAt(I)Z

    move-result v0

    if-eqz v0, :cond_21

    .line 66
    const/4 v0, 0x1

    if-ne v2, v0, :cond_1d

    .line 68
    iget v0, p0, Lcom/viewpagerindicator/IcsLinearLayout;->mDividerHeight:I

    iput v0, v3, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    goto :goto_21

    .line 71
    :cond_1d
    iget v0, p0, Lcom/viewpagerindicator/IcsLinearLayout;->mDividerWidth:I

    iput v0, v3, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    .line 75
    :cond_21
    :goto_21
    invoke-virtual {p0}, Lcom/viewpagerindicator/IcsLinearLayout;->getChildCount()I

    move-result v4

    .line 76
    .local v4, "count":I
    add-int/lit8 v0, v4, -0x1

    if-ne v1, v0, :cond_3b

    .line 77
    invoke-direct {p0, v4}, Lcom/viewpagerindicator/IcsLinearLayout;->hasDividerBeforeChildAt(I)Z

    move-result v0

    if-eqz v0, :cond_3b

    .line 78
    const/4 v0, 0x1

    if-ne v2, v0, :cond_37

    .line 79
    iget v0, p0, Lcom/viewpagerindicator/IcsLinearLayout;->mDividerHeight:I

    iput v0, v3, Landroid/widget/LinearLayout$LayoutParams;->bottomMargin:I

    goto :goto_3b

    .line 81
    :cond_37
    iget v0, p0, Lcom/viewpagerindicator/IcsLinearLayout;->mDividerWidth:I

    iput v0, v3, Landroid/widget/LinearLayout$LayoutParams;->rightMargin:I

    .line 85
    :cond_3b
    :goto_3b
    invoke-super/range {p0 .. p5}, Landroid/widget/LinearLayout;->measureChildWithMargins(Landroid/view/View;IIII)V

    .line 86
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .registers 4
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .line 90
    iget-object v0, p0, Lcom/viewpagerindicator/IcsLinearLayout;->mDivider:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_12

    .line 91
    invoke-virtual {p0}, Lcom/viewpagerindicator/IcsLinearLayout;->getOrientation()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_f

    .line 92
    invoke-direct {p0, p1}, Lcom/viewpagerindicator/IcsLinearLayout;->drawDividersVertical(Landroid/graphics/Canvas;)V

    goto :goto_12

    .line 94
    :cond_f
    invoke-direct {p0, p1}, Lcom/viewpagerindicator/IcsLinearLayout;->drawDividersHorizontal(Landroid/graphics/Canvas;)V

    .line 97
    :cond_12
    :goto_12
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onDraw(Landroid/graphics/Canvas;)V

    .line 98
    return-void
.end method

.method public setDividerDrawable(Landroid/graphics/drawable/Drawable;)V
    .registers 3
    .param p1, "divider"    # Landroid/graphics/drawable/Drawable;

    .line 45
    iget-object v0, p0, Lcom/viewpagerindicator/IcsLinearLayout;->mDivider:Landroid/graphics/drawable/Drawable;

    if-ne p1, v0, :cond_5

    .line 46
    return-void

    .line 48
    :cond_5
    iput-object p1, p0, Lcom/viewpagerindicator/IcsLinearLayout;->mDivider:Landroid/graphics/drawable/Drawable;

    .line 49
    if-eqz p1, :cond_16

    .line 50
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    iput v0, p0, Lcom/viewpagerindicator/IcsLinearLayout;->mDividerWidth:I

    .line 51
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v0

    iput v0, p0, Lcom/viewpagerindicator/IcsLinearLayout;->mDividerHeight:I

    goto :goto_1c

    .line 53
    :cond_16
    const/4 v0, 0x0

    iput v0, p0, Lcom/viewpagerindicator/IcsLinearLayout;->mDividerWidth:I

    .line 54
    const/4 v0, 0x0

    iput v0, p0, Lcom/viewpagerindicator/IcsLinearLayout;->mDividerHeight:I

    .line 56
    :goto_1c
    if-nez p1, :cond_20

    const/4 v0, 0x1

    goto :goto_21

    :cond_20
    const/4 v0, 0x0

    :goto_21
    invoke-virtual {p0, v0}, Lcom/viewpagerindicator/IcsLinearLayout;->setWillNotDraw(Z)V

    .line 57
    invoke-virtual {p0}, Lcom/viewpagerindicator/IcsLinearLayout;->requestLayout()V

    .line 58
    return-void
.end method

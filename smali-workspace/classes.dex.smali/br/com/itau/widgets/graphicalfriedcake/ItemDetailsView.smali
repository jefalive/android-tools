.class public Lbr/com/itau/widgets/graphicalfriedcake/ItemDetailsView;
.super Landroid/widget/LinearLayout;
.source "ItemDetailsView.java"


# instance fields
.field textDetailDescription:Landroid/widget/TextView;

.field textDetailName:Landroid/widget/TextView;

.field textItemCurrency:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 2
    .param p1, "context"    # Landroid/content/Context;

    .line 17
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 18
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .line 21
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 22
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .line 25
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 26
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .registers 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I
    .param p4, "defStyleRes"    # I
    .annotation build Landroid/annotation/TargetApi;
        value = 0x15
    .end annotation

    .line 42
    invoke-direct {p0, p1, p2, p3, p4}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    .line 43
    return-void
.end method


# virtual methods
.method public init(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 7
    .param p1, "detailName"    # Ljava/lang/String;
    .param p2, "itemCurrency"    # Ljava/lang/String;
    .param p3, "detailDescription"    # Ljava/lang/String;
    .param p4, "color"    # Ljava/lang/String;

    .line 30
    invoke-virtual {p0}, Lbr/com/itau/widgets/graphicalfriedcake/ItemDetailsView;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lbr/com/itau/widgets/graphicalfriedcake/R$layout;->view_item_details:I

    invoke-static {v0, v1, p0}, Lbr/com/itau/widgets/graphicalfriedcake/ItemDetailsView;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 31
    sget v0, Lbr/com/itau/widgets/graphicalfriedcake/R$id;->text_detail_name:I

    invoke-virtual {p0, v0}, Lbr/com/itau/widgets/graphicalfriedcake/ItemDetailsView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/ItemDetailsView;->textDetailName:Landroid/widget/TextView;

    .line 32
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/ItemDetailsView;->textDetailName:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 33
    sget v0, Lbr/com/itau/widgets/graphicalfriedcake/R$id;->text_item_currency:I

    invoke-virtual {p0, v0}, Lbr/com/itau/widgets/graphicalfriedcake/ItemDetailsView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/ItemDetailsView;->textItemCurrency:Landroid/widget/TextView;

    .line 34
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/ItemDetailsView;->textItemCurrency:Landroid/widget/TextView;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 35
    sget v0, Lbr/com/itau/widgets/graphicalfriedcake/R$id;->text_detail_description:I

    invoke-virtual {p0, v0}, Lbr/com/itau/widgets/graphicalfriedcake/ItemDetailsView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/ItemDetailsView;->textDetailDescription:Landroid/widget/TextView;

    .line 36
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/ItemDetailsView;->textDetailDescription:Landroid/widget/TextView;

    invoke-virtual {v0, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 37
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/ItemDetailsView;->textDetailName:Landroid/widget/TextView;

    invoke-static {p4}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 38
    return-void
.end method

.method protected onLayout(ZIIII)V
    .registers 6
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .line 47
    invoke-super/range {p0 .. p5}, Landroid/widget/LinearLayout;->onLayout(ZIIII)V

    .line 48
    return-void
.end method

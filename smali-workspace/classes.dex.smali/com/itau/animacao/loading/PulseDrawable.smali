.class public Lcom/itau/animacao/loading/PulseDrawable;
.super Landroid/graphics/drawable/Drawable;
.source "PulseDrawable.java"


# instance fields
.field private centerPaint:Landroid/graphics/Paint;

.field private final color:I

.field private final corPreenchimento:I

.field private currentAlphaAnimationValue:I

.field private currentExpandAnimationValue:F

.field private fullSizeRadius:F

.field private pulsePaint:Landroid/graphics/Paint;


# direct methods
.method public constructor <init>(II)V
    .registers 4
    .param p1, "color"    # I
    .param p2, "corPreenchimento"    # I

    .line 30
    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    .line 27
    const/4 v0, 0x0

    iput v0, p0, Lcom/itau/animacao/loading/PulseDrawable;->currentExpandAnimationValue:F

    .line 28
    const/16 v0, 0xff

    iput v0, p0, Lcom/itau/animacao/loading/PulseDrawable;->currentAlphaAnimationValue:I

    .line 31
    iput p1, p0, Lcom/itau/animacao/loading/PulseDrawable;->color:I

    .line 32
    iput p2, p0, Lcom/itau/animacao/loading/PulseDrawable;->corPreenchimento:I

    .line 33
    invoke-direct {p0}, Lcom/itau/animacao/loading/PulseDrawable;->initializeDrawable()V

    .line 34
    return-void
.end method

.method static synthetic access$000(Lcom/itau/animacao/loading/PulseDrawable;)F
    .registers 2
    .param p0, "x0"    # Lcom/itau/animacao/loading/PulseDrawable;

    .line 17
    iget v0, p0, Lcom/itau/animacao/loading/PulseDrawable;->currentExpandAnimationValue:F

    return v0
.end method

.method static synthetic access$002(Lcom/itau/animacao/loading/PulseDrawable;F)F
    .registers 2
    .param p0, "x0"    # Lcom/itau/animacao/loading/PulseDrawable;
    .param p1, "x1"    # F

    .line 17
    iput p1, p0, Lcom/itau/animacao/loading/PulseDrawable;->currentExpandAnimationValue:F

    return p1
.end method

.method static synthetic access$102(Lcom/itau/animacao/loading/PulseDrawable;I)I
    .registers 2
    .param p0, "x0"    # Lcom/itau/animacao/loading/PulseDrawable;
    .param p1, "x1"    # I

    .line 17
    iput p1, p0, Lcom/itau/animacao/loading/PulseDrawable;->currentAlphaAnimationValue:I

    return p1
.end method

.method private calculateFullSizeRadius()V
    .registers 5

    .line 148
    invoke-virtual {p0}, Lcom/itau/animacao/loading/PulseDrawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v2

    .line 149
    .local v2, "bounds":Landroid/graphics/Rect;
    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v0

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    int-to-float v3, v0

    .line 150
    .local v3, "minimumDiameter":F
    const/high16 v0, 0x40000000    # 2.0f

    div-float v0, v3, v0

    iput v0, p0, Lcom/itau/animacao/loading/PulseDrawable;->fullSizeRadius:F

    .line 151
    return-void
.end method

.method private getPulseColor()I
    .registers 5

    .line 142
    iget v0, p0, Lcom/itau/animacao/loading/PulseDrawable;->currentAlphaAnimationValue:I

    iget v1, p0, Lcom/itau/animacao/loading/PulseDrawable;->color:I

    invoke-static {v1}, Landroid/graphics/Color;->red(I)I

    move-result v1

    iget v2, p0, Lcom/itau/animacao/loading/PulseDrawable;->color:I

    .line 143
    invoke-static {v2}, Landroid/graphics/Color;->green(I)I

    move-result v2

    iget v3, p0, Lcom/itau/animacao/loading/PulseDrawable;->color:I

    .line 144
    invoke-static {v3}, Landroid/graphics/Color;->blue(I)I

    move-result v3

    .line 142
    invoke-static {v0, v1, v2, v3}, Landroid/graphics/Color;->argb(IIII)I

    move-result v0

    return v0
.end method

.method private initializeDrawable()V
    .registers 1

    .line 37
    invoke-direct {p0}, Lcom/itau/animacao/loading/PulseDrawable;->preparePaints()V

    .line 38
    invoke-direct {p0}, Lcom/itau/animacao/loading/PulseDrawable;->prepareAnimation()V

    .line 39
    return-void
.end method

.method private prepareAnimation()V
    .registers 7

    .line 42
    const/4 v0, 0x2

    new-array v0, v0, [F

    fill-array-data v0, :array_5c

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v3

    .line 43
    .local v3, "expandAnimator":Landroid/animation/ValueAnimator;
    const/4 v0, -0x1

    invoke-virtual {v3, v0}, Landroid/animation/ValueAnimator;->setRepeatCount(I)V

    .line 44
    const/4 v0, 0x1

    invoke-virtual {v3, v0}, Landroid/animation/ValueAnimator;->setRepeatMode(I)V

    .line 45
    new-instance v0, Lcom/itau/animacao/loading/PulseDrawable$1;

    invoke-direct {v0, p0}, Lcom/itau/animacao/loading/PulseDrawable$1;-><init>(Lcom/itau/animacao/loading/PulseDrawable;)V

    invoke-virtual {v3, v0}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 55
    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_64

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofInt([I)Landroid/animation/ValueAnimator;

    move-result-object v4

    .line 56
    .local v4, "alphaAnimator":Landroid/animation/ValueAnimator;
    const-wide/16 v0, 0x177

    invoke-virtual {v4, v0, v1}, Landroid/animation/ValueAnimator;->setStartDelay(J)V

    .line 57
    const/4 v0, -0x1

    invoke-virtual {v4, v0}, Landroid/animation/ValueAnimator;->setRepeatCount(I)V

    .line 58
    const/4 v0, 0x1

    invoke-virtual {v4, v0}, Landroid/animation/ValueAnimator;->setRepeatMode(I)V

    .line 59
    new-instance v0, Lcom/itau/animacao/loading/PulseDrawable$2;

    invoke-direct {v0, p0}, Lcom/itau/animacao/loading/PulseDrawable$2;-><init>(Lcom/itau/animacao/loading/PulseDrawable;)V

    invoke-virtual {v4, v0}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 65
    new-instance v5, Landroid/animation/AnimatorSet;

    invoke-direct {v5}, Landroid/animation/AnimatorSet;-><init>()V

    .line 66
    .local v5, "animation":Landroid/animation/AnimatorSet;
    const/4 v0, 0x2

    new-array v0, v0, [Landroid/animation/Animator;

    const/4 v1, 0x0

    aput-object v3, v0, v1

    const/4 v1, 0x1

    aput-object v4, v0, v1

    invoke-virtual {v5, v0}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    .line 67
    const-wide/16 v0, 0x5dc

    invoke-virtual {v5, v0, v1}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    .line 68
    new-instance v0, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {v5, v0}, Landroid/animation/AnimatorSet;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 69
    invoke-virtual {v5}, Landroid/animation/AnimatorSet;->start()V

    .line 70
    return-void

    nop

    :array_5c
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data

    :array_64
    .array-data 4
        0xff
        0x0
    .end array-data
.end method

.method private preparePaintShader()V
    .registers 15

    .line 125
    invoke-virtual {p0}, Lcom/itau/animacao/loading/PulseDrawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v8

    .line 126
    .local v8, "bounds":Landroid/graphics/Rect;
    invoke-virtual {v8}, Landroid/graphics/Rect;->exactCenterX()F

    move-result v9

    .line 127
    .local v9, "centerX":F
    invoke-virtual {v8}, Landroid/graphics/Rect;->exactCenterY()F

    move-result v10

    .line 128
    .local v10, "centerY":F
    invoke-virtual {v8}, Landroid/graphics/Rect;->width()I

    move-result v0

    invoke-virtual {v8}, Landroid/graphics/Rect;->height()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    int-to-float v11, v0

    .line 129
    .local v11, "radius":F
    const/4 v0, 0x0

    cmpl-float v0, v11, v0

    if-lez v0, :cond_4d

    .line 130
    invoke-direct {p0}, Lcom/itau/animacao/loading/PulseDrawable;->getPulseColor()I

    move-result v12

    .line 131
    .local v12, "edgeColor":I
    iget v0, p0, Lcom/itau/animacao/loading/PulseDrawable;->color:I

    invoke-static {v0}, Landroid/graphics/Color;->red(I)I

    move-result v0

    iget v1, p0, Lcom/itau/animacao/loading/PulseDrawable;->color:I

    .line 132
    invoke-static {v1}, Landroid/graphics/Color;->green(I)I

    move-result v1

    iget v2, p0, Lcom/itau/animacao/loading/PulseDrawable;->color:I

    .line 133
    invoke-static {v2}, Landroid/graphics/Color;->blue(I)I

    move-result v2

    .line 131
    const/4 v3, 0x0

    invoke-static {v3, v0, v1, v2}, Landroid/graphics/Color;->argb(IIII)I

    move-result v13

    .line 134
    .local v13, "centerColor":I
    iget-object v0, p0, Lcom/itau/animacao/loading/PulseDrawable;->pulsePaint:Landroid/graphics/Paint;

    new-instance v1, Landroid/graphics/RadialGradient;

    sget-object v7, Landroid/graphics/Shader$TileMode;->CLAMP:Landroid/graphics/Shader$TileMode;

    move v2, v9

    move v3, v10

    move v4, v11

    move v5, v13

    move v6, v12

    invoke-direct/range {v1 .. v7}, Landroid/graphics/RadialGradient;-><init>(FFFIILandroid/graphics/Shader$TileMode;)V

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    .line 136
    .end local v12    # "edgeColor":I
    .end local v13    # "centerColor":I
    goto :goto_53

    .line 137
    :cond_4d
    iget-object v0, p0, Lcom/itau/animacao/loading/PulseDrawable;->pulsePaint:Landroid/graphics/Paint;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    .line 139
    :goto_53
    return-void
.end method

.method private preparePaints()V
    .registers 3

    .line 73
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/itau/animacao/loading/PulseDrawable;->pulsePaint:Landroid/graphics/Paint;

    .line 74
    iget-object v0, p0, Lcom/itau/animacao/loading/PulseDrawable;->pulsePaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 75
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/itau/animacao/loading/PulseDrawable;->centerPaint:Landroid/graphics/Paint;

    .line 76
    iget-object v0, p0, Lcom/itau/animacao/loading/PulseDrawable;->centerPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 77
    iget-object v0, p0, Lcom/itau/animacao/loading/PulseDrawable;->centerPaint:Landroid/graphics/Paint;

    iget v1, p0, Lcom/itau/animacao/loading/PulseDrawable;->corPreenchimento:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 78
    return-void
.end method

.method private renderCenterArea(Landroid/graphics/Canvas;FF)V
    .registers 11
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "centerX"    # F
    .param p3, "centerY"    # F

    .line 112
    iget v0, p0, Lcom/itau/animacao/loading/PulseDrawable;->fullSizeRadius:F

    const v1, 0x3f4ccccd    # 0.8f

    mul-float v2, v0, v1

    .line 113
    .local v2, "currentCenterAreaRadius":F
    const/4 v0, 0x0

    cmpl-float v0, v2, v0

    if-lez v0, :cond_22

    .line 114
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 115
    sub-float v3, p2, v2

    .line 116
    .local v3, "left":F
    sub-float v4, p3, v2

    .line 117
    .local v4, "top":F
    add-float v5, p2, v2

    .line 118
    .local v5, "right":F
    add-float v6, p3, v2

    .line 119
    .local v6, "bottom":F
    invoke-virtual {p1, v3, v4, v5, v6}, Landroid/graphics/Canvas;->clipRect(FFFF)Z

    .line 120
    iget-object v0, p0, Lcom/itau/animacao/loading/PulseDrawable;->centerPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, p2, p3, v2, v0}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 121
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 123
    .end local v3    # "left":F
    .end local v4    # "top":F
    .end local v5    # "right":F
    .end local v6    # "bottom":F
    :cond_22
    return-void
.end method

.method private renderPulse(Landroid/graphics/Canvas;FF)V
    .registers 7
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "centerX"    # F
    .param p3, "centerY"    # F

    .line 105
    iget v0, p0, Lcom/itau/animacao/loading/PulseDrawable;->fullSizeRadius:F

    iget v1, p0, Lcom/itau/animacao/loading/PulseDrawable;->currentExpandAnimationValue:F

    mul-float v2, v0, v1

    .line 106
    .local v2, "currentRadius":F
    const/4 v0, 0x0

    cmpl-float v0, v2, v0

    if-lez v0, :cond_10

    .line 107
    iget-object v0, p0, Lcom/itau/animacao/loading/PulseDrawable;->pulsePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, p2, p3, v2, v0}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 109
    :cond_10
    return-void
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;)V
    .registers 5
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .line 95
    invoke-virtual {p0}, Lcom/itau/animacao/loading/PulseDrawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    .line 96
    .local v0, "bounds":Landroid/graphics/Rect;
    invoke-virtual {v0}, Landroid/graphics/Rect;->exactCenterX()F

    move-result v1

    .line 97
    .local v1, "centerX":F
    invoke-virtual {v0}, Landroid/graphics/Rect;->exactCenterY()F

    move-result v2

    .line 98
    .local v2, "centerY":F
    invoke-direct {p0}, Lcom/itau/animacao/loading/PulseDrawable;->calculateFullSizeRadius()V

    .line 99
    invoke-direct {p0}, Lcom/itau/animacao/loading/PulseDrawable;->preparePaintShader()V

    .line 100
    invoke-direct {p0, p1, v1, v2}, Lcom/itau/animacao/loading/PulseDrawable;->renderPulse(Landroid/graphics/Canvas;FF)V

    .line 101
    invoke-direct {p0, p1, v1, v2}, Lcom/itau/animacao/loading/PulseDrawable;->renderCenterArea(Landroid/graphics/Canvas;FF)V

    .line 102
    return-void
.end method

.method public getOpacity()I
    .registers 2

    .line 90
    const/4 v0, -0x1

    return v0
.end method

.method public setAlpha(I)V
    .registers 3
    .param p1, "alpha"    # I

    .line 82
    iget-object v0, p0, Lcom/itau/animacao/loading/PulseDrawable;->pulsePaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 83
    return-void
.end method

.method public setColorFilter(Landroid/graphics/ColorFilter;)V
    .registers 2
    .param p1, "colorFilter"    # Landroid/graphics/ColorFilter;

    .line 86
    return-void
.end method

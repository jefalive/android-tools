.class final Lcom/google/zxing/client/android/a/i;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/hardware/Camera$PreviewCallback;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Lcom/google/zxing/client/android/a/d;

.field private c:Landroid/os/Handler;

.field private d:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    const-class v0, Lcom/google/zxing/client/android/a/i;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/zxing/client/android/a/i;->a:Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Lcom/google/zxing/client/android/a/d;)V
    .registers 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/zxing/client/android/a/i;->b:Lcom/google/zxing/client/android/a/d;

    return-void
.end method


# virtual methods
.method a(Landroid/os/Handler;I)V
    .registers 3

    iput-object p1, p0, Lcom/google/zxing/client/android/a/i;->c:Landroid/os/Handler;

    iput p2, p0, Lcom/google/zxing/client/android/a/i;->d:I

    return-void
.end method

.method public onPreviewFrame([BLandroid/hardware/Camera;)V
    .registers 9

    iget-object v0, p0, Lcom/google/zxing/client/android/a/i;->b:Lcom/google/zxing/client/android/a/d;

    invoke-virtual {v0}, Lcom/google/zxing/client/android/a/d;->a()Landroid/graphics/Point;

    move-result-object v3

    iget-object v4, p0, Lcom/google/zxing/client/android/a/i;->c:Landroid/os/Handler;

    if-eqz v3, :cond_1d

    if-eqz v4, :cond_1d

    iget v0, p0, Lcom/google/zxing/client/android/a/i;->d:I

    iget v1, v3, Landroid/graphics/Point;->x:I

    iget v2, v3, Landroid/graphics/Point;->y:I

    invoke-virtual {v4, v0, v1, v2, p1}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v5

    invoke-virtual {v5}, Landroid/os/Message;->sendToTarget()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/zxing/client/android/a/i;->c:Landroid/os/Handler;

    goto :goto_24

    :cond_1d
    sget-object v0, Lcom/google/zxing/client/android/a/i;->a:Ljava/lang/String;

    const-string v1, "Got preview callback, but no handler or resolution available"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_24
    return-void
.end method

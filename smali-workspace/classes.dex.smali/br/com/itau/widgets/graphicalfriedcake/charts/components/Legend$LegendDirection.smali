.class public final enum Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendDirection;
.super Ljava/lang/Enum;
.source "Legend.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "LegendDirection"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendDirection;>;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendDirection;

.field public static final enum LEFT_TO_RIGHT:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendDirection;

.field public static final enum RIGHT_TO_LEFT:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendDirection;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .line 34
    new-instance v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendDirection;

    const-string v1, "LEFT_TO_RIGHT"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendDirection;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendDirection;->LEFT_TO_RIGHT:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendDirection;

    new-instance v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendDirection;

    const-string v1, "RIGHT_TO_LEFT"

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendDirection;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendDirection;->RIGHT_TO_LEFT:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendDirection;

    .line 33
    const/4 v0, 0x2

    new-array v0, v0, [Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendDirection;

    sget-object v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendDirection;->LEFT_TO_RIGHT:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendDirection;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sget-object v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendDirection;->RIGHT_TO_LEFT:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendDirection;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    sput-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendDirection;->$VALUES:[Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendDirection;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 33
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendDirection;
    .registers 2
    .param p0, "name"    # Ljava/lang/String;

    .line 33
    const-class v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendDirection;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendDirection;

    return-object v0
.end method

.method public static values()[Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendDirection;
    .registers 1

    .line 33
    sget-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendDirection;->$VALUES:[Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendDirection;

    invoke-virtual {v0}, [Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendDirection;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendDirection;

    return-object v0
.end method

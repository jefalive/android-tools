.class public Lbr/com/itau/widgets/material/MaterialEditText;
.super Landroid/support/v7/widget/AppCompatEditText;
.source "MaterialEditText.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lbr/com/itau/widgets/material/MaterialEditText$FloatingLabelType;
    }
.end annotation


# static fields
.field public static final FLOATING_LABEL_HIGHLIGHT:I = 0x2

.field public static final FLOATING_LABEL_NONE:I = 0x0

.field public static final FLOATING_LABEL_NORMAL:I = 0x1


# instance fields
.field private accentTypeface:Landroid/graphics/Typeface;

.field private alwaysHighlighted:Z

.field private autoValidate:Z

.field private baseColor:I

.field private bottomEllipsisSize:I

.field private bottomLines:F

.field bottomLinesAnimator:Lcom/nineoldandroids/animation/ObjectAnimator;

.field private bottomSpacing:I

.field private bottomTextSize:I

.field private charactersCountValid:Z

.field private checkCharactersCountAtBeginning:Z

.field private clearButtonBitmaps:[Landroid/graphics/Bitmap;

.field private clearButtonClicking:Z

.field private clearButtonTouched:Z

.field private currentBottomLines:F

.field private dottedDisabledLine:Z

.field private errorColor:I

.field private errorLineWidth:I

.field private extraPaddingBottom:I

.field private extraPaddingLeft:I

.field private extraPaddingRight:I

.field private extraPaddingTop:I

.field private firstShown:Z

.field private floatingLabelAlwaysShown:Z

.field private floatingLabelAnimating:Z

.field private floatingLabelEnabled:Z

.field private floatingLabelFraction:F

.field private floatingLabelPadding:I

.field private floatingLabelShown:Z

.field private floatingLabelText:Ljava/lang/CharSequence;

.field private floatingLabelTextColor:I

.field private floatingLabelTextSize:I

.field private focusEvaluator:Lcom/nineoldandroids/animation/ArgbEvaluator;

.field private focusFraction:F

.field private hasContentDescription:Z

.field private helperText:Ljava/lang/String;

.field private helperTextAlwaysShown:Z

.field private helperTextColor:I

.field private hideUnderline:Z

.field private highlightFloatingLabel:Z

.field private highlightLineWidth:I

.field private highlightUnderlineColor:I

.field private iconLeftBitmaps:[Landroid/graphics/Bitmap;

.field private iconOuterHeight:I

.field private iconOuterWidth:I

.field private iconPadding:I

.field private iconRightBitmaps:[Landroid/graphics/Bitmap;

.field private iconSize:I

.field innerFocusChangeListener:Landroid/view/View$OnFocusChangeListener;

.field private innerPaddingBottom:I

.field private innerPaddingLeft:I

.field private innerPaddingRight:I

.field private innerPaddingTop:I

.field private isPasswordAttrOn:Z

.field labelAnimator:Lcom/nineoldandroids/animation/ObjectAnimator;

.field labelFocusAnimator:Lcom/nineoldandroids/animation/ObjectAnimator;

.field private lengthChecker:Lbr/com/itau/widgets/material/validation/METLengthChecker;

.field private maxCharacters:I

.field private minBottomLines:I

.field private minBottomTextLines:I

.field private minCharacters:I

.field private normalLineWidth:I

.field outerFocusChangeListener:Landroid/view/View$OnFocusChangeListener;

.field paint:Landroid/graphics/Paint;

.field private primaryColor:I

.field private showClearButton:Z

.field private singleLineEllipsis:Z

.field private tempErrorText:Ljava/lang/String;

.field private textColorHintStateList:Landroid/content/res/ColorStateList;

.field private textColorStateList:Landroid/content/res/ColorStateList;

.field textLayout:Landroid/text/StaticLayout;

.field textPaint:Landroid/text/TextPaint;

.field private typeface:Landroid/graphics/Typeface;

.field private underlineColor:I

.field private validateOnFocusLost:Z

.field private validators:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Lbr/com/itau/widgets/material/validation/METValidator;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 4
    .param p1, "context"    # Landroid/content/Context;

    .line 297
    invoke-direct {p0, p1}, Landroid/support/v7/widget/AppCompatEditText;-><init>(Landroid/content/Context;)V

    .line 60
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->paint:Landroid/graphics/Paint;

    .line 61
    new-instance v0, Landroid/text/TextPaint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/text/TextPaint;-><init>(I)V

    iput-object v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->textPaint:Landroid/text/TextPaint;

    .line 187
    const/4 v0, -0x1

    iput v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->helperTextColor:I

    .line 291
    new-instance v0, Lcom/nineoldandroids/animation/ArgbEvaluator;

    invoke-direct {v0}, Lcom/nineoldandroids/animation/ArgbEvaluator;-><init>()V

    iput-object v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->focusEvaluator:Lcom/nineoldandroids/animation/ArgbEvaluator;

    .line 294
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->isPasswordAttrOn:Z

    .line 298
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lbr/com/itau/widgets/material/MaterialEditText;->init(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 299
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .line 302
    invoke-direct {p0, p1, p2}, Landroid/support/v7/widget/AppCompatEditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 60
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->paint:Landroid/graphics/Paint;

    .line 61
    new-instance v0, Landroid/text/TextPaint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/text/TextPaint;-><init>(I)V

    iput-object v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->textPaint:Landroid/text/TextPaint;

    .line 187
    const/4 v0, -0x1

    iput v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->helperTextColor:I

    .line 291
    new-instance v0, Lcom/nineoldandroids/animation/ArgbEvaluator;

    invoke-direct {v0}, Lcom/nineoldandroids/animation/ArgbEvaluator;-><init>()V

    iput-object v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->focusEvaluator:Lcom/nineoldandroids/animation/ArgbEvaluator;

    .line 294
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->isPasswordAttrOn:Z

    .line 303
    invoke-direct {p0, p1, p2}, Lbr/com/itau/widgets/material/MaterialEditText;->init(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 304
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "style"    # I
    .annotation build Landroid/annotation/TargetApi;
        value = 0x15
    .end annotation

    .line 308
    invoke-direct {p0, p1, p2, p3}, Landroid/support/v7/widget/AppCompatEditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 60
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->paint:Landroid/graphics/Paint;

    .line 61
    new-instance v0, Landroid/text/TextPaint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/text/TextPaint;-><init>(I)V

    iput-object v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->textPaint:Landroid/text/TextPaint;

    .line 187
    const/4 v0, -0x1

    iput v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->helperTextColor:I

    .line 291
    new-instance v0, Lcom/nineoldandroids/animation/ArgbEvaluator;

    invoke-direct {v0}, Lcom/nineoldandroids/animation/ArgbEvaluator;-><init>()V

    iput-object v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->focusEvaluator:Lcom/nineoldandroids/animation/ArgbEvaluator;

    .line 294
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->isPasswordAttrOn:Z

    .line 309
    invoke-direct {p0, p1, p2}, Lbr/com/itau/widgets/material/MaterialEditText;->init(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 310
    return-void
.end method

.method static synthetic access$000(Lbr/com/itau/widgets/material/MaterialEditText;)V
    .registers 1
    .param p0, "x0"    # Lbr/com/itau/widgets/material/MaterialEditText;

    .line 55
    invoke-direct {p0}, Lbr/com/itau/widgets/material/MaterialEditText;->checkCharactersCount()V

    return-void
.end method

.method static synthetic access$100(Lbr/com/itau/widgets/material/MaterialEditText;)Z
    .registers 2
    .param p0, "x0"    # Lbr/com/itau/widgets/material/MaterialEditText;

    .line 55
    iget-boolean v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->autoValidate:Z

    return v0
.end method

.method static synthetic access$200(Lbr/com/itau/widgets/material/MaterialEditText;)Z
    .registers 2
    .param p0, "x0"    # Lbr/com/itau/widgets/material/MaterialEditText;

    .line 55
    iget-boolean v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->floatingLabelEnabled:Z

    return v0
.end method

.method static synthetic access$300(Lbr/com/itau/widgets/material/MaterialEditText;)Z
    .registers 2
    .param p0, "x0"    # Lbr/com/itau/widgets/material/MaterialEditText;

    .line 55
    iget-boolean v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->floatingLabelShown:Z

    return v0
.end method

.method static synthetic access$302(Lbr/com/itau/widgets/material/MaterialEditText;Z)Z
    .registers 2
    .param p0, "x0"    # Lbr/com/itau/widgets/material/MaterialEditText;
    .param p1, "x1"    # Z

    .line 55
    iput-boolean p1, p0, Lbr/com/itau/widgets/material/MaterialEditText;->floatingLabelShown:Z

    return p1
.end method

.method static synthetic access$400(Lbr/com/itau/widgets/material/MaterialEditText;)Lcom/nineoldandroids/animation/ObjectAnimator;
    .registers 2
    .param p0, "x0"    # Lbr/com/itau/widgets/material/MaterialEditText;

    .line 55
    invoke-direct {p0}, Lbr/com/itau/widgets/material/MaterialEditText;->getLabelAnimator()Lcom/nineoldandroids/animation/ObjectAnimator;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$500(Lbr/com/itau/widgets/material/MaterialEditText;)Z
    .registers 2
    .param p0, "x0"    # Lbr/com/itau/widgets/material/MaterialEditText;

    .line 55
    iget-boolean v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->highlightFloatingLabel:Z

    return v0
.end method

.method static synthetic access$600(Lbr/com/itau/widgets/material/MaterialEditText;)Lcom/nineoldandroids/animation/ObjectAnimator;
    .registers 2
    .param p0, "x0"    # Lbr/com/itau/widgets/material/MaterialEditText;

    .line 55
    invoke-direct {p0}, Lbr/com/itau/widgets/material/MaterialEditText;->getLabelFocusAnimator()Lcom/nineoldandroids/animation/ObjectAnimator;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$700(Lbr/com/itau/widgets/material/MaterialEditText;)Z
    .registers 2
    .param p0, "x0"    # Lbr/com/itau/widgets/material/MaterialEditText;

    .line 55
    iget-boolean v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->validateOnFocusLost:Z

    return v0
.end method

.method private adjustBottomLines()Z
    .registers 11

    .line 872
    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialEditText;->getWidth()I

    move-result v0

    if-nez v0, :cond_8

    .line 873
    const/4 v0, 0x0

    return v0

    .line 876
    :cond_8
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->textPaint:Landroid/text/TextPaint;

    iget v1, p0, Lbr/com/itau/widgets/material/MaterialEditText;->bottomTextSize:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 877
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->tempErrorText:Ljava/lang/String;

    if-nez v0, :cond_18

    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->helperText:Ljava/lang/String;

    if-eqz v0, :cond_74

    .line 878
    :cond_18
    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialEditText;->getGravity()I

    move-result v0

    and-int/lit8 v0, v0, 0x5

    const/4 v1, 0x5

    if-eq v0, v1, :cond_27

    invoke-direct {p0}, Lbr/com/itau/widgets/material/MaterialEditText;->isRTL()Z

    move-result v0

    if-eqz v0, :cond_2a

    :cond_27
    sget-object v9, Landroid/text/Layout$Alignment;->ALIGN_OPPOSITE:Landroid/text/Layout$Alignment;

    goto :goto_38

    :cond_2a
    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialEditText;->getGravity()I

    move-result v0

    and-int/lit8 v0, v0, 0x3

    const/4 v1, 0x3

    if-ne v0, v1, :cond_36

    sget-object v9, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    goto :goto_38

    :cond_36
    sget-object v9, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    .line 881
    .local v9, "alignment":Landroid/text/Layout$Alignment;
    :goto_38
    new-instance v0, Landroid/text/StaticLayout;

    iget-object v1, p0, Lbr/com/itau/widgets/material/MaterialEditText;->tempErrorText:Ljava/lang/String;

    if-eqz v1, :cond_41

    iget-object v1, p0, Lbr/com/itau/widgets/material/MaterialEditText;->tempErrorText:Ljava/lang/String;

    goto :goto_43

    :cond_41
    iget-object v1, p0, Lbr/com/itau/widgets/material/MaterialEditText;->helperText:Ljava/lang/String;

    :goto_43
    iget-object v2, p0, Lbr/com/itau/widgets/material/MaterialEditText;->textPaint:Landroid/text/TextPaint;

    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialEditText;->getWidth()I

    move-result v3

    invoke-direct {p0}, Lbr/com/itau/widgets/material/MaterialEditText;->getBottomTextLeftOffset()I

    move-result v4

    sub-int/2addr v3, v4

    invoke-direct {p0}, Lbr/com/itau/widgets/material/MaterialEditText;->getBottomTextRightOffset()I

    move-result v4

    sub-int/2addr v3, v4

    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialEditText;->getPaddingLeft()I

    move-result v4

    sub-int/2addr v3, v4

    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialEditText;->getPaddingRight()I

    move-result v4

    sub-int/2addr v3, v4

    move-object v4, v9

    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v6, 0x0

    const/4 v7, 0x1

    invoke-direct/range {v0 .. v7}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    iput-object v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->textLayout:Landroid/text/StaticLayout;

    .line 882
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->textLayout:Landroid/text/StaticLayout;

    invoke-virtual {v0}, Landroid/text/StaticLayout;->getLineCount()I

    move-result v0

    iget v1, p0, Lbr/com/itau/widgets/material/MaterialEditText;->minBottomTextLines:I

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v8

    .line 883
    .local v8, "destBottomLines":I
    .end local v9    # "alignment":Landroid/text/Layout$Alignment;
    goto :goto_76

    .line 884
    .end local v8    # "destBottomLines":I
    :cond_74
    iget v8, p0, Lbr/com/itau/widgets/material/MaterialEditText;->minBottomLines:I

    .line 886
    .local v8, "destBottomLines":I
    :goto_76
    iget v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->bottomLines:F

    int-to-float v1, v8

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_85

    .line 887
    int-to-float v0, v8

    invoke-direct {p0, v0}, Lbr/com/itau/widgets/material/MaterialEditText;->getBottomLinesAnimator(F)Lcom/nineoldandroids/animation/ObjectAnimator;

    move-result-object v0

    invoke-virtual {v0}, Lcom/nineoldandroids/animation/ObjectAnimator;->start()V

    .line 889
    :cond_85
    int-to-float v0, v8

    iput v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->bottomLines:F

    .line 890
    const/4 v0, 0x1

    return v0
.end method

.method private checkCharactersCount()V
    .registers 4

    .line 1497
    iget-boolean v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->firstShown:Z

    if-nez v0, :cond_8

    iget-boolean v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->checkCharactersCountAtBeginning:Z

    if-eqz v0, :cond_e

    :cond_8
    invoke-direct {p0}, Lbr/com/itau/widgets/material/MaterialEditText;->hasCharactersCounter()Z

    move-result v0

    if-nez v0, :cond_12

    .line 1498
    :cond_e
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->charactersCountValid:Z

    goto :goto_2f

    .line 1500
    :cond_12
    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    .line 1501
    .local v1, "text":Ljava/lang/CharSequence;
    if-nez v1, :cond_1a

    const/4 v2, 0x0

    goto :goto_1e

    :cond_1a
    invoke-direct {p0, v1}, Lbr/com/itau/widgets/material/MaterialEditText;->checkLength(Ljava/lang/CharSequence;)I

    move-result v2

    .line 1502
    .local v2, "count":I
    :goto_1e
    iget v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->minCharacters:I

    if-lt v2, v0, :cond_2c

    iget v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->maxCharacters:I

    if-lez v0, :cond_2a

    iget v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->maxCharacters:I

    if-gt v2, v0, :cond_2c

    :cond_2a
    const/4 v0, 0x1

    goto :goto_2d

    :cond_2c
    const/4 v0, 0x0

    :goto_2d
    iput-boolean v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->charactersCountValid:Z

    .line 1504
    .end local v1    # "text":Ljava/lang/CharSequence;
    .end local v2    # "count":I
    :goto_2f
    return-void
.end method

.method private checkLength(Ljava/lang/CharSequence;)I
    .registers 3
    .param p1, "text"    # Ljava/lang/CharSequence;

    .line 1586
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->lengthChecker:Lbr/com/itau/widgets/material/validation/METLengthChecker;

    if-nez v0, :cond_9

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    return v0

    .line 1587
    :cond_9
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->lengthChecker:Lbr/com/itau/widgets/material/validation/METLengthChecker;

    invoke-virtual {v0, p1}, Lbr/com/itau/widgets/material/validation/METLengthChecker;->getLength(Ljava/lang/CharSequence;)I

    move-result v0

    return v0
.end method

.method private correctPaddings()V
    .registers 9

    .line 837
    const/4 v5, 0x0

    .local v5, "buttonsWidthLeft":I
    const/4 v6, 0x0

    .line 838
    .local v6, "buttonsWidthRight":I
    iget v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->iconOuterWidth:I

    invoke-direct {p0}, Lbr/com/itau/widgets/material/MaterialEditText;->getButtonsCount()I

    move-result v1

    mul-int v7, v0, v1

    .line 839
    .local v7, "buttonsWidth":I
    invoke-direct {p0}, Lbr/com/itau/widgets/material/MaterialEditText;->isRTL()Z

    move-result v0

    if-eqz v0, :cond_12

    .line 840
    move v5, v7

    goto :goto_13

    .line 842
    :cond_12
    move v6, v7

    .line 844
    :goto_13
    iget v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->innerPaddingLeft:I

    iget v1, p0, Lbr/com/itau/widgets/material/MaterialEditText;->extraPaddingLeft:I

    add-int/2addr v0, v1

    add-int/2addr v0, v5

    iget v1, p0, Lbr/com/itau/widgets/material/MaterialEditText;->innerPaddingTop:I

    iget v2, p0, Lbr/com/itau/widgets/material/MaterialEditText;->extraPaddingTop:I

    add-int/2addr v1, v2

    iget v2, p0, Lbr/com/itau/widgets/material/MaterialEditText;->innerPaddingRight:I

    iget v3, p0, Lbr/com/itau/widgets/material/MaterialEditText;->extraPaddingRight:I

    add-int/2addr v2, v3

    add-int/2addr v2, v6

    iget v3, p0, Lbr/com/itau/widgets/material/MaterialEditText;->innerPaddingBottom:I

    iget v4, p0, Lbr/com/itau/widgets/material/MaterialEditText;->extraPaddingBottom:I

    add-int/2addr v3, v4

    invoke-super {p0, v0, v1, v2, v3}, Landroid/support/v7/widget/AppCompatEditText;->setPadding(IIII)V

    .line 845
    return-void
.end method

.method private generateIconBitmaps(I)[Landroid/graphics/Bitmap;
    .registers 6
    .param p1, "origin"    # I

    .line 590
    const/4 v0, -0x1

    if-ne p1, v0, :cond_5

    .line 591
    const/4 v0, 0x0

    return-object v0

    .line 593
    :cond_5
    new-instance v2, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v2}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 594
    .local v2, "options":Landroid/graphics/BitmapFactory$Options;
    const/4 v0, 0x1

    iput-boolean v0, v2, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 595
    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialEditText;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0, p1, v2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 596
    iget v0, v2, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    iget v1, v2, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 597
    .local v3, "size":I
    iget v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->iconSize:I

    if-le v3, v0, :cond_25

    iget v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->iconSize:I

    div-int v0, v3, v0

    goto :goto_26

    :cond_25
    const/4 v0, 0x1

    :goto_26
    iput v0, v2, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 598
    const/4 v0, 0x0

    iput-boolean v0, v2, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 599
    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialEditText;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0, p1, v2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-direct {p0, v0}, Lbr/com/itau/widgets/material/MaterialEditText;->generateIconBitmaps(Landroid/graphics/Bitmap;)[Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method private generateIconBitmaps(Landroid/graphics/Bitmap;)[Landroid/graphics/Bitmap;
    .registers 6
    .param p1, "origin"    # Landroid/graphics/Bitmap;

    .line 613
    if-nez p1, :cond_4

    .line 614
    const/4 v0, 0x0

    return-object v0

    .line 616
    :cond_4
    const/4 v0, 0x4

    new-array v2, v0, [Landroid/graphics/Bitmap;

    .line 617
    .local v2, "iconBitmaps":[Landroid/graphics/Bitmap;
    invoke-direct {p0, p1}, Lbr/com/itau/widgets/material/MaterialEditText;->scaleIcon(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object p1

    .line 618
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    const/4 v1, 0x0

    aput-object v0, v2, v1

    .line 619
    new-instance v3, Landroid/graphics/Canvas;

    const/4 v0, 0x0

    aget-object v0, v2, v0

    invoke-direct {v3, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 620
    .local v3, "canvas":Landroid/graphics/Canvas;
    iget v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->baseColor:I

    const v1, 0xffffff

    and-int/2addr v0, v1

    iget v1, p0, Lbr/com/itau/widgets/material/MaterialEditText;->baseColor:I

    invoke-static {v1}, Lbr/com/itau/widgets/material/Colors;->isLight(I)Z

    move-result v1

    if-eqz v1, :cond_2e

    const/high16 v1, -0x1000000

    goto :goto_30

    :cond_2e
    const/high16 v1, -0x76000000

    :goto_30
    or-int/2addr v0, v1

    sget-object v1, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v3, v0, v1}, Landroid/graphics/Canvas;->drawColor(ILandroid/graphics/PorterDuff$Mode;)V

    .line 621
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    const/4 v1, 0x1

    aput-object v0, v2, v1

    .line 622
    new-instance v3, Landroid/graphics/Canvas;

    const/4 v0, 0x1

    aget-object v0, v2, v0

    invoke-direct {v3, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 623
    iget v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->primaryColor:I

    sget-object v1, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v3, v0, v1}, Landroid/graphics/Canvas;->drawColor(ILandroid/graphics/PorterDuff$Mode;)V

    .line 624
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    const/4 v1, 0x2

    aput-object v0, v2, v1

    .line 625
    new-instance v3, Landroid/graphics/Canvas;

    const/4 v0, 0x2

    aget-object v0, v2, v0

    invoke-direct {v3, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 626
    iget v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->baseColor:I

    const v1, 0xffffff

    and-int/2addr v0, v1

    iget v1, p0, Lbr/com/itau/widgets/material/MaterialEditText;->baseColor:I

    invoke-static {v1}, Lbr/com/itau/widgets/material/Colors;->isLight(I)Z

    move-result v1

    if-eqz v1, :cond_72

    const/high16 v1, 0x4c000000    # 3.3554432E7f

    goto :goto_74

    :cond_72
    const/high16 v1, 0x42000000    # 32.0f

    :goto_74
    or-int/2addr v0, v1

    sget-object v1, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v3, v0, v1}, Landroid/graphics/Canvas;->drawColor(ILandroid/graphics/PorterDuff$Mode;)V

    .line 627
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    const/4 v1, 0x3

    aput-object v0, v2, v1

    .line 628
    new-instance v3, Landroid/graphics/Canvas;

    const/4 v0, 0x3

    aget-object v0, v2, v0

    invoke-direct {v3, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 629
    iget v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->errorColor:I

    sget-object v1, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v3, v0, v1}, Landroid/graphics/Canvas;->drawColor(ILandroid/graphics/PorterDuff$Mode;)V

    .line 630
    return-object v2
.end method

.method private generateIconBitmaps(Landroid/graphics/drawable/Drawable;)[Landroid/graphics/Bitmap;
    .registers 8
    .param p1, "drawable"    # Landroid/graphics/drawable/Drawable;

    .line 603
    if-nez p1, :cond_4

    .line 604
    const/4 v0, 0x0

    return-object v0

    .line 605
    :cond_4
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 606
    .local v4, "bitmap":Landroid/graphics/Bitmap;
    new-instance v5, Landroid/graphics/Canvas;

    invoke-direct {v5, v4}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 607
    .local v5, "canvas":Landroid/graphics/Canvas;
    invoke-virtual {v5}, Landroid/graphics/Canvas;->getWidth()I

    move-result v0

    invoke-virtual {v5}, Landroid/graphics/Canvas;->getHeight()I

    move-result v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {p1, v2, v3, v0, v1}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 608
    invoke-virtual {p1, v5}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 609
    iget v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->iconSize:I

    iget v1, p0, Lbr/com/itau/widgets/material/MaterialEditText;->iconSize:I

    const/4 v2, 0x0

    invoke-static {v4, v0, v1, v2}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-direct {p0, v0}, Lbr/com/itau/widgets/material/MaterialEditText;->generateIconBitmaps(Landroid/graphics/Bitmap;)[Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method private getBottomEllipsisWidth()I
    .registers 3

    .line 1493
    iget-boolean v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->singleLineEllipsis:Z

    if-eqz v0, :cond_f

    iget v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->bottomEllipsisSize:I

    mul-int/lit8 v0, v0, 0x5

    const/4 v1, 0x4

    invoke-direct {p0, v1}, Lbr/com/itau/widgets/material/MaterialEditText;->getPixel(I)I

    move-result v1

    add-int/2addr v0, v1

    goto :goto_10

    :cond_f
    const/4 v0, 0x0

    :goto_10
    return v0
.end method

.method private getBottomLinesAnimator(F)Lcom/nineoldandroids/animation/ObjectAnimator;
    .registers 5
    .param p1, "destBottomLines"    # F

    .line 1323
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->bottomLinesAnimator:Lcom/nineoldandroids/animation/ObjectAnimator;

    if-nez v0, :cond_13

    .line 1324
    const-string v0, "currentBottomLines"

    const/4 v1, 0x1

    new-array v1, v1, [F

    const/4 v2, 0x0

    aput p1, v1, v2

    invoke-static {p0, v0, v1}, Lcom/nineoldandroids/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Lcom/nineoldandroids/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->bottomLinesAnimator:Lcom/nineoldandroids/animation/ObjectAnimator;

    goto :goto_23

    .line 1326
    :cond_13
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->bottomLinesAnimator:Lcom/nineoldandroids/animation/ObjectAnimator;

    invoke-virtual {v0}, Lcom/nineoldandroids/animation/ObjectAnimator;->cancel()V

    .line 1327
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->bottomLinesAnimator:Lcom/nineoldandroids/animation/ObjectAnimator;

    const/4 v1, 0x1

    new-array v1, v1, [F

    const/4 v2, 0x0

    aput p1, v1, v2

    invoke-virtual {v0, v1}, Lcom/nineoldandroids/animation/ObjectAnimator;->setFloatValues([F)V

    .line 1329
    :goto_23
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->bottomLinesAnimator:Lcom/nineoldandroids/animation/ObjectAnimator;

    return-object v0
.end method

.method private getBottomTextLeftOffset()I
    .registers 2

    .line 1481
    invoke-direct {p0}, Lbr/com/itau/widgets/material/MaterialEditText;->isRTL()Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-direct {p0}, Lbr/com/itau/widgets/material/MaterialEditText;->getCharactersCounterWidth()I

    move-result v0

    goto :goto_f

    :cond_b
    invoke-direct {p0}, Lbr/com/itau/widgets/material/MaterialEditText;->getBottomEllipsisWidth()I

    move-result v0

    :goto_f
    return v0
.end method

.method private getBottomTextRightOffset()I
    .registers 2

    .line 1485
    invoke-direct {p0}, Lbr/com/itau/widgets/material/MaterialEditText;->isRTL()Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-direct {p0}, Lbr/com/itau/widgets/material/MaterialEditText;->getBottomEllipsisWidth()I

    move-result v0

    goto :goto_f

    :cond_b
    invoke-direct {p0}, Lbr/com/itau/widgets/material/MaterialEditText;->getCharactersCounterWidth()I

    move-result v0

    :goto_f
    return v0
.end method

.method private getButtonsCount()I
    .registers 2

    .line 848
    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialEditText;->isShowClearButton()Z

    move-result v0

    if-eqz v0, :cond_8

    const/4 v0, 0x1

    goto :goto_9

    :cond_8
    const/4 v0, 0x0

    :goto_9
    return v0
.end method

.method private getCharactersCounterText()Ljava/lang/String;
    .registers 4

    .line 1516
    iget v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->minCharacters:I

    if-gtz v0, :cond_4f

    .line 1517
    invoke-direct {p0}, Lbr/com/itau/widgets/material/MaterialEditText;->isRTL()Z

    move-result v0

    if-eqz v0, :cond_2c

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget v1, p0, Lbr/com/itau/widgets/material/MaterialEditText;->maxCharacters:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " / "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-direct {p0, v1}, Lbr/com/itau/widgets/material/MaterialEditText;->checkLength(Ljava/lang/CharSequence;)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_4d

    :cond_2c
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-direct {p0, v1}, Lbr/com/itau/widgets/material/MaterialEditText;->checkLength(Ljava/lang/CharSequence;)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " / "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lbr/com/itau/widgets/material/MaterialEditText;->maxCharacters:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .local v2, "text":Ljava/lang/String;
    :goto_4d
    goto/16 :goto_10a

    .line 1518
    .end local v2    # "text":Ljava/lang/String;
    :cond_4f
    iget v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->maxCharacters:I

    if-gtz v0, :cond_a9

    .line 1519
    invoke-direct {p0}, Lbr/com/itau/widgets/material/MaterialEditText;->isRTL()Z

    move-result v0

    if-eqz v0, :cond_81

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "+"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lbr/com/itau/widgets/material/MaterialEditText;->minCharacters:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " / "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-direct {p0, v1}, Lbr/com/itau/widgets/material/MaterialEditText;->checkLength(Ljava/lang/CharSequence;)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_a8

    :cond_81
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-direct {p0, v1}, Lbr/com/itau/widgets/material/MaterialEditText;->checkLength(Ljava/lang/CharSequence;)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " / "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lbr/com/itau/widgets/material/MaterialEditText;->minCharacters:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "+"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .local v2, "text":Ljava/lang/String;
    :goto_a8
    goto :goto_10a

    .line 1521
    .end local v2    # "text":Ljava/lang/String;
    :cond_a9
    invoke-direct {p0}, Lbr/com/itau/widgets/material/MaterialEditText;->isRTL()Z

    move-result v0

    if-eqz v0, :cond_dd

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget v1, p0, Lbr/com/itau/widgets/material/MaterialEditText;->maxCharacters:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lbr/com/itau/widgets/material/MaterialEditText;->minCharacters:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " / "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-direct {p0, v1}, Lbr/com/itau/widgets/material/MaterialEditText;->checkLength(Ljava/lang/CharSequence;)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_10a

    :cond_dd
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-direct {p0, v1}, Lbr/com/itau/widgets/material/MaterialEditText;->checkLength(Ljava/lang/CharSequence;)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " / "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lbr/com/itau/widgets/material/MaterialEditText;->minCharacters:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lbr/com/itau/widgets/material/MaterialEditText;->maxCharacters:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1523
    .local v2, "text":Ljava/lang/String;
    :goto_10a
    return-object v2
.end method

.method private getCharactersCounterWidth()I
    .registers 3

    .line 1489
    invoke-direct {p0}, Lbr/com/itau/widgets/material/MaterialEditText;->hasCharactersCounter()Z

    move-result v0

    if-eqz v0, :cond_12

    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->textPaint:Landroid/text/TextPaint;

    invoke-direct {p0}, Lbr/com/itau/widgets/material/MaterialEditText;->getCharactersCounterText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v0

    float-to-int v0, v0

    goto :goto_13

    :cond_12
    const/4 v0, 0x0

    :goto_13
    return v0
.end method

.method private getCustomTypeface(Ljava/lang/String;)Landroid/graphics/Typeface;
    .registers 3
    .param p1, "fontPath"    # Ljava/lang/String;

    .line 547
    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialEditText;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    invoke-static {v0, p1}, Landroid/graphics/Typeface;->createFromAsset(Landroid/content/res/AssetManager;Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v0

    return-object v0
.end method

.method private getLabelAnimator()Lcom/nineoldandroids/animation/ObjectAnimator;
    .registers 5

    .line 1308
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->labelAnimator:Lcom/nineoldandroids/animation/ObjectAnimator;

    if-nez v0, :cond_12

    .line 1309
    const-string v0, "floatingLabelFraction"

    const/4 v1, 0x2

    new-array v1, v1, [F

    fill-array-data v1, :array_24

    invoke-static {p0, v0, v1}, Lcom/nineoldandroids/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Lcom/nineoldandroids/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->labelAnimator:Lcom/nineoldandroids/animation/ObjectAnimator;

    .line 1311
    :cond_12
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->labelAnimator:Lcom/nineoldandroids/animation/ObjectAnimator;

    iget-boolean v1, p0, Lbr/com/itau/widgets/material/MaterialEditText;->floatingLabelAnimating:Z

    if-eqz v1, :cond_1b

    const-wide/16 v1, 0x12c

    goto :goto_1d

    :cond_1b
    const-wide/16 v1, 0x0

    :goto_1d
    invoke-virtual {v0, v1, v2}, Lcom/nineoldandroids/animation/ObjectAnimator;->setDuration(J)Lcom/nineoldandroids/animation/ObjectAnimator;

    .line 1312
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->labelAnimator:Lcom/nineoldandroids/animation/ObjectAnimator;

    return-object v0

    nop

    :array_24
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method private getLabelFocusAnimator()Lcom/nineoldandroids/animation/ObjectAnimator;
    .registers 5

    .line 1316
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->labelFocusAnimator:Lcom/nineoldandroids/animation/ObjectAnimator;

    if-nez v0, :cond_12

    .line 1317
    const-string v0, "focusFraction"

    const/4 v1, 0x2

    new-array v1, v1, [F

    fill-array-data v1, :array_16

    invoke-static {p0, v0, v1}, Lcom/nineoldandroids/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Lcom/nineoldandroids/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->labelFocusAnimator:Lcom/nineoldandroids/animation/ObjectAnimator;

    .line 1319
    :cond_12
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->labelFocusAnimator:Lcom/nineoldandroids/animation/ObjectAnimator;

    return-object v0

    nop

    :array_16
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method private getPixel(I)I
    .registers 4
    .param p1, "dp"    # I

    .line 792
    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialEditText;->getContext()Landroid/content/Context;

    move-result-object v0

    int-to-float v1, p1

    invoke-static {v0, v1}, Lbr/com/itau/widgets/material/Density;->dp2px(Landroid/content/Context;F)I

    move-result v0

    return v0
.end method

.method private hasCharactersCounter()Z
    .registers 2

    .line 1511
    iget v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->minCharacters:I

    if-gtz v0, :cond_8

    iget v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->maxCharacters:I

    if-lez v0, :cond_a

    :cond_8
    const/4 v0, 0x1

    goto :goto_b

    :cond_a
    const/4 v0, 0x0

    :goto_b
    return v0
.end method

.method private init(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 19
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .line 313
    move-object/from16 v0, p0

    const/16 v1, 0x20

    invoke-direct {v0, v1}, Lbr/com/itau/widgets/material/MaterialEditText;->getPixel(I)I

    move-result v0

    move-object/from16 v1, p0

    iput v0, v1, Lbr/com/itau/widgets/material/MaterialEditText;->iconSize:I

    .line 314
    move-object/from16 v0, p0

    const/16 v1, 0x30

    invoke-direct {v0, v1}, Lbr/com/itau/widgets/material/MaterialEditText;->getPixel(I)I

    move-result v0

    move-object/from16 v1, p0

    iput v0, v1, Lbr/com/itau/widgets/material/MaterialEditText;->iconOuterWidth:I

    .line 315
    move-object/from16 v0, p0

    const/16 v1, 0x20

    invoke-direct {v0, v1}, Lbr/com/itau/widgets/material/MaterialEditText;->getPixel(I)I

    move-result v0

    move-object/from16 v1, p0

    iput v0, v1, Lbr/com/itau/widgets/material/MaterialEditText;->iconOuterHeight:I

    .line 317
    invoke-virtual/range {p0 .. p0}, Lbr/com/itau/widgets/material/MaterialEditText;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lbr/com/itau/widgets/material/R$dimen;->inner_components_spacing:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    move-object/from16 v1, p0

    iput v0, v1, Lbr/com/itau/widgets/material/MaterialEditText;->bottomSpacing:I

    .line 318
    invoke-virtual/range {p0 .. p0}, Lbr/com/itau/widgets/material/MaterialEditText;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lbr/com/itau/widgets/material/R$dimen;->bottom_ellipsis_height:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    move-object/from16 v1, p0

    iput v0, v1, Lbr/com/itau/widgets/material/MaterialEditText;->bottomEllipsisSize:I

    .line 321
    const/high16 v4, -0x1000000

    .line 323
    .local v4, "defaultBaseColor":I
    sget-object v0, Lbr/com/itau/widgets/material/R$styleable;->MaterialEditText:[I

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v5

    .line 324
    .local v5, "typedArray":Landroid/content/res/TypedArray;
    sget v0, Lbr/com/itau/widgets/material/R$styleable;->MaterialEditText_met_textColor:I

    invoke-virtual {v5, v0}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v0

    move-object/from16 v1, p0

    iput-object v0, v1, Lbr/com/itau/widgets/material/MaterialEditText;->textColorStateList:Landroid/content/res/ColorStateList;

    .line 325
    sget v0, Lbr/com/itau/widgets/material/R$styleable;->MaterialEditText_met_textColorHint:I

    invoke-virtual {v5, v0}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v0

    move-object/from16 v1, p0

    iput-object v0, v1, Lbr/com/itau/widgets/material/MaterialEditText;->textColorHintStateList:Landroid/content/res/ColorStateList;

    .line 326
    sget v0, Lbr/com/itau/widgets/material/R$styleable;->MaterialEditText_met_baseColor:I

    invoke-virtual {v5, v0, v4}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    move-object/from16 v1, p0

    iput v0, v1, Lbr/com/itau/widgets/material/MaterialEditText;->baseColor:I

    .line 330
    new-instance v7, Landroid/util/TypedValue;

    invoke-direct {v7}, Landroid/util/TypedValue;-><init>()V

    .line 332
    .local v7, "primaryColorTypedValue":Landroid/util/TypedValue;
    :try_start_6f
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_83

    .line 333
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    const v1, 0x1010433

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v7, v2}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 334
    iget v6, v7, Landroid/util/TypedValue;->data:I

    .local v6, "defaultPrimaryColor":I
    goto :goto_8b

    .line 336
    .end local v6    # "defaultPrimaryColor":I
    :cond_83
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "SDK_INT less than LOLLIPOP"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_8b
    .catch Ljava/lang/Exception; {:try_start_6f .. :try_end_8b} :catch_8c

    .line 350
    .local v6, "defaultPrimaryColor":I
    :goto_8b
    goto :goto_bc

    .line 338
    :catch_8c
    move-exception v8

    .line 340
    .local v8, "e":Ljava/lang/Exception;
    :try_start_8d
    invoke-virtual/range {p0 .. p0}, Lbr/com/itau/widgets/material/MaterialEditText;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const-string v1, "colorPrimary"

    const-string v2, "attr"

    invoke-virtual/range {p0 .. p0}, Lbr/com/itau/widgets/material/MaterialEditText;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v9

    .line 341
    .local v9, "colorPrimaryId":I
    if-eqz v9, :cond_ae

    .line 342
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v9, v7, v1}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 343
    iget v6, v7, Landroid/util/TypedValue;->data:I

    goto :goto_b6

    .line 345
    .end local v6    # "defaultPrimaryColor":I
    :cond_ae
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "colorPrimary not found"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_b6
    .catch Ljava/lang/Exception; {:try_start_8d .. :try_end_b6} :catch_b7

    .line 349
    .local v6, "defaultPrimaryColor":I
    .end local v9    # "colorPrimaryId":I
    :goto_b6
    goto :goto_bc

    .line 347
    :catch_b7
    move-exception v9

    .line 348
    .local v9, "e1":Ljava/lang/Exception;
    move-object/from16 v0, p0

    iget v6, v0, Lbr/com/itau/widgets/material/MaterialEditText;->baseColor:I

    .line 352
    .end local v8    # "e":Ljava/lang/Exception;
    .end local v9    # "e1":Ljava/lang/Exception;
    :goto_bc
    sget v0, Lbr/com/itau/widgets/material/R$styleable;->MaterialEditText_met_primaryColor:I

    invoke-virtual {v5, v0, v6}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    move-object/from16 v1, p0

    iput v0, v1, Lbr/com/itau/widgets/material/MaterialEditText;->primaryColor:I

    .line 353
    sget v0, Lbr/com/itau/widgets/material/R$styleable;->MaterialEditText_met_floatingLabel:I

    const/4 v1, 0x0

    invoke-virtual {v5, v0, v1}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    move-object/from16 v1, p0

    invoke-direct {v1, v0}, Lbr/com/itau/widgets/material/MaterialEditText;->setFloatingLabelInternal(I)V

    .line 354
    sget v0, Lbr/com/itau/widgets/material/R$styleable;->MaterialEditText_met_errorColor:I

    const-string v1, "#e7492E"

    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v5, v0, v1}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    move-object/from16 v1, p0

    iput v0, v1, Lbr/com/itau/widgets/material/MaterialEditText;->errorColor:I

    .line 355
    sget v0, Lbr/com/itau/widgets/material/R$styleable;->MaterialEditText_met_minCharacters:I

    const/4 v1, 0x0

    invoke-virtual {v5, v0, v1}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    move-object/from16 v1, p0

    iput v0, v1, Lbr/com/itau/widgets/material/MaterialEditText;->minCharacters:I

    .line 356
    sget v0, Lbr/com/itau/widgets/material/R$styleable;->MaterialEditText_met_maxCharacters:I

    const/4 v1, 0x0

    invoke-virtual {v5, v0, v1}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    move-object/from16 v1, p0

    iput v0, v1, Lbr/com/itau/widgets/material/MaterialEditText;->maxCharacters:I

    .line 357
    sget v0, Lbr/com/itau/widgets/material/R$styleable;->MaterialEditText_met_singleLineEllipsis:I

    const/4 v1, 0x0

    invoke-virtual {v5, v0, v1}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lbr/com/itau/widgets/material/MaterialEditText;->singleLineEllipsis:Z

    .line 358
    sget v0, Lbr/com/itau/widgets/material/R$styleable;->MaterialEditText_met_helperText:I

    invoke-virtual {v5, v0}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v0

    move-object/from16 v1, p0

    iput-object v0, v1, Lbr/com/itau/widgets/material/MaterialEditText;->helperText:Ljava/lang/String;

    .line 359
    sget v0, Lbr/com/itau/widgets/material/R$styleable;->MaterialEditText_met_helperTextColor:I

    const/4 v1, -0x1

    invoke-virtual {v5, v0, v1}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    move-object/from16 v1, p0

    iput v0, v1, Lbr/com/itau/widgets/material/MaterialEditText;->helperTextColor:I

    .line 360
    sget v0, Lbr/com/itau/widgets/material/R$styleable;->MaterialEditText_met_minBottomTextLines:I

    const/4 v1, 0x0

    invoke-virtual {v5, v0, v1}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    move-object/from16 v1, p0

    iput v0, v1, Lbr/com/itau/widgets/material/MaterialEditText;->minBottomTextLines:I

    .line 361
    sget v0, Lbr/com/itau/widgets/material/R$styleable;->MaterialEditText_met_accentTypeface:I

    invoke-virtual {v5, v0}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 362
    .local v8, "fontPathForAccent":Ljava/lang/String;
    if-eqz v8, :cond_146

    invoke-virtual/range {p0 .. p0}, Lbr/com/itau/widgets/material/MaterialEditText;->isInEditMode()Z

    move-result v0

    if-nez v0, :cond_146

    .line 363
    move-object/from16 v0, p0

    invoke-direct {v0, v8}, Lbr/com/itau/widgets/material/MaterialEditText;->getCustomTypeface(Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v0

    move-object/from16 v1, p0

    iput-object v0, v1, Lbr/com/itau/widgets/material/MaterialEditText;->accentTypeface:Landroid/graphics/Typeface;

    .line 364
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/material/MaterialEditText;->textPaint:Landroid/text/TextPaint;

    move-object/from16 v1, p0

    iget-object v1, v1, Lbr/com/itau/widgets/material/MaterialEditText;->accentTypeface:Landroid/graphics/Typeface;

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 366
    :cond_146
    sget v0, Lbr/com/itau/widgets/material/R$styleable;->MaterialEditText_met_typeface:I

    invoke-virtual {v5, v0}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 367
    .local v9, "fontPathForView":Ljava/lang/String;
    if-eqz v9, :cond_167

    invoke-virtual/range {p0 .. p0}, Lbr/com/itau/widgets/material/MaterialEditText;->isInEditMode()Z

    move-result v0

    if-nez v0, :cond_167

    .line 368
    move-object/from16 v0, p0

    invoke-direct {v0, v9}, Lbr/com/itau/widgets/material/MaterialEditText;->getCustomTypeface(Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v0

    move-object/from16 v1, p0

    iput-object v0, v1, Lbr/com/itau/widgets/material/MaterialEditText;->typeface:Landroid/graphics/Typeface;

    .line 369
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/material/MaterialEditText;->typeface:Landroid/graphics/Typeface;

    move-object/from16 v1, p0

    invoke-virtual {v1, v0}, Lbr/com/itau/widgets/material/MaterialEditText;->setTypeface(Landroid/graphics/Typeface;)V

    .line 371
    :cond_167
    sget v0, Lbr/com/itau/widgets/material/R$styleable;->MaterialEditText_met_floatingLabelText:I

    invoke-virtual {v5, v0}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v0

    move-object/from16 v1, p0

    iput-object v0, v1, Lbr/com/itau/widgets/material/MaterialEditText;->floatingLabelText:Ljava/lang/CharSequence;

    .line 372
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/material/MaterialEditText;->floatingLabelText:Ljava/lang/CharSequence;

    if-nez v0, :cond_17f

    .line 373
    invoke-virtual/range {p0 .. p0}, Lbr/com/itau/widgets/material/MaterialEditText;->getHint()Ljava/lang/CharSequence;

    move-result-object v0

    move-object/from16 v1, p0

    iput-object v0, v1, Lbr/com/itau/widgets/material/MaterialEditText;->floatingLabelText:Ljava/lang/CharSequence;

    .line 375
    :cond_17f
    sget v0, Lbr/com/itau/widgets/material/R$styleable;->MaterialEditText_met_floatingLabelPadding:I

    move-object/from16 v1, p0

    iget v1, v1, Lbr/com/itau/widgets/material/MaterialEditText;->bottomSpacing:I

    invoke-virtual {v5, v0, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    move-object/from16 v1, p0

    iput v0, v1, Lbr/com/itau/widgets/material/MaterialEditText;->floatingLabelPadding:I

    .line 376
    sget v0, Lbr/com/itau/widgets/material/R$styleable;->MaterialEditText_met_floatingLabelTextSize:I

    invoke-virtual/range {p0 .. p0}, Lbr/com/itau/widgets/material/MaterialEditText;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lbr/com/itau/widgets/material/R$dimen;->floating_label_text_size:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-virtual {v5, v0, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    move-object/from16 v1, p0

    iput v0, v1, Lbr/com/itau/widgets/material/MaterialEditText;->floatingLabelTextSize:I

    .line 377
    sget v0, Lbr/com/itau/widgets/material/R$styleable;->MaterialEditText_met_floatingLabelTextColor:I

    const/4 v1, -0x1

    invoke-virtual {v5, v0, v1}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    move-object/from16 v1, p0

    iput v0, v1, Lbr/com/itau/widgets/material/MaterialEditText;->floatingLabelTextColor:I

    .line 378
    sget v0, Lbr/com/itau/widgets/material/R$styleable;->MaterialEditText_met_floatingLabelAnimating:I

    const/4 v1, 0x1

    invoke-virtual {v5, v0, v1}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lbr/com/itau/widgets/material/MaterialEditText;->floatingLabelAnimating:Z

    .line 379
    sget v0, Lbr/com/itau/widgets/material/R$styleable;->MaterialEditText_met_bottomTextSize:I

    invoke-virtual/range {p0 .. p0}, Lbr/com/itau/widgets/material/MaterialEditText;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lbr/com/itau/widgets/material/R$dimen;->bottom_text_size:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-virtual {v5, v0, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    move-object/from16 v1, p0

    iput v0, v1, Lbr/com/itau/widgets/material/MaterialEditText;->bottomTextSize:I

    .line 380
    sget v0, Lbr/com/itau/widgets/material/R$styleable;->MaterialEditText_met_hideUnderline:I

    const/4 v1, 0x0

    invoke-virtual {v5, v0, v1}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lbr/com/itau/widgets/material/MaterialEditText;->hideUnderline:Z

    .line 381
    sget v0, Lbr/com/itau/widgets/material/R$styleable;->MaterialEditText_met_underlineColor:I

    const/4 v1, -0x1

    invoke-virtual {v5, v0, v1}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    move-object/from16 v1, p0

    iput v0, v1, Lbr/com/itau/widgets/material/MaterialEditText;->underlineColor:I

    .line 382
    sget v0, Lbr/com/itau/widgets/material/R$styleable;->MaterialEditText_met_autoValidate:I

    const/4 v1, 0x0

    invoke-virtual {v5, v0, v1}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lbr/com/itau/widgets/material/MaterialEditText;->autoValidate:Z

    .line 383
    sget v0, Lbr/com/itau/widgets/material/R$styleable;->MaterialEditText_met_iconLeft:I

    const/4 v1, -0x1

    invoke-virtual {v5, v0, v1}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    move-object/from16 v1, p0

    invoke-direct {v1, v0}, Lbr/com/itau/widgets/material/MaterialEditText;->generateIconBitmaps(I)[Landroid/graphics/Bitmap;

    move-result-object v0

    move-object/from16 v1, p0

    iput-object v0, v1, Lbr/com/itau/widgets/material/MaterialEditText;->iconLeftBitmaps:[Landroid/graphics/Bitmap;

    .line 384
    sget v0, Lbr/com/itau/widgets/material/R$styleable;->MaterialEditText_met_iconRight:I

    const/4 v1, -0x1

    invoke-virtual {v5, v0, v1}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    move-object/from16 v1, p0

    invoke-direct {v1, v0}, Lbr/com/itau/widgets/material/MaterialEditText;->generateIconBitmaps(I)[Landroid/graphics/Bitmap;

    move-result-object v0

    move-object/from16 v1, p0

    iput-object v0, v1, Lbr/com/itau/widgets/material/MaterialEditText;->iconRightBitmaps:[Landroid/graphics/Bitmap;

    .line 385
    sget v0, Lbr/com/itau/widgets/material/R$styleable;->MaterialEditText_met_clearButton:I

    const/4 v1, 0x0

    invoke-virtual {v5, v0, v1}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lbr/com/itau/widgets/material/MaterialEditText;->showClearButton:Z

    .line 386
    sget v0, Lbr/com/itau/widgets/material/R$drawable;->met_ic_clear:I

    move-object/from16 v1, p0

    invoke-direct {v1, v0}, Lbr/com/itau/widgets/material/MaterialEditText;->generateIconBitmaps(I)[Landroid/graphics/Bitmap;

    move-result-object v0

    move-object/from16 v1, p0

    iput-object v0, v1, Lbr/com/itau/widgets/material/MaterialEditText;->clearButtonBitmaps:[Landroid/graphics/Bitmap;

    .line 387
    sget v0, Lbr/com/itau/widgets/material/R$styleable;->MaterialEditText_met_iconPadding:I

    move-object/from16 v1, p0

    const/16 v2, 0x10

    invoke-direct {v1, v2}, Lbr/com/itau/widgets/material/MaterialEditText;->getPixel(I)I

    move-result v1

    invoke-virtual {v5, v0, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    move-object/from16 v1, p0

    iput v0, v1, Lbr/com/itau/widgets/material/MaterialEditText;->iconPadding:I

    .line 388
    sget v0, Lbr/com/itau/widgets/material/R$styleable;->MaterialEditText_met_floatingLabelAlwaysShown:I

    const/4 v1, 0x0

    invoke-virtual {v5, v0, v1}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lbr/com/itau/widgets/material/MaterialEditText;->floatingLabelAlwaysShown:Z

    .line 389
    sget v0, Lbr/com/itau/widgets/material/R$styleable;->MaterialEditText_met_helperTextAlwaysShown:I

    const/4 v1, 0x0

    invoke-virtual {v5, v0, v1}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lbr/com/itau/widgets/material/MaterialEditText;->helperTextAlwaysShown:Z

    .line 390
    sget v0, Lbr/com/itau/widgets/material/R$styleable;->MaterialEditText_met_validateOnFocusLost:I

    const/4 v1, 0x0

    invoke-virtual {v5, v0, v1}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lbr/com/itau/widgets/material/MaterialEditText;->validateOnFocusLost:Z

    .line 391
    sget v0, Lbr/com/itau/widgets/material/R$styleable;->MaterialEditText_met_checkCharactersCountAtBeginning:I

    const/4 v1, 0x1

    invoke-virtual {v5, v0, v1}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lbr/com/itau/widgets/material/MaterialEditText;->checkCharactersCountAtBeginning:Z

    .line 392
    sget v0, Lbr/com/itau/widgets/material/R$styleable;->MaterialEditText_met_alwaysHighlighted:I

    const/4 v1, 0x0

    invoke-virtual {v5, v0, v1}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lbr/com/itau/widgets/material/MaterialEditText;->alwaysHighlighted:Z

    .line 393
    sget v0, Lbr/com/itau/widgets/material/R$styleable;->MaterialEditText_met_errorLineWidth:I

    move-object/from16 v1, p0

    const/4 v2, 0x2

    invoke-direct {v1, v2}, Lbr/com/itau/widgets/material/MaterialEditText;->getPixel(I)I

    move-result v1

    invoke-virtual {v5, v0, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    move-object/from16 v1, p0

    iput v0, v1, Lbr/com/itau/widgets/material/MaterialEditText;->errorLineWidth:I

    .line 394
    sget v0, Lbr/com/itau/widgets/material/R$styleable;->MaterialEditText_met_normalLineWidth:I

    move-object/from16 v1, p0

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Lbr/com/itau/widgets/material/MaterialEditText;->getPixel(I)I

    move-result v1

    invoke-virtual {v5, v0, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    move-object/from16 v1, p0

    iput v0, v1, Lbr/com/itau/widgets/material/MaterialEditText;->normalLineWidth:I

    .line 395
    sget v0, Lbr/com/itau/widgets/material/R$styleable;->MaterialEditText_met_highlightLineWidth:I

    move-object/from16 v1, p0

    const/4 v2, 0x2

    invoke-direct {v1, v2}, Lbr/com/itau/widgets/material/MaterialEditText;->getPixel(I)I

    move-result v1

    invoke-virtual {v5, v0, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    move-object/from16 v1, p0

    iput v0, v1, Lbr/com/itau/widgets/material/MaterialEditText;->highlightLineWidth:I

    .line 396
    sget v0, Lbr/com/itau/widgets/material/R$styleable;->MaterialEditText_met_dottedDisabledLine:I

    const/4 v1, 0x1

    invoke-virtual {v5, v0, v1}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lbr/com/itau/widgets/material/MaterialEditText;->dottedDisabledLine:Z

    .line 397
    sget v0, Lbr/com/itau/widgets/material/R$styleable;->MaterialEditText_met_highlightUnderlineColor:I

    const/4 v1, -0x1

    invoke-virtual {v5, v0, v1}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    move-object/from16 v1, p0

    iput v0, v1, Lbr/com/itau/widgets/material/MaterialEditText;->highlightUnderlineColor:I

    .line 399
    invoke-virtual {v5}, Landroid/content/res/TypedArray;->recycle()V

    .line 401
    invoke-virtual/range {p0 .. p0}, Lbr/com/itau/widgets/material/MaterialEditText;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v0

    if-eqz v0, :cond_2cc

    invoke-virtual/range {p0 .. p0}, Lbr/com/itau/widgets/material/MaterialEditText;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-eqz v0, :cond_2cc

    const/4 v0, 0x1

    goto :goto_2cd

    :cond_2cc
    const/4 v0, 0x0

    :goto_2cd
    move-object/from16 v1, p0

    iput-boolean v0, v1, Lbr/com/itau/widgets/material/MaterialEditText;->hasContentDescription:Z

    .line 403
    const/4 v0, 0x1

    new-array v10, v0, [I

    fill-array-data v10, :array_35c

    .line 406
    .local v10, "isPassword":[I
    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-virtual {v0, v1, v10}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v11

    .line 407
    .local v11, "isPasswordTypedArray":Landroid/content/res/TypedArray;
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {v11, v0, v1}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lbr/com/itau/widgets/material/MaterialEditText;->isPasswordAttrOn:Z

    .line 409
    const/4 v0, 0x5

    new-array v12, v0, [I

    fill-array-data v12, :array_362

    .line 416
    .local v12, "paddings":[I
    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-virtual {v0, v1, v12}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v13

    .line 417
    .local v13, "paddingsTypedArray":Landroid/content/res/TypedArray;
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {v13, v0, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v14

    .line 418
    .local v14, "padding":I
    const/4 v0, 0x1

    invoke-virtual {v13, v0, v14}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    move-object/from16 v1, p0

    iput v0, v1, Lbr/com/itau/widgets/material/MaterialEditText;->innerPaddingLeft:I

    .line 419
    const/4 v0, 0x2

    invoke-virtual {v13, v0, v14}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    move-object/from16 v1, p0

    iput v0, v1, Lbr/com/itau/widgets/material/MaterialEditText;->innerPaddingTop:I

    .line 420
    const/4 v0, 0x3

    invoke-virtual {v13, v0, v14}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    move-object/from16 v1, p0

    iput v0, v1, Lbr/com/itau/widgets/material/MaterialEditText;->innerPaddingRight:I

    .line 421
    const/4 v0, 0x4

    invoke-virtual {v13, v0, v14}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    move-object/from16 v1, p0

    iput v0, v1, Lbr/com/itau/widgets/material/MaterialEditText;->innerPaddingBottom:I

    .line 422
    invoke-virtual {v13}, Landroid/content/res/TypedArray;->recycle()V

    .line 424
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_331

    .line 425
    move-object/from16 v0, p0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lbr/com/itau/widgets/material/MaterialEditText;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_337

    .line 427
    :cond_331
    move-object/from16 v0, p0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lbr/com/itau/widgets/material/MaterialEditText;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 429
    :goto_337
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lbr/com/itau/widgets/material/MaterialEditText;->singleLineEllipsis:Z

    if-eqz v0, :cond_349

    .line 430
    invoke-virtual/range {p0 .. p0}, Lbr/com/itau/widgets/material/MaterialEditText;->getTransformationMethod()Landroid/text/method/TransformationMethod;

    move-result-object v15

    .line 431
    .local v15, "transformationMethod":Landroid/text/method/TransformationMethod;
    invoke-virtual/range {p0 .. p0}, Lbr/com/itau/widgets/material/MaterialEditText;->setSingleLine()V

    .line 432
    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Lbr/com/itau/widgets/material/MaterialEditText;->setTransformationMethod(Landroid/text/method/TransformationMethod;)V

    .line 434
    .end local v15    # "transformationMethod":Landroid/text/method/TransformationMethod;
    :cond_349
    invoke-direct/range {p0 .. p0}, Lbr/com/itau/widgets/material/MaterialEditText;->initMinBottomLines()V

    .line 435
    invoke-direct/range {p0 .. p0}, Lbr/com/itau/widgets/material/MaterialEditText;->initPadding()V

    .line 436
    invoke-direct/range {p0 .. p0}, Lbr/com/itau/widgets/material/MaterialEditText;->initText()V

    .line 437
    invoke-direct/range {p0 .. p0}, Lbr/com/itau/widgets/material/MaterialEditText;->initFloatingLabel()V

    .line 438
    invoke-direct/range {p0 .. p0}, Lbr/com/itau/widgets/material/MaterialEditText;->initTextWatcher()V

    .line 439
    invoke-direct/range {p0 .. p0}, Lbr/com/itau/widgets/material/MaterialEditText;->checkCharactersCount()V

    .line 440
    return-void

    :array_35c
    .array-data 4
        0x101015c
    .end array-data

    :array_362
    .array-data 4
        0x10100d5
        0x10100d6
        0x10100d7
        0x10100d8
        0x10100d9
    .end array-data
.end method

.method private initFloatingLabel()V
    .registers 2

    .line 923
    new-instance v0, Lbr/com/itau/widgets/material/MaterialEditText$2;

    invoke-direct {v0, p0}, Lbr/com/itau/widgets/material/MaterialEditText$2;-><init>(Lbr/com/itau/widgets/material/MaterialEditText;)V

    invoke-virtual {p0, v0}, Lbr/com/itau/widgets/material/MaterialEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 948
    new-instance v0, Lbr/com/itau/widgets/material/MaterialEditText$3;

    invoke-direct {v0, p0}, Lbr/com/itau/widgets/material/MaterialEditText$3;-><init>(Lbr/com/itau/widgets/material/MaterialEditText;)V

    iput-object v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->innerFocusChangeListener:Landroid/view/View$OnFocusChangeListener;

    .line 966
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->innerFocusChangeListener:Landroid/view/View$OnFocusChangeListener;

    invoke-super {p0, v0}, Landroid/support/v7/widget/AppCompatEditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 967
    return-void
.end method

.method private initMinBottomLines()V
    .registers 3

    .line 809
    iget v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->minCharacters:I

    if-gtz v0, :cond_14

    iget v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->maxCharacters:I

    if-gtz v0, :cond_14

    iget-boolean v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->singleLineEllipsis:Z

    if-nez v0, :cond_14

    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->tempErrorText:Ljava/lang/String;

    if-nez v0, :cond_14

    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->helperText:Ljava/lang/String;

    if-eqz v0, :cond_16

    :cond_14
    const/4 v1, 0x1

    goto :goto_17

    :cond_16
    const/4 v1, 0x0

    .line 810
    .local v1, "extendBottom":Z
    :goto_17
    iget v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->minBottomTextLines:I

    if-lez v0, :cond_1e

    iget v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->minBottomTextLines:I

    goto :goto_23

    :cond_1e
    if-eqz v1, :cond_22

    const/4 v0, 0x1

    goto :goto_23

    :cond_22
    const/4 v0, 0x0

    :goto_23
    iput v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->minBottomLines:I

    int-to-float v0, v0

    iput v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->currentBottomLines:F

    .line 811
    return-void
.end method

.method private initPadding()V
    .registers 5

    .line 796
    iget-boolean v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->floatingLabelEnabled:Z

    if-eqz v0, :cond_a

    iget v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->floatingLabelTextSize:I

    iget v1, p0, Lbr/com/itau/widgets/material/MaterialEditText;->floatingLabelPadding:I

    add-int/2addr v0, v1

    goto :goto_c

    :cond_a
    iget v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->floatingLabelPadding:I

    :goto_c
    iput v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->extraPaddingTop:I

    .line 797
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->textPaint:Landroid/text/TextPaint;

    iget v1, p0, Lbr/com/itau/widgets/material/MaterialEditText;->bottomTextSize:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 798
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->textPaint:Landroid/text/TextPaint;

    invoke-virtual {v0}, Landroid/text/TextPaint;->getFontMetrics()Landroid/graphics/Paint$FontMetrics;

    move-result-object v3

    .line 799
    .local v3, "textMetrics":Landroid/graphics/Paint$FontMetrics;
    iget v0, v3, Landroid/graphics/Paint$FontMetrics;->descent:F

    iget v1, v3, Landroid/graphics/Paint$FontMetrics;->ascent:F

    sub-float/2addr v0, v1

    iget v1, p0, Lbr/com/itau/widgets/material/MaterialEditText;->currentBottomLines:F

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iget-boolean v1, p0, Lbr/com/itau/widgets/material/MaterialEditText;->hideUnderline:Z

    if-eqz v1, :cond_2c

    iget v1, p0, Lbr/com/itau/widgets/material/MaterialEditText;->bottomSpacing:I

    goto :goto_30

    :cond_2c
    iget v1, p0, Lbr/com/itau/widgets/material/MaterialEditText;->bottomSpacing:I

    mul-int/lit8 v1, v1, 0x2

    :goto_30
    add-int/2addr v0, v1

    iput v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->extraPaddingBottom:I

    .line 800
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->iconLeftBitmaps:[Landroid/graphics/Bitmap;

    if-nez v0, :cond_39

    const/4 v0, 0x0

    goto :goto_3e

    :cond_39
    iget v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->iconOuterWidth:I

    iget v1, p0, Lbr/com/itau/widgets/material/MaterialEditText;->iconPadding:I

    add-int/2addr v0, v1

    :goto_3e
    iput v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->extraPaddingLeft:I

    .line 801
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->iconRightBitmaps:[Landroid/graphics/Bitmap;

    if-nez v0, :cond_46

    const/4 v0, 0x0

    goto :goto_4b

    :cond_46
    iget v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->iconOuterWidth:I

    iget v1, p0, Lbr/com/itau/widgets/material/MaterialEditText;->iconPadding:I

    add-int/2addr v0, v1

    :goto_4b
    iput v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->extraPaddingRight:I

    .line 802
    invoke-direct {p0}, Lbr/com/itau/widgets/material/MaterialEditText;->correctPaddings()V

    .line 803
    return-void
.end method

.method private initText()V
    .registers 3

    .line 509
    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_27

    .line 510
    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    .line 511
    .local v1, "text":Ljava/lang/CharSequence;
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lbr/com/itau/widgets/material/MaterialEditText;->setText(Ljava/lang/CharSequence;)V

    .line 512
    invoke-direct {p0}, Lbr/com/itau/widgets/material/MaterialEditText;->resetHintTextColor()V

    .line 513
    invoke-virtual {p0, v1}, Lbr/com/itau/widgets/material/MaterialEditText;->setText(Ljava/lang/CharSequence;)V

    .line 514
    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    invoke-virtual {p0, v0}, Lbr/com/itau/widgets/material/MaterialEditText;->setSelection(I)V

    .line 515
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->floatingLabelFraction:F

    .line 516
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->floatingLabelShown:Z

    .line 517
    .end local v1    # "text":Ljava/lang/CharSequence;
    goto :goto_2a

    .line 518
    :cond_27
    invoke-direct {p0}, Lbr/com/itau/widgets/material/MaterialEditText;->resetHintTextColor()V

    .line 520
    :goto_2a
    invoke-direct {p0}, Lbr/com/itau/widgets/material/MaterialEditText;->resetTextColor()V

    .line 521
    return-void
.end method

.method private initTextWatcher()V
    .registers 2

    .line 524
    new-instance v0, Lbr/com/itau/widgets/material/MaterialEditText$1;

    invoke-direct {v0, p0}, Lbr/com/itau/widgets/material/MaterialEditText$1;-><init>(Lbr/com/itau/widgets/material/MaterialEditText;)V

    invoke-virtual {p0, v0}, Lbr/com/itau/widgets/material/MaterialEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 544
    return-void
.end method

.method private insideClearButton(Landroid/view/MotionEvent;)Z
    .registers 11
    .param p1, "event"    # Landroid/view/MotionEvent;

    .line 1571
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    .line 1572
    .local v3, "x":F
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    .line 1573
    .local v4, "y":F
    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialEditText;->getScrollX()I

    move-result v0

    iget-object v1, p0, Lbr/com/itau/widgets/material/MaterialEditText;->iconLeftBitmaps:[Landroid/graphics/Bitmap;

    if-nez v1, :cond_12

    const/4 v1, 0x0

    goto :goto_17

    :cond_12
    iget v1, p0, Lbr/com/itau/widgets/material/MaterialEditText;->iconOuterWidth:I

    iget v2, p0, Lbr/com/itau/widgets/material/MaterialEditText;->iconPadding:I

    add-int/2addr v1, v2

    :goto_17
    add-int v5, v0, v1

    .line 1574
    .local v5, "startX":I
    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialEditText;->getScrollX()I

    move-result v0

    iget-object v1, p0, Lbr/com/itau/widgets/material/MaterialEditText;->iconRightBitmaps:[Landroid/graphics/Bitmap;

    if-nez v1, :cond_26

    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialEditText;->getWidth()I

    move-result v1

    goto :goto_30

    :cond_26
    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialEditText;->getWidth()I

    move-result v1

    iget v2, p0, Lbr/com/itau/widgets/material/MaterialEditText;->iconOuterWidth:I

    sub-int/2addr v1, v2

    iget v2, p0, Lbr/com/itau/widgets/material/MaterialEditText;->iconPadding:I

    sub-int/2addr v1, v2

    :goto_30
    add-int v6, v0, v1

    .line 1576
    .local v6, "endX":I
    invoke-direct {p0}, Lbr/com/itau/widgets/material/MaterialEditText;->isRTL()Z

    move-result v0

    if-eqz v0, :cond_3a

    .line 1577
    move v7, v5

    .local v7, "buttonLeft":I
    goto :goto_3e

    .line 1579
    .end local v7    # "buttonLeft":I
    :cond_3a
    iget v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->iconOuterWidth:I

    sub-int v7, v6, v0

    .line 1581
    .local v7, "buttonLeft":I
    :goto_3e
    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialEditText;->getScrollY()I

    move-result v0

    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialEditText;->getHeight()I

    move-result v1

    add-int/2addr v0, v1

    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialEditText;->getPaddingBottom()I

    move-result v1

    sub-int/2addr v0, v1

    iget v1, p0, Lbr/com/itau/widgets/material/MaterialEditText;->bottomSpacing:I

    add-int/2addr v0, v1

    iget v1, p0, Lbr/com/itau/widgets/material/MaterialEditText;->iconOuterHeight:I

    sub-int v8, v0, v1

    .line 1582
    .local v8, "buttonTop":I
    int-to-float v0, v7

    cmpl-float v0, v3, v0

    if-ltz v0, :cond_6f

    iget v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->iconOuterWidth:I

    add-int/2addr v0, v7

    int-to-float v0, v0

    cmpg-float v0, v3, v0

    if-gez v0, :cond_6f

    int-to-float v0, v8

    cmpl-float v0, v4, v0

    if-ltz v0, :cond_6f

    iget v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->iconOuterHeight:I

    add-int/2addr v0, v8

    int-to-float v0, v0

    cmpg-float v0, v4, v0

    if-gez v0, :cond_6f

    const/4 v0, 0x1

    goto :goto_70

    :cond_6f
    const/4 v0, 0x0

    :goto_70
    return v0
.end method

.method private isInternalValid()Z
    .registers 2

    .line 1181
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->tempErrorText:Ljava/lang/String;

    if-nez v0, :cond_c

    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialEditText;->isCharactersCountValid()Z

    move-result v0

    if-eqz v0, :cond_c

    const/4 v0, 0x1

    goto :goto_d

    :cond_c
    const/4 v0, 0x0

    :goto_d
    return v0
.end method

.method private isPasswordInputType()Z
    .registers 6

    .line 443
    iget-boolean v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->isPasswordAttrOn:Z

    if-eqz v0, :cond_6

    .line 444
    const/4 v0, 0x1

    return v0

    .line 446
    :cond_6
    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialEditText;->getInputType()I

    move-result v2

    .line 447
    .local v2, "inputType":I
    const/4 v0, 0x4

    new-array v3, v0, [I

    fill-array-data v3, :array_22

    .line 452
    .local v3, "passwordType":[I
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_11
    array-length v0, v3

    if-ge v4, v0, :cond_20

    .line 453
    aget v0, v3, v4

    and-int/2addr v0, v2

    aget v1, v3, v4

    if-ne v0, v1, :cond_1d

    .line 454
    const/4 v0, 0x1

    return v0

    .line 452
    :cond_1d
    add-int/lit8 v4, v4, 0x1

    goto :goto_11

    .line 458
    .end local v4    # "i":I
    :cond_20
    const/4 v0, 0x0

    return v0

    :array_22
    .array-data 4
        0x80
        0x10
        0x90
        0xe0
    .end array-data
.end method

.method private isRTL()Z
    .registers 4
    .annotation build Landroid/annotation/TargetApi;
        value = 0x11
    .end annotation

    .line 1473
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x11

    if-ge v0, v1, :cond_8

    .line 1474
    const/4 v0, 0x0

    return v0

    .line 1476
    :cond_8
    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialEditText;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    .line 1477
    .local v2, "config":Landroid/content/res/Configuration;
    invoke-virtual {v2}, Landroid/content/res/Configuration;->getLayoutDirection()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_19

    const/4 v0, 0x1

    goto :goto_1a

    :cond_19
    const/4 v0, 0x0

    :goto_1a
    return v0
.end method

.method private resetHintTextColor()V
    .registers 3

    .line 1034
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->textColorHintStateList:Landroid/content/res/ColorStateList;

    if-nez v0, :cond_11

    .line 1035
    iget v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->baseColor:I

    const v1, 0xffffff

    and-int/2addr v0, v1

    const/high16 v1, 0x44000000    # 512.0f

    or-int/2addr v0, v1

    invoke-virtual {p0, v0}, Lbr/com/itau/widgets/material/MaterialEditText;->setHintTextColor(I)V

    goto :goto_16

    .line 1037
    :cond_11
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->textColorHintStateList:Landroid/content/res/ColorStateList;

    invoke-virtual {p0, v0}, Lbr/com/itau/widgets/material/MaterialEditText;->setHintTextColor(Landroid/content/res/ColorStateList;)V

    .line 1039
    :goto_16
    return-void
.end method

.method private resetTextColor()V
    .registers 6

    .line 1009
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->textColorStateList:Landroid/content/res/ColorStateList;

    if-nez v0, :cond_3d

    .line 1010
    new-instance v0, Landroid/content/res/ColorStateList;

    const/4 v1, 0x2

    new-array v1, v1, [[I

    const/4 v2, 0x1

    new-array v2, v2, [I

    fill-array-data v2, :array_44

    const/4 v3, 0x0

    aput-object v2, v1, v3

    sget-object v2, Lbr/com/itau/widgets/material/MaterialEditText;->EMPTY_STATE_SET:[I

    const/4 v3, 0x1

    aput-object v2, v1, v3

    const/4 v2, 0x2

    new-array v2, v2, [I

    iget v3, p0, Lbr/com/itau/widgets/material/MaterialEditText;->baseColor:I

    const v4, 0xffffff

    and-int/2addr v3, v4

    const/high16 v4, -0x21000000

    or-int/2addr v3, v4

    const/4 v4, 0x0

    aput v3, v2, v4

    iget v3, p0, Lbr/com/itau/widgets/material/MaterialEditText;->baseColor:I

    const v4, 0xffffff

    and-int/2addr v3, v4

    const/high16 v4, 0x44000000    # 512.0f

    or-int/2addr v3, v4

    const/4 v4, 0x1

    aput v3, v2, v4

    invoke-direct {v0, v1, v2}, Landroid/content/res/ColorStateList;-><init>([[I[I)V

    iput-object v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->textColorStateList:Landroid/content/res/ColorStateList;

    .line 1011
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->textColorStateList:Landroid/content/res/ColorStateList;

    invoke-virtual {p0, v0}, Lbr/com/itau/widgets/material/MaterialEditText;->setTextColor(Landroid/content/res/ColorStateList;)V

    goto :goto_42

    .line 1013
    :cond_3d
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->textColorStateList:Landroid/content/res/ColorStateList;

    invoke-virtual {p0, v0}, Lbr/com/itau/widgets/material/MaterialEditText;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 1015
    :goto_42
    return-void

    nop

    :array_44
    .array-data 4
        0x101009e
    .end array-data
.end method

.method private scaleIcon(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .registers 10
    .param p1, "origin"    # Landroid/graphics/Bitmap;

    .line 634
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    .line 635
    .local v3, "width":I
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    .line 636
    .local v4, "height":I
    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    move-result v5

    .line 637
    .local v5, "size":I
    iget v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->iconSize:I

    if-ne v5, v0, :cond_11

    .line 638
    return-object p1

    .line 639
    :cond_11
    iget v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->iconSize:I

    if-le v5, v0, :cond_34

    .line 642
    iget v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->iconSize:I

    if-le v3, v0, :cond_24

    .line 643
    iget v6, p0, Lbr/com/itau/widgets/material/MaterialEditText;->iconSize:I

    .line 644
    .local v6, "scaledWidth":I
    iget v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->iconSize:I

    int-to-float v0, v0

    int-to-float v1, v4

    int-to-float v2, v3

    div-float/2addr v1, v2

    mul-float/2addr v0, v1

    float-to-int v7, v0

    .local v7, "scaledHeight":I
    goto :goto_2e

    .line 646
    .end local v6    # "scaledWidth":I
    .end local v7    # "scaledHeight":I
    :cond_24
    iget v7, p0, Lbr/com/itau/widgets/material/MaterialEditText;->iconSize:I

    .line 647
    .local v7, "scaledHeight":I
    iget v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->iconSize:I

    int-to-float v0, v0

    int-to-float v1, v3

    int-to-float v2, v4

    div-float/2addr v1, v2

    mul-float/2addr v0, v1

    float-to-int v6, v0

    .line 649
    .local v6, "scaledWidth":I
    :goto_2e
    const/4 v0, 0x0

    invoke-static {p1, v6, v7, v0}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0

    .line 651
    .end local v6    # "scaledWidth":I
    .end local v7    # "scaledHeight":I
    :cond_34
    return-object p1
.end method

.method private setFloatingLabelInternal(I)V
    .registers 3
    .param p1, "mode"    # I

    .line 1042
    sparse-switch p1, :sswitch_data_1a

    goto :goto_12

    .line 1044
    :sswitch_4
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->floatingLabelEnabled:Z

    .line 1045
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->highlightFloatingLabel:Z

    .line 1046
    goto :goto_18

    .line 1048
    :sswitch_b
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->floatingLabelEnabled:Z

    .line 1049
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->highlightFloatingLabel:Z

    .line 1050
    goto :goto_18

    .line 1052
    :goto_12
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->floatingLabelEnabled:Z

    .line 1053
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->highlightFloatingLabel:Z

    .line 1056
    :goto_18
    return-void

    nop

    :sswitch_data_1a
    .sparse-switch
        0x1 -> :sswitch_4
        0x2 -> :sswitch_b
    .end sparse-switch
.end method


# virtual methods
.method public addValidator(Lbr/com/itau/widgets/material/validation/METValidator;)Lbr/com/itau/widgets/material/MaterialEditText;
    .registers 3
    .param p1, "validator"    # Lbr/com/itau/widgets/material/validation/METValidator;

    .line 1276
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->validators:Ljava/util/List;

    if-nez v0, :cond_b

    .line 1277
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->validators:Ljava/util/List;

    .line 1279
    :cond_b
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->validators:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1280
    return-object p0
.end method

.method public clearValidators()V
    .registers 2

    .line 1284
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->validators:Ljava/util/List;

    if-eqz v0, :cond_9

    .line 1285
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->validators:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1287
    :cond_9
    return-void
.end method

.method public getAccentTypeface()Landroid/graphics/Typeface;
    .registers 2

    .line 702
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->accentTypeface:Landroid/graphics/Typeface;

    return-object v0
.end method

.method public getAccessibility()Ljava/lang/String;
    .registers 6

    .line 462
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 463
    .local v2, "builder":Ljava/lang/StringBuilder;
    iget-boolean v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->floatingLabelShown:Z

    if-eqz v0, :cond_1a

    .line 464
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->floatingLabelText:Ljava/lang/CharSequence;

    if-eqz v0, :cond_13

    .line 465
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->floatingLabelText:Ljava/lang/CharSequence;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    goto :goto_1a

    .line 467
    :cond_13
    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialEditText;->getHint()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 471
    :cond_1a
    :goto_1a
    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    .line 473
    .local v3, "text":Ljava/lang/String;
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_4a

    .line 474
    invoke-direct {p0}, Lbr/com/itau/widgets/material/MaterialEditText;->isPasswordInputType()Z

    move-result v0

    if-eqz v0, :cond_41

    .line 475
    const/16 v0, 0x20

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 476
    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialEditText;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lbr/com/itau/widgets/material/R$string;->ms_senha:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_56

    .line 478
    :cond_41
    const/16 v0, 0x20

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 479
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_56

    .line 482
    :cond_4a
    const/16 v0, 0x20

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 483
    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialEditText;->getHint()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 486
    :goto_56
    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialEditText;->getError()Ljava/lang/CharSequence;

    move-result-object v4

    .line 487
    .local v4, "error":Ljava/lang/CharSequence;
    if-eqz v4, :cond_6a

    invoke-interface {v4}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-lez v0, :cond_6a

    .line 488
    const/16 v0, 0x20

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 489
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 492
    :cond_6a
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getBottomTextSize()I
    .registers 2

    .line 783
    iget v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->bottomTextSize:I

    return v0
.end method

.method public getCurrentBottomLines()F
    .registers 2

    .line 674
    iget v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->currentBottomLines:F

    return v0
.end method

.method public getError()Ljava/lang/CharSequence;
    .registers 2

    .line 1166
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->tempErrorText:Ljava/lang/String;

    return-object v0
.end method

.method public getErrorColor()I
    .registers 2

    .line 1136
    iget v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->errorColor:I

    return v0
.end method

.method public getFloatingLabelFraction()F
    .registers 2

    .line 656
    iget v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->floatingLabelFraction:F

    return v0
.end method

.method public getFloatingLabelPadding()I
    .registers 2

    .line 1064
    iget v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->floatingLabelPadding:I

    return v0
.end method

.method public getFloatingLabelText()Ljava/lang/CharSequence;
    .registers 2

    .line 749
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->floatingLabelText:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public getFloatingLabelTextColor()I
    .registers 2

    .line 774
    iget v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->floatingLabelTextColor:I

    return v0
.end method

.method public getFloatingLabelTextSize()I
    .registers 2

    .line 765
    iget v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->floatingLabelTextSize:I

    return v0
.end method

.method public getFocusFraction()F
    .registers 2

    .line 665
    iget v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->focusFraction:F

    return v0
.end method

.method public getHelperText()Ljava/lang/String;
    .registers 2

    .line 1145
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->helperText:Ljava/lang/String;

    return-object v0
.end method

.method public getHelperTextColor()I
    .registers 2

    .line 1156
    iget v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->helperTextColor:I

    return v0
.end method

.method public getInnerPaddingBottom()I
    .registers 2

    .line 904
    iget v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->innerPaddingBottom:I

    return v0
.end method

.method public getInnerPaddingLeft()I
    .registers 2

    .line 911
    iget v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->innerPaddingLeft:I

    return v0
.end method

.method public getInnerPaddingRight()I
    .registers 2

    .line 918
    iget v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->innerPaddingRight:I

    return v0
.end method

.method public getInnerPaddingTop()I
    .registers 2

    .line 897
    iget v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->innerPaddingTop:I

    return v0
.end method

.method public getMaxCharacters()I
    .registers 2

    .line 1092
    iget v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->maxCharacters:I

    return v0
.end method

.method public getMinBottomTextLines()I
    .registers 2

    .line 1114
    iget v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->minBottomTextLines:I

    return v0
.end method

.method public getMinCharacters()I
    .registers 2

    .line 1103
    iget v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->minCharacters:I

    return v0
.end method

.method public getUnderlineColor()I
    .registers 2

    .line 735
    iget v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->underlineColor:I

    return v0
.end method

.method public getValidators()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Lbr/com/itau/widgets/material/validation/METValidator;>;"
        }
    .end annotation

    .line 1291
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->validators:Ljava/util/List;

    return-object v0
.end method

.method public hasValidators()Z
    .registers 2

    .line 1264
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->validators:Ljava/util/List;

    if-eqz v0, :cond_e

    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->validators:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_e

    const/4 v0, 0x1

    goto :goto_f

    :cond_e
    const/4 v0, 0x0

    :goto_f
    return v0
.end method

.method public isAutoValidate()Z
    .registers 2

    .line 1125
    iget-boolean v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->autoValidate:Z

    return v0
.end method

.method public isCharactersCountValid()Z
    .registers 2

    .line 1507
    iget-boolean v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->charactersCountValid:Z

    return v0
.end method

.method public isFloatingLabelAlwaysShown()Z
    .registers 2

    .line 683
    iget-boolean v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->floatingLabelAlwaysShown:Z

    return v0
.end method

.method public isFloatingLabelAnimating()Z
    .registers 2

    .line 1073
    iget-boolean v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->floatingLabelAnimating:Z

    return v0
.end method

.method public isHelperTextAlwaysShown()Z
    .registers 2

    .line 692
    iget-boolean v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->helperTextAlwaysShown:Z

    return v0
.end method

.method public isHideUnderline()Z
    .registers 2

    .line 715
    iget-boolean v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->hideUnderline:Z

    return v0
.end method

.method public isShowClearButton()Z
    .registers 2

    .line 581
    iget-boolean v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->showClearButton:Z

    return v0
.end method

.method public isValid(Ljava/lang/String;)Z
    .registers 5
    .param p1, "regex"    # Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 1191
    if-nez p1, :cond_4

    .line 1192
    const/4 v0, 0x0

    return v0

    .line 1194
    :cond_4
    invoke-static {p1}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v1

    .line 1195
    .local v1, "pattern":Ljava/util/regex/Pattern;
    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v2

    .line 1196
    .local v2, "matcher":Ljava/util/regex/Matcher;
    invoke-virtual {v2}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    return v0
.end method

.method public isValidateOnFocusLost()Z
    .registers 2

    .line 970
    iget-boolean v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->validateOnFocusLost:Z

    return v0
.end method

.method protected onAttachedToWindow()V
    .registers 2

    .line 853
    invoke-super {p0}, Landroid/support/v7/widget/AppCompatEditText;->onAttachedToWindow()V

    .line 854
    iget-boolean v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->firstShown:Z

    if-nez v0, :cond_a

    .line 855
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->firstShown:Z

    .line 857
    :cond_a
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .registers 19
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .line 1334
    invoke-virtual/range {p0 .. p0}, Lbr/com/itau/widgets/material/MaterialEditText;->getScrollX()I

    move-result v0

    move-object/from16 v1, p0

    iget-object v1, v1, Lbr/com/itau/widgets/material/MaterialEditText;->iconLeftBitmaps:[Landroid/graphics/Bitmap;

    if-nez v1, :cond_c

    const/4 v1, 0x0

    goto :goto_15

    :cond_c
    move-object/from16 v1, p0

    iget v1, v1, Lbr/com/itau/widgets/material/MaterialEditText;->iconOuterWidth:I

    move-object/from16 v2, p0

    iget v2, v2, Lbr/com/itau/widgets/material/MaterialEditText;->iconPadding:I

    add-int/2addr v1, v2

    :goto_15
    add-int v6, v0, v1

    .line 1335
    .local v6, "startX":I
    invoke-virtual/range {p0 .. p0}, Lbr/com/itau/widgets/material/MaterialEditText;->getScrollX()I

    move-result v0

    move-object/from16 v1, p0

    iget-object v1, v1, Lbr/com/itau/widgets/material/MaterialEditText;->iconRightBitmaps:[Landroid/graphics/Bitmap;

    if-nez v1, :cond_26

    invoke-virtual/range {p0 .. p0}, Lbr/com/itau/widgets/material/MaterialEditText;->getWidth()I

    move-result v1

    goto :goto_34

    :cond_26
    invoke-virtual/range {p0 .. p0}, Lbr/com/itau/widgets/material/MaterialEditText;->getWidth()I

    move-result v1

    move-object/from16 v2, p0

    iget v2, v2, Lbr/com/itau/widgets/material/MaterialEditText;->iconOuterWidth:I

    sub-int/2addr v1, v2

    move-object/from16 v2, p0

    iget v2, v2, Lbr/com/itau/widgets/material/MaterialEditText;->iconPadding:I

    sub-int/2addr v1, v2

    :goto_34
    add-int v7, v0, v1

    .line 1336
    .local v7, "endX":I
    invoke-virtual/range {p0 .. p0}, Lbr/com/itau/widgets/material/MaterialEditText;->getScrollY()I

    move-result v0

    invoke-virtual/range {p0 .. p0}, Lbr/com/itau/widgets/material/MaterialEditText;->getHeight()I

    move-result v1

    add-int/2addr v0, v1

    invoke-virtual/range {p0 .. p0}, Lbr/com/itau/widgets/material/MaterialEditText;->getPaddingBottom()I

    move-result v1

    sub-int v8, v0, v1

    .line 1339
    .local v8, "lineStartY":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/material/MaterialEditText;->paint:Landroid/graphics/Paint;

    const/16 v1, 0xff

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 1340
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/material/MaterialEditText;->iconLeftBitmaps:[Landroid/graphics/Bitmap;

    if-eqz v0, :cond_ad

    .line 1341
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/material/MaterialEditText;->iconLeftBitmaps:[Landroid/graphics/Bitmap;

    invoke-direct/range {p0 .. p0}, Lbr/com/itau/widgets/material/MaterialEditText;->isInternalValid()Z

    move-result v1

    if-nez v1, :cond_60

    const/4 v1, 0x3

    goto :goto_71

    :cond_60
    invoke-virtual/range {p0 .. p0}, Lbr/com/itau/widgets/material/MaterialEditText;->isEnabled()Z

    move-result v1

    if-nez v1, :cond_68

    const/4 v1, 0x2

    goto :goto_71

    :cond_68
    invoke-virtual/range {p0 .. p0}, Lbr/com/itau/widgets/material/MaterialEditText;->hasFocus()Z

    move-result v1

    if-eqz v1, :cond_70

    const/4 v1, 0x1

    goto :goto_71

    :cond_70
    const/4 v1, 0x0

    :goto_71
    aget-object v9, v0, v1

    .line 1342
    .local v9, "icon":Landroid/graphics/Bitmap;
    move-object/from16 v0, p0

    iget v0, v0, Lbr/com/itau/widgets/material/MaterialEditText;->iconPadding:I

    sub-int v0, v6, v0

    move-object/from16 v1, p0

    iget v1, v1, Lbr/com/itau/widgets/material/MaterialEditText;->iconOuterWidth:I

    sub-int/2addr v0, v1

    move-object/from16 v1, p0

    iget v1, v1, Lbr/com/itau/widgets/material/MaterialEditText;->iconOuterWidth:I

    invoke-virtual {v9}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    sub-int/2addr v1, v2

    div-int/lit8 v1, v1, 0x2

    add-int v10, v0, v1

    .line 1343
    .local v10, "iconLeft":I
    move-object/from16 v0, p0

    iget v0, v0, Lbr/com/itau/widgets/material/MaterialEditText;->bottomSpacing:I

    add-int/2addr v0, v8

    move-object/from16 v1, p0

    iget v1, v1, Lbr/com/itau/widgets/material/MaterialEditText;->iconOuterHeight:I

    sub-int/2addr v0, v1

    move-object/from16 v1, p0

    iget v1, v1, Lbr/com/itau/widgets/material/MaterialEditText;->iconOuterHeight:I

    invoke-virtual {v9}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    sub-int/2addr v1, v2

    div-int/lit8 v1, v1, 0x2

    add-int v11, v0, v1

    .line 1344
    .local v11, "iconTop":I
    int-to-float v0, v10

    int-to-float v1, v11

    move-object/from16 v2, p0

    iget-object v2, v2, Lbr/com/itau/widgets/material/MaterialEditText;->paint:Landroid/graphics/Paint;

    move-object/from16 v3, p1

    invoke-virtual {v3, v9, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 1346
    .end local v9    # "icon":Landroid/graphics/Bitmap;
    .end local v10    # "iconLeft":I
    .end local v11    # "iconTop":I
    :cond_ad
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/material/MaterialEditText;->iconRightBitmaps:[Landroid/graphics/Bitmap;

    if-eqz v0, :cond_106

    .line 1347
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/material/MaterialEditText;->iconRightBitmaps:[Landroid/graphics/Bitmap;

    invoke-direct/range {p0 .. p0}, Lbr/com/itau/widgets/material/MaterialEditText;->isInternalValid()Z

    move-result v1

    if-nez v1, :cond_bf

    const/4 v1, 0x3

    goto :goto_d0

    :cond_bf
    invoke-virtual/range {p0 .. p0}, Lbr/com/itau/widgets/material/MaterialEditText;->isEnabled()Z

    move-result v1

    if-nez v1, :cond_c7

    const/4 v1, 0x2

    goto :goto_d0

    :cond_c7
    invoke-virtual/range {p0 .. p0}, Lbr/com/itau/widgets/material/MaterialEditText;->hasFocus()Z

    move-result v1

    if-eqz v1, :cond_cf

    const/4 v1, 0x1

    goto :goto_d0

    :cond_cf
    const/4 v1, 0x0

    :goto_d0
    aget-object v9, v0, v1

    .line 1348
    .local v9, "icon":Landroid/graphics/Bitmap;
    move-object/from16 v0, p0

    iget v0, v0, Lbr/com/itau/widgets/material/MaterialEditText;->iconPadding:I

    add-int/2addr v0, v7

    move-object/from16 v1, p0

    iget v1, v1, Lbr/com/itau/widgets/material/MaterialEditText;->iconOuterWidth:I

    invoke-virtual {v9}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    sub-int/2addr v1, v2

    div-int/lit8 v1, v1, 0x2

    add-int v10, v0, v1

    .line 1349
    .local v10, "iconRight":I
    move-object/from16 v0, p0

    iget v0, v0, Lbr/com/itau/widgets/material/MaterialEditText;->bottomSpacing:I

    add-int/2addr v0, v8

    move-object/from16 v1, p0

    iget v1, v1, Lbr/com/itau/widgets/material/MaterialEditText;->iconOuterHeight:I

    sub-int/2addr v0, v1

    move-object/from16 v1, p0

    iget v1, v1, Lbr/com/itau/widgets/material/MaterialEditText;->iconOuterHeight:I

    invoke-virtual {v9}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    sub-int/2addr v1, v2

    div-int/lit8 v1, v1, 0x2

    add-int v11, v0, v1

    .line 1350
    .local v11, "iconTop":I
    int-to-float v0, v10

    int-to-float v1, v11

    move-object/from16 v2, p0

    iget-object v2, v2, Lbr/com/itau/widgets/material/MaterialEditText;->paint:Landroid/graphics/Paint;

    move-object/from16 v3, p1

    invoke-virtual {v3, v9, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 1354
    .end local v9    # "icon":Landroid/graphics/Bitmap;
    .end local v10    # "iconRight":I
    .end local v11    # "iconTop":I
    :cond_106
    invoke-virtual/range {p0 .. p0}, Lbr/com/itau/widgets/material/MaterialEditText;->hasFocus()Z

    move-result v0

    if-eqz v0, :cond_168

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lbr/com/itau/widgets/material/MaterialEditText;->showClearButton:Z

    if-eqz v0, :cond_168

    invoke-virtual/range {p0 .. p0}, Lbr/com/itau/widgets/material/MaterialEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_168

    .line 1355
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/material/MaterialEditText;->paint:Landroid/graphics/Paint;

    const/16 v1, 0xff

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 1357
    invoke-direct/range {p0 .. p0}, Lbr/com/itau/widgets/material/MaterialEditText;->isRTL()Z

    move-result v0

    if-eqz v0, :cond_12d

    .line 1358
    move v9, v6

    .local v9, "buttonLeft":I
    goto :goto_133

    .line 1360
    .end local v9    # "buttonLeft":I
    :cond_12d
    move-object/from16 v0, p0

    iget v0, v0, Lbr/com/itau/widgets/material/MaterialEditText;->iconOuterWidth:I

    sub-int v9, v7, v0

    .line 1362
    .local v9, "buttonLeft":I
    :goto_133
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/material/MaterialEditText;->clearButtonBitmaps:[Landroid/graphics/Bitmap;

    const/4 v1, 0x0

    aget-object v10, v0, v1

    .line 1363
    .local v10, "clearButtonBitmap":Landroid/graphics/Bitmap;
    move-object/from16 v0, p0

    iget v0, v0, Lbr/com/itau/widgets/material/MaterialEditText;->iconOuterWidth:I

    invoke-virtual {v10}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    sub-int/2addr v0, v1

    div-int/lit8 v0, v0, 0x2

    add-int/2addr v9, v0

    .line 1364
    move-object/from16 v0, p0

    iget v0, v0, Lbr/com/itau/widgets/material/MaterialEditText;->bottomSpacing:I

    add-int/2addr v0, v8

    move-object/from16 v1, p0

    iget v1, v1, Lbr/com/itau/widgets/material/MaterialEditText;->iconOuterHeight:I

    sub-int/2addr v0, v1

    move-object/from16 v1, p0

    iget v1, v1, Lbr/com/itau/widgets/material/MaterialEditText;->iconOuterHeight:I

    invoke-virtual {v10}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    sub-int/2addr v1, v2

    div-int/lit8 v1, v1, 0x2

    add-int v11, v0, v1

    .line 1365
    .local v11, "iconTop":I
    int-to-float v0, v9

    int-to-float v1, v11

    move-object/from16 v2, p0

    iget-object v2, v2, Lbr/com/itau/widgets/material/MaterialEditText;->paint:Landroid/graphics/Paint;

    move-object/from16 v3, p1

    invoke-virtual {v3, v10, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 1369
    .end local v9    # "buttonLeft":I
    .end local v10    # "clearButtonBitmap":Landroid/graphics/Bitmap;
    .end local v11    # "iconTop":I
    :cond_168
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lbr/com/itau/widgets/material/MaterialEditText;->hideUnderline:Z

    if-nez v0, :cond_263

    .line 1370
    move-object/from16 v0, p0

    iget v0, v0, Lbr/com/itau/widgets/material/MaterialEditText;->bottomSpacing:I

    add-int/2addr v8, v0

    .line 1371
    invoke-direct/range {p0 .. p0}, Lbr/com/itau/widgets/material/MaterialEditText;->isInternalValid()Z

    move-result v0

    if-nez v0, :cond_198

    .line 1372
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/material/MaterialEditText;->paint:Landroid/graphics/Paint;

    move-object/from16 v1, p0

    iget v1, v1, Lbr/com/itau/widgets/material/MaterialEditText;->errorColor:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 1373
    move-object/from16 v0, p1

    int-to-float v1, v6

    int-to-float v2, v8

    int-to-float v3, v7

    move-object/from16 v4, p0

    iget v4, v4, Lbr/com/itau/widgets/material/MaterialEditText;->errorLineWidth:I

    add-int/2addr v4, v8

    int-to-float v4, v4

    move-object/from16 v5, p0

    iget-object v5, v5, Lbr/com/itau/widgets/material/MaterialEditText;->paint:Landroid/graphics/Paint;

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    goto/16 :goto_263

    .line 1374
    :cond_198
    invoke-virtual/range {p0 .. p0}, Lbr/com/itau/widgets/material/MaterialEditText;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_203

    .line 1375
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/material/MaterialEditText;->paint:Landroid/graphics/Paint;

    move-object/from16 v1, p0

    iget v1, v1, Lbr/com/itau/widgets/material/MaterialEditText;->underlineColor:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_1ae

    move-object/from16 v1, p0

    iget v1, v1, Lbr/com/itau/widgets/material/MaterialEditText;->underlineColor:I

    goto :goto_1b9

    :cond_1ae
    move-object/from16 v1, p0

    iget v1, v1, Lbr/com/itau/widgets/material/MaterialEditText;->baseColor:I

    const v2, 0xffffff

    and-int/2addr v1, v2

    const/high16 v2, 0x44000000    # 512.0f

    or-int/2addr v1, v2

    :goto_1b9
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 1376
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lbr/com/itau/widgets/material/MaterialEditText;->dottedDisabledLine:Z

    if-eqz v0, :cond_1f0

    .line 1377
    move-object/from16 v0, p0

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lbr/com/itau/widgets/material/MaterialEditText;->getPixel(I)I

    move-result v0

    int-to-float v9, v0

    .line 1378
    .local v9, "interval":F
    const/4 v10, 0x0

    .local v10, "xOffset":F
    :goto_1cb
    invoke-virtual/range {p0 .. p0}, Lbr/com/itau/widgets/material/MaterialEditText;->getWidth()I

    move-result v0

    int-to-float v0, v0

    cmpg-float v0, v10, v0

    if-gez v0, :cond_1ee

    .line 1379
    move-object/from16 v0, p1

    int-to-float v1, v6

    add-float/2addr v1, v10

    int-to-float v2, v8

    int-to-float v3, v6

    add-float/2addr v3, v10

    add-float/2addr v3, v9

    move-object/from16 v4, p0

    iget v4, v4, Lbr/com/itau/widgets/material/MaterialEditText;->normalLineWidth:I

    add-int/2addr v4, v8

    int-to-float v4, v4

    move-object/from16 v5, p0

    iget-object v5, v5, Lbr/com/itau/widgets/material/MaterialEditText;->paint:Landroid/graphics/Paint;

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 1378
    const/high16 v0, 0x40400000    # 3.0f

    mul-float/2addr v0, v9

    add-float/2addr v10, v0

    goto :goto_1cb

    .line 1381
    .end local v9    # "interval":F
    .end local v10    # "xOffset":F
    :cond_1ee
    goto/16 :goto_263

    .line 1382
    :cond_1f0
    move-object/from16 v0, p1

    int-to-float v1, v6

    int-to-float v2, v8

    int-to-float v3, v7

    move-object/from16 v4, p0

    iget v4, v4, Lbr/com/itau/widgets/material/MaterialEditText;->normalLineWidth:I

    add-int/2addr v4, v8

    int-to-float v4, v4

    move-object/from16 v5, p0

    iget-object v5, v5, Lbr/com/itau/widgets/material/MaterialEditText;->paint:Landroid/graphics/Paint;

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    goto :goto_263

    .line 1384
    :cond_203
    invoke-virtual/range {p0 .. p0}, Lbr/com/itau/widgets/material/MaterialEditText;->hasFocus()Z

    move-result v0

    if-eqz v0, :cond_233

    .line 1385
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/material/MaterialEditText;->paint:Landroid/graphics/Paint;

    move-object/from16 v1, p0

    iget v1, v1, Lbr/com/itau/widgets/material/MaterialEditText;->highlightUnderlineColor:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_219

    move-object/from16 v1, p0

    iget v1, v1, Lbr/com/itau/widgets/material/MaterialEditText;->highlightUnderlineColor:I

    goto :goto_21d

    :cond_219
    move-object/from16 v1, p0

    iget v1, v1, Lbr/com/itau/widgets/material/MaterialEditText;->primaryColor:I

    :goto_21d
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 1386
    move-object/from16 v0, p1

    int-to-float v1, v6

    int-to-float v2, v8

    int-to-float v3, v7

    move-object/from16 v4, p0

    iget v4, v4, Lbr/com/itau/widgets/material/MaterialEditText;->highlightLineWidth:I

    add-int/2addr v4, v8

    int-to-float v4, v4

    move-object/from16 v5, p0

    iget-object v5, v5, Lbr/com/itau/widgets/material/MaterialEditText;->paint:Landroid/graphics/Paint;

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    goto :goto_263

    .line 1388
    :cond_233
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/material/MaterialEditText;->paint:Landroid/graphics/Paint;

    move-object/from16 v1, p0

    iget v1, v1, Lbr/com/itau/widgets/material/MaterialEditText;->underlineColor:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_243

    move-object/from16 v1, p0

    iget v1, v1, Lbr/com/itau/widgets/material/MaterialEditText;->underlineColor:I

    goto :goto_24e

    :cond_243
    move-object/from16 v1, p0

    iget v1, v1, Lbr/com/itau/widgets/material/MaterialEditText;->baseColor:I

    const v2, 0xffffff

    and-int/2addr v1, v2

    const/high16 v2, 0x1e000000

    or-int/2addr v1, v2

    :goto_24e
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 1389
    move-object/from16 v0, p1

    int-to-float v1, v6

    int-to-float v2, v8

    int-to-float v3, v7

    move-object/from16 v4, p0

    iget v4, v4, Lbr/com/itau/widgets/material/MaterialEditText;->normalLineWidth:I

    add-int/2addr v4, v8

    int-to-float v4, v4

    move-object/from16 v5, p0

    iget-object v5, v5, Lbr/com/itau/widgets/material/MaterialEditText;->paint:Landroid/graphics/Paint;

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 1393
    :cond_263
    :goto_263
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/material/MaterialEditText;->textPaint:Landroid/text/TextPaint;

    move-object/from16 v1, p0

    iget v1, v1, Lbr/com/itau/widgets/material/MaterialEditText;->bottomTextSize:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 1394
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/material/MaterialEditText;->textPaint:Landroid/text/TextPaint;

    invoke-virtual {v0}, Landroid/text/TextPaint;->getFontMetrics()Landroid/graphics/Paint$FontMetrics;

    move-result-object v9

    .line 1395
    .local v9, "textMetrics":Landroid/graphics/Paint$FontMetrics;
    iget v0, v9, Landroid/graphics/Paint$FontMetrics;->ascent:F

    neg-float v0, v0

    iget v1, v9, Landroid/graphics/Paint$FontMetrics;->descent:F

    sub-float v10, v0, v1

    .line 1396
    .local v10, "relativeHeight":F
    move-object/from16 v0, p0

    iget v0, v0, Lbr/com/itau/widgets/material/MaterialEditText;->bottomTextSize:I

    int-to-float v0, v0

    iget v1, v9, Landroid/graphics/Paint$FontMetrics;->ascent:F

    add-float/2addr v0, v1

    iget v1, v9, Landroid/graphics/Paint$FontMetrics;->descent:F

    add-float v11, v0, v1

    .line 1399
    .local v11, "bottomTextPadding":F
    invoke-virtual/range {p0 .. p0}, Lbr/com/itau/widgets/material/MaterialEditText;->hasFocus()Z

    move-result v0

    if-eqz v0, :cond_296

    invoke-direct/range {p0 .. p0}, Lbr/com/itau/widgets/material/MaterialEditText;->hasCharactersCounter()Z

    move-result v0

    if-nez v0, :cond_29c

    :cond_296
    invoke-virtual/range {p0 .. p0}, Lbr/com/itau/widgets/material/MaterialEditText;->isCharactersCountValid()Z

    move-result v0

    if-nez v0, :cond_2df

    .line 1400
    :cond_29c
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/material/MaterialEditText;->textPaint:Landroid/text/TextPaint;

    invoke-virtual/range {p0 .. p0}, Lbr/com/itau/widgets/material/MaterialEditText;->isCharactersCountValid()Z

    move-result v1

    if-eqz v1, :cond_2b2

    move-object/from16 v1, p0

    iget v1, v1, Lbr/com/itau/widgets/material/MaterialEditText;->baseColor:I

    const v2, 0xffffff

    and-int/2addr v1, v2

    const/high16 v2, 0x44000000    # 512.0f

    or-int/2addr v1, v2

    goto :goto_2b6

    :cond_2b2
    move-object/from16 v1, p0

    iget v1, v1, Lbr/com/itau/widgets/material/MaterialEditText;->errorColor:I

    :goto_2b6
    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setColor(I)V

    .line 1401
    invoke-direct/range {p0 .. p0}, Lbr/com/itau/widgets/material/MaterialEditText;->getCharactersCounterText()Ljava/lang/String;

    move-result-object v12

    .line 1402
    .local v12, "charactersCounterText":Ljava/lang/String;
    invoke-direct/range {p0 .. p0}, Lbr/com/itau/widgets/material/MaterialEditText;->isRTL()Z

    move-result v0

    if-eqz v0, :cond_2c5

    int-to-float v0, v6

    goto :goto_2cf

    :cond_2c5
    int-to-float v0, v7

    move-object/from16 v1, p0

    iget-object v1, v1, Lbr/com/itau/widgets/material/MaterialEditText;->textPaint:Landroid/text/TextPaint;

    invoke-virtual {v1, v12}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v1

    sub-float/2addr v0, v1

    :goto_2cf
    move-object/from16 v1, p0

    iget v1, v1, Lbr/com/itau/widgets/material/MaterialEditText;->bottomSpacing:I

    add-int/2addr v1, v8

    int-to-float v1, v1

    add-float/2addr v1, v10

    move-object/from16 v2, p0

    iget-object v2, v2, Lbr/com/itau/widgets/material/MaterialEditText;->textPaint:Landroid/text/TextPaint;

    move-object/from16 v3, p1

    invoke-virtual {v3, v12, v0, v1, v2}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 1406
    .end local v12    # "charactersCounterText":Ljava/lang/String;
    :cond_2df
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/material/MaterialEditText;->textLayout:Landroid/text/StaticLayout;

    if-eqz v0, :cond_369

    .line 1407
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/material/MaterialEditText;->tempErrorText:Ljava/lang/String;

    if-nez v0, :cond_301

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lbr/com/itau/widgets/material/MaterialEditText;->helperTextAlwaysShown:Z

    if-nez v0, :cond_2f7

    invoke-virtual/range {p0 .. p0}, Lbr/com/itau/widgets/material/MaterialEditText;->hasFocus()Z

    move-result v0

    if-eqz v0, :cond_369

    :cond_2f7
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/material/MaterialEditText;->helperText:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_369

    .line 1408
    :cond_301
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/material/MaterialEditText;->textPaint:Landroid/text/TextPaint;

    move-object/from16 v1, p0

    iget-object v1, v1, Lbr/com/itau/widgets/material/MaterialEditText;->tempErrorText:Ljava/lang/String;

    if-eqz v1, :cond_310

    move-object/from16 v1, p0

    iget v1, v1, Lbr/com/itau/widgets/material/MaterialEditText;->errorColor:I

    goto :goto_327

    :cond_310
    move-object/from16 v1, p0

    iget v1, v1, Lbr/com/itau/widgets/material/MaterialEditText;->helperTextColor:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_31c

    move-object/from16 v1, p0

    iget v1, v1, Lbr/com/itau/widgets/material/MaterialEditText;->helperTextColor:I

    goto :goto_327

    :cond_31c
    move-object/from16 v1, p0

    iget v1, v1, Lbr/com/itau/widgets/material/MaterialEditText;->baseColor:I

    const v2, 0xffffff

    and-int/2addr v1, v2

    const/high16 v2, 0x44000000    # 512.0f

    or-int/2addr v1, v2

    :goto_327
    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setColor(I)V

    .line 1409
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    .line 1410
    invoke-direct/range {p0 .. p0}, Lbr/com/itau/widgets/material/MaterialEditText;->isRTL()Z

    move-result v0

    if-eqz v0, :cond_34b

    .line 1411
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/material/MaterialEditText;->textLayout:Landroid/text/StaticLayout;

    invoke-virtual {v0}, Landroid/text/StaticLayout;->getWidth()I

    move-result v0

    sub-int v0, v7, v0

    int-to-float v0, v0

    move-object/from16 v1, p0

    iget v1, v1, Lbr/com/itau/widgets/material/MaterialEditText;->bottomSpacing:I

    add-int/2addr v1, v8

    int-to-float v1, v1

    sub-float/2addr v1, v11

    move-object/from16 v2, p1

    invoke-virtual {v2, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    goto :goto_35d

    .line 1413
    :cond_34b
    invoke-direct/range {p0 .. p0}, Lbr/com/itau/widgets/material/MaterialEditText;->getBottomTextLeftOffset()I

    move-result v0

    add-int/2addr v0, v6

    int-to-float v0, v0

    move-object/from16 v1, p0

    iget v1, v1, Lbr/com/itau/widgets/material/MaterialEditText;->bottomSpacing:I

    add-int/2addr v1, v8

    int-to-float v1, v1

    sub-float/2addr v1, v11

    move-object/from16 v2, p1

    invoke-virtual {v2, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 1415
    :goto_35d
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/material/MaterialEditText;->textLayout:Landroid/text/StaticLayout;

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    .line 1416
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->restore()V

    .line 1421
    :cond_369
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lbr/com/itau/widgets/material/MaterialEditText;->floatingLabelEnabled:Z

    if-eqz v0, :cond_49a

    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/material/MaterialEditText;->floatingLabelText:Ljava/lang/CharSequence;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_49a

    .line 1422
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/material/MaterialEditText;->textPaint:Landroid/text/TextPaint;

    move-object/from16 v1, p0

    iget v1, v1, Lbr/com/itau/widgets/material/MaterialEditText;->floatingLabelTextSize:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 1424
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lbr/com/itau/widgets/material/MaterialEditText;->highlightFloatingLabel:Z

    if-eqz v0, :cond_397

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lbr/com/itau/widgets/material/MaterialEditText;->alwaysHighlighted:Z

    if-eqz v0, :cond_397

    .line 1425
    const/high16 v0, 0x3f800000    # 1.0f

    move-object/from16 v1, p0

    iput v0, v1, Lbr/com/itau/widgets/material/MaterialEditText;->focusFraction:F

    .line 1426
    :cond_397
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/material/MaterialEditText;->textPaint:Landroid/text/TextPaint;

    move-object/from16 v1, p0

    iget-object v1, v1, Lbr/com/itau/widgets/material/MaterialEditText;->focusEvaluator:Lcom/nineoldandroids/animation/ArgbEvaluator;

    move-object/from16 v2, p0

    iget v2, v2, Lbr/com/itau/widgets/material/MaterialEditText;->focusFraction:F

    move-object/from16 v3, p0

    iget v3, v3, Lbr/com/itau/widgets/material/MaterialEditText;->floatingLabelTextColor:I

    const/4 v4, -0x1

    if-eq v3, v4, :cond_3af

    move-object/from16 v3, p0

    iget v3, v3, Lbr/com/itau/widgets/material/MaterialEditText;->floatingLabelTextColor:I

    goto :goto_3ba

    :cond_3af
    move-object/from16 v3, p0

    iget v3, v3, Lbr/com/itau/widgets/material/MaterialEditText;->baseColor:I

    const v4, 0xffffff

    and-int/2addr v3, v4

    const/high16 v4, 0x44000000    # 512.0f

    or-int/2addr v3, v4

    :goto_3ba
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    move-object/from16 v4, p0

    iget v4, v4, Lbr/com/itau/widgets/material/MaterialEditText;->primaryColor:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v2, v3, v4}, Lcom/nineoldandroids/animation/ArgbEvaluator;->evaluate(FLjava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setColor(I)V

    .line 1429
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/material/MaterialEditText;->textPaint:Landroid/text/TextPaint;

    move-object/from16 v1, p0

    iget-object v1, v1, Lbr/com/itau/widgets/material/MaterialEditText;->floatingLabelText:Ljava/lang/CharSequence;

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v12

    .line 1431
    .local v12, "floatingLabelWidth":F
    invoke-virtual/range {p0 .. p0}, Lbr/com/itau/widgets/material/MaterialEditText;->getGravity()I

    move-result v0

    and-int/lit8 v0, v0, 0x5

    const/4 v1, 0x5

    if-eq v0, v1, :cond_3f2

    invoke-direct/range {p0 .. p0}, Lbr/com/itau/widgets/material/MaterialEditText;->isRTL()Z

    move-result v0

    if-eqz v0, :cond_3f6

    .line 1432
    :cond_3f2
    int-to-float v0, v7

    sub-float/2addr v0, v12

    float-to-int v13, v0

    .local v13, "floatingLabelStartX":I
    goto :goto_41d

    .line 1433
    .end local v13    # "floatingLabelStartX":I
    :cond_3f6
    invoke-virtual/range {p0 .. p0}, Lbr/com/itau/widgets/material/MaterialEditText;->getGravity()I

    move-result v0

    and-int/lit8 v0, v0, 0x3

    const/4 v1, 0x3

    if-ne v0, v1, :cond_401

    .line 1434
    move v13, v6

    .local v13, "floatingLabelStartX":I
    goto :goto_41d

    .line 1436
    .end local v13    # "floatingLabelStartX":I
    :cond_401
    invoke-virtual/range {p0 .. p0}, Lbr/com/itau/widgets/material/MaterialEditText;->getInnerPaddingLeft()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual/range {p0 .. p0}, Lbr/com/itau/widgets/material/MaterialEditText;->getWidth()I

    move-result v1

    invoke-virtual/range {p0 .. p0}, Lbr/com/itau/widgets/material/MaterialEditText;->getInnerPaddingLeft()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual/range {p0 .. p0}, Lbr/com/itau/widgets/material/MaterialEditText;->getInnerPaddingRight()I

    move-result v2

    sub-int/2addr v1, v2

    int-to-float v1, v1

    sub-float/2addr v1, v12

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    add-float/2addr v0, v1

    float-to-int v0, v0

    add-int v13, v6, v0

    .line 1440
    .local v13, "floatingLabelStartX":I
    :goto_41d
    move-object/from16 v0, p0

    iget v14, v0, Lbr/com/itau/widgets/material/MaterialEditText;->floatingLabelPadding:I

    .line 1441
    .local v14, "distance":I
    move-object/from16 v0, p0

    iget v0, v0, Lbr/com/itau/widgets/material/MaterialEditText;->innerPaddingTop:I

    move-object/from16 v1, p0

    iget v1, v1, Lbr/com/itau/widgets/material/MaterialEditText;->floatingLabelTextSize:I

    add-int/2addr v0, v1

    move-object/from16 v1, p0

    iget v1, v1, Lbr/com/itau/widgets/material/MaterialEditText;->floatingLabelPadding:I

    add-int/2addr v0, v1

    int-to-float v0, v0

    int-to-float v1, v14

    move-object/from16 v2, p0

    iget-boolean v2, v2, Lbr/com/itau/widgets/material/MaterialEditText;->floatingLabelAlwaysShown:Z

    if-eqz v2, :cond_43a

    const/high16 v2, 0x3f800000    # 1.0f

    goto :goto_43e

    :cond_43a
    move-object/from16 v2, p0

    iget v2, v2, Lbr/com/itau/widgets/material/MaterialEditText;->floatingLabelFraction:F

    :goto_43e
    mul-float/2addr v1, v2

    sub-float/2addr v0, v1

    invoke-virtual/range {p0 .. p0}, Lbr/com/itau/widgets/material/MaterialEditText;->getScrollY()I

    move-result v1

    int-to-float v1, v1

    add-float/2addr v0, v1

    float-to-int v15, v0

    .line 1444
    .local v15, "floatingLabelStartY":I
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lbr/com/itau/widgets/material/MaterialEditText;->floatingLabelAlwaysShown:Z

    if-eqz v0, :cond_450

    const/high16 v0, 0x3f800000    # 1.0f

    goto :goto_454

    :cond_450
    move-object/from16 v0, p0

    iget v0, v0, Lbr/com/itau/widgets/material/MaterialEditText;->floatingLabelFraction:F

    :goto_454
    const/high16 v1, 0x437f0000    # 255.0f

    mul-float/2addr v0, v1

    move-object/from16 v1, p0

    iget v1, v1, Lbr/com/itau/widgets/material/MaterialEditText;->focusFraction:F

    const v2, 0x3f3d70a4    # 0.74f

    mul-float/2addr v1, v2

    const v2, 0x3e851eb8    # 0.26f

    add-float/2addr v1, v2

    mul-float/2addr v0, v1

    move-object/from16 v1, p0

    iget v1, v1, Lbr/com/itau/widgets/material/MaterialEditText;->floatingLabelTextColor:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_46e

    const/high16 v1, 0x3f800000    # 1.0f

    goto :goto_47a

    :cond_46e
    move-object/from16 v1, p0

    iget v1, v1, Lbr/com/itau/widgets/material/MaterialEditText;->floatingLabelTextColor:I

    invoke-static {v1}, Landroid/graphics/Color;->alpha(I)I

    move-result v1

    int-to-float v1, v1

    const/high16 v2, 0x43800000    # 256.0f

    div-float/2addr v1, v2

    :goto_47a
    mul-float/2addr v0, v1

    float-to-int v1, v0

    move/from16 v16, v1

    .line 1445
    .local v16, "alpha":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/material/MaterialEditText;->textPaint:Landroid/text/TextPaint;

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setAlpha(I)V

    .line 1448
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/material/MaterialEditText;->floatingLabelText:Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    int-to-float v1, v13

    int-to-float v2, v15

    move-object/from16 v3, p0

    iget-object v3, v3, Lbr/com/itau/widgets/material/MaterialEditText;->textPaint:Landroid/text/TextPaint;

    move-object/from16 v4, p1

    invoke-virtual {v4, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 1452
    .end local v12    # "floatingLabelWidth":F
    .end local v13    # "floatingLabelStartX":I
    .end local v14    # "distance":I
    .end local v15    # "floatingLabelStartY":I
    .end local v16    # "alpha":I
    :cond_49a
    invoke-virtual/range {p0 .. p0}, Lbr/com/itau/widgets/material/MaterialEditText;->hasFocus()Z

    move-result v0

    if-eqz v0, :cond_541

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lbr/com/itau/widgets/material/MaterialEditText;->singleLineEllipsis:Z

    if-eqz v0, :cond_541

    invoke-virtual/range {p0 .. p0}, Lbr/com/itau/widgets/material/MaterialEditText;->getScrollX()I

    move-result v0

    if-eqz v0, :cond_541

    .line 1453
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/material/MaterialEditText;->paint:Landroid/graphics/Paint;

    invoke-direct/range {p0 .. p0}, Lbr/com/itau/widgets/material/MaterialEditText;->isInternalValid()Z

    move-result v1

    if-eqz v1, :cond_4bb

    move-object/from16 v1, p0

    iget v1, v1, Lbr/com/itau/widgets/material/MaterialEditText;->primaryColor:I

    goto :goto_4bf

    :cond_4bb
    move-object/from16 v1, p0

    iget v1, v1, Lbr/com/itau/widgets/material/MaterialEditText;->errorColor:I

    :goto_4bf
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 1454
    move-object/from16 v0, p0

    iget v0, v0, Lbr/com/itau/widgets/material/MaterialEditText;->bottomSpacing:I

    add-int/2addr v0, v8

    int-to-float v12, v0

    .line 1456
    .local v12, "startY":F
    invoke-direct/range {p0 .. p0}, Lbr/com/itau/widgets/material/MaterialEditText;->isRTL()Z

    move-result v0

    if-eqz v0, :cond_4d0

    .line 1457
    move v13, v7

    .local v13, "ellipsisStartX":I
    goto :goto_4d1

    .line 1459
    .end local v13    # "ellipsisStartX":I
    :cond_4d0
    move v13, v6

    .line 1461
    .local v13, "ellipsisStartX":I
    :goto_4d1
    invoke-direct/range {p0 .. p0}, Lbr/com/itau/widgets/material/MaterialEditText;->isRTL()Z

    move-result v0

    if-eqz v0, :cond_4d9

    const/4 v14, -0x1

    goto :goto_4da

    :cond_4d9
    const/4 v14, 0x1

    .line 1462
    .local v14, "signum":I
    :goto_4da
    move-object/from16 v0, p0

    iget v0, v0, Lbr/com/itau/widgets/material/MaterialEditText;->bottomEllipsisSize:I

    mul-int/2addr v0, v14

    div-int/lit8 v0, v0, 0x2

    add-int/2addr v0, v13

    int-to-float v0, v0

    move-object/from16 v1, p0

    iget v1, v1, Lbr/com/itau/widgets/material/MaterialEditText;->bottomEllipsisSize:I

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    add-float/2addr v1, v12

    move-object/from16 v2, p0

    iget v2, v2, Lbr/com/itau/widgets/material/MaterialEditText;->bottomEllipsisSize:I

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    move-object/from16 v3, p0

    iget-object v3, v3, Lbr/com/itau/widgets/material/MaterialEditText;->paint:Landroid/graphics/Paint;

    move-object/from16 v4, p1

    invoke-virtual {v4, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 1463
    move-object/from16 v0, p0

    iget v0, v0, Lbr/com/itau/widgets/material/MaterialEditText;->bottomEllipsisSize:I

    mul-int/2addr v0, v14

    mul-int/lit8 v0, v0, 0x5

    div-int/lit8 v0, v0, 0x2

    add-int/2addr v0, v13

    int-to-float v0, v0

    move-object/from16 v1, p0

    iget v1, v1, Lbr/com/itau/widgets/material/MaterialEditText;->bottomEllipsisSize:I

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    add-float/2addr v1, v12

    move-object/from16 v2, p0

    iget v2, v2, Lbr/com/itau/widgets/material/MaterialEditText;->bottomEllipsisSize:I

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    move-object/from16 v3, p0

    iget-object v3, v3, Lbr/com/itau/widgets/material/MaterialEditText;->paint:Landroid/graphics/Paint;

    move-object/from16 v4, p1

    invoke-virtual {v4, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 1464
    move-object/from16 v0, p0

    iget v0, v0, Lbr/com/itau/widgets/material/MaterialEditText;->bottomEllipsisSize:I

    mul-int/2addr v0, v14

    mul-int/lit8 v0, v0, 0x9

    div-int/lit8 v0, v0, 0x2

    add-int/2addr v0, v13

    int-to-float v0, v0

    move-object/from16 v1, p0

    iget v1, v1, Lbr/com/itau/widgets/material/MaterialEditText;->bottomEllipsisSize:I

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    add-float/2addr v1, v12

    move-object/from16 v2, p0

    iget v2, v2, Lbr/com/itau/widgets/material/MaterialEditText;->bottomEllipsisSize:I

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    move-object/from16 v3, p0

    iget-object v3, v3, Lbr/com/itau/widgets/material/MaterialEditText;->paint:Landroid/graphics/Paint;

    move-object/from16 v4, p1

    invoke-virtual {v4, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 1468
    .end local v12    # "startY":F
    .end local v13    # "ellipsisStartX":I
    .end local v14    # "signum":I
    :cond_541
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-super {v0, v1}, Landroid/support/v7/widget/AppCompatEditText;->onDraw(Landroid/graphics/Canvas;)V

    .line 1469
    return-void
.end method

.method public onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .registers 4
    .param p1, "info"    # Landroid/view/accessibility/AccessibilityNodeInfo;

    .line 497
    invoke-super {p0, p1}, Landroid/support/v7/widget/AppCompatEditText;->onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    .line 498
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_1c

    .line 500
    iget-boolean v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->hasContentDescription:Z

    if-eqz v0, :cond_15

    .line 501
    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialEditText;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1c

    .line 503
    :cond_15
    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialEditText;->getAccessibility()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setText(Ljava/lang/CharSequence;)V

    .line 506
    :cond_1c
    :goto_1c
    return-void
.end method

.method protected onLayout(ZIIII)V
    .registers 7
    .param p1, "changed"    # Z
    .param p2, "left"    # I
    .param p3, "top"    # I
    .param p4, "right"    # I
    .param p5, "bottom"    # I

    .line 861
    invoke-super/range {p0 .. p5}, Landroid/support/v7/widget/AppCompatEditText;->onLayout(ZIIII)V

    .line 862
    if-eqz p1, :cond_8

    .line 863
    invoke-direct {p0}, Lbr/com/itau/widgets/material/MaterialEditText;->adjustBottomLines()Z

    .line 865
    :cond_8
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 5
    .param p1, "event"    # Landroid/view/MotionEvent;

    .line 1528
    iget-boolean v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->singleLineEllipsis:Z

    if-eqz v0, :cond_48

    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialEditText;->getScrollX()I

    move-result v0

    if-lez v0, :cond_48

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_48

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    const/16 v1, 0x14

    invoke-direct {p0, v1}, Lbr/com/itau/widgets/material/MaterialEditText;->getPixel(I)I

    move-result v1

    int-to-float v1, v1

    cmpg-float v0, v0, v1

    if-gez v0, :cond_48

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialEditText;->getHeight()I

    move-result v1

    iget v2, p0, Lbr/com/itau/widgets/material/MaterialEditText;->extraPaddingBottom:I

    sub-int/2addr v1, v2

    iget v2, p0, Lbr/com/itau/widgets/material/MaterialEditText;->innerPaddingBottom:I

    sub-int/2addr v1, v2

    int-to-float v1, v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_48

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialEditText;->getHeight()I

    move-result v1

    iget v2, p0, Lbr/com/itau/widgets/material/MaterialEditText;->innerPaddingBottom:I

    sub-int/2addr v1, v2

    int-to-float v1, v1

    cmpg-float v0, v0, v1

    if-gez v0, :cond_48

    .line 1529
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lbr/com/itau/widgets/material/MaterialEditText;->setSelection(I)V

    .line 1530
    const/4 v0, 0x0

    return v0

    .line 1532
    :cond_48
    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialEditText;->hasFocus()Z

    move-result v0

    if-eqz v0, :cond_a4

    iget-boolean v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->showClearButton:Z

    if-eqz v0, :cond_a4

    .line 1533
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_aa

    goto/16 :goto_a4

    .line 1535
    :pswitch_5b
    invoke-direct {p0, p1}, Lbr/com/itau/widgets/material/MaterialEditText;->insideClearButton(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_69

    .line 1536
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->clearButtonTouched:Z

    .line 1537
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->clearButtonClicking:Z

    .line 1538
    const/4 v0, 0x1

    return v0

    .line 1541
    :cond_69
    :pswitch_69
    iget-boolean v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->clearButtonClicking:Z

    if-eqz v0, :cond_76

    invoke-direct {p0, p1}, Lbr/com/itau/widgets/material/MaterialEditText;->insideClearButton(Landroid/view/MotionEvent;)Z

    move-result v0

    if-nez v0, :cond_76

    .line 1542
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->clearButtonClicking:Z

    .line 1544
    :cond_76
    iget-boolean v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->clearButtonTouched:Z

    if-eqz v0, :cond_a4

    .line 1545
    const/4 v0, 0x1

    return v0

    .line 1549
    :pswitch_7c
    iget-boolean v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->clearButtonClicking:Z

    if-eqz v0, :cond_91

    .line 1550
    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_8e

    .line 1551
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lbr/com/itau/widgets/material/MaterialEditText;->setText(Ljava/lang/CharSequence;)V

    .line 1553
    :cond_8e
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->clearButtonClicking:Z

    .line 1555
    :cond_91
    iget-boolean v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->clearButtonTouched:Z

    if-eqz v0, :cond_9a

    .line 1556
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->clearButtonTouched:Z

    .line 1557
    const/4 v0, 0x1

    return v0

    .line 1559
    :cond_9a
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->clearButtonTouched:Z

    .line 1560
    goto :goto_a4

    .line 1562
    :pswitch_9e
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->clearButtonTouched:Z

    .line 1563
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->clearButtonClicking:Z

    .line 1567
    :cond_a4
    :goto_a4
    invoke-super {p0, p1}, Landroid/support/v7/widget/AppCompatEditText;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0

    nop

    :pswitch_data_aa
    .packed-switch 0x0
        :pswitch_5b
        :pswitch_7c
        :pswitch_69
        :pswitch_9e
    .end packed-switch
.end method

.method public setAccentTypeface(Landroid/graphics/Typeface;)V
    .registers 3
    .param p1, "accentTypeface"    # Landroid/graphics/Typeface;

    .line 709
    iput-object p1, p0, Lbr/com/itau/widgets/material/MaterialEditText;->accentTypeface:Landroid/graphics/Typeface;

    .line 710
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->textPaint:Landroid/text/TextPaint;

    invoke-virtual {v0, p1}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 711
    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialEditText;->postInvalidate()V

    .line 712
    return-void
.end method

.method public setAutoValidate(Z)V
    .registers 3
    .param p1, "autoValidate"    # Z

    .line 1129
    iput-boolean p1, p0, Lbr/com/itau/widgets/material/MaterialEditText;->autoValidate:Z

    .line 1130
    if-eqz p1, :cond_7

    .line 1131
    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialEditText;->validate()Z

    .line 1133
    :cond_7
    return-void
.end method

.method public setBaseColor(I)V
    .registers 3
    .param p1, "color"    # I

    .line 978
    iget v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->baseColor:I

    if-eq v0, p1, :cond_6

    .line 979
    iput p1, p0, Lbr/com/itau/widgets/material/MaterialEditText;->baseColor:I

    .line 982
    :cond_6
    invoke-direct {p0}, Lbr/com/itau/widgets/material/MaterialEditText;->initText()V

    .line 984
    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialEditText;->postInvalidate()V

    .line 985
    return-void
.end method

.method public setBottomTextSize(I)V
    .registers 2
    .param p1, "size"    # I

    .line 787
    iput p1, p0, Lbr/com/itau/widgets/material/MaterialEditText;->bottomTextSize:I

    .line 788
    invoke-direct {p0}, Lbr/com/itau/widgets/material/MaterialEditText;->initPadding()V

    .line 789
    return-void
.end method

.method public setCurrentBottomLines(F)V
    .registers 2
    .param p1, "currentBottomLines"    # F

    .line 678
    iput p1, p0, Lbr/com/itau/widgets/material/MaterialEditText;->currentBottomLines:F

    .line 679
    invoke-direct {p0}, Lbr/com/itau/widgets/material/MaterialEditText;->initPadding()V

    .line 680
    return-void
.end method

.method public setError(Ljava/lang/CharSequence;)V
    .registers 3
    .param p1, "errorText"    # Ljava/lang/CharSequence;

    .line 1171
    if-nez p1, :cond_4

    const/4 v0, 0x0

    goto :goto_8

    :cond_4
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_8
    iput-object v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->tempErrorText:Ljava/lang/String;

    .line 1172
    invoke-direct {p0}, Lbr/com/itau/widgets/material/MaterialEditText;->adjustBottomLines()Z

    move-result v0

    if-eqz v0, :cond_13

    .line 1173
    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialEditText;->postInvalidate()V

    .line 1175
    :cond_13
    return-void
.end method

.method public setErrorColor(I)V
    .registers 2
    .param p1, "color"    # I

    .line 1140
    iput p1, p0, Lbr/com/itau/widgets/material/MaterialEditText;->errorColor:I

    .line 1141
    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialEditText;->postInvalidate()V

    .line 1142
    return-void
.end method

.method public setFloatingLabel(I)V
    .registers 2
    .param p1, "mode"    # I
        .annotation build Lbr/com/itau/widgets/material/MaterialEditText$FloatingLabelType;
        .end annotation
    .end param

    .line 1059
    invoke-direct {p0, p1}, Lbr/com/itau/widgets/material/MaterialEditText;->setFloatingLabelInternal(I)V

    .line 1060
    invoke-direct {p0}, Lbr/com/itau/widgets/material/MaterialEditText;->initPadding()V

    .line 1061
    return-void
.end method

.method public setFloatingLabelAlwaysShown(Z)V
    .registers 2
    .param p1, "floatingLabelAlwaysShown"    # Z

    .line 687
    iput-boolean p1, p0, Lbr/com/itau/widgets/material/MaterialEditText;->floatingLabelAlwaysShown:Z

    .line 688
    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialEditText;->invalidate()V

    .line 689
    return-void
.end method

.method public setFloatingLabelAnimating(Z)V
    .registers 2
    .param p1, "animating"    # Z

    .line 1077
    iput-boolean p1, p0, Lbr/com/itau/widgets/material/MaterialEditText;->floatingLabelAnimating:Z

    .line 1078
    return-void
.end method

.method public setFloatingLabelFraction(F)V
    .registers 2
    .param p1, "floatingLabelFraction"    # F

    .line 660
    iput p1, p0, Lbr/com/itau/widgets/material/MaterialEditText;->floatingLabelFraction:F

    .line 661
    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialEditText;->invalidate()V

    .line 662
    return-void
.end method

.method public setFloatingLabelPadding(I)V
    .registers 2
    .param p1, "padding"    # I

    .line 1068
    iput p1, p0, Lbr/com/itau/widgets/material/MaterialEditText;->floatingLabelPadding:I

    .line 1069
    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialEditText;->postInvalidate()V

    .line 1070
    return-void
.end method

.method public setFloatingLabelText(Ljava/lang/CharSequence;)V
    .registers 3
    .param p1, "floatingLabelText"    # Ljava/lang/CharSequence;

    .line 760
    if-nez p1, :cond_7

    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialEditText;->getHint()Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_8

    :cond_7
    move-object v0, p1

    :goto_8
    iput-object v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->floatingLabelText:Ljava/lang/CharSequence;

    .line 761
    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialEditText;->postInvalidate()V

    .line 762
    return-void
.end method

.method public setFloatingLabelTextColor(I)V
    .registers 2
    .param p1, "color"    # I

    .line 778
    iput p1, p0, Lbr/com/itau/widgets/material/MaterialEditText;->floatingLabelTextColor:I

    .line 779
    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialEditText;->postInvalidate()V

    .line 780
    return-void
.end method

.method public setFloatingLabelTextSize(I)V
    .registers 2
    .param p1, "size"    # I

    .line 769
    iput p1, p0, Lbr/com/itau/widgets/material/MaterialEditText;->floatingLabelTextSize:I

    .line 770
    invoke-direct {p0}, Lbr/com/itau/widgets/material/MaterialEditText;->initPadding()V

    .line 771
    return-void
.end method

.method public setFocusFraction(F)V
    .registers 2
    .param p1, "focusFraction"    # F

    .line 669
    iput p1, p0, Lbr/com/itau/widgets/material/MaterialEditText;->focusFraction:F

    .line 670
    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialEditText;->invalidate()V

    .line 671
    return-void
.end method

.method public setHelperText(Ljava/lang/CharSequence;)V
    .registers 3
    .param p1, "helperText"    # Ljava/lang/CharSequence;

    .line 1149
    if-nez p1, :cond_4

    const/4 v0, 0x0

    goto :goto_8

    :cond_4
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_8
    iput-object v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->helperText:Ljava/lang/String;

    .line 1150
    invoke-direct {p0}, Lbr/com/itau/widgets/material/MaterialEditText;->adjustBottomLines()Z

    move-result v0

    if-eqz v0, :cond_13

    .line 1151
    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialEditText;->postInvalidate()V

    .line 1153
    :cond_13
    return-void
.end method

.method public setHelperTextAlwaysShown(Z)V
    .registers 2
    .param p1, "helperTextAlwaysShown"    # Z

    .line 696
    iput-boolean p1, p0, Lbr/com/itau/widgets/material/MaterialEditText;->helperTextAlwaysShown:Z

    .line 697
    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialEditText;->invalidate()V

    .line 698
    return-void
.end method

.method public setHelperTextColor(I)V
    .registers 2
    .param p1, "color"    # I

    .line 1160
    iput p1, p0, Lbr/com/itau/widgets/material/MaterialEditText;->helperTextColor:I

    .line 1161
    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialEditText;->postInvalidate()V

    .line 1162
    return-void
.end method

.method public setHideUnderline(Z)V
    .registers 2
    .param p1, "hideUnderline"    # Z

    .line 726
    iput-boolean p1, p0, Lbr/com/itau/widgets/material/MaterialEditText;->hideUnderline:Z

    .line 727
    invoke-direct {p0}, Lbr/com/itau/widgets/material/MaterialEditText;->initPadding()V

    .line 728
    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialEditText;->postInvalidate()V

    .line 729
    return-void
.end method

.method public setIconLeft(I)V
    .registers 3
    .param p1, "res"    # I

    .line 551
    invoke-direct {p0, p1}, Lbr/com/itau/widgets/material/MaterialEditText;->generateIconBitmaps(I)[Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->iconLeftBitmaps:[Landroid/graphics/Bitmap;

    .line 552
    invoke-direct {p0}, Lbr/com/itau/widgets/material/MaterialEditText;->initPadding()V

    .line 553
    return-void
.end method

.method public setIconLeft(Landroid/graphics/Bitmap;)V
    .registers 3
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .line 561
    invoke-direct {p0, p1}, Lbr/com/itau/widgets/material/MaterialEditText;->generateIconBitmaps(Landroid/graphics/Bitmap;)[Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->iconLeftBitmaps:[Landroid/graphics/Bitmap;

    .line 562
    invoke-direct {p0}, Lbr/com/itau/widgets/material/MaterialEditText;->initPadding()V

    .line 563
    return-void
.end method

.method public setIconLeft(Landroid/graphics/drawable/Drawable;)V
    .registers 3
    .param p1, "drawable"    # Landroid/graphics/drawable/Drawable;

    .line 556
    invoke-direct {p0, p1}, Lbr/com/itau/widgets/material/MaterialEditText;->generateIconBitmaps(Landroid/graphics/drawable/Drawable;)[Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->iconLeftBitmaps:[Landroid/graphics/Bitmap;

    .line 557
    invoke-direct {p0}, Lbr/com/itau/widgets/material/MaterialEditText;->initPadding()V

    .line 558
    return-void
.end method

.method public setIconRight(I)V
    .registers 3
    .param p1, "res"    # I

    .line 566
    invoke-direct {p0, p1}, Lbr/com/itau/widgets/material/MaterialEditText;->generateIconBitmaps(I)[Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->iconRightBitmaps:[Landroid/graphics/Bitmap;

    .line 567
    invoke-direct {p0}, Lbr/com/itau/widgets/material/MaterialEditText;->initPadding()V

    .line 568
    return-void
.end method

.method public setIconRight(Landroid/graphics/Bitmap;)V
    .registers 3
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .line 576
    invoke-direct {p0, p1}, Lbr/com/itau/widgets/material/MaterialEditText;->generateIconBitmaps(Landroid/graphics/Bitmap;)[Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->iconRightBitmaps:[Landroid/graphics/Bitmap;

    .line 577
    invoke-direct {p0}, Lbr/com/itau/widgets/material/MaterialEditText;->initPadding()V

    .line 578
    return-void
.end method

.method public setIconRight(Landroid/graphics/drawable/Drawable;)V
    .registers 3
    .param p1, "drawable"    # Landroid/graphics/drawable/Drawable;

    .line 571
    invoke-direct {p0, p1}, Lbr/com/itau/widgets/material/MaterialEditText;->generateIconBitmaps(Landroid/graphics/drawable/Drawable;)[Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->iconRightBitmaps:[Landroid/graphics/Bitmap;

    .line 572
    invoke-direct {p0}, Lbr/com/itau/widgets/material/MaterialEditText;->initPadding()V

    .line 573
    return-void
.end method

.method public setLengthChecker(Lbr/com/itau/widgets/material/validation/METLengthChecker;)V
    .registers 2
    .param p1, "lengthChecker"    # Lbr/com/itau/widgets/material/validation/METLengthChecker;

    .line 1295
    iput-object p1, p0, Lbr/com/itau/widgets/material/MaterialEditText;->lengthChecker:Lbr/com/itau/widgets/material/validation/METLengthChecker;

    .line 1296
    return-void
.end method

.method public setMaxCharacters(I)V
    .registers 2
    .param p1, "max"    # I

    .line 1096
    iput p1, p0, Lbr/com/itau/widgets/material/MaterialEditText;->maxCharacters:I

    .line 1097
    invoke-direct {p0}, Lbr/com/itau/widgets/material/MaterialEditText;->initMinBottomLines()V

    .line 1098
    invoke-direct {p0}, Lbr/com/itau/widgets/material/MaterialEditText;->initPadding()V

    .line 1099
    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialEditText;->postInvalidate()V

    .line 1100
    return-void
.end method

.method public setMetHintTextColor(I)V
    .registers 3
    .param p1, "color"    # I

    .line 1021
    invoke-static {p1}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->textColorHintStateList:Landroid/content/res/ColorStateList;

    .line 1022
    invoke-direct {p0}, Lbr/com/itau/widgets/material/MaterialEditText;->resetHintTextColor()V

    .line 1023
    return-void
.end method

.method public setMetHintTextColor(Landroid/content/res/ColorStateList;)V
    .registers 2
    .param p1, "colors"    # Landroid/content/res/ColorStateList;

    .line 1029
    iput-object p1, p0, Lbr/com/itau/widgets/material/MaterialEditText;->textColorHintStateList:Landroid/content/res/ColorStateList;

    .line 1030
    invoke-direct {p0}, Lbr/com/itau/widgets/material/MaterialEditText;->resetHintTextColor()V

    .line 1031
    return-void
.end method

.method public setMetTextColor(I)V
    .registers 3
    .param p1, "color"    # I

    .line 996
    invoke-static {p1}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->textColorStateList:Landroid/content/res/ColorStateList;

    .line 997
    invoke-direct {p0}, Lbr/com/itau/widgets/material/MaterialEditText;->resetTextColor()V

    .line 998
    return-void
.end method

.method public setMetTextColor(Landroid/content/res/ColorStateList;)V
    .registers 2
    .param p1, "colors"    # Landroid/content/res/ColorStateList;

    .line 1004
    iput-object p1, p0, Lbr/com/itau/widgets/material/MaterialEditText;->textColorStateList:Landroid/content/res/ColorStateList;

    .line 1005
    invoke-direct {p0}, Lbr/com/itau/widgets/material/MaterialEditText;->resetTextColor()V

    .line 1006
    return-void
.end method

.method public setMinBottomTextLines(I)V
    .registers 2
    .param p1, "lines"    # I

    .line 1118
    iput p1, p0, Lbr/com/itau/widgets/material/MaterialEditText;->minBottomTextLines:I

    .line 1119
    invoke-direct {p0}, Lbr/com/itau/widgets/material/MaterialEditText;->initMinBottomLines()V

    .line 1120
    invoke-direct {p0}, Lbr/com/itau/widgets/material/MaterialEditText;->initPadding()V

    .line 1121
    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialEditText;->postInvalidate()V

    .line 1122
    return-void
.end method

.method public setMinCharacters(I)V
    .registers 2
    .param p1, "min"    # I

    .line 1107
    iput p1, p0, Lbr/com/itau/widgets/material/MaterialEditText;->minCharacters:I

    .line 1108
    invoke-direct {p0}, Lbr/com/itau/widgets/material/MaterialEditText;->initMinBottomLines()V

    .line 1109
    invoke-direct {p0}, Lbr/com/itau/widgets/material/MaterialEditText;->initPadding()V

    .line 1110
    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialEditText;->postInvalidate()V

    .line 1111
    return-void
.end method

.method public setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V
    .registers 3
    .param p1, "listener"    # Landroid/view/View$OnFocusChangeListener;

    .line 1300
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->innerFocusChangeListener:Landroid/view/View$OnFocusChangeListener;

    if-nez v0, :cond_8

    .line 1301
    invoke-super {p0, p1}, Landroid/support/v7/widget/AppCompatEditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    goto :goto_a

    .line 1303
    :cond_8
    iput-object p1, p0, Lbr/com/itau/widgets/material/MaterialEditText;->outerFocusChangeListener:Landroid/view/View$OnFocusChangeListener;

    .line 1305
    :goto_a
    return-void
.end method

.method public final setPadding(IIII)V
    .registers 5
    .param p1, "left"    # I
    .param p2, "top"    # I
    .param p3, "right"    # I
    .param p4, "bottom"    # I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 819
    invoke-super {p0, p1, p2, p3, p4}, Landroid/support/v7/widget/AppCompatEditText;->setPadding(IIII)V

    .line 820
    return-void
.end method

.method public setPaddings(IIII)V
    .registers 5
    .param p1, "left"    # I
    .param p2, "top"    # I
    .param p3, "right"    # I
    .param p4, "bottom"    # I

    .line 826
    iput p2, p0, Lbr/com/itau/widgets/material/MaterialEditText;->innerPaddingTop:I

    .line 827
    iput p4, p0, Lbr/com/itau/widgets/material/MaterialEditText;->innerPaddingBottom:I

    .line 828
    iput p1, p0, Lbr/com/itau/widgets/material/MaterialEditText;->innerPaddingLeft:I

    .line 829
    iput p3, p0, Lbr/com/itau/widgets/material/MaterialEditText;->innerPaddingRight:I

    .line 830
    invoke-direct {p0}, Lbr/com/itau/widgets/material/MaterialEditText;->correctPaddings()V

    .line 831
    return-void
.end method

.method public setPrimaryColor(I)V
    .registers 2
    .param p1, "color"    # I

    .line 988
    iput p1, p0, Lbr/com/itau/widgets/material/MaterialEditText;->primaryColor:I

    .line 989
    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialEditText;->postInvalidate()V

    .line 990
    return-void
.end method

.method public setShowClearButton(Z)V
    .registers 2
    .param p1, "show"    # Z

    .line 585
    iput-boolean p1, p0, Lbr/com/itau/widgets/material/MaterialEditText;->showClearButton:Z

    .line 586
    invoke-direct {p0}, Lbr/com/itau/widgets/material/MaterialEditText;->correctPaddings()V

    .line 587
    return-void
.end method

.method public setSingleLineEllipsis()V
    .registers 2

    .line 1081
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lbr/com/itau/widgets/material/MaterialEditText;->setSingleLineEllipsis(Z)V

    .line 1082
    return-void
.end method

.method public setSingleLineEllipsis(Z)V
    .registers 2
    .param p1, "enabled"    # Z

    .line 1085
    iput-boolean p1, p0, Lbr/com/itau/widgets/material/MaterialEditText;->singleLineEllipsis:Z

    .line 1086
    invoke-direct {p0}, Lbr/com/itau/widgets/material/MaterialEditText;->initMinBottomLines()V

    .line 1087
    invoke-direct {p0}, Lbr/com/itau/widgets/material/MaterialEditText;->initPadding()V

    .line 1088
    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialEditText;->postInvalidate()V

    .line 1089
    return-void
.end method

.method public setUnderlineColor(I)V
    .registers 2
    .param p1, "color"    # I

    .line 744
    iput p1, p0, Lbr/com/itau/widgets/material/MaterialEditText;->underlineColor:I

    .line 745
    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialEditText;->postInvalidate()V

    .line 746
    return-void
.end method

.method public setValidateOnFocusLost(Z)V
    .registers 2
    .param p1, "validate"    # Z

    .line 974
    iput-boolean p1, p0, Lbr/com/itau/widgets/material/MaterialEditText;->validateOnFocusLost:Z

    .line 975
    return-void
.end method

.method public validate()Z
    .registers 7

    .line 1239
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->validators:Ljava/util/List;

    if-eqz v0, :cond_c

    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->validators:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_e

    .line 1240
    :cond_c
    const/4 v0, 0x1

    return v0

    .line 1243
    :cond_e
    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    .line 1244
    .local v1, "text":Ljava/lang/CharSequence;
    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-nez v0, :cond_1a

    const/4 v2, 0x1

    goto :goto_1b

    :cond_1a
    const/4 v2, 0x0

    .line 1246
    .local v2, "isEmpty":Z
    :goto_1b
    const/4 v3, 0x1

    .line 1247
    .local v3, "isValid":Z
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialEditText;->validators:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :goto_22
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_45

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lbr/com/itau/widgets/material/validation/METValidator;

    .line 1249
    .local v5, "validator":Lbr/com/itau/widgets/material/validation/METValidator;
    if-eqz v3, :cond_39

    invoke-virtual {v5, v1, v2}, Lbr/com/itau/widgets/material/validation/METValidator;->isValid(Ljava/lang/CharSequence;Z)Z

    move-result v0

    if-eqz v0, :cond_39

    const/4 v3, 0x1

    goto :goto_3a

    :cond_39
    const/4 v3, 0x0

    .line 1250
    :goto_3a
    if-nez v3, :cond_44

    .line 1251
    invoke-virtual {v5}, Lbr/com/itau/widgets/material/validation/METValidator;->getErrorMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lbr/com/itau/widgets/material/MaterialEditText;->setError(Ljava/lang/CharSequence;)V

    .line 1252
    goto :goto_45

    .line 1254
    .end local v5    # "validator":Lbr/com/itau/widgets/material/validation/METValidator;
    :cond_44
    goto :goto_22

    .line 1255
    .end local v4    # "i$":Ljava/util/Iterator;
    :cond_45
    :goto_45
    if-eqz v3, :cond_4b

    .line 1256
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lbr/com/itau/widgets/material/MaterialEditText;->setError(Ljava/lang/CharSequence;)V

    .line 1259
    :cond_4b
    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialEditText;->postInvalidate()V

    .line 1260
    return v3
.end method

.method public validate(Ljava/lang/String;Ljava/lang/CharSequence;)Z
    .registers 4
    .param p1, "regex"    # Ljava/lang/String;
    .param p2, "errorText"    # Ljava/lang/CharSequence;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 1207
    invoke-virtual {p0, p1}, Lbr/com/itau/widgets/material/MaterialEditText;->isValid(Ljava/lang/String;)Z

    move-result v0

    .line 1208
    .local v0, "isValid":Z
    if-nez v0, :cond_9

    .line 1209
    invoke-virtual {p0, p2}, Lbr/com/itau/widgets/material/MaterialEditText;->setError(Ljava/lang/CharSequence;)V

    .line 1211
    :cond_9
    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialEditText;->postInvalidate()V

    .line 1212
    return v0
.end method

.method public validateWith(Lbr/com/itau/widgets/material/validation/METValidator;)Z
    .registers 5
    .param p1, "validator"    # Lbr/com/itau/widgets/material/validation/METValidator;

    .line 1222
    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    .line 1223
    .local v1, "text":Ljava/lang/CharSequence;
    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-nez v0, :cond_c

    const/4 v0, 0x1

    goto :goto_d

    :cond_c
    const/4 v0, 0x0

    :goto_d
    invoke-virtual {p1, v1, v0}, Lbr/com/itau/widgets/material/validation/METValidator;->isValid(Ljava/lang/CharSequence;Z)Z

    move-result v2

    .line 1224
    .local v2, "isValid":Z
    if-nez v2, :cond_1a

    .line 1225
    invoke-virtual {p1}, Lbr/com/itau/widgets/material/validation/METValidator;->getErrorMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lbr/com/itau/widgets/material/MaterialEditText;->setError(Ljava/lang/CharSequence;)V

    .line 1227
    :cond_1a
    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialEditText;->postInvalidate()V

    .line 1228
    return v2
.end method

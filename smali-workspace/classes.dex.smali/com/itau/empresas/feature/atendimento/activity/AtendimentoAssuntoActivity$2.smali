.class Lcom/itau/empresas/feature/atendimento/activity/AtendimentoAssuntoActivity$2;
.super Ljava/lang/Object;
.source "AtendimentoAssuntoActivity.java"

# interfaces
.implements Landroid/widget/ExpandableListView$OnGroupExpandListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/itau/empresas/feature/atendimento/activity/AtendimentoAssuntoActivity;->configuraListaAssuntos()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/itau/empresas/feature/atendimento/activity/AtendimentoAssuntoActivity;


# direct methods
.method constructor <init>(Lcom/itau/empresas/feature/atendimento/activity/AtendimentoAssuntoActivity;)V
    .registers 2
    .param p1, "this$0"    # Lcom/itau/empresas/feature/atendimento/activity/AtendimentoAssuntoActivity;

    .line 97
    iput-object p1, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoAssuntoActivity$2;->this$0:Lcom/itau/empresas/feature/atendimento/activity/AtendimentoAssuntoActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onGroupExpand(I)V
    .registers 4
    .param p1, "groupPosition"    # I

    .line 100
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoAssuntoActivity$2;->this$0:Lcom/itau/empresas/feature/atendimento/activity/AtendimentoAssuntoActivity;

    iget-object v0, v0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoAssuntoActivity;->listaDuvidasPorAssunto:Lcom/itau/empresas/ui/widgets/NonScrollExpandableListView;

    invoke-virtual {v0}, Lcom/itau/empresas/ui/widgets/NonScrollExpandableListView;->getChildCount()I

    move-result v0

    if-ge v1, v0, :cond_1a

    .line 101
    const/4 v0, -0x1

    if-eq p1, v0, :cond_17

    if-eq p1, v1, :cond_17

    .line 102
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoAssuntoActivity$2;->this$0:Lcom/itau/empresas/feature/atendimento/activity/AtendimentoAssuntoActivity;

    iget-object v0, v0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoAssuntoActivity;->listaDuvidasPorAssunto:Lcom/itau/empresas/ui/widgets/NonScrollExpandableListView;

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/widgets/NonScrollExpandableListView;->collapseGroup(I)Z

    .line 100
    :cond_17
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 105
    .end local v1    # "i":I
    :cond_1a
    return-void
.end method

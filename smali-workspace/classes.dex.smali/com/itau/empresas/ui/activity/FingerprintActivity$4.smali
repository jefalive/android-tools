.class Lcom/itau/empresas/ui/activity/FingerprintActivity$4;
.super Ljava/lang/Object;
.source "FingerprintActivity.java"

# interfaces
.implements Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/itau/empresas/ui/activity/FingerprintActivity;->clicouEmContinuar()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/itau/empresas/ui/activity/FingerprintActivity;


# direct methods
.method constructor <init>(Lcom/itau/empresas/ui/activity/FingerprintActivity;)V
    .registers 2
    .param p1, "this$0"    # Lcom/itau/empresas/ui/activity/FingerprintActivity;

    .line 113
    iput-object p1, p0, Lcom/itau/empresas/ui/activity/FingerprintActivity$4;->this$0:Lcom/itau/empresas/ui/activity/FingerprintActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintException;)V
    .registers 4
    .param p1, "exception"    # Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintException;

    .line 124
    invoke-static {}, Lde/greenrobot/event/EventBus;->getDefault()Lde/greenrobot/event/EventBus;

    move-result-object v0

    invoke-virtual {v0, p1}, Lde/greenrobot/event/EventBus;->post(Ljava/lang/Object;)V

    .line 125
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/FingerprintActivity$4;->this$0:Lcom/itau/empresas/ui/activity/FingerprintActivity;

    const/4 v1, 0x0

    # setter for: Lcom/itau/empresas/ui/activity/FingerprintActivity;->fingerprintDialog:Lcom/itau/empresas/ui/dialog/FingerprintDialog;
    invoke-static {v0, v1}, Lcom/itau/empresas/ui/activity/FingerprintActivity;->access$102(Lcom/itau/empresas/ui/activity/FingerprintActivity;Lcom/itau/empresas/ui/dialog/FingerprintDialog;)Lcom/itau/empresas/ui/dialog/FingerprintDialog;

    .line 126
    return-void
.end method

.method public onResult(Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintResult;)V
    .registers 4
    .param p1, "result"    # Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintResult;

    .line 116
    sget-object v0, Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintResult;->AUTHENTICATED:Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintResult;

    if-ne p1, v0, :cond_d

    .line 117
    invoke-static {}, Lbr/com/itau/sdk/android/core/BackendClient;->registerKeyAccess()V

    .line 118
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/FingerprintActivity$4;->this$0:Lcom/itau/empresas/ui/activity/FingerprintActivity;

    const/4 v1, 0x0

    # setter for: Lcom/itau/empresas/ui/activity/FingerprintActivity;->fingerprintDialog:Lcom/itau/empresas/ui/dialog/FingerprintDialog;
    invoke-static {v0, v1}, Lcom/itau/empresas/ui/activity/FingerprintActivity;->access$102(Lcom/itau/empresas/ui/activity/FingerprintActivity;Lcom/itau/empresas/ui/dialog/FingerprintDialog;)Lcom/itau/empresas/ui/dialog/FingerprintDialog;

    .line 120
    :cond_d
    return-void
.end method

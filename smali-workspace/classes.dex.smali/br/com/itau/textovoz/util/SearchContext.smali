.class public final enum Lbr/com/itau/textovoz/util/SearchContext;
.super Ljava/lang/Enum;
.source "SearchContext.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<Lbr/com/itau/textovoz/util/SearchContext;>;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lbr/com/itau/textovoz/util/SearchContext;

.field public static final enum DEFAULT:Lbr/com/itau/textovoz/util/SearchContext;

.field public static final enum FAQ:Lbr/com/itau/textovoz/util/SearchContext;

.field public static final enum MENU:Lbr/com/itau/textovoz/util/SearchContext;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .line 8
    new-instance v0, Lbr/com/itau/textovoz/util/SearchContext;

    const-string v1, "FAQ"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lbr/com/itau/textovoz/util/SearchContext;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbr/com/itau/textovoz/util/SearchContext;->FAQ:Lbr/com/itau/textovoz/util/SearchContext;

    new-instance v0, Lbr/com/itau/textovoz/util/SearchContext;

    const-string v1, "MENU"

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lbr/com/itau/textovoz/util/SearchContext;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbr/com/itau/textovoz/util/SearchContext;->MENU:Lbr/com/itau/textovoz/util/SearchContext;

    new-instance v0, Lbr/com/itau/textovoz/util/SearchContext;

    const-string v1, "DEFAULT"

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2}, Lbr/com/itau/textovoz/util/SearchContext;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbr/com/itau/textovoz/util/SearchContext;->DEFAULT:Lbr/com/itau/textovoz/util/SearchContext;

    .line 7
    const/4 v0, 0x3

    new-array v0, v0, [Lbr/com/itau/textovoz/util/SearchContext;

    sget-object v1, Lbr/com/itau/textovoz/util/SearchContext;->FAQ:Lbr/com/itau/textovoz/util/SearchContext;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sget-object v1, Lbr/com/itau/textovoz/util/SearchContext;->MENU:Lbr/com/itau/textovoz/util/SearchContext;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    sget-object v1, Lbr/com/itau/textovoz/util/SearchContext;->DEFAULT:Lbr/com/itau/textovoz/util/SearchContext;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    sput-object v0, Lbr/com/itau/textovoz/util/SearchContext;->$VALUES:[Lbr/com/itau/textovoz/util/SearchContext;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 7
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lbr/com/itau/textovoz/util/SearchContext;
    .registers 2
    .param p0, "name"    # Ljava/lang/String;

    .line 7
    const-class v0, Lbr/com/itau/textovoz/util/SearchContext;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lbr/com/itau/textovoz/util/SearchContext;

    return-object v0
.end method

.method public static values()[Lbr/com/itau/textovoz/util/SearchContext;
    .registers 1

    .line 7
    sget-object v0, Lbr/com/itau/textovoz/util/SearchContext;->$VALUES:[Lbr/com/itau/textovoz/util/SearchContext;

    invoke-virtual {v0}, [Lbr/com/itau/textovoz/util/SearchContext;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lbr/com/itau/textovoz/util/SearchContext;

    return-object v0
.end method

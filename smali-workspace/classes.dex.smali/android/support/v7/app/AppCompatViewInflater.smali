.class Landroid/support/v7/app/AppCompatViewInflater;
.super Ljava/lang/Object;
.source "AppCompatViewInflater.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/support/v7/app/AppCompatViewInflater$DeclaredOnClickListener;
    }
.end annotation


# static fields
.field private static final sClassPrefixList:[Ljava/lang/String;

.field private static final sConstructorMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<Ljava/lang/String;Ljava/lang/reflect/Constructor<+Landroid/view/View;>;>;"
        }
    .end annotation
.end field

.field private static final sConstructorSignature:[Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[Ljava/lang/Class<*>;"
        }
    .end annotation
.end field

.field private static final sOnClickAttrs:[I


# instance fields
.field private final mConstructorArgs:[Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .line 63
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Class;

    const-class v1, Landroid/content/Context;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const-class v1, Landroid/util/AttributeSet;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    sput-object v0, Landroid/support/v7/app/AppCompatViewInflater;->sConstructorSignature:[Ljava/lang/Class;

    .line 65
    const/4 v0, 0x1

    new-array v0, v0, [I

    fill-array-data v0, :array_34

    sput-object v0, Landroid/support/v7/app/AppCompatViewInflater;->sOnClickAttrs:[I

    .line 67
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "android.widget."

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const-string v1, "android.view."

    const/4 v2, 0x1

    aput-object v1, v0, v2

    const-string v1, "android.webkit."

    const/4 v2, 0x2

    aput-object v1, v0, v2

    sput-object v0, Landroid/support/v7/app/AppCompatViewInflater;->sClassPrefixList:[Ljava/lang/String;

    .line 75
    new-instance v0, Landroid/support/v4/util/ArrayMap;

    invoke-direct {v0}, Landroid/support/v4/util/ArrayMap;-><init>()V

    sput-object v0, Landroid/support/v7/app/AppCompatViewInflater;->sConstructorMap:Ljava/util/Map;

    return-void

    nop

    :array_34
    .array-data 4
        0x101026f
    .end array-data
.end method

.method constructor <init>()V
    .registers 2

    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 78
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    iput-object v0, p0, Landroid/support/v7/app/AppCompatViewInflater;->mConstructorArgs:[Ljava/lang/Object;

    .line 269
    return-void
.end method

.method private checkOnClickListener(Landroid/view/View;Landroid/util/AttributeSet;)V
    .registers 8
    .param p1, "view"    # Landroid/view/View;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .line 194
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 196
    .local v2, "context":Landroid/content/Context;
    instance-of v0, v2, Landroid/content/ContextWrapper;

    if-eqz v0, :cond_14

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xf

    if-lt v0, v1, :cond_15

    .line 197
    invoke-static {p1}, Landroid/support/v4/view/ViewCompat;->hasOnClickListeners(Landroid/view/View;)Z

    move-result v0

    if-nez v0, :cond_15

    .line 201
    :cond_14
    return-void

    .line 204
    :cond_15
    sget-object v0, Landroid/support/v7/app/AppCompatViewInflater;->sOnClickAttrs:[I

    invoke-virtual {v2, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v3

    .line 205
    .local v3, "a":Landroid/content/res/TypedArray;
    const/4 v0, 0x0

    invoke-virtual {v3, v0}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 206
    .local v4, "handlerName":Ljava/lang/String;
    if-eqz v4, :cond_2a

    .line 207
    new-instance v0, Landroid/support/v7/app/AppCompatViewInflater$DeclaredOnClickListener;

    invoke-direct {v0, p1, v4}, Landroid/support/v7/app/AppCompatViewInflater$DeclaredOnClickListener;-><init>(Landroid/view/View;Ljava/lang/String;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 209
    :cond_2a
    invoke-virtual {v3}, Landroid/content/res/TypedArray;->recycle()V

    .line 210
    return-void
.end method

.method private createView(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/view/View;
    .registers 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "prefix"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/ClassNotFoundException;,
            Landroid/view/InflateException;
        }
    .end annotation

    .line 214
    sget-object v0, Landroid/support/v7/app/AppCompatViewInflater;->sConstructorMap:Ljava/util/Map;

    invoke-interface {v0, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Ljava/lang/reflect/Constructor;

    .line 217
    .local v2, "constructor":Ljava/lang/reflect/Constructor;, "Ljava/lang/reflect/Constructor<+Landroid/view/View;>;"
    if-nez v2, :cond_39

    .line 219
    :try_start_b
    invoke-virtual {p1}, Landroid/content/Context;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    if-eqz p3, :cond_23

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_24

    :cond_23
    move-object v1, p2

    :goto_24
    invoke-virtual {v0, v1}, Ljava/lang/ClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    const-class v1, Landroid/view/View;

    .line 220
    invoke-virtual {v0, v1}, Ljava/lang/Class;->asSubclass(Ljava/lang/Class;)Ljava/lang/Class;

    move-result-object v3

    .line 222
    .local v3, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<+Landroid/view/View;>;"
    sget-object v0, Landroid/support/v7/app/AppCompatViewInflater;->sConstructorSignature:[Ljava/lang/Class;

    invoke-virtual {v3, v0}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v2

    .line 223
    sget-object v0, Landroid/support/v7/app/AppCompatViewInflater;->sConstructorMap:Ljava/util/Map;

    invoke-interface {v0, p2, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 225
    .end local v3    # "clazz":Ljava/lang/Class;, "Ljava/lang/Class<+Landroid/view/View;>;"
    .end local v3
    :cond_39
    const/4 v0, 0x1

    invoke-virtual {v2, v0}, Ljava/lang/reflect/Constructor;->setAccessible(Z)V

    .line 226
    iget-object v0, p0, Landroid/support/v7/app/AppCompatViewInflater;->mConstructorArgs:[Ljava/lang/Object;

    invoke-virtual {v2, v0}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;
    :try_end_45
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_45} :catch_46

    return-object v0

    .line 227
    :catch_46
    move-exception v3

    .line 230
    .local v3, "e":Ljava/lang/Exception;
    const/4 v0, 0x0

    return-object v0
.end method

.method private createViewFromTag(Landroid/content/Context;Ljava/lang/String;Landroid/util/AttributeSet;)Landroid/view/View;
    .registers 11
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "attrs"    # Landroid/util/AttributeSet;

    .line 158
    const-string v0, "view"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 159
    const-string v0, "class"

    const/4 v1, 0x0

    invoke-interface {p3, v1, v0}, Landroid/util/AttributeSet;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    .line 163
    :cond_f
    :try_start_f
    iget-object v0, p0, Landroid/support/v7/app/AppCompatViewInflater;->mConstructorArgs:[Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    .line 164
    iget-object v0, p0, Landroid/support/v7/app/AppCompatViewInflater;->mConstructorArgs:[Ljava/lang/Object;

    const/4 v1, 0x1

    aput-object p3, v0, v1

    .line 166
    const/16 v0, 0x2e

    invoke-virtual {p2, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    const/4 v1, -0x1

    if-ne v1, v0, :cond_51

    .line 167
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_23
    sget-object v0, Landroid/support/v7/app/AppCompatViewInflater;->sClassPrefixList:[Ljava/lang/String;

    array-length v0, v0

    if-ge v3, v0, :cond_43

    .line 168
    sget-object v0, Landroid/support/v7/app/AppCompatViewInflater;->sClassPrefixList:[Ljava/lang/String;

    aget-object v0, v0, v3

    invoke-direct {p0, p1, p2, v0}, Landroid/support/v7/app/AppCompatViewInflater;->createView(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/view/View;
    :try_end_2f
    .catch Ljava/lang/Exception; {:try_start_f .. :try_end_2f} :catch_63
    .catchall {:try_start_f .. :try_end_2f} :catchall_72

    move-result-object v4

    .line 169
    .local v4, "view":Landroid/view/View;
    if-eqz v4, :cond_40

    .line 170
    move-object v5, v4

    .line 183
    iget-object v0, p0, Landroid/support/v7/app/AppCompatViewInflater;->mConstructorArgs:[Ljava/lang/Object;

    const/4 v1, 0x0

    const/4 v2, 0x0

    aput-object v1, v0, v2

    .line 184
    iget-object v0, p0, Landroid/support/v7/app/AppCompatViewInflater;->mConstructorArgs:[Ljava/lang/Object;

    const/4 v1, 0x0

    const/4 v2, 0x1

    aput-object v1, v0, v2

    return-object v5

    .line 167
    .end local v4    # "view":Landroid/view/View;
    :cond_40
    add-int/lit8 v3, v3, 0x1

    goto :goto_23

    .line 173
    .end local v3    # "i":I
    :cond_43
    const/4 v3, 0x0

    .line 183
    iget-object v0, p0, Landroid/support/v7/app/AppCompatViewInflater;->mConstructorArgs:[Ljava/lang/Object;

    const/4 v1, 0x0

    const/4 v2, 0x0

    aput-object v1, v0, v2

    .line 184
    iget-object v0, p0, Landroid/support/v7/app/AppCompatViewInflater;->mConstructorArgs:[Ljava/lang/Object;

    const/4 v1, 0x0

    const/4 v2, 0x1

    aput-object v1, v0, v2

    return-object v3

    .line 175
    :cond_51
    const/4 v0, 0x0

    :try_start_52
    invoke-direct {p0, p1, p2, v0}, Landroid/support/v7/app/AppCompatViewInflater;->createView(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/view/View;
    :try_end_55
    .catch Ljava/lang/Exception; {:try_start_52 .. :try_end_55} :catch_63
    .catchall {:try_start_52 .. :try_end_55} :catchall_72

    move-result-object v3

    .line 183
    iget-object v0, p0, Landroid/support/v7/app/AppCompatViewInflater;->mConstructorArgs:[Ljava/lang/Object;

    const/4 v1, 0x0

    const/4 v2, 0x0

    aput-object v1, v0, v2

    .line 184
    iget-object v0, p0, Landroid/support/v7/app/AppCompatViewInflater;->mConstructorArgs:[Ljava/lang/Object;

    const/4 v1, 0x0

    const/4 v2, 0x1

    aput-object v1, v0, v2

    return-object v3

    .line 177
    :catch_63
    move-exception v3

    .line 180
    .local v3, "e":Ljava/lang/Exception;
    const/4 v4, 0x0

    .line 183
    iget-object v0, p0, Landroid/support/v7/app/AppCompatViewInflater;->mConstructorArgs:[Ljava/lang/Object;

    const/4 v1, 0x0

    const/4 v2, 0x0

    aput-object v1, v0, v2

    .line 184
    iget-object v0, p0, Landroid/support/v7/app/AppCompatViewInflater;->mConstructorArgs:[Ljava/lang/Object;

    const/4 v1, 0x0

    const/4 v2, 0x1

    aput-object v1, v0, v2

    return-object v4

    .line 183
    .end local v3    # "e":Ljava/lang/Exception;
    :catchall_72
    move-exception v6

    iget-object v0, p0, Landroid/support/v7/app/AppCompatViewInflater;->mConstructorArgs:[Ljava/lang/Object;

    const/4 v1, 0x0

    const/4 v2, 0x0

    aput-object v1, v0, v2

    .line 184
    iget-object v0, p0, Landroid/support/v7/app/AppCompatViewInflater;->mConstructorArgs:[Ljava/lang/Object;

    const/4 v1, 0x0

    const/4 v2, 0x1

    aput-object v1, v0, v2

    throw v6
.end method

.method private static themifyContext(Landroid/content/Context;Landroid/util/AttributeSet;ZZ)Landroid/content/Context;
    .registers 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "attrs"    # Landroid/util/AttributeSet;
    .param p2, "useAndroidTheme"    # Z
    .param p3, "useAppTheme"    # Z

    .line 239
    sget-object v0, Landroid/support/v7/appcompat/R$styleable;->View:[I

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v3

    .line 240
    .local v3, "a":Landroid/content/res/TypedArray;
    const/4 v4, 0x0

    .line 241
    .local v4, "themeId":I
    if-eqz p2, :cond_12

    .line 243
    sget v0, Landroid/support/v7/appcompat/R$styleable;->View_android_theme:I

    const/4 v1, 0x0

    invoke-virtual {v3, v0, v1}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v4

    .line 245
    :cond_12
    if-eqz p3, :cond_26

    if-nez v4, :cond_26

    .line 247
    sget v0, Landroid/support/v7/appcompat/R$styleable;->View_theme:I

    const/4 v1, 0x0

    invoke-virtual {v3, v0, v1}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v4

    .line 249
    if-eqz v4, :cond_26

    .line 250
    const-string v0, "AppCompatViewInflater"

    const-string v1, "app:theme is now deprecated. Please move to using android:theme instead."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 254
    :cond_26
    invoke-virtual {v3}, Landroid/content/res/TypedArray;->recycle()V

    .line 256
    if-eqz v4, :cond_3e

    instance-of v0, p0, Landroid/support/v7/view/ContextThemeWrapper;

    if-eqz v0, :cond_38

    move-object v0, p0

    check-cast v0, Landroid/support/v7/view/ContextThemeWrapper;

    .line 257
    invoke-virtual {v0}, Landroid/support/v7/view/ContextThemeWrapper;->getThemeResId()I

    move-result v0

    if-eq v0, v4, :cond_3e

    .line 260
    :cond_38
    new-instance v0, Landroid/support/v7/view/ContextThemeWrapper;

    invoke-direct {v0, p0, v4}, Landroid/support/v7/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    move-object p0, v0

    .line 262
    :cond_3e
    return-object p0
.end method


# virtual methods
.method public final createView(Landroid/view/View;Ljava/lang/String;Landroid/content/Context;Landroid/util/AttributeSet;ZZZZ)Landroid/view/View;
    .registers 14
    .param p1, "parent"    # Landroid/view/View;
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "context"    # Landroid/content/Context;
    .param p4, "attrs"    # Landroid/util/AttributeSet;
    .param p5, "inheritContext"    # Z
    .param p6, "readAndroidTheme"    # Z
    .param p7, "readAppTheme"    # Z
    .param p8, "wrapContext"    # Z

    .line 83
    move-object v1, p3

    .line 87
    .local v1, "originalContext":Landroid/content/Context;
    if-eqz p5, :cond_9

    if-eqz p1, :cond_9

    .line 88
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p3

    .line 90
    :cond_9
    if-nez p6, :cond_d

    if-eqz p7, :cond_11

    .line 92
    :cond_d
    invoke-static {p3, p4, p6, p7}, Landroid/support/v7/app/AppCompatViewInflater;->themifyContext(Landroid/content/Context;Landroid/util/AttributeSet;ZZ)Landroid/content/Context;

    move-result-object p3

    .line 94
    :cond_11
    if-eqz p8, :cond_17

    .line 95
    invoke-static {p3}, Landroid/support/v7/widget/TintContextWrapper;->wrap(Landroid/content/Context;)Landroid/content/Context;

    move-result-object p3

    .line 98
    :cond_17
    const/4 v2, 0x0

    .line 101
    .local v2, "view":Landroid/view/View;
    move-object v3, p2

    const/4 v4, -0x1

    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v0

    sparse-switch v0, :sswitch_data_112

    goto/16 :goto_ae

    :sswitch_23
    const-string v0, "TextView"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_ae

    const/4 v4, 0x0

    goto/16 :goto_ae

    :sswitch_2e
    const-string v0, "ImageView"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_ae

    const/4 v4, 0x1

    goto/16 :goto_ae

    :sswitch_39
    const-string v0, "Button"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_ae

    const/4 v4, 0x2

    goto/16 :goto_ae

    :sswitch_44
    const-string v0, "EditText"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_ae

    const/4 v4, 0x3

    goto/16 :goto_ae

    :sswitch_4f
    const-string v0, "Spinner"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_ae

    const/4 v4, 0x4

    goto/16 :goto_ae

    :sswitch_5a
    const-string v0, "ImageButton"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_ae

    const/4 v4, 0x5

    goto :goto_ae

    :sswitch_64
    const-string v0, "CheckBox"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_ae

    const/4 v4, 0x6

    goto :goto_ae

    :sswitch_6e
    const-string v0, "RadioButton"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_ae

    const/4 v4, 0x7

    goto :goto_ae

    :sswitch_78
    const-string v0, "CheckedTextView"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_ae

    const/16 v4, 0x8

    goto :goto_ae

    :sswitch_83
    const-string v0, "AutoCompleteTextView"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_ae

    const/16 v4, 0x9

    goto :goto_ae

    :sswitch_8e
    const-string v0, "MultiAutoCompleteTextView"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_ae

    const/16 v4, 0xa

    goto :goto_ae

    :sswitch_99
    const-string v0, "RatingBar"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_ae

    const/16 v4, 0xb

    goto :goto_ae

    :sswitch_a4
    const-string v0, "SeekBar"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_ae

    const/16 v4, 0xc

    :cond_ae
    :goto_ae
    packed-switch v4, :pswitch_data_148

    goto/16 :goto_104

    .line 103
    :pswitch_b3
    new-instance v2, Landroid/support/v7/widget/AppCompatTextView;

    invoke-direct {v2, p3, p4}, Landroid/support/v7/widget/AppCompatTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 104
    goto/16 :goto_104

    .line 106
    :pswitch_ba
    new-instance v2, Landroid/support/v7/widget/AppCompatImageView;

    invoke-direct {v2, p3, p4}, Landroid/support/v7/widget/AppCompatImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 107
    goto/16 :goto_104

    .line 109
    :pswitch_c1
    new-instance v2, Landroid/support/v7/widget/AppCompatButton;

    invoke-direct {v2, p3, p4}, Landroid/support/v7/widget/AppCompatButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 110
    goto/16 :goto_104

    .line 112
    :pswitch_c8
    new-instance v2, Landroid/support/v7/widget/AppCompatEditText;

    invoke-direct {v2, p3, p4}, Landroid/support/v7/widget/AppCompatEditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 113
    goto/16 :goto_104

    .line 115
    :pswitch_cf
    new-instance v2, Landroid/support/v7/widget/AppCompatSpinner;

    invoke-direct {v2, p3, p4}, Landroid/support/v7/widget/AppCompatSpinner;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 116
    goto :goto_104

    .line 118
    :pswitch_d5
    new-instance v2, Landroid/support/v7/widget/AppCompatImageButton;

    invoke-direct {v2, p3, p4}, Landroid/support/v7/widget/AppCompatImageButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 119
    goto :goto_104

    .line 121
    :pswitch_db
    new-instance v2, Landroid/support/v7/widget/AppCompatCheckBox;

    invoke-direct {v2, p3, p4}, Landroid/support/v7/widget/AppCompatCheckBox;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 122
    goto :goto_104

    .line 124
    :pswitch_e1
    new-instance v2, Landroid/support/v7/widget/AppCompatRadioButton;

    invoke-direct {v2, p3, p4}, Landroid/support/v7/widget/AppCompatRadioButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 125
    goto :goto_104

    .line 127
    :pswitch_e7
    new-instance v2, Landroid/support/v7/widget/AppCompatCheckedTextView;

    invoke-direct {v2, p3, p4}, Landroid/support/v7/widget/AppCompatCheckedTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 128
    goto :goto_104

    .line 130
    :pswitch_ed
    new-instance v2, Landroid/support/v7/widget/AppCompatAutoCompleteTextView;

    invoke-direct {v2, p3, p4}, Landroid/support/v7/widget/AppCompatAutoCompleteTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 131
    goto :goto_104

    .line 133
    :pswitch_f3
    new-instance v2, Landroid/support/v7/widget/AppCompatMultiAutoCompleteTextView;

    invoke-direct {v2, p3, p4}, Landroid/support/v7/widget/AppCompatMultiAutoCompleteTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 134
    goto :goto_104

    .line 136
    :pswitch_f9
    new-instance v2, Landroid/support/v7/widget/AppCompatRatingBar;

    invoke-direct {v2, p3, p4}, Landroid/support/v7/widget/AppCompatRatingBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 137
    goto :goto_104

    .line 139
    :pswitch_ff
    new-instance v2, Landroid/support/v7/widget/AppCompatSeekBar;

    invoke-direct {v2, p3, p4}, Landroid/support/v7/widget/AppCompatSeekBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 143
    :goto_104
    if-nez v2, :cond_10c

    if-eq v1, p3, :cond_10c

    .line 146
    invoke-direct {p0, p3, p2, p4}, Landroid/support/v7/app/AppCompatViewInflater;->createViewFromTag(Landroid/content/Context;Ljava/lang/String;Landroid/util/AttributeSet;)Landroid/view/View;

    move-result-object v2

    .line 149
    :cond_10c
    if-eqz v2, :cond_111

    .line 151
    invoke-direct {p0, v2, p4}, Landroid/support/v7/app/AppCompatViewInflater;->checkOnClickListener(Landroid/view/View;Landroid/util/AttributeSet;)V

    .line 154
    :cond_111
    return-object v2

    :sswitch_data_112
    .sparse-switch
        -0x7404ceea -> :sswitch_99
        -0x56c015e7 -> :sswitch_78
        -0x503aa7ad -> :sswitch_8e
        -0x37f7066e -> :sswitch_23
        -0x37e04bb3 -> :sswitch_5a
        -0x274065a5 -> :sswitch_a4
        -0x1440b607 -> :sswitch_4f
        0x2e46a6ed -> :sswitch_6e
        0x431b5280 -> :sswitch_2e
        0x5445f9ba -> :sswitch_83
        0x5f7507c3 -> :sswitch_64
        0x63577677 -> :sswitch_44
        0x77471352 -> :sswitch_39
    .end sparse-switch

    :pswitch_data_148
    .packed-switch 0x0
        :pswitch_b3
        :pswitch_ba
        :pswitch_c1
        :pswitch_c8
        :pswitch_cf
        :pswitch_d5
        :pswitch_db
        :pswitch_e1
        :pswitch_e7
        :pswitch_ed
        :pswitch_f3
        :pswitch_f9
        :pswitch_ff
    .end packed-switch
.end method

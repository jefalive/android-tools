.class public final Lbr/com/itau/sdk/android/core_ui/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lbr/com/itau/sdk/android/core_ui/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final abc_action_bar_home_description:I = 0x7f070000

.field public static final abc_action_bar_home_description_format:I = 0x7f070001

.field public static final abc_action_bar_home_subtitle_description_format:I = 0x7f070002

.field public static final abc_action_bar_up_description:I = 0x7f070003

.field public static final abc_action_menu_overflow_description:I = 0x7f070004

.field public static final abc_action_mode_done:I = 0x7f070005

.field public static final abc_activity_chooser_view_see_all:I = 0x7f070006

.field public static final abc_activitychooserview_choose_application:I = 0x7f070007

.field public static final abc_capital_off:I = 0x7f070008

.field public static final abc_capital_on:I = 0x7f070009

.field public static final abc_search_hint:I = 0x7f07000a

.field public static final abc_searchview_description_clear:I = 0x7f07000b

.field public static final abc_searchview_description_query:I = 0x7f07000c

.field public static final abc_searchview_description_search:I = 0x7f07000d

.field public static final abc_searchview_description_submit:I = 0x7f07000e

.field public static final abc_searchview_description_voice:I = 0x7f07000f

.field public static final abc_shareactionprovider_share_with:I = 0x7f070010

.field public static final abc_shareactionprovider_share_with_application:I = 0x7f070011

.field public static final abc_toolbar_collapse_description:I = 0x7f070012

.field public static final app_name:I = 0x7f07011b

.field public static final itausdkcore_bt_confirmar:I = 0x7f07037d

.field public static final itausdkcore_bt_voltar:I = 0x7f07037e

.field public static final itausdkcore_dialog_card_password_btn:I = 0x7f07037f

.field public static final itausdkcore_dialog_card_password_btn_description:I = 0x7f070380

.field public static final itausdkcore_dialog_card_password_close:I = 0x7f070381

.field public static final itausdkcore_dialog_card_password_close_description:I = 0x7f070382

.field public static final itausdkcore_dialog_card_password_input_description:I = 0x7f070383

.field public static final itausdkcore_dialog_card_password_input_hint:I = 0x7f070384

.field public static final itausdkcore_dialog_card_password_subtitle:I = 0x7f070385

.field public static final itausdkcore_dialog_card_password_subtitle_description:I = 0x7f070386

.field public static final itausdkcore_dialog_card_password_title:I = 0x7f070387

.field public static final itausdkcore_dialog_card_password_title_description:I = 0x7f070388

.field public static final itausdkcore_dialog_eletronic_passoword_btn_ok:I = 0x7f070389

.field public static final itausdkcore_dialog_eletronic_passoword_hint:I = 0x7f07038a

.field public static final itausdkcore_dialog_eletronic_passoword_hint_description:I = 0x7f07038b

.field public static final itausdkcore_dialog_eletronic_password_subtitle:I = 0x7f07038c

.field public static final itausdkcore_dialog_eletronic_password_subtitle_description:I = 0x7f07038d

.field public static final itausdkcore_dialog_eletronic_password_title:I = 0x7f07038e

.field public static final itausdkcore_dialog_eletronic_password_title_description:I = 0x7f07038f

.field public static final itausdkcore_dialog_token_confirm_action_accessibility:I = 0x7f070390

.field public static final itausdkcore_dialog_token_sms_code_input_hint:I = 0x7f070391

.field public static final itausdkcore_dialog_token_sms_resend_action_accessibility:I = 0x7f070392

.field public static final itausdkcore_dialog_token_title:I = 0x7f070393

.field public static final itausdkcore_dialog_token_title_accessibility:I = 0x7f070394

.field public static final itausdkcore_first_token:I = 0x7f070395

.field public static final itausdkcore_first_token_description:I = 0x7f070396

.field public static final itausdkcore_message_sms_time:I = 0x7f070397

.field public static final itausdkcore_second_token:I = 0x7f070398

.field public static final itausdkcore_second_token_description:I = 0x7f070399

.field public static final itausdkcore_senha_do_cartao:I = 0x7f07039a

.field public static final itausdkcore_str_pass_description:I = 0x7f07039b

.field public static final itausdkcore_token_number:I = 0x7f07039c

.field public static final itausdkcore_unlock_warning_app:I = 0x7f07039d

.field public static final itausdkcore_unlock_warning_chaveiro:I = 0x7f07039e

.field public static final itausdkcore_view_sms_token_type_description:I = 0x7f07039f

.field public static final itausdkcore_view_sms_token_type_str:I = 0x7f0703a0

.field public static final itausdkcore_view_token_type_app_description:I = 0x7f0703a1

.field public static final itausdkcore_view_token_type_app_edit_text_description:I = 0x7f0703a2

.field public static final itausdkcore_view_token_type_ic_seta_baixo_description:I = 0x7f0703a3

.field public static final itausdkcore_view_token_type_key_description:I = 0x7f0703a4

.field public static final itausdkcore_view_token_type_key_edit_text_description:I = 0x7f0703a5

.field public static final itausdkcore_view_token_type_sms_description:I = 0x7f0703a6

.field public static final itausdkcore_view_token_type_sms_edit_text_description:I = 0x7f0703a7

.field public static final itausdkcore_view_token_type_sms_str:I = 0x7f0703a8

.field public static final itausdkcore_view_token_type_sms_str_description:I = 0x7f0703a9

.field public static final itausdkcore_view_token_type_warning_text_description:I = 0x7f0703aa

.field public static final itausdkcore_webview_update_negative:I = 0x7f0703ab

.field public static final itausdkcore_webview_update_positve:I = 0x7f0703ac

.field public static final status_bar_notification_info_overflow:I = 0x7f07004e


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 722
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

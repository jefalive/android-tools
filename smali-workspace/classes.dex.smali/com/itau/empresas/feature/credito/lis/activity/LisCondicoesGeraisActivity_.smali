.class public final Lcom/itau/empresas/feature/credito/lis/activity/LisCondicoesGeraisActivity_;
.super Lcom/itau/empresas/feature/credito/lis/activity/LisCondicoesGeraisActivity;
.source "LisCondicoesGeraisActivity_.java"

# interfaces
.implements Lorg/androidannotations/api/view/HasViews;
.implements Lorg/androidannotations/api/view/OnViewChangedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/itau/empresas/feature/credito/lis/activity/LisCondicoesGeraisActivity_$IntentBuilder_;
    }
.end annotation


# instance fields
.field private final onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;


# direct methods
.method public constructor <init>()V
    .registers 2

    .line 31
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisCondicoesGeraisActivity;-><init>()V

    .line 35
    new-instance v0, Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-direct {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisCondicoesGeraisActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    return-void
.end method

.method static synthetic access$001(Lcom/itau/empresas/feature/credito/lis/activity/LisCondicoesGeraisActivity_;)V
    .registers 1
    .param p0, "x0"    # Lcom/itau/empresas/feature/credito/lis/activity/LisCondicoesGeraisActivity_;

    .line 31
    invoke-super {p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisCondicoesGeraisActivity;->mostraDialogoDeProgresso()V

    return-void
.end method

.method static synthetic access$101(Lcom/itau/empresas/feature/credito/lis/activity/LisCondicoesGeraisActivity_;)V
    .registers 1
    .param p0, "x0"    # Lcom/itau/empresas/feature/credito/lis/activity/LisCondicoesGeraisActivity_;

    .line 31
    invoke-super {p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisCondicoesGeraisActivity;->escondeDialogoDeProgresso()V

    return-void
.end method

.method private init_(Landroid/os/Bundle;)V
    .registers 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 47
    new-instance v0, Lcom/itau/empresas/Preferences_;

    invoke-direct {v0, p0}, Lcom/itau/empresas/Preferences_;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisCondicoesGeraisActivity_;->prefs:Lcom/itau/empresas/Preferences_;

    .line 48
    invoke-static {p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->registerOnViewChangedListener(Lorg/androidannotations/api/view/OnViewChangedListener;)V

    .line 49
    invoke-static {}, Lcom/itau/empresas/CustomApplication_;->getInstance()Lcom/itau/empresas/CustomApplication;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisCondicoesGeraisActivity_;->application:Lcom/itau/empresas/CustomApplication;

    .line 50
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisCondicoesGeraisActivity_;->afterInject()V

    .line 51
    return-void
.end method


# virtual methods
.method public escondeDialogoDeProgresso()V
    .registers 5

    .line 107
    const-string v0, ""

    new-instance v1, Lcom/itau/empresas/feature/credito/lis/activity/LisCondicoesGeraisActivity_$2;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisCondicoesGeraisActivity_$2;-><init>(Lcom/itau/empresas/feature/credito/lis/activity/LisCondicoesGeraisActivity_;)V

    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3}, Lorg/androidannotations/api/UiThreadExecutor;->runTask(Ljava/lang/String;Ljava/lang/Runnable;J)V

    .line 115
    return-void
.end method

.method public mostraDialogoDeProgresso()V
    .registers 5
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "CommitTransaction"
        }
    .end annotation

    .line 95
    const-string v0, ""

    new-instance v1, Lcom/itau/empresas/feature/credito/lis/activity/LisCondicoesGeraisActivity_$1;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisCondicoesGeraisActivity_$1;-><init>(Lcom/itau/empresas/feature/credito/lis/activity/LisCondicoesGeraisActivity_;)V

    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3}, Lorg/androidannotations/api/UiThreadExecutor;->runTask(Ljava/lang/String;Ljava/lang/Runnable;J)V

    .line 103
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 39
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisCondicoesGeraisActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-static {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    move-result-object v1

    .line 40
    .local v1, "previousNotifier":Lorg/androidannotations/api/view/OnViewChangedNotifier;
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/credito/lis/activity/LisCondicoesGeraisActivity_;->init_(Landroid/os/Bundle;)V

    .line 41
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/credito/lis/activity/LisCondicoesGeraisActivity;->onCreate(Landroid/os/Bundle;)V

    .line 42
    invoke-static {v1}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 43
    const v0, 0x7f030046

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/credito/lis/activity/LisCondicoesGeraisActivity_;->setContentView(I)V

    .line 44
    return-void
.end method

.method public onViewChanged(Lorg/androidannotations/api/view/HasViews;)V
    .registers 3
    .param p1, "hasViews"    # Lorg/androidannotations/api/view/HasViews;

    .line 85
    const v0, 0x7f0e01f1

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisCondicoesGeraisActivity_;->recyclerView:Landroid/support/v7/widget/RecyclerView;

    .line 86
    const v0, 0x7f0e00b6

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/Toolbar;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisCondicoesGeraisActivity_;->toolBar:Landroid/support/v7/widget/Toolbar;

    .line 87
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisCondicoesGeraisActivity_;->aoCarregarTela()V

    .line 88
    return-void
.end method

.method public setContentView(I)V
    .registers 3
    .param p1, "layoutResID"    # I

    .line 55
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/credito/lis/activity/LisCondicoesGeraisActivity;->setContentView(I)V

    .line 56
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisCondicoesGeraisActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 57
    return-void
.end method

.method public setContentView(Landroid/view/View;)V
    .registers 3
    .param p1, "view"    # Landroid/view/View;

    .line 67
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/credito/lis/activity/LisCondicoesGeraisActivity;->setContentView(Landroid/view/View;)V

    .line 68
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisCondicoesGeraisActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 69
    return-void
.end method

.method public setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .registers 4
    .param p1, "view"    # Landroid/view/View;
    .param p2, "params"    # Landroid/view/ViewGroup$LayoutParams;

    .line 61
    invoke-super {p0, p1, p2}, Lcom/itau/empresas/feature/credito/lis/activity/LisCondicoesGeraisActivity;->setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 62
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisCondicoesGeraisActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 63
    return-void
.end method

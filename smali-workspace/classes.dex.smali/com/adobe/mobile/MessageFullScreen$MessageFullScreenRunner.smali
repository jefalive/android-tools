.class Lcom/adobe/mobile/MessageFullScreen$MessageFullScreenRunner;
.super Ljava/lang/Object;
.source "MessageFullScreen.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/adobe/mobile/MessageFullScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "MessageFullScreenRunner"
.end annotation


# instance fields
.field private message:Lcom/adobe/mobile/MessageFullScreen;


# direct methods
.method protected constructor <init>(Lcom/adobe/mobile/MessageFullScreen;)V
    .registers 2
    .param p1, "message"    # Lcom/adobe/mobile/MessageFullScreen;

    .line 217
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 218
    iput-object p1, p0, Lcom/adobe/mobile/MessageFullScreen$MessageFullScreenRunner;->message:Lcom/adobe/mobile/MessageFullScreen;

    .line 219
    return-void
.end method


# virtual methods
.method public run()V
    .registers 11

    .line 226
    :try_start_0
    iget-object v0, p0, Lcom/adobe/mobile/MessageFullScreen$MessageFullScreenRunner;->message:Lcom/adobe/mobile/MessageFullScreen;

    new-instance v1, Landroid/webkit/WebView;

    iget-object v2, p0, Lcom/adobe/mobile/MessageFullScreen$MessageFullScreenRunner;->message:Lcom/adobe/mobile/MessageFullScreen;

    iget-object v2, v2, Lcom/adobe/mobile/MessageFullScreen;->messageFullScreenActivity:Landroid/app/Activity;

    invoke-direct {v1, v2}, Landroid/webkit/WebView;-><init>(Landroid/content/Context;)V

    # setter for: Lcom/adobe/mobile/MessageFullScreen;->webView:Landroid/webkit/WebView;
    invoke-static {v0, v1}, Lcom/adobe/mobile/MessageFullScreen;->access$002(Lcom/adobe/mobile/MessageFullScreen;Landroid/webkit/WebView;)Landroid/webkit/WebView;

    .line 227
    iget-object v0, p0, Lcom/adobe/mobile/MessageFullScreen$MessageFullScreenRunner;->message:Lcom/adobe/mobile/MessageFullScreen;

    # getter for: Lcom/adobe/mobile/MessageFullScreen;->webView:Landroid/webkit/WebView;
    invoke-static {v0}, Lcom/adobe/mobile/MessageFullScreen;->access$000(Lcom/adobe/mobile/MessageFullScreen;)Landroid/webkit/WebView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setVerticalScrollBarEnabled(Z)V

    .line 228
    iget-object v0, p0, Lcom/adobe/mobile/MessageFullScreen$MessageFullScreenRunner;->message:Lcom/adobe/mobile/MessageFullScreen;

    # getter for: Lcom/adobe/mobile/MessageFullScreen;->webView:Landroid/webkit/WebView;
    invoke-static {v0}, Lcom/adobe/mobile/MessageFullScreen;->access$000(Lcom/adobe/mobile/MessageFullScreen;)Landroid/webkit/WebView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setHorizontalScrollBarEnabled(Z)V

    .line 229
    iget-object v0, p0, Lcom/adobe/mobile/MessageFullScreen$MessageFullScreenRunner;->message:Lcom/adobe/mobile/MessageFullScreen;

    # getter for: Lcom/adobe/mobile/MessageFullScreen;->webView:Landroid/webkit/WebView;
    invoke-static {v0}, Lcom/adobe/mobile/MessageFullScreen;->access$000(Lcom/adobe/mobile/MessageFullScreen;)Landroid/webkit/WebView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setBackgroundColor(I)V

    .line 230
    iget-object v0, p0, Lcom/adobe/mobile/MessageFullScreen$MessageFullScreenRunner;->message:Lcom/adobe/mobile/MessageFullScreen;

    # getter for: Lcom/adobe/mobile/MessageFullScreen;->webView:Landroid/webkit/WebView;
    invoke-static {v0}, Lcom/adobe/mobile/MessageFullScreen;->access$000(Lcom/adobe/mobile/MessageFullScreen;)Landroid/webkit/WebView;

    move-result-object v0

    new-instance v1, Lcom/adobe/mobile/MessageFullScreen$MessageFullScreenWebViewClient;

    iget-object v2, p0, Lcom/adobe/mobile/MessageFullScreen$MessageFullScreenRunner;->message:Lcom/adobe/mobile/MessageFullScreen;

    invoke-direct {v1, v2}, Lcom/adobe/mobile/MessageFullScreen$MessageFullScreenWebViewClient;-><init>(Lcom/adobe/mobile/MessageFullScreen;)V

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 231
    iget-object v0, p0, Lcom/adobe/mobile/MessageFullScreen$MessageFullScreenRunner;->message:Lcom/adobe/mobile/MessageFullScreen;

    # getter for: Lcom/adobe/mobile/MessageFullScreen;->webView:Landroid/webkit/WebView;
    invoke-static {v0}, Lcom/adobe/mobile/MessageFullScreen;->access$000(Lcom/adobe/mobile/MessageFullScreen;)Landroid/webkit/WebView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v6

    .line 232
    .local v6, "settings":Landroid/webkit/WebSettings;
    const/4 v0, 0x1

    invoke-virtual {v6, v0}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 233
    const/4 v0, 0x1

    invoke-virtual {v6, v0}, Landroid/webkit/WebSettings;->setAllowFileAccess(Z)V

    .line 234
    const-string v0, "UTF-8"

    invoke-virtual {v6, v0}, Landroid/webkit/WebSettings;->setDefaultTextEncodingName(Ljava/lang/String;)V

    .line 236
    iget-object v0, p0, Lcom/adobe/mobile/MessageFullScreen$MessageFullScreenRunner;->message:Lcom/adobe/mobile/MessageFullScreen;

    # getter for: Lcom/adobe/mobile/MessageFullScreen;->webView:Landroid/webkit/WebView;
    invoke-static {v0}, Lcom/adobe/mobile/MessageFullScreen;->access$000(Lcom/adobe/mobile/MessageFullScreen;)Landroid/webkit/WebView;

    move-result-object v0

    const-string v1, "file:///android_asset/"

    iget-object v2, p0, Lcom/adobe/mobile/MessageFullScreen$MessageFullScreenRunner;->message:Lcom/adobe/mobile/MessageFullScreen;

    iget-object v2, v2, Lcom/adobe/mobile/MessageFullScreen;->replacedHtml:Ljava/lang/String;

    const-string v3, "text/html"

    const-string v4, "UTF-8"

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/webkit/WebView;->loadDataWithBaseURL(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 238
    iget-object v0, p0, Lcom/adobe/mobile/MessageFullScreen$MessageFullScreenRunner;->message:Lcom/adobe/mobile/MessageFullScreen;

    iget-object v0, v0, Lcom/adobe/mobile/MessageFullScreen;->rootViewGroup:Landroid/view/ViewGroup;

    if-nez v0, :cond_7b

    .line 239
    const-string v0, "Messages - unable to get root view group from os"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logErrorFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 240
    iget-object v0, p0, Lcom/adobe/mobile/MessageFullScreen$MessageFullScreenRunner;->message:Lcom/adobe/mobile/MessageFullScreen;

    # invokes: Lcom/adobe/mobile/MessageFullScreen;->killMessageActivity(Lcom/adobe/mobile/MessageFullScreen;)V
    invoke-static {v0}, Lcom/adobe/mobile/MessageFullScreen;->access$100(Lcom/adobe/mobile/MessageFullScreen;)V
    :try_end_7a
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_7a} :catch_db

    .line 241
    return-void

    .line 244
    :cond_7b
    :try_start_7b
    iget-object v0, p0, Lcom/adobe/mobile/MessageFullScreen$MessageFullScreenRunner;->message:Lcom/adobe/mobile/MessageFullScreen;

    iget-object v0, v0, Lcom/adobe/mobile/MessageFullScreen;->rootViewGroup:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getMeasuredWidth()I

    move-result v7

    .line 245
    .local v7, "width":I
    iget-object v0, p0, Lcom/adobe/mobile/MessageFullScreen$MessageFullScreenRunner;->message:Lcom/adobe/mobile/MessageFullScreen;

    iget-object v0, v0, Lcom/adobe/mobile/MessageFullScreen;->rootViewGroup:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getMeasuredHeight()I

    move-result v8

    .line 248
    .local v8, "height":I
    if-eqz v7, :cond_8f

    if-nez v8, :cond_9d

    .line 249
    :cond_8f
    const-string v0, "Messages - root view hasn\'t been measured, cannot show message"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logErrorFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 250
    iget-object v0, p0, Lcom/adobe/mobile/MessageFullScreen$MessageFullScreenRunner;->message:Lcom/adobe/mobile/MessageFullScreen;

    # invokes: Lcom/adobe/mobile/MessageFullScreen;->killMessageActivity(Lcom/adobe/mobile/MessageFullScreen;)V
    invoke-static {v0}, Lcom/adobe/mobile/MessageFullScreen;->access$100(Lcom/adobe/mobile/MessageFullScreen;)V
    :try_end_9c
    .catch Ljava/lang/Exception; {:try_start_7b .. :try_end_9c} :catch_db

    .line 251
    return-void

    .line 255
    :cond_9d
    :try_start_9d
    iget-object v0, p0, Lcom/adobe/mobile/MessageFullScreen$MessageFullScreenRunner;->message:Lcom/adobe/mobile/MessageFullScreen;

    iget-boolean v0, v0, Lcom/adobe/mobile/MessageFullScreen;->isVisible:Z

    if-eqz v0, :cond_b1

    .line 256
    iget-object v0, p0, Lcom/adobe/mobile/MessageFullScreen$MessageFullScreenRunner;->message:Lcom/adobe/mobile/MessageFullScreen;

    iget-object v0, v0, Lcom/adobe/mobile/MessageFullScreen;->rootViewGroup:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/adobe/mobile/MessageFullScreen$MessageFullScreenRunner;->message:Lcom/adobe/mobile/MessageFullScreen;

    # getter for: Lcom/adobe/mobile/MessageFullScreen;->webView:Landroid/webkit/WebView;
    invoke-static {v1}, Lcom/adobe/mobile/MessageFullScreen;->access$000(Lcom/adobe/mobile/MessageFullScreen;)Landroid/webkit/WebView;

    move-result-object v1

    invoke-virtual {v0, v1, v7, v8}, Landroid/view/ViewGroup;->addView(Landroid/view/View;II)V

    goto :goto_d5

    .line 260
    :cond_b1
    new-instance v9, Landroid/view/animation/TranslateAnimation;

    int-to-float v0, v8

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-direct {v9, v1, v2, v0, v3}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .line 261
    .local v9, "translate":Landroid/view/animation/Animation;
    const-wide/16 v0, 0x12c

    invoke-virtual {v9, v0, v1}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 262
    iget-object v0, p0, Lcom/adobe/mobile/MessageFullScreen$MessageFullScreenRunner;->message:Lcom/adobe/mobile/MessageFullScreen;

    # getter for: Lcom/adobe/mobile/MessageFullScreen;->webView:Landroid/webkit/WebView;
    invoke-static {v0}, Lcom/adobe/mobile/MessageFullScreen;->access$000(Lcom/adobe/mobile/MessageFullScreen;)Landroid/webkit/WebView;

    move-result-object v0

    invoke-virtual {v0, v9}, Landroid/webkit/WebView;->setAnimation(Landroid/view/animation/Animation;)V

    .line 265
    iget-object v0, p0, Lcom/adobe/mobile/MessageFullScreen$MessageFullScreenRunner;->message:Lcom/adobe/mobile/MessageFullScreen;

    iget-object v0, v0, Lcom/adobe/mobile/MessageFullScreen;->rootViewGroup:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/adobe/mobile/MessageFullScreen$MessageFullScreenRunner;->message:Lcom/adobe/mobile/MessageFullScreen;

    # getter for: Lcom/adobe/mobile/MessageFullScreen;->webView:Landroid/webkit/WebView;
    invoke-static {v1}, Lcom/adobe/mobile/MessageFullScreen;->access$000(Lcom/adobe/mobile/MessageFullScreen;)Landroid/webkit/WebView;

    move-result-object v1

    invoke-virtual {v0, v1, v7, v8}, Landroid/view/ViewGroup;->addView(Landroid/view/View;II)V

    .line 269
    .end local v9    # "translate":Landroid/view/animation/Animation;
    :goto_d5
    iget-object v0, p0, Lcom/adobe/mobile/MessageFullScreen$MessageFullScreenRunner;->message:Lcom/adobe/mobile/MessageFullScreen;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/adobe/mobile/MessageFullScreen;->isVisible:Z
    :try_end_da
    .catch Ljava/lang/Exception; {:try_start_9d .. :try_end_da} :catch_db

    .line 273
    .end local v6    # "settings":Landroid/webkit/WebSettings;
    .end local v7    # "width":I
    .end local v8    # "height":I
    goto :goto_eb

    .line 271
    :catch_db
    move-exception v6

    .line 272
    .local v6, "ex":Ljava/lang/Exception;
    const-string v0, "Messages - Failed to show full screen message (%s)"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v6}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logDebugFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 274
    .end local v6    # "ex":Ljava/lang/Exception;
    :goto_eb
    return-void
.end method

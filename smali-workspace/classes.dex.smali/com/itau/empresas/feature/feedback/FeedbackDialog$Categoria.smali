.class public Lcom/itau/empresas/feature/feedback/FeedbackDialog$Categoria;
.super Ljava/lang/Object;
.source "FeedbackDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/itau/empresas/feature/feedback/FeedbackDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Categoria"
.end annotation


# instance fields
.field private codigo:I

.field private descricao:Ljava/lang/String;

.field private invalido:Ljava/lang/String;


# direct methods
.method public constructor <init>(ILjava/lang/String;Ljava/lang/String;)V
    .registers 4
    .param p1, "codigo"    # I
    .param p2, "descricao"    # Ljava/lang/String;
    .param p3, "invalido"    # Ljava/lang/String;

    .line 327
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 328
    iput p1, p0, Lcom/itau/empresas/feature/feedback/FeedbackDialog$Categoria;->codigo:I

    .line 329
    iput-object p2, p0, Lcom/itau/empresas/feature/feedback/FeedbackDialog$Categoria;->descricao:Ljava/lang/String;

    .line 330
    iput-object p3, p0, Lcom/itau/empresas/feature/feedback/FeedbackDialog$Categoria;->invalido:Ljava/lang/String;

    .line 331
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .registers 5
    .param p1, "o"    # Ljava/lang/Object;

    .line 352
    if-ne p0, p1, :cond_4

    .line 353
    const/4 v0, 0x1

    return v0

    .line 355
    :cond_4
    if-eqz p1, :cond_10

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    if-eq v0, v1, :cond_12

    .line 356
    :cond_10
    const/4 v0, 0x0

    return v0

    .line 359
    :cond_12
    move-object v2, p1

    check-cast v2, Lcom/itau/empresas/feature/feedback/FeedbackDialog$Categoria;

    .line 361
    .local v2, "categoria":Lcom/itau/empresas/feature/feedback/FeedbackDialog$Categoria;
    iget v0, p0, Lcom/itau/empresas/feature/feedback/FeedbackDialog$Categoria;->codigo:I

    iget v1, v2, Lcom/itau/empresas/feature/feedback/FeedbackDialog$Categoria;->codigo:I

    if-eq v0, v1, :cond_1d

    .line 362
    const/4 v0, 0x0

    return v0

    .line 364
    :cond_1d
    iget-object v0, p0, Lcom/itau/empresas/feature/feedback/FeedbackDialog$Categoria;->descricao:Ljava/lang/String;

    if-eqz v0, :cond_2c

    iget-object v0, p0, Lcom/itau/empresas/feature/feedback/FeedbackDialog$Categoria;->descricao:Ljava/lang/String;

    iget-object v1, v2, Lcom/itau/empresas/feature/feedback/FeedbackDialog$Categoria;->descricao:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_32

    goto :goto_30

    :cond_2c
    iget-object v0, v2, Lcom/itau/empresas/feature/feedback/FeedbackDialog$Categoria;->descricao:Ljava/lang/String;

    if-eqz v0, :cond_32

    .line 366
    :goto_30
    const/4 v0, 0x0

    return v0

    .line 368
    :cond_32
    iget-object v0, p0, Lcom/itau/empresas/feature/feedback/FeedbackDialog$Categoria;->invalido:Ljava/lang/String;

    if-eqz v0, :cond_41

    iget-object v0, p0, Lcom/itau/empresas/feature/feedback/FeedbackDialog$Categoria;->invalido:Ljava/lang/String;

    iget-object v1, v2, Lcom/itau/empresas/feature/feedback/FeedbackDialog$Categoria;->invalido:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_45

    goto :goto_47

    :cond_41
    iget-object v0, v2, Lcom/itau/empresas/feature/feedback/FeedbackDialog$Categoria;->invalido:Ljava/lang/String;

    if-nez v0, :cond_47

    :cond_45
    const/4 v0, 0x1

    goto :goto_48

    :cond_47
    :goto_47
    const/4 v0, 0x0

    :goto_48
    return v0
.end method

.method public getCodigo()I
    .registers 2

    .line 334
    iget v0, p0, Lcom/itau/empresas/feature/feedback/FeedbackDialog$Categoria;->codigo:I

    return v0
.end method

.method public getDescricao()Ljava/lang/String;
    .registers 2

    .line 338
    iget-object v0, p0, Lcom/itau/empresas/feature/feedback/FeedbackDialog$Categoria;->descricao:Ljava/lang/String;

    return-object v0
.end method

.method public getInvalido()Ljava/lang/String;
    .registers 2

    .line 342
    iget-object v0, p0, Lcom/itau/empresas/feature/feedback/FeedbackDialog$Categoria;->invalido:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .registers 2

    .line 374
    iget-object v0, p0, Lcom/itau/empresas/feature/feedback/FeedbackDialog$Categoria;->descricao:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .registers 2

    .line 347
    iget-object v0, p0, Lcom/itau/empresas/feature/feedback/FeedbackDialog$Categoria;->descricao:Ljava/lang/String;

    return-object v0
.end method

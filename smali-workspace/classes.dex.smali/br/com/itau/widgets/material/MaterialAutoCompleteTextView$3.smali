.class Lbr/com/itau/widgets/material/MaterialAutoCompleteTextView$3;
.super Ljava/lang/Object;
.source "MaterialAutoCompleteTextView.java"

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lbr/com/itau/widgets/material/MaterialAutoCompleteTextView;->initFloatingLabel()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lbr/com/itau/widgets/material/MaterialAutoCompleteTextView;


# direct methods
.method constructor <init>(Lbr/com/itau/widgets/material/MaterialAutoCompleteTextView;)V
    .registers 2

    .line 839
    iput-object p1, p0, Lbr/com/itau/widgets/material/MaterialAutoCompleteTextView$3;->this$0:Lbr/com/itau/widgets/material/MaterialAutoCompleteTextView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFocusChange(Landroid/view/View;Z)V
    .registers 4
    .param p1, "v"    # Landroid/view/View;
    .param p2, "hasFocus"    # Z

    .line 842
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialAutoCompleteTextView$3;->this$0:Lbr/com/itau/widgets/material/MaterialAutoCompleteTextView;

    # getter for: Lbr/com/itau/widgets/material/MaterialAutoCompleteTextView;->floatingLabelEnabled:Z
    invoke-static {v0}, Lbr/com/itau/widgets/material/MaterialAutoCompleteTextView;->access$200(Lbr/com/itau/widgets/material/MaterialAutoCompleteTextView;)Z

    move-result v0

    if-eqz v0, :cond_25

    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialAutoCompleteTextView$3;->this$0:Lbr/com/itau/widgets/material/MaterialAutoCompleteTextView;

    # getter for: Lbr/com/itau/widgets/material/MaterialAutoCompleteTextView;->highlightFloatingLabel:Z
    invoke-static {v0}, Lbr/com/itau/widgets/material/MaterialAutoCompleteTextView;->access$500(Lbr/com/itau/widgets/material/MaterialAutoCompleteTextView;)Z

    move-result v0

    if-eqz v0, :cond_25

    .line 843
    if-eqz p2, :cond_1c

    .line 844
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialAutoCompleteTextView$3;->this$0:Lbr/com/itau/widgets/material/MaterialAutoCompleteTextView;

    # invokes: Lbr/com/itau/widgets/material/MaterialAutoCompleteTextView;->getLabelFocusAnimator()Lcom/nineoldandroids/animation/ObjectAnimator;
    invoke-static {v0}, Lbr/com/itau/widgets/material/MaterialAutoCompleteTextView;->access$600(Lbr/com/itau/widgets/material/MaterialAutoCompleteTextView;)Lcom/nineoldandroids/animation/ObjectAnimator;

    move-result-object v0

    invoke-virtual {v0}, Lcom/nineoldandroids/animation/ObjectAnimator;->start()V

    goto :goto_25

    .line 846
    :cond_1c
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialAutoCompleteTextView$3;->this$0:Lbr/com/itau/widgets/material/MaterialAutoCompleteTextView;

    # invokes: Lbr/com/itau/widgets/material/MaterialAutoCompleteTextView;->getLabelFocusAnimator()Lcom/nineoldandroids/animation/ObjectAnimator;
    invoke-static {v0}, Lbr/com/itau/widgets/material/MaterialAutoCompleteTextView;->access$600(Lbr/com/itau/widgets/material/MaterialAutoCompleteTextView;)Lcom/nineoldandroids/animation/ObjectAnimator;

    move-result-object v0

    invoke-virtual {v0}, Lcom/nineoldandroids/animation/ObjectAnimator;->reverse()V

    .line 849
    :cond_25
    :goto_25
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialAutoCompleteTextView$3;->this$0:Lbr/com/itau/widgets/material/MaterialAutoCompleteTextView;

    # getter for: Lbr/com/itau/widgets/material/MaterialAutoCompleteTextView;->validateOnFocusLost:Z
    invoke-static {v0}, Lbr/com/itau/widgets/material/MaterialAutoCompleteTextView;->access$700(Lbr/com/itau/widgets/material/MaterialAutoCompleteTextView;)Z

    move-result v0

    if-eqz v0, :cond_34

    if-nez p2, :cond_34

    .line 850
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialAutoCompleteTextView$3;->this$0:Lbr/com/itau/widgets/material/MaterialAutoCompleteTextView;

    invoke-virtual {v0}, Lbr/com/itau/widgets/material/MaterialAutoCompleteTextView;->validate()Z

    .line 852
    :cond_34
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialAutoCompleteTextView$3;->this$0:Lbr/com/itau/widgets/material/MaterialAutoCompleteTextView;

    iget-object v0, v0, Lbr/com/itau/widgets/material/MaterialAutoCompleteTextView;->outerFocusChangeListener:Landroid/view/View$OnFocusChangeListener;

    if-eqz v0, :cond_41

    .line 853
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialAutoCompleteTextView$3;->this$0:Lbr/com/itau/widgets/material/MaterialAutoCompleteTextView;

    iget-object v0, v0, Lbr/com/itau/widgets/material/MaterialAutoCompleteTextView;->outerFocusChangeListener:Landroid/view/View$OnFocusChangeListener;

    invoke-interface {v0, p1, p2}, Landroid/view/View$OnFocusChangeListener;->onFocusChange(Landroid/view/View;Z)V

    .line 855
    :cond_41
    return-void
.end method

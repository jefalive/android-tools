.class public Lbr/com/itau/textovoz/model/OverflowMessage;
.super Ljava/lang/Object;
.source "OverflowMessage.java"


# instance fields
.field private image:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "imagem"
    .end annotation
.end field

.field private name:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "nome"
    .end annotation
.end field

.field private text:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "texto"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getName()Ljava/lang/String;
    .registers 2

    .line 18
    iget-object v0, p0, Lbr/com/itau/textovoz/model/OverflowMessage;->name:Ljava/lang/String;

    return-object v0
.end method

.method public getText()Ljava/lang/String;
    .registers 2

    .line 26
    iget-object v0, p0, Lbr/com/itau/textovoz/model/OverflowMessage;->text:Ljava/lang/String;

    return-object v0
.end method

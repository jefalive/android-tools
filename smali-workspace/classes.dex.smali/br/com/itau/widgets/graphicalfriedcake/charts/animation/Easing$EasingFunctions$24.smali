.class final Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingFunctions$24;
.super Ljava/lang/Object;
.source "Easing.java"

# interfaces
.implements Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/EasingFunction;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingFunctions;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .registers 1

    .line 584
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getInterpolation(F)F
    .registers 7
    .param p1, "input"    # F

    .line 595
    const v3, 0x3fd9cd60

    .line 596
    .local v3, "s":F
    move v4, p1

    .line 597
    .local v4, "position":F
    const/high16 v0, 0x3f800000    # 1.0f

    sub-float/2addr v4, v0

    .line 598
    mul-float v0, v4, v4

    const v1, 0x402ce6b0

    mul-float/2addr v1, v4

    const v2, 0x3fd9cd60

    add-float/2addr v1, v2

    mul-float/2addr v0, v1

    const/high16 v1, 0x3f800000    # 1.0f

    add-float/2addr v0, v1

    return v0
.end method

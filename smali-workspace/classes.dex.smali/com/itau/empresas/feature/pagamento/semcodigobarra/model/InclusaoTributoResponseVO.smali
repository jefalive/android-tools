.class public Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/InclusaoTributoResponseVO;
.super Ljava/lang/Object;
.source "InclusaoTributoResponseVO.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private codigoPagamento:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "codigo_pagamento"
    .end annotation
.end field

.field private codigoRetorno:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "codigo_retorno"
    .end annotation
.end field

.field private codigoServico:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "codigo_servico"
    .end annotation
.end field

.field private competencia:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "competencia"
    .end annotation
.end field

.field private dataEstouro:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "data_estouro_LDP"
    .end annotation
.end field

.field private dataPagamento:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "data_pagamento"
    .end annotation
.end field

.field private duplicado:Z
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "duplicado"
    .end annotation
.end field

.field private identificadorPagamento:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "identificador_pagamento"
    .end annotation
.end field

.field private indicadorEstouroLdp:Z
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "indicador_estouro_LDP"
    .end annotation
.end field

.field private modalidadePagamento:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "modalidade_pagamento"
    .end annotation
.end field

.field private numeroLote:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "numero_lote"
    .end annotation
.end field

.field private tipoPagamento:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "tipo_pagamento"
    .end annotation
.end field

.field private valorTotal:Ljava/math/BigDecimal;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "valor_total"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getDataPagamento()Ljava/lang/String;
    .registers 2

    .line 112
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/InclusaoTributoResponseVO;->dataPagamento:Ljava/lang/String;

    return-object v0
.end method

.method public getIdentificadorPagamento()Ljava/lang/String;
    .registers 2

    .line 96
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/InclusaoTributoResponseVO;->identificadorPagamento:Ljava/lang/String;

    return-object v0
.end method

.method public getTipoPagamento()Ljava/lang/String;
    .registers 2

    .line 80
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/InclusaoTributoResponseVO;->tipoPagamento:Ljava/lang/String;

    return-object v0
.end method

.method public getValorTotal()Ljava/math/BigDecimal;
    .registers 2

    .line 44
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/InclusaoTributoResponseVO;->valorTotal:Ljava/math/BigDecimal;

    return-object v0
.end method

.method public isDuplicado()Z
    .registers 2

    .line 104
    iget-boolean v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/InclusaoTributoResponseVO;->duplicado:Z

    return v0
.end method

.method public isIndicadorEstouroLdp()Z
    .registers 2

    .line 64
    iget-boolean v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/InclusaoTributoResponseVO;->indicadorEstouroLdp:Z

    return v0
.end method

.method public setIdentificadorPagamento(Ljava/lang/String;)V
    .registers 2
    .param p1, "identificadorPagamento"    # Ljava/lang/String;

    .line 100
    iput-object p1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/InclusaoTributoResponseVO;->identificadorPagamento:Ljava/lang/String;

    .line 101
    return-void
.end method

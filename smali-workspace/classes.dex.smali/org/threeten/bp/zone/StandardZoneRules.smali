.class final Lorg/threeten/bp/zone/StandardZoneRules;
.super Lorg/threeten/bp/zone/ZoneRules;
.source "StandardZoneRules.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = 0x2a3f985312278703L


# instance fields
.field private final lastRules:[Lorg/threeten/bp/zone/ZoneOffsetTransitionRule;

.field private final lastRulesCache:Ljava/util/concurrent/ConcurrentMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentMap<Ljava/lang/Integer;[Lorg/threeten/bp/zone/ZoneOffsetTransition;>;"
        }
    .end annotation
.end field

.field private final savingsInstantTransitions:[J

.field private final savingsLocalTransitions:[Lorg/threeten/bp/LocalDateTime;

.field private final standardOffsets:[Lorg/threeten/bp/ZoneOffset;

.field private final standardTransitions:[J

.field private final wallOffsets:[Lorg/threeten/bp/ZoneOffset;


# direct methods
.method private constructor <init>([J[Lorg/threeten/bp/ZoneOffset;[J[Lorg/threeten/bp/ZoneOffset;[Lorg/threeten/bp/zone/ZoneOffsetTransitionRule;)V
    .registers 13
    .param p1, "standardTransitions"    # [J
    .param p2, "standardOffsets"    # [Lorg/threeten/bp/ZoneOffset;
    .param p3, "savingsInstantTransitions"    # [J
    .param p4, "wallOffsets"    # [Lorg/threeten/bp/ZoneOffset;
    .param p5, "lastRules"    # [Lorg/threeten/bp/zone/ZoneOffsetTransitionRule;

    .line 175
    invoke-direct {p0}, Lorg/threeten/bp/zone/ZoneRules;-><init>()V

    .line 101
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lorg/threeten/bp/zone/StandardZoneRules;->lastRulesCache:Ljava/util/concurrent/ConcurrentMap;

    .line 177
    iput-object p1, p0, Lorg/threeten/bp/zone/StandardZoneRules;->standardTransitions:[J

    .line 178
    iput-object p2, p0, Lorg/threeten/bp/zone/StandardZoneRules;->standardOffsets:[Lorg/threeten/bp/ZoneOffset;

    .line 179
    iput-object p3, p0, Lorg/threeten/bp/zone/StandardZoneRules;->savingsInstantTransitions:[J

    .line 180
    iput-object p4, p0, Lorg/threeten/bp/zone/StandardZoneRules;->wallOffsets:[Lorg/threeten/bp/ZoneOffset;

    .line 181
    iput-object p5, p0, Lorg/threeten/bp/zone/StandardZoneRules;->lastRules:[Lorg/threeten/bp/zone/ZoneOffsetTransitionRule;

    .line 184
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 185
    .local v2, "localTransitionList":Ljava/util/List;, "Ljava/util/List<Lorg/threeten/bp/LocalDateTime;>;"
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1a
    array-length v0, p3

    if-ge v3, v0, :cond_50

    .line 186
    aget-object v4, p4, v3

    .line 187
    .local v4, "before":Lorg/threeten/bp/ZoneOffset;
    add-int/lit8 v0, v3, 0x1

    aget-object v5, p4, v0

    .line 188
    .local v5, "after":Lorg/threeten/bp/ZoneOffset;
    new-instance v6, Lorg/threeten/bp/zone/ZoneOffsetTransition;

    aget-wide v0, p3, v3

    invoke-direct {v6, v0, v1, v4, v5}, Lorg/threeten/bp/zone/ZoneOffsetTransition;-><init>(JLorg/threeten/bp/ZoneOffset;Lorg/threeten/bp/ZoneOffset;)V

    .line 189
    .local v6, "trans":Lorg/threeten/bp/zone/ZoneOffsetTransition;
    invoke-virtual {v6}, Lorg/threeten/bp/zone/ZoneOffsetTransition;->isGap()Z

    move-result v0

    if-eqz v0, :cond_3f

    .line 190
    invoke-virtual {v6}, Lorg/threeten/bp/zone/ZoneOffsetTransition;->getDateTimeBefore()Lorg/threeten/bp/LocalDateTime;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 191
    invoke-virtual {v6}, Lorg/threeten/bp/zone/ZoneOffsetTransition;->getDateTimeAfter()Lorg/threeten/bp/LocalDateTime;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_4d

    .line 193
    :cond_3f
    invoke-virtual {v6}, Lorg/threeten/bp/zone/ZoneOffsetTransition;->getDateTimeAfter()Lorg/threeten/bp/LocalDateTime;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 194
    invoke-virtual {v6}, Lorg/threeten/bp/zone/ZoneOffsetTransition;->getDateTimeBefore()Lorg/threeten/bp/LocalDateTime;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 185
    .end local v4    # "before":Lorg/threeten/bp/ZoneOffset;
    .end local v5    # "after":Lorg/threeten/bp/ZoneOffset;
    .end local v6    # "trans":Lorg/threeten/bp/zone/ZoneOffsetTransition;
    :goto_4d
    add-int/lit8 v3, v3, 0x1

    goto :goto_1a

    .line 197
    .end local v3    # "i":I
    :cond_50
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lorg/threeten/bp/LocalDateTime;

    invoke-interface {v2, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lorg/threeten/bp/LocalDateTime;

    iput-object v0, p0, Lorg/threeten/bp/zone/StandardZoneRules;->savingsLocalTransitions:[Lorg/threeten/bp/LocalDateTime;

    .line 198
    return-void
.end method

.method private findOffsetInfo(Lorg/threeten/bp/LocalDateTime;Lorg/threeten/bp/zone/ZoneOffsetTransition;)Ljava/lang/Object;
    .registers 5
    .param p1, "dt"    # Lorg/threeten/bp/LocalDateTime;
    .param p2, "trans"    # Lorg/threeten/bp/zone/ZoneOffsetTransition;

    .line 388
    invoke-virtual {p2}, Lorg/threeten/bp/zone/ZoneOffsetTransition;->getDateTimeBefore()Lorg/threeten/bp/LocalDateTime;

    move-result-object v1

    .line 389
    .local v1, "localTransition":Lorg/threeten/bp/LocalDateTime;
    invoke-virtual {p2}, Lorg/threeten/bp/zone/ZoneOffsetTransition;->isGap()Z

    move-result v0

    if-eqz v0, :cond_25

    .line 390
    invoke-virtual {p1, v1}, Lorg/threeten/bp/LocalDateTime;->isBefore(Lorg/threeten/bp/chrono/ChronoLocalDateTime;)Z

    move-result v0

    if-eqz v0, :cond_15

    .line 391
    invoke-virtual {p2}, Lorg/threeten/bp/zone/ZoneOffsetTransition;->getOffsetBefore()Lorg/threeten/bp/ZoneOffset;

    move-result-object v0

    return-object v0

    .line 393
    :cond_15
    invoke-virtual {p2}, Lorg/threeten/bp/zone/ZoneOffsetTransition;->getDateTimeAfter()Lorg/threeten/bp/LocalDateTime;

    move-result-object v0

    invoke-virtual {p1, v0}, Lorg/threeten/bp/LocalDateTime;->isBefore(Lorg/threeten/bp/chrono/ChronoLocalDateTime;)Z

    move-result v0

    if-eqz v0, :cond_20

    .line 394
    return-object p2

    .line 396
    :cond_20
    invoke-virtual {p2}, Lorg/threeten/bp/zone/ZoneOffsetTransition;->getOffsetAfter()Lorg/threeten/bp/ZoneOffset;

    move-result-object v0

    return-object v0

    .line 399
    :cond_25
    invoke-virtual {p1, v1}, Lorg/threeten/bp/LocalDateTime;->isBefore(Lorg/threeten/bp/chrono/ChronoLocalDateTime;)Z

    move-result v0

    if-nez v0, :cond_30

    .line 400
    invoke-virtual {p2}, Lorg/threeten/bp/zone/ZoneOffsetTransition;->getOffsetAfter()Lorg/threeten/bp/ZoneOffset;

    move-result-object v0

    return-object v0

    .line 402
    :cond_30
    invoke-virtual {p2}, Lorg/threeten/bp/zone/ZoneOffsetTransition;->getDateTimeAfter()Lorg/threeten/bp/LocalDateTime;

    move-result-object v0

    invoke-virtual {p1, v0}, Lorg/threeten/bp/LocalDateTime;->isBefore(Lorg/threeten/bp/chrono/ChronoLocalDateTime;)Z

    move-result v0

    if-eqz v0, :cond_3f

    .line 403
    invoke-virtual {p2}, Lorg/threeten/bp/zone/ZoneOffsetTransition;->getOffsetBefore()Lorg/threeten/bp/ZoneOffset;

    move-result-object v0

    return-object v0

    .line 405
    :cond_3f
    return-object p2
.end method

.method private findTransitionArray(I)[Lorg/threeten/bp/zone/ZoneOffsetTransition;
    .registers 7
    .param p1, "year"    # I

    .line 423
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 424
    .local v1, "yearObj":Ljava/lang/Integer;
    iget-object v0, p0, Lorg/threeten/bp/zone/StandardZoneRules;->lastRulesCache:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, [Lorg/threeten/bp/zone/ZoneOffsetTransition;

    .line 425
    .local v2, "transArray":[Lorg/threeten/bp/zone/ZoneOffsetTransition;
    if-eqz v2, :cond_10

    .line 426
    return-object v2

    .line 428
    :cond_10
    iget-object v3, p0, Lorg/threeten/bp/zone/StandardZoneRules;->lastRules:[Lorg/threeten/bp/zone/ZoneOffsetTransitionRule;

    .line 429
    .local v3, "ruleArray":[Lorg/threeten/bp/zone/ZoneOffsetTransitionRule;
    array-length v0, v3

    new-array v2, v0, [Lorg/threeten/bp/zone/ZoneOffsetTransition;

    .line 430
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_16
    array-length v0, v3

    if-ge v4, v0, :cond_24

    .line 431
    aget-object v0, v3, v4

    invoke-virtual {v0, p1}, Lorg/threeten/bp/zone/ZoneOffsetTransitionRule;->createTransition(I)Lorg/threeten/bp/zone/ZoneOffsetTransition;

    move-result-object v0

    aput-object v0, v2, v4

    .line 430
    add-int/lit8 v4, v4, 0x1

    goto :goto_16

    .line 433
    .end local v4    # "i":I
    :cond_24
    const/16 v0, 0x834

    if-ge p1, v0, :cond_2d

    .line 434
    iget-object v0, p0, Lorg/threeten/bp/zone/StandardZoneRules;->lastRulesCache:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0, v1, v2}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 436
    :cond_2d
    return-object v2
.end method

.method private findYear(JLorg/threeten/bp/ZoneOffset;)I
    .registers 10
    .param p1, "epochSecond"    # J
    .param p3, "offset"    # Lorg/threeten/bp/ZoneOffset;

    .line 548
    invoke-virtual {p3}, Lorg/threeten/bp/ZoneOffset;->getTotalSeconds()I

    move-result v0

    int-to-long v0, v0

    add-long v2, p1, v0

    .line 549
    .local v2, "localSecond":J
    const-wide/32 v0, 0x15180

    invoke-static {v2, v3, v0, v1}, Lorg/threeten/bp/jdk8/Jdk8Methods;->floorDiv(JJ)J

    move-result-wide v4

    .line 550
    .local v4, "localEpochDay":J
    invoke-static {v4, v5}, Lorg/threeten/bp/LocalDate;->ofEpochDay(J)Lorg/threeten/bp/LocalDate;

    move-result-object v0

    invoke-virtual {v0}, Lorg/threeten/bp/LocalDate;->getYear()I

    move-result v0

    return v0
.end method

.method private getOffsetInfo(Lorg/threeten/bp/LocalDateTime;)Ljava/lang/Object;
    .registers 11
    .param p1, "dt"    # Lorg/threeten/bp/LocalDateTime;

    .line 334
    iget-object v0, p0, Lorg/threeten/bp/zone/StandardZoneRules;->lastRules:[Lorg/threeten/bp/zone/ZoneOffsetTransitionRule;

    array-length v0, v0

    if-lez v0, :cond_3b

    iget-object v0, p0, Lorg/threeten/bp/zone/StandardZoneRules;->savingsLocalTransitions:[Lorg/threeten/bp/LocalDateTime;

    iget-object v1, p0, Lorg/threeten/bp/zone/StandardZoneRules;->savingsLocalTransitions:[Lorg/threeten/bp/LocalDateTime;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    aget-object v0, v0, v1

    invoke-virtual {p1, v0}, Lorg/threeten/bp/LocalDateTime;->isAfter(Lorg/threeten/bp/chrono/ChronoLocalDateTime;)Z

    move-result v0

    if-eqz v0, :cond_3b

    .line 336
    invoke-virtual {p1}, Lorg/threeten/bp/LocalDateTime;->getYear()I

    move-result v0

    invoke-direct {p0, v0}, Lorg/threeten/bp/zone/StandardZoneRules;->findTransitionArray(I)[Lorg/threeten/bp/zone/ZoneOffsetTransition;

    move-result-object v3

    .line 337
    .local v3, "transArray":[Lorg/threeten/bp/zone/ZoneOffsetTransition;
    const/4 v4, 0x0

    .line 338
    .local v4, "info":Ljava/lang/Object;
    move-object v5, v3

    .local v5, "arr$":[Lorg/threeten/bp/zone/ZoneOffsetTransition;
    array-length v6, v5

    .local v6, "len$":I
    const/4 v7, 0x0

    .local v7, "i$":I
    :goto_20
    if-ge v7, v6, :cond_3a

    aget-object v8, v5, v7

    .line 339
    .local v8, "trans":Lorg/threeten/bp/zone/ZoneOffsetTransition;
    invoke-direct {p0, p1, v8}, Lorg/threeten/bp/zone/StandardZoneRules;->findOffsetInfo(Lorg/threeten/bp/LocalDateTime;Lorg/threeten/bp/zone/ZoneOffsetTransition;)Ljava/lang/Object;

    move-result-object v4

    .line 340
    instance-of v0, v4, Lorg/threeten/bp/zone/ZoneOffsetTransition;

    if-nez v0, :cond_36

    invoke-virtual {v8}, Lorg/threeten/bp/zone/ZoneOffsetTransition;->getOffsetBefore()Lorg/threeten/bp/ZoneOffset;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_37

    .line 341
    :cond_36
    return-object v4

    .line 338
    .end local v8    # "trans":Lorg/threeten/bp/zone/ZoneOffsetTransition;
    :cond_37
    add-int/lit8 v7, v7, 0x1

    goto :goto_20

    .line 344
    .end local v5    # "arr$":[Lorg/threeten/bp/zone/ZoneOffsetTransition;
    .end local v6    # "len$":I
    .end local v7    # "i$":I
    :cond_3a
    return-object v4

    .line 348
    .end local v3    # "transArray":[Lorg/threeten/bp/zone/ZoneOffsetTransition;
    .end local v4    # "info":Ljava/lang/Object;
    :cond_3b
    iget-object v0, p0, Lorg/threeten/bp/zone/StandardZoneRules;->savingsLocalTransitions:[Lorg/threeten/bp/LocalDateTime;

    invoke-static {v0, p1}, Ljava/util/Arrays;->binarySearch([Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v3

    .line 349
    .local v3, "index":I
    const/4 v0, -0x1

    if-ne v3, v0, :cond_4a

    .line 351
    iget-object v0, p0, Lorg/threeten/bp/zone/StandardZoneRules;->wallOffsets:[Lorg/threeten/bp/ZoneOffset;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    return-object v0

    .line 353
    :cond_4a
    if-gez v3, :cond_50

    .line 355
    neg-int v0, v3

    add-int/lit8 v3, v0, -0x2

    goto :goto_69

    .line 356
    :cond_50
    iget-object v0, p0, Lorg/threeten/bp/zone/StandardZoneRules;->savingsLocalTransitions:[Lorg/threeten/bp/LocalDateTime;

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    if-ge v3, v0, :cond_69

    iget-object v0, p0, Lorg/threeten/bp/zone/StandardZoneRules;->savingsLocalTransitions:[Lorg/threeten/bp/LocalDateTime;

    aget-object v0, v0, v3

    iget-object v1, p0, Lorg/threeten/bp/zone/StandardZoneRules;->savingsLocalTransitions:[Lorg/threeten/bp/LocalDateTime;

    add-int/lit8 v2, v3, 0x1

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Lorg/threeten/bp/LocalDateTime;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_69

    .line 359
    add-int/lit8 v3, v3, 0x1

    .line 361
    :cond_69
    :goto_69
    and-int/lit8 v0, v3, 0x1

    if-nez v0, :cond_9b

    .line 363
    iget-object v0, p0, Lorg/threeten/bp/zone/StandardZoneRules;->savingsLocalTransitions:[Lorg/threeten/bp/LocalDateTime;

    aget-object v4, v0, v3

    .line 364
    .local v4, "dtBefore":Lorg/threeten/bp/LocalDateTime;
    iget-object v0, p0, Lorg/threeten/bp/zone/StandardZoneRules;->savingsLocalTransitions:[Lorg/threeten/bp/LocalDateTime;

    add-int/lit8 v1, v3, 0x1

    aget-object v5, v0, v1

    .line 365
    .local v5, "dtAfter":Lorg/threeten/bp/LocalDateTime;
    iget-object v0, p0, Lorg/threeten/bp/zone/StandardZoneRules;->wallOffsets:[Lorg/threeten/bp/ZoneOffset;

    div-int/lit8 v1, v3, 0x2

    aget-object v6, v0, v1

    .line 366
    .local v6, "offsetBefore":Lorg/threeten/bp/ZoneOffset;
    iget-object v0, p0, Lorg/threeten/bp/zone/StandardZoneRules;->wallOffsets:[Lorg/threeten/bp/ZoneOffset;

    div-int/lit8 v1, v3, 0x2

    add-int/lit8 v1, v1, 0x1

    aget-object v7, v0, v1

    .line 367
    .local v7, "offsetAfter":Lorg/threeten/bp/ZoneOffset;
    invoke-virtual {v7}, Lorg/threeten/bp/ZoneOffset;->getTotalSeconds()I

    move-result v0

    invoke-virtual {v6}, Lorg/threeten/bp/ZoneOffset;->getTotalSeconds()I

    move-result v1

    if-le v0, v1, :cond_95

    .line 369
    new-instance v0, Lorg/threeten/bp/zone/ZoneOffsetTransition;

    invoke-direct {v0, v4, v6, v7}, Lorg/threeten/bp/zone/ZoneOffsetTransition;-><init>(Lorg/threeten/bp/LocalDateTime;Lorg/threeten/bp/ZoneOffset;Lorg/threeten/bp/ZoneOffset;)V

    return-object v0

    .line 372
    :cond_95
    new-instance v0, Lorg/threeten/bp/zone/ZoneOffsetTransition;

    invoke-direct {v0, v5, v6, v7}, Lorg/threeten/bp/zone/ZoneOffsetTransition;-><init>(Lorg/threeten/bp/LocalDateTime;Lorg/threeten/bp/ZoneOffset;Lorg/threeten/bp/ZoneOffset;)V

    return-object v0

    .line 376
    .end local v4    # "dtBefore":Lorg/threeten/bp/LocalDateTime;
    .end local v5    # "dtAfter":Lorg/threeten/bp/LocalDateTime;
    .end local v6    # "offsetBefore":Lorg/threeten/bp/ZoneOffset;
    .end local v7    # "offsetAfter":Lorg/threeten/bp/ZoneOffset;
    :cond_9b
    iget-object v0, p0, Lorg/threeten/bp/zone/StandardZoneRules;->wallOffsets:[Lorg/threeten/bp/ZoneOffset;

    div-int/lit8 v1, v3, 0x2

    add-int/lit8 v1, v1, 0x1

    aget-object v0, v0, v1

    return-object v0
.end method

.method static readExternal(Ljava/io/DataInput;)Lorg/threeten/bp/zone/StandardZoneRules;
    .registers 16
    .param p0, "in"    # Ljava/io/DataInput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .line 245
    invoke-interface {p0}, Ljava/io/DataInput;->readInt()I

    move-result v6

    .line 246
    .local v6, "stdSize":I
    new-array v7, v6, [J

    .line 247
    .local v7, "stdTrans":[J
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_7
    if-ge v8, v6, :cond_12

    .line 248
    invoke-static {p0}, Lorg/threeten/bp/zone/Ser;->readEpochSec(Ljava/io/DataInput;)J

    move-result-wide v0

    aput-wide v0, v7, v8

    .line 247
    add-int/lit8 v8, v8, 0x1

    goto :goto_7

    .line 250
    .end local v8    # "i":I
    :cond_12
    add-int/lit8 v0, v6, 0x1

    new-array v8, v0, [Lorg/threeten/bp/ZoneOffset;

    .line 251
    .local v8, "stdOffsets":[Lorg/threeten/bp/ZoneOffset;
    const/4 v9, 0x0

    .local v9, "i":I
    :goto_17
    array-length v0, v8

    if-ge v9, v0, :cond_23

    .line 252
    invoke-static {p0}, Lorg/threeten/bp/zone/Ser;->readOffset(Ljava/io/DataInput;)Lorg/threeten/bp/ZoneOffset;

    move-result-object v0

    aput-object v0, v8, v9

    .line 251
    add-int/lit8 v9, v9, 0x1

    goto :goto_17

    .line 254
    .end local v9    # "i":I
    :cond_23
    invoke-interface {p0}, Ljava/io/DataInput;->readInt()I

    move-result v9

    .line 255
    .local v9, "savSize":I
    new-array v10, v9, [J

    .line 256
    .local v10, "savTrans":[J
    const/4 v11, 0x0

    .local v11, "i":I
    :goto_2a
    if-ge v11, v9, :cond_35

    .line 257
    invoke-static {p0}, Lorg/threeten/bp/zone/Ser;->readEpochSec(Ljava/io/DataInput;)J

    move-result-wide v0

    aput-wide v0, v10, v11

    .line 256
    add-int/lit8 v11, v11, 0x1

    goto :goto_2a

    .line 259
    .end local v11    # "i":I
    :cond_35
    add-int/lit8 v0, v9, 0x1

    new-array v11, v0, [Lorg/threeten/bp/ZoneOffset;

    .line 260
    .local v11, "savOffsets":[Lorg/threeten/bp/ZoneOffset;
    const/4 v12, 0x0

    .local v12, "i":I
    :goto_3a
    array-length v0, v11

    if-ge v12, v0, :cond_46

    .line 261
    invoke-static {p0}, Lorg/threeten/bp/zone/Ser;->readOffset(Ljava/io/DataInput;)Lorg/threeten/bp/ZoneOffset;

    move-result-object v0

    aput-object v0, v11, v12

    .line 260
    add-int/lit8 v12, v12, 0x1

    goto :goto_3a

    .line 263
    .end local v12    # "i":I
    :cond_46
    invoke-interface {p0}, Ljava/io/DataInput;->readByte()B

    move-result v12

    .line 264
    .local v12, "ruleSize":I
    new-array v13, v12, [Lorg/threeten/bp/zone/ZoneOffsetTransitionRule;

    .line 265
    .local v13, "rules":[Lorg/threeten/bp/zone/ZoneOffsetTransitionRule;
    const/4 v14, 0x0

    .local v14, "i":I
    :goto_4d
    if-ge v14, v12, :cond_58

    .line 266
    invoke-static {p0}, Lorg/threeten/bp/zone/ZoneOffsetTransitionRule;->readExternal(Ljava/io/DataInput;)Lorg/threeten/bp/zone/ZoneOffsetTransitionRule;

    move-result-object v0

    aput-object v0, v13, v14

    .line 265
    add-int/lit8 v14, v14, 0x1

    goto :goto_4d

    .line 268
    .end local v14    # "i":I
    :cond_58
    new-instance v0, Lorg/threeten/bp/zone/StandardZoneRules;

    move-object v1, v7

    move-object v2, v8

    move-object v3, v10

    move-object v4, v11

    move-object v5, v13

    invoke-direct/range {v0 .. v5}, Lorg/threeten/bp/zone/StandardZoneRules;-><init>([J[Lorg/threeten/bp/ZoneOffset;[J[Lorg/threeten/bp/ZoneOffset;[Lorg/threeten/bp/zone/ZoneOffsetTransitionRule;)V

    return-object v0
.end method

.method private writeReplace()Ljava/lang/Object;
    .registers 3

    .line 207
    new-instance v0, Lorg/threeten/bp/zone/Ser;

    const/4 v1, 0x1

    invoke-direct {v0, v1, p0}, Lorg/threeten/bp/zone/Ser;-><init>(BLjava/lang/Object;)V

    return-object v0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .registers 6
    .param p1, "obj"    # Ljava/lang/Object;

    .line 571
    if-ne p0, p1, :cond_4

    .line 572
    const/4 v0, 0x1

    return v0

    .line 574
    :cond_4
    instance-of v0, p1, Lorg/threeten/bp/zone/StandardZoneRules;

    if-eqz v0, :cond_41

    .line 575
    move-object v3, p1

    check-cast v3, Lorg/threeten/bp/zone/StandardZoneRules;

    .line 576
    .local v3, "other":Lorg/threeten/bp/zone/StandardZoneRules;
    iget-object v0, p0, Lorg/threeten/bp/zone/StandardZoneRules;->standardTransitions:[J

    iget-object v1, v3, Lorg/threeten/bp/zone/StandardZoneRules;->standardTransitions:[J

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([J[J)Z

    move-result v0

    if-eqz v0, :cond_3f

    iget-object v0, p0, Lorg/threeten/bp/zone/StandardZoneRules;->standardOffsets:[Lorg/threeten/bp/ZoneOffset;

    iget-object v1, v3, Lorg/threeten/bp/zone/StandardZoneRules;->standardOffsets:[Lorg/threeten/bp/ZoneOffset;

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3f

    iget-object v0, p0, Lorg/threeten/bp/zone/StandardZoneRules;->savingsInstantTransitions:[J

    iget-object v1, v3, Lorg/threeten/bp/zone/StandardZoneRules;->savingsInstantTransitions:[J

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([J[J)Z

    move-result v0

    if-eqz v0, :cond_3f

    iget-object v0, p0, Lorg/threeten/bp/zone/StandardZoneRules;->wallOffsets:[Lorg/threeten/bp/ZoneOffset;

    iget-object v1, v3, Lorg/threeten/bp/zone/StandardZoneRules;->wallOffsets:[Lorg/threeten/bp/ZoneOffset;

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3f

    iget-object v0, p0, Lorg/threeten/bp/zone/StandardZoneRules;->lastRules:[Lorg/threeten/bp/zone/ZoneOffsetTransitionRule;

    iget-object v1, v3, Lorg/threeten/bp/zone/StandardZoneRules;->lastRules:[Lorg/threeten/bp/zone/ZoneOffsetTransitionRule;

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3f

    const/4 v0, 0x1

    goto :goto_40

    :cond_3f
    const/4 v0, 0x0

    :goto_40
    return v0

    .line 582
    .end local v3    # "other":Lorg/threeten/bp/zone/StandardZoneRules;
    :cond_41
    instance-of v0, p1, Lorg/threeten/bp/zone/ZoneRules$Fixed;

    if-eqz v0, :cond_64

    .line 583
    invoke-virtual {p0}, Lorg/threeten/bp/zone/StandardZoneRules;->isFixedOffset()Z

    move-result v0

    if-eqz v0, :cond_62

    sget-object v0, Lorg/threeten/bp/Instant;->EPOCH:Lorg/threeten/bp/Instant;

    invoke-virtual {p0, v0}, Lorg/threeten/bp/zone/StandardZoneRules;->getOffset(Lorg/threeten/bp/Instant;)Lorg/threeten/bp/ZoneOffset;

    move-result-object v0

    move-object v1, p1

    check-cast v1, Lorg/threeten/bp/zone/ZoneRules$Fixed;

    sget-object v2, Lorg/threeten/bp/Instant;->EPOCH:Lorg/threeten/bp/Instant;

    invoke-virtual {v1, v2}, Lorg/threeten/bp/zone/ZoneRules$Fixed;->getOffset(Lorg/threeten/bp/Instant;)Lorg/threeten/bp/ZoneOffset;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/threeten/bp/ZoneOffset;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_62

    const/4 v0, 0x1

    goto :goto_63

    :cond_62
    const/4 v0, 0x0

    :goto_63
    return v0

    .line 585
    :cond_64
    const/4 v0, 0x0

    return v0
.end method

.method public getOffset(Lorg/threeten/bp/Instant;)Lorg/threeten/bp/ZoneOffset;
    .registers 11
    .param p1, "instant"    # Lorg/threeten/bp/Instant;

    .line 280
    invoke-virtual {p1}, Lorg/threeten/bp/Instant;->getEpochSecond()J

    move-result-wide v3

    .line 283
    .local v3, "epochSec":J
    iget-object v0, p0, Lorg/threeten/bp/zone/StandardZoneRules;->lastRules:[Lorg/threeten/bp/zone/ZoneOffsetTransitionRule;

    array-length v0, v0

    if-lez v0, :cond_43

    iget-object v0, p0, Lorg/threeten/bp/zone/StandardZoneRules;->savingsInstantTransitions:[J

    iget-object v1, p0, Lorg/threeten/bp/zone/StandardZoneRules;->savingsInstantTransitions:[J

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    aget-wide v0, v0, v1

    cmp-long v0, v3, v0

    if-lez v0, :cond_43

    .line 285
    iget-object v0, p0, Lorg/threeten/bp/zone/StandardZoneRules;->wallOffsets:[Lorg/threeten/bp/ZoneOffset;

    iget-object v1, p0, Lorg/threeten/bp/zone/StandardZoneRules;->wallOffsets:[Lorg/threeten/bp/ZoneOffset;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    aget-object v0, v0, v1

    invoke-direct {p0, v3, v4, v0}, Lorg/threeten/bp/zone/StandardZoneRules;->findYear(JLorg/threeten/bp/ZoneOffset;)I

    move-result v5

    .line 286
    .local v5, "year":I
    invoke-direct {p0, v5}, Lorg/threeten/bp/zone/StandardZoneRules;->findTransitionArray(I)[Lorg/threeten/bp/zone/ZoneOffsetTransition;

    move-result-object v6

    .line 287
    .local v6, "transArray":[Lorg/threeten/bp/zone/ZoneOffsetTransition;
    const/4 v7, 0x0

    .line 288
    .local v7, "trans":Lorg/threeten/bp/zone/ZoneOffsetTransition;
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_29
    array-length v0, v6

    if-ge v8, v0, :cond_3e

    .line 289
    aget-object v7, v6, v8

    .line 290
    invoke-virtual {v7}, Lorg/threeten/bp/zone/ZoneOffsetTransition;->toEpochSecond()J

    move-result-wide v0

    cmp-long v0, v3, v0

    if-gez v0, :cond_3b

    .line 291
    invoke-virtual {v7}, Lorg/threeten/bp/zone/ZoneOffsetTransition;->getOffsetBefore()Lorg/threeten/bp/ZoneOffset;

    move-result-object v0

    return-object v0

    .line 288
    :cond_3b
    add-int/lit8 v8, v8, 0x1

    goto :goto_29

    .line 294
    .end local v8    # "i":I
    :cond_3e
    invoke-virtual {v7}, Lorg/threeten/bp/zone/ZoneOffsetTransition;->getOffsetAfter()Lorg/threeten/bp/ZoneOffset;

    move-result-object v0

    return-object v0

    .line 298
    .end local v5    # "year":I
    .end local v6    # "transArray":[Lorg/threeten/bp/zone/ZoneOffsetTransition;
    .end local v7    # "trans":Lorg/threeten/bp/zone/ZoneOffsetTransition;
    :cond_43
    iget-object v0, p0, Lorg/threeten/bp/zone/StandardZoneRules;->savingsInstantTransitions:[J

    invoke-static {v0, v3, v4}, Ljava/util/Arrays;->binarySearch([JJ)I

    move-result v5

    .line 299
    .local v5, "index":I
    if-gez v5, :cond_4e

    .line 301
    neg-int v0, v5

    add-int/lit8 v5, v0, -0x2

    .line 303
    :cond_4e
    iget-object v0, p0, Lorg/threeten/bp/zone/StandardZoneRules;->wallOffsets:[Lorg/threeten/bp/ZoneOffset;

    add-int/lit8 v1, v5, 0x1

    aget-object v0, v0, v1

    return-object v0
.end method

.method public getStandardOffset(Lorg/threeten/bp/Instant;)Lorg/threeten/bp/ZoneOffset;
    .registers 7
    .param p1, "instant"    # Lorg/threeten/bp/Instant;

    .line 442
    invoke-virtual {p1}, Lorg/threeten/bp/Instant;->getEpochSecond()J

    move-result-wide v2

    .line 443
    .local v2, "epochSec":J
    iget-object v0, p0, Lorg/threeten/bp/zone/StandardZoneRules;->standardTransitions:[J

    invoke-static {v0, v2, v3}, Ljava/util/Arrays;->binarySearch([JJ)I

    move-result v4

    .line 444
    .local v4, "index":I
    if-gez v4, :cond_f

    .line 446
    neg-int v0, v4

    add-int/lit8 v4, v0, -0x2

    .line 448
    :cond_f
    iget-object v0, p0, Lorg/threeten/bp/zone/StandardZoneRules;->standardOffsets:[Lorg/threeten/bp/ZoneOffset;

    add-int/lit8 v1, v4, 0x1

    aget-object v0, v0, v1

    return-object v0
.end method

.method public getTransition(Lorg/threeten/bp/LocalDateTime;)Lorg/threeten/bp/zone/ZoneOffsetTransition;
    .registers 4
    .param p1, "localDateTime"    # Lorg/threeten/bp/LocalDateTime;

    .line 328
    invoke-direct {p0, p1}, Lorg/threeten/bp/zone/StandardZoneRules;->getOffsetInfo(Lorg/threeten/bp/LocalDateTime;)Ljava/lang/Object;

    move-result-object v1

    .line 329
    .local v1, "info":Ljava/lang/Object;
    instance-of v0, v1, Lorg/threeten/bp/zone/ZoneOffsetTransition;

    if-eqz v0, :cond_c

    move-object v0, v1

    check-cast v0, Lorg/threeten/bp/zone/ZoneOffsetTransition;

    goto :goto_d

    :cond_c
    const/4 v0, 0x0

    :goto_d
    return-object v0
.end method

.method public getValidOffsets(Lorg/threeten/bp/LocalDateTime;)Ljava/util/List;
    .registers 4
    .param p1, "localDateTime"    # Lorg/threeten/bp/LocalDateTime;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Lorg/threeten/bp/LocalDateTime;)Ljava/util/List<Lorg/threeten/bp/ZoneOffset;>;"
        }
    .end annotation

    .line 319
    invoke-direct {p0, p1}, Lorg/threeten/bp/zone/StandardZoneRules;->getOffsetInfo(Lorg/threeten/bp/LocalDateTime;)Ljava/lang/Object;

    move-result-object v1

    .line 320
    .local v1, "info":Ljava/lang/Object;
    instance-of v0, v1, Lorg/threeten/bp/zone/ZoneOffsetTransition;

    if-eqz v0, :cond_10

    .line 321
    move-object v0, v1

    check-cast v0, Lorg/threeten/bp/zone/ZoneOffsetTransition;

    invoke-virtual {v0}, Lorg/threeten/bp/zone/ZoneOffsetTransition;->getValidOffsets()Ljava/util/List;

    move-result-object v0

    return-object v0

    .line 323
    :cond_10
    move-object v0, v1

    check-cast v0, Lorg/threeten/bp/ZoneOffset;

    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public hashCode()I
    .registers 3

    .line 590
    iget-object v0, p0, Lorg/threeten/bp/zone/StandardZoneRules;->standardTransitions:[J

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([J)I

    move-result v0

    iget-object v1, p0, Lorg/threeten/bp/zone/StandardZoneRules;->standardOffsets:[Lorg/threeten/bp/ZoneOffset;

    invoke-static {v1}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v1

    xor-int/2addr v0, v1

    iget-object v1, p0, Lorg/threeten/bp/zone/StandardZoneRules;->savingsInstantTransitions:[J

    invoke-static {v1}, Ljava/util/Arrays;->hashCode([J)I

    move-result v1

    xor-int/2addr v0, v1

    iget-object v1, p0, Lorg/threeten/bp/zone/StandardZoneRules;->wallOffsets:[Lorg/threeten/bp/ZoneOffset;

    invoke-static {v1}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v1

    xor-int/2addr v0, v1

    iget-object v1, p0, Lorg/threeten/bp/zone/StandardZoneRules;->lastRules:[Lorg/threeten/bp/zone/ZoneOffsetTransitionRule;

    invoke-static {v1}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v1

    xor-int/2addr v0, v1

    return v0
.end method

.method public isDaylightSavings(Lorg/threeten/bp/Instant;)Z
    .registers 4
    .param p1, "instant"    # Lorg/threeten/bp/Instant;

    .line 460
    invoke-virtual {p0, p1}, Lorg/threeten/bp/zone/StandardZoneRules;->getStandardOffset(Lorg/threeten/bp/Instant;)Lorg/threeten/bp/ZoneOffset;

    move-result-object v0

    invoke-virtual {p0, p1}, Lorg/threeten/bp/zone/StandardZoneRules;->getOffset(Lorg/threeten/bp/Instant;)Lorg/threeten/bp/ZoneOffset;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/threeten/bp/ZoneOffset;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_10

    const/4 v0, 0x1

    goto :goto_11

    :cond_10
    const/4 v0, 0x0

    :goto_11
    return v0
.end method

.method public isFixedOffset()Z
    .registers 2

    .line 274
    iget-object v0, p0, Lorg/threeten/bp/zone/StandardZoneRules;->savingsInstantTransitions:[J

    array-length v0, v0

    if-nez v0, :cond_7

    const/4 v0, 0x1

    goto :goto_8

    :cond_7
    const/4 v0, 0x0

    :goto_8
    return v0
.end method

.method public isValidOffset(Lorg/threeten/bp/LocalDateTime;Lorg/threeten/bp/ZoneOffset;)Z
    .registers 4
    .param p1, "localDateTime"    # Lorg/threeten/bp/LocalDateTime;
    .param p2, "offset"    # Lorg/threeten/bp/ZoneOffset;

    .line 412
    invoke-virtual {p0, p1}, Lorg/threeten/bp/zone/StandardZoneRules;->getValidOffsets(Lorg/threeten/bp/LocalDateTime;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .registers 5

    .line 605
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "StandardZoneRules[currentStandardOffset="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/threeten/bp/zone/StandardZoneRules;->standardOffsets:[Lorg/threeten/bp/ZoneOffset;

    iget-object v2, p0, Lorg/threeten/bp/zone/StandardZoneRules;->standardOffsets:[Lorg/threeten/bp/ZoneOffset;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method writeExternal(Ljava/io/DataOutput;)V
    .registers 8
    .param p1, "out"    # Ljava/io/DataOutput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 217
    iget-object v0, p0, Lorg/threeten/bp/zone/StandardZoneRules;->standardTransitions:[J

    array-length v0, v0

    invoke-interface {p1, v0}, Ljava/io/DataOutput;->writeInt(I)V

    .line 218
    iget-object v1, p0, Lorg/threeten/bp/zone/StandardZoneRules;->standardTransitions:[J

    .local v1, "arr$":[J
    array-length v2, v1

    .local v2, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_a
    if-ge v3, v2, :cond_14

    aget-wide v4, v1, v3

    .line 219
    .local v4, "trans":J
    invoke-static {v4, v5, p1}, Lorg/threeten/bp/zone/Ser;->writeEpochSec(JLjava/io/DataOutput;)V

    .line 218
    .end local v4    # "trans":J
    add-int/lit8 v3, v3, 0x1

    goto :goto_a

    .line 221
    .end local v1    # "arr$":[J
    .end local v2    # "len$":I
    .end local v3    # "i$":I
    :cond_14
    iget-object v1, p0, Lorg/threeten/bp/zone/StandardZoneRules;->standardOffsets:[Lorg/threeten/bp/ZoneOffset;

    .local v1, "arr$":[Lorg/threeten/bp/ZoneOffset;
    array-length v2, v1

    .local v2, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_18
    if-ge v3, v2, :cond_22

    aget-object v4, v1, v3

    .line 222
    .local v4, "offset":Lorg/threeten/bp/ZoneOffset;
    invoke-static {v4, p1}, Lorg/threeten/bp/zone/Ser;->writeOffset(Lorg/threeten/bp/ZoneOffset;Ljava/io/DataOutput;)V

    .line 221
    .end local v4    # "offset":Lorg/threeten/bp/ZoneOffset;
    add-int/lit8 v3, v3, 0x1

    goto :goto_18

    .line 224
    .end local v1    # "arr$":[Lorg/threeten/bp/ZoneOffset;
    .end local v2    # "len$":I
    .end local v3    # "i$":I
    :cond_22
    iget-object v0, p0, Lorg/threeten/bp/zone/StandardZoneRules;->savingsInstantTransitions:[J

    array-length v0, v0

    invoke-interface {p1, v0}, Ljava/io/DataOutput;->writeInt(I)V

    .line 225
    iget-object v1, p0, Lorg/threeten/bp/zone/StandardZoneRules;->savingsInstantTransitions:[J

    .local v1, "arr$":[J
    array-length v2, v1

    .local v2, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_2c
    if-ge v3, v2, :cond_36

    aget-wide v4, v1, v3

    .line 226
    .local v4, "trans":J
    invoke-static {v4, v5, p1}, Lorg/threeten/bp/zone/Ser;->writeEpochSec(JLjava/io/DataOutput;)V

    .line 225
    .end local v4    # "trans":J
    add-int/lit8 v3, v3, 0x1

    goto :goto_2c

    .line 228
    .end local v1    # "arr$":[J
    .end local v2    # "len$":I
    .end local v3    # "i$":I
    :cond_36
    iget-object v1, p0, Lorg/threeten/bp/zone/StandardZoneRules;->wallOffsets:[Lorg/threeten/bp/ZoneOffset;

    .local v1, "arr$":[Lorg/threeten/bp/ZoneOffset;
    array-length v2, v1

    .local v2, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_3a
    if-ge v3, v2, :cond_44

    aget-object v4, v1, v3

    .line 229
    .local v4, "offset":Lorg/threeten/bp/ZoneOffset;
    invoke-static {v4, p1}, Lorg/threeten/bp/zone/Ser;->writeOffset(Lorg/threeten/bp/ZoneOffset;Ljava/io/DataOutput;)V

    .line 228
    .end local v4    # "offset":Lorg/threeten/bp/ZoneOffset;
    add-int/lit8 v3, v3, 0x1

    goto :goto_3a

    .line 231
    .end local v1    # "arr$":[Lorg/threeten/bp/ZoneOffset;
    .end local v2    # "len$":I
    .end local v3    # "i$":I
    :cond_44
    iget-object v0, p0, Lorg/threeten/bp/zone/StandardZoneRules;->lastRules:[Lorg/threeten/bp/zone/ZoneOffsetTransitionRule;

    array-length v0, v0

    invoke-interface {p1, v0}, Ljava/io/DataOutput;->writeByte(I)V

    .line 232
    iget-object v1, p0, Lorg/threeten/bp/zone/StandardZoneRules;->lastRules:[Lorg/threeten/bp/zone/ZoneOffsetTransitionRule;

    .local v1, "arr$":[Lorg/threeten/bp/zone/ZoneOffsetTransitionRule;
    array-length v2, v1

    .local v2, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_4e
    if-ge v3, v2, :cond_58

    aget-object v4, v1, v3

    .line 233
    .local v4, "rule":Lorg/threeten/bp/zone/ZoneOffsetTransitionRule;
    invoke-virtual {v4, p1}, Lorg/threeten/bp/zone/ZoneOffsetTransitionRule;->writeExternal(Ljava/io/DataOutput;)V

    .line 232
    .end local v4    # "rule":Lorg/threeten/bp/zone/ZoneOffsetTransitionRule;
    add-int/lit8 v3, v3, 0x1

    goto :goto_4e

    .line 235
    .end local v1    # "arr$":[Lorg/threeten/bp/zone/ZoneOffsetTransitionRule;
    .end local v2    # "len$":I
    .end local v3    # "i$":I
    :cond_58
    return-void
.end method

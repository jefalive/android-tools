.class public final Lbr/com/itau/security/token/til/Token;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static generateDeviceID(Landroid/content/Context;)Ljava/lang/String;
    .registers 2

    .line 19
    invoke-static {p0}, Lbr/com/itau/security/did/DID;->getDID(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getOTP(Landroid/content/Context;Ljava/lang/String;)Lbr/com/itau/security/token/model/OTPModel;
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lbr/com/itau/security/commons/exception/CommonSecurityException;,
            Lbr/com/itau/security/token/exception/UnavailableTokenException;
        }
    .end annotation

    .line 27
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lti/h;->a(Landroid/content/Context;Ljava/lang/String;Z)Lbr/com/itau/security/token/model/OTPModel;

    move-result-object v0

    return-object v0
.end method

.method public static getOfflineOTP(Landroid/content/Context;)Lbr/com/itau/security/token/model/OTPModel;
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lbr/com/itau/security/commons/exception/CommonSecurityException;,
            Lbr/com/itau/security/token/exception/UnavailableTokenException;
        }
    .end annotation

    .line 31
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Lti/h;->a(Landroid/content/Context;Ljava/lang/String;Z)Lbr/com/itau/security/token/model/OTPModel;

    move-result-object v0

    return-object v0
.end method

.method public static handleTokenAppData(Landroid/content/Context;Ljava/lang/String;)V
    .registers 2

    .line 23
    invoke-static {p0, p1}, Lti/h;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 24
    return-void
.end method

.method public static unlockAppToken(Landroid/content/Context;Ljava/lang/String;)Landroid/util/Pair;
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lbr/com/itau/security/commons/exception/CommonSecurityException;,
            Lbr/com/itau/security/token/exception/UnavailableTokenException;
        }
    .end annotation

    .line 35
    invoke-static {p0, p1}, Lti/h;->c(Landroid/content/Context;Ljava/lang/String;)Landroid/util/Pair;

    move-result-object v0

    return-object v0
.end method

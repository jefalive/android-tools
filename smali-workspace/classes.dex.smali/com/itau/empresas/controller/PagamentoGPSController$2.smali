.class Lcom/itau/empresas/controller/PagamentoGPSController$2;
.super Ljava/lang/Object;
.source "PagamentoGPSController.java"

# interfaces
.implements Lbr/com/itau/sdk/android/core/type/BackendCall;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/itau/empresas/controller/PagamentoGPSController;->consultaCnpjPagamentos()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/itau/empresas/controller/PagamentoGPSController;


# direct methods
.method constructor <init>(Lcom/itau/empresas/controller/PagamentoGPSController;)V
    .registers 2
    .param p1, "this$0"    # Lcom/itau/empresas/controller/PagamentoGPSController;

    .line 54
    iput-object p1, p0, Lcom/itau/empresas/controller/PagamentoGPSController$2;->this$0:Lcom/itau/empresas/controller/PagamentoGPSController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public callBackend()V
    .registers 8

    .line 57
    iget-object v0, p0, Lcom/itau/empresas/controller/PagamentoGPSController$2;->this$0:Lcom/itau/empresas/controller/PagamentoGPSController;

    invoke-virtual {v0}, Lcom/itau/empresas/controller/PagamentoGPSController;->api()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/api/Api;

    iget-object v1, p0, Lcom/itau/empresas/controller/PagamentoGPSController$2;->this$0:Lcom/itau/empresas/controller/PagamentoGPSController;

    iget-object v1, v1, Lcom/itau/empresas/controller/PagamentoGPSController;->app:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v1}, Lcom/itau/empresas/CustomApplication;->getCodigoOperador()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/itau/empresas/controller/PagamentoGPSController$2;->this$0:Lcom/itau/empresas/controller/PagamentoGPSController;

    iget-object v2, v2, Lcom/itau/empresas/controller/PagamentoGPSController;->app:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v2}, Lcom/itau/empresas/CustomApplication;->getContaSelecionada()Lcom/itau/empresas/api/model/ContaOperadorVO;

    move-result-object v2

    invoke-virtual {v2}, Lcom/itau/empresas/api/model/ContaOperadorVO;->getAgencia()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/itau/empresas/controller/PagamentoGPSController$2;->this$0:Lcom/itau/empresas/controller/PagamentoGPSController;

    iget-object v3, v3, Lcom/itau/empresas/controller/PagamentoGPSController;->app:Lcom/itau/empresas/CustomApplication;

    .line 58
    invoke-virtual {v3}, Lcom/itau/empresas/CustomApplication;->getContaSelecionada()Lcom/itau/empresas/api/model/ContaOperadorVO;

    move-result-object v3

    invoke-virtual {v3}, Lcom/itau/empresas/api/model/ContaOperadorVO;->getConta()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/itau/empresas/controller/PagamentoGPSController$2;->this$0:Lcom/itau/empresas/controller/PagamentoGPSController;

    iget-object v4, v4, Lcom/itau/empresas/controller/PagamentoGPSController;->app:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v4}, Lcom/itau/empresas/CustomApplication;->getContaSelecionada()Lcom/itau/empresas/api/model/ContaOperadorVO;

    move-result-object v4

    invoke-virtual {v4}, Lcom/itau/empresas/api/model/ContaOperadorVO;->getDac()Ljava/lang/String;

    move-result-object v4

    const-string v5, "Tributos"

    .line 57
    invoke-interface/range {v0 .. v5}, Lcom/itau/empresas/api/Api;->consultaCnpjPagadores(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v6

    .line 59
    .local v6, "list":Ljava/util/List;, "Ljava/util/List<Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/CnpjVO;>;"
    invoke-static {v6}, Lcom/itau/empresas/controller/BaseController;->post(Ljava/lang/Object;)V

    .line 60
    return-void
.end method

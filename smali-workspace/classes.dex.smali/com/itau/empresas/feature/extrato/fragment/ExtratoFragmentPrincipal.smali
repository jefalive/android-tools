.class public Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;
.super Lcom/itau/empresas/ui/fragment/BaseFragment;
.source "ExtratoFragmentPrincipal.java"

# interfaces
.implements Lcom/itau/empresas/feature/extrato/FiltroListener;
.implements Lcom/itau/empresas/SearchViewDelay$SearchViewDelayCallback;
.implements Lcom/itau/empresas/feature/extrato/ExtratoFilter$ListenerTextoBusca;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal$AtualizaFragmentoOnTabSelectedListener;
    }
.end annotation


# instance fields
.field application:Lcom/itau/empresas/CustomApplication;

.field extratoController:Lcom/itau/empresas/controller/ExtratoController;

.field private extratoVO:Lcom/itau/empresas/api/model/ExtratoVO;

.field private fragmentEntrada:Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragment;

.field private fragmentFuturo:Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragment;

.field private fragmentSaida:Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragment;

.field private fragmentTudo:Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragment;

.field itemMenuFiltro:Landroid/view/MenuItem;

.field itemMenuPesquisa:Landroid/view/MenuItem;

.field private listaExtratoFiltrada:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Lcom/itau/empresas/api/model/LancamentoVO;>;"
        }
    .end annotation
.end field

.field private listaLancamentos:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Lcom/itau/empresas/api/model/LancamentoVO;>;"
        }
    .end annotation
.end field

.field private listaLancamentosFuturos:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Lcom/itau/empresas/api/model/LancamentoVO;>;"
        }
    .end annotation
.end field

.field logoutHelper:Lcom/itau/empresas/feature/logout/LogoutHelper;

.field pagerExtrato:Landroid/support/v4/view/ViewPager;

.field private qtdeDiasFiltro:I

.field rlSaldoEmConta:Landroid/widget/RelativeLayout;

.field searchViewDelay:Lcom/itau/empresas/SearchViewDelay;

.field tabsExtrato:Landroid/support/design/widget/TabLayout;

.field private textoBusca:Ljava/lang/String;

.field textoSaldoConta:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .registers 2

    .line 57
    invoke-direct {p0}, Lcom/itau/empresas/ui/fragment/BaseFragment;-><init>()V

    .line 90
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->listaExtratoFiltrada:Ljava/util/List;

    .line 91
    const/4 v0, 0x7

    iput v0, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->qtdeDiasFiltro:I

    return-void
.end method

.method static synthetic access$100(Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;Ljava/lang/String;)V
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;
    .param p1, "x1"    # Ljava/lang/String;

    .line 57
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->carregarDados(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$200(Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;)Lcom/itau/empresas/api/model/ExtratoVO;
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;

    .line 57
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->extratoVO:Lcom/itau/empresas/api/model/ExtratoVO;

    return-object v0
.end method

.method static synthetic access$300(Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;)V
    .registers 1
    .param p0, "x0"    # Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;

    .line 57
    invoke-direct {p0}, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->trataDadosLancamentos()V

    return-void
.end method

.method static synthetic access$400(Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;)Ljava/util/List;
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;

    .line 57
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->listaLancamentos:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$500(Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;ILjava/util/List;)V
    .registers 3
    .param p0, "x0"    # Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;
    .param p1, "x1"    # I
    .param p2, "x2"    # Ljava/util/List;

    .line 57
    invoke-direct {p0, p1, p2}, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->exibirDadosNoFragmentSelecionado(ILjava/util/List;)V

    return-void
.end method

.method static synthetic access$600(Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;Landroid/support/design/widget/TabLayout$Tab;Z)V
    .registers 3
    .param p0, "x0"    # Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;
    .param p1, "x1"    # Landroid/support/design/widget/TabLayout$Tab;
    .param p2, "x2"    # Z

    .line 57
    invoke-direct {p0, p1, p2}, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->alterarStyleTab(Landroid/support/design/widget/TabLayout$Tab;Z)V

    return-void
.end method

.method private alterarStyleTab(Landroid/support/design/widget/TabLayout$Tab;Z)V
    .registers 8
    .param p1, "tab"    # Landroid/support/design/widget/TabLayout$Tab;
    .param p2, "isSelecionado"    # Z

    .line 142
    if-eqz p2, :cond_4

    const/4 v2, 0x1

    goto :goto_5

    :cond_4
    const/4 v2, 0x0

    .line 144
    .local v2, "typeFace":I
    :goto_5
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->tabsExtrato:Landroid/support/design/widget/TabLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/design/widget/TabLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 145
    invoke-virtual {p1}, Landroid/support/design/widget/TabLayout$Tab;->getPosition()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Landroid/widget/LinearLayout;

    .line 146
    .local v3, "tabLayout":Landroid/widget/LinearLayout;
    const/4 v0, 0x1

    invoke-virtual {v3, v0}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Landroid/widget/TextView;

    .line 147
    .local v4, "tabTextView":Landroid/widget/TextView;
    const/4 v0, 0x0

    invoke-virtual {v4, v0, v2}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 148
    return-void
.end method

.method private aoExpandirPesquisa()Landroid/support/v4/view/MenuItemCompat$OnActionExpandListener;
    .registers 2

    .line 378
    new-instance v0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal$2;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal$2;-><init>(Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;)V

    return-object v0
.end method

.method private cancelarPesquisa()V
    .registers 2

    .line 421
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->searchViewDelay:Lcom/itau/empresas/SearchViewDelay;

    if-eqz v0, :cond_9

    .line 422
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->searchViewDelay:Lcom/itau/empresas/SearchViewDelay;

    invoke-virtual {v0}, Lcom/itau/empresas/SearchViewDelay;->cancelSearch()V

    .line 424
    :cond_9
    return-void
.end method

.method private carregarDados(Ljava/lang/String;)V
    .registers 5
    .param p1, "periodo"    # Ljava/lang/String;

    .line 171
    invoke-virtual {p0}, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-static {v0}, Lcom/itau/empresas/ui/util/ProgressDialogUtils;->mostraDialogoDeProgresso(Landroid/support/v4/app/FragmentManager;)V

    .line 172
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->application:Lcom/itau/empresas/CustomApplication;

    invoke-static {v0}, Lcom/itau/empresas/ui/util/ViewUtils;->temConta(Lcom/itau/empresas/CustomApplication;)Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 173
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->extratoController:Lcom/itau/empresas/controller/ExtratoController;

    iget-object v1, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->application:Lcom/itau/empresas/CustomApplication;

    .line 174
    invoke-static {v1}, Lcom/itau/empresas/ui/util/ViewUtils;->getContaCompleta(Lcom/itau/empresas/CustomApplication;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Lcom/itau/empresas/controller/ExtratoController;->consultaLancamentos(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_25

    .line 176
    :cond_1c
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->logoutHelper:Lcom/itau/empresas/feature/logout/LogoutHelper;

    invoke-virtual {p0}, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/logout/LogoutHelper;->trataSessaoExpirada(Landroid/app/Activity;)V

    .line 178
    :goto_25
    return-void
.end method

.method private disparaEventoAnalytics(Z)V
    .registers 6
    .param p1, "abaFuturo"    # Z

    .line 427
    invoke-static {}, Lcom/itau/empresas/feature/analytics/AnalyticsHelper;->getInstance()Lcom/itau/empresas/feature/analytics/AnalyticsHelper;

    move-result-object v3

    .line 429
    .local v3, "analyticsHelper":Lcom/itau/empresas/feature/analytics/AnalyticsHelper;
    if-eqz p1, :cond_27

    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->listaLancamentosFuturos:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_27

    .line 430
    .line 431
    const v0, 0x7f0700e2

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 432
    const v1, 0x7f0700c8

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 433
    const v2, 0x7f0700c3

    invoke-virtual {p0, v2}, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 430
    invoke-virtual {v3, v0, v1, v2}, Lcom/itau/empresas/feature/analytics/AnalyticsHelper;->eventoAdobeState(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 434
    return-void

    .line 436
    :cond_27
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->listaLancamentos:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_47

    .line 437
    .line 438
    const v0, 0x7f0700df

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 439
    const v1, 0x7f0700c8

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 440
    const v2, 0x7f0700c3

    invoke-virtual {p0, v2}, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 437
    invoke-virtual {v3, v0, v1, v2}, Lcom/itau/empresas/feature/analytics/AnalyticsHelper;->eventoAdobeState(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 442
    :cond_47
    return-void
.end method

.method private exibirDadosNoFragmentSelecionado(ILjava/util/List;)V
    .registers 6
    .param p1, "tabPosition"    # I
    .param p2, "listaExtrato"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ILjava/util/List<Lcom/itau/empresas/api/model/LancamentoVO;>;)V"
        }
    .end annotation

    .line 238
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->application:Lcom/itau/empresas/CustomApplication;

    invoke-static {v0}, Lcom/itau/empresas/feature/extrato/util/ExtratoUtil;->obterOrdenacao(Lcom/itau/empresas/CustomApplication;)I

    move-result v2

    .line 239
    .local v2, "ordem":I
    if-nez p2, :cond_9

    .line 240
    return-void

    .line 242
    :cond_9
    invoke-direct {p0}, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->trataDadosLancamentos()V

    .line 243
    packed-switch p1, :pswitch_data_90

    goto/16 :goto_7a

    .line 245
    :pswitch_11
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->fragmentEntrada:Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragment;

    const-string v1, "C"

    invoke-direct {p0, v1}, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->filtrarLancamentosPorTipoOperacao(Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1, v2}, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragment;->setDados(Ljava/util/List;I)V

    .line 246
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->fragmentEntrada:Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragment;

    iget-object v1, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->textoBusca:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragment;->setTextoBuscado(Ljava/lang/String;)V

    .line 247
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->fragmentEntrada:Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragment;

    iget-object v1, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->textoBusca:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragment;->filtrar(Ljava/lang/String;)V

    .line 248
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->disparaEventoAnalytics(Z)V

    .line 249
    goto/16 :goto_8f

    .line 251
    :pswitch_30
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->fragmentSaida:Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragment;

    const-string v1, "D"

    invoke-direct {p0, v1}, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->filtrarLancamentosPorTipoOperacao(Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1, v2}, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragment;->setDados(Ljava/util/List;I)V

    .line 252
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->fragmentSaida:Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragment;

    iget-object v1, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->textoBusca:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragment;->setTextoBuscado(Ljava/lang/String;)V

    .line 253
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->fragmentSaida:Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragment;

    iget-object v1, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->textoBusca:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragment;->filtrar(Ljava/lang/String;)V

    .line 254
    goto :goto_8f

    .line 256
    :pswitch_4a
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->fragmentFuturo:Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragment;

    iget-object v1, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->listaLancamentosFuturos:Ljava/util/List;

    invoke-virtual {v0, v1, v2}, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragment;->setDados(Ljava/util/List;I)V

    .line 257
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->fragmentFuturo:Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragment;

    iget-object v1, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->textoBusca:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragment;->setTextoBuscado(Ljava/lang/String;)V

    .line 258
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->fragmentFuturo:Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragment;

    iget-object v1, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->textoBusca:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragment;->filtrar(Ljava/lang/String;)V

    .line 259
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->disparaEventoAnalytics(Z)V

    .line 260
    goto :goto_8f

    .line 262
    :pswitch_64
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->fragmentTudo:Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragment;

    iget-object v1, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->listaLancamentos:Ljava/util/List;

    invoke-virtual {v0, v1, v2}, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragment;->setDados(Ljava/util/List;I)V

    .line 263
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->fragmentTudo:Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragment;

    iget-object v1, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->textoBusca:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragment;->setTextoBuscado(Ljava/lang/String;)V

    .line 264
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->fragmentTudo:Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragment;

    iget-object v1, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->textoBusca:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragment;->filtrar(Ljava/lang/String;)V

    .line 265
    goto :goto_8f

    .line 267
    :goto_7a
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->fragmentTudo:Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragment;

    iget-object v1, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->listaLancamentos:Ljava/util/List;

    invoke-virtual {v0, v1, v2}, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragment;->setDados(Ljava/util/List;I)V

    .line 268
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->fragmentTudo:Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragment;

    iget-object v1, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->textoBusca:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragment;->setTextoBuscado(Ljava/lang/String;)V

    .line 269
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->fragmentTudo:Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragment;

    iget-object v1, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->textoBusca:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragment;->filtrar(Ljava/lang/String;)V

    .line 271
    :goto_8f
    return-void

    :pswitch_data_90
    .packed-switch 0x0
        :pswitch_64
        :pswitch_11
        :pswitch_30
        :pswitch_4a
    .end packed-switch
.end method

.method private exibirSaldoEmConta(Lcom/itau/empresas/api/model/SaldoVO;)V
    .registers 8
    .param p1, "saldo"    # Lcom/itau/empresas/api/model/SaldoVO;

    .line 218
    const-wide/16 v3, 0x0

    .line 220
    .local v3, "saldoDisponivelSaque":D
    if-eqz p1, :cond_12

    invoke-virtual {p1}, Lcom/itau/empresas/api/model/SaldoVO;->getSaldoDisponivelSaque()Ljava/lang/Double;

    move-result-object v0

    if-eqz v0, :cond_12

    .line 221
    invoke-virtual {p1}, Lcom/itau/empresas/api/model/SaldoVO;->getSaldoDisponivelSaque()Ljava/lang/Double;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v3

    .line 224
    .line 225
    :cond_12
    const/4 v0, 0x1

    invoke-static {v3, v4, v0}, Lcom/itau/empresas/ui/util/ValorMonetarioUtils;->formataValorDecimalParaMonetario(DZ)Ljava/lang/String;

    move-result-object v5

    .line 227
    .local v5, "saldoEmConta":Ljava/lang/String;
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->textoSaldoConta:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 229
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->textoSaldoConta:Landroid/widget/TextView;

    invoke-direct {p0, v3, v4}, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->getCorItens(D)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 231
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->textoSaldoConta:Landroid/widget/TextView;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object v5, v1, v2

    .line 232
    const v2, 0x7f07009f

    invoke-virtual {p0, v2, v1}, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 231
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 233
    return-void
.end method

.method private filtrarLancamentosPorTipoOperacao(Ljava/lang/String;)Ljava/util/List;
    .registers 6
    .param p1, "tipoOperacao"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/lang/String;)Ljava/util/List<Lcom/itau/empresas/api/model/LancamentoVO;>;"
        }
    .end annotation

    .line 275
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 276
    .local v1, "listaLancamentosPorTipoOperacao":Ljava/util/List;, "Ljava/util/List<Lcom/itau/empresas/api/model/LancamentoVO;>;"
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->listaLancamentos:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_b
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2c

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/itau/empresas/api/model/LancamentoVO;

    .line 277
    .local v3, "lancamentoVO":Lcom/itau/empresas/api/model/LancamentoVO;
    invoke-virtual {v3}, Lcom/itau/empresas/api/model/LancamentoVO;->getIndicadorOperacao()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2b

    invoke-virtual {v3}, Lcom/itau/empresas/api/model/LancamentoVO;->getIndicadorOperacao()Ljava/lang/String;

    move-result-object v0

    .line 278
    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2b

    .line 279
    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 281
    .end local v3    # "lancamentoVO":Lcom/itau/empresas/api/model/LancamentoVO;
    :cond_2b
    goto :goto_b

    .line 282
    :cond_2c
    return-object v1
.end method

.method private getCorItens(D)I
    .registers 6
    .param p1, "valor"    # D

    .line 214
    invoke-virtual {p0}, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const-wide/16 v1, 0x0

    cmpg-double v1, p1, v1

    if-gez v1, :cond_e

    const v1, 0x7f0c0151

    goto :goto_11

    :cond_e
    const v1, 0x7f0c014c

    :goto_11
    invoke-static {v0, v1}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v0

    return v0
.end method

.method private inicializarTabAcessibilidade()V
    .registers 6

    .line 131
    const/4 v4, 0x0

    .local v4, "tabIndex":I
    :goto_1
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->tabsExtrato:Landroid/support/design/widget/TabLayout;

    invoke-virtual {v0}, Landroid/support/design/widget/TabLayout;->getTabCount()I

    move-result v0

    if-ge v4, v0, :cond_21

    .line 132
    invoke-virtual {p0}, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->tabsExtrato:Landroid/support/design/widget/TabLayout;

    .line 133
    invoke-virtual {v1, v4}, Landroid/support/design/widget/TabLayout;->getTabAt(I)Landroid/support/design/widget/TabLayout$Tab;

    move-result-object v1

    add-int/lit8 v2, v4, 0x1

    iget-object v3, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->tabsExtrato:Landroid/support/design/widget/TabLayout;

    invoke-virtual {v3}, Landroid/support/design/widget/TabLayout;->getTabCount()I

    move-result v3

    .line 132
    invoke-static {v0, v1, v2, v3}, Lcom/itau/empresas/ui/util/acessibilidade/AcessibilidadeUtils;->aplicaAcessibilidadeTabLayout(Landroid/content/Context;Landroid/support/design/widget/TabLayout$Tab;II)V

    .line 131
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 135
    .end local v4    # "tabIndex":I
    :cond_21
    return-void
.end method

.method private inicializarTabListener()V
    .registers 4

    .line 138
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->tabsExtrato:Landroid/support/design/widget/TabLayout;

    new-instance v1, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal$AtualizaFragmentoOnTabSelectedListener;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal$AtualizaFragmentoOnTabSelectedListener;-><init>(Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal$1;)V

    invoke-virtual {v0, v1}, Landroid/support/design/widget/TabLayout;->addOnTabSelectedListener(Landroid/support/design/widget/TabLayout$OnTabSelectedListener;)V

    .line 139
    return-void
.end method

.method private pesquisarExtrato(Ljava/lang/String;)V
    .registers 3
    .param p1, "texto"    # Ljava/lang/String;

    .line 397
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->pagerExtrato:Landroid/support/v4/view/ViewPager;

    if-nez v0, :cond_5

    .line 398
    return-void

    .line 401
    :cond_5
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->pagerExtrato:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v0

    packed-switch v0, :pswitch_data_2e

    goto :goto_27

    .line 403
    :pswitch_f
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->fragmentTudo:Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragment;

    invoke-virtual {v0, p1}, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragment;->filtrar(Ljava/lang/String;)V

    .line 404
    goto :goto_2c

    .line 406
    :pswitch_15
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->fragmentEntrada:Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragment;

    invoke-virtual {v0, p1}, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragment;->filtrar(Ljava/lang/String;)V

    .line 407
    goto :goto_2c

    .line 409
    :pswitch_1b
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->fragmentSaida:Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragment;

    invoke-virtual {v0, p1}, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragment;->filtrar(Ljava/lang/String;)V

    .line 410
    goto :goto_2c

    .line 412
    :pswitch_21
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->fragmentFuturo:Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragment;

    invoke-virtual {v0, p1}, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragment;->filtrar(Ljava/lang/String;)V

    .line 413
    goto :goto_2c

    .line 415
    :goto_27
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->fragmentTudo:Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragment;

    invoke-virtual {v0, p1}, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragment;->filtrar(Ljava/lang/String;)V

    .line 418
    :goto_2c
    return-void

    nop

    :pswitch_data_2e
    .packed-switch 0x0
        :pswitch_f
        :pswitch_15
        :pswitch_1b
        :pswitch_21
    .end packed-switch
.end method

.method private setupPagerExtrato()V
    .registers 5

    .line 151
    new-instance v3, Lcom/itau/empresas/feature/extrato/adapter/ExtratoPagerAdapter;

    invoke-virtual {p0}, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-direct {v3, v0}, Lcom/itau/empresas/feature/extrato/adapter/ExtratoPagerAdapter;-><init>(Landroid/support/v4/app/FragmentManager;)V

    .line 153
    .local v3, "pagerAdapter":Lcom/itau/empresas/feature/extrato/adapter/ExtratoPagerAdapter;
    const v0, 0x7f0704dd

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->fragmentTudo:Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragment;

    invoke-virtual {v3, v0, v1}, Lcom/itau/empresas/feature/extrato/adapter/ExtratoPagerAdapter;->adicionarFragmento(Ljava/lang/String;Landroid/support/v4/app/Fragment;)V

    .line 155
    const v0, 0x7f0704da

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->fragmentEntrada:Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragment;

    invoke-virtual {v3, v0, v1}, Lcom/itau/empresas/feature/extrato/adapter/ExtratoPagerAdapter;->adicionarFragmento(Ljava/lang/String;Landroid/support/v4/app/Fragment;)V

    .line 157
    const v0, 0x7f0704dc

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->fragmentSaida:Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragment;

    invoke-virtual {v3, v0, v1}, Lcom/itau/empresas/feature/extrato/adapter/ExtratoPagerAdapter;->adicionarFragmento(Ljava/lang/String;Landroid/support/v4/app/Fragment;)V

    .line 159
    const v0, 0x7f0704db

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->fragmentFuturo:Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragment;

    invoke-virtual {v3, v0, v1}, Lcom/itau/empresas/feature/extrato/adapter/ExtratoPagerAdapter;->adicionarFragmento(Ljava/lang/String;Landroid/support/v4/app/Fragment;)V

    .line 162
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->pagerExtrato:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, v3}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 163
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->pagerExtrato:Landroid/support/v4/view/ViewPager;

    new-instance v1, Landroid/support/design/widget/TabLayout$TabLayoutOnPageChangeListener;

    iget-object v2, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->tabsExtrato:Landroid/support/design/widget/TabLayout;

    invoke-direct {v1, v2}, Landroid/support/design/widget/TabLayout$TabLayoutOnPageChangeListener;-><init>(Landroid/support/design/widget/TabLayout;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->addOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    .line 166
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->tabsExtrato:Landroid/support/design/widget/TabLayout;

    iget-object v1, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->pagerExtrato:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, v1}, Landroid/support/design/widget/TabLayout;->setupWithViewPager(Landroid/support/v4/view/ViewPager;)V

    .line 168
    return-void
.end method

.method private trataDadosLancamentos()V
    .registers 2

    .line 195
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->extratoVO:Lcom/itau/empresas/api/model/ExtratoVO;

    if-eqz v0, :cond_14

    .line 196
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->extratoVO:Lcom/itau/empresas/api/model/ExtratoVO;

    invoke-virtual {v0}, Lcom/itau/empresas/api/model/ExtratoVO;->getLancamentos()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->listaLancamentos:Ljava/util/List;

    .line 197
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->extratoVO:Lcom/itau/empresas/api/model/ExtratoVO;

    invoke-virtual {v0}, Lcom/itau/empresas/api/model/ExtratoVO;->getLancamentosFuturos()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->listaLancamentosFuturos:Ljava/util/List;

    .line 200
    :cond_14
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->listaExtratoFiltrada:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_20

    .line 201
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->listaExtratoFiltrada:Ljava/util/List;

    iput-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->listaLancamentos:Ljava/util/List;

    .line 204
    :cond_20
    return-void
.end method


# virtual methods
.method afterInject()V
    .registers 2

    .line 108
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->setHasOptionsMenu(Z)V

    .line 109
    return-void
.end method

.method afterViews()V
    .registers 2

    .line 113
    const/4 v0, 0x0

    invoke-static {v0, p0, p0}, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragment;->novaInstancia(ZLcom/itau/empresas/feature/extrato/FiltroListener;Lcom/itau/empresas/feature/extrato/ExtratoFilter$ListenerTextoBusca;)Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragment;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->fragmentTudo:Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragment;

    .line 115
    const/4 v0, 0x0

    invoke-static {v0, p0, p0}, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragment;->novaInstancia(ZLcom/itau/empresas/feature/extrato/FiltroListener;Lcom/itau/empresas/feature/extrato/ExtratoFilter$ListenerTextoBusca;)Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragment;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->fragmentEntrada:Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragment;

    .line 117
    const/4 v0, 0x0

    invoke-static {v0, p0, p0}, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragment;->novaInstancia(ZLcom/itau/empresas/feature/extrato/FiltroListener;Lcom/itau/empresas/feature/extrato/ExtratoFilter$ListenerTextoBusca;)Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragment;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->fragmentSaida:Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragment;

    .line 119
    const/4 v0, 0x1

    invoke-static {v0, p0, p0}, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragment;->novaInstancia(ZLcom/itau/empresas/feature/extrato/FiltroListener;Lcom/itau/empresas/feature/extrato/ExtratoFilter$ListenerTextoBusca;)Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragment;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->fragmentFuturo:Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragment;

    .line 121
    invoke-direct {p0}, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->setupPagerExtrato()V

    .line 123
    invoke-direct {p0}, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->inicializarTabListener()V

    .line 125
    invoke-direct {p0}, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->inicializarTabAcessibilidade()V

    .line 127
    iget v0, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->qtdeDiasFiltro:I

    invoke-static {v0}, Lcom/itau/empresas/ui/util/DataEHoraUtils;->subtraiDiasDeHoje(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->carregarDados(Ljava/lang/String;)V

    .line 128
    return-void
.end method

.method public aoClicaNoFiltro()V
    .registers 1

    .line 363
    invoke-virtual {p0}, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->exibirDialogDeFiltro()V

    .line 364
    return-void
.end method

.method public exibirDialogDeFiltro()V
    .registers 4

    .line 304
    invoke-static {}, Lcom/itau/empresas/feature/extrato/fragment/FiltroExtratoDialog_;->builder()Lcom/itau/empresas/feature/extrato/fragment/FiltroExtratoDialog_$FragmentBuilder_;

    move-result-object v0

    iget v1, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->qtdeDiasFiltro:I

    .line 305
    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/extrato/fragment/FiltroExtratoDialog_$FragmentBuilder_;->quantidadeDias(I)Lcom/itau/empresas/feature/extrato/fragment/FiltroExtratoDialog_$FragmentBuilder_;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/feature/extrato/fragment/FiltroExtratoDialog_$FragmentBuilder_;->build()Lcom/itau/empresas/feature/extrato/fragment/FiltroExtratoDialog;

    move-result-object v2

    .line 307
    .local v2, "filtroExtratoDialog":Lcom/itau/empresas/feature/extrato/fragment/FiltroExtratoDialog;
    invoke-virtual {p0}, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/feature/home/HomeLogadaActivity;

    invoke-virtual {v2, v0}, Lcom/itau/empresas/feature/extrato/fragment/FiltroExtratoDialog;->show(Landroid/support/v7/app/AppCompatActivity;)Lcom/itau/empresas/ui/dialog/BaseDialog;

    .line 308
    new-instance v0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal$1;

    invoke-direct {v0, p0, v2}, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal$1;-><init>(Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;Lcom/itau/empresas/feature/extrato/fragment/FiltroExtratoDialog;)V

    invoke-virtual {v2, v0}, Lcom/itau/empresas/feature/extrato/fragment/FiltroExtratoDialog;->setFiltroClickListener(Landroid/view/View$OnClickListener;)Lcom/itau/empresas/feature/extrato/fragment/FiltroExtratoDialog;

    .line 325
    return-void
.end method

.method public getQtdeDiasFiltro()I
    .registers 2

    .line 286
    iget v0, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->qtdeDiasFiltro:I

    return v0
.end method

.method onClickSaldoConta()V
    .registers 2

    .line 300
    invoke-virtual {p0}, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity;->intent(Landroid/content/Context;)Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity_$IntentBuilder_;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity_$IntentBuilder_;->start()Lorg/androidannotations/api/builder/PostActivityStarter;

    .line 301
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .registers 6
    .param p1, "menu"    # Landroid/view/Menu;
    .param p2, "inflater"    # Landroid/view/MenuInflater;

    .line 329
    invoke-interface {p1}, Landroid/view/Menu;->clear()V

    .line 331
    const/high16 v0, 0x7f0f0000

    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 333
    const v0, 0x7f0e06c6

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->itemMenuPesquisa:Landroid/view/MenuItem;

    .line 334
    const v0, 0x7f0e06c5

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->itemMenuFiltro:Landroid/view/MenuItem;

    .line 336
    new-instance v0, Lcom/itau/empresas/SearchViewDelay;

    invoke-direct {v0, p0}, Lcom/itau/empresas/SearchViewDelay;-><init>(Lcom/itau/empresas/SearchViewDelay$SearchViewDelayCallback;)V

    iput-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->searchViewDelay:Lcom/itau/empresas/SearchViewDelay;

    .line 338
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->itemMenuPesquisa:Landroid/view/MenuItem;

    invoke-static {v0}, Landroid/support/v4/view/MenuItemCompat;->getActionView(Landroid/view/MenuItem;)Landroid/view/View;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Landroid/support/v7/widget/SearchView;

    .line 339
    .local v2, "searchView":Landroid/support/v7/widget/SearchView;
    const v0, 0x7f07020b

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/support/v7/widget/SearchView;->setQueryHint(Ljava/lang/CharSequence;)V

    .line 340
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->searchViewDelay:Lcom/itau/empresas/SearchViewDelay;

    invoke-virtual {v2, v0}, Landroid/support/v7/widget/SearchView;->setOnQueryTextListener(Landroid/support/v7/widget/SearchView$OnQueryTextListener;)V

    .line 342
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->itemMenuPesquisa:Landroid/view/MenuItem;

    invoke-direct {p0}, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->aoExpandirPesquisa()Landroid/support/v4/view/MenuItemCompat$OnActionExpandListener;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/support/v4/view/MenuItemCompat;->setOnActionExpandListener(Landroid/view/MenuItem;Landroid/support/v4/view/MenuItemCompat$OnActionExpandListener;)Landroid/view/MenuItem;

    .line 344
    invoke-super {p0, p1, p2}, Lcom/itau/empresas/ui/fragment/BaseFragment;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 345
    return-void
.end method

.method public onEventMainThread(I)V
    .registers 3
    .param p1, "qtdeDiasFiltro"    # I

    .line 207
    iput p1, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->qtdeDiasFiltro:I

    .line 209
    invoke-static {p1}, Lcom/itau/empresas/ui/util/DataEHoraUtils;->subtraiDiasDeHoje(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->carregarDados(Ljava/lang/String;)V

    .line 210
    return-void
.end method

.method public onEventMainThread(Lcom/itau/empresas/api/model/ExtratoVO;)V
    .registers 5
    .param p1, "extratoVO"    # Lcom/itau/empresas/api/model/ExtratoVO;

    .line 181
    invoke-virtual {p0}, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-static {v0}, Lcom/itau/empresas/ui/util/ProgressDialogUtils;->escondeDialogoDeProgresso(Landroid/support/v4/app/FragmentManager;)V

    .line 182
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->listaLancamentosFuturos:Ljava/util/List;

    .line 183
    if-nez p1, :cond_11

    .line 184
    return-void

    .line 186
    :cond_11
    iput-object p1, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->extratoVO:Lcom/itau/empresas/api/model/ExtratoVO;

    .line 187
    invoke-virtual {p1}, Lcom/itau/empresas/api/model/ExtratoVO;->getLancamentos()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->listaLancamentos:Ljava/util/List;

    .line 188
    invoke-virtual {p1}, Lcom/itau/empresas/api/model/ExtratoVO;->getSaldo()Lcom/itau/empresas/api/model/SaldoVO;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->exibirSaldoEmConta(Lcom/itau/empresas/api/model/SaldoVO;)V

    .line 189
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->fragmentTudo:Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragment;

    invoke-virtual {p1}, Lcom/itau/empresas/api/model/ExtratoVO;->getLancamentos()Ljava/util/List;

    move-result-object v1

    iget-object v2, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->application:Lcom/itau/empresas/CustomApplication;

    invoke-static {v2}, Lcom/itau/empresas/feature/extrato/util/ExtratoUtil;->obterOrdenacao(Lcom/itau/empresas/CustomApplication;)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragment;->setDados(Ljava/util/List;I)V

    .line 190
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->tabsExtrato:Landroid/support/design/widget/TabLayout;

    invoke-virtual {v0}, Landroid/support/design/widget/TabLayout;->getSelectedTabPosition()I

    move-result v0

    iget-object v1, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->listaLancamentos:Ljava/util/List;

    invoke-direct {p0, v0, v1}, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->exibirDadosNoFragmentSelecionado(ILjava/util/List;)V

    .line 192
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .registers 4
    .param p1, "item"    # Landroid/view/MenuItem;

    .line 350
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    .line 352
    .local v1, "itemId":I
    const v0, 0x7f0e06c6

    if-ne v1, v0, :cond_b

    .line 353
    const/4 v0, 0x1

    return v0

    .line 354
    :cond_b
    const v0, 0x7f0e06c5

    if-ne v1, v0, :cond_15

    .line 355
    invoke-virtual {p0}, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->aoClicaNoFiltro()V

    .line 356
    const/4 v0, 0x1

    return v0

    .line 358
    :cond_15
    const/4 v0, 0x0

    return v0
.end method

.method public onPause()V
    .registers 1

    .line 102
    invoke-super {p0}, Lcom/itau/empresas/ui/fragment/BaseFragment;->onPause()V

    .line 103
    invoke-direct {p0}, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->cancelarPesquisa()V

    .line 104
    return-void
.end method

.method public opkeysConsumidas()Ljava/util/List;
    .registers 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Ljava/lang/String;>;"
        }
    .end annotation

    .line 295
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "extrato"

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const-string v1, "saldo"

    const/4 v2, 0x1

    aput-object v1, v0, v2

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public publicarResultados(Ljava/lang/String;)V
    .registers 2
    .param p1, "text"    # Ljava/lang/String;

    .line 368
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->pesquisarExtrato(Ljava/lang/String;)V

    .line 369
    return-void
.end method

.method public setQtdeDiasFiltro(I)V
    .registers 2
    .param p1, "qtdeDiasFiltro"    # I

    .line 290
    iput p1, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->qtdeDiasFiltro:I

    .line 291
    return-void
.end method

.method public textoDaBusca(Ljava/lang/String;)V
    .registers 2
    .param p1, "textoBuscado"    # Ljava/lang/String;

    .line 373
    iput-object p1, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->textoBusca:Ljava/lang/String;

    .line 375
    return-void
.end method

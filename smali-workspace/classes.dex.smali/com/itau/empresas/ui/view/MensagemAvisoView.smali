.class public Lcom/itau/empresas/ui/view/MensagemAvisoView;
.super Landroid/widget/LinearLayout;
.source "MensagemAvisoView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/itau/empresas/ui/view/MensagemAvisoView$Estado;
    }
.end annotation


# instance fields
.field corCinza:I

.field corVerde:I

.field ll_view_mensagem_aviso:Landroid/widget/LinearLayout;

.field texto_aviso:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 2
    .param p1, "context"    # Landroid/content/Context;

    .line 36
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 37
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .line 40
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 41
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .line 44
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 45
    return-void
.end method


# virtual methods
.method public setBackgroundColor(I)V
    .registers 3
    .param p1, "color"    # I

    .line 75
    iget-object v0, p0, Lcom/itau/empresas/ui/view/MensagemAvisoView;->ll_view_mensagem_aviso:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->setBackgroundColor(I)V

    .line 76
    return-void
.end method

.method public setMensagem(Lcom/itau/empresas/ui/view/MensagemAvisoView$Estado;I)V
    .registers 4
    .param p1, "estado"    # Lcom/itau/empresas/ui/view/MensagemAvisoView$Estado;
    .param p2, "resMensagem"    # I

    .line 54
    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/MensagemAvisoView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/itau/empresas/ui/view/MensagemAvisoView;->setMensagem(Lcom/itau/empresas/ui/view/MensagemAvisoView$Estado;Ljava/lang/String;)V

    .line 55
    return-void
.end method

.method public setMensagem(Lcom/itau/empresas/ui/view/MensagemAvisoView$Estado;Ljava/lang/String;)V
    .registers 5
    .param p1, "estado"    # Lcom/itau/empresas/ui/view/MensagemAvisoView$Estado;
    .param p2, "mensagem"    # Ljava/lang/String;

    .line 58
    sget-object v0, Lcom/itau/empresas/ui/view/MensagemAvisoView$1;->$SwitchMap$com$itau$empresas$ui$view$MensagemAvisoView$Estado:[I

    invoke-virtual {p1}, Lcom/itau/empresas/ui/view/MensagemAvisoView$Estado;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_28

    goto :goto_22

    .line 61
    :pswitch_c
    iget-object v0, p0, Lcom/itau/empresas/ui/view/MensagemAvisoView;->ll_view_mensagem_aviso:Landroid/widget/LinearLayout;

    iget v1, p0, Lcom/itau/empresas/ui/view/MensagemAvisoView;->corVerde:I

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundColor(I)V

    .line 62
    goto :goto_22

    .line 65
    :pswitch_14
    iget-object v0, p0, Lcom/itau/empresas/ui/view/MensagemAvisoView;->ll_view_mensagem_aviso:Landroid/widget/LinearLayout;

    iget v1, p0, Lcom/itau/empresas/ui/view/MensagemAvisoView;->corCinza:I

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundColor(I)V

    .line 68
    :pswitch_1b
    iget-object v0, p0, Lcom/itau/empresas/ui/view/MensagemAvisoView;->ll_view_mensagem_aviso:Landroid/widget/LinearLayout;

    iget v1, p0, Lcom/itau/empresas/ui/view/MensagemAvisoView;->corCinza:I

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundColor(I)V

    .line 71
    :goto_22
    iget-object v0, p0, Lcom/itau/empresas/ui/view/MensagemAvisoView;->texto_aviso:Landroid/widget/TextView;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 72
    return-void

    :pswitch_data_28
    .packed-switch 0x1
        :pswitch_c
        :pswitch_14
        :pswitch_1b
    .end packed-switch
.end method

.class Lcom/wdullaer/materialdatetimepicker/time/RadialPickerLayout$2;
.super Ljava/lang/Object;
.source "RadialPickerLayout.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/wdullaer/materialdatetimepicker/time/RadialPickerLayout;->onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/wdullaer/materialdatetimepicker/time/RadialPickerLayout;

.field final synthetic val$isInnerCircle:[Ljava/lang/Boolean;


# direct methods
.method constructor <init>(Lcom/wdullaer/materialdatetimepicker/time/RadialPickerLayout;[Ljava/lang/Boolean;)V
    .registers 3

    .line 634
    iput-object p1, p0, Lcom/wdullaer/materialdatetimepicker/time/RadialPickerLayout$2;->this$0:Lcom/wdullaer/materialdatetimepicker/time/RadialPickerLayout;

    iput-object p2, p0, Lcom/wdullaer/materialdatetimepicker/time/RadialPickerLayout$2;->val$isInnerCircle:[Ljava/lang/Boolean;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .registers 7

    .line 637
    iget-object v0, p0, Lcom/wdullaer/materialdatetimepicker/time/RadialPickerLayout$2;->this$0:Lcom/wdullaer/materialdatetimepicker/time/RadialPickerLayout;

    const/4 v1, 0x1

    # setter for: Lcom/wdullaer/materialdatetimepicker/time/RadialPickerLayout;->mDoingMove:Z
    invoke-static {v0, v1}, Lcom/wdullaer/materialdatetimepicker/time/RadialPickerLayout;->access$202(Lcom/wdullaer/materialdatetimepicker/time/RadialPickerLayout;Z)Z

    .line 638
    iget-object v0, p0, Lcom/wdullaer/materialdatetimepicker/time/RadialPickerLayout$2;->this$0:Lcom/wdullaer/materialdatetimepicker/time/RadialPickerLayout;

    iget-object v1, p0, Lcom/wdullaer/materialdatetimepicker/time/RadialPickerLayout$2;->this$0:Lcom/wdullaer/materialdatetimepicker/time/RadialPickerLayout;

    # getter for: Lcom/wdullaer/materialdatetimepicker/time/RadialPickerLayout;->mDownDegrees:I
    invoke-static {v1}, Lcom/wdullaer/materialdatetimepicker/time/RadialPickerLayout;->access$300(Lcom/wdullaer/materialdatetimepicker/time/RadialPickerLayout;)I

    move-result v1

    iget-object v2, p0, Lcom/wdullaer/materialdatetimepicker/time/RadialPickerLayout$2;->val$isInnerCircle:[Ljava/lang/Boolean;

    const/4 v3, 0x0

    aget-object v2, v2, v3

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    const/4 v3, 0x0

    const/4 v4, 0x1

    # invokes: Lcom/wdullaer/materialdatetimepicker/time/RadialPickerLayout;->reselectSelector(IZZZ)I
    invoke-static {v0, v1, v2, v3, v4}, Lcom/wdullaer/materialdatetimepicker/time/RadialPickerLayout;->access$400(Lcom/wdullaer/materialdatetimepicker/time/RadialPickerLayout;IZZZ)I

    move-result v5

    .line 640
    .local v5, "value":I
    iget-object v0, p0, Lcom/wdullaer/materialdatetimepicker/time/RadialPickerLayout$2;->this$0:Lcom/wdullaer/materialdatetimepicker/time/RadialPickerLayout;

    # setter for: Lcom/wdullaer/materialdatetimepicker/time/RadialPickerLayout;->mLastValueSelected:I
    invoke-static {v0, v5}, Lcom/wdullaer/materialdatetimepicker/time/RadialPickerLayout;->access$502(Lcom/wdullaer/materialdatetimepicker/time/RadialPickerLayout;I)I

    .line 641
    iget-object v0, p0, Lcom/wdullaer/materialdatetimepicker/time/RadialPickerLayout$2;->this$0:Lcom/wdullaer/materialdatetimepicker/time/RadialPickerLayout;

    # getter for: Lcom/wdullaer/materialdatetimepicker/time/RadialPickerLayout;->mListener:Lcom/wdullaer/materialdatetimepicker/time/RadialPickerLayout$OnValueSelectedListener;
    invoke-static {v0}, Lcom/wdullaer/materialdatetimepicker/time/RadialPickerLayout;->access$600(Lcom/wdullaer/materialdatetimepicker/time/RadialPickerLayout;)Lcom/wdullaer/materialdatetimepicker/time/RadialPickerLayout$OnValueSelectedListener;

    move-result-object v0

    iget-object v1, p0, Lcom/wdullaer/materialdatetimepicker/time/RadialPickerLayout$2;->this$0:Lcom/wdullaer/materialdatetimepicker/time/RadialPickerLayout;

    invoke-virtual {v1}, Lcom/wdullaer/materialdatetimepicker/time/RadialPickerLayout;->getCurrentItemShowing()I

    move-result v1

    const/4 v2, 0x0

    invoke-interface {v0, v1, v5, v2}, Lcom/wdullaer/materialdatetimepicker/time/RadialPickerLayout$OnValueSelectedListener;->onValueSelected(IIZ)V

    .line 642
    return-void
.end method

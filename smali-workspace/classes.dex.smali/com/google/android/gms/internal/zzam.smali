.class public Lcom/google/android/gms/internal/zzam;
.super Lcom/google/android/gms/internal/zzal;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/internal/zzam$zzb;,
        Lcom/google/android/gms/internal/zzam$zza;
    }
.end annotation


# static fields
.field private static zzok:Lcom/google/android/gms/ads/identifier/AdvertisingIdClient;

.field private static zzol:Ljava/util/concurrent/CountDownLatch;


# instance fields
.field private zzom:Z


# direct methods
.method static constructor <clinit>()V
    .registers 2

    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/gms/internal/zzam;->zzok:Lcom/google/android/gms/ads/identifier/AdvertisingIdClient;

    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    sput-object v0, Lcom/google/android/gms/internal/zzam;->zzol:Ljava/util/concurrent/CountDownLatch;

    return-void
.end method

.method protected constructor <init>(Landroid/content/Context;Lcom/google/android/gms/internal/zzap;Z)V
    .registers 4

    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/internal/zzal;-><init>(Landroid/content/Context;Lcom/google/android/gms/internal/zzap;)V

    iput-boolean p3, p0, Lcom/google/android/gms/internal/zzam;->zzom:Z

    return-void
.end method

.method static synthetic zzZ()Lcom/google/android/gms/ads/identifier/AdvertisingIdClient;
    .registers 1

    sget-object v0, Lcom/google/android/gms/internal/zzam;->zzok:Lcom/google/android/gms/ads/identifier/AdvertisingIdClient;

    return-object v0
.end method

.method static synthetic zza(Lcom/google/android/gms/ads/identifier/AdvertisingIdClient;)Lcom/google/android/gms/ads/identifier/AdvertisingIdClient;
    .registers 1

    sput-object p0, Lcom/google/android/gms/internal/zzam;->zzok:Lcom/google/android/gms/ads/identifier/AdvertisingIdClient;

    return-object p0
.end method

.method public static zza(Ljava/lang/String;Landroid/content/Context;Z)Lcom/google/android/gms/internal/zzam;
    .registers 8

    new-instance v2, Lcom/google/android/gms/internal/zzah;

    invoke-direct {v2}, Lcom/google/android/gms/internal/zzah;-><init>()V

    invoke-static {p0, p1, v2}, Lcom/google/android/gms/internal/zzam;->zza(Ljava/lang/String;Landroid/content/Context;Lcom/google/android/gms/internal/zzap;)V

    if-eqz p2, :cond_23

    const-class v3, Lcom/google/android/gms/internal/zzam;

    monitor-enter v3

    :try_start_d
    sget-object v0, Lcom/google/android/gms/internal/zzam;->zzok:Lcom/google/android/gms/ads/identifier/AdvertisingIdClient;

    if-nez v0, :cond_1e

    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/google/android/gms/internal/zzam$zzb;

    invoke-direct {v1, p1}, Lcom/google/android/gms/internal/zzam$zzb;-><init>(Landroid/content/Context;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V
    :try_end_1e
    .catchall {:try_start_d .. :try_end_1e} :catchall_20

    :cond_1e
    monitor-exit v3

    goto :goto_23

    :catchall_20
    move-exception v4

    monitor-exit v3

    throw v4

    :cond_23
    :goto_23
    new-instance v0, Lcom/google/android/gms/internal/zzam;

    invoke-direct {v0, p1, v2, p2}, Lcom/google/android/gms/internal/zzam;-><init>(Landroid/content/Context;Lcom/google/android/gms/internal/zzap;Z)V

    return-object v0
.end method

.method private zza(Landroid/content/Context;Lcom/google/ads/afma/nano/NanoAfmaSignals$AFMASignals;)V
    .registers 7

    iget-boolean v0, p0, Lcom/google/android/gms/internal/zzam;->zzom:Z

    if-nez v0, :cond_5

    return-void

    :cond_5
    :try_start_5
    invoke-virtual {p0}, Lcom/google/android/gms/internal/zzam;->zzS()Z

    move-result v0

    if-eqz v0, :cond_30

    invoke-virtual {p0}, Lcom/google/android/gms/internal/zzam;->zzY()Lcom/google/android/gms/internal/zzam$zza;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/internal/zzam$zza;->getId()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_2f

    invoke-virtual {v2}, Lcom/google/android/gms/internal/zzam$zza;->isLimitAdTrackingEnabled()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, Lcom/google/ads/afma/nano/NanoAfmaSignals$AFMASignals;->didOptOut:Ljava/lang/Boolean;

    const/4 v0, 0x5

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p2, Lcom/google/ads/afma/nano/NanoAfmaSignals$AFMASignals;->didSignalType:Ljava/lang/Integer;

    iput-object v3, p2, Lcom/google/ads/afma/nano/NanoAfmaSignals$AFMASignals;->didSignal:Ljava/lang/String;

    sget v0, Lcom/google/android/gms/internal/zzam;->zzob:I

    const/16 v1, 0x1c

    invoke-static {v1, v0}, Lcom/google/android/gms/internal/zzam;->zza(II)V

    :cond_2f
    goto :goto_3d

    :cond_30
    invoke-static {p1}, Lcom/google/android/gms/internal/zzam;->zzf(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p2, Lcom/google/ads/afma/nano/NanoAfmaSignals$AFMASignals;->didSignal:Ljava/lang/String;

    sget v0, Lcom/google/android/gms/internal/zzam;->zzob:I

    const/16 v1, 0x18

    invoke-static {v1, v0}, Lcom/google/android/gms/internal/zzam;->zza(II)V
    :try_end_3d
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_3d} :catch_3e
    .catch Lcom/google/android/gms/internal/zzal$zza; {:try_start_5 .. :try_end_3d} :catch_40

    :goto_3d
    goto :goto_42

    :catch_3e
    move-exception v2

    return-void

    :catch_40
    move-exception v2

    return-void

    :goto_42
    return-void
.end method

.method static synthetic zzaa()Ljava/util/concurrent/CountDownLatch;
    .registers 1

    sget-object v0, Lcom/google/android/gms/internal/zzam;->zzol:Ljava/util/concurrent/CountDownLatch;

    return-object v0
.end method


# virtual methods
.method zzY()Lcom/google/android/gms/internal/zzam$zza;
    .registers 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :try_start_0
    sget-object v0, Lcom/google/android/gms/internal/zzam;->zzol:Ljava/util/concurrent/CountDownLatch;

    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x2

    invoke-virtual {v0, v2, v3, v1}, Ljava/util/concurrent/CountDownLatch;->await(JLjava/util/concurrent/TimeUnit;)Z

    move-result v4

    if-nez v4, :cond_14

    new-instance v0, Lcom/google/android/gms/internal/zzam$zza;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-direct {v0, p0, v1, v2}, Lcom/google/android/gms/internal/zzam$zza;-><init>(Lcom/google/android/gms/internal/zzam;Ljava/lang/String;Z)V
    :try_end_13
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_13} :catch_15

    return-object v0

    :cond_14
    goto :goto_1e

    :catch_15
    move-exception v4

    new-instance v0, Lcom/google/android/gms/internal/zzam$zza;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-direct {v0, p0, v1, v2}, Lcom/google/android/gms/internal/zzam$zza;-><init>(Lcom/google/android/gms/internal/zzam;Ljava/lang/String;Z)V

    return-object v0

    :goto_1e
    const-class v5, Lcom/google/android/gms/internal/zzam;

    monitor-enter v5

    :try_start_21
    sget-object v0, Lcom/google/android/gms/internal/zzam;->zzok:Lcom/google/android/gms/ads/identifier/AdvertisingIdClient;

    if-nez v0, :cond_2e

    new-instance v0, Lcom/google/android/gms/internal/zzam$zza;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-direct {v0, p0, v1, v2}, Lcom/google/android/gms/internal/zzam$zza;-><init>(Lcom/google/android/gms/internal/zzam;Ljava/lang/String;Z)V
    :try_end_2c
    .catchall {:try_start_21 .. :try_end_2c} :catchall_36

    monitor-exit v5

    return-object v0

    :cond_2e
    :try_start_2e
    sget-object v0, Lcom/google/android/gms/internal/zzam;->zzok:Lcom/google/android/gms/ads/identifier/AdvertisingIdClient;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/identifier/AdvertisingIdClient;->getInfo()Lcom/google/android/gms/ads/identifier/AdvertisingIdClient$Info;
    :try_end_33
    .catchall {:try_start_2e .. :try_end_33} :catchall_36

    move-result-object v4

    monitor-exit v5

    goto :goto_39

    :catchall_36
    move-exception v6

    monitor-exit v5

    throw v6

    :goto_39
    invoke-virtual {v4}, Lcom/google/android/gms/ads/identifier/AdvertisingIdClient$Info;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/internal/zzam;->zzk(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    new-instance v0, Lcom/google/android/gms/internal/zzam$zza;

    invoke-virtual {v4}, Lcom/google/android/gms/ads/identifier/AdvertisingIdClient$Info;->isLimitAdTrackingEnabled()Z

    move-result v1

    invoke-direct {v0, p0, v5, v1}, Lcom/google/android/gms/internal/zzam$zza;-><init>(Lcom/google/android/gms/internal/zzam;Ljava/lang/String;Z)V

    return-object v0
.end method

.method protected zzc(Landroid/content/Context;)Lcom/google/ads/afma/nano/NanoAfmaSignals$AFMASignals;
    .registers 3

    invoke-super {p0, p1}, Lcom/google/android/gms/internal/zzal;->zzc(Landroid/content/Context;)Lcom/google/ads/afma/nano/NanoAfmaSignals$AFMASignals;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/internal/zzam;->zza(Landroid/content/Context;Lcom/google/ads/afma/nano/NanoAfmaSignals$AFMASignals;)V

    return-object v0
.end method

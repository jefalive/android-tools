.class public Lcom/itau/empresas/feature/logout/LogoutHelper;
.super Ljava/lang/Object;
.source "LogoutHelper.java"


# instance fields
.field application:Lcom/itau/empresas/CustomApplication;


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static agendarNotificacaoExtrato(Lcom/itau/empresas/CustomApplication;)V
    .registers 7
    .param p0, "application"    # Lcom/itau/empresas/CustomApplication;

    .line 84
    invoke-static {p0}, Lcom/itau/empresas/AlarmeUtils;->usuarioEfetuouLogout(Lcom/itau/empresas/CustomApplication;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 85
    return-void

    .line 87
    :cond_7
    invoke-static {}, Lbr/com/itau/sdk/android/core/BackendClient;->getOfflineOTP()Lbr/com/itau/security/token/model/OTPModel;

    move-result-object v0

    if-nez v0, :cond_18

    .line 88
    invoke-virtual {p0}, Lcom/itau/empresas/CustomApplication;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x1

    const-wide/32 v2, 0xf731400

    invoke-static {v0, v1, v2, v3}, Lcom/itau/empresas/AlarmeUtils;->agendarAlarmeUnico(Landroid/content/Context;IJ)V

    .line 93
    :cond_18
    invoke-virtual {p0}, Lcom/itau/empresas/CustomApplication;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x2

    const-wide/32 v2, 0x337f9800

    const-wide/32 v4, 0x337f9800

    invoke-static/range {v0 .. v5}, Lcom/itau/empresas/AlarmeUtils;->agendarAlarmeComRepeticao(Landroid/content/Context;IJJ)V

    .line 98
    invoke-static {p0}, Lcom/itau/empresas/AlarmeUtils;->salvaLogoutUsuario(Lcom/itau/empresas/CustomApplication;)V

    .line 100
    return-void
.end method

.method public static efetuarLogout(ZLandroid/app/Activity;Lcom/itau/empresas/CustomApplication;)V
    .registers 4
    .param p0, "requestLogout"    # Z
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "application"    # Lcom/itau/empresas/CustomApplication;

    .line 60
    invoke-virtual {p2}, Lcom/itau/empresas/CustomApplication;->limparCache()V

    .line 61
    invoke-virtual {p2}, Lcom/itau/empresas/CustomApplication;->limpaCachePendencias()V

    .line 63
    if-eqz p0, :cond_b

    .line 64
    invoke-static {}, Lcom/itau/empresas/controller/SessaoController;->executarLogout()V

    .line 67
    :cond_b
    invoke-static {p2}, Lcom/itau/empresas/feature/logout/LogoutHelper;->agendarNotificacaoExtrato(Lcom/itau/empresas/CustomApplication;)V

    .line 69
    if-nez p1, :cond_11

    .line 70
    return-void

    .line 72
    :cond_11
    invoke-static {}, Lcom/itau/empresas/ui/util/PoyntUtils;->isPoynt()Z

    move-result v0

    if-eqz v0, :cond_1b

    .line 73
    invoke-static {p1}, Lcom/itau/empresas/ui/util/ViewUtils;->sairDoApp(Landroid/content/Context;)V

    goto :goto_1e

    .line 75
    :cond_1b
    invoke-static {p1}, Lcom/itau/empresas/ui/util/ViewUtils;->redirecionaLogin(Landroid/app/Activity;)V

    .line 77
    :goto_1e
    return-void
.end method

.method private static exibirDialogDeConfirmacaoDeSaida(Landroid/support/v7/app/AppCompatActivity;)V
    .registers 4
    .param p0, "baseActivity"    # Landroid/support/v7/app/AppCompatActivity;

    .line 112
    invoke-static {}, Lcom/itau/empresas/ui/dialog/ConfirmarSairDialog_;->builder()Lcom/itau/empresas/ui/dialog/ConfirmarSairDialog_$FragmentBuilder_;

    move-result-object v0

    const-string v1, "isPoynt"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/itau/empresas/ui/dialog/ConfirmarSairDialog_$FragmentBuilder_;->arg(Ljava/lang/String;Z)Lorg/androidannotations/api/builder/FragmentBuilder;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/ui/dialog/ConfirmarSairDialog_$FragmentBuilder_;

    invoke-virtual {v0}, Lcom/itau/empresas/ui/dialog/ConfirmarSairDialog_$FragmentBuilder_;->build()Lcom/itau/empresas/ui/dialog/ConfirmarSairDialog;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/itau/empresas/ui/dialog/ConfirmarSairDialog;->show(Landroid/support/v7/app/AppCompatActivity;)Lcom/itau/empresas/ui/dialog/BaseDialog;

    .line 113
    return-void
.end method

.method public static trataSessaoExpirada(Landroid/app/Activity;Lcom/itau/empresas/CustomApplication;)V
    .registers 5
    .param p0, "activity"    # Landroid/app/Activity;
    .param p1, "application"    # Lcom/itau/empresas/CustomApplication;

    .line 37
    invoke-virtual {p1}, Lcom/itau/empresas/CustomApplication;->limpaCachePendencias()V

    .line 39
    if-eqz p0, :cond_2f

    .line 40
    new-instance v0, Landroid/support/v7/app/AlertDialog$Builder;

    const v1, 0x7f090102

    invoke-direct {v0, p0, v1}, Landroid/support/v7/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 41
    const v1, 0x7f0704ce

    invoke-virtual {v0, v1}, Landroid/support/v7/app/AlertDialog$Builder;->setTitle(I)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v0

    .line 42
    const v1, 0x7f0704cf

    invoke-virtual {v0, v1}, Landroid/support/v7/app/AlertDialog$Builder;->setMessage(I)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/itau/empresas/feature/logout/LogoutHelper$1;

    invoke-direct {v1, p0, p1}, Lcom/itau/empresas/feature/logout/LogoutHelper$1;-><init>(Landroid/app/Activity;Lcom/itau/empresas/CustomApplication;)V

    .line 43
    const v2, 0x7f07048c

    invoke-virtual {v0, v2, v1}, Landroid/support/v7/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v0

    .line 49
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v7/app/AlertDialog$Builder;->setCancelable(Z)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v0

    .line 50
    invoke-virtual {v0}, Landroid/support/v7/app/AlertDialog$Builder;->show()Landroid/support/v7/app/AlertDialog;

    .line 51
    :cond_2f
    invoke-static {}, Lbr/com/itau/sdk/android/core/BackendClient;->getInstance()Lbr/com/itau/sdk/android/core/BackendClient;

    move-result-object v0

    invoke-virtual {v0}, Lbr/com/itau/sdk/android/core/BackendClient;->reset()V

    .line 52
    return-void
.end method


# virtual methods
.method public efetuarLogout(ZLandroid/app/Activity;)V
    .registers 4
    .param p1, "requestLogout"    # Z
    .param p2, "activity"    # Landroid/app/Activity;

    .line 55
    iget-object v0, p0, Lcom/itau/empresas/feature/logout/LogoutHelper;->application:Lcom/itau/empresas/CustomApplication;

    invoke-static {p1, p2, v0}, Lcom/itau/empresas/feature/logout/LogoutHelper;->efetuarLogout(ZLandroid/app/Activity;Lcom/itau/empresas/CustomApplication;)V

    .line 56
    return-void
.end method

.method public sairDoApp(Landroid/support/v7/app/AppCompatActivity;)V
    .registers 3
    .param p1, "baseActivity"    # Landroid/support/v7/app/AppCompatActivity;

    .line 104
    iget-object v0, p0, Lcom/itau/empresas/feature/logout/LogoutHelper;->application:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-static {v0}, Lcom/itau/empresas/feature/feedback/FeedbackHelper;->deveExibirFeedback(Landroid/content/SharedPreferences;)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 105
    move-object v0, p1

    check-cast v0, Lcom/itau/empresas/ui/activity/BaseActivity;

    invoke-static {v0}, Lcom/itau/empresas/feature/feedback/FeedbackHelper;->exibirFeedback(Landroid/support/v7/app/AppCompatActivity;)V

    goto :goto_16

    .line 107
    :cond_13
    invoke-static {p1}, Lcom/itau/empresas/feature/logout/LogoutHelper;->exibirDialogDeConfirmacaoDeSaida(Landroid/support/v7/app/AppCompatActivity;)V

    .line 109
    :goto_16
    return-void
.end method

.method public trataSessaoExpirada(Landroid/app/Activity;)V
    .registers 3
    .param p1, "activity"    # Landroid/app/Activity;

    .line 32
    iget-object v0, p0, Lcom/itau/empresas/feature/logout/LogoutHelper;->application:Lcom/itau/empresas/CustomApplication;

    invoke-static {p1, v0}, Lcom/itau/empresas/feature/logout/LogoutHelper;->trataSessaoExpirada(Landroid/app/Activity;Lcom/itau/empresas/CustomApplication;)V

    .line 33
    return-void
.end method

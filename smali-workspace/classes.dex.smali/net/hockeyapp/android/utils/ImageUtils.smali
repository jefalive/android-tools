.class public Lnet/hockeyapp/android/utils/ImageUtils;
.super Ljava/lang/Object;
.source "ImageUtils.java"


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static calculateInSampleSize(Landroid/graphics/BitmapFactory$Options;II)I
    .registers 9
    .param p0, "options"    # Landroid/graphics/BitmapFactory$Options;
    .param p1, "reqWidth"    # I
    .param p2, "reqHeight"    # I

    .line 186
    iget v1, p0, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    .line 187
    .local v1, "height":I
    iget v2, p0, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    .line 188
    .local v2, "width":I
    const/4 v3, 0x1

    .line 190
    .local v3, "inSampleSize":I
    if-gt v1, p2, :cond_9

    if-le v2, p1, :cond_18

    .line 191
    :cond_9
    div-int/lit8 v4, v1, 0x2

    .line 192
    .local v4, "halfHeight":I
    div-int/lit8 v5, v2, 0x2

    .line 196
    .local v5, "halfWidth":I
    :goto_d
    div-int v0, v4, v3

    if-le v0, p2, :cond_18

    div-int v0, v5, v3

    if-le v0, p1, :cond_18

    .line 197
    mul-int/lit8 v3, v3, 0x2

    goto :goto_d

    .line 201
    .end local v4    # "halfHeight":I
    .end local v5    # "halfWidth":I
    :cond_18
    return v3
.end method

.method public static decodeSampledBitmap(Landroid/content/Context;Landroid/net/Uri;II)Landroid/graphics/Bitmap;
    .registers 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "imageUri"    # Landroid/net/Uri;
    .param p2, "reqWidth"    # I
    .param p3, "reqHeight"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 156
    new-instance v1, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v1}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 157
    .local v1, "options":Landroid/graphics/BitmapFactory$Options;
    const/4 v0, 0x1

    iput-boolean v0, v1, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 159
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v2

    .line 160
    .local v2, "inputBounds":Ljava/io/InputStream;
    const/4 v0, 0x0

    invoke-static {v2, v0, v1}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 163
    invoke-static {v1, p2, p3}, Lnet/hockeyapp/android/utils/ImageUtils;->calculateInSampleSize(Landroid/graphics/BitmapFactory$Options;II)I

    move-result v0

    iput v0, v1, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 166
    const/4 v0, 0x0

    iput-boolean v0, v1, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 167
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v3

    .line 168
    .local v3, "inputBitmap":Ljava/io/InputStream;
    const/4 v0, 0x0

    invoke-static {v3, v0, v1}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 170
    .local v4, "bitmap":Landroid/graphics/Bitmap;
    return-object v4
.end method

.method public static decodeSampledBitmap(Ljava/io/File;II)Landroid/graphics/Bitmap;
    .registers 6
    .param p0, "file"    # Ljava/io/File;
    .param p1, "reqWidth"    # I
    .param p2, "reqHeight"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 126
    new-instance v1, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v1}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 127
    .local v1, "options":Landroid/graphics/BitmapFactory$Options;
    const/4 v0, 0x1

    iput-boolean v0, v1, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 129
    invoke-virtual {p0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 132
    invoke-static {v1, p1, p2}, Lnet/hockeyapp/android/utils/ImageUtils;->calculateInSampleSize(Landroid/graphics/BitmapFactory$Options;II)I

    move-result v0

    iput v0, v1, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 135
    const/4 v0, 0x0

    iput-boolean v0, v1, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 136
    invoke-virtual {p0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 138
    .local v2, "bitmap":Landroid/graphics/Bitmap;
    return-object v2
.end method

.method public static determineOrientation(Landroid/content/Context;Landroid/net/Uri;)I
    .registers 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "uri"    # Landroid/net/Uri;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 80
    const/4 v1, 0x0

    .line 82
    .local v1, "input":Ljava/io/InputStream;
    :try_start_1
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v0

    move-object v1, v0

    .line 83
    invoke-static {v1}, Lnet/hockeyapp/android/utils/ImageUtils;->determineOrientation(Ljava/io/InputStream;)I
    :try_end_d
    .catchall {:try_start_1 .. :try_end_d} :catchall_14

    move-result v2

    .line 86
    if-eqz v1, :cond_13

    .line 87
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    :cond_13
    return v2

    .line 86
    :catchall_14
    move-exception v3

    if-eqz v1, :cond_1a

    .line 87
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    :cond_1a
    throw v3
.end method

.method public static determineOrientation(Ljava/io/File;)I
    .registers 5
    .param p0, "file"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 59
    const/4 v1, 0x0

    .line 61
    .local v1, "input":Ljava/io/InputStream;
    :try_start_1
    new-instance v0, Ljava/io/FileInputStream;

    invoke-direct {v0, p0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    move-object v1, v0

    .line 62
    invoke-static {v1}, Lnet/hockeyapp/android/utils/ImageUtils;->determineOrientation(Ljava/io/InputStream;)I
    :try_end_a
    .catchall {:try_start_1 .. :try_end_a} :catchall_11

    move-result v2

    .line 65
    if-eqz v1, :cond_10

    .line 66
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    :cond_10
    return v2

    .line 65
    :catchall_11
    move-exception v3

    if-eqz v1, :cond_17

    .line 66
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    :cond_17
    throw v3
.end method

.method public static determineOrientation(Ljava/io/InputStream;)I
    .registers 5
    .param p0, "input"    # Ljava/io/InputStream;

    .line 99
    new-instance v2, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v2}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 100
    .local v2, "options":Landroid/graphics/BitmapFactory$Options;
    const/4 v0, 0x1

    iput-boolean v0, v2, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 102
    const/4 v0, 0x0

    invoke-static {p0, v0, v2}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 103
    iget v0, v2, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_16

    iget v0, v2, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_18

    .line 104
    :cond_16
    const/4 v0, 0x0

    return v0

    .line 108
    :cond_18
    iget v0, v2, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    int-to-float v0, v0

    iget v1, v2, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    int-to-float v1, v1

    div-float v3, v0, v1

    .line 109
    .local v3, "ratio":F
    const/high16 v0, 0x3f800000    # 1.0f

    cmpl-float v0, v3, v0

    if-lez v0, :cond_28

    const/4 v0, 0x1

    goto :goto_29

    :cond_28
    const/4 v0, 0x0

    :goto_29
    return v0
.end method

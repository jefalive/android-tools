.class final synthetic Lbr/com/itau/sdk/android/core/endpoint/s;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field private final a:Landroid/app/Activity;

.field private final b:Lbr/com/itau/sdk/android/core/model/VersionControlDTO;


# direct methods
.method private constructor <init>(Landroid/app/Activity;Lbr/com/itau/sdk/android/core/model/VersionControlDTO;)V
    .registers 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lbr/com/itau/sdk/android/core/endpoint/s;->a:Landroid/app/Activity;

    iput-object p2, p0, Lbr/com/itau/sdk/android/core/endpoint/s;->b:Lbr/com/itau/sdk/android/core/model/VersionControlDTO;

    return-void
.end method

.method public static a(Landroid/app/Activity;Lbr/com/itau/sdk/android/core/model/VersionControlDTO;)Landroid/content/DialogInterface$OnClickListener;
    .registers 3

    new-instance v0, Lbr/com/itau/sdk/android/core/endpoint/s;

    invoke-direct {v0, p0, p1}, Lbr/com/itau/sdk/android/core/endpoint/s;-><init>(Landroid/app/Activity;Lbr/com/itau/sdk/android/core/model/VersionControlDTO;)V

    return-object v0
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .registers 5
    .annotation runtime Ljava/lang/invoke/LambdaForm$Hidden;
    .end annotation

    iget-object v0, p0, Lbr/com/itau/sdk/android/core/endpoint/s;->a:Landroid/app/Activity;

    iget-object v1, p0, Lbr/com/itau/sdk/android/core/endpoint/s;->b:Lbr/com/itau/sdk/android/core/model/VersionControlDTO;

    invoke-static {v0, v1, p1, p2}, Lbr/com/itau/sdk/android/core/endpoint/q;->a(Landroid/app/Activity;Lbr/com/itau/sdk/android/core/model/VersionControlDTO;Landroid/content/DialogInterface;I)V

    return-void
.end method

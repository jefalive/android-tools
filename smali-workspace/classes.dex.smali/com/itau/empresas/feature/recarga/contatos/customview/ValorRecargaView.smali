.class public Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView;
.super Landroid/widget/LinearLayout;
.source "ValorRecargaView.java"

# interfaces
.implements Lbr/com/itau/library/br/com/itau/library/interfaces/IAction;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView$Estados;
    }
.end annotation


# instance fields
.field application:Lcom/itau/empresas/CustomApplication;

.field campoValor:Lcom/itau/empresas/ui/view/EditTextMonetario;

.field controller:Lcom/itau/empresas/feature/recarga/efetivacao/controller/RecargaController;

.field private corCinza:I

.field private corVermelho:I

.field escolhePeriodoValor:Lbr/com/itau/library/LinearValuePickerView;

.field linhaEscolhePeriodoValor:Landroid/view/View;

.field llCampoValor:Landroid/widget/LinearLayout;

.field llRadioValor:Landroid/widget/LinearLayout;

.field private maquina:Lcom/itau/empresas/ui/util/MaquinaDeEstado;

.field private position:I

.field textoValorErro:Landroid/widget/TextView;

.field private timer:Ljava/util/Timer;

.field private valorMaximo:Lorg/joda/money/Money;

.field private valorMinimo:Lorg/joda/money/Money;

.field private final valorZero:Lorg/joda/money/Money;

.field private valoresRecarga:[Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 5
    .param p1, "context"    # Landroid/content/Context;

    .line 79
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 70
    const-string v0, "BRL 0"

    invoke-static {v0}, Lorg/joda/money/Money;->parse(Ljava/lang/String;)Lorg/joda/money/Money;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView;->valorZero:Lorg/joda/money/Money;

    .line 71
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView;->valorZero:Lorg/joda/money/Money;

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView;->valorMinimo:Lorg/joda/money/Money;

    .line 72
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView;->valorZero:Lorg/joda/money/Money;

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView;->valorMaximo:Lorg/joda/money/Money;

    .line 73
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "0"

    const/4 v2, 0x0

    aput-object v1, v0, v2

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView;->valoresRecarga:[Ljava/lang/String;

    .line 80
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .line 83
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 70
    const-string v0, "BRL 0"

    invoke-static {v0}, Lorg/joda/money/Money;->parse(Ljava/lang/String;)Lorg/joda/money/Money;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView;->valorZero:Lorg/joda/money/Money;

    .line 71
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView;->valorZero:Lorg/joda/money/Money;

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView;->valorMinimo:Lorg/joda/money/Money;

    .line 72
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView;->valorZero:Lorg/joda/money/Money;

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView;->valorMaximo:Lorg/joda/money/Money;

    .line 73
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "0"

    const/4 v2, 0x0

    aput-object v1, v0, v2

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView;->valoresRecarga:[Ljava/lang/String;

    .line 84
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .line 87
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 70
    const-string v0, "BRL 0"

    invoke-static {v0}, Lorg/joda/money/Money;->parse(Ljava/lang/String;)Lorg/joda/money/Money;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView;->valorZero:Lorg/joda/money/Money;

    .line 71
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView;->valorZero:Lorg/joda/money/Money;

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView;->valorMinimo:Lorg/joda/money/Money;

    .line 72
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView;->valorZero:Lorg/joda/money/Money;

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView;->valorMaximo:Lorg/joda/money/Money;

    .line 73
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "0"

    const/4 v2, 0x0

    aput-object v1, v0, v2

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView;->valoresRecarga:[Ljava/lang/String;

    .line 88
    return-void
.end method

.method static synthetic access$000(Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView;)V
    .registers 1
    .param p0, "x0"    # Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView;

    .line 39
    invoke-direct {p0}, Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView;->setHintCampoValorFixo()V

    return-void
.end method

.method private disparaEventoAnalytics()V
    .registers 6

    .line 114
    invoke-static {}, Lcom/itau/empresas/feature/analytics/AnalyticsHelper;->getInstance()Lcom/itau/empresas/feature/analytics/AnalyticsHelper;

    move-result-object v4

    .line 115
    .line 116
    .local v4, "analyticsHelper":Lcom/itau/empresas/feature/analytics/AnalyticsHelper;
    invoke-virtual {p0}, Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0700f2

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 117
    invoke-virtual {p0}, Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0700ce

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 118
    invoke-virtual {p0}, Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0700c3

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 115
    invoke-virtual {v4, v0, v1, v2}, Lcom/itau/empresas/feature/analytics/AnalyticsHelper;->eventoAdobeState(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 120
    return-void
.end method

.method private inicializaMaquinaDeEstado()Lcom/itau/empresas/ui/util/MaquinaDeEstado;
    .registers 7

    .line 232
    new-instance v0, Lcom/itau/empresas/ui/util/MaquinaDeEstado;

    invoke-direct {v0}, Lcom/itau/empresas/ui/util/MaquinaDeEstado;-><init>()V

    sget-object v1, Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView$Estados;->SEM_VALOR:Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView$Estados;

    new-instance v2, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;

    invoke-direct {v2}, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;-><init>()V

    const/4 v3, 0x2

    new-array v3, v3, [Landroid/view/View;

    iget-object v4, p0, Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView;->llRadioValor:Landroid/widget/LinearLayout;

    const/4 v5, 0x0

    aput-object v4, v3, v5

    iget-object v4, p0, Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView;->llCampoValor:Landroid/widget/LinearLayout;

    const/4 v5, 0x1

    aput-object v4, v3, v5

    .line 235
    invoke-virtual {v2, v3}, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;->gone([Landroid/view/View;)Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;

    move-result-object v2

    new-instance v3, Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView$4;

    invoke-direct {v3, p0}, Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView$4;-><init>(Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView;)V

    .line 236
    invoke-virtual {v2, v3}, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;->onChangeState(Lcom/itau/empresas/ui/util/maquinaestado/Estado$OnChangeState;)Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;

    move-result-object v2

    .line 244
    invoke-virtual {v2}, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;->build()Lcom/itau/empresas/ui/util/maquinaestado/Estado;

    move-result-object v2

    .line 233
    invoke-virtual {v0, v1, v2}, Lcom/itau/empresas/ui/util/MaquinaDeEstado;->novoEstado(Lcom/itau/empresas/ui/util/MaquinaDeEstado$Chave;Lcom/itau/empresas/ui/util/maquinaestado/Estado;)Lcom/itau/empresas/ui/util/MaquinaDeEstado;

    move-result-object v0

    sget-object v1, Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView$Estados;->VALOR_FIXO:Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView$Estados;

    new-instance v2, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;

    invoke-direct {v2}, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;-><init>()V

    const/4 v3, 0x1

    new-array v3, v3, [Landroid/view/View;

    iget-object v4, p0, Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView;->llCampoValor:Landroid/widget/LinearLayout;

    const/4 v5, 0x0

    aput-object v4, v3, v5

    .line 247
    invoke-virtual {v2, v3}, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;->gone([Landroid/view/View;)Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Landroid/view/View;

    iget-object v4, p0, Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView;->llRadioValor:Landroid/widget/LinearLayout;

    const/4 v5, 0x0

    aput-object v4, v3, v5

    .line 248
    invoke-virtual {v2, v3}, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;->visible([Landroid/view/View;)Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;

    move-result-object v2

    new-instance v3, Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView$3;

    invoke-direct {v3, p0}, Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView$3;-><init>(Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView;)V

    .line 249
    invoke-virtual {v2, v3}, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;->onChangeState(Lcom/itau/empresas/ui/util/maquinaestado/Estado$OnChangeState;)Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;

    move-result-object v2

    .line 255
    invoke-virtual {v2}, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;->build()Lcom/itau/empresas/ui/util/maquinaestado/Estado;

    move-result-object v2

    .line 245
    invoke-virtual {v0, v1, v2}, Lcom/itau/empresas/ui/util/MaquinaDeEstado;->novoEstado(Lcom/itau/empresas/ui/util/MaquinaDeEstado$Chave;Lcom/itau/empresas/ui/util/maquinaestado/Estado;)Lcom/itau/empresas/ui/util/MaquinaDeEstado;

    move-result-object v0

    sget-object v1, Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView$Estados;->VALOR_LIVRE:Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView$Estados;

    new-instance v2, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;

    invoke-direct {v2}, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;-><init>()V

    const/4 v3, 0x1

    new-array v3, v3, [Landroid/view/View;

    iget-object v4, p0, Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView;->llRadioValor:Landroid/widget/LinearLayout;

    const/4 v5, 0x0

    aput-object v4, v3, v5

    .line 258
    invoke-virtual {v2, v3}, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;->gone([Landroid/view/View;)Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Landroid/view/View;

    iget-object v4, p0, Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView;->llCampoValor:Landroid/widget/LinearLayout;

    const/4 v5, 0x0

    aput-object v4, v3, v5

    .line 259
    invoke-virtual {v2, v3}, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;->visible([Landroid/view/View;)Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;

    move-result-object v2

    new-instance v3, Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView$2;

    invoke-direct {v3, p0}, Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView$2;-><init>(Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView;)V

    .line 260
    invoke-virtual {v2, v3}, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;->onChangeState(Lcom/itau/empresas/ui/util/maquinaestado/Estado$OnChangeState;)Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;

    move-result-object v2

    .line 268
    invoke-virtual {v2}, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;->build()Lcom/itau/empresas/ui/util/maquinaestado/Estado;

    move-result-object v2

    .line 256
    invoke-virtual {v0, v1, v2}, Lcom/itau/empresas/ui/util/MaquinaDeEstado;->novoEstado(Lcom/itau/empresas/ui/util/MaquinaDeEstado$Chave;Lcom/itau/empresas/ui/util/maquinaestado/Estado;)Lcom/itau/empresas/ui/util/MaquinaDeEstado;

    move-result-object v0

    .line 232
    return-object v0
.end method

.method private setHintCampoValorFixo()V
    .registers 6

    .line 218
    invoke-virtual {p0}, Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView;->getContext()Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView;->valorMinimo:Lorg/joda/money/Money;

    .line 219
    invoke-virtual {v2}, Lorg/joda/money/Money;->getAmount()Ljava/math/BigDecimal;

    move-result-object v2

    const/4 v3, 0x1

    invoke-static {v2, v3}, Lcom/itau/empresas/ui/util/ValorMonetarioUtils;->formataValorDecimalParaMonetario(Ljava/math/BigDecimal;Z)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    iget-object v2, p0, Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView;->valorMaximo:Lorg/joda/money/Money;

    .line 220
    invoke-virtual {v2}, Lorg/joda/money/Money;->getAmount()Ljava/math/BigDecimal;

    move-result-object v2

    const/4 v3, 0x1

    invoke-static {v2, v3}, Lcom/itau/empresas/ui/util/ValorMonetarioUtils;->formataValorDecimalParaMonetario(Ljava/math/BigDecimal;Z)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    aput-object v2, v1, v3

    .line 218
    const v2, 0x7f07065c

    invoke-virtual {v0, v2, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 222
    .local v4, "hint":Ljava/lang/String;
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView;->campoValor:Lcom/itau/empresas/ui/view/EditTextMonetario;

    invoke-virtual {v0, v4}, Lcom/itau/empresas/ui/view/EditTextMonetario;->setFloatingLabelText(Ljava/lang/CharSequence;)V

    .line 223
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView;->campoValor:Lcom/itau/empresas/ui/view/EditTextMonetario;

    invoke-virtual {v0, v4}, Lcom/itau/empresas/ui/view/EditTextMonetario;->setHint(Ljava/lang/CharSequence;)V

    .line 224
    return-void
.end method


# virtual methods
.method public OnClickAction()V
    .registers 3

    .line 273
    invoke-virtual {p0}, Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView;->apagarMensagemDeErro()V

    .line 275
    invoke-static {}, Lde/greenrobot/event/EventBus;->getDefault()Lde/greenrobot/event/EventBus;

    move-result-object v0

    new-instance v1, Lcom/itau/empresas/Evento$ValorSelecionado;

    invoke-direct {v1}, Lcom/itau/empresas/Evento$ValorSelecionado;-><init>()V

    invoke-virtual {v0, v1}, Lde/greenrobot/event/EventBus;->post(Ljava/lang/Object;)V

    .line 276
    return-void
.end method

.method public alteraValor()V
    .registers 5

    .line 161
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView;->timer:Ljava/util/Timer;

    .line 162
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView;->timer:Ljava/util/Timer;

    new-instance v1, Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView$1;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView$1;-><init>(Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView;)V

    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 172
    return-void
.end method

.method aoCarregarTela()V
    .registers 4

    .line 98
    invoke-direct {p0}, Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView;->inicializaMaquinaDeEstado()Lcom/itau/empresas/ui/util/MaquinaDeEstado;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView;->maquina:Lcom/itau/empresas/ui/util/MaquinaDeEstado;

    .line 99
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView;->maquina:Lcom/itau/empresas/ui/util/MaquinaDeEstado;

    sget-object v1, Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView$Estados;->SEM_VALOR:Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView$Estados;

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/MaquinaDeEstado;->mudaParaEstado(Lcom/itau/empresas/ui/util/MaquinaDeEstado$Chave;)Lcom/itau/empresas/ui/util/MaquinaDeEstado$Chave;

    .line 101
    invoke-virtual {p0}, Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/ui/activity/BaseActivity;

    const/4 v1, 0x1

    new-array v1, v1, [Landroid/view/View;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/activity/BaseActivity;->registraView([Landroid/view/View;)V

    .line 102
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView;->escolhePeriodoValor:Lbr/com/itau/library/LinearValuePickerView;

    invoke-virtual {v0, p0}, Lbr/com/itau/library/LinearValuePickerView;->setOnClickAction(Lbr/com/itau/library/br/com/itau/library/interfaces/IAction;)V

    .line 103
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView;->escolhePeriodoValor:Lbr/com/itau/library/LinearValuePickerView;

    iget-object v1, p0, Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView;->valoresRecarga:[Ljava/lang/String;

    invoke-virtual {v0, v1}, Lbr/com/itau/library/LinearValuePickerView;->setValues([Ljava/lang/String;)V

    .line 105
    invoke-virtual {p0}, Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c0048

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView;->corCinza:I

    .line 106
    invoke-virtual {p0}, Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c0150

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView;->corVermelho:I

    .line 108
    invoke-direct {p0}, Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView;->setHintCampoValorFixo()V

    .line 110
    invoke-direct {p0}, Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView;->disparaEventoAnalytics()V

    .line 111
    return-void
.end method

.method public apagarMensagemDeErro()V
    .registers 3

    .line 279
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView;->maquina:Lcom/itau/empresas/ui/util/MaquinaDeEstado;

    invoke-virtual {v0}, Lcom/itau/empresas/ui/util/MaquinaDeEstado;->getEstadoAtual()Lcom/itau/empresas/ui/util/MaquinaDeEstado$Chave;

    move-result-object v0

    sget-object v1, Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView$Estados;->VALOR_LIVRE:Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView$Estados;

    if-ne v0, v1, :cond_11

    .line 280
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView;->campoValor:Lcom/itau/empresas/ui/view/EditTextMonetario;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/view/EditTextMonetario;->setError(Ljava/lang/CharSequence;)V

    goto :goto_29

    .line 281
    :cond_11
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView;->maquina:Lcom/itau/empresas/ui/util/MaquinaDeEstado;

    invoke-virtual {v0}, Lcom/itau/empresas/ui/util/MaquinaDeEstado;->getEstadoAtual()Lcom/itau/empresas/ui/util/MaquinaDeEstado$Chave;

    move-result-object v0

    sget-object v1, Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView$Estados;->VALOR_FIXO:Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView$Estados;

    if-ne v0, v1, :cond_29

    .line 282
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView;->linhaEscolhePeriodoValor:Landroid/view/View;

    iget v1, p0, Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView;->corCinza:I

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 283
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView;->textoValorErro:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 285
    :cond_29
    :goto_29
    return-void
.end method

.method public carregarValores(Ljava/lang/String;Ljava/lang/String;)V
    .registers 4
    .param p1, "nomeOperadora"    # Ljava/lang/String;
    .param p2, "nomeRegiao"    # Ljava/lang/String;

    .line 148
    invoke-virtual {p0}, Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/ui/activity/BaseActivity;

    invoke-virtual {v0}, Lcom/itau/empresas/ui/activity/BaseActivity;->mostraDialogoDeProgresso()V

    .line 149
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView;->controller:Lcom/itau/empresas/feature/recarga/efetivacao/controller/RecargaController;

    invoke-virtual {v0, p1, p2}, Lcom/itau/empresas/feature/recarga/efetivacao/controller/RecargaController;->buscaDetalheOperadoraRecarga(Ljava/lang/String;Ljava/lang/String;)V

    .line 150
    return-void
.end method

.method public carregarValores(Ljava/lang/String;Ljava/lang/String;I)V
    .registers 5
    .param p1, "nomeOperadora"    # Ljava/lang/String;
    .param p2, "nomeRegiao"    # Ljava/lang/String;
    .param p3, "position"    # I

    .line 153
    invoke-virtual {p0}, Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/ui/activity/BaseActivity;

    invoke-virtual {v0}, Lcom/itau/empresas/ui/activity/BaseActivity;->mostraDialogoDeProgresso()V

    .line 154
    iput p3, p0, Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView;->position:I

    .line 155
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView;->controller:Lcom/itau/empresas/feature/recarga/efetivacao/controller/RecargaController;

    invoke-virtual {v0, p1, p2}, Lcom/itau/empresas/feature/recarga/efetivacao/controller/RecargaController;->buscaDetalheOperadoraRecarga(Ljava/lang/String;Ljava/lang/String;)V

    .line 156
    return-void
.end method

.method public desabilitado()V
    .registers 3

    .line 227
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView;->maquina:Lcom/itau/empresas/ui/util/MaquinaDeEstado;

    sget-object v1, Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView$Estados;->SEM_VALOR:Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView$Estados;

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/MaquinaDeEstado;->mudaParaEstado(Lcom/itau/empresas/ui/util/MaquinaDeEstado$Chave;)Lcom/itau/empresas/ui/util/MaquinaDeEstado$Chave;

    .line 228
    return-void
.end method

.method public getValor()Lorg/joda/money/Money;
    .registers 5

    .line 206
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView;->maquina:Lcom/itau/empresas/ui/util/MaquinaDeEstado;

    invoke-virtual {v0}, Lcom/itau/empresas/ui/util/MaquinaDeEstado;->getEstadoAtual()Lcom/itau/empresas/ui/util/MaquinaDeEstado$Chave;

    move-result-object v2

    .line 207
    .local v2, "e":Lcom/itau/empresas/ui/util/MaquinaDeEstado$Chave;
    sget-object v0, Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView$Estados;->VALOR_LIVRE:Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView$Estados;

    if-ne v2, v0, :cond_11

    .line 208
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView;->campoValor:Lcom/itau/empresas/ui/view/EditTextMonetario;

    invoke-virtual {v0}, Lcom/itau/empresas/ui/view/EditTextMonetario;->getValorMonetario()Lorg/joda/money/Money;

    move-result-object v0

    return-object v0

    .line 209
    :cond_11
    sget-object v0, Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView$Estados;->VALOR_FIXO:Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView$Estados;

    if-ne v2, v0, :cond_37

    .line 210
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView;->valoresRecarga:[Ljava/lang/String;

    iget-object v1, p0, Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView;->escolhePeriodoValor:Lbr/com/itau/library/LinearValuePickerView;

    invoke-virtual {v1}, Lbr/com/itau/library/LinearValuePickerView;->getSelectedPosition()I

    move-result v1

    aget-object v3, v0, v1

    .line 211
    .local v3, "valor":Ljava/lang/String;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "BRL "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lorg/joda/money/Money;->parse(Ljava/lang/String;)Lorg/joda/money/Money;

    move-result-object v0

    return-object v0

    .line 213
    .end local v3    # "valor":Ljava/lang/String;
    :cond_37
    const/4 v0, 0x0

    return-object v0
.end method

.method public isViewCarregada()Z
    .registers 3

    .line 292
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView;->maquina:Lcom/itau/empresas/ui/util/MaquinaDeEstado;

    invoke-virtual {v0}, Lcom/itau/empresas/ui/util/MaquinaDeEstado;->getEstadoAtual()Lcom/itau/empresas/ui/util/MaquinaDeEstado$Chave;

    move-result-object v0

    sget-object v1, Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView$Estados;->SEM_VALOR:Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView$Estados;

    if-eq v0, v1, :cond_c

    const/4 v0, 0x1

    goto :goto_d

    :cond_c
    const/4 v0, 0x0

    :goto_d
    return v0
.end method

.method public onEventMainThread(Lcom/itau/empresas/feature/recarga/contatos/model/RecargaDetalheOperadoraVO;)V
    .registers 5
    .param p1, "detalheOperadoraVO"    # Lcom/itau/empresas/feature/recarga/contatos/model/RecargaDetalheOperadoraVO;

    .line 180
    invoke-virtual {p0}, Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/ui/activity/BaseActivity;

    invoke-virtual {v0}, Lcom/itau/empresas/ui/activity/BaseActivity;->escondeDialogoDeProgresso()V

    .line 181
    invoke-virtual {p1}, Lcom/itau/empresas/feature/recarga/contatos/model/RecargaDetalheOperadoraVO;->getValorMinimo()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_6b

    invoke-virtual {p1}, Lcom/itau/empresas/feature/recarga/contatos/model/RecargaDetalheOperadoraVO;->getValorMinimo()Ljava/lang/String;

    move-result-object v0

    .line 182
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_6b

    .line 183
    invoke-virtual {p1}, Lcom/itau/empresas/feature/recarga/contatos/model/RecargaDetalheOperadoraVO;->getValorMaximo()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_6b

    .line 184
    invoke-virtual {p1}, Lcom/itau/empresas/feature/recarga/contatos/model/RecargaDetalheOperadoraVO;->getValorMaximo()Ljava/lang/String;

    move-result-object v0

    .line 185
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_6b

    .line 186
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "BRL "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/itau/empresas/feature/recarga/contatos/model/RecargaDetalheOperadoraVO;->getValorMinimo()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lorg/joda/money/Money;->parse(Ljava/lang/String;)Lorg/joda/money/Money;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView;->valorMinimo:Lorg/joda/money/Money;

    .line 187
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "BRL "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/itau/empresas/feature/recarga/contatos/model/RecargaDetalheOperadoraVO;->getValorMaximo()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lorg/joda/money/Money;->parse(Ljava/lang/String;)Lorg/joda/money/Money;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView;->valorMaximo:Lorg/joda/money/Money;

    .line 189
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView;->maquina:Lcom/itau/empresas/ui/util/MaquinaDeEstado;

    sget-object v1, Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView$Estados;->VALOR_LIVRE:Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView$Estados;

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/MaquinaDeEstado;->mudaParaEstado(Lcom/itau/empresas/ui/util/MaquinaDeEstado$Chave;)Lcom/itau/empresas/ui/util/MaquinaDeEstado$Chave;

    goto :goto_aa

    .line 191
    :cond_6b
    invoke-virtual {p1}, Lcom/itau/empresas/feature/recarga/contatos/model/RecargaDetalheOperadoraVO;->getRecargas()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_aa

    .line 192
    invoke-virtual {p1}, Lcom/itau/empresas/feature/recarga/contatos/model/RecargaDetalheOperadoraVO;->getRecargas()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView;->valoresRecarga:[Ljava/lang/String;

    .line 194
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_7e
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView;->valoresRecarga:[Ljava/lang/String;

    array-length v0, v0

    if-ge v2, v0, :cond_9c

    .line 195
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView;->valoresRecarga:[Ljava/lang/String;

    invoke-virtual {p1}, Lcom/itau/empresas/feature/recarga/contatos/model/RecargaDetalheOperadoraVO;->getRecargas()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/itau/empresas/feature/recarga/efetivacao/model/ValorRecargaVO;

    invoke-virtual {v1}, Lcom/itau/empresas/feature/recarga/efetivacao/model/ValorRecargaVO;->getValorRecarga()Ljava/lang/Integer;

    move-result-object v1

    .line 196
    invoke-virtual {v1}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v2

    .line 194
    add-int/lit8 v2, v2, 0x1

    goto :goto_7e

    .line 199
    .end local v2    # "i":I
    :cond_9c
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView;->escolhePeriodoValor:Lbr/com/itau/library/LinearValuePickerView;

    iget-object v1, p0, Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView;->valoresRecarga:[Ljava/lang/String;

    invoke-virtual {v0, v1}, Lbr/com/itau/library/LinearValuePickerView;->setValues([Ljava/lang/String;)V

    .line 201
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView;->maquina:Lcom/itau/empresas/ui/util/MaquinaDeEstado;

    sget-object v1, Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView$Estados;->VALOR_FIXO:Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView$Estados;

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/MaquinaDeEstado;->mudaParaEstado(Lcom/itau/empresas/ui/util/MaquinaDeEstado$Chave;)Lcom/itau/empresas/ui/util/MaquinaDeEstado$Chave;

    .line 203
    :cond_aa
    :goto_aa
    return-void
.end method

.method public validaValor()Z
    .registers 8

    .line 124
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView;->maquina:Lcom/itau/empresas/ui/util/MaquinaDeEstado;

    invoke-virtual {v0}, Lcom/itau/empresas/ui/util/MaquinaDeEstado;->getEstadoAtual()Lcom/itau/empresas/ui/util/MaquinaDeEstado$Chave;

    move-result-object v0

    sget-object v1, Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView$Estados;->VALOR_LIVRE:Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView$Estados;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5b

    .line 125
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView;->campoValor:Lcom/itau/empresas/ui/view/EditTextMonetario;

    invoke-virtual {v0}, Lcom/itau/empresas/ui/view/EditTextMonetario;->getValorMonetario()Lorg/joda/money/Money;

    move-result-object v6

    .line 127
    .local v6, "moneyValor":Lorg/joda/money/Money;
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView;->valorMinimo:Lorg/joda/money/Money;

    .line 128
    invoke-virtual {v6, v0}, Lorg/joda/money/Money;->isLessThan(Lorg/joda/money/BigMoneyProvider;)Z

    move-result v0

    if-nez v0, :cond_26

    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView;->valorMaximo:Lorg/joda/money/Money;

    invoke-virtual {v6, v0}, Lorg/joda/money/Money;->isGreaterThan(Lorg/joda/money/BigMoneyProvider;)Z

    move-result v0

    if-nez v0, :cond_26

    const/4 v5, 0x1

    goto :goto_27

    :cond_26
    const/4 v5, 0x0

    .line 129
    .local v5, "valorValido":Z
    :goto_27
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView;->campoValor:Lcom/itau/empresas/ui/view/EditTextMonetario;

    if-eqz v5, :cond_2d

    const/4 v1, 0x0

    goto :goto_57

    .line 131
    :cond_2d
    invoke-virtual {p0}, Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView;->getContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView;->valorMinimo:Lorg/joda/money/Money;

    .line 133
    invoke-virtual {v3}, Lorg/joda/money/Money;->getAmount()Ljava/math/BigDecimal;

    move-result-object v3

    .line 132
    const/4 v4, 0x1

    invoke-static {v3, v4}, Lcom/itau/empresas/ui/util/ValorMonetarioUtils;->formataValorDecimalParaMonetario(Ljava/math/BigDecimal;Z)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    aput-object v3, v2, v4

    iget-object v3, p0, Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView;->valorMaximo:Lorg/joda/money/Money;

    .line 135
    invoke-virtual {v3}, Lorg/joda/money/Money;->getAmount()Ljava/math/BigDecimal;

    move-result-object v3

    .line 134
    const/4 v4, 0x1

    invoke-static {v3, v4}, Lcom/itau/empresas/ui/util/ValorMonetarioUtils;->formataValorDecimalParaMonetario(Ljava/math/BigDecimal;Z)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    aput-object v3, v2, v4

    .line 131
    const v3, 0x7f07065d

    invoke-virtual {v1, v3, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 129
    :goto_57
    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/view/EditTextMonetario;->setError(Ljava/lang/CharSequence;)V

    .line 136
    .end local v6    # "moneyValor":Lorg/joda/money/Money;
    goto :goto_8e

    .end local v5    # "valorValido":Z
    :cond_5b
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView;->maquina:Lcom/itau/empresas/ui/util/MaquinaDeEstado;

    invoke-virtual {v0}, Lcom/itau/empresas/ui/util/MaquinaDeEstado;->getEstadoAtual()Lcom/itau/empresas/ui/util/MaquinaDeEstado$Chave;

    move-result-object v0

    sget-object v1, Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView$Estados;->VALOR_FIXO:Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView$Estados;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8d

    .line 137
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView;->escolhePeriodoValor:Lbr/com/itau/library/LinearValuePickerView;

    invoke-virtual {v0}, Lbr/com/itau/library/LinearValuePickerView;->getSelectedPosition()I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_74

    const/4 v5, 0x1

    goto :goto_75

    :cond_74
    const/4 v5, 0x0

    .line 138
    .local v5, "valorValido":Z
    :goto_75
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView;->textoValorErro:Landroid/widget/TextView;

    if-eqz v5, :cond_7c

    const/16 v1, 0x8

    goto :goto_7d

    :cond_7c
    const/4 v1, 0x0

    :goto_7d
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 139
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView;->linhaEscolhePeriodoValor:Landroid/view/View;

    if-eqz v5, :cond_87

    iget v1, p0, Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView;->corCinza:I

    goto :goto_89

    :cond_87
    iget v1, p0, Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView;->corVermelho:I

    :goto_89
    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    goto :goto_8e

    .line 141
    .end local v5    # "valorValido":Z
    :cond_8d
    const/4 v5, 0x0

    .line 144
    .local v5, "valorValido":Z
    :goto_8e
    return v5
.end method

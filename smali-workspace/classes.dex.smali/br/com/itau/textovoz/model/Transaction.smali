.class public Lbr/com/itau/textovoz/model/Transaction;
.super Ljava/lang/Object;
.source "Transaction.java"


# instance fields
.field private keyMobile:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "chaveMobile"
    .end annotation
.end field

.field private name:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "nome"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getKeyMobile()Ljava/lang/String;
    .registers 2

    .line 18
    iget-object v0, p0, Lbr/com/itau/textovoz/model/Transaction;->keyMobile:Ljava/lang/String;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .registers 2

    .line 14
    iget-object v0, p0, Lbr/com/itau/textovoz/model/Transaction;->name:Ljava/lang/String;

    return-object v0
.end method

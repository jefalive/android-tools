.class public Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity_$IntentBuilder_;
.super Lorg/androidannotations/api/builder/ActivityIntentBuilder;
.source "RecargaCadastroContatoActivity_.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity_;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "IntentBuilder_"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/androidannotations/api/builder/ActivityIntentBuilder<Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity_$IntentBuilder_;>;"
    }
.end annotation


# instance fields
.field private fragmentSupport_:Landroid/support/v4/app/Fragment;

.field private fragment_:Landroid/app/Fragment;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .param p1, "context"    # Landroid/content/Context;

    .line 264
    const-class v0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity_;

    invoke-direct {p0, p1, v0}, Lorg/androidannotations/api/builder/ActivityIntentBuilder;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 265
    return-void
.end method


# virtual methods
.method public contatoRecarga(Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;)Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity_$IntentBuilder_;
    .registers 3
    .param p1, "contatoRecarga"    # Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;

    .line 311
    const-string v0, "contatoRecarga"

    invoke-super {p0, v0, p1}, Lorg/androidannotations/api/builder/ActivityIntentBuilder;->extra(Ljava/lang/String;Ljava/io/Serializable;)Lorg/androidannotations/api/builder/IntentBuilder;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity_$IntentBuilder_;

    return-object v0
.end method

.method public startForResult(I)Lorg/androidannotations/api/builder/PostActivityStarter;
    .registers 6
    .param p1, "requestCode"    # I

    .line 279
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity_$IntentBuilder_;->fragmentSupport_:Landroid/support/v4/app/Fragment;

    if-eqz v0, :cond_c

    .line 280
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity_$IntentBuilder_;->fragmentSupport_:Landroid/support/v4/app/Fragment;

    iget-object v1, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity_$IntentBuilder_;->intent:Landroid/content/Intent;

    invoke-virtual {v0, v1, p1}, Landroid/support/v4/app/Fragment;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_52

    .line 282
    :cond_c
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity_$IntentBuilder_;->fragment_:Landroid/app/Fragment;

    if-eqz v0, :cond_28

    .line 283
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_20

    .line 284
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity_$IntentBuilder_;->fragment_:Landroid/app/Fragment;

    iget-object v1, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity_$IntentBuilder_;->intent:Landroid/content/Intent;

    iget-object v2, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity_$IntentBuilder_;->lastOptions:Landroid/os/Bundle;

    invoke-virtual {v0, v1, p1, v2}, Landroid/app/Fragment;->startActivityForResult(Landroid/content/Intent;ILandroid/os/Bundle;)V

    goto :goto_52

    .line 286
    :cond_20
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity_$IntentBuilder_;->fragment_:Landroid/app/Fragment;

    iget-object v1, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity_$IntentBuilder_;->intent:Landroid/content/Intent;

    invoke-virtual {v0, v1, p1}, Landroid/app/Fragment;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_52

    .line 289
    :cond_28
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity_$IntentBuilder_;->context:Landroid/content/Context;

    instance-of v0, v0, Landroid/app/Activity;

    if-eqz v0, :cond_3b

    .line 290
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity_$IntentBuilder_;->context:Landroid/content/Context;

    move-object v3, v0

    check-cast v3, Landroid/app/Activity;

    .line 291
    .local v3, "activity":Landroid/app/Activity;
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity_$IntentBuilder_;->intent:Landroid/content/Intent;

    iget-object v1, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity_$IntentBuilder_;->lastOptions:Landroid/os/Bundle;

    invoke-static {v3, v0, p1, v1}, Landroid/support/v4/app/ActivityCompat;->startActivityForResult(Landroid/app/Activity;Landroid/content/Intent;ILandroid/os/Bundle;)V

    .line 292
    .end local v3    # "activity":Landroid/app/Activity;
    goto :goto_52

    .line 293
    :cond_3b
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_4b

    .line 294
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity_$IntentBuilder_;->context:Landroid/content/Context;

    iget-object v1, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity_$IntentBuilder_;->intent:Landroid/content/Intent;

    iget-object v2, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity_$IntentBuilder_;->lastOptions:Landroid/os/Bundle;

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;Landroid/os/Bundle;)V

    goto :goto_52

    .line 296
    :cond_4b
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity_$IntentBuilder_;->context:Landroid/content/Context;

    iget-object v1, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity_$IntentBuilder_;->intent:Landroid/content/Intent;

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 301
    :goto_52
    new-instance v0, Lorg/androidannotations/api/builder/PostActivityStarter;

    iget-object v1, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity_$IntentBuilder_;->context:Landroid/content/Context;

    invoke-direct {v0, v1}, Lorg/androidannotations/api/builder/PostActivityStarter;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method public tipo(Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity$TIPOS;)Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity_$IntentBuilder_;
    .registers 3
    .param p1, "tipo"    # Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity$TIPOS;

    .line 321
    const-string v0, "tipo"

    invoke-super {p0, v0, p1}, Lorg/androidannotations/api/builder/ActivityIntentBuilder;->extra(Ljava/lang/String;Ljava/io/Serializable;)Lorg/androidannotations/api/builder/IntentBuilder;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity_$IntentBuilder_;

    return-object v0
.end method

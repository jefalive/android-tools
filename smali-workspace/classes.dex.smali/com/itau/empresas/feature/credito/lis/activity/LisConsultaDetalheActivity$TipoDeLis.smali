.class final enum Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity$TipoDeLis;
.super Ljava/lang/Enum;
.source "LisConsultaDetalheActivity.java"

# interfaces
.implements Lcom/itau/empresas/ui/util/MaquinaDeEstado$Chave;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "TipoDeLis"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity$TipoDeLis;>;Lcom/itau/empresas/ui/util/MaquinaDeEstado$Chave;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity$TipoDeLis;

.field public static final enum LIS:Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity$TipoDeLis;

.field public static final enum LIS_ADICIONAL:Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity$TipoDeLis;

.field public static final enum LIS_RECEBIVEL:Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity$TipoDeLis;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .line 81
    new-instance v0, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity$TipoDeLis;

    const-string v1, "LIS"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity$TipoDeLis;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity$TipoDeLis;->LIS:Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity$TipoDeLis;

    new-instance v0, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity$TipoDeLis;

    const-string v1, "LIS_RECEBIVEL"

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity$TipoDeLis;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity$TipoDeLis;->LIS_RECEBIVEL:Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity$TipoDeLis;

    new-instance v0, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity$TipoDeLis;

    const-string v1, "LIS_ADICIONAL"

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2}, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity$TipoDeLis;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity$TipoDeLis;->LIS_ADICIONAL:Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity$TipoDeLis;

    .line 80
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity$TipoDeLis;

    sget-object v1, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity$TipoDeLis;->LIS:Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity$TipoDeLis;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sget-object v1, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity$TipoDeLis;->LIS_RECEBIVEL:Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity$TipoDeLis;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    sget-object v1, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity$TipoDeLis;->LIS_ADICIONAL:Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity$TipoDeLis;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    sput-object v0, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity$TipoDeLis;->$VALUES:[Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity$TipoDeLis;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 80
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity$TipoDeLis;
    .registers 2
    .param p0, "name"    # Ljava/lang/String;

    .line 80
    const-class v0, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity$TipoDeLis;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity$TipoDeLis;

    return-object v0
.end method

.method public static values()[Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity$TipoDeLis;
    .registers 1

    .line 80
    sget-object v0, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity$TipoDeLis;->$VALUES:[Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity$TipoDeLis;

    invoke-virtual {v0}, [Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity$TipoDeLis;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity$TipoDeLis;

    return-object v0
.end method

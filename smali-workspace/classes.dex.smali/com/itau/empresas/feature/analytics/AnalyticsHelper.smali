.class public final Lcom/itau/empresas/feature/analytics/AnalyticsHelper;
.super Ljava/lang/Object;
.source "AnalyticsHelper.java"


# static fields
.field private static analyticsHelper:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<Lcom/itau/empresas/feature/analytics/AnalyticsHelper;>;"
        }
    .end annotation
.end field

.field private static application:Lcom/itau/empresas/CustomApplication;


# direct methods
.method private constructor <init>()V
    .registers 1

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getInstance()Lcom/itau/empresas/feature/analytics/AnalyticsHelper;
    .registers 2

    .line 20
    sget-object v0, Lcom/itau/empresas/feature/analytics/AnalyticsHelper;->analyticsHelper:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_c

    sget-object v0, Lcom/itau/empresas/feature/analytics/AnalyticsHelper;->analyticsHelper:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_18

    .line 21
    :cond_c
    new-instance v0, Ljava/lang/ref/WeakReference;

    new-instance v1, Lcom/itau/empresas/feature/analytics/AnalyticsHelper;

    invoke-direct {v1}, Lcom/itau/empresas/feature/analytics/AnalyticsHelper;-><init>()V

    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    sput-object v0, Lcom/itau/empresas/feature/analytics/AnalyticsHelper;->analyticsHelper:Ljava/lang/ref/WeakReference;

    .line 23
    :cond_18
    sget-object v0, Lcom/itau/empresas/feature/analytics/AnalyticsHelper;->application:Lcom/itau/empresas/CustomApplication;

    if-nez v0, :cond_22

    .line 24
    invoke-static {}, Lcom/itau/empresas/CustomApplication;->getSelfInstance()Lcom/itau/empresas/CustomApplication;

    move-result-object v0

    sput-object v0, Lcom/itau/empresas/feature/analytics/AnalyticsHelper;->application:Lcom/itau/empresas/CustomApplication;

    .line 26
    :cond_22
    sget-object v0, Lcom/itau/empresas/feature/analytics/AnalyticsHelper;->analyticsHelper:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/feature/analytics/AnalyticsHelper;

    return-object v0
.end method


# virtual methods
.method public eventoAdobeAction(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 7
    .param p1, "p1"    # Ljava/lang/String;
    .param p2, "chave"    # Ljava/lang/String;
    .param p3, "valor"    # Ljava/lang/String;

    .line 57
    sget-object v0, Lcom/itau/empresas/feature/analytics/AnalyticsHelper;->application:Lcom/itau/empresas/CustomApplication;

    if-nez v0, :cond_5

    .line 58
    return-void

    .line 61
    :cond_5
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 62
    .local v2, "cDados":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    sget-object v0, Lcom/itau/empresas/feature/analytics/AnalyticsHelper;->application:Lcom/itau/empresas/CustomApplication;

    const v1, 0x7f0700c1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/CustomApplication;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v0, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 63
    invoke-interface {v2, p2, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 65
    invoke-static {p1, v2}, Lcom/adobe/mobile/Analytics;->trackAction(Ljava/lang/String;Ljava/util/Map;)V

    .line 66
    return-void
.end method

.method public eventoAdobeState(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 7
    .param p1, "nome"    # Ljava/lang/String;
    .param p2, "secao"    # Ljava/lang/String;
    .param p3, "estadoLogin"    # Ljava/lang/String;

    .line 37
    sget-object v0, Lcom/itau/empresas/feature/analytics/AnalyticsHelper;->application:Lcom/itau/empresas/CustomApplication;

    if-nez v0, :cond_5

    .line 38
    return-void

    .line 41
    :cond_5
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 42
    .local v2, "cDados":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    sget-object v0, Lcom/itau/empresas/feature/analytics/AnalyticsHelper;->application:Lcom/itau/empresas/CustomApplication;

    const v1, 0x7f0700c1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/CustomApplication;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v0, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 43
    sget-object v0, Lcom/itau/empresas/feature/analytics/AnalyticsHelper;->application:Lcom/itau/empresas/CustomApplication;

    const v1, 0x7f0700c0

    invoke-virtual {v0, v1}, Lcom/itau/empresas/CustomApplication;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v0, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 44
    sget-object v0, Lcom/itau/empresas/feature/analytics/AnalyticsHelper;->application:Lcom/itau/empresas/CustomApplication;

    const v1, 0x7f0700bc

    invoke-virtual {v0, v1}, Lcom/itau/empresas/CustomApplication;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v0, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 46
    invoke-static {p1, v2}, Lcom/adobe/mobile/Analytics;->trackState(Ljava/lang/String;Ljava/util/Map;)V

    .line 47
    return-void
.end method

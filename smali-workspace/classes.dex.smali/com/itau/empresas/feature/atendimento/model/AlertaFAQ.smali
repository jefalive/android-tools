.class public Lcom/itau/empresas/feature/atendimento/model/AlertaFAQ;
.super Ljava/lang/Object;
.source "AlertaFAQ.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private imagemMobile:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "imagem_mobile"
    .end annotation
.end field

.field private linkBotaoMobile:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "link_botao_mobile"
    .end annotation
.end field

.field private mensagem:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "mensagem"
    .end annotation
.end field

.field private mensagemResumida:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "mensagem_resumida"
    .end annotation
.end field

.field private textoBotaoMobile:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "texto_botao_mobile"
    .end annotation
.end field

.field private tituloMobile:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "titulo_mobile"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getLink_botao_mobile()Ljava/lang/String;
    .registers 2

    .line 46
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/model/AlertaFAQ;->linkBotaoMobile:Ljava/lang/String;

    return-object v0
.end method

.method public getMensagem()Ljava/lang/String;
    .registers 2

    .line 26
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/model/AlertaFAQ;->mensagem:Ljava/lang/String;

    return-object v0
.end method

.method public getMensagem_resumida()Ljava/lang/String;
    .registers 2

    .line 30
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/model/AlertaFAQ;->mensagemResumida:Ljava/lang/String;

    return-object v0
.end method

.method public getTitulo_mobile()Ljava/lang/String;
    .registers 2

    .line 38
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/model/AlertaFAQ;->tituloMobile:Ljava/lang/String;

    return-object v0
.end method

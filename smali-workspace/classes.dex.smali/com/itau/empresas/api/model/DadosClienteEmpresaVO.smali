.class public Lcom/itau/empresas/api/model/DadosClienteEmpresaVO;
.super Ljava/lang/Object;
.source "DadosClienteEmpresaVO.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private cnpj:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "cnpj"
    .end annotation
.end field

.field private id_empresa:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "id_empresa"
    .end annotation
.end field

.field private nome:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "nome"
    .end annotation
.end field

.field private novo_cadastro:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "novo_cadastro"
    .end annotation
.end field

.field private segmento:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "segmento"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getSegmento()Ljava/lang/String;
    .registers 2

    .line 25
    iget-object v0, p0, Lcom/itau/empresas/api/model/DadosClienteEmpresaVO;->segmento:Ljava/lang/String;

    return-object v0
.end method

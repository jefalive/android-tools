.class Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal$2;
.super Ljava/lang/Object;
.source "ExtratoFragmentPrincipal.java"

# interfaces
.implements Landroid/support/v4/view/MenuItemCompat$OnActionExpandListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->aoExpandirPesquisa()Landroid/support/v4/view/MenuItemCompat$OnActionExpandListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;


# direct methods
.method constructor <init>(Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;)V
    .registers 2
    .param p1, "this$0"    # Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;

    .line 378
    iput-object p1, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal$2;->this$0:Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onMenuItemActionCollapse(Landroid/view/MenuItem;)Z
    .registers 4
    .param p1, "item"    # Landroid/view/MenuItem;

    .line 390
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal$2;->this$0:Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;

    iget-object v0, v0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->itemMenuFiltro:Landroid/view/MenuItem;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 391
    const/4 v0, 0x1

    return v0
.end method

.method public onMenuItemActionExpand(Landroid/view/MenuItem;)Z
    .registers 6
    .param p1, "item"    # Landroid/view/MenuItem;

    .line 381
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal$2;->this$0:Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;

    iget-object v1, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal$2;->this$0:Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;

    const v2, 0x7f0702b0

    invoke-virtual {v1, v2}, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal$2;->this$0:Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;

    .line 382
    const v3, 0x7f0702ff

    invoke-virtual {v2, v3}, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 381
    invoke-virtual {v0, v1, v2}, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->analyticsHit(Ljava/lang/String;Ljava/lang/String;)V

    .line 384
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal$2;->this$0:Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;

    iget-object v0, v0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->itemMenuFiltro:Landroid/view/MenuItem;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 385
    const/4 v0, 0x1

    return v0
.end method

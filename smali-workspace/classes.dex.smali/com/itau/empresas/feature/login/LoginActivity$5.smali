.class Lcom/itau/empresas/feature/login/LoginActivity$5;
.super Ljava/lang/Object;
.source "LoginActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/itau/empresas/feature/login/LoginActivity;->exibeMensagemError(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/itau/empresas/feature/login/LoginActivity;

.field final synthetic val$mensagem:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/itau/empresas/feature/login/LoginActivity;Ljava/lang/String;)V
    .registers 3
    .param p1, "this$0"    # Lcom/itau/empresas/feature/login/LoginActivity;

    .line 539
    iput-object p1, p0, Lcom/itau/empresas/feature/login/LoginActivity$5;->this$0:Lcom/itau/empresas/feature/login/LoginActivity;

    iput-object p2, p0, Lcom/itau/empresas/feature/login/LoginActivity$5;->val$mensagem:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .registers 4

    .line 542
    iget-object v0, p0, Lcom/itau/empresas/feature/login/LoginActivity$5;->this$0:Lcom/itau/empresas/feature/login/LoginActivity;

    iget-object v0, v0, Lcom/itau/empresas/feature/login/LoginActivity;->root:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/itau/empresas/feature/login/LoginActivity$5;->val$mensagem:Ljava/lang/String;

    const/4 v2, -0x2

    invoke-static {v0, v1, v2}, Lbr/com/itau/topsnackbar/TopSnackbar;->make(Landroid/view/ViewGroup;Ljava/lang/CharSequence;I)Lbr/com/itau/topsnackbar/TopSnackbar;

    move-result-object v0

    invoke-virtual {v0}, Lbr/com/itau/topsnackbar/TopSnackbar;->show()Lbr/com/itau/topsnackbar/TopSnackbar;

    .line 543
    iget-object v0, p0, Lcom/itau/empresas/feature/login/LoginActivity$5;->this$0:Lcom/itau/empresas/feature/login/LoginActivity;

    iget-object v1, p0, Lcom/itau/empresas/feature/login/LoginActivity$5;->this$0:Lcom/itau/empresas/feature/login/LoginActivity;

    iget-object v1, v1, Lcom/itau/empresas/feature/login/LoginActivity;->textoLoginSenha:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    # invokes: Lcom/itau/empresas/feature/login/LoginActivity;->botaoContinuarDeveEstarAcessivel(I)V
    invoke-static {v0, v1}, Lcom/itau/empresas/feature/login/LoginActivity;->access$100(Lcom/itau/empresas/feature/login/LoginActivity;I)V

    .line 544
    iget-object v0, p0, Lcom/itau/empresas/feature/login/LoginActivity$5;->this$0:Lcom/itau/empresas/feature/login/LoginActivity;

    iget-object v0, v0, Lcom/itau/empresas/feature/login/LoginActivity;->botaoAcessar:Lcom/itau/empresas/ui/view/BotaoComProgresso;

    sget-object v1, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/view/BotaoComProgresso;->alternaExibicaoParaProgresso(Z)V

    .line 545
    return-void
.end method

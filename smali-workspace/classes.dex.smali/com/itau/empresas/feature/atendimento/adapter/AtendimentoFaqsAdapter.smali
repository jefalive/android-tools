.class public Lcom/itau/empresas/feature/atendimento/adapter/AtendimentoFaqsAdapter;
.super Landroid/widget/BaseAdapter;
.source "AtendimentoFaqsAdapter.java"


# instance fields
.field private context:Landroid/content/Context;

.field private list:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Lcom/itau/empresas/feature/atendimento/model/AtendimentoFAQ;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/ArrayList;)V
    .registers 3
    .param p1, "mContext"    # Landroid/content/Context;
    .param p2, "list"    # Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Landroid/content/Context;Ljava/util/ArrayList<Lcom/itau/empresas/feature/atendimento/model/AtendimentoFAQ;>;)V"
        }
    .end annotation

    .line 25
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 26
    iput-object p2, p0, Lcom/itau/empresas/feature/atendimento/adapter/AtendimentoFaqsAdapter;->list:Ljava/util/List;

    .line 27
    iput-object p1, p0, Lcom/itau/empresas/feature/atendimento/adapter/AtendimentoFaqsAdapter;->context:Landroid/content/Context;

    .line 28
    return-void
.end method


# virtual methods
.method public getCount()I
    .registers 2

    .line 32
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/adapter/AtendimentoFaqsAdapter;->list:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .registers 3
    .param p1, "position"    # I

    .line 37
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/adapter/AtendimentoFaqsAdapter;->list:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .registers 4
    .param p1, "position"    # I

    .line 42
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 9
    .param p1, "position"    # I
    .param p2, "view"    # Landroid/view/View;
    .param p3, "viewGroup"    # Landroid/view/ViewGroup;

    .line 47
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/adapter/AtendimentoFaqsAdapter;->context:Landroid/content/Context;

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Landroid/view/LayoutInflater;

    .line 49
    .local v2, "inflater":Landroid/view/LayoutInflater;
    const v0, 0x7f0300de

    const/4 v1, 0x0

    invoke-virtual {v2, v0, p3, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    .line 50
    .local v3, "rowView":Landroid/view/View;
    const v0, 0x7f0e04f1

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Landroid/widget/TextView;

    .line 52
    .local v4, "faq":Landroid/widget/TextView;
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/adapter/AtendimentoFaqsAdapter;->list:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/feature/atendimento/model/AtendimentoFAQ;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/atendimento/model/AtendimentoFAQ;->getPergunta()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 54
    return-object v3
.end method

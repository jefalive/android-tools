.class public Lbr/com/itau/sdk/android/core/model/SdkDataDTO;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private keyAccess:Lbr/com/itau/sdk/android/core/login/model/a;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "chave_acesso"
    .end annotation
.end field

.field private remoteBundle:Lbr/com/itau/sdk/android/core/model/RemoteBundleDTO;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "remoteBundle"
    .end annotation
.end field

.field private versionControl:Lbr/com/itau/sdk/android/core/model/VersionControlDTO;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "versionControl"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getKeyAccess()Lbr/com/itau/sdk/android/core/login/model/a;
    .registers 2

    .line 28
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/model/SdkDataDTO;->keyAccess:Lbr/com/itau/sdk/android/core/login/model/a;

    return-object v0
.end method

.method public getRemoteBundle()Lbr/com/itau/sdk/android/core/model/RemoteBundleDTO;
    .registers 2

    .line 24
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/model/SdkDataDTO;->remoteBundle:Lbr/com/itau/sdk/android/core/model/RemoteBundleDTO;

    return-object v0
.end method

.method public getVersionControl()Lbr/com/itau/sdk/android/core/model/VersionControlDTO;
    .registers 2

    .line 20
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/model/SdkDataDTO;->versionControl:Lbr/com/itau/sdk/android/core/model/VersionControlDTO;

    return-object v0
.end method

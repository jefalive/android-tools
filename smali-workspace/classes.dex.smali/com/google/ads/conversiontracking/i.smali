.class public final Lcom/google/ads/conversiontracking/i;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/ads/conversiontracking/i$a;
    }
.end annotation


# direct methods
.method public static a(Landroid/content/Context;)Lcom/google/ads/conversiontracking/i$a;
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/IllegalStateException;,
            Lcom/google/ads/conversiontracking/j;,
            Lcom/google/ads/conversiontracking/k;
        }
    .end annotation

    const-string v0, "Calling this from your main thread can lead to deadlock"

    invoke-static {v0}, Lcom/google/ads/conversiontracking/p;->a(Ljava/lang/String;)V

    invoke-static {p0}, Lcom/google/ads/conversiontracking/i;->b(Landroid/content/Context;)Lcom/google/ads/conversiontracking/n;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/google/ads/conversiontracking/i;->a(Landroid/content/Context;Lcom/google/ads/conversiontracking/n;)Lcom/google/ads/conversiontracking/i$a;

    move-result-object v0

    return-object v0
.end method

.method static a(Landroid/content/Context;Lcom/google/ads/conversiontracking/n;)Lcom/google/ads/conversiontracking/i$a;
    .registers 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :try_start_0
    invoke-virtual {p1}, Lcom/google/ads/conversiontracking/n;->a()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/google/ads/conversiontracking/q$a;->a(Landroid/os/IBinder;)Lcom/google/ads/conversiontracking/q;

    move-result-object v2

    new-instance v3, Lcom/google/ads/conversiontracking/i$a;

    invoke-interface {v2}, Lcom/google/ads/conversiontracking/q;->a()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v2, v1}, Lcom/google/ads/conversiontracking/q;->a(Z)Z

    move-result v1

    invoke-direct {v3, v0, v1}, Lcom/google/ads/conversiontracking/i$a;-><init>(Ljava/lang/String;Z)V
    :try_end_16
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_16} :catch_23
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_16} :catch_33
    .catchall {:try_start_0 .. :try_end_16} :catchall_3c

    :try_start_16
    invoke-virtual {p0, p1}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V
    :try_end_19
    .catch Ljava/lang/IllegalArgumentException; {:try_start_16 .. :try_end_19} :catch_1a

    goto :goto_22

    :catch_1a
    move-exception v4

    const-string v0, "AdvertisingIdClient"

    const-string v1, "getAdvertisingIdInfo unbindService failed."

    invoke-static {v0, v1, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :goto_22
    return-object v3

    :catch_23
    move-exception v2

    const-string v0, "AdvertisingIdClient"

    const-string v1, "GMS remote exception "

    :try_start_28
    invoke-static {v0, v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    new-instance v0, Ljava/io/IOException;

    const-string v1, "Remote exception"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :catch_33
    move-exception v2

    new-instance v0, Ljava/io/IOException;

    const-string v1, "Interrupted exception"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_3c
    .catchall {:try_start_28 .. :try_end_3c} :catchall_3c

    :catchall_3c
    move-exception v5

    :try_start_3d
    invoke-virtual {p0, p1}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V
    :try_end_40
    .catch Ljava/lang/IllegalArgumentException; {:try_start_3d .. :try_end_40} :catch_41

    goto :goto_49

    :catch_41
    move-exception v6

    const-string v0, "AdvertisingIdClient"

    const-string v1, "getAdvertisingIdInfo unbindService failed."

    invoke-static {v0, v1, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :goto_49
    throw v5
.end method

.method private static b(Landroid/content/Context;)Lcom/google/ads/conversiontracking/n;
    .registers 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/google/ads/conversiontracking/j;,
            Lcom/google/ads/conversiontracking/k;
        }
    .end annotation

    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    const-string v0, "com.android.vending"

    const/4 v1, 0x0

    invoke-virtual {v2, v0, v1}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_a
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_a} :catch_b

    goto :goto_14

    :catch_b
    move-exception v2

    new-instance v0, Lcom/google/ads/conversiontracking/j;

    const/16 v1, 0x9

    invoke-direct {v0, v1}, Lcom/google/ads/conversiontracking/j;-><init>(I)V

    throw v0

    :goto_14
    :try_start_14
    invoke-static {p0}, Lcom/google/ads/conversiontracking/l;->b(Landroid/content/Context;)V
    :try_end_17
    .catch Lcom/google/ads/conversiontracking/j; {:try_start_14 .. :try_end_17} :catch_18

    goto :goto_1f

    :catch_18
    move-exception v2

    new-instance v0, Ljava/io/IOException;

    invoke-direct {v0, v2}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v0

    :goto_1f
    new-instance v2, Lcom/google/ads/conversiontracking/n;

    invoke-direct {v2}, Lcom/google/ads/conversiontracking/n;-><init>()V

    new-instance v3, Landroid/content/Intent;

    const-string v0, "com.google.android.gms.ads.identifier.service.START"

    invoke-direct {v3, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v0, "com.google.android.gms"

    invoke-virtual {v3, v0}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    const/4 v0, 0x1

    invoke-virtual {p0, v3, v2, v0}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v0

    if-eqz v0, :cond_38

    return-object v2

    :cond_38
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Connection failure"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

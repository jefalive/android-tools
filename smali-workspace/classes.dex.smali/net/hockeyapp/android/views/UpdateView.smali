.class public Lnet/hockeyapp/android/views/UpdateView;
.super Landroid/widget/RelativeLayout;
.source "UpdateView.java"


# instance fields
.field protected headerView:Landroid/widget/RelativeLayout;

.field protected layoutHorizontally:Z

.field protected limitHeight:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .param p1, "context"    # Landroid/content/Context;

    .line 67
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lnet/hockeyapp/android/views/UpdateView;-><init>(Landroid/content/Context;Z)V

    .line 68
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .line 71
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lnet/hockeyapp/android/views/UpdateView;-><init>(Landroid/content/Context;ZZ)V

    .line 72
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Z)V
    .registers 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "allowHorizontalLayout"    # Z

    .line 75
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lnet/hockeyapp/android/views/UpdateView;-><init>(Landroid/content/Context;ZZ)V

    .line 76
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ZZ)V
    .registers 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "allowHorizontalLayout"    # Z
    .param p3, "limitHeight"    # Z

    .line 79
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 62
    const/4 v0, 0x0

    iput-object v0, p0, Lnet/hockeyapp/android/views/UpdateView;->headerView:Landroid/widget/RelativeLayout;

    .line 63
    const/4 v0, 0x0

    iput-boolean v0, p0, Lnet/hockeyapp/android/views/UpdateView;->layoutHorizontally:Z

    .line 64
    const/4 v0, 0x0

    iput-boolean v0, p0, Lnet/hockeyapp/android/views/UpdateView;->limitHeight:Z

    .line 81
    if-eqz p2, :cond_12

    .line 82
    invoke-direct {p0, p1}, Lnet/hockeyapp/android/views/UpdateView;->setLayoutHorizontally(Landroid/content/Context;)V

    goto :goto_15

    .line 85
    :cond_12
    const/4 v0, 0x0

    iput-boolean v0, p0, Lnet/hockeyapp/android/views/UpdateView;->layoutHorizontally:Z

    .line 87
    :goto_15
    iput-boolean p3, p0, Lnet/hockeyapp/android/views/UpdateView;->limitHeight:Z

    .line 89
    invoke-direct {p0, p1}, Lnet/hockeyapp/android/views/UpdateView;->loadLayoutParams(Landroid/content/Context;)V

    .line 90
    invoke-direct {p0, p1}, Lnet/hockeyapp/android/views/UpdateView;->loadHeaderView(Landroid/content/Context;)V

    .line 91
    invoke-direct {p0, p1}, Lnet/hockeyapp/android/views/UpdateView;->loadWebView(Landroid/content/Context;)V

    .line 92
    iget-object v0, p0, Lnet/hockeyapp/android/views/UpdateView;->headerView:Landroid/widget/RelativeLayout;

    invoke-direct {p0, v0, p1}, Lnet/hockeyapp/android/views/UpdateView;->loadShadow(Landroid/widget/RelativeLayout;Landroid/content/Context;)V

    .line 93
    return-void
.end method

.method private getButtonSelector()Landroid/graphics/drawable/Drawable;
    .registers 5

    .line 196
    new-instance v3, Landroid/graphics/drawable/StateListDrawable;

    invoke-direct {v3}, Landroid/graphics/drawable/StateListDrawable;-><init>()V

    .line 197
    .local v3, "drawable":Landroid/graphics/drawable/StateListDrawable;
    const/4 v0, 0x1

    new-array v0, v0, [I

    fill-array-data v0, :array_38

    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    const/high16 v2, -0x1000000

    invoke-direct {v1, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v3, v0, v1}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 198
    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_3e

    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    const v2, -0xbbbbbc

    invoke-direct {v1, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v3, v0, v1}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 199
    const/4 v0, 0x1

    new-array v0, v0, [I

    fill-array-data v0, :array_46

    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    const v2, -0x777778

    invoke-direct {v1, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v3, v0, v1}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 200
    return-object v3

    :array_38
    .array-data 4
        -0x10100a7
    .end array-data

    :array_3e
    .array-data 4
        -0x10100a7
        0x101009c
    .end array-data

    :array_46
    .array-data 4
        0x10100a7
    .end array-data
.end method

.method private loadHeaderView(Landroid/content/Context;)V
    .registers 8
    .param p1, "context"    # Landroid/content/Context;

    .line 112
    new-instance v0, Landroid/widget/RelativeLayout;

    invoke-direct {v0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lnet/hockeyapp/android/views/UpdateView;->headerView:Landroid/widget/RelativeLayout;

    .line 113
    iget-object v0, p0, Lnet/hockeyapp/android/views/UpdateView;->headerView:Landroid/widget/RelativeLayout;

    const/16 v1, 0x1001

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setId(I)V

    .line 115
    const/4 v5, 0x0

    .line 116
    .local v5, "params":Landroid/widget/RelativeLayout$LayoutParams;
    iget-boolean v0, p0, Lnet/hockeyapp/android/views/UpdateView;->layoutHorizontally:Z

    if-eqz v0, :cond_39

    .line 117
    new-instance v5, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {p0}, Lnet/hockeyapp/android/views/UpdateView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    const/4 v1, 0x1

    const/high16 v2, 0x437a0000    # 250.0f

    invoke-static {v1, v2, v0}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    float-to-int v0, v0

    const/4 v1, -0x1

    invoke-direct {v5, v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 118
    const/16 v0, 0x9

    const/4 v1, -0x1

    invoke-virtual {v5, v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 119
    iget-object v0, p0, Lnet/hockeyapp/android/views/UpdateView;->headerView:Landroid/widget/RelativeLayout;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/RelativeLayout;->setPadding(IIII)V

    goto :goto_58

    .line 122
    :cond_39
    new-instance v5, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v0, -0x1

    const/4 v1, -0x2

    invoke-direct {v5, v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 123
    iget-object v0, p0, Lnet/hockeyapp/android/views/UpdateView;->headerView:Landroid/widget/RelativeLayout;

    invoke-virtual {p0}, Lnet/hockeyapp/android/views/UpdateView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    const/4 v2, 0x1

    const/high16 v3, 0x41a00000    # 20.0f

    invoke-static {v2, v3, v1}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v1

    float-to-int v1, v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v0, v2, v3, v4, v1}, Landroid/widget/RelativeLayout;->setPadding(IIII)V

    .line 125
    :goto_58
    iget-object v0, p0, Lnet/hockeyapp/android/views/UpdateView;->headerView:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v5}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 126
    iget-object v0, p0, Lnet/hockeyapp/android/views/UpdateView;->headerView:Landroid/widget/RelativeLayout;

    const/16 v1, 0xe6

    const/16 v2, 0xec

    const/16 v3, 0xef

    invoke-static {v1, v2, v3}, Landroid/graphics/Color;->rgb(III)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setBackgroundColor(I)V

    .line 128
    iget-object v0, p0, Lnet/hockeyapp/android/views/UpdateView;->headerView:Landroid/widget/RelativeLayout;

    invoke-direct {p0, v0, p1}, Lnet/hockeyapp/android/views/UpdateView;->loadTitleLabel(Landroid/widget/RelativeLayout;Landroid/content/Context;)V

    .line 129
    iget-object v0, p0, Lnet/hockeyapp/android/views/UpdateView;->headerView:Landroid/widget/RelativeLayout;

    invoke-direct {p0, v0, p1}, Lnet/hockeyapp/android/views/UpdateView;->loadVersionLabel(Landroid/widget/RelativeLayout;Landroid/content/Context;)V

    .line 130
    iget-object v0, p0, Lnet/hockeyapp/android/views/UpdateView;->headerView:Landroid/widget/RelativeLayout;

    invoke-direct {p0, v0, p1}, Lnet/hockeyapp/android/views/UpdateView;->loadUpdateButton(Landroid/widget/RelativeLayout;Landroid/content/Context;)V

    .line 132
    iget-object v0, p0, Lnet/hockeyapp/android/views/UpdateView;->headerView:Landroid/widget/RelativeLayout;

    invoke-virtual {p0, v0}, Lnet/hockeyapp/android/views/UpdateView;->addView(Landroid/view/View;)V

    .line 133
    return-void
.end method

.method private loadLayoutParams(Landroid/content/Context;)V
    .registers 5
    .param p1, "context"    # Landroid/content/Context;

    .line 106
    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v0, -0x1

    const/4 v1, -0x2

    invoke-direct {v2, v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 107
    .local v2, "params":Landroid/widget/RelativeLayout$LayoutParams;
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lnet/hockeyapp/android/views/UpdateView;->setBackgroundColor(I)V

    .line 108
    invoke-virtual {p0, v2}, Lnet/hockeyapp/android/views/UpdateView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 109
    return-void
.end method

.method private loadShadow(Landroid/widget/RelativeLayout;Landroid/content/Context;)V
    .registers 10
    .param p1, "headerView"    # Landroid/widget/RelativeLayout;
    .param p2, "context"    # Landroid/content/Context;

    .line 205
    invoke-virtual {p0}, Lnet/hockeyapp/android/views/UpdateView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    const/4 v1, 0x1

    const/high16 v2, 0x40400000    # 3.0f

    invoke-static {v1, v2, v0}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    float-to-int v3, v0

    .line 206
    .local v3, "height":I
    const/4 v4, 0x0

    .line 208
    .local v4, "params":Landroid/widget/RelativeLayout$LayoutParams;
    new-instance v5, Landroid/widget/ImageView;

    invoke-direct {v5, p2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 209
    .local v5, "topShadowView":Landroid/widget/ImageView;
    iget-boolean v0, p0, Lnet/hockeyapp/android/views/UpdateView;->layoutHorizontally:Z

    if-eqz v0, :cond_32

    .line 210
    new-instance v4, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v0, 0x1

    const/4 v1, -0x1

    invoke-direct {v4, v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 211
    const/16 v0, 0xb

    const/4 v1, -0x1

    invoke-virtual {v4, v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 212
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    const/high16 v1, -0x1000000

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v5, v0}, Landroid/widget/ImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_45

    .line 215
    :cond_32
    new-instance v4, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v0, -0x1

    invoke-direct {v4, v0, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 216
    const/16 v0, 0xa

    const/4 v1, -0x1

    invoke-virtual {v4, v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 217
    invoke-static {}, Lnet/hockeyapp/android/utils/ViewHelper;->getGradient()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v5, v0}, Landroid/widget/ImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 219
    :goto_45
    invoke-virtual {v5, v4}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 221
    invoke-virtual {p1, v5}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 223
    new-instance v6, Landroid/widget/ImageView;

    invoke-direct {v6, p2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 224
    .local v6, "bottomShadowView":Landroid/widget/ImageView;
    new-instance v4, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v0, -0x1

    invoke-direct {v4, v0, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 225
    iget-boolean v0, p0, Lnet/hockeyapp/android/views/UpdateView;->layoutHorizontally:Z

    if-eqz v0, :cond_61

    .line 226
    const/16 v0, 0xa

    const/4 v1, -0x1

    invoke-virtual {v4, v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    goto :goto_67

    .line 229
    :cond_61
    const/4 v0, 0x3

    const/16 v1, 0x1001

    invoke-virtual {v4, v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 231
    :goto_67
    invoke-virtual {v6, v4}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 232
    invoke-static {}, Lnet/hockeyapp/android/utils/ViewHelper;->getGradient()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v6, v0}, Landroid/widget/ImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 234
    invoke-virtual {p0, v6}, Lnet/hockeyapp/android/views/UpdateView;->addView(Landroid/view/View;)V

    .line 235
    return-void
.end method

.method private loadTitleLabel(Landroid/widget/RelativeLayout;Landroid/content/Context;)V
    .registers 10
    .param p1, "headerView"    # Landroid/widget/RelativeLayout;
    .param p2, "context"    # Landroid/content/Context;

    .line 136
    new-instance v4, Landroid/widget/TextView;

    invoke-direct {v4, p2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 137
    .local v4, "textView":Landroid/widget/TextView;
    const/16 v0, 0x1002

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setId(I)V

    .line 139
    new-instance v5, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v0, -0x2

    const/4 v1, -0x2

    invoke-direct {v5, v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 140
    .local v5, "params":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {p0}, Lnet/hockeyapp/android/views/UpdateView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    const/4 v1, 0x1

    const/high16 v2, 0x41a00000    # 20.0f

    invoke-static {v1, v2, v0}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    float-to-int v6, v0

    .line 141
    .local v6, "margin":I
    const/4 v0, 0x0

    invoke-virtual {v5, v6, v6, v6, v0}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 142
    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 143
    sget-object v0, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 144
    const/high16 v0, 0x3f800000    # 1.0f

    const/4 v1, 0x0

    const/high16 v2, 0x3f800000    # 1.0f

    const/4 v3, -0x1

    invoke-virtual {v4, v0, v1, v2, v3}, Landroid/widget/TextView;->setShadowLayer(FFFI)V

    .line 145
    const/4 v0, 0x1

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setSingleLine(Z)V

    .line 146
    const/high16 v0, -0x1000000

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 147
    const/4 v0, 0x2

    const/high16 v1, 0x41a00000    # 20.0f

    invoke-virtual {v4, v0, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 148
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {v4, v0, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 150
    invoke-virtual {p1, v4}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 151
    return-void
.end method

.method private loadUpdateButton(Landroid/widget/RelativeLayout;Landroid/content/Context;)V
    .registers 10
    .param p1, "headerView"    # Landroid/widget/RelativeLayout;
    .param p2, "context"    # Landroid/content/Context;

    .line 176
    new-instance v3, Landroid/widget/Button;

    invoke-direct {v3, p2}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    .line 177
    .local v3, "button":Landroid/widget/Button;
    const/16 v0, 0x1004

    invoke-virtual {v3, v0}, Landroid/widget/Button;->setId(I)V

    .line 179
    invoke-virtual {p0}, Lnet/hockeyapp/android/views/UpdateView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    const/4 v1, 0x1

    const/high16 v2, 0x41a00000    # 20.0f

    invoke-static {v1, v2, v0}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    float-to-int v4, v0

    .line 180
    .local v4, "margin":I
    invoke-virtual {p0}, Lnet/hockeyapp/android/views/UpdateView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    const/4 v1, 0x1

    const/high16 v2, 0x42f00000    # 120.0f

    invoke-static {v1, v2, v0}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    float-to-int v5, v0

    .line 182
    .local v5, "width":I
    new-instance v6, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v0, -0x2

    invoke-direct {v6, v5, v0}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 183
    .local v6, "params":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {v6, v4, v4, v4, v4}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 184
    const/16 v0, 0x9

    const/4 v1, -0x1

    invoke-virtual {v6, v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 185
    const/4 v0, 0x3

    const/16 v1, 0x1003

    invoke-virtual {v6, v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 186
    invoke-virtual {v3, v6}, Landroid/widget/Button;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 187
    invoke-direct {p0}, Lnet/hockeyapp/android/views/UpdateView;->getButtonSelector()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/widget/Button;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 188
    const-string v0, "Update"

    invoke-virtual {v3, v0}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 189
    const/4 v0, -0x1

    invoke-virtual {v3, v0}, Landroid/widget/Button;->setTextColor(I)V

    .line 190
    const/4 v0, 0x2

    const/high16 v1, 0x41800000    # 16.0f

    invoke-virtual {v3, v0, v1}, Landroid/widget/Button;->setTextSize(IF)V

    .line 192
    invoke-virtual {p1, v3}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 193
    return-void
.end method

.method private loadVersionLabel(Landroid/widget/RelativeLayout;Landroid/content/Context;)V
    .registers 11
    .param p1, "headerView"    # Landroid/widget/RelativeLayout;
    .param p2, "context"    # Landroid/content/Context;

    .line 154
    new-instance v4, Landroid/widget/TextView;

    invoke-direct {v4, p2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 155
    .local v4, "textView":Landroid/widget/TextView;
    const/16 v0, 0x1003

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setId(I)V

    .line 157
    new-instance v5, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v0, -0x2

    const/4 v1, -0x2

    invoke-direct {v5, v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 158
    .local v5, "params":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {p0}, Lnet/hockeyapp/android/views/UpdateView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    const/4 v1, 0x1

    const/high16 v2, 0x41a00000    # 20.0f

    invoke-static {v1, v2, v0}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    float-to-int v6, v0

    .line 159
    .local v6, "marginSide":I
    invoke-virtual {p0}, Lnet/hockeyapp/android/views/UpdateView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    const/4 v1, 0x1

    const/high16 v2, 0x41200000    # 10.0f

    invoke-static {v1, v2, v0}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    float-to-int v7, v0

    .line 160
    .local v7, "marginTop":I
    const/4 v0, 0x0

    invoke-virtual {v5, v6, v7, v6, v0}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 161
    const/4 v0, 0x3

    const/16 v1, 0x1002

    invoke-virtual {v5, v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 162
    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 163
    sget-object v0, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 164
    const/high16 v0, 0x3f800000    # 1.0f

    const/4 v1, 0x0

    const/high16 v2, 0x3f800000    # 1.0f

    const/4 v3, -0x1

    invoke-virtual {v4, v0, v1, v2, v3}, Landroid/widget/TextView;->setShadowLayer(FFFI)V

    .line 165
    const/4 v0, 0x2

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setLines(I)V

    .line 166
    const/4 v0, 0x0

    const v1, 0x3f8ccccd    # 1.1f

    invoke-virtual {v4, v0, v1}, Landroid/widget/TextView;->setLineSpacing(FF)V

    .line 167
    const/high16 v0, -0x1000000

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 168
    const/4 v0, 0x2

    const/high16 v1, 0x41800000    # 16.0f

    invoke-virtual {v4, v0, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 169
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {v4, v0, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 171
    invoke-virtual {p1, v4}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 172
    return-void
.end method

.method private loadWebView(Landroid/content/Context;)V
    .registers 9
    .param p1, "context"    # Landroid/content/Context;

    .line 238
    new-instance v4, Landroid/webkit/WebView;

    invoke-direct {v4, p1}, Landroid/webkit/WebView;-><init>(Landroid/content/Context;)V

    .line 239
    .local v4, "webView":Landroid/webkit/WebView;
    const/16 v0, 0x1005

    invoke-virtual {v4, v0}, Landroid/webkit/WebView;->setId(I)V

    .line 241
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    const/4 v1, 0x1

    const/high16 v2, 0x43c80000    # 400.0f

    invoke-static {v1, v2, v0}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    float-to-int v5, v0

    .line 242
    .local v5, "height":I
    new-instance v6, Landroid/widget/RelativeLayout$LayoutParams;

    iget-boolean v0, p0, Lnet/hockeyapp/android/views/UpdateView;->limitHeight:Z

    if-eqz v0, :cond_22

    move v0, v5

    goto :goto_23

    :cond_22
    const/4 v0, -0x1

    :goto_23
    const/4 v1, -0x1

    invoke-direct {v6, v1, v0}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 243
    .local v6, "params":Landroid/widget/RelativeLayout$LayoutParams;
    iget-boolean v0, p0, Lnet/hockeyapp/android/views/UpdateView;->layoutHorizontally:Z

    if-eqz v0, :cond_32

    .line 244
    const/4 v0, 0x1

    const/16 v1, 0x1001

    invoke-virtual {v6, v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    goto :goto_38

    .line 247
    :cond_32
    const/4 v0, 0x3

    const/16 v1, 0x1001

    invoke-virtual {v6, v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 249
    :goto_38
    const/4 v0, 0x0

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v6, v0, v1, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 250
    invoke-virtual {v4, v6}, Landroid/webkit/WebView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 251
    const/4 v0, -0x1

    invoke-virtual {v4, v0}, Landroid/webkit/WebView;->setBackgroundColor(I)V

    .line 253
    invoke-virtual {p0, v4}, Lnet/hockeyapp/android/views/UpdateView;->addView(Landroid/view/View;)V

    .line 254
    return-void
.end method

.method private setLayoutHorizontally(Landroid/content/Context;)V
    .registers 4
    .param p1, "context"    # Landroid/content/Context;

    .line 96
    invoke-virtual {p0}, Lnet/hockeyapp/android/views/UpdateView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v1, v0, Landroid/content/res/Configuration;->orientation:I

    .line 97
    .local v1, "orientation":I
    const/4 v0, 0x2

    if-ne v1, v0, :cond_11

    .line 98
    const/4 v0, 0x1

    iput-boolean v0, p0, Lnet/hockeyapp/android/views/UpdateView;->layoutHorizontally:Z

    goto :goto_14

    .line 101
    :cond_11
    const/4 v0, 0x0

    iput-boolean v0, p0, Lnet/hockeyapp/android/views/UpdateView;->layoutHorizontally:Z

    .line 103
    :goto_14
    return-void
.end method

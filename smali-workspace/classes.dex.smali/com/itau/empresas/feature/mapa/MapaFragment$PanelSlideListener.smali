.class Lcom/itau/empresas/feature/mapa/MapaFragment$PanelSlideListener;
.super Ljava/lang/Object;
.source "MapaFragment.java"

# interfaces
.implements Lcom/sothree/slidinguppanel/SlidingUpPanelLayout$PanelSlideListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/itau/empresas/feature/mapa/MapaFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PanelSlideListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/itau/empresas/feature/mapa/MapaFragment;


# direct methods
.method private constructor <init>(Lcom/itau/empresas/feature/mapa/MapaFragment;)V
    .registers 2

    .line 449
    iput-object p1, p0, Lcom/itau/empresas/feature/mapa/MapaFragment$PanelSlideListener;->this$0:Lcom/itau/empresas/feature/mapa/MapaFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/itau/empresas/feature/mapa/MapaFragment;Lcom/itau/empresas/feature/mapa/MapaFragment$1;)V
    .registers 3
    .param p1, "x0"    # Lcom/itau/empresas/feature/mapa/MapaFragment;
    .param p2, "x1"    # Lcom/itau/empresas/feature/mapa/MapaFragment$1;

    .line 449
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/mapa/MapaFragment$PanelSlideListener;-><init>(Lcom/itau/empresas/feature/mapa/MapaFragment;)V

    return-void
.end method


# virtual methods
.method public onPanelAnchored(Landroid/view/View;)V
    .registers 2
    .param p1, "view"    # Landroid/view/View;

    .line 473
    return-void
.end method

.method public onPanelCollapsed(Landroid/view/View;)V
    .registers 5
    .param p1, "view"    # Landroid/view/View;

    .line 460
    iget-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaFragment$PanelSlideListener;->this$0:Lcom/itau/empresas/feature/mapa/MapaFragment;

    iget-object v0, v0, Lcom/itau/empresas/feature/mapa/MapaFragment;->llMapaTitulo:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/itau/empresas/feature/mapa/MapaFragment$PanelSlideListener;->this$0:Lcom/itau/empresas/feature/mapa/MapaFragment;

    invoke-virtual {v1}, Lcom/itau/empresas/feature/mapa/MapaFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c0022

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundColor(I)V

    .line 461
    iget-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaFragment$PanelSlideListener;->this$0:Lcom/itau/empresas/feature/mapa/MapaFragment;

    iget-object v0, v0, Lcom/itau/empresas/feature/mapa/MapaFragment;->textoNomeAgencia:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/itau/empresas/feature/mapa/MapaFragment$PanelSlideListener;->this$0:Lcom/itau/empresas/feature/mapa/MapaFragment;

    invoke-virtual {v1}, Lcom/itau/empresas/feature/mapa/MapaFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c0035

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 462
    return-void
.end method

.method public onPanelExpanded(Landroid/view/View;)V
    .registers 5
    .param p1, "view"    # Landroid/view/View;

    .line 466
    iget-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaFragment$PanelSlideListener;->this$0:Lcom/itau/empresas/feature/mapa/MapaFragment;

    iget-object v0, v0, Lcom/itau/empresas/feature/mapa/MapaFragment;->llMapaTitulo:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/itau/empresas/feature/mapa/MapaFragment$PanelSlideListener;->this$0:Lcom/itau/empresas/feature/mapa/MapaFragment;

    invoke-virtual {v1}, Lcom/itau/empresas/feature/mapa/MapaFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c00cd

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundColor(I)V

    .line 467
    iget-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaFragment$PanelSlideListener;->this$0:Lcom/itau/empresas/feature/mapa/MapaFragment;

    iget-object v0, v0, Lcom/itau/empresas/feature/mapa/MapaFragment;->textoNomeAgencia:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/itau/empresas/feature/mapa/MapaFragment$PanelSlideListener;->this$0:Lcom/itau/empresas/feature/mapa/MapaFragment;

    invoke-virtual {v1}, Lcom/itau/empresas/feature/mapa/MapaFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c0022

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 468
    return-void
.end method

.method public onPanelHidden(Landroid/view/View;)V
    .registers 5
    .param p1, "view"    # Landroid/view/View;

    .line 477
    iget-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaFragment$PanelSlideListener;->this$0:Lcom/itau/empresas/feature/mapa/MapaFragment;

    iget-object v0, v0, Lcom/itau/empresas/feature/mapa/MapaFragment;->llMapaTitulo:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/itau/empresas/feature/mapa/MapaFragment$PanelSlideListener;->this$0:Lcom/itau/empresas/feature/mapa/MapaFragment;

    invoke-virtual {v1}, Lcom/itau/empresas/feature/mapa/MapaFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c0022

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundColor(I)V

    .line 478
    iget-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaFragment$PanelSlideListener;->this$0:Lcom/itau/empresas/feature/mapa/MapaFragment;

    iget-object v0, v0, Lcom/itau/empresas/feature/mapa/MapaFragment;->textoNomeAgencia:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/itau/empresas/feature/mapa/MapaFragment$PanelSlideListener;->this$0:Lcom/itau/empresas/feature/mapa/MapaFragment;

    invoke-virtual {v1}, Lcom/itau/empresas/feature/mapa/MapaFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c0035

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 479
    return-void
.end method

.method public onPanelSlide(Landroid/view/View;F)V
    .registers 7
    .param p1, "view"    # Landroid/view/View;
    .param p2, "v"    # F

    .line 452
    float-to-double v0, p2

    const-wide/high16 v2, 0x3fd0000000000000L    # 0.25

    cmpl-double v0, v0, v2

    if-ltz v0, :cond_36

    float-to-double v0, p2

    const-wide/high16 v2, 0x3fe8000000000000L    # 0.75

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_36

    .line 453
    iget-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaFragment$PanelSlideListener;->this$0:Lcom/itau/empresas/feature/mapa/MapaFragment;

    iget-object v0, v0, Lcom/itau/empresas/feature/mapa/MapaFragment;->llMapaTitulo:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/itau/empresas/feature/mapa/MapaFragment$PanelSlideListener;->this$0:Lcom/itau/empresas/feature/mapa/MapaFragment;

    invoke-virtual {v1}, Lcom/itau/empresas/feature/mapa/MapaFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c00cd

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundColor(I)V

    .line 454
    iget-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaFragment$PanelSlideListener;->this$0:Lcom/itau/empresas/feature/mapa/MapaFragment;

    iget-object v0, v0, Lcom/itau/empresas/feature/mapa/MapaFragment;->textoNomeAgencia:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/itau/empresas/feature/mapa/MapaFragment$PanelSlideListener;->this$0:Lcom/itau/empresas/feature/mapa/MapaFragment;

    invoke-virtual {v1}, Lcom/itau/empresas/feature/mapa/MapaFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c0022

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 456
    :cond_36
    return-void
.end method

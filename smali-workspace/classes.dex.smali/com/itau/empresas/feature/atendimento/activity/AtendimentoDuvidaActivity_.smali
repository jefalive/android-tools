.class public final Lcom/itau/empresas/feature/atendimento/activity/AtendimentoDuvidaActivity_;
.super Lcom/itau/empresas/feature/atendimento/activity/AtendimentoDuvidaActivity;
.source "AtendimentoDuvidaActivity_.java"

# interfaces
.implements Lorg/androidannotations/api/view/HasViews;
.implements Lorg/androidannotations/api/view/OnViewChangedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/itau/empresas/feature/atendimento/activity/AtendimentoDuvidaActivity_$IntentBuilder_;
    }
.end annotation


# instance fields
.field private final onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;


# direct methods
.method public constructor <init>()V
    .registers 2

    .line 33
    invoke-direct {p0}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoDuvidaActivity;-><init>()V

    .line 37
    new-instance v0, Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-direct {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoDuvidaActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    return-void
.end method

.method static synthetic access$001(Lcom/itau/empresas/feature/atendimento/activity/AtendimentoDuvidaActivity_;)V
    .registers 1
    .param p0, "x0"    # Lcom/itau/empresas/feature/atendimento/activity/AtendimentoDuvidaActivity_;

    .line 33
    invoke-super {p0}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoDuvidaActivity;->mostraDialogoDeProgresso()V

    return-void
.end method

.method static synthetic access$101(Lcom/itau/empresas/feature/atendimento/activity/AtendimentoDuvidaActivity_;)V
    .registers 1
    .param p0, "x0"    # Lcom/itau/empresas/feature/atendimento/activity/AtendimentoDuvidaActivity_;

    .line 33
    invoke-super {p0}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoDuvidaActivity;->escondeDialogoDeProgresso()V

    return-void
.end method

.method private init_(Landroid/os/Bundle;)V
    .registers 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 49
    new-instance v0, Lcom/itau/empresas/Preferences_;

    invoke-direct {v0, p0}, Lcom/itau/empresas/Preferences_;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoDuvidaActivity_;->prefs:Lcom/itau/empresas/Preferences_;

    .line 50
    invoke-static {p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->registerOnViewChangedListener(Lorg/androidannotations/api/view/OnViewChangedListener;)V

    .line 51
    invoke-static {}, Lcom/itau/empresas/CustomApplication_;->getInstance()Lcom/itau/empresas/CustomApplication;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoDuvidaActivity_;->application:Lcom/itau/empresas/CustomApplication;

    .line 52
    invoke-static {p0}, Lcom/itau/empresas/feature/atendimento/controller/AtendimentoController_;->getInstance_(Landroid/content/Context;)Lcom/itau/empresas/feature/atendimento/controller/AtendimentoController_;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoDuvidaActivity_;->controller:Lcom/itau/empresas/feature/atendimento/controller/AtendimentoController;

    .line 53
    invoke-virtual {p0}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoDuvidaActivity_;->afterInject()V

    .line 54
    return-void
.end method


# virtual methods
.method public escondeDialogoDeProgresso()V
    .registers 5

    .line 111
    const-string v0, ""

    new-instance v1, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoDuvidaActivity_$2;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoDuvidaActivity_$2;-><init>(Lcom/itau/empresas/feature/atendimento/activity/AtendimentoDuvidaActivity_;)V

    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3}, Lorg/androidannotations/api/UiThreadExecutor;->runTask(Ljava/lang/String;Ljava/lang/Runnable;J)V

    .line 119
    return-void
.end method

.method public mostraDialogoDeProgresso()V
    .registers 5
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "CommitTransaction"
        }
    .end annotation

    .line 99
    const-string v0, ""

    new-instance v1, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoDuvidaActivity_$1;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoDuvidaActivity_$1;-><init>(Lcom/itau/empresas/feature/atendimento/activity/AtendimentoDuvidaActivity_;)V

    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3}, Lorg/androidannotations/api/UiThreadExecutor;->runTask(Ljava/lang/String;Ljava/lang/Runnable;J)V

    .line 107
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 41
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoDuvidaActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-static {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    move-result-object v1

    .line 42
    .local v1, "previousNotifier":Lorg/androidannotations/api/view/OnViewChangedNotifier;
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoDuvidaActivity_;->init_(Landroid/os/Bundle;)V

    .line 43
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoDuvidaActivity;->onCreate(Landroid/os/Bundle;)V

    .line 44
    invoke-static {v1}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 45
    const v0, 0x7f030020

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoDuvidaActivity_;->setContentView(I)V

    .line 46
    return-void
.end method

.method public onViewChanged(Lorg/androidannotations/api/view/HasViews;)V
    .registers 3
    .param p1, "hasViews"    # Lorg/androidannotations/api/view/HasViews;

    .line 88
    const v0, 0x7f0e00d0

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoDuvidaActivity_;->root:Landroid/widget/LinearLayout;

    .line 89
    const v0, 0x7f0e00d2

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/ui/widgets/NonScrollListView;

    iput-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoDuvidaActivity_;->listaFaqsDuvidas:Lcom/itau/empresas/ui/widgets/NonScrollListView;

    .line 90
    const v0, 0x7f0e00b6

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/Toolbar;

    iput-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoDuvidaActivity_;->toolBar:Landroid/support/v7/widget/Toolbar;

    .line 91
    invoke-virtual {p0}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoDuvidaActivity_;->carregaElementosDeTela()V

    .line 92
    return-void
.end method

.method public setContentView(I)V
    .registers 3
    .param p1, "layoutResID"    # I

    .line 58
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoDuvidaActivity;->setContentView(I)V

    .line 59
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoDuvidaActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 60
    return-void
.end method

.method public setContentView(Landroid/view/View;)V
    .registers 3
    .param p1, "view"    # Landroid/view/View;

    .line 70
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoDuvidaActivity;->setContentView(Landroid/view/View;)V

    .line 71
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoDuvidaActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 72
    return-void
.end method

.method public setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .registers 4
    .param p1, "view"    # Landroid/view/View;
    .param p2, "params"    # Landroid/view/ViewGroup$LayoutParams;

    .line 64
    invoke-super {p0, p1, p2}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoDuvidaActivity;->setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 65
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoDuvidaActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 66
    return-void
.end method

.class public abstract Lorg/androidannotations/api/builder/FragmentBuilder;
.super Lorg/androidannotations/api/builder/Builder;
.source "FragmentBuilder.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<I:Lorg/androidannotations/api/builder/FragmentBuilder<TI;TF;>;F:Ljava/lang/Object;>Lorg/androidannotations/api/builder/Builder;"
    }
.end annotation


# instance fields
.field protected args:Landroid/os/Bundle;


# direct methods
.method public constructor <init>()V
    .registers 2

    .line 30
    invoke-direct {p0}, Lorg/androidannotations/api/builder/Builder;-><init>()V

    .line 31
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lorg/androidannotations/api/builder/FragmentBuilder;->args:Landroid/os/Bundle;

    .line 32
    return-void
.end method


# virtual methods
.method public arg(Ljava/lang/String;Z)Lorg/androidannotations/api/builder/FragmentBuilder;
    .registers 4
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/lang/String;Z)TI;"
        }
    .end annotation

    .line 42
    iget-object v0, p0, Lorg/androidannotations/api/builder/FragmentBuilder;->args:Landroid/os/Bundle;

    invoke-virtual {v0, p1, p2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 43
    return-object p0
.end method

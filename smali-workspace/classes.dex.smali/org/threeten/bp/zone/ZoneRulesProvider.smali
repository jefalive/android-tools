.class public abstract Lorg/threeten/bp/zone/ZoneRulesProvider;
.super Ljava/lang/Object;
.source "ZoneRulesProvider.java"


# static fields
.field private static final PROVIDERS:Ljava/util/concurrent/CopyOnWriteArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArrayList<Lorg/threeten/bp/zone/ZoneRulesProvider;>;"
        }
    .end annotation
.end field

.field private static final ZONES:Ljava/util/concurrent/ConcurrentMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentMap<Ljava/lang/String;Lorg/threeten/bp/zone/ZoneRulesProvider;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .registers 8

    .line 78
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    sput-object v0, Lorg/threeten/bp/zone/ZoneRulesProvider;->PROVIDERS:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 82
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    const/16 v1, 0x200

    const/high16 v2, 0x3f400000    # 0.75f

    const/4 v3, 0x2

    invoke-direct {v0, v1, v2, v3}, Ljava/util/concurrent/ConcurrentHashMap;-><init>(IFI)V

    sput-object v0, Lorg/threeten/bp/zone/ZoneRulesProvider;->ZONES:Ljava/util/concurrent/ConcurrentMap;

    .line 84
    const-class v0, Lorg/threeten/bp/zone/ZoneRulesProvider;

    const-class v1, Lorg/threeten/bp/zone/ZoneRulesProvider;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-static {v0, v1}, Ljava/util/ServiceLoader;->load(Ljava/lang/Class;Ljava/lang/ClassLoader;)Ljava/util/ServiceLoader;

    move-result-object v4

    .line 85
    .local v4, "loader":Ljava/util/ServiceLoader;, "Ljava/util/ServiceLoader<Lorg/threeten/bp/zone/ZoneRulesProvider;>;"
    invoke-virtual {v4}, Ljava/util/ServiceLoader;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .local v5, "i$":Ljava/util/Iterator;
    :goto_23
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3f

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lorg/threeten/bp/zone/ZoneRulesProvider;

    .line 87
    .local v6, "provider":Lorg/threeten/bp/zone/ZoneRulesProvider;
    :try_start_30
    invoke-static {v6}, Lorg/threeten/bp/zone/ZoneRulesProvider;->registerProvider0(Lorg/threeten/bp/zone/ZoneRulesProvider;)V
    :try_end_33
    .catch Ljava/util/ServiceConfigurationError; {:try_start_30 .. :try_end_33} :catch_34

    .line 92
    goto :goto_3e

    .line 88
    :catch_34
    move-exception v7

    .line 89
    .local v7, "ex":Ljava/util/ServiceConfigurationError;
    invoke-virtual {v7}, Ljava/util/ServiceConfigurationError;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    instance-of v0, v0, Ljava/lang/SecurityException;

    if-nez v0, :cond_3e

    .line 90
    throw v7

    .line 92
    .end local v6    # "provider":Lorg/threeten/bp/zone/ZoneRulesProvider;
    .end local v7    # "ex":Ljava/util/ServiceConfigurationError;
    :cond_3e
    :goto_3e
    goto :goto_23

    .line 94
    .end local v4    # "loader":Ljava/util/ServiceLoader;, "Ljava/util/ServiceLoader<Lorg/threeten/bp/zone/ZoneRulesProvider;>;"
    .end local v4
    .end local v5    # "i$":Ljava/util/Iterator;
    :cond_3f
    return-void
.end method

.method protected constructor <init>()V
    .registers 1

    .line 250
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 251
    return-void
.end method

.method public static getAvailableZoneIds()Ljava/util/Set;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/Set<Ljava/lang/String;>;"
        }
    .end annotation

    .line 105
    new-instance v0, Ljava/util/HashSet;

    sget-object v1, Lorg/threeten/bp/zone/ZoneRulesProvider;->ZONES:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v1}, Ljava/util/concurrent/ConcurrentMap;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method

.method private static getProvider(Ljava/lang/String;)Lorg/threeten/bp/zone/ZoneRulesProvider;
    .registers 5
    .param p0, "zoneId"    # Ljava/lang/String;

    .line 173
    sget-object v0, Lorg/threeten/bp/zone/ZoneRulesProvider;->ZONES:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0, p0}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lorg/threeten/bp/zone/ZoneRulesProvider;

    .line 174
    .local v3, "provider":Lorg/threeten/bp/zone/ZoneRulesProvider;
    if-nez v3, :cond_34

    .line 175
    sget-object v0, Lorg/threeten/bp/zone/ZoneRulesProvider;->ZONES:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0}, Ljava/util/concurrent/ConcurrentMap;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1b

    .line 176
    new-instance v0, Lorg/threeten/bp/zone/ZoneRulesException;

    const-string v1, "No time-zone data files registered"

    invoke-direct {v0, v1}, Lorg/threeten/bp/zone/ZoneRulesException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 178
    :cond_1b
    new-instance v0, Lorg/threeten/bp/zone/ZoneRulesException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown time-zone ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/threeten/bp/zone/ZoneRulesException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 180
    :cond_34
    return-object v3
.end method

.method public static getRules(Ljava/lang/String;Z)Lorg/threeten/bp/zone/ZoneRules;
    .registers 3
    .param p0, "zoneId"    # Ljava/lang/String;
    .param p1, "forCaching"    # Z

    .line 132
    const-string v0, "zoneId"

    invoke-static {p0, v0}, Lorg/threeten/bp/jdk8/Jdk8Methods;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 133
    invoke-static {p0}, Lorg/threeten/bp/zone/ZoneRulesProvider;->getProvider(Ljava/lang/String;)Lorg/threeten/bp/zone/ZoneRulesProvider;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lorg/threeten/bp/zone/ZoneRulesProvider;->provideRules(Ljava/lang/String;Z)Lorg/threeten/bp/zone/ZoneRules;

    move-result-object v0

    return-object v0
.end method

.method public static registerProvider(Lorg/threeten/bp/zone/ZoneRulesProvider;)V
    .registers 2
    .param p0, "provider"    # Lorg/threeten/bp/zone/ZoneRulesProvider;

    .line 200
    const-string v0, "provider"

    invoke-static {p0, v0}, Lorg/threeten/bp/jdk8/Jdk8Methods;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 201
    invoke-static {p0}, Lorg/threeten/bp/zone/ZoneRulesProvider;->registerProvider0(Lorg/threeten/bp/zone/ZoneRulesProvider;)V

    .line 202
    sget-object v0, Lorg/threeten/bp/zone/ZoneRulesProvider;->PROVIDERS:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p0}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    .line 203
    return-void
.end method

.method private static registerProvider0(Lorg/threeten/bp/zone/ZoneRulesProvider;)V
    .registers 7
    .param p0, "provider"    # Lorg/threeten/bp/zone/ZoneRulesProvider;

    .line 212
    invoke-virtual {p0}, Lorg/threeten/bp/zone/ZoneRulesProvider;->provideZoneIds()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_8
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_49

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Ljava/lang/String;

    .line 213
    .local v4, "zoneId":Ljava/lang/String;
    const-string v0, "zoneId"

    invoke-static {v4, v0}, Lorg/threeten/bp/jdk8/Jdk8Methods;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 214
    sget-object v0, Lorg/threeten/bp/zone/ZoneRulesProvider;->ZONES:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0, v4, p0}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lorg/threeten/bp/zone/ZoneRulesProvider;

    .line 215
    .local v5, "old":Lorg/threeten/bp/zone/ZoneRulesProvider;
    if-eqz v5, :cond_48

    .line 216
    new-instance v0, Lorg/threeten/bp/zone/ZoneRulesException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unable to register zone as one already registered with that ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", currently loading from provider: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/threeten/bp/zone/ZoneRulesException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 220
    .end local v4    # "zoneId":Ljava/lang/String;
    .end local v5    # "old":Lorg/threeten/bp/zone/ZoneRulesProvider;
    :cond_48
    goto :goto_8

    .line 221
    .end local v3    # "i$":Ljava/util/Iterator;
    :cond_49
    return-void
.end method


# virtual methods
.method protected abstract provideRules(Ljava/lang/String;Z)Lorg/threeten/bp/zone/ZoneRules;
.end method

.method protected abstract provideZoneIds()Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/Set<Ljava/lang/String;>;"
        }
    .end annotation
.end method

.class public Lcom/itau/empresas/feature/login/controller/MenuController;
.super Lcom/itau/empresas/controller/BaseController;
.source "MenuController.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/itau/empresas/controller/BaseController<Lcom/itau/empresas/feature/login/LoginApi;>;"
    }
.end annotation


# instance fields
.field activity:Lcom/itau/empresas/ui/activity/BaseActivity;

.field app:Lcom/itau/empresas/CustomApplication;


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 20
    invoke-direct {p0}, Lcom/itau/empresas/controller/BaseController;-><init>()V

    return-void
.end method

.method private geraCodigoVersao()I
    .registers 9

    .line 53
    const-string v5, "0.0.0"

    .line 55
    .local v5, "versaoApp":Ljava/lang/String;
    :try_start_2
    iget-object v0, p0, Lcom/itau/empresas/feature/login/controller/MenuController;->activity:Lcom/itau/empresas/ui/activity/BaseActivity;

    invoke-virtual {v0}, Lcom/itau/empresas/ui/activity/BaseActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/login/controller/MenuController;->activity:Lcom/itau/empresas/ui/activity/BaseActivity;

    .line 56
    invoke-virtual {v1}, Lcom/itau/empresas/ui/activity/BaseActivity;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v6

    .line 57
    .local v6, "pInfo":Landroid/content/pm/PackageInfo;
    iget-object v0, v6, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;
    :try_end_15
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_2 .. :try_end_15} :catch_17

    move-object v5, v0

    .line 61
    .end local v6    # "pInfo":Landroid/content/pm/PackageInfo;
    goto :goto_24

    .line 58
    :catch_17
    move-exception v6

    .line 59
    .local v6, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v0, "menu"

    invoke-virtual {v6}, Landroid/content/pm/PackageManager$NameNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 60
    invoke-virtual {v6}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    .line 63
    .end local v6    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :goto_24
    const-string v0, "."

    const-string v1, ";"

    invoke-virtual {v5, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v5

    .line 64
    const-string v0, ";"

    invoke-virtual {v5, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    .line 65
    .local v6, "versaoSplit":[Ljava/lang/String;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "1"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "%03d"

    .line 66
    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aget-object v3, v6, v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const/4 v4, 0x0

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "%03d"

    .line 67
    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x1

    aget-object v3, v6, v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const/4 v4, 0x0

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "%03d"

    .line 68
    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x2

    aget-object v3, v6, v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const/4 v4, 0x0

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 69
    .local v7, "versaoMenu":Ljava/lang/String;
    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method private tratarRetornoMenu(Lcom/itau/empresas/api/model/MenuVO;)V
    .registers 6
    .param p1, "menu"    # Lcom/itau/empresas/api/model/MenuVO;

    .line 39
    invoke-virtual {p1}, Lcom/itau/empresas/api/model/MenuVO;->getFilhos()Ljava/util/List;

    move-result-object v2

    .line 41
    .local v2, "menuFilhos":Ljava/util/List;, "Ljava/util/List<Lcom/itau/empresas/api/model/MenuVO;>;"
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_5
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    if-ge v3, v0, :cond_31

    .line 42
    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/api/model/MenuVO;

    invoke-virtual {v0}, Lcom/itau/empresas/api/model/MenuVO;->getChaveMobile()Ljava/lang/String;

    move-result-object v0

    const-string v1, "exibicao"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2e

    .line 43
    iget-object v0, p0, Lcom/itau/empresas/feature/login/controller/MenuController;->app:Lcom/itau/empresas/CustomApplication;

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/itau/empresas/api/model/MenuVO;

    invoke-virtual {v0, v1}, Lcom/itau/empresas/CustomApplication;->setPermissaoAutorizante(Lcom/itau/empresas/api/model/MenuVO;)V

    .line 44
    invoke-interface {v2, v3}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 45
    invoke-virtual {p1, v2}, Lcom/itau/empresas/api/model/MenuVO;->setFilhos(Ljava/util/List;)V

    .line 41
    :cond_2e
    add-int/lit8 v3, v3, 0x1

    goto :goto_5

    .line 49
    .end local v3    # "i":I
    :cond_31
    iget-object v0, p0, Lcom/itau/empresas/feature/login/controller/MenuController;->app:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0, p1}, Lcom/itau/empresas/CustomApplication;->setMenuVO(Lcom/itau/empresas/api/model/MenuVO;)V

    .line 50
    return-void
.end method


# virtual methods
.method public carregarMenu()V
    .registers 4

    .line 30
    iget-object v0, p0, Lcom/itau/empresas/feature/login/controller/MenuController;->app:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getEventoTracker()Lcom/itau/empresas/ui/util/analytics/TrackerEvento;

    move-result-object v0

    const-string v1, "menu"

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/analytics/TrackerEvento;->inicioMonitorarTempo(Ljava/lang/String;)V

    .line 31
    invoke-virtual {p0}, Lcom/itau/empresas/feature/login/controller/MenuController;->api()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/feature/login/LoginApi;

    invoke-direct {p0}, Lcom/itau/empresas/feature/login/controller/MenuController;->geraCodigoVersao()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/itau/empresas/feature/login/LoginApi;->menu(I)Lcom/itau/empresas/api/model/MenuVO;

    move-result-object v2

    .line 32
    .local v2, "menuResponse":Lcom/itau/empresas/api/model/MenuVO;
    iget-object v0, p0, Lcom/itau/empresas/feature/login/controller/MenuController;->app:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getEventoTracker()Lcom/itau/empresas/ui/util/analytics/TrackerEvento;

    move-result-object v0

    const-string v1, "menu"

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/analytics/TrackerEvento;->fimMonitorarTempo(Ljava/lang/String;)V

    .line 34
    invoke-direct {p0, v2}, Lcom/itau/empresas/feature/login/controller/MenuController;->tratarRetornoMenu(Lcom/itau/empresas/api/model/MenuVO;)V

    .line 35
    return-void
.end method

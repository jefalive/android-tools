.class public Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesDetalheActivity;
.super Lcom/itau/empresas/ui/activity/BaseActivity;
.source "ComprovantesDetalheActivity.java"


# instance fields
.field botaoEnviarDetalheComprovante:Landroid/widget/TextView;

.field botaoEnviarDetalheComprovanteEmail:Landroid/widget/TextView;

.field comprovanteDetalheBotaoEmail:Landroid/widget/LinearLayout;

.field comprovanteDetalheBotoes:Landroid/widget/LinearLayout;

.field comprovanteDetalheIdentificacao:Landroid/widget/LinearLayout;

.field comprovantesDetalheConteiner:Landroid/widget/LinearLayout;

.field comprovantesDetalheScrollConteiner:Landroid/widget/ScrollView;

.field comprovantesVO:Lcom/itau/empresas/feature/comprovantes/model/ComprovantesVO;

.field private indicadorEmail:Z

.field mensagemAvisoComprovante:Lcom/itau/empresas/ui/view/MensagemAvisoView;

.field textoDataPagamentoDetalhe:Landroid/widget/TextView;

.field textoDescricaoIdentificacaoDetalhe:Landroid/widget/TextView;

.field textoNomeOperacaoDetalheComprovante:Landroid/widget/TextView;

.field textoValorComprovanteDetalhe:Landroid/widget/TextView;

.field toolBar:Landroid/support/v7/widget/Toolbar;


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 38
    invoke-direct {p0}, Lcom/itau/empresas/ui/activity/BaseActivity;-><init>()V

    return-void
.end method

.method private analyticsComprovante(Ljava/lang/String;)V
    .registers 5
    .param p1, "acao"    # Ljava/lang/String;

    .line 202
    new-instance v2, Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;

    const v0, 0x7f0702da

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesDetalheActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesDetalheActivity;->comprovantesVO:Lcom/itau/empresas/feature/comprovantes/model/ComprovantesVO;

    .line 203
    invoke-virtual {v1}, Lcom/itau/empresas/feature/comprovantes/model/ComprovantesVO;->getNomeOperacao()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v0, p1, v1}, Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 204
    .local v2, "eventoAnalytics":Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;
    iget-object v0, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesDetalheActivity;->application:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getEventoTracker()Lcom/itau/empresas/ui/util/analytics/TrackerEvento;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/itau/empresas/ui/util/analytics/TrackerEvento;->enviarHitEvento(Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;)V

    .line 205
    return-void
.end method

.method private compartilhaComprovante()V
    .registers 5

    .line 187
    iget-object v0, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesDetalheActivity;->comprovantesDetalheScrollConteiner:Landroid/widget/ScrollView;

    .line 188
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesDetalheActivity;->comprovantesDetalheConteiner:Landroid/widget/LinearLayout;

    invoke-static {v0, v1}, Lcom/itau/empresas/ui/util/BitmapUtils;->getBitmapFromView(Landroid/view/View;Landroid/view/View;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 190
    .line 191
    .local v2, "comprovanteBitmap":Landroid/graphics/Bitmap;
    invoke-static {p0, v2}, Lcom/itau/empresas/ui/util/BitmapUtils;->getUriBitmapTemporario(Landroid/content/Context;Landroid/graphics/Bitmap;)Landroid/net/Uri;

    move-result-object v3

    .line 192
    .local v3, "bitmapUri":Landroid/net/Uri;
    const-string v0, "Comprovante compartilhado"

    .line 193
    invoke-static {p0, v3, v0}, Lcom/itau/empresas/ui/util/CompartilhamentoUtils;->compartilharArquivoImagemPorUri(Landroid/app/Activity;Landroid/net/Uri;Ljava/lang/String;)V

    .line 194
    return-void
.end method

.method private constroiToolbar()V
    .registers 3

    .line 225
    iget-object v0, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesDetalheActivity;->toolBar:Landroid/support/v7/widget/Toolbar;

    invoke-static {v0, p0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom;->Builder(Landroid/support/v7/widget/Toolbar;Landroid/support/v7/app/AppCompatActivity;)Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    move-result-object v0

    .line 226
    const v1, 0x7f07071e

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->comTitulo(I)Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    move-result-object v0

    .line 227
    invoke-virtual {v0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->comClickListenerFinish()Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    move-result-object v0

    .line 228
    invoke-virtual {v0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->build()Lcom/itau/empresas/ui/util/listener/ToolbarCustom;

    .line 229
    return-void
.end method

.method private deveMostrarBotaoDeEmail(Z)V
    .registers 4
    .param p1, "indicador"    # Z

    .line 214
    if-eqz p1, :cond_f

    .line 215
    iget-object v0, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesDetalheActivity;->comprovanteDetalheBotaoEmail:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 216
    iget-object v0, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesDetalheActivity;->botaoEnviarDetalheComprovanteEmail:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1d

    .line 219
    :cond_f
    iget-object v0, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesDetalheActivity;->comprovanteDetalheBotaoEmail:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 220
    iget-object v0, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesDetalheActivity;->botaoEnviarDetalheComprovanteEmail:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 222
    :goto_1d
    return-void
.end method

.method private inicializaLayout()V
    .registers 3

    .line 150
    iget-object v0, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesDetalheActivity;->comprovantesVO:Lcom/itau/empresas/feature/comprovantes/model/ComprovantesVO;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/comprovantes/model/ComprovantesVO;->getLiteralIdentificacaoComprovante()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1b

    iget-object v0, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesDetalheActivity;->comprovantesVO:Lcom/itau/empresas/feature/comprovantes/model/ComprovantesVO;

    .line 151
    invoke-virtual {v0}, Lcom/itau/empresas/feature/comprovantes/model/ComprovantesVO;->getLiteralIdentificacaoComprovante()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1b

    .line 152
    iget-object v0, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesDetalheActivity;->comprovanteDetalheIdentificacao:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 156
    :cond_1b
    iget-boolean v0, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesDetalheActivity;->indicadorEmail:Z

    invoke-direct {p0, v0}, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesDetalheActivity;->deveMostrarBotaoDeEmail(Z)V

    .line 157
    return-void
.end method

.method public static intent(Landroid/content/Context;)Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesDetalheActivity_$IntentBuilder_;
    .registers 2
    .param p0, "context"    # Landroid/content/Context;

    .line 232
    new-instance v0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesDetalheActivity_$IntentBuilder_;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesDetalheActivity_$IntentBuilder_;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method private preencherCampos()V
    .registers 7

    .line 161
    iget-object v0, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesDetalheActivity;->comprovantesVO:Lcom/itau/empresas/feature/comprovantes/model/ComprovantesVO;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/comprovantes/model/ComprovantesVO;->getNomeOperacao()Ljava/lang/String;

    move-result-object v2

    .line 162
    .local v2, "operacao":Ljava/lang/String;
    iget-object v0, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesDetalheActivity;->comprovantesVO:Lcom/itau/empresas/feature/comprovantes/model/ComprovantesVO;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/comprovantes/model/ComprovantesVO;->getValor()Ljava/lang/String;

    move-result-object v3

    .line 163
    .local v3, "valor":Ljava/lang/String;
    iget-object v0, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesDetalheActivity;->comprovantesVO:Lcom/itau/empresas/feature/comprovantes/model/ComprovantesVO;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/comprovantes/model/ComprovantesVO;->getLiteralIdentificacaoComprovante()Ljava/lang/String;

    move-result-object v4

    .line 164
    .local v4, "identificacao":Ljava/lang/String;
    iget-object v0, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesDetalheActivity;->comprovantesVO:Lcom/itau/empresas/feature/comprovantes/model/ComprovantesVO;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/comprovantes/model/ComprovantesVO;->getDataComprovante()Ljava/lang/String;

    move-result-object v5

    .line 165
    .local v5, "dataPagamento":Ljava/lang/String;
    iget-object v0, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesDetalheActivity;->comprovantesVO:Lcom/itau/empresas/feature/comprovantes/model/ComprovantesVO;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/comprovantes/model/ComprovantesVO;->getIndicadorEmail()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesDetalheActivity;->indicadorEmail:Z

    .line 167
    iget-object v0, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesDetalheActivity;->textoNomeOperacaoDetalheComprovante:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 168
    iget-object v0, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesDetalheActivity;->textoValorComprovanteDetalhe:Landroid/widget/TextView;

    .line 169
    const/4 v1, 0x1

    invoke-static {v3, v1}, Lcom/itau/empresas/ui/util/ValorMonetarioUtils;->formataValorDecimalParaMonetario(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 170
    iget-object v0, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesDetalheActivity;->textoDataPagamentoDetalhe:Landroid/widget/TextView;

    invoke-static {v5}, Lcom/itau/empresas/ui/util/DataEHoraUtils;->formataDataComBarra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 171
    iget-object v0, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesDetalheActivity;->textoDescricaoIdentificacaoDetalhe:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 173
    return-void
.end method

.method private preencherDadosAviso()V
    .registers 4

    .line 177
    const v0, 0x7f070470

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesDetalheActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 179
    .local v2, "mensagem":Ljava/lang/String;
    iget-object v0, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesDetalheActivity;->mensagemAvisoComprovante:Lcom/itau/empresas/ui/view/MensagemAvisoView;

    sget-object v1, Lcom/itau/empresas/ui/view/MensagemAvisoView$Estado;->SUCESSO:Lcom/itau/empresas/ui/view/MensagemAvisoView$Estado;

    invoke-virtual {v0, v1, v2}, Lcom/itau/empresas/ui/view/MensagemAvisoView;->setMensagem(Lcom/itau/empresas/ui/view/MensagemAvisoView$Estado;Ljava/lang/String;)V

    .line 180
    return-void
.end method


# virtual methods
.method protected aoClicarEnviarComprovante()V
    .registers 3

    .line 124
    const v0, 0x7f0702a6

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesDetalheActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesDetalheActivity;->analyticsComprovante(Ljava/lang/String;)V

    .line 125
    invoke-static {p0}, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesEnviarActivity;->intent(Landroid/content/Context;)Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesEnviarActivity_$IntentBuilder_;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesDetalheActivity;->comprovantesVO:Lcom/itau/empresas/feature/comprovantes/model/ComprovantesVO;

    .line 126
    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesEnviarActivity_$IntentBuilder_;->comprovantesVO(Lcom/itau/empresas/feature/comprovantes/model/ComprovantesVO;)Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesEnviarActivity_$IntentBuilder_;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesEnviarActivity_$IntentBuilder_;->startForResult(I)Lorg/androidannotations/api/builder/PostActivityStarter;

    .line 127
    return-void
.end method

.method protected carregarElementosTela()V
    .registers 2

    .line 94
    invoke-direct {p0}, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesDetalheActivity;->preencherCampos()V

    .line 95
    invoke-direct {p0}, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesDetalheActivity;->inicializaLayout()V

    .line 96
    const v0, 0x7f070290

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesDetalheActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesDetalheActivity;->analyticsComprovante(Ljava/lang/String;)V

    .line 97
    invoke-direct {p0}, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesDetalheActivity;->preencherDadosAviso()V

    .line 98
    invoke-direct {p0}, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesDetalheActivity;->constroiToolbar()V

    .line 99
    return-void
.end method

.method protected compartilharComprovante(Landroid/view/View;)V
    .registers 4
    .param p1, "view"    # Landroid/view/View;

    .line 114
    iget-object v0, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesDetalheActivity;->comprovanteDetalheBotoes:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 116
    invoke-direct {p0}, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesDetalheActivity;->compartilhaComprovante()V

    .line 117
    const v0, 0x7f0702a1

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesDetalheActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesDetalheActivity;->analyticsComprovante(Ljava/lang/String;)V

    .line 119
    iget-object v0, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesDetalheActivity;->comprovanteDetalheBotoes:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 120
    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .registers 6
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .line 103
    if-nez p1, :cond_14

    const/4 v0, -0x1

    if-ne p2, v0, :cond_14

    .line 104
    new-instance v0, Landroid/support/v7/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/support/v7/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 105
    const v1, 0x7f070520

    invoke-virtual {v0, v1}, Landroid/support/v7/app/AlertDialog$Builder;->setMessage(I)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v0

    .line 106
    invoke-virtual {v0}, Landroid/support/v7/app/AlertDialog$Builder;->show()Landroid/support/v7/app/AlertDialog;

    .line 108
    :cond_14
    invoke-super {p0, p1, p2, p3}, Lcom/itau/empresas/ui/activity/BaseActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 109
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .registers 3
    .param p1, "menu"    # Landroid/view/Menu;

    .line 136
    invoke-static {p0, p1}, Lcom/itau/empresas/ui/util/PoyntUtils;->onCreateOptionsMenu(Landroid/app/Activity;Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .registers 3
    .param p1, "item"    # Landroid/view/MenuItem;

    .line 141
    invoke-static {p0, p1}, Lcom/itau/empresas/ui/util/PoyntUtils;->onOptionsItemSelected(Lcom/itau/empresas/ui/activity/BaseActivity;Landroid/view/MenuItem;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 142
    const/4 v0, 0x1

    return v0

    .line 143
    :cond_8
    invoke-super {p0, p1}, Lcom/itau/empresas/ui/activity/BaseActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0
.end method

.method public opkeysConsumidas()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Ljava/lang/String;>;"
        }
    .end annotation

    .line 131
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

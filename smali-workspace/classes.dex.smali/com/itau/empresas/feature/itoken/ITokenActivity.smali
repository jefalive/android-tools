.class public Lcom/itau/empresas/feature/itoken/ITokenActivity;
.super Lcom/itau/empresas/ui/activity/BaseActivity;
.source "ITokenActivity.java"

# interfaces
.implements Lcom/itau/empresas/ui/util/SnackbarUtils$Gerenciador;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/itau/empresas/feature/itoken/ITokenActivity$Estados;
    }
.end annotation


# static fields
.field private static final ESTATUS_MAQUINA_DE_ESTADOS:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<Ljava/lang/String;Lcom/itau/empresas/feature/itoken/ITokenActivity$Estados;>;"
        }
    .end annotation
.end field


# instance fields
.field private anterior:Lcom/itau/empresas/ui/util/MaquinaDeEstado$Chave;

.field controller:Lcom/itau/empresas/controller/ITokenController;

.field iTokenAssociarFragment:Lcom/itau/empresas/feature/itoken/fragment/ITokenAssociarFragment;

.field iTokenCancelamentoOutro:Lcom/itau/empresas/feature/itoken/fragment/ITokenCancelamentoOutroOperador;

.field iTokenCancelamentoSimples:Lcom/itau/empresas/feature/itoken/fragment/ITokenCancelamentoSimples;

.field iTokenEfetivacaoDesbloqueioFragment:Lcom/itau/empresas/feature/itoken/fragment/ITokenEfetivacaoDesbloqueioFragment;

.field iTokenInstalacaoFragment:Lcom/itau/empresas/feature/itoken/fragment/ITokenInstalacaoFragment;

.field iTokenInstaladoFragment:Lcom/itau/empresas/feature/itoken/fragment/ITokenInstaladoFragment;

.field iTokenLiberacaoFragment:Lcom/itau/empresas/feature/itoken/fragment/ITokenLiberacaoFragment;

.field iTokenLiberacaoOutroCelularFragment:Lcom/itau/empresas/feature/itoken/fragment/ITokenLiberacaoOutroCelularFragment;

.field iTokenNovaInstalacao:Lcom/itau/empresas/feature/itoken/fragment/ITokenNovaInstalacaoFragment;

.field public llMensagemConfirmacao:Landroid/widget/LinearLayout;

.field private maquina:Lcom/itau/empresas/ui/util/MaquinaDeEstado;

.field private final onItokenAssociar:Lcom/itau/empresas/ui/util/maquinaestado/Estado$OnChangeState;

.field private final onItokenBloqueado:Lcom/itau/empresas/ui/util/maquinaestado/Estado$OnChangeState;

.field private final onItokenCancelamentoOutro:Lcom/itau/empresas/ui/util/maquinaestado/Estado$OnChangeState;

.field private final onItokenCancelamentoSimples:Lcom/itau/empresas/ui/util/maquinaestado/Estado$OnChangeState;

.field private final onItokenInstalacao:Lcom/itau/empresas/ui/util/maquinaestado/Estado$OnChangeState;

.field private final onItokenInstalacaoCancelado:Lcom/itau/empresas/ui/util/maquinaestado/Estado$OnChangeState;

.field private final onItokenInstalado:Lcom/itau/empresas/ui/util/maquinaestado/Estado$OnChangeState;

.field private final onItokenInstaladoAssociado:Lcom/itau/empresas/ui/util/maquinaestado/Estado$OnChangeState;

.field private final onItokenInstaladoNovo:Lcom/itau/empresas/ui/util/maquinaestado/Estado$OnChangeState;

.field private final onItokenLiberar:Lcom/itau/empresas/ui/util/maquinaestado/Estado$OnChangeState;

.field private final onItokenLiberarOutroAparelho:Lcom/itau/empresas/ui/util/maquinaestado/Estado$OnChangeState;

.field private final onItokenNovaInstalacao:Lcom/itau/empresas/ui/util/maquinaestado/Estado$OnChangeState;

.field root:Landroid/widget/LinearLayout;

.field public textoConfirmacao:Landroid/widget/TextView;

.field toolBar:Landroid/support/v7/widget/Toolbar;

.field private topSnackbar:Lcom/itau/empresas/ui/util/SnackbarUtils$Gerenciador;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .line 63
    new-instance v0, Lcom/itau/empresas/feature/itoken/ITokenActivity$1;

    invoke-direct {v0}, Lcom/itau/empresas/feature/itoken/ITokenActivity$1;-><init>()V

    sput-object v0, Lcom/itau/empresas/feature/itoken/ITokenActivity;->ESTATUS_MAQUINA_DE_ESTADOS:Ljava/util/Map;

    return-void
.end method

.method public constructor <init>()V
    .registers 2

    .line 61
    invoke-direct {p0}, Lcom/itau/empresas/ui/activity/BaseActivity;-><init>()V

    .line 90
    new-instance v0, Lcom/itau/empresas/feature/itoken/ITokenActivity$2;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/itoken/ITokenActivity$2;-><init>(Lcom/itau/empresas/feature/itoken/ITokenActivity;)V

    iput-object v0, p0, Lcom/itau/empresas/feature/itoken/ITokenActivity;->onItokenInstalacao:Lcom/itau/empresas/ui/util/maquinaestado/Estado$OnChangeState;

    .line 97
    new-instance v0, Lcom/itau/empresas/feature/itoken/ITokenActivity$3;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/itoken/ITokenActivity$3;-><init>(Lcom/itau/empresas/feature/itoken/ITokenActivity;)V

    iput-object v0, p0, Lcom/itau/empresas/feature/itoken/ITokenActivity;->onItokenInstalacaoCancelado:Lcom/itau/empresas/ui/util/maquinaestado/Estado$OnChangeState;

    .line 108
    new-instance v0, Lcom/itau/empresas/feature/itoken/ITokenActivity$4;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/itoken/ITokenActivity$4;-><init>(Lcom/itau/empresas/feature/itoken/ITokenActivity;)V

    iput-object v0, p0, Lcom/itau/empresas/feature/itoken/ITokenActivity;->onItokenAssociar:Lcom/itau/empresas/ui/util/maquinaestado/Estado$OnChangeState;

    .line 116
    new-instance v0, Lcom/itau/empresas/feature/itoken/ITokenActivity$5;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/itoken/ITokenActivity$5;-><init>(Lcom/itau/empresas/feature/itoken/ITokenActivity;)V

    iput-object v0, p0, Lcom/itau/empresas/feature/itoken/ITokenActivity;->onItokenCancelamentoSimples:Lcom/itau/empresas/ui/util/maquinaestado/Estado$OnChangeState;

    .line 124
    new-instance v0, Lcom/itau/empresas/feature/itoken/ITokenActivity$6;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/itoken/ITokenActivity$6;-><init>(Lcom/itau/empresas/feature/itoken/ITokenActivity;)V

    iput-object v0, p0, Lcom/itau/empresas/feature/itoken/ITokenActivity;->onItokenInstalado:Lcom/itau/empresas/ui/util/maquinaestado/Estado$OnChangeState;

    .line 132
    new-instance v0, Lcom/itau/empresas/feature/itoken/ITokenActivity$7;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/itoken/ITokenActivity$7;-><init>(Lcom/itau/empresas/feature/itoken/ITokenActivity;)V

    iput-object v0, p0, Lcom/itau/empresas/feature/itoken/ITokenActivity;->onItokenInstaladoNovo:Lcom/itau/empresas/ui/util/maquinaestado/Estado$OnChangeState;

    .line 142
    new-instance v0, Lcom/itau/empresas/feature/itoken/ITokenActivity$8;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/itoken/ITokenActivity$8;-><init>(Lcom/itau/empresas/feature/itoken/ITokenActivity;)V

    iput-object v0, p0, Lcom/itau/empresas/feature/itoken/ITokenActivity;->onItokenInstaladoAssociado:Lcom/itau/empresas/ui/util/maquinaestado/Estado$OnChangeState;

    .line 151
    new-instance v0, Lcom/itau/empresas/feature/itoken/ITokenActivity$9;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/itoken/ITokenActivity$9;-><init>(Lcom/itau/empresas/feature/itoken/ITokenActivity;)V

    iput-object v0, p0, Lcom/itau/empresas/feature/itoken/ITokenActivity;->onItokenNovaInstalacao:Lcom/itau/empresas/ui/util/maquinaestado/Estado$OnChangeState;

    .line 159
    new-instance v0, Lcom/itau/empresas/feature/itoken/ITokenActivity$10;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/itoken/ITokenActivity$10;-><init>(Lcom/itau/empresas/feature/itoken/ITokenActivity;)V

    iput-object v0, p0, Lcom/itau/empresas/feature/itoken/ITokenActivity;->onItokenCancelamentoOutro:Lcom/itau/empresas/ui/util/maquinaestado/Estado$OnChangeState;

    .line 167
    new-instance v0, Lcom/itau/empresas/feature/itoken/ITokenActivity$11;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/itoken/ITokenActivity$11;-><init>(Lcom/itau/empresas/feature/itoken/ITokenActivity;)V

    iput-object v0, p0, Lcom/itau/empresas/feature/itoken/ITokenActivity;->onItokenBloqueado:Lcom/itau/empresas/ui/util/maquinaestado/Estado$OnChangeState;

    .line 175
    new-instance v0, Lcom/itau/empresas/feature/itoken/ITokenActivity$12;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/itoken/ITokenActivity$12;-><init>(Lcom/itau/empresas/feature/itoken/ITokenActivity;)V

    iput-object v0, p0, Lcom/itau/empresas/feature/itoken/ITokenActivity;->onItokenLiberar:Lcom/itau/empresas/ui/util/maquinaestado/Estado$OnChangeState;

    .line 183
    new-instance v0, Lcom/itau/empresas/feature/itoken/ITokenActivity$13;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/itoken/ITokenActivity$13;-><init>(Lcom/itau/empresas/feature/itoken/ITokenActivity;)V

    iput-object v0, p0, Lcom/itau/empresas/feature/itoken/ITokenActivity;->onItokenLiberarOutroAparelho:Lcom/itau/empresas/ui/util/maquinaestado/Estado$OnChangeState;

    return-void
.end method

.method private abreFragment(Lcom/itau/empresas/ui/fragment/BaseFragment;)V
    .registers 5
    .param p1, "fragment"    # Lcom/itau/empresas/ui/fragment/BaseFragment;

    .line 370
    invoke-virtual {p0}, Lcom/itau/empresas/feature/itoken/ITokenActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    .line 371
    .local v1, "manager":Landroid/support/v4/app/FragmentManager;
    invoke-virtual {v1}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v2

    .line 372
    .local v2, "transaction":Landroid/support/v4/app/FragmentTransaction;
    const v0, 0x7f0e01db

    invoke-virtual {v1, v0}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    if-eqz v0, :cond_18

    .line 373
    const v0, 0x7f0e01db

    invoke-virtual {v2, v0, p1}, Landroid/support/v4/app/FragmentTransaction;->replace(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    goto :goto_1e

    .line 375
    :cond_18
    const v0, 0x7f0e01db

    invoke-virtual {v2, v0, p1}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    .line 377
    :goto_1e
    invoke-virtual {v2}, Landroid/support/v4/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 378
    return-void
.end method

.method static synthetic access$000(Lcom/itau/empresas/feature/itoken/ITokenActivity;Lcom/itau/empresas/ui/fragment/BaseFragment;)V
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/feature/itoken/ITokenActivity;
    .param p1, "x1"    # Lcom/itau/empresas/ui/fragment/BaseFragment;

    .line 61
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/itoken/ITokenActivity;->abreFragment(Lcom/itau/empresas/ui/fragment/BaseFragment;)V

    return-void
.end method

.method static synthetic access$100(Lcom/itau/empresas/feature/itoken/ITokenActivity;Ljava/lang/String;)V
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/feature/itoken/ITokenActivity;
    .param p1, "x1"    # Ljava/lang/String;

    .line 61
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/itoken/ITokenActivity;->disparaEventoAnalytics(Ljava/lang/String;)V

    return-void
.end method

.method private atualizaEstado(Ljava/lang/String;)V
    .registers 5
    .param p1, "status"    # Ljava/lang/String;

    .line 256
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    .line 257
    .local v2, "i":I
    const/4 v0, 0x1

    if-ne v2, v0, :cond_b

    .line 258
    invoke-direct {p0}, Lcom/itau/empresas/feature/itoken/ITokenActivity;->mudaParaEstadoCanceladoOuInstalacao()V

    .line 259
    return-void

    .line 261
    :cond_b
    const/4 v0, 0x2

    if-ne v2, v0, :cond_12

    .line 262
    invoke-direct {p0}, Lcom/itau/empresas/feature/itoken/ITokenActivity;->mudaParaEstadoInstaladoOuAssociar()V

    .line 263
    return-void

    .line 265
    :cond_12
    const/4 v0, 0x3

    if-lt v2, v0, :cond_22

    const/4 v0, 0x6

    if-gt v2, v0, :cond_22

    .line 266
    sget-object v0, Lcom/itau/empresas/feature/itoken/ITokenActivity;->ESTATUS_MAQUINA_DE_ESTADOS:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/ui/util/MaquinaDeEstado$Chave;

    iput-object v0, p0, Lcom/itau/empresas/feature/itoken/ITokenActivity;->anterior:Lcom/itau/empresas/ui/util/MaquinaDeEstado$Chave;

    .line 268
    :cond_22
    iget-object v0, p0, Lcom/itau/empresas/feature/itoken/ITokenActivity;->maquina:Lcom/itau/empresas/ui/util/MaquinaDeEstado;

    sget-object v1, Lcom/itau/empresas/feature/itoken/ITokenActivity;->ESTATUS_MAQUINA_DE_ESTADOS:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/itau/empresas/ui/util/MaquinaDeEstado$Chave;

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/MaquinaDeEstado;->mudaParaEstado(Lcom/itau/empresas/ui/util/MaquinaDeEstado$Chave;)Lcom/itau/empresas/ui/util/MaquinaDeEstado$Chave;

    .line 269
    return-void
.end method

.method private constroiToolbar()V
    .registers 3

    .line 240
    iget-object v0, p0, Lcom/itau/empresas/feature/itoken/ITokenActivity;->toolBar:Landroid/support/v7/widget/Toolbar;

    invoke-static {v0, p0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom;->Builder(Landroid/support/v7/widget/Toolbar;Landroid/support/v7/app/AppCompatActivity;)Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    move-result-object v0

    .line 241
    const v1, 0x7f070582

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->comTitulo(I)Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    move-result-object v0

    .line 242
    invoke-virtual {v0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->comClickListenerFinish()Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    move-result-object v0

    .line 243
    invoke-virtual {v0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->build()Lcom/itau/empresas/ui/util/listener/ToolbarCustom;

    .line 244
    return-void
.end method

.method private disparaEventoAnalytics(Ljava/lang/String;)V
    .registers 5
    .param p1, "nome"    # Ljava/lang/String;

    .line 414
    invoke-static {}, Lcom/itau/empresas/feature/analytics/AnalyticsHelper;->getInstance()Lcom/itau/empresas/feature/analytics/AnalyticsHelper;

    move-result-object v2

    .line 415
    .line 417
    .local v2, "analyticsHelper":Lcom/itau/empresas/feature/analytics/AnalyticsHelper;
    const v0, 0x7f0700cf

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/itoken/ITokenActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 418
    const v1, 0x7f0700c3

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/itoken/ITokenActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 415
    invoke-virtual {v2, p1, v0, v1}, Lcom/itau/empresas/feature/analytics/AnalyticsHelper;->eventoAdobeState(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 420
    return-void
.end method

.method private inicializaMaquinaDeEstado()Lcom/itau/empresas/ui/util/MaquinaDeEstado;
    .registers 5

    .line 313
    new-instance v0, Lcom/itau/empresas/ui/util/MaquinaDeEstado;

    invoke-direct {v0}, Lcom/itau/empresas/ui/util/MaquinaDeEstado;-><init>()V

    sget-object v1, Lcom/itau/empresas/feature/itoken/ITokenActivity$Estados;->ITOKEN_INSTALACAO:Lcom/itau/empresas/feature/itoken/ITokenActivity$Estados;

    new-instance v2, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;

    invoke-direct {v2}, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;-><init>()V

    iget-object v3, p0, Lcom/itau/empresas/feature/itoken/ITokenActivity;->onItokenInstalacao:Lcom/itau/empresas/ui/util/maquinaestado/Estado$OnChangeState;

    .line 316
    invoke-virtual {v2, v3}, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;->onChangeState(Lcom/itau/empresas/ui/util/maquinaestado/Estado$OnChangeState;)Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;

    move-result-object v2

    .line 317
    invoke-virtual {v2}, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;->build()Lcom/itau/empresas/ui/util/maquinaestado/Estado;

    move-result-object v2

    .line 314
    invoke-virtual {v0, v1, v2}, Lcom/itau/empresas/ui/util/MaquinaDeEstado;->novoEstado(Lcom/itau/empresas/ui/util/MaquinaDeEstado$Chave;Lcom/itau/empresas/ui/util/maquinaestado/Estado;)Lcom/itau/empresas/ui/util/MaquinaDeEstado;

    move-result-object v0

    sget-object v1, Lcom/itau/empresas/feature/itoken/ITokenActivity$Estados;->ITOKEN_INSTALACAO_CANCELADO:Lcom/itau/empresas/feature/itoken/ITokenActivity$Estados;

    new-instance v2, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;

    invoke-direct {v2}, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;-><init>()V

    iget-object v3, p0, Lcom/itau/empresas/feature/itoken/ITokenActivity;->onItokenInstalacaoCancelado:Lcom/itau/empresas/ui/util/maquinaestado/Estado$OnChangeState;

    .line 320
    invoke-virtual {v2, v3}, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;->onChangeState(Lcom/itau/empresas/ui/util/maquinaestado/Estado$OnChangeState;)Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;

    move-result-object v2

    .line 321
    invoke-virtual {v2}, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;->build()Lcom/itau/empresas/ui/util/maquinaestado/Estado;

    move-result-object v2

    .line 318
    invoke-virtual {v0, v1, v2}, Lcom/itau/empresas/ui/util/MaquinaDeEstado;->novoEstado(Lcom/itau/empresas/ui/util/MaquinaDeEstado$Chave;Lcom/itau/empresas/ui/util/maquinaestado/Estado;)Lcom/itau/empresas/ui/util/MaquinaDeEstado;

    move-result-object v0

    sget-object v1, Lcom/itau/empresas/feature/itoken/ITokenActivity$Estados;->ITOKEN_INSTALADO:Lcom/itau/empresas/feature/itoken/ITokenActivity$Estados;

    new-instance v2, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;

    invoke-direct {v2}, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;-><init>()V

    iget-object v3, p0, Lcom/itau/empresas/feature/itoken/ITokenActivity;->onItokenInstalado:Lcom/itau/empresas/ui/util/maquinaestado/Estado$OnChangeState;

    .line 324
    invoke-virtual {v2, v3}, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;->onChangeState(Lcom/itau/empresas/ui/util/maquinaestado/Estado$OnChangeState;)Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;

    move-result-object v2

    .line 325
    invoke-virtual {v2}, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;->build()Lcom/itau/empresas/ui/util/maquinaestado/Estado;

    move-result-object v2

    .line 322
    invoke-virtual {v0, v1, v2}, Lcom/itau/empresas/ui/util/MaquinaDeEstado;->novoEstado(Lcom/itau/empresas/ui/util/MaquinaDeEstado$Chave;Lcom/itau/empresas/ui/util/maquinaestado/Estado;)Lcom/itau/empresas/ui/util/MaquinaDeEstado;

    move-result-object v0

    sget-object v1, Lcom/itau/empresas/feature/itoken/ITokenActivity$Estados;->ITOKEN_INSTALADO_NOVO:Lcom/itau/empresas/feature/itoken/ITokenActivity$Estados;

    new-instance v2, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;

    invoke-direct {v2}, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;-><init>()V

    iget-object v3, p0, Lcom/itau/empresas/feature/itoken/ITokenActivity;->onItokenInstaladoNovo:Lcom/itau/empresas/ui/util/maquinaestado/Estado$OnChangeState;

    .line 328
    invoke-virtual {v2, v3}, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;->onChangeState(Lcom/itau/empresas/ui/util/maquinaestado/Estado$OnChangeState;)Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;

    move-result-object v2

    .line 329
    invoke-virtual {v2}, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;->build()Lcom/itau/empresas/ui/util/maquinaestado/Estado;

    move-result-object v2

    .line 326
    invoke-virtual {v0, v1, v2}, Lcom/itau/empresas/ui/util/MaquinaDeEstado;->novoEstado(Lcom/itau/empresas/ui/util/MaquinaDeEstado$Chave;Lcom/itau/empresas/ui/util/maquinaestado/Estado;)Lcom/itau/empresas/ui/util/MaquinaDeEstado;

    move-result-object v0

    sget-object v1, Lcom/itau/empresas/feature/itoken/ITokenActivity$Estados;->ITOKEN_INSTALADO_ASSOCIADO:Lcom/itau/empresas/feature/itoken/ITokenActivity$Estados;

    new-instance v2, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;

    invoke-direct {v2}, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;-><init>()V

    iget-object v3, p0, Lcom/itau/empresas/feature/itoken/ITokenActivity;->onItokenInstaladoAssociado:Lcom/itau/empresas/ui/util/maquinaestado/Estado$OnChangeState;

    .line 332
    invoke-virtual {v2, v3}, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;->onChangeState(Lcom/itau/empresas/ui/util/maquinaestado/Estado$OnChangeState;)Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;

    move-result-object v2

    .line 333
    invoke-virtual {v2}, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;->build()Lcom/itau/empresas/ui/util/maquinaestado/Estado;

    move-result-object v2

    .line 330
    invoke-virtual {v0, v1, v2}, Lcom/itau/empresas/ui/util/MaquinaDeEstado;->novoEstado(Lcom/itau/empresas/ui/util/MaquinaDeEstado$Chave;Lcom/itau/empresas/ui/util/maquinaestado/Estado;)Lcom/itau/empresas/ui/util/MaquinaDeEstado;

    move-result-object v0

    sget-object v1, Lcom/itau/empresas/feature/itoken/ITokenActivity$Estados;->ITOKEN_CANCELAMENTO_SIMPLES:Lcom/itau/empresas/feature/itoken/ITokenActivity$Estados;

    new-instance v2, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;

    invoke-direct {v2}, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;-><init>()V

    iget-object v3, p0, Lcom/itau/empresas/feature/itoken/ITokenActivity;->onItokenCancelamentoSimples:Lcom/itau/empresas/ui/util/maquinaestado/Estado$OnChangeState;

    .line 336
    invoke-virtual {v2, v3}, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;->onChangeState(Lcom/itau/empresas/ui/util/maquinaestado/Estado$OnChangeState;)Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;

    move-result-object v2

    .line 337
    invoke-virtual {v2}, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;->build()Lcom/itau/empresas/ui/util/maquinaestado/Estado;

    move-result-object v2

    .line 334
    invoke-virtual {v0, v1, v2}, Lcom/itau/empresas/ui/util/MaquinaDeEstado;->novoEstado(Lcom/itau/empresas/ui/util/MaquinaDeEstado$Chave;Lcom/itau/empresas/ui/util/maquinaestado/Estado;)Lcom/itau/empresas/ui/util/MaquinaDeEstado;

    move-result-object v0

    sget-object v1, Lcom/itau/empresas/feature/itoken/ITokenActivity$Estados;->ITOKEN_CANCELAMENTO_OUTRO:Lcom/itau/empresas/feature/itoken/ITokenActivity$Estados;

    new-instance v2, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;

    invoke-direct {v2}, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;-><init>()V

    iget-object v3, p0, Lcom/itau/empresas/feature/itoken/ITokenActivity;->onItokenCancelamentoOutro:Lcom/itau/empresas/ui/util/maquinaestado/Estado$OnChangeState;

    .line 340
    invoke-virtual {v2, v3}, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;->onChangeState(Lcom/itau/empresas/ui/util/maquinaestado/Estado$OnChangeState;)Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;

    move-result-object v2

    .line 341
    invoke-virtual {v2}, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;->build()Lcom/itau/empresas/ui/util/maquinaestado/Estado;

    move-result-object v2

    .line 338
    invoke-virtual {v0, v1, v2}, Lcom/itau/empresas/ui/util/MaquinaDeEstado;->novoEstado(Lcom/itau/empresas/ui/util/MaquinaDeEstado$Chave;Lcom/itau/empresas/ui/util/maquinaestado/Estado;)Lcom/itau/empresas/ui/util/MaquinaDeEstado;

    move-result-object v0

    sget-object v1, Lcom/itau/empresas/feature/itoken/ITokenActivity$Estados;->ITOKEN_BLOQUEADO:Lcom/itau/empresas/feature/itoken/ITokenActivity$Estados;

    new-instance v2, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;

    invoke-direct {v2}, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;-><init>()V

    iget-object v3, p0, Lcom/itau/empresas/feature/itoken/ITokenActivity;->onItokenBloqueado:Lcom/itau/empresas/ui/util/maquinaestado/Estado$OnChangeState;

    .line 345
    invoke-virtual {v2, v3}, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;->onChangeState(Lcom/itau/empresas/ui/util/maquinaestado/Estado$OnChangeState;)Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;

    move-result-object v2

    .line 346
    invoke-virtual {v2}, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;->build()Lcom/itau/empresas/ui/util/maquinaestado/Estado;

    move-result-object v2

    .line 343
    invoke-virtual {v0, v1, v2}, Lcom/itau/empresas/ui/util/MaquinaDeEstado;->novoEstado(Lcom/itau/empresas/ui/util/MaquinaDeEstado$Chave;Lcom/itau/empresas/ui/util/maquinaestado/Estado;)Lcom/itau/empresas/ui/util/MaquinaDeEstado;

    move-result-object v0

    sget-object v1, Lcom/itau/empresas/feature/itoken/ITokenActivity$Estados;->ITOKEN_LIBERAR:Lcom/itau/empresas/feature/itoken/ITokenActivity$Estados;

    new-instance v2, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;

    invoke-direct {v2}, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;-><init>()V

    iget-object v3, p0, Lcom/itau/empresas/feature/itoken/ITokenActivity;->onItokenLiberar:Lcom/itau/empresas/ui/util/maquinaestado/Estado$OnChangeState;

    .line 349
    invoke-virtual {v2, v3}, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;->onChangeState(Lcom/itau/empresas/ui/util/maquinaestado/Estado$OnChangeState;)Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;

    move-result-object v2

    .line 350
    invoke-virtual {v2}, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;->build()Lcom/itau/empresas/ui/util/maquinaestado/Estado;

    move-result-object v2

    .line 347
    invoke-virtual {v0, v1, v2}, Lcom/itau/empresas/ui/util/MaquinaDeEstado;->novoEstado(Lcom/itau/empresas/ui/util/MaquinaDeEstado$Chave;Lcom/itau/empresas/ui/util/maquinaestado/Estado;)Lcom/itau/empresas/ui/util/MaquinaDeEstado;

    move-result-object v0

    sget-object v1, Lcom/itau/empresas/feature/itoken/ITokenActivity$Estados;->ITOKEN_ASSOCIAR:Lcom/itau/empresas/feature/itoken/ITokenActivity$Estados;

    new-instance v2, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;

    invoke-direct {v2}, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;-><init>()V

    iget-object v3, p0, Lcom/itau/empresas/feature/itoken/ITokenActivity;->onItokenAssociar:Lcom/itau/empresas/ui/util/maquinaestado/Estado$OnChangeState;

    .line 353
    invoke-virtual {v2, v3}, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;->onChangeState(Lcom/itau/empresas/ui/util/maquinaestado/Estado$OnChangeState;)Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;

    move-result-object v2

    .line 354
    invoke-virtual {v2}, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;->build()Lcom/itau/empresas/ui/util/maquinaestado/Estado;

    move-result-object v2

    .line 351
    invoke-virtual {v0, v1, v2}, Lcom/itau/empresas/ui/util/MaquinaDeEstado;->novoEstado(Lcom/itau/empresas/ui/util/MaquinaDeEstado$Chave;Lcom/itau/empresas/ui/util/maquinaestado/Estado;)Lcom/itau/empresas/ui/util/MaquinaDeEstado;

    move-result-object v0

    sget-object v1, Lcom/itau/empresas/feature/itoken/ITokenActivity$Estados;->ITOKEN_LIBERAR_OUTRO_APARELHO:Lcom/itau/empresas/feature/itoken/ITokenActivity$Estados;

    new-instance v2, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;

    invoke-direct {v2}, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;-><init>()V

    iget-object v3, p0, Lcom/itau/empresas/feature/itoken/ITokenActivity;->onItokenLiberarOutroAparelho:Lcom/itau/empresas/ui/util/maquinaestado/Estado$OnChangeState;

    .line 357
    invoke-virtual {v2, v3}, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;->onChangeState(Lcom/itau/empresas/ui/util/maquinaestado/Estado$OnChangeState;)Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;

    move-result-object v2

    .line 358
    invoke-virtual {v2}, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;->build()Lcom/itau/empresas/ui/util/maquinaestado/Estado;

    move-result-object v2

    .line 355
    invoke-virtual {v0, v1, v2}, Lcom/itau/empresas/ui/util/MaquinaDeEstado;->novoEstado(Lcom/itau/empresas/ui/util/MaquinaDeEstado$Chave;Lcom/itau/empresas/ui/util/maquinaestado/Estado;)Lcom/itau/empresas/ui/util/MaquinaDeEstado;

    move-result-object v0

    sget-object v1, Lcom/itau/empresas/feature/itoken/ITokenActivity$Estados;->ITOKEN_LIBERAR_OUTRO_OPERADOR:Lcom/itau/empresas/feature/itoken/ITokenActivity$Estados;

    new-instance v2, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;

    invoke-direct {v2}, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;-><init>()V

    iget-object v3, p0, Lcom/itau/empresas/feature/itoken/ITokenActivity;->onItokenLiberar:Lcom/itau/empresas/ui/util/maquinaestado/Estado$OnChangeState;

    .line 361
    invoke-virtual {v2, v3}, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;->onChangeState(Lcom/itau/empresas/ui/util/maquinaestado/Estado$OnChangeState;)Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;

    move-result-object v2

    .line 362
    invoke-virtual {v2}, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;->build()Lcom/itau/empresas/ui/util/maquinaestado/Estado;

    move-result-object v2

    .line 359
    invoke-virtual {v0, v1, v2}, Lcom/itau/empresas/ui/util/MaquinaDeEstado;->novoEstado(Lcom/itau/empresas/ui/util/MaquinaDeEstado$Chave;Lcom/itau/empresas/ui/util/maquinaestado/Estado;)Lcom/itau/empresas/ui/util/MaquinaDeEstado;

    move-result-object v0

    sget-object v1, Lcom/itau/empresas/feature/itoken/ITokenActivity$Estados;->ITOKEN_NOVA_INSTALACAO:Lcom/itau/empresas/feature/itoken/ITokenActivity$Estados;

    new-instance v2, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;

    invoke-direct {v2}, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;-><init>()V

    iget-object v3, p0, Lcom/itau/empresas/feature/itoken/ITokenActivity;->onItokenNovaInstalacao:Lcom/itau/empresas/ui/util/maquinaestado/Estado$OnChangeState;

    .line 365
    invoke-virtual {v2, v3}, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;->onChangeState(Lcom/itau/empresas/ui/util/maquinaestado/Estado$OnChangeState;)Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;

    move-result-object v2

    .line 366
    invoke-virtual {v2}, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;->build()Lcom/itau/empresas/ui/util/maquinaestado/Estado;

    move-result-object v2

    .line 363
    invoke-virtual {v0, v1, v2}, Lcom/itau/empresas/ui/util/MaquinaDeEstado;->novoEstado(Lcom/itau/empresas/ui/util/MaquinaDeEstado$Chave;Lcom/itau/empresas/ui/util/maquinaestado/Estado;)Lcom/itau/empresas/ui/util/MaquinaDeEstado;

    move-result-object v0

    .line 313
    return-object v0
.end method

.method public static intent(Landroid/content/Context;)Lcom/itau/empresas/feature/itoken/ITokenActivity_$IntentBuilder_;
    .registers 2
    .param p0, "context"    # Landroid/content/Context;

    .line 410
    new-instance v0, Lcom/itau/empresas/feature/itoken/ITokenActivity_$IntentBuilder_;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/itoken/ITokenActivity_$IntentBuilder_;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method private mudaParaEstadoCanceladoOuInstalacao()V
    .registers 3

    .line 272
    iget-object v0, p0, Lcom/itau/empresas/feature/itoken/ITokenActivity;->anterior:Lcom/itau/empresas/ui/util/MaquinaDeEstado$Chave;

    if-eqz v0, :cond_10

    .line 273
    sget-object v0, Lcom/itau/empresas/feature/itoken/ITokenActivity$Estados;->ITOKEN_INSTALACAO_CANCELADO:Lcom/itau/empresas/feature/itoken/ITokenActivity$Estados;

    iput-object v0, p0, Lcom/itau/empresas/feature/itoken/ITokenActivity;->anterior:Lcom/itau/empresas/ui/util/MaquinaDeEstado$Chave;

    .line 274
    iget-object v0, p0, Lcom/itau/empresas/feature/itoken/ITokenActivity;->maquina:Lcom/itau/empresas/ui/util/MaquinaDeEstado;

    sget-object v1, Lcom/itau/empresas/feature/itoken/ITokenActivity$Estados;->ITOKEN_INSTALACAO_CANCELADO:Lcom/itau/empresas/feature/itoken/ITokenActivity$Estados;

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/MaquinaDeEstado;->mudaParaEstado(Lcom/itau/empresas/ui/util/MaquinaDeEstado$Chave;)Lcom/itau/empresas/ui/util/MaquinaDeEstado$Chave;

    goto :goto_1b

    .line 276
    :cond_10
    sget-object v0, Lcom/itau/empresas/feature/itoken/ITokenActivity$Estados;->ITOKEN_INSTALACAO:Lcom/itau/empresas/feature/itoken/ITokenActivity$Estados;

    iput-object v0, p0, Lcom/itau/empresas/feature/itoken/ITokenActivity;->anterior:Lcom/itau/empresas/ui/util/MaquinaDeEstado$Chave;

    .line 277
    iget-object v0, p0, Lcom/itau/empresas/feature/itoken/ITokenActivity;->maquina:Lcom/itau/empresas/ui/util/MaquinaDeEstado;

    sget-object v1, Lcom/itau/empresas/feature/itoken/ITokenActivity$Estados;->ITOKEN_INSTALACAO:Lcom/itau/empresas/feature/itoken/ITokenActivity$Estados;

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/MaquinaDeEstado;->mudaParaEstado(Lcom/itau/empresas/ui/util/MaquinaDeEstado$Chave;)Lcom/itau/empresas/ui/util/MaquinaDeEstado$Chave;

    .line 279
    :goto_1b
    return-void
.end method

.method private mudaParaEstadoInstaladoOuAssociar()V
    .registers 6

    .line 282
    iget-object v0, p0, Lcom/itau/empresas/feature/itoken/ITokenActivity;->anterior:Lcom/itau/empresas/ui/util/MaquinaDeEstado$Chave;

    sget-object v1, Lcom/itau/empresas/feature/itoken/ITokenActivity$Estados;->ITOKEN_INSTALACAO:Lcom/itau/empresas/feature/itoken/ITokenActivity$Estados;

    if-eq v0, v1, :cond_c

    iget-object v0, p0, Lcom/itau/empresas/feature/itoken/ITokenActivity;->anterior:Lcom/itau/empresas/ui/util/MaquinaDeEstado$Chave;

    sget-object v1, Lcom/itau/empresas/feature/itoken/ITokenActivity$Estados;->ITOKEN_INSTALACAO_CANCELADO:Lcom/itau/empresas/feature/itoken/ITokenActivity$Estados;

    if-ne v0, v1, :cond_6a

    .line 284
    :cond_c
    sget-object v0, Lcom/itau/empresas/feature/itoken/ITokenActivity$Estados;->ITOKEN_INSTALADO_NOVO:Lcom/itau/empresas/feature/itoken/ITokenActivity$Estados;

    iput-object v0, p0, Lcom/itau/empresas/feature/itoken/ITokenActivity;->anterior:Lcom/itau/empresas/ui/util/MaquinaDeEstado$Chave;

    .line 285
    iget-object v0, p0, Lcom/itau/empresas/feature/itoken/ITokenActivity;->maquina:Lcom/itau/empresas/ui/util/MaquinaDeEstado;

    sget-object v1, Lcom/itau/empresas/feature/itoken/ITokenActivity$Estados;->ITOKEN_INSTALADO_NOVO:Lcom/itau/empresas/feature/itoken/ITokenActivity$Estados;

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/MaquinaDeEstado;->mudaParaEstado(Lcom/itau/empresas/ui/util/MaquinaDeEstado$Chave;)Lcom/itau/empresas/ui/util/MaquinaDeEstado$Chave;

    .line 287
    const v0, 0x7f0702df

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/itoken/ITokenActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 288
    const v1, 0x7f07030e

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/itoken/ITokenActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 287
    invoke-virtual {p0, v0, v1}, Lcom/itau/empresas/feature/itoken/ITokenActivity;->analyticsHit(Ljava/lang/String;Ljava/lang/String;)V

    .line 290
    new-instance v0, Landroid/support/v7/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/support/v7/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 291
    const v1, 0x7f0706b1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/AlertDialog$Builder;->setTitle(I)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v0

    .line 292
    const v1, 0x7f07046f

    invoke-virtual {v0, v1}, Landroid/support/v7/app/AlertDialog$Builder;->setMessage(I)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v0

    .line 293
    const v1, 0x7f07048c

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/support/v7/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v0

    .line 294
    invoke-virtual {v0}, Landroid/support/v7/app/AlertDialog$Builder;->show()Landroid/support/v7/app/AlertDialog;

    .line 296
    iget-object v0, p0, Lcom/itau/empresas/feature/itoken/ITokenActivity;->application:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getEventoTracker()Lcom/itau/empresas/ui/util/analytics/TrackerEvento;

    move-result-object v0

    new-instance v1, Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;

    .line 297
    const v2, 0x7f0702df

    invoke-virtual {p0, v2}, Lcom/itau/empresas/feature/itoken/ITokenActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 298
    const v3, 0x7f070316

    invoke-virtual {p0, v3}, Lcom/itau/empresas/feature/itoken/ITokenActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 299
    const v4, 0x7f07030a

    invoke-virtual {p0, v4}, Lcom/itau/empresas/feature/itoken/ITokenActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 296
    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/analytics/TrackerEvento;->enviarHitEvento(Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;)V

    goto :goto_87

    .line 301
    :cond_6a
    iget-object v0, p0, Lcom/itau/empresas/feature/itoken/ITokenActivity;->anterior:Lcom/itau/empresas/ui/util/MaquinaDeEstado$Chave;

    sget-object v1, Lcom/itau/empresas/feature/itoken/ITokenActivity$Estados;->ITOKEN_ASSOCIAR:Lcom/itau/empresas/feature/itoken/ITokenActivity$Estados;

    if-ne v0, v1, :cond_7c

    .line 302
    sget-object v0, Lcom/itau/empresas/feature/itoken/ITokenActivity$Estados;->ITOKEN_INSTALADO_ASSOCIADO:Lcom/itau/empresas/feature/itoken/ITokenActivity$Estados;

    iput-object v0, p0, Lcom/itau/empresas/feature/itoken/ITokenActivity;->anterior:Lcom/itau/empresas/ui/util/MaquinaDeEstado$Chave;

    .line 303
    iget-object v0, p0, Lcom/itau/empresas/feature/itoken/ITokenActivity;->maquina:Lcom/itau/empresas/ui/util/MaquinaDeEstado;

    sget-object v1, Lcom/itau/empresas/feature/itoken/ITokenActivity$Estados;->ITOKEN_INSTALADO_ASSOCIADO:Lcom/itau/empresas/feature/itoken/ITokenActivity$Estados;

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/MaquinaDeEstado;->mudaParaEstado(Lcom/itau/empresas/ui/util/MaquinaDeEstado$Chave;)Lcom/itau/empresas/ui/util/MaquinaDeEstado$Chave;

    goto :goto_87

    .line 305
    :cond_7c
    sget-object v0, Lcom/itau/empresas/feature/itoken/ITokenActivity$Estados;->ITOKEN_INSTALADO:Lcom/itau/empresas/feature/itoken/ITokenActivity$Estados;

    iput-object v0, p0, Lcom/itau/empresas/feature/itoken/ITokenActivity;->anterior:Lcom/itau/empresas/ui/util/MaquinaDeEstado$Chave;

    .line 306
    iget-object v0, p0, Lcom/itau/empresas/feature/itoken/ITokenActivity;->maquina:Lcom/itau/empresas/ui/util/MaquinaDeEstado;

    sget-object v1, Lcom/itau/empresas/feature/itoken/ITokenActivity$Estados;->ITOKEN_INSTALADO:Lcom/itau/empresas/feature/itoken/ITokenActivity$Estados;

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/MaquinaDeEstado;->mudaParaEstado(Lcom/itau/empresas/ui/util/MaquinaDeEstado$Chave;)Lcom/itau/empresas/ui/util/MaquinaDeEstado$Chave;

    .line 308
    :goto_87
    return-void
.end method


# virtual methods
.method aoCarregarTela()V
    .registers 2

    .line 217
    invoke-direct {p0}, Lcom/itau/empresas/feature/itoken/ITokenActivity;->constroiToolbar()V

    .line 220
    invoke-static {}, Lcom/itau/empresas/feature/itoken/fragment/ITokenInstalacaoFragment_;->builder()Lcom/itau/empresas/feature/itoken/fragment/ITokenInstalacaoFragment_$FragmentBuilder_;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/feature/itoken/fragment/ITokenInstalacaoFragment_$FragmentBuilder_;->build()Lcom/itau/empresas/feature/itoken/fragment/ITokenInstalacaoFragment;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/itoken/ITokenActivity;->iTokenInstalacaoFragment:Lcom/itau/empresas/feature/itoken/fragment/ITokenInstalacaoFragment;

    .line 221
    invoke-static {}, Lcom/itau/empresas/feature/itoken/fragment/ITokenAssociarFragment_;->builder()Lcom/itau/empresas/feature/itoken/fragment/ITokenAssociarFragment_$FragmentBuilder_;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/feature/itoken/fragment/ITokenAssociarFragment_$FragmentBuilder_;->build()Lcom/itau/empresas/feature/itoken/fragment/ITokenAssociarFragment;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/itoken/ITokenActivity;->iTokenAssociarFragment:Lcom/itau/empresas/feature/itoken/fragment/ITokenAssociarFragment;

    .line 222
    invoke-static {}, Lcom/itau/empresas/feature/itoken/fragment/ITokenCancelamentoSimples_;->builder()Lcom/itau/empresas/feature/itoken/fragment/ITokenCancelamentoSimples_$FragmentBuilder_;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/feature/itoken/fragment/ITokenCancelamentoSimples_$FragmentBuilder_;->build()Lcom/itau/empresas/feature/itoken/fragment/ITokenCancelamentoSimples;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/itoken/ITokenActivity;->iTokenCancelamentoSimples:Lcom/itau/empresas/feature/itoken/fragment/ITokenCancelamentoSimples;

    .line 223
    invoke-static {}, Lcom/itau/empresas/feature/itoken/fragment/ITokenInstaladoFragment_;->builder()Lcom/itau/empresas/feature/itoken/fragment/ITokenInstaladoFragment_$FragmentBuilder_;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/feature/itoken/fragment/ITokenInstaladoFragment_$FragmentBuilder_;->build()Lcom/itau/empresas/feature/itoken/fragment/ITokenInstaladoFragment;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/itoken/ITokenActivity;->iTokenInstaladoFragment:Lcom/itau/empresas/feature/itoken/fragment/ITokenInstaladoFragment;

    .line 224
    invoke-static {}, Lcom/itau/empresas/feature/itoken/fragment/ITokenCancelamentoOutroOperador_;->builder()Lcom/itau/empresas/feature/itoken/fragment/ITokenCancelamentoOutroOperador_$FragmentBuilder_;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/feature/itoken/fragment/ITokenCancelamentoOutroOperador_$FragmentBuilder_;->build()Lcom/itau/empresas/feature/itoken/fragment/ITokenCancelamentoOutroOperador;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/itoken/ITokenActivity;->iTokenCancelamentoOutro:Lcom/itau/empresas/feature/itoken/fragment/ITokenCancelamentoOutroOperador;

    .line 225
    invoke-static {}, Lcom/itau/empresas/feature/itoken/fragment/ITokenEfetivacaoDesbloqueioFragment_;->builder()Lcom/itau/empresas/feature/itoken/fragment/ITokenEfetivacaoDesbloqueioFragment_$FragmentBuilder_;

    move-result-object v0

    .line 226
    invoke-virtual {v0}, Lcom/itau/empresas/feature/itoken/fragment/ITokenEfetivacaoDesbloqueioFragment_$FragmentBuilder_;->build()Lcom/itau/empresas/feature/itoken/fragment/ITokenEfetivacaoDesbloqueioFragment;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/itoken/ITokenActivity;->iTokenEfetivacaoDesbloqueioFragment:Lcom/itau/empresas/feature/itoken/fragment/ITokenEfetivacaoDesbloqueioFragment;

    .line 227
    invoke-static {}, Lcom/itau/empresas/feature/itoken/fragment/ITokenLiberacaoFragment_;->builder()Lcom/itau/empresas/feature/itoken/fragment/ITokenLiberacaoFragment_$FragmentBuilder_;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/feature/itoken/fragment/ITokenLiberacaoFragment_$FragmentBuilder_;->build()Lcom/itau/empresas/feature/itoken/fragment/ITokenLiberacaoFragment;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/itoken/ITokenActivity;->iTokenLiberacaoFragment:Lcom/itau/empresas/feature/itoken/fragment/ITokenLiberacaoFragment;

    .line 228
    invoke-static {}, Lcom/itau/empresas/feature/itoken/fragment/ITokenLiberacaoOutroCelularFragment_;->builder()Lcom/itau/empresas/feature/itoken/fragment/ITokenLiberacaoOutroCelularFragment_$FragmentBuilder_;

    move-result-object v0

    .line 229
    invoke-virtual {v0}, Lcom/itau/empresas/feature/itoken/fragment/ITokenLiberacaoOutroCelularFragment_$FragmentBuilder_;->build()Lcom/itau/empresas/feature/itoken/fragment/ITokenLiberacaoOutroCelularFragment;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/itoken/ITokenActivity;->iTokenLiberacaoOutroCelularFragment:Lcom/itau/empresas/feature/itoken/fragment/ITokenLiberacaoOutroCelularFragment;

    .line 230
    invoke-static {}, Lcom/itau/empresas/feature/itoken/fragment/ITokenNovaInstalacaoFragment_;->builder()Lcom/itau/empresas/feature/itoken/fragment/ITokenNovaInstalacaoFragment_$FragmentBuilder_;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/feature/itoken/fragment/ITokenNovaInstalacaoFragment_$FragmentBuilder_;->build()Lcom/itau/empresas/feature/itoken/fragment/ITokenNovaInstalacaoFragment;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/itoken/ITokenActivity;->iTokenNovaInstalacao:Lcom/itau/empresas/feature/itoken/fragment/ITokenNovaInstalacaoFragment;

    .line 232
    invoke-virtual {p0}, Lcom/itau/empresas/feature/itoken/ITokenActivity;->mostraDialogoDeProgresso()V

    .line 233
    invoke-direct {p0}, Lcom/itau/empresas/feature/itoken/ITokenActivity;->inicializaMaquinaDeEstado()Lcom/itau/empresas/ui/util/MaquinaDeEstado;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/itoken/ITokenActivity;->maquina:Lcom/itau/empresas/ui/util/MaquinaDeEstado;

    .line 235
    invoke-static {}, Lbr/com/itau/topsnackbar/TopSnackbar;->dismissPrevious()V

    .line 236
    iget-object v0, p0, Lcom/itau/empresas/feature/itoken/ITokenActivity;->controller:Lcom/itau/empresas/controller/ITokenController;

    invoke-virtual {v0}, Lcom/itau/empresas/controller/ITokenController;->consultarTokenApp()V

    .line 237
    return-void
.end method

.method public dismissSnackbar()V
    .registers 2

    .line 207
    iget-object v0, p0, Lcom/itau/empresas/feature/itoken/ITokenActivity;->topSnackbar:Lcom/itau/empresas/ui/util/SnackbarUtils$Gerenciador;

    if-eqz v0, :cond_9

    .line 208
    iget-object v0, p0, Lcom/itau/empresas/feature/itoken/ITokenActivity;->topSnackbar:Lcom/itau/empresas/ui/util/SnackbarUtils$Gerenciador;

    invoke-interface {v0}, Lcom/itau/empresas/ui/util/SnackbarUtils$Gerenciador;->dismissSnackbar()V

    .line 209
    :cond_9
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 196
    invoke-static {p0}, Lcom/itau/empresas/ui/util/X;->y(Landroid/content/Context;)V

    .line 197
    invoke-super {p0, p1}, Lcom/itau/empresas/ui/activity/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 198
    new-instance v0, Lcom/itau/empresas/ui/util/SnackbarUtils$GerenciadorDelegate;

    iget-object v1, p0, Lcom/itau/empresas/feature/itoken/ITokenActivity;->root:Landroid/widget/LinearLayout;

    invoke-direct {v0, v1}, Lcom/itau/empresas/ui/util/SnackbarUtils$GerenciadorDelegate;-><init>(Landroid/view/ViewGroup;)V

    iput-object v0, p0, Lcom/itau/empresas/feature/itoken/ITokenActivity;->topSnackbar:Lcom/itau/empresas/ui/util/SnackbarUtils$Gerenciador;

    .line 199
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .registers 6
    .param p1, "menu"    # Landroid/view/Menu;

    .line 382
    invoke-static {}, Lcom/itau/empresas/ui/util/PoyntUtils;->isPoynt()Z

    move-result v0

    if-eqz v0, :cond_26

    .line 383
    invoke-virtual {p0}, Lcom/itau/empresas/feature/itoken/ITokenActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f0f0005

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 385
    const v0, 0x7f0e06c6

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    .line 386
    .local v2, "itemMenuPesquisa":Landroid/view/MenuItem;
    const/4 v0, 0x0

    invoke-interface {v2, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 388
    const v0, 0x7f0e06c5

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v3

    .line 389
    .local v3, "itemMenuFiltro":Landroid/view/MenuItem;
    const/4 v0, 0x0

    invoke-interface {v3, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 392
    .end local v2    # "itemMenuPesquisa":Landroid/view/MenuItem;
    .end local v3    # "itemMenuFiltro":Landroid/view/MenuItem;
    :cond_26
    const/4 v0, 0x1

    return v0
.end method

.method public onEventMainThread(Lcom/itau/empresas/api/model/TokenAppVO;)V
    .registers 4
    .param p1, "tokenAppVO"    # Lcom/itau/empresas/api/model/TokenAppVO;

    .line 247
    invoke-virtual {p0}, Lcom/itau/empresas/feature/itoken/ITokenActivity;->escondeDialogoDeProgresso()V

    .line 248
    const-string v0, "B"

    invoke-virtual {p1}, Lcom/itau/empresas/api/model/TokenAppVO;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_17

    .line 249
    iget-object v0, p0, Lcom/itau/empresas/feature/itoken/ITokenActivity;->maquina:Lcom/itau/empresas/ui/util/MaquinaDeEstado;

    sget-object v1, Lcom/itau/empresas/feature/itoken/ITokenActivity$Estados;->ITOKEN_BLOQUEADO:Lcom/itau/empresas/feature/itoken/ITokenActivity$Estados;

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/MaquinaDeEstado;->mudaParaEstado(Lcom/itau/empresas/ui/util/MaquinaDeEstado$Chave;)Lcom/itau/empresas/ui/util/MaquinaDeEstado$Chave;

    .line 250
    return-void

    .line 252
    :cond_17
    invoke-virtual {p1}, Lcom/itau/empresas/api/model/TokenAppVO;->getStatus()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/itau/empresas/feature/itoken/ITokenActivity;->atualizaEstado(Ljava/lang/String;)V

    .line 253
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .registers 4
    .param p1, "item"    # Landroid/view/MenuItem;

    .line 397
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x7f0e06cf

    if-ne v0, v1, :cond_10

    .line 398
    iget-object v0, p0, Lcom/itau/empresas/feature/itoken/ITokenActivity;->application:Lcom/itau/empresas/CustomApplication;

    invoke-static {v0, p0}, Lcom/itau/empresas/ui/util/ViewUtils;->aoClicarBotaoSairMenu(Lcom/itau/empresas/CustomApplication;Lcom/itau/empresas/ui/activity/BaseActivity;)V

    .line 399
    const/4 v0, 0x1

    return v0

    .line 401
    :cond_10
    invoke-super {p0, p1}, Lcom/itau/empresas/ui/activity/BaseActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0
.end method

.method public opkeysConsumidas()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Ljava/lang/String;>;"
        }
    .end annotation

    .line 203
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public showSnackbar(Ljava/lang/CharSequence;I)V
    .registers 4
    .param p1, "texto"    # Ljava/lang/CharSequence;
    .param p2, "duracao"    # I

    .line 212
    iget-object v0, p0, Lcom/itau/empresas/feature/itoken/ITokenActivity;->topSnackbar:Lcom/itau/empresas/ui/util/SnackbarUtils$Gerenciador;

    invoke-interface {v0, p1, p2}, Lcom/itau/empresas/ui/util/SnackbarUtils$Gerenciador;->showSnackbar(Ljava/lang/CharSequence;I)V

    .line 213
    return-void
.end method

.class Lde/greenrobot/event/SubscriberMethodFinder;
.super Ljava/lang/Object;
.source "SubscriberMethodFinder.java"


# static fields
.field private static final methodCache:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Lde/greenrobot/event/SubscriberMethod;>;>;"
        }
    .end annotation
.end field


# instance fields
.field private final skipMethodVerificationForClasses:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<Ljava/lang/Class<*>;Ljava/lang/Class<*>;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .line 40
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lde/greenrobot/event/SubscriberMethodFinder;->methodCache:Ljava/util/Map;

    return-void
.end method

.method constructor <init>(Ljava/util/List;)V
    .registers 5
    .param p1, "skipMethodVerificationForClassesList"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<Ljava/lang/Class<*>;>;)V"
        }
    .end annotation

    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lde/greenrobot/event/SubscriberMethodFinder;->skipMethodVerificationForClasses:Ljava/util/Map;

    .line 46
    if-eqz p1, :cond_23

    .line 47
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_10
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_23

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Ljava/lang/Class;

    .line 48
    .local v2, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    iget-object v0, p0, Lde/greenrobot/event/SubscriberMethodFinder;->skipMethodVerificationForClasses:Ljava/util/Map;

    invoke-interface {v0, v2, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 49
    .end local v2    # "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .end local v2
    goto :goto_10

    .line 51
    :cond_23
    return-void
.end method

.method private filterSubscriberMethods(Ljava/util/List;Ljava/util/HashMap;Ljava/lang/StringBuilder;[Ljava/lang/reflect/Method;)V
    .registers 20
    .param p1, "subscriberMethods"    # Ljava/util/List;
    .param p2, "eventTypesFound"    # Ljava/util/HashMap;
    .param p3, "methodKeyBuilder"    # Ljava/lang/StringBuilder;
    .param p4, "methods"    # [Ljava/lang/reflect/Method;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<Lde/greenrobot/event/SubscriberMethod;>;Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Class;>;Ljava/lang/StringBuilder;[Ljava/lang/reflect/Method;)V"
        }
    .end annotation

    .line 103
    move-object/from16 v3, p4

    array-length v4, v3

    const/4 v5, 0x0

    :goto_4
    if-ge v5, v4, :cond_a5

    aget-object v6, v3, v5

    .line 104
    .local v6, "method":Ljava/lang/reflect/Method;
    invoke-virtual {v6}, Ljava/lang/reflect/Method;->getName()Ljava/lang/String;

    move-result-object v7

    .line 105
    .local v7, "methodName":Ljava/lang/String;
    const-string v0, "onEvent"

    invoke-virtual {v7, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a1

    .line 106
    invoke-virtual {v6}, Ljava/lang/reflect/Method;->getModifiers()I

    move-result v8

    .line 107
    .local v8, "modifiers":I
    invoke-virtual {v6}, Ljava/lang/reflect/Method;->getDeclaringClass()Ljava/lang/Class;

    move-result-object v9

    .line 108
    .local v9, "methodClass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    and-int/lit8 v0, v8, 0x1

    if-eqz v0, :cond_77

    and-int/lit16 v0, v8, 0x1448

    if-nez v0, :cond_77

    .line 109
    invoke-virtual {v6}, Ljava/lang/reflect/Method;->getParameterTypes()[Ljava/lang/Class;

    move-result-object v10

    .line 110
    .local v10, "parameterTypes":[Ljava/lang/Class;, "[Ljava/lang/Class<*>;"
    array-length v0, v10

    const/4 v1, 0x1

    if-ne v0, v1, :cond_76

    .line 111
    invoke-direct {p0, v9, v6, v7}, Lde/greenrobot/event/SubscriberMethodFinder;->getThreadMode(Ljava/lang/Class;Ljava/lang/reflect/Method;Ljava/lang/String;)Lde/greenrobot/event/ThreadMode;

    move-result-object v11

    .line 112
    .local v11, "threadMode":Lde/greenrobot/event/ThreadMode;
    if-nez v11, :cond_34

    .line 113
    goto/16 :goto_a1

    .line 115
    :cond_34
    const/4 v0, 0x0

    aget-object v12, v10, v0

    .line 116
    .local v12, "eventType":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    move-object/from16 v0, p3

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 117
    move-object/from16 v0, p3

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 118
    move-object/from16 v0, p3

    const/16 v1, 0x3e

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v12}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 119
    invoke-virtual/range {p3 .. p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    .line 120
    .local v13, "methodKey":Ljava/lang/String;
    move-object/from16 v0, p2

    invoke-virtual {v0, v13, v9}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v14, v0

    check-cast v14, Ljava/lang/Class;

    .line 121
    .local v14, "methodClassOld":Ljava/lang/Class;
    if-eqz v14, :cond_66

    invoke-virtual {v14, v9}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_71

    .line 123
    :cond_66
    new-instance v0, Lde/greenrobot/event/SubscriberMethod;

    invoke-direct {v0, v6, v11, v12}, Lde/greenrobot/event/SubscriberMethod;-><init>(Ljava/lang/reflect/Method;Lde/greenrobot/event/ThreadMode;Ljava/lang/Class;)V

    move-object/from16 v1, p1

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_76

    .line 126
    :cond_71
    move-object/from16 v0, p2

    invoke-virtual {v0, v13, v14}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 129
    .end local v10    # "parameterTypes":[Ljava/lang/Class;, "[Ljava/lang/Class<*>;"
    .end local v10
    .end local v11    # "threadMode":Lde/greenrobot/event/ThreadMode;
    .end local v12    # "eventType":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .end local v12
    .end local v13    # "methodKey":Ljava/lang/String;
    .end local v14    # "methodClassOld":Ljava/lang/Class;
    :cond_76
    :goto_76
    goto :goto_a1

    :cond_77
    iget-object v0, p0, Lde/greenrobot/event/SubscriberMethodFinder;->skipMethodVerificationForClasses:Ljava/util/Map;

    invoke-interface {v0, v9}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_a1

    .line 130
    sget-object v0, Lde/greenrobot/event/EventBus;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Skipping method (not public, static or abstract): "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 103
    .end local v6    # "method":Ljava/lang/reflect/Method;
    .end local v7    # "methodName":Ljava/lang/String;
    .end local v8    # "modifiers":I
    .end local v9    # "methodClass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .end local v9
    :cond_a1
    :goto_a1
    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_4

    .line 135
    :cond_a5
    return-void
.end method

.method private getThreadMode(Ljava/lang/Class;Ljava/lang/reflect/Method;Ljava/lang/String;)Lde/greenrobot/event/ThreadMode;
    .registers 9
    .param p1, "clazz"    # Ljava/lang/Class;
    .param p2, "method"    # Ljava/lang/reflect/Method;
    .param p3, "methodName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/lang/Class<*>;Ljava/lang/reflect/Method;Ljava/lang/String;)Lde/greenrobot/event/ThreadMode;"
        }
    .end annotation

    .line 138
    const-string v0, "onEvent"

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    invoke-virtual {p3, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    .line 140
    .local v3, "modifierString":Ljava/lang/String;
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_13

    .line 141
    sget-object v4, Lde/greenrobot/event/ThreadMode;->PostThread:Lde/greenrobot/event/ThreadMode;

    .local v4, "threadMode":Lde/greenrobot/event/ThreadMode;
    goto :goto_56

    .line 142
    .end local v4    # "threadMode":Lde/greenrobot/event/ThreadMode;
    :cond_13
    const-string v0, "MainThread"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1e

    .line 143
    sget-object v4, Lde/greenrobot/event/ThreadMode;->MainThread:Lde/greenrobot/event/ThreadMode;

    .local v4, "threadMode":Lde/greenrobot/event/ThreadMode;
    goto :goto_56

    .line 144
    .end local v4    # "threadMode":Lde/greenrobot/event/ThreadMode;
    :cond_1e
    const-string v0, "BackgroundThread"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_29

    .line 145
    sget-object v4, Lde/greenrobot/event/ThreadMode;->BackgroundThread:Lde/greenrobot/event/ThreadMode;

    .local v4, "threadMode":Lde/greenrobot/event/ThreadMode;
    goto :goto_56

    .line 146
    .end local v4    # "threadMode":Lde/greenrobot/event/ThreadMode;
    :cond_29
    const-string v0, "Async"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_34

    .line 147
    sget-object v4, Lde/greenrobot/event/ThreadMode;->Async:Lde/greenrobot/event/ThreadMode;

    .local v4, "threadMode":Lde/greenrobot/event/ThreadMode;
    goto :goto_56

    .line 149
    .end local v4    # "threadMode":Lde/greenrobot/event/ThreadMode;
    :cond_34
    iget-object v0, p0, Lde/greenrobot/event/SubscriberMethodFinder;->skipMethodVerificationForClasses:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_55

    .line 150
    new-instance v0, Lde/greenrobot/event/EventBusException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Illegal onEvent method, check for typos: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lde/greenrobot/event/EventBusException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 152
    :cond_55
    const/4 v4, 0x0

    .line 155
    .local v4, "threadMode":Lde/greenrobot/event/ThreadMode;
    :goto_56
    return-object v4
.end method


# virtual methods
.method findSubscriberMethods(Ljava/lang/Class;)Ljava/util/List;
    .registers 14
    .param p1, "subscriberClass"    # Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/lang/Class<*>;)Ljava/util/List<Lde/greenrobot/event/SubscriberMethod;>;"
        }
    .end annotation

    .line 54
    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    .line 56
    .local v3, "key":Ljava/lang/String;
    sget-object v5, Lde/greenrobot/event/SubscriberMethodFinder;->methodCache:Ljava/util/Map;

    monitor-enter v5

    .line 57
    :try_start_7
    sget-object v0, Lde/greenrobot/event/SubscriberMethodFinder;->methodCache:Ljava/util/Map;

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Ljava/util/List;
    :try_end_10
    .catchall {:try_start_7 .. :try_end_10} :catchall_12

    .line 58
    .local v4, "subscriberMethods":Ljava/util/List;, "Ljava/util/List<Lde/greenrobot/event/SubscriberMethod;>;"
    monitor-exit v5

    goto :goto_15

    :catchall_12
    move-exception v6

    monitor-exit v5

    throw v6

    .line 59
    :goto_15
    if-eqz v4, :cond_18

    .line 60
    return-object v4

    .line 62
    :cond_18
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 63
    move-object v5, p1

    .line 64
    .local v5, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    .line 65
    .local v6, "eventTypesFound":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Class;>;"
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    .line 66
    .local v7, "methodKeyBuilder":Ljava/lang/StringBuilder;
    :goto_28
    if-eqz v5, :cond_66

    .line 67
    invoke-virtual {v5}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v8

    .line 68
    .local v8, "name":Ljava/lang/String;
    const-string v0, "java."

    invoke-virtual {v8, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_66

    const-string v0, "javax."

    invoke-virtual {v8, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_66

    const-string v0, "android."

    invoke-virtual {v8, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_47

    .line 70
    goto :goto_66

    .line 76
    :cond_47
    :try_start_47
    invoke-virtual {v5}, Ljava/lang/Class;->getDeclaredMethods()[Ljava/lang/reflect/Method;

    move-result-object v9

    .line 77
    .local v9, "methods":[Ljava/lang/reflect/Method;
    invoke-direct {p0, v4, v6, v7, v9}, Lde/greenrobot/event/SubscriberMethodFinder;->filterSubscriberMethods(Ljava/util/List;Ljava/util/HashMap;Ljava/lang/StringBuilder;[Ljava/lang/reflect/Method;)V
    :try_end_4e
    .catch Ljava/lang/Throwable; {:try_start_47 .. :try_end_4e} :catch_4f

    .line 86
    .end local v9    # "methods":[Ljava/lang/reflect/Method;
    goto :goto_61

    .line 78
    :catch_4f
    move-exception v9

    .line 79
    .local v9, "th":Ljava/lang/Throwable;
    invoke-virtual {v9}, Ljava/lang/Throwable;->printStackTrace()V

    .line 81
    invoke-virtual {p1}, Ljava/lang/Class;->getMethods()[Ljava/lang/reflect/Method;

    move-result-object v10

    .line 82
    .local v10, "methods":[Ljava/lang/reflect/Method;
    invoke-interface {v4}, Ljava/util/List;->clear()V

    .line 83
    invoke-virtual {v6}, Ljava/util/HashMap;->clear()V

    .line 84
    invoke-direct {p0, v4, v6, v7, v10}, Lde/greenrobot/event/SubscriberMethodFinder;->filterSubscriberMethods(Ljava/util/List;Ljava/util/HashMap;Ljava/lang/StringBuilder;[Ljava/lang/reflect/Method;)V

    .line 85
    goto :goto_66

    .line 87
    .end local v9    # "th":Ljava/lang/Throwable;
    .end local v10    # "methods":[Ljava/lang/reflect/Method;
    :goto_61
    invoke-virtual {v5}, Ljava/lang/Class;->getSuperclass()Ljava/lang/Class;

    move-result-object v5

    .line 88
    .end local v8    # "name":Ljava/lang/String;
    goto :goto_28

    .line 89
    :cond_66
    :goto_66
    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_91

    .line 90
    new-instance v0, Lde/greenrobot/event/EventBusException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Subscriber "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " has no public methods called "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "onEvent"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lde/greenrobot/event/EventBusException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 93
    :cond_91
    sget-object v8, Lde/greenrobot/event/SubscriberMethodFinder;->methodCache:Ljava/util/Map;

    monitor-enter v8

    .line 94
    :try_start_94
    sget-object v0, Lde/greenrobot/event/SubscriberMethodFinder;->methodCache:Ljava/util/Map;

    invoke-interface {v0, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_99
    .catchall {:try_start_94 .. :try_end_99} :catchall_9b

    .line 95
    monitor-exit v8

    goto :goto_9e

    :catchall_9b
    move-exception v11

    monitor-exit v8

    throw v11

    .line 96
    :goto_9e
    return-object v4
.end method

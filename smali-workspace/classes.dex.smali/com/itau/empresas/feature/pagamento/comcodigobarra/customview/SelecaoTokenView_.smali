.class public final Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView_;
.super Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView;
.source "SelecaoTokenView_.java"

# interfaces
.implements Lorg/androidannotations/api/view/HasViews;
.implements Lorg/androidannotations/api/view/OnViewChangedListener;


# instance fields
.field private alreadyInflated_:Z

.field private final onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .param p1, "context"    # Landroid/content/Context;

    .line 44
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView;-><init>(Landroid/content/Context;)V

    .line 40
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView_;->alreadyInflated_:Z

    .line 41
    new-instance v0, Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-direct {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 45
    invoke-direct {p0}, Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView_;->init_()V

    .line 46
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .line 49
    invoke-direct {p0, p1, p2}, Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 40
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView_;->alreadyInflated_:Z

    .line 41
    new-instance v0, Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-direct {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 50
    invoke-direct {p0}, Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView_;->init_()V

    .line 51
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .line 54
    invoke-direct {p0, p1, p2, p3}, Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 40
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView_;->alreadyInflated_:Z

    .line 41
    new-instance v0, Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-direct {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 55
    invoke-direct {p0}, Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView_;->init_()V

    .line 56
    return-void
.end method

.method private init_()V
    .registers 5

    .line 81
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-static {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    move-result-object v2

    .line 82
    .local v2, "previousNotifier":Lorg/androidannotations/api/view/OnViewChangedNotifier;
    invoke-static {p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->registerOnViewChangedListener(Lorg/androidannotations/api/view/OnViewChangedListener;)V

    .line 83
    invoke-virtual {p0}, Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView_;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 84
    .local v3, "resources_":Landroid/content/res/Resources;
    const v0, 0x7f0706c1

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView_;->tokenInfo:Ljava/lang/String;

    .line 85
    const v0, 0x7f0706c3

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView_;->tokenInfoSms:Ljava/lang/String;

    .line 86
    const v0, 0x7f0706c2

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView_;->tokenInfoChaveiro:Ljava/lang/String;

    .line 87
    invoke-virtual {p0}, Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView_;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f04000a

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView_;->apareceDeBaixo:Landroid/view/animation/Animation;

    .line 88
    invoke-static {}, Lcom/itau/empresas/CustomApplication_;->getInstance()Lcom/itau/empresas/CustomApplication;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView_;->app:Lcom/itau/empresas/CustomApplication;

    .line 90
    invoke-static {v2}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 91
    return-void
.end method


# virtual methods
.method public onFinishInflate()V
    .registers 3

    .line 72
    iget-boolean v0, p0, Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView_;->alreadyInflated_:Z

    if-nez v0, :cond_16

    .line 73
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView_;->alreadyInflated_:Z

    .line 74
    invoke-virtual {p0}, Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView_;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f030183

    invoke-static {v0, v1, p0}, Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView_;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 75
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 77
    :cond_16
    invoke-super {p0}, Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView;->onFinishInflate()V

    .line 78
    return-void
.end method

.method public onViewChanged(Lorg/androidannotations/api/view/HasViews;)V
    .registers 5
    .param p1, "hasViews"    # Lorg/androidannotations/api/view/HasViews;

    .line 107
    const v0, 0x7f0e0639

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView_;->modalPrincipal:Landroid/view/ViewGroup;

    .line 108
    const v0, 0x7f0e0687

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView_;->containerFundo:Landroid/widget/RelativeLayout;

    .line 109
    const v0, 0x7f0e068c

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView_;->selecaoTokenSms:Landroid/widget/TextView;

    .line 110
    const v0, 0x7f0e068b

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView_;->selecaoTokenChaveiro:Landroid/widget/TextView;

    .line 111
    const v0, 0x7f0e068d

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView_;->selecaoTokenAplicativo:Landroid/widget/TextView;

    .line 112
    const v0, 0x7f0e0689

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView_;->infoEscolhaDispositivoToken:Landroid/widget/TextView;

    .line 113
    const v0, 0x7f0e068f

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView_;->campoToken:Landroid/widget/EditText;

    .line 114
    const v0, 0x7f0e0690

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView_;->botaoTokenContinuar:Landroid/widget/Button;

    .line 115
    const v0, 0x7f0e068e

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView_;->iconeVoltar:Landroid/widget/ImageView;

    .line 116
    const v0, 0x7f0e021e

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 118
    .local v2, "view_botao_fechar":Landroid/view/View;
    if-eqz v2, :cond_74

    .line 119
    new-instance v0, Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView_$1;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView_$1;-><init>(Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView_;)V

    invoke-virtual {v2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 128
    :cond_74
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView_;->iconeVoltar:Landroid/widget/ImageView;

    if-eqz v0, :cond_82

    .line 129
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView_;->iconeVoltar:Landroid/widget/ImageView;

    new-instance v1, Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView_$2;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView_$2;-><init>(Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView_;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 138
    :cond_82
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView_;->selecaoTokenChaveiro:Landroid/widget/TextView;

    if-eqz v0, :cond_90

    .line 139
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView_;->selecaoTokenChaveiro:Landroid/widget/TextView;

    new-instance v1, Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView_$3;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView_$3;-><init>(Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView_;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 148
    :cond_90
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView_;->selecaoTokenSms:Landroid/widget/TextView;

    if-eqz v0, :cond_9e

    .line 149
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView_;->selecaoTokenSms:Landroid/widget/TextView;

    new-instance v1, Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView_$4;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView_$4;-><init>(Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView_;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 158
    :cond_9e
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView_;->selecaoTokenAplicativo:Landroid/widget/TextView;

    if-eqz v0, :cond_ac

    .line 159
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView_;->selecaoTokenAplicativo:Landroid/widget/TextView;

    new-instance v1, Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView_$5;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView_$5;-><init>(Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView_;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 168
    :cond_ac
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView_;->botaoTokenContinuar:Landroid/widget/Button;

    if-eqz v0, :cond_ba

    .line 169
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView_;->botaoTokenContinuar:Landroid/widget/Button;

    new-instance v1, Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView_$6;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView_$6;-><init>(Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView_;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 178
    :cond_ba
    return-void
.end method

.class public final Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity_;
.super Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;
.source "TransferenciaRecebidasActivity_.java"

# interfaces
.implements Lorg/androidannotations/api/view/HasViews;
.implements Lorg/androidannotations/api/view/OnViewChangedListener;


# instance fields
.field private final onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;


# direct methods
.method public constructor <init>()V
    .registers 2

    .line 42
    invoke-direct {p0}, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;-><init>()V

    .line 46
    new-instance v0, Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-direct {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    return-void
.end method

.method static synthetic access$001(Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity_;)V
    .registers 1
    .param p0, "x0"    # Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity_;

    .line 42
    invoke-super {p0}, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;->mostraDialogoDeProgresso()V

    return-void
.end method

.method static synthetic access$101(Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity_;)V
    .registers 1
    .param p0, "x0"    # Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity_;

    .line 42
    invoke-super {p0}, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;->escondeDialogoDeProgresso()V

    return-void
.end method

.method private init_(Landroid/os/Bundle;)V
    .registers 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 58
    new-instance v0, Lcom/itau/empresas/Preferences_;

    invoke-direct {v0, p0}, Lcom/itau/empresas/Preferences_;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity_;->prefs:Lcom/itau/empresas/Preferences_;

    .line 59
    invoke-static {p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->registerOnViewChangedListener(Lorg/androidannotations/api/view/OnViewChangedListener;)V

    .line 60
    invoke-virtual {p0}, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity_;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 61
    .local v1, "resources_":Landroid/content/res/Resources;
    const v0, 0x7f0d0001

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity_;->dias:[Ljava/lang/String;

    .line 62
    invoke-static {}, Lcom/itau/empresas/CustomApplication_;->getInstance()Lcom/itau/empresas/CustomApplication;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity_;->application:Lcom/itau/empresas/CustomApplication;

    .line 63
    invoke-static {p0}, Lcom/itau/empresas/feature/logout/LogoutHelper_;->getInstance_(Landroid/content/Context;)Lcom/itau/empresas/feature/logout/LogoutHelper_;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity_;->logoutHelper:Lcom/itau/empresas/feature/logout/LogoutHelper;

    .line 64
    invoke-static {p0}, Lcom/itau/empresas/feature/transferencia/TransferenciaController_;->getInstance_(Landroid/content/Context;)Lcom/itau/empresas/feature/transferencia/TransferenciaController_;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity_;->controller:Lcom/itau/empresas/feature/transferencia/TransferenciaController;

    .line 66
    invoke-virtual {p0}, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity_;->afterInject()V

    .line 67
    return-void
.end method


# virtual methods
.method public escondeDialogoDeProgresso()V
    .registers 5

    .line 161
    const-string v0, ""

    new-instance v1, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity_$4;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity_$4;-><init>(Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity_;)V

    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3}, Lorg/androidannotations/api/UiThreadExecutor;->runTask(Ljava/lang/String;Ljava/lang/Runnable;J)V

    .line 169
    return-void
.end method

.method public mostraDialogoDeProgresso()V
    .registers 5
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "CommitTransaction"
        }
    .end annotation

    .line 149
    const-string v0, ""

    new-instance v1, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity_$3;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity_$3;-><init>(Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity_;)V

    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3}, Lorg/androidannotations/api/UiThreadExecutor;->runTask(Ljava/lang/String;Ljava/lang/Runnable;J)V

    .line 157
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 50
    iget-object v0, p0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-static {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    move-result-object v1

    .line 51
    .local v1, "previousNotifier":Lorg/androidannotations/api/view/OnViewChangedNotifier;
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity_;->init_(Landroid/os/Bundle;)V

    .line 52
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;->onCreate(Landroid/os/Bundle;)V

    .line 53
    invoke-static {v1}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 54
    const v0, 0x7f030069

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity_;->setContentView(I)V

    .line 55
    return-void
.end method

.method public onViewChanged(Lorg/androidannotations/api/view/HasViews;)V
    .registers 5
    .param p1, "hasViews"    # Lorg/androidannotations/api/view/HasViews;

    .line 101
    const v0, 0x7f0e0331

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity_;->root:Landroid/widget/LinearLayout;

    .line 102
    const v0, 0x7f0e0347

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity_;->listaTransferenciaRecebidas:Landroid/widget/ListView;

    .line 103
    const v0, 0x7f0e033f

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioGroup;

    iput-object v0, p0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity_;->rgTransferenciaRecebidasTipoFiltro:Landroid/widget/RadioGroup;

    .line 104
    const v0, 0x7f0e0341

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    iput-object v0, p0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity_;->rbTransferenciaRecebidasData:Landroid/widget/RadioButton;

    .line 105
    const v0, 0x7f0e0340

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    iput-object v0, p0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity_;->rbTransferenciaRecebidasDias:Landroid/widget/RadioButton;

    .line 106
    const v0, 0x7f0e0179

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lbr/com/itau/library/LinearValuePickerView;

    iput-object v0, p0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity_;->escolhePeriodo:Lbr/com/itau/library/LinearValuePickerView;

    .line 107
    const v0, 0x7f0e0338

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity_;->campoSelecionarInicioMes:Landroid/widget/TextView;

    .line 108
    const v0, 0x7f0e033c

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity_;->campoSelecionarFimMes:Landroid/widget/TextView;

    .line 109
    const v0, 0x7f0e0346

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity_;->textoSemTransferenciaRecebidas:Landroid/widget/TextView;

    .line 110
    const v0, 0x7f0e0334

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity_;->textoEscolhaData:Landroid/widget/TextView;

    .line 111
    const v0, 0x7f0e0335

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity_;->llTransferenciaRecebidasFiltroData:Landroid/widget/LinearLayout;

    .line 112
    const v0, 0x7f0e033e

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity_;->llTransferenciaRecebidasDataDias:Landroid/widget/LinearLayout;

    .line 113
    const v0, 0x7f0e0332

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity_;->llTransferenciaRecebidasFiltro:Landroid/widget/LinearLayout;

    .line 114
    const v0, 0x7f0e0333

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity_;->rlTransferenciasRecebidasFiltroDias:Landroid/widget/RelativeLayout;

    .line 115
    const v0, 0x7f0e0344

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity_;->headerListaTransferenciasRecebidas:Landroid/widget/RelativeLayout;

    .line 116
    const v0, 0x7f0e0343

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/TabLayout;

    iput-object v0, p0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity_;->tabTransferencias:Landroid/support/design/widget/TabLayout;

    .line 117
    const v0, 0x7f0e00b6

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/Toolbar;

    iput-object v0, p0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity_;->toolBar:Landroid/support/v7/widget/Toolbar;

    .line 118
    const v0, 0x7f0e0336

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 119
    .local v1, "view_rl_selecionar_inicio_mes":Landroid/view/View;
    const v0, 0x7f0e033a

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 121
    .local v2, "view_rl_selecionar_fim_mes":Landroid/view/View;
    if-eqz v1, :cond_d3

    .line 122
    new-instance v0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity_$1;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity_$1;-><init>(Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity_;)V

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 131
    :cond_d3
    if-eqz v2, :cond_dd

    .line 132
    new-instance v0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity_$2;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity_$2;-><init>(Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity_;)V

    invoke-virtual {v2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 141
    :cond_dd
    invoke-virtual {p0}, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity_;->carregaElementosDaTela()V

    .line 142
    return-void
.end method

.method public setContentView(I)V
    .registers 3
    .param p1, "layoutResID"    # I

    .line 71
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;->setContentView(I)V

    .line 72
    iget-object v0, p0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 73
    return-void
.end method

.method public setContentView(Landroid/view/View;)V
    .registers 3
    .param p1, "view"    # Landroid/view/View;

    .line 83
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;->setContentView(Landroid/view/View;)V

    .line 84
    iget-object v0, p0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 85
    return-void
.end method

.method public setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .registers 4
    .param p1, "view"    # Landroid/view/View;
    .param p2, "params"    # Landroid/view/ViewGroup$LayoutParams;

    .line 77
    invoke-super {p0, p1, p2}, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;->setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 78
    iget-object v0, p0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 79
    return-void
.end method

.class public Lcom/itau/empresas/api/model/BancoVO;
.super Ljava/lang/Object;
.source "BancoVO.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private codigoBanco:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "codigo_banco"
    .end annotation
.end field

.field private ispb:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "ispb"
    .end annotation
.end field

.field private nomeBanco:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "nome_banco"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.class public Lcom/daimajia/swipe/SwipeLayout;
.super Landroid/widget/FrameLayout;
.source "SwipeLayout.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/daimajia/swipe/SwipeLayout$4;,
        Lcom/daimajia/swipe/SwipeLayout$DoubleClickListener;,
        Lcom/daimajia/swipe/SwipeLayout$Status;,
        Lcom/daimajia/swipe/SwipeLayout$SwipeDetector;,
        Lcom/daimajia/swipe/SwipeLayout$OnLayout;,
        Lcom/daimajia/swipe/SwipeLayout$OnRevealListener;,
        Lcom/daimajia/swipe/SwipeLayout$SwipeDenier;,
        Lcom/daimajia/swipe/SwipeLayout$SwipeListener;,
        Lcom/daimajia/swipe/SwipeLayout$ShowMode;,
        Lcom/daimajia/swipe/SwipeLayout$DragEdge;
    }
.end annotation


# static fields
.field private static final DefaultDragEdge:Lcom/daimajia/swipe/SwipeLayout$DragEdge;


# instance fields
.field clickListener:Landroid/view/View$OnClickListener;

.field private gestureDetector:Landroid/view/GestureDetector;

.field private hitSurfaceRect:Landroid/graphics/Rect;

.field longClickListener:Landroid/view/View$OnLongClickListener;

.field private mClickToClose:Z

.field private mCurrentDragEdge:Lcom/daimajia/swipe/SwipeLayout$DragEdge;

.field private mDoubleClickListener:Lcom/daimajia/swipe/SwipeLayout$DoubleClickListener;

.field private mDragDistance:I

.field private mDragEdges:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap<Lcom/daimajia/swipe/SwipeLayout$DragEdge;Landroid/view/View;>;"
        }
    .end annotation
.end field

.field private mDragHelper:Landroid/support/v4/widget/ViewDragHelper;

.field private mDragHelperCallback:Landroid/support/v4/widget/ViewDragHelper$Callback;

.field private mEdgeSwipesOffset:[F

.field private mEventCounter:I

.field private mIsBeingDragged:Z

.field private mOnLayoutListeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Lcom/daimajia/swipe/SwipeLayout$OnLayout;>;"
        }
    .end annotation
.end field

.field private mRevealListeners:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<Landroid/view/View;Ljava/util/ArrayList<Lcom/daimajia/swipe/SwipeLayout$OnRevealListener;>;>;"
        }
    .end annotation
.end field

.field private mShowEntirely:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<Landroid/view/View;Ljava/lang/Boolean;>;"
        }
    .end annotation
.end field

.field private mShowMode:Lcom/daimajia/swipe/SwipeLayout$ShowMode;

.field private mSwipeDeniers:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Lcom/daimajia/swipe/SwipeLayout$SwipeDenier;>;"
        }
    .end annotation
.end field

.field private mSwipeEnabled:Z

.field private mSwipeListeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Lcom/daimajia/swipe/SwipeLayout$SwipeListener;>;"
        }
    .end annotation
.end field

.field private mSwipesEnabled:[Z

.field private mTouchSlop:I

.field private sX:F

.field private sY:F


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .line 38
    sget-object v0, Lcom/daimajia/swipe/SwipeLayout$DragEdge;->Right:Lcom/daimajia/swipe/SwipeLayout$DragEdge;

    sput-object v0, Lcom/daimajia/swipe/SwipeLayout;->DefaultDragEdge:Lcom/daimajia/swipe/SwipeLayout$DragEdge;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .param p1, "context"    # Landroid/content/Context;

    .line 75
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/daimajia/swipe/SwipeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 76
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .line 79
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/daimajia/swipe/SwipeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 80
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 11
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .line 83
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 42
    sget-object v0, Lcom/daimajia/swipe/SwipeLayout;->DefaultDragEdge:Lcom/daimajia/swipe/SwipeLayout$DragEdge;

    iput-object v0, p0, Lcom/daimajia/swipe/SwipeLayout;->mCurrentDragEdge:Lcom/daimajia/swipe/SwipeLayout$DragEdge;

    .line 45
    const/4 v0, 0x0

    iput v0, p0, Lcom/daimajia/swipe/SwipeLayout;->mDragDistance:I

    .line 46
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/daimajia/swipe/SwipeLayout;->mDragEdges:Ljava/util/LinkedHashMap;

    .line 49
    const/4 v0, 0x4

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/daimajia/swipe/SwipeLayout;->mEdgeSwipesOffset:[F

    .line 51
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/daimajia/swipe/SwipeLayout;->mSwipeListeners:Ljava/util/List;

    .line 52
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/daimajia/swipe/SwipeLayout;->mSwipeDeniers:Ljava/util/List;

    .line 53
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/daimajia/swipe/SwipeLayout;->mRevealListeners:Ljava/util/Map;

    .line 54
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/daimajia/swipe/SwipeLayout;->mShowEntirely:Ljava/util/Map;

    .line 58
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/daimajia/swipe/SwipeLayout;->mSwipeEnabled:Z

    .line 59
    const/4 v0, 0x4

    new-array v0, v0, [Z

    fill-array-data v0, :array_11e

    iput-object v0, p0, Lcom/daimajia/swipe/SwipeLayout;->mSwipesEnabled:[Z

    .line 60
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/daimajia/swipe/SwipeLayout;->mClickToClose:Z

    .line 214
    new-instance v0, Lcom/daimajia/swipe/SwipeLayout$1;

    invoke-direct {v0, p0}, Lcom/daimajia/swipe/SwipeLayout$1;-><init>(Lcom/daimajia/swipe/SwipeLayout;)V

    iput-object v0, p0, Lcom/daimajia/swipe/SwipeLayout;->mDragHelperCallback:Landroid/support/v4/widget/ViewDragHelper$Callback;

    .line 482
    const/4 v0, 0x0

    iput v0, p0, Lcom/daimajia/swipe/SwipeLayout;->mEventCounter:I

    .line 881
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lcom/daimajia/swipe/SwipeLayout;->sX:F

    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lcom/daimajia/swipe/SwipeLayout;->sY:F

    .line 1073
    new-instance v0, Landroid/view/GestureDetector;

    invoke-virtual {p0}, Lcom/daimajia/swipe/SwipeLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, Lcom/daimajia/swipe/SwipeLayout$SwipeDetector;

    invoke-direct {v2, p0}, Lcom/daimajia/swipe/SwipeLayout$SwipeDetector;-><init>(Lcom/daimajia/swipe/SwipeLayout;)V

    invoke-direct {v0, v1, v2}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lcom/daimajia/swipe/SwipeLayout;->gestureDetector:Landroid/view/GestureDetector;

    .line 84
    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout;->mDragHelperCallback:Landroid/support/v4/widget/ViewDragHelper$Callback;

    invoke-static {p0, v0}, Landroid/support/v4/widget/ViewDragHelper;->create(Landroid/view/ViewGroup;Landroid/support/v4/widget/ViewDragHelper$Callback;)Landroid/support/v4/widget/ViewDragHelper;

    move-result-object v0

    iput-object v0, p0, Lcom/daimajia/swipe/SwipeLayout;->mDragHelper:Landroid/support/v4/widget/ViewDragHelper;

    .line 85
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v0

    iput v0, p0, Lcom/daimajia/swipe/SwipeLayout;->mTouchSlop:I

    .line 87
    sget-object v0, Lcom/daimajia/swipe/R$styleable;->SwipeLayout:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v4

    .line 88
    .local v4, "a":Landroid/content/res/TypedArray;
    sget v0, Lcom/daimajia/swipe/R$styleable;->SwipeLayout_drag_edge:I

    const/4 v1, 0x2

    invoke-virtual {v4, v0, v1}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v5

    .line 89
    .local v5, "dragEdgeChoices":I
    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout;->mEdgeSwipesOffset:[F

    sget-object v1, Lcom/daimajia/swipe/SwipeLayout$DragEdge;->Left:Lcom/daimajia/swipe/SwipeLayout$DragEdge;

    invoke-virtual {v1}, Lcom/daimajia/swipe/SwipeLayout$DragEdge;->ordinal()I

    move-result v1

    sget v2, Lcom/daimajia/swipe/R$styleable;->SwipeLayout_leftEdgeSwipeOffset:I

    const/4 v3, 0x0

    invoke-virtual {v4, v2, v3}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v2

    aput v2, v0, v1

    .line 90
    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout;->mEdgeSwipesOffset:[F

    sget-object v1, Lcom/daimajia/swipe/SwipeLayout$DragEdge;->Right:Lcom/daimajia/swipe/SwipeLayout$DragEdge;

    invoke-virtual {v1}, Lcom/daimajia/swipe/SwipeLayout$DragEdge;->ordinal()I

    move-result v1

    sget v2, Lcom/daimajia/swipe/R$styleable;->SwipeLayout_rightEdgeSwipeOffset:I

    const/4 v3, 0x0

    invoke-virtual {v4, v2, v3}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v2

    aput v2, v0, v1

    .line 91
    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout;->mEdgeSwipesOffset:[F

    sget-object v1, Lcom/daimajia/swipe/SwipeLayout$DragEdge;->Top:Lcom/daimajia/swipe/SwipeLayout$DragEdge;

    invoke-virtual {v1}, Lcom/daimajia/swipe/SwipeLayout$DragEdge;->ordinal()I

    move-result v1

    sget v2, Lcom/daimajia/swipe/R$styleable;->SwipeLayout_topEdgeSwipeOffset:I

    const/4 v3, 0x0

    invoke-virtual {v4, v2, v3}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v2

    aput v2, v0, v1

    .line 92
    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout;->mEdgeSwipesOffset:[F

    sget-object v1, Lcom/daimajia/swipe/SwipeLayout$DragEdge;->Bottom:Lcom/daimajia/swipe/SwipeLayout$DragEdge;

    invoke-virtual {v1}, Lcom/daimajia/swipe/SwipeLayout$DragEdge;->ordinal()I

    move-result v1

    sget v2, Lcom/daimajia/swipe/R$styleable;->SwipeLayout_bottomEdgeSwipeOffset:I

    const/4 v3, 0x0

    invoke-virtual {v4, v2, v3}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v2

    aput v2, v0, v1

    .line 93
    sget v0, Lcom/daimajia/swipe/R$styleable;->SwipeLayout_clickToClose:I

    iget-boolean v1, p0, Lcom/daimajia/swipe/SwipeLayout;->mClickToClose:Z

    invoke-virtual {v4, v0, v1}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/daimajia/swipe/SwipeLayout;->setClickToClose(Z)V

    .line 95
    and-int/lit8 v0, v5, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_dd

    .line 96
    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout;->mDragEdges:Ljava/util/LinkedHashMap;

    sget-object v1, Lcom/daimajia/swipe/SwipeLayout$DragEdge;->Left:Lcom/daimajia/swipe/SwipeLayout$DragEdge;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 98
    :cond_dd
    and-int/lit8 v0, v5, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_ea

    .line 99
    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout;->mDragEdges:Ljava/util/LinkedHashMap;

    sget-object v1, Lcom/daimajia/swipe/SwipeLayout$DragEdge;->Top:Lcom/daimajia/swipe/SwipeLayout$DragEdge;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 101
    :cond_ea
    and-int/lit8 v0, v5, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_f7

    .line 102
    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout;->mDragEdges:Ljava/util/LinkedHashMap;

    sget-object v1, Lcom/daimajia/swipe/SwipeLayout$DragEdge;->Right:Lcom/daimajia/swipe/SwipeLayout$DragEdge;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 104
    :cond_f7
    and-int/lit8 v0, v5, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_105

    .line 105
    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout;->mDragEdges:Ljava/util/LinkedHashMap;

    sget-object v1, Lcom/daimajia/swipe/SwipeLayout$DragEdge;->Bottom:Lcom/daimajia/swipe/SwipeLayout$DragEdge;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 107
    :cond_105
    sget v0, Lcom/daimajia/swipe/R$styleable;->SwipeLayout_show_mode:I

    sget-object v1, Lcom/daimajia/swipe/SwipeLayout$ShowMode;->PullOut:Lcom/daimajia/swipe/SwipeLayout$ShowMode;

    invoke-virtual {v1}, Lcom/daimajia/swipe/SwipeLayout$ShowMode;->ordinal()I

    move-result v1

    invoke-virtual {v4, v0, v1}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v6

    .line 108
    .local v6, "ordinal":I
    invoke-static {}, Lcom/daimajia/swipe/SwipeLayout$ShowMode;->values()[Lcom/daimajia/swipe/SwipeLayout$ShowMode;

    move-result-object v0

    aget-object v0, v0, v6

    iput-object v0, p0, Lcom/daimajia/swipe/SwipeLayout;->mShowMode:Lcom/daimajia/swipe/SwipeLayout$ShowMode;

    .line 109
    invoke-virtual {v4}, Landroid/content/res/TypedArray;->recycle()V

    .line 111
    return-void

    nop

    :array_11e
    .array-data 1
        0x1t
        0x1t
        0x1t
        0x1t
    .end array-data
.end method

.method static synthetic access$000(Lcom/daimajia/swipe/SwipeLayout;)Lcom/daimajia/swipe/SwipeLayout$DragEdge;
    .registers 2
    .param p0, "x0"    # Lcom/daimajia/swipe/SwipeLayout;

    .line 31
    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout;->mCurrentDragEdge:Lcom/daimajia/swipe/SwipeLayout$DragEdge;

    return-object v0
.end method

.method static synthetic access$100(Lcom/daimajia/swipe/SwipeLayout;)I
    .registers 2
    .param p0, "x0"    # Lcom/daimajia/swipe/SwipeLayout;

    .line 31
    iget v0, p0, Lcom/daimajia/swipe/SwipeLayout;->mDragDistance:I

    return v0
.end method

.method static synthetic access$200(Lcom/daimajia/swipe/SwipeLayout;)Lcom/daimajia/swipe/SwipeLayout$ShowMode;
    .registers 2
    .param p0, "x0"    # Lcom/daimajia/swipe/SwipeLayout;

    .line 31
    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout;->mShowMode:Lcom/daimajia/swipe/SwipeLayout$ShowMode;

    return-object v0
.end method

.method static synthetic access$300(Lcom/daimajia/swipe/SwipeLayout;)Ljava/util/List;
    .registers 2
    .param p0, "x0"    # Lcom/daimajia/swipe/SwipeLayout;

    .line 31
    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout;->mSwipeListeners:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$400(Lcom/daimajia/swipe/SwipeLayout;Lcom/daimajia/swipe/SwipeLayout$DragEdge;)Landroid/graphics/Rect;
    .registers 3
    .param p0, "x0"    # Lcom/daimajia/swipe/SwipeLayout;
    .param p1, "x1"    # Lcom/daimajia/swipe/SwipeLayout$DragEdge;

    .line 31
    invoke-direct {p0, p1}, Lcom/daimajia/swipe/SwipeLayout;->computeBottomLayDown(Lcom/daimajia/swipe/SwipeLayout$DragEdge;)Landroid/graphics/Rect;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$500(Lcom/daimajia/swipe/SwipeLayout;)V
    .registers 1
    .param p0, "x0"    # Lcom/daimajia/swipe/SwipeLayout;

    .line 31
    invoke-direct {p0}, Lcom/daimajia/swipe/SwipeLayout;->performAdapterViewItemClick()V

    return-void
.end method

.method static synthetic access$600(Lcom/daimajia/swipe/SwipeLayout;)Z
    .registers 2
    .param p0, "x0"    # Lcom/daimajia/swipe/SwipeLayout;

    .line 31
    invoke-direct {p0}, Lcom/daimajia/swipe/SwipeLayout;->performAdapterViewItemLongClick()Z

    move-result v0

    return v0
.end method

.method static synthetic access$700(Lcom/daimajia/swipe/SwipeLayout;)Z
    .registers 2
    .param p0, "x0"    # Lcom/daimajia/swipe/SwipeLayout;

    .line 31
    iget-boolean v0, p0, Lcom/daimajia/swipe/SwipeLayout;->mClickToClose:Z

    return v0
.end method

.method static synthetic access$800(Lcom/daimajia/swipe/SwipeLayout;Landroid/view/MotionEvent;)Z
    .registers 3
    .param p0, "x0"    # Lcom/daimajia/swipe/SwipeLayout;
    .param p1, "x1"    # Landroid/view/MotionEvent;

    .line 31
    invoke-direct {p0, p1}, Lcom/daimajia/swipe/SwipeLayout;->isTouchOnSurface(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$900(Lcom/daimajia/swipe/SwipeLayout;)Lcom/daimajia/swipe/SwipeLayout$DoubleClickListener;
    .registers 2
    .param p0, "x0"    # Lcom/daimajia/swipe/SwipeLayout;

    .line 31
    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout;->mDoubleClickListener:Lcom/daimajia/swipe/SwipeLayout$DoubleClickListener;

    return-object v0
.end method

.method private checkCanDrag(Landroid/view/MotionEvent;)V
    .registers 10
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .line 758
    iget-boolean v0, p0, Lcom/daimajia/swipe/SwipeLayout;->mIsBeingDragged:Z

    if-eqz v0, :cond_5

    return-void

    .line 759
    :cond_5
    invoke-virtual {p0}, Lcom/daimajia/swipe/SwipeLayout;->getOpenStatus()Lcom/daimajia/swipe/SwipeLayout$Status;

    move-result-object v0

    sget-object v1, Lcom/daimajia/swipe/SwipeLayout$Status;->Middle:Lcom/daimajia/swipe/SwipeLayout$Status;

    if-ne v0, v1, :cond_11

    .line 760
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/daimajia/swipe/SwipeLayout;->mIsBeingDragged:Z

    .line 761
    return-void

    .line 763
    :cond_11
    invoke-virtual {p0}, Lcom/daimajia/swipe/SwipeLayout;->getOpenStatus()Lcom/daimajia/swipe/SwipeLayout$Status;

    move-result-object v2

    .line 764
    .local v2, "status":Lcom/daimajia/swipe/SwipeLayout$Status;
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v0

    iget v1, p0, Lcom/daimajia/swipe/SwipeLayout;->sX:F

    sub-float v3, v0, v1

    .line 765
    .local v3, "distanceX":F
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v0

    iget v1, p0, Lcom/daimajia/swipe/SwipeLayout;->sY:F

    sub-float v4, v0, v1

    .line 766
    .local v4, "distanceY":F
    div-float v0, v4, v3

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v5

    .line 767
    .local v5, "angle":F
    float-to-double v0, v5

    invoke-static {v0, v1}, Ljava/lang/Math;->atan(D)D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v0

    double-to-float v5, v0

    .line 768
    invoke-virtual {p0}, Lcom/daimajia/swipe/SwipeLayout;->getOpenStatus()Lcom/daimajia/swipe/SwipeLayout$Status;

    move-result-object v0

    sget-object v1, Lcom/daimajia/swipe/SwipeLayout$Status;->Close:Lcom/daimajia/swipe/SwipeLayout$Status;

    if-ne v0, v1, :cond_80

    .line 770
    const/high16 v0, 0x42340000    # 45.0f

    cmpg-float v0, v5, v0

    if-gez v0, :cond_60

    .line 771
    const/4 v0, 0x0

    cmpl-float v0, v3, v0

    if-lez v0, :cond_51

    invoke-virtual {p0}, Lcom/daimajia/swipe/SwipeLayout;->isLeftSwipeEnabled()Z

    move-result v0

    if-eqz v0, :cond_51

    .line 772
    sget-object v6, Lcom/daimajia/swipe/SwipeLayout$DragEdge;->Left:Lcom/daimajia/swipe/SwipeLayout$DragEdge;

    .local v6, "dragEdge":Lcom/daimajia/swipe/SwipeLayout$DragEdge;
    goto :goto_7d

    .line 773
    .end local v6    # "dragEdge":Lcom/daimajia/swipe/SwipeLayout$DragEdge;
    :cond_51
    const/4 v0, 0x0

    cmpg-float v0, v3, v0

    if-gez v0, :cond_5f

    invoke-virtual {p0}, Lcom/daimajia/swipe/SwipeLayout;->isRightSwipeEnabled()Z

    move-result v0

    if-eqz v0, :cond_5f

    .line 774
    sget-object v6, Lcom/daimajia/swipe/SwipeLayout$DragEdge;->Right:Lcom/daimajia/swipe/SwipeLayout$DragEdge;

    .local v6, "dragEdge":Lcom/daimajia/swipe/SwipeLayout$DragEdge;
    goto :goto_7d

    .line 775
    .end local v6    # "dragEdge":Lcom/daimajia/swipe/SwipeLayout$DragEdge;
    :cond_5f
    return-void

    .line 778
    :cond_60
    const/4 v0, 0x0

    cmpl-float v0, v4, v0

    if-lez v0, :cond_6e

    invoke-virtual {p0}, Lcom/daimajia/swipe/SwipeLayout;->isTopSwipeEnabled()Z

    move-result v0

    if-eqz v0, :cond_6e

    .line 779
    sget-object v6, Lcom/daimajia/swipe/SwipeLayout$DragEdge;->Top:Lcom/daimajia/swipe/SwipeLayout$DragEdge;

    .local v6, "dragEdge":Lcom/daimajia/swipe/SwipeLayout$DragEdge;
    goto :goto_7d

    .line 780
    .end local v6    # "dragEdge":Lcom/daimajia/swipe/SwipeLayout$DragEdge;
    :cond_6e
    const/4 v0, 0x0

    cmpg-float v0, v4, v0

    if-gez v0, :cond_7c

    invoke-virtual {p0}, Lcom/daimajia/swipe/SwipeLayout;->isBottomSwipeEnabled()Z

    move-result v0

    if-eqz v0, :cond_7c

    .line 781
    sget-object v6, Lcom/daimajia/swipe/SwipeLayout$DragEdge;->Bottom:Lcom/daimajia/swipe/SwipeLayout$DragEdge;

    .local v6, "dragEdge":Lcom/daimajia/swipe/SwipeLayout$DragEdge;
    goto :goto_7d

    .line 782
    .end local v6    # "dragEdge":Lcom/daimajia/swipe/SwipeLayout$DragEdge;
    :cond_7c
    return-void

    .line 784
    .local v6, "dragEdge":Lcom/daimajia/swipe/SwipeLayout$DragEdge;
    :goto_7d
    invoke-direct {p0, v6}, Lcom/daimajia/swipe/SwipeLayout;->setCurrentDragEdge(Lcom/daimajia/swipe/SwipeLayout$DragEdge;)V

    .line 787
    .end local v6    # "dragEdge":Lcom/daimajia/swipe/SwipeLayout$DragEdge;
    :cond_80
    const/4 v6, 0x0

    .line 788
    .local v6, "doNothing":Z
    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout;->mCurrentDragEdge:Lcom/daimajia/swipe/SwipeLayout$DragEdge;

    sget-object v1, Lcom/daimajia/swipe/SwipeLayout$DragEdge;->Right:Lcom/daimajia/swipe/SwipeLayout$DragEdge;

    if-ne v0, v1, :cond_b3

    .line 789
    sget-object v0, Lcom/daimajia/swipe/SwipeLayout$Status;->Open:Lcom/daimajia/swipe/SwipeLayout$Status;

    if-ne v2, v0, :cond_92

    iget v0, p0, Lcom/daimajia/swipe/SwipeLayout;->mTouchSlop:I

    int-to-float v0, v0

    cmpl-float v0, v3, v0

    if-gtz v0, :cond_9e

    :cond_92
    sget-object v0, Lcom/daimajia/swipe/SwipeLayout$Status;->Close:Lcom/daimajia/swipe/SwipeLayout$Status;

    if-ne v2, v0, :cond_a0

    iget v0, p0, Lcom/daimajia/swipe/SwipeLayout;->mTouchSlop:I

    neg-int v0, v0

    int-to-float v0, v0

    cmpg-float v0, v3, v0

    if-gez v0, :cond_a0

    :cond_9e
    const/4 v7, 0x1

    goto :goto_a1

    :cond_a0
    const/4 v7, 0x0

    .line 791
    .local v7, "suitable":Z
    :goto_a1
    if-nez v7, :cond_a7

    sget-object v0, Lcom/daimajia/swipe/SwipeLayout$Status;->Middle:Lcom/daimajia/swipe/SwipeLayout$Status;

    if-ne v2, v0, :cond_a9

    :cond_a7
    const/4 v7, 0x1

    goto :goto_aa

    :cond_a9
    const/4 v7, 0x0

    .line 793
    :goto_aa
    const/high16 v0, 0x41f00000    # 30.0f

    cmpl-float v0, v5, v0

    if-gtz v0, :cond_b2

    if-nez v7, :cond_b3

    .line 794
    :cond_b2
    const/4 v6, 0x1

    .line 798
    .end local v7    # "suitable":Z
    :cond_b3
    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout;->mCurrentDragEdge:Lcom/daimajia/swipe/SwipeLayout$DragEdge;

    sget-object v1, Lcom/daimajia/swipe/SwipeLayout$DragEdge;->Left:Lcom/daimajia/swipe/SwipeLayout$DragEdge;

    if-ne v0, v1, :cond_e5

    .line 799
    sget-object v0, Lcom/daimajia/swipe/SwipeLayout$Status;->Open:Lcom/daimajia/swipe/SwipeLayout$Status;

    if-ne v2, v0, :cond_c5

    iget v0, p0, Lcom/daimajia/swipe/SwipeLayout;->mTouchSlop:I

    neg-int v0, v0

    int-to-float v0, v0

    cmpg-float v0, v3, v0

    if-ltz v0, :cond_d0

    :cond_c5
    sget-object v0, Lcom/daimajia/swipe/SwipeLayout$Status;->Close:Lcom/daimajia/swipe/SwipeLayout$Status;

    if-ne v2, v0, :cond_d2

    iget v0, p0, Lcom/daimajia/swipe/SwipeLayout;->mTouchSlop:I

    int-to-float v0, v0

    cmpl-float v0, v3, v0

    if-lez v0, :cond_d2

    :cond_d0
    const/4 v7, 0x1

    goto :goto_d3

    :cond_d2
    const/4 v7, 0x0

    .line 801
    .local v7, "suitable":Z
    :goto_d3
    if-nez v7, :cond_d9

    sget-object v0, Lcom/daimajia/swipe/SwipeLayout$Status;->Middle:Lcom/daimajia/swipe/SwipeLayout$Status;

    if-ne v2, v0, :cond_db

    :cond_d9
    const/4 v7, 0x1

    goto :goto_dc

    :cond_db
    const/4 v7, 0x0

    .line 803
    :goto_dc
    const/high16 v0, 0x41f00000    # 30.0f

    cmpl-float v0, v5, v0

    if-gtz v0, :cond_e4

    if-nez v7, :cond_e5

    .line 804
    :cond_e4
    const/4 v6, 0x1

    .line 808
    .end local v7    # "suitable":Z
    :cond_e5
    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout;->mCurrentDragEdge:Lcom/daimajia/swipe/SwipeLayout$DragEdge;

    sget-object v1, Lcom/daimajia/swipe/SwipeLayout$DragEdge;->Top:Lcom/daimajia/swipe/SwipeLayout$DragEdge;

    if-ne v0, v1, :cond_117

    .line 809
    sget-object v0, Lcom/daimajia/swipe/SwipeLayout$Status;->Open:Lcom/daimajia/swipe/SwipeLayout$Status;

    if-ne v2, v0, :cond_f7

    iget v0, p0, Lcom/daimajia/swipe/SwipeLayout;->mTouchSlop:I

    neg-int v0, v0

    int-to-float v0, v0

    cmpg-float v0, v4, v0

    if-ltz v0, :cond_102

    :cond_f7
    sget-object v0, Lcom/daimajia/swipe/SwipeLayout$Status;->Close:Lcom/daimajia/swipe/SwipeLayout$Status;

    if-ne v2, v0, :cond_104

    iget v0, p0, Lcom/daimajia/swipe/SwipeLayout;->mTouchSlop:I

    int-to-float v0, v0

    cmpl-float v0, v4, v0

    if-lez v0, :cond_104

    :cond_102
    const/4 v7, 0x1

    goto :goto_105

    :cond_104
    const/4 v7, 0x0

    .line 811
    .local v7, "suitable":Z
    :goto_105
    if-nez v7, :cond_10b

    sget-object v0, Lcom/daimajia/swipe/SwipeLayout$Status;->Middle:Lcom/daimajia/swipe/SwipeLayout$Status;

    if-ne v2, v0, :cond_10d

    :cond_10b
    const/4 v7, 0x1

    goto :goto_10e

    :cond_10d
    const/4 v7, 0x0

    .line 813
    :goto_10e
    const/high16 v0, 0x42700000    # 60.0f

    cmpg-float v0, v5, v0

    if-ltz v0, :cond_116

    if-nez v7, :cond_117

    .line 814
    :cond_116
    const/4 v6, 0x1

    .line 818
    .end local v7    # "suitable":Z
    :cond_117
    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout;->mCurrentDragEdge:Lcom/daimajia/swipe/SwipeLayout$DragEdge;

    sget-object v1, Lcom/daimajia/swipe/SwipeLayout$DragEdge;->Bottom:Lcom/daimajia/swipe/SwipeLayout$DragEdge;

    if-ne v0, v1, :cond_149

    .line 819
    sget-object v0, Lcom/daimajia/swipe/SwipeLayout$Status;->Open:Lcom/daimajia/swipe/SwipeLayout$Status;

    if-ne v2, v0, :cond_128

    iget v0, p0, Lcom/daimajia/swipe/SwipeLayout;->mTouchSlop:I

    int-to-float v0, v0

    cmpl-float v0, v4, v0

    if-gtz v0, :cond_134

    :cond_128
    sget-object v0, Lcom/daimajia/swipe/SwipeLayout$Status;->Close:Lcom/daimajia/swipe/SwipeLayout$Status;

    if-ne v2, v0, :cond_136

    iget v0, p0, Lcom/daimajia/swipe/SwipeLayout;->mTouchSlop:I

    neg-int v0, v0

    int-to-float v0, v0

    cmpg-float v0, v4, v0

    if-gez v0, :cond_136

    :cond_134
    const/4 v7, 0x1

    goto :goto_137

    :cond_136
    const/4 v7, 0x0

    .line 821
    .local v7, "suitable":Z
    :goto_137
    if-nez v7, :cond_13d

    sget-object v0, Lcom/daimajia/swipe/SwipeLayout$Status;->Middle:Lcom/daimajia/swipe/SwipeLayout$Status;

    if-ne v2, v0, :cond_13f

    :cond_13d
    const/4 v7, 0x1

    goto :goto_140

    :cond_13f
    const/4 v7, 0x0

    .line 823
    :goto_140
    const/high16 v0, 0x42700000    # 60.0f

    cmpg-float v0, v5, v0

    if-ltz v0, :cond_148

    if-nez v7, :cond_149

    .line 824
    :cond_148
    const/4 v6, 0x1

    .line 827
    .end local v7    # "suitable":Z
    :cond_149
    if-nez v6, :cond_14d

    const/4 v0, 0x1

    goto :goto_14e

    :cond_14d
    const/4 v0, 0x0

    :goto_14e
    iput-boolean v0, p0, Lcom/daimajia/swipe/SwipeLayout;->mIsBeingDragged:Z

    .line 828
    return-void
.end method

.method private computeBottomLayDown(Lcom/daimajia/swipe/SwipeLayout$DragEdge;)Landroid/graphics/Rect;
    .registers 8
    .param p1, "dragEdge"    # Lcom/daimajia/swipe/SwipeLayout$DragEdge;

    .line 1402
    invoke-virtual {p0}, Lcom/daimajia/swipe/SwipeLayout;->getPaddingLeft()I

    move-result v2

    .local v2, "bl":I
    invoke-virtual {p0}, Lcom/daimajia/swipe/SwipeLayout;->getPaddingTop()I

    move-result v3

    .line 1404
    .local v3, "bt":I
    sget-object v0, Lcom/daimajia/swipe/SwipeLayout$DragEdge;->Right:Lcom/daimajia/swipe/SwipeLayout$DragEdge;

    if-ne p1, v0, :cond_15

    .line 1405
    invoke-virtual {p0}, Lcom/daimajia/swipe/SwipeLayout;->getMeasuredWidth()I

    move-result v0

    iget v1, p0, Lcom/daimajia/swipe/SwipeLayout;->mDragDistance:I

    sub-int v2, v0, v1

    goto :goto_21

    .line 1406
    :cond_15
    sget-object v0, Lcom/daimajia/swipe/SwipeLayout$DragEdge;->Bottom:Lcom/daimajia/swipe/SwipeLayout$DragEdge;

    if-ne p1, v0, :cond_21

    .line 1407
    invoke-virtual {p0}, Lcom/daimajia/swipe/SwipeLayout;->getMeasuredHeight()I

    move-result v0

    iget v1, p0, Lcom/daimajia/swipe/SwipeLayout;->mDragDistance:I

    sub-int v3, v0, v1

    .line 1409
    :cond_21
    :goto_21
    sget-object v0, Lcom/daimajia/swipe/SwipeLayout$DragEdge;->Left:Lcom/daimajia/swipe/SwipeLayout$DragEdge;

    if-eq p1, v0, :cond_29

    sget-object v0, Lcom/daimajia/swipe/SwipeLayout$DragEdge;->Right:Lcom/daimajia/swipe/SwipeLayout$DragEdge;

    if-ne p1, v0, :cond_34

    .line 1410
    :cond_29
    iget v0, p0, Lcom/daimajia/swipe/SwipeLayout;->mDragDistance:I

    add-int v4, v2, v0

    .line 1411
    .local v4, "br":I
    invoke-virtual {p0}, Lcom/daimajia/swipe/SwipeLayout;->getMeasuredHeight()I

    move-result v0

    add-int v5, v3, v0

    .local v5, "bb":I
    goto :goto_3e

    .line 1413
    .end local v4    # "br":I
    .end local v5    # "bb":I
    :cond_34
    invoke-virtual {p0}, Lcom/daimajia/swipe/SwipeLayout;->getMeasuredWidth()I

    move-result v0

    add-int v4, v2, v0

    .line 1414
    .local v4, "br":I
    iget v0, p0, Lcom/daimajia/swipe/SwipeLayout;->mDragDistance:I

    add-int v5, v3, v0

    .line 1416
    .local v5, "bb":I
    :goto_3e
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0, v2, v3, v4, v5}, Landroid/graphics/Rect;-><init>(IIII)V

    return-object v0
.end method

.method private computeBottomLayoutAreaViaSurface(Lcom/daimajia/swipe/SwipeLayout$ShowMode;Landroid/graphics/Rect;)Landroid/graphics/Rect;
    .registers 11
    .param p1, "mode"    # Lcom/daimajia/swipe/SwipeLayout$ShowMode;
    .param p2, "surfaceArea"    # Landroid/graphics/Rect;

    .line 1367
    move-object v2, p2

    .line 1368
    .local v2, "rect":Landroid/graphics/Rect;
    invoke-virtual {p0}, Lcom/daimajia/swipe/SwipeLayout;->getCurrentBottomView()Landroid/view/View;

    move-result-object v3

    .line 1370
    .local v3, "bottomView":Landroid/view/View;
    iget v4, v2, Landroid/graphics/Rect;->left:I

    .local v4, "bl":I
    iget v5, v2, Landroid/graphics/Rect;->top:I

    .local v5, "bt":I
    iget v6, v2, Landroid/graphics/Rect;->right:I

    .local v6, "br":I
    iget v7, v2, Landroid/graphics/Rect;->bottom:I

    .line 1371
    .local v7, "bb":I
    sget-object v0, Lcom/daimajia/swipe/SwipeLayout$ShowMode;->PullOut:Lcom/daimajia/swipe/SwipeLayout$ShowMode;

    if-ne p1, v0, :cond_5c

    .line 1372
    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout;->mCurrentDragEdge:Lcom/daimajia/swipe/SwipeLayout$DragEdge;

    sget-object v1, Lcom/daimajia/swipe/SwipeLayout$DragEdge;->Left:Lcom/daimajia/swipe/SwipeLayout$DragEdge;

    if-ne v0, v1, :cond_1e

    .line 1373
    iget v0, v2, Landroid/graphics/Rect;->left:I

    iget v1, p0, Lcom/daimajia/swipe/SwipeLayout;->mDragDistance:I

    sub-int v4, v0, v1

    goto :goto_36

    .line 1374
    :cond_1e
    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout;->mCurrentDragEdge:Lcom/daimajia/swipe/SwipeLayout$DragEdge;

    sget-object v1, Lcom/daimajia/swipe/SwipeLayout$DragEdge;->Right:Lcom/daimajia/swipe/SwipeLayout$DragEdge;

    if-ne v0, v1, :cond_27

    .line 1375
    iget v4, v2, Landroid/graphics/Rect;->right:I

    goto :goto_36

    .line 1376
    :cond_27
    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout;->mCurrentDragEdge:Lcom/daimajia/swipe/SwipeLayout$DragEdge;

    sget-object v1, Lcom/daimajia/swipe/SwipeLayout$DragEdge;->Top:Lcom/daimajia/swipe/SwipeLayout$DragEdge;

    if-ne v0, v1, :cond_34

    .line 1377
    iget v0, v2, Landroid/graphics/Rect;->top:I

    iget v1, p0, Lcom/daimajia/swipe/SwipeLayout;->mDragDistance:I

    sub-int v5, v0, v1

    goto :goto_36

    .line 1378
    :cond_34
    iget v5, v2, Landroid/graphics/Rect;->bottom:I

    .line 1380
    :goto_36
    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout;->mCurrentDragEdge:Lcom/daimajia/swipe/SwipeLayout$DragEdge;

    sget-object v1, Lcom/daimajia/swipe/SwipeLayout$DragEdge;->Left:Lcom/daimajia/swipe/SwipeLayout$DragEdge;

    if-eq v0, v1, :cond_42

    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout;->mCurrentDragEdge:Lcom/daimajia/swipe/SwipeLayout$DragEdge;

    sget-object v1, Lcom/daimajia/swipe/SwipeLayout$DragEdge;->Right:Lcom/daimajia/swipe/SwipeLayout$DragEdge;

    if-ne v0, v1, :cond_4f

    .line 1381
    :cond_42
    iget v7, v2, Landroid/graphics/Rect;->bottom:I

    .line 1382
    if-nez v3, :cond_48

    const/4 v0, 0x0

    goto :goto_4c

    :cond_48
    invoke-virtual {v3}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    :goto_4c
    add-int v6, v4, v0

    goto :goto_85

    .line 1384
    :cond_4f
    if-nez v3, :cond_53

    const/4 v0, 0x0

    goto :goto_57

    :cond_53
    invoke-virtual {v3}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    :goto_57
    add-int v7, v5, v0

    .line 1385
    iget v6, v2, Landroid/graphics/Rect;->right:I

    goto :goto_85

    .line 1387
    :cond_5c
    sget-object v0, Lcom/daimajia/swipe/SwipeLayout$ShowMode;->LayDown:Lcom/daimajia/swipe/SwipeLayout$ShowMode;

    if-ne p1, v0, :cond_85

    .line 1388
    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout;->mCurrentDragEdge:Lcom/daimajia/swipe/SwipeLayout$DragEdge;

    sget-object v1, Lcom/daimajia/swipe/SwipeLayout$DragEdge;->Left:Lcom/daimajia/swipe/SwipeLayout$DragEdge;

    if-ne v0, v1, :cond_6b

    .line 1389
    iget v0, p0, Lcom/daimajia/swipe/SwipeLayout;->mDragDistance:I

    add-int v6, v4, v0

    goto :goto_85

    .line 1390
    :cond_6b
    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout;->mCurrentDragEdge:Lcom/daimajia/swipe/SwipeLayout$DragEdge;

    sget-object v1, Lcom/daimajia/swipe/SwipeLayout$DragEdge;->Right:Lcom/daimajia/swipe/SwipeLayout$DragEdge;

    if-ne v0, v1, :cond_76

    .line 1391
    iget v0, p0, Lcom/daimajia/swipe/SwipeLayout;->mDragDistance:I

    sub-int v4, v6, v0

    goto :goto_85

    .line 1392
    :cond_76
    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout;->mCurrentDragEdge:Lcom/daimajia/swipe/SwipeLayout$DragEdge;

    sget-object v1, Lcom/daimajia/swipe/SwipeLayout$DragEdge;->Top:Lcom/daimajia/swipe/SwipeLayout$DragEdge;

    if-ne v0, v1, :cond_81

    .line 1393
    iget v0, p0, Lcom/daimajia/swipe/SwipeLayout;->mDragDistance:I

    add-int v7, v5, v0

    goto :goto_85

    .line 1394
    :cond_81
    iget v0, p0, Lcom/daimajia/swipe/SwipeLayout;->mDragDistance:I

    sub-int v5, v7, v0

    .line 1397
    :cond_85
    :goto_85
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0, v4, v5, v6, v7}, Landroid/graphics/Rect;-><init>(IIII)V

    return-object v0
.end method

.method private computeSurfaceLayoutArea(Z)Landroid/graphics/Rect;
    .registers 7
    .param p1, "open"    # Z

    .line 1353
    invoke-virtual {p0}, Lcom/daimajia/swipe/SwipeLayout;->getPaddingLeft()I

    move-result v3

    .local v3, "l":I
    invoke-virtual {p0}, Lcom/daimajia/swipe/SwipeLayout;->getPaddingTop()I

    move-result v4

    .line 1354
    .local v4, "t":I
    if-eqz p1, :cond_3f

    .line 1355
    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout;->mCurrentDragEdge:Lcom/daimajia/swipe/SwipeLayout$DragEdge;

    sget-object v1, Lcom/daimajia/swipe/SwipeLayout$DragEdge;->Left:Lcom/daimajia/swipe/SwipeLayout$DragEdge;

    if-ne v0, v1, :cond_19

    .line 1356
    invoke-virtual {p0}, Lcom/daimajia/swipe/SwipeLayout;->getPaddingLeft()I

    move-result v0

    iget v1, p0, Lcom/daimajia/swipe/SwipeLayout;->mDragDistance:I

    add-int v3, v0, v1

    goto :goto_3f

    .line 1357
    :cond_19
    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout;->mCurrentDragEdge:Lcom/daimajia/swipe/SwipeLayout$DragEdge;

    sget-object v1, Lcom/daimajia/swipe/SwipeLayout$DragEdge;->Right:Lcom/daimajia/swipe/SwipeLayout$DragEdge;

    if-ne v0, v1, :cond_28

    .line 1358
    invoke-virtual {p0}, Lcom/daimajia/swipe/SwipeLayout;->getPaddingLeft()I

    move-result v0

    iget v1, p0, Lcom/daimajia/swipe/SwipeLayout;->mDragDistance:I

    sub-int v3, v0, v1

    goto :goto_3f

    .line 1359
    :cond_28
    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout;->mCurrentDragEdge:Lcom/daimajia/swipe/SwipeLayout$DragEdge;

    sget-object v1, Lcom/daimajia/swipe/SwipeLayout$DragEdge;->Top:Lcom/daimajia/swipe/SwipeLayout$DragEdge;

    if-ne v0, v1, :cond_37

    .line 1360
    invoke-virtual {p0}, Lcom/daimajia/swipe/SwipeLayout;->getPaddingTop()I

    move-result v0

    iget v1, p0, Lcom/daimajia/swipe/SwipeLayout;->mDragDistance:I

    add-int v4, v0, v1

    goto :goto_3f

    .line 1361
    :cond_37
    invoke-virtual {p0}, Lcom/daimajia/swipe/SwipeLayout;->getPaddingTop()I

    move-result v0

    iget v1, p0, Lcom/daimajia/swipe/SwipeLayout;->mDragDistance:I

    sub-int v4, v0, v1

    .line 1363
    :cond_3f
    :goto_3f
    new-instance v0, Landroid/graphics/Rect;

    invoke-virtual {p0}, Lcom/daimajia/swipe/SwipeLayout;->getMeasuredWidth()I

    move-result v1

    add-int/2addr v1, v3

    invoke-virtual {p0}, Lcom/daimajia/swipe/SwipeLayout;->getMeasuredHeight()I

    move-result v2

    add-int/2addr v2, v4

    invoke-direct {v0, v3, v4, v1, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    return-object v0
.end method

.method private dp2px(F)I
    .registers 4
    .param p1, "dp"    # F

    .line 1428
    invoke-virtual {p0}, Lcom/daimajia/swipe/SwipeLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v0, p1

    const/high16 v1, 0x3f000000    # 0.5f

    add-float/2addr v0, v1

    float-to-int v0, v0

    return v0
.end method

.method private getAdapterView()Landroid/widget/AdapterView;
    .registers 3

    .line 978
    invoke-virtual {p0}, Lcom/daimajia/swipe/SwipeLayout;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    .line 979
    .local v1, "t":Landroid/view/ViewParent;
    instance-of v0, v1, Landroid/widget/AdapterView;

    if-eqz v0, :cond_c

    .line 980
    move-object v0, v1

    check-cast v0, Landroid/widget/AdapterView;

    return-object v0

    .line 982
    :cond_c
    const/4 v0, 0x0

    return-object v0
.end method

.method private getCurrentOffset()F
    .registers 3

    .line 1491
    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout;->mCurrentDragEdge:Lcom/daimajia/swipe/SwipeLayout$DragEdge;

    if-nez v0, :cond_6

    const/4 v0, 0x0

    return v0

    .line 1492
    :cond_6
    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout;->mEdgeSwipesOffset:[F

    iget-object v1, p0, Lcom/daimajia/swipe/SwipeLayout;->mCurrentDragEdge:Lcom/daimajia/swipe/SwipeLayout$DragEdge;

    invoke-virtual {v1}, Lcom/daimajia/swipe/SwipeLayout$DragEdge;->ordinal()I

    move-result v1

    aget v0, v0, v1

    return v0
.end method

.method private insideAdapterView()Z
    .registers 2

    .line 974
    invoke-direct {p0}, Lcom/daimajia/swipe/SwipeLayout;->getAdapterView()Landroid/widget/AdapterView;

    move-result-object v0

    if-eqz v0, :cond_8

    const/4 v0, 0x1

    goto :goto_9

    :cond_8
    const/4 v0, 0x0

    :goto_9
    return v0
.end method

.method private isTouchOnSurface(Landroid/view/MotionEvent;)Z
    .registers 6
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .line 1063
    invoke-virtual {p0}, Lcom/daimajia/swipe/SwipeLayout;->getSurfaceView()Landroid/view/View;

    move-result-object v3

    .line 1064
    .local v3, "surfaceView":Landroid/view/View;
    if-nez v3, :cond_8

    .line 1065
    const/4 v0, 0x0

    return v0

    .line 1067
    :cond_8
    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout;->hitSurfaceRect:Landroid/graphics/Rect;

    if-nez v0, :cond_13

    .line 1068
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/daimajia/swipe/SwipeLayout;->hitSurfaceRect:Landroid/graphics/Rect;

    .line 1070
    :cond_13
    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout;->hitSurfaceRect:Landroid/graphics/Rect;

    invoke-virtual {v3, v0}, Landroid/view/View;->getHitRect(Landroid/graphics/Rect;)V

    .line 1071
    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout;->hitSurfaceRect:Landroid/graphics/Rect;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v0

    return v0
.end method

.method private performAdapterViewItemClick()V
    .registers 7

    .line 986
    invoke-virtual {p0}, Lcom/daimajia/swipe/SwipeLayout;->getOpenStatus()Lcom/daimajia/swipe/SwipeLayout$Status;

    move-result-object v0

    sget-object v1, Lcom/daimajia/swipe/SwipeLayout$Status;->Close:Lcom/daimajia/swipe/SwipeLayout$Status;

    if-eq v0, v1, :cond_9

    return-void

    .line 987
    :cond_9
    invoke-virtual {p0}, Lcom/daimajia/swipe/SwipeLayout;->getParent()Landroid/view/ViewParent;

    move-result-object v3

    .line 988
    .local v3, "t":Landroid/view/ViewParent;
    instance-of v0, v3, Landroid/widget/AdapterView;

    if-eqz v0, :cond_30

    .line 989
    move-object v4, v3

    check-cast v4, Landroid/widget/AdapterView;

    .line 990
    .local v4, "view":Landroid/widget/AdapterView;
    invoke-virtual {v4, p0}, Landroid/widget/AdapterView;->getPositionForView(Landroid/view/View;)I

    move-result v5

    .line 991
    .local v5, "p":I
    const/4 v0, -0x1

    if-eq v5, v0, :cond_30

    .line 992
    invoke-virtual {v4}, Landroid/widget/AdapterView;->getFirstVisiblePosition()I

    move-result v0

    sub-int v0, v5, v0

    invoke-virtual {v4, v0}, Landroid/widget/AdapterView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v4}, Landroid/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v1

    invoke-interface {v1, v5}, Landroid/widget/Adapter;->getItemId(I)J

    move-result-wide v1

    invoke-virtual {v4, v0, v5, v1, v2}, Landroid/widget/AdapterView;->performItemClick(Landroid/view/View;IJ)Z

    .line 996
    .end local v4    # "view":Landroid/widget/AdapterView;
    .end local v5    # "p":I
    :cond_30
    return-void
.end method

.method private performAdapterViewItemLongClick()Z
    .registers 14

    .line 998
    invoke-virtual {p0}, Lcom/daimajia/swipe/SwipeLayout;->getOpenStatus()Lcom/daimajia/swipe/SwipeLayout$Status;

    move-result-object v0

    sget-object v1, Lcom/daimajia/swipe/SwipeLayout$Status;->Close:Lcom/daimajia/swipe/SwipeLayout$Status;

    if-eq v0, v1, :cond_a

    const/4 v0, 0x0

    return v0

    .line 999
    :cond_a
    invoke-virtual {p0}, Lcom/daimajia/swipe/SwipeLayout;->getParent()Landroid/view/ViewParent;

    move-result-object v6

    .line 1000
    .local v6, "t":Landroid/view/ViewParent;
    instance-of v0, v6, Landroid/widget/AdapterView;

    if-eqz v0, :cond_7e

    .line 1001
    move-object v7, v6

    check-cast v7, Landroid/widget/AdapterView;

    .line 1002
    .local v7, "view":Landroid/widget/AdapterView;
    invoke-virtual {v7, p0}, Landroid/widget/AdapterView;->getPositionForView(Landroid/view/View;)I

    move-result v8

    .line 1003
    .local v8, "p":I
    const/4 v0, -0x1

    if-ne v8, v0, :cond_1e

    const/4 v0, 0x0

    return v0

    .line 1004
    :cond_1e
    invoke-virtual {v7, v8}, Landroid/widget/AdapterView;->getItemIdAtPosition(I)J

    move-result-wide v9

    .line 1005
    .local v9, "vId":J
    const/4 v11, 0x0

    .line 1007
    .local v11, "handled":Z
    :try_start_23
    const-class v0, Landroid/widget/AbsListView;

    const-string v1, "performLongPress"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Class;

    const-class v3, Landroid/view/View;

    const/4 v4, 0x0

    aput-object v3, v2, v4

    sget-object v3, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    const/4 v4, 0x1

    aput-object v3, v2, v4

    sget-object v3, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    const/4 v4, 0x2

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v12

    .line 1008
    .local v12, "m":Ljava/lang/reflect/Method;
    const/4 v0, 0x1

    invoke-virtual {v12, v0}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 1009
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x1

    aput-object v1, v0, v2

    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    const/4 v2, 0x2

    aput-object v1, v0, v2

    invoke-virtual {v12, v7, v0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_5e
    .catch Ljava/lang/Exception; {:try_start_23 .. :try_end_5e} :catch_61

    move-result v0

    move v11, v0

    .line 1020
    .end local v12    # "m":Ljava/lang/reflect/Method;
    goto :goto_7d

    .line 1011
    :catch_61
    move-exception v12

    .line 1012
    .local v12, "e":Ljava/lang/Exception;
    invoke-virtual {v12}, Ljava/lang/Exception;->printStackTrace()V

    .line 1014
    invoke-virtual {v7}, Landroid/widget/AdapterView;->getOnItemLongClickListener()Landroid/widget/AdapterView$OnItemLongClickListener;

    move-result-object v0

    if-eqz v0, :cond_77

    .line 1015
    invoke-virtual {v7}, Landroid/widget/AdapterView;->getOnItemLongClickListener()Landroid/widget/AdapterView$OnItemLongClickListener;

    move-result-object v0

    move-object v1, v7

    move-object v2, p0

    move v3, v8

    move-wide v4, v9

    invoke-interface/range {v0 .. v5}, Landroid/widget/AdapterView$OnItemLongClickListener;->onItemLongClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)Z

    move-result v11

    .line 1017
    :cond_77
    if-eqz v11, :cond_7d

    .line 1018
    const/4 v0, 0x0

    invoke-virtual {v7, v0}, Landroid/widget/AdapterView;->performHapticFeedback(I)Z

    .line 1021
    .end local v12    # "e":Ljava/lang/Exception;
    :cond_7d
    :goto_7d
    return v11

    .line 1023
    .end local v7    # "view":Landroid/widget/AdapterView;
    .end local v8    # "p":I
    .end local v9    # "vId":J
    .end local v11    # "handled":Z
    :cond_7e
    const/4 v0, 0x0

    return v0
.end method

.method private safeBottomView()V
    .registers 7

    .line 541
    invoke-virtual {p0}, Lcom/daimajia/swipe/SwipeLayout;->getOpenStatus()Lcom/daimajia/swipe/SwipeLayout$Status;

    move-result-object v2

    .line 542
    .local v2, "status":Lcom/daimajia/swipe/SwipeLayout$Status;
    invoke-virtual {p0}, Lcom/daimajia/swipe/SwipeLayout;->getBottomViews()Ljava/util/List;

    move-result-object v3

    .line 544
    .local v3, "bottoms":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    sget-object v0, Lcom/daimajia/swipe/SwipeLayout$Status;->Close:Lcom/daimajia/swipe/SwipeLayout$Status;

    if-ne v2, v0, :cond_2c

    .line 545
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :goto_10
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2b

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Landroid/view/View;

    .line 546
    .local v5, "bottom":Landroid/view/View;
    if-eqz v5, :cond_2a

    invoke-virtual {v5}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/4 v1, 0x4

    if-eq v0, v1, :cond_2a

    .line 547
    const/4 v0, 0x4

    invoke-virtual {v5, v0}, Landroid/view/View;->setVisibility(I)V

    .line 549
    .end local v5    # "bottom":Landroid/view/View;
    :cond_2a
    goto :goto_10

    .end local v4    # "i$":Ljava/util/Iterator;
    :cond_2b
    goto :goto_3c

    .line 551
    :cond_2c
    invoke-virtual {p0}, Lcom/daimajia/swipe/SwipeLayout;->getCurrentBottomView()Landroid/view/View;

    move-result-object v4

    .line 552
    .local v4, "currentBottomView":Landroid/view/View;
    if-eqz v4, :cond_3c

    invoke-virtual {v4}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_3c

    .line 553
    const/4 v0, 0x0

    invoke-virtual {v4, v0}, Landroid/view/View;->setVisibility(I)V

    .line 556
    .end local v4    # "currentBottomView":Landroid/view/View;
    :cond_3c
    :goto_3c
    return-void
.end method

.method private setCurrentDragEdge(Lcom/daimajia/swipe/SwipeLayout$DragEdge;)V
    .registers 3
    .param p1, "dragEdge"    # Lcom/daimajia/swipe/SwipeLayout$DragEdge;

    .line 1496
    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout;->mCurrentDragEdge:Lcom/daimajia/swipe/SwipeLayout$DragEdge;

    if-eq v0, p1, :cond_9

    .line 1497
    iput-object p1, p0, Lcom/daimajia/swipe/SwipeLayout;->mCurrentDragEdge:Lcom/daimajia/swipe/SwipeLayout$DragEdge;

    .line 1498
    invoke-direct {p0}, Lcom/daimajia/swipe/SwipeLayout;->updateBottomViews()V

    .line 1500
    :cond_9
    return-void
.end method

.method private updateBottomViews()V
    .registers 4

    .line 1503
    invoke-virtual {p0}, Lcom/daimajia/swipe/SwipeLayout;->getCurrentBottomView()Landroid/view/View;

    move-result-object v2

    .line 1504
    .local v2, "currentBottomView":Landroid/view/View;
    if-eqz v2, :cond_31

    .line 1505
    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout;->mCurrentDragEdge:Lcom/daimajia/swipe/SwipeLayout$DragEdge;

    sget-object v1, Lcom/daimajia/swipe/SwipeLayout$DragEdge;->Left:Lcom/daimajia/swipe/SwipeLayout$DragEdge;

    if-eq v0, v1, :cond_12

    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout;->mCurrentDragEdge:Lcom/daimajia/swipe/SwipeLayout$DragEdge;

    sget-object v1, Lcom/daimajia/swipe/SwipeLayout$DragEdge;->Right:Lcom/daimajia/swipe/SwipeLayout$DragEdge;

    if-ne v0, v1, :cond_22

    .line 1506
    :cond_12
    invoke-virtual {v2}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    invoke-direct {p0}, Lcom/daimajia/swipe/SwipeLayout;->getCurrentOffset()F

    move-result v1

    invoke-direct {p0, v1}, Lcom/daimajia/swipe/SwipeLayout;->dp2px(F)I

    move-result v1

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/daimajia/swipe/SwipeLayout;->mDragDistance:I

    goto :goto_31

    .line 1508
    :cond_22
    invoke-virtual {v2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    invoke-direct {p0}, Lcom/daimajia/swipe/SwipeLayout;->getCurrentOffset()F

    move-result v1

    invoke-direct {p0, v1}, Lcom/daimajia/swipe/SwipeLayout;->dp2px(F)I

    move-result v1

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/daimajia/swipe/SwipeLayout;->mDragDistance:I

    .line 1511
    :cond_31
    :goto_31
    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout;->mShowMode:Lcom/daimajia/swipe/SwipeLayout$ShowMode;

    sget-object v1, Lcom/daimajia/swipe/SwipeLayout$ShowMode;->PullOut:Lcom/daimajia/swipe/SwipeLayout$ShowMode;

    if-ne v0, v1, :cond_3b

    .line 1512
    invoke-virtual {p0}, Lcom/daimajia/swipe/SwipeLayout;->layoutPullOut()V

    goto :goto_44

    .line 1513
    :cond_3b
    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout;->mShowMode:Lcom/daimajia/swipe/SwipeLayout$ShowMode;

    sget-object v1, Lcom/daimajia/swipe/SwipeLayout$ShowMode;->LayDown:Lcom/daimajia/swipe/SwipeLayout$ShowMode;

    if-ne v0, v1, :cond_44

    invoke-virtual {p0}, Lcom/daimajia/swipe/SwipeLayout;->layoutLayDown()V

    .line 1515
    :cond_44
    :goto_44
    invoke-direct {p0}, Lcom/daimajia/swipe/SwipeLayout;->safeBottomView()V

    .line 1516
    return-void
.end method


# virtual methods
.method public addDrag(Lcom/daimajia/swipe/SwipeLayout$DragEdge;Landroid/view/View;)V
    .registers 4
    .param p1, "dragEdge"    # Lcom/daimajia/swipe/SwipeLayout$DragEdge;
    .param p2, "child"    # Landroid/view/View;

    .line 659
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/daimajia/swipe/SwipeLayout;->addDrag(Lcom/daimajia/swipe/SwipeLayout$DragEdge;Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 660
    return-void
.end method

.method public addDrag(Lcom/daimajia/swipe/SwipeLayout$DragEdge;Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .registers 7
    .param p1, "dragEdge"    # Lcom/daimajia/swipe/SwipeLayout$DragEdge;
    .param p2, "child"    # Landroid/view/View;
    .param p3, "params"    # Landroid/view/ViewGroup$LayoutParams;

    .line 662
    if-nez p3, :cond_6

    .line 663
    invoke-virtual {p0}, Lcom/daimajia/swipe/SwipeLayout;->generateDefaultLayoutParams()Landroid/widget/FrameLayout$LayoutParams;

    move-result-object p3

    .line 665
    :cond_6
    invoke-virtual {p0, p3}, Lcom/daimajia/swipe/SwipeLayout;->checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z

    move-result v0

    if-nez v0, :cond_10

    .line 666
    invoke-virtual {p0, p3}, Lcom/daimajia/swipe/SwipeLayout;->generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;

    move-result-object p3

    .line 668
    :cond_10
    const/4 v2, -0x1

    .line 669
    .local v2, "gravity":I
    sget-object v0, Lcom/daimajia/swipe/SwipeLayout$4;->$SwitchMap$com$daimajia$swipe$SwipeLayout$DragEdge:[I

    invoke-virtual {p1}, Lcom/daimajia/swipe/SwipeLayout$DragEdge;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_34

    goto :goto_26

    .line 670
    :pswitch_1d
    const/4 v2, 0x3

    goto :goto_26

    .line 671
    :pswitch_1f
    const/4 v2, 0x5

    goto :goto_26

    .line 672
    :pswitch_21
    const/16 v2, 0x30

    goto :goto_26

    .line 673
    :pswitch_24
    const/16 v2, 0x50

    .line 675
    :goto_26
    instance-of v0, p3, Landroid/widget/FrameLayout$LayoutParams;

    if-eqz v0, :cond_2f

    .line 676
    move-object v0, p3

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    iput v2, v0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 678
    :cond_2f
    const/4 v0, 0x0

    invoke-virtual {p0, p2, v0, p3}, Lcom/daimajia/swipe/SwipeLayout;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 679
    return-void

    :pswitch_data_34
    .packed-switch 0x1
        :pswitch_21
        :pswitch_24
        :pswitch_1d
        :pswitch_1f
    .end packed-switch
.end method

.method public addOnLayoutListener(Lcom/daimajia/swipe/SwipeLayout$OnLayout;)V
    .registers 3
    .param p1, "l"    # Lcom/daimajia/swipe/SwipeLayout$OnLayout;

    .line 651
    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout;->mOnLayoutListeners:Ljava/util/List;

    if-nez v0, :cond_b

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/daimajia/swipe/SwipeLayout;->mOnLayoutListeners:Ljava/util/List;

    .line 652
    :cond_b
    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout;->mOnLayoutListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 653
    return-void
.end method

.method public addSwipeListener(Lcom/daimajia/swipe/SwipeLayout$SwipeListener;)V
    .registers 3
    .param p1, "l"    # Lcom/daimajia/swipe/SwipeLayout$SwipeListener;

    .line 128
    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout;->mSwipeListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 129
    return-void
.end method

.method public addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V
    .registers 9
    .param p1, "child"    # Landroid/view/View;
    .param p2, "index"    # I
    .param p3, "params"    # Landroid/view/ViewGroup$LayoutParams;

    .line 682
    const/4 v2, 0x0

    .line 684
    .local v2, "gravity":I
    :try_start_1
    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "gravity"

    invoke-virtual {v0, v1}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
    :try_end_14
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_14} :catch_17

    move-result v0

    move v2, v0

    .line 687
    goto :goto_1b

    .line 685
    :catch_17
    move-exception v3

    .line 686
    .local v3, "e":Ljava/lang/Exception;
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V

    .line 689
    .end local v3    # "e":Ljava/lang/Exception;
    :goto_1b
    if-lez v2, :cond_58

    .line 690
    invoke-static {p0}, Landroid/support/v4/view/ViewCompat;->getLayoutDirection(Landroid/view/View;)I

    move-result v0

    invoke-static {v2, v0}, Landroid/support/v4/view/GravityCompat;->getAbsoluteGravity(II)I

    move-result v2

    .line 692
    and-int/lit8 v0, v2, 0x3

    const/4 v1, 0x3

    if-ne v0, v1, :cond_31

    .line 693
    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout;->mDragEdges:Ljava/util/LinkedHashMap;

    sget-object v1, Lcom/daimajia/swipe/SwipeLayout$DragEdge;->Left:Lcom/daimajia/swipe/SwipeLayout$DragEdge;

    invoke-virtual {v0, v1, p1}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 695
    :cond_31
    and-int/lit8 v0, v2, 0x5

    const/4 v1, 0x5

    if-ne v0, v1, :cond_3d

    .line 696
    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout;->mDragEdges:Ljava/util/LinkedHashMap;

    sget-object v1, Lcom/daimajia/swipe/SwipeLayout$DragEdge;->Right:Lcom/daimajia/swipe/SwipeLayout$DragEdge;

    invoke-virtual {v0, v1, p1}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 698
    :cond_3d
    and-int/lit8 v0, v2, 0x30

    const/16 v1, 0x30

    if-ne v0, v1, :cond_4a

    .line 699
    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout;->mDragEdges:Ljava/util/LinkedHashMap;

    sget-object v1, Lcom/daimajia/swipe/SwipeLayout$DragEdge;->Top:Lcom/daimajia/swipe/SwipeLayout$DragEdge;

    invoke-virtual {v0, v1, p1}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 701
    :cond_4a
    and-int/lit8 v0, v2, 0x50

    const/16 v1, 0x50

    if-ne v0, v1, :cond_80

    .line 702
    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout;->mDragEdges:Ljava/util/LinkedHashMap;

    sget-object v1, Lcom/daimajia/swipe/SwipeLayout$DragEdge;->Bottom:Lcom/daimajia/swipe/SwipeLayout$DragEdge;

    invoke-virtual {v0, v1, p1}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_80

    .line 705
    :cond_58
    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout;->mDragEdges:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_62
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_80

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Ljava/util/Map$Entry;

    .line 706
    .local v4, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lcom/daimajia/swipe/SwipeLayout$DragEdge;Landroid/view/View;>;"
    invoke-interface {v4}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_7f

    .line 708
    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout;->mDragEdges:Ljava/util/LinkedHashMap;

    invoke-interface {v4}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 709
    goto :goto_80

    .line 711
    .end local v4    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lcom/daimajia/swipe/SwipeLayout$DragEdge;Landroid/view/View;>;"
    .end local v4
    :cond_7f
    goto :goto_62

    .line 713
    .end local v3    # "i$":Ljava/util/Iterator;
    :cond_80
    :goto_80
    if-eqz p1, :cond_88

    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-ne v0, p0, :cond_89

    .line 714
    :cond_88
    return-void

    .line 716
    :cond_89
    invoke-super {p0, p1, p2, p3}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 717
    return-void
.end method

.method public close()V
    .registers 3

    .line 1301
    const/4 v0, 0x1

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/daimajia/swipe/SwipeLayout;->close(ZZ)V

    .line 1302
    return-void
.end method

.method public close(Z)V
    .registers 3
    .param p1, "smooth"    # Z

    .line 1305
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/daimajia/swipe/SwipeLayout;->close(ZZ)V

    .line 1306
    return-void
.end method

.method public close(ZZ)V
    .registers 11
    .param p1, "smooth"    # Z
    .param p2, "notify"    # Z

    .line 1315
    invoke-virtual {p0}, Lcom/daimajia/swipe/SwipeLayout;->getSurfaceView()Landroid/view/View;

    move-result-object v4

    .line 1316
    .local v4, "surface":Landroid/view/View;
    if-nez v4, :cond_7

    .line 1317
    return-void

    .line 1320
    :cond_7
    if-eqz p1, :cond_1b

    .line 1321
    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout;->mDragHelper:Landroid/support/v4/widget/ViewDragHelper;

    invoke-virtual {p0}, Lcom/daimajia/swipe/SwipeLayout;->getSurfaceView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {p0}, Lcom/daimajia/swipe/SwipeLayout;->getPaddingLeft()I

    move-result v2

    invoke-virtual {p0}, Lcom/daimajia/swipe/SwipeLayout;->getPaddingTop()I

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Landroid/support/v4/widget/ViewDragHelper;->smoothSlideViewTo(Landroid/view/View;II)Z

    goto :goto_53

    .line 1323
    :cond_1b
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/daimajia/swipe/SwipeLayout;->computeSurfaceLayoutArea(Z)Landroid/graphics/Rect;

    move-result-object v7

    .line 1324
    .local v7, "rect":Landroid/graphics/Rect;
    iget v0, v7, Landroid/graphics/Rect;->left:I

    invoke-virtual {v4}, Landroid/view/View;->getLeft()I

    move-result v1

    sub-int v5, v0, v1

    .line 1325
    .local v5, "dx":I
    iget v0, v7, Landroid/graphics/Rect;->top:I

    invoke-virtual {v4}, Landroid/view/View;->getTop()I

    move-result v1

    sub-int v6, v0, v1

    .line 1326
    .local v6, "dy":I
    iget v0, v7, Landroid/graphics/Rect;->left:I

    iget v1, v7, Landroid/graphics/Rect;->top:I

    iget v2, v7, Landroid/graphics/Rect;->right:I

    iget v3, v7, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v4, v0, v1, v2, v3}, Landroid/view/View;->layout(IIII)V

    .line 1327
    if-eqz p2, :cond_50

    .line 1328
    iget v0, v7, Landroid/graphics/Rect;->left:I

    iget v1, v7, Landroid/graphics/Rect;->top:I

    iget v2, v7, Landroid/graphics/Rect;->right:I

    iget v3, v7, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/daimajia/swipe/SwipeLayout;->dispatchRevealEvent(IIII)V

    .line 1329
    iget v0, v7, Landroid/graphics/Rect;->left:I

    iget v1, v7, Landroid/graphics/Rect;->top:I

    invoke-virtual {p0, v0, v1, v5, v6}, Lcom/daimajia/swipe/SwipeLayout;->dispatchSwipeEvent(IIII)V

    goto :goto_53

    .line 1331
    :cond_50
    invoke-direct {p0}, Lcom/daimajia/swipe/SwipeLayout;->safeBottomView()V

    .line 1334
    .end local v5    # "dx":I
    .end local v6    # "dy":I
    .end local v7    # "rect":Landroid/graphics/Rect;
    :goto_53
    invoke-virtual {p0}, Lcom/daimajia/swipe/SwipeLayout;->invalidate()V

    .line 1335
    return-void
.end method

.method public computeScroll()V
    .registers 3

    .line 634
    invoke-super {p0}, Landroid/widget/FrameLayout;->computeScroll()V

    .line 635
    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout;->mDragHelper:Landroid/support/v4/widget/ViewDragHelper;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/ViewDragHelper;->continueSettling(Z)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 636
    invoke-static {p0}, Landroid/support/v4/view/ViewCompat;->postInvalidateOnAnimation(Landroid/view/View;)V

    .line 638
    :cond_f
    return-void
.end method

.method protected dispatchRevealEvent(IIII)V
    .registers 21
    .param p1, "surfaceLeft"    # I
    .param p2, "surfaceTop"    # I
    .param p3, "surfaceRight"    # I
    .param p4, "surfaceBottom"    # I

    .line 560
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/daimajia/swipe/SwipeLayout;->mRevealListeners:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_b

    return-void

    .line 561
    :cond_b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/daimajia/swipe/SwipeLayout;->mRevealListeners:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .local v8, "i$":Ljava/util/Iterator;
    :goto_17
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_19e

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Ljava/util/Map$Entry;

    .line 562
    .local v9, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Landroid/view/View;Ljava/util/ArrayList<Lcom/daimajia/swipe/SwipeLayout$OnRevealListener;>;>;"
    invoke-interface {v9}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Landroid/view/View;

    .line 563
    .local v10, "child":Landroid/view/View;
    move-object/from16 v0, p0

    invoke-virtual {v0, v10}, Lcom/daimajia/swipe/SwipeLayout;->getRelativePosition(Landroid/view/View;)Landroid/graphics/Rect;

    move-result-object v11

    .line 564
    .local v11, "rect":Landroid/graphics/Rect;
    move-object/from16 v0, p0

    move-object v1, v10

    move-object v2, v11

    move-object/from16 v3, p0

    iget-object v3, v3, Lcom/daimajia/swipe/SwipeLayout;->mCurrentDragEdge:Lcom/daimajia/swipe/SwipeLayout$DragEdge;

    move/from16 v4, p1

    move/from16 v5, p2

    move/from16 v6, p3

    move/from16 v7, p4

    invoke-virtual/range {v0 .. v7}, Lcom/daimajia/swipe/SwipeLayout;->isViewShowing(Landroid/view/View;Landroid/graphics/Rect;Lcom/daimajia/swipe/SwipeLayout$DragEdge;IIII)Z

    move-result v0

    if-eqz v0, :cond_137

    .line 566
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/daimajia/swipe/SwipeLayout;->mShowEntirely:Ljava/util/Map;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-interface {v0, v10, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 567
    const/4 v12, 0x0

    .line 568
    .local v12, "distance":I
    const/4 v13, 0x0

    .line 569
    .local v13, "fraction":F
    invoke-virtual/range {p0 .. p0}, Lcom/daimajia/swipe/SwipeLayout;->getShowMode()Lcom/daimajia/swipe/SwipeLayout$ShowMode;

    move-result-object v0

    sget-object v1, Lcom/daimajia/swipe/SwipeLayout$ShowMode;->LayDown:Lcom/daimajia/swipe/SwipeLayout$ShowMode;

    if-ne v0, v1, :cond_a2

    .line 570
    sget-object v0, Lcom/daimajia/swipe/SwipeLayout$4;->$SwitchMap$com$daimajia$swipe$SwipeLayout$DragEdge:[I

    move-object/from16 v1, p0

    iget-object v1, v1, Lcom/daimajia/swipe/SwipeLayout;->mCurrentDragEdge:Lcom/daimajia/swipe/SwipeLayout$DragEdge;

    invoke-virtual {v1}, Lcom/daimajia/swipe/SwipeLayout$DragEdge;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_1a0

    goto :goto_a0

    .line 572
    :pswitch_6d
    iget v0, v11, Landroid/graphics/Rect;->left:I

    sub-int v12, v0, p1

    .line 573
    int-to-float v0, v12

    invoke-virtual {v10}, Landroid/view/View;->getWidth()I

    move-result v1

    int-to-float v1, v1

    div-float v13, v0, v1

    .line 574
    goto :goto_a0

    .line 576
    :pswitch_7a
    iget v0, v11, Landroid/graphics/Rect;->right:I

    sub-int v12, v0, p3

    .line 577
    int-to-float v0, v12

    invoke-virtual {v10}, Landroid/view/View;->getWidth()I

    move-result v1

    int-to-float v1, v1

    div-float v13, v0, v1

    .line 578
    goto :goto_a0

    .line 580
    :pswitch_87
    iget v0, v11, Landroid/graphics/Rect;->top:I

    sub-int v12, v0, p2

    .line 581
    int-to-float v0, v12

    invoke-virtual {v10}, Landroid/view/View;->getHeight()I

    move-result v1

    int-to-float v1, v1

    div-float v13, v0, v1

    .line 582
    goto :goto_a0

    .line 584
    :pswitch_94
    iget v0, v11, Landroid/graphics/Rect;->bottom:I

    sub-int v12, v0, p4

    .line 585
    int-to-float v0, v12

    invoke-virtual {v10}, Landroid/view/View;->getHeight()I

    move-result v1

    int-to-float v1, v1

    div-float v13, v0, v1

    .line 586
    :goto_a0
    goto/16 :goto_fe

    .line 588
    :cond_a2
    invoke-virtual/range {p0 .. p0}, Lcom/daimajia/swipe/SwipeLayout;->getShowMode()Lcom/daimajia/swipe/SwipeLayout$ShowMode;

    move-result-object v0

    sget-object v1, Lcom/daimajia/swipe/SwipeLayout$ShowMode;->PullOut:Lcom/daimajia/swipe/SwipeLayout$ShowMode;

    if-ne v0, v1, :cond_fe

    .line 589
    sget-object v0, Lcom/daimajia/swipe/SwipeLayout$4;->$SwitchMap$com$daimajia$swipe$SwipeLayout$DragEdge:[I

    move-object/from16 v1, p0

    iget-object v1, v1, Lcom/daimajia/swipe/SwipeLayout;->mCurrentDragEdge:Lcom/daimajia/swipe/SwipeLayout$DragEdge;

    invoke-virtual {v1}, Lcom/daimajia/swipe/SwipeLayout$DragEdge;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_1ac

    goto/16 :goto_fe

    .line 591
    :pswitch_bb
    iget v0, v11, Landroid/graphics/Rect;->right:I

    invoke-virtual/range {p0 .. p0}, Lcom/daimajia/swipe/SwipeLayout;->getPaddingLeft()I

    move-result v1

    sub-int v12, v0, v1

    .line 592
    int-to-float v0, v12

    invoke-virtual {v10}, Landroid/view/View;->getWidth()I

    move-result v1

    int-to-float v1, v1

    div-float v13, v0, v1

    .line 593
    goto :goto_fe

    .line 595
    :pswitch_cc
    iget v0, v11, Landroid/graphics/Rect;->left:I

    invoke-virtual/range {p0 .. p0}, Lcom/daimajia/swipe/SwipeLayout;->getWidth()I

    move-result v1

    sub-int v12, v0, v1

    .line 596
    int-to-float v0, v12

    invoke-virtual {v10}, Landroid/view/View;->getWidth()I

    move-result v1

    int-to-float v1, v1

    div-float v13, v0, v1

    .line 597
    goto :goto_fe

    .line 599
    :pswitch_dd
    iget v0, v11, Landroid/graphics/Rect;->bottom:I

    invoke-virtual/range {p0 .. p0}, Lcom/daimajia/swipe/SwipeLayout;->getPaddingTop()I

    move-result v1

    sub-int v12, v0, v1

    .line 600
    int-to-float v0, v12

    invoke-virtual {v10}, Landroid/view/View;->getHeight()I

    move-result v1

    int-to-float v1, v1

    div-float v13, v0, v1

    .line 601
    goto :goto_fe

    .line 603
    :pswitch_ee
    iget v0, v11, Landroid/graphics/Rect;->top:I

    invoke-virtual/range {p0 .. p0}, Lcom/daimajia/swipe/SwipeLayout;->getHeight()I

    move-result v1

    sub-int v12, v0, v1

    .line 604
    int-to-float v0, v12

    invoke-virtual {v10}, Landroid/view/View;->getHeight()I

    move-result v1

    int-to-float v1, v1

    div-float v13, v0, v1

    .line 609
    :cond_fe
    :goto_fe
    invoke-interface {v9}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v14

    .local v14, "i$":Ljava/util/Iterator;
    :goto_108
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_137

    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v15, v0

    check-cast v15, Lcom/daimajia/swipe/SwipeLayout$OnRevealListener;

    .line 610
    .local v15, "l":Lcom/daimajia/swipe/SwipeLayout$OnRevealListener;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/daimajia/swipe/SwipeLayout;->mCurrentDragEdge:Lcom/daimajia/swipe/SwipeLayout$DragEdge;

    invoke-static {v13}, Ljava/lang/Math;->abs(F)F

    move-result v1

    invoke-interface {v15, v10, v0, v1, v12}, Lcom/daimajia/swipe/SwipeLayout$OnRevealListener;->onReveal(Landroid/view/View;Lcom/daimajia/swipe/SwipeLayout$DragEdge;FI)V

    .line 611
    invoke-static {v13}, Ljava/lang/Math;->abs(F)F

    move-result v0

    const/high16 v1, 0x3f800000    # 1.0f

    cmpl-float v0, v0, v1

    if-nez v0, :cond_136

    .line 612
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/daimajia/swipe/SwipeLayout;->mShowEntirely:Ljava/util/Map;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-interface {v0, v10, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 614
    .end local v15    # "l":Lcom/daimajia/swipe/SwipeLayout$OnRevealListener;
    :cond_136
    goto :goto_108

    .line 617
    .end local v12    # "distance":I
    .end local v13    # "fraction":F
    .end local v14    # "i$":Ljava/util/Iterator;
    :cond_137
    move-object/from16 v0, p0

    move-object v1, v10

    move-object v2, v11

    move-object/from16 v3, p0

    iget-object v3, v3, Lcom/daimajia/swipe/SwipeLayout;->mCurrentDragEdge:Lcom/daimajia/swipe/SwipeLayout$DragEdge;

    move/from16 v4, p1

    move/from16 v5, p2

    move/from16 v6, p3

    move/from16 v7, p4

    invoke-virtual/range {v0 .. v7}, Lcom/daimajia/swipe/SwipeLayout;->isViewTotallyFirstShowed(Landroid/view/View;Landroid/graphics/Rect;Lcom/daimajia/swipe/SwipeLayout$DragEdge;IIII)Z

    move-result v0

    if-eqz v0, :cond_19c

    .line 619
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/daimajia/swipe/SwipeLayout;->mShowEntirely:Ljava/util/Map;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-interface {v0, v10, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 620
    invoke-interface {v9}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v12

    .local v12, "i$":Ljava/util/Iterator;
    :goto_163
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_19c

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v13, v0

    check-cast v13, Lcom/daimajia/swipe/SwipeLayout$OnRevealListener;

    .line 621
    .local v13, "l":Lcom/daimajia/swipe/SwipeLayout$OnRevealListener;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/daimajia/swipe/SwipeLayout;->mCurrentDragEdge:Lcom/daimajia/swipe/SwipeLayout$DragEdge;

    sget-object v1, Lcom/daimajia/swipe/SwipeLayout$DragEdge;->Left:Lcom/daimajia/swipe/SwipeLayout$DragEdge;

    if-eq v0, v1, :cond_180

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/daimajia/swipe/SwipeLayout;->mCurrentDragEdge:Lcom/daimajia/swipe/SwipeLayout$DragEdge;

    sget-object v1, Lcom/daimajia/swipe/SwipeLayout$DragEdge;->Right:Lcom/daimajia/swipe/SwipeLayout$DragEdge;

    if-ne v0, v1, :cond_18e

    .line 623
    :cond_180
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/daimajia/swipe/SwipeLayout;->mCurrentDragEdge:Lcom/daimajia/swipe/SwipeLayout$DragEdge;

    invoke-virtual {v10}, Landroid/view/View;->getWidth()I

    move-result v1

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-interface {v13, v10, v0, v2, v1}, Lcom/daimajia/swipe/SwipeLayout$OnRevealListener;->onReveal(Landroid/view/View;Lcom/daimajia/swipe/SwipeLayout$DragEdge;FI)V

    goto :goto_19b

    .line 625
    :cond_18e
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/daimajia/swipe/SwipeLayout;->mCurrentDragEdge:Lcom/daimajia/swipe/SwipeLayout$DragEdge;

    invoke-virtual {v10}, Landroid/view/View;->getHeight()I

    move-result v1

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-interface {v13, v10, v0, v2, v1}, Lcom/daimajia/swipe/SwipeLayout$OnRevealListener;->onReveal(Landroid/view/View;Lcom/daimajia/swipe/SwipeLayout$DragEdge;FI)V

    .line 626
    .end local v13    # "l":Lcom/daimajia/swipe/SwipeLayout$OnRevealListener;
    :goto_19b
    goto :goto_163

    .line 629
    .end local v9    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Landroid/view/View;Ljava/util/ArrayList<Lcom/daimajia/swipe/SwipeLayout$OnRevealListener;>;>;"
    .end local v9
    .end local v10    # "child":Landroid/view/View;
    .end local v11    # "rect":Landroid/graphics/Rect;
    .end local v12    # "i$":Ljava/util/Iterator;
    :cond_19c
    goto/16 :goto_17

    .line 630
    .end local v8    # "i$":Ljava/util/Iterator;
    :cond_19e
    return-void

    nop

    :pswitch_data_1a0
    .packed-switch 0x1
        :pswitch_87
        :pswitch_94
        :pswitch_6d
        :pswitch_7a
    .end packed-switch

    :pswitch_data_1ac
    .packed-switch 0x1
        :pswitch_dd
        :pswitch_ee
        :pswitch_bb
        :pswitch_cc
    .end packed-switch
.end method

.method protected dispatchSwipeEvent(IIII)V
    .registers 8
    .param p1, "surfaceLeft"    # I
    .param p2, "surfaceTop"    # I
    .param p3, "dx"    # I
    .param p4, "dy"    # I

    .line 485
    invoke-virtual {p0}, Lcom/daimajia/swipe/SwipeLayout;->getDragEdge()Lcom/daimajia/swipe/SwipeLayout$DragEdge;

    move-result-object v1

    .line 486
    .local v1, "edge":Lcom/daimajia/swipe/SwipeLayout$DragEdge;
    const/4 v2, 0x1

    .line 487
    .local v2, "open":Z
    sget-object v0, Lcom/daimajia/swipe/SwipeLayout$DragEdge;->Left:Lcom/daimajia/swipe/SwipeLayout$DragEdge;

    if-ne v1, v0, :cond_d

    .line 488
    if-gez p3, :cond_24

    const/4 v2, 0x0

    goto :goto_24

    .line 489
    :cond_d
    sget-object v0, Lcom/daimajia/swipe/SwipeLayout$DragEdge;->Right:Lcom/daimajia/swipe/SwipeLayout$DragEdge;

    if-ne v1, v0, :cond_15

    .line 490
    if-lez p3, :cond_24

    const/4 v2, 0x0

    goto :goto_24

    .line 491
    :cond_15
    sget-object v0, Lcom/daimajia/swipe/SwipeLayout$DragEdge;->Top:Lcom/daimajia/swipe/SwipeLayout$DragEdge;

    if-ne v1, v0, :cond_1d

    .line 492
    if-gez p4, :cond_24

    const/4 v2, 0x0

    goto :goto_24

    .line 493
    :cond_1d
    sget-object v0, Lcom/daimajia/swipe/SwipeLayout$DragEdge;->Bottom:Lcom/daimajia/swipe/SwipeLayout$DragEdge;

    if-ne v1, v0, :cond_24

    .line 494
    if-lez p4, :cond_24

    const/4 v2, 0x0

    .line 497
    :cond_24
    :goto_24
    invoke-virtual {p0, p1, p2, v2}, Lcom/daimajia/swipe/SwipeLayout;->dispatchSwipeEvent(IIZ)V

    .line 498
    return-void
.end method

.method protected dispatchSwipeEvent(IIZ)V
    .registers 10
    .param p1, "surfaceLeft"    # I
    .param p2, "surfaceTop"    # I
    .param p3, "open"    # Z

    .line 501
    invoke-direct {p0}, Lcom/daimajia/swipe/SwipeLayout;->safeBottomView()V

    .line 502
    invoke-virtual {p0}, Lcom/daimajia/swipe/SwipeLayout;->getOpenStatus()Lcom/daimajia/swipe/SwipeLayout$Status;

    move-result-object v2

    .line 504
    .local v2, "status":Lcom/daimajia/swipe/SwipeLayout$Status;
    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout;->mSwipeListeners:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_8c

    .line 505
    iget v0, p0, Lcom/daimajia/swipe/SwipeLayout;->mEventCounter:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/daimajia/swipe/SwipeLayout;->mEventCounter:I

    .line 506
    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout;->mSwipeListeners:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_1b
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_46

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/daimajia/swipe/SwipeLayout$SwipeListener;

    .line 507
    .local v4, "l":Lcom/daimajia/swipe/SwipeLayout$SwipeListener;
    iget v0, p0, Lcom/daimajia/swipe/SwipeLayout;->mEventCounter:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_36

    .line 508
    if-eqz p3, :cond_33

    .line 509
    invoke-interface {v4, p0}, Lcom/daimajia/swipe/SwipeLayout$SwipeListener;->onStartOpen(Lcom/daimajia/swipe/SwipeLayout;)V

    goto :goto_36

    .line 511
    :cond_33
    invoke-interface {v4, p0}, Lcom/daimajia/swipe/SwipeLayout$SwipeListener;->onStartClose(Lcom/daimajia/swipe/SwipeLayout;)V

    .line 514
    :cond_36
    :goto_36
    invoke-virtual {p0}, Lcom/daimajia/swipe/SwipeLayout;->getPaddingLeft()I

    move-result v0

    sub-int v0, p1, v0

    invoke-virtual {p0}, Lcom/daimajia/swipe/SwipeLayout;->getPaddingTop()I

    move-result v1

    sub-int v1, p2, v1

    invoke-interface {v4, p0, v0, v1}, Lcom/daimajia/swipe/SwipeLayout$SwipeListener;->onUpdate(Lcom/daimajia/swipe/SwipeLayout;II)V

    .line 515
    .end local v4    # "l":Lcom/daimajia/swipe/SwipeLayout$SwipeListener;
    goto :goto_1b

    .line 517
    .end local v3    # "i$":Ljava/util/Iterator;
    :cond_46
    sget-object v0, Lcom/daimajia/swipe/SwipeLayout$Status;->Close:Lcom/daimajia/swipe/SwipeLayout$Status;

    if-ne v2, v0, :cond_64

    .line 518
    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout;->mSwipeListeners:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_50
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_61

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/daimajia/swipe/SwipeLayout$SwipeListener;

    .line 519
    .local v4, "l":Lcom/daimajia/swipe/SwipeLayout$SwipeListener;
    invoke-interface {v4, p0}, Lcom/daimajia/swipe/SwipeLayout$SwipeListener;->onClose(Lcom/daimajia/swipe/SwipeLayout;)V

    .line 520
    .end local v4    # "l":Lcom/daimajia/swipe/SwipeLayout$SwipeListener;
    goto :goto_50

    .line 521
    .end local v3    # "i$":Ljava/util/Iterator;
    :cond_61
    const/4 v0, 0x0

    iput v0, p0, Lcom/daimajia/swipe/SwipeLayout;->mEventCounter:I

    .line 524
    :cond_64
    sget-object v0, Lcom/daimajia/swipe/SwipeLayout$Status;->Open:Lcom/daimajia/swipe/SwipeLayout$Status;

    if-ne v2, v0, :cond_8c

    .line 525
    invoke-virtual {p0}, Lcom/daimajia/swipe/SwipeLayout;->getCurrentBottomView()Landroid/view/View;

    move-result-object v3

    .line 526
    .local v3, "currentBottomView":Landroid/view/View;
    if-eqz v3, :cond_72

    .line 527
    const/4 v0, 0x1

    invoke-virtual {v3, v0}, Landroid/view/View;->setEnabled(Z)V

    .line 529
    :cond_72
    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout;->mSwipeListeners:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :goto_78
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_89

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/daimajia/swipe/SwipeLayout$SwipeListener;

    .line 530
    .local v5, "l":Lcom/daimajia/swipe/SwipeLayout$SwipeListener;
    invoke-interface {v5, p0}, Lcom/daimajia/swipe/SwipeLayout$SwipeListener;->onOpen(Lcom/daimajia/swipe/SwipeLayout;)V

    .line 531
    .end local v5    # "l":Lcom/daimajia/swipe/SwipeLayout$SwipeListener;
    goto :goto_78

    .line 532
    .end local v4    # "i$":Ljava/util/Iterator;
    :cond_89
    const/4 v0, 0x0

    iput v0, p0, Lcom/daimajia/swipe/SwipeLayout;->mEventCounter:I

    .line 535
    .end local v3    # "currentBottomView":Landroid/view/View;
    :cond_8c
    return-void
.end method

.method public getBottomViews()Ljava/util/List;
    .registers 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Landroid/view/View;>;"
        }
    .end annotation

    .line 1156
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1157
    .local v1, "bottoms":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/view/View;>;"
    invoke-static {}, Lcom/daimajia/swipe/SwipeLayout$DragEdge;->values()[Lcom/daimajia/swipe/SwipeLayout$DragEdge;

    move-result-object v2

    .local v2, "arr$":[Lcom/daimajia/swipe/SwipeLayout$DragEdge;
    array-length v3, v2

    .local v3, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_b
    if-ge v4, v3, :cond_1b

    aget-object v5, v2, v4

    .line 1158
    .local v5, "dragEdge":Lcom/daimajia/swipe/SwipeLayout$DragEdge;
    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout;->mDragEdges:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, v5}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1157
    .end local v5    # "dragEdge":Lcom/daimajia/swipe/SwipeLayout$DragEdge;
    add-int/lit8 v4, v4, 0x1

    goto :goto_b

    .line 1160
    .end local v2    # "arr$":[Lcom/daimajia/swipe/SwipeLayout$DragEdge;
    .end local v3    # "len$":I
    .end local v4    # "i$":I
    :cond_1b
    return-object v1
.end method

.method public getCurrentBottomView()Landroid/view/View;
    .registers 4

    .line 1146
    invoke-virtual {p0}, Lcom/daimajia/swipe/SwipeLayout;->getBottomViews()Ljava/util/List;

    move-result-object v2

    .line 1147
    .local v2, "bottoms":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout;->mCurrentDragEdge:Lcom/daimajia/swipe/SwipeLayout$DragEdge;

    invoke-virtual {v0}, Lcom/daimajia/swipe/SwipeLayout$DragEdge;->ordinal()I

    move-result v0

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_1d

    .line 1148
    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout;->mCurrentDragEdge:Lcom/daimajia/swipe/SwipeLayout$DragEdge;

    invoke-virtual {v0}, Lcom/daimajia/swipe/SwipeLayout$DragEdge;->ordinal()I

    move-result v0

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0

    .line 1150
    :cond_1d
    const/4 v0, 0x0

    return-object v0
.end method

.method public getDragEdge()Lcom/daimajia/swipe/SwipeLayout$DragEdge;
    .registers 2

    .line 1126
    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout;->mCurrentDragEdge:Lcom/daimajia/swipe/SwipeLayout$DragEdge;

    return-object v0
.end method

.method public getOpenStatus()Lcom/daimajia/swipe/SwipeLayout$Status;
    .registers 6

    .line 1176
    invoke-virtual {p0}, Lcom/daimajia/swipe/SwipeLayout;->getSurfaceView()Landroid/view/View;

    move-result-object v2

    .line 1177
    .local v2, "surfaceView":Landroid/view/View;
    if-nez v2, :cond_9

    .line 1178
    sget-object v0, Lcom/daimajia/swipe/SwipeLayout$Status;->Close:Lcom/daimajia/swipe/SwipeLayout$Status;

    return-object v0

    .line 1180
    :cond_9
    invoke-virtual {v2}, Landroid/view/View;->getLeft()I

    move-result v3

    .line 1181
    .local v3, "surfaceLeft":I
    invoke-virtual {v2}, Landroid/view/View;->getTop()I

    move-result v4

    .line 1182
    .local v4, "surfaceTop":I
    invoke-virtual {p0}, Lcom/daimajia/swipe/SwipeLayout;->getPaddingLeft()I

    move-result v0

    if-ne v3, v0, :cond_20

    invoke-virtual {p0}, Lcom/daimajia/swipe/SwipeLayout;->getPaddingTop()I

    move-result v0

    if-ne v4, v0, :cond_20

    sget-object v0, Lcom/daimajia/swipe/SwipeLayout$Status;->Close:Lcom/daimajia/swipe/SwipeLayout$Status;

    return-object v0

    .line 1184
    :cond_20
    invoke-virtual {p0}, Lcom/daimajia/swipe/SwipeLayout;->getPaddingLeft()I

    move-result v0

    iget v1, p0, Lcom/daimajia/swipe/SwipeLayout;->mDragDistance:I

    sub-int/2addr v0, v1

    if-eq v3, v0, :cond_44

    invoke-virtual {p0}, Lcom/daimajia/swipe/SwipeLayout;->getPaddingLeft()I

    move-result v0

    iget v1, p0, Lcom/daimajia/swipe/SwipeLayout;->mDragDistance:I

    add-int/2addr v0, v1

    if-eq v3, v0, :cond_44

    invoke-virtual {p0}, Lcom/daimajia/swipe/SwipeLayout;->getPaddingTop()I

    move-result v0

    iget v1, p0, Lcom/daimajia/swipe/SwipeLayout;->mDragDistance:I

    sub-int/2addr v0, v1

    if-eq v4, v0, :cond_44

    invoke-virtual {p0}, Lcom/daimajia/swipe/SwipeLayout;->getPaddingTop()I

    move-result v0

    iget v1, p0, Lcom/daimajia/swipe/SwipeLayout;->mDragDistance:I

    add-int/2addr v0, v1

    if-ne v4, v0, :cond_47

    .line 1186
    :cond_44
    sget-object v0, Lcom/daimajia/swipe/SwipeLayout$Status;->Open:Lcom/daimajia/swipe/SwipeLayout$Status;

    return-object v0

    .line 1188
    :cond_47
    sget-object v0, Lcom/daimajia/swipe/SwipeLayout$Status;->Middle:Lcom/daimajia/swipe/SwipeLayout$Status;

    return-object v0
.end method

.method protected getRelativePosition(Landroid/view/View;)Landroid/graphics/Rect;
    .registers 8
    .param p1, "child"    # Landroid/view/View;

    .line 469
    move-object v4, p1

    .line 470
    .local v4, "t":Landroid/view/View;
    new-instance v5, Landroid/graphics/Rect;

    invoke-virtual {v4}, Landroid/view/View;->getLeft()I

    move-result v0

    invoke-virtual {v4}, Landroid/view/View;->getTop()I

    move-result v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-direct {v5, v0, v1, v2, v3}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 471
    .local v5, "r":Landroid/graphics/Rect;
    :goto_10
    invoke-virtual {v4}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_39

    invoke-virtual {p0}, Lcom/daimajia/swipe/SwipeLayout;->getRootView()Landroid/view/View;

    move-result-object v0

    if-eq v4, v0, :cond_39

    .line 472
    invoke-virtual {v4}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Landroid/view/View;

    .line 473
    if-ne v4, p0, :cond_26

    goto :goto_39

    .line 474
    :cond_26
    iget v0, v5, Landroid/graphics/Rect;->left:I

    invoke-virtual {v4}, Landroid/view/View;->getLeft()I

    move-result v1

    add-int/2addr v0, v1

    iput v0, v5, Landroid/graphics/Rect;->left:I

    .line 475
    iget v0, v5, Landroid/graphics/Rect;->top:I

    invoke-virtual {v4}, Landroid/view/View;->getTop()I

    move-result v1

    add-int/2addr v0, v1

    iput v0, v5, Landroid/graphics/Rect;->top:I

    goto :goto_10

    .line 477
    :cond_39
    :goto_39
    iget v0, v5, Landroid/graphics/Rect;->left:I

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    add-int/2addr v0, v1

    iput v0, v5, Landroid/graphics/Rect;->right:I

    .line 478
    iget v0, v5, Landroid/graphics/Rect;->top:I

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    add-int/2addr v0, v1

    iput v0, v5, Landroid/graphics/Rect;->bottom:I

    .line 479
    return-object v5
.end method

.method public getShowMode()Lcom/daimajia/swipe/SwipeLayout$ShowMode;
    .registers 2

    .line 1134
    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout;->mShowMode:Lcom/daimajia/swipe/SwipeLayout$ShowMode;

    return-object v0
.end method

.method public getSurfaceView()Landroid/view/View;
    .registers 3

    .line 1139
    invoke-virtual {p0}, Lcom/daimajia/swipe/SwipeLayout;->getChildCount()I

    move-result v0

    if-nez v0, :cond_8

    const/4 v0, 0x0

    return-object v0

    .line 1140
    :cond_8
    invoke-virtual {p0}, Lcom/daimajia/swipe/SwipeLayout;->getChildCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0}, Lcom/daimajia/swipe/SwipeLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public isBottomSwipeEnabled()Z
    .registers 4

    .line 965
    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout;->mDragEdges:Ljava/util/LinkedHashMap;

    sget-object v1, Lcom/daimajia/swipe/SwipeLayout$DragEdge;->Bottom:Lcom/daimajia/swipe/SwipeLayout$DragEdge;

    invoke-virtual {v0, v1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Landroid/view/View;

    .line 966
    .local v2, "bottomView":Landroid/view/View;
    if-eqz v2, :cond_27

    invoke-virtual {v2}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-ne v0, p0, :cond_27

    invoke-virtual {p0}, Lcom/daimajia/swipe/SwipeLayout;->getSurfaceView()Landroid/view/View;

    move-result-object v0

    if-eq v2, v0, :cond_27

    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout;->mSwipesEnabled:[Z

    sget-object v1, Lcom/daimajia/swipe/SwipeLayout$DragEdge;->Bottom:Lcom/daimajia/swipe/SwipeLayout$DragEdge;

    invoke-virtual {v1}, Lcom/daimajia/swipe/SwipeLayout$DragEdge;->ordinal()I

    move-result v1

    aget-boolean v0, v0, v1

    if-eqz v0, :cond_27

    const/4 v0, 0x1

    goto :goto_28

    :cond_27
    const/4 v0, 0x0

    :goto_28
    return v0
.end method

.method public isLeftSwipeEnabled()Z
    .registers 4

    .line 935
    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout;->mDragEdges:Ljava/util/LinkedHashMap;

    sget-object v1, Lcom/daimajia/swipe/SwipeLayout$DragEdge;->Left:Lcom/daimajia/swipe/SwipeLayout$DragEdge;

    invoke-virtual {v0, v1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Landroid/view/View;

    .line 936
    .local v2, "bottomView":Landroid/view/View;
    if-eqz v2, :cond_27

    invoke-virtual {v2}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-ne v0, p0, :cond_27

    invoke-virtual {p0}, Lcom/daimajia/swipe/SwipeLayout;->getSurfaceView()Landroid/view/View;

    move-result-object v0

    if-eq v2, v0, :cond_27

    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout;->mSwipesEnabled:[Z

    sget-object v1, Lcom/daimajia/swipe/SwipeLayout$DragEdge;->Left:Lcom/daimajia/swipe/SwipeLayout$DragEdge;

    invoke-virtual {v1}, Lcom/daimajia/swipe/SwipeLayout$DragEdge;->ordinal()I

    move-result v1

    aget-boolean v0, v0, v1

    if-eqz v0, :cond_27

    const/4 v0, 0x1

    goto :goto_28

    :cond_27
    const/4 v0, 0x0

    :goto_28
    return v0
.end method

.method public isRightSwipeEnabled()Z
    .registers 4

    .line 945
    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout;->mDragEdges:Ljava/util/LinkedHashMap;

    sget-object v1, Lcom/daimajia/swipe/SwipeLayout$DragEdge;->Right:Lcom/daimajia/swipe/SwipeLayout$DragEdge;

    invoke-virtual {v0, v1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Landroid/view/View;

    .line 946
    .local v2, "bottomView":Landroid/view/View;
    if-eqz v2, :cond_27

    invoke-virtual {v2}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-ne v0, p0, :cond_27

    invoke-virtual {p0}, Lcom/daimajia/swipe/SwipeLayout;->getSurfaceView()Landroid/view/View;

    move-result-object v0

    if-eq v2, v0, :cond_27

    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout;->mSwipesEnabled:[Z

    sget-object v1, Lcom/daimajia/swipe/SwipeLayout$DragEdge;->Right:Lcom/daimajia/swipe/SwipeLayout$DragEdge;

    invoke-virtual {v1}, Lcom/daimajia/swipe/SwipeLayout$DragEdge;->ordinal()I

    move-result v1

    aget-boolean v0, v0, v1

    if-eqz v0, :cond_27

    const/4 v0, 0x1

    goto :goto_28

    :cond_27
    const/4 v0, 0x0

    :goto_28
    return v0
.end method

.method public isSwipeEnabled()Z
    .registers 2

    .line 931
    iget-boolean v0, p0, Lcom/daimajia/swipe/SwipeLayout;->mSwipeEnabled:Z

    return v0
.end method

.method public isTopSwipeEnabled()Z
    .registers 4

    .line 955
    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout;->mDragEdges:Ljava/util/LinkedHashMap;

    sget-object v1, Lcom/daimajia/swipe/SwipeLayout$DragEdge;->Top:Lcom/daimajia/swipe/SwipeLayout$DragEdge;

    invoke-virtual {v0, v1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Landroid/view/View;

    .line 956
    .local v2, "bottomView":Landroid/view/View;
    if-eqz v2, :cond_27

    invoke-virtual {v2}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-ne v0, p0, :cond_27

    invoke-virtual {p0}, Lcom/daimajia/swipe/SwipeLayout;->getSurfaceView()Landroid/view/View;

    move-result-object v0

    if-eq v2, v0, :cond_27

    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout;->mSwipesEnabled:[Z

    sget-object v1, Lcom/daimajia/swipe/SwipeLayout$DragEdge;->Top:Lcom/daimajia/swipe/SwipeLayout$DragEdge;

    invoke-virtual {v1}, Lcom/daimajia/swipe/SwipeLayout$DragEdge;->ordinal()I

    move-result v1

    aget-boolean v0, v0, v1

    if-eqz v0, :cond_27

    const/4 v0, 0x1

    goto :goto_28

    :cond_27
    const/4 v0, 0x0

    :goto_28
    return v0
.end method

.method protected isViewShowing(Landroid/view/View;Landroid/graphics/Rect;Lcom/daimajia/swipe/SwipeLayout$DragEdge;IIII)Z
    .registers 14
    .param p1, "child"    # Landroid/view/View;
    .param p2, "relativePosition"    # Landroid/graphics/Rect;
    .param p3, "availableEdge"    # Lcom/daimajia/swipe/SwipeLayout$DragEdge;
    .param p4, "surfaceLeft"    # I
    .param p5, "surfaceTop"    # I
    .param p6, "surfaceRight"    # I
    .param p7, "surfaceBottom"    # I

    .line 422
    iget v2, p2, Landroid/graphics/Rect;->left:I

    .line 423
    .local v2, "childLeft":I
    iget v3, p2, Landroid/graphics/Rect;->right:I

    .line 424
    .local v3, "childRight":I
    iget v4, p2, Landroid/graphics/Rect;->top:I

    .line 425
    .local v4, "childTop":I
    iget v5, p2, Landroid/graphics/Rect;->bottom:I

    .line 426
    .local v5, "childBottom":I
    invoke-virtual {p0}, Lcom/daimajia/swipe/SwipeLayout;->getShowMode()Lcom/daimajia/swipe/SwipeLayout$ShowMode;

    move-result-object v0

    sget-object v1, Lcom/daimajia/swipe/SwipeLayout$ShowMode;->LayDown:Lcom/daimajia/swipe/SwipeLayout$ShowMode;

    if-ne v0, v1, :cond_36

    .line 427
    sget-object v0, Lcom/daimajia/swipe/SwipeLayout$4;->$SwitchMap$com$daimajia$swipe$SwipeLayout$DragEdge:[I

    invoke-virtual {p3}, Lcom/daimajia/swipe/SwipeLayout$DragEdge;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_84

    goto :goto_34

    .line 429
    :pswitch_1c
    if-le p6, v2, :cond_34

    if-gt p6, v3, :cond_34

    .line 430
    const/4 v0, 0x1

    return v0

    .line 434
    :pswitch_22
    if-ge p4, v3, :cond_34

    if-lt p4, v2, :cond_34

    .line 435
    const/4 v0, 0x1

    return v0

    .line 439
    :pswitch_28
    if-lt p5, v4, :cond_34

    if-ge p5, v5, :cond_34

    .line 440
    const/4 v0, 0x1

    return v0

    .line 444
    :pswitch_2e
    if-le p7, v4, :cond_34

    if-gt p7, v5, :cond_34

    .line 445
    const/4 v0, 0x1

    return v0

    .line 447
    :cond_34
    :goto_34
    goto/16 :goto_82

    .line 449
    :cond_36
    invoke-virtual {p0}, Lcom/daimajia/swipe/SwipeLayout;->getShowMode()Lcom/daimajia/swipe/SwipeLayout$ShowMode;

    move-result-object v0

    sget-object v1, Lcom/daimajia/swipe/SwipeLayout$ShowMode;->PullOut:Lcom/daimajia/swipe/SwipeLayout$ShowMode;

    if-ne v0, v1, :cond_82

    .line 450
    sget-object v0, Lcom/daimajia/swipe/SwipeLayout$4;->$SwitchMap$com$daimajia$swipe$SwipeLayout$DragEdge:[I

    invoke-virtual {p3}, Lcom/daimajia/swipe/SwipeLayout$DragEdge;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_90

    goto :goto_82

    .line 452
    :pswitch_4a
    invoke-virtual {p0}, Lcom/daimajia/swipe/SwipeLayout;->getWidth()I

    move-result v0

    if-gt v2, v0, :cond_82

    invoke-virtual {p0}, Lcom/daimajia/swipe/SwipeLayout;->getWidth()I

    move-result v0

    if-le v3, v0, :cond_82

    const/4 v0, 0x1

    return v0

    .line 455
    :pswitch_58
    invoke-virtual {p0}, Lcom/daimajia/swipe/SwipeLayout;->getPaddingLeft()I

    move-result v0

    if-lt v3, v0, :cond_82

    invoke-virtual {p0}, Lcom/daimajia/swipe/SwipeLayout;->getPaddingLeft()I

    move-result v0

    if-ge v2, v0, :cond_82

    const/4 v0, 0x1

    return v0

    .line 458
    :pswitch_66
    invoke-virtual {p0}, Lcom/daimajia/swipe/SwipeLayout;->getPaddingTop()I

    move-result v0

    if-ge v4, v0, :cond_82

    invoke-virtual {p0}, Lcom/daimajia/swipe/SwipeLayout;->getPaddingTop()I

    move-result v0

    if-lt v5, v0, :cond_82

    const/4 v0, 0x1

    return v0

    .line 461
    :pswitch_74
    invoke-virtual {p0}, Lcom/daimajia/swipe/SwipeLayout;->getHeight()I

    move-result v0

    if-ge v4, v0, :cond_82

    invoke-virtual {p0}, Lcom/daimajia/swipe/SwipeLayout;->getPaddingTop()I

    move-result v0

    if-lt v4, v0, :cond_82

    const/4 v0, 0x1

    return v0

    .line 465
    :cond_82
    :goto_82
    const/4 v0, 0x0

    return v0

    :pswitch_data_84
    .packed-switch 0x1
        :pswitch_28
        :pswitch_2e
        :pswitch_22
        :pswitch_1c
    .end packed-switch

    :pswitch_data_90
    .packed-switch 0x1
        :pswitch_66
        :pswitch_74
        :pswitch_58
        :pswitch_4a
    .end packed-switch
.end method

.method protected isViewTotallyFirstShowed(Landroid/view/View;Landroid/graphics/Rect;Lcom/daimajia/swipe/SwipeLayout$DragEdge;IIII)Z
    .registers 15
    .param p1, "child"    # Landroid/view/View;
    .param p2, "relativePosition"    # Landroid/graphics/Rect;
    .param p3, "edge"    # Lcom/daimajia/swipe/SwipeLayout$DragEdge;
    .param p4, "surfaceLeft"    # I
    .param p5, "surfaceTop"    # I
    .param p6, "surfaceRight"    # I
    .param p7, "surfaceBottom"    # I

    .line 400
    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout;->mShowEntirely:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_10

    const/4 v0, 0x0

    return v0

    .line 401
    :cond_10
    iget v2, p2, Landroid/graphics/Rect;->left:I

    .line 402
    .local v2, "childLeft":I
    iget v3, p2, Landroid/graphics/Rect;->right:I

    .line 403
    .local v3, "childRight":I
    iget v4, p2, Landroid/graphics/Rect;->top:I

    .line 404
    .local v4, "childTop":I
    iget v5, p2, Landroid/graphics/Rect;->bottom:I

    .line 405
    .local v5, "childBottom":I
    const/4 v6, 0x0

    .line 406
    .local v6, "r":Z
    invoke-virtual {p0}, Lcom/daimajia/swipe/SwipeLayout;->getShowMode()Lcom/daimajia/swipe/SwipeLayout$ShowMode;

    move-result-object v0

    sget-object v1, Lcom/daimajia/swipe/SwipeLayout$ShowMode;->LayDown:Lcom/daimajia/swipe/SwipeLayout$ShowMode;

    if-ne v0, v1, :cond_3b

    .line 407
    sget-object v0, Lcom/daimajia/swipe/SwipeLayout$DragEdge;->Right:Lcom/daimajia/swipe/SwipeLayout$DragEdge;

    if-ne p3, v0, :cond_27

    if-le p6, v2, :cond_39

    :cond_27
    sget-object v0, Lcom/daimajia/swipe/SwipeLayout$DragEdge;->Left:Lcom/daimajia/swipe/SwipeLayout$DragEdge;

    if-ne p3, v0, :cond_2d

    if-ge p4, v3, :cond_39

    :cond_2d
    sget-object v0, Lcom/daimajia/swipe/SwipeLayout$DragEdge;->Top:Lcom/daimajia/swipe/SwipeLayout$DragEdge;

    if-ne p3, v0, :cond_33

    if-ge p5, v5, :cond_39

    :cond_33
    sget-object v0, Lcom/daimajia/swipe/SwipeLayout$DragEdge;->Bottom:Lcom/daimajia/swipe/SwipeLayout$DragEdge;

    if-ne p3, v0, :cond_6c

    if-gt p7, v4, :cond_6c

    .line 410
    :cond_39
    const/4 v6, 0x1

    goto :goto_6c

    .line 411
    :cond_3b
    invoke-virtual {p0}, Lcom/daimajia/swipe/SwipeLayout;->getShowMode()Lcom/daimajia/swipe/SwipeLayout$ShowMode;

    move-result-object v0

    sget-object v1, Lcom/daimajia/swipe/SwipeLayout$ShowMode;->PullOut:Lcom/daimajia/swipe/SwipeLayout$ShowMode;

    if-ne v0, v1, :cond_6c

    .line 412
    sget-object v0, Lcom/daimajia/swipe/SwipeLayout$DragEdge;->Right:Lcom/daimajia/swipe/SwipeLayout$DragEdge;

    if-ne p3, v0, :cond_4d

    invoke-virtual {p0}, Lcom/daimajia/swipe/SwipeLayout;->getWidth()I

    move-result v0

    if-le v3, v0, :cond_6b

    :cond_4d
    sget-object v0, Lcom/daimajia/swipe/SwipeLayout$DragEdge;->Left:Lcom/daimajia/swipe/SwipeLayout$DragEdge;

    if-ne p3, v0, :cond_57

    invoke-virtual {p0}, Lcom/daimajia/swipe/SwipeLayout;->getPaddingLeft()I

    move-result v0

    if-ge v2, v0, :cond_6b

    :cond_57
    sget-object v0, Lcom/daimajia/swipe/SwipeLayout$DragEdge;->Top:Lcom/daimajia/swipe/SwipeLayout$DragEdge;

    if-ne p3, v0, :cond_61

    invoke-virtual {p0}, Lcom/daimajia/swipe/SwipeLayout;->getPaddingTop()I

    move-result v0

    if-ge v4, v0, :cond_6b

    :cond_61
    sget-object v0, Lcom/daimajia/swipe/SwipeLayout$DragEdge;->Bottom:Lcom/daimajia/swipe/SwipeLayout$DragEdge;

    if-ne p3, v0, :cond_6c

    invoke-virtual {p0}, Lcom/daimajia/swipe/SwipeLayout;->getHeight()I

    move-result v0

    if-gt v5, v0, :cond_6c

    .line 415
    :cond_6b
    const/4 v6, 0x1

    .line 417
    :cond_6c
    :goto_6c
    return v6
.end method

.method layoutLayDown()V
    .registers 8

    .line 743
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/daimajia/swipe/SwipeLayout;->computeSurfaceLayoutArea(Z)Landroid/graphics/Rect;

    move-result-object v4

    .line 744
    .local v4, "rect":Landroid/graphics/Rect;
    invoke-virtual {p0}, Lcom/daimajia/swipe/SwipeLayout;->getSurfaceView()Landroid/view/View;

    move-result-object v5

    .line 745
    .local v5, "surfaceView":Landroid/view/View;
    if-eqz v5, :cond_19

    .line 746
    iget v0, v4, Landroid/graphics/Rect;->left:I

    iget v1, v4, Landroid/graphics/Rect;->top:I

    iget v2, v4, Landroid/graphics/Rect;->right:I

    iget v3, v4, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v5, v0, v1, v2, v3}, Landroid/view/View;->layout(IIII)V

    .line 747
    invoke-virtual {p0, v5}, Lcom/daimajia/swipe/SwipeLayout;->bringChildToFront(Landroid/view/View;)V

    .line 749
    :cond_19
    sget-object v0, Lcom/daimajia/swipe/SwipeLayout$ShowMode;->LayDown:Lcom/daimajia/swipe/SwipeLayout$ShowMode;

    invoke-direct {p0, v0, v4}, Lcom/daimajia/swipe/SwipeLayout;->computeBottomLayoutAreaViaSurface(Lcom/daimajia/swipe/SwipeLayout$ShowMode;Landroid/graphics/Rect;)Landroid/graphics/Rect;

    move-result-object v4

    .line 750
    invoke-virtual {p0}, Lcom/daimajia/swipe/SwipeLayout;->getCurrentBottomView()Landroid/view/View;

    move-result-object v6

    .line 751
    .local v6, "currentBottomView":Landroid/view/View;
    if-eqz v6, :cond_30

    .line 752
    iget v0, v4, Landroid/graphics/Rect;->left:I

    iget v1, v4, Landroid/graphics/Rect;->top:I

    iget v2, v4, Landroid/graphics/Rect;->right:I

    iget v3, v4, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v6, v0, v1, v2, v3}, Landroid/view/View;->layout(IIII)V

    .line 754
    :cond_30
    return-void
.end method

.method layoutPullOut()V
    .registers 8

    .line 729
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/daimajia/swipe/SwipeLayout;->computeSurfaceLayoutArea(Z)Landroid/graphics/Rect;

    move-result-object v4

    .line 730
    .local v4, "rect":Landroid/graphics/Rect;
    invoke-virtual {p0}, Lcom/daimajia/swipe/SwipeLayout;->getSurfaceView()Landroid/view/View;

    move-result-object v5

    .line 731
    .local v5, "surfaceView":Landroid/view/View;
    if-eqz v5, :cond_19

    .line 732
    iget v0, v4, Landroid/graphics/Rect;->left:I

    iget v1, v4, Landroid/graphics/Rect;->top:I

    iget v2, v4, Landroid/graphics/Rect;->right:I

    iget v3, v4, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v5, v0, v1, v2, v3}, Landroid/view/View;->layout(IIII)V

    .line 733
    invoke-virtual {p0, v5}, Lcom/daimajia/swipe/SwipeLayout;->bringChildToFront(Landroid/view/View;)V

    .line 735
    :cond_19
    sget-object v0, Lcom/daimajia/swipe/SwipeLayout$ShowMode;->PullOut:Lcom/daimajia/swipe/SwipeLayout$ShowMode;

    invoke-direct {p0, v0, v4}, Lcom/daimajia/swipe/SwipeLayout;->computeBottomLayoutAreaViaSurface(Lcom/daimajia/swipe/SwipeLayout$ShowMode;Landroid/graphics/Rect;)Landroid/graphics/Rect;

    move-result-object v4

    .line 736
    invoke-virtual {p0}, Lcom/daimajia/swipe/SwipeLayout;->getCurrentBottomView()Landroid/view/View;

    move-result-object v6

    .line 737
    .local v6, "currentBottomView":Landroid/view/View;
    if-eqz v6, :cond_30

    .line 738
    iget v0, v4, Landroid/graphics/Rect;->left:I

    iget v1, v4, Landroid/graphics/Rect;->top:I

    iget v2, v4, Landroid/graphics/Rect;->right:I

    iget v3, v4, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v6, v0, v1, v2, v3}, Landroid/view/View;->layout(IIII)V

    .line 740
    :cond_30
    return-void
.end method

.method protected onAttachedToWindow()V
    .registers 2

    .line 1027
    invoke-super {p0}, Landroid/widget/FrameLayout;->onAttachedToWindow()V

    .line 1028
    invoke-direct {p0}, Lcom/daimajia/swipe/SwipeLayout;->insideAdapterView()Z

    move-result v0

    if-eqz v0, :cond_21

    .line 1029
    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout;->clickListener:Landroid/view/View$OnClickListener;

    if-nez v0, :cond_15

    .line 1030
    new-instance v0, Lcom/daimajia/swipe/SwipeLayout$2;

    invoke-direct {v0, p0}, Lcom/daimajia/swipe/SwipeLayout$2;-><init>(Lcom/daimajia/swipe/SwipeLayout;)V

    invoke-virtual {p0, v0}, Lcom/daimajia/swipe/SwipeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1037
    :cond_15
    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout;->longClickListener:Landroid/view/View$OnLongClickListener;

    if-nez v0, :cond_21

    .line 1038
    new-instance v0, Lcom/daimajia/swipe/SwipeLayout$3;

    invoke-direct {v0, p0}, Lcom/daimajia/swipe/SwipeLayout$3;-><init>(Lcom/daimajia/swipe/SwipeLayout;)V

    invoke-virtual {p0, v0}, Lcom/daimajia/swipe/SwipeLayout;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 1047
    :cond_21
    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 6
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .line 831
    invoke-virtual {p0}, Lcom/daimajia/swipe/SwipeLayout;->isSwipeEnabled()Z

    move-result v0

    if-nez v0, :cond_8

    .line 832
    const/4 v0, 0x0

    return v0

    .line 834
    :cond_8
    iget-boolean v0, p0, Lcom/daimajia/swipe/SwipeLayout;->mClickToClose:Z

    if-eqz v0, :cond_1c

    invoke-virtual {p0}, Lcom/daimajia/swipe/SwipeLayout;->getOpenStatus()Lcom/daimajia/swipe/SwipeLayout$Status;

    move-result-object v0

    sget-object v1, Lcom/daimajia/swipe/SwipeLayout$Status;->Open:Lcom/daimajia/swipe/SwipeLayout$Status;

    if-ne v0, v1, :cond_1c

    invoke-direct {p0, p1}, Lcom/daimajia/swipe/SwipeLayout;->isTouchOnSurface(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 835
    const/4 v0, 0x1

    return v0

    .line 837
    :cond_1c
    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout;->mSwipeDeniers:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_22
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3a

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/daimajia/swipe/SwipeLayout$SwipeDenier;

    .line 838
    .local v3, "denier":Lcom/daimajia/swipe/SwipeLayout$SwipeDenier;
    if-eqz v3, :cond_39

    invoke-interface {v3, p1}, Lcom/daimajia/swipe/SwipeLayout$SwipeDenier;->shouldDenySwipe(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_39

    .line 839
    const/4 v0, 0x0

    return v0

    .line 841
    .end local v3    # "denier":Lcom/daimajia/swipe/SwipeLayout$SwipeDenier;
    :cond_39
    goto :goto_22

    .line 843
    .end local v2    # "i$":Ljava/util/Iterator;
    :cond_3a
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_90

    goto/16 :goto_87

    .line 845
    :pswitch_43
    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout;->mDragHelper:Landroid/support/v4/widget/ViewDragHelper;

    invoke-virtual {v0, p1}, Landroid/support/v4/widget/ViewDragHelper;->processTouchEvent(Landroid/view/MotionEvent;)V

    .line 846
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/daimajia/swipe/SwipeLayout;->mIsBeingDragged:Z

    .line 847
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v0

    iput v0, p0, Lcom/daimajia/swipe/SwipeLayout;->sX:F

    .line 848
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v0

    iput v0, p0, Lcom/daimajia/swipe/SwipeLayout;->sY:F

    .line 850
    invoke-virtual {p0}, Lcom/daimajia/swipe/SwipeLayout;->getOpenStatus()Lcom/daimajia/swipe/SwipeLayout$Status;

    move-result-object v0

    sget-object v1, Lcom/daimajia/swipe/SwipeLayout$Status;->Middle:Lcom/daimajia/swipe/SwipeLayout$Status;

    if-ne v0, v1, :cond_8c

    .line 851
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/daimajia/swipe/SwipeLayout;->mIsBeingDragged:Z

    goto :goto_8c

    .line 855
    :pswitch_63
    iget-boolean v2, p0, Lcom/daimajia/swipe/SwipeLayout;->mIsBeingDragged:Z

    .line 856
    .local v2, "beforeCheck":Z
    invoke-direct {p0, p1}, Lcom/daimajia/swipe/SwipeLayout;->checkCanDrag(Landroid/view/MotionEvent;)V

    .line 857
    iget-boolean v0, p0, Lcom/daimajia/swipe/SwipeLayout;->mIsBeingDragged:Z

    if-eqz v0, :cond_76

    .line 858
    invoke-virtual {p0}, Lcom/daimajia/swipe/SwipeLayout;->getParent()Landroid/view/ViewParent;

    move-result-object v3

    .line 859
    .local v3, "parent":Landroid/view/ViewParent;
    if-eqz v3, :cond_76

    .line 860
    const/4 v0, 0x1

    invoke-interface {v3, v0}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    .line 863
    .end local v3    # "parent":Landroid/view/ViewParent;
    :cond_76
    if-nez v2, :cond_8c

    iget-boolean v0, p0, Lcom/daimajia/swipe/SwipeLayout;->mIsBeingDragged:Z

    if-eqz v0, :cond_8c

    .line 866
    const/4 v0, 0x0

    return v0

    .line 872
    :pswitch_7e
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/daimajia/swipe/SwipeLayout;->mIsBeingDragged:Z

    .line 873
    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout;->mDragHelper:Landroid/support/v4/widget/ViewDragHelper;

    invoke-virtual {v0, p1}, Landroid/support/v4/widget/ViewDragHelper;->processTouchEvent(Landroid/view/MotionEvent;)V

    .line 874
    goto :goto_8c

    .line 876
    :goto_87
    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout;->mDragHelper:Landroid/support/v4/widget/ViewDragHelper;

    invoke-virtual {v0, p1}, Landroid/support/v4/widget/ViewDragHelper;->processTouchEvent(Landroid/view/MotionEvent;)V

    .line 878
    .end local v2    # "beforeCheck":Z
    :cond_8c
    :goto_8c
    iget-boolean v0, p0, Lcom/daimajia/swipe/SwipeLayout;->mIsBeingDragged:Z

    return v0

    nop

    :pswitch_data_90
    .packed-switch 0x0
        :pswitch_43
        :pswitch_7e
        :pswitch_63
        :pswitch_7e
    .end packed-switch
.end method

.method protected onLayout(ZIIII)V
    .registers 8
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .line 721
    invoke-direct {p0}, Lcom/daimajia/swipe/SwipeLayout;->updateBottomViews()V

    .line 723
    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout;->mOnLayoutListeners:Ljava/util/List;

    if-eqz v0, :cond_1e

    const/4 v1, 0x0

    .local v1, "i":I
    :goto_8
    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout;->mOnLayoutListeners:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1e

    .line 724
    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout;->mOnLayoutListeners:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/daimajia/swipe/SwipeLayout$OnLayout;

    invoke-interface {v0, p0}, Lcom/daimajia/swipe/SwipeLayout$OnLayout;->onLayout(Lcom/daimajia/swipe/SwipeLayout;)V

    .line 723
    add-int/lit8 v1, v1, 0x1

    goto :goto_8

    .line 726
    .end local v1    # "i":I
    :cond_1e
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 5
    .param p1, "event"    # Landroid/view/MotionEvent;

    .line 885
    invoke-virtual {p0}, Lcom/daimajia/swipe/SwipeLayout;->isSwipeEnabled()Z

    move-result v0

    if-nez v0, :cond_b

    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0

    .line 887
    :cond_b
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v2

    .line 888
    .local v2, "action":I
    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout;->gestureDetector:Landroid/view/GestureDetector;

    invoke-virtual {v0, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 890
    packed-switch v2, :pswitch_data_5c

    goto :goto_47

    .line 892
    :pswitch_18
    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout;->mDragHelper:Landroid/support/v4/widget/ViewDragHelper;

    invoke-virtual {v0, p1}, Landroid/support/v4/widget/ViewDragHelper;->processTouchEvent(Landroid/view/MotionEvent;)V

    .line 893
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v0

    iput v0, p0, Lcom/daimajia/swipe/SwipeLayout;->sX:F

    .line 894
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v0

    iput v0, p0, Lcom/daimajia/swipe/SwipeLayout;->sY:F

    .line 899
    :pswitch_29
    invoke-direct {p0, p1}, Lcom/daimajia/swipe/SwipeLayout;->checkCanDrag(Landroid/view/MotionEvent;)V

    .line 900
    iget-boolean v0, p0, Lcom/daimajia/swipe/SwipeLayout;->mIsBeingDragged:Z

    if-eqz v0, :cond_4c

    .line 901
    invoke-virtual {p0}, Lcom/daimajia/swipe/SwipeLayout;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    .line 902
    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout;->mDragHelper:Landroid/support/v4/widget/ViewDragHelper;

    invoke-virtual {v0, p1}, Landroid/support/v4/widget/ViewDragHelper;->processTouchEvent(Landroid/view/MotionEvent;)V

    goto :goto_4c

    .line 908
    :pswitch_3e
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/daimajia/swipe/SwipeLayout;->mIsBeingDragged:Z

    .line 909
    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout;->mDragHelper:Landroid/support/v4/widget/ViewDragHelper;

    invoke-virtual {v0, p1}, Landroid/support/v4/widget/ViewDragHelper;->processTouchEvent(Landroid/view/MotionEvent;)V

    .line 910
    goto :goto_4c

    .line 913
    :goto_47
    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout;->mDragHelper:Landroid/support/v4/widget/ViewDragHelper;

    invoke-virtual {v0, p1}, Landroid/support/v4/widget/ViewDragHelper;->processTouchEvent(Landroid/view/MotionEvent;)V

    .line 916
    :cond_4c
    :goto_4c
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    if-nez v0, :cond_58

    iget-boolean v0, p0, Lcom/daimajia/swipe/SwipeLayout;->mIsBeingDragged:Z

    if-nez v0, :cond_58

    if-nez v2, :cond_5a

    :cond_58
    const/4 v0, 0x1

    goto :goto_5b

    :cond_5a
    const/4 v0, 0x0

    :goto_5b
    return v0

    :pswitch_data_5c
    .packed-switch 0x0
        :pswitch_18
        :pswitch_3e
        :pswitch_29
        :pswitch_3e
    .end packed-switch
.end method

.method protected onViewRemoved(Landroid/view/View;)V
    .registers 6
    .param p1, "child"    # Landroid/view/View;

    .line 1442
    new-instance v0, Ljava/util/HashMap;

    iget-object v1, p0, Lcom/daimajia/swipe/SwipeLayout;->mDragEdges:Ljava/util/LinkedHashMap;

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_f
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2c

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Ljava/util/Map$Entry;

    .line 1443
    .local v3, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lcom/daimajia/swipe/SwipeLayout$DragEdge;Landroid/view/View;>;"
    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    if-ne v0, p1, :cond_2b

    .line 1444
    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout;->mDragEdges:Ljava/util/LinkedHashMap;

    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/LinkedHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1446
    .end local v3    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lcom/daimajia/swipe/SwipeLayout$DragEdge;Landroid/view/View;>;"
    .end local v3
    :cond_2b
    goto :goto_f

    .line 1447
    .end local v2    # "i$":Ljava/util/Iterator;
    :cond_2c
    return-void
.end method

.method public open()V
    .registers 3

    .line 1246
    const/4 v0, 0x1

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/daimajia/swipe/SwipeLayout;->open(ZZ)V

    .line 1247
    return-void
.end method

.method public open(ZZ)V
    .registers 13
    .param p1, "smooth"    # Z
    .param p2, "notify"    # Z

    .line 1254
    invoke-virtual {p0}, Lcom/daimajia/swipe/SwipeLayout;->getSurfaceView()Landroid/view/View;

    move-result-object v4

    .local v4, "surface":Landroid/view/View;
    invoke-virtual {p0}, Lcom/daimajia/swipe/SwipeLayout;->getCurrentBottomView()Landroid/view/View;

    move-result-object v5

    .line 1255
    .local v5, "bottom":Landroid/view/View;
    if-nez v4, :cond_b

    .line 1256
    return-void

    .line 1259
    :cond_b
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/daimajia/swipe/SwipeLayout;->computeSurfaceLayoutArea(Z)Landroid/graphics/Rect;

    move-result-object v8

    .line 1260
    .local v8, "rect":Landroid/graphics/Rect;
    if-eqz p1, :cond_1d

    .line 1261
    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout;->mDragHelper:Landroid/support/v4/widget/ViewDragHelper;

    iget v1, v8, Landroid/graphics/Rect;->left:I

    iget v2, v8, Landroid/graphics/Rect;->top:I

    invoke-virtual {v0, v4, v1, v2}, Landroid/support/v4/widget/ViewDragHelper;->smoothSlideViewTo(Landroid/view/View;II)Z

    goto/16 :goto_6b

    .line 1263
    :cond_1d
    iget v0, v8, Landroid/graphics/Rect;->left:I

    invoke-virtual {v4}, Landroid/view/View;->getLeft()I

    move-result v1

    sub-int v6, v0, v1

    .line 1264
    .local v6, "dx":I
    iget v0, v8, Landroid/graphics/Rect;->top:I

    invoke-virtual {v4}, Landroid/view/View;->getTop()I

    move-result v1

    sub-int v7, v0, v1

    .line 1265
    .local v7, "dy":I
    iget v0, v8, Landroid/graphics/Rect;->left:I

    iget v1, v8, Landroid/graphics/Rect;->top:I

    iget v2, v8, Landroid/graphics/Rect;->right:I

    iget v3, v8, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v4, v0, v1, v2, v3}, Landroid/view/View;->layout(IIII)V

    .line 1266
    invoke-virtual {p0}, Lcom/daimajia/swipe/SwipeLayout;->getShowMode()Lcom/daimajia/swipe/SwipeLayout$ShowMode;

    move-result-object v0

    sget-object v1, Lcom/daimajia/swipe/SwipeLayout$ShowMode;->PullOut:Lcom/daimajia/swipe/SwipeLayout$ShowMode;

    if-ne v0, v1, :cond_53

    .line 1267
    sget-object v0, Lcom/daimajia/swipe/SwipeLayout$ShowMode;->PullOut:Lcom/daimajia/swipe/SwipeLayout$ShowMode;

    invoke-direct {p0, v0, v8}, Lcom/daimajia/swipe/SwipeLayout;->computeBottomLayoutAreaViaSurface(Lcom/daimajia/swipe/SwipeLayout$ShowMode;Landroid/graphics/Rect;)Landroid/graphics/Rect;

    move-result-object v9

    .line 1268
    .local v9, "bRect":Landroid/graphics/Rect;
    if-eqz v5, :cond_53

    .line 1269
    iget v0, v9, Landroid/graphics/Rect;->left:I

    iget v1, v9, Landroid/graphics/Rect;->top:I

    iget v2, v9, Landroid/graphics/Rect;->right:I

    iget v3, v9, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v5, v0, v1, v2, v3}, Landroid/view/View;->layout(IIII)V

    .line 1272
    .end local v9    # "bRect":Landroid/graphics/Rect;
    :cond_53
    if-eqz p2, :cond_68

    .line 1273
    iget v0, v8, Landroid/graphics/Rect;->left:I

    iget v1, v8, Landroid/graphics/Rect;->top:I

    iget v2, v8, Landroid/graphics/Rect;->right:I

    iget v3, v8, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/daimajia/swipe/SwipeLayout;->dispatchRevealEvent(IIII)V

    .line 1274
    iget v0, v8, Landroid/graphics/Rect;->left:I

    iget v1, v8, Landroid/graphics/Rect;->top:I

    invoke-virtual {p0, v0, v1, v6, v7}, Lcom/daimajia/swipe/SwipeLayout;->dispatchSwipeEvent(IIII)V

    goto :goto_6b

    .line 1276
    :cond_68
    invoke-direct {p0}, Lcom/daimajia/swipe/SwipeLayout;->safeBottomView()V

    .line 1279
    .end local v6    # "dx":I
    .end local v7    # "dy":I
    :goto_6b
    invoke-virtual {p0}, Lcom/daimajia/swipe/SwipeLayout;->invalidate()V

    .line 1280
    return-void
.end method

.method protected processHandRelease(FFZ)V
    .registers 11
    .param p1, "xvel"    # F
    .param p2, "yvel"    # F
    .param p3, "isCloseBeforeDragged"    # Z

    .line 1200
    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout;->mDragHelper:Landroid/support/v4/widget/ViewDragHelper;

    invoke-virtual {v0}, Landroid/support/v4/widget/ViewDragHelper;->getMinVelocity()F

    move-result v2

    .line 1201
    .local v2, "minVelocity":F
    invoke-virtual {p0}, Lcom/daimajia/swipe/SwipeLayout;->getSurfaceView()Landroid/view/View;

    move-result-object v3

    .line 1202
    .local v3, "surfaceView":Landroid/view/View;
    iget-object v4, p0, Lcom/daimajia/swipe/SwipeLayout;->mCurrentDragEdge:Lcom/daimajia/swipe/SwipeLayout$DragEdge;

    .line 1203
    .local v4, "currentDragEdge":Lcom/daimajia/swipe/SwipeLayout$DragEdge;
    if-eqz v4, :cond_10

    if-nez v3, :cond_11

    .line 1204
    :cond_10
    return-void

    .line 1206
    :cond_11
    if-eqz p3, :cond_16

    const/high16 v5, 0x3e800000    # 0.25f

    goto :goto_18

    :cond_16
    const/high16 v5, 0x3f400000    # 0.75f

    .line 1207
    .local v5, "willOpenPercent":F
    :goto_18
    sget-object v0, Lcom/daimajia/swipe/SwipeLayout$DragEdge;->Left:Lcom/daimajia/swipe/SwipeLayout$DragEdge;

    if-ne v4, v0, :cond_4d

    .line 1208
    cmpl-float v0, p1, v2

    if-lez v0, :cond_25

    invoke-virtual {p0}, Lcom/daimajia/swipe/SwipeLayout;->open()V

    goto/16 :goto_e8

    .line 1209
    :cond_25
    neg-float v0, v2

    cmpg-float v0, p1, v0

    if-gez v0, :cond_2f

    invoke-virtual {p0}, Lcom/daimajia/swipe/SwipeLayout;->close()V

    goto/16 :goto_e8

    .line 1211
    :cond_2f
    invoke-virtual {p0}, Lcom/daimajia/swipe/SwipeLayout;->getSurfaceView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v0

    int-to-float v0, v0

    const/high16 v1, 0x3f800000    # 1.0f

    mul-float/2addr v0, v1

    iget v1, p0, Lcom/daimajia/swipe/SwipeLayout;->mDragDistance:I

    int-to-float v1, v1

    div-float v6, v0, v1

    .line 1212
    .local v6, "openPercent":F
    cmpl-float v0, v6, v5

    if-lez v0, :cond_48

    invoke-virtual {p0}, Lcom/daimajia/swipe/SwipeLayout;->open()V

    goto :goto_4b

    .line 1213
    :cond_48
    invoke-virtual {p0}, Lcom/daimajia/swipe/SwipeLayout;->close()V

    .line 1214
    .end local v6    # "openPercent":F
    :goto_4b
    goto/16 :goto_e8

    .line 1215
    :cond_4d
    sget-object v0, Lcom/daimajia/swipe/SwipeLayout$DragEdge;->Right:Lcom/daimajia/swipe/SwipeLayout$DragEdge;

    if-ne v4, v0, :cond_83

    .line 1216
    cmpl-float v0, p1, v2

    if-lez v0, :cond_5a

    invoke-virtual {p0}, Lcom/daimajia/swipe/SwipeLayout;->close()V

    goto/16 :goto_e8

    .line 1217
    :cond_5a
    neg-float v0, v2

    cmpg-float v0, p1, v0

    if-gez v0, :cond_64

    invoke-virtual {p0}, Lcom/daimajia/swipe/SwipeLayout;->open()V

    goto/16 :goto_e8

    .line 1219
    :cond_64
    invoke-virtual {p0}, Lcom/daimajia/swipe/SwipeLayout;->getSurfaceView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v0

    neg-int v0, v0

    int-to-float v0, v0

    const/high16 v1, 0x3f800000    # 1.0f

    mul-float/2addr v0, v1

    iget v1, p0, Lcom/daimajia/swipe/SwipeLayout;->mDragDistance:I

    int-to-float v1, v1

    div-float v6, v0, v1

    .line 1220
    .local v6, "openPercent":F
    cmpl-float v0, v6, v5

    if-lez v0, :cond_7e

    invoke-virtual {p0}, Lcom/daimajia/swipe/SwipeLayout;->open()V

    goto :goto_81

    .line 1221
    :cond_7e
    invoke-virtual {p0}, Lcom/daimajia/swipe/SwipeLayout;->close()V

    .line 1222
    .end local v6    # "openPercent":F
    :goto_81
    goto/16 :goto_e8

    .line 1223
    :cond_83
    sget-object v0, Lcom/daimajia/swipe/SwipeLayout$DragEdge;->Top:Lcom/daimajia/swipe/SwipeLayout$DragEdge;

    if-ne v4, v0, :cond_b6

    .line 1224
    cmpl-float v0, p2, v2

    if-lez v0, :cond_90

    invoke-virtual {p0}, Lcom/daimajia/swipe/SwipeLayout;->open()V

    goto/16 :goto_e8

    .line 1225
    :cond_90
    neg-float v0, v2

    cmpg-float v0, p2, v0

    if-gez v0, :cond_99

    invoke-virtual {p0}, Lcom/daimajia/swipe/SwipeLayout;->close()V

    goto :goto_e8

    .line 1227
    :cond_99
    invoke-virtual {p0}, Lcom/daimajia/swipe/SwipeLayout;->getSurfaceView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v0

    int-to-float v0, v0

    const/high16 v1, 0x3f800000    # 1.0f

    mul-float/2addr v0, v1

    iget v1, p0, Lcom/daimajia/swipe/SwipeLayout;->mDragDistance:I

    int-to-float v1, v1

    div-float v6, v0, v1

    .line 1228
    .local v6, "openPercent":F
    cmpl-float v0, v6, v5

    if-lez v0, :cond_b2

    invoke-virtual {p0}, Lcom/daimajia/swipe/SwipeLayout;->open()V

    goto :goto_b5

    .line 1229
    :cond_b2
    invoke-virtual {p0}, Lcom/daimajia/swipe/SwipeLayout;->close()V

    .line 1230
    .end local v6    # "openPercent":F
    :goto_b5
    goto :goto_e8

    .line 1231
    :cond_b6
    sget-object v0, Lcom/daimajia/swipe/SwipeLayout$DragEdge;->Bottom:Lcom/daimajia/swipe/SwipeLayout$DragEdge;

    if-ne v4, v0, :cond_e8

    .line 1232
    cmpl-float v0, p2, v2

    if-lez v0, :cond_c2

    invoke-virtual {p0}, Lcom/daimajia/swipe/SwipeLayout;->close()V

    goto :goto_e8

    .line 1233
    :cond_c2
    neg-float v0, v2

    cmpg-float v0, p2, v0

    if-gez v0, :cond_cb

    invoke-virtual {p0}, Lcom/daimajia/swipe/SwipeLayout;->open()V

    goto :goto_e8

    .line 1235
    :cond_cb
    invoke-virtual {p0}, Lcom/daimajia/swipe/SwipeLayout;->getSurfaceView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v0

    neg-int v0, v0

    int-to-float v0, v0

    const/high16 v1, 0x3f800000    # 1.0f

    mul-float/2addr v0, v1

    iget v1, p0, Lcom/daimajia/swipe/SwipeLayout;->mDragDistance:I

    int-to-float v1, v1

    div-float v6, v0, v1

    .line 1236
    .local v6, "openPercent":F
    cmpl-float v0, v6, v5

    if-lez v0, :cond_e5

    invoke-virtual {p0}, Lcom/daimajia/swipe/SwipeLayout;->open()V

    goto :goto_e8

    .line 1237
    :cond_e5
    invoke-virtual {p0}, Lcom/daimajia/swipe/SwipeLayout;->close()V

    .line 1240
    .end local v6    # "openPercent":F
    :cond_e8
    :goto_e8
    return-void
.end method

.method public setBottomSwipeEnabled(Z)V
    .registers 4
    .param p1, "bottomSwipeEnabled"    # Z

    .line 971
    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout;->mSwipesEnabled:[Z

    sget-object v1, Lcom/daimajia/swipe/SwipeLayout$DragEdge;->Bottom:Lcom/daimajia/swipe/SwipeLayout$DragEdge;

    invoke-virtual {v1}, Lcom/daimajia/swipe/SwipeLayout$DragEdge;->ordinal()I

    move-result v1

    aput-boolean p1, v0, v1

    .line 972
    return-void
.end method

.method public setBottomViewIds(IIII)V
    .registers 7
    .param p1, "leftId"    # I
    .param p2, "rightId"    # I
    .param p3, "topId"    # I
    .param p4, "bottomId"    # I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 1484
    sget-object v0, Lcom/daimajia/swipe/SwipeLayout$DragEdge;->Left:Lcom/daimajia/swipe/SwipeLayout$DragEdge;

    invoke-virtual {p0, p1}, Lcom/daimajia/swipe/SwipeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/daimajia/swipe/SwipeLayout;->addDrag(Lcom/daimajia/swipe/SwipeLayout$DragEdge;Landroid/view/View;)V

    .line 1485
    sget-object v0, Lcom/daimajia/swipe/SwipeLayout$DragEdge;->Right:Lcom/daimajia/swipe/SwipeLayout$DragEdge;

    invoke-virtual {p0, p2}, Lcom/daimajia/swipe/SwipeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/daimajia/swipe/SwipeLayout;->addDrag(Lcom/daimajia/swipe/SwipeLayout$DragEdge;Landroid/view/View;)V

    .line 1486
    sget-object v0, Lcom/daimajia/swipe/SwipeLayout$DragEdge;->Top:Lcom/daimajia/swipe/SwipeLayout$DragEdge;

    invoke-virtual {p0, p3}, Lcom/daimajia/swipe/SwipeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/daimajia/swipe/SwipeLayout;->addDrag(Lcom/daimajia/swipe/SwipeLayout$DragEdge;Landroid/view/View;)V

    .line 1487
    sget-object v0, Lcom/daimajia/swipe/SwipeLayout$DragEdge;->Bottom:Lcom/daimajia/swipe/SwipeLayout$DragEdge;

    invoke-virtual {p0, p4}, Lcom/daimajia/swipe/SwipeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/daimajia/swipe/SwipeLayout;->addDrag(Lcom/daimajia/swipe/SwipeLayout$DragEdge;Landroid/view/View;)V

    .line 1488
    return-void
.end method

.method public setClickToClose(Z)V
    .registers 2
    .param p1, "mClickToClose"    # Z

    .line 923
    iput-boolean p1, p0, Lcom/daimajia/swipe/SwipeLayout;->mClickToClose:Z

    .line 924
    return-void
.end method

.method public setDragDistance(I)V
    .registers 3
    .param p1, "max"    # I

    .line 1108
    if-gez p1, :cond_3

    const/4 p1, 0x0

    .line 1109
    :cond_3
    int-to-float v0, p1

    invoke-direct {p0, v0}, Lcom/daimajia/swipe/SwipeLayout;->dp2px(F)I

    move-result v0

    iput v0, p0, Lcom/daimajia/swipe/SwipeLayout;->mDragDistance:I

    .line 1110
    invoke-virtual {p0}, Lcom/daimajia/swipe/SwipeLayout;->requestLayout()V

    .line 1111
    return-void
.end method

.method public setDragEdge(Lcom/daimajia/swipe/SwipeLayout$DragEdge;)V
    .registers 5
    .param p1, "dragEdge"    # Lcom/daimajia/swipe/SwipeLayout$DragEdge;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 1435
    invoke-virtual {p0}, Lcom/daimajia/swipe/SwipeLayout;->getChildCount()I

    move-result v0

    const/4 v1, 0x2

    if-lt v0, v1, :cond_16

    .line 1436
    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout;->mDragEdges:Ljava/util/LinkedHashMap;

    invoke-virtual {p0}, Lcom/daimajia/swipe/SwipeLayout;->getChildCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x2

    invoke-virtual {p0, v1}, Lcom/daimajia/swipe/SwipeLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1438
    :cond_16
    invoke-direct {p0, p1}, Lcom/daimajia/swipe/SwipeLayout;->setCurrentDragEdge(Lcom/daimajia/swipe/SwipeLayout$DragEdge;)V

    .line 1439
    return-void
.end method

.method public setDragEdges(Ljava/util/List;)V
    .registers 8
    .param p1, "dragEdges"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<Lcom/daimajia/swipe/SwipeLayout$DragEdge;>;)V"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 1461
    const/4 v3, 0x0

    .local v3, "i":I
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    invoke-virtual {p0}, Lcom/daimajia/swipe/SwipeLayout;->getChildCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v4

    .local v4, "size":I
    :goto_f
    if-ge v3, v4, :cond_24

    .line 1462
    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/daimajia/swipe/SwipeLayout$DragEdge;

    .line 1463
    .local v5, "dragEdge":Lcom/daimajia/swipe/SwipeLayout$DragEdge;
    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout;->mDragEdges:Ljava/util/LinkedHashMap;

    invoke-virtual {p0, v3}, Lcom/daimajia/swipe/SwipeLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v5, v1}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1461
    .end local v5    # "dragEdge":Lcom/daimajia/swipe/SwipeLayout$DragEdge;
    add-int/lit8 v3, v3, 0x1

    goto :goto_f

    .line 1465
    .end local v3    # "i":I
    .end local v4    # "size":I
    :cond_24
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-eqz v0, :cond_32

    sget-object v0, Lcom/daimajia/swipe/SwipeLayout;->DefaultDragEdge:Lcom/daimajia/swipe/SwipeLayout$DragEdge;

    invoke-interface {p1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_38

    .line 1466
    :cond_32
    sget-object v0, Lcom/daimajia/swipe/SwipeLayout;->DefaultDragEdge:Lcom/daimajia/swipe/SwipeLayout$DragEdge;

    invoke-direct {p0, v0}, Lcom/daimajia/swipe/SwipeLayout;->setCurrentDragEdge(Lcom/daimajia/swipe/SwipeLayout$DragEdge;)V

    goto :goto_42

    .line 1468
    :cond_38
    const/4 v0, 0x0

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/daimajia/swipe/SwipeLayout$DragEdge;

    invoke-direct {p0, v0}, Lcom/daimajia/swipe/SwipeLayout;->setCurrentDragEdge(Lcom/daimajia/swipe/SwipeLayout$DragEdge;)V

    .line 1470
    :goto_42
    return-void
.end method

.method public varargs setDragEdges([Lcom/daimajia/swipe/SwipeLayout$DragEdge;)V
    .registers 3
    .param p1, "mDragEdges"    # [Lcom/daimajia/swipe/SwipeLayout$DragEdge;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 1475
    invoke-static {p1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/daimajia/swipe/SwipeLayout;->setDragEdges(Ljava/util/List;)V

    .line 1476
    return-void
.end method

.method public setLeftSwipeEnabled(Z)V
    .registers 4
    .param p1, "leftSwipeEnabled"    # Z

    .line 941
    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout;->mSwipesEnabled:[Z

    sget-object v1, Lcom/daimajia/swipe/SwipeLayout$DragEdge;->Left:Lcom/daimajia/swipe/SwipeLayout$DragEdge;

    invoke-virtual {v1}, Lcom/daimajia/swipe/SwipeLayout$DragEdge;->ordinal()I

    move-result v1

    aput-boolean p1, v0, v1

    .line 942
    return-void
.end method

.method public setOnClickListener(Landroid/view/View$OnClickListener;)V
    .registers 2
    .param p1, "l"    # Landroid/view/View$OnClickListener;

    .line 1051
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1052
    iput-object p1, p0, Lcom/daimajia/swipe/SwipeLayout;->clickListener:Landroid/view/View$OnClickListener;

    .line 1053
    return-void
.end method

.method public setOnDoubleClickListener(Lcom/daimajia/swipe/SwipeLayout$DoubleClickListener;)V
    .registers 2
    .param p1, "doubleClickListener"    # Lcom/daimajia/swipe/SwipeLayout$DoubleClickListener;

    .line 1420
    iput-object p1, p0, Lcom/daimajia/swipe/SwipeLayout;->mDoubleClickListener:Lcom/daimajia/swipe/SwipeLayout$DoubleClickListener;

    .line 1421
    return-void
.end method

.method public setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V
    .registers 2
    .param p1, "l"    # Landroid/view/View$OnLongClickListener;

    .line 1057
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 1058
    iput-object p1, p0, Lcom/daimajia/swipe/SwipeLayout;->longClickListener:Landroid/view/View$OnLongClickListener;

    .line 1059
    return-void
.end method

.method public setRightSwipeEnabled(Z)V
    .registers 4
    .param p1, "rightSwipeEnabled"    # Z

    .line 951
    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout;->mSwipesEnabled:[Z

    sget-object v1, Lcom/daimajia/swipe/SwipeLayout$DragEdge;->Right:Lcom/daimajia/swipe/SwipeLayout$DragEdge;

    invoke-virtual {v1}, Lcom/daimajia/swipe/SwipeLayout$DragEdge;->ordinal()I

    move-result v1

    aput-boolean p1, v0, v1

    .line 952
    return-void
.end method

.method public setShowMode(Lcom/daimajia/swipe/SwipeLayout$ShowMode;)V
    .registers 2
    .param p1, "mode"    # Lcom/daimajia/swipe/SwipeLayout$ShowMode;

    .line 1121
    iput-object p1, p0, Lcom/daimajia/swipe/SwipeLayout;->mShowMode:Lcom/daimajia/swipe/SwipeLayout$ShowMode;

    .line 1122
    invoke-virtual {p0}, Lcom/daimajia/swipe/SwipeLayout;->requestLayout()V

    .line 1123
    return-void
.end method

.method public setSwipeEnabled(Z)V
    .registers 2
    .param p1, "enabled"    # Z

    .line 927
    iput-boolean p1, p0, Lcom/daimajia/swipe/SwipeLayout;->mSwipeEnabled:Z

    .line 928
    return-void
.end method

.method public setTopSwipeEnabled(Z)V
    .registers 4
    .param p1, "topSwipeEnabled"    # Z

    .line 961
    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout;->mSwipesEnabled:[Z

    sget-object v1, Lcom/daimajia/swipe/SwipeLayout$DragEdge;->Top:Lcom/daimajia/swipe/SwipeLayout$DragEdge;

    invoke-virtual {v1}, Lcom/daimajia/swipe/SwipeLayout$DragEdge;->ordinal()I

    move-result v1

    aput-boolean p1, v0, v1

    .line 962
    return-void
.end method

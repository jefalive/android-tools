.class public final Lcom/itau/empresas/ui/dialog/FiltroComprovantesDialog_;
.super Lcom/itau/empresas/ui/dialog/FiltroComprovantesDialog;
.source "FiltroComprovantesDialog_.java"

# interfaces
.implements Lorg/androidannotations/api/view/HasViews;
.implements Lorg/androidannotations/api/view/OnViewChangedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/itau/empresas/ui/dialog/FiltroComprovantesDialog_$FragmentBuilder_;
    }
.end annotation


# instance fields
.field private contentView_:Landroid/view/View;

.field private final onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;


# direct methods
.method public constructor <init>()V
    .registers 2

    .line 24
    invoke-direct {p0}, Lcom/itau/empresas/ui/dialog/FiltroComprovantesDialog;-><init>()V

    .line 28
    new-instance v0, Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-direct {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/ui/dialog/FiltroComprovantesDialog_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    return-void
.end method

.method private init_(Landroid/os/Bundle;)V
    .registers 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 66
    invoke-static {p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->registerOnViewChangedListener(Lorg/androidannotations/api/view/OnViewChangedListener;)V

    .line 67
    invoke-virtual {p0}, Lcom/itau/empresas/ui/dialog/FiltroComprovantesDialog_;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 68
    .local v1, "resources_":Landroid/content/res/Resources;
    const v0, 0x7f0d0001

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/ui/dialog/FiltroComprovantesDialog_;->dias:[Ljava/lang/String;

    .line 69
    invoke-direct {p0}, Lcom/itau/empresas/ui/dialog/FiltroComprovantesDialog_;->injectFragmentArguments_()V

    .line 70
    invoke-static {}, Lcom/itau/empresas/CustomApplication_;->getInstance()Lcom/itau/empresas/CustomApplication;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/ui/dialog/FiltroComprovantesDialog_;->application:Lcom/itau/empresas/CustomApplication;

    .line 72
    return-void
.end method

.method private injectFragmentArguments_()V
    .registers 3

    .line 92
    invoke-virtual {p0}, Lcom/itau/empresas/ui/dialog/FiltroComprovantesDialog_;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    .line 93
    .local v1, "args_":Landroid/os/Bundle;
    if-eqz v1, :cond_18

    .line 94
    const-string v0, "selecionado"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_18

    .line 95
    const-string v0, "selecionado"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    iput-object v0, p0, Lcom/itau/empresas/ui/dialog/FiltroComprovantesDialog_;->selecionado:Ljava/lang/Integer;

    .line 98
    :cond_18
    return-void
.end method


# virtual methods
.method public findViewById(I)Landroid/view/View;
    .registers 3
    .param p1, "id"    # I

    .line 42
    iget-object v0, p0, Lcom/itau/empresas/ui/dialog/FiltroComprovantesDialog_;->contentView_:Landroid/view/View;

    if-nez v0, :cond_6

    .line 43
    const/4 v0, 0x0

    return-object v0

    .line 45
    :cond_6
    iget-object v0, p0, Lcom/itau/empresas/ui/dialog/FiltroComprovantesDialog_;->contentView_:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 34
    iget-object v0, p0, Lcom/itau/empresas/ui/dialog/FiltroComprovantesDialog_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-static {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    move-result-object v1

    .line 35
    .local v1, "previousNotifier":Lorg/androidannotations/api/view/OnViewChangedNotifier;
    invoke-direct {p0, p1}, Lcom/itau/empresas/ui/dialog/FiltroComprovantesDialog_;->init_(Landroid/os/Bundle;)V

    .line 36
    invoke-super {p0, p1}, Lcom/itau/empresas/ui/dialog/FiltroComprovantesDialog;->onCreate(Landroid/os/Bundle;)V

    .line 37
    invoke-static {v1}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 38
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .registers 6
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .line 50
    invoke-super {p0, p1, p2, p3}, Lcom/itau/empresas/ui/dialog/FiltroComprovantesDialog;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/ui/dialog/FiltroComprovantesDialog_;->contentView_:Landroid/view/View;

    .line 51
    iget-object v0, p0, Lcom/itau/empresas/ui/dialog/FiltroComprovantesDialog_;->contentView_:Landroid/view/View;

    if-nez v0, :cond_14

    .line 52
    const v0, 0x7f03016b

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/ui/dialog/FiltroComprovantesDialog_;->contentView_:Landroid/view/View;

    .line 54
    :cond_14
    iget-object v0, p0, Lcom/itau/empresas/ui/dialog/FiltroComprovantesDialog_;->contentView_:Landroid/view/View;

    return-object v0
.end method

.method public onDestroyView()V
    .registers 2

    .line 59
    invoke-super {p0}, Lcom/itau/empresas/ui/dialog/FiltroComprovantesDialog;->onDestroyView()V

    .line 60
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/itau/empresas/ui/dialog/FiltroComprovantesDialog_;->contentView_:Landroid/view/View;

    .line 61
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/itau/empresas/ui/dialog/FiltroComprovantesDialog_;->botaoConfirmarFiltrosComprovantes:Landroid/widget/Button;

    .line 62
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/itau/empresas/ui/dialog/FiltroComprovantesDialog_;->escolhePeriodo:Lbr/com/itau/library/LinearValuePickerView;

    .line 63
    return-void
.end method

.method public onViewChanged(Lorg/androidannotations/api/view/HasViews;)V
    .registers 3
    .param p1, "hasViews"    # Lorg/androidannotations/api/view/HasViews;

    .line 86
    const v0, 0x7f0e063b

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/itau/empresas/ui/dialog/FiltroComprovantesDialog_;->botaoConfirmarFiltrosComprovantes:Landroid/widget/Button;

    .line 87
    const v0, 0x7f0e0179

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lbr/com/itau/library/LinearValuePickerView;

    iput-object v0, p0, Lcom/itau/empresas/ui/dialog/FiltroComprovantesDialog_;->escolhePeriodo:Lbr/com/itau/library/LinearValuePickerView;

    .line 88
    invoke-virtual {p0}, Lcom/itau/empresas/ui/dialog/FiltroComprovantesDialog_;->configuraViews()V

    .line 89
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .registers 4
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;

    .line 76
    invoke-super {p0, p1, p2}, Lcom/itau/empresas/ui/dialog/FiltroComprovantesDialog;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 77
    iget-object v0, p0, Lcom/itau/empresas/ui/dialog/FiltroComprovantesDialog_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 78
    return-void
.end method

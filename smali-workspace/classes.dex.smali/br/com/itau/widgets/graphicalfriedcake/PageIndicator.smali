.class public Lbr/com/itau/widgets/graphicalfriedcake/PageIndicator;
.super Landroid/widget/LinearLayout;
.source "PageIndicator.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lbr/com/itau/widgets/graphicalfriedcake/PageIndicator$Configuration;
    }
.end annotation


# instance fields
.field private configuration:Lbr/com/itau/widgets/graphicalfriedcake/PageIndicator$Configuration;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 2
    .param p1, "context"    # Landroid/content/Context;

    .line 18
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 19
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .line 22
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 23
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .line 26
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 27
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .registers 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I
    .param p4, "defStyleRes"    # I
    .annotation build Landroid/annotation/TargetApi;
        value = 0x15
    .end annotation

    .line 31
    invoke-direct {p0, p1, p2, p3, p4}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    .line 32
    return-void
.end method


# virtual methods
.method public refresh(Ljava/lang/String;)V
    .registers 11
    .param p1, "color"    # Ljava/lang/String;

    .line 44
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/PageIndicator;->configuration:Lbr/com/itau/widgets/graphicalfriedcake/PageIndicator$Configuration;

    if-nez v0, :cond_5

    .line 45
    return-void

    .line 47
    :cond_5
    invoke-virtual {p0}, Lbr/com/itau/widgets/graphicalfriedcake/PageIndicator;->removeAllViews()V

    .line 48
    invoke-virtual {p0}, Lbr/com/itau/widgets/graphicalfriedcake/PageIndicator;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    .line 50
    .local v2, "inflater":Landroid/view/LayoutInflater;
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/PageIndicator;->configuration:Lbr/com/itau/widgets/graphicalfriedcake/PageIndicator$Configuration;

    invoke-interface {v0}, Lbr/com/itau/widgets/graphicalfriedcake/PageIndicator$Configuration;->count()I

    move-result v3

    .line 51
    .local v3, "count":I
    const/4 v0, 0x1

    if-gt v3, v0, :cond_1f

    .line 52
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lbr/com/itau/widgets/graphicalfriedcake/PageIndicator;->setVisibility(I)V

    goto :goto_23

    .line 54
    :cond_1f
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lbr/com/itau/widgets/graphicalfriedcake/PageIndicator;->setVisibility(I)V

    .line 57
    :goto_23
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_24
    if-ge v4, v3, :cond_94

    .line 59
    sget v0, Lbr/com/itau/widgets/graphicalfriedcake/R$layout;->pagenavigator_indicator:I

    const/4 v1, 0x0

    invoke-virtual {v2, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v5

    .line 61
    .local v5, "view":Landroid/view/View;
    invoke-virtual {p0, v5}, Lbr/com/itau/widgets/graphicalfriedcake/PageIndicator;->addView(Landroid/view/View;)V

    .line 63
    sget v0, Lbr/com/itau/widgets/graphicalfriedcake/R$id;->indicator:I

    invoke-virtual {v5, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Landroid/widget/ImageView;

    .line 65
    .local v6, "cardNavigator":Landroid/widget/ImageView;
    move-object v7, p0

    monitor-enter v7

    .line 67
    :try_start_3b
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/PageIndicator;->configuration:Lbr/com/itau/widgets/graphicalfriedcake/PageIndicator$Configuration;

    invoke-interface {v0, v4}, Lbr/com/itau/widgets/graphicalfriedcake/PageIndicator$Configuration;->selected(I)Z

    move-result v0

    if-eqz v0, :cond_69

    .line 68
    invoke-static {p1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v0

    sget-object v1, Landroid/graphics/PorterDuff$Mode;->MULTIPLY:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v6, v0, v1}, Landroid/widget/ImageView;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 69
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "selected("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    goto :goto_8b

    .line 71
    :cond_69
    const v0, -0x333334

    invoke-virtual {v6, v0}, Landroid/widget/ImageView;->setColorFilter(I)V

    .line 72
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "not-selected("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V
    :try_end_8b
    .catchall {:try_start_3b .. :try_end_8b} :catchall_8d

    .line 74
    :goto_8b
    monitor-exit v7

    goto :goto_90

    :catchall_8d
    move-exception v8

    monitor-exit v7

    throw v8

    .line 57
    .end local v5    # "view":Landroid/view/View;
    .end local v6    # "cardNavigator":Landroid/widget/ImageView;
    :goto_90
    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_24

    .line 77
    .end local v4    # "i":I
    :cond_94
    return-void
.end method

.method public setConfiguration(Lbr/com/itau/widgets/graphicalfriedcake/PageIndicator$Configuration;)V
    .registers 2
    .param p1, "configuration"    # Lbr/com/itau/widgets/graphicalfriedcake/PageIndicator$Configuration;

    .line 35
    iput-object p1, p0, Lbr/com/itau/widgets/graphicalfriedcake/PageIndicator;->configuration:Lbr/com/itau/widgets/graphicalfriedcake/PageIndicator$Configuration;

    .line 36
    return-void
.end method

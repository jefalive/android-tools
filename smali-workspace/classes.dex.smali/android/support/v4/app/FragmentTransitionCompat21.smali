.class Landroid/support/v4/app/FragmentTransitionCompat21;
.super Ljava/lang/Object;
.source "FragmentTransitionCompat21.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/support/v4/app/FragmentTransitionCompat21$EpicenterView;,
        Landroid/support/v4/app/FragmentTransitionCompat21$ViewRetriever;
    }
.end annotation


# direct methods
.method constructor <init>()V
    .registers 1

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 497
    return-void
.end method

.method static synthetic access$000(Ljava/util/ArrayList;Landroid/view/View;)V
    .registers 2
    .param p0, "x0"    # Ljava/util/ArrayList;
    .param p1, "x1"    # Landroid/view/View;

    .line 31
    invoke-static {p0, p1}, Landroid/support/v4/app/FragmentTransitionCompat21;->captureTransitioningViews(Ljava/util/ArrayList;Landroid/view/View;)V

    return-void
.end method

.method static synthetic access$100(Landroid/transition/Transition;Landroid/transition/Transition;Ljava/util/ArrayList;Z)V
    .registers 4
    .param p0, "x0"    # Landroid/transition/Transition;
    .param p1, "x1"    # Landroid/transition/Transition;
    .param p2, "x2"    # Ljava/util/ArrayList;
    .param p3, "x3"    # Z

    .line 31
    invoke-static {p0, p1, p2, p3}, Landroid/support/v4/app/FragmentTransitionCompat21;->excludeViews(Landroid/transition/Transition;Landroid/transition/Transition;Ljava/util/ArrayList;Z)V

    return-void
.end method

.method static synthetic access$200(Landroid/view/View;)Landroid/graphics/Rect;
    .registers 2
    .param p0, "x0"    # Landroid/view/View;

    .line 31
    invoke-static {p0}, Landroid/support/v4/app/FragmentTransitionCompat21;->getBoundsOnScreen(Landroid/view/View;)Landroid/graphics/Rect;

    move-result-object v0

    return-object v0
.end method

.method public static addTargets(Ljava/lang/Object;Ljava/util/ArrayList;)V
    .registers 8
    .param p0, "transitionObject"    # Ljava/lang/Object;
    .param p1, "views"    # Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/lang/Object;Ljava/util/ArrayList<Landroid/view/View;>;)V"
        }
    .end annotation

    .line 463
    move-object v1, p0

    check-cast v1, Landroid/transition/Transition;

    .line 464
    .local v1, "transition":Landroid/transition/Transition;
    instance-of v0, v1, Landroid/transition/TransitionSet;

    if-eqz v0, :cond_1c

    .line 465
    move-object v2, v1

    check-cast v2, Landroid/transition/TransitionSet;

    .line 466
    .local v2, "set":Landroid/transition/TransitionSet;
    invoke-virtual {v2}, Landroid/transition/TransitionSet;->getTransitionCount()I

    move-result v3

    .line 467
    .local v3, "numTransitions":I
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_f
    if-ge v4, v3, :cond_1b

    .line 468
    invoke-virtual {v2, v4}, Landroid/transition/TransitionSet;->getTransitionAt(I)Landroid/transition/Transition;

    move-result-object v5

    .line 469
    .local v5, "child":Landroid/transition/Transition;
    invoke-static {v5, p1}, Landroid/support/v4/app/FragmentTransitionCompat21;->addTargets(Ljava/lang/Object;Ljava/util/ArrayList;)V

    .line 467
    .end local v5    # "child":Landroid/transition/Transition;
    add-int/lit8 v4, v4, 0x1

    goto :goto_f

    .line 471
    .end local v2    # "set":Landroid/transition/TransitionSet;
    .end local v3    # "numTransitions":I
    .end local v4    # "i":I
    :cond_1b
    goto :goto_3f

    :cond_1c
    invoke-static {v1}, Landroid/support/v4/app/FragmentTransitionCompat21;->hasSimpleTarget(Landroid/transition/Transition;)Z

    move-result v0

    if-nez v0, :cond_3f

    .line 472
    invoke-virtual {v1}, Landroid/transition/Transition;->getTargets()Ljava/util/List;

    move-result-object v2

    .line 473
    .local v2, "targets":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    invoke-static {v2}, Landroid/support/v4/app/FragmentTransitionCompat21;->isNullOrEmpty(Ljava/util/List;)Z

    move-result v0

    if-eqz v0, :cond_3f

    .line 475
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 476
    .local v3, "numViews":I
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_31
    if-ge v4, v3, :cond_3f

    .line 477
    invoke-virtual {p1, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/transition/Transition;->addTarget(Landroid/view/View;)Landroid/transition/Transition;

    .line 476
    add-int/lit8 v4, v4, 0x1

    goto :goto_31

    .line 481
    .end local v2    # "targets":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    .end local v2
    .end local v3    # "numViews":I
    .end local v4    # "i":I
    :cond_3f
    :goto_3f
    return-void
.end method

.method public static addTransitionTargets(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Landroid/view/View;Landroid/support/v4/app/FragmentTransitionCompat21$ViewRetriever;Landroid/view/View;Landroid/support/v4/app/FragmentTransitionCompat21$EpicenterView;Ljava/util/Map;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/Map;Ljava/util/Map;Ljava/util/ArrayList;)V
    .registers 26
    .param p0, "enterTransitionObject"    # Ljava/lang/Object;
    .param p1, "sharedElementTransitionObject"    # Ljava/lang/Object;
    .param p2, "exitTransitionObject"    # Ljava/lang/Object;
    .param p3, "container"    # Landroid/view/View;
    .param p4, "inFragment"    # Landroid/support/v4/app/FragmentTransitionCompat21$ViewRetriever;
    .param p5, "nonExistentView"    # Landroid/view/View;
    .param p6, "epicenterView"    # Landroid/support/v4/app/FragmentTransitionCompat21$EpicenterView;
    .param p7, "nameOverrides"    # Ljava/util/Map;
    .param p8, "enteringViews"    # Ljava/util/ArrayList;
    .param p9, "exitingViews"    # Ljava/util/ArrayList;
    .param p10, "namedViews"    # Ljava/util/Map;
    .param p11, "renamedViews"    # Ljava/util/Map;
    .param p12, "sharedElementTargets"    # Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Landroid/view/View;Landroid/support/v4/app/FragmentTransitionCompat21$ViewRetriever;Landroid/view/View;Landroid/support/v4/app/FragmentTransitionCompat21$EpicenterView;Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;Ljava/util/ArrayList<Landroid/view/View;>;Ljava/util/ArrayList<Landroid/view/View;>;Ljava/util/Map<Ljava/lang/String;Landroid/view/View;>;Ljava/util/Map<Ljava/lang/String;Landroid/view/View;>;Ljava/util/ArrayList<Landroid/view/View;>;)V"
        }
    .end annotation

    .line 143
    move-object v10, p0

    check-cast v10, Landroid/transition/Transition;

    .line 144
    .local v10, "enterTransition":Landroid/transition/Transition;
    move-object v11, p2

    check-cast v11, Landroid/transition/Transition;

    .line 145
    .local v11, "exitTransition":Landroid/transition/Transition;
    move-object v12, p1

    check-cast v12, Landroid/transition/Transition;

    .line 146
    .local v12, "sharedElementTransition":Landroid/transition/Transition;
    move-object/from16 v0, p9

    const/4 v1, 0x1

    invoke-static {v10, v11, v0, v1}, Landroid/support/v4/app/FragmentTransitionCompat21;->excludeViews(Landroid/transition/Transition;Landroid/transition/Transition;Ljava/util/ArrayList;Z)V

    .line 147
    if-nez p0, :cond_13

    if-eqz p1, :cond_50

    .line 148
    :cond_13
    if-eqz v10, :cond_1a

    .line 149
    move-object/from16 v0, p5

    invoke-virtual {v10, v0}, Landroid/transition/Transition;->addTarget(Landroid/view/View;)Landroid/transition/Transition;

    .line 151
    :cond_1a
    if-eqz p1, :cond_31

    .line 152
    move-object/from16 v0, p5

    move-object/from16 v1, p10

    move-object/from16 v2, p12

    invoke-static {v12, v0, v1, v2}, Landroid/support/v4/app/FragmentTransitionCompat21;->setSharedElementTargets(Ljava/lang/Object;Landroid/view/View;Ljava/util/Map;Ljava/util/ArrayList;)V

    .line 154
    move-object/from16 v0, p12

    const/4 v1, 0x1

    invoke-static {v10, v12, v0, v1}, Landroid/support/v4/app/FragmentTransitionCompat21;->excludeViews(Landroid/transition/Transition;Landroid/transition/Transition;Ljava/util/ArrayList;Z)V

    .line 155
    move-object/from16 v0, p12

    const/4 v1, 0x1

    invoke-static {v11, v12, v0, v1}, Landroid/support/v4/app/FragmentTransitionCompat21;->excludeViews(Landroid/transition/Transition;Landroid/transition/Transition;Ljava/util/ArrayList;Z)V

    .line 158
    :cond_31
    invoke-virtual/range {p3 .. p3}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    new-instance v1, Landroid/support/v4/app/FragmentTransitionCompat21$2;

    move-object/from16 v2, p3

    move-object v3, v10

    move-object/from16 v4, p5

    move-object/from16 v5, p4

    move-object/from16 v6, p7

    move-object/from16 v7, p11

    move-object/from16 v8, p8

    move-object v9, v11

    invoke-direct/range {v1 .. v9}, Landroid/support/v4/app/FragmentTransitionCompat21$2;-><init>(Landroid/view/View;Landroid/transition/Transition;Landroid/view/View;Landroid/support/v4/app/FragmentTransitionCompat21$ViewRetriever;Ljava/util/Map;Ljava/util/Map;Ljava/util/ArrayList;Landroid/transition/Transition;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 195
    move-object/from16 v0, p6

    invoke-static {v10, v0}, Landroid/support/v4/app/FragmentTransitionCompat21;->setSharedElementEpicenter(Landroid/transition/Transition;Landroid/support/v4/app/FragmentTransitionCompat21$EpicenterView;)V

    .line 197
    :cond_50
    return-void
.end method

.method public static beginDelayedTransition(Landroid/view/ViewGroup;Ljava/lang/Object;)V
    .registers 3
    .param p0, "sceneRoot"    # Landroid/view/ViewGroup;
    .param p1, "transitionObject"    # Ljava/lang/Object;

    .line 66
    move-object v0, p1

    check-cast v0, Landroid/transition/Transition;

    .line 67
    .local v0, "transition":Landroid/transition/Transition;
    invoke-static {p0, v0}, Landroid/transition/TransitionManager;->beginDelayedTransition(Landroid/view/ViewGroup;Landroid/transition/Transition;)V

    .line 68
    return-void
.end method

.method private static bfsAddViewChildren(Ljava/util/List;Landroid/view/View;)V
    .registers 10
    .param p0, "views"    # Ljava/util/List;
    .param p1, "startView"    # Landroid/view/View;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<Landroid/view/View;>;Landroid/view/View;)V"
        }
    .end annotation

    .line 286
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v1

    .line 287
    .local v1, "startIndex":I
    invoke-static {p0, p1, v1}, Landroid/support/v4/app/FragmentTransitionCompat21;->containedBeforeIndex(Ljava/util/List;Landroid/view/View;I)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 288
    return-void

    .line 290
    :cond_b
    invoke-interface {p0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 291
    move v2, v1

    .local v2, "index":I
    :goto_f
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_3d

    .line 292
    invoke-interface {p0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Landroid/view/View;

    .line 293
    .local v3, "view":Landroid/view/View;
    instance-of v0, v3, Landroid/view/ViewGroup;

    if-eqz v0, :cond_3a

    .line 294
    move-object v4, v3

    check-cast v4, Landroid/view/ViewGroup;

    .line 295
    .local v4, "viewGroup":Landroid/view/ViewGroup;
    invoke-virtual {v4}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v5

    .line 296
    .local v5, "childCount":I
    const/4 v6, 0x0

    .local v6, "childIndex":I
    :goto_28
    if-ge v6, v5, :cond_3a

    .line 297
    invoke-virtual {v4, v6}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v7

    .line 298
    .local v7, "child":Landroid/view/View;
    invoke-static {p0, v7, v1}, Landroid/support/v4/app/FragmentTransitionCompat21;->containedBeforeIndex(Ljava/util/List;Landroid/view/View;I)Z

    move-result v0

    if-nez v0, :cond_37

    .line 299
    invoke-interface {p0, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 296
    .end local v7    # "child":Landroid/view/View;
    :cond_37
    add-int/lit8 v6, v6, 0x1

    goto :goto_28

    .line 291
    .end local v3    # "view":Landroid/view/View;
    .end local v4    # "viewGroup":Landroid/view/ViewGroup;
    .end local v5    # "childCount":I
    .end local v6    # "childIndex":I
    :cond_3a
    add-int/lit8 v2, v2, 0x1

    goto :goto_f

    .line 304
    .end local v2    # "index":I
    :cond_3d
    return-void
.end method

.method public static captureExitingViews(Ljava/lang/Object;Landroid/view/View;Ljava/util/ArrayList;Ljava/util/Map;Landroid/view/View;)Ljava/lang/Object;
    .registers 6
    .param p0, "exitTransition"    # Ljava/lang/Object;
    .param p1, "root"    # Landroid/view/View;
    .param p2, "viewList"    # Ljava/util/ArrayList;
    .param p3, "namedViews"    # Ljava/util/Map;
    .param p4, "nonExistentView"    # Landroid/view/View;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/lang/Object;Landroid/view/View;Ljava/util/ArrayList<Landroid/view/View;>;Ljava/util/Map<Ljava/lang/String;Landroid/view/View;>;Landroid/view/View;)Ljava/lang/Object;"
        }
    .end annotation

    .line 45
    if-eqz p0, :cond_1f

    .line 46
    invoke-static {p2, p1}, Landroid/support/v4/app/FragmentTransitionCompat21;->captureTransitioningViews(Ljava/util/ArrayList;Landroid/view/View;)V

    .line 47
    if-eqz p3, :cond_e

    .line 48
    invoke-interface {p3}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->removeAll(Ljava/util/Collection;)Z

    .line 50
    :cond_e
    invoke-virtual {p2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_16

    .line 51
    const/4 p0, 0x0

    goto :goto_1f

    .line 53
    :cond_16
    invoke-virtual {p2, p4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 54
    move-object v0, p0

    check-cast v0, Landroid/transition/Transition;

    invoke-static {v0, p2}, Landroid/support/v4/app/FragmentTransitionCompat21;->addTargets(Ljava/lang/Object;Ljava/util/ArrayList;)V

    .line 57
    :cond_1f
    :goto_1f
    return-object p0
.end method

.method private static captureTransitioningViews(Ljava/util/ArrayList;Landroid/view/View;)V
    .registers 7
    .param p0, "transitioningViews"    # Ljava/util/ArrayList;
    .param p1, "view"    # Landroid/view/View;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/ArrayList<Landroid/view/View;>;Landroid/view/View;)V"
        }
    .end annotation

    .line 346
    invoke-virtual {p1}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_2c

    .line 347
    instance-of v0, p1, Landroid/view/ViewGroup;

    if-eqz v0, :cond_29

    .line 348
    move-object v1, p1

    check-cast v1, Landroid/view/ViewGroup;

    .line 349
    .local v1, "viewGroup":Landroid/view/ViewGroup;
    invoke-virtual {v1}, Landroid/view/ViewGroup;->isTransitionGroup()Z

    move-result v0

    if-eqz v0, :cond_17

    .line 350
    invoke-virtual {p0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_28

    .line 352
    :cond_17
    invoke-virtual {v1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v2

    .line 353
    .local v2, "count":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1c
    if-ge v3, v2, :cond_28

    .line 354
    invoke-virtual {v1, v3}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 355
    .local v4, "child":Landroid/view/View;
    invoke-static {p0, v4}, Landroid/support/v4/app/FragmentTransitionCompat21;->captureTransitioningViews(Ljava/util/ArrayList;Landroid/view/View;)V

    .line 353
    .end local v4    # "child":Landroid/view/View;
    add-int/lit8 v3, v3, 0x1

    goto :goto_1c

    .line 358
    .end local v1    # "viewGroup":Landroid/view/ViewGroup;
    .end local v2    # "count":I
    .end local v3    # "i":I
    :cond_28
    :goto_28
    goto :goto_2c

    .line 359
    :cond_29
    invoke-virtual {p0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 362
    :cond_2c
    :goto_2c
    return-void
.end method

.method public static cleanupTransitions(Landroid/view/View;Landroid/view/View;Ljava/lang/Object;Ljava/util/ArrayList;Ljava/lang/Object;Ljava/util/ArrayList;Ljava/lang/Object;Ljava/util/ArrayList;Ljava/lang/Object;Ljava/util/ArrayList;Ljava/util/Map;)V
    .registers 28
    .param p0, "sceneRoot"    # Landroid/view/View;
    .param p1, "nonExistentView"    # Landroid/view/View;
    .param p2, "enterTransitionObject"    # Ljava/lang/Object;
    .param p3, "enteringViews"    # Ljava/util/ArrayList;
    .param p4, "exitTransitionObject"    # Ljava/lang/Object;
    .param p5, "exitingViews"    # Ljava/util/ArrayList;
    .param p6, "sharedElementTransitionObject"    # Ljava/lang/Object;
    .param p7, "sharedElementTargets"    # Ljava/util/ArrayList;
    .param p8, "overallTransitionObject"    # Ljava/lang/Object;
    .param p9, "hiddenViews"    # Ljava/util/ArrayList;
    .param p10, "renamedViews"    # Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Landroid/view/View;Landroid/view/View;Ljava/lang/Object;Ljava/util/ArrayList<Landroid/view/View;>;Ljava/lang/Object;Ljava/util/ArrayList<Landroid/view/View;>;Ljava/lang/Object;Ljava/util/ArrayList<Landroid/view/View;>;Ljava/lang/Object;Ljava/util/ArrayList<Landroid/view/View;>;Ljava/util/Map<Ljava/lang/String;Landroid/view/View;>;)V"
        }
    .end annotation

    .line 387
    move-object/from16 v13, p2

    check-cast v13, Landroid/transition/Transition;

    .line 388
    .local v13, "enterTransition":Landroid/transition/Transition;
    move-object/from16 v14, p4

    check-cast v14, Landroid/transition/Transition;

    .line 389
    .local v14, "exitTransition":Landroid/transition/Transition;
    move-object/from16 v15, p6

    check-cast v15, Landroid/transition/Transition;

    .line 390
    .local v15, "sharedElementTransition":Landroid/transition/Transition;
    move-object/from16 v16, p8

    check-cast v16, Landroid/transition/Transition;

    .line 391
    .local v16, "overallTransition":Landroid/transition/Transition;
    if-eqz v16, :cond_31

    .line 392
    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    new-instance v1, Landroid/support/v4/app/FragmentTransitionCompat21$4;

    move-object/from16 v2, p0

    move-object v3, v13

    move-object/from16 v4, p3

    move-object v5, v14

    move-object/from16 v6, p5

    move-object v7, v15

    move-object/from16 v8, p7

    move-object/from16 v9, p10

    move-object/from16 v10, p9

    move-object/from16 v11, v16

    move-object/from16 v12, p1

    invoke-direct/range {v1 .. v12}, Landroid/support/v4/app/FragmentTransitionCompat21$4;-><init>(Landroid/view/View;Landroid/transition/Transition;Ljava/util/ArrayList;Landroid/transition/Transition;Ljava/util/ArrayList;Landroid/transition/Transition;Ljava/util/ArrayList;Ljava/util/Map;Ljava/util/ArrayList;Landroid/transition/Transition;Landroid/view/View;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 426
    :cond_31
    return-void
.end method

.method public static cloneTransition(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 2
    .param p0, "transition"    # Ljava/lang/Object;

    .line 37
    if-eqz p0, :cond_9

    .line 38
    move-object v0, p0

    check-cast v0, Landroid/transition/Transition;

    invoke-virtual {v0}, Landroid/transition/Transition;->clone()Landroid/transition/Transition;

    move-result-object p0

    .line 40
    :cond_9
    return-object p0
.end method

.method private static containedBeforeIndex(Ljava/util/List;Landroid/view/View;I)Z
    .registers 5
    .param p0, "views"    # Ljava/util/List;
    .param p1, "view"    # Landroid/view/View;
    .param p2, "maxIndex"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<Landroid/view/View;>;Landroid/view/View;I)Z"
        }
    .end annotation

    .line 311
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    if-ge v1, p2, :cond_e

    .line 312
    invoke-interface {p0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-ne v0, p1, :cond_b

    .line 313
    const/4 v0, 0x1

    return v0

    .line 311
    :cond_b
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 316
    .end local v1    # "i":I
    :cond_e
    const/4 v0, 0x0

    return v0
.end method

.method public static excludeSharedElementViews(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/util/ArrayList;Z)V
    .registers 8
    .param p0, "enterTransitionObj"    # Ljava/lang/Object;
    .param p1, "exitTransitionObj"    # Ljava/lang/Object;
    .param p2, "sharedElementTransitionObj"    # Ljava/lang/Object;
    .param p3, "views"    # Ljava/util/ArrayList;
    .param p4, "exclude"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/util/ArrayList<Landroid/view/View;>;Z)V"
        }
    .end annotation

    .line 118
    move-object v0, p0

    check-cast v0, Landroid/transition/Transition;

    .line 119
    .local v0, "enterTransition":Landroid/transition/Transition;
    move-object v1, p1

    check-cast v1, Landroid/transition/Transition;

    .line 120
    .local v1, "exitTransition":Landroid/transition/Transition;
    move-object v2, p2

    check-cast v2, Landroid/transition/Transition;

    .line 121
    .local v2, "sharedElementTransition":Landroid/transition/Transition;
    invoke-static {v0, v2, p3, p4}, Landroid/support/v4/app/FragmentTransitionCompat21;->excludeViews(Landroid/transition/Transition;Landroid/transition/Transition;Ljava/util/ArrayList;Z)V

    .line 122
    invoke-static {v1, v2, p3, p4}, Landroid/support/v4/app/FragmentTransitionCompat21;->excludeViews(Landroid/transition/Transition;Landroid/transition/Transition;Ljava/util/ArrayList;Z)V

    .line 123
    return-void
.end method

.method public static excludeTarget(Ljava/lang/Object;Landroid/view/View;Z)V
    .registers 5
    .param p0, "transitionObject"    # Ljava/lang/Object;
    .param p1, "view"    # Landroid/view/View;
    .param p2, "exclude"    # Z

    .line 61
    move-object v1, p0

    check-cast v1, Landroid/transition/Transition;

    .line 62
    .local v1, "transition":Landroid/transition/Transition;
    invoke-virtual {v1, p1, p2}, Landroid/transition/Transition;->excludeTarget(Landroid/view/View;Z)Landroid/transition/Transition;

    .line 63
    return-void
.end method

.method private static excludeViews(Landroid/transition/Transition;Landroid/transition/Transition;Ljava/util/ArrayList;Z)V
    .registers 7
    .param p0, "transition"    # Landroid/transition/Transition;
    .param p1, "fromTransition"    # Landroid/transition/Transition;
    .param p2, "views"    # Ljava/util/ArrayList;
    .param p3, "exclude"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Landroid/transition/Transition;Landroid/transition/Transition;Ljava/util/ArrayList<Landroid/view/View;>;Z)V"
        }
    .end annotation

    .line 97
    if-eqz p0, :cond_19

    .line 98
    if-nez p1, :cond_6

    const/4 v1, 0x0

    goto :goto_a

    :cond_6
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 99
    .local v1, "viewCount":I
    :goto_a
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_b
    if-ge v2, v1, :cond_19

    .line 100
    invoke-virtual {p2, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {p0, v0, p3}, Landroid/transition/Transition;->excludeTarget(Landroid/view/View;Z)Landroid/transition/Transition;

    .line 99
    add-int/lit8 v2, v2, 0x1

    goto :goto_b

    .line 103
    .end local v1    # "viewCount":I
    .end local v2    # "i":I
    :cond_19
    return-void
.end method

.method public static findNamedViews(Ljava/util/Map;Landroid/view/View;)V
    .registers 8
    .param p0, "namedViews"    # Ljava/util/Map;
    .param p1, "view"    # Landroid/view/View;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/Map<Ljava/lang/String;Landroid/view/View;>;Landroid/view/View;)V"
        }
    .end annotation

    .line 365
    invoke-virtual {p1}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_27

    .line 366
    invoke-virtual {p1}, Landroid/view/View;->getTransitionName()Ljava/lang/String;

    move-result-object v1

    .line 367
    .local v1, "transitionName":Ljava/lang/String;
    if-eqz v1, :cond_f

    .line 368
    invoke-interface {p0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 370
    :cond_f
    instance-of v0, p1, Landroid/view/ViewGroup;

    if-eqz v0, :cond_27

    .line 371
    move-object v2, p1

    check-cast v2, Landroid/view/ViewGroup;

    .line 372
    .local v2, "viewGroup":Landroid/view/ViewGroup;
    invoke-virtual {v2}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v3

    .line 373
    .local v3, "count":I
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_1b
    if-ge v4, v3, :cond_27

    .line 374
    invoke-virtual {v2, v4}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    .line 375
    .local v5, "child":Landroid/view/View;
    invoke-static {p0, v5}, Landroid/support/v4/app/FragmentTransitionCompat21;->findNamedViews(Ljava/util/Map;Landroid/view/View;)V

    .line 373
    .end local v5    # "child":Landroid/view/View;
    add-int/lit8 v4, v4, 0x1

    goto :goto_1b

    .line 379
    .end local v1    # "transitionName":Ljava/lang/String;
    .end local v2    # "viewGroup":Landroid/view/ViewGroup;
    .end local v3    # "count":I
    .end local v4    # "i":I
    :cond_27
    return-void
.end method

.method private static getBoundsOnScreen(Landroid/view/View;)Landroid/graphics/Rect;
    .registers 8
    .param p0, "view"    # Landroid/view/View;

    .line 337
    new-instance v5, Landroid/graphics/Rect;

    invoke-direct {v5}, Landroid/graphics/Rect;-><init>()V

    .line 338
    .local v5, "epicenter":Landroid/graphics/Rect;
    const/4 v0, 0x2

    new-array v6, v0, [I

    .line 339
    .local v6, "loc":[I
    invoke-virtual {p0, v6}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 341
    const/4 v0, 0x0

    aget v0, v6, v0

    const/4 v1, 0x1

    aget v1, v6, v1

    const/4 v2, 0x0

    aget v2, v6, v2

    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v3

    add-int/2addr v2, v3

    const/4 v3, 0x1

    aget v3, v6, v3

    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result v4

    add-int/2addr v3, v4

    invoke-virtual {v5, v0, v1, v2, v3}, Landroid/graphics/Rect;->set(IIII)V

    .line 342
    return-object v5
.end method

.method public static getTransitionName(Landroid/view/View;)Ljava/lang/String;
    .registers 2
    .param p0, "view"    # Landroid/view/View;

    .line 33
    invoke-virtual {p0}, Landroid/view/View;->getTransitionName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static hasSimpleTarget(Landroid/transition/Transition;)Z
    .registers 2
    .param p0, "transition"    # Landroid/transition/Transition;

    .line 484
    invoke-virtual {p0}, Landroid/transition/Transition;->getTargetIds()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v4/app/FragmentTransitionCompat21;->isNullOrEmpty(Ljava/util/List;)Z

    move-result v0

    if-eqz v0, :cond_1e

    .line 485
    invoke-virtual {p0}, Landroid/transition/Transition;->getTargetNames()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v4/app/FragmentTransitionCompat21;->isNullOrEmpty(Ljava/util/List;)Z

    move-result v0

    if-eqz v0, :cond_1e

    .line 486
    invoke-virtual {p0}, Landroid/transition/Transition;->getTargetTypes()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v4/app/FragmentTransitionCompat21;->isNullOrEmpty(Ljava/util/List;)Z

    move-result v0

    if-nez v0, :cond_20

    :cond_1e
    const/4 v0, 0x1

    goto :goto_21

    :cond_20
    const/4 v0, 0x0

    :goto_21
    return v0
.end method

.method private static isNullOrEmpty(Ljava/util/List;)Z
    .registers 2
    .param p0, "list"    # Ljava/util/List;

    .line 490
    if-eqz p0, :cond_8

    invoke-interface {p0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_a

    :cond_8
    const/4 v0, 0x1

    goto :goto_b

    :cond_a
    const/4 v0, 0x0

    :goto_b
    return v0
.end method

.method public static mergeTransitions(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Z)Ljava/lang/Object;
    .registers 13
    .param p0, "enterTransitionObject"    # Ljava/lang/Object;
    .param p1, "exitTransitionObject"    # Ljava/lang/Object;
    .param p2, "sharedElementTransitionObject"    # Ljava/lang/Object;
    .param p3, "allowOverlap"    # Z

    .line 202
    const/4 v2, 0x1

    .line 203
    .local v2, "overlap":Z
    move-object v3, p0

    check-cast v3, Landroid/transition/Transition;

    .line 204
    .local v3, "enterTransition":Landroid/transition/Transition;
    move-object v4, p1

    check-cast v4, Landroid/transition/Transition;

    .line 205
    .local v4, "exitTransition":Landroid/transition/Transition;
    move-object v5, p2

    check-cast v5, Landroid/transition/Transition;

    .line 207
    .local v5, "sharedElementTransition":Landroid/transition/Transition;
    if-eqz v3, :cond_f

    if-eqz v4, :cond_f

    .line 208
    move v2, p3

    .line 216
    :cond_f
    if-eqz v2, :cond_27

    .line 218
    new-instance v7, Landroid/transition/TransitionSet;

    invoke-direct {v7}, Landroid/transition/TransitionSet;-><init>()V

    .line 219
    .local v7, "transitionSet":Landroid/transition/TransitionSet;
    if-eqz v3, :cond_1b

    .line 220
    invoke-virtual {v7, v3}, Landroid/transition/TransitionSet;->addTransition(Landroid/transition/Transition;)Landroid/transition/TransitionSet;

    .line 222
    :cond_1b
    if-eqz v4, :cond_20

    .line 223
    invoke-virtual {v7, v4}, Landroid/transition/TransitionSet;->addTransition(Landroid/transition/Transition;)Landroid/transition/TransitionSet;

    .line 225
    :cond_20
    if-eqz v5, :cond_25

    .line 226
    invoke-virtual {v7, v5}, Landroid/transition/TransitionSet;->addTransition(Landroid/transition/Transition;)Landroid/transition/TransitionSet;

    .line 228
    :cond_25
    move-object v6, v7

    .line 229
    .local v6, "transition":Landroid/transition/Transition;
    .end local v7    # "transitionSet":Landroid/transition/TransitionSet;
    goto :goto_58

    .line 232
    .end local v6    # "transition":Landroid/transition/Transition;
    :cond_27
    const/4 v7, 0x0

    .line 233
    .local v7, "staggered":Landroid/transition/Transition;
    if-eqz v4, :cond_3f

    if-eqz v3, :cond_3f

    .line 234
    new-instance v0, Landroid/transition/TransitionSet;

    invoke-direct {v0}, Landroid/transition/TransitionSet;-><init>()V

    .line 235
    invoke-virtual {v0, v4}, Landroid/transition/TransitionSet;->addTransition(Landroid/transition/Transition;)Landroid/transition/TransitionSet;

    move-result-object v0

    .line 236
    invoke-virtual {v0, v3}, Landroid/transition/TransitionSet;->addTransition(Landroid/transition/Transition;)Landroid/transition/TransitionSet;

    move-result-object v0

    .line 237
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/transition/TransitionSet;->setOrdering(I)Landroid/transition/TransitionSet;

    move-result-object v7

    goto :goto_46

    .line 238
    :cond_3f
    if-eqz v4, :cond_43

    .line 239
    move-object v7, v4

    goto :goto_46

    .line 240
    :cond_43
    if-eqz v3, :cond_46

    .line 241
    move-object v7, v3

    .line 243
    :cond_46
    :goto_46
    if-eqz v5, :cond_57

    .line 244
    new-instance v8, Landroid/transition/TransitionSet;

    invoke-direct {v8}, Landroid/transition/TransitionSet;-><init>()V

    .line 245
    .local v8, "together":Landroid/transition/TransitionSet;
    if-eqz v7, :cond_52

    .line 246
    invoke-virtual {v8, v7}, Landroid/transition/TransitionSet;->addTransition(Landroid/transition/Transition;)Landroid/transition/TransitionSet;

    .line 248
    :cond_52
    invoke-virtual {v8, v5}, Landroid/transition/TransitionSet;->addTransition(Landroid/transition/Transition;)Landroid/transition/TransitionSet;

    .line 249
    move-object v6, v8

    .line 250
    .local v6, "transition":Landroid/transition/Transition;
    .end local v8    # "together":Landroid/transition/TransitionSet;
    goto :goto_58

    .line 251
    .end local v6    # "transition":Landroid/transition/Transition;
    :cond_57
    move-object v6, v7

    .line 254
    .local v6, "transition":Landroid/transition/Transition;
    .end local v7    # "staggered":Landroid/transition/Transition;
    :goto_58
    return-object v6
.end method

.method public static removeTargets(Ljava/lang/Object;Ljava/util/ArrayList;)V
    .registers 9
    .param p0, "transitionObject"    # Ljava/lang/Object;
    .param p1, "views"    # Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/lang/Object;Ljava/util/ArrayList<Landroid/view/View;>;)V"
        }
    .end annotation

    .line 434
    move-object v2, p0

    check-cast v2, Landroid/transition/Transition;

    .line 435
    .local v2, "transition":Landroid/transition/Transition;
    instance-of v0, v2, Landroid/transition/TransitionSet;

    if-eqz v0, :cond_1c

    .line 436
    move-object v3, v2

    check-cast v3, Landroid/transition/TransitionSet;

    .line 437
    .local v3, "set":Landroid/transition/TransitionSet;
    invoke-virtual {v3}, Landroid/transition/TransitionSet;->getTransitionCount()I

    move-result v4

    .line 438
    .local v4, "numTransitions":I
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_f
    if-ge v5, v4, :cond_1b

    .line 439
    invoke-virtual {v3, v5}, Landroid/transition/TransitionSet;->getTransitionAt(I)Landroid/transition/Transition;

    move-result-object v6

    .line 440
    .local v6, "child":Landroid/transition/Transition;
    invoke-static {v6, p1}, Landroid/support/v4/app/FragmentTransitionCompat21;->removeTargets(Ljava/lang/Object;Ljava/util/ArrayList;)V

    .line 438
    .end local v6    # "child":Landroid/transition/Transition;
    add-int/lit8 v5, v5, 0x1

    goto :goto_f

    .line 442
    .end local v3    # "set":Landroid/transition/TransitionSet;
    .end local v4    # "numTransitions":I
    .end local v5    # "i":I
    :cond_1b
    goto :goto_4c

    :cond_1c
    invoke-static {v2}, Landroid/support/v4/app/FragmentTransitionCompat21;->hasSimpleTarget(Landroid/transition/Transition;)Z

    move-result v0

    if-nez v0, :cond_4c

    .line 443
    invoke-virtual {v2}, Landroid/transition/Transition;->getTargets()Ljava/util/List;

    move-result-object v3

    .line 444
    .local v3, "targets":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    if-eqz v3, :cond_4c

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ne v0, v1, :cond_4c

    .line 445
    invoke-interface {v3, p1}, Ljava/util/List;->containsAll(Ljava/util/Collection;)Z

    move-result v0

    if-eqz v0, :cond_4c

    .line 447
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v4, v0, -0x1

    .local v4, "i":I
    :goto_3e
    if-ltz v4, :cond_4c

    .line 448
    invoke-virtual {p1, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v2, v0}, Landroid/transition/Transition;->removeTarget(Landroid/view/View;)Landroid/transition/Transition;

    .line 447
    add-int/lit8 v4, v4, -0x1

    goto :goto_3e

    .line 452
    .end local v3    # "targets":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    .end local v3
    .end local v4    # "i":I
    :cond_4c
    :goto_4c
    return-void
.end method

.method public static setEpicenter(Ljava/lang/Object;Landroid/view/View;)V
    .registers 5
    .param p0, "transitionObject"    # Ljava/lang/Object;
    .param p1, "view"    # Landroid/view/View;

    .line 71
    move-object v1, p0

    check-cast v1, Landroid/transition/Transition;

    .line 72
    .local v1, "transition":Landroid/transition/Transition;
    invoke-static {p1}, Landroid/support/v4/app/FragmentTransitionCompat21;->getBoundsOnScreen(Landroid/view/View;)Landroid/graphics/Rect;

    move-result-object v2

    .line 74
    .local v2, "epicenter":Landroid/graphics/Rect;
    new-instance v0, Landroid/support/v4/app/FragmentTransitionCompat21$1;

    invoke-direct {v0, v2}, Landroid/support/v4/app/FragmentTransitionCompat21$1;-><init>(Landroid/graphics/Rect;)V

    invoke-virtual {v1, v0}, Landroid/transition/Transition;->setEpicenterCallback(Landroid/transition/Transition$EpicenterCallback;)V

    .line 80
    return-void
.end method

.method private static setSharedElementEpicenter(Landroid/transition/Transition;Landroid/support/v4/app/FragmentTransitionCompat21$EpicenterView;)V
    .registers 3
    .param p0, "transition"    # Landroid/transition/Transition;
    .param p1, "epicenterView"    # Landroid/support/v4/app/FragmentTransitionCompat21$EpicenterView;

    .line 321
    if-eqz p0, :cond_a

    .line 322
    new-instance v0, Landroid/support/v4/app/FragmentTransitionCompat21$3;

    invoke-direct {v0, p1}, Landroid/support/v4/app/FragmentTransitionCompat21$3;-><init>(Landroid/support/v4/app/FragmentTransitionCompat21$EpicenterView;)V

    invoke-virtual {p0, v0}, Landroid/transition/Transition;->setEpicenterCallback(Landroid/transition/Transition$EpicenterCallback;)V

    .line 334
    :cond_a
    return-void
.end method

.method public static setSharedElementTargets(Ljava/lang/Object;Landroid/view/View;Ljava/util/Map;Ljava/util/ArrayList;)V
    .registers 10
    .param p0, "transitionObj"    # Ljava/lang/Object;
    .param p1, "nonExistentView"    # Landroid/view/View;
    .param p2, "namedViews"    # Ljava/util/Map;
    .param p3, "sharedElementTargets"    # Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/lang/Object;Landroid/view/View;Ljava/util/Map<Ljava/lang/String;Landroid/view/View;>;Ljava/util/ArrayList<Landroid/view/View;>;)V"
        }
    .end annotation

    .line 266
    move-object v1, p0

    check-cast v1, Landroid/transition/TransitionSet;

    .line 267
    .local v1, "transition":Landroid/transition/TransitionSet;
    invoke-virtual {p3}, Ljava/util/ArrayList;->clear()V

    .line 268
    invoke-interface {p2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 270
    invoke-virtual {v1}, Landroid/transition/TransitionSet;->getTargets()Ljava/util/List;

    move-result-object v2

    .line 271
    .local v2, "views":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 272
    invoke-virtual {p3}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 273
    .local v3, "count":I
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_19
    if-ge v4, v3, :cond_28

    .line 274
    invoke-virtual {p3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Landroid/view/View;

    .line 275
    .local v5, "view":Landroid/view/View;
    invoke-static {v2, v5}, Landroid/support/v4/app/FragmentTransitionCompat21;->bfsAddViewChildren(Ljava/util/List;Landroid/view/View;)V

    .line 273
    .end local v5    # "view":Landroid/view/View;
    add-int/lit8 v4, v4, 0x1

    goto :goto_19

    .line 277
    .end local v4    # "i":I
    :cond_28
    invoke-virtual {p3, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 278
    invoke-static {v1, p3}, Landroid/support/v4/app/FragmentTransitionCompat21;->addTargets(Ljava/lang/Object;Ljava/util/ArrayList;)V

    .line 279
    return-void
.end method

.method public static wrapSharedElementTransition(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 4
    .param p0, "transitionObj"    # Ljava/lang/Object;

    .line 83
    if-nez p0, :cond_4

    .line 84
    const/4 v0, 0x0

    return-object v0

    .line 86
    :cond_4
    move-object v1, p0

    check-cast v1, Landroid/transition/Transition;

    .line 87
    .local v1, "transition":Landroid/transition/Transition;
    if-nez v1, :cond_b

    .line 88
    const/4 v0, 0x0

    return-object v0

    .line 90
    :cond_b
    new-instance v2, Landroid/transition/TransitionSet;

    invoke-direct {v2}, Landroid/transition/TransitionSet;-><init>()V

    .line 91
    .local v2, "transitionSet":Landroid/transition/TransitionSet;
    invoke-virtual {v2, v1}, Landroid/transition/TransitionSet;->addTransition(Landroid/transition/Transition;)Landroid/transition/TransitionSet;

    .line 92
    return-object v2
.end method

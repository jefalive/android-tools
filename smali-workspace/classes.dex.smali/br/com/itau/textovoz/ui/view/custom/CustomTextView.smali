.class public Lbr/com/itau/textovoz/ui/view/custom/CustomTextView;
.super Landroid/support/v7/widget/AppCompatTextView;
.source "CustomTextView.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .param p1, "context"    # Landroid/content/Context;

    .line 24
    invoke-direct {p0, p1}, Landroid/support/v7/widget/AppCompatTextView;-><init>(Landroid/content/Context;)V

    .line 25
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lbr/com/itau/textovoz/ui/view/custom/CustomTextView;->init(Landroid/util/AttributeSet;)V

    .line 26
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .line 19
    invoke-direct {p0, p1, p2}, Landroid/support/v7/widget/AppCompatTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 20
    invoke-direct {p0, p2}, Lbr/com/itau/textovoz/ui/view/custom/CustomTextView;->init(Landroid/util/AttributeSet;)V

    .line 21
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .line 14
    invoke-direct {p0, p1, p2, p3}, Landroid/support/v7/widget/AppCompatTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 15
    invoke-direct {p0, p2}, Lbr/com/itau/textovoz/ui/view/custom/CustomTextView;->init(Landroid/util/AttributeSet;)V

    .line 16
    return-void
.end method

.method private init(Landroid/util/AttributeSet;)V
    .registers 8
    .param p1, "attrs"    # Landroid/util/AttributeSet;

    .line 29
    if-eqz p1, :cond_39

    .line 30
    invoke-virtual {p0}, Lbr/com/itau/textovoz/ui/view/custom/CustomTextView;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, Lbr/com/itau/textovoz/R$styleable;->CustomTextView:[I

    invoke-virtual {v0, p1, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v3

    .line 31
    .local v3, "a":Landroid/content/res/TypedArray;
    sget v0, Lbr/com/itau/textovoz/R$styleable;->CustomTextView_font:I

    invoke-virtual {v3, v0}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 32
    .local v4, "fontName":Ljava/lang/String;
    if-eqz v4, :cond_36

    .line 33
    invoke-virtual {p0}, Lbr/com/itau/textovoz/ui/view/custom/CustomTextView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "fonts/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/graphics/Typeface;->createFromAsset(Landroid/content/res/AssetManager;Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v5

    .line 35
    .local v5, "myTypeface":Landroid/graphics/Typeface;
    invoke-virtual {p0, v5}, Lbr/com/itau/textovoz/ui/view/custom/CustomTextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 37
    .end local v5    # "myTypeface":Landroid/graphics/Typeface;
    :cond_36
    invoke-virtual {v3}, Landroid/content/res/TypedArray;->recycle()V

    .line 39
    .end local v3    # "a":Landroid/content/res/TypedArray;
    .end local v4    # "fontName":Ljava/lang/String;
    :cond_39
    return-void
.end method

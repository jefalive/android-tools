.class public final enum Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendForm;
.super Ljava/lang/Enum;
.source "Legend.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "LegendForm"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendForm;>;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendForm;

.field public static final enum CIRCLE:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendForm;

.field public static final enum LINE:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendForm;

.field public static final enum SQUARE:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendForm;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .line 30
    new-instance v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendForm;

    const-string v1, "SQUARE"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendForm;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendForm;->SQUARE:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendForm;

    new-instance v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendForm;

    const-string v1, "CIRCLE"

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendForm;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendForm;->CIRCLE:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendForm;

    new-instance v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendForm;

    const-string v1, "LINE"

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendForm;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendForm;->LINE:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendForm;

    .line 29
    const/4 v0, 0x3

    new-array v0, v0, [Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendForm;

    sget-object v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendForm;->SQUARE:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendForm;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sget-object v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendForm;->CIRCLE:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendForm;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    sget-object v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendForm;->LINE:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendForm;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    sput-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendForm;->$VALUES:[Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendForm;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 29
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendForm;
    .registers 2
    .param p0, "name"    # Ljava/lang/String;

    .line 29
    const-class v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendForm;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendForm;

    return-object v0
.end method

.method public static values()[Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendForm;
    .registers 1

    .line 29
    sget-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendForm;->$VALUES:[Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendForm;

    invoke-virtual {v0}, [Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendForm;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendForm;

    return-object v0
.end method

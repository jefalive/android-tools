.class public Lcom/itau/empresas/feature/credito/giro/customview/GiroPersonalizadoValoresView;
.super Landroid/widget/RelativeLayout;
.source "GiroPersonalizadoValoresView.java"


# instance fields
.field listaValoresPersonalizados:Landroid/widget/ListView;

.field parcelasAdapter:Lcom/itau/empresas/feature/credito/giro/adapter/GiroPersonalizadoValoresAdapter;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 2
    .param p1, "context"    # Landroid/content/Context;

    .line 45
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 46
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .line 49
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 50
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .line 53
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 54
    return-void
.end method

.method private recalculateHeight(Z)V
    .registers 4
    .param p1, "hasItems"    # Z

    .line 145
    if-eqz p1, :cond_f

    .line 146
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/giro/customview/GiroPersonalizadoValoresView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/giro/customview/GiroPersonalizadoValoresView;->listaValoresPersonalizados:Landroid/widget/ListView;

    invoke-static {v1}, Lcom/itau/empresas/ui/util/ViewUtils;->calculateHeight(Landroid/widget/ListView;)I

    move-result v1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    goto :goto_1b

    .line 148
    :cond_f
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/giro/customview/GiroPersonalizadoValoresView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/giro/customview/GiroPersonalizadoValoresView;->listaValoresPersonalizados:Landroid/widget/ListView;

    invoke-virtual {v1}, Landroid/widget/ListView;->getDividerHeight()I

    move-result v1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 150
    :goto_1b
    return-void
.end method


# virtual methods
.method public addItem(Ljava/math/BigDecimal;)V
    .registers 6
    .param p1, "valorEmprestimo"    # Ljava/math/BigDecimal;

    .line 125
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/GiroPersonalizadoValoresView;->parcelasAdapter:Lcom/itau/empresas/feature/credito/giro/adapter/GiroPersonalizadoValoresAdapter;

    if-eqz v0, :cond_39

    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/GiroPersonalizadoValoresView;->parcelasAdapter:Lcom/itau/empresas/feature/credito/giro/adapter/GiroPersonalizadoValoresAdapter;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/credito/giro/adapter/GiroPersonalizadoValoresAdapter;->getItems()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_39

    .line 128
    new-instance v3, Lcom/itau/empresas/feature/credito/giro/model/ValoresEmprestimoVO;

    invoke-direct {v3}, Lcom/itau/empresas/feature/credito/giro/model/ValoresEmprestimoVO;-><init>()V

    .line 129
    .local v3, "valoresEmprestimoVO":Lcom/itau/empresas/feature/credito/giro/model/ValoresEmprestimoVO;
    invoke-virtual {v3, p1}, Lcom/itau/empresas/feature/credito/giro/model/ValoresEmprestimoVO;->setValoresEmprestimo(Ljava/math/BigDecimal;)V

    .line 130
    sget-object v0, Lcom/itau/empresas/feature/credito/giro/model/ValoresEmprestimoVO$EnumValoresEmprestimo;->CARREGADO_MANUALMENTE:Lcom/itau/empresas/feature/credito/giro/model/ValoresEmprestimoVO$EnumValoresEmprestimo;

    invoke-virtual {v3, v0}, Lcom/itau/empresas/feature/credito/giro/model/ValoresEmprestimoVO;->setEnumValoresEmprestimo(Lcom/itau/empresas/feature/credito/giro/model/ValoresEmprestimoVO$EnumValoresEmprestimo;)V

    .line 132
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/GiroPersonalizadoValoresView;->parcelasAdapter:Lcom/itau/empresas/feature/credito/giro/adapter/GiroPersonalizadoValoresAdapter;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/credito/giro/adapter/GiroPersonalizadoValoresAdapter;->getItems()Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/giro/customview/GiroPersonalizadoValoresView;->parcelasAdapter:Lcom/itau/empresas/feature/credito/giro/adapter/GiroPersonalizadoValoresAdapter;

    .line 133
    invoke-virtual {v1}, Lcom/itau/empresas/feature/credito/giro/adapter/GiroPersonalizadoValoresAdapter;->getItems()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1, v3}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 134
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/GiroPersonalizadoValoresView;->listaValoresPersonalizados:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/giro/customview/GiroPersonalizadoValoresView;->parcelasAdapter:Lcom/itau/empresas/feature/credito/giro/adapter/GiroPersonalizadoValoresAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 135
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/itau/empresas/feature/credito/giro/customview/GiroPersonalizadoValoresView;->recalculateHeight(Z)V

    .line 137
    .end local v3    # "valoresEmprestimoVO":Lcom/itau/empresas/feature/credito/giro/model/ValoresEmprestimoVO;
    :cond_39
    return-void
.end method

.method public clearData()V
    .registers 4

    .line 95
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/GiroPersonalizadoValoresView;->listaValoresPersonalizados:Landroid/widget/ListView;

    if-eqz v0, :cond_15

    .line 97
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/GiroPersonalizadoValoresView;->listaValoresPersonalizados:Landroid/widget/ListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 98
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/GiroPersonalizadoValoresView;->listaValoresPersonalizados:Landroid/widget/ListView;

    const/4 v1, -0x1

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 99
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/itau/empresas/feature/credito/giro/customview/GiroPersonalizadoValoresView;->recalculateHeight(Z)V

    .line 101
    :cond_15
    return-void
.end method

.method public getSelectedItem()Lcom/itau/empresas/feature/credito/giro/model/ValoresEmprestimoVO;
    .registers 3

    .line 113
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/GiroPersonalizadoValoresView;->listaValoresPersonalizados:Landroid/widget/ListView;

    if-eqz v0, :cond_1b

    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/GiroPersonalizadoValoresView;->listaValoresPersonalizados:Landroid/widget/ListView;

    .line 114
    invoke-virtual {v0}, Landroid/widget/ListView;->getCheckedItemPosition()I

    move-result v0

    if-ltz v0, :cond_1b

    .line 116
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/GiroPersonalizadoValoresView;->listaValoresPersonalizados:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/giro/customview/GiroPersonalizadoValoresView;->listaValoresPersonalizados:Landroid/widget/ListView;

    .line 117
    invoke-virtual {v1}, Landroid/widget/ListView;->getCheckedItemPosition()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/feature/credito/giro/model/ValoresEmprestimoVO;

    .line 116
    return-object v0

    .line 120
    :cond_1b
    const/4 v0, 0x0

    return-object v0
.end method

.method public removeSelection()V
    .registers 4

    .line 105
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/GiroPersonalizadoValoresView;->listaValoresPersonalizados:Landroid/widget/ListView;

    if-eqz v0, :cond_b

    .line 107
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/GiroPersonalizadoValoresView;->listaValoresPersonalizados:Landroid/widget/ListView;

    const/4 v1, -0x1

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 109
    :cond_b
    return-void
.end method

.method public setData(Landroid/content/Context;Ljava/util/List;FF)V
    .registers 10
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "listValoresEmprestimo"    # Ljava/util/List;
    .param p3, "valorMinimo"    # F
    .param p4, "valorMaximo"    # F
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Landroid/content/Context;Ljava/util/List<Lcom/itau/empresas/feature/credito/giro/model/ValoresEmprestimoVO;>;FF)V"
        }
    .end annotation

    .line 71
    move-object v2, p2

    .line 75
    .local v2, "listValores":Ljava/util/List;, "Ljava/util/List<Lcom/itau/empresas/feature/credito/giro/model/ValoresEmprestimoVO;>;"
    if-eqz v2, :cond_1e

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x3

    if-le v0, v1, :cond_1e

    .line 77
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 79
    .local v3, "listaTemporaria":Ljava/util/List;, "Ljava/util/List<Lcom/itau/empresas/feature/credito/giro/model/ValoresEmprestimoVO;>;"
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_10
    const/4 v0, 0x3

    if-ge v4, v0, :cond_1d

    .line 81
    invoke-interface {v2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 79
    add-int/lit8 v4, v4, 0x1

    goto :goto_10

    .line 84
    .end local v4    # "i":I
    :cond_1d
    move-object v2, v3

    .line 87
    .end local v3    # "listaTemporaria":Ljava/util/List;, "Ljava/util/List<Lcom/itau/empresas/feature/credito/giro/model/ValoresEmprestimoVO;>;"
    .end local v3
    :cond_1e
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/GiroPersonalizadoValoresView;->parcelasAdapter:Lcom/itau/empresas/feature/credito/giro/adapter/GiroPersonalizadoValoresAdapter;

    invoke-virtual {v0, p1}, Lcom/itau/empresas/feature/credito/giro/adapter/GiroPersonalizadoValoresAdapter;->setContext(Landroid/content/Context;)V

    .line 88
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/GiroPersonalizadoValoresView;->parcelasAdapter:Lcom/itau/empresas/feature/credito/giro/adapter/GiroPersonalizadoValoresAdapter;

    invoke-virtual {v0, v2, p3, p4}, Lcom/itau/empresas/feature/credito/giro/adapter/GiroPersonalizadoValoresAdapter;->setData(Ljava/util/List;FF)V

    .line 89
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/GiroPersonalizadoValoresView;->listaValoresPersonalizados:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/giro/customview/GiroPersonalizadoValoresView;->parcelasAdapter:Lcom/itau/empresas/feature/credito/giro/adapter/GiroPersonalizadoValoresAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 90
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/itau/empresas/feature/credito/giro/customview/GiroPersonalizadoValoresView;->recalculateHeight(Z)V

    .line 91
    return-void
.end method

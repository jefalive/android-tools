.class public final Lcom/itau/empresas/feature/chaveacesso/ChaveAcessoUtil;
.super Ljava/lang/Object;
.source "ChaveAcessoUtil.java"


# direct methods
.method public static defineChaveAcesso(Lcom/itau/empresas/CustomApplication;Lcom/itau/empresas/api/model/OperadorVO;)V
    .registers 5
    .param p0, "application"    # Lcom/itau/empresas/CustomApplication;
    .param p1, "operadorVO"    # Lcom/itau/empresas/api/model/OperadorVO;

    .line 14
    invoke-virtual {p1}, Lcom/itau/empresas/api/model/OperadorVO;->getChaveAcesso()Ljava/util/Map;

    move-result-object v0

    if-eqz v0, :cond_28

    .line 15
    invoke-virtual {p1}, Lcom/itau/empresas/api/model/OperadorVO;->getChaveAcesso()Ljava/util/Map;

    move-result-object v0

    .line 16
    invoke-virtual {p1}, Lcom/itau/empresas/api/model/OperadorVO;->getChaveAcesso()Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Ljava/lang/String;

    .line 17
    .line 18
    .local v2, "next":Ljava/lang/String;
    invoke-static {v2}, Lcom/itau/empresas/feature/chaveacesso/ChaveAcessoValidador;->parserChaveAcesso(Ljava/lang/String;)I

    move-result v0

    .line 17
    invoke-virtual {p0, v0}, Lcom/itau/empresas/CustomApplication;->setChaveAcesso(I)V

    .line 20
    .end local v2    # "next":Ljava/lang/String;
    :cond_28
    return-void
.end method

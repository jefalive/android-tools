.class Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity$1;
.super Ljava/lang/Object;
.source "TransferenciaRecebidasDetalheActivity.java"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity;->carregaElementosDaTela()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity;


# direct methods
.method constructor <init>(Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity;)V
    .registers 2
    .param p1, "this$0"    # Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity;

    .line 94
    iput-object p1, p0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity$1;->this$0:Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onGlobalLayout()V
    .registers 3

    .line 97
    iget-object v0, p0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity$1;->this$0:Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity;

    iget-object v0, v0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity;->llTransferenciaRecebidasDetalhes:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    .line 98
    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeGlobalOnLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 101
    iget-object v0, p0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity$1;->this$0:Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity;

    iget-object v0, v0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity;->acao:Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity$Acao;

    if-eqz v0, :cond_31

    .line 102
    iget-object v0, p0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity$1;->this$0:Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity;

    iget-object v0, v0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity;->acao:Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity$Acao;

    sget-object v1, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity$Acao;->SALVAR:Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity$Acao;

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity$Acao;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_25

    .line 103
    iget-object v0, p0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity$1;->this$0:Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity;

    iget-object v0, v0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity;->botaoSalvar:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->performClick()Z

    goto :goto_2c

    .line 105
    :cond_25
    iget-object v0, p0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity$1;->this$0:Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity;

    iget-object v0, v0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity;->botaoCompartilhar:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->performClick()Z

    .line 107
    :goto_2c
    iget-object v0, p0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity$1;->this$0:Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity;->acao:Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity$Acao;

    .line 109
    :cond_31
    return-void
.end method

.class public final Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity_;
.super Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity;
.source "RecargaComprovanteActivity_.java"

# interfaces
.implements Lorg/androidannotations/api/view/HasViews;
.implements Lorg/androidannotations/api/view/OnViewChangedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity_$IntentBuilder_;
    }
.end annotation


# instance fields
.field private final onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;


# direct methods
.method public constructor <init>()V
    .registers 2

    .line 38
    invoke-direct {p0}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity;-><init>()V

    .line 42
    new-instance v0, Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-direct {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    return-void
.end method

.method static synthetic access$001(Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity_;)V
    .registers 1
    .param p0, "x0"    # Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity_;

    .line 38
    invoke-super {p0}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity;->mostraDialogoDeProgresso()V

    return-void
.end method

.method static synthetic access$101(Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity_;)V
    .registers 1
    .param p0, "x0"    # Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity_;

    .line 38
    invoke-super {p0}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity;->escondeDialogoDeProgresso()V

    return-void
.end method

.method private init_(Landroid/os/Bundle;)V
    .registers 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 57
    new-instance v0, Lcom/itau/empresas/Preferences_;

    invoke-direct {v0, p0}, Lcom/itau/empresas/Preferences_;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity_;->prefs:Lcom/itau/empresas/Preferences_;

    .line 58
    invoke-static {p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->registerOnViewChangedListener(Lorg/androidannotations/api/view/OnViewChangedListener;)V

    .line 59
    invoke-static {}, Lcom/itau/empresas/CustomApplication_;->getInstance()Lcom/itau/empresas/CustomApplication;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity_;->application:Lcom/itau/empresas/CustomApplication;

    .line 60
    invoke-static {p0}, Lcom/itau/empresas/feature/recarga/contatos/controller/ContatoRecargaController_;->getInstance_(Landroid/content/Context;)Lcom/itau/empresas/feature/recarga/contatos/controller/ContatoRecargaController_;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity_;->contatoRecargaController:Lcom/itau/empresas/feature/recarga/contatos/controller/ContatoRecargaController;

    .line 61
    invoke-direct {p0}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity_;->injectExtras_()V

    .line 62
    invoke-virtual {p0}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity_;->afterInject()V

    .line 63
    return-void
.end method

.method private injectExtras_()V
    .registers 3

    .line 179
    invoke-virtual {p0}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity_;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 180
    .local v1, "extras_":Landroid/os/Bundle;
    if-eqz v1, :cond_3c

    .line 181
    const-string v0, "nome"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 182
    const-string v0, "nome"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity_;->nome:Ljava/lang/String;

    .line 184
    :cond_1a
    const-string v0, "novo"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2a

    .line 185
    const-string v0, "novo"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity_;->novo:Z

    .line 187
    :cond_2a
    const-string v0, "resultadoEfetivacaoRecargaVO"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3c

    .line 188
    const-string v0, "resultadoEfetivacaoRecargaVO"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoEfetivacaoRecargaVO;

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity_;->resultadoEfetivacaoRecargaVO:Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoEfetivacaoRecargaVO;

    .line 191
    :cond_3c
    return-void
.end method


# virtual methods
.method public escondeDialogoDeProgresso()V
    .registers 5

    .line 216
    const-string v0, ""

    new-instance v1, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity_$8;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity_$8;-><init>(Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity_;)V

    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3}, Lorg/androidannotations/api/UiThreadExecutor;->runTask(Ljava/lang/String;Ljava/lang/Runnable;J)V

    .line 224
    return-void
.end method

.method public mostraDialogoDeProgresso()V
    .registers 5
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "CommitTransaction"
        }
    .end annotation

    .line 204
    const-string v0, ""

    new-instance v1, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity_$7;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity_$7;-><init>(Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity_;)V

    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3}, Lorg/androidannotations/api/UiThreadExecutor;->runTask(Ljava/lang/String;Ljava/lang/Runnable;J)V

    .line 212
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 49
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-static {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    move-result-object v1

    .line 50
    .local v1, "previousNotifier":Lorg/androidannotations/api/view/OnViewChangedNotifier;
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity_;->init_(Landroid/os/Bundle;)V

    .line 51
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity;->onCreate(Landroid/os/Bundle;)V

    .line 52
    invoke-static {v1}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 53
    const v0, 0x7f03005c

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity_;->setContentView(I)V

    .line 54
    return-void
.end method

.method public onViewChanged(Lorg/androidannotations/api/view/HasViews;)V
    .registers 9
    .param p1, "hasViews"    # Lorg/androidannotations/api/view/HasViews;

    .line 97
    const v0, 0x7f0e02af

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity_;->root:Landroid/widget/LinearLayout;

    .line 98
    const v0, 0x7f0e02b2

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity_;->textoOperadoraRegiao:Landroid/widget/TextView;

    .line 99
    const v0, 0x7f0e02b1

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity_;->textoNome:Landroid/widget/TextView;

    .line 100
    const v0, 0x7f0e02b4

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity_;->textoValorRecarga:Landroid/widget/TextView;

    .line 101
    const v0, 0x7f0e02b3

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity_;->textoDataRecarga:Landroid/widget/TextView;

    .line 102
    const v0, 0x7f0e02b8

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity_;->textoContatoNome:Landroid/widget/TextView;

    .line 103
    const v0, 0x7f0e02b9

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity_;->textoContatoTelefone:Landroid/widget/TextView;

    .line 104
    const v0, 0x7f0e02ba

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity_;->textoContatoLocal:Landroid/widget/TextView;

    .line 105
    const v0, 0x7f0e02b6

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity_;->llSalvarContato:Landroid/widget/LinearLayout;

    .line 106
    const v0, 0x7f0e02b0

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/ui/view/WarningView;

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity_;->warning:Lcom/itau/empresas/ui/view/WarningView;

    .line 107
    const v0, 0x7f0e00b6

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/Toolbar;

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity_;->toolBar:Landroid/support/v7/widget/Toolbar;

    .line 108
    const v0, 0x7f0e02b5

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 109
    .local v1, "view_botao_ver_detalhe":Landroid/view/View;
    const v0, 0x7f0e0116

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 110
    .local v2, "view_botao_enviar":Landroid/view/View;
    const v0, 0x7f0e02bc

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 111
    .local v3, "view_texto_nova_recarga":Landroid/view/View;
    const v0, 0x7f0e02bd

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v4

    .line 112
    .local v4, "view_texto_extrato":Landroid/view/View;
    const v0, 0x7f0e02be

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v5

    .line 113
    .local v5, "view_texto_inicio":Landroid/view/View;
    const v0, 0x7f0e02bb

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v6

    .line 115
    .local v6, "view_botao_salvar_contato":Landroid/view/View;
    if-eqz v1, :cond_ad

    .line 116
    new-instance v0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity_$1;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity_$1;-><init>(Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity_;)V

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 125
    :cond_ad
    if-eqz v2, :cond_b7

    .line 126
    new-instance v0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity_$2;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity_$2;-><init>(Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity_;)V

    invoke-virtual {v2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 135
    :cond_b7
    if-eqz v3, :cond_c1

    .line 136
    new-instance v0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity_$3;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity_$3;-><init>(Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity_;)V

    invoke-virtual {v3, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 145
    :cond_c1
    if-eqz v4, :cond_cb

    .line 146
    new-instance v0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity_$4;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity_$4;-><init>(Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity_;)V

    invoke-virtual {v4, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 155
    :cond_cb
    if-eqz v5, :cond_d5

    .line 156
    new-instance v0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity_$5;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity_$5;-><init>(Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity_;)V

    invoke-virtual {v5, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 165
    :cond_d5
    if-eqz v6, :cond_df

    .line 166
    new-instance v0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity_$6;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity_$6;-><init>(Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity_;)V

    invoke-virtual {v6, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 175
    :cond_df
    invoke-virtual {p0}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity_;->aoInicializarViews()V

    .line 176
    return-void
.end method

.method public setContentView(I)V
    .registers 3
    .param p1, "layoutResID"    # I

    .line 67
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity;->setContentView(I)V

    .line 68
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 69
    return-void
.end method

.method public setContentView(Landroid/view/View;)V
    .registers 3
    .param p1, "view"    # Landroid/view/View;

    .line 79
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity;->setContentView(Landroid/view/View;)V

    .line 80
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 81
    return-void
.end method

.method public setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .registers 4
    .param p1, "view"    # Landroid/view/View;
    .param p2, "params"    # Landroid/view/ViewGroup$LayoutParams;

    .line 73
    invoke-super {p0, p1, p2}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity;->setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 74
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 75
    return-void
.end method

.method public setIntent(Landroid/content/Intent;)V
    .registers 2
    .param p1, "newIntent"    # Landroid/content/Intent;

    .line 195
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity;->setIntent(Landroid/content/Intent;)V

    .line 196
    invoke-direct {p0}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity_;->injectExtras_()V

    .line 197
    return-void
.end method

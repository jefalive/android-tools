.class public final Lcom/itau/empresas/feature/home/EmptyStateFragment_;
.super Lcom/itau/empresas/feature/home/EmptyStateFragment;
.source "EmptyStateFragment_.java"

# interfaces
.implements Lorg/androidannotations/api/view/HasViews;
.implements Lorg/androidannotations/api/view/OnViewChangedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/itau/empresas/feature/home/EmptyStateFragment_$FragmentBuilder_;
    }
.end annotation


# instance fields
.field private contentView_:Landroid/view/View;

.field private final onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;


# direct methods
.method public constructor <init>()V
    .registers 2

    .line 22
    invoke-direct {p0}, Lcom/itau/empresas/feature/home/EmptyStateFragment;-><init>()V

    .line 26
    new-instance v0, Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-direct {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/feature/home/EmptyStateFragment_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    return-void
.end method

.method public static builder()Lcom/itau/empresas/feature/home/EmptyStateFragment_$FragmentBuilder_;
    .registers 1

    .line 76
    new-instance v0, Lcom/itau/empresas/feature/home/EmptyStateFragment_$FragmentBuilder_;

    invoke-direct {v0}, Lcom/itau/empresas/feature/home/EmptyStateFragment_$FragmentBuilder_;-><init>()V

    return-object v0
.end method

.method private init_(Landroid/os/Bundle;)V
    .registers 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 65
    invoke-static {p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->registerOnViewChangedListener(Lorg/androidannotations/api/view/OnViewChangedListener;)V

    .line 66
    invoke-direct {p0}, Lcom/itau/empresas/feature/home/EmptyStateFragment_;->injectFragmentArguments_()V

    .line 67
    return-void
.end method

.method private injectFragmentArguments_()V
    .registers 3

    .line 87
    invoke-virtual {p0}, Lcom/itau/empresas/feature/home/EmptyStateFragment_;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    .line 88
    .local v1, "args_":Landroid/os/Bundle;
    if-eqz v1, :cond_28

    .line 89
    const-string v0, "icone"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_18

    .line 90
    const-string v0, "icone"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    iput-object v0, p0, Lcom/itau/empresas/feature/home/EmptyStateFragment_;->icone:Ljava/lang/Integer;

    .line 92
    :cond_18
    const-string v0, "descricao"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_28

    .line 93
    const-string v0, "descricao"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/home/EmptyStateFragment_;->descricao:Ljava/lang/String;

    .line 96
    :cond_28
    return-void
.end method


# virtual methods
.method public findViewById(I)Landroid/view/View;
    .registers 3
    .param p1, "id"    # I

    .line 41
    iget-object v0, p0, Lcom/itau/empresas/feature/home/EmptyStateFragment_;->contentView_:Landroid/view/View;

    if-nez v0, :cond_6

    .line 42
    const/4 v0, 0x0

    return-object v0

    .line 44
    :cond_6
    iget-object v0, p0, Lcom/itau/empresas/feature/home/EmptyStateFragment_;->contentView_:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 33
    iget-object v0, p0, Lcom/itau/empresas/feature/home/EmptyStateFragment_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-static {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    move-result-object v1

    .line 34
    .local v1, "previousNotifier":Lorg/androidannotations/api/view/OnViewChangedNotifier;
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/home/EmptyStateFragment_;->init_(Landroid/os/Bundle;)V

    .line 35
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/home/EmptyStateFragment;->onCreate(Landroid/os/Bundle;)V

    .line 36
    invoke-static {v1}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 37
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .registers 6
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .line 49
    invoke-super {p0, p1, p2, p3}, Lcom/itau/empresas/feature/home/EmptyStateFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/home/EmptyStateFragment_;->contentView_:Landroid/view/View;

    .line 50
    iget-object v0, p0, Lcom/itau/empresas/feature/home/EmptyStateFragment_;->contentView_:Landroid/view/View;

    if-nez v0, :cond_14

    .line 51
    const v0, 0x7f0300a0

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/home/EmptyStateFragment_;->contentView_:Landroid/view/View;

    .line 53
    :cond_14
    iget-object v0, p0, Lcom/itau/empresas/feature/home/EmptyStateFragment_;->contentView_:Landroid/view/View;

    return-object v0
.end method

.method public onDestroyView()V
    .registers 2

    .line 58
    invoke-super {p0}, Lcom/itau/empresas/feature/home/EmptyStateFragment;->onDestroyView()V

    .line 59
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/itau/empresas/feature/home/EmptyStateFragment_;->contentView_:Landroid/view/View;

    .line 60
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/itau/empresas/feature/home/EmptyStateFragment_;->emptyStateImage:Landroid/widget/ImageView;

    .line 61
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/itau/empresas/feature/home/EmptyStateFragment_;->emptyStateText:Landroid/widget/TextView;

    .line 62
    return-void
.end method

.method public onViewChanged(Lorg/androidannotations/api/view/HasViews;)V
    .registers 3
    .param p1, "hasViews"    # Lorg/androidannotations/api/view/HasViews;

    .line 81
    const v0, 0x7f0e016d

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/itau/empresas/feature/home/EmptyStateFragment_;->emptyStateImage:Landroid/widget/ImageView;

    .line 82
    const v0, 0x7f0e016e

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/home/EmptyStateFragment_;->emptyStateText:Landroid/widget/TextView;

    .line 83
    invoke-virtual {p0}, Lcom/itau/empresas/feature/home/EmptyStateFragment_;->aoCarregarTela()V

    .line 84
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .registers 4
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;

    .line 71
    invoke-super {p0, p1, p2}, Lcom/itau/empresas/feature/home/EmptyStateFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 72
    iget-object v0, p0, Lcom/itau/empresas/feature/home/EmptyStateFragment_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 73
    return-void
.end method

.class Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity$4;
.super Ljava/lang/Object;
.source "PagamentosComCodigoBarraActivity.java"

# interfaces
.implements Lbr/com/concretesolutions/canarinho/watcher/evento/EventoDeValidacao;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity;->geraParaCampo(Lbr/com/itau/widgets/material/MaterialEditText;)Lbr/com/concretesolutions/canarinho/watcher/evento/EventoDeValidacao;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity;

.field final synthetic val$campo:Lbr/com/itau/widgets/material/MaterialEditText;


# direct methods
.method constructor <init>(Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity;Lbr/com/itau/widgets/material/MaterialEditText;)V
    .registers 3
    .param p1, "this$0"    # Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity;

    .line 310
    iput-object p1, p0, Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity$4;->this$0:Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity;

    iput-object p2, p0, Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity$4;->val$campo:Lbr/com/itau/widgets/material/MaterialEditText;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public invalido(Ljava/lang/String;Ljava/lang/String;)V
    .registers 5
    .param p1, "valorAtual"    # Ljava/lang/String;
    .param p2, "mensagem"    # Ljava/lang/String;

    .line 313
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity$4;->this$0:Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity;

    iget-object v1, p0, Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity$4;->val$campo:Lbr/com/itau/widgets/material/MaterialEditText;

    # invokes: Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity;->campoInvalido(Ljava/lang/String;Lbr/com/itau/widgets/material/MaterialEditText;)V
    invoke-static {v0, p2, v1}, Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity;->access$200(Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity;Ljava/lang/String;Lbr/com/itau/widgets/material/MaterialEditText;)V

    .line 314
    return-void
.end method

.method public parcialmenteValido(Ljava/lang/String;)V
    .registers 4
    .param p1, "valorAtual"    # Ljava/lang/String;

    .line 318
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity$4;->this$0:Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity;

    iget-object v1, p0, Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity$4;->val$campo:Lbr/com/itau/widgets/material/MaterialEditText;

    # invokes: Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity;->campoParcialmenteInvalido(Lbr/com/itau/widgets/material/MaterialEditText;)V
    invoke-static {v0, v1}, Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity;->access$300(Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity;Lbr/com/itau/widgets/material/MaterialEditText;)V

    .line 319
    return-void
.end method

.method public totalmenteValido(Ljava/lang/String;)V
    .registers 4
    .param p1, "valorAtual"    # Ljava/lang/String;

    .line 323
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity$4;->this$0:Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity;

    iget-object v1, p0, Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity$4;->val$campo:Lbr/com/itau/widgets/material/MaterialEditText;

    # invokes: Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity;->campoTotalmenteValido(Lbr/com/itau/widgets/material/MaterialEditText;)V
    invoke-static {v0, v1}, Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity;->access$400(Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity;Lbr/com/itau/widgets/material/MaterialEditText;)V

    .line 324
    return-void
.end method

.class public Lcom/itau/empresas/feature/atendimento/model/AtendimentoFAQ;
.super Ljava/lang/Object;
.source "AtendimentoFAQ.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private assunto:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "assunto"
    .end annotation
.end field

.field private canaisDisponiveis:Ljava/util/ArrayList;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "canais_disponiveis"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<Lcom/itau/empresas/feature/atendimento/model/AtendimentoCanal;>;"
        }
    .end annotation
.end field

.field private id:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "id"
    .end annotation
.end field

.field private links:Ljava/util/ArrayList;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "links"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<Lcom/itau/empresas/feature/atendimento/model/AtendimentoLink;>;"
        }
    .end annotation
.end field

.field private mobileRelacionadas:Ljava/util/ArrayList;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "mobile_relacionadas"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<Lcom/itau/empresas/feature/atendimento/model/AtendimentoRelacionadas;>;"
        }
    .end annotation
.end field

.field private pergunta:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "pergunta"
    .end annotation
.end field

.field private produto:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "produto"
    .end annotation
.end field

.field private resposta:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "resposta"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 2

    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    const-string v0, "mais perguntas"

    iput-object v0, p0, Lcom/itau/empresas/feature/atendimento/model/AtendimentoFAQ;->pergunta:Ljava/lang/String;

    .line 36
    return-void
.end method


# virtual methods
.method public getCanaisDisponiveis()Ljava/util/ArrayList;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/ArrayList<Lcom/itau/empresas/feature/atendimento/model/AtendimentoCanal;>;"
        }
    .end annotation

    .line 59
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/model/AtendimentoFAQ;->canaisDisponiveis:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getMobile_relacionadas()Ljava/util/ArrayList;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/ArrayList<Lcom/itau/empresas/feature/atendimento/model/AtendimentoRelacionadas;>;"
        }
    .end annotation

    .line 63
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/model/AtendimentoFAQ;->mobileRelacionadas:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getPergunta()Ljava/lang/String;
    .registers 2

    .line 39
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/model/AtendimentoFAQ;->pergunta:Ljava/lang/String;

    return-object v0
.end method

.method public getResposta()Ljava/lang/String;
    .registers 2

    .line 47
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/model/AtendimentoFAQ;->resposta:Ljava/lang/String;

    return-object v0
.end method

.method public setAssunto(Ljava/lang/String;)V
    .registers 2
    .param p1, "assunto"    # Ljava/lang/String;

    .line 80
    iput-object p1, p0, Lcom/itau/empresas/feature/atendimento/model/AtendimentoFAQ;->assunto:Ljava/lang/String;

    .line 81
    return-void
.end method

.method public setCanaisDisponiveis(Ljava/util/ArrayList;)V
    .registers 2
    .param p1, "canaisDisponiveis"    # Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/ArrayList<Lcom/itau/empresas/feature/atendimento/model/AtendimentoCanal;>;)V"
        }
    .end annotation

    .line 93
    iput-object p1, p0, Lcom/itau/empresas/feature/atendimento/model/AtendimentoFAQ;->canaisDisponiveis:Ljava/util/ArrayList;

    .line 94
    return-void
.end method

.method public setLinks(Ljava/util/ArrayList;)V
    .registers 2
    .param p1, "links"    # Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/ArrayList<Lcom/itau/empresas/feature/atendimento/model/AtendimentoLink;>;)V"
        }
    .end annotation

    .line 88
    iput-object p1, p0, Lcom/itau/empresas/feature/atendimento/model/AtendimentoFAQ;->links:Ljava/util/ArrayList;

    .line 89
    return-void
.end method

.method public setMobileRelacionadas(Ljava/util/ArrayList;)V
    .registers 2
    .param p1, "mobileRelacionadas"    # Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/ArrayList<Lcom/itau/empresas/feature/atendimento/model/AtendimentoRelacionadas;>;)V"
        }
    .end annotation

    .line 68
    iput-object p1, p0, Lcom/itau/empresas/feature/atendimento/model/AtendimentoFAQ;->mobileRelacionadas:Ljava/util/ArrayList;

    .line 69
    return-void
.end method

.method public setPergunta(Ljava/lang/String;)V
    .registers 2
    .param p1, "pergunta"    # Ljava/lang/String;

    .line 72
    iput-object p1, p0, Lcom/itau/empresas/feature/atendimento/model/AtendimentoFAQ;->pergunta:Ljava/lang/String;

    .line 73
    return-void
.end method

.method public setResposta(Ljava/lang/String;)V
    .registers 2
    .param p1, "resposta"    # Ljava/lang/String;

    .line 76
    iput-object p1, p0, Lcom/itau/empresas/feature/atendimento/model/AtendimentoFAQ;->resposta:Ljava/lang/String;

    .line 77
    return-void
.end method

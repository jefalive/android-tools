.class Lcom/itau/empresas/feature/credito/lis/LisController$4;
.super Ljava/lang/Object;
.source "LisController.java"

# interfaces
.implements Lbr/com/itau/sdk/android/core/type/BackendCall;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/itau/empresas/feature/credito/lis/LisController;->efetuaContratacao(Lcom/itau/empresas/feature/credito/lis/model/LisContratacaoEnvioVO;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/itau/empresas/feature/credito/lis/LisController;

.field final synthetic val$envioVO:Lcom/itau/empresas/feature/credito/lis/model/LisContratacaoEnvioVO;

.field final synthetic val$produtoLis:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/itau/empresas/feature/credito/lis/LisController;Ljava/lang/String;Lcom/itau/empresas/feature/credito/lis/model/LisContratacaoEnvioVO;)V
    .registers 4
    .param p1, "this$0"    # Lcom/itau/empresas/feature/credito/lis/LisController;

    .line 74
    iput-object p1, p0, Lcom/itau/empresas/feature/credito/lis/LisController$4;->this$0:Lcom/itau/empresas/feature/credito/lis/LisController;

    iput-object p2, p0, Lcom/itau/empresas/feature/credito/lis/LisController$4;->val$produtoLis:Ljava/lang/String;

    iput-object p3, p0, Lcom/itau/empresas/feature/credito/lis/LisController$4;->val$envioVO:Lcom/itau/empresas/feature/credito/lis/model/LisContratacaoEnvioVO;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public callBackend()V
    .registers 4

    .line 77
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/LisController$4;->this$0:Lcom/itau/empresas/feature/credito/lis/LisController;

    # invokes: Lcom/itau/empresas/feature/credito/lis/LisController;->api()Ljava/lang/Object;
    invoke-static {v0}, Lcom/itau/empresas/feature/credito/lis/LisController;->access$600(Lcom/itau/empresas/feature/credito/lis/LisController;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/api/Api;

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/lis/LisController$4;->val$produtoLis:Ljava/lang/String;

    iget-object v2, p0, Lcom/itau/empresas/feature/credito/lis/LisController$4;->val$envioVO:Lcom/itau/empresas/feature/credito/lis/model/LisContratacaoEnvioVO;

    invoke-interface {v0, v1, v2}, Lcom/itau/empresas/api/Api;->efetuaContratacaoLis(Ljava/lang/String;Lcom/itau/empresas/feature/credito/lis/model/LisContratacaoEnvioVO;)Lcom/itau/empresas/feature/credito/lis/model/LisContratacaoRespostaVO;

    move-result-object v0

    # invokes: Lcom/itau/empresas/feature/credito/lis/LisController;->post(Ljava/lang/Object;)V
    invoke-static {v0}, Lcom/itau/empresas/feature/credito/lis/LisController;->access$700(Ljava/lang/Object;)V

    .line 78
    return-void
.end method

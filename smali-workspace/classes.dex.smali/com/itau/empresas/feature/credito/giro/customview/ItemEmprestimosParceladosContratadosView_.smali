.class public final Lcom/itau/empresas/feature/credito/giro/customview/ItemEmprestimosParceladosContratadosView_;
.super Lcom/itau/empresas/feature/credito/giro/customview/ItemEmprestimosParceladosContratadosView;
.source "ItemEmprestimosParceladosContratadosView_.java"

# interfaces
.implements Lorg/androidannotations/api/view/HasViews;
.implements Lorg/androidannotations/api/view/OnViewChangedListener;


# instance fields
.field private alreadyInflated_:Z

.field private final onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .param p1, "context"    # Landroid/content/Context;

    .line 36
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/credito/giro/customview/ItemEmprestimosParceladosContratadosView;-><init>(Landroid/content/Context;)V

    .line 32
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/ItemEmprestimosParceladosContratadosView_;->alreadyInflated_:Z

    .line 33
    new-instance v0, Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-direct {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/ItemEmprestimosParceladosContratadosView_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 37
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/giro/customview/ItemEmprestimosParceladosContratadosView_;->init_()V

    .line 38
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .line 41
    invoke-direct {p0, p1, p2}, Lcom/itau/empresas/feature/credito/giro/customview/ItemEmprestimosParceladosContratadosView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 32
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/ItemEmprestimosParceladosContratadosView_;->alreadyInflated_:Z

    .line 33
    new-instance v0, Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-direct {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/ItemEmprestimosParceladosContratadosView_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 42
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/giro/customview/ItemEmprestimosParceladosContratadosView_;->init_()V

    .line 43
    return-void
.end method

.method public static build(Landroid/content/Context;)Lcom/itau/empresas/feature/credito/giro/customview/ItemEmprestimosParceladosContratadosView;
    .registers 2
    .param p0, "context"    # Landroid/content/Context;

    .line 46
    new-instance v0, Lcom/itau/empresas/feature/credito/giro/customview/ItemEmprestimosParceladosContratadosView_;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/credito/giro/customview/ItemEmprestimosParceladosContratadosView_;-><init>(Landroid/content/Context;)V

    .line 47
    .local v0, "instance":Lcom/itau/empresas/feature/credito/giro/customview/ItemEmprestimosParceladosContratadosView_;
    invoke-virtual {v0}, Lcom/itau/empresas/feature/credito/giro/customview/ItemEmprestimosParceladosContratadosView_;->onFinishInflate()V

    .line 48
    return-object v0
.end method

.method private init_()V
    .registers 3

    .line 68
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/ItemEmprestimosParceladosContratadosView_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-static {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    move-result-object v1

    .line 69
    .local v1, "previousNotifier":Lorg/androidannotations/api/view/OnViewChangedNotifier;
    invoke-static {p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->registerOnViewChangedListener(Lorg/androidannotations/api/view/OnViewChangedListener;)V

    .line 70
    invoke-static {v1}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 71
    return-void
.end method


# virtual methods
.method public onFinishInflate()V
    .registers 3

    .line 59
    iget-boolean v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/ItemEmprestimosParceladosContratadosView_;->alreadyInflated_:Z

    if-nez v0, :cond_16

    .line 60
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/ItemEmprestimosParceladosContratadosView_;->alreadyInflated_:Z

    .line 61
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/giro/customview/ItemEmprestimosParceladosContratadosView_;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0300ef

    invoke-static {v0, v1, p0}, Lcom/itau/empresas/feature/credito/giro/customview/ItemEmprestimosParceladosContratadosView_;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 62
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/ItemEmprestimosParceladosContratadosView_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 64
    :cond_16
    invoke-super {p0}, Lcom/itau/empresas/feature/credito/giro/customview/ItemEmprestimosParceladosContratadosView;->onFinishInflate()V

    .line 65
    return-void
.end method

.method public onViewChanged(Lorg/androidannotations/api/view/HasViews;)V
    .registers 4
    .param p1, "hasViews"    # Lorg/androidannotations/api/view/HasViews;

    .line 81
    const v0, 0x7f0e0519

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/ItemEmprestimosParceladosContratadosView_;->textoCreditoContratadosTitulo:Landroid/widget/TextView;

    .line 82
    const v0, 0x7f0e051b

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/ItemEmprestimosParceladosContratadosView_;->textoCreditoContratadosDataContratacao:Landroid/widget/TextView;

    .line 83
    const v0, 0x7f0e051d

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/ItemEmprestimosParceladosContratadosView_;->textoCreditoContratadosOperacao:Landroid/widget/TextView;

    .line 84
    const v0, 0x7f0e051c

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/ItemEmprestimosParceladosContratadosView_;->textoCreditoContratadosParcela:Landroid/widget/TextView;

    .line 85
    const v0, 0x7f0e051a

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/ItemEmprestimosParceladosContratadosView_;->textoCreditoContratadosValor:Landroid/widget/TextView;

    .line 86
    const v0, 0x7f0e051e

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 88
    .local v1, "view_botao_credito_contratados_mais_detalhes":Landroid/view/View;
    if-eqz v1, :cond_48

    .line 89
    new-instance v0, Lcom/itau/empresas/feature/credito/giro/customview/ItemEmprestimosParceladosContratadosView_$1;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/credito/giro/customview/ItemEmprestimosParceladosContratadosView_$1;-><init>(Lcom/itau/empresas/feature/credito/giro/customview/ItemEmprestimosParceladosContratadosView_;)V

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 98
    :cond_48
    return-void
.end method

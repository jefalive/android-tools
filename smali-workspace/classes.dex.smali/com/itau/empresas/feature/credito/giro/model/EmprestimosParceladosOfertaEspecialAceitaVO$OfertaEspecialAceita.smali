.class public Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO$OfertaEspecialAceita;
.super Ljava/lang/Object;
.source "EmprestimosParceladosOfertaEspecialAceitaVO.java"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "OfertaEspecialAceita"
.end annotation


# instance fields
.field codigoBandeiraCartao:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "codigo_bandeira_cartao"
    .end annotation
.end field

.field private codigoPlanoFinanciamento:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "codigo_plano_financiamento"
    .end annotation
.end field

.field private codigoProduto:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "codigo_produto"
    .end annotation
.end field

.field private dataContrato:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "data_contrato"
    .end annotation
.end field

.field private dataPrimeiroVencimento:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "data_primeiro_vencimento"
    .end annotation
.end field

.field private descricaoBandeiraCartao:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "descricao_bandeira_cartao"
    .end annotation
.end field

.field private indicadorSeguro:Z
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "indicador_seguro"
    .end annotation
.end field

.field private nomeProduto:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "nome_produto"
    .end annotation
.end field

.field private quantidadeDiasPrimeiroPagamento:I
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "quantidade_dias_primeiro_pagamento"
    .end annotation
.end field

.field private taxaJuros:D
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "taxa_juros"
    .end annotation
.end field

.field final synthetic this$0:Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;

.field private tipoOferta:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "tipo_oferta"
    .end annotation
.end field

.field valorOferta:Ljava/math/BigDecimal;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "valor_oferta"
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;)V
    .registers 2
    .param p1, "this$0"    # Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;

    .line 250
    iput-object p1, p0, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO$OfertaEspecialAceita;->this$0:Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$002(Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO$OfertaEspecialAceita;Ljava/lang/String;)Ljava/lang/String;
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO$OfertaEspecialAceita;
    .param p1, "x1"    # Ljava/lang/String;

    .line 250
    iput-object p1, p0, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO$OfertaEspecialAceita;->descricaoBandeiraCartao:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$102(Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO$OfertaEspecialAceita;D)D
    .registers 3
    .param p0, "x0"    # Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO$OfertaEspecialAceita;
    .param p1, "x1"    # D

    .line 250
    iput-wide p1, p0, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO$OfertaEspecialAceita;->taxaJuros:D

    return-wide p1
.end method

.method static synthetic access$202(Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO$OfertaEspecialAceita;I)I
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO$OfertaEspecialAceita;
    .param p1, "x1"    # I

    .line 250
    iput p1, p0, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO$OfertaEspecialAceita;->quantidadeDiasPrimeiroPagamento:I

    return p1
.end method

.method static synthetic access$302(Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO$OfertaEspecialAceita;Ljava/lang/String;)Ljava/lang/String;
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO$OfertaEspecialAceita;
    .param p1, "x1"    # Ljava/lang/String;

    .line 250
    iput-object p1, p0, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO$OfertaEspecialAceita;->dataContrato:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$402(Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO$OfertaEspecialAceita;Ljava/lang/String;)Ljava/lang/String;
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO$OfertaEspecialAceita;
    .param p1, "x1"    # Ljava/lang/String;

    .line 250
    iput-object p1, p0, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO$OfertaEspecialAceita;->dataPrimeiroVencimento:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$500(Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO$OfertaEspecialAceita;)Ljava/lang/String;
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO$OfertaEspecialAceita;

    .line 250
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO$OfertaEspecialAceita;->tipoOferta:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$502(Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO$OfertaEspecialAceita;Ljava/lang/String;)Ljava/lang/String;
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO$OfertaEspecialAceita;
    .param p1, "x1"    # Ljava/lang/String;

    .line 250
    iput-object p1, p0, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO$OfertaEspecialAceita;->tipoOferta:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$602(Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO$OfertaEspecialAceita;Ljava/lang/String;)Ljava/lang/String;
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO$OfertaEspecialAceita;
    .param p1, "x1"    # Ljava/lang/String;

    .line 250
    iput-object p1, p0, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO$OfertaEspecialAceita;->codigoProduto:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$702(Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO$OfertaEspecialAceita;Ljava/lang/String;)Ljava/lang/String;
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO$OfertaEspecialAceita;
    .param p1, "x1"    # Ljava/lang/String;

    .line 250
    iput-object p1, p0, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO$OfertaEspecialAceita;->nomeProduto:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$802(Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO$OfertaEspecialAceita;Ljava/lang/String;)Ljava/lang/String;
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO$OfertaEspecialAceita;
    .param p1, "x1"    # Ljava/lang/String;

    .line 250
    iput-object p1, p0, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO$OfertaEspecialAceita;->codigoPlanoFinanciamento:Ljava/lang/String;

    return-object p1
.end method

.class public Lcom/itau/empresas/ui/util/menu/WebViewActivityCommand;
.super Ljava/lang/Object;
.source "WebViewActivityCommand.java"

# interfaces
.implements Lcom/itau/empresas/ui/util/menu/Command;


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public executar(Landroid/support/v7/app/AppCompatActivity;Lcom/itau/empresas/api/model/MenuVO;)V
    .registers 5
    .param p1, "activity"    # Landroid/support/v7/app/AppCompatActivity;
    .param p2, "menu"    # Lcom/itau/empresas/api/model/MenuVO;

    .line 12
    invoke-virtual {p2}, Lcom/itau/empresas/api/model/MenuVO;->getChaveMobile()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2}, Lcom/itau/empresas/api/model/MenuVO;->getNome()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, Lcom/itau/empresas/ui/activity/WebViewActivity;->iniciarWebViewActivity(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 13
    return-void
.end method

.class final enum Lcom/itau/empresas/feature/feedback/FeedbackDialog$Estados;
.super Ljava/lang/Enum;
.source "FeedbackDialog.java"

# interfaces
.implements Lcom/itau/empresas/ui/util/MaquinaDeEstado$Chave;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/itau/empresas/feature/feedback/FeedbackDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "Estados"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<Lcom/itau/empresas/feature/feedback/FeedbackDialog$Estados;>;Lcom/itau/empresas/ui/util/MaquinaDeEstado$Chave;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/itau/empresas/feature/feedback/FeedbackDialog$Estados;

.field public static final enum DIALOGO_ABRIR_LOJA:Lcom/itau/empresas/feature/feedback/FeedbackDialog$Estados;

.field public static final enum DIALOGO_ERROR:Lcom/itau/empresas/feature/feedback/FeedbackDialog$Estados;

.field public static final enum DIALOGO_NORMAL:Lcom/itau/empresas/feature/feedback/FeedbackDialog$Estados;

.field public static final enum DIALOGO_OK:Lcom/itau/empresas/feature/feedback/FeedbackDialog$Estados;

.field public static final enum DIALOGO_OK_BETA:Lcom/itau/empresas/feature/feedback/FeedbackDialog$Estados;

.field public static final enum PREENCHIMENTO_TOTAL:Lcom/itau/empresas/feature/feedback/FeedbackDialog$Estados;

.field public static final enum PREENCHIMENTO_TOTAL_BETA:Lcom/itau/empresas/feature/feedback/FeedbackDialog$Estados;

.field public static final enum SEM_PREENCHIMENTO_TOTAL:Lcom/itau/empresas/feature/feedback/FeedbackDialog$Estados;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .line 312
    new-instance v0, Lcom/itau/empresas/feature/feedback/FeedbackDialog$Estados;

    const-string v1, "SEM_PREENCHIMENTO_TOTAL"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/itau/empresas/feature/feedback/FeedbackDialog$Estados;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/itau/empresas/feature/feedback/FeedbackDialog$Estados;->SEM_PREENCHIMENTO_TOTAL:Lcom/itau/empresas/feature/feedback/FeedbackDialog$Estados;

    new-instance v0, Lcom/itau/empresas/feature/feedback/FeedbackDialog$Estados;

    const-string v1, "PREENCHIMENTO_TOTAL"

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lcom/itau/empresas/feature/feedback/FeedbackDialog$Estados;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/itau/empresas/feature/feedback/FeedbackDialog$Estados;->PREENCHIMENTO_TOTAL:Lcom/itau/empresas/feature/feedback/FeedbackDialog$Estados;

    new-instance v0, Lcom/itau/empresas/feature/feedback/FeedbackDialog$Estados;

    const-string v1, "DIALOGO_OK"

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2}, Lcom/itau/empresas/feature/feedback/FeedbackDialog$Estados;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/itau/empresas/feature/feedback/FeedbackDialog$Estados;->DIALOGO_OK:Lcom/itau/empresas/feature/feedback/FeedbackDialog$Estados;

    new-instance v0, Lcom/itau/empresas/feature/feedback/FeedbackDialog$Estados;

    const-string v1, "DIALOGO_ABRIR_LOJA"

    const/4 v2, 0x3

    invoke-direct {v0, v1, v2}, Lcom/itau/empresas/feature/feedback/FeedbackDialog$Estados;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/itau/empresas/feature/feedback/FeedbackDialog$Estados;->DIALOGO_ABRIR_LOJA:Lcom/itau/empresas/feature/feedback/FeedbackDialog$Estados;

    new-instance v0, Lcom/itau/empresas/feature/feedback/FeedbackDialog$Estados;

    const-string v1, "DIALOGO_NORMAL"

    const/4 v2, 0x4

    invoke-direct {v0, v1, v2}, Lcom/itau/empresas/feature/feedback/FeedbackDialog$Estados;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/itau/empresas/feature/feedback/FeedbackDialog$Estados;->DIALOGO_NORMAL:Lcom/itau/empresas/feature/feedback/FeedbackDialog$Estados;

    .line 313
    new-instance v0, Lcom/itau/empresas/feature/feedback/FeedbackDialog$Estados;

    const-string v1, "DIALOGO_ERROR"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/itau/empresas/feature/feedback/FeedbackDialog$Estados;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/itau/empresas/feature/feedback/FeedbackDialog$Estados;->DIALOGO_ERROR:Lcom/itau/empresas/feature/feedback/FeedbackDialog$Estados;

    new-instance v0, Lcom/itau/empresas/feature/feedback/FeedbackDialog$Estados;

    const-string v1, "DIALOGO_OK_BETA"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/itau/empresas/feature/feedback/FeedbackDialog$Estados;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/itau/empresas/feature/feedback/FeedbackDialog$Estados;->DIALOGO_OK_BETA:Lcom/itau/empresas/feature/feedback/FeedbackDialog$Estados;

    new-instance v0, Lcom/itau/empresas/feature/feedback/FeedbackDialog$Estados;

    const-string v1, "PREENCHIMENTO_TOTAL_BETA"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/itau/empresas/feature/feedback/FeedbackDialog$Estados;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/itau/empresas/feature/feedback/FeedbackDialog$Estados;->PREENCHIMENTO_TOTAL_BETA:Lcom/itau/empresas/feature/feedback/FeedbackDialog$Estados;

    .line 311
    const/16 v0, 0x8

    new-array v0, v0, [Lcom/itau/empresas/feature/feedback/FeedbackDialog$Estados;

    sget-object v1, Lcom/itau/empresas/feature/feedback/FeedbackDialog$Estados;->SEM_PREENCHIMENTO_TOTAL:Lcom/itau/empresas/feature/feedback/FeedbackDialog$Estados;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sget-object v1, Lcom/itau/empresas/feature/feedback/FeedbackDialog$Estados;->PREENCHIMENTO_TOTAL:Lcom/itau/empresas/feature/feedback/FeedbackDialog$Estados;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    sget-object v1, Lcom/itau/empresas/feature/feedback/FeedbackDialog$Estados;->DIALOGO_OK:Lcom/itau/empresas/feature/feedback/FeedbackDialog$Estados;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    sget-object v1, Lcom/itau/empresas/feature/feedback/FeedbackDialog$Estados;->DIALOGO_ABRIR_LOJA:Lcom/itau/empresas/feature/feedback/FeedbackDialog$Estados;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    sget-object v1, Lcom/itau/empresas/feature/feedback/FeedbackDialog$Estados;->DIALOGO_NORMAL:Lcom/itau/empresas/feature/feedback/FeedbackDialog$Estados;

    const/4 v2, 0x4

    aput-object v1, v0, v2

    sget-object v1, Lcom/itau/empresas/feature/feedback/FeedbackDialog$Estados;->DIALOGO_ERROR:Lcom/itau/empresas/feature/feedback/FeedbackDialog$Estados;

    const/4 v2, 0x5

    aput-object v1, v0, v2

    sget-object v1, Lcom/itau/empresas/feature/feedback/FeedbackDialog$Estados;->DIALOGO_OK_BETA:Lcom/itau/empresas/feature/feedback/FeedbackDialog$Estados;

    const/4 v2, 0x6

    aput-object v1, v0, v2

    sget-object v1, Lcom/itau/empresas/feature/feedback/FeedbackDialog$Estados;->PREENCHIMENTO_TOTAL_BETA:Lcom/itau/empresas/feature/feedback/FeedbackDialog$Estados;

    const/4 v2, 0x7

    aput-object v1, v0, v2

    sput-object v0, Lcom/itau/empresas/feature/feedback/FeedbackDialog$Estados;->$VALUES:[Lcom/itau/empresas/feature/feedback/FeedbackDialog$Estados;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 311
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/itau/empresas/feature/feedback/FeedbackDialog$Estados;
    .registers 2
    .param p0, "name"    # Ljava/lang/String;

    .line 311
    const-class v0, Lcom/itau/empresas/feature/feedback/FeedbackDialog$Estados;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/feature/feedback/FeedbackDialog$Estados;

    return-object v0
.end method

.method public static values()[Lcom/itau/empresas/feature/feedback/FeedbackDialog$Estados;
    .registers 1

    .line 311
    sget-object v0, Lcom/itau/empresas/feature/feedback/FeedbackDialog$Estados;->$VALUES:[Lcom/itau/empresas/feature/feedback/FeedbackDialog$Estados;

    invoke-virtual {v0}, [Lcom/itau/empresas/feature/feedback/FeedbackDialog$Estados;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/itau/empresas/feature/feedback/FeedbackDialog$Estados;

    return-object v0
.end method

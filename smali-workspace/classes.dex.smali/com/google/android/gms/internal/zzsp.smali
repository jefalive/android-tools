.class public Lcom/google/android/gms/internal/zzsp;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<M:Lcom/google/android/gms/internal/zzso<TM;>;T:Ljava/lang/Object;>Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field public final tag:I

.field protected final type:I

.field protected final zzbuk:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class<TT;>;"
        }
    .end annotation
.end field

.field protected final zzbul:Z


# virtual methods
.method zzY(Ljava/lang/Object;)I
    .registers 3

    iget-boolean v0, p0, Lcom/google/android/gms/internal/zzsp;->zzbul:Z

    if-eqz v0, :cond_9

    invoke-virtual {p0, p1}, Lcom/google/android/gms/internal/zzsp;->zzZ(Ljava/lang/Object;)I

    move-result v0

    return v0

    :cond_9
    invoke-virtual {p0, p1}, Lcom/google/android/gms/internal/zzsp;->zzaa(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method protected zzZ(Ljava/lang/Object;)I
    .registers 7

    const/4 v1, 0x0

    invoke-static {p1}, Ljava/lang/reflect/Array;->getLength(Ljava/lang/Object;)I

    move-result v2

    const/4 v3, 0x0

    :goto_6
    if-ge v3, v2, :cond_1a

    invoke-static {p1, v3}, Ljava/lang/reflect/Array;->get(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v4

    if-eqz v4, :cond_17

    invoke-static {p1, v3}, Ljava/lang/reflect/Array;->get(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/internal/zzsp;->zzaa(Ljava/lang/Object;)I

    move-result v0

    add-int/2addr v1, v0

    :cond_17
    add-int/lit8 v3, v3, 0x1

    goto :goto_6

    :cond_1a
    return v1
.end method

.method zza(Ljava/lang/Object;Lcom/google/android/gms/internal/zzsn;)V
    .registers 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-boolean v0, p0, Lcom/google/android/gms/internal/zzsp;->zzbul:Z

    if-eqz v0, :cond_8

    invoke-virtual {p0, p1, p2}, Lcom/google/android/gms/internal/zzsp;->zzc(Ljava/lang/Object;Lcom/google/android/gms/internal/zzsn;)V

    goto :goto_b

    :cond_8
    invoke-virtual {p0, p1, p2}, Lcom/google/android/gms/internal/zzsp;->zzb(Ljava/lang/Object;Lcom/google/android/gms/internal/zzsn;)V

    :goto_b
    return-void
.end method

.method protected zzaa(Ljava/lang/Object;)I
    .registers 8

    iget v0, p0, Lcom/google/android/gms/internal/zzsp;->tag:I

    invoke-static {v0}, Lcom/google/android/gms/internal/zzsx;->zzmJ(I)I

    move-result v3

    iget v0, p0, Lcom/google/android/gms/internal/zzsp;->type:I

    sparse-switch v0, :sswitch_data_38

    goto :goto_1c

    :sswitch_c
    move-object v4, p1

    check-cast v4, Lcom/google/android/gms/internal/zzsu;

    invoke-static {v3, v4}, Lcom/google/android/gms/internal/zzsn;->zzb(ILcom/google/android/gms/internal/zzsu;)I

    move-result v0

    return v0

    :sswitch_14
    move-object v5, p1

    check-cast v5, Lcom/google/android/gms/internal/zzsu;

    invoke-static {v3, v5}, Lcom/google/android/gms/internal/zzsn;->zzc(ILcom/google/android/gms/internal/zzsu;)I

    move-result v0

    return v0

    :goto_1c
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown type "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/gms/internal/zzsp;->type:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    nop

    :sswitch_data_38
    .sparse-switch
        0xa -> :sswitch_c
        0xb -> :sswitch_14
    .end sparse-switch
.end method

.method protected zzb(Ljava/lang/Object;Lcom/google/android/gms/internal/zzsn;)V
    .registers 9

    :try_start_0
    iget v0, p0, Lcom/google/android/gms/internal/zzsp;->tag:I

    invoke-virtual {p2, v0}, Lcom/google/android/gms/internal/zzsn;->zzmB(I)V

    iget v0, p0, Lcom/google/android/gms/internal/zzsp;->type:I

    sparse-switch v0, :sswitch_data_48

    goto :goto_23

    :sswitch_b
    move-object v3, p1

    check-cast v3, Lcom/google/android/gms/internal/zzsu;

    iget v0, p0, Lcom/google/android/gms/internal/zzsp;->tag:I

    invoke-static {v0}, Lcom/google/android/gms/internal/zzsx;->zzmJ(I)I

    move-result v4

    invoke-virtual {p2, v3}, Lcom/google/android/gms/internal/zzsn;->zzb(Lcom/google/android/gms/internal/zzsu;)V

    const/4 v0, 0x4

    invoke-virtual {p2, v4, v0}, Lcom/google/android/gms/internal/zzsn;->zzE(II)V

    goto :goto_3e

    :sswitch_1c
    move-object v5, p1

    check-cast v5, Lcom/google/android/gms/internal/zzsu;

    invoke-virtual {p2, v5}, Lcom/google/android/gms/internal/zzsn;->zzc(Lcom/google/android/gms/internal/zzsu;)V

    goto :goto_3e

    :goto_23
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown type "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/gms/internal/zzsp;->type:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_3e
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_3e} :catch_3f

    :goto_3e
    goto :goto_46

    :catch_3f
    move-exception v3

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v0

    :goto_46
    return-void

    nop

    :sswitch_data_48
    .sparse-switch
        0xa -> :sswitch_b
        0xb -> :sswitch_1c
    .end sparse-switch
.end method

.method protected zzc(Ljava/lang/Object;Lcom/google/android/gms/internal/zzsn;)V
    .registers 7

    invoke-static {p1}, Ljava/lang/reflect/Array;->getLength(Ljava/lang/Object;)I

    move-result v1

    const/4 v2, 0x0

    :goto_5
    if-ge v2, v1, :cond_13

    invoke-static {p1, v2}, Ljava/lang/reflect/Array;->get(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_10

    invoke-virtual {p0, v3, p2}, Lcom/google/android/gms/internal/zzsp;->zzb(Ljava/lang/Object;Lcom/google/android/gms/internal/zzsn;)V

    :cond_10
    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    :cond_13
    return-void
.end method

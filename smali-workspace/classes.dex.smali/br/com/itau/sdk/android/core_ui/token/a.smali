.class final synthetic Lbr/com/itau/sdk/android/core_ui/token/a;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private final a:Lbr/com/itau/sdk/android/core_ui/token/CardPasswordDialog;

.field private final b:Landroid/widget/EditText;


# direct methods
.method private constructor <init>(Lbr/com/itau/sdk/android/core_ui/token/CardPasswordDialog;Landroid/widget/EditText;)V
    .registers 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lbr/com/itau/sdk/android/core_ui/token/a;->a:Lbr/com/itau/sdk/android/core_ui/token/CardPasswordDialog;

    iput-object p2, p0, Lbr/com/itau/sdk/android/core_ui/token/a;->b:Landroid/widget/EditText;

    return-void
.end method

.method public static a(Lbr/com/itau/sdk/android/core_ui/token/CardPasswordDialog;Landroid/widget/EditText;)Landroid/view/View$OnClickListener;
    .registers 3

    new-instance v0, Lbr/com/itau/sdk/android/core_ui/token/a;

    invoke-direct {v0, p0, p1}, Lbr/com/itau/sdk/android/core_ui/token/a;-><init>(Lbr/com/itau/sdk/android/core_ui/token/CardPasswordDialog;Landroid/widget/EditText;)V

    return-object v0
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .registers 4
    .annotation runtime Ljava/lang/invoke/LambdaForm$Hidden;
    .end annotation

    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/a;->a:Lbr/com/itau/sdk/android/core_ui/token/CardPasswordDialog;

    iget-object v1, p0, Lbr/com/itau/sdk/android/core_ui/token/a;->b:Landroid/widget/EditText;

    invoke-static {v0, v1, p1}, Lbr/com/itau/sdk/android/core_ui/token/CardPasswordDialog;->a(Lbr/com/itau/sdk/android/core_ui/token/CardPasswordDialog;Landroid/widget/EditText;Landroid/view/View;)V

    return-void
.end method

.class public Lcom/itau/empresas/feature/credito/lis/activity/LisCondicoesGeraisActivity;
.super Lcom/itau/empresas/ui/activity/BaseActivity;
.source "LisCondicoesGeraisActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field recyclerView:Landroid/support/v7/widget/RecyclerView;

.field toolBar:Landroid/support/v7/widget/Toolbar;


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 25
    invoke-direct {p0}, Lcom/itau/empresas/ui/activity/BaseActivity;-><init>()V

    return-void
.end method

.method private buildCondicoesGerais()Ljava/util/List;
    .registers 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Lcom/itau/empresas/feature/credito/lis/model/LisCondicoesGeraisVO;>;"
        }
    .end annotation

    .line 37
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 38
    .local v5, "list":Ljava/util/List;, "Ljava/util/List<Lcom/itau/empresas/feature/credito/lis/model/LisCondicoesGeraisVO;>;"
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisCondicoesGeraisActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d0002

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v6

    .line 39
    .local v6, "stringArray":[Ljava/lang/String;
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisCondicoesGeraisActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 40
    const v1, 0x7f0d0003

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v7

    .line 42
    .local v7, "stringArrayAcessibilidade":[Ljava/lang/String;
    new-instance v0, Lcom/itau/empresas/feature/credito/lis/model/LisCondicoesGeraisVO;

    const v1, 0x7f070266

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/credito/lis/activity/LisCondicoesGeraisActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 43
    const v2, 0x7f0705a5

    invoke-virtual {p0, v2}, Lcom/itau/empresas/feature/credito/lis/activity/LisCondicoesGeraisActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 44
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v3

    aget-object v3, v6, v3

    .line 45
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v4

    aget-object v4, v7, v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/itau/empresas/feature/credito/lis/model/LisCondicoesGeraisVO;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 42
    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 46
    new-instance v0, Lcom/itau/empresas/feature/credito/lis/model/LisCondicoesGeraisVO;

    const v1, 0x7f070256

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/credito/lis/activity/LisCondicoesGeraisActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 47
    const v2, 0x7f0705a6

    invoke-virtual {p0, v2}, Lcom/itau/empresas/feature/credito/lis/activity/LisCondicoesGeraisActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 48
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v3

    aget-object v3, v6, v3

    .line 49
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v4

    aget-object v4, v7, v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/itau/empresas/feature/credito/lis/model/LisCondicoesGeraisVO;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 46
    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 50
    new-instance v0, Lcom/itau/empresas/feature/credito/lis/model/LisCondicoesGeraisVO;

    const v1, 0x7f07025f

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/credito/lis/activity/LisCondicoesGeraisActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 51
    const v2, 0x7f0705a8

    invoke-virtual {p0, v2}, Lcom/itau/empresas/feature/credito/lis/activity/LisCondicoesGeraisActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 52
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v3

    aget-object v3, v6, v3

    .line 53
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v4

    aget-object v4, v7, v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/itau/empresas/feature/credito/lis/model/LisCondicoesGeraisVO;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 50
    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 54
    new-instance v0, Lcom/itau/empresas/feature/credito/lis/model/LisCondicoesGeraisVO;

    const v1, 0x7f070260

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/credito/lis/activity/LisCondicoesGeraisActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 55
    const v2, 0x7f0705a2

    invoke-virtual {p0, v2}, Lcom/itau/empresas/feature/credito/lis/activity/LisCondicoesGeraisActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 56
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v3

    aget-object v3, v6, v3

    .line 57
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v4

    aget-object v4, v7, v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/itau/empresas/feature/credito/lis/model/LisCondicoesGeraisVO;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 54
    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 58
    new-instance v0, Lcom/itau/empresas/feature/credito/lis/model/LisCondicoesGeraisVO;

    const v1, 0x7f070257

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/credito/lis/activity/LisCondicoesGeraisActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 59
    const v2, 0x7f0705a1

    invoke-virtual {p0, v2}, Lcom/itau/empresas/feature/credito/lis/activity/LisCondicoesGeraisActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 60
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v3

    aget-object v3, v6, v3

    .line 61
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v4

    aget-object v4, v7, v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/itau/empresas/feature/credito/lis/model/LisCondicoesGeraisVO;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 58
    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 62
    new-instance v0, Lcom/itau/empresas/feature/credito/lis/model/LisCondicoesGeraisVO;

    const v1, 0x7f070255

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/credito/lis/activity/LisCondicoesGeraisActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 63
    const v2, 0x7f0705a7

    invoke-virtual {p0, v2}, Lcom/itau/empresas/feature/credito/lis/activity/LisCondicoesGeraisActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 64
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v3

    aget-object v3, v6, v3

    .line 65
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v4

    aget-object v4, v7, v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/itau/empresas/feature/credito/lis/model/LisCondicoesGeraisVO;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 62
    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 66
    new-instance v0, Lcom/itau/empresas/feature/credito/lis/model/LisCondicoesGeraisVO;

    const v1, 0x7f07024e

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/credito/lis/activity/LisCondicoesGeraisActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 67
    const v2, 0x7f0705a9

    invoke-virtual {p0, v2}, Lcom/itau/empresas/feature/credito/lis/activity/LisCondicoesGeraisActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 68
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v3

    aget-object v3, v6, v3

    .line 69
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v4

    aget-object v4, v7, v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/itau/empresas/feature/credito/lis/model/LisCondicoesGeraisVO;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 66
    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 70
    new-instance v0, Lcom/itau/empresas/feature/credito/lis/model/LisCondicoesGeraisVO;

    const v1, 0x7f07028a

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/credito/lis/activity/LisCondicoesGeraisActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 71
    const v2, 0x7f0705a3

    invoke-virtual {p0, v2}, Lcom/itau/empresas/feature/credito/lis/activity/LisCondicoesGeraisActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 72
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v3

    aget-object v3, v6, v3

    .line 73
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v4

    aget-object v4, v7, v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/itau/empresas/feature/credito/lis/model/LisCondicoesGeraisVO;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 70
    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 74
    new-instance v0, Lcom/itau/empresas/feature/credito/lis/model/LisCondicoesGeraisVO;

    const v1, 0x7f07026e

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/credito/lis/activity/LisCondicoesGeraisActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 75
    const v2, 0x7f0705a4

    invoke-virtual {p0, v2}, Lcom/itau/empresas/feature/credito/lis/activity/LisCondicoesGeraisActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 76
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v3

    aget-object v3, v6, v3

    .line 77
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v4

    aget-object v4, v7, v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/itau/empresas/feature/credito/lis/model/LisCondicoesGeraisVO;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 74
    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 79
    return-object v5
.end method

.method private configuraRecycler(Ljava/util/List;)V
    .registers 4
    .param p1, "list"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<Lcom/itau/empresas/feature/credito/lis/model/LisCondicoesGeraisVO;>;)V"
        }
    .end annotation

    .line 83
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisCondicoesGeraisActivity;->recyclerView:Landroid/support/v7/widget/RecyclerView;

    new-instance v1, Lcom/itau/empresas/feature/credito/lis/adapter/LisCondicoesGeraisAdapter;

    invoke-direct {v1, p1}, Lcom/itau/empresas/feature/credito/lis/adapter/LisCondicoesGeraisAdapter;-><init>(Ljava/util/List;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 84
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisCondicoesGeraisActivity;->recyclerView:Landroid/support/v7/widget/RecyclerView;

    new-instance v1, Landroid/support/v7/widget/LinearLayoutManager;

    invoke-direct {v1, p0}, Landroid/support/v7/widget/LinearLayoutManager;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(Landroid/support/v7/widget/RecyclerView$LayoutManager;)V

    .line 85
    return-void
.end method

.method private constroiToolbar()V
    .registers 3

    .line 88
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisCondicoesGeraisActivity;->toolBar:Landroid/support/v7/widget/Toolbar;

    invoke-static {v0, p0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom;->Builder(Landroid/support/v7/widget/Toolbar;Landroid/support/v7/app/AppCompatActivity;)Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    move-result-object v0

    .line 89
    const v1, 0x7f07040a

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->comTitulo(I)Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    move-result-object v0

    .line 90
    invoke-virtual {v0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->comClickListenerFinish()Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    move-result-object v0

    .line 91
    invoke-virtual {v0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->build()Lcom/itau/empresas/ui/util/listener/ToolbarCustom;

    .line 92
    return-void
.end method

.method public static intent(Landroid/content/Context;)Lcom/itau/empresas/feature/credito/lis/activity/LisCondicoesGeraisActivity_$IntentBuilder_;
    .registers 2
    .param p0, "context"    # Landroid/content/Context;

    .line 105
    new-instance v0, Lcom/itau/empresas/feature/credito/lis/activity/LisCondicoesGeraisActivity_$IntentBuilder_;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisCondicoesGeraisActivity_$IntentBuilder_;-><init>(Landroid/content/Context;)V

    return-object v0
.end method


# virtual methods
.method aoCarregarTela()V
    .registers 2

    .line 32
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisCondicoesGeraisActivity;->constroiToolbar()V

    .line 33
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisCondicoesGeraisActivity;->buildCondicoesGerais()Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/itau/empresas/feature/credito/lis/activity/LisCondicoesGeraisActivity;->configuraRecycler(Ljava/util/List;)V

    .line 34
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .registers 2
    .param p1, "v"    # Landroid/view/View;

    .line 101
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisCondicoesGeraisActivity;->onBackPressed()V

    .line 102
    return-void
.end method

.method public opkeysConsumidas()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Ljava/lang/String;>;"
        }
    .end annotation

    .line 96
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.class public final enum Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;
.super Ljava/lang/Enum;
.source "Easing.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "EasingOption"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;>;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;

.field public static final enum EaseInBack:Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;

.field public static final enum EaseInBounce:Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;

.field public static final enum EaseInCirc:Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;

.field public static final enum EaseInCubic:Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;

.field public static final enum EaseInElastic:Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;

.field public static final enum EaseInExpo:Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;

.field public static final enum EaseInOutBack:Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;

.field public static final enum EaseInOutBounce:Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;

.field public static final enum EaseInOutCirc:Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;

.field public static final enum EaseInOutCubic:Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;

.field public static final enum EaseInOutElastic:Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;

.field public static final enum EaseInOutExpo:Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;

.field public static final enum EaseInOutQuad:Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;

.field public static final enum EaseInOutQuart:Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;

.field public static final enum EaseInOutSine:Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;

.field public static final enum EaseInQuad:Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;

.field public static final enum EaseInQuart:Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;

.field public static final enum EaseInSine:Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;

.field public static final enum EaseOutBack:Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;

.field public static final enum EaseOutBounce:Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;

.field public static final enum EaseOutCirc:Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;

.field public static final enum EaseOutCubic:Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;

.field public static final enum EaseOutElastic:Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;

.field public static final enum EaseOutExpo:Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;

.field public static final enum EaseOutQuad:Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;

.field public static final enum EaseOutQuart:Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;

.field public static final enum EaseOutSine:Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;

.field public static final enum Linear:Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .line 15
    new-instance v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;

    const-string v1, "Linear"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;->Linear:Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;

    .line 16
    new-instance v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;

    const-string v1, "EaseInQuad"

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;->EaseInQuad:Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;

    .line 17
    new-instance v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;

    const-string v1, "EaseOutQuad"

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;->EaseOutQuad:Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;

    .line 18
    new-instance v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;

    const-string v1, "EaseInOutQuad"

    const/4 v2, 0x3

    invoke-direct {v0, v1, v2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;->EaseInOutQuad:Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;

    .line 19
    new-instance v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;

    const-string v1, "EaseInCubic"

    const/4 v2, 0x4

    invoke-direct {v0, v1, v2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;->EaseInCubic:Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;

    .line 20
    new-instance v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;

    const-string v1, "EaseOutCubic"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;->EaseOutCubic:Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;

    .line 21
    new-instance v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;

    const-string v1, "EaseInOutCubic"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;->EaseInOutCubic:Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;

    .line 22
    new-instance v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;

    const-string v1, "EaseInQuart"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;->EaseInQuart:Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;

    .line 23
    new-instance v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;

    const-string v1, "EaseOutQuart"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;->EaseOutQuart:Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;

    .line 24
    new-instance v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;

    const-string v1, "EaseInOutQuart"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;->EaseInOutQuart:Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;

    .line 25
    new-instance v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;

    const-string v1, "EaseInSine"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;->EaseInSine:Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;

    .line 26
    new-instance v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;

    const-string v1, "EaseOutSine"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;->EaseOutSine:Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;

    .line 27
    new-instance v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;

    const-string v1, "EaseInOutSine"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;->EaseInOutSine:Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;

    .line 28
    new-instance v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;

    const-string v1, "EaseInExpo"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;->EaseInExpo:Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;

    .line 29
    new-instance v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;

    const-string v1, "EaseOutExpo"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;->EaseOutExpo:Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;

    .line 30
    new-instance v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;

    const-string v1, "EaseInOutExpo"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;->EaseInOutExpo:Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;

    .line 31
    new-instance v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;

    const-string v1, "EaseInCirc"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;->EaseInCirc:Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;

    .line 32
    new-instance v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;

    const-string v1, "EaseOutCirc"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;->EaseOutCirc:Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;

    .line 33
    new-instance v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;

    const-string v1, "EaseInOutCirc"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;->EaseInOutCirc:Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;

    .line 34
    new-instance v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;

    const-string v1, "EaseInElastic"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;->EaseInElastic:Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;

    .line 35
    new-instance v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;

    const-string v1, "EaseOutElastic"

    const/16 v2, 0x14

    invoke-direct {v0, v1, v2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;->EaseOutElastic:Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;

    .line 36
    new-instance v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;

    const-string v1, "EaseInOutElastic"

    const/16 v2, 0x15

    invoke-direct {v0, v1, v2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;->EaseInOutElastic:Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;

    .line 37
    new-instance v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;

    const-string v1, "EaseInBack"

    const/16 v2, 0x16

    invoke-direct {v0, v1, v2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;->EaseInBack:Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;

    .line 38
    new-instance v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;

    const-string v1, "EaseOutBack"

    const/16 v2, 0x17

    invoke-direct {v0, v1, v2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;->EaseOutBack:Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;

    .line 39
    new-instance v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;

    const-string v1, "EaseInOutBack"

    const/16 v2, 0x18

    invoke-direct {v0, v1, v2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;->EaseInOutBack:Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;

    .line 40
    new-instance v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;

    const-string v1, "EaseInBounce"

    const/16 v2, 0x19

    invoke-direct {v0, v1, v2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;->EaseInBounce:Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;

    .line 41
    new-instance v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;

    const-string v1, "EaseOutBounce"

    const/16 v2, 0x1a

    invoke-direct {v0, v1, v2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;->EaseOutBounce:Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;

    .line 42
    new-instance v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;

    const-string v1, "EaseInOutBounce"

    const/16 v2, 0x1b

    invoke-direct {v0, v1, v2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;->EaseInOutBounce:Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;

    .line 14
    const/16 v0, 0x1c

    new-array v0, v0, [Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;

    sget-object v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;->Linear:Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sget-object v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;->EaseInQuad:Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    sget-object v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;->EaseOutQuad:Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    sget-object v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;->EaseInOutQuad:Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    sget-object v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;->EaseInCubic:Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;

    const/4 v2, 0x4

    aput-object v1, v0, v2

    sget-object v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;->EaseOutCubic:Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;

    const/4 v2, 0x5

    aput-object v1, v0, v2

    sget-object v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;->EaseInOutCubic:Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;

    const/4 v2, 0x6

    aput-object v1, v0, v2

    sget-object v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;->EaseInQuart:Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;

    const/4 v2, 0x7

    aput-object v1, v0, v2

    sget-object v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;->EaseOutQuart:Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;

    const/16 v2, 0x8

    aput-object v1, v0, v2

    sget-object v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;->EaseInOutQuart:Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;

    const/16 v2, 0x9

    aput-object v1, v0, v2

    sget-object v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;->EaseInSine:Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;

    const/16 v2, 0xa

    aput-object v1, v0, v2

    sget-object v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;->EaseOutSine:Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;

    const/16 v2, 0xb

    aput-object v1, v0, v2

    sget-object v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;->EaseInOutSine:Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;

    const/16 v2, 0xc

    aput-object v1, v0, v2

    sget-object v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;->EaseInExpo:Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;

    const/16 v2, 0xd

    aput-object v1, v0, v2

    sget-object v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;->EaseOutExpo:Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;

    const/16 v2, 0xe

    aput-object v1, v0, v2

    sget-object v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;->EaseInOutExpo:Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;

    const/16 v2, 0xf

    aput-object v1, v0, v2

    sget-object v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;->EaseInCirc:Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;

    const/16 v2, 0x10

    aput-object v1, v0, v2

    sget-object v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;->EaseOutCirc:Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;

    const/16 v2, 0x11

    aput-object v1, v0, v2

    sget-object v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;->EaseInOutCirc:Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;

    const/16 v2, 0x12

    aput-object v1, v0, v2

    sget-object v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;->EaseInElastic:Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;

    const/16 v2, 0x13

    aput-object v1, v0, v2

    sget-object v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;->EaseOutElastic:Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;

    const/16 v2, 0x14

    aput-object v1, v0, v2

    sget-object v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;->EaseInOutElastic:Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;

    const/16 v2, 0x15

    aput-object v1, v0, v2

    sget-object v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;->EaseInBack:Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;

    const/16 v2, 0x16

    aput-object v1, v0, v2

    sget-object v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;->EaseOutBack:Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;

    const/16 v2, 0x17

    aput-object v1, v0, v2

    sget-object v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;->EaseInOutBack:Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;

    const/16 v2, 0x18

    aput-object v1, v0, v2

    sget-object v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;->EaseInBounce:Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;

    const/16 v2, 0x19

    aput-object v1, v0, v2

    sget-object v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;->EaseOutBounce:Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;

    const/16 v2, 0x1a

    aput-object v1, v0, v2

    sget-object v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;->EaseInOutBounce:Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;

    const/16 v2, 0x1b

    aput-object v1, v0, v2

    sput-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;->$VALUES:[Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 14
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;
    .registers 2
    .param p0, "name"    # Ljava/lang/String;

    .line 14
    const-class v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;

    return-object v0
.end method

.method public static values()[Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;
    .registers 1

    .line 14
    sget-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;->$VALUES:[Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;

    invoke-virtual {v0}, [Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;

    return-object v0
.end method

.class public final Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragment_;
.super Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragment;
.source "ExtratoFragment_.java"

# interfaces
.implements Lorg/androidannotations/api/view/HasViews;
.implements Lorg/androidannotations/api/view/OnViewChangedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragment_$FragmentBuilder_;
    }
.end annotation


# instance fields
.field private contentView_:Landroid/view/View;

.field private final onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;


# direct methods
.method public constructor <init>()V
    .registers 2

    .line 21
    invoke-direct {p0}, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragment;-><init>()V

    .line 25
    new-instance v0, Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-direct {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragment_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    return-void
.end method

.method public static builder()Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragment_$FragmentBuilder_;
    .registers 1

    .line 71
    new-instance v0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragment_$FragmentBuilder_;

    invoke-direct {v0}, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragment_$FragmentBuilder_;-><init>()V

    return-object v0
.end method

.method private init_(Landroid/os/Bundle;)V
    .registers 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 61
    invoke-static {p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->registerOnViewChangedListener(Lorg/androidannotations/api/view/OnViewChangedListener;)V

    .line 62
    return-void
.end method


# virtual methods
.method public findViewById(I)Landroid/view/View;
    .registers 3
    .param p1, "id"    # I

    .line 38
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragment_;->contentView_:Landroid/view/View;

    if-nez v0, :cond_6

    .line 39
    const/4 v0, 0x0

    return-object v0

    .line 41
    :cond_6
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragment_;->contentView_:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 30
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragment_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-static {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    move-result-object v1

    .line 31
    .local v1, "previousNotifier":Lorg/androidannotations/api/view/OnViewChangedNotifier;
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragment_;->init_(Landroid/os/Bundle;)V

    .line 32
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragment;->onCreate(Landroid/os/Bundle;)V

    .line 33
    invoke-static {v1}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 34
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .registers 6
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .line 46
    invoke-super {p0, p1, p2, p3}, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragment_;->contentView_:Landroid/view/View;

    .line 47
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragment_;->contentView_:Landroid/view/View;

    if-nez v0, :cond_14

    .line 48
    const v0, 0x7f0300a2

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragment_;->contentView_:Landroid/view/View;

    .line 50
    :cond_14
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragment_;->contentView_:Landroid/view/View;

    return-object v0
.end method

.method public onDestroyView()V
    .registers 2

    .line 55
    invoke-super {p0}, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragment;->onDestroyView()V

    .line 56
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragment_;->contentView_:Landroid/view/View;

    .line 57
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragment_;->rvExtrato:Landroid/support/v7/widget/RecyclerView;

    .line 58
    return-void
.end method

.method public onViewChanged(Lorg/androidannotations/api/view/HasViews;)V
    .registers 3
    .param p1, "hasViews"    # Lorg/androidannotations/api/view/HasViews;

    .line 76
    const v0, 0x7f0e0411

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    iput-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragment_;->rvExtrato:Landroid/support/v7/widget/RecyclerView;

    .line 77
    invoke-virtual {p0}, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragment_;->afterViews()V

    .line 78
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .registers 4
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;

    .line 66
    invoke-super {p0, p1, p2}, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 67
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragment_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 68
    return-void
.end method

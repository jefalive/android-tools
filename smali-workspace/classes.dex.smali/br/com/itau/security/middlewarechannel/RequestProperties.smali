.class public final Lbr/com/itau/security/middlewarechannel/RequestProperties;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;

.field private final e:Ljava/lang/String;

.field private final f:I

.field private final g:I


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 7

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    iput-object p1, p0, Lbr/com/itau/security/middlewarechannel/RequestProperties;->a:Ljava/lang/String;

    .line 15
    iput-object p2, p0, Lbr/com/itau/security/middlewarechannel/RequestProperties;->b:Ljava/lang/String;

    .line 16
    iput-object p3, p0, Lbr/com/itau/security/middlewarechannel/RequestProperties;->d:Ljava/lang/String;

    .line 17
    iput-object p4, p0, Lbr/com/itau/security/middlewarechannel/RequestProperties;->c:Ljava/lang/String;

    .line 18
    iput-object p5, p0, Lbr/com/itau/security/middlewarechannel/RequestProperties;->e:Ljava/lang/String;

    .line 19
    const v0, 0x1d4c0

    iput v0, p0, Lbr/com/itau/security/middlewarechannel/RequestProperties;->f:I

    .line 20
    const v0, 0x1d4c0

    iput v0, p0, Lbr/com/itau/security/middlewarechannel/RequestProperties;->g:I

    .line 21
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V
    .registers 8

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lbr/com/itau/security/middlewarechannel/RequestProperties;->a:Ljava/lang/String;

    .line 25
    iput-object p2, p0, Lbr/com/itau/security/middlewarechannel/RequestProperties;->b:Ljava/lang/String;

    .line 26
    iput-object p3, p0, Lbr/com/itau/security/middlewarechannel/RequestProperties;->d:Ljava/lang/String;

    .line 27
    iput-object p4, p0, Lbr/com/itau/security/middlewarechannel/RequestProperties;->c:Ljava/lang/String;

    .line 28
    iput-object p5, p0, Lbr/com/itau/security/middlewarechannel/RequestProperties;->e:Ljava/lang/String;

    .line 29
    iput p6, p0, Lbr/com/itau/security/middlewarechannel/RequestProperties;->f:I

    .line 30
    iput p7, p0, Lbr/com/itau/security/middlewarechannel/RequestProperties;->g:I

    .line 31
    return-void
.end method


# virtual methods
.method public final getAuthToken()Ljava/lang/String;
    .registers 2

    .line 50
    iget-object v0, p0, Lbr/com/itau/security/middlewarechannel/RequestProperties;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final getConnectTimeout()I
    .registers 2

    .line 58
    iget v0, p0, Lbr/com/itau/security/middlewarechannel/RequestProperties;->g:I

    return v0
.end method

.method public final getJson()Ljava/lang/String;
    .registers 2

    .line 34
    iget-object v0, p0, Lbr/com/itau/security/middlewarechannel/RequestProperties;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final getOperation()Ljava/lang/String;
    .registers 2

    .line 38
    iget-object v0, p0, Lbr/com/itau/security/middlewarechannel/RequestProperties;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final getReadTimeout()I
    .registers 2

    .line 54
    iget v0, p0, Lbr/com/itau/security/middlewarechannel/RequestProperties;->f:I

    return v0
.end method

.method public final getUserAgent()Ljava/lang/String;
    .registers 2

    .line 46
    iget-object v0, p0, Lbr/com/itau/security/middlewarechannel/RequestProperties;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final getUserID()Ljava/lang/String;
    .registers 2

    .line 42
    iget-object v0, p0, Lbr/com/itau/security/middlewarechannel/RequestProperties;->d:Ljava/lang/String;

    return-object v0
.end method

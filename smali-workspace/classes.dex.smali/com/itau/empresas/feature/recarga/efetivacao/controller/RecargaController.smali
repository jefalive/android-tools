.class public Lcom/itau/empresas/feature/recarga/efetivacao/controller/RecargaController;
.super Lcom/itau/empresas/controller/BaseController;
.source "RecargaController.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/itau/empresas/controller/BaseController<Lcom/itau/empresas/api/Api;>;"
    }
.end annotation


# static fields
.field private static recargaOperadoraVOList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Lcom/itau/empresas/feature/recarga/contatos/model/RecargaOperadoraVO;>;"
        }
    .end annotation
.end field


# instance fields
.field app:Lcom/itau/empresas/CustomApplication;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .line 23
    const/4 v0, 0x0

    sput-object v0, Lcom/itau/empresas/feature/recarga/efetivacao/controller/RecargaController;->recargaOperadoraVOList:Ljava/util/List;

    return-void
.end method

.method public constructor <init>()V
    .registers 1

    .line 19
    invoke-direct {p0}, Lcom/itau/empresas/controller/BaseController;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/itau/empresas/feature/recarga/efetivacao/controller/RecargaController;)Ljava/lang/Object;
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/feature/recarga/efetivacao/controller/RecargaController;

    .line 19
    invoke-virtual {p0}, Lcom/itau/empresas/feature/recarga/efetivacao/controller/RecargaController;->api()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100(Ljava/util/List;)V
    .registers 1
    .param p0, "x0"    # Ljava/util/List;

    .line 19
    invoke-static {p0}, Lcom/itau/empresas/feature/recarga/efetivacao/controller/RecargaController;->setRecargaOperadoraVOList(Ljava/util/List;)V

    return-void
.end method

.method static synthetic access$1000(Lcom/itau/empresas/feature/recarga/efetivacao/controller/RecargaController;)Ljava/lang/Object;
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/feature/recarga/efetivacao/controller/RecargaController;

    .line 19
    invoke-virtual {p0}, Lcom/itau/empresas/feature/recarga/efetivacao/controller/RecargaController;->api()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1100(Ljava/lang/Object;)V
    .registers 1
    .param p0, "x0"    # Ljava/lang/Object;

    .line 19
    invoke-static {p0}, Lcom/itau/empresas/feature/recarga/efetivacao/controller/RecargaController;->post(Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic access$200()Ljava/util/List;
    .registers 1

    .line 19
    sget-object v0, Lcom/itau/empresas/feature/recarga/efetivacao/controller/RecargaController;->recargaOperadoraVOList:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$300(Ljava/lang/Object;)V
    .registers 1
    .param p0, "x0"    # Ljava/lang/Object;

    .line 19
    invoke-static {p0}, Lcom/itau/empresas/feature/recarga/efetivacao/controller/RecargaController;->post(Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic access$400(Lcom/itau/empresas/feature/recarga/efetivacao/controller/RecargaController;)Ljava/lang/Object;
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/feature/recarga/efetivacao/controller/RecargaController;

    .line 19
    invoke-virtual {p0}, Lcom/itau/empresas/feature/recarga/efetivacao/controller/RecargaController;->api()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$500(Ljava/lang/Object;)V
    .registers 1
    .param p0, "x0"    # Ljava/lang/Object;

    .line 19
    invoke-static {p0}, Lcom/itau/empresas/feature/recarga/efetivacao/controller/RecargaController;->post(Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic access$600(Lcom/itau/empresas/feature/recarga/efetivacao/controller/RecargaController;)Ljava/lang/Object;
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/feature/recarga/efetivacao/controller/RecargaController;

    .line 19
    invoke-virtual {p0}, Lcom/itau/empresas/feature/recarga/efetivacao/controller/RecargaController;->api()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$700(Ljava/lang/Object;)V
    .registers 1
    .param p0, "x0"    # Ljava/lang/Object;

    .line 19
    invoke-static {p0}, Lcom/itau/empresas/feature/recarga/efetivacao/controller/RecargaController;->post(Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic access$800(Lcom/itau/empresas/feature/recarga/efetivacao/controller/RecargaController;)Ljava/lang/Object;
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/feature/recarga/efetivacao/controller/RecargaController;

    .line 19
    invoke-virtual {p0}, Lcom/itau/empresas/feature/recarga/efetivacao/controller/RecargaController;->api()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$900(Ljava/lang/Object;)V
    .registers 1
    .param p0, "x0"    # Ljava/lang/Object;

    .line 19
    invoke-static {p0}, Lcom/itau/empresas/feature/recarga/efetivacao/controller/RecargaController;->post(Ljava/lang/Object;)V

    return-void
.end method

.method private static setRecargaOperadoraVOList(Ljava/util/List;)V
    .registers 1
    .param p0, "recargaOperadoraVOList"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<Lcom/itau/empresas/feature/recarga/contatos/model/RecargaOperadoraVO;>;)V"
        }
    .end annotation

    .line 44
    sput-object p0, Lcom/itau/empresas/feature/recarga/efetivacao/controller/RecargaController;->recargaOperadoraVOList:Ljava/util/List;

    .line 45
    return-void
.end method


# virtual methods
.method public buscaDetalheOperadoraRecarga(Ljava/lang/String;Ljava/lang/String;)V
    .registers 4
    .param p1, "idOperadora"    # Ljava/lang/String;
    .param p2, "nomeRegiao"    # Ljava/lang/String;

    .line 50
    new-instance v0, Lcom/itau/empresas/feature/recarga/efetivacao/controller/RecargaController$2;

    invoke-direct {v0, p0, p1, p2}, Lcom/itau/empresas/feature/recarga/efetivacao/controller/RecargaController$2;-><init>(Lcom/itau/empresas/feature/recarga/efetivacao/controller/RecargaController;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Lbr/com/itau/sdk/android/core/BackendClient;->callApi(Lbr/com/itau/sdk/android/core/type/BackendCall;)V

    .line 64
    return-void
.end method

.method public buscaOperadorasRecarga(Ljava/lang/String;)V
    .registers 3
    .param p1, "finalidade"    # Ljava/lang/String;

    .line 30
    sget-object v0, Lcom/itau/empresas/feature/recarga/efetivacao/controller/RecargaController;->recargaOperadoraVOList:Ljava/util/List;

    if-eqz v0, :cond_c

    sget-object v0, Lcom/itau/empresas/feature/recarga/efetivacao/controller/RecargaController;->recargaOperadoraVOList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_15

    .line 31
    :cond_c
    new-instance v0, Lcom/itau/empresas/feature/recarga/efetivacao/controller/RecargaController$1;

    invoke-direct {v0, p0, p1}, Lcom/itau/empresas/feature/recarga/efetivacao/controller/RecargaController$1;-><init>(Lcom/itau/empresas/feature/recarga/efetivacao/controller/RecargaController;Ljava/lang/String;)V

    invoke-static {v0}, Lbr/com/itau/sdk/android/core/BackendClient;->callApi(Lbr/com/itau/sdk/android/core/type/BackendCall;)V

    goto :goto_1a

    .line 39
    :cond_15
    sget-object v0, Lcom/itau/empresas/feature/recarga/efetivacao/controller/RecargaController;->recargaOperadoraVOList:Ljava/util/List;

    invoke-static {v0}, Lcom/itau/empresas/feature/recarga/efetivacao/controller/RecargaController;->post(Ljava/lang/Object;)V

    .line 41
    :goto_1a
    return-void
.end method

.method public efetuaRecarga(Ljava/lang/String;Lcom/itau/empresas/feature/recarga/contatos/model/RecargaInputVO;)V
    .registers 4
    .param p1, "idTelefone"    # Ljava/lang/String;
    .param p2, "recargaInputVO"    # Lcom/itau/empresas/feature/recarga/contatos/model/RecargaInputVO;

    .line 86
    new-instance v0, Lcom/itau/empresas/feature/recarga/efetivacao/controller/RecargaController$5;

    invoke-direct {v0, p0, p1, p2}, Lcom/itau/empresas/feature/recarga/efetivacao/controller/RecargaController$5;-><init>(Lcom/itau/empresas/feature/recarga/efetivacao/controller/RecargaController;Ljava/lang/String;Lcom/itau/empresas/feature/recarga/contatos/model/RecargaInputVO;)V

    invoke-static {v0}, Lbr/com/itau/sdk/android/core/BackendClient;->callApi(Lbr/com/itau/sdk/android/core/type/BackendCall;)V

    .line 92
    return-void
.end method

.method public simulaRecarga(Ljava/lang/String;Lcom/itau/empresas/feature/recarga/contatos/model/RecargaInputVO;)V
    .registers 4
    .param p1, "idTelefone"    # Ljava/lang/String;
    .param p2, "recargaInputVO"    # Lcom/itau/empresas/feature/recarga/contatos/model/RecargaInputVO;

    .line 67
    new-instance v0, Lcom/itau/empresas/feature/recarga/efetivacao/controller/RecargaController$3;

    invoke-direct {v0, p0, p1, p2}, Lcom/itau/empresas/feature/recarga/efetivacao/controller/RecargaController$3;-><init>(Lcom/itau/empresas/feature/recarga/efetivacao/controller/RecargaController;Ljava/lang/String;Lcom/itau/empresas/feature/recarga/contatos/model/RecargaInputVO;)V

    invoke-static {v0}, Lbr/com/itau/sdk/android/core/BackendClient;->callApi(Lbr/com/itau/sdk/android/core/type/BackendCall;)V

    .line 73
    return-void
.end method

.method public simulaRecargaAutorizar(Ljava/lang/String;Lcom/itau/empresas/feature/recarga/contatos/model/RecargaInputVO;)V
    .registers 4
    .param p1, "idTelefone"    # Ljava/lang/String;
    .param p2, "recargaInputVO"    # Lcom/itau/empresas/feature/recarga/contatos/model/RecargaInputVO;

    .line 77
    new-instance v0, Lcom/itau/empresas/feature/recarga/efetivacao/controller/RecargaController$4;

    invoke-direct {v0, p0, p1, p2}, Lcom/itau/empresas/feature/recarga/efetivacao/controller/RecargaController$4;-><init>(Lcom/itau/empresas/feature/recarga/efetivacao/controller/RecargaController;Ljava/lang/String;Lcom/itau/empresas/feature/recarga/contatos/model/RecargaInputVO;)V

    invoke-static {v0}, Lbr/com/itau/sdk/android/core/BackendClient;->callApi(Lbr/com/itau/sdk/android/core/type/BackendCall;)V

    .line 83
    return-void
.end method

.class public Lcom/itau/empresas/feature/credito/giro/model/SeguroProdutosParceladoSaidaVO;
.super Ljava/lang/Object;
.source "SeguroProdutosParceladoSaidaVO.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private indicadorSeguro:Z
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "indicador_seguro"
    .end annotation
.end field

.field private valorIof:F
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "valor_iof"
    .end annotation
.end field

.field private valorPremio:F
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "valor_premio"
    .end annotation
.end field

.field private valorSeguro:F
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "valor_seguro"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getIndicadorSeguro()Z
    .registers 2

    .line 22
    iget-boolean v0, p0, Lcom/itau/empresas/feature/credito/giro/model/SeguroProdutosParceladoSaidaVO;->indicadorSeguro:Z

    return v0
.end method

.method public getValorSeguro()F
    .registers 2

    .line 39
    iget v0, p0, Lcom/itau/empresas/feature/credito/giro/model/SeguroProdutosParceladoSaidaVO;->valorSeguro:F

    return v0
.end method

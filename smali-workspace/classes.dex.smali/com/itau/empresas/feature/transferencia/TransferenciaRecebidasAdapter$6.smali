.class Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter$6;
.super Ljava/lang/Object;
.source "TransferenciaRecebidasAdapter.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter;->ordenar()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;Ljava/util/Comparator<Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasVO;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter;


# direct methods
.method constructor <init>(Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter;)V
    .registers 2
    .param p1, "this$0"    # Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter;

    .line 256
    iput-object p1, p0, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter$6;->this$0:Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public compare(Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasVO;Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasVO;)I
    .registers 5
    .param p1, "lft"    # Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasVO;
    .param p2, "rht"    # Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasVO;

    .line 260
    invoke-virtual {p2}, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasVO;->getDataRecebimento()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasVO;->getDataRecebimento()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .registers 5

    .line 256
    move-object v0, p1

    check-cast v0, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasVO;

    move-object v1, p2

    check-cast v1, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasVO;

    invoke-virtual {p0, v0, v1}, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter$6;->compare(Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasVO;Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasVO;)I

    move-result v0

    return v0
.end method

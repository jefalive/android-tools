.class final Lcom/adobe/mobile/WearableFunctionBridge;
.super Ljava/lang/Object;
.source "WearableFunctionBridge.java"


# static fields
.field private static configSynchronizerClassLoader:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class<*>;"
        }
    .end annotation
.end field

.field private static wearableFunctionClassLoader:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class<*>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .registers 1

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static getConfigSynchronizerClass()Ljava/lang/Class;
    .registers 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/lang/Class<*>;"
        }
    .end annotation

    .line 47
    sget-object v0, Lcom/adobe/mobile/WearableFunctionBridge;->configSynchronizerClassLoader:Ljava/lang/Class;

    if-eqz v0, :cond_7

    .line 48
    sget-object v0, Lcom/adobe/mobile/WearableFunctionBridge;->configSynchronizerClassLoader:Ljava/lang/Class;

    return-object v0

    .line 52
    :cond_7
    :try_start_7
    const-class v0, Lcom/adobe/mobile/WearableFunctionBridge;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v4

    .line 53
    .local v4, "classLoader":Ljava/lang/ClassLoader;
    const-string v0, "com.adobe.mobile.ConfigSynchronizer"

    invoke-virtual {v4, v0}, Ljava/lang/ClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    sput-object v0, Lcom/adobe/mobile/WearableFunctionBridge;->configSynchronizerClassLoader:Ljava/lang/Class;
    :try_end_15
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_15} :catch_16

    .line 57
    .end local v4    # "classLoader":Ljava/lang/ClassLoader;
    goto :goto_26

    .line 55
    :catch_16
    move-exception v4

    .line 56
    .local v4, "e":Ljava/lang/Exception;
    const-string v0, "Wearable - Failed to load class com.adobe.mobile.ConfigSynchronizer"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v4}, Ljava/lang/Exception;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logDebugFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 58
    .end local v4    # "e":Ljava/lang/Exception;
    :goto_26
    sget-object v0, Lcom/adobe/mobile/WearableFunctionBridge;->configSynchronizerClassLoader:Ljava/lang/Class;

    return-object v0
.end method

.method private static getWearableFunctionClass()Ljava/lang/Class;
    .registers 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/lang/Class<*>;"
        }
    .end annotation

    .line 30
    sget-object v0, Lcom/adobe/mobile/WearableFunctionBridge;->wearableFunctionClassLoader:Ljava/lang/Class;

    if-eqz v0, :cond_7

    .line 31
    sget-object v0, Lcom/adobe/mobile/WearableFunctionBridge;->wearableFunctionClassLoader:Ljava/lang/Class;

    return-object v0

    .line 35
    :cond_7
    :try_start_7
    const-class v0, Lcom/adobe/mobile/WearableFunctionBridge;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v4

    .line 36
    .local v4, "classLoader":Ljava/lang/ClassLoader;
    const-string v0, "com.adobe.mobile.WearableFunction"

    invoke-virtual {v4, v0}, Ljava/lang/ClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    sput-object v0, Lcom/adobe/mobile/WearableFunctionBridge;->wearableFunctionClassLoader:Ljava/lang/Class;
    :try_end_15
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_15} :catch_16

    .line 40
    .end local v4    # "classLoader":Ljava/lang/ClassLoader;
    goto :goto_26

    .line 38
    :catch_16
    move-exception v4

    .line 39
    .local v4, "e":Ljava/lang/Exception;
    const-string v0, "Wearable - Failed to load class com.adobe.mobile.WearableFunction"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v4}, Ljava/lang/Exception;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logDebugFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 41
    .end local v4    # "e":Ljava/lang/Exception;
    :goto_26
    sget-object v0, Lcom/adobe/mobile/WearableFunctionBridge;->wearableFunctionClassLoader:Ljava/lang/Class;

    return-object v0
.end method

.method protected static isGooglePlayServicesEnabled()Z
    .registers 10

    .line 65
    :try_start_0
    const-class v0, Lcom/adobe/mobile/WearableFunctionBridge;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v4

    .line 66
    .local v4, "classLoader":Ljava/lang/ClassLoader;
    const-string v0, "com.google.android.gms.common.GoogleApiAvailability"

    invoke-virtual {v4, v0}, Ljava/lang/ClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v5

    .line 67
    .local v5, "GoogleApiAvailabilityClass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const-string v0, "getInstance"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Class;

    invoke-virtual {v5, v0, v1}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v6

    .line 68
    .local v6, "getInstanceMethod":Ljava/lang/reflect/Method;
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-virtual {v6, v1, v0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    .line 69
    .local v7, "instance":Ljava/lang/Object;
    const-string v0, "isGooglePlayServicesAvailable"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Class;

    const-class v2, Landroid/content/Context;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-virtual {v5, v0, v1}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v8

    .line 70
    .local v8, "isGooglePlayServicesAvailableMethod":Ljava/lang/reflect/Method;
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getSharedContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    invoke-virtual {v8, v7, v0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    .line 71
    .local v9, "result":Ljava/lang/Object;
    instance-of v0, v9, Ljava/lang/Integer;

    if-eqz v0, :cond_4a

    .line 73
    move-object v0, v9

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
    :try_end_43
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_43} :catch_4b
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_43} :catch_5c

    move-result v0

    if-nez v0, :cond_48

    const/4 v0, 0x1

    goto :goto_49

    :cond_48
    const/4 v0, 0x0

    :goto_49
    return v0

    .line 82
    .end local v4    # "classLoader":Ljava/lang/ClassLoader;
    .end local v5    # "GoogleApiAvailabilityClass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .end local v5
    .end local v6    # "getInstanceMethod":Ljava/lang/reflect/Method;
    .end local v7    # "instance":Ljava/lang/Object;
    .end local v8    # "isGooglePlayServicesAvailableMethod":Ljava/lang/reflect/Method;
    .end local v9    # "result":Ljava/lang/Object;
    :cond_4a
    goto :goto_5d

    .line 76
    :catch_4b
    move-exception v4

    .line 78
    .local v4, "e":Ljava/lang/IllegalStateException;
    const-string v0, "Wearable - Google Play Services is not enabled in your app\'s AndroidManifest.xml"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v4}, Ljava/lang/IllegalStateException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logDebugFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 82
    .end local v4    # "e":Ljava/lang/IllegalStateException;
    goto :goto_5d

    .line 80
    :catch_5c
    move-exception v4

    .line 87
    :goto_5d
    :try_start_5d
    const-class v0, Lcom/adobe/mobile/WearableFunctionBridge;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v4

    .line 88
    .local v4, "classLoader":Ljava/lang/ClassLoader;
    const-string v0, "com.google.android.gms.common.GooglePlayServicesUtil"

    invoke-virtual {v4, v0}, Ljava/lang/ClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v5

    .line 89
    .local v5, "GooglePlayServicesUtilClass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const-string v0, "isGooglePlayServicesAvailable"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Class;

    const-class v2, Landroid/content/Context;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-virtual {v5, v0, v1}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v6

    .line 90
    .local v6, "isGooglePlayServicesAvailableMethod":Ljava/lang/reflect/Method;
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getSharedContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const/4 v1, 0x0

    invoke-virtual {v6, v1, v0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    .line 91
    .local v7, "result":Ljava/lang/Object;
    instance-of v0, v7, Ljava/lang/Integer;

    if-eqz v0, :cond_97

    .line 93
    move-object v0, v7

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
    :try_end_90
    .catch Ljava/lang/IllegalStateException; {:try_start_5d .. :try_end_90} :catch_98
    .catch Ljava/lang/Exception; {:try_start_5d .. :try_end_90} :catch_a9

    move-result v0

    if-nez v0, :cond_95

    const/4 v0, 0x1

    goto :goto_96

    :cond_95
    const/4 v0, 0x0

    :goto_96
    return v0

    .line 100
    .end local v4    # "classLoader":Ljava/lang/ClassLoader;
    .end local v5    # "GooglePlayServicesUtilClass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .end local v5
    .end local v6    # "isGooglePlayServicesAvailableMethod":Ljava/lang/reflect/Method;
    .end local v7    # "result":Ljava/lang/Object;
    :cond_97
    goto :goto_aa

    .line 96
    :catch_98
    move-exception v4

    .line 97
    .local v4, "e":Ljava/lang/IllegalStateException;
    const-string v0, "Wearable - Google Play Services is not enabled in your app\'s AndroidManifest.xml"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v4}, Ljava/lang/IllegalStateException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logDebugFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 100
    .end local v4    # "e":Ljava/lang/IllegalStateException;
    goto :goto_aa

    .line 98
    :catch_a9
    move-exception v4

    .line 103
    :goto_aa
    const/4 v0, 0x0

    return v0
.end method

.method protected static retrieveAnalyticsRequestData(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)[B
    .registers 11
    .param p0, "url"    # Ljava/lang/String;
    .param p1, "postBody"    # Ljava/lang/String;
    .param p2, "timeout"    # I
    .param p3, "logPrefix"    # Ljava/lang/String;

    .line 149
    :try_start_0
    invoke-static {}, Lcom/adobe/mobile/WearableFunctionBridge;->getWearableFunctionClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "retrieveAnalyticsRequestData"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Class;

    const-class v3, Ljava/lang/String;

    const/4 v4, 0x0

    aput-object v3, v2, v4

    const-class v3, Ljava/lang/String;

    const/4 v4, 0x1

    aput-object v3, v2, v4

    sget-object v3, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    const/4 v4, 0x2

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v5

    .line 150
    .local v5, "retrieveDataMethod":Ljava/lang/reflect/Method;
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    const/4 v1, 0x1

    aput-object p1, v0, v1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x2

    aput-object v1, v0, v2

    const/4 v1, 0x0

    invoke-virtual {v5, v1, v0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    .line 151
    .local v6, "result":Ljava/lang/Object;
    instance-of v0, v6, [B

    if-eqz v0, :cond_3b

    .line 152
    move-object v0, v6

    check-cast v0, [B

    check-cast v0, [B
    :try_end_3a
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_3a} :catch_3c

    return-object v0

    .line 156
    .end local v5    # "retrieveDataMethod":Ljava/lang/reflect/Method;
    .end local v6    # "result":Ljava/lang/Object;
    :cond_3b
    goto :goto_4c

    .line 154
    :catch_3c
    move-exception v5

    .line 155
    .local v5, "e":Ljava/lang/Exception;
    const-string v0, "Wearable - Error sending request (%s)"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v5}, Ljava/lang/Exception;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logDebugFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 157
    .end local v5    # "e":Ljava/lang/Exception;
    :goto_4c
    const/4 v0, 0x0

    return-object v0
.end method

.method protected static retrieveData(Ljava/lang/String;I)[B
    .registers 9
    .param p0, "url"    # Ljava/lang/String;
    .param p1, "readTimeout"    # I

    .line 136
    :try_start_0
    invoke-static {}, Lcom/adobe/mobile/WearableFunctionBridge;->getWearableFunctionClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "retrieveData"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Class;

    const-class v3, Ljava/lang/String;

    const/4 v4, 0x0

    aput-object v3, v2, v4

    sget-object v3, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    const/4 v4, 0x1

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v5

    .line 137
    .local v5, "retrieveDataMethod":Ljava/lang/reflect/Method;
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x1

    aput-object v1, v0, v2

    const/4 v1, 0x0

    invoke-virtual {v5, v1, v0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    .line 138
    .local v6, "result":Ljava/lang/Object;
    instance-of v0, v6, [B

    if-eqz v0, :cond_33

    .line 139
    move-object v0, v6

    check-cast v0, [B

    check-cast v0, [B
    :try_end_32
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_32} :catch_34

    return-object v0

    .line 143
    .end local v5    # "retrieveDataMethod":Ljava/lang/reflect/Method;
    .end local v6    # "result":Ljava/lang/Object;
    :cond_33
    goto :goto_44

    .line 141
    :catch_34
    move-exception v5

    .line 142
    .local v5, "e":Ljava/lang/Exception;
    const-string v0, "Wearable - Error sending request (%s)"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v5}, Ljava/lang/Exception;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logDebugFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 144
    .end local v5    # "e":Ljava/lang/Exception;
    :goto_44
    const/4 v0, 0x0

    return-object v0
.end method

.method protected static sendGenericRequest(Ljava/lang/String;ILjava/lang/String;)V
    .registers 9
    .param p0, "url"    # Ljava/lang/String;
    .param p1, "timeout"    # I
    .param p2, "source"    # Ljava/lang/String;

    .line 125
    :try_start_0
    invoke-static {}, Lcom/adobe/mobile/WearableFunctionBridge;->getWearableFunctionClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "sendGenericRequest"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Class;

    const-class v3, Ljava/lang/String;

    const/4 v4, 0x0

    aput-object v3, v2, v4

    sget-object v3, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    const/4 v4, 0x1

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v5

    .line 126
    .local v5, "sendGenericRequestMethod":Ljava/lang/reflect/Method;
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x1

    aput-object v1, v0, v2

    const/4 v1, 0x0

    invoke-virtual {v5, v1, v0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 127
    const-string v0, "%s - Request Sent(%s)"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p2, v1, v2

    const/4 v2, 0x1

    aput-object p0, v1, v2

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logDebugFormat(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_36
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_36} :catch_37

    .line 130
    .end local v5    # "sendGenericRequestMethod":Ljava/lang/reflect/Method;
    goto :goto_47

    .line 128
    :catch_37
    move-exception v5

    .line 129
    .local v5, "e":Ljava/lang/Exception;
    const-string v0, "Wearable - Error sending request (%s)"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v5}, Ljava/lang/Exception;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logDebugFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 132
    .end local v5    # "e":Ljava/lang/Exception;
    :goto_47
    return-void
.end method

.method protected static sendThirdPartyRequest(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)Z
    .registers 12
    .param p0, "url"    # Ljava/lang/String;
    .param p1, "postBody"    # Ljava/lang/String;
    .param p2, "timeout"    # I
    .param p3, "postType"    # Ljava/lang/String;
    .param p4, "logPrefix"    # Ljava/lang/String;

    .line 162
    :try_start_0
    invoke-static {}, Lcom/adobe/mobile/WearableFunctionBridge;->getWearableFunctionClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "sendThirdPartyRequest"

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Class;

    const-class v3, Ljava/lang/String;

    const/4 v4, 0x0

    aput-object v3, v2, v4

    const-class v3, Ljava/lang/String;

    const/4 v4, 0x1

    aput-object v3, v2, v4

    sget-object v3, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    const/4 v4, 0x2

    aput-object v3, v2, v4

    const-class v3, Ljava/lang/String;

    const/4 v4, 0x3

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v5

    .line 163
    .local v5, "sendThirdPartyRequestMethod":Ljava/lang/reflect/Method;
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    const/4 v1, 0x1

    aput-object p1, v0, v1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x2

    aput-object v1, v0, v2

    const/4 v1, 0x3

    aput-object p3, v0, v1

    const/4 v1, 0x0

    invoke-virtual {v5, v1, v0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    .line 164
    .local v6, "result":Ljava/lang/Object;
    instance-of v0, v6, Ljava/lang/Boolean;

    if-eqz v0, :cond_77

    .line 165
    move-object v0, v6

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_5b

    .line 166
    const-string v0, "%s - Successfully forwarded hit (url:%s body:%s contentType:%s)"

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p4, v1, v2

    const/4 v2, 0x1

    aput-object p0, v1, v2

    const/4 v2, 0x2

    aput-object p1, v1, v2

    const/4 v2, 0x3

    aput-object p3, v1, v2

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logDebugFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_6f

    .line 168
    :cond_5b
    const-string v0, "%s - Failed to forwarded hit (url:%s body:%s contentType:%s)"

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p4, v1, v2

    const/4 v2, 0x1

    aput-object p0, v1, v2

    const/4 v2, 0x2

    aput-object p1, v1, v2

    const/4 v2, 0x3

    aput-object p3, v1, v2

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logDebugFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 170
    :goto_6f
    move-object v0, v6

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_75
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_75} :catch_78

    move-result v0

    return v0

    .line 175
    .end local v5    # "sendThirdPartyRequestMethod":Ljava/lang/reflect/Method;
    .end local v6    # "result":Ljava/lang/Object;
    :cond_77
    goto :goto_88

    .line 173
    :catch_78
    move-exception v5

    .line 174
    .local v5, "e":Ljava/lang/Exception;
    const-string v0, "Wearable - Error sending request (%s)"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v5}, Ljava/lang/Exception;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logDebugFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 176
    .end local v5    # "e":Ljava/lang/Exception;
    :goto_88
    const/4 v0, 0x0

    return v0
.end method

.method protected static shouldSendHit()Z
    .registers 6

    .line 108
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->isWearableApp()Z

    move-result v0

    if-nez v0, :cond_8

    .line 109
    const/4 v0, 0x1

    return v0

    .line 112
    :cond_8
    :try_start_8
    invoke-static {}, Lcom/adobe/mobile/WearableFunctionBridge;->getWearableFunctionClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "shouldSendHit"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Class;

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v4

    .line 113
    .local v4, "shouldSendHitMethod":Ljava/lang/reflect/Method;
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-virtual {v4, v1, v0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    .line 114
    .local v5, "result":Ljava/lang/Object;
    instance-of v0, v5, Ljava/lang/Boolean;

    if-eqz v0, :cond_29

    .line 115
    move-object v0, v5

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_27
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_27} :catch_2a

    move-result v0

    return v0

    .line 119
    .end local v4    # "shouldSendHitMethod":Ljava/lang/reflect/Method;
    .end local v5    # "result":Ljava/lang/Object;
    :cond_29
    goto :goto_3a

    .line 117
    :catch_2a
    move-exception v4

    .line 118
    .local v4, "e":Ljava/lang/Exception;
    const-string v0, "Wearable - Error checking status of handheld app (%s)"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v4}, Ljava/lang/Exception;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logDebugFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 120
    .end local v4    # "e":Ljava/lang/Exception;
    :goto_3a
    const/4 v0, 0x1

    return v0
.end method

.method protected static syncConfigFromHandheld()V
    .registers 5

    .line 244
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->isWearableApp()Z

    move-result v0

    if-nez v0, :cond_7

    .line 245
    return-void

    .line 248
    :cond_7
    :try_start_7
    invoke-static {}, Lcom/adobe/mobile/WearableFunctionBridge;->getConfigSynchronizerClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "syncConfigFromHandheld"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Class;

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v4

    .line 249
    .local v4, "syncConfigFromHandheldMethod":Ljava/lang/reflect/Method;
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-virtual {v4, v1, v0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1b
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_1b} :catch_1c

    .line 252
    .end local v4    # "syncConfigFromHandheldMethod":Ljava/lang/reflect/Method;
    goto :goto_2c

    .line 250
    :catch_1c
    move-exception v4

    .line 251
    .local v4, "e":Ljava/lang/Exception;
    const-string v0, "Wearable - Unable to sync config (%s)"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v4}, Ljava/lang/Exception;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logDebugFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 253
    .end local v4    # "e":Ljava/lang/Exception;
    :goto_2c
    return-void
.end method

.method protected static syncPrivacyStatusToWearable(I)V
    .registers 7
    .param p0, "privacyStatus"    # I

    .line 231
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->isWearableApp()Z

    move-result v0

    if-nez v0, :cond_10

    invoke-static {}, Lcom/adobe/mobile/MobileConfig;->getInstance()Lcom/adobe/mobile/MobileConfig;

    move-result-object v0

    invoke-virtual {v0}, Lcom/adobe/mobile/MobileConfig;->mobileUsingGooglePlayServices()Z

    move-result v0

    if-nez v0, :cond_11

    .line 232
    :cond_10
    return-void

    .line 235
    :cond_11
    :try_start_11
    invoke-static {}, Lcom/adobe/mobile/WearableFunctionBridge;->getConfigSynchronizerClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "syncPrivacyStatus"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    sget-object v3, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    const/4 v4, 0x0

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v5

    .line 236
    .local v5, "syncPrivacyStatusMethod":Ljava/lang/reflect/Method;
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const/4 v1, 0x0

    invoke-virtual {v5, v1, v0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_31
    .catch Ljava/lang/Exception; {:try_start_11 .. :try_end_31} :catch_32

    .line 239
    .end local v5    # "syncPrivacyStatusMethod":Ljava/lang/reflect/Method;
    goto :goto_42

    .line 237
    :catch_32
    move-exception v5

    .line 238
    .local v5, "e":Ljava/lang/Exception;
    const-string v0, "Wearable - Unable to sync privacy status (%s)"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v5}, Ljava/lang/Exception;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logDebugFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 240
    .end local v5    # "e":Ljava/lang/Exception;
    :goto_42
    return-void
.end method

.method protected static syncVidServiceToWearable(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJLjava/lang/String;)V
    .registers 14
    .param p0, "mid"    # Ljava/lang/String;
    .param p1, "hint"    # Ljava/lang/String;
    .param p2, "blob"    # Ljava/lang/String;
    .param p3, "ssl"    # J
    .param p5, "lastSync"    # J
    .param p7, "customerIDs"    # Ljava/lang/String;

    .line 218
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->isWearableApp()Z

    move-result v0

    if-nez v0, :cond_10

    invoke-static {}, Lcom/adobe/mobile/MobileConfig;->getInstance()Lcom/adobe/mobile/MobileConfig;

    move-result-object v0

    invoke-virtual {v0}, Lcom/adobe/mobile/MobileConfig;->mobileUsingGooglePlayServices()Z

    move-result v0

    if-nez v0, :cond_11

    .line 219
    :cond_10
    return-void

    .line 222
    :cond_11
    :try_start_11
    invoke-static {}, Lcom/adobe/mobile/WearableFunctionBridge;->getConfigSynchronizerClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "syncVidService"

    const/4 v2, 0x6

    new-array v2, v2, [Ljava/lang/Class;

    const-class v3, Ljava/lang/String;

    const/4 v4, 0x0

    aput-object v3, v2, v4

    const-class v3, Ljava/lang/String;

    const/4 v4, 0x1

    aput-object v3, v2, v4

    const-class v3, Ljava/lang/String;

    const/4 v4, 0x2

    aput-object v3, v2, v4

    sget-object v3, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    const/4 v4, 0x3

    aput-object v3, v2, v4

    sget-object v3, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    const/4 v4, 0x4

    aput-object v3, v2, v4

    const-class v3, Ljava/lang/String;

    const/4 v4, 0x5

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v5

    .line 223
    .local v5, "syncVidServiceMethod":Ljava/lang/reflect/Method;
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    const/4 v1, 0x1

    aput-object p1, v0, v1

    const/4 v1, 0x2

    aput-object p2, v0, v1

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    const/4 v2, 0x3

    aput-object v1, v0, v2

    invoke-static {p5, p6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    const/4 v2, 0x4

    aput-object v1, v0, v2

    const/4 v1, 0x5

    aput-object p7, v0, v1

    const/4 v1, 0x0

    invoke-virtual {v5, v1, v0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_5d
    .catch Ljava/lang/Exception; {:try_start_11 .. :try_end_5d} :catch_5e

    .line 226
    .end local v5    # "syncVidServiceMethod":Ljava/lang/reflect/Method;
    goto :goto_6e

    .line 224
    :catch_5e
    move-exception v5

    .line 225
    .local v5, "e":Ljava/lang/Exception;
    const-string v0, "Wearable - Unable to sync visitor id service (%s)"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v5}, Ljava/lang/Exception;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logDebugFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 227
    .end local v5    # "e":Ljava/lang/Exception;
    :goto_6e
    return-void
.end method

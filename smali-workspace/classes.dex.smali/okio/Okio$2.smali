.class final Lokio/Okio$2;
.super Ljava/lang/Object;
.source "Okio.java"

# interfaces
.implements Lokio/Source;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lokio/Okio;->source(Ljava/io/InputStream;Lokio/Timeout;)Lokio/Source;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$in:Ljava/io/InputStream;

.field final synthetic val$timeout:Lokio/Timeout;


# direct methods
.method constructor <init>(Lokio/Timeout;Ljava/io/InputStream;)V
    .registers 3

    .line 130
    iput-object p1, p0, Lokio/Okio$2;->val$timeout:Lokio/Timeout;

    iput-object p2, p0, Lokio/Okio$2;->val$in:Ljava/io/InputStream;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public close()V
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 150
    iget-object v0, p0, Lokio/Okio$2;->val$in:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    .line 151
    return-void
.end method

.method public read(Lokio/Buffer;J)J
    .registers 11
    .param p1, "sink"    # Lokio/Buffer;
    .param p2, "byteCount"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 132
    const-wide/16 v0, 0x0

    cmp-long v0, p2, v0

    if-gez v0, :cond_1f

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "byteCount < 0: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 133
    :cond_1f
    const-wide/16 v0, 0x0

    cmp-long v0, p2, v0

    if-nez v0, :cond_28

    const-wide/16 v0, 0x0

    return-wide v0

    .line 135
    :cond_28
    :try_start_28
    iget-object v0, p0, Lokio/Okio$2;->val$timeout:Lokio/Timeout;

    invoke-virtual {v0}, Lokio/Timeout;->throwIfReached()V

    .line 136
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lokio/Buffer;->writableSegment(I)Lokio/Segment;

    move-result-object v4

    .line 137
    .local v4, "tail":Lokio/Segment;
    iget v0, v4, Lokio/Segment;->limit:I

    rsub-int v0, v0, 0x2000

    int-to-long v0, v0

    invoke-static {p2, p3, v0, v1}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    long-to-int v5, v0

    .line 138
    .local v5, "maxToCopy":I
    iget-object v0, p0, Lokio/Okio$2;->val$in:Ljava/io/InputStream;

    iget-object v1, v4, Lokio/Segment;->data:[B

    iget v2, v4, Lokio/Segment;->limit:I

    invoke-virtual {v0, v1, v2, v5}, Ljava/io/InputStream;->read([BII)I
    :try_end_45
    .catch Ljava/lang/AssertionError; {:try_start_28 .. :try_end_45} :catch_59

    move-result v6

    .line 139
    .local v6, "bytesRead":I
    const/4 v0, -0x1

    if-ne v6, v0, :cond_4c

    const-wide/16 v0, -0x1

    return-wide v0

    .line 140
    :cond_4c
    :try_start_4c
    iget v0, v4, Lokio/Segment;->limit:I

    add-int/2addr v0, v6

    iput v0, v4, Lokio/Segment;->limit:I

    .line 141
    iget-wide v0, p1, Lokio/Buffer;->size:J

    int-to-long v2, v6

    add-long/2addr v0, v2

    iput-wide v0, p1, Lokio/Buffer;->size:J
    :try_end_57
    .catch Ljava/lang/AssertionError; {:try_start_4c .. :try_end_57} :catch_59

    .line 142
    int-to-long v0, v6

    return-wide v0

    .line 143
    .end local v4    # "tail":Lokio/Segment;
    .end local v5    # "maxToCopy":I
    .end local v6    # "bytesRead":I
    :catch_59
    move-exception v4

    .line 144
    .local v4, "e":Ljava/lang/AssertionError;
    invoke-static {v4}, Lokio/Okio;->isAndroidGetsocknameError(Ljava/lang/AssertionError;)Z

    move-result v0

    if-eqz v0, :cond_66

    new-instance v0, Ljava/io/IOException;

    invoke-direct {v0, v4}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v0

    .line 145
    :cond_66
    throw v4
.end method

.method public timeout()Lokio/Timeout;
    .registers 2

    .line 154
    iget-object v0, p0, Lokio/Okio$2;->val$timeout:Lokio/Timeout;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    .line 158
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "source("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lokio/Okio$2;->val$in:Ljava/io/InputStream;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.class Lcom/google/android/gms/internal/zzbh$2;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/gms/internal/zzbh;->zzw(Ljava/lang/String;)Ljava/lang/String;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;Ljava/util/Comparator<Lcom/google/android/gms/internal/zzbk$zza;>;"
    }
.end annotation


# instance fields
.field final synthetic zztt:Lcom/google/android/gms/internal/zzbh;


# direct methods
.method constructor <init>(Lcom/google/android/gms/internal/zzbh;)V
    .registers 2

    iput-object p1, p0, Lcom/google/android/gms/internal/zzbh$2;->zztt:Lcom/google/android/gms/internal/zzbh;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .registers 5

    move-object v0, p1

    check-cast v0, Lcom/google/android/gms/internal/zzbk$zza;

    move-object v1, p2

    check-cast v1, Lcom/google/android/gms/internal/zzbk$zza;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/internal/zzbh$2;->zza(Lcom/google/android/gms/internal/zzbk$zza;Lcom/google/android/gms/internal/zzbk$zza;)I

    move-result v0

    return v0
.end method

.method public zza(Lcom/google/android/gms/internal/zzbk$zza;Lcom/google/android/gms/internal/zzbk$zza;)I
    .registers 8

    iget v0, p1, Lcom/google/android/gms/internal/zzbk$zza;->zzty:I

    iget v1, p2, Lcom/google/android/gms/internal/zzbk$zza;->zzty:I

    sub-int v4, v0, v1

    if-eqz v4, :cond_a

    move v0, v4

    goto :goto_10

    :cond_a
    iget-wide v0, p1, Lcom/google/android/gms/internal/zzbk$zza;->value:J

    iget-wide v2, p2, Lcom/google/android/gms/internal/zzbk$zza;->value:J

    sub-long/2addr v0, v2

    long-to-int v0, v0

    :goto_10
    return v0
.end method

.class public Lcom/google/android/gms/common/stats/zze;
.super Ljava/lang/Object;


# instance fields
.field private final zzanN:J

.field private final zzanO:I

.field private final zzanP:Landroid/support/v4/util/SimpleArrayMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/support/v4/util/SimpleArrayMap<Ljava/lang/String;Ljava/lang/Long;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/32 v0, 0xea60

    iput-wide v0, p0, Lcom/google/android/gms/common/stats/zze;->zzanN:J

    const/16 v0, 0xa

    iput v0, p0, Lcom/google/android/gms/common/stats/zze;->zzanO:I

    new-instance v0, Landroid/support/v4/util/SimpleArrayMap;

    const/16 v1, 0xa

    invoke-direct {v0, v1}, Landroid/support/v4/util/SimpleArrayMap;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/gms/common/stats/zze;->zzanP:Landroid/support/v4/util/SimpleArrayMap;

    return-void
.end method

.method public constructor <init>(IJ)V
    .registers 5

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p2, p0, Lcom/google/android/gms/common/stats/zze;->zzanN:J

    iput p1, p0, Lcom/google/android/gms/common/stats/zze;->zzanO:I

    new-instance v0, Landroid/support/v4/util/SimpleArrayMap;

    invoke-direct {v0}, Landroid/support/v4/util/SimpleArrayMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/common/stats/zze;->zzanP:Landroid/support/v4/util/SimpleArrayMap;

    return-void
.end method

.method private zzb(JJ)V
    .registers 8

    iget-object v0, p0, Lcom/google/android/gms/common/stats/zze;->zzanP:Landroid/support/v4/util/SimpleArrayMap;

    invoke-virtual {v0}, Landroid/support/v4/util/SimpleArrayMap;->size()I

    move-result v0

    add-int/lit8 v2, v0, -0x1

    :goto_8
    if-ltz v2, :cond_24

    iget-object v0, p0, Lcom/google/android/gms/common/stats/zze;->zzanP:Landroid/support/v4/util/SimpleArrayMap;

    invoke-virtual {v0, v2}, Landroid/support/v4/util/SimpleArrayMap;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    sub-long v0, p3, v0

    cmp-long v0, v0, p1

    if-lez v0, :cond_21

    iget-object v0, p0, Lcom/google/android/gms/common/stats/zze;->zzanP:Landroid/support/v4/util/SimpleArrayMap;

    invoke-virtual {v0, v2}, Landroid/support/v4/util/SimpleArrayMap;->removeAt(I)Ljava/lang/Object;

    :cond_21
    add-int/lit8 v2, v2, -0x1

    goto :goto_8

    :cond_24
    return-void
.end method


# virtual methods
.method public zzcS(Ljava/lang/String;)Ljava/lang/Long;
    .registers 11

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v3

    iget-wide v5, p0, Lcom/google/android/gms/common/stats/zze;->zzanN:J

    move-object v7, p0

    monitor-enter v7

    :goto_8
    :try_start_8
    iget-object v0, p0, Lcom/google/android/gms/common/stats/zze;->zzanP:Landroid/support/v4/util/SimpleArrayMap;

    invoke-virtual {v0}, Landroid/support/v4/util/SimpleArrayMap;->size()I

    move-result v0

    iget v1, p0, Lcom/google/android/gms/common/stats/zze;->zzanO:I

    if-lt v0, v1, :cond_3d

    invoke-direct {p0, v5, v6, v3, v4}, Lcom/google/android/gms/common/stats/zze;->zzb(JJ)V

    const-wide/16 v0, 0x2

    div-long/2addr v5, v0

    const-string v0, "ConnectionTracker"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "The max capacity "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/gms/common/stats/zze;->zzanO:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not enough. Current durationThreshold is: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_8

    :cond_3d
    iget-object v0, p0, Lcom/google/android/gms/common/stats/zze;->zzanP:Landroid/support/v4/util/SimpleArrayMap;

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Landroid/support/v4/util/SimpleArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;
    :try_end_49
    .catchall {:try_start_8 .. :try_end_49} :catchall_4b

    monitor-exit v7

    return-object v0

    :catchall_4b
    move-exception v8

    monitor-exit v7

    throw v8
.end method

.method public zzcT(Ljava/lang/String;)Z
    .registers 5

    move-object v1, p0

    monitor-enter v1

    :try_start_2
    iget-object v0, p0, Lcom/google/android/gms/common/stats/zze;->zzanP:Landroid/support/v4/util/SimpleArrayMap;

    invoke-virtual {v0, p1}, Landroid/support/v4/util/SimpleArrayMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_7
    .catchall {:try_start_2 .. :try_end_7} :catchall_f

    move-result-object v0

    if-eqz v0, :cond_c

    const/4 v0, 0x1

    goto :goto_d

    :cond_c
    const/4 v0, 0x0

    :goto_d
    monitor-exit v1

    return v0

    :catchall_f
    move-exception v2

    monitor-exit v1

    throw v2
.end method

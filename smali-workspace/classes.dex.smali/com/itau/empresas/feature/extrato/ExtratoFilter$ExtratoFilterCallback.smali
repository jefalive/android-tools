.class public interface abstract Lcom/itau/empresas/feature/extrato/ExtratoFilter$ExtratoFilterCallback;
.super Ljava/lang/Object;
.source "ExtratoFilter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/itau/empresas/feature/extrato/ExtratoFilter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "ExtratoFilterCallback"
.end annotation


# virtual methods
.method public abstract aplicarResultadoDeBusca(Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<Lcom/itau/empresas/api/model/LancamentoVO;>;)V"
        }
    .end annotation
.end method

.method public abstract exibeEmptyState()V
.end method

.method public abstract exibeTextoDaBusca(Ljava/lang/String;)V
.end method

.method public abstract mostraEstadoOriginal()V
.end method

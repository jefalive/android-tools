.class final Lorg/threeten/bp/chrono/Chronology$1;
.super Ljava/lang/Object;
.source "Chronology.java"

# interfaces
.implements Lorg/threeten/bp/temporal/TemporalQuery;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/threeten/bp/chrono/Chronology;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;Lorg/threeten/bp/temporal/TemporalQuery<Lorg/threeten/bp/chrono/Chronology;>;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .registers 1

    .line 146
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic queryFrom(Lorg/threeten/bp/temporal/TemporalAccessor;)Ljava/lang/Object;
    .registers 3
    .param p1, "x0"    # Lorg/threeten/bp/temporal/TemporalAccessor;

    .line 146
    invoke-virtual {p0, p1}, Lorg/threeten/bp/chrono/Chronology$1;->queryFrom(Lorg/threeten/bp/temporal/TemporalAccessor;)Lorg/threeten/bp/chrono/Chronology;

    move-result-object v0

    return-object v0
.end method

.method public queryFrom(Lorg/threeten/bp/temporal/TemporalAccessor;)Lorg/threeten/bp/chrono/Chronology;
    .registers 3
    .param p1, "temporal"    # Lorg/threeten/bp/temporal/TemporalAccessor;

    .line 149
    invoke-static {p1}, Lorg/threeten/bp/chrono/Chronology;->from(Lorg/threeten/bp/temporal/TemporalAccessor;)Lorg/threeten/bp/chrono/Chronology;

    move-result-object v0

    return-object v0
.end method

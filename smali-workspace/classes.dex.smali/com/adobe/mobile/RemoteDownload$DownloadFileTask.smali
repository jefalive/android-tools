.class Lcom/adobe/mobile/RemoteDownload$DownloadFileTask;
.super Ljava/lang/Object;
.source "RemoteDownload.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/adobe/mobile/RemoteDownload;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "DownloadFileTask"
.end annotation


# instance fields
.field private final callback:Lcom/adobe/mobile/RemoteDownload$RemoteDownloadBlock;

.field private final connectionTimeout:I

.field private final directory:Ljava/lang/String;

.field private final readTimeout:I

.field private final url:Ljava/lang/String;


# direct methods
.method private constructor <init>(Ljava/lang/String;Lcom/adobe/mobile/RemoteDownload$RemoteDownloadBlock;IILjava/lang/String;)V
    .registers 6
    .param p1, "initRequest"    # Ljava/lang/String;
    .param p2, "initCallback"    # Lcom/adobe/mobile/RemoteDownload$RemoteDownloadBlock;
    .param p3, "initConnectionTimeout"    # I
    .param p4, "initReadTimeout"    # I
    .param p5, "initDirectory"    # Ljava/lang/String;

    .line 329
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 331
    iput-object p1, p0, Lcom/adobe/mobile/RemoteDownload$DownloadFileTask;->url:Ljava/lang/String;

    .line 332
    iput-object p2, p0, Lcom/adobe/mobile/RemoteDownload$DownloadFileTask;->callback:Lcom/adobe/mobile/RemoteDownload$RemoteDownloadBlock;

    .line 333
    iput p3, p0, Lcom/adobe/mobile/RemoteDownload$DownloadFileTask;->connectionTimeout:I

    .line 334
    iput p4, p0, Lcom/adobe/mobile/RemoteDownload$DownloadFileTask;->readTimeout:I

    .line 335
    iput-object p5, p0, Lcom/adobe/mobile/RemoteDownload$DownloadFileTask;->directory:Ljava/lang/String;

    .line 336
    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;Lcom/adobe/mobile/RemoteDownload$RemoteDownloadBlock;IILjava/lang/String;Lcom/adobe/mobile/RemoteDownload$1;)V
    .registers 7
    .param p1, "x0"    # Ljava/lang/String;
    .param p2, "x1"    # Lcom/adobe/mobile/RemoteDownload$RemoteDownloadBlock;
    .param p3, "x2"    # I
    .param p4, "x3"    # I
    .param p5, "x4"    # Ljava/lang/String;
    .param p6, "x5"    # Lcom/adobe/mobile/RemoteDownload$1;

    .line 320
    invoke-direct/range {p0 .. p5}, Lcom/adobe/mobile/RemoteDownload$DownloadFileTask;-><init>(Ljava/lang/String;Lcom/adobe/mobile/RemoteDownload$RemoteDownloadBlock;IILjava/lang/String;)V

    return-void
.end method

.method protected static requestConnect(Ljava/lang/String;)Ljava/net/HttpURLConnection;
    .registers 6
    .param p0, "url"    # Ljava/lang/String;

    .line 488
    :try_start_0
    new-instance v4, Ljava/net/URL;

    invoke-direct {v4, p0}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 489
    .local v4, "requestURL":Ljava/net/URL;
    invoke-virtual {v4}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v0

    check-cast v0, Ljava/net/HttpURLConnection;
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_b} :catch_c

    return-object v0

    .line 490
    .end local v4    # "requestURL":Ljava/net/URL;
    :catch_c
    move-exception v4

    .line 491
    .local v4, "e":Ljava/lang/Exception;
    const-string v0, "Cached Files - Exception opening URL(%s)"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v4}, Ljava/lang/Exception;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logErrorFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 494
    .end local v4    # "e":Ljava/lang/Exception;
    const/4 v0, 0x0

    return-object v0
.end method


# virtual methods
.method public run()V
    .registers 17

    .line 340
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/adobe/mobile/RemoteDownload$DownloadFileTask;->url:Ljava/lang/String;

    if-nez v0, :cond_1e

    .line 341
    const-string v0, "Cached Files - url is null and cannot be cached"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logDebugFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 342
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/adobe/mobile/RemoteDownload$DownloadFileTask;->callback:Lcom/adobe/mobile/RemoteDownload$RemoteDownloadBlock;

    if-eqz v0, :cond_1d

    .line 343
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/adobe/mobile/RemoteDownload$DownloadFileTask;->callback:Lcom/adobe/mobile/RemoteDownload$RemoteDownloadBlock;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/adobe/mobile/RemoteDownload$RemoteDownloadBlock;->call(ZLjava/io/File;)V

    .line 345
    :cond_1d
    return-void

    .line 349
    :cond_1e
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/adobe/mobile/RemoteDownload$DownloadFileTask;->url:Ljava/lang/String;

    invoke-static {v0}, Lcom/adobe/mobile/RemoteDownload;->stringIsUrl(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_47

    .line 350
    const-string v0, "Cached Files - given url is not valid and cannot be cached (\"%s\")"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    move-object/from16 v2, p0

    iget-object v2, v2, Lcom/adobe/mobile/RemoteDownload$DownloadFileTask;->url:Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logDebugFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 351
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/adobe/mobile/RemoteDownload$DownloadFileTask;->callback:Lcom/adobe/mobile/RemoteDownload$RemoteDownloadBlock;

    if-eqz v0, :cond_46

    .line 352
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/adobe/mobile/RemoteDownload$DownloadFileTask;->callback:Lcom/adobe/mobile/RemoteDownload$RemoteDownloadBlock;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/adobe/mobile/RemoteDownload$RemoteDownloadBlock;->call(ZLjava/io/File;)V

    .line 354
    :cond_46
    return-void

    .line 357
    :cond_47
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/adobe/mobile/RemoteDownload$DownloadFileTask;->url:Ljava/lang/String;

    move-object/from16 v1, p0

    iget-object v1, v1, Lcom/adobe/mobile/RemoteDownload$DownloadFileTask;->directory:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/adobe/mobile/RemoteDownload;->getFileForCachedURL(Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;

    move-result-object v4

    .line 359
    .local v4, "cachefile":Ljava/io/File;
    # invokes: Lcom/adobe/mobile/RemoteDownload;->createRFC2822Formatter()Ljava/text/SimpleDateFormat;
    invoke-static {}, Lcom/adobe/mobile/RemoteDownload;->access$100()Ljava/text/SimpleDateFormat;

    move-result-object v5

    .line 360
    .local v5, "rfc2822Formatter":Ljava/text/SimpleDateFormat;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/adobe/mobile/RemoteDownload$DownloadFileTask;->url:Ljava/lang/String;

    invoke-static {v0}, Lcom/adobe/mobile/RemoteDownload$DownloadFileTask;->requestConnect(Ljava/lang/String;)Ljava/net/HttpURLConnection;

    move-result-object v6

    .line 362
    .local v6, "connection":Ljava/net/HttpURLConnection;
    const/4 v8, 0x0

    .line 363
    .local v8, "input":Ljava/io/InputStream;
    const/4 v9, 0x0

    .line 365
    .local v9, "output":Ljava/io/OutputStream;
    if-nez v6, :cond_73

    .line 366
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/adobe/mobile/RemoteDownload$DownloadFileTask;->callback:Lcom/adobe/mobile/RemoteDownload$RemoteDownloadBlock;

    if-eqz v0, :cond_72

    .line 367
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/adobe/mobile/RemoteDownload$DownloadFileTask;->callback:Lcom/adobe/mobile/RemoteDownload$RemoteDownloadBlock;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/adobe/mobile/RemoteDownload$RemoteDownloadBlock;->call(ZLjava/io/File;)V

    .line 369
    :cond_72
    return-void

    .line 372
    :cond_73
    move-object/from16 v0, p0

    iget v0, v0, Lcom/adobe/mobile/RemoteDownload$DownloadFileTask;->connectionTimeout:I

    invoke-virtual {v6, v0}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    .line 373
    move-object/from16 v0, p0

    iget v0, v0, Lcom/adobe/mobile/RemoteDownload$DownloadFileTask;->readTimeout:I

    invoke-virtual {v6, v0}, Ljava/net/HttpURLConnection;->setReadTimeout(I)V

    .line 375
    if-eqz v4, :cond_b5

    .line 376
    invoke-virtual {v4}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v0

    # invokes: Lcom/adobe/mobile/RemoteDownload;->getEtagOfFile(Ljava/lang/String;)Ljava/lang/String;
    invoke-static {v0}, Lcom/adobe/mobile/RemoteDownload;->access$200(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/adobe/mobile/StaticMethods;->hexToString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 377
    .local v10, "etag":Ljava/lang/String;
    invoke-virtual {v4}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v0

    # invokes: Lcom/adobe/mobile/RemoteDownload;->getLastModifiedOfFile(Ljava/lang/String;)J
    invoke-static {v0}, Lcom/adobe/mobile/RemoteDownload;->access$300(Ljava/lang/String;)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    .line 379
    .local v11, "date":Ljava/lang/Long;
    invoke-virtual {v11}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_ae

    .line 380
    const-string v0, "If-Modified-Since"

    invoke-virtual {v5, v11}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v0, v1}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 382
    :cond_ae
    if-eqz v10, :cond_b5

    .line 383
    const-string v0, "If-None-Match"

    invoke-virtual {v6, v0, v10}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 387
    .end local v10    # "etag":Ljava/lang/String;
    .end local v11    # "date":Ljava/lang/Long;
    :cond_b5
    :try_start_b5
    invoke-virtual {v6}, Ljava/net/HttpURLConnection;->connect()V

    .line 389
    invoke-virtual {v6}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v0

    const/16 v1, 0x130

    if-ne v0, v1, :cond_f4

    .line 390
    const-string v0, "Cached Files - File has not been modified since last download. (%s)"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    move-object/from16 v2, p0

    iget-object v2, v2, Lcom/adobe/mobile/RemoteDownload$DownloadFileTask;->url:Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logDebugFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 391
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/adobe/mobile/RemoteDownload$DownloadFileTask;->callback:Lcom/adobe/mobile/RemoteDownload$RemoteDownloadBlock;

    if-eqz v0, :cond_dd

    .line 392
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/adobe/mobile/RemoteDownload$DownloadFileTask;->callback:Lcom/adobe/mobile/RemoteDownload$RemoteDownloadBlock;

    const/4 v1, 0x0

    invoke-interface {v0, v1, v4}, Lcom/adobe/mobile/RemoteDownload$RemoteDownloadBlock;->call(ZLjava/io/File;)V
    :try_end_dd
    .catch Ljava/net/SocketTimeoutException; {:try_start_b5 .. :try_end_dd} :catch_22e
    .catch Ljava/io/IOException; {:try_start_b5 .. :try_end_dd} :catch_26e
    .catch Ljava/lang/Exception; {:try_start_b5 .. :try_end_dd} :catch_2ae
    .catch Ljava/lang/Error; {:try_start_b5 .. :try_end_dd} :catch_2ee
    .catchall {:try_start_b5 .. :try_end_dd} :catchall_32c

    .line 471
    :cond_dd
    nop

    .line 472
    .line 474
    nop

    .line 475
    .line 477
    :try_start_df
    invoke-virtual {v6}, Ljava/net/HttpURLConnection;->disconnect()V
    :try_end_e2
    .catch Ljava/io/IOException; {:try_start_df .. :try_end_e2} :catch_e3

    .line 481
    goto :goto_f3

    .line 479
    :catch_e3
    move-exception v10

    .line 480
    .local v10, "ex":Ljava/io/IOException;
    const-string v0, "Cached Files - Exception while attempting to close streams (%s)"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v10}, Ljava/io/IOException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logWarningFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 394
    .end local v10    # "ex":Ljava/io/IOException;
    :goto_f3
    return-void

    .line 396
    :cond_f4
    :try_start_f4
    invoke-virtual {v6}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v0

    const/16 v1, 0x194

    if-ne v0, v1, :cond_130

    .line 397
    const-string v0, "Cached Files - File not found. (%s)"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    move-object/from16 v2, p0

    iget-object v2, v2, Lcom/adobe/mobile/RemoteDownload$DownloadFileTask;->url:Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logDebugFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 398
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/adobe/mobile/RemoteDownload$DownloadFileTask;->callback:Lcom/adobe/mobile/RemoteDownload$RemoteDownloadBlock;

    if-eqz v0, :cond_119

    .line 399
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/adobe/mobile/RemoteDownload$DownloadFileTask;->callback:Lcom/adobe/mobile/RemoteDownload$RemoteDownloadBlock;

    const/4 v1, 0x0

    invoke-interface {v0, v1, v4}, Lcom/adobe/mobile/RemoteDownload$RemoteDownloadBlock;->call(ZLjava/io/File;)V
    :try_end_119
    .catch Ljava/net/SocketTimeoutException; {:try_start_f4 .. :try_end_119} :catch_22e
    .catch Ljava/io/IOException; {:try_start_f4 .. :try_end_119} :catch_26e
    .catch Ljava/lang/Exception; {:try_start_f4 .. :try_end_119} :catch_2ae
    .catch Ljava/lang/Error; {:try_start_f4 .. :try_end_119} :catch_2ee
    .catchall {:try_start_f4 .. :try_end_119} :catchall_32c

    .line 471
    :cond_119
    nop

    .line 472
    .line 474
    nop

    .line 475
    .line 477
    :try_start_11b
    invoke-virtual {v6}, Ljava/net/HttpURLConnection;->disconnect()V
    :try_end_11e
    .catch Ljava/io/IOException; {:try_start_11b .. :try_end_11e} :catch_11f

    .line 481
    goto :goto_12f

    .line 479
    :catch_11f
    move-exception v10

    .line 480
    .local v10, "ex":Ljava/io/IOException;
    const-string v0, "Cached Files - Exception while attempting to close streams (%s)"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v10}, Ljava/io/IOException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logWarningFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 401
    .end local v10    # "ex":Ljava/io/IOException;
    :goto_12f
    return-void

    .line 403
    :cond_130
    :try_start_130
    invoke-virtual {v6}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v0

    const/16 v1, 0xc8

    if-eq v0, v1, :cond_17e

    .line 404
    const-string v0, "Cached Files - File could not be downloaded from URL (%s) Response: (%d) Message: (%s)"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    move-object/from16 v2, p0

    iget-object v2, v2, Lcom/adobe/mobile/RemoteDownload$DownloadFileTask;->url:Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-virtual {v6}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x1

    aput-object v2, v1, v3

    invoke-virtual {v6}, Ljava/net/HttpURLConnection;->getResponseMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x2

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logWarningFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 405
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/adobe/mobile/RemoteDownload$DownloadFileTask;->callback:Lcom/adobe/mobile/RemoteDownload$RemoteDownloadBlock;

    if-eqz v0, :cond_167

    .line 406
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/adobe/mobile/RemoteDownload$DownloadFileTask;->callback:Lcom/adobe/mobile/RemoteDownload$RemoteDownloadBlock;

    const/4 v1, 0x0

    invoke-interface {v0, v1, v4}, Lcom/adobe/mobile/RemoteDownload$RemoteDownloadBlock;->call(ZLjava/io/File;)V
    :try_end_167
    .catch Ljava/net/SocketTimeoutException; {:try_start_130 .. :try_end_167} :catch_22e
    .catch Ljava/io/IOException; {:try_start_130 .. :try_end_167} :catch_26e
    .catch Ljava/lang/Exception; {:try_start_130 .. :try_end_167} :catch_2ae
    .catch Ljava/lang/Error; {:try_start_130 .. :try_end_167} :catch_2ee
    .catchall {:try_start_130 .. :try_end_167} :catchall_32c

    .line 471
    :cond_167
    nop

    .line 472
    .line 474
    nop

    .line 475
    .line 477
    :try_start_169
    invoke-virtual {v6}, Ljava/net/HttpURLConnection;->disconnect()V
    :try_end_16c
    .catch Ljava/io/IOException; {:try_start_169 .. :try_end_16c} :catch_16d

    .line 481
    goto :goto_17d

    .line 479
    :catch_16d
    move-exception v10

    .line 480
    .local v10, "ex":Ljava/io/IOException;
    const-string v0, "Cached Files - Exception while attempting to close streams (%s)"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v10}, Ljava/io/IOException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logWarningFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 408
    .end local v10    # "ex":Ljava/io/IOException;
    :goto_17d
    return-void

    .line 411
    :cond_17e
    if-eqz v4, :cond_18b

    .line 412
    move-object/from16 v0, p0

    :try_start_182
    iget-object v0, v0, Lcom/adobe/mobile/RemoteDownload$DownloadFileTask;->url:Ljava/lang/String;

    move-object/from16 v1, p0

    iget-object v1, v1, Lcom/adobe/mobile/RemoteDownload$DownloadFileTask;->directory:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/adobe/mobile/RemoteDownload;->deleteCachedDataForURL(Ljava/lang/String;Ljava/lang/String;)Z

    .line 415
    :cond_18b
    new-instance v10, Ljava/util/Date;

    invoke-virtual {v6}, Ljava/net/HttpURLConnection;->getLastModified()J

    move-result-wide v0

    invoke-direct {v10, v0, v1}, Ljava/util/Date;-><init>(J)V

    .line 416
    .local v10, "lastModifiedDate":Ljava/util/Date;
    const-string v0, "ETag"

    invoke-virtual {v6, v0}, Ljava/net/HttpURLConnection;->getHeaderField(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 418
    .local v11, "newETag":Ljava/lang/String;
    if-eqz v11, :cond_1a0

    .line 419
    invoke-static {v11}, Lcom/adobe/mobile/StaticMethods;->getHexString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 423
    :cond_1a0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/adobe/mobile/RemoteDownload$DownloadFileTask;->url:Ljava/lang/String;

    move-object/from16 v1, p0

    iget-object v1, v1, Lcom/adobe/mobile/RemoteDownload$DownloadFileTask;->directory:Ljava/lang/String;

    # invokes: Lcom/adobe/mobile/RemoteDownload;->getNewCachedFile(Ljava/lang/String;Ljava/util/Date;Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;
    invoke-static {v0, v10, v11, v1}, Lcom/adobe/mobile/RemoteDownload;->access$400(Ljava/lang/String;Ljava/util/Date;Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;

    move-result-object v7

    .line 425
    .local v7, "newCacheFile":Ljava/io/File;
    if-nez v7, :cond_1d4

    .line 426
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/adobe/mobile/RemoteDownload$DownloadFileTask;->callback:Lcom/adobe/mobile/RemoteDownload$RemoteDownloadBlock;

    if-eqz v0, :cond_1bd

    .line 427
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/adobe/mobile/RemoteDownload$DownloadFileTask;->callback:Lcom/adobe/mobile/RemoteDownload$RemoteDownloadBlock;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/adobe/mobile/RemoteDownload$RemoteDownloadBlock;->call(ZLjava/io/File;)V
    :try_end_1bd
    .catch Ljava/net/SocketTimeoutException; {:try_start_182 .. :try_end_1bd} :catch_22e
    .catch Ljava/io/IOException; {:try_start_182 .. :try_end_1bd} :catch_26e
    .catch Ljava/lang/Exception; {:try_start_182 .. :try_end_1bd} :catch_2ae
    .catch Ljava/lang/Error; {:try_start_182 .. :try_end_1bd} :catch_2ee
    .catchall {:try_start_182 .. :try_end_1bd} :catchall_32c

    .line 471
    :cond_1bd
    nop

    .line 472
    .line 474
    nop

    .line 475
    .line 477
    :try_start_1bf
    invoke-virtual {v6}, Ljava/net/HttpURLConnection;->disconnect()V
    :try_end_1c2
    .catch Ljava/io/IOException; {:try_start_1bf .. :try_end_1c2} :catch_1c3

    .line 481
    goto :goto_1d3

    .line 479
    :catch_1c3
    move-exception v12

    .line 480
    .local v12, "ex":Ljava/io/IOException;
    const-string v0, "Cached Files - Exception while attempting to close streams (%s)"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v12}, Ljava/io/IOException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logWarningFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 429
    .end local v12    # "ex":Ljava/io/IOException;
    :goto_1d3
    return-void

    .line 432
    :cond_1d4
    :try_start_1d4
    invoke-virtual {v6}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v0

    move-object v8, v0

    .line 433
    new-instance v0, Ljava/io/FileOutputStream;

    invoke-direct {v0, v7}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    move-object v9, v0

    .line 435
    const/16 v0, 0x1000

    new-array v12, v0, [B

    .line 438
    .local v12, "data":[B
    :goto_1e3
    invoke-virtual {v8, v12}, Ljava/io/InputStream;->read([B)I

    move-result v0

    move v13, v0

    .local v13, "count":I
    const/4 v1, -0x1

    if-eq v0, v1, :cond_1f0

    .line 439
    const/4 v0, 0x0

    invoke-virtual {v9, v12, v0, v13}, Ljava/io/OutputStream;->write([BII)V

    goto :goto_1e3

    .line 442
    :cond_1f0
    const-string v0, "Cached Files - Caching successful (%s)"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    move-object/from16 v2, p0

    iget-object v2, v2, Lcom/adobe/mobile/RemoteDownload$DownloadFileTask;->url:Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logDebugFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 444
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/adobe/mobile/RemoteDownload$DownloadFileTask;->callback:Lcom/adobe/mobile/RemoteDownload$RemoteDownloadBlock;

    if-eqz v0, :cond_20d

    .line 445
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/adobe/mobile/RemoteDownload$DownloadFileTask;->callback:Lcom/adobe/mobile/RemoteDownload$RemoteDownloadBlock;

    const/4 v1, 0x1

    invoke-interface {v0, v1, v7}, Lcom/adobe/mobile/RemoteDownload$RemoteDownloadBlock;->call(ZLjava/io/File;)V
    :try_end_20d
    .catch Ljava/net/SocketTimeoutException; {:try_start_1d4 .. :try_end_20d} :catch_22e
    .catch Ljava/io/IOException; {:try_start_1d4 .. :try_end_20d} :catch_26e
    .catch Ljava/lang/Exception; {:try_start_1d4 .. :try_end_20d} :catch_2ae
    .catch Ljava/lang/Error; {:try_start_1d4 .. :try_end_20d} :catch_2ee
    .catchall {:try_start_1d4 .. :try_end_20d} :catchall_32c

    .line 471
    .end local v10    # "lastModifiedDate":Ljava/util/Date;
    .end local v11    # "newETag":Ljava/lang/String;
    .end local v12    # "data":[B
    .end local v13    # "count":I
    :cond_20d
    if-eqz v9, :cond_212

    .line 472
    :try_start_20f
    invoke-virtual {v9}, Ljava/io/OutputStream;->close()V

    .line 474
    :cond_212
    if-eqz v8, :cond_217

    .line 475
    invoke-virtual {v8}, Ljava/io/InputStream;->close()V

    .line 477
    :cond_217
    invoke-virtual {v6}, Ljava/net/HttpURLConnection;->disconnect()V
    :try_end_21a
    .catch Ljava/io/IOException; {:try_start_20f .. :try_end_21a} :catch_21c

    .line 481
    goto/16 :goto_34c

    .line 479
    :catch_21c
    move-exception v10

    .line 480
    .local v10, "ex":Ljava/io/IOException;
    const-string v0, "Cached Files - Exception while attempting to close streams (%s)"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v10}, Ljava/io/IOException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logWarningFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 482
    .end local v10    # "ex":Ljava/io/IOException;
    goto/16 :goto_34c

    .line 447
    .end local v7    # "newCacheFile":Ljava/io/File;
    :catch_22e
    move-exception v10

    .line 448
    .local v10, "e":Ljava/net/SocketTimeoutException;
    const-string v0, "Cached Files - Timed out making request (%s)"

    const/4 v1, 0x1

    :try_start_232
    new-array v1, v1, [Ljava/lang/Object;

    move-object/from16 v2, p0

    iget-object v2, v2, Lcom/adobe/mobile/RemoteDownload$DownloadFileTask;->url:Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logWarningFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 449
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/adobe/mobile/RemoteDownload$DownloadFileTask;->callback:Lcom/adobe/mobile/RemoteDownload$RemoteDownloadBlock;

    if-eqz v0, :cond_24d

    .line 450
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/adobe/mobile/RemoteDownload$DownloadFileTask;->callback:Lcom/adobe/mobile/RemoteDownload$RemoteDownloadBlock;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/adobe/mobile/RemoteDownload$RemoteDownloadBlock;->call(ZLjava/io/File;)V
    :try_end_24d
    .catchall {:try_start_232 .. :try_end_24d} :catchall_32c

    .line 471
    .end local v10    # "e":Ljava/net/SocketTimeoutException;
    :cond_24d
    if-eqz v9, :cond_252

    .line 472
    :try_start_24f
    invoke-virtual {v9}, Ljava/io/OutputStream;->close()V

    .line 474
    :cond_252
    if-eqz v8, :cond_257

    .line 475
    invoke-virtual {v8}, Ljava/io/InputStream;->close()V

    .line 477
    :cond_257
    invoke-virtual {v6}, Ljava/net/HttpURLConnection;->disconnect()V
    :try_end_25a
    .catch Ljava/io/IOException; {:try_start_24f .. :try_end_25a} :catch_25c

    .line 481
    goto/16 :goto_34c

    .line 479
    :catch_25c
    move-exception v10

    .line 480
    .local v10, "ex":Ljava/io/IOException;
    const-string v0, "Cached Files - Exception while attempting to close streams (%s)"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v10}, Ljava/io/IOException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logWarningFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 482
    .end local v10    # "ex":Ljava/io/IOException;
    goto/16 :goto_34c

    .line 452
    :catch_26e
    move-exception v10

    .line 453
    .local v10, "e":Ljava/io/IOException;
    const-string v0, "Cached Files - IOException while making request (%s)"

    const/4 v1, 0x1

    :try_start_272
    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v10}, Ljava/io/IOException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logWarningFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 454
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/adobe/mobile/RemoteDownload$DownloadFileTask;->callback:Lcom/adobe/mobile/RemoteDownload$RemoteDownloadBlock;

    if-eqz v0, :cond_28d

    .line 455
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/adobe/mobile/RemoteDownload$DownloadFileTask;->callback:Lcom/adobe/mobile/RemoteDownload$RemoteDownloadBlock;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/adobe/mobile/RemoteDownload$RemoteDownloadBlock;->call(ZLjava/io/File;)V
    :try_end_28d
    .catchall {:try_start_272 .. :try_end_28d} :catchall_32c

    .line 471
    .end local v10    # "e":Ljava/io/IOException;
    :cond_28d
    if-eqz v9, :cond_292

    .line 472
    :try_start_28f
    invoke-virtual {v9}, Ljava/io/OutputStream;->close()V

    .line 474
    :cond_292
    if-eqz v8, :cond_297

    .line 475
    invoke-virtual {v8}, Ljava/io/InputStream;->close()V

    .line 477
    :cond_297
    invoke-virtual {v6}, Ljava/net/HttpURLConnection;->disconnect()V
    :try_end_29a
    .catch Ljava/io/IOException; {:try_start_28f .. :try_end_29a} :catch_29c

    .line 481
    goto/16 :goto_34c

    .line 479
    :catch_29c
    move-exception v10

    .line 480
    .local v10, "ex":Ljava/io/IOException;
    const-string v0, "Cached Files - Exception while attempting to close streams (%s)"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v10}, Ljava/io/IOException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logWarningFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 482
    .end local v10    # "ex":Ljava/io/IOException;
    goto/16 :goto_34c

    .line 457
    :catch_2ae
    move-exception v10

    .line 458
    .local v10, "e":Ljava/lang/Exception;
    const-string v0, "Cached Files - Unexpected exception while attempting to get remote file (%s)"

    const/4 v1, 0x1

    :try_start_2b2
    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v10}, Ljava/lang/Exception;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logWarningFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 459
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/adobe/mobile/RemoteDownload$DownloadFileTask;->callback:Lcom/adobe/mobile/RemoteDownload$RemoteDownloadBlock;

    if-eqz v0, :cond_2cd

    .line 460
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/adobe/mobile/RemoteDownload$DownloadFileTask;->callback:Lcom/adobe/mobile/RemoteDownload$RemoteDownloadBlock;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/adobe/mobile/RemoteDownload$RemoteDownloadBlock;->call(ZLjava/io/File;)V
    :try_end_2cd
    .catchall {:try_start_2b2 .. :try_end_2cd} :catchall_32c

    .line 471
    .end local v10    # "e":Ljava/lang/Exception;
    :cond_2cd
    if-eqz v9, :cond_2d2

    .line 472
    :try_start_2cf
    invoke-virtual {v9}, Ljava/io/OutputStream;->close()V

    .line 474
    :cond_2d2
    if-eqz v8, :cond_2d7

    .line 475
    invoke-virtual {v8}, Ljava/io/InputStream;->close()V

    .line 477
    :cond_2d7
    invoke-virtual {v6}, Ljava/net/HttpURLConnection;->disconnect()V
    :try_end_2da
    .catch Ljava/io/IOException; {:try_start_2cf .. :try_end_2da} :catch_2dc

    .line 481
    goto/16 :goto_34c

    .line 479
    :catch_2dc
    move-exception v10

    .line 480
    .local v10, "ex":Ljava/io/IOException;
    const-string v0, "Cached Files - Exception while attempting to close streams (%s)"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v10}, Ljava/io/IOException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logWarningFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 482
    .end local v10    # "ex":Ljava/io/IOException;
    goto/16 :goto_34c

    .line 462
    :catch_2ee
    move-exception v10

    .line 463
    .local v10, "e":Ljava/lang/Error;
    const-string v0, "Cached Files - Unexpected error while attempting to get remote file (%s)"

    const/4 v1, 0x1

    :try_start_2f2
    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v10}, Ljava/lang/Error;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logWarningFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 464
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/adobe/mobile/RemoteDownload$DownloadFileTask;->callback:Lcom/adobe/mobile/RemoteDownload$RemoteDownloadBlock;

    if-eqz v0, :cond_30d

    .line 465
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/adobe/mobile/RemoteDownload$DownloadFileTask;->callback:Lcom/adobe/mobile/RemoteDownload$RemoteDownloadBlock;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/adobe/mobile/RemoteDownload$RemoteDownloadBlock;->call(ZLjava/io/File;)V
    :try_end_30d
    .catchall {:try_start_2f2 .. :try_end_30d} :catchall_32c

    .line 471
    .end local v10    # "e":Ljava/lang/Error;
    :cond_30d
    if-eqz v9, :cond_312

    .line 472
    :try_start_30f
    invoke-virtual {v9}, Ljava/io/OutputStream;->close()V

    .line 474
    :cond_312
    if-eqz v8, :cond_317

    .line 475
    invoke-virtual {v8}, Ljava/io/InputStream;->close()V

    .line 477
    :cond_317
    invoke-virtual {v6}, Ljava/net/HttpURLConnection;->disconnect()V
    :try_end_31a
    .catch Ljava/io/IOException; {:try_start_30f .. :try_end_31a} :catch_31b

    .line 481
    goto :goto_34c

    .line 479
    :catch_31b
    move-exception v10

    .line 480
    .local v10, "ex":Ljava/io/IOException;
    const-string v0, "Cached Files - Exception while attempting to close streams (%s)"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v10}, Ljava/io/IOException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logWarningFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 482
    .end local v10    # "ex":Ljava/io/IOException;
    goto :goto_34c

    .line 470
    :catchall_32c
    move-exception v14

    .line 471
    if-eqz v9, :cond_332

    .line 472
    :try_start_32f
    invoke-virtual {v9}, Ljava/io/OutputStream;->close()V

    .line 474
    :cond_332
    if-eqz v8, :cond_337

    .line 475
    invoke-virtual {v8}, Ljava/io/InputStream;->close()V

    .line 477
    :cond_337
    invoke-virtual {v6}, Ljava/net/HttpURLConnection;->disconnect()V
    :try_end_33a
    .catch Ljava/io/IOException; {:try_start_32f .. :try_end_33a} :catch_33b

    .line 481
    goto :goto_34b

    .line 479
    :catch_33b
    move-exception v15

    .line 480
    .local v15, "ex":Ljava/io/IOException;
    const-string v0, "Cached Files - Exception while attempting to close streams (%s)"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v15}, Ljava/io/IOException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logWarningFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 481
    .end local v15    # "ex":Ljava/io/IOException;
    :goto_34b
    throw v14

    .line 483
    :goto_34c
    return-void
.end method

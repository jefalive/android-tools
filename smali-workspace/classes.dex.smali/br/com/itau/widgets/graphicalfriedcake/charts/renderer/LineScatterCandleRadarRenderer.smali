.class public abstract Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/LineScatterCandleRadarRenderer;
.super Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/DataRenderer;
.source "LineScatterCandleRadarRenderer.java"


# direct methods
.method public constructor <init>(Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/ChartAnimator;Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;)V
    .registers 3
    .param p1, "animator"    # Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/ChartAnimator;
    .param p2, "viewPortHandler"    # Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;

    .line 17
    invoke-direct {p0, p1, p2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/DataRenderer;-><init>(Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/ChartAnimator;Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;)V

    .line 18
    return-void
.end method


# virtual methods
.method protected drawHighlightLines(Landroid/graphics/Canvas;[FZZ)V
    .registers 11
    .param p1, "c"    # Landroid/graphics/Canvas;
    .param p2, "pts"    # [F
    .param p3, "horizontal"    # Z
    .param p4, "vertical"    # Z

    .line 32
    if-eqz p4, :cond_14

    .line 33
    move-object v0, p1

    const/4 v1, 0x0

    aget v1, p2, v1

    const/4 v2, 0x1

    aget v2, p2, v2

    const/4 v3, 0x2

    aget v3, p2, v3

    const/4 v4, 0x3

    aget v4, p2, v4

    iget-object v5, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/LineScatterCandleRadarRenderer;->mHighlightPaint:Landroid/graphics/Paint;

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 37
    :cond_14
    if-eqz p3, :cond_28

    .line 38
    move-object v0, p1

    const/4 v1, 0x4

    aget v1, p2, v1

    const/4 v2, 0x5

    aget v2, p2, v2

    const/4 v3, 0x6

    aget v3, p2, v3

    const/4 v4, 0x7

    aget v4, p2, v4

    iget-object v5, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/LineScatterCandleRadarRenderer;->mHighlightPaint:Landroid/graphics/Paint;

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 39
    :cond_28
    return-void
.end method

.class public final Lbr/com/itau/sdk/android/core/endpoint/y;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final d:[S

.field private static e:I


# instance fields
.field private final a:Ljava/util/concurrent/CountDownLatch;

.field private final b:Lbr/com/itau/sdk/android/core/endpoint/TokenErrorHandler;

.field private c:I


# direct methods
.method static constructor <clinit>()V
    .registers 3

    const/16 v0, 0xc8

    new-array v0, v0, [S

    fill-array-data v0, :array_e

    sput-object v0, Lbr/com/itau/sdk/android/core/endpoint/y;->d:[S

    const/16 v0, 0x1d

    sput v0, Lbr/com/itau/sdk/android/core/endpoint/y;->e:I

    return-void

    :array_e
    .array-data 2
        0x7bs
        -0x13s
        0x2s
        -0x31s
        -0x1bs
        0x4s
        0x6s
        -0x9s
        0x4es
        -0x49s
        -0x5s
        -0x8s
        -0x6bs
        0x75s
        0x3s
        0x5s
        -0xbs
        -0x19s
        0x9s
        -0x13s
        0x8s
        0xbs
        0x2s
        -0x11s
        0xfs
        0x1s
        0x0s
        0x0s
        -0x95s
        0x74s
        0x4fs
        -0x45s
        -0x13s
        0xfs
        -0xas
        -0x1s
        0xfs
        0x45s
        -0x4es
        0x9s
        -0x9s
        0x6s
        -0xds
        0x8s
        0x4ds
        -0x54s
        0x5s
        0x4s
        0x6s
        -0x9s
        0x4es
        -0x50s
        0xfs
        -0x11s
        0x11s
        0x41s
        -0x53s
        0xes
        -0xds
        0x52s
        -0x56s
        0x15s
        -0xbs
        0x3s
        0x5s
        0x3s
        -0x3s
        -0xbs
        -0x25s
        -0x6s
        0xfs
        -0xds
        0x0s
        -0x3s
        0x5s
        -0x4s
        0xfs
        0x1s
        0x44s
        -0x54s
        0x5s
        0x4s
        0x6s
        -0x9s
        0x4es
        -0x54s
        0xcs
        -0xas
        0xds
        0x4s
        -0x3s
        0x2fs
        -0x53s
        0xes
        0x2s
        -0x12s
        0x3s
        0x9s
        -0xbs
        -0x5s
        0x35s
        -0x1ds
        -0x13s
        0x13s
        0x41s
        -0x45s
        -0xes
        -0x1s
        -0x6ds
        0xc1s
        -0x56s
        0x15s
        -0x19s
        0x11s
        -0x6s
        0x43s
        0xcs
        -0x4es
        -0x75s
        0x74s
        0x4fs
        -0xc9s
        0xc9s
        -0x50s
        0x1s
        -0x4s
        0x0s
        -0x7as
        0x77s
        0x11s
        -0x7s
        0x4cs
        -0x53s
        0xes
        -0x2s
        -0xes
        0xcs
        -0x9s
        0x52s
        -0x41s
        0x41s
        -0x4fs
        -0x1s
        0xbs
        -0xds
        0x11s
        -0x86s
        0x4s
        0x74s
        0x59s
        -0x41s
        0x2s
        -0xds
        0xbs
        -0x2s
        -0x7s
        0x19s
        -0x12s
        -0xds
        0x9s
        0x8s
        -0xbs
        0x1es
        -0x27s
        0x8s
        0xbs
        -0x3s
        -0xds
        0x50s
        -0x18s
        0x1as
        -0x2s
        -0xes
        0x0s
        0x0s
        0x0s
        0x0s
        0x0s
        0x0s
        0x0s
        0x0s
        0x0s
        0x0s
        0x0s
        0xes
        -0x5bs
        -0x1bs
        0x4s
        0x6s
        -0x9s
        0x4es
        -0x43s
        0x2s
        -0xds
        0xbs
        -0x2s
        -0x7s
        0xbs
        -0x3s
        -0xbs
    .end array-data
.end method

.method constructor <init>(Lbr/com/itau/sdk/android/core/endpoint/TokenErrorHandler;)V
    .registers 4

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, Lbr/com/itau/sdk/android/core/endpoint/y;->a:Ljava/util/concurrent/CountDownLatch;

    .line 34
    const/4 v0, 0x1

    iput v0, p0, Lbr/com/itau/sdk/android/core/endpoint/y;->c:I

    .line 37
    iput-object p1, p0, Lbr/com/itau/sdk/android/core/endpoint/y;->b:Lbr/com/itau/sdk/android/core/endpoint/TokenErrorHandler;

    .line 38
    return-void
.end method

.method private static a(III)Ljava/lang/String;
    .registers 9

    sget-object v5, Lbr/com/itau/sdk/android/core/endpoint/y;->d:[S

    rsub-int/lit8 p1, p1, 0x3b

    add-int/lit8 p0, p0, 0x4

    add-int/lit8 p2, p2, 0x49

    const/4 v4, 0x0

    new-instance v0, Ljava/lang/String;

    new-array v1, p1, [B

    if-nez v5, :cond_16

    move v2, p0

    move v3, p2

    :goto_11
    neg-int v3, v3

    add-int p2, v2, v3

    add-int/lit8 p0, p0, 0x1

    :cond_16
    move v2, v4

    int-to-byte v3, p2

    add-int/lit8 v4, v4, 0x1

    aput-byte v3, v1, v2

    if-ne v4, p1, :cond_23

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Ljava/lang/String;-><init>([BI)V

    return-object v0

    :cond_23
    move v2, p2

    aget-short v3, v5, p0

    goto :goto_11
.end method

.method private a()V
    .registers 6

    .line 307
    sget-object v0, Lbr/com/itau/sdk/android/core/endpoint/y;->d:[S

    const/16 v1, 0x3d

    aget-short v0, v0, v1

    const/16 v1, 0x91

    const/16 v2, 0x32

    invoke-static {v1, v0, v2}, Lbr/com/itau/sdk/android/core/endpoint/y;->a(III)Ljava/lang/String;

    move-result-object v4

    .line 308
    invoke-static {}, Lbr/com/itau/sdk/android/core/BackendClient;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, Lbr/com/itau/sdk/android/core/endpoint/y;->d:[S

    const/16 v2, 0x3d

    aget-short v1, v1, v2

    const/16 v2, 0x91

    const/16 v3, 0x32

    invoke-static {v2, v1, v3}, Lbr/com/itau/sdk/android/core/endpoint/y;->a(III)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbr/com/itau/security/token/til/Token;->handleTokenAppData(Landroid/content/Context;Ljava/lang/String;)V

    .line 309
    return-void
.end method

.method private synthetic a(I)V
    .registers 3

    .line 76
    iput p1, p0, Lbr/com/itau/sdk/android/core/endpoint/y;->c:I

    .line 77
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/endpoint/y;->a:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 78
    return-void
.end method

.method static synthetic a(Lbr/com/itau/sdk/android/core/endpoint/y;I)V
    .registers 2

    invoke-direct {p0, p1}, Lbr/com/itau/sdk/android/core/endpoint/y;->a(I)V

    return-void
.end method

.method private a(Lbr/com/itau/sdk/android/core/model/UiModelDTO;)Z
    .registers 15

    .line 119
    if-eqz p1, :cond_11d

    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/model/UiModelDTO;->getTokenAplicativo()Lbr/com/itau/sdk/android/core/model/TokenDTO;

    move-result-object v0

    if-eqz v0, :cond_11d

    .line 121
    :try_start_8
    invoke-static {}, Lbr/com/itau/sdk/android/core/BackendClient;->getInstance()Lbr/com/itau/sdk/android/core/BackendClient;

    move-result-object v0

    invoke-virtual {v0}, Lbr/com/itau/sdk/android/core/BackendClient;->getTokenAppByPassValidation()Lbr/com/itau/sdk/android/core/token/TokenAppByPassValidation;
    :try_end_f
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_f} :catch_c3
    .catchall {:try_start_8 .. :try_end_f} :catchall_104

    move-result-object v0

    if-eqz v0, :cond_1f

    .line 123
    :try_start_12
    invoke-static {}, Lbr/com/itau/sdk/android/core/BackendClient;->getInstance()Lbr/com/itau/sdk/android/core/BackendClient;

    move-result-object v0

    invoke-virtual {v0}, Lbr/com/itau/sdk/android/core/BackendClient;->getTokenAppByPassValidation()Lbr/com/itau/sdk/android/core/token/TokenAppByPassValidation;

    move-result-object v0

    invoke-interface {v0}, Lbr/com/itau/sdk/android/core/token/TokenAppByPassValidation;->onStartedByPass()V
    :try_end_1d
    .catch Ljava/lang/Exception; {:try_start_12 .. :try_end_1d} :catch_1e
    .catchall {:try_start_12 .. :try_end_1d} :catchall_104

    .line 126
    goto :goto_1f

    .line 124
    :catch_1e
    move-exception v6

    .line 129
    :cond_1f
    :goto_1f
    :try_start_1f
    invoke-direct {p0, p1}, Lbr/com/itau/sdk/android/core/endpoint/y;->c(Lbr/com/itau/sdk/android/core/model/UiModelDTO;)Ljava/lang/String;

    move-result-object v6

    .line 131
    if-eqz v6, :cond_ab

    invoke-virtual {v6}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_ab

    .line 134
    invoke-static {}, Lbr/com/itau/sdk/android/core/token/c;->d()Lbr/com/itau/sdk/android/core/token/c;

    move-result-object v0

    invoke-virtual {v0, v6}, Lbr/com/itau/sdk/android/core/token/c;->a(Ljava/lang/String;)Lbr/com/itau/sdk/android/core/SDKCustomUiController$ValidacaoAppResponseDTO;

    move-result-object v7

    .line 135
    if-eqz v7, :cond_3d

    invoke-virtual {v7}, Lbr/com/itau/sdk/android/core/SDKCustomUiController$ValidacaoAppResponseDTO;->isSucesso()Z

    move-result v0

    if-eqz v0, :cond_3d

    const/4 v8, 0x1

    goto :goto_3e

    :cond_3d
    const/4 v8, 0x0

    .line 137
    :goto_3e
    if-eqz v8, :cond_88

    .line 138
    invoke-virtual {v7}, Lbr/com/itau/sdk/android/core/SDKCustomUiController$ValidacaoAppResponseDTO;->isTokenValidoInformarNovoCodigo()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_80

    .line 139
    invoke-direct {p0, p1}, Lbr/com/itau/sdk/android/core/endpoint/y;->d(Lbr/com/itau/sdk/android/core/model/UiModelDTO;)Landroid/util/Pair;

    move-result-object v9

    .line 140
    if-eqz v9, :cond_7e

    .line 143
    invoke-static {}, Lbr/com/itau/sdk/android/core/token/c;->d()Lbr/com/itau/sdk/android/core/token/c;

    move-result-object v0

    iget-object v1, v9, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Lbr/com/itau/security/token/model/OTPModel;

    .line 144
    invoke-virtual {v1}, Lbr/com/itau/security/token/model/OTPModel;->token()Ljava/lang/String;

    move-result-object v1

    iget-object v2, v9, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v2, Lbr/com/itau/security/token/model/OTPModel;

    .line 145
    invoke-virtual {v2}, Lbr/com/itau/security/token/model/OTPModel;->token()Ljava/lang/String;

    move-result-object v2

    .line 144
    invoke-virtual {v0, v1, v2}, Lbr/com/itau/sdk/android/core/token/c;->c(Ljava/lang/String;Ljava/lang/String;)Lbr/com/itau/sdk/android/core/SDKCustomUiController$ValidacaoAppResponseDTO;

    move-result-object v7

    .line 146
    if-eqz v7, :cond_7c

    .line 147
    invoke-virtual {v7}, Lbr/com/itau/sdk/android/core/SDKCustomUiController$ValidacaoAppResponseDTO;->isSucesso()Z

    move-result v0

    if-eqz v0, :cond_7c

    .line 148
    invoke-virtual {v7}, Lbr/com/itau/sdk/android/core/SDKCustomUiController$ValidacaoAppResponseDTO;->isTokenValido()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_7c

    const/4 v8, 0x1

    goto :goto_7d

    :cond_7c
    const/4 v8, 0x0

    :goto_7d
    goto :goto_7f

    .line 150
    :cond_7e
    const/4 v8, 0x0

    .line 152
    :goto_7f
    goto :goto_88

    .line 153
    :cond_80
    invoke-virtual {v7}, Lbr/com/itau/sdk/android/core/SDKCustomUiController$ValidacaoAppResponseDTO;->isTokenValido()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v8

    .line 157
    :cond_88
    :goto_88
    if-nez v8, :cond_92

    .line 158
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lbr/com/itau/sdk/android/core/model/UiModelDTO;->setTokenAplicativo(Lbr/com/itau/sdk/android/core/model/TokenDTO;)V

    .line 159
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lbr/com/itau/sdk/android/core/model/UiModelDTO;->setNumeroSerieTokenAplicativo(Ljava/lang/String;)V
    :try_end_92
    .catch Ljava/lang/Exception; {:try_start_1f .. :try_end_92} :catch_c3
    .catchall {:try_start_1f .. :try_end_92} :catchall_104

    .line 162
    :cond_92
    move v9, v8

    .line 170
    invoke-static {}, Lbr/com/itau/sdk/android/core/BackendClient;->getInstance()Lbr/com/itau/sdk/android/core/BackendClient;

    move-result-object v0

    invoke-virtual {v0}, Lbr/com/itau/sdk/android/core/BackendClient;->getTokenAppByPassValidation()Lbr/com/itau/sdk/android/core/token/TokenAppByPassValidation;

    move-result-object v0

    if-eqz v0, :cond_aa

    .line 172
    :try_start_9d
    invoke-static {}, Lbr/com/itau/sdk/android/core/BackendClient;->getInstance()Lbr/com/itau/sdk/android/core/BackendClient;

    move-result-object v0

    invoke-virtual {v0}, Lbr/com/itau/sdk/android/core/BackendClient;->getTokenAppByPassValidation()Lbr/com/itau/sdk/android/core/token/TokenAppByPassValidation;

    move-result-object v0

    invoke-interface {v0}, Lbr/com/itau/sdk/android/core/token/TokenAppByPassValidation;->onFinishedByPass()V
    :try_end_a8
    .catch Ljava/lang/Exception; {:try_start_9d .. :try_end_a8} :catch_a9

    .line 175
    goto :goto_aa

    .line 173
    :catch_a9
    move-exception v10

    .line 162
    :cond_aa
    :goto_aa
    return v9

    .line 170
    :cond_ab
    invoke-static {}, Lbr/com/itau/sdk/android/core/BackendClient;->getInstance()Lbr/com/itau/sdk/android/core/BackendClient;

    move-result-object v0

    invoke-virtual {v0}, Lbr/com/itau/sdk/android/core/BackendClient;->getTokenAppByPassValidation()Lbr/com/itau/sdk/android/core/token/TokenAppByPassValidation;

    move-result-object v0

    if-eqz v0, :cond_11d

    .line 172
    :try_start_b5
    invoke-static {}, Lbr/com/itau/sdk/android/core/BackendClient;->getInstance()Lbr/com/itau/sdk/android/core/BackendClient;

    move-result-object v0

    invoke-virtual {v0}, Lbr/com/itau/sdk/android/core/BackendClient;->getTokenAppByPassValidation()Lbr/com/itau/sdk/android/core/token/TokenAppByPassValidation;

    move-result-object v0

    invoke-interface {v0}, Lbr/com/itau/sdk/android/core/token/TokenAppByPassValidation;->onFinishedByPass()V
    :try_end_c0
    .catch Ljava/lang/Exception; {:try_start_b5 .. :try_end_c0} :catch_c1

    .line 175
    goto :goto_11d

    .line 173
    :catch_c1
    move-exception v6

    .line 175
    goto :goto_11d

    .line 164
    :catch_c3
    move-exception v6

    .line 165
    :try_start_c4
    invoke-direct {p0, p1}, Lbr/com/itau/sdk/android/core/endpoint/y;->b(Lbr/com/itau/sdk/android/core/model/UiModelDTO;)Z

    move-result v0

    if-nez v0, :cond_eb

    .line 166
    new-instance v0, Lbr/com/itau/sdk/android/core/exception/TokenException;

    sget-object v1, Lbr/com/itau/sdk/android/core/exception/TokenException$Type;->UNEXPECTED:Lbr/com/itau/sdk/android/core/exception/TokenException$Type;

    sget-object v2, Lbr/com/itau/sdk/android/core/endpoint/y;->d:[S

    const/16 v3, 0xa9

    aget-short v2, v2, v3

    neg-int v2, v2

    sget-object v3, Lbr/com/itau/sdk/android/core/endpoint/y;->d:[S

    const/16 v4, 0x5f

    aget-short v3, v3, v4

    neg-int v3, v3

    sget-object v4, Lbr/com/itau/sdk/android/core/endpoint/y;->d:[S

    const/16 v5, 0xf

    aget-short v4, v4, v5

    invoke-static {v2, v3, v4}, Lbr/com/itau/sdk/android/core/endpoint/y;->a(III)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lbr/com/itau/sdk/android/core/exception/TokenException;-><init>(Lbr/com/itau/sdk/android/core/exception/TokenException$Type;Ljava/lang/String;Ljava/lang/Exception;)V

    throw v0
    :try_end_eb
    .catchall {:try_start_c4 .. :try_end_eb} :catchall_104

    .line 168
    :cond_eb
    const/4 v7, 0x0

    .line 170
    invoke-static {}, Lbr/com/itau/sdk/android/core/BackendClient;->getInstance()Lbr/com/itau/sdk/android/core/BackendClient;

    move-result-object v0

    invoke-virtual {v0}, Lbr/com/itau/sdk/android/core/BackendClient;->getTokenAppByPassValidation()Lbr/com/itau/sdk/android/core/token/TokenAppByPassValidation;

    move-result-object v0

    if-eqz v0, :cond_103

    .line 172
    :try_start_f6
    invoke-static {}, Lbr/com/itau/sdk/android/core/BackendClient;->getInstance()Lbr/com/itau/sdk/android/core/BackendClient;

    move-result-object v0

    invoke-virtual {v0}, Lbr/com/itau/sdk/android/core/BackendClient;->getTokenAppByPassValidation()Lbr/com/itau/sdk/android/core/token/TokenAppByPassValidation;

    move-result-object v0

    invoke-interface {v0}, Lbr/com/itau/sdk/android/core/token/TokenAppByPassValidation;->onFinishedByPass()V
    :try_end_101
    .catch Ljava/lang/Exception; {:try_start_f6 .. :try_end_101} :catch_102

    .line 175
    goto :goto_103

    .line 173
    :catch_102
    move-exception v8

    .line 168
    :cond_103
    :goto_103
    return v7

    .line 170
    :catchall_104
    move-exception v11

    invoke-static {}, Lbr/com/itau/sdk/android/core/BackendClient;->getInstance()Lbr/com/itau/sdk/android/core/BackendClient;

    move-result-object v0

    invoke-virtual {v0}, Lbr/com/itau/sdk/android/core/BackendClient;->getTokenAppByPassValidation()Lbr/com/itau/sdk/android/core/token/TokenAppByPassValidation;

    move-result-object v0

    if-eqz v0, :cond_11c

    .line 172
    :try_start_10f
    invoke-static {}, Lbr/com/itau/sdk/android/core/BackendClient;->getInstance()Lbr/com/itau/sdk/android/core/BackendClient;

    move-result-object v0

    invoke-virtual {v0}, Lbr/com/itau/sdk/android/core/BackendClient;->getTokenAppByPassValidation()Lbr/com/itau/sdk/android/core/token/TokenAppByPassValidation;

    move-result-object v0

    invoke-interface {v0}, Lbr/com/itau/sdk/android/core/token/TokenAppByPassValidation;->onFinishedByPass()V
    :try_end_11a
    .catch Ljava/lang/Exception; {:try_start_10f .. :try_end_11a} :catch_11b

    .line 175
    goto :goto_11c

    .line 173
    :catch_11b
    move-exception v12

    .line 175
    :cond_11c
    :goto_11c
    throw v11

    .line 180
    :cond_11d
    :goto_11d
    const/4 v0, 0x0

    return v0
.end method

.method private a(Ljava/lang/String;)Z
    .registers 5

    .line 184
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_22

    sget-object v0, Lbr/com/itau/sdk/android/core/endpoint/y;->d:[S

    const/16 v1, 0x3d

    aget-short v0, v0, v1

    add-int/lit8 v0, v0, 0x1

    sget-object v1, Lbr/com/itau/sdk/android/core/endpoint/y;->d:[S

    const/16 v2, 0x18

    aget-short v1, v1, v2

    const/16 v2, 0x38

    invoke-static {v0, v2, v1}, Lbr/com/itau/sdk/android/core/endpoint/y;->a(III)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_22

    const/4 v0, 0x1

    goto :goto_23

    :cond_22
    const/4 v0, 0x0

    :goto_23
    return v0
.end method

.method private b(Ljava/lang/String;)Ljava/lang/String;
    .registers 4

    .line 312
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 313
    return-object p1

    .line 315
    :cond_7
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    .line 317
    const/16 v0, 0xc

    if-gt v1, v0, :cond_10

    .line 318
    return-object p1

    .line 320
    :cond_10
    add-int/lit8 v0, v1, -0xc

    invoke-virtual {p1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private b(Lbr/com/itau/sdk/android/core/model/SecurityDataDTO;)Z
    .registers 3

    .line 188
    if-nez p1, :cond_4

    .line 189
    const/4 v0, 0x0

    return v0

    .line 191
    :cond_4
    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/model/SecurityDataDTO;->getUiModel()Lbr/com/itau/sdk/android/core/model/UiModelDTO;

    move-result-object v0

    invoke-direct {p0, v0}, Lbr/com/itau/sdk/android/core/endpoint/y;->b(Lbr/com/itau/sdk/android/core/model/UiModelDTO;)Z

    move-result v0

    if-nez v0, :cond_1a

    .line 192
    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/model/SecurityDataDTO;->isDispositivoNecessario()Z

    move-result v0

    if-nez v0, :cond_1c

    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/model/SecurityDataDTO;->isSenhaNecessaria()Z

    move-result v0

    if-eqz v0, :cond_1c

    :cond_1a
    const/4 v0, 0x1

    goto :goto_1d

    :cond_1c
    const/4 v0, 0x0

    .line 191
    :goto_1d
    return v0
.end method

.method private b(Lbr/com/itau/sdk/android/core/model/UiModelDTO;)Z
    .registers 3

    .line 196
    if-nez p1, :cond_4

    .line 197
    const/4 v0, 0x0

    return v0

    .line 198
    :cond_4
    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/model/UiModelDTO;->getNumeroSerieTokenSMS()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lbr/com/itau/sdk/android/core/endpoint/y;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_22

    .line 199
    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/model/UiModelDTO;->getNumeroSerieTokenAplicativo()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lbr/com/itau/sdk/android/core/endpoint/y;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_22

    .line 200
    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/model/UiModelDTO;->getNumeroSerieTokenFisico()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lbr/com/itau/sdk/android/core/endpoint/y;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_24

    :cond_22
    const/4 v0, 0x1

    goto :goto_25

    :cond_24
    const/4 v0, 0x0

    .line 198
    :goto_25
    return v0
.end method

.method private c(Lbr/com/itau/sdk/android/core/model/UiModelDTO;)Ljava/lang/String;
    .registers 8

    .line 204
    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/model/UiModelDTO;->getTokenAplicativo()Lbr/com/itau/sdk/android/core/model/TokenDTO;

    move-result-object v0

    invoke-virtual {v0}, Lbr/com/itau/sdk/android/core/model/TokenDTO;->getSerialNumber()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lbr/com/itau/sdk/android/core/endpoint/y;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 205
    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/model/UiModelDTO;->getTokenAplicativo()Lbr/com/itau/sdk/android/core/model/TokenDTO;

    move-result-object v0

    invoke-virtual {v0, v2}, Lbr/com/itau/sdk/android/core/model/TokenDTO;->setSerialNumber(Ljava/lang/String;)V

    .line 207
    invoke-direct {p0, v2}, Lbr/com/itau/sdk/android/core/endpoint/y;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_63

    .line 209
    :try_start_19
    invoke-static {}, Lbr/com/itau/sdk/android/core/BackendClient;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v2}, Lbr/com/itau/security/token/til/Token;->getOTP(Landroid/content/Context;Ljava/lang/String;)Lbr/com/itau/security/token/model/OTPModel;

    move-result-object v3

    .line 211
    if-eqz v3, :cond_28

    .line 212
    invoke-virtual {v3}, Lbr/com/itau/security/token/model/OTPModel;->token()Ljava/lang/String;
    :try_end_26
    .catch Lbr/com/itau/security/token/exception/UnavailableTokenException; {:try_start_19 .. :try_end_26} :catch_29

    move-result-object v0

    return-object v0

    .line 239
    :cond_28
    goto :goto_63

    .line 214
    :catch_29
    move-exception v3

    .line 216
    :try_start_2a
    new-instance v4, Lbr/com/itau/sdk/android/core/token/TokenWaiter;

    invoke-direct {v4}, Lbr/com/itau/sdk/android/core/token/TokenWaiter;-><init>()V

    .line 218
    invoke-static {}, Lbr/com/itau/sdk/android/core/BackendClient;->getInstance()Lbr/com/itau/sdk/android/core/BackendClient;

    move-result-object v0

    invoke-virtual {v0}, Lbr/com/itau/sdk/android/core/BackendClient;->getTokenAppByPassValidation()Lbr/com/itau/sdk/android/core/token/TokenAppByPassValidation;
    :try_end_36
    .catch Ljava/lang/InterruptedException; {:try_start_2a .. :try_end_36} :catch_5f
    .catch Ljava/lang/Exception; {:try_start_2a .. :try_end_36} :catch_62

    move-result-object v0

    if-eqz v0, :cond_4a

    .line 221
    :try_start_39
    invoke-static {}, Lbr/com/itau/sdk/android/core/BackendClient;->getInstance()Lbr/com/itau/sdk/android/core/BackendClient;

    move-result-object v0

    .line 222
    invoke-virtual {v0}, Lbr/com/itau/sdk/android/core/BackendClient;->getTokenAppByPassValidation()Lbr/com/itau/sdk/android/core/token/TokenAppByPassValidation;

    move-result-object v0

    .line 223
    invoke-virtual {v3}, Lbr/com/itau/security/token/exception/UnavailableTokenException;->getSecondsRemaining()I

    move-result v1

    invoke-interface {v0, v1, v4}, Lbr/com/itau/sdk/android/core/token/TokenAppByPassValidation;->onWaitingForToken(ILbr/com/itau/sdk/android/core/token/TokenWaiter;)V
    :try_end_48
    .catch Ljava/lang/Exception; {:try_start_39 .. :try_end_48} :catch_49
    .catch Ljava/lang/InterruptedException; {:try_start_39 .. :try_end_48} :catch_5f

    .line 226
    goto :goto_4a

    .line 224
    :catch_49
    move-exception v5

    .line 228
    :cond_4a
    :goto_4a
    :try_start_4a
    invoke-virtual {v3}, Lbr/com/itau/security/token/exception/UnavailableTokenException;->getSecondsRemaining()I

    move-result v0

    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v4, v0, v1}, Lbr/com/itau/sdk/android/core/token/TokenWaiter;->startWaiting(ILjava/util/concurrent/TimeUnit;)V

    .line 230
    invoke-virtual {v4}, Lbr/com/itau/sdk/android/core/token/TokenWaiter;->wasCanceled()Z

    move-result v0

    if-nez v0, :cond_5e

    .line 231
    invoke-direct {p0, p1}, Lbr/com/itau/sdk/android/core/endpoint/y;->c(Lbr/com/itau/sdk/android/core/model/UiModelDTO;)Ljava/lang/String;
    :try_end_5c
    .catch Ljava/lang/InterruptedException; {:try_start_4a .. :try_end_5c} :catch_5f
    .catch Ljava/lang/Exception; {:try_start_4a .. :try_end_5c} :catch_62

    move-result-object v0

    return-object v0

    .line 238
    :cond_5e
    goto :goto_63

    .line 234
    :catch_5f
    move-exception v4

    .line 235
    const/4 v0, 0x0

    return-object v0

    .line 236
    :catch_62
    move-exception v4

    .line 242
    :cond_63
    :goto_63
    const/4 v0, 0x0

    return-object v0
.end method

.method private c(Lbr/com/itau/sdk/android/core/model/SecurityDataDTO;)V
    .registers 8

    .line 284
    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/model/SecurityDataDTO;->getUiModel()Lbr/com/itau/sdk/android/core/model/UiModelDTO;

    move-result-object v0

    if-nez v0, :cond_32

    .line 285
    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/model/SecurityDataDTO;->isDispositivoNecessario()Z

    move-result v0

    if-nez v0, :cond_32

    .line 286
    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/model/SecurityDataDTO;->isSenhaNecessaria()Z

    move-result v0

    if-nez v0, :cond_32

    .line 287
    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/model/SecurityDataDTO;->isExibeMensagemTokeNaoNecessario()Z

    move-result v0

    if-nez v0, :cond_32

    .line 288
    new-instance v0, Lbr/com/itau/sdk/android/core/exception/TokenException;

    sget-object v1, Lbr/com/itau/sdk/android/core/exception/TokenException$Type;->UNEXPECTED:Lbr/com/itau/sdk/android/core/exception/TokenException$Type;

    sget-object v2, Lbr/com/itau/sdk/android/core/endpoint/y;->d:[S

    const/16 v3, 0x1a

    aget-short v2, v2, v3

    sget-object v3, Lbr/com/itau/sdk/android/core/endpoint/y;->d:[S

    const/4 v4, 0x6

    aget-short v3, v3, v4

    const/16 v4, 0x57

    invoke-static {v4, v2, v3}, Lbr/com/itau/sdk/android/core/endpoint/y;->a(III)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lbr/com/itau/sdk/android/core/exception/TokenException;-><init>(Lbr/com/itau/sdk/android/core/exception/TokenException$Type;Ljava/lang/String;Ljava/lang/Exception;)V

    throw v0

    .line 291
    :cond_32
    invoke-direct {p0, p1}, Lbr/com/itau/sdk/android/core/endpoint/y;->b(Lbr/com/itau/sdk/android/core/model/SecurityDataDTO;)Z

    move-result v0

    if-nez v0, :cond_59

    .line 292
    new-instance v0, Lbr/com/itau/sdk/android/core/exception/TokenException;

    sget-object v1, Lbr/com/itau/sdk/android/core/exception/TokenException$Type;->UNEXPECTED:Lbr/com/itau/sdk/android/core/exception/TokenException$Type;

    sget-object v2, Lbr/com/itau/sdk/android/core/endpoint/y;->d:[S

    const/16 v3, 0xa9

    aget-short v2, v2, v3

    neg-int v2, v2

    sget-object v3, Lbr/com/itau/sdk/android/core/endpoint/y;->d:[S

    const/16 v4, 0x5f

    aget-short v3, v3, v4

    neg-int v3, v3

    sget-object v4, Lbr/com/itau/sdk/android/core/endpoint/y;->d:[S

    const/16 v5, 0xf

    aget-short v4, v4, v5

    invoke-static {v2, v3, v4}, Lbr/com/itau/sdk/android/core/endpoint/y;->a(III)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lbr/com/itau/sdk/android/core/exception/TokenException;-><init>(Lbr/com/itau/sdk/android/core/exception/TokenException$Type;Ljava/lang/String;Ljava/lang/Exception;)V

    throw v0

    .line 294
    :cond_59
    return-void
.end method

.method private d(Lbr/com/itau/sdk/android/core/model/UiModelDTO;)Landroid/util/Pair;
    .registers 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Lbr/com/itau/sdk/android/core/model/UiModelDTO;)Landroid/util/Pair<Lbr/com/itau/security/token/model/OTPModel;Lbr/com/itau/security/token/model/OTPModel;>;"
        }
    .end annotation

    .line 246
    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/model/UiModelDTO;->getTokenAplicativo()Lbr/com/itau/sdk/android/core/model/TokenDTO;

    move-result-object v0

    invoke-virtual {v0}, Lbr/com/itau/sdk/android/core/model/TokenDTO;->getSerialNumber()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lbr/com/itau/sdk/android/core/endpoint/y;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 247
    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/model/UiModelDTO;->getTokenAplicativo()Lbr/com/itau/sdk/android/core/model/TokenDTO;

    move-result-object v0

    invoke-virtual {v0, v2}, Lbr/com/itau/sdk/android/core/model/TokenDTO;->setSerialNumber(Ljava/lang/String;)V

    .line 249
    invoke-direct {p0, v2}, Lbr/com/itau/sdk/android/core/endpoint/y;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5c

    .line 251
    :try_start_19
    invoke-static {}, Lbr/com/itau/sdk/android/core/BackendClient;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v2}, Lbr/com/itau/security/token/til/Token;->unlockAppToken(Landroid/content/Context;Ljava/lang/String;)Landroid/util/Pair;
    :try_end_20
    .catch Lbr/com/itau/security/token/exception/UnavailableTokenException; {:try_start_19 .. :try_end_20} :catch_22

    move-result-object v0

    return-object v0

    .line 252
    :catch_22
    move-exception v3

    .line 254
    :try_start_23
    new-instance v4, Lbr/com/itau/sdk/android/core/token/TokenWaiter;

    invoke-direct {v4}, Lbr/com/itau/sdk/android/core/token/TokenWaiter;-><init>()V

    .line 256
    invoke-static {}, Lbr/com/itau/sdk/android/core/BackendClient;->getInstance()Lbr/com/itau/sdk/android/core/BackendClient;

    move-result-object v0

    invoke-virtual {v0}, Lbr/com/itau/sdk/android/core/BackendClient;->getTokenAppByPassValidation()Lbr/com/itau/sdk/android/core/token/TokenAppByPassValidation;
    :try_end_2f
    .catch Ljava/lang/InterruptedException; {:try_start_23 .. :try_end_2f} :catch_58
    .catch Ljava/lang/Exception; {:try_start_23 .. :try_end_2f} :catch_5b

    move-result-object v0

    if-eqz v0, :cond_43

    .line 259
    :try_start_32
    invoke-static {}, Lbr/com/itau/sdk/android/core/BackendClient;->getInstance()Lbr/com/itau/sdk/android/core/BackendClient;

    move-result-object v0

    .line 260
    invoke-virtual {v0}, Lbr/com/itau/sdk/android/core/BackendClient;->getTokenAppByPassValidation()Lbr/com/itau/sdk/android/core/token/TokenAppByPassValidation;

    move-result-object v0

    .line 261
    invoke-virtual {v3}, Lbr/com/itau/security/token/exception/UnavailableTokenException;->getSecondsRemaining()I

    move-result v1

    invoke-interface {v0, v1, v4}, Lbr/com/itau/sdk/android/core/token/TokenAppByPassValidation;->onWaitingForToken(ILbr/com/itau/sdk/android/core/token/TokenWaiter;)V
    :try_end_41
    .catch Ljava/lang/Exception; {:try_start_32 .. :try_end_41} :catch_42
    .catch Ljava/lang/InterruptedException; {:try_start_32 .. :try_end_41} :catch_58

    .line 264
    goto :goto_43

    .line 262
    :catch_42
    move-exception v5

    .line 266
    :cond_43
    :goto_43
    :try_start_43
    invoke-virtual {v3}, Lbr/com/itau/security/token/exception/UnavailableTokenException;->getSecondsRemaining()I

    move-result v0

    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v4, v0, v1}, Lbr/com/itau/sdk/android/core/token/TokenWaiter;->startWaiting(ILjava/util/concurrent/TimeUnit;)V

    .line 268
    invoke-virtual {v4}, Lbr/com/itau/sdk/android/core/token/TokenWaiter;->wasCanceled()Z

    move-result v0

    if-nez v0, :cond_57

    .line 269
    invoke-direct {p0, p1}, Lbr/com/itau/sdk/android/core/endpoint/y;->d(Lbr/com/itau/sdk/android/core/model/UiModelDTO;)Landroid/util/Pair;
    :try_end_55
    .catch Ljava/lang/InterruptedException; {:try_start_43 .. :try_end_55} :catch_58
    .catch Ljava/lang/Exception; {:try_start_43 .. :try_end_55} :catch_5b

    move-result-object v0

    return-object v0

    .line 276
    :cond_57
    goto :goto_5c

    .line 272
    :catch_58
    move-exception v4

    .line 273
    const/4 v0, 0x0

    return-object v0

    .line 274
    :catch_5b
    move-exception v4

    .line 280
    :cond_5c
    :goto_5c
    const/4 v0, 0x0

    return-object v0
.end method

.method private d(Lbr/com/itau/sdk/android/core/model/SecurityDataDTO;)V
    .registers 4

    .line 297
    if-eqz p1, :cond_e

    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/model/SecurityDataDTO;->getUiModel()Lbr/com/itau/sdk/android/core/model/UiModelDTO;

    move-result-object v0

    if-eqz v0, :cond_e

    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/model/SecurityDataDTO;->isSenhaNecessaria()Z

    move-result v0

    if-eqz v0, :cond_f

    .line 298
    :cond_e
    return-void

    .line 300
    :cond_f
    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/model/SecurityDataDTO;->getUiModel()Lbr/com/itau/sdk/android/core/model/UiModelDTO;

    move-result-object v1

    .line 302
    invoke-virtual {v1}, Lbr/com/itau/sdk/android/core/model/UiModelDTO;->getTokenAplicativo()Lbr/com/itau/sdk/android/core/model/TokenDTO;

    move-result-object v0

    if-eqz v0, :cond_23

    invoke-virtual {v1}, Lbr/com/itau/sdk/android/core/model/UiModelDTO;->getNumeroSerieTokenAplicativo()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lbr/com/itau/sdk/android/core/endpoint/y;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_26

    .line 303
    :cond_23
    invoke-direct {p0}, Lbr/com/itau/sdk/android/core/endpoint/y;->a()V

    .line 304
    :cond_26
    return-void
.end method


# virtual methods
.method a(Lbr/com/itau/sdk/android/core/model/SecurityDataDTO;)Lbr/com/itau/sdk/android/core/model/TokenAppDataDTO;
    .registers 5

    .line 41
    if-nez p1, :cond_4

    .line 42
    const/4 v0, 0x0

    return-object v0

    .line 44
    :cond_4
    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/model/SecurityDataDTO;->getTokenAppData()Lbr/com/itau/sdk/android/core/model/TokenAppDataDTO;

    move-result-object v2

    .line 46
    if-eqz v2, :cond_22

    .line 47
    invoke-virtual {v2}, Lbr/com/itau/sdk/android/core/model/TokenAppDataDTO;->getCancelSerialNumber()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lbr/com/itau/sdk/android/core/endpoint/y;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lbr/com/itau/sdk/android/core/model/TokenAppDataDTO;->setCancelSerialNumber(Ljava/lang/String;)V

    .line 48
    invoke-static {}, Lbr/com/itau/sdk/android/core/BackendClient;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, Lbr/com/itau/sdk/android/core/n;->a:Lcom/google/gson/Gson;

    invoke-virtual {v1, v2}, Lcom/google/gson/Gson;->toJson(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbr/com/itau/security/token/til/Token;->handleTokenAppData(Landroid/content/Context;Ljava/lang/String;)V

    .line 51
    :cond_22
    return-object v2
.end method

.method public a(Lbr/com/itau/sdk/android/core/model/ResponseDTO;Ljava/lang/String;)V
    .registers 14

    .line 55
    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/model/ResponseDTO;->getSecurityData()Lbr/com/itau/sdk/android/core/model/SecurityDataDTO;

    move-result-object v6

    .line 57
    if-nez v6, :cond_7

    .line 58
    return-void

    .line 60
    :cond_7
    invoke-static {}, Lbr/com/itau/sdk/android/core/BackendClient;->getInstance()Lbr/com/itau/sdk/android/core/BackendClient;

    move-result-object v0

    invoke-virtual {v0}, Lbr/com/itau/sdk/android/core/BackendClient;->getShouldCallPasswordValidation()Ljava/lang/Boolean;

    move-result-object v7

    .line 61
    invoke-static {}, Lbr/com/itau/sdk/android/core/BackendClient;->getInstance()Lbr/com/itau/sdk/android/core/BackendClient;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbr/com/itau/sdk/android/core/BackendClient;->setShouldCallPasswordValidation(Ljava/lang/Boolean;)V

    .line 63
    invoke-virtual {v7}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_27

    .line 64
    invoke-direct {p0, v6}, Lbr/com/itau/sdk/android/core/endpoint/y;->d(Lbr/com/itau/sdk/android/core/model/SecurityDataDTO;)V

    .line 65
    invoke-direct {p0, v6}, Lbr/com/itau/sdk/android/core/endpoint/y;->c(Lbr/com/itau/sdk/android/core/model/SecurityDataDTO;)V

    .line 68
    :cond_27
    invoke-virtual {v6}, Lbr/com/itau/sdk/android/core/model/SecurityDataDTO;->getUiModel()Lbr/com/itau/sdk/android/core/model/UiModelDTO;

    move-result-object v0

    invoke-direct {p0, v0}, Lbr/com/itau/sdk/android/core/endpoint/y;->a(Lbr/com/itau/sdk/android/core/model/UiModelDTO;)Z

    move-result v0

    if-eqz v0, :cond_32

    .line 69
    return-void

    .line 71
    :cond_32
    invoke-virtual {v7}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_3b

    .line 72
    invoke-direct {p0, v6}, Lbr/com/itau/sdk/android/core/endpoint/y;->c(Lbr/com/itau/sdk/android/core/model/SecurityDataDTO;)V

    .line 75
    :cond_3b
    invoke-static {p0}, Lbr/com/itau/sdk/android/core/endpoint/z;->a(Lbr/com/itau/sdk/android/core/endpoint/y;)Lbr/com/itau/sdk/android/core/token/ui/BaseDialog$OnTokenAction;

    move-result-object v8

    .line 80
    invoke-virtual {v7}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_79

    invoke-virtual {v6}, Lbr/com/itau/sdk/android/core/model/SecurityDataDTO;->getUiModel()Lbr/com/itau/sdk/android/core/model/UiModelDTO;

    move-result-object v0

    if-eqz v0, :cond_79

    .line 81
    invoke-virtual {v6}, Lbr/com/itau/sdk/android/core/model/SecurityDataDTO;->getUiModel()Lbr/com/itau/sdk/android/core/model/UiModelDTO;

    move-result-object v0

    invoke-virtual {v0}, Lbr/com/itau/sdk/android/core/model/UiModelDTO;->getNumeroSerieTokenSMS()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lbr/com/itau/sdk/android/core/endpoint/y;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_79

    .line 82
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    .line 83
    invoke-virtual {v6}, Lbr/com/itau/sdk/android/core/model/SecurityDataDTO;->getUiModel()Lbr/com/itau/sdk/android/core/model/UiModelDTO;

    move-result-object v0

    sget-object v1, Lbr/com/itau/sdk/android/core/endpoint/y;->d:[S

    const/16 v2, 0x3d

    aget-short v1, v1, v2

    add-int/lit8 v1, v1, 0x1

    sget-object v2, Lbr/com/itau/sdk/android/core/endpoint/y;->d:[S

    const/16 v3, 0x18

    aget-short v2, v2, v3

    const/16 v3, 0x38

    invoke-static {v1, v3, v2}, Lbr/com/itau/sdk/android/core/endpoint/y;->a(III)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbr/com/itau/sdk/android/core/model/UiModelDTO;->setNumeroSerieTokenFisico(Ljava/lang/String;)V

    .line 86
    :cond_79
    invoke-virtual {v6}, Lbr/com/itau/sdk/android/core/model/SecurityDataDTO;->isDispositivoNecessario()Z

    move-result v0

    if-nez v0, :cond_85

    invoke-virtual {v6}, Lbr/com/itau/sdk/android/core/model/SecurityDataDTO;->isSenhaNecessaria()Z

    move-result v0

    if-nez v0, :cond_8b

    .line 87
    :cond_85
    invoke-virtual {v7}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_ad

    .line 88
    :cond_8b
    invoke-static {}, Lbr/com/itau/sdk/android/core/BackendClient;->getInstance()Lbr/com/itau/sdk/android/core/BackendClient;

    move-result-object v0

    invoke-virtual {v0}, Lbr/com/itau/sdk/android/core/BackendClient;->getSdkCustomUi()Lbr/com/itau/sdk/android/core/SDKCustomUi;

    move-result-object v0

    iget-object v1, p0, Lbr/com/itau/sdk/android/core/endpoint/y;->b:Lbr/com/itau/sdk/android/core/endpoint/TokenErrorHandler;

    .line 89
    invoke-virtual {v6}, Lbr/com/itau/sdk/android/core/model/SecurityDataDTO;->getUiModel()Lbr/com/itau/sdk/android/core/model/UiModelDTO;

    move-result-object v2

    if-eqz v2, :cond_a8

    .line 90
    invoke-virtual {v6}, Lbr/com/itau/sdk/android/core/model/SecurityDataDTO;->getUiModel()Lbr/com/itau/sdk/android/core/model/UiModelDTO;

    move-result-object v2

    invoke-virtual {v2}, Lbr/com/itau/sdk/android/core/model/UiModelDTO;->getQtdDigitosSenha()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    goto :goto_a9

    :cond_a8
    const/4 v2, 0x0

    .line 88
    :goto_a9
    invoke-interface {v0, v1, v8, v2}, Lbr/com/itau/sdk/android/core/SDKCustomUi;->initPasswordFlow(Lbr/com/itau/sdk/android/core/endpoint/TokenErrorHandler;Lbr/com/itau/sdk/android/core/token/ui/BaseDialog$OnTokenAction;Ljava/lang/Integer;)V

    goto :goto_ca

    .line 92
    :cond_ad
    invoke-static {}, Lbr/com/itau/sdk/android/core/BackendClient;->getInstance()Lbr/com/itau/sdk/android/core/BackendClient;

    move-result-object v0

    invoke-virtual {v0}, Lbr/com/itau/sdk/android/core/BackendClient;->getSdkCustomUi()Lbr/com/itau/sdk/android/core/SDKCustomUi;

    move-result-object v0

    iget-object v1, p0, Lbr/com/itau/sdk/android/core/endpoint/y;->b:Lbr/com/itau/sdk/android/core/endpoint/TokenErrorHandler;

    .line 93
    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/model/ResponseDTO;->getSecurityData()Lbr/com/itau/sdk/android/core/model/SecurityDataDTO;

    move-result-object v2

    if-eqz v2, :cond_c6

    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/model/ResponseDTO;->getSecurityData()Lbr/com/itau/sdk/android/core/model/SecurityDataDTO;

    move-result-object v2

    invoke-virtual {v2}, Lbr/com/itau/sdk/android/core/model/SecurityDataDTO;->getUiModel()Lbr/com/itau/sdk/android/core/model/UiModelDTO;

    move-result-object v2

    goto :goto_c7

    :cond_c6
    const/4 v2, 0x0

    .line 92
    :goto_c7
    invoke-interface {v0, v1, v8, v2, p2}, Lbr/com/itau/sdk/android/core/SDKCustomUi;->initTokenFlow(Lbr/com/itau/sdk/android/core/endpoint/TokenErrorHandler;Lbr/com/itau/sdk/android/core/token/ui/BaseDialog$OnTokenAction;Lbr/com/itau/sdk/android/core/model/UiModelDTO;Ljava/lang/String;)V

    .line 97
    :goto_ca
    :try_start_ca
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/endpoint/y;->a:Ljava/util/concurrent/CountDownLatch;

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x5

    invoke-virtual {v0, v2, v3, v1}, Ljava/util/concurrent/CountDownLatch;->await(JLjava/util/concurrent/TimeUnit;)Z

    .line 99
    iget v0, p0, Lbr/com/itau/sdk/android/core/endpoint/y;->c:I

    packed-switch v0, :pswitch_data_162

    goto :goto_10c

    .line 102
    :pswitch_d9
    goto :goto_12b

    .line 104
    :pswitch_da
    new-instance v0, Lbr/com/itau/sdk/android/core/exception/TokenException;

    sget-object v1, Lbr/com/itau/sdk/android/core/exception/TokenException$Type;->INVALID:Lbr/com/itau/sdk/android/core/exception/TokenException$Type;

    sget-object v2, Lbr/com/itau/sdk/android/core/endpoint/y;->d:[S

    const/16 v3, 0x1a

    aget-short v2, v2, v3

    or-int/lit8 v3, v2, 0x2d

    sget-object v4, Lbr/com/itau/sdk/android/core/endpoint/y;->d:[S

    const/16 v5, 0x15

    aget-short v4, v4, v5

    invoke-static {v2, v3, v4}, Lbr/com/itau/sdk/android/core/endpoint/y;->a(III)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lbr/com/itau/sdk/android/core/exception/TokenException;-><init>(Lbr/com/itau/sdk/android/core/exception/TokenException$Type;Ljava/lang/String;Ljava/lang/Exception;)V

    throw v0

    .line 106
    :pswitch_f5
    new-instance v0, Lbr/com/itau/sdk/android/core/exception/TokenException;

    sget-object v1, Lbr/com/itau/sdk/android/core/exception/TokenException$Type;->CANCELED:Lbr/com/itau/sdk/android/core/exception/TokenException$Type;

    sget-object v2, Lbr/com/itau/sdk/android/core/endpoint/y;->d:[S

    const/16 v3, 0x15

    aget-short v2, v2, v3

    const/16 v3, 0xb6

    const/16 v4, 0x2c

    invoke-static {v3, v4, v2}, Lbr/com/itau/sdk/android/core/endpoint/y;->a(III)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lbr/com/itau/sdk/android/core/exception/TokenException;-><init>(Lbr/com/itau/sdk/android/core/exception/TokenException$Type;Ljava/lang/String;Ljava/lang/Exception;)V

    throw v0

    .line 108
    :goto_10c
    new-instance v0, Lbr/com/itau/sdk/android/core/exception/TokenException;

    sget-object v1, Lbr/com/itau/sdk/android/core/exception/TokenException$Type;->UNEXPECTED:Lbr/com/itau/sdk/android/core/exception/TokenException$Type;

    sget-object v2, Lbr/com/itau/sdk/android/core/endpoint/y;->d:[S

    const/16 v3, 0x58

    aget-short v2, v2, v3

    sget-object v3, Lbr/com/itau/sdk/android/core/endpoint/y;->d:[S

    const/4 v4, 0x3

    aget-short v3, v3, v4

    neg-int v3, v3

    sget-object v4, Lbr/com/itau/sdk/android/core/endpoint/y;->d:[S

    const/16 v5, 0x56

    aget-short v4, v4, v5

    invoke-static {v2, v3, v4}, Lbr/com/itau/sdk/android/core/endpoint/y;->a(III)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lbr/com/itau/sdk/android/core/exception/TokenException;-><init>(Lbr/com/itau/sdk/android/core/exception/TokenException$Type;Ljava/lang/String;Ljava/lang/Exception;)V

    throw v0
    :try_end_12b
    .catch Ljava/lang/InterruptedException; {:try_start_ca .. :try_end_12b} :catch_137
    .catchall {:try_start_ca .. :try_end_12b} :catchall_154

    .line 114
    :goto_12b
    invoke-static {}, Lbr/com/itau/sdk/android/core/BackendClient;->getInstance()Lbr/com/itau/sdk/android/core/BackendClient;

    move-result-object v0

    invoke-virtual {v0}, Lbr/com/itau/sdk/android/core/BackendClient;->getSdkCustomUi()Lbr/com/itau/sdk/android/core/SDKCustomUi;

    move-result-object v0

    invoke-interface {v0}, Lbr/com/itau/sdk/android/core/SDKCustomUi;->dimisssDialog()V

    .line 115
    goto :goto_161

    .line 111
    :catch_137
    move-exception v9

    .line 112
    :try_start_138
    new-instance v0, Lbr/com/itau/sdk/android/core/exception/TokenException;

    sget-object v1, Lbr/com/itau/sdk/android/core/exception/TokenException$Type;->INTERRUPTED:Lbr/com/itau/sdk/android/core/exception/TokenException$Type;

    sget-object v2, Lbr/com/itau/sdk/android/core/endpoint/y;->d:[S

    const/16 v3, 0x37

    aget-short v2, v2, v3

    add-int/lit8 v2, v2, -0x1

    sget-object v3, Lbr/com/itau/sdk/android/core/endpoint/y;->d:[S

    const/16 v4, 0x1a

    aget-short v3, v3, v4

    const/16 v4, 0x23

    invoke-static {v2, v4, v3}, Lbr/com/itau/sdk/android/core/endpoint/y;->a(III)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2, v9}, Lbr/com/itau/sdk/android/core/exception/TokenException;-><init>(Lbr/com/itau/sdk/android/core/exception/TokenException$Type;Ljava/lang/String;Ljava/lang/Exception;)V

    throw v0
    :try_end_154
    .catchall {:try_start_138 .. :try_end_154} :catchall_154

    .line 114
    :catchall_154
    move-exception v10

    invoke-static {}, Lbr/com/itau/sdk/android/core/BackendClient;->getInstance()Lbr/com/itau/sdk/android/core/BackendClient;

    move-result-object v0

    invoke-virtual {v0}, Lbr/com/itau/sdk/android/core/BackendClient;->getSdkCustomUi()Lbr/com/itau/sdk/android/core/SDKCustomUi;

    move-result-object v0

    invoke-interface {v0}, Lbr/com/itau/sdk/android/core/SDKCustomUi;->dimisssDialog()V

    throw v10

    .line 116
    :goto_161
    return-void

    :pswitch_data_162
    .packed-switch 0x0
        :pswitch_d9
        :pswitch_da
        :pswitch_f5
    .end packed-switch
.end method

.class public Lcom/itau/empresas/Evento$EventoListaMapas;
.super Ljava/lang/Object;
.source "Evento.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/itau/empresas/Evento;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "EventoListaMapas"
.end annotation


# static fields
.field private static agencias:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Lcom/itau/empresas/api/model/AgenciaVO;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 88
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getAgencias()Ljava/util/List;
    .registers 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Lcom/itau/empresas/api/model/AgenciaVO;>;"
        }
    .end annotation

    .line 97
    sget-object v0, Lcom/itau/empresas/Evento$EventoListaMapas;->agencias:Ljava/util/List;

    return-object v0
.end method

.method public static setAgencias(Ljava/util/List;)V
    .registers 1
    .param p0, "lista"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<Lcom/itau/empresas/api/model/AgenciaVO;>;)V"
        }
    .end annotation

    .line 93
    sput-object p0, Lcom/itau/empresas/Evento$EventoListaMapas;->agencias:Ljava/util/List;

    .line 94
    return-void
.end method

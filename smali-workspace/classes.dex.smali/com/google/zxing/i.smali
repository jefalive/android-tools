.class public final Lcom/google/zxing/i;
.super Lcom/google/zxing/g;


# instance fields
.field private final a:[B

.field private final b:I

.field private final c:I

.field private final d:I

.field private final e:I


# direct methods
.method public constructor <init>([BIIIIIIZ)V
    .registers 11

    invoke-direct {p0, p6, p7}, Lcom/google/zxing/g;-><init>(II)V

    add-int v0, p4, p6

    if-gt v0, p2, :cond_b

    add-int v0, p5, p7

    if-le v0, p3, :cond_13

    :cond_b
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Crop rectangle does not fit within image data."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_13
    iput-object p1, p0, Lcom/google/zxing/i;->a:[B

    iput p2, p0, Lcom/google/zxing/i;->b:I

    iput p3, p0, Lcom/google/zxing/i;->c:I

    iput p4, p0, Lcom/google/zxing/i;->d:I

    iput p5, p0, Lcom/google/zxing/i;->e:I

    if-eqz p8, :cond_22

    invoke-direct {p0, p6, p7}, Lcom/google/zxing/i;->a(II)V

    :cond_22
    return-void
.end method

.method private a(II)V
    .registers 12

    iget-object v2, p0, Lcom/google/zxing/i;->a:[B

    const/4 v3, 0x0

    iget v0, p0, Lcom/google/zxing/i;->e:I

    iget v1, p0, Lcom/google/zxing/i;->b:I

    mul-int/2addr v0, v1

    iget v1, p0, Lcom/google/zxing/i;->d:I

    add-int v4, v0, v1

    :goto_c
    if-ge v3, p2, :cond_2c

    div-int/lit8 v0, p1, 0x2

    add-int v5, v4, v0

    move v6, v4

    add-int v0, v4, p1

    add-int/lit8 v7, v0, -0x1

    :goto_17
    if-ge v6, v5, :cond_26

    aget-byte v8, v2, v6

    aget-byte v0, v2, v7

    aput-byte v0, v2, v6

    aput-byte v8, v2, v7

    add-int/lit8 v6, v6, 0x1

    add-int/lit8 v7, v7, -0x1

    goto :goto_17

    :cond_26
    add-int/lit8 v3, v3, 0x1

    iget v0, p0, Lcom/google/zxing/i;->b:I

    add-int/2addr v4, v0

    goto :goto_c

    :cond_2c
    return-void
.end method


# virtual methods
.method public a()[B
    .registers 11

    invoke-virtual {p0}, Lcom/google/zxing/i;->b()I

    move-result v2

    invoke-virtual {p0}, Lcom/google/zxing/i;->c()I

    move-result v3

    iget v0, p0, Lcom/google/zxing/i;->b:I

    if-ne v2, v0, :cond_13

    iget v0, p0, Lcom/google/zxing/i;->c:I

    if-ne v3, v0, :cond_13

    iget-object v0, p0, Lcom/google/zxing/i;->a:[B

    return-object v0

    :cond_13
    mul-int v4, v2, v3

    new-array v5, v4, [B

    iget v0, p0, Lcom/google/zxing/i;->e:I

    iget v1, p0, Lcom/google/zxing/i;->b:I

    mul-int/2addr v0, v1

    iget v1, p0, Lcom/google/zxing/i;->d:I

    add-int v6, v0, v1

    iget v0, p0, Lcom/google/zxing/i;->b:I

    if-ne v2, v0, :cond_2b

    iget-object v0, p0, Lcom/google/zxing/i;->a:[B

    const/4 v1, 0x0

    invoke-static {v0, v6, v5, v1, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v5

    :cond_2b
    iget-object v7, p0, Lcom/google/zxing/i;->a:[B

    const/4 v8, 0x0

    :goto_2e
    if-ge v8, v3, :cond_3b

    mul-int v9, v8, v2

    invoke-static {v7, v6, v5, v9, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget v0, p0, Lcom/google/zxing/i;->b:I

    add-int/2addr v6, v0

    add-int/lit8 v8, v8, 0x1

    goto :goto_2e

    :cond_3b
    return-object v5
.end method

.method public a(I[B)[B
    .registers 8

    if-ltz p1, :cond_8

    invoke-virtual {p0}, Lcom/google/zxing/i;->c()I

    move-result v0

    if-lt p1, v0, :cond_21

    :cond_8
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Requested row is outside the image: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_21
    invoke-virtual {p0}, Lcom/google/zxing/i;->b()I

    move-result v3

    if-eqz p2, :cond_2a

    array-length v0, p2

    if-ge v0, v3, :cond_2c

    :cond_2a
    new-array p2, v3, [B

    :cond_2c
    iget v0, p0, Lcom/google/zxing/i;->e:I

    add-int/2addr v0, p1

    iget v1, p0, Lcom/google/zxing/i;->b:I

    mul-int/2addr v0, v1

    iget v1, p0, Lcom/google/zxing/i;->d:I

    add-int v4, v0, v1

    iget-object v0, p0, Lcom/google/zxing/i;->a:[B

    const/4 v1, 0x0

    invoke-static {v0, v4, p2, v1, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object p2
.end method

.method public f()[I
    .registers 13

    invoke-virtual {p0}, Lcom/google/zxing/i;->b()I

    move-result v0

    div-int/lit8 v3, v0, 0x2

    invoke-virtual {p0}, Lcom/google/zxing/i;->c()I

    move-result v0

    div-int/lit8 v4, v0, 0x2

    mul-int v0, v3, v4

    new-array v5, v0, [I

    iget-object v6, p0, Lcom/google/zxing/i;->a:[B

    iget v0, p0, Lcom/google/zxing/i;->e:I

    iget v1, p0, Lcom/google/zxing/i;->b:I

    mul-int/2addr v0, v1

    iget v1, p0, Lcom/google/zxing/i;->d:I

    add-int v7, v0, v1

    const/4 v8, 0x0

    :goto_1c
    if-ge v8, v4, :cond_40

    mul-int v9, v8, v3

    const/4 v10, 0x0

    :goto_21
    if-ge v10, v3, :cond_38

    mul-int/lit8 v0, v10, 0x2

    add-int/2addr v0, v7

    aget-byte v0, v6, v0

    and-int/lit16 v11, v0, 0xff

    add-int v0, v9, v10

    const v1, 0x10101

    mul-int/2addr v1, v11

    const/high16 v2, -0x1000000

    or-int/2addr v1, v2

    aput v1, v5, v0

    add-int/lit8 v10, v10, 0x1

    goto :goto_21

    :cond_38
    iget v0, p0, Lcom/google/zxing/i;->b:I

    mul-int/lit8 v0, v0, 0x2

    add-int/2addr v7, v0

    add-int/lit8 v8, v8, 0x1

    goto :goto_1c

    :cond_40
    return-object v5
.end method

.method public g()I
    .registers 3

    invoke-virtual {p0}, Lcom/google/zxing/i;->b()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    return v0
.end method

.method public h()I
    .registers 3

    invoke-virtual {p0}, Lcom/google/zxing/i;->c()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    return v0
.end method

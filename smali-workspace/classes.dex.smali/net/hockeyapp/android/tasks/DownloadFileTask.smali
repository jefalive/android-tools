.class public Lnet/hockeyapp/android/tasks/DownloadFileTask;
.super Landroid/os/AsyncTask;
.source "DownloadFileTask.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<Ljava/lang/Void;Ljava/lang/Integer;Ljava/lang/Long;>;"
    }
.end annotation


# instance fields
.field protected context:Landroid/content/Context;

.field private downloadErrorMessage:Ljava/lang/String;

.field protected filePath:Ljava/lang/String;

.field protected filename:Ljava/lang/String;

.field protected notifier:Lnet/hockeyapp/android/listeners/DownloadFileListener;

.field protected progressDialog:Landroid/app/ProgressDialog;

.field protected urlString:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Lnet/hockeyapp/android/listeners/DownloadFileListener;)V
    .registers 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "urlString"    # Ljava/lang/String;
    .param p3, "notifier"    # Lnet/hockeyapp/android/listeners/DownloadFileListener;

    .line 68
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 69
    iput-object p1, p0, Lnet/hockeyapp/android/tasks/DownloadFileTask;->context:Landroid/content/Context;

    .line 70
    iput-object p2, p0, Lnet/hockeyapp/android/tasks/DownloadFileTask;->urlString:Ljava/lang/String;

    .line 71
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".apk"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnet/hockeyapp/android/tasks/DownloadFileTask;->filename:Ljava/lang/String;

    .line 72
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/Download"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnet/hockeyapp/android/tasks/DownloadFileTask;->filePath:Ljava/lang/String;

    .line 73
    iput-object p3, p0, Lnet/hockeyapp/android/tasks/DownloadFileTask;->notifier:Lnet/hockeyapp/android/listeners/DownloadFileListener;

    .line 74
    const/4 v0, 0x0

    iput-object v0, p0, Lnet/hockeyapp/android/tasks/DownloadFileTask;->downloadErrorMessage:Ljava/lang/String;

    .line 75
    return-void
.end method


# virtual methods
.method public attach(Landroid/content/Context;)V
    .registers 2
    .param p1, "context"    # Landroid/content/Context;

    .line 78
    iput-object p1, p0, Lnet/hockeyapp/android/tasks/DownloadFileTask;->context:Landroid/content/Context;

    .line 79
    return-void
.end method

.method protected createConnection(Ljava/net/URL;I)Ljava/net/URLConnection;
    .registers 8
    .param p1, "url"    # Ljava/net/URL;
    .param p2, "remainingRedirects"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 152
    invoke-virtual {p1}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Ljava/net/HttpURLConnection;

    .line 153
    .local v2, "connection":Ljava/net/HttpURLConnection;
    invoke-virtual {p0, v2}, Lnet/hockeyapp/android/tasks/DownloadFileTask;->setConnectionProperties(Ljava/net/HttpURLConnection;)V

    .line 155
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v3

    .line 156
    .local v3, "code":I
    const/16 v0, 0x12d

    if-eq v3, v0, :cond_1a

    const/16 v0, 0x12e

    if-eq v3, v0, :cond_1a

    const/16 v0, 0x12f

    if-ne v3, v0, :cond_40

    .line 160
    :cond_1a
    if-nez p2, :cond_1d

    .line 162
    return-object v2

    .line 165
    :cond_1d
    new-instance v4, Ljava/net/URL;

    const-string v0, "Location"

    invoke-virtual {v2, v0}, Ljava/net/HttpURLConnection;->getHeaderField(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v0}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 166
    .local v4, "movedUrl":Ljava/net/URL;
    invoke-virtual {p1}, Ljava/net/URL;->getProtocol()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4}, Ljava/net/URL;->getProtocol()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_40

    .line 169
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 170
    add-int/lit8 p2, p2, -0x1

    invoke-virtual {p0, v4, p2}, Lnet/hockeyapp/android/tasks/DownloadFileTask;->createConnection(Ljava/net/URL;I)Ljava/net/URLConnection;

    move-result-object v0

    return-object v0

    .line 173
    .end local v4    # "movedUrl":Ljava/net/URL;
    :cond_40
    return-object v2
.end method

.method public detach()V
    .registers 2

    .line 82
    const/4 v0, 0x0

    iput-object v0, p0, Lnet/hockeyapp/android/tasks/DownloadFileTask;->context:Landroid/content/Context;

    .line 83
    const/4 v0, 0x0

    iput-object v0, p0, Lnet/hockeyapp/android/tasks/DownloadFileTask;->progressDialog:Landroid/app/ProgressDialog;

    .line 84
    return-void
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Long;
    .registers 18
    .param p1, "args"    # [Ljava/lang/Void;

    .line 89
    :try_start_0
    new-instance v3, Ljava/net/URL;

    invoke-virtual/range {p0 .. p0}, Lnet/hockeyapp/android/tasks/DownloadFileTask;->getURLString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 90
    .local v3, "url":Ljava/net/URL;
    move-object/from16 v0, p0

    const/4 v1, 0x6

    invoke-virtual {v0, v3, v1}, Lnet/hockeyapp/android/tasks/DownloadFileTask;->createConnection(Ljava/net/URL;I)Ljava/net/URLConnection;

    move-result-object v4

    .line 91
    .local v4, "connection":Ljava/net/URLConnection;
    invoke-virtual {v4}, Ljava/net/URLConnection;->connect()V

    .line 93
    invoke-virtual {v4}, Ljava/net/URLConnection;->getContentLength()I

    move-result v5

    .line 94
    .local v5, "lengthOfFile":I
    invoke-virtual {v4}, Ljava/net/URLConnection;->getContentType()Ljava/lang/String;

    move-result-object v6

    .line 96
    .local v6, "contentType":Ljava/lang/String;
    if-eqz v6, :cond_32

    const-string v0, "text"

    invoke-virtual {v6, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_32

    .line 98
    const-string v0, "The requested download does not appear to be a file."

    move-object/from16 v1, p0

    iput-object v0, v1, Lnet/hockeyapp/android/tasks/DownloadFileTask;->downloadErrorMessage:Ljava/lang/String;

    .line 99
    const-wide/16 v0, 0x0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;
    :try_end_30
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_30} :catch_b7

    move-result-object v0

    return-object v0

    .line 102
    :cond_32
    :try_start_32
    new-instance v7, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v0, v0, Lnet/hockeyapp/android/tasks/DownloadFileTask;->filePath:Ljava/lang/String;

    invoke-direct {v7, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 103
    .local v7, "dir":Ljava/io/File;
    invoke-virtual {v7}, Ljava/io/File;->mkdirs()Z

    move-result v8

    .line 104
    .local v8, "result":Z
    if-nez v8, :cond_64

    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_64

    .line 105
    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Could not create the dir(s):"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v7}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 107
    :cond_64
    new-instance v9, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v0, v0, Lnet/hockeyapp/android/tasks/DownloadFileTask;->filename:Ljava/lang/String;

    invoke-direct {v9, v7, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 109
    .local v9, "file":Ljava/io/File;
    new-instance v10, Ljava/io/BufferedInputStream;

    invoke-virtual {v4}, Ljava/net/URLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v0

    invoke-direct {v10, v0}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    .line 110
    .local v10, "input":Ljava/io/InputStream;
    new-instance v11, Ljava/io/FileOutputStream;

    invoke-direct {v11, v9}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 112
    .local v11, "output":Ljava/io/OutputStream;
    const/16 v0, 0x400

    new-array v12, v0, [B

    .line 114
    .local v12, "data":[B
    const-wide/16 v14, 0x0

    .line 115
    .local v14, "total":J
    :goto_81
    invoke-virtual {v10, v12}, Ljava/io/InputStream;->read([B)I

    move-result v0

    move v13, v0

    .local v13, "count":I
    const/4 v1, -0x1

    if-eq v0, v1, :cond_a9

    .line 116
    int-to-long v0, v13

    add-long/2addr v14, v0

    .line 117
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Integer;

    long-to-float v1, v14

    const/high16 v2, 0x42c80000    # 100.0f

    mul-float/2addr v1, v2

    int-to-float v2, v5

    div-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    move-object/from16 v1, p0

    invoke-virtual {v1, v0}, Lnet/hockeyapp/android/tasks/DownloadFileTask;->publishProgress([Ljava/lang/Object;)V

    .line 118
    const/4 v0, 0x0

    invoke-virtual {v11, v12, v0, v13}, Ljava/io/OutputStream;->write([BII)V

    goto :goto_81

    .line 121
    :cond_a9
    invoke-virtual {v11}, Ljava/io/OutputStream;->flush()V

    .line 122
    invoke-virtual {v11}, Ljava/io/OutputStream;->close()V

    .line 123
    invoke-virtual {v10}, Ljava/io/InputStream;->close()V

    .line 125
    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;
    :try_end_b5
    .catch Ljava/lang/Exception; {:try_start_32 .. :try_end_b5} :catch_b7

    move-result-object v0

    return-object v0

    .line 127
    .end local v3    # "url":Ljava/net/URL;
    .end local v4    # "connection":Ljava/net/URLConnection;
    .end local v5    # "lengthOfFile":I
    .end local v6    # "contentType":Ljava/lang/String;
    .end local v7    # "dir":Ljava/io/File;
    .end local v8    # "result":Z
    .end local v9    # "file":Ljava/io/File;
    .end local v10    # "input":Ljava/io/InputStream;
    .end local v11    # "output":Ljava/io/OutputStream;
    .end local v12    # "data":[B
    .end local v13    # "count":I
    .end local v14    # "total":J
    :catch_b7
    move-exception v3

    .line 128
    .local v3, "e":Ljava/lang/Exception;
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V

    .line 129
    const-wide/16 v0, 0x0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3

    .line 57
    move-object v0, p1

    check-cast v0, [Ljava/lang/Void;

    invoke-virtual {p0, v0}, Lnet/hockeyapp/android/tasks/DownloadFileTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method protected getURLString()Ljava/lang/String;
    .registers 3

    .line 247
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lnet/hockeyapp/android/tasks/DownloadFileTask;->urlString:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "&type=apk"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Ljava/lang/Long;)V
    .registers 8
    .param p1, "result"    # Ljava/lang/Long;

    .line 195
    iget-object v0, p0, Lnet/hockeyapp/android/tasks/DownloadFileTask;->progressDialog:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_b

    .line 197
    :try_start_4
    iget-object v0, p0, Lnet/hockeyapp/android/tasks/DownloadFileTask;->progressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_9} :catch_a

    .line 201
    goto :goto_b

    .line 199
    :catch_a
    move-exception v4

    .line 204
    :cond_b
    :goto_b
    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_3e

    .line 205
    iget-object v0, p0, Lnet/hockeyapp/android/tasks/DownloadFileTask;->notifier:Lnet/hockeyapp/android/listeners/DownloadFileListener;

    invoke-virtual {v0, p0}, Lnet/hockeyapp/android/listeners/DownloadFileListener;->downloadSuccessful(Lnet/hockeyapp/android/tasks/DownloadFileTask;)V

    .line 207
    new-instance v4, Landroid/content/Intent;

    const-string v0, "android.intent.action.VIEW"

    invoke-direct {v4, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 208
    .local v4, "intent":Landroid/content/Intent;
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lnet/hockeyapp/android/tasks/DownloadFileTask;->filePath:Ljava/lang/String;

    iget-object v2, p0, Lnet/hockeyapp/android/tasks/DownloadFileTask;->filename:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    const-string v1, "application/vnd.android.package-archive"

    invoke-virtual {v4, v0, v1}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    .line 209
    const/high16 v0, 0x10000000

    invoke-virtual {v4, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 210
    iget-object v0, p0, Lnet/hockeyapp/android/tasks/DownloadFileTask;->context:Landroid/content/Context;

    invoke-virtual {v0, v4}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 211
    .end local v4    # "intent":Landroid/content/Intent;
    goto :goto_8b

    .line 214
    :cond_3e
    :try_start_3e
    new-instance v4, Landroid/app/AlertDialog$Builder;

    iget-object v0, p0, Lnet/hockeyapp/android/tasks/DownloadFileTask;->context:Landroid/content/Context;

    invoke-direct {v4, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 215
    .local v4, "builder":Landroid/app/AlertDialog$Builder;
    iget-object v0, p0, Lnet/hockeyapp/android/tasks/DownloadFileTask;->notifier:Lnet/hockeyapp/android/listeners/DownloadFileListener;

    const/16 v1, 0x100

    invoke-static {v0, v1}, Lnet/hockeyapp/android/Strings;->get(Lnet/hockeyapp/android/StringListener;I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 218
    iget-object v0, p0, Lnet/hockeyapp/android/tasks/DownloadFileTask;->downloadErrorMessage:Ljava/lang/String;

    if-nez v0, :cond_5d

    .line 219
    iget-object v0, p0, Lnet/hockeyapp/android/tasks/DownloadFileTask;->notifier:Lnet/hockeyapp/android/listeners/DownloadFileListener;

    const/16 v1, 0x101

    invoke-static {v0, v1}, Lnet/hockeyapp/android/Strings;->get(Lnet/hockeyapp/android/StringListener;I)Ljava/lang/String;

    move-result-object v5

    .local v5, "message":Ljava/lang/String;
    goto :goto_5f

    .line 222
    .end local v5    # "message":Ljava/lang/String;
    :cond_5d
    iget-object v5, p0, Lnet/hockeyapp/android/tasks/DownloadFileTask;->downloadErrorMessage:Ljava/lang/String;

    .line 224
    .local v5, "message":Ljava/lang/String;
    :goto_5f
    invoke-virtual {v4, v5}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 226
    iget-object v0, p0, Lnet/hockeyapp/android/tasks/DownloadFileTask;->notifier:Lnet/hockeyapp/android/listeners/DownloadFileListener;

    const/16 v1, 0x102

    invoke-static {v0, v1}, Lnet/hockeyapp/android/Strings;->get(Lnet/hockeyapp/android/StringListener;I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lnet/hockeyapp/android/tasks/DownloadFileTask$1;

    invoke-direct {v1, p0}, Lnet/hockeyapp/android/tasks/DownloadFileTask$1;-><init>(Lnet/hockeyapp/android/tasks/DownloadFileTask;)V

    invoke-virtual {v4, v0, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 232
    iget-object v0, p0, Lnet/hockeyapp/android/tasks/DownloadFileTask;->notifier:Lnet/hockeyapp/android/listeners/DownloadFileListener;

    const/16 v1, 0x103

    invoke-static {v0, v1}, Lnet/hockeyapp/android/Strings;->get(Lnet/hockeyapp/android/StringListener;I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lnet/hockeyapp/android/tasks/DownloadFileTask$2;

    invoke-direct {v1, p0}, Lnet/hockeyapp/android/tasks/DownloadFileTask$2;-><init>(Lnet/hockeyapp/android/tasks/DownloadFileTask;)V

    invoke-virtual {v4, v0, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 238
    invoke-virtual {v4}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V
    :try_end_89
    .catch Ljava/lang/Exception; {:try_start_3e .. :try_end_89} :catch_8a

    .line 242
    .end local v4    # "builder":Landroid/app/AlertDialog$Builder;
    .end local v5    # "message":Ljava/lang/String;
    goto :goto_8b

    .line 240
    :catch_8a
    move-exception v4

    .line 244
    :goto_8b
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .registers 3

    .line 57
    move-object v0, p1

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {p0, v0}, Lnet/hockeyapp/android/tasks/DownloadFileTask;->onPostExecute(Ljava/lang/Long;)V

    return-void
.end method

.method protected varargs onProgressUpdate([Ljava/lang/Integer;)V
    .registers 5
    .param p1, "args"    # [Ljava/lang/Integer;

    .line 179
    :try_start_0
    iget-object v0, p0, Lnet/hockeyapp/android/tasks/DownloadFileTask;->progressDialog:Landroid/app/ProgressDialog;

    if-nez v0, :cond_25

    .line 180
    new-instance v0, Landroid/app/ProgressDialog;

    iget-object v1, p0, Lnet/hockeyapp/android/tasks/DownloadFileTask;->context:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lnet/hockeyapp/android/tasks/DownloadFileTask;->progressDialog:Landroid/app/ProgressDialog;

    .line 181
    iget-object v0, p0, Lnet/hockeyapp/android/tasks/DownloadFileTask;->progressDialog:Landroid/app/ProgressDialog;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    .line 182
    iget-object v0, p0, Lnet/hockeyapp/android/tasks/DownloadFileTask;->progressDialog:Landroid/app/ProgressDialog;

    const-string v1, "Loading..."

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 183
    iget-object v0, p0, Lnet/hockeyapp/android/tasks/DownloadFileTask;->progressDialog:Landroid/app/ProgressDialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 184
    iget-object v0, p0, Lnet/hockeyapp/android/tasks/DownloadFileTask;->progressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 186
    :cond_25
    iget-object v0, p0, Lnet/hockeyapp/android/tasks/DownloadFileTask;->progressDialog:Landroid/app/ProgressDialog;

    const/4 v1, 0x0

    aget-object v1, p1, v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setProgress(I)V
    :try_end_31
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_31} :catch_32

    .line 190
    goto :goto_33

    .line 188
    :catch_32
    move-exception v2

    .line 191
    :goto_33
    return-void
.end method

.method protected bridge synthetic onProgressUpdate([Ljava/lang/Object;)V
    .registers 3

    .line 57
    move-object v0, p1

    check-cast v0, [Ljava/lang/Integer;

    invoke-virtual {p0, v0}, Lnet/hockeyapp/android/tasks/DownloadFileTask;->onProgressUpdate([Ljava/lang/Integer;)V

    return-void
.end method

.method protected setConnectionProperties(Ljava/net/HttpURLConnection;)V
    .registers 4
    .param p1, "connection"    # Ljava/net/HttpURLConnection;

    .line 134
    const-string v0, "User-Agent"

    const-string v1, "HockeySDK/Android"

    invoke-virtual {p1, v0, v1}, Ljava/net/HttpURLConnection;->addRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 135
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Ljava/net/HttpURLConnection;->setInstanceFollowRedirects(Z)V

    .line 138
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x9

    if-gt v0, v1, :cond_18

    .line 139
    const-string v0, "connection"

    const-string v1, "close"

    invoke-virtual {p1, v0, v1}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 141
    :cond_18
    return-void
.end method

.class public Lcom/itau/empresas/ui/view/BotaoComProgresso;
.super Landroid/widget/FrameLayout;
.source "BotaoComProgresso.java"


# instance fields
.field private botao:Lcom/itau/empresas/ui/view/ButtonIcon;

.field private progressBar:Landroid/widget/ProgressBar;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 2
    .param p1, "context"    # Landroid/content/Context;

    .line 24
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 25
    invoke-direct {p0}, Lcom/itau/empresas/ui/view/BotaoComProgresso;->aoIniciarCustomView()V

    .line 26
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .line 29
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 30
    invoke-direct {p0}, Lcom/itau/empresas/ui/view/BotaoComProgresso;->aoIniciarCustomView()V

    .line 31
    return-void
.end method

.method private aoIniciarCustomView()V
    .registers 1

    .line 34
    invoke-direct {p0}, Lcom/itau/empresas/ui/view/BotaoComProgresso;->iniciaViews()V

    .line 35
    invoke-direct {p0}, Lcom/itau/empresas/ui/view/BotaoComProgresso;->configuraProgressBar()V

    .line 36
    return-void
.end method

.method private configuraProgressBar()V
    .registers 3

    .line 60
    iget-object v0, p0, Lcom/itau/empresas/ui/view/BotaoComProgresso;->progressBar:Landroid/widget/ProgressBar;

    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    .line 61
    return-void
.end method

.method private exibeBotaoEscondeProgresso()V
    .registers 3

    .line 69
    iget-object v0, p0, Lcom/itau/empresas/ui/view/BotaoComProgresso;->botao:Lcom/itau/empresas/ui/view/ButtonIcon;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/view/ButtonIcon;->setVisibility(I)V

    .line 70
    iget-object v0, p0, Lcom/itau/empresas/ui/view/BotaoComProgresso;->progressBar:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 71
    return-void
.end method

.method private exibeProgressoEscondeBotao()V
    .registers 3

    .line 64
    iget-object v0, p0, Lcom/itau/empresas/ui/view/BotaoComProgresso;->botao:Lcom/itau/empresas/ui/view/ButtonIcon;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/view/ButtonIcon;->setVisibility(I)V

    .line 65
    iget-object v0, p0, Lcom/itau/empresas/ui/view/BotaoComProgresso;->progressBar:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 66
    return-void
.end method

.method private iniciaViews()V
    .registers 3

    .line 74
    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/BotaoComProgresso;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f030152

    invoke-static {v0, v1, p0}, Lcom/itau/empresas/ui/view/BotaoComProgresso;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 75
    const v0, 0x7f0e05d2

    invoke-virtual {p0, v0}, Lcom/itau/empresas/ui/view/BotaoComProgresso;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/ui/view/ButtonIcon;

    iput-object v0, p0, Lcom/itau/empresas/ui/view/BotaoComProgresso;->botao:Lcom/itau/empresas/ui/view/ButtonIcon;

    .line 76
    const v0, 0x7f0e036e

    invoke-virtual {p0, v0}, Lcom/itau/empresas/ui/view/BotaoComProgresso;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/itau/empresas/ui/view/BotaoComProgresso;->progressBar:Landroid/widget/ProgressBar;

    .line 77
    return-void
.end method


# virtual methods
.method public alternaExibicaoParaProgresso(Z)V
    .registers 2
    .param p1, "alteraParaProgresso"    # Z

    .line 51
    if-eqz p1, :cond_6

    .line 52
    invoke-direct {p0}, Lcom/itau/empresas/ui/view/BotaoComProgresso;->exibeProgressoEscondeBotao()V

    goto :goto_9

    .line 55
    :cond_6
    invoke-direct {p0}, Lcom/itau/empresas/ui/view/BotaoComProgresso;->exibeBotaoEscondeProgresso()V

    .line 57
    :goto_9
    return-void
.end method

.method public setEnabled(Z)V
    .registers 3
    .param p1, "enabled"    # Z

    .line 81
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->setEnabled(Z)V

    .line 82
    iget-object v0, p0, Lcom/itau/empresas/ui/view/BotaoComProgresso;->botao:Lcom/itau/empresas/ui/view/ButtonIcon;

    invoke-virtual {v0, p1}, Lcom/itau/empresas/ui/view/ButtonIcon;->setEnabled(Z)V

    .line 83
    return-void
.end method

.method public setOnClickListener(Landroid/view/View$OnClickListener;)V
    .registers 3
    .param p1, "l"    # Landroid/view/View$OnClickListener;

    .line 87
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 88
    iget-object v0, p0, Lcom/itau/empresas/ui/view/BotaoComProgresso;->botao:Lcom/itau/empresas/ui/view/ButtonIcon;

    invoke-virtual {v0, p1}, Lcom/itau/empresas/ui/view/ButtonIcon;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 89
    return-void
.end method

.method public setTextoBotao(Ljava/lang/String;)V
    .registers 3
    .param p1, "texto"    # Ljava/lang/String;

    .line 39
    iget-object v0, p0, Lcom/itau/empresas/ui/view/BotaoComProgresso;->botao:Lcom/itau/empresas/ui/view/ButtonIcon;

    invoke-virtual {v0, p1}, Lcom/itau/empresas/ui/view/ButtonIcon;->setText(Ljava/lang/CharSequence;)V

    .line 40
    return-void
.end method

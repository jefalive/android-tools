.class public final Lcom/google/ads/conversiontracking/AdWordsRemarketingReporter;
.super Lcom/google/ads/conversiontracking/GoogleConversionReporter;
.source "SourceFile"


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .registers 4
    .param p1, "applicationContext"    # Landroid/content/Context;
    .param p2, "conversionId"    # Ljava/lang/String;

    .line 47
    invoke-direct {p0}, Lcom/google/ads/conversiontracking/GoogleConversionReporter;-><init>()V

    .line 48
    iput-object p1, p0, Lcom/google/ads/conversiontracking/AdWordsRemarketingReporter;->a:Landroid/content/Context;

    .line 49
    iput-object p2, p0, Lcom/google/ads/conversiontracking/AdWordsRemarketingReporter;->b:Ljava/lang/String;

    .line 50
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/ads/conversiontracking/AdWordsRemarketingReporter;->c:Ljava/util/Map;

    .line 51
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/util/Map;)V
    .registers 4
    .param p1, "applicationContext"    # Landroid/content/Context;
    .param p2, "conversionId"    # Ljava/lang/String;
    .param p3, "customParameters"    # Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Landroid/content/Context;Ljava/lang/String;Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;)V"
        }
    .end annotation

    .line 34
    invoke-direct {p0}, Lcom/google/ads/conversiontracking/GoogleConversionReporter;-><init>()V

    .line 35
    iput-object p1, p0, Lcom/google/ads/conversiontracking/AdWordsRemarketingReporter;->a:Landroid/content/Context;

    .line 36
    iput-object p2, p0, Lcom/google/ads/conversiontracking/AdWordsRemarketingReporter;->b:Ljava/lang/String;

    .line 37
    iput-object p3, p0, Lcom/google/ads/conversiontracking/AdWordsRemarketingReporter;->c:Ljava/util/Map;

    .line 38
    return-void
.end method

.method public static reportWithConversionId(Landroid/content/Context;Ljava/lang/String;)V
    .registers 3
    .param p0, "applicationContext"    # Landroid/content/Context;
    .param p1, "conversionId"    # Ljava/lang/String;

    .line 103
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lcom/google/ads/conversiontracking/AdWordsRemarketingReporter;->reportWithConversionId(Landroid/content/Context;Ljava/lang/String;Ljava/util/Map;)V

    .line 104
    return-void
.end method

.method public static reportWithConversionId(Landroid/content/Context;Ljava/lang/String;Ljava/util/Map;)V
    .registers 4
    .param p0, "applicationContext"    # Landroid/content/Context;
    .param p1, "conversionId"    # Ljava/lang/String;
    .param p2, "customParameters"    # Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Landroid/content/Context;Ljava/lang/String;Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;)V"
        }
    .end annotation

    .line 91
    new-instance v0, Lcom/google/ads/conversiontracking/AdWordsRemarketingReporter;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/ads/conversiontracking/AdWordsRemarketingReporter;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/util/Map;)V

    .line 92
    invoke-virtual {v0}, Lcom/google/ads/conversiontracking/AdWordsRemarketingReporter;->report()V

    .line 93
    return-void
.end method


# virtual methods
.method public report()V
    .registers 9

    .line 58
    iget-object v0, p0, Lcom/google/ads/conversiontracking/AdWordsRemarketingReporter;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/ads/conversiontracking/c;->a(Landroid/content/Context;)Lcom/google/ads/conversiontracking/c;

    move-result-object v6

    .line 59
    iget-object v0, p0, Lcom/google/ads/conversiontracking/AdWordsRemarketingReporter;->b:Ljava/lang/String;

    invoke-virtual {v6, v0}, Lcom/google/ads/conversiontracking/c;->c(Ljava/lang/String;)V

    .line 62
    :try_start_b
    new-instance v0, Lcom/google/ads/conversiontracking/g$c;

    invoke-direct {v0}, Lcom/google/ads/conversiontracking/g$c;-><init>()V

    iget-object v1, p0, Lcom/google/ads/conversiontracking/AdWordsRemarketingReporter;->b:Ljava/lang/String;

    .line 63
    invoke-virtual {v0, v1}, Lcom/google/ads/conversiontracking/g$c;->a(Ljava/lang/String;)Lcom/google/ads/conversiontracking/g$c;

    move-result-object v0

    .line 64
    invoke-virtual {v0}, Lcom/google/ads/conversiontracking/g$c;->a()Lcom/google/ads/conversiontracking/g$c;

    move-result-object v0

    iget-object v1, p0, Lcom/google/ads/conversiontracking/AdWordsRemarketingReporter;->c:Ljava/util/Map;

    .line 65
    invoke-virtual {v0, v1}, Lcom/google/ads/conversiontracking/g$c;->a(Ljava/util/Map;)Lcom/google/ads/conversiontracking/g$c;

    move-result-object v0

    iget-object v1, p0, Lcom/google/ads/conversiontracking/AdWordsRemarketingReporter;->b:Ljava/lang/String;

    .line 66
    invoke-virtual {v6, v1}, Lcom/google/ads/conversiontracking/c;->d(Ljava/lang/String;)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/ads/conversiontracking/g$c;->a(Z)Lcom/google/ads/conversiontracking/g$c;

    move-result-object v7

    .line 68
    move-object v0, p0

    iget-object v1, p0, Lcom/google/ads/conversiontracking/AdWordsRemarketingReporter;->a:Landroid/content/Context;

    move-object v2, v7

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x1

    invoke-virtual/range {v0 .. v5}, Lcom/google/ads/conversiontracking/AdWordsRemarketingReporter;->a(Landroid/content/Context;Lcom/google/ads/conversiontracking/g$c;ZZZ)V
    :try_end_34
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_34} :catch_35

    .line 76
    goto :goto_3d

    .line 74
    :catch_35
    move-exception v7

    .line 75
    const-string v0, "GoogleConversionReporter"

    const-string v1, "Error sending ping"

    invoke-static {v0, v1, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 77
    :goto_3d
    return-void
.end method

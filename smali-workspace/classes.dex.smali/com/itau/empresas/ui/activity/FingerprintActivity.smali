.class public Lcom/itau/empresas/ui/activity/FingerprintActivity;
.super Lcom/itau/empresas/ui/activity/BaseActivity;
.source "FingerprintActivity.java"


# instance fields
.field private fingerprintDialog:Lcom/itau/empresas/ui/dialog/FingerprintDialog;

.field isRedirecionaConfiguracoes:Z

.field status:Ljava/lang/String;

.field textoFingerprint:Landroid/widget/TextView;

.field textoTermosDeUso:Landroid/widget/TextView;

.field textoTituloFingerprint:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 34
    invoke-direct {p0}, Lcom/itau/empresas/ui/activity/BaseActivity;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/itau/empresas/ui/activity/FingerprintActivity;)V
    .registers 1
    .param p0, "x0"    # Lcom/itau/empresas/ui/activity/FingerprintActivity;

    .line 34
    invoke-direct {p0}, Lcom/itau/empresas/ui/activity/FingerprintActivity;->redirecionaAcesso()V

    return-void
.end method

.method static synthetic access$102(Lcom/itau/empresas/ui/activity/FingerprintActivity;Lcom/itau/empresas/ui/dialog/FingerprintDialog;)Lcom/itau/empresas/ui/dialog/FingerprintDialog;
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/ui/activity/FingerprintActivity;
    .param p1, "x1"    # Lcom/itau/empresas/ui/dialog/FingerprintDialog;

    .line 34
    iput-object p1, p0, Lcom/itau/empresas/ui/activity/FingerprintActivity;->fingerprintDialog:Lcom/itau/empresas/ui/dialog/FingerprintDialog;

    return-object p1
.end method

.method public static intent(Landroid/content/Context;)Lcom/itau/empresas/ui/activity/FingerprintActivity_$IntentBuilder_;
    .registers 2
    .param p0, "context"    # Landroid/content/Context;

    .line 167
    new-instance v0, Lcom/itau/empresas/ui/activity/FingerprintActivity_$IntentBuilder_;

    invoke-direct {v0, p0}, Lcom/itau/empresas/ui/activity/FingerprintActivity_$IntentBuilder_;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method private redirecionaAcesso()V
    .registers 2

    .line 90
    iget-boolean v0, p0, Lcom/itau/empresas/ui/activity/FingerprintActivity;->isRedirecionaConfiguracoes:Z

    if-eqz v0, :cond_8

    .line 91
    invoke-virtual {p0}, Lcom/itau/empresas/ui/activity/FingerprintActivity;->finish()V

    goto :goto_f

    .line 94
    :cond_8
    invoke-static {p0}, Lcom/itau/empresas/feature/home/HomeLogadaActivity;->intent(Landroid/content/Context;)Lcom/itau/empresas/feature/home/HomeLogadaActivity_$IntentBuilder_;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/feature/home/HomeLogadaActivity_$IntentBuilder_;->start()Lorg/androidannotations/api/builder/PostActivityStarter;

    .line 96
    :goto_f
    return-void
.end method


# virtual methods
.method public carregaDadosTela()V
    .registers 3

    .line 46
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/FingerprintActivity;->status:Ljava/lang/String;

    if-eqz v0, :cond_2f

    .line 47
    const-string v0, "HABILITADO_OUTRO_DISPOSITIVO"

    iget-object v1, p0, Lcom/itau/empresas/ui/activity/FingerprintActivity;->status:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1f

    .line 48
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/FingerprintActivity;->textoTituloFingerprint:Landroid/widget/TextView;

    const v1, 0x7f070236

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 49
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/FingerprintActivity;->textoFingerprint:Landroid/widget/TextView;

    const v1, 0x7f07022e

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_2f

    .line 51
    :cond_1f
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/FingerprintActivity;->textoTituloFingerprint:Landroid/widget/TextView;

    const v1, 0x7f070235

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 52
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/FingerprintActivity;->textoFingerprint:Landroid/widget/TextView;

    const v1, 0x7f07022d

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 56
    :cond_2f
    :goto_2f
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/FingerprintActivity;->textoTermosDeUso:Landroid/widget/TextView;

    const v1, 0x7f070233

    invoke-virtual {p0, v1}, Lcom/itau/empresas/ui/activity/FingerprintActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 57
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/FingerprintActivity;->textoTermosDeUso:Landroid/widget/TextView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 58
    return-void
.end method

.method public clicouEmAgoraNao()V
    .registers 4

    .line 62
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/FingerprintActivity;->application:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getCodigoOperador()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lbr/com/itau/sdk/android/core/BackendClient;->hasKeyAccess(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 64
    new-instance v0, Lcom/itau/empresas/ui/activity/FingerprintActivity$1;

    invoke-direct {v0, p0}, Lcom/itau/empresas/ui/activity/FingerprintActivity$1;-><init>(Lcom/itau/empresas/ui/activity/FingerprintActivity;)V

    invoke-static {v0}, Lbr/com/itau/sdk/android/core/BackendClient;->callApi(Lbr/com/itau/sdk/android/core/type/BackendCall;)V

    .line 74
    :cond_14
    new-instance v0, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;

    invoke-direct {v0, p0}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget-object v1, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Estados;->UM_BOTAO:Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Estados;

    .line 75
    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;->estado(Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Estados;)Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;

    move-result-object v0

    .line 76
    const v1, 0x7f070238

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;->mensagem(I)Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;

    move-result-object v0

    .line 77
    const v1, 0x7f07048c

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;->textoNeutro(I)Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;->build()Lcom/itau/empresas/ui/dialog/CustomAlertDialog;

    move-result-object v2

    .line 79
    .local v2, "dialog":Lcom/itau/empresas/ui/dialog/CustomAlertDialog;
    new-instance v0, Lcom/itau/empresas/ui/activity/FingerprintActivity$2;

    invoke-direct {v0, p0, v2}, Lcom/itau/empresas/ui/activity/FingerprintActivity$2;-><init>(Lcom/itau/empresas/ui/activity/FingerprintActivity;Lcom/itau/empresas/ui/dialog/CustomAlertDialog;)V

    invoke-virtual {v2, v0}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog;->setNeutroListener(Landroid/view/View$OnClickListener;)V

    .line 86
    invoke-virtual {v2, p0}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog;->show(Landroid/support/v7/app/AppCompatActivity;)Lcom/itau/empresas/ui/dialog/BaseDialog;

    .line 87
    return-void
.end method

.method public clicouEmContinuar()V
    .registers 4

    .line 100
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/FingerprintActivity;->application:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getCodigoOperador()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lbr/com/itau/sdk/android/core/BackendClient;->hasKeyAccess(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 101
    new-instance v0, Lcom/itau/empresas/ui/activity/FingerprintActivity$3;

    invoke-direct {v0, p0}, Lcom/itau/empresas/ui/activity/FingerprintActivity$3;-><init>(Lcom/itau/empresas/ui/activity/FingerprintActivity;)V

    invoke-static {v0}, Lbr/com/itau/sdk/android/core/BackendClient;->callApi(Lbr/com/itau/sdk/android/core/type/BackendCall;)V

    .line 111
    :cond_14
    :try_start_14
    new-instance v0, Lcom/itau/empresas/ui/dialog/FingerprintDialog;

    invoke-direct {v0}, Lcom/itau/empresas/ui/dialog/FingerprintDialog;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/ui/activity/FingerprintActivity;->fingerprintDialog:Lcom/itau/empresas/ui/dialog/FingerprintDialog;

    .line 112
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/FingerprintActivity;->fingerprintDialog:Lcom/itau/empresas/ui/dialog/FingerprintDialog;

    const v1, 0x7f0706c6

    invoke-virtual {p0, v1}, Lcom/itau/empresas/ui/activity/FingerprintActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p0, v1}, Lcom/itau/empresas/ui/dialog/FingerprintDialog;->getBuilder(Landroid/content/Context;Ljava/lang/String;)Lcom/itau/empresas/ui/dialog/FingerprintDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/itau/empresas/ui/activity/FingerprintActivity$4;

    invoke-direct {v1, p0}, Lcom/itau/empresas/ui/activity/FingerprintActivity$4;-><init>(Lcom/itau/empresas/ui/activity/FingerprintActivity;)V

    .line 113
    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/dialog/FingerprintDialog$Builder;->callback(Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintCallback;)Lcom/itau/empresas/ui/dialog/FingerprintDialog$Builder;

    move-result-object v0

    .line 128
    invoke-virtual {v0}, Lcom/itau/empresas/ui/dialog/FingerprintDialog$Builder;->show()V
    :try_end_34
    .catch Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintException; {:try_start_14 .. :try_end_34} :catch_35

    .line 133
    goto :goto_3c

    .line 130
    :catch_35
    move-exception v2

    .line 131
    .local v2, "e":Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintException;
    invoke-virtual {p0, v2}, Lcom/itau/empresas/ui/activity/FingerprintActivity;->publicaEvento(Ljava/lang/Object;)V

    .line 132
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/itau/empresas/ui/activity/FingerprintActivity;->fingerprintDialog:Lcom/itau/empresas/ui/dialog/FingerprintDialog;

    .line 135
    .end local v2    # "e":Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintException;
    :goto_3c
    return-void
.end method

.method public onEventMainThread(Lbr/com/itau/sdk/android/core/login/model/RouterResponseKeyAccessDTO;)V
    .registers 6
    .param p1, "response"    # Lbr/com/itau/sdk/android/core/login/model/RouterResponseKeyAccessDTO;

    .line 138
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/FingerprintActivity;->application:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getCodigoOperador()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lbr/com/itau/sdk/android/core/BackendClient;->hasKeyAccess(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_51

    .line 140
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/FingerprintActivity;->application:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "OFP"

    iget-object v2, p0, Lcom/itau/empresas/ui/activity/FingerprintActivity;->application:Lcom/itau/empresas/CustomApplication;

    .line 141
    invoke-virtual {v2}, Lcom/itau/empresas/CustomApplication;->getCodigoOperador()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 142
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 144
    new-instance v0, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;

    invoke-direct {v0, p0}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget-object v1, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Estados;->UM_BOTAO:Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Estados;

    .line 145
    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;->estado(Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Estados;)Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;

    move-result-object v0

    .line 146
    const v1, 0x7f070237

    invoke-virtual {p0, v1}, Lcom/itau/empresas/ui/activity/FingerprintActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;->mensagem(Ljava/lang/String;)Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;

    move-result-object v0

    .line 147
    const v1, 0x7f07048c

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;->textoNeutro(I)Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;->build()Lcom/itau/empresas/ui/dialog/CustomAlertDialog;

    move-result-object v3

    .line 149
    .local v3, "dialog":Lcom/itau/empresas/ui/dialog/CustomAlertDialog;
    new-instance v0, Lcom/itau/empresas/ui/activity/FingerprintActivity$5;

    invoke-direct {v0, p0, v3}, Lcom/itau/empresas/ui/activity/FingerprintActivity$5;-><init>(Lcom/itau/empresas/ui/activity/FingerprintActivity;Lcom/itau/empresas/ui/dialog/CustomAlertDialog;)V

    invoke-virtual {v3, v0}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog;->setNeutroListener(Landroid/view/View$OnClickListener;)V

    .line 156
    invoke-virtual {v3, p0}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog;->show(Landroid/support/v7/app/AppCompatActivity;)Lcom/itau/empresas/ui/dialog/BaseDialog;

    .line 159
    .end local v3    # "dialog":Lcom/itau/empresas/ui/dialog/CustomAlertDialog;
    :cond_51
    return-void
.end method

.method public opkeysConsumidas()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Ljava/lang/String;>;"
        }
    .end annotation

    .line 163
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.class public Lcom/itau/empresas/api/model/DadosGpsRequest;
.super Ljava/lang/Object;
.source "DadosGpsRequest.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field public gpsRequestVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/GpsRequestVO;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "dados_gps"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public setGpsRequestVO(Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/GpsRequestVO;)V
    .registers 2
    .param p1, "gpsRequestVO"    # Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/GpsRequestVO;

    .line 18
    iput-object p1, p0, Lcom/itau/empresas/api/model/DadosGpsRequest;->gpsRequestVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/GpsRequestVO;

    .line 19
    return-void
.end method

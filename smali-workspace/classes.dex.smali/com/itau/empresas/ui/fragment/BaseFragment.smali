.class public abstract Lcom/itau/empresas/ui/fragment/BaseFragment;
.super Landroid/support/v4/app/Fragment;
.source "BaseFragment.java"

# interfaces
.implements Lcom/itau/empresas/api/ApiConsumidor;


# instance fields
.field protected application:Lcom/itau/empresas/CustomApplication;


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 30
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    return-void
.end method


# virtual methods
.method public analyticsHit(Ljava/lang/String;Ljava/lang/String;)V
    .registers 4
    .param p1, "acao"    # Ljava/lang/String;
    .param p2, "label"    # Ljava/lang/String;

    .line 122
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/itau/empresas/ui/fragment/BaseFragment;->analyticsHit(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    .line 123
    return-void
.end method

.method public analyticsHit(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V
    .registers 10
    .param p1, "acao"    # Ljava/lang/String;
    .param p2, "label"    # Ljava/lang/String;
    .param p3, "valor"    # Ljava/lang/Long;

    .line 126
    invoke-virtual {p0}, Lcom/itau/empresas/ui/fragment/BaseFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    .line 127
    .local v3, "activity":Landroid/app/Activity;
    if-eqz v3, :cond_26

    instance-of v0, v3, Lcom/itau/empresas/ui/activity/BaseActivity;

    if-eqz v0, :cond_26

    .line 129
    move-object v4, v3

    check-cast v4, Lcom/itau/empresas/ui/activity/BaseActivity;

    .line 130
    .local v4, "base":Lcom/itau/empresas/ui/activity/BaseActivity;
    invoke-virtual {v3}, Landroid/app/Activity;->getApplication()Landroid/app/Application;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/itau/empresas/CustomApplication;

    .line 131
    .local v5, "app":Lcom/itau/empresas/CustomApplication;
    if-eqz v5, :cond_26

    .line 132
    invoke-virtual {v5}, Lcom/itau/empresas/CustomApplication;->getEventoTracker()Lcom/itau/empresas/ui/util/analytics/TrackerEvento;

    move-result-object v0

    new-instance v1, Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;

    .line 134
    invoke-virtual {v4}, Lcom/itau/empresas/ui/activity/BaseActivity;->hitAnalytics()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, p1, p2, p3}, Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    .line 133
    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/analytics/TrackerEvento;->enviarHitEvento(Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;)V

    .line 137
    .end local v4    # "base":Lcom/itau/empresas/ui/activity/BaseActivity;
    .end local v5    # "app":Lcom/itau/empresas/CustomApplication;
    :cond_26
    return-void
.end method

.method public baseActivity()Lcom/itau/empresas/ui/activity/BaseActivity;
    .registers 2

    .line 40
    invoke-virtual {p0}, Lcom/itau/empresas/ui/fragment/BaseFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/ui/activity/BaseActivity;

    return-object v0
.end method

.method public escondeDialogoDeProgresso()V
    .registers 3

    .line 105
    invoke-virtual {p0}, Lcom/itau/empresas/ui/fragment/BaseFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 106
    .local v0, "act":Landroid/app/Activity;
    if-eqz v0, :cond_c

    .line 107
    move-object v1, v0

    check-cast v1, Lcom/itau/empresas/ui/activity/BaseActivity;

    .line 108
    .local v1, "base":Lcom/itau/empresas/ui/activity/BaseActivity;
    invoke-virtual {v1}, Lcom/itau/empresas/ui/activity/BaseActivity;->escondeDialogoDeProgresso()V

    .line 110
    .end local v1    # "base":Lcom/itau/empresas/ui/activity/BaseActivity;
    :cond_c
    return-void
.end method

.method public eventoAdobe(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 6
    .param p1, "nome"    # Ljava/lang/String;
    .param p2, "secao"    # Ljava/lang/String;
    .param p3, "estadoLogin"    # Ljava/lang/String;

    .line 141
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 142
    .local v1, "cDados":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    const v0, 0x7f0700c1

    invoke-virtual {p0, v0}, Lcom/itau/empresas/ui/fragment/BaseFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 143
    const v0, 0x7f0700c0

    invoke-virtual {p0, v0}, Lcom/itau/empresas/ui/fragment/BaseFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 144
    const v0, 0x7f0700bc

    invoke-virtual {p0, v0}, Lcom/itau/empresas/ui/fragment/BaseFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0, p3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 146
    invoke-static {p1, v1}, Lcom/adobe/mobile/Analytics;->trackState(Ljava/lang/String;Ljava/util/Map;)V

    .line 147
    return-void
.end method

.method public mostraDialogoDeProgresso()V
    .registers 3

    .line 97
    invoke-virtual {p0}, Lcom/itau/empresas/ui/fragment/BaseFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 98
    .local v0, "act":Landroid/app/Activity;
    if-eqz v0, :cond_c

    .line 99
    move-object v1, v0

    check-cast v1, Lcom/itau/empresas/ui/activity/BaseActivity;

    .line 100
    .local v1, "base":Lcom/itau/empresas/ui/activity/BaseActivity;
    invoke-virtual {v1}, Lcom/itau/empresas/ui/activity/BaseActivity;->mostraDialogoDeProgresso()V

    .line 102
    .end local v1    # "base":Lcom/itau/empresas/ui/activity/BaseActivity;
    :cond_c
    return-void
.end method

.method protected ocultarMensagemErro()V
    .registers 2

    .line 150
    invoke-virtual {p0}, Lcom/itau/empresas/ui/fragment/BaseFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/itau/empresas/ui/util/SnackbarUtils;->tentaDispensarSnackbarNo(Ljava/lang/Object;)V

    .line 151
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 46
    :try_start_0
    invoke-static {p0}, Lcom/itau/empresas/ui/util/EventBusUtils;->registraEventBus(Landroid/support/v4/app/Fragment;)V
    :try_end_3
    .catchall {:try_start_0 .. :try_end_3} :catchall_7

    .line 48
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 49
    goto :goto_c

    .line 48
    :catchall_7
    move-exception v0

    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    throw v0

    .line 50
    :goto_c
    return-void
.end method

.method public onDestroy()V
    .registers 2

    .line 82
    :try_start_0
    invoke-static {p0}, Lcom/itau/empresas/ui/util/EventBusUtils;->desregistraEventBus(Landroid/support/v4/app/Fragment;)V
    :try_end_3
    .catchall {:try_start_0 .. :try_end_3} :catchall_7

    .line 84
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroy()V

    .line 85
    goto :goto_c

    .line 84
    :catchall_7
    move-exception v0

    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroy()V

    throw v0

    .line 86
    :goto_c
    return-void
.end method

.method public onEventMainThread(Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent$Conversion;)V
    .registers 8
    .param p1, "e"    # Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent$Conversion;

    .line 154
    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent$Conversion;->getException()Lbr/com/itau/sdk/android/core/exception/BackendException;

    move-result-object v3

    .line 155
    .local v3, "exception":Lbr/com/itau/sdk/android/core/exception/BackendException;
    const-string v0, "ItauEmpresas"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Erro na Chamanda: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v3}, Lbr/com/itau/sdk/android/core/exception/BackendException;->getOpKey()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 157
    invoke-virtual {p0}, Lcom/itau/empresas/ui/fragment/BaseFragment;->escondeDialogoDeProgresso()V

    .line 158
    invoke-virtual {v3}, Lbr/com/itau/sdk/android/core/exception/BackendException;->getOpKey()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v4

    .line 159
    .local v4, "opKey":Ljava/lang/String;
    const-string v0, "saldo"

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5f

    const-string v0, "multiLimites"

    .line 160
    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5f

    const-string v0, "lis"

    .line 161
    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5f

    .line 162
    invoke-virtual {v3}, Lbr/com/itau/sdk/android/core/exception/BackendException;->getError()Lbr/com/itau/sdk/android/core/model/ErrorDTO;

    move-result-object v5

    .line 163
    .local v5, "error":Lbr/com/itau/sdk/android/core/model/ErrorDTO;
    invoke-virtual {p0}, Lcom/itau/empresas/ui/fragment/BaseFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v5}, Lbr/com/itau/sdk/android/core/model/ErrorDTO;->getMensagem()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/itau/empresas/ui/util/SnackbarUtils;->tentaMostrarSnackbarNo(Ljava/lang/Object;Ljava/lang/CharSequence;I)V

    .line 166
    .end local v5    # "error":Lbr/com/itau/sdk/android/core/model/ErrorDTO;
    :cond_5f
    return-void
.end method

.method public onEventMainThread(Ljava/lang/Void;)V
    .registers 2
    .param p1, "dummy"    # Ljava/lang/Void;

    .line 115
    return-void
.end method

.method public onFragmentVisible()V
    .registers 1

    .line 36
    return-void
.end method

.method public onPause()V
    .registers 2

    .line 73
    :try_start_0
    invoke-static {p0}, Lcom/itau/empresas/ui/util/EventBusUtils;->registraEventBus(Landroid/support/v4/app/Fragment;)V
    :try_end_3
    .catchall {:try_start_0 .. :try_end_3} :catchall_7

    .line 75
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onPause()V

    .line 76
    goto :goto_c

    .line 75
    :catchall_7
    move-exception v0

    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onPause()V

    throw v0

    .line 77
    :goto_c
    return-void
.end method

.method public onResume()V
    .registers 2

    .line 55
    :try_start_0
    invoke-static {p0}, Lcom/itau/empresas/ui/util/EventBusUtils;->registraEventBus(Landroid/support/v4/app/Fragment;)V
    :try_end_3
    .catchall {:try_start_0 .. :try_end_3} :catchall_7

    .line 57
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    .line 58
    goto :goto_c

    .line 57
    :catchall_7
    move-exception v0

    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    throw v0

    .line 59
    :goto_c
    return-void
.end method

.method public onStart()V
    .registers 2

    .line 64
    :try_start_0
    invoke-static {p0}, Lcom/itau/empresas/ui/util/EventBusUtils;->registraEventBus(Landroid/support/v4/app/Fragment;)V
    :try_end_3
    .catchall {:try_start_0 .. :try_end_3} :catchall_7

    .line 66
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onStart()V

    .line 67
    goto :goto_c

    .line 66
    :catchall_7
    move-exception v0

    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onStart()V

    throw v0

    .line 68
    :goto_c
    return-void
.end method

.method public setUserVisibleHint(Z)V
    .registers 2
    .param p1, "isVisibleToUser"    # Z

    .line 90
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->setUserVisibleHint(Z)V

    .line 91
    if-eqz p1, :cond_8

    .line 92
    invoke-virtual {p0}, Lcom/itau/empresas/ui/fragment/BaseFragment;->onFragmentVisible()V

    .line 94
    :cond_8
    return-void
.end method

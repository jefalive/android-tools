.class Lokhttp3/ConnectionPool$1;
.super Ljava/lang/Object;
.source "ConnectionPool.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lokhttp3/ConnectionPool;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lokhttp3/ConnectionPool;


# direct methods
.method constructor <init>(Lokhttp3/ConnectionPool;)V
    .registers 2
    .param p1, "this$0"    # Lokhttp3/ConnectionPool;

    .line 56
    iput-object p1, p0, Lokhttp3/ConnectionPool$1;->this$0:Lokhttp3/ConnectionPool;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .registers 11

    .line 59
    :goto_0
    iget-object v0, p0, Lokhttp3/ConnectionPool$1;->this$0:Lokhttp3/ConnectionPool;

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lokhttp3/ConnectionPool;->cleanup(J)J

    move-result-wide v3

    .line 60
    .local v3, "waitNanos":J
    const-wide/16 v0, -0x1

    cmp-long v0, v3, v0

    if-nez v0, :cond_11

    return-void

    .line 61
    :cond_11
    const-wide/16 v0, 0x0

    cmp-long v0, v3, v0

    if-lez v0, :cond_31

    .line 62
    const-wide/32 v0, 0xf4240

    div-long v5, v3, v0

    .line 63
    .local v5, "waitMillis":J
    const-wide/32 v0, 0xf4240

    mul-long/2addr v0, v5

    sub-long/2addr v3, v0

    .line 64
    iget-object v7, p0, Lokhttp3/ConnectionPool$1;->this$0:Lokhttp3/ConnectionPool;

    monitor-enter v7

    .line 66
    :try_start_24
    iget-object v0, p0, Lokhttp3/ConnectionPool$1;->this$0:Lokhttp3/ConnectionPool;

    long-to-int v1, v3

    invoke-virtual {v0, v5, v6, v1}, Ljava/lang/Object;->wait(JI)V
    :try_end_2a
    .catch Ljava/lang/InterruptedException; {:try_start_24 .. :try_end_2a} :catch_2b
    .catchall {:try_start_24 .. :try_end_2a} :catchall_2e

    .line 68
    goto :goto_2c

    .line 67
    :catch_2b
    move-exception v8

    .line 69
    :goto_2c
    monitor-exit v7

    goto :goto_31

    :catchall_2e
    move-exception v9

    monitor-exit v7

    throw v9

    .line 71
    .end local v3    # "waitNanos":J
    .end local v5    # "waitMillis":J
    :cond_31
    :goto_31
    goto :goto_0
.end method

.class public Lbr/com/itau/textovoz/api/handler/NetworkHandlerSearch;
.super Ljava/lang/Object;
.source "NetworkHandlerSearch.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lbr/com/itau/textovoz/api/handler/NetworkHandlerSearch$OnPushMessageListener;
    }
.end annotation


# static fields
.field public static final TAG:Ljava/lang/String;

.field public static networkHandlerSearch:Lbr/com/itau/textovoz/api/handler/NetworkHandlerSearch;


# instance fields
.field private listener:Lbr/com/itau/textovoz/api/handler/NetworkHandlerSearch$OnPushMessageListener;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .line 10
    const-class v0, Lbr/com/itau/textovoz/api/handler/NetworkHandlerSearch;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lbr/com/itau/textovoz/api/handler/NetworkHandlerSearch;->TAG:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .registers 2

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    invoke-static {}, Lde/greenrobot/event/EventBus;->getDefault()Lde/greenrobot/event/EventBus;

    move-result-object v0

    invoke-virtual {v0, p0}, Lde/greenrobot/event/EventBus;->register(Ljava/lang/Object;)V

    .line 25
    return-void
.end method

.method private checkUrl(Ljava/lang/String;)Z
    .registers 3
    .param p1, "opKey"    # Ljava/lang/String;

    .line 39
    const-string v0, "atendimentosBuscas"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public static getInstance()Lbr/com/itau/textovoz/api/handler/NetworkHandlerSearch;
    .registers 1

    .line 28
    sget-object v0, Lbr/com/itau/textovoz/api/handler/NetworkHandlerSearch;->networkHandlerSearch:Lbr/com/itau/textovoz/api/handler/NetworkHandlerSearch;

    if-nez v0, :cond_b

    .line 29
    new-instance v0, Lbr/com/itau/textovoz/api/handler/NetworkHandlerSearch;

    invoke-direct {v0}, Lbr/com/itau/textovoz/api/handler/NetworkHandlerSearch;-><init>()V

    sput-object v0, Lbr/com/itau/textovoz/api/handler/NetworkHandlerSearch;->networkHandlerSearch:Lbr/com/itau/textovoz/api/handler/NetworkHandlerSearch;

    .line 31
    :cond_b
    sget-object v0, Lbr/com/itau/textovoz/api/handler/NetworkHandlerSearch;->networkHandlerSearch:Lbr/com/itau/textovoz/api/handler/NetworkHandlerSearch;

    return-object v0
.end method


# virtual methods
.method public onEventMainThread(Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;)V
    .registers 3
    .param p1, "backendExceptionEvent"    # Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;

    .line 50
    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;->getException()Lbr/com/itau/sdk/android/core/exception/BackendException;

    move-result-object v0

    invoke-virtual {v0}, Lbr/com/itau/sdk/android/core/exception/BackendException;->getOpKey()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lbr/com/itau/textovoz/api/handler/NetworkHandlerSearch;->checkUrl(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 51
    iget-object v0, p0, Lbr/com/itau/textovoz/api/handler/NetworkHandlerSearch;->listener:Lbr/com/itau/textovoz/api/handler/NetworkHandlerSearch$OnPushMessageListener;

    invoke-interface {v0}, Lbr/com/itau/textovoz/api/handler/NetworkHandlerSearch$OnPushMessageListener;->onPushMessagesListFailure()V

    .line 53
    :cond_13
    return-void
.end method

.method public onEventMainThread(Lbr/com/itau/textovoz/api/event/EventClientSearchResponse;)V
    .registers 4
    .param p1, "eventPushMessagesListFetch"    # Lbr/com/itau/textovoz/api/event/EventClientSearchResponse;

    .line 44
    invoke-virtual {p1}, Lbr/com/itau/textovoz/api/event/EventClientSearchResponse;->getClientAttendanceResponse()Lbr/com/itau/textovoz/model/response/SearchResponse;

    move-result-object v1

    .line 45
    .local v1, "searchResponse":Lbr/com/itau/textovoz/model/response/SearchResponse;
    iget-object v0, p0, Lbr/com/itau/textovoz/api/handler/NetworkHandlerSearch;->listener:Lbr/com/itau/textovoz/api/handler/NetworkHandlerSearch$OnPushMessageListener;

    invoke-interface {v0, v1}, Lbr/com/itau/textovoz/api/handler/NetworkHandlerSearch$OnPushMessageListener;->onPushMessageListSuccessful(Lbr/com/itau/textovoz/model/response/SearchResponse;)V

    .line 46
    return-void
.end method

.method public setHandler(Lbr/com/itau/textovoz/api/handler/NetworkHandlerSearch$OnPushMessageListener;)V
    .registers 2
    .param p1, "onPushMessageListener"    # Lbr/com/itau/textovoz/api/handler/NetworkHandlerSearch$OnPushMessageListener;

    .line 35
    iput-object p1, p0, Lbr/com/itau/textovoz/api/handler/NetworkHandlerSearch;->listener:Lbr/com/itau/textovoz/api/handler/NetworkHandlerSearch$OnPushMessageListener;

    .line 36
    return-void
.end method

.class public Lcom/itau/empresas/ui/dialog/FingerprintDialog;
.super Ljava/lang/Object;
.source "FingerprintDialog.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/itau/empresas/ui/dialog/FingerprintDialog$DismissOnClickListener;,
        Lcom/itau/empresas/ui/dialog/FingerprintDialog$AbreLoginComSenhaOnClickListener;,
        Lcom/itau/empresas/ui/dialog/FingerprintDialog$Builder;
    }
.end annotation


# instance fields
.field private build:Lcom/itau/empresas/ui/dialog/FingerprintDialog$Builder;


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getBuilder(Landroid/content/Context;Ljava/lang/String;)Lcom/itau/empresas/ui/dialog/FingerprintDialog$Builder;
    .registers 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "string"    # Ljava/lang/String;

    .line 24
    iget-object v0, p0, Lcom/itau/empresas/ui/dialog/FingerprintDialog;->build:Lcom/itau/empresas/ui/dialog/FingerprintDialog$Builder;

    if-nez v0, :cond_b

    .line 25
    new-instance v0, Lcom/itau/empresas/ui/dialog/FingerprintDialog$Builder;

    invoke-direct {v0, p1, p2}, Lcom/itau/empresas/ui/dialog/FingerprintDialog$Builder;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/itau/empresas/ui/dialog/FingerprintDialog;->build:Lcom/itau/empresas/ui/dialog/FingerprintDialog$Builder;

    .line 27
    :cond_b
    iget-object v0, p0, Lcom/itau/empresas/ui/dialog/FingerprintDialog;->build:Lcom/itau/empresas/ui/dialog/FingerprintDialog$Builder;

    return-object v0
.end method

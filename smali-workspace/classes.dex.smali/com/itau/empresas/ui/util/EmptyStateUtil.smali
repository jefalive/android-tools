.class public final Lcom/itau/empresas/ui/util/EmptyStateUtil;
.super Ljava/lang/Object;
.source "EmptyStateUtil.java"


# direct methods
.method public static mostraEmptyState(Landroid/content/Context;Landroid/view/ViewGroup;Ljava/lang/String;I)V
    .registers 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "layout"    # Landroid/view/ViewGroup;
    .param p2, "mensagem"    # Ljava/lang/String;
    .param p3, "icone"    # I

    .line 23
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030033

    invoke-virtual {v0, v1, p1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 26
    .local v2, "inflate":Landroid/view/View;
    const v0, 0x7f0e016e

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Landroid/widget/TextView;

    .line 27
    .local v3, "emptyStateText":Landroid/widget/TextView;
    const v0, 0x7f0e016d

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Landroid/widget/TextView;

    .line 28
    .local v4, "emptyStateImage":Landroid/widget/TextView;
    invoke-virtual {v3, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 29
    invoke-virtual {v4, p3}, Landroid/widget/TextView;->setText(I)V

    .line 30
    return-void
.end method

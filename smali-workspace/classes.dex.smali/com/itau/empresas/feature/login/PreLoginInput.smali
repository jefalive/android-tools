.class public interface abstract Lcom/itau/empresas/feature/login/PreLoginInput;
.super Ljava/lang/Object;
.source "PreLoginInput.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/itau/empresas/feature/login/PreLoginInput$PreLoginInputListener;
    }
.end annotation


# virtual methods
.method public abstract alternaExibicaoBotaoEntrarParProgresso(Z)V
.end method

.method public abstract modoDocumentoUnico(Ljava/lang/String;)V
.end method

.method public abstract modoListaOperadores(Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<Lcom/itau/empresas/api/model/OperadorVO;>;)V"
        }
    .end annotation
.end method

.method public abstract modoMultiDocumentos()V
.end method

.method public abstract postaTipoDeInput()V
.end method

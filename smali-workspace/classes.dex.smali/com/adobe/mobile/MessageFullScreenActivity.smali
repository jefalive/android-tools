.class public Lcom/adobe/mobile/MessageFullScreenActivity;
.super Lcom/adobe/mobile/AdobeMarketingActivity;
.source "MessageFullScreenActivity.java"


# instance fields
.field protected message:Lcom/adobe/mobile/MessageFullScreen;


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 26
    invoke-direct {p0}, Lcom/adobe/mobile/AdobeMarketingActivity;-><init>()V

    return-void
.end method

.method private messageIsValid()Z
    .registers 3

    .line 124
    iget-object v0, p0, Lcom/adobe/mobile/MessageFullScreenActivity;->message:Lcom/adobe/mobile/MessageFullScreen;

    if-nez v0, :cond_1a

    .line 125
    const-string v0, "Messages - unable to display fullscreen message, message is undefined"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logWarningFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 126
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/adobe/mobile/Messages;->setCurrentMessage(Lcom/adobe/mobile/Message;)V

    .line 127
    invoke-virtual {p0}, Lcom/adobe/mobile/MessageFullScreenActivity;->finish()V

    .line 128
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/adobe/mobile/MessageFullScreenActivity;->overridePendingTransition(II)V

    .line 129
    const/4 v0, 0x0

    return v0

    .line 132
    :cond_1a
    const/4 v0, 0x1

    return v0
.end method

.method private restoreFromSavedState(Landroid/os/Bundle;)Lcom/adobe/mobile/MessageFullScreen;
    .registers 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 137
    const-string v0, "MessageFullScreenActivity.messageId"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 138
    .local v2, "messageId":Ljava/lang/String;
    invoke-static {v2}, Lcom/adobe/mobile/Messages;->getFullScreenMessageById(Ljava/lang/String;)Lcom/adobe/mobile/MessageFullScreen;

    move-result-object v1

    .line 140
    .local v1, "message":Lcom/adobe/mobile/MessageFullScreen;
    if-eqz v1, :cond_14

    .line 141
    const-string v0, "MessageFullScreenActivity.replacedHtml"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/adobe/mobile/MessageFullScreen;->replacedHtml:Ljava/lang/String;

    .line 145
    :cond_14
    return-object v1
.end method


# virtual methods
.method public onBackPressed()V
    .registers 3

    .line 111
    iget-object v0, p0, Lcom/adobe/mobile/MessageFullScreenActivity;->message:Lcom/adobe/mobile/MessageFullScreen;

    if-eqz v0, :cond_e

    .line 112
    iget-object v0, p0, Lcom/adobe/mobile/MessageFullScreenActivity;->message:Lcom/adobe/mobile/MessageFullScreen;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/adobe/mobile/MessageFullScreen;->isVisible:Z

    .line 113
    iget-object v0, p0, Lcom/adobe/mobile/MessageFullScreenActivity;->message:Lcom/adobe/mobile/MessageFullScreen;

    invoke-virtual {v0}, Lcom/adobe/mobile/MessageFullScreen;->viewed()V

    .line 116
    :cond_e
    invoke-virtual {p0}, Lcom/adobe/mobile/MessageFullScreenActivity;->finish()V

    .line 117
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/adobe/mobile/MessageFullScreenActivity;->overridePendingTransition(II)V

    .line 118
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 35
    invoke-super {p0, p1}, Lcom/adobe/mobile/AdobeMarketingActivity;->onCreate(Landroid/os/Bundle;)V

    .line 37
    if-eqz p1, :cond_11

    .line 38
    invoke-direct {p0, p1}, Lcom/adobe/mobile/MessageFullScreenActivity;->restoreFromSavedState(Landroid/os/Bundle;)Lcom/adobe/mobile/MessageFullScreen;

    move-result-object v0

    iput-object v0, p0, Lcom/adobe/mobile/MessageFullScreenActivity;->message:Lcom/adobe/mobile/MessageFullScreen;

    .line 41
    iget-object v0, p0, Lcom/adobe/mobile/MessageFullScreenActivity;->message:Lcom/adobe/mobile/MessageFullScreen;

    invoke-static {v0}, Lcom/adobe/mobile/Messages;->setCurrentMessageFullscreen(Lcom/adobe/mobile/MessageFullScreen;)V

    goto :goto_17

    .line 43
    :cond_11
    invoke-static {}, Lcom/adobe/mobile/Messages;->getCurrentFullscreenMessage()Lcom/adobe/mobile/MessageFullScreen;

    move-result-object v0

    iput-object v0, p0, Lcom/adobe/mobile/MessageFullScreenActivity;->message:Lcom/adobe/mobile/MessageFullScreen;

    .line 47
    :goto_17
    invoke-direct {p0}, Lcom/adobe/mobile/MessageFullScreenActivity;->messageIsValid()Z

    move-result v0

    if-nez v0, :cond_1e

    .line 48
    return-void

    .line 53
    :cond_1e
    iget-object v0, p0, Lcom/adobe/mobile/MessageFullScreenActivity;->message:Lcom/adobe/mobile/MessageFullScreen;

    iput-object p0, v0, Lcom/adobe/mobile/MessageFullScreen;->messageFullScreenActivity:Landroid/app/Activity;

    .line 55
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/adobe/mobile/MessageFullScreenActivity;->requestWindowFeature(I)Z

    .line 58
    new-instance v1, Landroid/widget/RelativeLayout;

    invoke-direct {v1, p0}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 59
    .local v1, "relativeLayout":Landroid/widget/RelativeLayout;
    invoke-virtual {p0, v1}, Lcom/adobe/mobile/MessageFullScreenActivity;->setContentView(Landroid/view/View;)V

    .line 62
    return-void
.end method

.method public onResume()V
    .registers 6

    .line 68
    invoke-super {p0}, Lcom/adobe/mobile/AdobeMarketingActivity;->onResume()V

    .line 71
    invoke-direct {p0}, Lcom/adobe/mobile/MessageFullScreenActivity;->messageIsValid()Z

    move-result v0

    if-nez v0, :cond_a

    .line 72
    return-void

    .line 77
    :cond_a
    const v0, 0x1020002

    :try_start_d
    invoke-virtual {p0, v0}, Lcom/adobe/mobile/MessageFullScreenActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Landroid/view/ViewGroup;

    .line 79
    .local v4, "rootViewGroup":Landroid/view/ViewGroup;
    if-nez v4, :cond_27

    .line 80
    const-string v0, "Messages - unable to get root view group from os"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logErrorFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 81
    invoke-virtual {p0}, Lcom/adobe/mobile/MessageFullScreenActivity;->finish()V

    .line 82
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/adobe/mobile/MessageFullScreenActivity;->overridePendingTransition(II)V

    goto :goto_2f

    .line 85
    :cond_27
    new-instance v0, Lcom/adobe/mobile/MessageFullScreenActivity$1;

    invoke-direct {v0, p0, v4}, Lcom/adobe/mobile/MessageFullScreenActivity$1;-><init>(Lcom/adobe/mobile/MessageFullScreenActivity;Landroid/view/ViewGroup;)V

    invoke-virtual {v4, v0}, Landroid/view/ViewGroup;->post(Ljava/lang/Runnable;)Z
    :try_end_2f
    .catch Ljava/lang/NullPointerException; {:try_start_d .. :try_end_2f} :catch_30

    .line 98
    .end local v4    # "rootViewGroup":Landroid/view/ViewGroup;
    :goto_2f
    goto :goto_48

    .line 94
    :catch_30
    move-exception v4

    .line 95
    .local v4, "ex":Ljava/lang/NullPointerException;
    const-string v0, "Messages - content view is in undefined state (%s)"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v4}, Ljava/lang/NullPointerException;->getMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logWarningFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 96
    invoke-virtual {p0}, Lcom/adobe/mobile/MessageFullScreenActivity;->finish()V

    .line 97
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/adobe/mobile/MessageFullScreenActivity;->overridePendingTransition(II)V

    .line 99
    .end local v4    # "ex":Ljava/lang/NullPointerException;
    :goto_48
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .registers 4
    .param p1, "outState"    # Landroid/os/Bundle;

    .line 104
    const-string v0, "MessageFullScreenActivity.messageId"

    iget-object v1, p0, Lcom/adobe/mobile/MessageFullScreenActivity;->message:Lcom/adobe/mobile/MessageFullScreen;

    iget-object v1, v1, Lcom/adobe/mobile/MessageFullScreen;->messageId:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 105
    const-string v0, "MessageFullScreenActivity.replacedHtml"

    iget-object v1, p0, Lcom/adobe/mobile/MessageFullScreenActivity;->message:Lcom/adobe/mobile/MessageFullScreen;

    iget-object v1, v1, Lcom/adobe/mobile/MessageFullScreen;->replacedHtml:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 106
    invoke-super {p0, p1}, Lcom/adobe/mobile/AdobeMarketingActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 107
    return-void
.end method

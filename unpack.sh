#!/bin/bash

# prepare temporary directory
rm -rf tmp/
mkdir tmp

# unpack apk
/usr/lib/jvm/java-7-oracle/bin/java -jar apktool/apktool_2.2.3.jar d -r -s apks/com.itau.empresas.apk -o tmp/com.itau.empresas.apk.decompiled

# decompile .dex file into smali files directory
/usr/lib/jvm/java-8-oracle/bin/java -jar jesus-freke/baksmali-2.2.1.jar d tmp/com.itau.empresas.apk.decompiled/classes.dex -o tmp/classes.dex.smali
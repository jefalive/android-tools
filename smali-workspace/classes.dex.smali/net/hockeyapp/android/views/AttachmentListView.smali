.class public Lnet/hockeyapp/android/views/AttachmentListView;
.super Landroid/view/ViewGroup;
.source "AttachmentListView.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private line_height:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .line 44
    const-class v0, Lnet/hockeyapp/android/views/AttachmentListView;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_a

    const/4 v0, 0x1

    goto :goto_b

    :cond_a
    const/4 v0, 0x0

    :goto_b
    sput-boolean v0, Lnet/hockeyapp/android/views/AttachmentListView;->$assertionsDisabled:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 2
    .param p1, "context"    # Landroid/content/Context;

    .line 49
    invoke-direct {p0, p1}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    .line 50
    return-void
.end method


# virtual methods
.method protected checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z
    .registers 3
    .param p1, "p"    # Landroid/view/ViewGroup$LayoutParams;

    .line 118
    instance-of v0, p1, Landroid/view/ViewGroup$LayoutParams;

    return v0
.end method

.method protected generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
    .registers 4

    .line 113
    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    const/4 v1, 0x1

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    return-object v0
.end method

.method public getAttachments()Ljava/util/ArrayList;
    .registers 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/ArrayList<Landroid/net/Uri;>;"
        }
    .end annotation

    .line 58
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 60
    .local v1, "attachments":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_6
    invoke-virtual {p0}, Lnet/hockeyapp/android/views/AttachmentListView;->getChildCount()I

    move-result v0

    if-ge v2, v0, :cond_1d

    .line 61
    invoke-virtual {p0, v2}, Lnet/hockeyapp/android/views/AttachmentListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lnet/hockeyapp/android/views/AttachmentView;

    .line 62
    .local v3, "attachmentView":Lnet/hockeyapp/android/views/AttachmentView;
    invoke-virtual {v3}, Lnet/hockeyapp/android/views/AttachmentView;->getAttachmentUri()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 60
    .end local v3    # "attachmentView":Lnet/hockeyapp/android/views/AttachmentView;
    add-int/lit8 v2, v2, 0x1

    goto :goto_6

    .line 64
    .end local v2    # "i":I
    :cond_1d
    return-object v1
.end method

.method protected onLayout(ZIIII)V
    .registers 17
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .line 123
    invoke-virtual {p0}, Lnet/hockeyapp/android/views/AttachmentListView;->getChildCount()I

    move-result v2

    .line 124
    .local v2, "count":I
    sub-int v3, p4, p2

    .line 125
    .local v3, "width":I
    invoke-virtual {p0}, Lnet/hockeyapp/android/views/AttachmentListView;->getPaddingLeft()I

    move-result v4

    .line 126
    .local v4, "xPos":I
    invoke-virtual {p0}, Lnet/hockeyapp/android/views/AttachmentListView;->getPaddingTop()I

    move-result v5

    .line 128
    .local v5, "yPos":I
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_f
    if-ge v6, v2, :cond_4d

    .line 129
    invoke-virtual {p0, v6}, Lnet/hockeyapp/android/views/AttachmentListView;->getChildAt(I)Landroid/view/View;

    move-result-object v7

    .line 131
    .local v7, "child":Landroid/view/View;
    invoke-virtual {v7}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-eq v0, v1, :cond_4a

    .line 132
    invoke-virtual {v7}, Landroid/view/View;->invalidate()V

    .line 133
    invoke-virtual {v7}, Landroid/view/View;->getMeasuredWidth()I

    move-result v8

    .line 134
    .local v8, "childWidth":I
    invoke-virtual {v7}, Landroid/view/View;->getMeasuredHeight()I

    move-result v9

    .line 136
    .local v9, "childHeight":I
    invoke-virtual {v7}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v10

    .line 137
    .local v10, "lp":Landroid/view/ViewGroup$LayoutParams;
    add-int v0, v4, v8

    if-le v0, v3, :cond_37

    .line 138
    invoke-virtual {p0}, Lnet/hockeyapp/android/views/AttachmentListView;->getPaddingLeft()I

    move-result v4

    .line 139
    iget v0, p0, Lnet/hockeyapp/android/views/AttachmentListView;->line_height:I

    add-int/2addr v5, v0

    .line 141
    :cond_37
    add-int v0, v4, v8

    add-int v1, v5, v9

    invoke-virtual {v7, v4, v5, v0, v1}, Landroid/view/View;->layout(IIII)V

    .line 142
    iget v0, v10, Landroid/view/ViewGroup$LayoutParams;->width:I

    add-int/2addr v0, v8

    move-object v1, v7

    check-cast v1, Lnet/hockeyapp/android/views/AttachmentView;

    invoke-virtual {v1}, Lnet/hockeyapp/android/views/AttachmentView;->getGap()I

    move-result v1

    add-int/2addr v0, v1

    add-int/2addr v4, v0

    .line 128
    .end local v7    # "child":Landroid/view/View;
    .end local v8    # "childWidth":I
    .end local v9    # "childHeight":I
    .end local v10    # "lp":Landroid/view/ViewGroup$LayoutParams;
    :cond_4a
    add-int/lit8 v6, v6, 0x1

    goto :goto_f

    .line 145
    .end local v6    # "i":I
    :cond_4d
    return-void
.end method

.method protected onMeasure(II)V
    .registers 16
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .line 69
    sget-boolean v0, Lnet/hockeyapp/android/views/AttachmentListView;->$assertionsDisabled:Z

    if-nez v0, :cond_10

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    if-nez v0, :cond_10

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 71
    :cond_10
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    .line 72
    .local v2, "width":I
    invoke-virtual {p0}, Lnet/hockeyapp/android/views/AttachmentListView;->getChildCount()I

    move-result v3

    .line 73
    .local v3, "count":I
    const/4 v4, 0x0

    .line 74
    .local v4, "height":I
    const/4 v5, 0x0

    .line 76
    .local v5, "line_height":I
    invoke-virtual {p0}, Lnet/hockeyapp/android/views/AttachmentListView;->getPaddingLeft()I

    move-result v6

    .line 77
    .local v6, "xPos":I
    invoke-virtual {p0}, Lnet/hockeyapp/android/views/AttachmentListView;->getPaddingTop()I

    move-result v7

    .line 79
    .local v7, "yPos":I
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_23
    if-ge v8, v3, :cond_71

    .line 80
    invoke-virtual {p0, v8}, Lnet/hockeyapp/android/views/AttachmentListView;->getChildAt(I)Landroid/view/View;

    move-result-object v9

    .line 81
    .local v9, "child":Landroid/view/View;
    move-object v10, v9

    check-cast v10, Lnet/hockeyapp/android/views/AttachmentView;

    .line 82
    .local v10, "attachmentView":Lnet/hockeyapp/android/views/AttachmentView;
    invoke-virtual {v10}, Lnet/hockeyapp/android/views/AttachmentView;->getEffectiveMaxHeight()I

    move-result v0

    invoke-virtual {v10}, Lnet/hockeyapp/android/views/AttachmentView;->getPaddingTop()I

    move-result v1

    add-int v4, v0, v1

    .line 84
    invoke-virtual {v9}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-eq v0, v1, :cond_6d

    .line 85
    invoke-virtual {v9}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v11

    .line 86
    .local v11, "lp":Landroid/view/ViewGroup$LayoutParams;
    const/high16 v0, -0x80000000

    invoke-static {v2, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    const/high16 v1, -0x80000000

    invoke-static {v4, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    invoke-virtual {v9, v0, v1}, Landroid/view/View;->measure(II)V

    .line 88
    invoke-virtual {v9}, Landroid/view/View;->getMeasuredWidth()I

    move-result v12

    .line 89
    .local v12, "childWidth":I
    invoke-virtual {v9}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    iget v1, v11, Landroid/view/ViewGroup$LayoutParams;->height:I

    add-int/2addr v0, v1

    invoke-static {v5, v0}, Ljava/lang/Math;->max(II)I

    move-result v5

    .line 91
    add-int v0, v6, v12

    if-le v0, v2, :cond_69

    .line 92
    invoke-virtual {p0}, Lnet/hockeyapp/android/views/AttachmentListView;->getPaddingLeft()I

    move-result v6

    .line 93
    add-int/2addr v7, v5

    .line 95
    :cond_69
    iget v0, v11, Landroid/view/ViewGroup$LayoutParams;->width:I

    add-int/2addr v0, v12

    add-int/2addr v6, v0

    .line 79
    .end local v9    # "child":Landroid/view/View;
    .end local v10    # "attachmentView":Lnet/hockeyapp/android/views/AttachmentView;
    .end local v11    # "lp":Landroid/view/ViewGroup$LayoutParams;
    .end local v12    # "childWidth":I
    :cond_6d
    add-int/lit8 v8, v8, 0x1

    goto/16 :goto_23

    .line 98
    .end local v8    # "i":I
    :cond_71
    iput v5, p0, Lnet/hockeyapp/android/views/AttachmentListView;->line_height:I

    .line 100
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    if-nez v0, :cond_82

    .line 101
    add-int v0, v7, v5

    invoke-virtual {p0}, Lnet/hockeyapp/android/views/AttachmentListView;->getPaddingBottom()I

    move-result v1

    add-int v4, v0, v1

    goto :goto_9b

    .line 103
    :cond_82
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    const/high16 v1, -0x80000000

    if-ne v0, v1, :cond_9b

    .line 104
    add-int v0, v7, v5

    invoke-virtual {p0}, Lnet/hockeyapp/android/views/AttachmentListView;->getPaddingBottom()I

    move-result v1

    add-int/2addr v0, v1

    if-ge v0, v4, :cond_9b

    .line 105
    add-int v0, v7, v5

    invoke-virtual {p0}, Lnet/hockeyapp/android/views/AttachmentListView;->getPaddingBottom()I

    move-result v1

    add-int v4, v0, v1

    .line 108
    :cond_9b
    :goto_9b
    invoke-virtual {p0, v2, v4}, Lnet/hockeyapp/android/views/AttachmentListView;->setMeasuredDimension(II)V

    .line 109
    return-void
.end method

.class public final Lbr/com/itau/sdk/android/core_ui/token/APPTokenView;
.super Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;
.source "SourceFile"


# static fields
.field private static final u:[B

.field private static v:I


# direct methods
.method static constructor <clinit>()V
    .registers 3

    const/16 v0, 0xd

    new-array v0, v0, [B

    fill-array-data v0, :array_e

    sput-object v0, Lbr/com/itau/sdk/android/core_ui/token/APPTokenView;->u:[B

    const/16 v0, 0x9e

    sput v0, Lbr/com/itau/sdk/android/core_ui/token/APPTokenView;->v:I

    return-void

    :array_e
    .array-data 1
        0x6t
        -0x3ft
        -0x1et
        -0x3t
        0xbt
        -0x8t
        -0x7t
        -0xat
        -0x6t
        0xft
        -0xft
        0x9t
        -0xbt
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 2

    .line 15
    invoke-direct {p0, p1}, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;-><init>(Landroid/content/Context;)V

    .line 16
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 3

    .line 19
    invoke-direct {p0, p1, p2}, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 20
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 4

    .line 23
    invoke-direct {p0, p1, p2, p3}, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 24
    return-void
.end method

.method private static a(III)Ljava/lang/String;
    .registers 9

    mul-int/lit8 p1, p1, 0x4

    add-int/lit8 p1, p1, 0xa

    mul-int/lit8 p0, p0, 0x2

    rsub-int/lit8 p0, p0, 0x3

    const/4 v5, -0x1

    sget-object v4, Lbr/com/itau/sdk/android/core_ui/token/APPTokenView;->u:[B

    mul-int/lit8 p2, p2, 0x4

    rsub-int/lit8 p2, p2, 0x61

    new-instance v0, Ljava/lang/String;

    new-array v1, p1, [B

    add-int/lit8 p1, p1, -0x1

    if-nez v4, :cond_1c

    move v2, p0

    move v3, p2

    :goto_19
    add-int/2addr v2, v3

    add-int/lit8 p2, v2, 0x4

    :cond_1c
    add-int/lit8 v5, v5, 0x1

    add-int/lit8 p0, p0, 0x1

    int-to-byte v2, p2

    aput-byte v2, v1, v5

    if-ne v5, p1, :cond_2a

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Ljava/lang/String;-><init>([BI)V

    return-object v0

    :cond_2a
    move v2, p2

    aget-byte v3, v4, p0

    goto :goto_19
.end method


# virtual methods
.method a(Landroid/util/AttributeSet;)V
    .registers 5

    .line 32
    invoke-super {p0, p1}, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->a(Landroid/util/AttributeSet;)V

    .line 34
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/APPTokenView;->g:Landroid/widget/EditText;

    invoke-virtual {p0}, Lbr/com/itau/sdk/android/core_ui/token/APPTokenView;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lbr/com/itau/sdk/android/core_ui/R$string;->itausdkcore_view_token_type_app_edit_text_description:I

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 35
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/APPTokenView;->h:Landroid/widget/EditText;

    invoke-virtual {p0}, Lbr/com/itau/sdk/android/core_ui/token/APPTokenView;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lbr/com/itau/sdk/android/core_ui/R$string;->itausdkcore_view_token_type_app_edit_text_description:I

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 36
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/APPTokenView;->t:Landroid/widget/RelativeLayout;

    invoke-virtual {p0}, Lbr/com/itau/sdk/android/core_ui/token/APPTokenView;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lbr/com/itau/sdk/android/core_ui/R$string;->itausdkcore_view_token_type_app_description:I

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 37
    return-void
.end method

.method protected a(Ljava/lang/String;)V
    .registers 5

    .line 42
    const/4 v0, 0x0

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lbr/com/itau/sdk/android/core_ui/token/APPTokenView;->a(III)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_13

    .line 43
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lbr/com/itau/sdk/android/core_ui/token/APPTokenView;->setVisibility(I)V

    .line 44
    return-void

    .line 47
    :cond_13
    invoke-super {p0, p1}, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->a(Ljava/lang/String;)V

    .line 48
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/APPTokenView;->i:Landroid/widget/TextView;

    sget v1, Lbr/com/itau/sdk/android/core_ui/R$string;->itausdkcore_unlock_warning_app:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 49
    return-void
.end method

.method protected getSelectionAction()Lbr/com/itau/sdk/android/core/token/TokenEvent$Action;
    .registers 2

    .line 53
    sget-object v0, Lbr/com/itau/sdk/android/core/token/TokenEvent$Action;->APP_SELECTED:Lbr/com/itau/sdk/android/core/token/TokenEvent$Action;

    return-object v0
.end method

.method protected getSendAction()Lbr/com/itau/sdk/android/core/token/TokenEvent$Action;
    .registers 2

    .line 58
    sget-object v0, Lbr/com/itau/sdk/android/core/token/TokenEvent$Action;->APP_CONFIRMED:Lbr/com/itau/sdk/android/core/token/TokenEvent$Action;

    return-object v0
.end method

.method getType()I
    .registers 2

    .line 63
    const/4 v0, 0x2

    return v0
.end method

.method public onEventMainThread(Lbr/com/itau/sdk/android/core/SDKCustomUiController$ValidacaoAppResponseDTO;)V
    .registers 2

    .line 27
    invoke-virtual {p0, p1}, Lbr/com/itau/sdk/android/core_ui/token/APPTokenView;->a(Lbr/com/itau/sdk/android/core/type/SecurityAssertResponseDTO;)V

    .line 28
    return-void
.end method

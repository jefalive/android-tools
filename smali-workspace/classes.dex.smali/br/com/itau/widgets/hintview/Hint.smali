.class public final Lbr/com/itau/widgets/hintview/Hint;
.super Ljava/lang/Object;
.source "Hint.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lbr/com/itau/widgets/hintview/Hint$Builder;
    }
.end annotation


# instance fields
.field private final isCancelable:Z

.field private final isDismissOnClick:Z

.field private final mAnchorView:Landroid/view/View;

.field private final mArrowLayoutListener:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

.field private final mArrowMargin:F

.field private mArrowView:Landroid/widget/ImageView;

.field private final mClickListener:Landroid/view/View$OnClickListener;

.field private mContentView:Landroid/widget/LinearLayout;

.field private mDefaultWidth:I

.field private final mGravity:I

.field private final mLocationLayoutListener:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

.field private final mLongClickListener:Landroid/view/View$OnLongClickListener;

.field private final mMargin:F

.field private final mOnAttachStateChangeListener:Landroid/view/View$OnAttachStateChangeListener;

.field private mOnClickListener:Lbr/com/itau/widgets/hintview/OnClickListener;

.field private mOnDismissListener:Lbr/com/itau/widgets/hintview/OnDismissListener;

.field private mOnLongClickListener:Lbr/com/itau/widgets/hintview/OnLongClickListener;

.field private final mPopupWindow:Landroid/widget/PopupWindow;

.field private final mTouchListener:Landroid/view/View$OnTouchListener;


# direct methods
.method private constructor <init>(Lbr/com/itau/widgets/hintview/Hint$Builder;)V
    .registers 4
    .param p1, "builder"    # Lbr/com/itau/widgets/hintview/Hint$Builder;

    .line 149
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    new-instance v0, Lbr/com/itau/widgets/hintview/Hint$1;

    invoke-direct {v0, p0}, Lbr/com/itau/widgets/hintview/Hint$1;-><init>(Lbr/com/itau/widgets/hintview/Hint;)V

    iput-object v0, p0, Lbr/com/itau/widgets/hintview/Hint;->mTouchListener:Landroid/view/View$OnTouchListener;

    .line 59
    new-instance v0, Lbr/com/itau/widgets/hintview/Hint$2;

    invoke-direct {v0, p0}, Lbr/com/itau/widgets/hintview/Hint$2;-><init>(Lbr/com/itau/widgets/hintview/Hint;)V

    iput-object v0, p0, Lbr/com/itau/widgets/hintview/Hint;->mOnAttachStateChangeListener:Landroid/view/View$OnAttachStateChangeListener;

    .line 71
    new-instance v0, Lbr/com/itau/widgets/hintview/Hint$3;

    invoke-direct {v0, p0}, Lbr/com/itau/widgets/hintview/Hint$3;-><init>(Lbr/com/itau/widgets/hintview/Hint;)V

    iput-object v0, p0, Lbr/com/itau/widgets/hintview/Hint;->mClickListener:Landroid/view/View$OnClickListener;

    .line 80
    new-instance v0, Lbr/com/itau/widgets/hintview/Hint$4;

    invoke-direct {v0, p0}, Lbr/com/itau/widgets/hintview/Hint$4;-><init>(Lbr/com/itau/widgets/hintview/Hint;)V

    iput-object v0, p0, Lbr/com/itau/widgets/hintview/Hint;->mLongClickListener:Landroid/view/View$OnLongClickListener;

    .line 89
    new-instance v0, Lbr/com/itau/widgets/hintview/Hint$5;

    invoke-direct {v0, p0}, Lbr/com/itau/widgets/hintview/Hint$5;-><init>(Lbr/com/itau/widgets/hintview/Hint;)V

    iput-object v0, p0, Lbr/com/itau/widgets/hintview/Hint;->mArrowLayoutListener:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    .line 136
    new-instance v0, Lbr/com/itau/widgets/hintview/Hint$6;

    invoke-direct {v0, p0}, Lbr/com/itau/widgets/hintview/Hint$6;-><init>(Lbr/com/itau/widgets/hintview/Hint;)V

    iput-object v0, p0, Lbr/com/itau/widgets/hintview/Hint;->mLocationLayoutListener:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    .line 147
    const/4 v0, -0x2

    iput v0, p0, Lbr/com/itau/widgets/hintview/Hint;->mDefaultWidth:I

    .line 150
    # getter for: Lbr/com/itau/widgets/hintview/Hint$Builder;->isCancelable:Z
    invoke-static {p1}, Lbr/com/itau/widgets/hintview/Hint$Builder;->access$1200(Lbr/com/itau/widgets/hintview/Hint$Builder;)Z

    move-result v0

    iput-boolean v0, p0, Lbr/com/itau/widgets/hintview/Hint;->isCancelable:Z

    .line 151
    # getter for: Lbr/com/itau/widgets/hintview/Hint$Builder;->isDismissOnClick:Z
    invoke-static {p1}, Lbr/com/itau/widgets/hintview/Hint$Builder;->access$1300(Lbr/com/itau/widgets/hintview/Hint$Builder;)Z

    move-result v0

    iput-boolean v0, p0, Lbr/com/itau/widgets/hintview/Hint;->isDismissOnClick:Z

    .line 153
    # getter for: Lbr/com/itau/widgets/hintview/Hint$Builder;->mGravity:I
    invoke-static {p1}, Lbr/com/itau/widgets/hintview/Hint$Builder;->access$1400(Lbr/com/itau/widgets/hintview/Hint$Builder;)I

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/hintview/Hint;->mGravity:I

    .line 154
    # getter for: Lbr/com/itau/widgets/hintview/Hint$Builder;->mMargin:F
    invoke-static {p1}, Lbr/com/itau/widgets/hintview/Hint$Builder;->access$1500(Lbr/com/itau/widgets/hintview/Hint$Builder;)F

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/hintview/Hint;->mMargin:F

    .line 155
    # getter for: Lbr/com/itau/widgets/hintview/Hint$Builder;->mArrowMargin:F
    invoke-static {p1}, Lbr/com/itau/widgets/hintview/Hint$Builder;->access$1600(Lbr/com/itau/widgets/hintview/Hint$Builder;)F

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/hintview/Hint;->mArrowMargin:F

    .line 156
    # getter for: Lbr/com/itau/widgets/hintview/Hint$Builder;->mAnchorView:Landroid/view/View;
    invoke-static {p1}, Lbr/com/itau/widgets/hintview/Hint$Builder;->access$1700(Lbr/com/itau/widgets/hintview/Hint$Builder;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/widgets/hintview/Hint;->mAnchorView:Landroid/view/View;

    .line 157
    # getter for: Lbr/com/itau/widgets/hintview/Hint$Builder;->mOnClickListener:Lbr/com/itau/widgets/hintview/OnClickListener;
    invoke-static {p1}, Lbr/com/itau/widgets/hintview/Hint$Builder;->access$1800(Lbr/com/itau/widgets/hintview/Hint$Builder;)Lbr/com/itau/widgets/hintview/OnClickListener;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/widgets/hintview/Hint;->mOnClickListener:Lbr/com/itau/widgets/hintview/OnClickListener;

    .line 158
    # getter for: Lbr/com/itau/widgets/hintview/Hint$Builder;->mOnLongClickListener:Lbr/com/itau/widgets/hintview/OnLongClickListener;
    invoke-static {p1}, Lbr/com/itau/widgets/hintview/Hint$Builder;->access$1900(Lbr/com/itau/widgets/hintview/Hint$Builder;)Lbr/com/itau/widgets/hintview/OnLongClickListener;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/widgets/hintview/Hint;->mOnLongClickListener:Lbr/com/itau/widgets/hintview/OnLongClickListener;

    .line 159
    # getter for: Lbr/com/itau/widgets/hintview/Hint$Builder;->mOnDismissListener:Lbr/com/itau/widgets/hintview/OnDismissListener;
    invoke-static {p1}, Lbr/com/itau/widgets/hintview/Hint$Builder;->access$2000(Lbr/com/itau/widgets/hintview/Hint$Builder;)Lbr/com/itau/widgets/hintview/OnDismissListener;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/widgets/hintview/Hint;->mOnDismissListener:Lbr/com/itau/widgets/hintview/OnDismissListener;

    .line 161
    iget v0, p0, Lbr/com/itau/widgets/hintview/Hint;->mGravity:I

    const/4 v1, 0x7

    if-ne v0, v1, :cond_6e

    .line 162
    const/4 v0, -0x1

    iput v0, p0, Lbr/com/itau/widgets/hintview/Hint;->mDefaultWidth:I

    .line 165
    :cond_6e
    new-instance v0, Landroid/widget/PopupWindow;

    # getter for: Lbr/com/itau/widgets/hintview/Hint$Builder;->mContext:Landroid/content/Context;
    invoke-static {p1}, Lbr/com/itau/widgets/hintview/Hint$Builder;->access$2100(Lbr/com/itau/widgets/hintview/Hint$Builder;)Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/PopupWindow;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lbr/com/itau/widgets/hintview/Hint;->mPopupWindow:Landroid/widget/PopupWindow;

    .line 166
    iget-object v0, p0, Lbr/com/itau/widgets/hintview/Hint;->mPopupWindow:Landroid/widget/PopupWindow;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 167
    iget-object v0, p0, Lbr/com/itau/widgets/hintview/Hint;->mPopupWindow:Landroid/widget/PopupWindow;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setClippingEnabled(Z)V

    .line 168
    iget-object v0, p0, Lbr/com/itau/widgets/hintview/Hint;->mPopupWindow:Landroid/widget/PopupWindow;

    iget v1, p0, Lbr/com/itau/widgets/hintview/Hint;->mDefaultWidth:I

    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setWidth(I)V

    .line 169
    iget-object v0, p0, Lbr/com/itau/widgets/hintview/Hint;->mPopupWindow:Landroid/widget/PopupWindow;

    const/4 v1, -0x2

    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setHeight(I)V

    .line 170
    iget-object v0, p0, Lbr/com/itau/widgets/hintview/Hint;->mPopupWindow:Landroid/widget/PopupWindow;

    invoke-direct {p0, p1}, Lbr/com/itau/widgets/hintview/Hint;->getContentView(Lbr/com/itau/widgets/hintview/Hint$Builder;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setContentView(Landroid/view/View;)V

    .line 171
    iget-object v0, p0, Lbr/com/itau/widgets/hintview/Hint;->mPopupWindow:Landroid/widget/PopupWindow;

    # getter for: Lbr/com/itau/widgets/hintview/Hint$Builder;->isCancelable:Z
    invoke-static {p1}, Lbr/com/itau/widgets/hintview/Hint$Builder;->access$1200(Lbr/com/itau/widgets/hintview/Hint$Builder;)Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setOutsideTouchable(Z)V

    .line 172
    iget-object v0, p0, Lbr/com/itau/widgets/hintview/Hint;->mPopupWindow:Landroid/widget/PopupWindow;

    new-instance v1, Lbr/com/itau/widgets/hintview/Hint$7;

    invoke-direct {v1, p0}, Lbr/com/itau/widgets/hintview/Hint$7;-><init>(Lbr/com/itau/widgets/hintview/Hint;)V

    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setOnDismissListener(Landroid/widget/PopupWindow$OnDismissListener;)V

    .line 182
    return-void
.end method

.method synthetic constructor <init>(Lbr/com/itau/widgets/hintview/Hint$Builder;Lbr/com/itau/widgets/hintview/Hint$1;)V
    .registers 3
    .param p1, "x0"    # Lbr/com/itau/widgets/hintview/Hint$Builder;
    .param p2, "x1"    # Lbr/com/itau/widgets/hintview/Hint$1;

    .line 37
    invoke-direct {p0, p1}, Lbr/com/itau/widgets/hintview/Hint;-><init>(Lbr/com/itau/widgets/hintview/Hint$Builder;)V

    return-void
.end method

.method static synthetic access$000(Lbr/com/itau/widgets/hintview/Hint;)Z
    .registers 2
    .param p0, "x0"    # Lbr/com/itau/widgets/hintview/Hint;

    .line 37
    iget-boolean v0, p0, Lbr/com/itau/widgets/hintview/Hint;->isCancelable:Z

    return v0
.end method

.method static synthetic access$100(Lbr/com/itau/widgets/hintview/Hint;)Z
    .registers 2
    .param p0, "x0"    # Lbr/com/itau/widgets/hintview/Hint;

    .line 37
    iget-boolean v0, p0, Lbr/com/itau/widgets/hintview/Hint;->isDismissOnClick:Z

    return v0
.end method

.method static synthetic access$1000(Lbr/com/itau/widgets/hintview/Hint;)Landroid/graphics/PointF;
    .registers 2
    .param p0, "x0"    # Lbr/com/itau/widgets/hintview/Hint;

    .line 37
    invoke-direct {p0}, Lbr/com/itau/widgets/hintview/Hint;->calculateLocation()Landroid/graphics/PointF;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1100(Lbr/com/itau/widgets/hintview/Hint;)Landroid/widget/PopupWindow;
    .registers 2
    .param p0, "x0"    # Lbr/com/itau/widgets/hintview/Hint;

    .line 37
    iget-object v0, p0, Lbr/com/itau/widgets/hintview/Hint;->mPopupWindow:Landroid/widget/PopupWindow;

    return-object v0
.end method

.method static synthetic access$200(Lbr/com/itau/widgets/hintview/Hint;)Lbr/com/itau/widgets/hintview/OnClickListener;
    .registers 2
    .param p0, "x0"    # Lbr/com/itau/widgets/hintview/Hint;

    .line 37
    iget-object v0, p0, Lbr/com/itau/widgets/hintview/Hint;->mOnClickListener:Lbr/com/itau/widgets/hintview/OnClickListener;

    return-object v0
.end method

.method static synthetic access$2200(Lbr/com/itau/widgets/hintview/Hint;)Landroid/view/View$OnAttachStateChangeListener;
    .registers 2
    .param p0, "x0"    # Lbr/com/itau/widgets/hintview/Hint;

    .line 37
    iget-object v0, p0, Lbr/com/itau/widgets/hintview/Hint;->mOnAttachStateChangeListener:Landroid/view/View$OnAttachStateChangeListener;

    return-object v0
.end method

.method static synthetic access$2300(Lbr/com/itau/widgets/hintview/Hint;)Lbr/com/itau/widgets/hintview/OnDismissListener;
    .registers 2
    .param p0, "x0"    # Lbr/com/itau/widgets/hintview/Hint;

    .line 37
    iget-object v0, p0, Lbr/com/itau/widgets/hintview/Hint;->mOnDismissListener:Lbr/com/itau/widgets/hintview/OnDismissListener;

    return-object v0
.end method

.method static synthetic access$300(Lbr/com/itau/widgets/hintview/Hint;)Lbr/com/itau/widgets/hintview/OnLongClickListener;
    .registers 2
    .param p0, "x0"    # Lbr/com/itau/widgets/hintview/Hint;

    .line 37
    iget-object v0, p0, Lbr/com/itau/widgets/hintview/Hint;->mOnLongClickListener:Lbr/com/itau/widgets/hintview/OnLongClickListener;

    return-object v0
.end method

.method static synthetic access$400(Lbr/com/itau/widgets/hintview/Hint;)Landroid/widget/LinearLayout;
    .registers 2
    .param p0, "x0"    # Lbr/com/itau/widgets/hintview/Hint;

    .line 37
    iget-object v0, p0, Lbr/com/itau/widgets/hintview/Hint;->mContentView:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$500(Lbr/com/itau/widgets/hintview/Hint;)Landroid/view/View;
    .registers 2
    .param p0, "x0"    # Lbr/com/itau/widgets/hintview/Hint;

    .line 37
    iget-object v0, p0, Lbr/com/itau/widgets/hintview/Hint;->mAnchorView:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$600(Lbr/com/itau/widgets/hintview/Hint;)I
    .registers 2
    .param p0, "x0"    # Lbr/com/itau/widgets/hintview/Hint;

    .line 37
    iget v0, p0, Lbr/com/itau/widgets/hintview/Hint;->mGravity:I

    return v0
.end method

.method static synthetic access$700(Lbr/com/itau/widgets/hintview/Hint;)Landroid/widget/ImageView;
    .registers 2
    .param p0, "x0"    # Lbr/com/itau/widgets/hintview/Hint;

    .line 37
    iget-object v0, p0, Lbr/com/itau/widgets/hintview/Hint;->mArrowView:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$800(Lbr/com/itau/widgets/hintview/Hint;)F
    .registers 2
    .param p0, "x0"    # Lbr/com/itau/widgets/hintview/Hint;

    .line 37
    iget v0, p0, Lbr/com/itau/widgets/hintview/Hint;->mArrowMargin:F

    return v0
.end method

.method static synthetic access$900(Lbr/com/itau/widgets/hintview/Hint;)Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;
    .registers 2
    .param p0, "x0"    # Lbr/com/itau/widgets/hintview/Hint;

    .line 37
    iget-object v0, p0, Lbr/com/itau/widgets/hintview/Hint;->mArrowLayoutListener:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    return-object v0
.end method

.method private calculateLocation()Landroid/graphics/PointF;
    .registers 7

    .line 352
    new-instance v3, Landroid/graphics/PointF;

    invoke-direct {v3}, Landroid/graphics/PointF;-><init>()V

    .line 354
    .local v3, "location":Landroid/graphics/PointF;
    iget-object v0, p0, Lbr/com/itau/widgets/hintview/Hint;->mAnchorView:Landroid/view/View;

    invoke-static {v0}, Lbr/com/itau/widgets/hintview/Util;->calculateRectInWindow(Landroid/view/View;)Landroid/graphics/RectF;

    move-result-object v4

    .line 355
    .local v4, "anchorRect":Landroid/graphics/RectF;
    new-instance v5, Landroid/graphics/PointF;

    invoke-virtual {v4}, Landroid/graphics/RectF;->centerX()F

    move-result v0

    invoke-virtual {v4}, Landroid/graphics/RectF;->centerY()F

    move-result v1

    invoke-direct {v5, v0, v1}, Landroid/graphics/PointF;-><init>(FF)V

    .line 357
    .local v5, "anchorCenter":Landroid/graphics/PointF;
    iget v0, p0, Lbr/com/itau/widgets/hintview/Hint;->mGravity:I

    sparse-switch v0, :sswitch_data_ac

    goto/16 :goto_aa

    .line 359
    :sswitch_1f
    iget v0, v4, Landroid/graphics/RectF;->left:F

    iget-object v1, p0, Lbr/com/itau/widgets/hintview/Hint;->mContentView:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getWidth()I

    move-result v1

    int-to-float v1, v1

    sub-float/2addr v0, v1

    iget v1, p0, Lbr/com/itau/widgets/hintview/Hint;->mMargin:F

    sub-float/2addr v0, v1

    iput v0, v3, Landroid/graphics/PointF;->x:F

    .line 360
    iget v0, v5, Landroid/graphics/PointF;->y:F

    iget-object v1, p0, Lbr/com/itau/widgets/hintview/Hint;->mContentView:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getHeight()I

    move-result v1

    int-to-float v1, v1

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    sub-float/2addr v0, v1

    iput v0, v3, Landroid/graphics/PointF;->y:F

    .line 361
    goto/16 :goto_aa

    .line 363
    :sswitch_3f
    iget v0, v4, Landroid/graphics/RectF;->right:F

    iget v1, p0, Lbr/com/itau/widgets/hintview/Hint;->mMargin:F

    add-float/2addr v0, v1

    iput v0, v3, Landroid/graphics/PointF;->x:F

    .line 364
    iget v0, v5, Landroid/graphics/PointF;->y:F

    iget-object v1, p0, Lbr/com/itau/widgets/hintview/Hint;->mContentView:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getHeight()I

    move-result v1

    int-to-float v1, v1

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    sub-float/2addr v0, v1

    iput v0, v3, Landroid/graphics/PointF;->y:F

    .line 365
    goto :goto_aa

    .line 367
    :sswitch_56
    iget v0, v5, Landroid/graphics/PointF;->x:F

    iget-object v1, p0, Lbr/com/itau/widgets/hintview/Hint;->mContentView:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getWidth()I

    move-result v1

    int-to-float v1, v1

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    sub-float/2addr v0, v1

    iput v0, v3, Landroid/graphics/PointF;->x:F

    .line 368
    iget v0, v4, Landroid/graphics/RectF;->top:F

    iget-object v1, p0, Lbr/com/itau/widgets/hintview/Hint;->mContentView:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getHeight()I

    move-result v1

    int-to-float v1, v1

    sub-float/2addr v0, v1

    iget v1, p0, Lbr/com/itau/widgets/hintview/Hint;->mMargin:F

    sub-float/2addr v0, v1

    iput v0, v3, Landroid/graphics/PointF;->y:F

    .line 369
    goto :goto_aa

    .line 371
    :sswitch_75
    iget v0, v5, Landroid/graphics/PointF;->x:F

    iget-object v1, p0, Lbr/com/itau/widgets/hintview/Hint;->mContentView:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getWidth()I

    move-result v1

    int-to-float v1, v1

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    sub-float/2addr v0, v1

    iput v0, v3, Landroid/graphics/PointF;->x:F

    .line 372
    iget v0, v4, Landroid/graphics/RectF;->bottom:F

    iget v1, p0, Lbr/com/itau/widgets/hintview/Hint;->mMargin:F

    add-float/2addr v0, v1

    iput v0, v3, Landroid/graphics/PointF;->y:F

    .line 373
    goto :goto_aa

    .line 375
    :sswitch_8c
    iget v0, v5, Landroid/graphics/PointF;->x:F

    iget-object v1, p0, Lbr/com/itau/widgets/hintview/Hint;->mContentView:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getWidth()I

    move-result v1

    int-to-float v1, v1

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    sub-float/2addr v0, v1

    iput v0, v3, Landroid/graphics/PointF;->x:F

    .line 376
    iget v0, v4, Landroid/graphics/RectF;->top:F

    iget-object v1, p0, Lbr/com/itau/widgets/hintview/Hint;->mContentView:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getHeight()I

    move-result v1

    int-to-float v1, v1

    sub-float/2addr v0, v1

    iget v1, p0, Lbr/com/itau/widgets/hintview/Hint;->mMargin:F

    sub-float/2addr v0, v1

    iput v0, v3, Landroid/graphics/PointF;->y:F

    .line 380
    :goto_aa
    return-object v3

    nop

    :sswitch_data_ac
    .sparse-switch
        0x7 -> :sswitch_8c
        0x30 -> :sswitch_56
        0x50 -> :sswitch_75
        0x800003 -> :sswitch_1f
        0x800005 -> :sswitch_3f
    .end sparse-switch
.end method

.method private getContentView(Lbr/com/itau/widgets/hintview/Hint$Builder;)Landroid/view/View;
    .registers 16
    .param p1, "builder"    # Lbr/com/itau/widgets/hintview/Hint$Builder;

    .line 185
    new-instance v5, Landroid/graphics/drawable/GradientDrawable;

    invoke-direct {v5}, Landroid/graphics/drawable/GradientDrawable;-><init>()V

    .line 186
    .local v5, "drawable":Landroid/graphics/drawable/GradientDrawable;
    # getter for: Lbr/com/itau/widgets/hintview/Hint$Builder;->mBackgroundColor:I
    invoke-static {p1}, Lbr/com/itau/widgets/hintview/Hint$Builder;->access$2400(Lbr/com/itau/widgets/hintview/Hint$Builder;)I

    move-result v0

    invoke-virtual {v5, v0}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    .line 187
    # getter for: Lbr/com/itau/widgets/hintview/Hint$Builder;->mCornerRadius:F
    invoke-static {p1}, Lbr/com/itau/widgets/hintview/Hint$Builder;->access$2500(Lbr/com/itau/widgets/hintview/Hint$Builder;)F

    move-result v0

    invoke-virtual {v5, v0}, Landroid/graphics/drawable/GradientDrawable;->setCornerRadius(F)V

    .line 189
    # getter for: Lbr/com/itau/widgets/hintview/Hint$Builder;->mPadding:F
    invoke-static {p1}, Lbr/com/itau/widgets/hintview/Hint$Builder;->access$2600(Lbr/com/itau/widgets/hintview/Hint$Builder;)F

    move-result v0

    float-to-int v6, v0

    .line 190
    .local v6, "padding":I
    # getter for: Lbr/com/itau/widgets/hintview/Hint$Builder;->mPaddingLeft:F
    invoke-static {p1}, Lbr/com/itau/widgets/hintview/Hint$Builder;->access$2700(Lbr/com/itau/widgets/hintview/Hint$Builder;)F

    move-result v0

    float-to-int v7, v0

    .line 191
    .local v7, "paddingL":I
    # getter for: Lbr/com/itau/widgets/hintview/Hint$Builder;->mPaddingRight:F
    invoke-static {p1}, Lbr/com/itau/widgets/hintview/Hint$Builder;->access$2800(Lbr/com/itau/widgets/hintview/Hint$Builder;)F

    move-result v0

    float-to-int v8, v0

    .line 192
    .local v8, "paddingR":I
    # getter for: Lbr/com/itau/widgets/hintview/Hint$Builder;->mPaddingTop:F
    invoke-static {p1}, Lbr/com/itau/widgets/hintview/Hint$Builder;->access$2900(Lbr/com/itau/widgets/hintview/Hint$Builder;)F

    move-result v0

    float-to-int v9, v0

    .line 193
    .local v9, "paddingT":I
    # getter for: Lbr/com/itau/widgets/hintview/Hint$Builder;->mPaddingBottom:F
    invoke-static {p1}, Lbr/com/itau/widgets/hintview/Hint$Builder;->access$3000(Lbr/com/itau/widgets/hintview/Hint$Builder;)F

    move-result v0

    float-to-int v10, v0

    .line 195
    .local v10, "paddingB":I
    new-instance v11, Landroid/widget/TextView;

    # getter for: Lbr/com/itau/widgets/hintview/Hint$Builder;->mContext:Landroid/content/Context;
    invoke-static {p1}, Lbr/com/itau/widgets/hintview/Hint$Builder;->access$2100(Lbr/com/itau/widgets/hintview/Hint$Builder;)Landroid/content/Context;

    move-result-object v0

    invoke-direct {v11, v0}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 197
    .local v11, "textView":Landroid/widget/TextView;
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x17

    if-lt v0, v1, :cond_42

    .line 198
    # getter for: Lbr/com/itau/widgets/hintview/Hint$Builder;->mTextAppearance:I
    invoke-static {p1}, Lbr/com/itau/widgets/hintview/Hint$Builder;->access$3100(Lbr/com/itau/widgets/hintview/Hint$Builder;)I

    move-result v0

    invoke-virtual {v11, v0}, Landroid/widget/TextView;->setTextAppearance(I)V

    .line 200
    :cond_42
    # getter for: Lbr/com/itau/widgets/hintview/Hint$Builder;->mText:Ljava/lang/CharSequence;
    invoke-static {p1}, Lbr/com/itau/widgets/hintview/Hint$Builder;->access$3200(Lbr/com/itau/widgets/hintview/Hint$Builder;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v11, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 202
    const/4 v0, -0x1

    if-eq v7, v0, :cond_55

    const/4 v0, -0x1

    if-eq v8, v0, :cond_55

    const/4 v0, -0x1

    if-eq v9, v0, :cond_55

    const/4 v0, -0x1

    if-ne v10, v0, :cond_59

    .line 203
    :cond_55
    invoke-virtual {v11, v6, v6, v6, v6}, Landroid/widget/TextView;->setPadding(IIII)V

    goto :goto_5c

    .line 205
    :cond_59
    invoke-virtual {v11, v7, v9, v8, v10}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 208
    :goto_5c
    # getter for: Lbr/com/itau/widgets/hintview/Hint$Builder;->mLineSpacingExtra:F
    invoke-static {p1}, Lbr/com/itau/widgets/hintview/Hint$Builder;->access$3300(Lbr/com/itau/widgets/hintview/Hint$Builder;)F

    move-result v0

    # getter for: Lbr/com/itau/widgets/hintview/Hint$Builder;->mLineSpacingMultiplier:F
    invoke-static {p1}, Lbr/com/itau/widgets/hintview/Hint$Builder;->access$3400(Lbr/com/itau/widgets/hintview/Hint$Builder;)F

    move-result v1

    invoke-virtual {v11, v0, v1}, Landroid/widget/TextView;->setLineSpacing(FF)V

    .line 209
    # getter for: Lbr/com/itau/widgets/hintview/Hint$Builder;->mTypeface:Landroid/graphics/Typeface;
    invoke-static {p1}, Lbr/com/itau/widgets/hintview/Hint$Builder;->access$3500(Lbr/com/itau/widgets/hintview/Hint$Builder;)Landroid/graphics/Typeface;

    move-result-object v0

    # getter for: Lbr/com/itau/widgets/hintview/Hint$Builder;->mTextStyle:I
    invoke-static {p1}, Lbr/com/itau/widgets/hintview/Hint$Builder;->access$3600(Lbr/com/itau/widgets/hintview/Hint$Builder;)I

    move-result v1

    invoke-virtual {v11, v0, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 211
    # getter for: Lbr/com/itau/widgets/hintview/Hint$Builder;->mTextSize:F
    invoke-static {p1}, Lbr/com/itau/widgets/hintview/Hint$Builder;->access$3700(Lbr/com/itau/widgets/hintview/Hint$Builder;)F

    move-result v0

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_83

    .line 212
    # getter for: Lbr/com/itau/widgets/hintview/Hint$Builder;->mTextSize:F
    invoke-static {p1}, Lbr/com/itau/widgets/hintview/Hint$Builder;->access$3700(Lbr/com/itau/widgets/hintview/Hint$Builder;)F

    move-result v0

    const/4 v1, 0x0

    invoke-virtual {v11, v1, v0}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 214
    :cond_83
    # getter for: Lbr/com/itau/widgets/hintview/Hint$Builder;->mTextColor:Landroid/content/res/ColorStateList;
    invoke-static {p1}, Lbr/com/itau/widgets/hintview/Hint$Builder;->access$3800(Lbr/com/itau/widgets/hintview/Hint$Builder;)Landroid/content/res/ColorStateList;

    move-result-object v0

    if-eqz v0, :cond_90

    .line 215
    # getter for: Lbr/com/itau/widgets/hintview/Hint$Builder;->mTextColor:Landroid/content/res/ColorStateList;
    invoke-static {p1}, Lbr/com/itau/widgets/hintview/Hint$Builder;->access$3800(Lbr/com/itau/widgets/hintview/Hint$Builder;)Landroid/content/res/ColorStateList;

    move-result-object v0

    invoke-virtual {v11, v0}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 218
    :cond_90
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_9a

    .line 219
    invoke-virtual {v11, v5}, Landroid/widget/TextView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_9d

    .line 222
    :cond_9a
    invoke-virtual {v11, v5}, Landroid/widget/TextView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 225
    :goto_9d
    new-instance v12, Landroid/widget/LinearLayout$LayoutParams;

    iget v0, p0, Lbr/com/itau/widgets/hintview/Hint;->mDefaultWidth:I

    const/4 v1, -0x2

    const/4 v2, 0x0

    invoke-direct {v12, v0, v1, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    .line 226
    .local v12, "textViewParams":Landroid/widget/LinearLayout$LayoutParams;
    const/16 v0, 0x11

    iput v0, v12, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 227
    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 229
    new-instance v0, Landroid/widget/ImageView;

    # getter for: Lbr/com/itau/widgets/hintview/Hint$Builder;->mContext:Landroid/content/Context;
    invoke-static {p1}, Lbr/com/itau/widgets/hintview/Hint$Builder;->access$2100(Lbr/com/itau/widgets/hintview/Hint$Builder;)Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lbr/com/itau/widgets/hintview/Hint;->mArrowView:Landroid/widget/ImageView;

    .line 230
    iget-object v0, p0, Lbr/com/itau/widgets/hintview/Hint;->mArrowView:Landroid/widget/ImageView;

    # getter for: Lbr/com/itau/widgets/hintview/Hint$Builder;->mArrowDrawable:Landroid/graphics/drawable/Drawable;
    invoke-static {p1}, Lbr/com/itau/widgets/hintview/Hint$Builder;->access$3900(Lbr/com/itau/widgets/hintview/Hint$Builder;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 233
    iget v0, p0, Lbr/com/itau/widgets/hintview/Hint;->mGravity:I

    const/16 v1, 0x30

    if-eq v0, v1, :cond_d2

    iget v0, p0, Lbr/com/itau/widgets/hintview/Hint;->mGravity:I

    const/16 v1, 0x50

    if-eq v0, v1, :cond_d2

    iget v0, p0, Lbr/com/itau/widgets/hintview/Hint;->mGravity:I

    const/4 v1, 0x7

    if-ne v0, v1, :cond_e3

    .line 234
    :cond_d2
    new-instance v13, Landroid/widget/LinearLayout$LayoutParams;

    # getter for: Lbr/com/itau/widgets/hintview/Hint$Builder;->mArrowWidth:F
    invoke-static {p1}, Lbr/com/itau/widgets/hintview/Hint$Builder;->access$4000(Lbr/com/itau/widgets/hintview/Hint$Builder;)F

    move-result v0

    float-to-int v0, v0

    # getter for: Lbr/com/itau/widgets/hintview/Hint$Builder;->mArrowHeight:F
    invoke-static {p1}, Lbr/com/itau/widgets/hintview/Hint$Builder;->access$4100(Lbr/com/itau/widgets/hintview/Hint$Builder;)F

    move-result v1

    float-to-int v1, v1

    const/4 v2, 0x0

    invoke-direct {v13, v0, v1, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    .local v13, "arrowLayoutParams":Landroid/widget/LinearLayout$LayoutParams;
    goto :goto_f3

    .line 236
    .end local v13    # "arrowLayoutParams":Landroid/widget/LinearLayout$LayoutParams;
    :cond_e3
    new-instance v13, Landroid/widget/LinearLayout$LayoutParams;

    # getter for: Lbr/com/itau/widgets/hintview/Hint$Builder;->mArrowHeight:F
    invoke-static {p1}, Lbr/com/itau/widgets/hintview/Hint$Builder;->access$4100(Lbr/com/itau/widgets/hintview/Hint$Builder;)F

    move-result v0

    float-to-int v0, v0

    # getter for: Lbr/com/itau/widgets/hintview/Hint$Builder;->mArrowWidth:F
    invoke-static {p1}, Lbr/com/itau/widgets/hintview/Hint$Builder;->access$4000(Lbr/com/itau/widgets/hintview/Hint$Builder;)F

    move-result v1

    float-to-int v1, v1

    const/4 v2, 0x0

    invoke-direct {v13, v0, v1, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    .line 238
    .local v13, "arrowLayoutParams":Landroid/widget/LinearLayout$LayoutParams;
    :goto_f3
    const/16 v0, 0x11

    iput v0, v13, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 239
    iget-object v0, p0, Lbr/com/itau/widgets/hintview/Hint;->mArrowView:Landroid/widget/ImageView;

    invoke-virtual {v0, v13}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 241
    new-instance v0, Landroid/widget/LinearLayout;

    # getter for: Lbr/com/itau/widgets/hintview/Hint$Builder;->mContext:Landroid/content/Context;
    invoke-static {p1}, Lbr/com/itau/widgets/hintview/Hint$Builder;->access$2100(Lbr/com/itau/widgets/hintview/Hint$Builder;)Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lbr/com/itau/widgets/hintview/Hint;->mContentView:Landroid/widget/LinearLayout;

    .line 243
    iget-object v0, p0, Lbr/com/itau/widgets/hintview/Hint;->mContentView:Landroid/widget/LinearLayout;

    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    iget v2, p0, Lbr/com/itau/widgets/hintview/Hint;->mDefaultWidth:I

    const/4 v3, -0x2

    invoke-direct {v1, v2, v3}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 244
    iget-object v0, p0, Lbr/com/itau/widgets/hintview/Hint;->mContentView:Landroid/widget/LinearLayout;

    iget v1, p0, Lbr/com/itau/widgets/hintview/Hint;->mGravity:I

    const v2, 0x800003

    if-eq v1, v2, :cond_124

    iget v1, p0, Lbr/com/itau/widgets/hintview/Hint;->mGravity:I

    const v2, 0x800005

    if-ne v1, v2, :cond_126

    :cond_124
    const/4 v1, 0x0

    goto :goto_127

    :cond_126
    const/4 v1, 0x1

    :goto_127
    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 246
    const/high16 v0, 0x40a00000    # 5.0f

    invoke-static {v0}, Lbr/com/itau/widgets/hintview/Util;->dpToPx(F)F

    move-result v0

    float-to-int v6, v0

    .line 248
    iget v0, p0, Lbr/com/itau/widgets/hintview/Hint;->mGravity:I

    sparse-switch v0, :sswitch_data_1b6

    goto :goto_166

    .line 250
    :sswitch_137
    iget-object v0, p0, Lbr/com/itau/widgets/hintview/Hint;->mContentView:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v6, v3}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    .line 251
    goto :goto_166

    .line 254
    :sswitch_140
    iget-object v0, p0, Lbr/com/itau/widgets/hintview/Hint;->mContentView:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v6, v1, v6, v2}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    .line 255
    goto :goto_166

    .line 257
    :sswitch_148
    iget-object v0, p0, Lbr/com/itau/widgets/hintview/Hint;->mContentView:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v6, v1, v2, v3}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    .line 258
    goto :goto_166

    .line 260
    :sswitch_151
    iget-object v0, p0, Lbr/com/itau/widgets/hintview/Hint;->mContentView:Landroid/widget/LinearLayout;

    const/high16 v1, 0x41800000    # 16.0f

    invoke-static {v1}, Lbr/com/itau/widgets/hintview/Util;->dpToPx(F)F

    move-result v1

    float-to-int v1, v1

    const/high16 v2, 0x41800000    # 16.0f

    invoke-static {v2}, Lbr/com/itau/widgets/hintview/Util;->dpToPx(F)F

    move-result v2

    float-to-int v2, v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v3, v2, v4}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    .line 265
    :goto_166
    iget v0, p0, Lbr/com/itau/widgets/hintview/Hint;->mGravity:I

    const/16 v1, 0x30

    if-eq v0, v1, :cond_178

    iget v0, p0, Lbr/com/itau/widgets/hintview/Hint;->mGravity:I

    const v1, 0x800003

    if-eq v0, v1, :cond_178

    iget v0, p0, Lbr/com/itau/widgets/hintview/Hint;->mGravity:I

    const/4 v1, 0x7

    if-ne v0, v1, :cond_185

    .line 266
    :cond_178
    iget-object v0, p0, Lbr/com/itau/widgets/hintview/Hint;->mContentView:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v11}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 267
    iget-object v0, p0, Lbr/com/itau/widgets/hintview/Hint;->mContentView:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lbr/com/itau/widgets/hintview/Hint;->mArrowView:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto :goto_191

    .line 269
    :cond_185
    iget-object v0, p0, Lbr/com/itau/widgets/hintview/Hint;->mContentView:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lbr/com/itau/widgets/hintview/Hint;->mArrowView:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 270
    iget-object v0, p0, Lbr/com/itau/widgets/hintview/Hint;->mContentView:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v11}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 273
    :goto_191
    iget-object v0, p0, Lbr/com/itau/widgets/hintview/Hint;->mContentView:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lbr/com/itau/widgets/hintview/Hint;->mClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 274
    iget-object v0, p0, Lbr/com/itau/widgets/hintview/Hint;->mContentView:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lbr/com/itau/widgets/hintview/Hint;->mLongClickListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 276
    # getter for: Lbr/com/itau/widgets/hintview/Hint$Builder;->isCancelable:Z
    invoke-static {p1}, Lbr/com/itau/widgets/hintview/Hint$Builder;->access$1200(Lbr/com/itau/widgets/hintview/Hint$Builder;)Z

    move-result v0

    if-nez v0, :cond_1ab

    # getter for: Lbr/com/itau/widgets/hintview/Hint$Builder;->isDismissOnClick:Z
    invoke-static {p1}, Lbr/com/itau/widgets/hintview/Hint$Builder;->access$1300(Lbr/com/itau/widgets/hintview/Hint$Builder;)Z

    move-result v0

    if-eqz v0, :cond_1b2

    .line 277
    :cond_1ab
    iget-object v0, p0, Lbr/com/itau/widgets/hintview/Hint;->mContentView:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lbr/com/itau/widgets/hintview/Hint;->mTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 279
    :cond_1b2
    iget-object v0, p0, Lbr/com/itau/widgets/hintview/Hint;->mContentView:Landroid/widget/LinearLayout;

    return-object v0

    nop

    :sswitch_data_1b6
    .sparse-switch
        0x7 -> :sswitch_151
        0x30 -> :sswitch_140
        0x50 -> :sswitch_140
        0x800003 -> :sswitch_137
        0x800005 -> :sswitch_148
    .end sparse-switch
.end method


# virtual methods
.method public dismiss()V
    .registers 2

    .line 321
    iget-object v0, p0, Lbr/com/itau/widgets/hintview/Hint;->mPopupWindow:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V

    .line 322
    return-void
.end method

.method public isShowing()Z
    .registers 2

    .line 288
    iget-object v0, p0, Lbr/com/itau/widgets/hintview/Hint;->mPopupWindow:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v0

    return v0
.end method

.method public setOnClickListener(Lbr/com/itau/widgets/hintview/OnClickListener;)V
    .registers 2
    .param p1, "listener"    # Lbr/com/itau/widgets/hintview/OnClickListener;

    .line 330
    iput-object p1, p0, Lbr/com/itau/widgets/hintview/Hint;->mOnClickListener:Lbr/com/itau/widgets/hintview/OnClickListener;

    .line 331
    return-void
.end method

.method public setOnDismissListener(Lbr/com/itau/widgets/hintview/OnDismissListener;)V
    .registers 2
    .param p1, "listener"    # Lbr/com/itau/widgets/hintview/OnDismissListener;

    .line 348
    iput-object p1, p0, Lbr/com/itau/widgets/hintview/Hint;->mOnDismissListener:Lbr/com/itau/widgets/hintview/OnDismissListener;

    .line 349
    return-void
.end method

.method public setOnLongClickListener(Lbr/com/itau/widgets/hintview/OnLongClickListener;)V
    .registers 2
    .param p1, "listener"    # Lbr/com/itau/widgets/hintview/OnLongClickListener;

    .line 339
    iput-object p1, p0, Lbr/com/itau/widgets/hintview/Hint;->mOnLongClickListener:Lbr/com/itau/widgets/hintview/OnLongClickListener;

    .line 340
    return-void
.end method

.method public show()V
    .registers 5

    .line 298
    invoke-virtual {p0}, Lbr/com/itau/widgets/hintview/Hint;->isShowing()Z

    move-result v0

    if-nez v0, :cond_24

    .line 299
    iget-object v0, p0, Lbr/com/itau/widgets/hintview/Hint;->mContentView:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    iget-object v1, p0, Lbr/com/itau/widgets/hintview/Hint;->mLocationLayoutListener:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 301
    iget-object v0, p0, Lbr/com/itau/widgets/hintview/Hint;->mAnchorView:Landroid/view/View;

    iget-object v1, p0, Lbr/com/itau/widgets/hintview/Hint;->mOnAttachStateChangeListener:Landroid/view/View$OnAttachStateChangeListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->addOnAttachStateChangeListener(Landroid/view/View$OnAttachStateChangeListener;)V

    .line 302
    iget-object v0, p0, Lbr/com/itau/widgets/hintview/Hint;->mAnchorView:Landroid/view/View;

    new-instance v1, Lbr/com/itau/widgets/hintview/Hint$8;

    invoke-direct {v1, p0}, Lbr/com/itau/widgets/hintview/Hint$8;-><init>(Lbr/com/itau/widgets/hintview/Hint;)V

    const-wide/16 v2, 0x2bc

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 311
    :cond_24
    return-void
.end method

.class public final Landroid/support/v4/os/CancellationSignal;
.super Ljava/lang/Object;
.source "CancellationSignal.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/support/v4/os/CancellationSignal$OnCancelListener;
    }
.end annotation


# instance fields
.field private mCancelInProgress:Z

.field private mCancellationSignalObj:Ljava/lang/Object;

.field private mIsCanceled:Z

.field private mOnCancelListener:Landroid/support/v4/os/CancellationSignal$OnCancelListener;


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    return-void
.end method


# virtual methods
.method public cancel()V
    .registers 10

    .line 67
    move-object v3, p0

    monitor-enter v3

    .line 68
    :try_start_2
    iget-boolean v0, p0, Landroid/support/v4/os/CancellationSignal;->mIsCanceled:Z
    :try_end_4
    .catchall {:try_start_2 .. :try_end_4} :catchall_14

    if-eqz v0, :cond_8

    .line 69
    monitor-exit v3

    return-void

    .line 71
    :cond_8
    const/4 v0, 0x1

    :try_start_9
    iput-boolean v0, p0, Landroid/support/v4/os/CancellationSignal;->mIsCanceled:Z

    .line 72
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v4/os/CancellationSignal;->mCancelInProgress:Z

    .line 73
    iget-object v1, p0, Landroid/support/v4/os/CancellationSignal;->mOnCancelListener:Landroid/support/v4/os/CancellationSignal$OnCancelListener;

    .line 74
    .local v1, "listener":Landroid/support/v4/os/CancellationSignal$OnCancelListener;
    iget-object v2, p0, Landroid/support/v4/os/CancellationSignal;->mCancellationSignalObj:Ljava/lang/Object;
    :try_end_12
    .catchall {:try_start_9 .. :try_end_12} :catchall_14

    .line 75
    .local v2, "obj":Ljava/lang/Object;
    monitor-exit v3

    goto :goto_17

    :catchall_14
    move-exception v4

    monitor-exit v3

    throw v4

    .line 78
    :goto_17
    if-eqz v1, :cond_1c

    .line 79
    :try_start_19
    invoke-interface {v1}, Landroid/support/v4/os/CancellationSignal$OnCancelListener;->onCancel()V

    .line 81
    :cond_1c
    if-eqz v2, :cond_21

    .line 82
    invoke-static {v2}, Landroid/support/v4/os/CancellationSignalCompatJellybean;->cancel(Ljava/lang/Object;)V
    :try_end_21
    .catchall {:try_start_19 .. :try_end_21} :catchall_2f

    .line 85
    :cond_21
    move-object v3, p0

    monitor-enter v3

    .line 86
    const/4 v0, 0x0

    :try_start_24
    iput-boolean v0, p0, Landroid/support/v4/os/CancellationSignal;->mCancelInProgress:Z

    .line 87
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V
    :try_end_29
    .catchall {:try_start_24 .. :try_end_29} :catchall_2b

    .line 88
    monitor-exit v3

    goto :goto_2e

    :catchall_2b
    move-exception v5

    monitor-exit v3

    throw v5

    .line 89
    :goto_2e
    goto :goto_3e

    .line 85
    :catchall_2f
    move-exception v6

    move-object v7, p0

    monitor-enter v7

    .line 86
    const/4 v0, 0x0

    :try_start_33
    iput-boolean v0, p0, Landroid/support/v4/os/CancellationSignal;->mCancelInProgress:Z

    .line 87
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V
    :try_end_38
    .catchall {:try_start_33 .. :try_end_38} :catchall_3a

    .line 88
    monitor-exit v7

    goto :goto_3d

    :catchall_3a
    move-exception v8

    monitor-exit v7

    throw v8

    :goto_3d
    throw v6

    .line 90
    :goto_3e
    return-void
.end method

.method public getCancellationSignalObject()Ljava/lang/Object;
    .registers 5

    .line 135
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-ge v0, v1, :cond_8

    .line 136
    const/4 v0, 0x0

    return-object v0

    .line 138
    :cond_8
    move-object v2, p0

    monitor-enter v2

    .line 139
    :try_start_a
    iget-object v0, p0, Landroid/support/v4/os/CancellationSignal;->mCancellationSignalObj:Ljava/lang/Object;

    if-nez v0, :cond_1d

    .line 140
    invoke-static {}, Landroid/support/v4/os/CancellationSignalCompatJellybean;->create()Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v4/os/CancellationSignal;->mCancellationSignalObj:Ljava/lang/Object;

    .line 141
    iget-boolean v0, p0, Landroid/support/v4/os/CancellationSignal;->mIsCanceled:Z

    if-eqz v0, :cond_1d

    .line 142
    iget-object v0, p0, Landroid/support/v4/os/CancellationSignal;->mCancellationSignalObj:Ljava/lang/Object;

    invoke-static {v0}, Landroid/support/v4/os/CancellationSignalCompatJellybean;->cancel(Ljava/lang/Object;)V

    .line 145
    :cond_1d
    iget-object v0, p0, Landroid/support/v4/os/CancellationSignal;->mCancellationSignalObj:Ljava/lang/Object;
    :try_end_1f
    .catchall {:try_start_a .. :try_end_1f} :catchall_21

    monitor-exit v2

    return-object v0

    .line 146
    :catchall_21
    move-exception v3

    monitor-exit v2

    throw v3
.end method

.class Lcom/google/android/gms/measurement/zzb$1;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/gms/measurement/zzb;->zzb(Lcom/google/android/gms/measurement/zzc;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;Ljava/util/Comparator<Lcom/google/android/gms/measurement/zze;>;"
    }
.end annotation


# instance fields
.field final synthetic zzaUh:Lcom/google/android/gms/measurement/zzb;


# direct methods
.method constructor <init>(Lcom/google/android/gms/measurement/zzb;)V
    .registers 2

    iput-object p1, p0, Lcom/google/android/gms/measurement/zzb$1;->zzaUh:Lcom/google/android/gms/measurement/zzb;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .registers 5

    move-object v0, p1

    check-cast v0, Lcom/google/android/gms/measurement/zze;

    move-object v1, p2

    check-cast v1, Lcom/google/android/gms/measurement/zze;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/measurement/zzb$1;->zza(Lcom/google/android/gms/measurement/zze;Lcom/google/android/gms/measurement/zze;)I

    move-result v0

    return v0
.end method

.method public zza(Lcom/google/android/gms/measurement/zze;Lcom/google/android/gms/measurement/zze;)I
    .registers 6

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

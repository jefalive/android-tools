.class public Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;
.super Ljava/lang/Object;
.source "ContatoRecargaVO.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field apelido:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "apelido"
    .end annotation
.end field

.field ddd:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "ddd"
    .end annotation
.end field

.field ddi:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "ddi"
    .end annotation
.end field

.field estadoOperadora:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "estado_operadora"
    .end annotation
.end field

.field idContato:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "id_contato"
    .end annotation
.end field

.field nomeOperadora:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "nome_operadora"
    .end annotation
.end field

.field private positionValorOperadora:I

.field telefone:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "telefone"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getApelido()Ljava/lang/String;
    .registers 2

    .line 28
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;->apelido:Ljava/lang/String;

    return-object v0
.end method

.method public getDdd()Ljava/lang/String;
    .registers 2

    .line 36
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;->ddd:Ljava/lang/String;

    invoke-static {v0}, Lcom/itau/empresas/ui/util/ViewUtils;->removaPaddingsZero(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getEstadoOperadora()Ljava/lang/String;
    .registers 2

    .line 44
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;->estadoOperadora:Ljava/lang/String;

    return-object v0
.end method

.method public getIdContato()Ljava/lang/String;
    .registers 2

    .line 52
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;->idContato:Ljava/lang/String;

    return-object v0
.end method

.method public getNomeOperadora()Ljava/lang/String;
    .registers 2

    .line 57
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;->nomeOperadora:Ljava/lang/String;

    return-object v0
.end method

.method public getPositionValorOperadora()I
    .registers 2

    .line 73
    iget v0, p0, Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;->positionValorOperadora:I

    return v0
.end method

.method public getTelefone()Ljava/lang/String;
    .registers 2

    .line 65
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;->telefone:Ljava/lang/String;

    invoke-static {v0}, Lcom/itau/empresas/ui/util/ViewUtils;->removaPaddingsZero(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public setApelido(Ljava/lang/String;)V
    .registers 2
    .param p1, "apelido"    # Ljava/lang/String;

    .line 32
    iput-object p1, p0, Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;->apelido:Ljava/lang/String;

    .line 33
    return-void
.end method

.method public setDdd(Ljava/lang/String;)V
    .registers 3
    .param p1, "ddd"    # Ljava/lang/String;

    .line 40
    invoke-static {p1}, Lcom/itau/empresas/ui/util/ViewUtils;->removaPaddingsZero(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;->ddd:Ljava/lang/String;

    .line 41
    return-void
.end method

.method public setEstadoOperadora(Ljava/lang/String;)V
    .registers 2
    .param p1, "estadoOperadora"    # Ljava/lang/String;

    .line 48
    iput-object p1, p0, Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;->estadoOperadora:Ljava/lang/String;

    .line 49
    return-void
.end method

.method public setNomeOperadora(Ljava/lang/String;)V
    .registers 2
    .param p1, "nomeOperadora"    # Ljava/lang/String;

    .line 61
    iput-object p1, p0, Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;->nomeOperadora:Ljava/lang/String;

    .line 62
    return-void
.end method

.method public setTelefone(Ljava/lang/String;)V
    .registers 3
    .param p1, "telefone"    # Ljava/lang/String;

    .line 69
    invoke-static {p1}, Lcom/itau/empresas/ui/util/ViewUtils;->removaPaddingsZero(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;->telefone:Ljava/lang/String;

    .line 70
    return-void
.end method

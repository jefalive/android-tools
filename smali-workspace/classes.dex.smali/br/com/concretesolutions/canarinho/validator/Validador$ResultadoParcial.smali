.class public Lbr/com/concretesolutions/canarinho/validator/Validador$ResultadoParcial;
.super Ljava/lang/Object;
.source "Validador.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lbr/com/concretesolutions/canarinho/validator/Validador;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ResultadoParcial"
.end annotation


# instance fields
.field private mensagem:Ljava/lang/String;

.field private parcialmenteValido:Z

.field private valido:Z


# direct methods
.method public constructor <init>()V
    .registers 2

    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 65
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbr/com/concretesolutions/canarinho/validator/Validador$ResultadoParcial;->parcialmenteValido:Z

    return-void
.end method


# virtual methods
.method public getMensagem()Ljava/lang/String;
    .registers 2

    .line 77
    iget-object v0, p0, Lbr/com/concretesolutions/canarinho/validator/Validador$ResultadoParcial;->mensagem:Ljava/lang/String;

    return-object v0
.end method

.method public isParcialmenteValido()Z
    .registers 2

    .line 73
    iget-boolean v0, p0, Lbr/com/concretesolutions/canarinho/validator/Validador$ResultadoParcial;->parcialmenteValido:Z

    return v0
.end method

.method public isValido()Z
    .registers 2

    .line 69
    iget-boolean v0, p0, Lbr/com/concretesolutions/canarinho/validator/Validador$ResultadoParcial;->valido:Z

    return v0
.end method

.method public mensagem(Ljava/lang/String;)Lbr/com/concretesolutions/canarinho/validator/Validador$ResultadoParcial;
    .registers 2
    .param p1, "mensagem"    # Ljava/lang/String;

    .line 109
    iput-object p1, p0, Lbr/com/concretesolutions/canarinho/validator/Validador$ResultadoParcial;->mensagem:Ljava/lang/String;

    .line 110
    return-object p0
.end method

.method public parcialmenteValido(Z)Lbr/com/concretesolutions/canarinho/validator/Validador$ResultadoParcial;
    .registers 2
    .param p1, "parcialmenteValido"    # Z

    .line 98
    iput-boolean p1, p0, Lbr/com/concretesolutions/canarinho/validator/Validador$ResultadoParcial;->parcialmenteValido:Z

    .line 99
    return-object p0
.end method

.method public totalmenteValido(Z)Lbr/com/concretesolutions/canarinho/validator/Validador$ResultadoParcial;
    .registers 2
    .param p1, "valido"    # Z

    .line 87
    iput-boolean p1, p0, Lbr/com/concretesolutions/canarinho/validator/Validador$ResultadoParcial;->valido:Z

    .line 88
    return-object p0
.end method

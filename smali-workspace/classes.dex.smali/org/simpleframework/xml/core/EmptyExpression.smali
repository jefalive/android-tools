.class Lorg/simpleframework/xml/core/EmptyExpression;
.super Ljava/lang/Object;
.source "EmptyExpression.java"

# interfaces
.implements Lorg/simpleframework/xml/core/Expression;


# instance fields
.field private final list:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Ljava/lang/String;>;"
        }
    .end annotation
.end field

.field private final style:Lorg/simpleframework/xml/stream/Style;


# direct methods
.method public constructor <init>(Lorg/simpleframework/xml/stream/Format;)V
    .registers 3
    .param p1, "format"    # Lorg/simpleframework/xml/stream/Format;

    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lorg/simpleframework/xml/core/EmptyExpression;->list:Ljava/util/List;

    .line 59
    invoke-virtual {p1}, Lorg/simpleframework/xml/stream/Format;->getStyle()Lorg/simpleframework/xml/stream/Style;

    move-result-object v0

    iput-object v0, p0, Lorg/simpleframework/xml/core/EmptyExpression;->style:Lorg/simpleframework/xml/stream/Style;

    .line 60
    return-void
.end method


# virtual methods
.method public getAttribute(Ljava/lang/String;)Ljava/lang/String;
    .registers 3
    .param p1, "name"    # Ljava/lang/String;

    .line 159
    iget-object v0, p0, Lorg/simpleframework/xml/core/EmptyExpression;->style:Lorg/simpleframework/xml/stream/Style;

    invoke-interface {v0, p1}, Lorg/simpleframework/xml/stream/Style;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getElement(Ljava/lang/String;)Ljava/lang/String;
    .registers 3
    .param p1, "name"    # Ljava/lang/String;

    .line 145
    iget-object v0, p0, Lorg/simpleframework/xml/core/EmptyExpression;->style:Lorg/simpleframework/xml/stream/Style;

    invoke-interface {v0, p1}, Lorg/simpleframework/xml/stream/Style;->getElement(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getFirst()Ljava/lang/String;
    .registers 2

    .line 107
    const/4 v0, 0x0

    return-object v0
.end method

.method public getIndex()I
    .registers 2

    .line 83
    const/4 v0, 0x0

    return v0
.end method

.method public getLast()Ljava/lang/String;
    .registers 2

    .line 119
    const/4 v0, 0x0

    return-object v0
.end method

.method public getPath()Ljava/lang/String;
    .registers 2

    .line 131
    const-string v0, ""

    return-object v0
.end method

.method public getPath(I)Lorg/simpleframework/xml/core/Expression;
    .registers 3
    .param p1, "from"    # I

    .line 174
    const/4 v0, 0x0

    return-object v0
.end method

.method public getPath(II)Lorg/simpleframework/xml/core/Expression;
    .registers 4
    .param p1, "from"    # I
    .param p2, "trim"    # I

    .line 190
    const/4 v0, 0x0

    return-object v0
.end method

.method public getPrefix()Ljava/lang/String;
    .registers 2

    .line 95
    const/4 v0, 0x0

    return-object v0
.end method

.method public isAttribute()Z
    .registers 2

    .line 202
    const/4 v0, 0x0

    return v0
.end method

.method public isEmpty()Z
    .registers 2

    .line 224
    const/4 v0, 0x1

    return v0
.end method

.method public isPath()Z
    .registers 2

    .line 213
    const/4 v0, 0x0

    return v0
.end method

.method public iterator()Ljava/util/Iterator;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/Iterator<Ljava/lang/String;>;"
        }
    .end annotation

    .line 71
    iget-object v0, p0, Lorg/simpleframework/xml/core/EmptyExpression;->list:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.class final Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingFunctions$28;
.super Ljava/lang/Object;
.source "Easing.java"

# interfaces
.implements Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/EasingFunction;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingFunctions;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .registers 1

    .line 696
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getInterpolation(F)F
    .registers 5
    .param p1, "input"    # F

    .line 710
    const/high16 v0, 0x3f000000    # 0.5f

    cmpg-float v0, p1, v0

    if-gez v0, :cond_13

    .line 712
    sget-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingFunctions;->EaseInBounce:Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/EasingFunction;

    const/high16 v1, 0x40000000    # 2.0f

    mul-float/2addr v1, p1

    invoke-interface {v0, v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/EasingFunction;->getInterpolation(F)F

    move-result v0

    const/high16 v1, 0x3f000000    # 0.5f

    mul-float/2addr v0, v1

    return v0

    .line 714
    :cond_13
    sget-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingFunctions;->EaseOutBounce:Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/EasingFunction;

    const/high16 v1, 0x40000000    # 2.0f

    mul-float/2addr v1, p1

    const/high16 v2, 0x3f800000    # 1.0f

    sub-float/2addr v1, v2

    invoke-interface {v0, v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/EasingFunction;->getInterpolation(F)F

    move-result v0

    const/high16 v1, 0x3f000000    # 0.5f

    mul-float/2addr v0, v1

    const/high16 v1, 0x3f000000    # 0.5f

    add-float/2addr v0, v1

    return v0
.end method

.class final synthetic Lbr/com/itau/sdk/android/core/endpoint/v;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private final a:Lbr/com/itau/sdk/android/core/model/VersionControlDTO;

.field private final b:Ljava/lang/String;

.field private final c:Landroid/content/DialogInterface$OnClickListener;

.field private final d:Ljava/lang/String;

.field private final e:Landroid/content/DialogInterface$OnClickListener;


# direct methods
.method private constructor <init>(Lbr/com/itau/sdk/android/core/model/VersionControlDTO;Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)V
    .registers 6

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lbr/com/itau/sdk/android/core/endpoint/v;->a:Lbr/com/itau/sdk/android/core/model/VersionControlDTO;

    iput-object p2, p0, Lbr/com/itau/sdk/android/core/endpoint/v;->b:Ljava/lang/String;

    iput-object p3, p0, Lbr/com/itau/sdk/android/core/endpoint/v;->c:Landroid/content/DialogInterface$OnClickListener;

    iput-object p4, p0, Lbr/com/itau/sdk/android/core/endpoint/v;->d:Ljava/lang/String;

    iput-object p5, p0, Lbr/com/itau/sdk/android/core/endpoint/v;->e:Landroid/content/DialogInterface$OnClickListener;

    return-void
.end method

.method public static a(Lbr/com/itau/sdk/android/core/model/VersionControlDTO;Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)Ljava/lang/Runnable;
    .registers 11

    new-instance v0, Lbr/com/itau/sdk/android/core/endpoint/v;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lbr/com/itau/sdk/android/core/endpoint/v;-><init>(Lbr/com/itau/sdk/android/core/model/VersionControlDTO;Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)V

    return-object v0
.end method


# virtual methods
.method public run()V
    .registers 6
    .annotation runtime Ljava/lang/invoke/LambdaForm$Hidden;
    .end annotation

    iget-object v0, p0, Lbr/com/itau/sdk/android/core/endpoint/v;->a:Lbr/com/itau/sdk/android/core/model/VersionControlDTO;

    iget-object v1, p0, Lbr/com/itau/sdk/android/core/endpoint/v;->b:Ljava/lang/String;

    iget-object v2, p0, Lbr/com/itau/sdk/android/core/endpoint/v;->c:Landroid/content/DialogInterface$OnClickListener;

    iget-object v3, p0, Lbr/com/itau/sdk/android/core/endpoint/v;->d:Ljava/lang/String;

    iget-object v4, p0, Lbr/com/itau/sdk/android/core/endpoint/v;->e:Landroid/content/DialogInterface$OnClickListener;

    invoke-static {v0, v1, v2, v3, v4}, Lbr/com/itau/sdk/android/core/endpoint/q;->a(Lbr/com/itau/sdk/android/core/model/VersionControlDTO;Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)V

    return-void
.end method

.class public final Lcom/google/zxing/client/android/c;
.super Landroid/os/Handler;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;

.field private final c:Lcom/google/zxing/client/android/g;

.field private d:Lcom/google/zxing/client/android/d;

.field private final e:Lcom/google/zxing/client/android/a/g;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    const-class v0, Lcom/google/zxing/client/android/c;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/zxing/client/android/c;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;Ljava/util/Collection;Ljava/util/Map;Ljava/lang/String;Lcom/google/zxing/client/android/a/g;)V
    .registers 12

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    iput-object p1, p0, Lcom/google/zxing/client/android/c;->b:Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;

    new-instance v0, Lcom/google/zxing/client/android/g;

    new-instance v5, Lcom/google/zxing/client/android/m;

    invoke-virtual {p1}, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->getViewfinderView()Lcom/google/zxing/client/android/ViewfinderView;

    move-result-object v1

    invoke-direct {v5, v1}, Lcom/google/zxing/client/android/m;-><init>(Lcom/google/zxing/client/android/ViewfinderView;)V

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/zxing/client/android/g;-><init>(Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;Ljava/util/Collection;Ljava/util/Map;Ljava/lang/String;Lcom/google/zxing/n;)V

    iput-object v0, p0, Lcom/google/zxing/client/android/c;->c:Lcom/google/zxing/client/android/g;

    iget-object v0, p0, Lcom/google/zxing/client/android/c;->c:Lcom/google/zxing/client/android/g;

    invoke-virtual {v0}, Lcom/google/zxing/client/android/g;->start()V

    sget-object v0, Lcom/google/zxing/client/android/d;->b:Lcom/google/zxing/client/android/d;

    iput-object v0, p0, Lcom/google/zxing/client/android/c;->d:Lcom/google/zxing/client/android/d;

    iput-object p5, p0, Lcom/google/zxing/client/android/c;->e:Lcom/google/zxing/client/android/a/g;

    invoke-virtual {p5}, Lcom/google/zxing/client/android/a/g;->d()V

    invoke-direct {p0}, Lcom/google/zxing/client/android/c;->b()V

    return-void
.end method

.method private b()V
    .registers 4

    iget-object v0, p0, Lcom/google/zxing/client/android/c;->d:Lcom/google/zxing/client/android/d;

    sget-object v1, Lcom/google/zxing/client/android/d;->b:Lcom/google/zxing/client/android/d;

    if-ne v0, v1, :cond_1c

    sget-object v0, Lcom/google/zxing/client/android/d;->a:Lcom/google/zxing/client/android/d;

    iput-object v0, p0, Lcom/google/zxing/client/android/c;->d:Lcom/google/zxing/client/android/d;

    iget-object v0, p0, Lcom/google/zxing/client/android/c;->e:Lcom/google/zxing/client/android/a/g;

    iget-object v1, p0, Lcom/google/zxing/client/android/c;->c:Lcom/google/zxing/client/android/g;

    invoke-virtual {v1}, Lcom/google/zxing/client/android/g;->a()Landroid/os/Handler;

    move-result-object v1

    sget v2, Lbr/com/itau/sdk/android/leitorboleto/R$id;->zxing_decode:I

    invoke-virtual {v0, v1, v2}, Lcom/google/zxing/client/android/a/g;->a(Landroid/os/Handler;I)V

    iget-object v0, p0, Lcom/google/zxing/client/android/c;->b:Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;

    invoke-virtual {v0}, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->drawViewfinder()V

    :cond_1c
    return-void
.end method


# virtual methods
.method public a()V
    .registers 6

    sget-object v0, Lcom/google/zxing/client/android/d;->c:Lcom/google/zxing/client/android/d;

    iput-object v0, p0, Lcom/google/zxing/client/android/c;->d:Lcom/google/zxing/client/android/d;

    iget-object v0, p0, Lcom/google/zxing/client/android/c;->e:Lcom/google/zxing/client/android/a/g;

    invoke-virtual {v0}, Lcom/google/zxing/client/android/a/g;->e()V

    iget-object v0, p0, Lcom/google/zxing/client/android/c;->c:Lcom/google/zxing/client/android/g;

    invoke-virtual {v0}, Lcom/google/zxing/client/android/g;->a()Landroid/os/Handler;

    move-result-object v0

    sget v1, Lbr/com/itau/sdk/android/leitorboleto/R$id;->zxing_quit:I

    invoke-static {v0, v1}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v3

    invoke-virtual {v3}, Landroid/os/Message;->sendToTarget()V

    :try_start_18
    iget-object v0, p0, Lcom/google/zxing/client/android/c;->c:Lcom/google/zxing/client/android/g;

    const-wide/16 v1, 0x1f4

    invoke-virtual {v0, v1, v2}, Lcom/google/zxing/client/android/g;->join(J)V
    :try_end_1f
    .catch Ljava/lang/InterruptedException; {:try_start_18 .. :try_end_1f} :catch_20

    goto :goto_21

    :catch_20
    move-exception v4

    :goto_21
    sget v0, Lbr/com/itau/sdk/android/leitorboleto/R$id;->zxing_decode_succeeded:I

    invoke-virtual {p0, v0}, Lcom/google/zxing/client/android/c;->removeMessages(I)V

    sget v0, Lbr/com/itau/sdk/android/leitorboleto/R$id;->zxing_decode_failed:I

    invoke-virtual {p0, v0}, Lcom/google/zxing/client/android/c;->removeMessages(I)V

    return-void
.end method

.method public handleMessage(Landroid/os/Message;)V
    .registers 9

    iget v0, p1, Landroid/os/Message;->what:I

    sget v1, Lbr/com/itau/sdk/android/leitorboleto/R$id;->zxing_restart_preview:I

    if-ne v0, v1, :cond_12

    const-string v0, "DECODE_HANDLER"

    const-string v1, "Restart preview"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/google/zxing/client/android/c;->b()V

    goto/16 :goto_78

    :cond_12
    iget v0, p1, Landroid/os/Message;->what:I

    sget v1, Lbr/com/itau/sdk/android/leitorboleto/R$id;->zxing_decode_succeeded:I

    if-ne v0, v1, :cond_4b

    sget-object v0, Lcom/google/zxing/client/android/d;->b:Lcom/google/zxing/client/android/d;

    iput-object v0, p0, Lcom/google/zxing/client/android/c;->d:Lcom/google/zxing/client/android/d;

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v3

    const/4 v4, 0x0

    const/high16 v5, 0x3f800000    # 1.0f

    if-eqz v3, :cond_41

    const-string v0, "barcode_bitmap"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v6

    if-eqz v6, :cond_3b

    array-length v0, v6

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-static {v6, v1, v0, v2}, Landroid/graphics/BitmapFactory;->decodeByteArray([BIILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v4

    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    const/4 v1, 0x1

    invoke-virtual {v4, v0, v1}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object v4

    :cond_3b
    const-string v0, "barcode_scaled_factor"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getFloat(Ljava/lang/String;)F

    move-result v5

    :cond_41
    iget-object v0, p0, Lcom/google/zxing/client/android/c;->b:Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;

    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Lcom/google/zxing/k;

    invoke-virtual {v0, v1, v4, v5}, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->handleDecode(Lcom/google/zxing/k;Landroid/graphics/Bitmap;F)V

    goto :goto_78

    :cond_4b
    iget v0, p1, Landroid/os/Message;->what:I

    sget v1, Lbr/com/itau/sdk/android/leitorboleto/R$id;->zxing_decode_failed:I

    if-ne v0, v1, :cond_63

    sget-object v0, Lcom/google/zxing/client/android/d;->a:Lcom/google/zxing/client/android/d;

    iput-object v0, p0, Lcom/google/zxing/client/android/c;->d:Lcom/google/zxing/client/android/d;

    iget-object v0, p0, Lcom/google/zxing/client/android/c;->e:Lcom/google/zxing/client/android/a/g;

    iget-object v1, p0, Lcom/google/zxing/client/android/c;->c:Lcom/google/zxing/client/android/g;

    invoke-virtual {v1}, Lcom/google/zxing/client/android/g;->a()Landroid/os/Handler;

    move-result-object v1

    sget v2, Lbr/com/itau/sdk/android/leitorboleto/R$id;->zxing_decode:I

    invoke-virtual {v0, v1, v2}, Lcom/google/zxing/client/android/a/g;->a(Landroid/os/Handler;I)V

    goto :goto_78

    :cond_63
    iget v0, p1, Landroid/os/Message;->what:I

    sget v1, Lbr/com/itau/sdk/android/leitorboleto/R$id;->zxing_return_scan_result:I

    if-ne v0, v1, :cond_78

    iget-object v0, p0, Lcom/google/zxing/client/android/c;->b:Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;

    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Landroid/content/Intent;

    const/4 v2, -0x1

    invoke-virtual {v0, v2, v1}, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->setResult(ILandroid/content/Intent;)V

    iget-object v0, p0, Lcom/google/zxing/client/android/c;->b:Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;

    invoke-virtual {v0}, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->finish()V

    :cond_78
    :goto_78
    return-void
.end method

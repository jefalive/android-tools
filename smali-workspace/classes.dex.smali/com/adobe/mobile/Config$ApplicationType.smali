.class public final enum Lcom/adobe/mobile/Config$ApplicationType;
.super Ljava/lang/Enum;
.source "Config.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/adobe/mobile/Config;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ApplicationType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<Lcom/adobe/mobile/Config$ApplicationType;>;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/adobe/mobile/Config$ApplicationType;

.field public static final enum APPLICATION_TYPE_HANDHELD:Lcom/adobe/mobile/Config$ApplicationType;

.field public static final enum APPLICATION_TYPE_WEARABLE:Lcom/adobe/mobile/Config$ApplicationType;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .registers 4

    .line 36
    new-instance v0, Lcom/adobe/mobile/Config$ApplicationType;

    const-string v1, "APPLICATION_TYPE_HANDHELD"

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/adobe/mobile/Config$ApplicationType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/adobe/mobile/Config$ApplicationType;->APPLICATION_TYPE_HANDHELD:Lcom/adobe/mobile/Config$ApplicationType;

    .line 37
    new-instance v0, Lcom/adobe/mobile/Config$ApplicationType;

    const-string v1, "APPLICATION_TYPE_WEARABLE"

    const/4 v2, 0x1

    const/4 v3, 0x1

    invoke-direct {v0, v1, v2, v3}, Lcom/adobe/mobile/Config$ApplicationType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/adobe/mobile/Config$ApplicationType;->APPLICATION_TYPE_WEARABLE:Lcom/adobe/mobile/Config$ApplicationType;

    .line 35
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/adobe/mobile/Config$ApplicationType;

    sget-object v1, Lcom/adobe/mobile/Config$ApplicationType;->APPLICATION_TYPE_HANDHELD:Lcom/adobe/mobile/Config$ApplicationType;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sget-object v1, Lcom/adobe/mobile/Config$ApplicationType;->APPLICATION_TYPE_WEARABLE:Lcom/adobe/mobile/Config$ApplicationType;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    sput-object v0, Lcom/adobe/mobile/Config$ApplicationType;->$VALUES:[Lcom/adobe/mobile/Config$ApplicationType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .registers 4
    .param p3, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 41
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 42
    iput p3, p0, Lcom/adobe/mobile/Config$ApplicationType;->value:I

    .line 43
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/adobe/mobile/Config$ApplicationType;
    .registers 2
    .param p0, "name"    # Ljava/lang/String;

    .line 35
    const-class v0, Lcom/adobe/mobile/Config$ApplicationType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/adobe/mobile/Config$ApplicationType;

    return-object v0
.end method

.method public static values()[Lcom/adobe/mobile/Config$ApplicationType;
    .registers 1

    .line 35
    sget-object v0, Lcom/adobe/mobile/Config$ApplicationType;->$VALUES:[Lcom/adobe/mobile/Config$ApplicationType;

    invoke-virtual {v0}, [Lcom/adobe/mobile/Config$ApplicationType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/adobe/mobile/Config$ApplicationType;

    return-object v0
.end method

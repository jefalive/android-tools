.class public final Lorg/joda/money/CurrencyUnit;
.super Ljava/lang/Object;
.source "CurrencyUnit.java"

# interfaces
.implements Ljava/lang/Comparable;
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;Ljava/lang/Comparable<Lorg/joda/money/CurrencyUnit;>;Ljava/io/Serializable;"
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field public static final AUD:Lorg/joda/money/CurrencyUnit;

.field public static final CAD:Lorg/joda/money/CurrencyUnit;

.field public static final CHF:Lorg/joda/money/CurrencyUnit;

.field private static final CODE:Ljava/util/regex/Pattern;

.field public static final EUR:Lorg/joda/money/CurrencyUnit;

.field public static final GBP:Lorg/joda/money/CurrencyUnit;

.field public static final JPY:Lorg/joda/money/CurrencyUnit;

.field public static final USD:Lorg/joda/money/CurrencyUnit;

.field private static final currenciesByCode:Ljava/util/concurrent/ConcurrentMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentMap<Ljava/lang/String;Lorg/joda/money/CurrencyUnit;>;"
        }
    .end annotation
.end field

.field private static final currenciesByCountry:Ljava/util/concurrent/ConcurrentMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentMap<Ljava/lang/String;Lorg/joda/money/CurrencyUnit;>;"
        }
    .end annotation
.end field

.field private static final currenciesByNumericCode:Ljava/util/concurrent/ConcurrentMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentMap<Ljava/lang/Integer;Lorg/joda/money/CurrencyUnit;>;"
        }
    .end annotation
.end field

.field private static final serialVersionUID:J = 0x4c54817ef7L


# instance fields
.field private final code:Ljava/lang/String;

.field private final decimalPlaces:S

.field private final numericCode:S


# direct methods
.method static constructor <clinit>()V
    .registers 4

    .line 49
    const-class v0, Lorg/joda/money/CurrencyUnit;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_a

    const/4 v0, 0x1

    goto :goto_b

    :cond_a
    const/4 v0, 0x0

    :goto_b
    sput-boolean v0, Lorg/joda/money/CurrencyUnit;->$assertionsDisabled:Z

    .line 58
    const-string v0, "[A-Z][A-Z][A-Z]"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lorg/joda/money/CurrencyUnit;->CODE:Ljava/util/regex/Pattern;

    .line 62
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    sput-object v0, Lorg/joda/money/CurrencyUnit;->currenciesByCode:Ljava/util/concurrent/ConcurrentMap;

    .line 66
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    sput-object v0, Lorg/joda/money/CurrencyUnit;->currenciesByNumericCode:Ljava/util/concurrent/ConcurrentMap;

    .line 70
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    sput-object v0, Lorg/joda/money/CurrencyUnit;->currenciesByCountry:Ljava/util/concurrent/ConcurrentMap;

    .line 75
    const-string v0, "org.joda.money.CurrencyUnitDataProvider"

    const-string v1, "org.joda.money.DefaultCurrencyUnitDataProvider"

    :try_start_2e
    invoke-static {v0, v1}, Ljava/lang/System;->getProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 77
    .local v2, "clsName":Ljava/lang/String;
    const-class v0, Lorg/joda/money/CurrencyUnit;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/ClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    const-class v1, Lorg/joda/money/CurrencyUnitDataProvider;

    invoke-virtual {v0, v1}, Ljava/lang/Class;->asSubclass(Ljava/lang/Class;)Ljava/lang/Class;

    move-result-object v3

    .line 79
    .local v3, "cls":Ljava/lang/Class;, "Ljava/lang/Class<+Lorg/joda/money/CurrencyUnitDataProvider;>;"
    invoke-virtual {v3}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/joda/money/CurrencyUnitDataProvider;

    invoke-virtual {v0}, Lorg/joda/money/CurrencyUnitDataProvider;->registerCurrencies()V
    :try_end_4b
    .catch Ljava/lang/SecurityException; {:try_start_2e .. :try_end_4b} :catch_4c
    .catch Ljava/lang/RuntimeException; {:try_start_2e .. :try_end_4b} :catch_56
    .catch Ljava/lang/Exception; {:try_start_2e .. :try_end_4b} :catch_58

    .line 82
    .end local v2    # "clsName":Ljava/lang/String;
    .end local v3    # "cls":Ljava/lang/Class;, "Ljava/lang/Class<+Lorg/joda/money/CurrencyUnitDataProvider;>;"
    .end local v3
    goto :goto_55

    .line 80
    :catch_4c
    move-exception v2

    .line 81
    .local v2, "ex":Ljava/lang/SecurityException;
    :try_start_4d
    new-instance v0, Lorg/joda/money/DefaultCurrencyUnitDataProvider;

    invoke-direct {v0}, Lorg/joda/money/DefaultCurrencyUnitDataProvider;-><init>()V

    invoke-virtual {v0}, Lorg/joda/money/DefaultCurrencyUnitDataProvider;->registerCurrencies()V
    :try_end_55
    .catch Ljava/lang/RuntimeException; {:try_start_4d .. :try_end_55} :catch_56
    .catch Ljava/lang/Exception; {:try_start_4d .. :try_end_55} :catch_58

    .line 87
    .end local v2    # "ex":Ljava/lang/SecurityException;
    :goto_55
    goto :goto_63

    .line 83
    :catch_56
    move-exception v2

    .line 84
    .local v2, "ex":Ljava/lang/RuntimeException;
    throw v2

    .line 85
    .end local v2    # "ex":Ljava/lang/RuntimeException;
    :catch_58
    move-exception v2

    .line 86
    .local v2, "ex":Ljava/lang/Exception;
    new-instance v0, Ljava/lang/RuntimeException;

    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0

    .line 94
    .end local v2    # "ex":Ljava/lang/Exception;
    :goto_63
    const-string v0, "USD"

    invoke-static {v0}, Lorg/joda/money/CurrencyUnit;->of(Ljava/lang/String;)Lorg/joda/money/CurrencyUnit;

    move-result-object v0

    sput-object v0, Lorg/joda/money/CurrencyUnit;->USD:Lorg/joda/money/CurrencyUnit;

    .line 98
    const-string v0, "EUR"

    invoke-static {v0}, Lorg/joda/money/CurrencyUnit;->of(Ljava/lang/String;)Lorg/joda/money/CurrencyUnit;

    move-result-object v0

    sput-object v0, Lorg/joda/money/CurrencyUnit;->EUR:Lorg/joda/money/CurrencyUnit;

    .line 102
    const-string v0, "JPY"

    invoke-static {v0}, Lorg/joda/money/CurrencyUnit;->of(Ljava/lang/String;)Lorg/joda/money/CurrencyUnit;

    move-result-object v0

    sput-object v0, Lorg/joda/money/CurrencyUnit;->JPY:Lorg/joda/money/CurrencyUnit;

    .line 106
    const-string v0, "GBP"

    invoke-static {v0}, Lorg/joda/money/CurrencyUnit;->of(Ljava/lang/String;)Lorg/joda/money/CurrencyUnit;

    move-result-object v0

    sput-object v0, Lorg/joda/money/CurrencyUnit;->GBP:Lorg/joda/money/CurrencyUnit;

    .line 110
    const-string v0, "CHF"

    invoke-static {v0}, Lorg/joda/money/CurrencyUnit;->of(Ljava/lang/String;)Lorg/joda/money/CurrencyUnit;

    move-result-object v0

    sput-object v0, Lorg/joda/money/CurrencyUnit;->CHF:Lorg/joda/money/CurrencyUnit;

    .line 114
    const-string v0, "AUD"

    invoke-static {v0}, Lorg/joda/money/CurrencyUnit;->of(Ljava/lang/String;)Lorg/joda/money/CurrencyUnit;

    move-result-object v0

    sput-object v0, Lorg/joda/money/CurrencyUnit;->AUD:Lorg/joda/money/CurrencyUnit;

    .line 118
    const-string v0, "CAD"

    invoke-static {v0}, Lorg/joda/money/CurrencyUnit;->of(Ljava/lang/String;)Lorg/joda/money/CurrencyUnit;

    move-result-object v0

    sput-object v0, Lorg/joda/money/CurrencyUnit;->CAD:Lorg/joda/money/CurrencyUnit;

    return-void
.end method

.method constructor <init>(Ljava/lang/String;SS)V
    .registers 6
    .param p1, "code"    # Ljava/lang/String;
    .param p2, "numericCurrencyCode"    # S
    .param p3, "decimalPlaces"    # S

    .line 399
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 400
    sget-boolean v0, Lorg/joda/money/CurrencyUnit;->$assertionsDisabled:Z

    if-nez v0, :cond_11

    if-nez p1, :cond_11

    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "Joda-Money bug: Currency code must not be null"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 401
    :cond_11
    iput-object p1, p0, Lorg/joda/money/CurrencyUnit;->code:Ljava/lang/String;

    .line 402
    iput-short p2, p0, Lorg/joda/money/CurrencyUnit;->numericCode:S

    .line 403
    iput-short p3, p0, Lorg/joda/money/CurrencyUnit;->decimalPlaces:S

    .line 404
    return-void
.end method

.method public static getInstance(Ljava/lang/String;)Lorg/joda/money/CurrencyUnit;
    .registers 2
    .param p0, "currencyCode"    # Ljava/lang/String;

    .line 375
    invoke-static {p0}, Lorg/joda/money/CurrencyUnit;->of(Ljava/lang/String;)Lorg/joda/money/CurrencyUnit;

    move-result-object v0

    return-object v0
.end method

.method public static getInstance(Ljava/util/Locale;)Lorg/joda/money/CurrencyUnit;
    .registers 2
    .param p0, "locale"    # Ljava/util/Locale;

    .line 388
    invoke-static {p0}, Lorg/joda/money/CurrencyUnit;->of(Ljava/util/Locale;)Lorg/joda/money/CurrencyUnit;

    move-result-object v0

    return-object v0
.end method

.method public static of(Ljava/lang/String;)Lorg/joda/money/CurrencyUnit;
    .registers 5
    .param p0, "currencyCode"    # Ljava/lang/String;
    .annotation runtime Lorg/joda/convert/FromString;
    .end annotation

    .line 275
    const-string v0, "Currency code must not be null"

    invoke-static {p0, v0}, Lorg/joda/money/MoneyUtils;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 276
    sget-object v0, Lorg/joda/money/CurrencyUnit;->currenciesByCode:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0, p0}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lorg/joda/money/CurrencyUnit;

    .line 277
    .local v3, "currency":Lorg/joda/money/CurrencyUnit;
    if-nez v3, :cond_2f

    .line 278
    new-instance v0, Lorg/joda/money/IllegalCurrencyException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown currency \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v2, 0x27

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/joda/money/IllegalCurrencyException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 280
    :cond_2f
    return-object v3
.end method

.method public static of(Ljava/util/Currency;)Lorg/joda/money/CurrencyUnit;
    .registers 2
    .param p0, "currency"    # Ljava/util/Currency;

    .line 259
    const-string v0, "Currency must not be null"

    invoke-static {p0, v0}, Lorg/joda/money/MoneyUtils;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 260
    invoke-virtual {p0}, Ljava/util/Currency;->getCurrencyCode()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lorg/joda/money/CurrencyUnit;->of(Ljava/lang/String;)Lorg/joda/money/CurrencyUnit;

    move-result-object v0

    return-object v0
.end method

.method public static of(Ljava/util/Locale;)Lorg/joda/money/CurrencyUnit;
    .registers 5
    .param p0, "locale"    # Ljava/util/Locale;

    .line 337
    const-string v0, "Locale must not be null"

    invoke-static {p0, v0}, Lorg/joda/money/MoneyUtils;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 338
    sget-object v0, Lorg/joda/money/CurrencyUnit;->currenciesByCountry:Ljava/util/concurrent/ConcurrentMap;

    invoke-virtual {p0}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lorg/joda/money/CurrencyUnit;

    .line 339
    .local v3, "currency":Lorg/joda/money/CurrencyUnit;
    if-nez v3, :cond_33

    .line 340
    new-instance v0, Lorg/joda/money/IllegalCurrencyException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown currency for locale \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v2, 0x27

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/joda/money/IllegalCurrencyException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 342
    :cond_33
    return-object v3
.end method

.method public static ofCountry(Ljava/lang/String;)Lorg/joda/money/CurrencyUnit;
    .registers 5
    .param p0, "countryCode"    # Ljava/lang/String;

    .line 356
    const-string v0, "Country code must not be null"

    invoke-static {p0, v0}, Lorg/joda/money/MoneyUtils;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 357
    sget-object v0, Lorg/joda/money/CurrencyUnit;->currenciesByCountry:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0, p0}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lorg/joda/money/CurrencyUnit;

    .line 358
    .local v3, "currency":Lorg/joda/money/CurrencyUnit;
    if-nez v3, :cond_2f

    .line 359
    new-instance v0, Lorg/joda/money/IllegalCurrencyException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown currency for country \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v2, 0x27

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/joda/money/IllegalCurrencyException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 361
    :cond_2f
    return-object v3
.end method

.method public static ofNumericCode(I)Lorg/joda/money/CurrencyUnit;
    .registers 5
    .param p0, "numericCurrencyCode"    # I

    .line 320
    sget-object v0, Lorg/joda/money/CurrencyUnit;->currenciesByNumericCode:Ljava/util/concurrent/ConcurrentMap;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lorg/joda/money/CurrencyUnit;

    .line 321
    .local v3, "currency":Lorg/joda/money/CurrencyUnit;
    if-nez v3, :cond_2e

    .line 322
    new-instance v0, Lorg/joda/money/IllegalCurrencyException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown currency \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v2, 0x27

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/joda/money/IllegalCurrencyException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 324
    :cond_2e
    return-object v3
.end method

.method public static ofNumericCode(Ljava/lang/String;)Lorg/joda/money/CurrencyUnit;
    .registers 4
    .param p0, "numericCurrencyCode"    # Ljava/lang/String;

    .line 294
    const-string v0, "Currency code must not be null"

    invoke-static {p0, v0}, Lorg/joda/money/MoneyUtils;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 295
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    packed-switch v0, :pswitch_data_6e

    goto :goto_4f

    .line 297
    :pswitch_d
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v0

    add-int/lit8 v0, v0, -0x30

    invoke-static {v0}, Lorg/joda/money/CurrencyUnit;->ofNumericCode(I)Lorg/joda/money/CurrencyUnit;

    move-result-object v0

    return-object v0

    .line 299
    :pswitch_19
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v0

    add-int/lit8 v0, v0, -0x30

    mul-int/lit8 v0, v0, 0xa

    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v1

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, -0x30

    invoke-static {v0}, Lorg/joda/money/CurrencyUnit;->ofNumericCode(I)Lorg/joda/money/CurrencyUnit;

    move-result-object v0

    return-object v0

    .line 302
    :pswitch_2f
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v0

    add-int/lit8 v0, v0, -0x30

    mul-int/lit8 v0, v0, 0x64

    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v1

    add-int/lit8 v1, v1, -0x30

    mul-int/lit8 v1, v1, 0xa

    add-int/2addr v0, v1

    const/4 v1, 0x2

    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v1

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, -0x30

    invoke-static {v0}, Lorg/joda/money/CurrencyUnit;->ofNumericCode(I)Lorg/joda/money/CurrencyUnit;

    move-result-object v0

    return-object v0

    .line 306
    :goto_4f
    new-instance v0, Lorg/joda/money/IllegalCurrencyException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown currency \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v2, 0x27

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/joda/money/IllegalCurrencyException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_data_6e
    .packed-switch 0x1
        :pswitch_d
        :pswitch_19
        :pswitch_2f
    .end packed-switch
.end method

.method private readObject(Ljava/io/ObjectInputStream;)V
    .registers 4
    .param p1, "ois"    # Ljava/io/ObjectInputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/InvalidObjectException;
        }
    .end annotation

    .line 413
    new-instance v0, Ljava/io/InvalidObjectException;

    const-string v1, "Serialization delegate required"

    invoke-direct {v0, v1}, Ljava/io/InvalidObjectException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static declared-synchronized registerCurrency(Ljava/lang/String;IILjava/util/List;)Lorg/joda/money/CurrencyUnit;
    .registers 6
    .param p0, "currencyCode"    # Ljava/lang/String;
    .param p1, "numericCurrencyCode"    # I
    .param p2, "decimalPlaces"    # I
    .param p3, "countryCodes"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/lang/String;IILjava/util/List<Ljava/lang/String;>;)Lorg/joda/money/CurrencyUnit;"
        }
    .end annotation

    const-class v1, Lorg/joda/money/CurrencyUnit;

    monitor-enter v1

    .line 158
    const/4 v0, 0x0

    :try_start_4
    invoke-static {p0, p1, p2, p3, v0}, Lorg/joda/money/CurrencyUnit;->registerCurrency(Ljava/lang/String;IILjava/util/List;Z)Lorg/joda/money/CurrencyUnit;
    :try_end_7
    .catchall {:try_start_4 .. :try_end_7} :catchall_a

    move-result-object v0

    monitor-exit v1

    return-object v0

    :catchall_a
    move-exception p0

    monitor-exit v1

    throw p0
.end method

.method public static declared-synchronized registerCurrency(Ljava/lang/String;IILjava/util/List;Z)Lorg/joda/money/CurrencyUnit;
    .registers 12
    .param p0, "currencyCode"    # Ljava/lang/String;
    .param p1, "numericCurrencyCode"    # I
    .param p2, "decimalPlaces"    # I
    .param p3, "countryCodes"    # Ljava/util/List;
    .param p4, "force"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/lang/String;IILjava/util/List<Ljava/lang/String;>;Z)Lorg/joda/money/CurrencyUnit;"
        }
    .end annotation

    const-class v6, Lorg/joda/money/CurrencyUnit;

    monitor-enter v6

    .line 191
    const-string v0, "Currency code must not be null"

    :try_start_5
    invoke-static {p0, v0}, Lorg/joda/money/MoneyUtils;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 192
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x3

    if-eq v0, v1, :cond_17

    .line 193
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid string code, must be length 3"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 195
    :cond_17
    sget-object v0, Lorg/joda/money/CurrencyUnit;->CODE:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    if-nez v0, :cond_2b

    .line 196
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid string code, must be ASCII upper-case letters"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 198
    :cond_2b
    const/4 v0, -0x1

    if-lt p1, v0, :cond_32

    const/16 v0, 0x3e7

    if-le p1, v0, :cond_3a

    .line 199
    :cond_32
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid numeric code"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 201
    :cond_3a
    const/4 v0, -0x1

    if-lt p2, v0, :cond_41

    const/16 v0, 0x9

    if-le p2, v0, :cond_49

    .line 202
    :cond_41
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid number of decimal places"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 204
    :cond_49
    const-string v0, "Country codes must not be null"

    invoke-static {p3, v0}, Lorg/joda/money/MoneyUtils;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 206
    new-instance v3, Lorg/joda/money/CurrencyUnit;

    int-to-short v0, p1

    int-to-short v1, p2

    invoke-direct {v3, p0, v0, v1}, Lorg/joda/money/CurrencyUnit;-><init>(Ljava/lang/String;SS)V

    .line 207
    .local v3, "currency":Lorg/joda/money/CurrencyUnit;
    if-eqz p4, :cond_7e

    .line 208
    sget-object v0, Lorg/joda/money/CurrencyUnit;->currenciesByCode:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0, p0}, Ljava/util/concurrent/ConcurrentMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 209
    sget-object v0, Lorg/joda/money/CurrencyUnit;->currenciesByNumericCode:Ljava/util/concurrent/ConcurrentMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/concurrent/ConcurrentMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 210
    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :goto_69
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7c

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Ljava/lang/String;

    .line 211
    .local v5, "countryCode":Ljava/lang/String;
    sget-object v0, Lorg/joda/money/CurrencyUnit;->currenciesByCountry:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0, v5}, Ljava/util/concurrent/ConcurrentMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .end local v5    # "countryCode":Ljava/lang/String;
    goto :goto_69

    .end local v4    # "i$":Ljava/util/Iterator;
    :cond_7c
    goto/16 :goto_de

    .line 214
    :cond_7e
    sget-object v0, Lorg/joda/money/CurrencyUnit;->currenciesByCode:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0, p0}, Ljava/util/concurrent/ConcurrentMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_92

    sget-object v0, Lorg/joda/money/CurrencyUnit;->currenciesByNumericCode:Ljava/util/concurrent/ConcurrentMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/concurrent/ConcurrentMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_ab

    .line 215
    :cond_92
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Currency already registered: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 217
    :cond_ab
    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :goto_af
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_de

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Ljava/lang/String;

    .line 218
    .local v5, "countryCode":Ljava/lang/String;
    sget-object v0, Lorg/joda/money/CurrencyUnit;->currenciesByCountry:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0, v5}, Ljava/util/concurrent/ConcurrentMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_dd

    .line 219
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Currency already registered for country: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .end local v5    # "countryCode":Ljava/lang/String;
    :cond_dd
    goto :goto_af

    .line 223
    .end local v4    # "i$":Ljava/util/Iterator;
    :cond_de
    :goto_de
    sget-object v0, Lorg/joda/money/CurrencyUnit;->currenciesByCode:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0, p0, v3}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 224
    if-ltz p1, :cond_ee

    .line 225
    sget-object v0, Lorg/joda/money/CurrencyUnit;->currenciesByNumericCode:Ljava/util/concurrent/ConcurrentMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1, v3}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 227
    :cond_ee
    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :goto_f2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_105

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Ljava/lang/String;

    .line 228
    .local v5, "countryCode":Ljava/lang/String;
    sget-object v0, Lorg/joda/money/CurrencyUnit;->currenciesByCountry:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0, v5, v3}, Ljava/util/concurrent/ConcurrentMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .end local v5    # "countryCode":Ljava/lang/String;
    goto :goto_f2

    .line 230
    .end local v4    # "i$":Ljava/util/Iterator;
    :cond_105
    sget-object v0, Lorg/joda/money/CurrencyUnit;->currenciesByCode:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0, p0}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/joda/money/CurrencyUnit;
    :try_end_10d
    .catchall {:try_start_5 .. :try_end_10d} :catchall_10f

    monitor-exit v6

    return-object v0

    .end local v3    # "currency":Lorg/joda/money/CurrencyUnit;
    :catchall_10f
    move-exception p0

    monitor-exit v6

    throw p0
.end method

.method public static registeredCurrencies()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Lorg/joda/money/CurrencyUnit;>;"
        }
    .end annotation

    .line 243
    new-instance v1, Ljava/util/ArrayList;

    sget-object v0, Lorg/joda/money/CurrencyUnit;->currenciesByCode:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0}, Ljava/util/concurrent/ConcurrentMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 244
    .local v1, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/joda/money/CurrencyUnit;>;"
    invoke-static {v1}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 245
    return-object v1
.end method

.method private writeReplace()Ljava/lang/Object;
    .registers 3

    .line 422
    new-instance v0, Lorg/joda/money/Ser;

    const/16 v1, 0x43

    invoke-direct {v0, v1, p0}, Lorg/joda/money/Ser;-><init>(BLjava/lang/Object;)V

    return-object v0
.end method


# virtual methods
.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .registers 3
    .param p1, "x0"    # Ljava/lang/Object;

    .line 49
    move-object v0, p1

    check-cast v0, Lorg/joda/money/CurrencyUnit;

    invoke-virtual {p0, v0}, Lorg/joda/money/CurrencyUnit;->compareTo(Lorg/joda/money/CurrencyUnit;)I

    move-result v0

    return v0
.end method

.method public compareTo(Lorg/joda/money/CurrencyUnit;)I
    .registers 4
    .param p1, "other"    # Lorg/joda/money/CurrencyUnit;

    .line 603
    iget-object v0, p0, Lorg/joda/money/CurrencyUnit;->code:Ljava/lang/String;

    iget-object v1, p1, Lorg/joda/money/CurrencyUnit;->code:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 4
    .param p1, "obj"    # Ljava/lang/Object;

    .line 616
    if-ne p1, p0, :cond_4

    .line 617
    const/4 v0, 0x1

    return v0

    .line 619
    :cond_4
    instance-of v0, p1, Lorg/joda/money/CurrencyUnit;

    if-eqz v0, :cond_14

    .line 620
    iget-object v0, p0, Lorg/joda/money/CurrencyUnit;->code:Ljava/lang/String;

    move-object v1, p1

    check-cast v1, Lorg/joda/money/CurrencyUnit;

    iget-object v1, v1, Lorg/joda/money/CurrencyUnit;->code:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0

    .line 622
    :cond_14
    const/4 v0, 0x0

    return v0
.end method

.method public getCode()Ljava/lang/String;
    .registers 2

    .line 434
    iget-object v0, p0, Lorg/joda/money/CurrencyUnit;->code:Ljava/lang/String;

    return-object v0
.end method

.method public getCountryCodes()Ljava/util/Set;
    .registers 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/Set<Ljava/lang/String;>;"
        }
    .end annotation

    .line 480
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 481
    .local v1, "countryCodes":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    sget-object v0, Lorg/joda/money/CurrencyUnit;->currenciesByCountry:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0}, Ljava/util/concurrent/ConcurrentMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_f
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2e

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Ljava/util/Map$Entry;

    .line 482
    .local v3, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lorg/joda/money/CurrencyUnit;>;"
    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/joda/money/CurrencyUnit;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2d

    .line 483
    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .end local v3    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lorg/joda/money/CurrencyUnit;>;"
    .end local v3
    :cond_2d
    goto :goto_f

    .line 486
    .end local v2    # "i$":Ljava/util/Iterator;
    :cond_2e
    return-object v1
.end method

.method public getCurrencyCode()Ljava/lang/String;
    .registers 2

    .line 523
    iget-object v0, p0, Lorg/joda/money/CurrencyUnit;->code:Ljava/lang/String;

    return-object v0
.end method

.method public getDecimalPlaces()I
    .registers 2

    .line 502
    iget-short v0, p0, Lorg/joda/money/CurrencyUnit;->decimalPlaces:S

    if-gez v0, :cond_6

    const/4 v0, 0x0

    goto :goto_8

    :cond_6
    iget-short v0, p0, Lorg/joda/money/CurrencyUnit;->decimalPlaces:S

    :goto_8
    return v0
.end method

.method public getDefaultFractionDigits()I
    .registers 2

    .line 539
    iget-short v0, p0, Lorg/joda/money/CurrencyUnit;->decimalPlaces:S

    return v0
.end method

.method public getNumeric3Code()Ljava/lang/String;
    .registers 4

    .line 457
    iget-short v0, p0, Lorg/joda/money/CurrencyUnit;->numericCode:S

    if-gez v0, :cond_7

    .line 458
    const-string v0, ""

    return-object v0

    .line 460
    :cond_7
    iget-short v0, p0, Lorg/joda/money/CurrencyUnit;->numericCode:S

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    .line 461
    .local v2, "str":Ljava/lang/String;
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_28

    .line 462
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "00"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 464
    :cond_28
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_43

    .line 465
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "0"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 467
    :cond_43
    return-object v2
.end method

.method public getNumericCode()I
    .registers 2

    .line 445
    iget-short v0, p0, Lorg/joda/money/CurrencyUnit;->numericCode:S

    return v0
.end method

.method public getSymbol()Ljava/lang/String;
    .registers 3

    .line 555
    :try_start_0
    iget-object v0, p0, Lorg/joda/money/CurrencyUnit;->code:Ljava/lang/String;

    invoke-static {v0}, Ljava/util/Currency;->getInstance(Ljava/lang/String;)Ljava/util/Currency;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Currency;->getSymbol()Ljava/lang/String;
    :try_end_9
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_9} :catch_b

    move-result-object v0

    return-object v0

    .line 556
    :catch_b
    move-exception v1

    .line 557
    .local v1, "ex":Ljava/lang/IllegalArgumentException;
    iget-object v0, p0, Lorg/joda/money/CurrencyUnit;->code:Ljava/lang/String;

    return-object v0
.end method

.method public getSymbol(Ljava/util/Locale;)Ljava/lang/String;
    .registers 4
    .param p1, "locale"    # Ljava/util/Locale;

    .line 573
    const-string v0, "Locale must not be null"

    invoke-static {p1, v0}, Lorg/joda/money/MoneyUtils;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 575
    :try_start_5
    iget-object v0, p0, Lorg/joda/money/CurrencyUnit;->code:Ljava/lang/String;

    invoke-static {v0}, Ljava/util/Currency;->getInstance(Ljava/lang/String;)Ljava/util/Currency;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/Currency;->getSymbol(Ljava/util/Locale;)Ljava/lang/String;
    :try_end_e
    .catch Ljava/lang/IllegalArgumentException; {:try_start_5 .. :try_end_e} :catch_10

    move-result-object v0

    return-object v0

    .line 576
    :catch_10
    move-exception v1

    .line 577
    .local v1, "ex":Ljava/lang/IllegalArgumentException;
    iget-object v0, p0, Lorg/joda/money/CurrencyUnit;->code:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .registers 2

    .line 632
    iget-object v0, p0, Lorg/joda/money/CurrencyUnit;->code:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method

.method public isPseudoCurrency()Z
    .registers 2

    .line 511
    iget-short v0, p0, Lorg/joda/money/CurrencyUnit;->decimalPlaces:S

    if-gez v0, :cond_6

    const/4 v0, 0x1

    goto :goto_7

    :cond_6
    const/4 v0, 0x0

    :goto_7
    return v0
.end method

.method public toCurrency()Ljava/util/Currency;
    .registers 2

    .line 591
    iget-object v0, p0, Lorg/joda/money/CurrencyUnit;->code:Ljava/lang/String;

    invoke-static {v0}, Ljava/util/Currency;->getInstance(Ljava/lang/String;)Ljava/util/Currency;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .registers 2
    .annotation runtime Lorg/joda/convert/ToString;
    .end annotation

    .line 644
    iget-object v0, p0, Lorg/joda/money/CurrencyUnit;->code:Ljava/lang/String;

    return-object v0
.end method

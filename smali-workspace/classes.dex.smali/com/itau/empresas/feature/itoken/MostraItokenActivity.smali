.class public Lcom/itau/empresas/feature/itoken/MostraItokenActivity;
.super Lcom/itau/empresas/ui/activity/BaseActivity;
.source "MostraItokenActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/itau/empresas/feature/itoken/MostraItokenActivity$FinishOnClickListener;,
        Lcom/itau/empresas/feature/itoken/MostraItokenActivity$FlipTextWatcher;
    }
.end annotation


# instance fields
.field circulo:Lcom/itau/animacao/loading/LoadingView;

.field private otpModel:Lbr/com/itau/security/token/model/OTPModel;

.field primeiroCirculo:Landroid/widget/ImageView;

.field root:Landroid/widget/RelativeLayout;

.field segundoCirculo:Landroid/widget/ImageView;

.field textoNumero:Landroid/widget/TextView;

.field textoNumeroToken:Landroid/widget/TextView;

.field private uiHandler:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 36
    invoke-direct {p0}, Lcom/itau/empresas/ui/activity/BaseActivity;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/itau/empresas/feature/itoken/MostraItokenActivity;)V
    .registers 1
    .param p0, "x0"    # Lcom/itau/empresas/feature/itoken/MostraItokenActivity;

    .line 36
    invoke-direct {p0}, Lcom/itau/empresas/feature/itoken/MostraItokenActivity;->atualizaOtp()V

    return-void
.end method

.method private atualizaOtp()V
    .registers 5

    .line 85
    iget-object v0, p0, Lcom/itau/empresas/feature/itoken/MostraItokenActivity;->uiHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 86
    invoke-static {}, Lbr/com/itau/sdk/android/core/BackendClient;->getOfflineOTP()Lbr/com/itau/security/token/model/OTPModel;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/itoken/MostraItokenActivity;->otpModel:Lbr/com/itau/security/token/model/OTPModel;

    .line 87
    iget-object v0, p0, Lcom/itau/empresas/feature/itoken/MostraItokenActivity;->otpModel:Lbr/com/itau/security/token/model/OTPModel;

    if-nez v0, :cond_15

    .line 88
    return-void

    .line 89
    :cond_15
    invoke-direct {p0}, Lcom/itau/empresas/feature/itoken/MostraItokenActivity;->iniciaAnimacaoLoading()V

    .line 90
    invoke-virtual {p0}, Lcom/itau/empresas/feature/itoken/MostraItokenActivity;->atualizaDadosView()V

    .line 91
    iget-object v0, p0, Lcom/itau/empresas/feature/itoken/MostraItokenActivity;->uiHandler:Landroid/os/Handler;

    new-instance v1, Lcom/itau/empresas/feature/itoken/MostraItokenActivity$1;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/itoken/MostraItokenActivity$1;-><init>(Lcom/itau/empresas/feature/itoken/MostraItokenActivity;)V

    .line 95
    invoke-direct {p0}, Lcom/itau/empresas/feature/itoken/MostraItokenActivity;->tempoRestanteDoToken()J

    move-result-wide v2

    .line 91
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 96
    return-void
.end method

.method private disparaEventoAnalytics()V
    .registers 5

    .line 147
    invoke-static {}, Lcom/itau/empresas/feature/analytics/AnalyticsHelper;->getInstance()Lcom/itau/empresas/feature/analytics/AnalyticsHelper;

    move-result-object v3

    .line 148
    .line 149
    .local v3, "analyticsHelper":Lcom/itau/empresas/feature/analytics/AnalyticsHelper;
    const v0, 0x7f0700e6

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/itoken/MostraItokenActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 150
    const v1, 0x7f0700cb

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/itoken/MostraItokenActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 151
    const v2, 0x7f0700c4

    invoke-virtual {p0, v2}, Lcom/itau/empresas/feature/itoken/MostraItokenActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 148
    invoke-virtual {v3, v0, v1, v2}, Lcom/itau/empresas/feature/analytics/AnalyticsHelper;->eventoAdobeState(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 152
    return-void
.end method

.method private findView(I)Landroid/view/View;
    .registers 3
    .param p1, "viewId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:Landroid/view/View;>(I)TT;"
        }
    .end annotation

    .line 74
    invoke-virtual {p0, p1}, Lcom/itau/empresas/feature/itoken/MostraItokenActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method private iniciaAnimacaoLoading()V
    .registers 5

    .line 99
    iget-object v0, p0, Lcom/itau/empresas/feature/itoken/MostraItokenActivity;->circulo:Lcom/itau/animacao/loading/LoadingView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/itau/animacao/loading/LoadingView;->setAngle(F)V

    .line 100
    iget-object v0, p0, Lcom/itau/empresas/feature/itoken/MostraItokenActivity;->circulo:Lcom/itau/animacao/loading/LoadingView;

    invoke-virtual {v0}, Lcom/itau/animacao/loading/LoadingView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v1, 0x64

    invoke-virtual {v0, v1, v2}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    .line 101
    new-instance v3, Lcom/itau/animacao/loading/LoadingViewAnimation;

    iget-object v0, p0, Lcom/itau/empresas/feature/itoken/MostraItokenActivity;->circulo:Lcom/itau/animacao/loading/LoadingView;

    const/high16 v1, 0x43b40000    # 360.0f

    invoke-direct {v3, v0, v1}, Lcom/itau/animacao/loading/LoadingViewAnimation;-><init>(Lcom/itau/animacao/loading/LoadingView;F)V

    .line 102
    .local v3, "animation":Landroid/view/animation/Animation;
    invoke-direct {p0}, Lcom/itau/empresas/feature/itoken/MostraItokenActivity;->tempoRestanteDoToken()J

    move-result-wide v0

    invoke-virtual {v3, v0, v1}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 103
    new-instance v0, Lcom/itau/empresas/feature/itoken/MostraItokenActivity$2;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/itoken/MostraItokenActivity$2;-><init>(Lcom/itau/empresas/feature/itoken/MostraItokenActivity;)V

    invoke-virtual {v3, v0}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 116
    iget-object v0, p0, Lcom/itau/empresas/feature/itoken/MostraItokenActivity;->circulo:Lcom/itau/animacao/loading/LoadingView;

    invoke-virtual {v0, v3}, Lcom/itau/animacao/loading/LoadingView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 117
    return-void
.end method

.method private tempoRestanteDoToken()J
    .registers 5

    .line 120
    iget-object v0, p0, Lcom/itau/empresas/feature/itoken/MostraItokenActivity;->otpModel:Lbr/com/itau/security/token/model/OTPModel;

    invoke-virtual {v0}, Lbr/com/itau/security/token/model/OTPModel;->timeRemaining()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    mul-long/2addr v0, v2

    return-wide v0
.end method


# virtual methods
.method animaArcosExternos()V
    .registers 5

    .line 124
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_2b

    .line 125
    iget-object v0, p0, Lcom/itau/empresas/feature/itoken/MostraItokenActivity;->primeiroCirculo:Landroid/widget/ImageView;

    new-instance v1, Lcom/itau/animacao/loading/PulseDrawable;

    .line 126
    const v2, 0x7f0c000b

    invoke-static {p0, v2}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v2

    const/4 v3, -0x1

    invoke-direct {v1, v3, v2}, Lcom/itau/animacao/loading/PulseDrawable;-><init>(II)V

    .line 125
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 127
    iget-object v0, p0, Lcom/itau/empresas/feature/itoken/MostraItokenActivity;->segundoCirculo:Landroid/widget/ImageView;

    new-instance v1, Lcom/itau/animacao/loading/PulseDrawable;

    .line 128
    const v2, 0x7f0c000b

    invoke-static {p0, v2}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v2

    const/4 v3, -0x1

    invoke-direct {v1, v3, v2}, Lcom/itau/animacao/loading/PulseDrawable;-><init>(II)V

    .line 127
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_4f

    .line 130
    :cond_2b
    iget-object v0, p0, Lcom/itau/empresas/feature/itoken/MostraItokenActivity;->primeiroCirculo:Landroid/widget/ImageView;

    new-instance v1, Lcom/itau/animacao/loading/PulseDrawable;

    .line 131
    const v2, 0x7f0c000b

    invoke-static {p0, v2}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v2

    const/4 v3, -0x1

    invoke-direct {v1, v3, v2}, Lcom/itau/animacao/loading/PulseDrawable;-><init>(II)V

    .line 130
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 132
    iget-object v0, p0, Lcom/itau/empresas/feature/itoken/MostraItokenActivity;->segundoCirculo:Landroid/widget/ImageView;

    new-instance v1, Lcom/itau/animacao/loading/PulseDrawable;

    .line 133
    const v2, 0x7f0c000b

    invoke-static {p0, v2}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v2

    const/4 v3, -0x1

    invoke-direct {v1, v3, v2}, Lcom/itau/animacao/loading/PulseDrawable;-><init>(II)V

    .line 132
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 135
    :goto_4f
    return-void
.end method

.method atualizaDadosView()V
    .registers 5

    .line 138
    iget-object v0, p0, Lcom/itau/empresas/feature/itoken/MostraItokenActivity;->otpModel:Lbr/com/itau/security/token/model/OTPModel;

    if-nez v0, :cond_5

    .line 139
    return-void

    .line 140
    :cond_5
    iget-object v0, p0, Lcom/itau/empresas/feature/itoken/MostraItokenActivity;->textoNumeroToken:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/itau/empresas/feature/itoken/MostraItokenActivity;->otpModel:Lbr/com/itau/security/token/model/OTPModel;

    invoke-virtual {v1}, Lbr/com/itau/security/token/model/OTPModel;->token()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 141
    iget-object v0, p0, Lcom/itau/empresas/feature/itoken/MostraItokenActivity;->textoNumero:Landroid/widget/TextView;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/itau/empresas/feature/itoken/MostraItokenActivity;->otpModel:Lbr/com/itau/security/token/model/OTPModel;

    invoke-virtual {v2}, Lbr/com/itau/security/token/model/OTPModel;->serialNumber()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const v2, 0x7f0703ee

    invoke-virtual {p0, v2, v1}, Lcom/itau/empresas/feature/itoken/MostraItokenActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 143
    iget-object v0, p0, Lcom/itau/empresas/feature/itoken/MostraItokenActivity;->textoNumeroToken:Landroid/widget/TextView;

    new-instance v1, Lcom/itau/empresas/feature/itoken/MostraItokenActivity$FlipTextWatcher;

    iget-object v2, p0, Lcom/itau/empresas/feature/itoken/MostraItokenActivity;->textoNumero:Landroid/widget/TextView;

    invoke-direct {v1, v2}, Lcom/itau/empresas/feature/itoken/MostraItokenActivity$FlipTextWatcher;-><init>(Landroid/widget/TextView;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 144
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .registers 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 57
    invoke-super {p0, p1}, Lcom/itau/empresas/ui/activity/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 58
    const v0, 0x7f030052

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/itoken/MostraItokenActivity;->setContentView(I)V

    .line 60
    const v0, 0x7f0e0275

    invoke-direct {p0, v0}, Lcom/itau/empresas/feature/itoken/MostraItokenActivity;->findView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/itau/empresas/feature/itoken/MostraItokenActivity;->root:Landroid/widget/RelativeLayout;

    .line 61
    const v0, 0x7f0e0279

    invoke-direct {p0, v0}, Lcom/itau/empresas/feature/itoken/MostraItokenActivity;->findView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/itoken/MostraItokenActivity;->textoNumero:Landroid/widget/TextView;

    .line 62
    const v0, 0x7f0e027a

    invoke-direct {p0, v0}, Lcom/itau/empresas/feature/itoken/MostraItokenActivity;->findView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/itoken/MostraItokenActivity;->textoNumeroToken:Landroid/widget/TextView;

    .line 63
    const v0, 0x7f0e0278

    invoke-direct {p0, v0}, Lcom/itau/empresas/feature/itoken/MostraItokenActivity;->findView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/itau/animacao/loading/LoadingView;

    iput-object v0, p0, Lcom/itau/empresas/feature/itoken/MostraItokenActivity;->circulo:Lcom/itau/animacao/loading/LoadingView;

    .line 64
    const v0, 0x7f0e0276

    invoke-direct {p0, v0}, Lcom/itau/empresas/feature/itoken/MostraItokenActivity;->findView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/itau/empresas/feature/itoken/MostraItokenActivity;->primeiroCirculo:Landroid/widget/ImageView;

    .line 65
    const v0, 0x7f0e0277

    invoke-direct {p0, v0}, Lcom/itau/empresas/feature/itoken/MostraItokenActivity;->findView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/itau/empresas/feature/itoken/MostraItokenActivity;->segundoCirculo:Landroid/widget/ImageView;

    .line 67
    const v0, 0x7f0e027b

    invoke-direct {p0, v0}, Lcom/itau/empresas/feature/itoken/MostraItokenActivity;->findView(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/itau/empresas/feature/itoken/MostraItokenActivity$FinishOnClickListener;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/itoken/MostraItokenActivity$FinishOnClickListener;-><init>(Landroid/app/Activity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 69
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/itau/empresas/feature/itoken/MostraItokenActivity;->uiHandler:Landroid/os/Handler;

    .line 70
    return-void
.end method

.method protected onStart()V
    .registers 1

    .line 78
    invoke-super {p0}, Lcom/itau/empresas/ui/activity/BaseActivity;->onStart()V

    .line 79
    invoke-direct {p0}, Lcom/itau/empresas/feature/itoken/MostraItokenActivity;->atualizaOtp()V

    .line 80
    invoke-virtual {p0}, Lcom/itau/empresas/feature/itoken/MostraItokenActivity;->animaArcosExternos()V

    .line 81
    invoke-direct {p0}, Lcom/itau/empresas/feature/itoken/MostraItokenActivity;->disparaEventoAnalytics()V

    .line 82
    return-void
.end method

.method public opkeysConsumidas()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Ljava/lang/String;>;"
        }
    .end annotation

    .line 53
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

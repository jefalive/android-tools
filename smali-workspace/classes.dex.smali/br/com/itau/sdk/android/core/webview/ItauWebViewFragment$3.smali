.class Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment$3;
.super Lbr/com/itau/sdk/android/core/j$b;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# static fields
.field private static final $:[B

.field private static $$:I


# instance fields
.field final synthetic this$0:Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;

.field final synthetic val$urlWithParameters:Ljava/lang/String;


# direct methods
.method private static $(III)Ljava/lang/String;
    .registers 9

    mul-int/lit8 p2, p2, 0x4

    add-int/lit8 p2, p2, 0x5

    mul-int/lit8 p1, p1, 0x1f

    add-int/lit8 p1, p1, 0x55

    new-instance v0, Ljava/lang/String;

    add-int/lit8 p0, p0, 0x4

    const/4 v5, 0x0

    sget-object v4, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment$3;->$:[B

    new-array v1, p2, [B

    if-nez v4, :cond_19

    move v2, p0

    move v3, p1

    :goto_15
    neg-int v3, v3

    add-int/2addr v2, v3

    add-int/lit8 p1, v2, -0x6

    :cond_19
    move v2, v5

    add-int/lit8 v5, v5, 0x1

    int-to-byte v3, p1

    add-int/lit8 p0, p0, 0x1

    aput-byte v3, v1, v2

    if-ne v5, p2, :cond_28

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Ljava/lang/String;-><init>([BI)V

    return-object v0

    :cond_28
    move v2, p1

    aget-byte v3, v4, p0

    goto :goto_15
.end method

.method static constructor <clinit>()V
    .registers 3

    const/16 v0, 0x10

    new-array v0, v0, [B

    fill-array-data v0, :array_e

    sput-object v0, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment$3;->$:[B

    const/16 v0, 0xf5

    sput v0, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment$3;->$$:I

    return-void

    :array_e
    .array-data 1
        0x2bt
        -0x4at
        -0x28t
        0x41t
        -0x5t
        0x8t
        0x13t
        -0x11t
        0x9t
        -0x19t
        -0x2t
        0x3ft
        -0x3ft
        -0x12t
        0x1t
        -0x5t
    .end array-data
.end method

.method constructor <init>(Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;Ljava/lang/String;Ljava/lang/String;Lokhttp3/FormBody$Builder;Ljava/lang/String;)V
    .registers 6

    .line 243
    iput-object p1, p0, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment$3;->this$0:Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;

    iput-object p5, p0, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment$3;->val$urlWithParameters:Ljava/lang/String;

    invoke-direct {p0, p2, p3, p4}, Lbr/com/itau/sdk/android/core/j$b;-><init>(Ljava/lang/String;Ljava/lang/String;Lokhttp3/FormBody$Builder;)V

    return-void
.end method


# virtual methods
.method protected onPageLoaded(Ljava/lang/String;)V
    .registers 8

    .line 246
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment$3;->this$0:Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;

    # getter for: Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->webView:Landroid/webkit/WebView;
    invoke-static {v0}, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->access$100(Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;)Landroid/webkit/WebView;

    move-result-object v0

    iget-object v1, p0, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment$3;->val$urlWithParameters:Ljava/lang/String;

    move-object v2, p1

    sget-object v3, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment$3;->$:[B

    const/16 v4, 0xe

    aget-byte v3, v3, v4

    const/4 v4, 0x3

    invoke-static {v4, v3, v3}, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment$3;->$(III)Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment$3;->$:[B

    const/16 v5, 0xe

    aget-byte v4, v4, v5

    neg-int v4, v4

    add-int/lit8 v5, v4, 0x1

    invoke-static {v4, v5, v5}, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment$3;->$(III)Ljava/lang/String;

    move-result-object v4

    const-string v5, ""

    invoke-virtual/range {v0 .. v5}, Landroid/webkit/WebView;->loadDataWithBaseURL(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 247
    return-void
.end method

.class public Lcom/google/android/gms/ads/internal/formats/zzk;
.super Lcom/google/android/gms/internal/zzcj$zza;

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnTouchListener;
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;
.implements Landroid/view/ViewTreeObserver$OnScrollChangedListener;


# annotations
.annotation runtime Lcom/google/android/gms/internal/zzhb;
.end annotation


# instance fields
.field private zzoQ:Landroid/widget/FrameLayout;

.field private final zzpV:Ljava/lang/Object;

.field private final zzyD:Landroid/widget/FrameLayout;

.field private zzyE:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<Ljava/lang/String;Ljava/lang/ref/WeakReference<Landroid/view/View;>;>;"
        }
    .end annotation
.end field

.field private zzyF:Lcom/google/android/gms/ads/internal/formats/zzb;

.field zzyG:Z

.field zzyH:I

.field zzyI:I

.field private zzyf:Lcom/google/android/gms/ads/internal/formats/zzh;


# direct methods
.method public constructor <init>(Landroid/widget/FrameLayout;Landroid/widget/FrameLayout;)V
    .registers 4

    invoke-direct {p0}, Lcom/google/android/gms/internal/zzcj$zza;-><init>()V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/ads/internal/formats/zzk;->zzpV:Ljava/lang/Object;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/ads/internal/formats/zzk;->zzyE:Ljava/util/Map;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/ads/internal/formats/zzk;->zzyG:Z

    iput-object p1, p0, Lcom/google/android/gms/ads/internal/formats/zzk;->zzyD:Landroid/widget/FrameLayout;

    iput-object p2, p0, Lcom/google/android/gms/ads/internal/formats/zzk;->zzoQ:Landroid/widget/FrameLayout;

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/formats/zzk;->zzyD:Landroid/widget/FrameLayout;

    invoke-static {v0, p0}, Lcom/google/android/gms/internal/zzjk;->zza(Landroid/view/View;Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/formats/zzk;->zzyD:Landroid/widget/FrameLayout;

    invoke-static {v0, p0}, Lcom/google/android/gms/internal/zzjk;->zza(Landroid/view/View;Landroid/view/ViewTreeObserver$OnScrollChangedListener;)V

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/formats/zzk;->zzyD:Landroid/widget/FrameLayout;

    invoke-virtual {v0, p0}, Landroid/widget/FrameLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    return-void
.end method

.method static synthetic zza(Lcom/google/android/gms/ads/internal/formats/zzk;)Landroid/widget/FrameLayout;
    .registers 2

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/formats/zzk;->zzoQ:Landroid/widget/FrameLayout;

    return-object v0
.end method


# virtual methods
.method public destroy()V
    .registers 2

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/formats/zzk;->zzoQ:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->removeAllViews()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/ads/internal/formats/zzk;->zzoQ:Landroid/widget/FrameLayout;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/ads/internal/formats/zzk;->zzyE:Ljava/util/Map;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/ads/internal/formats/zzk;->zzyF:Lcom/google/android/gms/ads/internal/formats/zzb;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/ads/internal/formats/zzk;->zzyf:Lcom/google/android/gms/ads/internal/formats/zzh;

    return-void
.end method

.method getMeasuredHeight()I
    .registers 2

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/formats/zzk;->zzyD:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getMeasuredHeight()I

    move-result v0

    return v0
.end method

.method getMeasuredWidth()I
    .registers 2

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/formats/zzk;->zzyD:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getMeasuredWidth()I

    move-result v0

    return v0
.end method

.method public onClick(Landroid/view/View;)V
    .registers 17
    .param p1, "view"    # Landroid/view/View;

    iget-object v6, p0, Lcom/google/android/gms/ads/internal/formats/zzk;->zzpV:Ljava/lang/Object;

    monitor-enter v6

    :try_start_3
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/formats/zzk;->zzyf:Lcom/google/android/gms/ads/internal/formats/zzh;
    :try_end_5
    .catchall {:try_start_3 .. :try_end_5} :catchall_104

    if-nez v0, :cond_9

    monitor-exit v6

    return-void

    :cond_9
    :try_start_9
    new-instance v7, Lorg/json/JSONObject;

    invoke-direct {v7}, Lorg/json/JSONObject;-><init>()V

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/formats/zzk;->zzyE:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_18
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_94

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Ljava/util/Map$Entry;

    invoke-interface {v9}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Landroid/view/View;

    invoke-virtual {p0, v10}, Lcom/google/android/gms/ads/internal/formats/zzk;->zzj(Landroid/view/View;)Landroid/graphics/Point;

    move-result-object v11

    new-instance v12, Lorg/json/JSONObject;

    invoke-direct {v12}, Lorg/json/JSONObject;-><init>()V
    :try_end_3b
    .catchall {:try_start_9 .. :try_end_3b} :catchall_104

    const-string v0, "width"

    :try_start_3d
    invoke-virtual {v10}, Landroid/view/View;->getWidth()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/android/gms/ads/internal/formats/zzk;->zzq(I)I

    move-result v1

    invoke-virtual {v12, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v0, "height"

    invoke-virtual {v10}, Landroid/view/View;->getHeight()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/android/gms/ads/internal/formats/zzk;->zzq(I)I

    move-result v1

    invoke-virtual {v12, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v0, "x"

    iget v1, v11, Landroid/graphics/Point;->x:I

    invoke-virtual {p0, v1}, Lcom/google/android/gms/ads/internal/formats/zzk;->zzq(I)I

    move-result v1

    invoke-virtual {v12, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v0, "y"

    iget v1, v11, Landroid/graphics/Point;->y:I

    invoke-virtual {p0, v1}, Lcom/google/android/gms/ads/internal/formats/zzk;->zzq(I)I

    move-result v1

    invoke-virtual {v12, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    invoke-interface {v9}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v7, v0, v12}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_74
    .catch Lorg/json/JSONException; {:try_start_3d .. :try_end_74} :catch_75
    .catchall {:try_start_3d .. :try_end_74} :catchall_104

    goto :goto_92

    :catch_75
    move-exception v13

    :try_start_76
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unable to get view rectangle for view "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-interface {v9}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/internal/zzin;->zzaK(Ljava/lang/String;)V

    :goto_92
    goto/16 :goto_18

    :cond_94
    new-instance v8, Lorg/json/JSONObject;

    invoke-direct {v8}, Lorg/json/JSONObject;-><init>()V
    :try_end_99
    .catchall {:try_start_76 .. :try_end_99} :catchall_104

    const-string v0, "x"

    :try_start_9b
    iget v1, p0, Lcom/google/android/gms/ads/internal/formats/zzk;->zzyH:I

    invoke-virtual {p0, v1}, Lcom/google/android/gms/ads/internal/formats/zzk;->zzq(I)I

    move-result v1

    invoke-virtual {v8, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v0, "y"

    iget v1, p0, Lcom/google/android/gms/ads/internal/formats/zzk;->zzyI:I

    invoke-virtual {p0, v1}, Lcom/google/android/gms/ads/internal/formats/zzk;->zzq(I)I

    move-result v1

    invoke-virtual {v8, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
    :try_end_af
    .catch Lorg/json/JSONException; {:try_start_9b .. :try_end_af} :catch_b0
    .catchall {:try_start_9b .. :try_end_af} :catchall_104

    goto :goto_b6

    :catch_b0
    move-exception v9

    const-string v0, "Unable to get click location"

    :try_start_b3
    invoke-static {v0}, Lcom/google/android/gms/internal/zzin;->zzaK(Ljava/lang/String;)V

    :goto_b6
    new-instance v9, Lorg/json/JSONObject;

    invoke-direct {v9}, Lorg/json/JSONObject;-><init>()V
    :try_end_bb
    .catchall {:try_start_b3 .. :try_end_bb} :catchall_104

    const-string v0, "width"

    :try_start_bd
    invoke-virtual {p0}, Lcom/google/android/gms/ads/internal/formats/zzk;->getMeasuredWidth()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/android/gms/ads/internal/formats/zzk;->zzq(I)I

    move-result v1

    invoke-virtual {v9, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v0, "height"

    invoke-virtual {p0}, Lcom/google/android/gms/ads/internal/formats/zzk;->getMeasuredHeight()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/android/gms/ads/internal/formats/zzk;->zzq(I)I

    move-result v1

    invoke-virtual {v9, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
    :try_end_d5
    .catch Lorg/json/JSONException; {:try_start_bd .. :try_end_d5} :catch_d6
    .catchall {:try_start_bd .. :try_end_d5} :catchall_104

    goto :goto_dc

    :catch_d6
    move-exception v10

    const-string v0, "Unable to get native ad view bounding box"

    :try_start_d9
    invoke-static {v0}, Lcom/google/android/gms/internal/zzin;->zzaK(Ljava/lang/String;)V

    :goto_dc
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/formats/zzk;->zzyF:Lcom/google/android/gms/ads/internal/formats/zzb;

    if-eqz v0, :cond_f6

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/formats/zzk;->zzyF:Lcom/google/android/gms/ads/internal/formats/zzb;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/internal/formats/zzb;->zzdI()Landroid/view/ViewGroup;

    move-result-object v0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f6

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/formats/zzk;->zzyf:Lcom/google/android/gms/ads/internal/formats/zzh;

    const-string v1, "1007"

    invoke-interface {v0, v1, v7, v8, v9}, Lcom/google/android/gms/ads/internal/formats/zzh;->zza(Ljava/lang/String;Lorg/json/JSONObject;Lorg/json/JSONObject;Lorg/json/JSONObject;)V

    goto :goto_102

    :cond_f6
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/formats/zzk;->zzyf:Lcom/google/android/gms/ads/internal/formats/zzh;

    move-object/from16 v1, p1

    iget-object v2, p0, Lcom/google/android/gms/ads/internal/formats/zzk;->zzyE:Ljava/util/Map;

    move-object v3, v7

    move-object v4, v8

    move-object v5, v9

    invoke-interface/range {v0 .. v5}, Lcom/google/android/gms/ads/internal/formats/zzh;->zza(Landroid/view/View;Ljava/util/Map;Lorg/json/JSONObject;Lorg/json/JSONObject;Lorg/json/JSONObject;)V
    :try_end_102
    .catchall {:try_start_d9 .. :try_end_102} :catchall_104

    :goto_102
    monitor-exit v6

    goto :goto_107

    :catchall_104
    move-exception v14

    monitor-exit v6

    throw v14

    :goto_107
    return-void
.end method

.method public onGlobalLayout()V
    .registers 7

    iget-object v2, p0, Lcom/google/android/gms/ads/internal/formats/zzk;->zzpV:Ljava/lang/Object;

    monitor-enter v2

    :try_start_3
    iget-boolean v0, p0, Lcom/google/android/gms/ads/internal/formats/zzk;->zzyG:Z

    if-eqz v0, :cond_20

    invoke-virtual {p0}, Lcom/google/android/gms/ads/internal/formats/zzk;->getMeasuredWidth()I

    move-result v3

    invoke-virtual {p0}, Lcom/google/android/gms/ads/internal/formats/zzk;->getMeasuredHeight()I

    move-result v4

    if-eqz v3, :cond_20

    if-eqz v4, :cond_20

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/formats/zzk;->zzoQ:Landroid/widget/FrameLayout;

    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v1, v3, v4}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/ads/internal/formats/zzk;->zzyG:Z

    :cond_20
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/formats/zzk;->zzyf:Lcom/google/android/gms/ads/internal/formats/zzh;

    if-eqz v0, :cond_2b

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/formats/zzk;->zzyf:Lcom/google/android/gms/ads/internal/formats/zzh;

    iget-object v1, p0, Lcom/google/android/gms/ads/internal/formats/zzk;->zzyD:Landroid/widget/FrameLayout;

    invoke-interface {v0, v1}, Lcom/google/android/gms/ads/internal/formats/zzh;->zzh(Landroid/view/View;)V
    :try_end_2b
    .catchall {:try_start_3 .. :try_end_2b} :catchall_2d

    :cond_2b
    monitor-exit v2

    goto :goto_30

    :catchall_2d
    move-exception v5

    monitor-exit v2

    throw v5

    :goto_30
    return-void
.end method

.method public onScrollChanged()V
    .registers 5

    iget-object v2, p0, Lcom/google/android/gms/ads/internal/formats/zzk;->zzpV:Ljava/lang/Object;

    monitor-enter v2

    :try_start_3
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/formats/zzk;->zzyf:Lcom/google/android/gms/ads/internal/formats/zzh;

    if-eqz v0, :cond_e

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/formats/zzk;->zzyf:Lcom/google/android/gms/ads/internal/formats/zzh;

    iget-object v1, p0, Lcom/google/android/gms/ads/internal/formats/zzk;->zzyD:Landroid/widget/FrameLayout;

    invoke-interface {v0, v1}, Lcom/google/android/gms/ads/internal/formats/zzh;->zzh(Landroid/view/View;)V
    :try_end_e
    .catchall {:try_start_3 .. :try_end_e} :catchall_10

    :cond_e
    monitor-exit v2

    goto :goto_13

    :catchall_10
    move-exception v3

    monitor-exit v2

    throw v3

    :goto_13
    return-void
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .registers 9
    .param p1, "view"    # Landroid/view/View;
    .param p2, "motionEvent"    # Landroid/view/MotionEvent;

    iget-object v2, p0, Lcom/google/android/gms/ads/internal/formats/zzk;->zzpV:Ljava/lang/Object;

    monitor-enter v2

    :try_start_3
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/formats/zzk;->zzyf:Lcom/google/android/gms/ads/internal/formats/zzh;
    :try_end_5
    .catchall {:try_start_3 .. :try_end_5} :catchall_2e

    if-nez v0, :cond_a

    monitor-exit v2

    const/4 v0, 0x0

    return v0

    :cond_a
    :try_start_a
    invoke-virtual {p0, p2}, Lcom/google/android/gms/ads/internal/formats/zzk;->zzc(Landroid/view/MotionEvent;)Landroid/graphics/Point;

    move-result-object v3

    iget v0, v3, Landroid/graphics/Point;->x:I

    iput v0, p0, Lcom/google/android/gms/ads/internal/formats/zzk;->zzyH:I

    iget v0, v3, Landroid/graphics/Point;->y:I

    iput v0, p0, Lcom/google/android/gms/ads/internal/formats/zzk;->zzyI:I

    invoke-static {p2}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v4

    iget v0, v3, Landroid/graphics/Point;->x:I

    int-to-float v0, v0

    iget v1, v3, Landroid/graphics/Point;->y:I

    int-to-float v1, v1

    invoke-virtual {v4, v0, v1}, Landroid/view/MotionEvent;->setLocation(FF)V

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/formats/zzk;->zzyf:Lcom/google/android/gms/ads/internal/formats/zzh;

    invoke-interface {v0, v4}, Lcom/google/android/gms/ads/internal/formats/zzh;->zzb(Landroid/view/MotionEvent;)V

    invoke-virtual {v4}, Landroid/view/MotionEvent;->recycle()V
    :try_end_2b
    .catchall {:try_start_a .. :try_end_2b} :catchall_2e

    monitor-exit v2

    const/4 v0, 0x0

    return v0

    :catchall_2e
    move-exception v5

    monitor-exit v2

    throw v5
.end method

.method public zzK(Ljava/lang/String;)Lcom/google/android/gms/dynamic/zzd;
    .registers 6

    iget-object v1, p0, Lcom/google/android/gms/ads/internal/formats/zzk;->zzpV:Ljava/lang/Object;

    monitor-enter v1

    :try_start_3
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/formats/zzk;->zzyE:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Ljava/lang/ref/WeakReference;

    if-nez v2, :cond_10

    const/4 v0, 0x0

    goto :goto_16

    :cond_10
    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    :goto_16
    invoke-static {v0}, Lcom/google/android/gms/dynamic/zze;->zzC(Ljava/lang/Object;)Lcom/google/android/gms/dynamic/zzd;
    :try_end_19
    .catchall {:try_start_3 .. :try_end_19} :catchall_1c

    move-result-object v0

    monitor-exit v1

    return-object v0

    :catchall_1c
    move-exception v3

    monitor-exit v1

    throw v3
.end method

.method public zza(Lcom/google/android/gms/dynamic/zzd;)V
    .registers 10

    iget-object v4, p0, Lcom/google/android/gms/ads/internal/formats/zzk;->zzpV:Ljava/lang/Object;

    monitor-enter v4

    const/4 v0, 0x1

    :try_start_4
    iput-boolean v0, p0, Lcom/google/android/gms/ads/internal/formats/zzk;->zzyG:Z

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/ads/internal/formats/zzk;->zzi(Landroid/view/View;)V

    invoke-static {p1}, Lcom/google/android/gms/dynamic/zze;->zzp(Lcom/google/android/gms/dynamic/zzd;)Ljava/lang/Object;

    move-result-object v5

    instance-of v0, v5, Lcom/google/android/gms/ads/internal/formats/zzi;

    if-nez v0, :cond_19

    const-string v0, "Not an instance of native engine. This is most likely a transient error"

    invoke-static {v0}, Lcom/google/android/gms/internal/zzin;->zzaK(Ljava/lang/String;)V
    :try_end_17
    .catchall {:try_start_4 .. :try_end_17} :catchall_82

    monitor-exit v4

    return-void

    :cond_19
    move-object v6, v5

    :try_start_1a
    check-cast v6, Lcom/google/android/gms/ads/internal/formats/zzi;

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/formats/zzk;->zzyf:Lcom/google/android/gms/ads/internal/formats/zzh;

    instance-of v0, v0, Lcom/google/android/gms/ads/internal/formats/zzg;

    if-eqz v0, :cond_34

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/formats/zzk;->zzyf:Lcom/google/android/gms/ads/internal/formats/zzh;

    check-cast v0, Lcom/google/android/gms/ads/internal/formats/zzg;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/internal/formats/zzg;->zzdP()Z

    move-result v0

    if-eqz v0, :cond_34

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/formats/zzk;->zzyf:Lcom/google/android/gms/ads/internal/formats/zzh;

    check-cast v0, Lcom/google/android/gms/ads/internal/formats/zzg;

    invoke-virtual {v0, v6}, Lcom/google/android/gms/ads/internal/formats/zzg;->zzc(Lcom/google/android/gms/ads/internal/formats/zzh;)V

    goto :goto_44

    :cond_34
    iput-object v6, p0, Lcom/google/android/gms/ads/internal/formats/zzk;->zzyf:Lcom/google/android/gms/ads/internal/formats/zzh;

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/formats/zzk;->zzyf:Lcom/google/android/gms/ads/internal/formats/zzh;

    instance-of v0, v0, Lcom/google/android/gms/ads/internal/formats/zzg;

    if-eqz v0, :cond_44

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/formats/zzk;->zzyf:Lcom/google/android/gms/ads/internal/formats/zzh;

    check-cast v0, Lcom/google/android/gms/ads/internal/formats/zzg;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/ads/internal/formats/zzg;->zzc(Lcom/google/android/gms/ads/internal/formats/zzh;)V

    :cond_44
    :goto_44
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/formats/zzk;->zzoQ:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->removeAllViews()V

    invoke-virtual {p0, v6}, Lcom/google/android/gms/ads/internal/formats/zzk;->zzd(Lcom/google/android/gms/ads/internal/formats/zzi;)Lcom/google/android/gms/ads/internal/formats/zzb;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/ads/internal/formats/zzk;->zzyF:Lcom/google/android/gms/ads/internal/formats/zzb;

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/formats/zzk;->zzyF:Lcom/google/android/gms/ads/internal/formats/zzb;

    if-eqz v0, :cond_6c

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/formats/zzk;->zzyE:Ljava/util/Map;

    const-string v1, "1007"

    new-instance v2, Ljava/lang/ref/WeakReference;

    iget-object v3, p0, Lcom/google/android/gms/ads/internal/formats/zzk;->zzyF:Lcom/google/android/gms/ads/internal/formats/zzb;

    invoke-virtual {v3}, Lcom/google/android/gms/ads/internal/formats/zzb;->zzdI()Landroid/view/ViewGroup;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/formats/zzk;->zzoQ:Landroid/widget/FrameLayout;

    iget-object v1, p0, Lcom/google/android/gms/ads/internal/formats/zzk;->zzyF:Lcom/google/android/gms/ads/internal/formats/zzb;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    :cond_6c
    sget-object v0, Lcom/google/android/gms/internal/zzir;->zzMc:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/gms/ads/internal/formats/zzk$1;

    invoke-direct {v1, p0, v6}, Lcom/google/android/gms/ads/internal/formats/zzk$1;-><init>(Lcom/google/android/gms/ads/internal/formats/zzk;Lcom/google/android/gms/ads/internal/formats/zzi;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/formats/zzk;->zzyD:Landroid/widget/FrameLayout;

    invoke-virtual {v6, v0}, Lcom/google/android/gms/ads/internal/formats/zzi;->zzg(Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/formats/zzk;->zzyD:Landroid/widget/FrameLayout;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/ads/internal/formats/zzk;->zzi(Landroid/view/View;)V
    :try_end_80
    .catchall {:try_start_1a .. :try_end_80} :catchall_82

    monitor-exit v4

    goto :goto_85

    :catchall_82
    move-exception v7

    monitor-exit v4

    throw v7

    :goto_85
    return-void
.end method

.method public zza(Ljava/lang/String;Lcom/google/android/gms/dynamic/zzd;)V
    .registers 8

    invoke-static {p2}, Lcom/google/android/gms/dynamic/zze;->zzp(Lcom/google/android/gms/dynamic/zzd;)Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Landroid/view/View;

    iget-object v3, p0, Lcom/google/android/gms/ads/internal/formats/zzk;->zzpV:Ljava/lang/Object;

    monitor-enter v3

    if-nez v2, :cond_12

    :try_start_c
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/formats/zzk;->zzyE:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_22

    :cond_12
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/formats/zzk;->zzyE:Ljava/util/Map;

    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, v2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v2, p0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    invoke-virtual {v2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V
    :try_end_22
    .catchall {:try_start_c .. :try_end_22} :catchall_24

    :goto_22
    monitor-exit v3

    goto :goto_27

    :catchall_24
    move-exception v4

    monitor-exit v3

    throw v4

    :goto_27
    return-void
.end method

.method zzc(Landroid/view/MotionEvent;)Landroid/graphics/Point;
    .registers 8

    const/4 v0, 0x2

    new-array v3, v0, [I

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/formats/zzk;->zzyD:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v3}, Landroid/widget/FrameLayout;->getLocationOnScreen([I)V

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v0

    const/4 v1, 0x0

    aget v1, v3, v1

    int-to-float v1, v1

    sub-float v4, v0, v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v0

    const/4 v1, 0x1

    aget v1, v3, v1

    int-to-float v1, v1

    sub-float v5, v0, v1

    new-instance v0, Landroid/graphics/Point;

    float-to-int v1, v4

    float-to-int v2, v5

    invoke-direct {v0, v1, v2}, Landroid/graphics/Point;-><init>(II)V

    return-object v0
.end method

.method zzd(Lcom/google/android/gms/ads/internal/formats/zzi;)Lcom/google/android/gms/ads/internal/formats/zzb;
    .registers 3

    invoke-virtual {p1, p0}, Lcom/google/android/gms/ads/internal/formats/zzi;->zza(Landroid/view/View$OnClickListener;)Lcom/google/android/gms/ads/internal/formats/zzb;

    move-result-object v0

    return-object v0
.end method

.method zzi(Landroid/view/View;)V
    .registers 4

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/formats/zzk;->zzyf:Lcom/google/android/gms/ads/internal/formats/zzh;

    if-eqz v0, :cond_1b

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/formats/zzk;->zzyf:Lcom/google/android/gms/ads/internal/formats/zzh;

    instance-of v0, v0, Lcom/google/android/gms/ads/internal/formats/zzg;

    if-eqz v0, :cond_14

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/formats/zzk;->zzyf:Lcom/google/android/gms/ads/internal/formats/zzh;

    check-cast v0, Lcom/google/android/gms/ads/internal/formats/zzg;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/internal/formats/zzg;->zzdQ()Lcom/google/android/gms/ads/internal/formats/zzh;

    move-result-object v1

    goto :goto_16

    :cond_14
    iget-object v1, p0, Lcom/google/android/gms/ads/internal/formats/zzk;->zzyf:Lcom/google/android/gms/ads/internal/formats/zzh;

    :goto_16
    if-eqz v1, :cond_1b

    invoke-interface {v1, p1}, Lcom/google/android/gms/ads/internal/formats/zzh;->zzi(Landroid/view/View;)V

    :cond_1b
    return-void
.end method

.method zzj(Landroid/view/View;)Landroid/graphics/Point;
    .registers 8

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/formats/zzk;->zzyF:Lcom/google/android/gms/ads/internal/formats/zzb;

    if-eqz v0, :cond_3c

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/formats/zzk;->zzyF:Lcom/google/android/gms/ads/internal/formats/zzb;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/internal/formats/zzb;->zzdI()Landroid/view/ViewGroup;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3c

    new-instance v4, Landroid/graphics/Point;

    invoke-direct {v4}, Landroid/graphics/Point;-><init>()V

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/formats/zzk;->zzyD:Landroid/widget/FrameLayout;

    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    invoke-virtual {v0, v1, v4}, Landroid/widget/FrameLayout;->getGlobalVisibleRect(Landroid/graphics/Rect;Landroid/graphics/Point;)Z

    new-instance v5, Landroid/graphics/Point;

    invoke-direct {v5}, Landroid/graphics/Point;-><init>()V

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    invoke-virtual {p1, v0, v5}, Landroid/view/View;->getGlobalVisibleRect(Landroid/graphics/Rect;Landroid/graphics/Point;)Z

    new-instance v0, Landroid/graphics/Point;

    iget v1, v5, Landroid/graphics/Point;->x:I

    iget v2, v4, Landroid/graphics/Point;->x:I

    sub-int/2addr v1, v2

    iget v2, v5, Landroid/graphics/Point;->y:I

    iget v3, v4, Landroid/graphics/Point;->y:I

    sub-int/2addr v2, v3

    invoke-direct {v0, v1, v2}, Landroid/graphics/Point;-><init>(II)V

    return-object v0

    :cond_3c
    new-instance v4, Landroid/graphics/Point;

    invoke-direct {v4}, Landroid/graphics/Point;-><init>()V

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    invoke-virtual {p1, v0, v4}, Landroid/view/View;->getGlobalVisibleRect(Landroid/graphics/Rect;Landroid/graphics/Point;)Z

    return-object v4
.end method

.method zzq(I)I
    .registers 4

    invoke-static {}, Lcom/google/android/gms/ads/internal/client/zzn;->zzcS()Lcom/google/android/gms/ads/internal/util/client/zza;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/ads/internal/formats/zzk;->zzyf:Lcom/google/android/gms/ads/internal/formats/zzh;

    invoke-interface {v1}, Lcom/google/android/gms/ads/internal/formats/zzh;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Lcom/google/android/gms/ads/internal/util/client/zza;->zzc(Landroid/content/Context;I)I

    move-result v0

    return v0
.end method

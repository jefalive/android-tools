.class public final Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity_;
.super Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity;
.source "LisConsultaDetalheActivity_.java"

# interfaces
.implements Lorg/androidannotations/api/view/HasViews;
.implements Lorg/androidannotations/api/view/OnViewChangedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity_$IntentBuilder_;
    }
.end annotation


# instance fields
.field private final onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;


# direct methods
.method public constructor <init>()V
    .registers 2

    .line 39
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity;-><init>()V

    .line 43
    new-instance v0, Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-direct {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    return-void
.end method

.method static synthetic access$001(Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity_;)V
    .registers 1
    .param p0, "x0"    # Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity_;

    .line 39
    invoke-super {p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity;->mostraDialogoDeProgresso()V

    return-void
.end method

.method static synthetic access$101(Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity_;)V
    .registers 1
    .param p0, "x0"    # Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity_;

    .line 39
    invoke-super {p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity;->escondeDialogoDeProgresso()V

    return-void
.end method

.method private init_(Landroid/os/Bundle;)V
    .registers 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 58
    new-instance v0, Lcom/itau/empresas/Preferences_;

    invoke-direct {v0, p0}, Lcom/itau/empresas/Preferences_;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity_;->prefs:Lcom/itau/empresas/Preferences_;

    .line 59
    invoke-static {p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->registerOnViewChangedListener(Lorg/androidannotations/api/view/OnViewChangedListener;)V

    .line 60
    invoke-static {}, Lcom/itau/empresas/CustomApplication_;->getInstance()Lcom/itau/empresas/CustomApplication;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity_;->application:Lcom/itau/empresas/CustomApplication;

    .line 61
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity_;->injectExtras_()V

    .line 62
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity_;->afterInject()V

    .line 63
    return-void
.end method

.method private injectExtras_()V
    .registers 3

    .line 125
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity_;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 126
    .local v1, "extras_":Landroid/os/Bundle;
    if-eqz v1, :cond_3e

    .line 127
    const-string v0, "comprovante"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 128
    const-string v0, "comprovante"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/api/model/ComprovanteServiceVO;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity_;->comprovante:Lcom/itau/empresas/api/model/ComprovanteServiceVO;

    .line 130
    :cond_1c
    const-string v0, "lisConsultaDetalhadaRespostaVO"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2e

    .line 131
    const-string v0, "lisConsultaDetalhadaRespostaVO"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/feature/credito/lis/model/LisConsultaDetalhadaRespostaVO;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity_;->lisConsultaDetalhadaRespostaVO:Lcom/itau/empresas/feature/credito/lis/model/LisConsultaDetalhadaRespostaVO;

    .line 133
    :cond_2e
    const-string v0, "tipoComprovante"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3e

    .line 134
    const-string v0, "tipoComprovante"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity_;->tipoComprovante:Ljava/lang/String;

    .line 137
    :cond_3e
    return-void
.end method


# virtual methods
.method public escondeDialogoDeProgresso()V
    .registers 5

    .line 162
    const-string v0, ""

    new-instance v1, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity_$3;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity_$3;-><init>(Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity_;)V

    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3}, Lorg/androidannotations/api/UiThreadExecutor;->runTask(Ljava/lang/String;Ljava/lang/Runnable;J)V

    .line 170
    return-void
.end method

.method public mostraDialogoDeProgresso()V
    .registers 5
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "CommitTransaction"
        }
    .end annotation

    .line 150
    const-string v0, ""

    new-instance v1, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity_$2;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity_$2;-><init>(Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity_;)V

    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3}, Lorg/androidannotations/api/UiThreadExecutor;->runTask(Ljava/lang/String;Ljava/lang/Runnable;J)V

    .line 158
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 50
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-static {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    move-result-object v1

    .line 51
    .local v1, "previousNotifier":Lorg/androidannotations/api/view/OnViewChangedNotifier;
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity_;->init_(Landroid/os/Bundle;)V

    .line 52
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity;->onCreate(Landroid/os/Bundle;)V

    .line 53
    invoke-static {v1}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 54
    const v0, 0x7f030048

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity_;->setContentView(I)V

    .line 55
    return-void
.end method

.method public onViewChanged(Lorg/androidannotations/api/view/HasViews;)V
    .registers 4
    .param p1, "hasViews"    # Lorg/androidannotations/api/view/HasViews;

    .line 97
    const v0, 0x7f0e00b6

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/Toolbar;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity_;->toolbar:Landroid/support/v7/widget/Toolbar;

    .line 98
    const v0, 0x7f0e01fa

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity_;->comprovanteDetalheLayoutPrincipal:Landroid/widget/LinearLayout;

    .line 99
    const v0, 0x7f0e01fb

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ScrollView;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity_;->comporvanteDetalhe:Landroid/widget/ScrollView;

    .line 100
    const v0, 0x7f0e0201

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity_;->comprovanteValores:Landroid/widget/LinearLayout;

    .line 101
    const v0, 0x7f0e0202

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity_;->warning:Landroid/view/View;

    .line 102
    const v0, 0x7f0e020d

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity_;->textoDadoscontroleDescricao:Landroid/widget/TextView;

    .line 103
    const v0, 0x7f0e01fe

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity_;->comprovanteHeader:Landroid/widget/LinearLayout;

    .line 104
    const v0, 0x7f0e01fd

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity_;->descricaoProduto:Landroid/widget/TextView;

    .line 105
    const v0, 0x7f0e0208

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity_;->lisAdicionalAtencao:Landroid/widget/TextView;

    .line 106
    const v0, 0x7f0e0207

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity_;->lisRecevivelAtencao:Landroid/widget/TextView;

    .line 107
    const v0, 0x7f0e01fc

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity_;->rlLisConsultaDetalheHeader:Landroid/view/View;

    .line 108
    const v0, 0x7f0e01f0

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 110
    .local v1, "view_botao_compartilhar":Landroid/view/View;
    if-eqz v1, :cond_86

    .line 111
    new-instance v0, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity_$1;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity_$1;-><init>(Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity_;)V

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 120
    :cond_86
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity_;->aoCarregarTela()V

    .line 121
    return-void
.end method

.method public setContentView(I)V
    .registers 3
    .param p1, "layoutResID"    # I

    .line 67
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity;->setContentView(I)V

    .line 68
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 69
    return-void
.end method

.method public setContentView(Landroid/view/View;)V
    .registers 3
    .param p1, "view"    # Landroid/view/View;

    .line 79
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity;->setContentView(Landroid/view/View;)V

    .line 80
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 81
    return-void
.end method

.method public setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .registers 4
    .param p1, "view"    # Landroid/view/View;
    .param p2, "params"    # Landroid/view/ViewGroup$LayoutParams;

    .line 73
    invoke-super {p0, p1, p2}, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity;->setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 74
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 75
    return-void
.end method

.method public setIntent(Landroid/content/Intent;)V
    .registers 2
    .param p1, "newIntent"    # Landroid/content/Intent;

    .line 141
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity;->setIntent(Landroid/content/Intent;)V

    .line 142
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity_;->injectExtras_()V

    .line 143
    return-void
.end method

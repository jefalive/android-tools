.class Lbr/com/itau/textovoz/ui/view/custom/PulseLayout$PulseView;
.super Landroid/view/View;
.source "PulseLayout.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lbr/com/itau/textovoz/ui/view/custom/PulseLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PulseView"
.end annotation


# instance fields
.field final synthetic this$0:Lbr/com/itau/textovoz/ui/view/custom/PulseLayout;


# direct methods
.method constructor <init>(Lbr/com/itau/textovoz/ui/view/custom/PulseLayout;Landroid/content/Context;)V
    .registers 3
    .param p2, "context"    # Landroid/content/Context;

    .line 318
    iput-object p1, p0, Lbr/com/itau/textovoz/ui/view/custom/PulseLayout$PulseView;->this$0:Lbr/com/itau/textovoz/ui/view/custom/PulseLayout;

    .line 319
    invoke-direct {p0, p2}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 320
    return-void
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .registers 6
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .line 324
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/view/custom/PulseLayout$PulseView;->this$0:Lbr/com/itau/textovoz/ui/view/custom/PulseLayout;

    # getter for: Lbr/com/itau/textovoz/ui/view/custom/PulseLayout;->centerX:F
    invoke-static {v0}, Lbr/com/itau/textovoz/ui/view/custom/PulseLayout;->access$000(Lbr/com/itau/textovoz/ui/view/custom/PulseLayout;)F

    move-result v0

    iget-object v1, p0, Lbr/com/itau/textovoz/ui/view/custom/PulseLayout$PulseView;->this$0:Lbr/com/itau/textovoz/ui/view/custom/PulseLayout;

    # getter for: Lbr/com/itau/textovoz/ui/view/custom/PulseLayout;->centerY:F
    invoke-static {v1}, Lbr/com/itau/textovoz/ui/view/custom/PulseLayout;->access$100(Lbr/com/itau/textovoz/ui/view/custom/PulseLayout;)F

    move-result v1

    iget-object v2, p0, Lbr/com/itau/textovoz/ui/view/custom/PulseLayout$PulseView;->this$0:Lbr/com/itau/textovoz/ui/view/custom/PulseLayout;

    # getter for: Lbr/com/itau/textovoz/ui/view/custom/PulseLayout;->radius:F
    invoke-static {v2}, Lbr/com/itau/textovoz/ui/view/custom/PulseLayout;->access$200(Lbr/com/itau/textovoz/ui/view/custom/PulseLayout;)F

    move-result v2

    iget-object v3, p0, Lbr/com/itau/textovoz/ui/view/custom/PulseLayout$PulseView;->this$0:Lbr/com/itau/textovoz/ui/view/custom/PulseLayout;

    # getter for: Lbr/com/itau/textovoz/ui/view/custom/PulseLayout;->paint:Landroid/graphics/Paint;
    invoke-static {v3}, Lbr/com/itau/textovoz/ui/view/custom/PulseLayout;->access$300(Lbr/com/itau/textovoz/ui/view/custom/PulseLayout;)Landroid/graphics/Paint;

    move-result-object v3

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 325
    return-void
.end method

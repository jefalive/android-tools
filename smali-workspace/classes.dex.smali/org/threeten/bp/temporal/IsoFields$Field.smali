.class abstract enum Lorg/threeten/bp/temporal/IsoFields$Field;
.super Ljava/lang/Enum;
.source "IsoFields.java"

# interfaces
.implements Lorg/threeten/bp/temporal/TemporalField;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/threeten/bp/temporal/IsoFields;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x440a
    name = "Field"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<Lorg/threeten/bp/temporal/IsoFields$Field;>;Lorg/threeten/bp/temporal/TemporalField;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lorg/threeten/bp/temporal/IsoFields$Field;

.field public static final enum DAY_OF_QUARTER:Lorg/threeten/bp/temporal/IsoFields$Field;

.field private static final QUARTER_DAYS:[I

.field public static final enum QUARTER_OF_YEAR:Lorg/threeten/bp/temporal/IsoFields$Field;

.field public static final enum WEEK_BASED_YEAR:Lorg/threeten/bp/temporal/IsoFields$Field;

.field public static final enum WEEK_OF_WEEK_BASED_YEAR:Lorg/threeten/bp/temporal/IsoFields$Field;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .line 206
    new-instance v0, Lorg/threeten/bp/temporal/IsoFields$Field$1;

    const-string v1, "DAY_OF_QUARTER"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lorg/threeten/bp/temporal/IsoFields$Field$1;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/threeten/bp/temporal/IsoFields$Field;->DAY_OF_QUARTER:Lorg/threeten/bp/temporal/IsoFields$Field;

    .line 298
    new-instance v0, Lorg/threeten/bp/temporal/IsoFields$Field$2;

    const-string v1, "QUARTER_OF_YEAR"

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lorg/threeten/bp/temporal/IsoFields$Field$2;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/threeten/bp/temporal/IsoFields$Field;->QUARTER_OF_YEAR:Lorg/threeten/bp/temporal/IsoFields$Field;

    .line 339
    new-instance v0, Lorg/threeten/bp/temporal/IsoFields$Field$3;

    const-string v1, "WEEK_OF_WEEK_BASED_YEAR"

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2}, Lorg/threeten/bp/temporal/IsoFields$Field$3;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/threeten/bp/temporal/IsoFields$Field;->WEEK_OF_WEEK_BASED_YEAR:Lorg/threeten/bp/temporal/IsoFields$Field;

    .line 425
    new-instance v0, Lorg/threeten/bp/temporal/IsoFields$Field$4;

    const-string v1, "WEEK_BASED_YEAR"

    const/4 v2, 0x3

    invoke-direct {v0, v1, v2}, Lorg/threeten/bp/temporal/IsoFields$Field$4;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/threeten/bp/temporal/IsoFields$Field;->WEEK_BASED_YEAR:Lorg/threeten/bp/temporal/IsoFields$Field;

    .line 205
    const/4 v0, 0x4

    new-array v0, v0, [Lorg/threeten/bp/temporal/IsoFields$Field;

    sget-object v1, Lorg/threeten/bp/temporal/IsoFields$Field;->DAY_OF_QUARTER:Lorg/threeten/bp/temporal/IsoFields$Field;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sget-object v1, Lorg/threeten/bp/temporal/IsoFields$Field;->QUARTER_OF_YEAR:Lorg/threeten/bp/temporal/IsoFields$Field;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    sget-object v1, Lorg/threeten/bp/temporal/IsoFields$Field;->WEEK_OF_WEEK_BASED_YEAR:Lorg/threeten/bp/temporal/IsoFields$Field;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    sget-object v1, Lorg/threeten/bp/temporal/IsoFields$Field;->WEEK_BASED_YEAR:Lorg/threeten/bp/temporal/IsoFields$Field;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    sput-object v0, Lorg/threeten/bp/temporal/IsoFields$Field;->$VALUES:[Lorg/threeten/bp/temporal/IsoFields$Field;

    .line 490
    const/16 v0, 0x8

    new-array v0, v0, [I

    fill-array-data v0, :array_4c

    sput-object v0, Lorg/threeten/bp/temporal/IsoFields$Field;->QUARTER_DAYS:[I

    return-void

    nop

    :array_4c
    .array-data 4
        0x0
        0x5a
        0xb5
        0x111
        0x0
        0x5b
        0xb6
        0x112
    .end array-data
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 205
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;ILorg/threeten/bp/temporal/IsoFields$1;)V
    .registers 4
    .param p1, "x0"    # Ljava/lang/String;
    .param p2, "x1"    # I
    .param p3, "x2"    # Lorg/threeten/bp/temporal/IsoFields$1;

    .line 205
    invoke-direct {p0, p1, p2}, Lorg/threeten/bp/temporal/IsoFields$Field;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method static synthetic access$100(Lorg/threeten/bp/temporal/TemporalAccessor;)Z
    .registers 2
    .param p0, "x0"    # Lorg/threeten/bp/temporal/TemporalAccessor;

    .line 205
    invoke-static {p0}, Lorg/threeten/bp/temporal/IsoFields$Field;->isIso(Lorg/threeten/bp/temporal/TemporalAccessor;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$200()[I
    .registers 1

    .line 205
    sget-object v0, Lorg/threeten/bp/temporal/IsoFields$Field;->QUARTER_DAYS:[I

    return-object v0
.end method

.method static synthetic access$300(Lorg/threeten/bp/LocalDate;)Lorg/threeten/bp/temporal/ValueRange;
    .registers 2
    .param p0, "x0"    # Lorg/threeten/bp/LocalDate;

    .line 205
    invoke-static {p0}, Lorg/threeten/bp/temporal/IsoFields$Field;->getWeekRange(Lorg/threeten/bp/LocalDate;)Lorg/threeten/bp/temporal/ValueRange;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$400(Lorg/threeten/bp/LocalDate;)I
    .registers 2
    .param p0, "x0"    # Lorg/threeten/bp/LocalDate;

    .line 205
    invoke-static {p0}, Lorg/threeten/bp/temporal/IsoFields$Field;->getWeek(Lorg/threeten/bp/LocalDate;)I

    move-result v0

    return v0
.end method

.method static synthetic access$500(Lorg/threeten/bp/LocalDate;)I
    .registers 2
    .param p0, "x0"    # Lorg/threeten/bp/LocalDate;

    .line 205
    invoke-static {p0}, Lorg/threeten/bp/temporal/IsoFields$Field;->getWeekBasedYear(Lorg/threeten/bp/LocalDate;)I

    move-result v0

    return v0
.end method

.method static synthetic access$600(I)I
    .registers 2
    .param p0, "x0"    # I

    .line 205
    invoke-static {p0}, Lorg/threeten/bp/temporal/IsoFields$Field;->getWeekRange(I)I

    move-result v0

    return v0
.end method

.method private static getWeek(Lorg/threeten/bp/LocalDate;)I
    .registers 11
    .param p0, "date"    # Lorg/threeten/bp/LocalDate;

    .line 520
    invoke-virtual {p0}, Lorg/threeten/bp/LocalDate;->getDayOfWeek()Lorg/threeten/bp/DayOfWeek;

    move-result-object v0

    invoke-virtual {v0}, Lorg/threeten/bp/DayOfWeek;->ordinal()I

    move-result v3

    .line 521
    .local v3, "dow0":I
    invoke-virtual {p0}, Lorg/threeten/bp/LocalDate;->getDayOfYear()I

    move-result v0

    add-int/lit8 v4, v0, -0x1

    .line 522
    .local v4, "doy0":I
    rsub-int/lit8 v0, v3, 0x3

    add-int v5, v4, v0

    .line 523
    .local v5, "doyThu0":I
    div-int/lit8 v6, v5, 0x7

    .line 524
    .local v6, "alignedWeek":I
    mul-int/lit8 v0, v6, 0x7

    sub-int v7, v5, v0

    .line 525
    .local v7, "firstThuDoy0":I
    add-int/lit8 v8, v7, -0x3

    .line 526
    .local v8, "firstMonDoy0":I
    const/4 v0, -0x3

    if-ge v8, v0, :cond_1f

    .line 527
    add-int/lit8 v8, v8, 0x7

    .line 529
    :cond_1f
    if-ge v4, v8, :cond_37

    .line 530
    const/16 v0, 0xb4

    invoke-virtual {p0, v0}, Lorg/threeten/bp/LocalDate;->withDayOfYear(I)Lorg/threeten/bp/LocalDate;

    move-result-object v0

    const-wide/16 v1, 0x1

    invoke-virtual {v0, v1, v2}, Lorg/threeten/bp/LocalDate;->minusYears(J)Lorg/threeten/bp/LocalDate;

    move-result-object v0

    invoke-static {v0}, Lorg/threeten/bp/temporal/IsoFields$Field;->getWeekRange(Lorg/threeten/bp/LocalDate;)Lorg/threeten/bp/temporal/ValueRange;

    move-result-object v0

    invoke-virtual {v0}, Lorg/threeten/bp/temporal/ValueRange;->getMaximum()J

    move-result-wide v0

    long-to-int v0, v0

    return v0

    .line 532
    :cond_37
    sub-int v0, v4, v8

    div-int/lit8 v0, v0, 0x7

    add-int/lit8 v9, v0, 0x1

    .line 533
    .local v9, "week":I
    const/16 v0, 0x35

    if-ne v9, v0, :cond_53

    .line 534
    const/4 v0, -0x3

    if-eq v8, v0, :cond_4d

    const/4 v0, -0x2

    if-ne v8, v0, :cond_4f

    invoke-virtual {p0}, Lorg/threeten/bp/LocalDate;->isLeapYear()Z

    move-result v0

    if-eqz v0, :cond_4f

    :cond_4d
    const/4 v0, 0x1

    goto :goto_50

    :cond_4f
    const/4 v0, 0x0

    :goto_50
    if-nez v0, :cond_53

    .line 535
    const/4 v9, 0x1

    .line 538
    :cond_53
    return v9
.end method

.method private static getWeekBasedYear(Lorg/threeten/bp/LocalDate;)I
    .registers 6
    .param p0, "date"    # Lorg/threeten/bp/LocalDate;

    .line 542
    invoke-virtual {p0}, Lorg/threeten/bp/LocalDate;->getYear()I

    move-result v2

    .line 543
    .local v2, "year":I
    invoke-virtual {p0}, Lorg/threeten/bp/LocalDate;->getDayOfYear()I

    move-result v3

    .line 544
    .local v3, "doy":I
    const/4 v0, 0x3

    if-gt v3, v0, :cond_1b

    .line 545
    invoke-virtual {p0}, Lorg/threeten/bp/LocalDate;->getDayOfWeek()Lorg/threeten/bp/DayOfWeek;

    move-result-object v0

    invoke-virtual {v0}, Lorg/threeten/bp/DayOfWeek;->ordinal()I

    move-result v4

    .line 546
    .local v4, "dow":I
    sub-int v0, v3, v4

    const/4 v1, -0x2

    if-ge v0, v1, :cond_1a

    .line 547
    add-int/lit8 v2, v2, -0x1

    .line 549
    .end local v4    # "dow":I
    :cond_1a
    goto :goto_3a

    :cond_1b
    const/16 v0, 0x16b

    if-lt v3, v0, :cond_3a

    .line 550
    invoke-virtual {p0}, Lorg/threeten/bp/LocalDate;->getDayOfWeek()Lorg/threeten/bp/DayOfWeek;

    move-result-object v0

    invoke-virtual {v0}, Lorg/threeten/bp/DayOfWeek;->ordinal()I

    move-result v4

    .line 551
    .local v4, "dow":I
    add-int/lit16 v0, v3, -0x16b

    invoke-virtual {p0}, Lorg/threeten/bp/LocalDate;->isLeapYear()Z

    move-result v1

    if-eqz v1, :cond_31

    const/4 v1, 0x1

    goto :goto_32

    :cond_31
    const/4 v1, 0x0

    :goto_32
    sub-int v3, v0, v1

    .line 552
    sub-int v0, v3, v4

    if-ltz v0, :cond_3a

    .line 553
    add-int/lit8 v2, v2, 0x1

    .line 556
    .end local v4    # "dow":I
    :cond_3a
    :goto_3a
    return v2
.end method

.method private static getWeekRange(I)I
    .registers 4
    .param p0, "wby"    # I

    .line 511
    const/4 v0, 0x1

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Lorg/threeten/bp/LocalDate;->of(III)Lorg/threeten/bp/LocalDate;

    move-result-object v2

    .line 513
    .local v2, "date":Lorg/threeten/bp/LocalDate;
    invoke-virtual {v2}, Lorg/threeten/bp/LocalDate;->getDayOfWeek()Lorg/threeten/bp/DayOfWeek;

    move-result-object v0

    sget-object v1, Lorg/threeten/bp/DayOfWeek;->THURSDAY:Lorg/threeten/bp/DayOfWeek;

    if-eq v0, v1, :cond_1c

    invoke-virtual {v2}, Lorg/threeten/bp/LocalDate;->getDayOfWeek()Lorg/threeten/bp/DayOfWeek;

    move-result-object v0

    sget-object v1, Lorg/threeten/bp/DayOfWeek;->WEDNESDAY:Lorg/threeten/bp/DayOfWeek;

    if-ne v0, v1, :cond_1f

    invoke-virtual {v2}, Lorg/threeten/bp/LocalDate;->isLeapYear()Z

    move-result v0

    if-eqz v0, :cond_1f

    .line 514
    :cond_1c
    const/16 v0, 0x35

    return v0

    .line 516
    :cond_1f
    const/16 v0, 0x34

    return v0
.end method

.method private static getWeekRange(Lorg/threeten/bp/LocalDate;)Lorg/threeten/bp/temporal/ValueRange;
    .registers 6
    .param p0, "date"    # Lorg/threeten/bp/LocalDate;

    .line 506
    invoke-static {p0}, Lorg/threeten/bp/temporal/IsoFields$Field;->getWeekBasedYear(Lorg/threeten/bp/LocalDate;)I

    move-result v4

    .line 507
    .local v4, "wby":I
    invoke-static {v4}, Lorg/threeten/bp/temporal/IsoFields$Field;->getWeekRange(I)I

    move-result v0

    int-to-long v0, v0

    const-wide/16 v2, 0x1

    invoke-static {v2, v3, v0, v1}, Lorg/threeten/bp/temporal/ValueRange;->of(JJ)Lorg/threeten/bp/temporal/ValueRange;

    move-result-object v0

    return-object v0
.end method

.method private static isIso(Lorg/threeten/bp/temporal/TemporalAccessor;)Z
    .registers 3
    .param p0, "temporal"    # Lorg/threeten/bp/temporal/TemporalAccessor;

    .line 502
    invoke-static {p0}, Lorg/threeten/bp/chrono/Chronology;->from(Lorg/threeten/bp/temporal/TemporalAccessor;)Lorg/threeten/bp/chrono/Chronology;

    move-result-object v0

    sget-object v1, Lorg/threeten/bp/chrono/IsoChronology;->INSTANCE:Lorg/threeten/bp/chrono/IsoChronology;

    invoke-virtual {v0, v1}, Lorg/threeten/bp/chrono/Chronology;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public static valueOf(Ljava/lang/String;)Lorg/threeten/bp/temporal/IsoFields$Field;
    .registers 2
    .param p0, "name"    # Ljava/lang/String;

    .line 205
    const-class v0, Lorg/threeten/bp/temporal/IsoFields$Field;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lorg/threeten/bp/temporal/IsoFields$Field;

    return-object v0
.end method

.method public static values()[Lorg/threeten/bp/temporal/IsoFields$Field;
    .registers 1

    .line 205
    sget-object v0, Lorg/threeten/bp/temporal/IsoFields$Field;->$VALUES:[Lorg/threeten/bp/temporal/IsoFields$Field;

    invoke-virtual {v0}, [Lorg/threeten/bp/temporal/IsoFields$Field;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lorg/threeten/bp/temporal/IsoFields$Field;

    return-object v0
.end method


# virtual methods
.method public isDateBased()Z
    .registers 2

    .line 494
    const/4 v0, 0x1

    return v0
.end method

.method public isTimeBased()Z
    .registers 2

    .line 498
    const/4 v0, 0x0

    return v0
.end method

.method public resolve(Ljava/util/Map;Lorg/threeten/bp/temporal/TemporalAccessor;Lorg/threeten/bp/format/ResolverStyle;)Lorg/threeten/bp/temporal/TemporalAccessor;
    .registers 5
    .param p1, "fieldValues"    # Ljava/util/Map;
    .param p2, "partialTemporal"    # Lorg/threeten/bp/temporal/TemporalAccessor;
    .param p3, "resolverStyle"    # Lorg/threeten/bp/format/ResolverStyle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/Map<Lorg/threeten/bp/temporal/TemporalField;Ljava/lang/Long;>;Lorg/threeten/bp/temporal/TemporalAccessor;Lorg/threeten/bp/format/ResolverStyle;)Lorg/threeten/bp/temporal/TemporalAccessor;"
        }
    .end annotation

    .line 486
    const/4 v0, 0x0

    return-object v0
.end method

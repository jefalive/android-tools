.class Lcom/itau/empresas/feature/comprovantes/ComprovantesAdapter$1;
.super Ljava/lang/Object;
.source "ComprovantesAdapter.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/itau/empresas/feature/comprovantes/ComprovantesAdapter;->ordenaListaComprovantePelaDataComprovante()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;Ljava/util/Comparator<Lcom/itau/empresas/feature/comprovantes/model/ComprovantesVO;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/itau/empresas/feature/comprovantes/ComprovantesAdapter;


# direct methods
.method constructor <init>(Lcom/itau/empresas/feature/comprovantes/ComprovantesAdapter;)V
    .registers 2
    .param p1, "this$0"    # Lcom/itau/empresas/feature/comprovantes/ComprovantesAdapter;

    .line 245
    iput-object p1, p0, Lcom/itau/empresas/feature/comprovantes/ComprovantesAdapter$1;->this$0:Lcom/itau/empresas/feature/comprovantes/ComprovantesAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public compare(Lcom/itau/empresas/feature/comprovantes/model/ComprovantesVO;Lcom/itau/empresas/feature/comprovantes/model/ComprovantesVO;)I
    .registers 5
    .param p1, "lft"    # Lcom/itau/empresas/feature/comprovantes/model/ComprovantesVO;
    .param p2, "rht"    # Lcom/itau/empresas/feature/comprovantes/model/ComprovantesVO;

    .line 248
    invoke-virtual {p2}, Lcom/itau/empresas/feature/comprovantes/model/ComprovantesVO;->getDataComprovante()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/itau/empresas/feature/comprovantes/model/ComprovantesVO;->getDataComprovante()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .registers 5

    .line 245
    move-object v0, p1

    check-cast v0, Lcom/itau/empresas/feature/comprovantes/model/ComprovantesVO;

    move-object v1, p2

    check-cast v1, Lcom/itau/empresas/feature/comprovantes/model/ComprovantesVO;

    invoke-virtual {p0, v0, v1}, Lcom/itau/empresas/feature/comprovantes/ComprovantesAdapter$1;->compare(Lcom/itau/empresas/feature/comprovantes/model/ComprovantesVO;Lcom/itau/empresas/feature/comprovantes/model/ComprovantesVO;)I

    move-result v0

    return v0
.end method

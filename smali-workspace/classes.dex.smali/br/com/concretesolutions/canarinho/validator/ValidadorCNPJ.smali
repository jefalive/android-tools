.class public final Lbr/com/concretesolutions/canarinho/validator/ValidadorCNPJ;
.super Ljava/lang/Object;
.source "ValidadorCNPJ.java"

# interfaces
.implements Lbr/com/concretesolutions/canarinho/validator/Validador;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lbr/com/concretesolutions/canarinho/validator/ValidadorCNPJ$SingletonHolder;
    }
.end annotation


# static fields
.field private static final DIGITO_PARA_CNPJ:Lbr/com/concretesolutions/canarinho/DigitoPara;


# direct methods
.method static constructor <clinit>()V
    .registers 5

    .line 15
    new-instance v0, Lbr/com/concretesolutions/canarinho/DigitoPara$Builder;

    invoke-direct {v0}, Lbr/com/concretesolutions/canarinho/DigitoPara$Builder;-><init>()V

    .line 16
    invoke-virtual {v0}, Lbr/com/concretesolutions/canarinho/DigitoPara$Builder;->complementarAoModulo()Lbr/com/concretesolutions/canarinho/DigitoPara$Builder;

    move-result-object v0

    const-string v1, "0"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Integer;

    .line 17
    const/16 v3, 0xa

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const/4 v4, 0x0

    aput-object v3, v2, v4

    const/16 v3, 0xb

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const/4 v4, 0x1

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Lbr/com/concretesolutions/canarinho/DigitoPara$Builder;->trocandoPorSeEncontrar(Ljava/lang/String;[Ljava/lang/Integer;)Lbr/com/concretesolutions/canarinho/DigitoPara$Builder;

    move-result-object v0

    .line 18
    invoke-virtual {v0}, Lbr/com/concretesolutions/canarinho/DigitoPara$Builder;->build()Lbr/com/concretesolutions/canarinho/DigitoPara;

    move-result-object v0

    sput-object v0, Lbr/com/concretesolutions/canarinho/validator/ValidadorCNPJ;->DIGITO_PARA_CNPJ:Lbr/com/concretesolutions/canarinho/DigitoPara;

    .line 15
    return-void
.end method

.method private constructor <init>()V
    .registers 1

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    return-void
.end method

.method synthetic constructor <init>(Lbr/com/concretesolutions/canarinho/validator/ValidadorCNPJ$1;)V
    .registers 2
    .param p1, "x0"    # Lbr/com/concretesolutions/canarinho/validator/ValidadorCNPJ$1;

    .line 13
    invoke-direct {p0}, Lbr/com/concretesolutions/canarinho/validator/ValidadorCNPJ;-><init>()V

    return-void
.end method

.method public static getInstance()Lbr/com/concretesolutions/canarinho/validator/ValidadorCNPJ;
    .registers 1

    .line 25
    # getter for: Lbr/com/concretesolutions/canarinho/validator/ValidadorCNPJ$SingletonHolder;->INSTANCE:Lbr/com/concretesolutions/canarinho/validator/ValidadorCNPJ;
    invoke-static {}, Lbr/com/concretesolutions/canarinho/validator/ValidadorCNPJ$SingletonHolder;->access$000()Lbr/com/concretesolutions/canarinho/validator/ValidadorCNPJ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public ehValido(Landroid/text/Editable;Lbr/com/concretesolutions/canarinho/validator/Validador$ResultadoParcial;)Lbr/com/concretesolutions/canarinho/validator/Validador$ResultadoParcial;
    .registers 6
    .param p1, "valor"    # Landroid/text/Editable;
    .param p2, "resultadoParcial"    # Lbr/com/concretesolutions/canarinho/validator/Validador$ResultadoParcial;

    .line 53
    if-eqz p2, :cond_4

    if-nez p1, :cond_c

    .line 54
    :cond_4
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Valores n\u00e3o podem ser nulos"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 57
    :cond_c
    sget-object v0, Lbr/com/concretesolutions/canarinho/formatador/Formatador$Padroes;->PADRAO_SOMENTE_NUMEROS:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/util/regex/Matcher;->replaceAll(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 59
    .local v2, "desformatado":Ljava/lang/String;
    invoke-virtual {p0, v2}, Lbr/com/concretesolutions/canarinho/validator/ValidadorCNPJ;->ehValido(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_39

    .line 60
    .line 61
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v1, 0xe

    if-ge v0, v1, :cond_28

    const/4 v0, 0x1

    goto :goto_29

    :cond_28
    const/4 v0, 0x0

    :goto_29
    invoke-virtual {p2, v0}, Lbr/com/concretesolutions/canarinho/validator/Validador$ResultadoParcial;->parcialmenteValido(Z)Lbr/com/concretesolutions/canarinho/validator/Validador$ResultadoParcial;

    move-result-object v0

    const-string v1, "CNPJ inv\u00e1lido"

    .line 62
    invoke-virtual {v0, v1}, Lbr/com/concretesolutions/canarinho/validator/Validador$ResultadoParcial;->mensagem(Ljava/lang/String;)Lbr/com/concretesolutions/canarinho/validator/Validador$ResultadoParcial;

    move-result-object v0

    .line 63
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lbr/com/concretesolutions/canarinho/validator/Validador$ResultadoParcial;->totalmenteValido(Z)Lbr/com/concretesolutions/canarinho/validator/Validador$ResultadoParcial;

    move-result-object v0

    return-object v0

    .line 66
    .line 67
    :cond_39
    const/4 v0, 0x1

    invoke-virtual {p2, v0}, Lbr/com/concretesolutions/canarinho/validator/Validador$ResultadoParcial;->parcialmenteValido(Z)Lbr/com/concretesolutions/canarinho/validator/Validador$ResultadoParcial;

    move-result-object v0

    .line 68
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lbr/com/concretesolutions/canarinho/validator/Validador$ResultadoParcial;->totalmenteValido(Z)Lbr/com/concretesolutions/canarinho/validator/Validador$ResultadoParcial;

    move-result-object v0

    return-object v0
.end method

.method public ehValido(Ljava/lang/String;)Z
    .registers 9
    .param p1, "value"    # Ljava/lang/String;

    .line 31
    if-eqz p1, :cond_a

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v1, 0xe

    if-ge v0, v1, :cond_c

    .line 32
    :cond_a
    const/4 v0, 0x0

    return v0

    .line 35
    :cond_c
    sget-object v0, Lbr/com/concretesolutions/canarinho/formatador/Formatador$Padroes;->PADRAO_SOMENTE_NUMEROS:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/util/regex/Matcher;->replaceAll(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 37
    .local v2, "desformatado":Ljava/lang/String;
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v1, 0xe

    if-eq v0, v1, :cond_22

    .line 38
    const/4 v0, 0x0

    return v0

    .line 41
    :cond_22
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x2

    const/4 v1, 0x0

    invoke-virtual {v2, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 42
    .local v3, "cnpjSemDigitos":Ljava/lang/String;
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x2

    invoke-virtual {v2, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    .line 44
    .local v4, "digitos":Ljava/lang/String;
    sget-object v0, Lbr/com/concretesolutions/canarinho/validator/ValidadorCNPJ;->DIGITO_PARA_CNPJ:Lbr/com/concretesolutions/canarinho/DigitoPara;

    invoke-virtual {v0, v3}, Lbr/com/concretesolutions/canarinho/DigitoPara;->calcula(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 45
    .local v5, "dig1":Ljava/lang/String;
    sget-object v0, Lbr/com/concretesolutions/canarinho/validator/ValidadorCNPJ;->DIGITO_PARA_CNPJ:Lbr/com/concretesolutions/canarinho/DigitoPara;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbr/com/concretesolutions/canarinho/DigitoPara;->calcula(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 47
    .local v6, "dig2":Ljava/lang/String;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.class public Lbr/com/itau/widgets/graphicalfriedcake/charts/highlight/ChartHighlighter;
.super Ljava/lang/Object;
.source "ChartHighlighter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::Lbr/com/itau/widgets/graphicalfriedcake/charts/interfaces/BarLineScatterCandleBubbleDataProvider;>Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field protected mChart:Lbr/com/itau/widgets/graphicalfriedcake/charts/interfaces/BarLineScatterCandleBubbleDataProvider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lbr/com/itau/widgets/graphicalfriedcake/charts/interfaces/BarLineScatterCandleBubbleDataProvider;)V
    .registers 2
    .param p1, "chart"    # Lbr/com/itau/widgets/graphicalfriedcake/charts/interfaces/BarLineScatterCandleBubbleDataProvider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/highlight/ChartHighlighter;->mChart:Lbr/com/itau/widgets/graphicalfriedcake/charts/interfaces/BarLineScatterCandleBubbleDataProvider;

    .line 25
    return-void
.end method


# virtual methods
.method protected getDataSetIndex(IFF)I
    .registers 10
    .param p1, "xIndex"    # I
    .param p2, "x"    # F
    .param p3, "y"    # F

    .line 110
    invoke-virtual {p0, p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/highlight/ChartHighlighter;->getSelectionDetailsAtIndex(I)Ljava/util/List;

    move-result-object v1

    .line 113
    .local v1, "valsAtIndex":Ljava/util/List;, "Ljava/util/List<Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/SelectionDetail;>;"
    sget-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$AxisDependency;->LEFT:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$AxisDependency;

    invoke-static {v1, p3, v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Utils;->getMinimumDistance(Ljava/util/List;FLbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$AxisDependency;)F

    move-result v2

    .line 114
    .local v2, "leftdist":F
    sget-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$AxisDependency;->RIGHT:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$AxisDependency;

    invoke-static {v1, p3, v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Utils;->getMinimumDistance(Ljava/util/List;FLbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$AxisDependency;)F

    move-result v3

    .line 117
    .local v3, "rightdist":F
    cmpg-float v0, v2, v3

    if-gez v0, :cond_17

    sget-object v4, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$AxisDependency;->LEFT:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$AxisDependency;

    goto :goto_19

    :cond_17
    sget-object v4, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$AxisDependency;->RIGHT:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$AxisDependency;

    .line 120
    .local v4, "axis":Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$AxisDependency;
    :goto_19
    invoke-static {v1, p3, v4}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Utils;->getClosestDataSetIndex(Ljava/util/List;FLbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$AxisDependency;)I

    move-result v5

    .line 123
    .local v5, "dataSetIndex":I
    return v5
.end method

.method public getHighlight(FF)Lbr/com/itau/widgets/graphicalfriedcake/charts/highlight/Highlight;
    .registers 6
    .param p1, "x"    # F
    .param p2, "y"    # F

    .line 37
    invoke-virtual {p0, p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/highlight/ChartHighlighter;->getXIndex(F)I

    move-result v1

    .line 38
    .local v1, "xIndex":I
    const v0, -0x7fffffff

    if-ne v1, v0, :cond_b

    .line 39
    const/4 v0, 0x0

    return-object v0

    .line 42
    :cond_b
    invoke-virtual {p0, v1, p1, p2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/highlight/ChartHighlighter;->getDataSetIndex(IFF)I

    move-result v2

    .line 43
    .local v2, "dataSetIndex":I
    const v0, -0x7fffffff

    if-ne v2, v0, :cond_16

    .line 44
    const/4 v0, 0x0

    return-object v0

    .line 47
    :cond_16
    new-instance v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/highlight/Highlight;

    invoke-direct {v0, v1, v2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/highlight/Highlight;-><init>(II)V

    return-object v0
.end method

.method protected getSelectionDetailsAtIndex(I)Ljava/util/List;
    .registers 9
    .param p1, "xIndex"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)Ljava/util/List<Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/SelectionDetail;>;"
        }
    .end annotation

    .line 135
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 138
    .local v2, "vals":Ljava/util/List;, "Ljava/util/List<Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/SelectionDetail;>;"
    const/4 v0, 0x2

    new-array v3, v0, [F

    .line 141
    .local v3, "pts":[F
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_9
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/highlight/ChartHighlighter;->mChart:Lbr/com/itau/widgets/graphicalfriedcake/charts/interfaces/BarLineScatterCandleBubbleDataProvider;

    invoke-interface {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/interfaces/BarLineScatterCandleBubbleDataProvider;->getData()Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarLineScatterCandleData;

    move-result-object v0

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarLineScatterCandleData;->getDataSetCount()I

    move-result v0

    if-ge v4, v0, :cond_58

    .line 144
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/highlight/ChartHighlighter;->mChart:Lbr/com/itau/widgets/graphicalfriedcake/charts/interfaces/BarLineScatterCandleBubbleDataProvider;

    invoke-interface {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/interfaces/BarLineScatterCandleBubbleDataProvider;->getData()Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarLineScatterCandleData;

    move-result-object v0

    invoke-virtual {v0, v4}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarLineScatterCandleData;->getDataSetByIndex(I)Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;

    move-result-object v5

    .line 148
    .local v5, "dataSet":Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;, "Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet<*>;"
    invoke-virtual {v5}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->isHighlightEnabled()Z

    move-result v0

    if-nez v0, :cond_26

    .line 149
    goto :goto_55

    .line 153
    :cond_26
    invoke-virtual {v5, p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->getYValForXIndex(I)F

    move-result v6

    .line 154
    .local v6, "yVal":F
    const/high16 v0, 0x7fc00000    # NaNf

    cmpl-float v0, v6, v0

    if-nez v0, :cond_31

    .line 155
    goto :goto_55

    .line 158
    :cond_31
    const/4 v0, 0x1

    aput v6, v3, v0

    .line 161
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/highlight/ChartHighlighter;->mChart:Lbr/com/itau/widgets/graphicalfriedcake/charts/interfaces/BarLineScatterCandleBubbleDataProvider;

    invoke-virtual {v5}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->getAxisDependency()Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$AxisDependency;

    move-result-object v1

    invoke-interface {v0, v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/interfaces/BarLineScatterCandleBubbleDataProvider;->getTransformer(Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$AxisDependency;)Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Transformer;

    move-result-object v0

    invoke-virtual {v0, v3}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Transformer;->pointValuesToPixel([F)V

    .line 164
    const/4 v0, 0x1

    aget v0, v3, v0

    invoke-static {v0}, Ljava/lang/Float;->isNaN(F)Z

    move-result v0

    if-nez v0, :cond_55

    .line 165
    new-instance v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/SelectionDetail;

    const/4 v1, 0x1

    aget v1, v3, v1

    invoke-direct {v0, v1, v4, v5}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/SelectionDetail;-><init>(FILbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;)V

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 141
    .end local v5    # "dataSet":Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;, "Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet<*>;"
    .end local v5
    .end local v6    # "yVal":F
    :cond_55
    :goto_55
    add-int/lit8 v4, v4, 0x1

    goto :goto_9

    .line 170
    .end local v4    # "i":I
    :cond_58
    return-object v2
.end method

.method protected getXIndex(F)I
    .registers 5
    .param p1, "x"    # F

    .line 60
    const/4 v0, 0x2

    new-array v2, v0, [F

    .line 61
    .local v2, "pts":[F
    const/4 v0, 0x0

    aput p1, v2, v0

    .line 65
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/highlight/ChartHighlighter;->mChart:Lbr/com/itau/widgets/graphicalfriedcake/charts/interfaces/BarLineScatterCandleBubbleDataProvider;

    sget-object v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$AxisDependency;->LEFT:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$AxisDependency;

    invoke-interface {v0, v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/interfaces/BarLineScatterCandleBubbleDataProvider;->getTransformer(Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$AxisDependency;)Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Transformer;

    move-result-object v0

    invoke-virtual {v0, v2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Transformer;->pixelsToValue([F)V

    .line 68
    const/4 v0, 0x0

    aget v0, v2, v0

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    return v0
.end method

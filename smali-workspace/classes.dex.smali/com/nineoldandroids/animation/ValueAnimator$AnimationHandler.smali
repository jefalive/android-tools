.class Lcom/nineoldandroids/animation/ValueAnimator$AnimationHandler;
.super Landroid/os/Handler;
.source "ValueAnimator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/nineoldandroids/animation/ValueAnimator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "AnimationHandler"
.end annotation


# direct methods
.method private constructor <init>()V
    .registers 1

    .line 570
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/nineoldandroids/animation/ValueAnimator$1;)V
    .registers 2
    .param p1, "x0"    # Lcom/nineoldandroids/animation/ValueAnimator$1;

    .line 570
    invoke-direct {p0}, Lcom/nineoldandroids/animation/ValueAnimator$AnimationHandler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 19
    .param p1, "msg"    # Landroid/os/Message;

    .line 585
    const/4 v4, 0x1

    .line 586
    .local v4, "callAgain":Z
    # getter for: Lcom/nineoldandroids/animation/ValueAnimator;->sAnimations:Ljava/lang/ThreadLocal;
    invoke-static {}, Lcom/nineoldandroids/animation/ValueAnimator;->access$000()Ljava/lang/ThreadLocal;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Ljava/util/ArrayList;

    .line 587
    .local v5, "animations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/nineoldandroids/animation/ValueAnimator;>;"
    # getter for: Lcom/nineoldandroids/animation/ValueAnimator;->sDelayedAnims:Ljava/lang/ThreadLocal;
    invoke-static {}, Lcom/nineoldandroids/animation/ValueAnimator;->access$100()Ljava/lang/ThreadLocal;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Ljava/util/ArrayList;

    .line 588
    .local v6, "delayedAnims":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/nineoldandroids/animation/ValueAnimator;>;"
    move-object/from16 v0, p1

    iget v0, v0, Landroid/os/Message;->what:I

    sparse-switch v0, :sswitch_data_12e

    goto/16 :goto_12c

    .line 592
    :sswitch_20
    # getter for: Lcom/nineoldandroids/animation/ValueAnimator;->sPendingAnimations:Ljava/lang/ThreadLocal;
    invoke-static {}, Lcom/nineoldandroids/animation/ValueAnimator;->access$200()Ljava/lang/ThreadLocal;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Ljava/util/ArrayList;

    .line 593
    .local v7, "pendingAnimations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/nineoldandroids/animation/ValueAnimator;>;"
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-gtz v0, :cond_37

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_38

    .line 594
    :cond_37
    const/4 v4, 0x0

    .line 601
    :cond_38
    :goto_38
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_6b

    .line 602
    invoke-virtual {v7}, Ljava/util/ArrayList;->clone()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Ljava/util/ArrayList;

    .line 604
    .local v8, "pendingCopy":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/nineoldandroids/animation/ValueAnimator;>;"
    invoke-virtual {v7}, Ljava/util/ArrayList;->clear()V

    .line 605
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v9

    .line 606
    .local v9, "count":I
    const/4 v10, 0x0

    .local v10, "i":I
    :goto_4d
    if-ge v10, v9, :cond_6a

    .line 607
    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v11, v0

    check-cast v11, Lcom/nineoldandroids/animation/ValueAnimator;

    .line 609
    .local v11, "anim":Lcom/nineoldandroids/animation/ValueAnimator;
    # getter for: Lcom/nineoldandroids/animation/ValueAnimator;->mStartDelay:J
    invoke-static {v11}, Lcom/nineoldandroids/animation/ValueAnimator;->access$300(Lcom/nineoldandroids/animation/ValueAnimator;)J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_64

    .line 610
    # invokes: Lcom/nineoldandroids/animation/ValueAnimator;->startAnimation()V
    invoke-static {v11}, Lcom/nineoldandroids/animation/ValueAnimator;->access$400(Lcom/nineoldandroids/animation/ValueAnimator;)V

    goto :goto_67

    .line 612
    :cond_64
    invoke-virtual {v6, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 606
    .end local v11    # "anim":Lcom/nineoldandroids/animation/ValueAnimator;
    :goto_67
    add-int/lit8 v10, v10, 0x1

    goto :goto_4d

    .line 615
    .end local v8    # "pendingCopy":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/nineoldandroids/animation/ValueAnimator;>;"
    .end local v8
    .end local v9    # "count":I
    .end local v10    # "i":I
    :cond_6a
    goto :goto_38

    .line 620
    :cond_6b
    :sswitch_6b
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    move-result-wide v8

    .line 621
    .local v8, "currentTime":J
    # getter for: Lcom/nineoldandroids/animation/ValueAnimator;->sReadyAnims:Ljava/lang/ThreadLocal;
    invoke-static {}, Lcom/nineoldandroids/animation/ValueAnimator;->access$500()Ljava/lang/ThreadLocal;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Ljava/util/ArrayList;

    .line 622
    .local v10, "readyAnims":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/nineoldandroids/animation/ValueAnimator;>;"
    # getter for: Lcom/nineoldandroids/animation/ValueAnimator;->sEndingAnims:Ljava/lang/ThreadLocal;
    invoke-static {}, Lcom/nineoldandroids/animation/ValueAnimator;->access$600()Ljava/lang/ThreadLocal;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v11, v0

    check-cast v11, Ljava/util/ArrayList;

    .line 626
    .local v11, "endingAnims":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/nineoldandroids/animation/ValueAnimator;>;"
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v12

    .line 627
    .local v12, "numDelayedAnims":I
    const/4 v13, 0x0

    .local v13, "i":I
    :goto_8a
    if-ge v13, v12, :cond_9f

    .line 628
    invoke-virtual {v6, v13}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v14, v0

    check-cast v14, Lcom/nineoldandroids/animation/ValueAnimator;

    .line 629
    .local v14, "anim":Lcom/nineoldandroids/animation/ValueAnimator;
    # invokes: Lcom/nineoldandroids/animation/ValueAnimator;->delayedAnimationFrame(J)Z
    invoke-static {v14, v8, v9}, Lcom/nineoldandroids/animation/ValueAnimator;->access$700(Lcom/nineoldandroids/animation/ValueAnimator;J)Z

    move-result v0

    if-eqz v0, :cond_9c

    .line 630
    invoke-virtual {v10, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 627
    .end local v14    # "anim":Lcom/nineoldandroids/animation/ValueAnimator;
    :cond_9c
    add-int/lit8 v13, v13, 0x1

    goto :goto_8a

    .line 633
    .end local v13    # "i":I
    :cond_9f
    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v13

    .line 634
    .local v13, "numReadyAnims":I
    if-lez v13, :cond_bf

    .line 635
    const/4 v14, 0x0

    .local v14, "i":I
    :goto_a6
    if-ge v14, v13, :cond_bc

    .line 636
    invoke-virtual {v10, v14}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v15, v0

    check-cast v15, Lcom/nineoldandroids/animation/ValueAnimator;

    .line 637
    .local v15, "anim":Lcom/nineoldandroids/animation/ValueAnimator;
    # invokes: Lcom/nineoldandroids/animation/ValueAnimator;->startAnimation()V
    invoke-static {v15}, Lcom/nineoldandroids/animation/ValueAnimator;->access$400(Lcom/nineoldandroids/animation/ValueAnimator;)V

    .line 638
    const/4 v0, 0x1

    # setter for: Lcom/nineoldandroids/animation/ValueAnimator;->mRunning:Z
    invoke-static {v15, v0}, Lcom/nineoldandroids/animation/ValueAnimator;->access$802(Lcom/nineoldandroids/animation/ValueAnimator;Z)Z

    .line 639
    invoke-virtual {v6, v15}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 635
    .end local v15    # "anim":Lcom/nineoldandroids/animation/ValueAnimator;
    add-int/lit8 v14, v14, 0x1

    goto :goto_a6

    .line 641
    .end local v14    # "i":I
    :cond_bc
    invoke-virtual {v10}, Ljava/util/ArrayList;->clear()V

    .line 646
    :cond_bf
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v14

    .line 647
    .local v14, "numAnims":I
    const/4 v15, 0x0

    .line 648
    .local v15, "i":I
    :goto_c4
    if-ge v15, v14, :cond_ec

    .line 649
    invoke-virtual {v5, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object/from16 v16, v0

    check-cast v16, Lcom/nineoldandroids/animation/ValueAnimator;

    .line 650
    .local v16, "anim":Lcom/nineoldandroids/animation/ValueAnimator;
    move-object/from16 v0, v16

    invoke-virtual {v0, v8, v9}, Lcom/nineoldandroids/animation/ValueAnimator;->animationFrame(J)Z

    move-result v0

    if-eqz v0, :cond_db

    .line 651
    move-object/from16 v0, v16

    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 653
    :cond_db
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ne v0, v14, :cond_e4

    .line 654
    add-int/lit8 v15, v15, 0x1

    goto :goto_eb

    .line 663
    :cond_e4
    add-int/lit8 v14, v14, -0x1

    .line 664
    move-object/from16 v0, v16

    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 666
    .end local v16    # "anim":Lcom/nineoldandroids/animation/ValueAnimator;
    :goto_eb
    goto :goto_c4

    .line 667
    :cond_ec
    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_108

    .line 668
    const/4 v15, 0x0

    :goto_f3
    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v15, v0, :cond_105

    .line 669
    invoke-virtual {v11, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/nineoldandroids/animation/ValueAnimator;

    # invokes: Lcom/nineoldandroids/animation/ValueAnimator;->endAnimation()V
    invoke-static {v0}, Lcom/nineoldandroids/animation/ValueAnimator;->access$900(Lcom/nineoldandroids/animation/ValueAnimator;)V

    .line 668
    add-int/lit8 v15, v15, 0x1

    goto :goto_f3

    .line 671
    :cond_105
    invoke-virtual {v11}, Ljava/util/ArrayList;->clear()V

    .line 676
    :cond_108
    if-eqz v4, :cond_12c

    invoke-virtual {v5}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_116

    invoke-virtual {v6}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_12c

    .line 677
    :cond_116
    # getter for: Lcom/nineoldandroids/animation/ValueAnimator;->sFrameDelay:J
    invoke-static {}, Lcom/nineoldandroids/animation/ValueAnimator;->access$1000()J

    move-result-wide v0

    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    move-result-wide v2

    sub-long/2addr v2, v8

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x0

    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    move-object/from16 v2, p0

    const/4 v3, 0x1

    invoke-virtual {v2, v3, v0, v1}, Lcom/nineoldandroids/animation/ValueAnimator$AnimationHandler;->sendEmptyMessageDelayed(IJ)Z

    .line 682
    .end local v7    # "pendingAnimations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/nineoldandroids/animation/ValueAnimator;>;"
    .end local v7
    .end local v8    # "currentTime":J
    .end local v10    # "readyAnims":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/nineoldandroids/animation/ValueAnimator;>;"
    .end local v10
    .end local v11    # "endingAnims":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/nineoldandroids/animation/ValueAnimator;>;"
    .end local v11
    .end local v12    # "numDelayedAnims":I
    .end local v13    # "numReadyAnims":I
    .end local v14    # "numAnims":I
    .end local v15    # "i":I
    :cond_12c
    :goto_12c
    return-void

    nop

    :sswitch_data_12e
    .sparse-switch
        0x0 -> :sswitch_20
        0x1 -> :sswitch_6b
    .end sparse-switch
.end method

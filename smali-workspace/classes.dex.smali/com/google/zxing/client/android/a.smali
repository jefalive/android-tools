.class public final Lcom/google/zxing/client/android/a;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/hardware/SensorEventListener;


# instance fields
.field private final a:Landroid/content/Context;

.field private b:Lcom/google/zxing/client/android/a/g;

.field private c:Landroid/hardware/Sensor;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/zxing/client/android/a;->a:Landroid/content/Context;

    return-void
.end method


# virtual methods
.method public a()V
    .registers 4

    iget-object v0, p0, Lcom/google/zxing/client/android/a;->c:Landroid/hardware/Sensor;

    if-eqz v0, :cond_18

    iget-object v0, p0, Lcom/google/zxing/client/android/a;->a:Landroid/content/Context;

    const-string v1, "sensor"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Landroid/hardware/SensorManager;

    invoke-virtual {v2, p0}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/zxing/client/android/a;->b:Lcom/google/zxing/client/android/a/g;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/zxing/client/android/a;->c:Landroid/hardware/Sensor;

    :cond_18
    return-void
.end method

.method public a(Lcom/google/zxing/client/android/a/g;)V
    .registers 6

    iput-object p1, p0, Lcom/google/zxing/client/android/a;->b:Lcom/google/zxing/client/android/a/g;

    iget-object v0, p0, Lcom/google/zxing/client/android/a;->a:Landroid/content/Context;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v2

    invoke-static {v2}, Lcom/google/zxing/client/android/a/h;->a(Landroid/content/SharedPreferences;)Lcom/google/zxing/client/android/a/h;

    move-result-object v0

    sget-object v1, Lcom/google/zxing/client/android/a/h;->b:Lcom/google/zxing/client/android/a/h;

    if-ne v0, v1, :cond_2c

    iget-object v0, p0, Lcom/google/zxing/client/android/a;->a:Landroid/content/Context;

    const-string v1, "sensor"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Landroid/hardware/SensorManager;

    const/4 v0, 0x5

    invoke-virtual {v3, v0}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v0

    iput-object v0, p0, Lcom/google/zxing/client/android/a;->c:Landroid/hardware/Sensor;

    iget-object v0, p0, Lcom/google/zxing/client/android/a;->c:Landroid/hardware/Sensor;

    if-eqz v0, :cond_2c

    iget-object v0, p0, Lcom/google/zxing/client/android/a;->c:Landroid/hardware/Sensor;

    const/4 v1, 0x3

    invoke-virtual {v3, p0, v0, v1}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    :cond_2c
    return-void
.end method

.method public onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .registers 3

    return-void
.end method

.method public onSensorChanged(Landroid/hardware/SensorEvent;)V
    .registers 5

    iget-object v0, p1, Landroid/hardware/SensorEvent;->values:[F

    const/4 v1, 0x0

    aget v2, v0, v1

    iget-object v0, p0, Lcom/google/zxing/client/android/a;->b:Lcom/google/zxing/client/android/a/g;

    if-eqz v0, :cond_22

    const/high16 v0, 0x42340000    # 45.0f

    cmpg-float v0, v2, v0

    if-gtz v0, :cond_16

    iget-object v0, p0, Lcom/google/zxing/client/android/a;->b:Lcom/google/zxing/client/android/a/g;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/zxing/client/android/a/g;->a(Z)V

    goto :goto_22

    :cond_16
    const/high16 v0, 0x43e10000    # 450.0f

    cmpl-float v0, v2, v0

    if-ltz v0, :cond_22

    iget-object v0, p0, Lcom/google/zxing/client/android/a;->b:Lcom/google/zxing/client/android/a/g;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/zxing/client/android/a/g;->a(Z)V

    :cond_22
    :goto_22
    return-void
.end method

.class public Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/PieChartRenderer;
.super Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/DataRenderer;
.source "PieChartRenderer.java"


# instance fields
.field protected mBitmapCanvas:Landroid/graphics/Canvas;

.field private mCenterTextLastBounds:Landroid/graphics/RectF;

.field private mCenterTextLastValue:Ljava/lang/String;

.field private mCenterTextLayout:Landroid/text/StaticLayout;

.field private mCenterTextPaint:Landroid/text/TextPaint;

.field protected mChart:Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;

.field protected mDrawBitmap:Landroid/graphics/Bitmap;

.field protected mHolePaint:Landroid/graphics/Paint;

.field private mRectBuffer:[Landroid/graphics/RectF;

.field protected mTransparentCirclePaint:Landroid/graphics/Paint;


# direct methods
.method public constructor <init>(Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/ChartAnimator;Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;)V
    .registers 7
    .param p1, "chart"    # Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;
    .param p2, "animator"    # Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/ChartAnimator;
    .param p3, "viewPortHandler"    # Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;

    .line 56
    invoke-direct {p0, p2, p3}, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/DataRenderer;-><init>(Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/ChartAnimator;Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;)V

    .line 46
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/PieChartRenderer;->mCenterTextLastBounds:Landroid/graphics/RectF;

    .line 47
    const/4 v0, 0x3

    new-array v0, v0, [Landroid/graphics/RectF;

    new-instance v1, Landroid/graphics/RectF;

    invoke-direct {v1}, Landroid/graphics/RectF;-><init>()V

    const/4 v2, 0x0

    aput-object v1, v0, v2

    new-instance v1, Landroid/graphics/RectF;

    invoke-direct {v1}, Landroid/graphics/RectF;-><init>()V

    const/4 v2, 0x1

    aput-object v1, v0, v2

    new-instance v1, Landroid/graphics/RectF;

    invoke-direct {v1}, Landroid/graphics/RectF;-><init>()V

    const/4 v2, 0x2

    aput-object v1, v0, v2

    iput-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/PieChartRenderer;->mRectBuffer:[Landroid/graphics/RectF;

    .line 57
    iput-object p1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/PieChartRenderer;->mChart:Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;

    .line 59
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/PieChartRenderer;->mHolePaint:Landroid/graphics/Paint;

    .line 60
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/PieChartRenderer;->mHolePaint:Landroid/graphics/Paint;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 61
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/PieChartRenderer;->mHolePaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 63
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/PieChartRenderer;->mTransparentCirclePaint:Landroid/graphics/Paint;

    .line 64
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/PieChartRenderer;->mTransparentCirclePaint:Landroid/graphics/Paint;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 65
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/PieChartRenderer;->mTransparentCirclePaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 67
    new-instance v0, Landroid/text/TextPaint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/text/TextPaint;-><init>(I)V

    iput-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/PieChartRenderer;->mCenterTextPaint:Landroid/text/TextPaint;

    .line 68
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/PieChartRenderer;->mCenterTextPaint:Landroid/text/TextPaint;

    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setColor(I)V

    .line 69
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/PieChartRenderer;->mCenterTextPaint:Landroid/text/TextPaint;

    const/high16 v1, 0x41400000    # 12.0f

    invoke-static {v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Utils;->convertDpToPixel(F)F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 70
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/PieChartRenderer;->mCenterTextPaint:Landroid/text/TextPaint;

    sget-object v1, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 72
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/PieChartRenderer;->mValuePaint:Landroid/graphics/Paint;

    const/high16 v1, 0x41500000    # 13.0f

    invoke-static {v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Utils;->convertDpToPixel(F)F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 73
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/PieChartRenderer;->mValuePaint:Landroid/graphics/Paint;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 74
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/PieChartRenderer;->mValuePaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 75
    return-void
.end method


# virtual methods
.method protected drawCenterText(Landroid/graphics/Canvas;)V
    .registers 22
    .param p1, "c"    # Landroid/graphics/Canvas;

    .line 287
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/PieChartRenderer;->mChart:Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->getCenterText()Ljava/lang/String;

    move-result-object v10

    .line 289
    .local v10, "centerText":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/PieChartRenderer;->mChart:Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->isDrawCenterTextEnabled()Z

    move-result v0

    if-eqz v0, :cond_18f

    if-eqz v10, :cond_18f

    .line 291
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/PieChartRenderer;->mChart:Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->getCenterCircleBox()Landroid/graphics/PointF;

    move-result-object v11

    .line 293
    .local v11, "center":Landroid/graphics/PointF;
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/PieChartRenderer;->mChart:Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->isCenterTextWordWrapEnabled()Z

    move-result v0

    if-eqz v0, :cond_126

    .line 295
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/PieChartRenderer;->mChart:Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->isDrawHoleEnabled()Z

    move-result v0

    if-eqz v0, :cond_50

    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/PieChartRenderer;->mChart:Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->isHoleTransparent()Z

    move-result v0

    if-eqz v0, :cond_50

    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/PieChartRenderer;->mChart:Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->getRadius()F

    move-result v0

    move-object/from16 v1, p0

    iget-object v1, v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/PieChartRenderer;->mChart:Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;

    invoke-virtual {v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->getHoleRadius()F

    move-result v1

    const/high16 v2, 0x42c80000    # 100.0f

    div-float/2addr v1, v2

    mul-float v12, v0, v1

    goto :goto_58

    :cond_50
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/PieChartRenderer;->mChart:Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->getRadius()F

    move-result v12

    .line 297
    .local v12, "innerRadius":F
    :goto_58
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/PieChartRenderer;->mRectBuffer:[Landroid/graphics/RectF;

    const/4 v1, 0x0

    aget-object v13, v0, v1

    .line 298
    .local v13, "holeRect":Landroid/graphics/RectF;
    iget v0, v11, Landroid/graphics/PointF;->x:F

    sub-float/2addr v0, v12

    iput v0, v13, Landroid/graphics/RectF;->left:F

    .line 299
    iget v0, v11, Landroid/graphics/PointF;->y:F

    sub-float/2addr v0, v12

    iput v0, v13, Landroid/graphics/RectF;->top:F

    .line 300
    iget v0, v11, Landroid/graphics/PointF;->x:F

    add-float/2addr v0, v12

    iput v0, v13, Landroid/graphics/RectF;->right:F

    .line 301
    iget v0, v11, Landroid/graphics/PointF;->y:F

    add-float/2addr v0, v12

    iput v0, v13, Landroid/graphics/RectF;->bottom:F

    .line 302
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/PieChartRenderer;->mRectBuffer:[Landroid/graphics/RectF;

    const/4 v1, 0x1

    aget-object v14, v0, v1

    .line 303
    .local v14, "boundingRect":Landroid/graphics/RectF;
    invoke-virtual {v14, v13}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    .line 305
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/PieChartRenderer;->mChart:Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->getCenterTextRadiusPercent()F

    move-result v15

    .line 306
    .local v15, "radiusPercent":F
    float-to-double v0, v15

    const-wide/16 v2, 0x0

    cmpl-double v0, v0, v2

    if-lez v0, :cond_a9

    .line 307
    invoke-virtual {v14}, Landroid/graphics/RectF;->width()F

    move-result v0

    invoke-virtual {v14}, Landroid/graphics/RectF;->width()F

    move-result v1

    mul-float/2addr v1, v15

    sub-float/2addr v0, v1

    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr v0, v1

    .line 308
    invoke-virtual {v14}, Landroid/graphics/RectF;->height()F

    move-result v1

    invoke-virtual {v14}, Landroid/graphics/RectF;->height()F

    move-result v2

    mul-float/2addr v2, v15

    sub-float/2addr v1, v2

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    .line 307
    invoke-virtual {v14, v0, v1}, Landroid/graphics/RectF;->inset(FF)V

    .line 311
    :cond_a9
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/PieChartRenderer;->mCenterTextLastValue:Ljava/lang/String;

    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_bd

    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/PieChartRenderer;->mCenterTextLastBounds:Landroid/graphics/RectF;

    invoke-virtual {v14, v0}, Landroid/graphics/RectF;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_f5

    .line 314
    :cond_bd
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/PieChartRenderer;->mCenterTextLastBounds:Landroid/graphics/RectF;

    invoke-virtual {v0, v14}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    .line 315
    move-object/from16 v0, p0

    iput-object v10, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/PieChartRenderer;->mCenterTextLastValue:Ljava/lang/String;

    .line 318
    new-instance v0, Landroid/text/StaticLayout;

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v3

    move-object/from16 v1, p0

    iget-object v4, v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/PieChartRenderer;->mCenterTextPaint:Landroid/text/TextPaint;

    move-object/from16 v1, p0

    iget-object v1, v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/PieChartRenderer;->mCenterTextLastBounds:Landroid/graphics/RectF;

    .line 320
    invoke-virtual {v1}, Landroid/graphics/RectF;->width()F

    move-result v1

    float-to-double v1, v1

    invoke-static {v1, v2}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v1

    const-wide/high16 v5, 0x3ff0000000000000L    # 1.0

    invoke-static {v1, v2, v5, v6}, Ljava/lang/Math;->max(DD)D

    move-result-wide v1

    double-to-int v5, v1

    sget-object v6, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    move-object v1, v10

    const/4 v2, 0x0

    const/high16 v7, 0x3f800000    # 1.0f

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;IILandroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    move-object/from16 v1, p0

    iput-object v0, v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/PieChartRenderer;->mCenterTextLayout:Landroid/text/StaticLayout;

    .line 328
    :cond_f5
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/PieChartRenderer;->mCenterTextLayout:Landroid/text/StaticLayout;

    invoke-virtual {v0}, Landroid/text/StaticLayout;->getHeight()I

    move-result v0

    int-to-float v1, v0

    move/from16 v16, v1

    .line 330
    .local v16, "layoutHeight":F
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    .line 331
    invoke-virtual {v14}, Landroid/graphics/RectF;->centerX()F

    move-result v0

    iget v1, v14, Landroid/graphics/RectF;->top:F

    invoke-virtual {v14}, Landroid/graphics/RectF;->height()F

    move-result v2

    sub-float v2, v2, v16

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v2, v3

    add-float/2addr v1, v2

    move-object/from16 v2, p1

    invoke-virtual {v2, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 332
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/PieChartRenderer;->mCenterTextLayout:Landroid/text/StaticLayout;

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    .line 333
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->restore()V

    .line 335
    .end local v12    # "innerRadius":F
    .end local v13    # "holeRect":Landroid/graphics/RectF;
    .end local v14    # "boundingRect":Landroid/graphics/RectF;
    .end local v15    # "radiusPercent":F
    .end local v16    # "layoutHeight":F
    goto/16 :goto_18f

    .line 338
    :cond_126
    const-string v0, "\n"

    invoke-virtual {v10, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v12

    .line 340
    .local v12, "lines":[Ljava/lang/String;
    const/4 v13, 0x0

    .line 343
    .local v13, "maxlineheight":F
    move-object v14, v12

    array-length v15, v14

    const/16 v16, 0x0

    :goto_131
    move/from16 v0, v16

    if-ge v0, v15, :cond_14d

    aget-object v17, v14, v16

    .line 344
    .local v17, "line":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/PieChartRenderer;->mCenterTextPaint:Landroid/text/TextPaint;

    move-object/from16 v1, v17

    invoke-static {v0, v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Utils;->calcTextHeight(Landroid/graphics/Paint;Ljava/lang/String;)I

    move-result v0

    int-to-float v1, v0

    move/from16 v18, v1

    .line 345
    .local v18, "curHeight":F
    cmpl-float v0, v18, v13

    if-lez v0, :cond_14a

    .line 346
    move/from16 v13, v18

    .line 343
    .end local v17    # "line":Ljava/lang/String;
    .end local v18    # "curHeight":F
    :cond_14a
    add-int/lit8 v16, v16, 0x1

    goto :goto_131

    .line 349
    :cond_14d
    const/high16 v0, 0x3e800000    # 0.25f

    mul-float v14, v13, v0

    .line 351
    .local v14, "linespacing":F
    array-length v0, v12

    int-to-float v0, v0

    mul-float/2addr v0, v13

    array-length v1, v12

    add-int/lit8 v1, v1, -0x1

    int-to-float v1, v1

    mul-float/2addr v1, v14

    sub-float v15, v0, v1

    .line 353
    .local v15, "totalheight":F
    array-length v0, v12

    move/from16 v16, v0

    .line 355
    .local v16, "cnt":I
    iget v0, v11, Landroid/graphics/PointF;->y:F

    move/from16 v17, v0

    .line 357
    .local v17, "y":F
    const/16 v18, 0x0

    .local v18, "i":I
    :goto_164
    array-length v0, v12

    move/from16 v1, v18

    if-ge v1, v0, :cond_18f

    .line 359
    array-length v0, v12

    sub-int v0, v0, v18

    add-int/lit8 v0, v0, -0x1

    aget-object v19, v12, v0

    .line 361
    .local v19, "line":Ljava/lang/String;
    iget v0, v11, Landroid/graphics/PointF;->x:F

    move/from16 v1, v16

    int-to-float v1, v1

    mul-float/2addr v1, v13

    add-float v1, v1, v17

    const/high16 v2, 0x40000000    # 2.0f

    div-float v2, v15, v2

    sub-float/2addr v1, v2

    move-object/from16 v2, p0

    iget-object v2, v2, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/PieChartRenderer;->mCenterTextPaint:Landroid/text/TextPaint;

    move-object/from16 v3, p1

    move-object/from16 v4, v19

    invoke-virtual {v3, v4, v0, v1, v2}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 364
    add-int/lit8 v16, v16, -0x1

    .line 365
    sub-float v17, v17, v14

    .line 357
    .end local v19    # "line":Ljava/lang/String;
    add-int/lit8 v18, v18, 0x1

    goto :goto_164

    .line 369
    .end local v11    # "center":Landroid/graphics/PointF;
    .end local v12    # "lines":[Ljava/lang/String;
    .end local v13    # "maxlineheight":F
    .end local v14    # "linespacing":F
    .end local v15    # "totalheight":F
    .end local v16    # "cnt":I
    .end local v17    # "y":F
    .end local v18    # "i":I
    :cond_18f
    :goto_18f
    return-void
.end method

.method public drawData(Landroid/graphics/Canvas;)V
    .registers 9
    .param p1, "c"    # Landroid/graphics/Canvas;

    .line 98
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/PieChartRenderer;->mViewPortHandler:Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->getChartWidth()F

    move-result v0

    float-to-int v2, v0

    .line 99
    .local v2, "width":I
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/PieChartRenderer;->mViewPortHandler:Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->getChartHeight()F

    move-result v0

    float-to-int v3, v0

    .line 101
    .local v3, "height":I
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/PieChartRenderer;->mDrawBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_22

    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/PieChartRenderer;->mDrawBitmap:Landroid/graphics/Bitmap;

    .line 102
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    if-ne v0, v2, :cond_22

    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/PieChartRenderer;->mDrawBitmap:Landroid/graphics/Bitmap;

    .line 103
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    if-eq v0, v3, :cond_39

    .line 105
    :cond_22
    if-lez v2, :cond_38

    if-lez v3, :cond_38

    .line 107
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_4444:Landroid/graphics/Bitmap$Config;

    invoke-static {v2, v3, v0}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/PieChartRenderer;->mDrawBitmap:Landroid/graphics/Bitmap;

    .line 108
    new-instance v0, Landroid/graphics/Canvas;

    iget-object v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/PieChartRenderer;->mDrawBitmap:Landroid/graphics/Bitmap;

    invoke-direct {v0, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    iput-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/PieChartRenderer;->mBitmapCanvas:Landroid/graphics/Canvas;

    goto :goto_39

    .line 110
    :cond_38
    return-void

    .line 113
    :cond_39
    :goto_39
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/PieChartRenderer;->mDrawBitmap:Landroid/graphics/Bitmap;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/graphics/Bitmap;->eraseColor(I)V

    .line 115
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/PieChartRenderer;->mChart:Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->getData()Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/PieData;

    .line 117
    .local v4, "pieData":Lbr/com/itau/widgets/graphicalfriedcake/charts/data/PieData;
    invoke-virtual {v4}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/PieData;->getDataSets()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_50
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_67

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/PieDataSet;

    .line 119
    .local v6, "set":Lbr/com/itau/widgets/graphicalfriedcake/charts/data/PieDataSet;
    invoke-virtual {v6}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/PieDataSet;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_66

    .line 120
    invoke-virtual {p0, p1, v6}, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/PieChartRenderer;->drawDataSet(Landroid/graphics/Canvas;Lbr/com/itau/widgets/graphicalfriedcake/charts/data/PieDataSet;)V

    .line 121
    .end local v6    # "set":Lbr/com/itau/widgets/graphicalfriedcake/charts/data/PieDataSet;
    :cond_66
    goto :goto_50

    .line 122
    :cond_67
    return-void
.end method

.method protected drawDataSet(Landroid/graphics/Canvas;Lbr/com/itau/widgets/graphicalfriedcake/charts/data/PieDataSet;)V
    .registers 16
    .param p1, "c"    # Landroid/graphics/Canvas;
    .param p2, "dataSet"    # Lbr/com/itau/widgets/graphicalfriedcake/charts/data/PieDataSet;

    .line 126
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/PieChartRenderer;->mChart:Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->getRotationAngle()F

    move-result v6

    .line 128
    .local v6, "angle":F
    invoke-virtual {p2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/PieDataSet;->getYVals()Ljava/util/List;

    move-result-object v7

    .line 129
    .local v7, "entries":Ljava/util/List;, "Ljava/util/List<Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;>;"
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/PieChartRenderer;->mChart:Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->getDrawAngles()[F

    move-result-object v8

    .line 131
    .local v8, "drawAngles":[F
    const/4 v9, 0x0

    .local v9, "j":I
    :goto_11
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v0

    if-ge v9, v0, :cond_8a

    .line 133
    aget v10, v8, v9

    .line 134
    .local v10, "newangle":F
    invoke-virtual {p2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/PieDataSet;->getSliceSpace()F

    move-result v11

    .line 136
    .local v11, "sliceSpace":F
    invoke-interface {v7, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v12, v0

    check-cast v12, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;

    .line 139
    .local v12, "e":Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;
    invoke-virtual {v12}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;->getVal()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    float-to-double v0, v0

    const-wide v2, 0x3eb0c6f7a0b5ed8dL    # 1.0E-6

    cmpl-double v0, v0, v2

    if-lez v0, :cond_7e

    .line 141
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/PieChartRenderer;->mChart:Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;

    invoke-virtual {v12}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;->getXIndex()I

    move-result v1

    iget-object v2, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/PieChartRenderer;->mChart:Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;

    .line 142
    invoke-virtual {v2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->getData()Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;

    move-result-object v2

    check-cast v2, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/PieData;

    invoke-virtual {v2, p2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/PieData;->getIndexOfDataSet(Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;)I

    move-result v2

    .line 141
    invoke-virtual {v0, v1, v2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->needsHighlight(II)Z

    move-result v0

    if-nez v0, :cond_7e

    .line 144
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/PieChartRenderer;->mRenderPaint:Landroid/graphics/Paint;

    invoke-virtual {p2, v9}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/PieDataSet;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 145
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/PieChartRenderer;->mBitmapCanvas:Landroid/graphics/Canvas;

    iget-object v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/PieChartRenderer;->mChart:Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;

    invoke-virtual {v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->getCircleBox()Landroid/graphics/RectF;

    move-result-object v1

    const/high16 v2, 0x40000000    # 2.0f

    div-float v2, v11, v2

    add-float/2addr v2, v6

    iget-object v3, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/PieChartRenderer;->mAnimator:Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/ChartAnimator;

    .line 146
    invoke-virtual {v3}, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/ChartAnimator;->getPhaseY()F

    move-result v3

    mul-float/2addr v2, v3

    const/high16 v3, 0x40000000    # 2.0f

    div-float v3, v11, v3

    sub-float v3, v10, v3

    iget-object v4, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/PieChartRenderer;->mAnimator:Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/ChartAnimator;

    .line 147
    invoke-virtual {v4}, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/ChartAnimator;->getPhaseY()F

    move-result v4

    mul-float/2addr v3, v4

    iget-object v5, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/PieChartRenderer;->mRenderPaint:Landroid/graphics/Paint;

    .line 145
    const/4 v4, 0x1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    .line 152
    :cond_7e
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/PieChartRenderer;->mAnimator:Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/ChartAnimator;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/ChartAnimator;->getPhaseX()F

    move-result v0

    mul-float/2addr v0, v10

    add-float/2addr v6, v0

    .line 131
    .end local v10    # "newangle":F
    .end local v11    # "sliceSpace":F
    .end local v12    # "e":Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;
    add-int/lit8 v9, v9, 0x1

    goto/16 :goto_11

    .line 154
    .end local v9    # "j":I
    :cond_8a
    return-void
.end method

.method public drawExtras(Landroid/graphics/Canvas;)V
    .registers 6
    .param p1, "c"    # Landroid/graphics/Canvas;

    .line 241
    invoke-virtual {p0, p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/PieChartRenderer;->drawHole(Landroid/graphics/Canvas;)V

    .line 242
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/PieChartRenderer;->mDrawBitmap:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/PieChartRenderer;->mRenderPaint:Landroid/graphics/Paint;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {p1, v0, v2, v3, v1}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 243
    invoke-virtual {p0, p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/PieChartRenderer;->drawCenterText(Landroid/graphics/Canvas;)V

    .line 244
    return-void
.end method

.method public drawHighlighted(Landroid/graphics/Canvas;[Lbr/com/itau/widgets/graphicalfriedcake/charts/highlight/Highlight;)V
    .registers 20
    .param p1, "c"    # Landroid/graphics/Canvas;
    .param p2, "indices"    # [Lbr/com/itau/widgets/graphicalfriedcake/charts/highlight/Highlight;

    .line 374
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/PieChartRenderer;->mChart:Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->getRotationAngle()F

    move-result v6

    .line 375
    .local v6, "rotationAngle":F
    const/4 v7, 0x0

    .line 377
    .local v7, "angle":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/PieChartRenderer;->mChart:Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->getDrawAngles()[F

    move-result-object v8

    .line 378
    .local v8, "drawAngles":[F
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/PieChartRenderer;->mChart:Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->getAbsoluteAngles()[F

    move-result-object v9

    .line 380
    .local v9, "absoluteAngles":[F
    const/4 v10, 0x0

    .local v10, "i":I
    :goto_1a
    move-object/from16 v0, p2

    array-length v0, v0

    if-ge v10, v0, :cond_b2

    .line 383
    aget-object v0, p2, v10

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/highlight/Highlight;->getXIndex()I

    move-result v11

    .line 384
    .local v11, "xIndex":I
    array-length v0, v8

    if-lt v11, v0, :cond_2a

    .line 385
    goto/16 :goto_ae

    .line 387
    :cond_2a
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/PieChartRenderer;->mChart:Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->getData()Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;

    move-result-object v0

    check-cast v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/PieData;

    aget-object v1, p2, v10

    .line 389
    invoke-virtual {v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/highlight/Highlight;->getDataSetIndex()I

    move-result v1

    .line 388
    invoke-virtual {v0, v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/PieData;->getDataSetByIndex(I)Lbr/com/itau/widgets/graphicalfriedcake/charts/data/PieDataSet;

    move-result-object v12

    .line 391
    .local v12, "set":Lbr/com/itau/widgets/graphicalfriedcake/charts/data/PieDataSet;
    if-eqz v12, :cond_ae

    invoke-virtual {v12}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/PieDataSet;->isHighlightEnabled()Z

    move-result v0

    if-nez v0, :cond_48

    .line 392
    goto/16 :goto_ae

    .line 394
    :cond_48
    if-nez v11, :cond_4c

    .line 395
    move v7, v6

    goto :goto_52

    .line 397
    :cond_4c
    add-int/lit8 v0, v11, -0x1

    aget v0, v9, v0

    add-float v7, v6, v0

    .line 399
    :goto_52
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/PieChartRenderer;->mAnimator:Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/ChartAnimator;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/ChartAnimator;->getPhaseY()F

    move-result v0

    mul-float/2addr v7, v0

    .line 401
    aget v13, v8, v11

    .line 403
    .local v13, "sliceDegrees":F
    invoke-virtual {v12}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/PieDataSet;->getSelectionShift()F

    move-result v14

    .line 404
    .local v14, "shift":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/PieChartRenderer;->mChart:Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->getCircleBox()Landroid/graphics/RectF;

    move-result-object v15

    .line 412
    .local v15, "circleBox":Landroid/graphics/RectF;
    new-instance v16, Landroid/graphics/RectF;

    iget v0, v15, Landroid/graphics/RectF;->left:F

    sub-float/2addr v0, v14

    iget v1, v15, Landroid/graphics/RectF;->top:F

    sub-float/2addr v1, v14

    iget v2, v15, Landroid/graphics/RectF;->right:F

    add-float/2addr v2, v14

    iget v3, v15, Landroid/graphics/RectF;->bottom:F

    add-float/2addr v3, v14

    move-object/from16 v4, v16

    invoke-direct {v4, v0, v1, v2, v3}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 417
    .local v16, "highlighted":Landroid/graphics/RectF;
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/PieChartRenderer;->mRenderPaint:Landroid/graphics/Paint;

    invoke-virtual {v12, v11}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/PieDataSet;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 421
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/PieChartRenderer;->mBitmapCanvas:Landroid/graphics/Canvas;

    move-object/from16 v1, v16

    invoke-virtual {v12}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/PieDataSet;->getSliceSpace()F

    move-result v2

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v2, v3

    add-float/2addr v2, v7

    move-object/from16 v3, p0

    iget-object v3, v3, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/PieChartRenderer;->mAnimator:Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/ChartAnimator;

    .line 422
    invoke-virtual {v3}, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/ChartAnimator;->getPhaseY()F

    move-result v3

    mul-float/2addr v3, v13

    .line 423
    invoke-virtual {v12}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/PieDataSet;->getSliceSpace()F

    move-result v4

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v4, v5

    sub-float/2addr v3, v4

    move-object/from16 v4, p0

    iget-object v5, v4, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/PieChartRenderer;->mRenderPaint:Landroid/graphics/Paint;

    .line 421
    const/4 v4, 0x1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    .line 380
    .end local v11    # "xIndex":I
    .end local v12    # "set":Lbr/com/itau/widgets/graphicalfriedcake/charts/data/PieDataSet;
    .end local v13    # "sliceDegrees":F
    .end local v14    # "shift":F
    .end local v15    # "circleBox":Landroid/graphics/RectF;
    .end local v16    # "highlighted":Landroid/graphics/RectF;
    :cond_ae
    :goto_ae
    add-int/lit8 v10, v10, 0x1

    goto/16 :goto_1a

    .line 425
    .end local v10    # "i":I
    :cond_b2
    return-void
.end method

.method protected drawHole(Landroid/graphics/Canvas;)V
    .registers 12
    .param p1, "c"    # Landroid/graphics/Canvas;

    .line 252
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/PieChartRenderer;->mChart:Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->isDrawHoleEnabled()Z

    move-result v0

    if-eqz v0, :cond_70

    .line 254
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/PieChartRenderer;->mChart:Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->getTransparentCircleRadius()F

    move-result v5

    .line 255
    .local v5, "transparentCircleRadius":F
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/PieChartRenderer;->mChart:Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->getHoleRadius()F

    move-result v6

    .line 256
    .local v6, "holeRadius":F
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/PieChartRenderer;->mChart:Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->getRadius()F

    move-result v7

    .line 258
    .local v7, "radius":F
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/PieChartRenderer;->mChart:Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->getCenterCircleBox()Landroid/graphics/PointF;

    move-result-object v8

    .line 260
    .local v8, "center":Landroid/graphics/PointF;
    cmpl-float v0, v5, v6

    if-lez v0, :cond_60

    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/PieChartRenderer;->mAnimator:Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/ChartAnimator;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/ChartAnimator;->getPhaseX()F

    move-result v0

    const/high16 v1, 0x3f800000    # 1.0f

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_60

    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/PieChartRenderer;->mAnimator:Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/ChartAnimator;

    .line 261
    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/ChartAnimator;->getPhaseY()F

    move-result v0

    const/high16 v1, 0x3f800000    # 1.0f

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_60

    .line 263
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/PieChartRenderer;->mTransparentCirclePaint:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getColor()I

    move-result v9

    .line 266
    .local v9, "color":I
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/PieChartRenderer;->mTransparentCirclePaint:Landroid/graphics/Paint;

    const v1, 0x60ffffff

    and-int/2addr v1, v9

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 269
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/PieChartRenderer;->mBitmapCanvas:Landroid/graphics/Canvas;

    iget v1, v8, Landroid/graphics/PointF;->x:F

    iget v2, v8, Landroid/graphics/PointF;->y:F

    const/high16 v3, 0x42c80000    # 100.0f

    div-float v3, v7, v3

    mul-float/2addr v3, v5

    iget-object v4, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/PieChartRenderer;->mTransparentCirclePaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 272
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/PieChartRenderer;->mTransparentCirclePaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v9}, Landroid/graphics/Paint;->setColor(I)V

    .line 276
    .end local v9    # "color":I
    :cond_60
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/PieChartRenderer;->mBitmapCanvas:Landroid/graphics/Canvas;

    iget v1, v8, Landroid/graphics/PointF;->x:F

    iget v2, v8, Landroid/graphics/PointF;->y:F

    const/high16 v3, 0x42c80000    # 100.0f

    div-float v3, v7, v3

    mul-float/2addr v3, v6

    iget-object v4, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/PieChartRenderer;->mHolePaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 279
    .end local v5    # "transparentCircleRadius":F
    .end local v6    # "holeRadius":F
    .end local v7    # "radius":F
    .end local v8    # "center":Landroid/graphics/PointF;
    :cond_70
    return-void
.end method

.method protected drawRoundedSlices(Landroid/graphics/Canvas;)V
    .registers 18
    .param p1, "c"    # Landroid/graphics/Canvas;

    .line 434
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/PieChartRenderer;->mChart:Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->isDrawRoundedSlicesEnabled()Z

    move-result v0

    if-nez v0, :cond_b

    .line 435
    return-void

    .line 437
    :cond_b
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/PieChartRenderer;->mChart:Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->getData()Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;

    move-result-object v0

    check-cast v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/PieData;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/PieData;->getDataSet()Lbr/com/itau/widgets/graphicalfriedcake/charts/data/PieDataSet;

    move-result-object v4

    .line 439
    .local v4, "dataSet":Lbr/com/itau/widgets/graphicalfriedcake/charts/data/PieDataSet;
    invoke-virtual {v4}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/PieDataSet;->isVisible()Z

    move-result v0

    if-nez v0, :cond_20

    .line 440
    return-void

    .line 442
    :cond_20
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/PieChartRenderer;->mChart:Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->getCenterCircleBox()Landroid/graphics/PointF;

    move-result-object v5

    .line 443
    .local v5, "center":Landroid/graphics/PointF;
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/PieChartRenderer;->mChart:Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->getRadius()F

    move-result v6

    .line 446
    .local v6, "r":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/PieChartRenderer;->mChart:Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->getHoleRadius()F

    move-result v0

    mul-float/2addr v0, v6

    const/high16 v1, 0x42c80000    # 100.0f

    div-float/2addr v0, v1

    sub-float v0, v6, v0

    const/high16 v1, 0x40000000    # 2.0f

    div-float v7, v0, v1

    .line 448
    .local v7, "circleRadius":F
    invoke-virtual {v4}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/PieDataSet;->getYVals()Ljava/util/List;

    move-result-object v8

    .line 449
    .local v8, "entries":Ljava/util/List;, "Ljava/util/List<Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;>;"
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/PieChartRenderer;->mChart:Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->getDrawAngles()[F

    move-result-object v9

    .line 450
    .local v9, "drawAngles":[F
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/PieChartRenderer;->mChart:Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->getRotationAngle()F

    move-result v10

    .line 452
    .local v10, "angle":F
    const/4 v11, 0x0

    .local v11, "j":I
    :goto_57
    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v0

    if-ge v11, v0, :cond_d6

    .line 454
    aget v12, v9, v11

    .line 456
    .local v12, "newangle":F
    invoke-interface {v8, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v13, v0

    check-cast v13, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;

    .line 459
    .local v13, "e":Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;
    invoke-virtual {v13}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;->getVal()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    float-to-double v0, v0

    const-wide v2, 0x3eb0c6f7a0b5ed8dL    # 1.0E-6

    cmpl-double v0, v0, v2

    if-lez v0, :cond_c8

    .line 461
    sub-float v0, v6, v7

    float-to-double v0, v0

    add-float v2, v10, v12

    move-object/from16 v3, p0

    iget-object v3, v3, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/PieChartRenderer;->mAnimator:Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/ChartAnimator;

    .line 463
    invoke-virtual {v3}, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/ChartAnimator;->getPhaseY()F

    move-result v3

    mul-float/2addr v2, v3

    float-to-double v2, v2

    .line 462
    invoke-static {v2, v3}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Math;->cos(D)D

    move-result-wide v2

    mul-double/2addr v0, v2

    iget v2, v5, Landroid/graphics/PointF;->x:F

    float-to-double v2, v2

    add-double/2addr v0, v2

    double-to-float v14, v0

    .line 464
    .local v14, "x":F
    sub-float v0, v6, v7

    float-to-double v0, v0

    add-float v2, v10, v12

    move-object/from16 v3, p0

    iget-object v3, v3, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/PieChartRenderer;->mAnimator:Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/ChartAnimator;

    .line 466
    invoke-virtual {v3}, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/ChartAnimator;->getPhaseY()F

    move-result v3

    mul-float/2addr v2, v3

    float-to-double v2, v2

    .line 465
    invoke-static {v2, v3}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Math;->sin(D)D

    move-result-wide v2

    mul-double/2addr v0, v2

    iget v2, v5, Landroid/graphics/PointF;->y:F

    float-to-double v2, v2

    add-double/2addr v0, v2

    double-to-float v15, v0

    .line 468
    .local v15, "y":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/PieChartRenderer;->mRenderPaint:Landroid/graphics/Paint;

    invoke-virtual {v4, v11}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/PieDataSet;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 469
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/PieChartRenderer;->mBitmapCanvas:Landroid/graphics/Canvas;

    move-object/from16 v1, p0

    iget-object v1, v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/PieChartRenderer;->mRenderPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v14, v15, v7, v1}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 472
    .end local v14    # "x":F
    .end local v15    # "y":F
    :cond_c8
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/PieChartRenderer;->mAnimator:Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/ChartAnimator;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/ChartAnimator;->getPhaseX()F

    move-result v0

    mul-float/2addr v0, v12

    add-float/2addr v10, v0

    .line 452
    .end local v12    # "newangle":F
    .end local v13    # "e":Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;
    add-int/lit8 v11, v11, 0x1

    goto/16 :goto_57

    .line 474
    .end local v11    # "j":I
    :cond_d6
    return-void
.end method

.method public drawValues(Landroid/graphics/Canvas;)V
    .registers 29
    .param p1, "c"    # Landroid/graphics/Canvas;

    .line 159
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/PieChartRenderer;->mChart:Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->getCenterCircleBox()Landroid/graphics/PointF;

    move-result-object v5

    .line 162
    .local v5, "center":Landroid/graphics/PointF;
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/PieChartRenderer;->mChart:Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->getRadius()F

    move-result v6

    .line 163
    .local v6, "r":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/PieChartRenderer;->mChart:Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->getRotationAngle()F

    move-result v7

    .line 164
    .local v7, "rotationAngle":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/PieChartRenderer;->mChart:Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->getDrawAngles()[F

    move-result-object v8

    .line 165
    .local v8, "drawAngles":[F
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/PieChartRenderer;->mChart:Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->getAbsoluteAngles()[F

    move-result-object v9

    .line 167
    .local v9, "absoluteAngles":[F
    const/high16 v0, 0x41200000    # 10.0f

    div-float v0, v6, v0

    const v1, 0x40666666    # 3.6f

    mul-float v10, v0, v1

    .line 169
    .local v10, "off":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/PieChartRenderer;->mChart:Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->isDrawHoleEnabled()Z

    move-result v0

    if-eqz v0, :cond_4e

    .line 170
    const/high16 v0, 0x42c80000    # 100.0f

    div-float v0, v6, v0

    move-object/from16 v1, p0

    iget-object v1, v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/PieChartRenderer;->mChart:Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;

    invoke-virtual {v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->getHoleRadius()F

    move-result v1

    mul-float/2addr v0, v1

    sub-float v0, v6, v0

    const/high16 v1, 0x40000000    # 2.0f

    div-float v10, v0, v1

    .line 173
    :cond_4e
    sub-float/2addr v6, v10

    .line 175
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/PieChartRenderer;->mChart:Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->getData()Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;

    move-result-object v0

    move-object v11, v0

    check-cast v11, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/PieData;

    .line 176
    .local v11, "data":Lbr/com/itau/widgets/graphicalfriedcake/charts/data/PieData;
    invoke-virtual {v11}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/PieData;->getDataSets()Ljava/util/List;

    move-result-object v12

    .line 177
    .local v12, "dataSets":Ljava/util/List;, "Ljava/util/List<Lbr/com/itau/widgets/graphicalfriedcake/charts/data/PieDataSet;>;"
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/PieChartRenderer;->mChart:Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->isDrawSliceTextEnabled()Z

    move-result v13

    .line 179
    .local v13, "drawXVals":Z
    const/4 v14, 0x0

    .line 181
    .local v14, "cnt":I
    const/4 v15, 0x0

    .local v15, "i":I
    :goto_68
    invoke-interface {v12}, Ljava/util/List;->size()I

    move-result v0

    if-ge v15, v0, :cond_1ca

    .line 183
    invoke-interface {v12, v15}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object/from16 v16, v0

    check-cast v16, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/PieDataSet;

    .line 185
    .local v16, "dataSet":Lbr/com/itau/widgets/graphicalfriedcake/charts/data/PieDataSet;
    invoke-virtual/range {v16 .. v16}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/PieDataSet;->isDrawValuesEnabled()Z

    move-result v0

    if-nez v0, :cond_80

    if-nez v13, :cond_80

    .line 186
    goto/16 :goto_1c6

    .line 189
    :cond_80
    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/PieChartRenderer;->applyValueTextStyle(Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;)V

    .line 191
    invoke-virtual/range {v16 .. v16}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/PieDataSet;->getYVals()Ljava/util/List;

    move-result-object v17

    .line 193
    .local v17, "entries":Ljava/util/List;, "Ljava/util/List<Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;>;"
    const/16 v18, 0x0

    .line 194
    .local v18, "j":I
    invoke-interface/range {v17 .. v17}, Ljava/util/List;->size()I

    move-result v0

    int-to-float v0, v0

    move-object/from16 v1, p0

    iget-object v1, v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/PieChartRenderer;->mAnimator:Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/ChartAnimator;

    invoke-virtual {v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/ChartAnimator;->getPhaseX()F

    move-result v1

    mul-float/2addr v0, v1

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v0, v0

    invoke-interface/range {v17 .. v17}, Ljava/util/List;->size()I

    move-result v1

    .line 193
    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v19

    .line 194
    .local v19, "maxEntry":I
    :goto_a9
    move/from16 v0, v18

    move/from16 v1, v19

    if-ge v0, v1, :cond_1c6

    .line 197
    aget v0, v8, v14

    const/high16 v1, 0x40000000    # 2.0f

    div-float v20, v0, v1

    .line 200
    .local v20, "offset":F
    float-to-double v0, v6

    aget v2, v9, v14

    add-float/2addr v2, v7

    sub-float v2, v2, v20

    move-object/from16 v3, p0

    iget-object v3, v3, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/PieChartRenderer;->mAnimator:Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/ChartAnimator;

    .line 202
    invoke-virtual {v3}, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/ChartAnimator;->getPhaseY()F

    move-result v3

    mul-float/2addr v2, v3

    float-to-double v2, v2

    .line 201
    invoke-static {v2, v3}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Math;->cos(D)D

    move-result-wide v2

    mul-double/2addr v0, v2

    iget v2, v5, Landroid/graphics/PointF;->x:F

    float-to-double v2, v2

    add-double/2addr v0, v2

    double-to-float v2, v0

    move/from16 v21, v2

    .line 203
    .local v21, "x":F
    float-to-double v0, v6

    aget v2, v9, v14

    add-float/2addr v2, v7

    sub-float v2, v2, v20

    move-object/from16 v3, p0

    iget-object v3, v3, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/PieChartRenderer;->mAnimator:Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/ChartAnimator;

    .line 205
    invoke-virtual {v3}, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/ChartAnimator;->getPhaseY()F

    move-result v3

    mul-float/2addr v2, v3

    float-to-double v2, v2

    .line 204
    invoke-static {v2, v3}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Math;->sin(D)D

    move-result-wide v2

    mul-double/2addr v0, v2

    iget v2, v5, Landroid/graphics/PointF;->y:F

    float-to-double v2, v2

    add-double/2addr v0, v2

    double-to-float v2, v0

    move/from16 v22, v2

    .line 207
    .local v22, "y":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/PieChartRenderer;->mChart:Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->isUsePercentValuesEnabled()Z

    move-result v0

    if-eqz v0, :cond_11b

    move-object/from16 v0, v17

    move/from16 v1, v18

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;->getVal()F

    move-result v0

    move-object/from16 v1, p0

    iget-object v1, v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/PieChartRenderer;->mChart:Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;

    .line 208
    invoke-virtual {v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->getYValueSum()F

    move-result v1

    div-float/2addr v0, v1

    const/high16 v1, 0x42c80000    # 100.0f

    mul-float v23, v0, v1

    goto :goto_129

    :cond_11b
    move-object/from16 v0, v17

    move/from16 v1, v18

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;->getVal()F

    move-result v23

    .line 210
    .local v23, "value":F
    :goto_129
    invoke-virtual/range {v16 .. v16}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/PieDataSet;->getValueFormatter()Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ValueFormatter;

    move-result-object v0

    move/from16 v1, v23

    invoke-interface {v0, v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ValueFormatter;->getFormattedValue(F)Ljava/lang/String;

    move-result-object v24

    .line 212
    .local v24, "val":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/PieChartRenderer;->mValuePaint:Landroid/graphics/Paint;

    move-object/from16 v1, v24

    invoke-static {v0, v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Utils;->calcTextHeight(Landroid/graphics/Paint;Ljava/lang/String;)I

    move-result v0

    int-to-float v0, v0

    .line 213
    const/high16 v1, 0x40800000    # 4.0f

    invoke-static {v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Utils;->convertDpToPixel(F)F

    move-result v1

    add-float v25, v0, v1

    .line 215
    .local v25, "lineHeight":F
    invoke-virtual/range {v16 .. v16}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/PieDataSet;->isDrawValuesEnabled()Z

    move-result v26

    .line 218
    .local v26, "drawYVals":Z
    if-eqz v13, :cond_17f

    if-eqz v26, :cond_17f

    .line 220
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/PieChartRenderer;->mValuePaint:Landroid/graphics/Paint;

    move-object/from16 v1, p1

    move-object/from16 v2, v24

    move/from16 v3, v21

    move/from16 v4, v22

    invoke-virtual {v1, v2, v3, v4, v0}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 221
    invoke-virtual {v11}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/PieData;->getXValCount()I

    move-result v0

    move/from16 v1, v18

    if-ge v1, v0, :cond_1c0

    .line 222
    invoke-virtual {v11}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/PieData;->getXVals()Ljava/util/List;

    move-result-object v0

    move/from16 v1, v18

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    add-float v1, v22, v25

    move-object/from16 v2, p0

    iget-object v2, v2, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/PieChartRenderer;->mValuePaint:Landroid/graphics/Paint;

    move-object/from16 v3, p1

    move/from16 v4, v21

    invoke-virtual {v3, v0, v4, v1, v2}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    goto :goto_1c0

    .line 225
    :cond_17f
    if-eqz v13, :cond_1a9

    if-nez v26, :cond_1a9

    .line 226
    invoke-virtual {v11}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/PieData;->getXValCount()I

    move-result v0

    move/from16 v1, v18

    if-ge v1, v0, :cond_1c0

    .line 227
    invoke-virtual {v11}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/PieData;->getXVals()Ljava/util/List;

    move-result-object v0

    move/from16 v1, v18

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const/high16 v1, 0x40000000    # 2.0f

    div-float v1, v25, v1

    add-float v1, v1, v22

    move-object/from16 v2, p0

    iget-object v2, v2, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/PieChartRenderer;->mValuePaint:Landroid/graphics/Paint;

    move-object/from16 v3, p1

    move/from16 v4, v21

    invoke-virtual {v3, v0, v4, v1, v2}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    goto :goto_1c0

    .line 228
    :cond_1a9
    if-nez v13, :cond_1c0

    if-eqz v26, :cond_1c0

    .line 230
    const/high16 v0, 0x40000000    # 2.0f

    div-float v0, v25, v0

    add-float v0, v0, v22

    move-object/from16 v1, p0

    iget-object v1, v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/PieChartRenderer;->mValuePaint:Landroid/graphics/Paint;

    move-object/from16 v2, p1

    move-object/from16 v3, v24

    move/from16 v4, v21

    invoke-virtual {v2, v3, v4, v0, v1}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 233
    :cond_1c0
    :goto_1c0
    add-int/lit8 v14, v14, 0x1

    .line 194
    .end local v20    # "offset":F
    .end local v21    # "x":F
    .end local v22    # "y":F
    .end local v23    # "value":F
    .end local v24    # "val":Ljava/lang/String;
    .end local v25    # "lineHeight":F
    .end local v26    # "drawYVals":Z
    add-int/lit8 v18, v18, 0x1

    goto/16 :goto_a9

    .line 181
    .end local v16    # "dataSet":Lbr/com/itau/widgets/graphicalfriedcake/charts/data/PieDataSet;
    .end local v17    # "entries":Ljava/util/List;, "Ljava/util/List<Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;>;"
    .end local v17
    .end local v18    # "j":I
    .end local v19    # "maxEntry":I
    :cond_1c6
    :goto_1c6
    add-int/lit8 v15, v15, 0x1

    goto/16 :goto_68

    .line 236
    .end local v15    # "i":I
    :cond_1ca
    return-void
.end method

.method public getPaintCenterText()Landroid/text/TextPaint;
    .registers 2

    .line 86
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/PieChartRenderer;->mCenterTextPaint:Landroid/text/TextPaint;

    return-object v0
.end method

.method public getPaintHole()Landroid/graphics/Paint;
    .registers 2

    .line 78
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/PieChartRenderer;->mHolePaint:Landroid/graphics/Paint;

    return-object v0
.end method

.method public getPaintTransparentCircle()Landroid/graphics/Paint;
    .registers 2

    .line 82
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/PieChartRenderer;->mTransparentCirclePaint:Landroid/graphics/Paint;

    return-object v0
.end method

.method public initBuffers()V
    .registers 1

    .line 93
    return-void
.end method

.class public Lcom/itau/empresas/ui/util/listener/TextWatcherWrapper;
.super Ljava/lang/Object;
.source "TextWatcherWrapper.java"

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field private final textWatcher:Landroid/text/TextWatcher;


# direct methods
.method public constructor <init>(Landroid/text/TextWatcher;)V
    .registers 2
    .param p1, "textWatcher"    # Landroid/text/TextWatcher;

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/itau/empresas/ui/util/listener/TextWatcherWrapper;->textWatcher:Landroid/text/TextWatcher;

    .line 21
    return-void
.end method


# virtual methods
.method public declared-synchronized afterTextChanged(Landroid/text/Editable;)V
    .registers 5
    .param p1, "mainEditable"    # Landroid/text/Editable;

    monitor-enter p0

    .line 36
    :try_start_1
    iget-object v0, p0, Lcom/itau/empresas/ui/util/listener/TextWatcherWrapper;->textWatcher:Landroid/text/TextWatcher;

    invoke-interface {v0, p1}, Landroid/text/TextWatcher;->afterTextChanged(Landroid/text/Editable;)V
    :try_end_6
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_1 .. :try_end_6} :catch_7
    .catchall {:try_start_1 .. :try_end_6} :catchall_14

    .line 40
    goto :goto_12

    .line 37
    :catch_7
    move-exception v2

    .line 38
    .local v2, "e":Ljava/lang/IndexOutOfBoundsException;
    const-string v0, "TextWatcherWrapper"

    const-string v1, "afterTextChanged: Failed to run the inner wrapper"

    :try_start_c
    invoke-static {v0, v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 39
    invoke-static {v2}, Lcom/crashlytics/android/Crashlytics;->logException(Ljava/lang/Throwable;)V
    :try_end_12
    .catchall {:try_start_c .. :try_end_12} :catchall_14

    .line 41
    .end local v2    # "e":Ljava/lang/IndexOutOfBoundsException;
    :goto_12
    monitor-exit p0

    return-void

    :catchall_14
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .registers 6
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "count"    # I
    .param p4, "after"    # I

    .line 25
    iget-object v0, p0, Lcom/itau/empresas/ui/util/listener/TextWatcherWrapper;->textWatcher:Landroid/text/TextWatcher;

    invoke-interface {v0, p1, p2, p3, p4}, Landroid/text/TextWatcher;->beforeTextChanged(Ljava/lang/CharSequence;III)V

    .line 26
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .registers 6
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "before"    # I
    .param p4, "count"    # I

    .line 30
    iget-object v0, p0, Lcom/itau/empresas/ui/util/listener/TextWatcherWrapper;->textWatcher:Landroid/text/TextWatcher;

    invoke-interface {v0, p1, p2, p3, p4}, Landroid/text/TextWatcher;->onTextChanged(Ljava/lang/CharSequence;III)V

    .line 31
    return-void
.end method

.class public Lcom/itau/empresas/ui/util/CustomViewPager;
.super Landroid/support/v4/view/ViewPager;
.source "CustomViewPager.java"


# instance fields
.field private enabled:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .line 13
    invoke-direct {p0, p1, p2}, Landroid/support/v4/view/ViewPager;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 14
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/itau/empresas/ui/util/CustomViewPager;->enabled:Z

    .line 15
    return-void
.end method


# virtual methods
.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 3
    .param p1, "event"    # Landroid/view/MotionEvent;

    .line 32
    iget-boolean v0, p0, Lcom/itau/empresas/ui/util/CustomViewPager;->enabled:Z

    if-eqz v0, :cond_9

    .line 33
    invoke-super {p0, p1}, Landroid/support/v4/view/ViewPager;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0

    .line 36
    :cond_9
    const/4 v0, 0x0

    return v0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 3
    .param p1, "event"    # Landroid/view/MotionEvent;

    .line 23
    iget-boolean v0, p0, Lcom/itau/empresas/ui/util/CustomViewPager;->enabled:Z

    if-eqz v0, :cond_9

    .line 24
    invoke-super {p0, p1}, Landroid/support/v4/view/ViewPager;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0

    .line 27
    :cond_9
    const/4 v0, 0x0

    return v0
.end method

.method public setEnabled(Z)V
    .registers 2
    .param p1, "enabled"    # Z

    .line 18
    iput-boolean p1, p0, Lcom/itau/empresas/ui/util/CustomViewPager;->enabled:Z

    .line 19
    return-void
.end method

.method public setPagingEnabled(Z)V
    .registers 2
    .param p1, "enabled"    # Z

    .line 40
    iput-boolean p1, p0, Lcom/itau/empresas/ui/util/CustomViewPager;->enabled:Z

    .line 41
    return-void
.end method

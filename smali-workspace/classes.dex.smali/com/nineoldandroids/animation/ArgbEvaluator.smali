.class public Lcom/nineoldandroids/animation/ArgbEvaluator;
.super Ljava/lang/Object;
.source "ArgbEvaluator.java"

# interfaces
.implements Lcom/nineoldandroids/animation/TypeEvaluator;


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public evaluate(FLjava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .registers 17
    .param p1, "fraction"    # F
    .param p2, "startValue"    # Ljava/lang/Object;
    .param p3, "endValue"    # Ljava/lang/Object;

    .line 42
    move-object v0, p2

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 43
    .local v3, "startInt":I
    shr-int/lit8 v4, v3, 0x18

    .line 44
    .local v4, "startA":I
    shr-int/lit8 v0, v3, 0x10

    and-int/lit16 v5, v0, 0xff

    .line 45
    .local v5, "startR":I
    shr-int/lit8 v0, v3, 0x8

    and-int/lit16 v6, v0, 0xff

    .line 46
    .local v6, "startG":I
    and-int/lit16 v7, v3, 0xff

    .line 48
    .local v7, "startB":I
    move-object/from16 v0, p3

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v8

    .line 49
    .local v8, "endInt":I
    shr-int/lit8 v9, v8, 0x18

    .line 50
    .local v9, "endA":I
    shr-int/lit8 v0, v8, 0x10

    and-int/lit16 v10, v0, 0xff

    .line 51
    .local v10, "endR":I
    shr-int/lit8 v0, v8, 0x8

    and-int/lit16 v11, v0, 0xff

    .line 52
    .local v11, "endG":I
    and-int/lit16 v12, v8, 0xff

    .line 54
    .local v12, "endB":I
    sub-int v0, v9, v4

    int-to-float v0, v0

    mul-float/2addr v0, p1

    float-to-int v0, v0

    add-int/2addr v0, v4

    shl-int/lit8 v0, v0, 0x18

    sub-int v1, v10, v5

    int-to-float v1, v1

    mul-float/2addr v1, p1

    float-to-int v1, v1

    add-int/2addr v1, v5

    shl-int/lit8 v1, v1, 0x10

    or-int/2addr v0, v1

    sub-int v1, v11, v6

    int-to-float v1, v1

    mul-float/2addr v1, p1

    float-to-int v1, v1

    add-int/2addr v1, v6

    shl-int/lit8 v1, v1, 0x8

    or-int/2addr v0, v1

    sub-int v1, v12, v7

    int-to-float v1, v1

    mul-float/2addr v1, p1

    float-to-int v1, v1

    add-int/2addr v1, v7

    or-int/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

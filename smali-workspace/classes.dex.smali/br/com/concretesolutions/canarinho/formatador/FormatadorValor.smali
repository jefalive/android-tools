.class public final Lbr/com/concretesolutions/canarinho/formatador/FormatadorValor;
.super Ljava/lang/Object;
.source "FormatadorValor.java"

# interfaces
.implements Lbr/com/concretesolutions/canarinho/formatador/Formatador;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lbr/com/concretesolutions/canarinho/formatador/FormatadorValor$SingletonHolder;
    }
.end annotation


# static fields
.field private static final FORMATADOR_MOEDA:Ljava/text/DecimalFormat;

.field private static final PADRAO_DECIMAL:Ljava/util/regex/Pattern;

.field private static final PADRAO_MOEDA:Ljava/util/regex/Pattern;


# instance fields
.field private final adicionaSimboloReal:Z


# direct methods
.method static constructor <clinit>()V
    .registers 4

    .line 20
    new-instance v0, Ljava/util/Locale;

    const-string v1, "pt"

    const-string v2, "BR"

    invoke-direct {v0, v1, v2}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 21
    invoke-static {v0}, Ljava/text/NumberFormat;->getCurrencyInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object v0

    check-cast v0, Ljava/text/DecimalFormat;

    sput-object v0, Lbr/com/concretesolutions/canarinho/formatador/FormatadorValor;->FORMATADOR_MOEDA:Ljava/text/DecimalFormat;

    .line 22
    const-string v0, "^\\d+(\\.\\d{1,2})?$"

    .line 23
    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lbr/com/concretesolutions/canarinho/formatador/FormatadorValor;->PADRAO_DECIMAL:Ljava/util/regex/Pattern;

    .line 24
    const-string v0, "\\d{1,3}(\\.\\d{3})*(,\\d{2})?"

    .line 25
    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lbr/com/concretesolutions/canarinho/formatador/FormatadorValor;->PADRAO_MOEDA:Ljava/util/regex/Pattern;

    .line 30
    sget-object v0, Lbr/com/concretesolutions/canarinho/formatador/FormatadorValor;->FORMATADOR_MOEDA:Ljava/text/DecimalFormat;

    invoke-virtual {v0}, Ljava/text/DecimalFormat;->getDecimalFormatSymbols()Ljava/text/DecimalFormatSymbols;

    move-result-object v3

    .line 31
    .local v3, "decimalFormatSymbols":Ljava/text/DecimalFormatSymbols;
    const-string v0, ""

    invoke-virtual {v3, v0}, Ljava/text/DecimalFormatSymbols;->setCurrencySymbol(Ljava/lang/String;)V

    .line 32
    sget-object v0, Lbr/com/concretesolutions/canarinho/formatador/FormatadorValor;->FORMATADOR_MOEDA:Ljava/text/DecimalFormat;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Ljava/text/DecimalFormat;->setMinimumFractionDigits(I)V

    .line 33
    sget-object v0, Lbr/com/concretesolutions/canarinho/formatador/FormatadorValor;->FORMATADOR_MOEDA:Ljava/text/DecimalFormat;

    invoke-virtual {v0, v3}, Ljava/text/DecimalFormat;->setDecimalFormatSymbols(Ljava/text/DecimalFormatSymbols;)V

    .line 34
    sget-object v0, Lbr/com/concretesolutions/canarinho/formatador/FormatadorValor;->FORMATADOR_MOEDA:Ljava/text/DecimalFormat;

    const-string v1, "-"

    invoke-virtual {v0, v1}, Ljava/text/DecimalFormat;->setNegativePrefix(Ljava/lang/String;)V

    .line 35
    sget-object v0, Lbr/com/concretesolutions/canarinho/formatador/FormatadorValor;->FORMATADOR_MOEDA:Ljava/text/DecimalFormat;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/text/DecimalFormat;->setNegativeSuffix(Ljava/lang/String;)V

    .line 36
    sget-object v0, Lbr/com/concretesolutions/canarinho/formatador/FormatadorValor;->FORMATADOR_MOEDA:Ljava/text/DecimalFormat;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/text/DecimalFormat;->setPositivePrefix(Ljava/lang/String;)V

    .line 37
    sget-object v0, Lbr/com/concretesolutions/canarinho/formatador/FormatadorValor;->FORMATADOR_MOEDA:Ljava/text/DecimalFormat;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/text/DecimalFormat;->setParseBigDecimal(Z)V

    .line 38
    .end local v3    # "decimalFormatSymbols":Ljava/text/DecimalFormatSymbols;
    return-void
.end method

.method private constructor <init>(Z)V
    .registers 2
    .param p1, "comSimboloReal"    # Z

    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    iput-boolean p1, p0, Lbr/com/concretesolutions/canarinho/formatador/FormatadorValor;->adicionaSimboloReal:Z

    .line 45
    return-void
.end method

.method synthetic constructor <init>(ZLbr/com/concretesolutions/canarinho/formatador/FormatadorValor$1;)V
    .registers 3
    .param p1, "x0"    # Z
    .param p2, "x1"    # Lbr/com/concretesolutions/canarinho/formatador/FormatadorValor$1;

    .line 18
    invoke-direct {p0, p1}, Lbr/com/concretesolutions/canarinho/formatador/FormatadorValor;-><init>(Z)V

    return-void
.end method

.method static getInstance(Z)Lbr/com/concretesolutions/canarinho/formatador/FormatadorValor;
    .registers 2
    .param p0, "comSimboloReal"    # Z

    .line 54
    if-eqz p0, :cond_7

    .line 55
    # getter for: Lbr/com/concretesolutions/canarinho/formatador/FormatadorValor$SingletonHolder;->INSTANCE_COM_SIMBOLO:Lbr/com/concretesolutions/canarinho/formatador/FormatadorValor;
    invoke-static {}, Lbr/com/concretesolutions/canarinho/formatador/FormatadorValor$SingletonHolder;->access$000()Lbr/com/concretesolutions/canarinho/formatador/FormatadorValor;

    move-result-object v0

    goto :goto_b

    .line 56
    :cond_7
    # getter for: Lbr/com/concretesolutions/canarinho/formatador/FormatadorValor$SingletonHolder;->INSTANCE_SEM_SIMBOLO:Lbr/com/concretesolutions/canarinho/formatador/FormatadorValor;
    invoke-static {}, Lbr/com/concretesolutions/canarinho/formatador/FormatadorValor$SingletonHolder;->access$100()Lbr/com/concretesolutions/canarinho/formatador/FormatadorValor;

    move-result-object v0

    :goto_b
    return-object v0
.end method


# virtual methods
.method public desformata(Ljava/lang/String;)Ljava/lang/String;
    .registers 7
    .param p1, "value"    # Ljava/lang/String;

    .line 68
    move-object v3, p1

    .line 69
    .local v3, "realValue":Ljava/lang/String;
    const-string v0, "R$ "

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 70
    const-string v0, "R$ "

    invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    .line 73
    :cond_13
    sget-object v0, Lbr/com/concretesolutions/canarinho/formatador/FormatadorValor;->FORMATADOR_MOEDA:Ljava/text/DecimalFormat;

    new-instance v1, Ljava/text/ParsePosition;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Ljava/text/ParsePosition;-><init>(I)V

    invoke-virtual {v0, v3, v1}, Ljava/text/DecimalFormat;->parse(Ljava/lang/String;Ljava/text/ParsePosition;)Ljava/lang/Number;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Ljava/math/BigDecimal;

    .line 74
    .local v4, "valor":Ljava/math/BigDecimal;
    invoke-virtual {v4}, Ljava/math/BigDecimal;->toPlainString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public estaFormatado(Ljava/lang/String;)Z
    .registers 4
    .param p1, "value"    # Ljava/lang/String;

    .line 80
    if-nez p1, :cond_a

    .line 81
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Valor n\u00e3o pode ser nulo"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 84
    :cond_a
    sget-object v0, Lbr/com/concretesolutions/canarinho/formatador/FormatadorValor;->PADRAO_MOEDA:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    return v0
.end method

.method public formata(Ljava/lang/String;)Ljava/lang/String;
    .registers 5
    .param p1, "value"    # Ljava/lang/String;

    .line 61
    sget-object v0, Lbr/com/concretesolutions/canarinho/formatador/FormatadorValor;->FORMATADOR_MOEDA:Ljava/text/DecimalFormat;

    new-instance v1, Ljava/math/BigDecimal;

    invoke-direct {v1, p1}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/text/DecimalFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 62
    .local v2, "resultado":Ljava/lang/String;
    iget-boolean v0, p0, Lbr/com/concretesolutions/canarinho/formatador/FormatadorValor;->adicionaSimboloReal:Z

    if-eqz v0, :cond_23

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "R$ "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_24

    :cond_23
    move-object v0, v2

    :goto_24
    return-object v0
.end method

.method public podeSerFormatado(Ljava/lang/String;)Z
    .registers 4
    .param p1, "value"    # Ljava/lang/String;

    .line 90
    if-nez p1, :cond_a

    .line 91
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Valor n\u00e3o pode ser nulo"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 94
    :cond_a
    sget-object v0, Lbr/com/concretesolutions/canarinho/formatador/FormatadorValor;->PADRAO_DECIMAL:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    return v0
.end method

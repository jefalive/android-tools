.class public Lcom/itau/empresas/ui/activity/AplicativosActivity;
.super Lcom/itau/empresas/ui/activity/BaseActivity;
.source "AplicativosActivity.java"


# instance fields
.field llAplicativosGerais:Landroid/widget/LinearLayout;

.field llAplicativosRecomendados:Landroid/widget/LinearLayout;

.field toolBar:Landroid/support/v7/widget/Toolbar;

.field toolbarTitle:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 23
    invoke-direct {p0}, Lcom/itau/empresas/ui/activity/BaseActivity;-><init>()V

    return-void
.end method

.method private adicionarItemAplicativo(Landroid/view/ViewGroup;IIIZ)V
    .registers 9
    .param p1, "container"    # Landroid/view/ViewGroup;
    .param p2, "imagemAplicativo"    # I
    .param p3, "nomeAplicativo"    # I
    .param p4, "pacote"    # I
    .param p5, "divisorVisivel"    # Z

    .line 95
    .line 96
    invoke-virtual {p0, p3}, Lcom/itau/empresas/ui/activity/AplicativosActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p4}, Lcom/itau/empresas/ui/activity/AplicativosActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, p2, v0, v1, p5}, Lcom/itau/empresas/ui/view/AplicativoItemView_;->build(Lcom/itau/empresas/ui/activity/BaseActivity;ILjava/lang/String;Ljava/lang/String;Z)Lcom/itau/empresas/ui/view/AplicativoItemView;

    move-result-object v2

    .line 98
    .local v2, "v":Landroid/view/View;
    invoke-virtual {p1, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 99
    return-void
.end method

.method private constroiToolbar()V
    .registers 3

    .line 78
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/AplicativosActivity;->toolBar:Landroid/support/v7/widget/Toolbar;

    invoke-static {v0, p0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom;->Builder(Landroid/support/v7/widget/Toolbar;Landroid/support/v7/app/AppCompatActivity;)Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    move-result-object v0

    .line 79
    const v1, 0x7f070696

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->comTitulo(I)Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    move-result-object v0

    .line 80
    invoke-virtual {v0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->comClickListenerFinish()Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    move-result-object v0

    .line 81
    invoke-virtual {v0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->build()Lcom/itau/empresas/ui/util/listener/ToolbarCustom;

    .line 82
    return-void
.end method

.method public static intent(Landroid/support/v4/app/Fragment;)Lcom/itau/empresas/ui/activity/AplicativosActivity_$IntentBuilder_;
    .registers 2
    .param p0, "fragment"    # Landroid/support/v4/app/Fragment;

    .line 116
    new-instance v0, Lcom/itau/empresas/ui/activity/AplicativosActivity_$IntentBuilder_;

    invoke-direct {v0, p0}, Lcom/itau/empresas/ui/activity/AplicativosActivity_$IntentBuilder_;-><init>(Landroid/support/v4/app/Fragment;)V

    return-object v0
.end method


# virtual methods
.method aoIniciarActivity()V
    .registers 7

    .line 32
    invoke-direct {p0}, Lcom/itau/empresas/ui/activity/AplicativosActivity;->constroiToolbar()V

    .line 35
    move-object v0, p0

    iget-object v1, p0, Lcom/itau/empresas/ui/activity/AplicativosActivity;->llAplicativosGerais:Landroid/widget/LinearLayout;

    const v2, 0x7f020142

    const v3, 0x7f0703ad

    const v4, 0x7f0703b3

    const/4 v5, 0x1

    invoke-direct/range {v0 .. v5}, Lcom/itau/empresas/ui/activity/AplicativosActivity;->adicionarItemAplicativo(Landroid/view/ViewGroup;IIIZ)V

    .line 39
    move-object v0, p0

    iget-object v1, p0, Lcom/itau/empresas/ui/activity/AplicativosActivity;->llAplicativosGerais:Landroid/widget/LinearLayout;

    const v2, 0x7f020155

    const v3, 0x7f0703b0

    const v4, 0x7f0703b6

    const/4 v5, 0x1

    invoke-direct/range {v0 .. v5}, Lcom/itau/empresas/ui/activity/AplicativosActivity;->adicionarItemAplicativo(Landroid/view/ViewGroup;IIIZ)V

    .line 42
    move-object v0, p0

    iget-object v1, p0, Lcom/itau/empresas/ui/activity/AplicativosActivity;->llAplicativosGerais:Landroid/widget/LinearLayout;

    const v2, 0x7f020055

    const v3, 0x7f0703c3

    const v4, 0x7f0703bd

    const/4 v5, 0x1

    invoke-direct/range {v0 .. v5}, Lcom/itau/empresas/ui/activity/AplicativosActivity;->adicionarItemAplicativo(Landroid/view/ViewGroup;IIIZ)V

    .line 45
    move-object v0, p0

    iget-object v1, p0, Lcom/itau/empresas/ui/activity/AplicativosActivity;->llAplicativosGerais:Landroid/widget/LinearLayout;

    const v2, 0x7f020153

    const v3, 0x7f0703af

    const v4, 0x7f0703b5

    const/4 v5, 0x1

    invoke-direct/range {v0 .. v5}, Lcom/itau/empresas/ui/activity/AplicativosActivity;->adicionarItemAplicativo(Landroid/view/ViewGroup;IIIZ)V

    .line 49
    move-object v0, p0

    iget-object v1, p0, Lcom/itau/empresas/ui/activity/AplicativosActivity;->llAplicativosGerais:Landroid/widget/LinearLayout;

    const v2, 0x7f020177

    const v3, 0x7f0703b1

    const v4, 0x7f0703b7

    const/4 v5, 0x1

    invoke-direct/range {v0 .. v5}, Lcom/itau/empresas/ui/activity/AplicativosActivity;->adicionarItemAplicativo(Landroid/view/ViewGroup;IIIZ)V

    .line 52
    move-object v0, p0

    iget-object v1, p0, Lcom/itau/empresas/ui/activity/AplicativosActivity;->llAplicativosGerais:Landroid/widget/LinearLayout;

    const v2, 0x7f020144

    const v3, 0x7f0703b2

    const v4, 0x7f0703b8

    const/4 v5, 0x1

    invoke-direct/range {v0 .. v5}, Lcom/itau/empresas/ui/activity/AplicativosActivity;->adicionarItemAplicativo(Landroid/view/ViewGroup;IIIZ)V

    .line 54
    move-object v0, p0

    iget-object v1, p0, Lcom/itau/empresas/ui/activity/AplicativosActivity;->llAplicativosGerais:Landroid/widget/LinearLayout;

    const v2, 0x7f020123

    const v3, 0x7f0703ae

    const v4, 0x7f0703b4

    const/4 v5, 0x1

    invoke-direct/range {v0 .. v5}, Lcom/itau/empresas/ui/activity/AplicativosActivity;->adicionarItemAplicativo(Landroid/view/ViewGroup;IIIZ)V

    .line 58
    move-object v0, p0

    iget-object v1, p0, Lcom/itau/empresas/ui/activity/AplicativosActivity;->llAplicativosGerais:Landroid/widget/LinearLayout;

    const v2, 0x7f020056

    const v3, 0x7f0703c4

    const v4, 0x7f0703be

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lcom/itau/empresas/ui/activity/AplicativosActivity;->adicionarItemAplicativo(Landroid/view/ViewGroup;IIIZ)V

    .line 63
    move-object v0, p0

    iget-object v1, p0, Lcom/itau/empresas/ui/activity/AplicativosActivity;->llAplicativosRecomendados:Landroid/widget/LinearLayout;

    const v2, 0x7f02010d

    const v3, 0x7f0703c1

    const v4, 0x7f0703bb

    const/4 v5, 0x1

    invoke-direct/range {v0 .. v5}, Lcom/itau/empresas/ui/activity/AplicativosActivity;->adicionarItemAplicativo(Landroid/view/ViewGroup;IIIZ)V

    .line 66
    move-object v0, p0

    iget-object v1, p0, Lcom/itau/empresas/ui/activity/AplicativosActivity;->llAplicativosRecomendados:Landroid/widget/LinearLayout;

    const v2, 0x7f02010c

    const v3, 0x7f0703c0

    const v4, 0x7f0703ba

    const/4 v5, 0x1

    invoke-direct/range {v0 .. v5}, Lcom/itau/empresas/ui/activity/AplicativosActivity;->adicionarItemAplicativo(Landroid/view/ViewGroup;IIIZ)V

    .line 69
    move-object v0, p0

    iget-object v1, p0, Lcom/itau/empresas/ui/activity/AplicativosActivity;->llAplicativosRecomendados:Landroid/widget/LinearLayout;

    const v2, 0x7f02013e

    const v3, 0x7f0703c2

    const v4, 0x7f0703bc

    const/4 v5, 0x1

    invoke-direct/range {v0 .. v5}, Lcom/itau/empresas/ui/activity/AplicativosActivity;->adicionarItemAplicativo(Landroid/view/ViewGroup;IIIZ)V

    .line 72
    move-object v0, p0

    iget-object v1, p0, Lcom/itau/empresas/ui/activity/AplicativosActivity;->llAplicativosRecomendados:Landroid/widget/LinearLayout;

    const v2, 0x7f020109

    const v3, 0x7f0703bf

    const v4, 0x7f0703b9

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lcom/itau/empresas/ui/activity/AplicativosActivity;->adicionarItemAplicativo(Landroid/view/ViewGroup;IIIZ)V

    .line 75
    return-void
.end method

.method public hitAnalytics()Ljava/lang/String;
    .registers 2

    .line 103
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/AplicativosActivity;->toolbarTitle:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public opkeysConsumidas()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Ljava/lang/String;>;"
        }
    .end annotation

    .line 108
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

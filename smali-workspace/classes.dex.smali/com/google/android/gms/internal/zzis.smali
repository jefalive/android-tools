.class public Lcom/google/android/gms/internal/zzis;
.super Ljava/lang/Object;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x8
.end annotation

.annotation runtime Lcom/google/android/gms/internal/zzhb;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/internal/zzis$1;,
        Lcom/google/android/gms/internal/zzis$zzg;,
        Lcom/google/android/gms/internal/zzis$zze;,
        Lcom/google/android/gms/internal/zzis$zzd;,
        Lcom/google/android/gms/internal/zzis$zzf;,
        Lcom/google/android/gms/internal/zzis$zzc;,
        Lcom/google/android/gms/internal/zzis$zzb;,
        Lcom/google/android/gms/internal/zzis$zza;
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .registers 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/internal/zzis$1;)V
    .registers 2

    invoke-direct {p0}, Lcom/google/android/gms/internal/zzis;-><init>()V

    return-void
.end method

.method public static zzP(I)Lcom/google/android/gms/internal/zzis;
    .registers 2

    const/16 v0, 0x13

    if-lt p0, v0, :cond_a

    new-instance v0, Lcom/google/android/gms/internal/zzis$zzg;

    invoke-direct {v0}, Lcom/google/android/gms/internal/zzis$zzg;-><init>()V

    return-object v0

    :cond_a
    const/16 v0, 0x12

    if-lt p0, v0, :cond_14

    new-instance v0, Lcom/google/android/gms/internal/zzis$zze;

    invoke-direct {v0}, Lcom/google/android/gms/internal/zzis$zze;-><init>()V

    return-object v0

    :cond_14
    const/16 v0, 0x11

    if-lt p0, v0, :cond_1e

    new-instance v0, Lcom/google/android/gms/internal/zzis$zzd;

    invoke-direct {v0}, Lcom/google/android/gms/internal/zzis$zzd;-><init>()V

    return-object v0

    :cond_1e
    const/16 v0, 0x10

    if-lt p0, v0, :cond_28

    new-instance v0, Lcom/google/android/gms/internal/zzis$zzf;

    invoke-direct {v0}, Lcom/google/android/gms/internal/zzis$zzf;-><init>()V

    return-object v0

    :cond_28
    const/16 v0, 0xe

    if-lt p0, v0, :cond_32

    new-instance v0, Lcom/google/android/gms/internal/zzis$zzc;

    invoke-direct {v0}, Lcom/google/android/gms/internal/zzis$zzc;-><init>()V

    return-object v0

    :cond_32
    const/16 v0, 0xb

    if-lt p0, v0, :cond_3c

    new-instance v0, Lcom/google/android/gms/internal/zzis$zzb;

    invoke-direct {v0}, Lcom/google/android/gms/internal/zzis$zzb;-><init>()V

    return-object v0

    :cond_3c
    const/16 v0, 0x9

    if-lt p0, v0, :cond_46

    new-instance v0, Lcom/google/android/gms/internal/zzis$zza;

    invoke-direct {v0}, Lcom/google/android/gms/internal/zzis$zza;-><init>()V

    return-object v0

    :cond_46
    new-instance v0, Lcom/google/android/gms/internal/zzis;

    invoke-direct {v0}, Lcom/google/android/gms/internal/zzis;-><init>()V

    return-object v0
.end method


# virtual methods
.method public getDefaultUserAgent(Landroid/content/Context;)Ljava/lang/String;
    .registers 3
    .param p1, "context"    # Landroid/content/Context;

    const-string v0, ""

    return-object v0
.end method

.method public isAttachedToWindow(Landroid/view/View;)Z
    .registers 4
    .param p1, "view"    # Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v0

    if-nez v0, :cond_e

    invoke-virtual {p1}, Landroid/view/View;->getWindowVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-eq v0, v1, :cond_10

    :cond_e
    const/4 v0, 0x1

    goto :goto_11

    :cond_10
    const/4 v0, 0x0

    :goto_11
    return v0
.end method

.method public zza(Landroid/content/Context;Landroid/graphics/Bitmap;ZF)Landroid/graphics/drawable/Drawable;
    .registers 7

    new-instance v0, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, v1, p2}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    return-object v0
.end method

.method public zza(Landroid/net/http/SslError;)Ljava/lang/String;
    .registers 3

    const-string v0, ""

    return-object v0
.end method

.method public zza(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V
    .registers 3

    invoke-virtual {p1, p2}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    return-void
.end method

.method public zza(Landroid/view/ViewTreeObserver;Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V
    .registers 3

    invoke-virtual {p1, p2}, Landroid/view/ViewTreeObserver;->removeGlobalOnLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    return-void
.end method

.method public zza(Landroid/app/DownloadManager$Request;)Z
    .registers 3

    const/4 v0, 0x0

    return v0
.end method

.method public zza(Landroid/content/Context;Landroid/webkit/WebSettings;)Z
    .registers 4

    const/4 v0, 0x0

    return v0
.end method

.method public zza(Landroid/view/Window;)Z
    .registers 3

    const/4 v0, 0x0

    return v0
.end method

.method public zzb(Lcom/google/android/gms/internal/zzjp;Z)Lcom/google/android/gms/internal/zzjq;
    .registers 4

    new-instance v0, Lcom/google/android/gms/internal/zzjq;

    invoke-direct {v0, p1, p2}, Lcom/google/android/gms/internal/zzjq;-><init>(Lcom/google/android/gms/internal/zzjp;Z)V

    return-object v0
.end method

.method public zzb(Landroid/app/Activity;Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V
    .registers 5

    invoke-virtual {p1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v1

    if-eqz v1, :cond_21

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_21

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    if-eqz v0, :cond_21

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    invoke-virtual {p0, v0, p2}, Lcom/google/android/gms/internal/zzis;->zza(Landroid/view/ViewTreeObserver;Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    :cond_21
    return-void
.end method

.method public zzf(Landroid/net/Uri;)Ljava/util/Set;
    .registers 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Landroid/net/Uri;)Ljava/util/Set<Ljava/lang/String;>;"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/net/Uri;->isOpaque()Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v0

    return-object v0

    :cond_b
    invoke-virtual {p1}, Landroid/net/Uri;->getEncodedQuery()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_16

    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v0

    return-object v0

    :cond_16
    new-instance v2, Ljava/util/LinkedHashSet;

    invoke-direct {v2}, Ljava/util/LinkedHashSet;-><init>()V

    const/4 v3, 0x0

    :cond_1c
    const/16 v0, 0x26

    invoke-virtual {v1, v0, v3}, Ljava/lang/String;->indexOf(II)I

    move-result v4

    const/4 v0, -0x1

    if-ne v4, v0, :cond_2a

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v5

    goto :goto_2b

    :cond_2a
    move v5, v4

    :goto_2b
    const/16 v0, 0x3d

    invoke-virtual {v1, v0, v3}, Ljava/lang/String;->indexOf(II)I

    move-result v6

    if-gt v6, v5, :cond_36

    const/4 v0, -0x1

    if-ne v6, v0, :cond_37

    :cond_36
    move v6, v5

    :cond_37
    invoke-virtual {v1, v3, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Landroid/net/Uri;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    add-int/lit8 v3, v5, 0x1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v0

    if-lt v3, v0, :cond_1c

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public zzhv()I
    .registers 2

    const/4 v0, 0x0

    return v0
.end method

.method public zzhw()I
    .registers 2

    const/4 v0, 0x1

    return v0
.end method

.method public zzhx()I
    .registers 2

    const/4 v0, 0x5

    return v0
.end method

.method public zzhy()Landroid/view/ViewGroup$LayoutParams;
    .registers 4

    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    const/4 v1, -0x2

    const/4 v2, -0x2

    invoke-direct {v0, v1, v2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    return-object v0
.end method

.method public zzi(Lcom/google/android/gms/internal/zzjp;)Z
    .registers 3

    if-nez p1, :cond_4

    const/4 v0, 0x0

    return v0

    :cond_4
    invoke-interface {p1}, Lcom/google/android/gms/internal/zzjp;->onPause()V

    const/4 v0, 0x1

    return v0
.end method

.method public zzj(Lcom/google/android/gms/internal/zzjp;)Z
    .registers 3

    if-nez p1, :cond_4

    const/4 v0, 0x0

    return v0

    :cond_4
    invoke-interface {p1}, Lcom/google/android/gms/internal/zzjp;->onResume()V

    const/4 v0, 0x1

    return v0
.end method

.method public zzk(Lcom/google/android/gms/internal/zzjp;)Landroid/webkit/WebChromeClient;
    .registers 3

    const/4 v0, 0x0

    return-object v0
.end method

.method public zzm(Landroid/view/View;)Z
    .registers 3

    const/4 v0, 0x0

    return v0
.end method

.method public zzn(Landroid/view/View;)Z
    .registers 3

    const/4 v0, 0x0

    return v0
.end method

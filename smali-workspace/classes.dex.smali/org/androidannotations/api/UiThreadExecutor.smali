.class public final Lorg/androidannotations/api/UiThreadExecutor;
.super Ljava/lang/Object;
.source "UiThreadExecutor.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/androidannotations/api/UiThreadExecutor$Token;
    }
.end annotation


# static fields
.field private static final HANDLER:Landroid/os/Handler;

.field private static final TOKENS:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<Ljava/lang/String;Lorg/androidannotations/api/UiThreadExecutor$Token;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .line 32
    new-instance v0, Lorg/androidannotations/api/UiThreadExecutor$1;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/androidannotations/api/UiThreadExecutor$1;-><init>(Landroid/os/Looper;)V

    sput-object v0, Lorg/androidannotations/api/UiThreadExecutor;->HANDLER:Landroid/os/Handler;

    .line 45
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lorg/androidannotations/api/UiThreadExecutor;->TOKENS:Ljava/util/Map;

    return-void
.end method

.method static synthetic access$000(Lorg/androidannotations/api/UiThreadExecutor$Token;)V
    .registers 1
    .param p0, "x0"    # Lorg/androidannotations/api/UiThreadExecutor$Token;

    .line 30
    invoke-static {p0}, Lorg/androidannotations/api/UiThreadExecutor;->decrementToken(Lorg/androidannotations/api/UiThreadExecutor$Token;)V

    return-void
.end method

.method private static decrementToken(Lorg/androidannotations/api/UiThreadExecutor$Token;)V
    .registers 7
    .param p0, "token"    # Lorg/androidannotations/api/UiThreadExecutor$Token;

    .line 84
    sget-object v2, Lorg/androidannotations/api/UiThreadExecutor;->TOKENS:Ljava/util/Map;

    monitor-enter v2

    .line 85
    :try_start_3
    iget v0, p0, Lorg/androidannotations/api/UiThreadExecutor$Token;->runnablesCount:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lorg/androidannotations/api/UiThreadExecutor$Token;->runnablesCount:I

    if-nez v0, :cond_1d

    .line 86
    iget-object v3, p0, Lorg/androidannotations/api/UiThreadExecutor$Token;->id:Ljava/lang/String;

    .line 87
    .local v3, "id":Ljava/lang/String;
    sget-object v0, Lorg/androidannotations/api/UiThreadExecutor;->TOKENS:Ljava/util/Map;

    invoke-interface {v0, v3}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lorg/androidannotations/api/UiThreadExecutor$Token;

    .line 88
    .local v4, "old":Lorg/androidannotations/api/UiThreadExecutor$Token;
    if-eq v4, p0, :cond_1d

    .line 91
    sget-object v0, Lorg/androidannotations/api/UiThreadExecutor;->TOKENS:Ljava/util/Map;

    invoke-interface {v0, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1d
    .catchall {:try_start_3 .. :try_end_1d} :catchall_1f

    .line 94
    .end local v3    # "id":Ljava/lang/String;
    .end local v4    # "old":Lorg/androidannotations/api/UiThreadExecutor$Token;
    :cond_1d
    monitor-exit v2

    goto :goto_22

    :catchall_1f
    move-exception v5

    monitor-exit v2

    throw v5

    .line 95
    :goto_22
    return-void
.end method

.method private static nextToken(Ljava/lang/String;)Lorg/androidannotations/api/UiThreadExecutor$Token;
    .registers 6
    .param p0, "id"    # Ljava/lang/String;

    .line 72
    sget-object v2, Lorg/androidannotations/api/UiThreadExecutor;->TOKENS:Ljava/util/Map;

    monitor-enter v2

    .line 73
    :try_start_3
    sget-object v0, Lorg/androidannotations/api/UiThreadExecutor;->TOKENS:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lorg/androidannotations/api/UiThreadExecutor$Token;

    .line 74
    .local v3, "token":Lorg/androidannotations/api/UiThreadExecutor$Token;
    if-nez v3, :cond_19

    .line 75
    new-instance v3, Lorg/androidannotations/api/UiThreadExecutor$Token;

    const/4 v0, 0x0

    invoke-direct {v3, p0, v0}, Lorg/androidannotations/api/UiThreadExecutor$Token;-><init>(Ljava/lang/String;Lorg/androidannotations/api/UiThreadExecutor$1;)V

    .line 76
    sget-object v0, Lorg/androidannotations/api/UiThreadExecutor;->TOKENS:Ljava/util/Map;

    invoke-interface {v0, p0, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 78
    :cond_19
    iget v0, v3, Lorg/androidannotations/api/UiThreadExecutor$Token;->runnablesCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, v3, Lorg/androidannotations/api/UiThreadExecutor$Token;->runnablesCount:I
    :try_end_1f
    .catchall {:try_start_3 .. :try_end_1f} :catchall_21

    .line 79
    monitor-exit v2

    return-object v3

    .line 80
    .end local v3    # "token":Lorg/androidannotations/api/UiThreadExecutor$Token;
    :catchall_21
    move-exception v4

    monitor-exit v2

    throw v4
.end method

.method public static runTask(Ljava/lang/String;Ljava/lang/Runnable;J)V
    .registers 8
    .param p0, "id"    # Ljava/lang/String;
    .param p1, "task"    # Ljava/lang/Runnable;
    .param p2, "delay"    # J

    .line 63
    const-string v0, ""

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 64
    sget-object v0, Lorg/androidannotations/api/UiThreadExecutor;->HANDLER:Landroid/os/Handler;

    invoke-virtual {v0, p1, p2, p3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 65
    return-void

    .line 67
    :cond_e
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    add-long v2, v0, p2

    .line 68
    .local v2, "time":J
    sget-object v0, Lorg/androidannotations/api/UiThreadExecutor;->HANDLER:Landroid/os/Handler;

    invoke-static {p0}, Lorg/androidannotations/api/UiThreadExecutor;->nextToken(Ljava/lang/String;)Lorg/androidannotations/api/UiThreadExecutor$Token;

    move-result-object v1

    invoke-virtual {v0, p1, v1, v2, v3}, Landroid/os/Handler;->postAtTime(Ljava/lang/Runnable;Ljava/lang/Object;J)Z

    .line 69
    return-void
.end method

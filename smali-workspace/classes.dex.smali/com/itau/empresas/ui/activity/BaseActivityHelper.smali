.class final Lcom/itau/empresas/ui/activity/BaseActivityHelper;
.super Ljava/lang/Object;
.source "BaseActivityHelper.java"


# direct methods
.method static desregistraViews(Ljava/util/List;)V
    .registers 4
    .param p0, "views"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<Landroid/view/View;>;)V"
        }
    .end annotation

    .line 34
    if-eqz p0, :cond_2b

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_2b

    .line 35
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_c
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2b

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Landroid/view/View;

    .line 36
    .local v2, "view":Landroid/view/View;
    invoke-static {}, Lde/greenrobot/event/EventBus;->getDefault()Lde/greenrobot/event/EventBus;

    move-result-object v0

    invoke-virtual {v0, v2}, Lde/greenrobot/event/EventBus;->isRegistered(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2a

    .line 37
    invoke-static {}, Lde/greenrobot/event/EventBus;->getDefault()Lde/greenrobot/event/EventBus;

    move-result-object v0

    invoke-virtual {v0, v2}, Lde/greenrobot/event/EventBus;->unregister(Ljava/lang/Object;)V

    .line 39
    .end local v2    # "view":Landroid/view/View;
    :cond_2a
    goto :goto_c

    .line 41
    :cond_2b
    return-void
.end method

.method static registraViews(Ljava/util/List;)V
    .registers 4
    .param p0, "views"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<Landroid/view/View;>;)V"
        }
    .end annotation

    .line 21
    if-eqz p0, :cond_8

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_9

    .line 22
    :cond_8
    return-void

    .line 25
    :cond_9
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_d
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2c

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Landroid/view/View;

    .line 26
    .local v2, "view":Landroid/view/View;
    invoke-static {}, Lde/greenrobot/event/EventBus;->getDefault()Lde/greenrobot/event/EventBus;

    move-result-object v0

    invoke-virtual {v0, v2}, Lde/greenrobot/event/EventBus;->isRegistered(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2b

    .line 27
    invoke-static {}, Lde/greenrobot/event/EventBus;->getDefault()Lde/greenrobot/event/EventBus;

    move-result-object v0

    invoke-virtual {v0, v2}, Lde/greenrobot/event/EventBus;->register(Ljava/lang/Object;)V

    .line 29
    .end local v2    # "view":Landroid/view/View;
    :cond_2b
    goto :goto_d

    .line 30
    :cond_2c
    return-void
.end method

.method static trataErroSemConexao(Lcom/itau/empresas/ui/activity/BaseActivity;)V
    .registers 4
    .param p0, "baseActivity"    # Lcom/itau/empresas/ui/activity/BaseActivity;

    .line 44
    invoke-virtual {p0}, Lcom/itau/empresas/ui/activity/BaseActivity;->escondeDialogoDeProgresso()V

    .line 45
    new-instance v0, Landroid/support/v7/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/support/v7/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 46
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v7/app/AlertDialog$Builder;->setCancelable(Z)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v0

    .line 47
    const v1, 0x7f07047e

    invoke-virtual {v0, v1}, Landroid/support/v7/app/AlertDialog$Builder;->setMessage(I)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/itau/empresas/ui/activity/BaseActivityHelper$2;

    invoke-direct {v1}, Lcom/itau/empresas/ui/activity/BaseActivityHelper$2;-><init>()V

    .line 48
    const v2, 0x7f0704ae

    invoke-virtual {v0, v2, v1}, Landroid/support/v7/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/itau/empresas/ui/activity/BaseActivityHelper$1;

    invoke-direct {v1}, Lcom/itau/empresas/ui/activity/BaseActivityHelper$1;-><init>()V

    .line 56
    const v2, 0x7f070175

    invoke-virtual {v0, v2, v1}, Landroid/support/v7/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v0

    .line 62
    invoke-virtual {v0}, Landroid/support/v7/app/AlertDialog$Builder;->show()Landroid/support/v7/app/AlertDialog;

    .line 63
    return-void
.end method

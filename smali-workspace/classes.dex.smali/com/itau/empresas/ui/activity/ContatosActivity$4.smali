.class Lcom/itau/empresas/ui/activity/ContatosActivity$4;
.super Ljava/lang/Object;
.source "ContatosActivity.java"

# interfaces
.implements Landroid/widget/ExpandableListView$OnChildClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/itau/empresas/ui/activity/ContatosActivity;->aoIniciarActivity()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/itau/empresas/ui/activity/ContatosActivity;


# direct methods
.method constructor <init>(Lcom/itau/empresas/ui/activity/ContatosActivity;)V
    .registers 2
    .param p1, "this$0"    # Lcom/itau/empresas/ui/activity/ContatosActivity;

    .line 104
    iput-object p1, p0, Lcom/itau/empresas/ui/activity/ContatosActivity$4;->this$0:Lcom/itau/empresas/ui/activity/ContatosActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onChildClick(Landroid/widget/ExpandableListView;Landroid/view/View;IIJ)Z
    .registers 13
    .param p1, "parent"    # Landroid/widget/ExpandableListView;
    .param p2, "v"    # Landroid/view/View;
    .param p3, "groupPosition"    # I
    .param p4, "childPosition"    # I
    .param p5, "id"    # J

    .line 109
    invoke-static {}, Lcom/itau/empresas/ui/util/PoyntUtils;->isPoynt()Z

    move-result v0

    if-nez v0, :cond_4c

    .line 110
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/ContatosActivity$4;->this$0:Lcom/itau/empresas/ui/activity/ContatosActivity;

    # getter for: Lcom/itau/empresas/ui/activity/ContatosActivity;->listaTituloContatos:Ljava/util/List;
    invoke-static {v0}, Lcom/itau/empresas/ui/activity/ContatosActivity;->access$000(Lcom/itau/empresas/ui/activity/ContatosActivity;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Ljava/lang/String;

    .line 111
    .local v3, "titulo":Ljava/lang/String;
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/ContatosActivity$4;->this$0:Lcom/itau/empresas/ui/activity/ContatosActivity;

    # getter for: Lcom/itau/empresas/ui/activity/ContatosActivity;->listaItensContatos:Ljava/util/HashMap;
    invoke-static {v0}, Lcom/itau/empresas/ui/activity/ContatosActivity;->access$200(Lcom/itau/empresas/ui/activity/ContatosActivity;)Ljava/util/HashMap;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-interface {v0, p4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/itau/empresas/ui/widgets/ContatosChildItem;

    .line 112
    .local v4, "child":Lcom/itau/empresas/ui/widgets/ContatosChildItem;
    invoke-virtual {v4}, Lcom/itau/empresas/ui/widgets/ContatosChildItem;->getNumeroTelefone()Ljava/lang/String;

    move-result-object v5

    .line 114
    .local v5, "telefone":Ljava/lang/String;
    if-eqz v5, :cond_4c

    .line 115
    invoke-virtual {v4}, Lcom/itau/empresas/ui/widgets/ContatosChildItem;->getNumeroTelefone()Ljava/lang/String;

    move-result-object v0

    const-string v1, " "

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/itau/empresas/ui/util/ViewUtils;->validaNumero(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4c

    .line 116
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/ContatosActivity$4;->this$0:Lcom/itau/empresas/ui/activity/ContatosActivity;

    invoke-virtual {v4}, Lcom/itau/empresas/ui/widgets/ContatosChildItem;->getNumeroTelefone()Ljava/lang/String;

    move-result-object v1

    # setter for: Lcom/itau/empresas/ui/activity/ContatosActivity;->tel:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/itau/empresas/ui/activity/ContatosActivity;->access$302(Lcom/itau/empresas/ui/activity/ContatosActivity;Ljava/lang/String;)Ljava/lang/String;

    .line 117
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/ContatosActivity$4;->this$0:Lcom/itau/empresas/ui/activity/ContatosActivity;

    # invokes: Lcom/itau/empresas/ui/activity/ContatosActivity;->ligar()V
    invoke-static {v0}, Lcom/itau/empresas/ui/activity/ContatosActivity;->access$400(Lcom/itau/empresas/ui/activity/ContatosActivity;)V

    .line 120
    .end local v3    # "titulo":Ljava/lang/String;
    .end local v4    # "child":Lcom/itau/empresas/ui/widgets/ContatosChildItem;
    .end local v5    # "telefone":Ljava/lang/String;
    :cond_4c
    const/4 v0, 0x0

    return v0
.end method

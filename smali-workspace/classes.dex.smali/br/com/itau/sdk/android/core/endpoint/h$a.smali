.class Lbr/com/itau/sdk/android/core/endpoint/h$a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/reflect/InvocationHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lbr/com/itau/sdk/android/core/endpoint/h;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "a"
.end annotation


# instance fields
.field final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class<*>;"
        }
    .end annotation
.end field

.field final synthetic b:Lbr/com/itau/sdk/android/core/endpoint/h;


# direct methods
.method constructor <init>(Lbr/com/itau/sdk/android/core/endpoint/h;Ljava/lang/Class;)V
    .registers 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/lang/Class<*>;)V"
        }
    .end annotation

    .line 217
    iput-object p1, p0, Lbr/com/itau/sdk/android/core/endpoint/h$a;->b:Lbr/com/itau/sdk/android/core/endpoint/h;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 218
    iput-object p2, p0, Lbr/com/itau/sdk/android/core/endpoint/h$a;->a:Ljava/lang/Class;

    .line 219
    return-void
.end method


# virtual methods
.method public invoke(Ljava/lang/Object;Ljava/lang/reflect/Method;[Ljava/lang/Object;)Ljava/lang/Object;
    .registers 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 224
    invoke-virtual {p2}, Ljava/lang/reflect/Method;->getDeclaringClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Ljava/lang/Object;

    if-ne v0, v1, :cond_d

    .line 225
    invoke-virtual {p2, p0, p3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0

    .line 228
    :cond_d
    instance-of v0, p1, Lbr/com/itau/sdk/android/core/token/b;

    if-nez v0, :cond_19

    instance-of v0, p1, Lbr/com/itau/sdk/android/core/c/d;

    if-nez v0, :cond_19

    instance-of v0, p1, Lbr/com/itau/sdk/android/core/login/a;

    if-eqz v0, :cond_1b

    :cond_19
    const/4 v2, 0x1

    goto :goto_1c

    :cond_1b
    const/4 v2, 0x0

    .line 232
    :goto_1c
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/endpoint/h$a;->b:Lbr/com/itau/sdk/android/core/endpoint/h;

    iget-object v1, p0, Lbr/com/itau/sdk/android/core/endpoint/h$a;->a:Ljava/lang/Class;

    invoke-virtual {v0, v2, v1, p2, p3}, Lbr/com/itau/sdk/android/core/endpoint/h;->a(ZLjava/lang/Class;Ljava/lang/reflect/Method;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

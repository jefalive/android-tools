.class public Lcom/itau/empresas/feature/atendimento/activity/AtendimentoDuvidaActivity;
.super Lcom/itau/empresas/ui/activity/BaseActivity;
.source "AtendimentoDuvidaActivity.java"


# instance fields
.field controller:Lcom/itau/empresas/feature/atendimento/controller/AtendimentoController;

.field duvida:Ljava/lang/String;

.field faqsAdapter:Lcom/itau/empresas/feature/atendimento/adapter/AtendimentoFaqsAdapter;

.field listaFaqsDuvidas:Lcom/itau/empresas/ui/widgets/NonScrollListView;

.field private opkeys:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Ljava/lang/String;>;"
        }
    .end annotation
.end field

.field root:Landroid/widget/LinearLayout;

.field toolBar:Landroid/support/v7/widget/Toolbar;


# direct methods
.method public constructor <init>()V
    .registers 2

    .line 35
    invoke-direct {p0}, Lcom/itau/empresas/ui/activity/BaseActivity;-><init>()V

    .line 44
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoDuvidaActivity;->duvida:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$000(Lcom/itau/empresas/feature/atendimento/activity/AtendimentoDuvidaActivity;)Lcom/itau/empresas/CustomApplication;
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/feature/atendimento/activity/AtendimentoDuvidaActivity;

    .line 35
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoDuvidaActivity;->application:Lcom/itau/empresas/CustomApplication;

    return-object v0
.end method

.method private carregaAdapterFaqs(Ljava/util/ArrayList;)V
    .registers 7
    .param p1, "faqs"    # Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/ArrayList<Lcom/itau/empresas/feature/atendimento/model/AtendimentoFAQ;>;)V"
        }
    .end annotation

    .line 85
    invoke-virtual {p0}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoDuvidaActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoDuvidaActivity;->listaFaqsDuvidas:Lcom/itau/empresas/ui/widgets/NonScrollListView;

    .line 86
    const v2, 0x7f030153

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Landroid/view/ViewGroup;

    .line 88
    .local v4, "header":Landroid/view/ViewGroup;
    const v0, 0x7f0e05d3

    invoke-virtual {v4, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoDuvidaActivity;->duvida:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 90
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoDuvidaActivity;->listaFaqsDuvidas:Lcom/itau/empresas/ui/widgets/NonScrollListView;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v4, v1, v2}, Lcom/itau/empresas/ui/widgets/NonScrollListView;->addHeaderView(Landroid/view/View;Ljava/lang/Object;Z)V

    .line 92
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_3d

    .line 93
    new-instance v0, Lcom/itau/empresas/feature/atendimento/adapter/AtendimentoFaqsAdapter;

    invoke-direct {v0, p0, p1}, Lcom/itau/empresas/feature/atendimento/adapter/AtendimentoFaqsAdapter;-><init>(Landroid/content/Context;Ljava/util/ArrayList;)V

    iput-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoDuvidaActivity;->faqsAdapter:Lcom/itau/empresas/feature/atendimento/adapter/AtendimentoFaqsAdapter;

    .line 94
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoDuvidaActivity;->listaFaqsDuvidas:Lcom/itau/empresas/ui/widgets/NonScrollListView;

    iget-object v1, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoDuvidaActivity;->faqsAdapter:Lcom/itau/empresas/feature/atendimento/adapter/AtendimentoFaqsAdapter;

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/widgets/NonScrollListView;->setAdapter(Landroid/widget/ListAdapter;)V

    goto :goto_50

    .line 96
    :cond_3d
    new-instance v0, Lcom/itau/empresas/feature/atendimento/adapter/AtendimentoFaqsAdapter;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/itau/empresas/feature/atendimento/adapter/AtendimentoFaqsAdapter;-><init>(Landroid/content/Context;Ljava/util/ArrayList;)V

    iput-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoDuvidaActivity;->faqsAdapter:Lcom/itau/empresas/feature/atendimento/adapter/AtendimentoFaqsAdapter;

    .line 97
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoDuvidaActivity;->listaFaqsDuvidas:Lcom/itau/empresas/ui/widgets/NonScrollListView;

    iget-object v1, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoDuvidaActivity;->faqsAdapter:Lcom/itau/empresas/feature/atendimento/adapter/AtendimentoFaqsAdapter;

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/widgets/NonScrollListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 100
    :goto_50
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoDuvidaActivity;->configuraClick(Ljava/util/ArrayList;)V

    .line 101
    return-void
.end method

.method private configuraClick(Ljava/util/ArrayList;)V
    .registers 4
    .param p1, "faqs"    # Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/ArrayList<Lcom/itau/empresas/feature/atendimento/model/AtendimentoFAQ;>;)V"
        }
    .end annotation

    .line 104
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoDuvidaActivity;->listaFaqsDuvidas:Lcom/itau/empresas/ui/widgets/NonScrollListView;

    new-instance v1, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoDuvidaActivity$1;

    invoke-direct {v1, p0, p1}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoDuvidaActivity$1;-><init>(Lcom/itau/empresas/feature/atendimento/activity/AtendimentoDuvidaActivity;Ljava/util/ArrayList;)V

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/widgets/NonScrollListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 120
    return-void
.end method

.method private constroiToolbar()V
    .registers 3

    .line 67
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoDuvidaActivity;->toolBar:Landroid/support/v7/widget/Toolbar;

    invoke-static {v0, p0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom;->Builder(Landroid/support/v7/widget/Toolbar;Landroid/support/v7/app/AppCompatActivity;)Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    move-result-object v0

    .line 69
    const v1, 0x7f070703

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->comTitulo(I)Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    move-result-object v0

    .line 70
    invoke-virtual {v0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->comClickListenerFinish()Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    move-result-object v0

    .line 71
    invoke-virtual {v0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->build()Lcom/itau/empresas/ui/util/listener/ToolbarCustom;

    .line 72
    return-void
.end method

.method public static intent(Landroid/content/Context;)Lcom/itau/empresas/feature/atendimento/activity/AtendimentoDuvidaActivity_$IntentBuilder_;
    .registers 2
    .param p0, "context"    # Landroid/content/Context;

    .line 154
    new-instance v0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoDuvidaActivity_$IntentBuilder_;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoDuvidaActivity_$IntentBuilder_;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method private setListaFaqs(Ljava/util/ArrayList;)V
    .registers 2
    .param p1, "retornoFaqs"    # Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/ArrayList<Lcom/itau/empresas/feature/atendimento/model/AtendimentoFAQ;>;)V"
        }
    .end annotation

    .line 80
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoDuvidaActivity;->carregaAdapterFaqs(Ljava/util/ArrayList;)V

    .line 81
    return-void
.end method


# virtual methods
.method carregaElementosDeTela()V
    .registers 8

    .line 52
    invoke-direct {p0}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoDuvidaActivity;->constroiToolbar()V

    .line 54
    invoke-virtual {p0}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoDuvidaActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v6

    .line 55
    .local v6, "bundle":Landroid/os/Bundle;
    invoke-virtual {v6}, Landroid/os/Bundle;->size()I

    move-result v0

    if-nez v0, :cond_23

    .line 56
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoDuvidaActivity;->root:Landroid/widget/LinearLayout;

    const v1, 0x7f0701ef

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoDuvidaActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lbr/com/itau/topsnackbar/TopSnackbar;->make(Landroid/view/ViewGroup;Ljava/lang/CharSequence;I)Lbr/com/itau/topsnackbar/TopSnackbar;

    move-result-object v0

    .line 57
    invoke-virtual {v0}, Lbr/com/itau/topsnackbar/TopSnackbar;->show()Lbr/com/itau/topsnackbar/TopSnackbar;

    .line 58
    return-void

    .line 61
    :cond_23
    invoke-virtual {p0}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoDuvidaActivity;->mostraDialogoDeProgresso()V

    .line 62
    const v0, 0x7f070709

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoDuvidaActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoDuvidaActivity;->duvida:Ljava/lang/String;

    .line 63
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoDuvidaActivity;->controller:Lcom/itau/empresas/feature/atendimento/controller/AtendimentoController;

    iget-object v2, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoDuvidaActivity;->duvida:Ljava/lang/String;

    const-string v5, "true"

    const/4 v1, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual/range {v0 .. v5}, Lcom/itau/empresas/feature/atendimento/controller/AtendimentoController;->buscaFaqs(Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;)V

    .line 64
    return-void
.end method

.method public hitAnalytics()Ljava/lang/String;
    .registers 2

    .line 142
    const v0, 0x7f070703

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoDuvidaActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public onEventMainThread(Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent$Unexpected;)V
    .registers 5
    .param p1, "e"    # Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent$Unexpected;

    .line 123
    invoke-static {p1, p0}, Lcom/itau/empresas/api/ApiHelper;->devoTratarEvento(Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;Lcom/itau/empresas/api/ApiConsumidor;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 124
    return-void

    .line 126
    :cond_7
    invoke-virtual {p0}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoDuvidaActivity;->escondeDialogoDeProgresso()V

    .line 127
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoDuvidaActivity;->root:Landroid/widget/LinearLayout;

    const v1, 0x7f0701ef

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoDuvidaActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lbr/com/itau/topsnackbar/TopSnackbar;->make(Landroid/view/ViewGroup;Ljava/lang/CharSequence;I)Lbr/com/itau/topsnackbar/TopSnackbar;

    move-result-object v0

    .line 128
    invoke-virtual {v0}, Lbr/com/itau/topsnackbar/TopSnackbar;->show()Lbr/com/itau/topsnackbar/TopSnackbar;

    .line 129
    return-void
.end method

.method public onEventMainThread(Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;)V
    .registers 5
    .param p1, "e"    # Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;

    .line 132
    invoke-static {p1, p0}, Lcom/itau/empresas/api/ApiHelper;->devoTratarEvento(Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;Lcom/itau/empresas/api/ApiConsumidor;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 133
    return-void

    .line 135
    :cond_7
    invoke-virtual {p0}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoDuvidaActivity;->escondeDialogoDeProgresso()V

    .line 136
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoDuvidaActivity;->root:Landroid/widget/LinearLayout;

    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;->getException()Lbr/com/itau/sdk/android/core/exception/BackendException;

    move-result-object v1

    invoke-virtual {v1}, Lbr/com/itau/sdk/android/core/exception/BackendException;->getError()Lbr/com/itau/sdk/android/core/model/ErrorDTO;

    move-result-object v1

    invoke-virtual {v1}, Lbr/com/itau/sdk/android/core/model/ErrorDTO;->getMensagem()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lbr/com/itau/topsnackbar/TopSnackbar;->make(Landroid/view/ViewGroup;Ljava/lang/CharSequence;I)Lbr/com/itau/topsnackbar/TopSnackbar;

    move-result-object v0

    .line 137
    invoke-virtual {v0}, Lbr/com/itau/topsnackbar/TopSnackbar;->show()Lbr/com/itau/topsnackbar/TopSnackbar;

    .line 138
    return-void
.end method

.method public onEventMainThread(Lcom/itau/empresas/feature/atendimento/model/AtendimentoVO;)V
    .registers 3
    .param p1, "atendimento"    # Lcom/itau/empresas/feature/atendimento/model/AtendimentoVO;

    .line 75
    invoke-virtual {p1}, Lcom/itau/empresas/feature/atendimento/model/AtendimentoVO;->getFaqs()Ljava/util/ArrayList;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoDuvidaActivity;->setListaFaqs(Ljava/util/ArrayList;)V

    .line 76
    invoke-virtual {p0}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoDuvidaActivity;->escondeDialogoDeProgresso()V

    .line 77
    return-void
.end method

.method public opkeysConsumidas()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Ljava/lang/String;>;"
        }
    .end annotation

    .line 147
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoDuvidaActivity;->opkeys:Ljava/util/List;

    if-nez v0, :cond_b

    .line 148
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoDuvidaActivity;->opkeys:Ljava/util/List;

    .line 150
    :cond_b
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoDuvidaActivity;->opkeys:Ljava/util/List;

    return-object v0
.end method

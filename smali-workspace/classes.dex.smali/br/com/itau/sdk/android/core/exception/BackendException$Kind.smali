.class public final enum Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lbr/com/itau/sdk/android/core/exception/BackendException;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Kind"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;>;"
    }
.end annotation


# static fields
.field private static final $:[B

.field private static $$:I

.field private static final synthetic $VALUES:[Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;

.field public static final enum CONVERSION:Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;

.field public static final enum HTTP:Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;

.field public static final enum NETWORK:Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;

.field public static final enum ROUTER:Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;

.field public static final enum SESSION:Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;

.field public static final enum SIMULTANEOUS_SESSION:Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;

.field public static final enum TOKEN:Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;

.field public static final enum UNEXPECTED:Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;

.field public static final enum VERSION_CONTROL:Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;


# direct methods
.method private static $(III)Ljava/lang/String;
    .registers 9

    add-int/lit8 p1, p1, 0x43

    new-instance v0, Ljava/lang/String;

    sget-object v4, Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;->$:[B

    const/4 v5, 0x0

    rsub-int/lit8 p0, p0, 0x41

    add-int/lit8 p2, p2, 0x4

    new-array v1, p2, [B

    add-int/lit8 p2, p2, -0x1

    if-nez v4, :cond_18

    move v2, p2

    move v3, p0

    :goto_13
    add-int/lit8 p0, p0, 0x1

    add-int/2addr v2, v3

    add-int/lit8 p1, v2, -0x4

    :cond_18
    int-to-byte v2, p1

    aput-byte v2, v1, v5

    if-ne v5, p2, :cond_22

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Ljava/lang/String;-><init>([BI)V

    return-object v0

    :cond_22
    add-int/lit8 v5, v5, 0x1

    move v2, p1

    aget-byte v3, v4, p0

    goto :goto_13
.end method

.method static constructor <clinit>()V
    .registers 5

    .line 96
    const/16 v0, 0x4f

    new-array v0, v0, [B

    fill-array-data v0, :array_142

    sput-object v0, Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;->$:[B

    const/16 v0, 0xe4

    sput v0, Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;->$$:I

    new-instance v0, Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;

    sget-object v1, Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;->$:[B

    const/16 v2, 0xb

    aget-byte v1, v1, v2

    neg-int v1, v1

    sget-object v2, Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;->$:[B

    const/16 v3, 0xc

    aget-byte v2, v2, v3

    const/16 v3, 0x1d

    invoke-static {v3, v1, v2}, Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;->$(III)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;->NETWORK:Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;

    .line 100
    new-instance v0, Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;

    sget-object v1, Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;->$:[B

    const/4 v2, 0x6

    aget-byte v1, v1, v2

    sget-object v2, Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;->$:[B

    const/16 v3, 0xe

    aget-byte v2, v2, v3

    sget-object v3, Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;->$:[B

    const/16 v4, 0x11

    aget-byte v3, v3, v4

    neg-int v3, v3

    invoke-static {v1, v2, v3}, Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;->$(III)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;->CONVERSION:Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;

    .line 104
    new-instance v0, Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;

    sget-object v1, Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;->$:[B

    const/16 v2, 0x1a

    aget-byte v1, v1, v2

    add-int/lit8 v1, v1, -0x1

    sget-object v2, Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;->$:[B

    const/16 v3, 0x2f

    aget-byte v2, v2, v3

    sget-object v3, Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;->$:[B

    const/16 v4, 0xe

    aget-byte v3, v3, v4

    invoke-static {v1, v2, v3}, Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;->$(III)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2}, Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;->HTTP:Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;

    .line 109
    new-instance v0, Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;

    sget-object v1, Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;->$:[B

    const/16 v2, 0x1f

    aget-byte v1, v1, v2

    sget-object v2, Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;->$:[B

    const/16 v3, 0x11

    aget-byte v2, v2, v3

    neg-int v2, v2

    const/16 v3, 0x3d

    invoke-static {v3, v1, v2}, Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;->$(III)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x3

    invoke-direct {v0, v1, v2}, Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;->UNEXPECTED:Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;

    .line 114
    new-instance v0, Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;

    sget-object v1, Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;->$:[B

    const/16 v2, 0x19

    aget-byte v1, v1, v2

    sget-object v2, Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;->$:[B

    const/4 v3, 0x3

    aget-byte v2, v2, v3

    neg-int v2, v2

    sget-object v3, Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;->$:[B

    const/16 v4, 0x9

    aget-byte v3, v3, v4

    invoke-static {v1, v2, v3}, Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;->$(III)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x4

    invoke-direct {v0, v1, v2}, Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;->ROUTER:Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;

    .line 119
    new-instance v0, Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;

    sget-object v1, Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;->$:[B

    const/16 v2, 0x17

    aget-byte v1, v1, v2

    sget-object v2, Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;->$:[B

    const/16 v3, 0x33

    aget-byte v2, v2, v3

    const/16 v3, 0x34

    invoke-static {v3, v1, v2}, Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;->$(III)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;->TOKEN:Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;

    .line 124
    new-instance v0, Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;

    sget-object v1, Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;->$:[B

    const/16 v2, 0x11

    aget-byte v1, v1, v2

    neg-int v1, v1

    sget-object v2, Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;->$:[B

    const/16 v3, 0x1c

    aget-byte v2, v2, v3

    sget-object v3, Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;->$:[B

    const/16 v4, 0xc

    aget-byte v3, v3, v4

    invoke-static {v1, v2, v3}, Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;->$(III)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;->SESSION:Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;

    .line 131
    new-instance v0, Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;

    sget-object v1, Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;->$:[B

    const/16 v2, 0x1c

    aget-byte v1, v1, v2

    const/16 v2, 0x30

    invoke-static {v2, v1, v1}, Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;->$(III)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;->SIMULTANEOUS_SESSION:Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;

    .line 136
    new-instance v0, Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;

    sget-object v1, Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;->$:[B

    const/16 v2, 0xe

    aget-byte v1, v1, v2

    sget-object v2, Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;->$:[B

    const/16 v3, 0x25

    aget-byte v2, v2, v3

    sget-object v3, Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;->$:[B

    const/16 v4, 0xb

    aget-byte v3, v3, v4

    neg-int v3, v3

    invoke-static {v1, v2, v3}, Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;->$(III)Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;->VERSION_CONTROL:Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;

    .line 92
    const/16 v0, 0x9

    new-array v0, v0, [Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;

    sget-object v1, Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;->NETWORK:Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sget-object v1, Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;->CONVERSION:Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    sget-object v1, Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;->HTTP:Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    sget-object v1, Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;->UNEXPECTED:Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    sget-object v1, Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;->ROUTER:Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;

    const/4 v2, 0x4

    aput-object v1, v0, v2

    sget-object v1, Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;->TOKEN:Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;

    const/4 v2, 0x5

    aput-object v1, v0, v2

    sget-object v1, Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;->SESSION:Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;

    const/4 v2, 0x6

    aput-object v1, v0, v2

    sget-object v1, Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;->SIMULTANEOUS_SESSION:Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;

    const/4 v2, 0x7

    aput-object v1, v0, v2

    sget-object v1, Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;->VERSION_CONTROL:Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;

    const/16 v2, 0x8

    aput-object v1, v0, v2

    sput-object v0, Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;->$VALUES:[Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;

    return-void

    nop

    :array_142
    .array-data 1
        0x62t
        0x2bt
        -0x68t
        -0xft
        -0x3t
        -0x5t
        0x17t
        -0x4t
        -0x7t
        0x2t
        0x15t
        -0xbt
        0x3t
        -0x1t
        0x0t
        -0x2t
        0xdt
        -0x6t
        0x8t
        0xct
        -0x5t
        0xct
        -0xft
        0x11t
        -0x5t
        0xet
        0xat
        0x2t
        0x10t
        -0x8t
        -0xat
        0x12t
        0x4t
        -0x6t
        0xat
        0x3t
        -0x5t
        0x13t
        0x7t
        -0x4t
        0x7t
        -0x3t
        0x10t
        0x3t
        0xct
        -0xdt
        0x11t
        0x5t
        -0x6t
        0xat
        0x3t
        0x1t
        0xat
        0x3t
        -0xbt
        0x11t
        0x10t
        0x4t
        0x0t
        -0xat
        0x12t
        0x4t
        -0x6t
        0xat
        0x3t
        -0xdt
        0x11t
        0x5t
        -0x6t
        0xat
        0x3t
        0x15t
        -0x18t
        0x10t
        0x3t
        0xat
        0x2t
        0x1t
        0x1t
    .end array-data
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 92
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;
    .registers 2

    .line 92
    const-class v0, Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;

    return-object v0
.end method

.method public static values()[Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;
    .registers 1

    .line 92
    sget-object v0, Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;->$VALUES:[Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;

    invoke-virtual {v0}, [Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;

    return-object v0
.end method

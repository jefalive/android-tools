.class public Lbr/com/itau/sdk/android/core_ui/password/EletronicPasswordDialog;
.super Lbr/com/itau/sdk/android/core/token/ui/BaseDialog;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lbr/com/itau/sdk/android/core/login/EletronicPassword;


# static fields
.field private static final e:[B

.field private static f:I


# instance fields
.field private a:Landroid/widget/Button;

.field private b:Lbr/com/itau/sdk/android/core/login/EletronicPassword$Callback;

.field private c:Landroid/widget/EditText;

.field private d:I


# direct methods
.method static constructor <clinit>()V
    .registers 3

    const/16 v0, 0x17

    new-array v0, v0, [B

    fill-array-data v0, :array_e

    sput-object v0, Lbr/com/itau/sdk/android/core_ui/password/EletronicPasswordDialog;->e:[B

    const/16 v0, 0xd0

    sput v0, Lbr/com/itau/sdk/android/core_ui/password/EletronicPasswordDialog;->f:I

    return-void

    :array_e
    .array-data 1
        0x7et
        0x3t
        -0x43t
        0x38t
        -0xat
        0x4t
        -0x12t
        -0x1t
        0x0t
        -0x2t
        0x2t
        0x3t
        0x1t
        -0xft
        0x3t
        -0x17t
        0x17t
        -0x8t
        -0x8t
        0x5t
        -0xet
        -0x6t
        0x5t
    .end array-data
.end method

.method public constructor <init>()V
    .registers 2

    .line 30
    invoke-direct {p0}, Lbr/com/itau/sdk/android/core/token/ui/BaseDialog;-><init>()V

    .line 35
    const/4 v0, 0x6

    iput v0, p0, Lbr/com/itau/sdk/android/core_ui/password/EletronicPasswordDialog;->d:I

    return-void
.end method

.method static synthetic a(Lbr/com/itau/sdk/android/core_ui/password/EletronicPasswordDialog;)I
    .registers 2

    .line 30
    iget v0, p0, Lbr/com/itau/sdk/android/core_ui/password/EletronicPasswordDialog;->d:I

    return v0
.end method

.method private static a(III)Ljava/lang/String;
    .registers 9

    const/4 v5, 0x0

    mul-int/lit8 p1, p1, 0x2

    rsub-int/lit8 p1, p1, 0x4

    mul-int/lit8 p0, p0, 0x4

    rsub-int/lit8 p0, p0, 0x14

    sget-object v4, Lbr/com/itau/sdk/android/core_ui/password/EletronicPasswordDialog;->e:[B

    new-instance v0, Ljava/lang/String;

    mul-int/lit8 p2, p2, 0x4

    rsub-int/lit8 p2, p2, 0x65

    new-array v1, p0, [B

    add-int/lit8 p0, p0, -0x1

    if-nez v4, :cond_1f

    move v2, p0

    move v3, p2

    :goto_19
    neg-int v3, v3

    add-int/2addr v2, v3

    add-int/lit8 p2, v2, -0x3

    add-int/lit8 p1, p1, 0x1

    :cond_1f
    int-to-byte v2, p2

    aput-byte v2, v1, v5

    move v2, v5

    add-int/lit8 v5, v5, 0x1

    if-ne v2, p0, :cond_2c

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Ljava/lang/String;-><init>([BI)V

    return-object v0

    :cond_2c
    move v2, p2

    aget-byte v3, v4, p1

    goto :goto_19
.end method

.method private a()V
    .registers 2

    .line 116
    invoke-static {}, Lde/greenrobot/event/EventBus;->getDefault()Lde/greenrobot/event/EventBus;

    move-result-object v0

    invoke-virtual {v0, p0}, Lde/greenrobot/event/EventBus;->isRegistered(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_11

    .line 117
    invoke-static {}, Lde/greenrobot/event/EventBus;->getDefault()Lde/greenrobot/event/EventBus;

    move-result-object v0

    invoke-virtual {v0, p0}, Lde/greenrobot/event/EventBus;->register(Ljava/lang/Object;)V

    .line 118
    :cond_11
    return-void
.end method

.method private a(Landroid/view/View;)V
    .registers 9

    .line 85
    sget v0, Lbr/com/itau/sdk/android/core_ui/R$id;->eletronic_password_dialog:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    .line 86
    if-eqz v4, :cond_b

    .line 87
    invoke-static {v4}, Lbr/com/itau/sdk/android/core_ui/theme/b;->a(Landroid/view/View;)V

    .line 89
    :cond_b
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/password/EletronicPasswordDialog;->loading:Landroid/view/View;

    invoke-static {v0}, Lbr/com/itau/sdk/android/core_ui/theme/b;->b(Landroid/view/View;)V

    .line 90
    sget v0, Lbr/com/itau/sdk/android/core_ui/R$id;->itausdkcore_edit_password:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Landroid/widget/EditText;

    .line 91
    sget v0, Lbr/com/itau/sdk/android/core_ui/R$id;->itausdkcore_dialog_eletronic_password_img_finger_print:I

    .line 92
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Landroid/widget/ImageView;

    .line 94
    invoke-static {}, Lbr/com/itau/sdk/android/core_ui/theme/b;->h()I

    move-result v0

    invoke-virtual {v6, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 95
    invoke-static {}, Lbr/com/itau/sdk/android/core_ui/theme/b;->i()I

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v5, v0, v1, v2, v3}, Landroid/widget/EditText;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    .line 96
    return-void
.end method

.method static synthetic a(Lbr/com/itau/sdk/android/core_ui/password/EletronicPasswordDialog;Landroid/view/View;)V
    .registers 2

    invoke-direct {p0, p1}, Lbr/com/itau/sdk/android/core_ui/password/EletronicPasswordDialog;->b(Landroid/view/View;)V

    return-void
.end method

.method static synthetic b(Lbr/com/itau/sdk/android/core_ui/password/EletronicPasswordDialog;)Landroid/widget/Button;
    .registers 2

    .line 30
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/password/EletronicPasswordDialog;->a:Landroid/widget/Button;

    return-object v0
.end method

.method private b()V
    .registers 2

    .line 127
    invoke-static {}, Lde/greenrobot/event/EventBus;->getDefault()Lde/greenrobot/event/EventBus;

    move-result-object v0

    invoke-virtual {v0, p0}, Lde/greenrobot/event/EventBus;->isRegistered(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 128
    invoke-static {}, Lde/greenrobot/event/EventBus;->getDefault()Lde/greenrobot/event/EventBus;

    move-result-object v0

    invoke-virtual {v0, p0}, Lde/greenrobot/event/EventBus;->unregister(Ljava/lang/Object;)V

    .line 129
    :cond_11
    return-void
.end method

.method private synthetic b(Landroid/view/View;)V
    .registers 2

    .line 72
    invoke-virtual {p0}, Lbr/com/itau/sdk/android/core_ui/password/EletronicPasswordDialog;->hideKeyboard()V

    .line 73
    invoke-virtual {p0}, Lbr/com/itau/sdk/android/core_ui/password/EletronicPasswordDialog;->dismissAllowingStateLoss()V

    .line 74
    return-void
.end method


# virtual methods
.method public hideLoading()V
    .registers 1

    .line 144
    invoke-virtual {p0}, Lbr/com/itau/sdk/android/core_ui/password/EletronicPasswordDialog;->hideKeyboard()V

    .line 145
    invoke-super {p0}, Lbr/com/itau/sdk/android/core/token/ui/BaseDialog;->hideLoading()V

    .line 146
    invoke-direct {p0}, Lbr/com/itau/sdk/android/core_ui/password/EletronicPasswordDialog;->b()V

    .line 147
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .registers 4

    .line 110
    invoke-direct {p0}, Lbr/com/itau/sdk/android/core_ui/password/EletronicPasswordDialog;->a()V

    .line 111
    invoke-virtual {p0}, Lbr/com/itau/sdk/android/core_ui/password/EletronicPasswordDialog;->showLoading()V

    .line 112
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/password/EletronicPasswordDialog;->b:Lbr/com/itau/sdk/android/core/login/EletronicPassword$Callback;

    iget-object v1, p0, Lbr/com/itau/sdk/android/core_ui/password/EletronicPasswordDialog;->c:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lbr/com/itau/sdk/android/core/login/EletronicPassword$Callback;->confirm(Ljava/lang/String;)V

    .line 113
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .registers 5

    .line 40
    invoke-static {}, Lbr/com/itau/sdk/android/core_ui/theme/SDKThemeManager;->getInstance()Lbr/com/itau/sdk/android/core_ui/theme/SDKThemeManager;

    move-result-object v1

    .line 41
    invoke-virtual {v1}, Lbr/com/itau/sdk/android/core/theme/BaseSDKThemeManager;->getDialogTheme()I

    move-result v0

    invoke-super {p0, v0}, Lbr/com/itau/sdk/android/core/token/ui/BaseDialog;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v2

    .line 42
    invoke-virtual {v1, v2}, Lbr/com/itau/sdk/android/core/theme/BaseSDKThemeManager;->checkDialogTheme(Landroid/app/Dialog;)Landroid/app/Dialog;

    move-result-object v0

    return-object v0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .registers 8

    .line 50
    sget v0, Lbr/com/itau/sdk/android/core_ui/R$layout;->itausdkcore_dialog_eletronic_password:I

    invoke-virtual {p1, v0, p2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 51
    sget v0, Lbr/com/itau/sdk/android/core_ui/R$id;->global_loading:I

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/sdk/android/core_ui/password/EletronicPasswordDialog;->loading:Landroid/view/View;

    .line 52
    sget v0, Lbr/com/itau/sdk/android/core_ui/R$id;->itausdkcore_dialog_eletronic_password_button_ok:I

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lbr/com/itau/sdk/android/core_ui/password/EletronicPasswordDialog;->a:Landroid/widget/Button;

    .line 53
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/password/EletronicPasswordDialog;->a:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 54
    sget v0, Lbr/com/itau/sdk/android/core_ui/R$id;->itausdkcore_edit_password:I

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lbr/com/itau/sdk/android/core_ui/password/EletronicPasswordDialog;->c:Landroid/widget/EditText;

    .line 55
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/password/EletronicPasswordDialog;->c:Landroid/widget/EditText;

    new-instance v1, Lbr/com/itau/sdk/android/core_ui/password/b;

    invoke-direct {v1, p0}, Lbr/com/itau/sdk/android/core_ui/password/b;-><init>(Lbr/com/itau/sdk/android/core_ui/password/EletronicPasswordDialog;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 70
    sget v0, Lbr/com/itau/sdk/android/core_ui/R$id;->itausdkcore_dialog_eletronic_password_back:I

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Landroid/widget/ImageView;

    .line 71
    invoke-static {p0}, Lbr/com/itau/sdk/android/core_ui/password/a;->a(Lbr/com/itau/sdk/android/core_ui/password/EletronicPasswordDialog;)Landroid/view/View$OnClickListener;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 76
    invoke-direct {p0, v2}, Lbr/com/itau/sdk/android/core_ui/password/EletronicPasswordDialog;->a(Landroid/view/View;)V

    .line 78
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/password/EletronicPasswordDialog;->c:Landroid/widget/EditText;

    invoke-virtual {p0, v0}, Lbr/com/itau/sdk/android/core_ui/password/EletronicPasswordDialog;->showKeyboard(Landroid/widget/EditText;)V

    .line 79
    invoke-direct {p0}, Lbr/com/itau/sdk/android/core_ui/password/EletronicPasswordDialog;->a()V

    .line 81
    return-object v2
.end method

.method public onEventMainThread(Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;)V
    .registers 2

    .line 132
    invoke-virtual {p0}, Lbr/com/itau/sdk/android/core_ui/password/EletronicPasswordDialog;->hideLoading()V

    .line 133
    return-void
.end method

.method public onEventMainThread(Lbr/com/itau/sdk/android/core_ui/password/EletronicPasswordDialog;)V
    .registers 3

    .line 136
    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core_ui/password/EletronicPasswordDialog;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 137
    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core_ui/password/EletronicPasswordDialog;->hideLoading()V

    .line 138
    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core_ui/password/EletronicPasswordDialog;->dismissAllowingStateLoss()V

    .line 140
    :cond_c
    return-void
.end method

.method public onStop()V
    .registers 1

    .line 122
    invoke-direct {p0}, Lbr/com/itau/sdk/android/core_ui/password/EletronicPasswordDialog;->b()V

    .line 123
    invoke-super {p0}, Lbr/com/itau/sdk/android/core/token/ui/BaseDialog;->onStop()V

    .line 124
    return-void
.end method

.method public setCallback(Lbr/com/itau/sdk/android/core/login/EletronicPassword$Callback;)V
    .registers 2

    .line 99
    iput-object p1, p0, Lbr/com/itau/sdk/android/core_ui/password/EletronicPasswordDialog;->b:Lbr/com/itau/sdk/android/core/login/EletronicPassword$Callback;

    .line 100
    return-void
.end method

.method public show()V
    .registers 5

    .line 104
    invoke-static {}, Lbr/com/itau/sdk/android/core/BackendClient;->currentActivity()Landroid/app/Activity;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Landroid/support/v4/app/FragmentActivity;

    .line 105
    invoke-virtual {v3}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    sget-object v1, Lbr/com/itau/sdk/android/core_ui/password/EletronicPasswordDialog;->e:[B

    const/16 v2, 0x8

    aget-byte v1, v1, v2

    invoke-static {v1, v1, v1}, Lbr/com/itau/sdk/android/core_ui/password/EletronicPasswordDialog;->a(III)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lbr/com/itau/sdk/android/core_ui/password/EletronicPasswordDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 106
    return-void
.end method

.class public Lcom/itau/empresas/feature/itoken/fragment/ITokenLiberacaoOutroCelularFragment;
.super Lcom/itau/empresas/ui/fragment/BaseFragment;
.source "ITokenLiberacaoOutroCelularFragment.java"


# instance fields
.field controller:Lcom/itau/empresas/controller/ITokenController;

.field textoMensagem:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 24
    invoke-direct {p0}, Lcom/itau/empresas/ui/fragment/BaseFragment;-><init>()V

    return-void
.end method


# virtual methods
.method aoCarregarTela()V
    .registers 3

    .line 34
    invoke-virtual {p0}, Lcom/itau/empresas/feature/itoken/fragment/ITokenLiberacaoOutroCelularFragment;->escondeDialogoDeProgresso()V

    .line 35
    invoke-virtual {p0}, Lcom/itau/empresas/feature/itoken/fragment/ITokenLiberacaoOutroCelularFragment;->baseActivity()Lcom/itau/empresas/ui/activity/BaseActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/ui/activity/BaseActivity;->escondeDialogoDeProgresso()V

    .line 36
    iget-object v0, p0, Lcom/itau/empresas/feature/itoken/fragment/ITokenLiberacaoOutroCelularFragment;->textoMensagem:Landroid/widget/TextView;

    const v1, 0x7f0703f1

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/itoken/fragment/ITokenLiberacaoOutroCelularFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 37
    return-void
.end method

.method aoClicarCancelar()V
    .registers 7

    .line 51
    iget-object v0, p0, Lcom/itau/empresas/feature/itoken/fragment/ITokenLiberacaoOutroCelularFragment;->application:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getEventoTracker()Lcom/itau/empresas/ui/util/analytics/TrackerEvento;

    move-result-object v0

    new-instance v1, Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;

    .line 52
    const v2, 0x7f0702df

    invoke-virtual {p0, v2}, Lcom/itau/empresas/feature/itoken/fragment/ITokenLiberacaoOutroCelularFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 53
    const v3, 0x7f0702a0

    invoke-virtual {p0, v3}, Lcom/itau/empresas/feature/itoken/fragment/ITokenLiberacaoOutroCelularFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 54
    const v4, 0x7f070304

    invoke-virtual {p0, v4}, Lcom/itau/empresas/feature/itoken/fragment/ITokenLiberacaoOutroCelularFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 51
    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/analytics/TrackerEvento;->enviarHitEvento(Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;)V

    .line 57
    invoke-virtual {p0}, Lcom/itau/empresas/feature/itoken/fragment/ITokenLiberacaoOutroCelularFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    .line 59
    .local v5, "activity":Landroid/support/v4/app/FragmentActivity;
    instance-of v0, v5, Lcom/itau/empresas/ui/activity/BaseActivity;

    if-eqz v0, :cond_31

    .line 60
    move-object v0, v5

    check-cast v0, Lcom/itau/empresas/ui/activity/BaseActivity;

    invoke-virtual {v0}, Lcom/itau/empresas/ui/activity/BaseActivity;->mostraDialogoDeProgresso()V

    .line 61
    :cond_31
    invoke-static {v5}, Lcom/itau/empresas/ui/util/SnackbarUtils;->tentaDispensarSnackbarNo(Ljava/lang/Object;)V

    .line 63
    iget-object v0, p0, Lcom/itau/empresas/feature/itoken/fragment/ITokenLiberacaoOutroCelularFragment;->controller:Lcom/itau/empresas/controller/ITokenController;

    invoke-virtual {v0}, Lcom/itau/empresas/controller/ITokenController;->cancelarTokenApp()V

    .line 64
    return-void
.end method

.method aoClicarEmInicio()V
    .registers 6

    .line 41
    iget-object v0, p0, Lcom/itau/empresas/feature/itoken/fragment/ITokenLiberacaoOutroCelularFragment;->application:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getEventoTracker()Lcom/itau/empresas/ui/util/analytics/TrackerEvento;

    move-result-object v0

    new-instance v1, Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;

    .line 42
    const v2, 0x7f0702df

    invoke-virtual {p0, v2}, Lcom/itau/empresas/feature/itoken/fragment/ITokenLiberacaoOutroCelularFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 43
    const v3, 0x7f0702a0

    invoke-virtual {p0, v3}, Lcom/itau/empresas/feature/itoken/fragment/ITokenLiberacaoOutroCelularFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 44
    const v4, 0x7f070309

    invoke-virtual {p0, v4}, Lcom/itau/empresas/feature/itoken/fragment/ITokenLiberacaoOutroCelularFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 41
    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/analytics/TrackerEvento;->enviarHitEvento(Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;)V

    .line 46
    invoke-static {p0}, Lcom/itau/empresas/feature/home/HomeLogadaActivity;->intent(Landroid/support/v4/app/Fragment;)Lcom/itau/empresas/feature/home/HomeLogadaActivity_$IntentBuilder_;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/feature/home/HomeLogadaActivity_$IntentBuilder_;->start()Lorg/androidannotations/api/builder/PostActivityStarter;

    .line 47
    return-void
.end method

.method public opkeysConsumidas()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Ljava/lang/String;>;"
        }
    .end annotation

    .line 68
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.class public Lcom/itau/empresas/Evento$EventoBuscaSimulacaoEspecialOuPersonalizada;
.super Ljava/lang/Object;
.source "Evento.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/itau/empresas/Evento;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "EventoBuscaSimulacaoEspecialOuPersonalizada"
.end annotation


# instance fields
.field private ofertaProdutosParceladosSaidaVO:Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosSaidaVO;

.field private personalizado:Z


# direct methods
.method public constructor <init>(Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosSaidaVO;Z)V
    .registers 3
    .param p1, "ofertaProdutosParceladosSaidaVO"    # Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosSaidaVO;
    .param p2, "personalizado"    # Z

    .line 242
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 244
    iput-boolean p2, p0, Lcom/itau/empresas/Evento$EventoBuscaSimulacaoEspecialOuPersonalizada;->personalizado:Z

    .line 245
    iput-object p1, p0, Lcom/itau/empresas/Evento$EventoBuscaSimulacaoEspecialOuPersonalizada;->ofertaProdutosParceladosSaidaVO:Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosSaidaVO;

    .line 246
    return-void
.end method


# virtual methods
.method public getOfertaProdutosParceladosSaidaVO()Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosSaidaVO;
    .registers 2

    .line 253
    iget-object v0, p0, Lcom/itau/empresas/Evento$EventoBuscaSimulacaoEspecialOuPersonalizada;->ofertaProdutosParceladosSaidaVO:Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosSaidaVO;

    return-object v0
.end method

.method public getPersonalizado()Z
    .registers 2

    .line 249
    iget-boolean v0, p0, Lcom/itau/empresas/Evento$EventoBuscaSimulacaoEspecialOuPersonalizada;->personalizado:Z

    return v0
.end method

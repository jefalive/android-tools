.class Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment$3;
.super Ljava/lang/Object;
.source "WidgetSaldoMultiLimiteFragment.java"

# interfaces
.implements Landroid/widget/ExpandableListView$OnGroupClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->carregandoElementosDeTela()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;


# direct methods
.method constructor <init>(Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;)V
    .registers 2
    .param p1, "this$0"    # Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;

    .line 295
    iput-object p1, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment$3;->this$0:Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onGroupClick(Landroid/widget/ExpandableListView;Landroid/view/View;IJ)Z
    .registers 10
    .param p1, "parent"    # Landroid/widget/ExpandableListView;
    .param p2, "clickedView"    # Landroid/view/View;
    .param p3, "groupPosition"    # I
    .param p4, "rowId"    # J

    .line 299
    invoke-virtual {p1, p3}, Landroid/widget/ExpandableListView;->isGroupExpanded(I)Z

    move-result v0

    if-eqz v0, :cond_23

    .line 300
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment$3;->this$0:Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment$3;->this$0:Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;

    const v2, 0x7f0702d4

    invoke-virtual {v1, v2}, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment$3;->this$0:Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;

    const v3, 0x7f070319

    invoke-virtual {v2, v3}, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->analyticsHit(Ljava/lang/String;Ljava/lang/String;)V

    .line 301
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment$3;->this$0:Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->encolherWidget()V

    goto :goto_45

    .line 305
    :cond_23
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment$3;->this$0:Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;

    const/4 v1, 0x1

    # invokes: Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->setCarregando(Z)V
    invoke-static {v0, v1}, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->access$100(Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;Z)V

    .line 306
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment$3;->this$0:Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;

    # invokes: Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->consultaMultiLimite()V
    invoke-static {v0}, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->access$200(Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;)V

    .line 310
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment$3;->this$0:Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment$3;->this$0:Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;

    const v2, 0x7f0702d4

    invoke-virtual {v1, v2}, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment$3;->this$0:Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;

    const v3, 0x7f070317

    invoke-virtual {v2, v3}, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->analyticsHit(Ljava/lang/String;Ljava/lang/String;)V

    .line 312
    :goto_45
    const/4 v0, 0x1

    return v0
.end method

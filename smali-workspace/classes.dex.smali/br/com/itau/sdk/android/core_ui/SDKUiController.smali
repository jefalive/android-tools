.class public Lbr/com/itau/sdk/android/core_ui/SDKUiController;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lbr/com/itau/sdk/android/core/SDKCustomUi;


# static fields
.field private static final c:[B

.field private static d:I


# instance fields
.field private a:Lbr/com/itau/sdk/android/core_ui/token/CardPasswordDialog;

.field private b:Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    const/16 v0, 0x1a

    new-array v0, v0, [B

    fill-array-data v0, :array_e

    sput-object v0, Lbr/com/itau/sdk/android/core_ui/SDKUiController;->c:[B

    const/16 v0, 0x12

    sput v0, Lbr/com/itau/sdk/android/core_ui/SDKUiController;->d:I

    return-void

    :array_e
    .array-data 1
        0x2ct
        -0x22t
        -0x4et
        0x6bt
        -0x3t
        0x10t
        -0xft
        -0x6t
        0x4t
        0x4t
        -0x9t
        0xat
        0x2t
        -0x9t
        -0x6t
        -0x5t
        -0x7t
        0x8t
        -0x10t
        0x4t
        0x4t
        -0x9t
        0xat
        0x2t
        -0x9t
        -0x5t
    .end array-data
.end method

.method public constructor <init>()V
    .registers 1

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(III)Ljava/lang/String;
    .registers 9

    rsub-int/lit8 p2, p2, 0xc

    rsub-int/lit8 p1, p1, 0x19

    new-instance v0, Ljava/lang/String;

    const/4 v5, 0x0

    sget-object v4, Lbr/com/itau/sdk/android/core_ui/SDKUiController;->c:[B

    rsub-int/lit8 p0, p0, 0x74

    new-array v1, p2, [B

    add-int/lit8 p2, p2, -0x1

    if-nez v4, :cond_18

    move v2, p2

    move v3, p1

    :goto_13
    add-int/lit8 p1, p1, 0x1

    add-int/2addr v2, v3

    add-int/lit8 p0, v2, 0x1

    :cond_18
    int-to-byte v2, p0

    aput-byte v2, v1, v5

    move v2, v5

    add-int/lit8 v5, v5, 0x1

    if-ne v2, p2, :cond_25

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Ljava/lang/String;-><init>([BI)V

    return-object v0

    :cond_25
    move v2, p0

    aget-byte v3, v4, p1

    goto :goto_13
.end method

.method static synthetic a(Landroid/content/DialogInterface;I)V
    .registers 2

    invoke-static {p0, p1}, Lbr/com/itau/sdk/android/core_ui/SDKUiController;->b(Landroid/content/DialogInterface;I)V

    return-void
.end method

.method static synthetic a(Ljava/util/concurrent/CountDownLatch;Landroid/content/DialogInterface;)V
    .registers 2

    invoke-static {p0, p1}, Lbr/com/itau/sdk/android/core_ui/SDKUiController;->b(Ljava/util/concurrent/CountDownLatch;Landroid/content/DialogInterface;)V

    return-void
.end method

.method private static synthetic b(Landroid/content/DialogInterface;I)V
    .registers 2

    .line 79
    return-void
.end method

.method private static synthetic b(Ljava/util/concurrent/CountDownLatch;Landroid/content/DialogInterface;)V
    .registers 2

    .line 101
    invoke-virtual {p0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    return-void
.end method

.method public static newInstance()Lbr/com/itau/sdk/android/core_ui/SDKUiController;
    .registers 1

    .line 29
    new-instance v0, Lbr/com/itau/sdk/android/core_ui/SDKUiController;

    invoke-direct {v0}, Lbr/com/itau/sdk/android/core_ui/SDKUiController;-><init>()V

    return-object v0
.end method


# virtual methods
.method public dimisssDialog()V
    .registers 2

    .line 65
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/SDKUiController;->a:Lbr/com/itau/sdk/android/core_ui/token/CardPasswordDialog;

    if-eqz v0, :cond_11

    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/SDKUiController;->a:Lbr/com/itau/sdk/android/core_ui/token/CardPasswordDialog;

    invoke-virtual {v0}, Lbr/com/itau/sdk/android/core_ui/token/CardPasswordDialog;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_11

    .line 66
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/SDKUiController;->a:Lbr/com/itau/sdk/android/core_ui/token/CardPasswordDialog;

    invoke-virtual {v0}, Lbr/com/itau/sdk/android/core_ui/token/CardPasswordDialog;->dismissAllowingStateLoss()V

    .line 68
    :cond_11
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/SDKUiController;->b:Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;

    if-eqz v0, :cond_22

    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/SDKUiController;->b:Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;

    invoke-virtual {v0}, Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_22

    .line 69
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/SDKUiController;->b:Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;

    invoke-virtual {v0}, Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;->dismissAllowingStateLoss()V

    .line 70
    :cond_22
    return-void
.end method

.method public getEletronicPasswordView()Lbr/com/itau/sdk/android/core/login/EletronicPassword;
    .registers 2

    .line 86
    new-instance v0, Lbr/com/itau/sdk/android/core_ui/password/EletronicPasswordDialog;

    invoke-direct {v0}, Lbr/com/itau/sdk/android/core_ui/password/EletronicPasswordDialog;-><init>()V

    return-object v0
.end method

.method public getThemeManager()Lbr/com/itau/sdk/android/core/theme/BaseSDKThemeManager;
    .registers 2

    .line 108
    invoke-static {}, Lbr/com/itau/sdk/android/core_ui/theme/SDKThemeManager;->getInstance()Lbr/com/itau/sdk/android/core_ui/theme/SDKThemeManager;

    move-result-object v0

    return-object v0
.end method

.method public initPasswordFlow(Lbr/com/itau/sdk/android/core/endpoint/TokenErrorHandler;Lbr/com/itau/sdk/android/core/token/ui/BaseDialog$OnTokenAction;Ljava/lang/Integer;)V
    .registers 10

    .line 36
    new-instance v0, Lbr/com/itau/sdk/android/core_ui/token/CardPasswordDialog;

    invoke-direct {v0}, Lbr/com/itau/sdk/android/core_ui/token/CardPasswordDialog;-><init>()V

    iput-object v0, p0, Lbr/com/itau/sdk/android/core_ui/SDKUiController;->a:Lbr/com/itau/sdk/android/core_ui/token/CardPasswordDialog;

    .line 37
    invoke-static {}, Lbr/com/itau/sdk/android/core/BackendClient;->currentActivity()Landroid/app/Activity;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Landroid/support/v4/app/FragmentActivity;

    .line 39
    if-eqz p3, :cond_19

    .line 40
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/SDKUiController;->a:Lbr/com/itau/sdk/android/core_ui/token/CardPasswordDialog;

    invoke-virtual {p3}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lbr/com/itau/sdk/android/core_ui/token/CardPasswordDialog;->setPasswordLength(I)V

    .line 42
    :cond_19
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/SDKUiController;->a:Lbr/com/itau/sdk/android/core_ui/token/CardPasswordDialog;

    invoke-virtual {p1, v0}, Lbr/com/itau/sdk/android/core/endpoint/TokenErrorHandler;->setDialog(Lbr/com/itau/sdk/android/core/token/ui/BaseDialog;)V

    .line 44
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/SDKUiController;->a:Lbr/com/itau/sdk/android/core_ui/token/CardPasswordDialog;

    invoke-virtual {v0, p2}, Lbr/com/itau/sdk/android/core_ui/token/CardPasswordDialog;->setListener(Lbr/com/itau/sdk/android/core/token/ui/BaseDialog$OnTokenAction;)V

    .line 45
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/SDKUiController;->a:Lbr/com/itau/sdk/android/core_ui/token/CardPasswordDialog;

    invoke-virtual {v5}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    sget v2, Lbr/com/itau/sdk/android/core_ui/SDKUiController;->d:I

    add-int/lit8 v2, v2, -0x1

    add-int/lit8 v3, v2, 0x4

    and-int/lit8 v4, v3, 0x3

    invoke-static {v2, v3, v4}, Lbr/com/itau/sdk/android/core_ui/SDKUiController;->a(III)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lbr/com/itau/sdk/android/core_ui/token/CardPasswordDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 46
    return-void
.end method

.method public initTokenFlow(Lbr/com/itau/sdk/android/core/endpoint/TokenErrorHandler;Lbr/com/itau/sdk/android/core/token/ui/BaseDialog$OnTokenAction;Lbr/com/itau/sdk/android/core/model/UiModelDTO;Ljava/lang/String;)V
    .registers 11

    .line 53
    new-instance v0, Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;

    invoke-direct {v0}, Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;-><init>()V

    iput-object v0, p0, Lbr/com/itau/sdk/android/core_ui/SDKUiController;->b:Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;

    .line 54
    invoke-static {}, Lbr/com/itau/sdk/android/core/BackendClient;->currentActivity()Landroid/app/Activity;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Landroid/support/v4/app/FragmentActivity;

    .line 56
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/SDKUiController;->b:Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;

    invoke-virtual {p1, v0}, Lbr/com/itau/sdk/android/core/endpoint/TokenErrorHandler;->setDialog(Lbr/com/itau/sdk/android/core/token/ui/BaseDialog;)V

    .line 57
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/SDKUiController;->b:Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;

    invoke-virtual {v0, p4}, Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;->setOpKey(Ljava/lang/String;)V

    .line 58
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/SDKUiController;->b:Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;

    invoke-virtual {v0, p2}, Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;->setListener(Lbr/com/itau/sdk/android/core/token/ui/BaseDialog$OnTokenAction;)V

    .line 59
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/SDKUiController;->b:Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;

    invoke-virtual {v0, p3}, Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;->setUiModel(Lbr/com/itau/sdk/android/core/model/UiModelDTO;)V

    .line 60
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/SDKUiController;->b:Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;

    invoke-virtual {v5}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const/4 v2, 0x0

    const/16 v3, 0xb

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Lbr/com/itau/sdk/android/core_ui/SDKUiController;->a(III)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 61
    return-void
.end method

.method public onError(Ljava/lang/String;)V
    .registers 10

    .line 74
    invoke-static {}, Lbr/com/itau/sdk/android/core/BackendClient;->currentActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lbr/com/itau/sdk/android/core_ui/theme/b;->a(Landroid/content/Context;)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v0

    .line 75
    invoke-static {}, Lbr/com/itau/sdk/android/core_ui/theme/SDKThemeManager;->getInstance()Lbr/com/itau/sdk/android/core_ui/theme/SDKThemeManager;

    move-result-object v1

    invoke-static {}, Lbr/com/itau/sdk/android/core/BackendClient;->currentActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v1, v2, p1}, Lbr/com/itau/sdk/android/core/util/TypefaceUtils;->getRegularText(Lbr/com/itau/sdk/android/core/theme/BaseSDKThemeManager;Landroid/content/Context;Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v0

    .line 77
    invoke-static {}, Lbr/com/itau/sdk/android/core_ui/theme/SDKThemeManager;->getInstance()Lbr/com/itau/sdk/android/core_ui/theme/SDKThemeManager;

    move-result-object v1

    invoke-static {}, Lbr/com/itau/sdk/android/core/BackendClient;->currentActivity()Landroid/app/Activity;

    move-result-object v2

    sget-object v3, Lbr/com/itau/sdk/android/core_ui/SDKUiController;->c:[B

    const/16 v4, 0xf

    aget-byte v3, v3, v4

    neg-int v3, v3

    add-int/lit8 v4, v3, -0x5

    sget-object v5, Lbr/com/itau/sdk/android/core_ui/SDKUiController;->c:[B

    const/16 v6, 0xb

    aget-byte v5, v5, v6

    invoke-static {v3, v4, v5}, Lbr/com/itau/sdk/android/core_ui/SDKUiController;->a(III)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lbr/com/itau/sdk/android/core/util/TypefaceUtils;->getBoldText(Lbr/com/itau/sdk/android/core/theme/BaseSDKThemeManager;Landroid/content/Context;Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-static {}, Lbr/com/itau/sdk/android/core_ui/b;->a()Landroid/content/DialogInterface$OnClickListener;

    move-result-object v2

    .line 76
    invoke-virtual {v0, v1, v2}, Landroid/support/v7/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v0

    .line 80
    invoke-virtual {v0}, Landroid/support/v7/app/AlertDialog$Builder;->show()Landroid/support/v7/app/AlertDialog;

    move-result-object v7

    .line 81
    invoke-static {v7}, Lbr/com/itau/sdk/android/core_ui/theme/b;->a(Landroid/support/v7/app/AlertDialog;)V

    .line 82
    return-void
.end method

.method public showVersionDialog(Ljava/lang/String;Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;Ljava/util/concurrent/CountDownLatch;)V
    .registers 10

    .line 96
    invoke-static {}, Lbr/com/itau/sdk/android/core/BackendClient;->currentActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lbr/com/itau/sdk/android/core_ui/theme/b;->a(Landroid/content/Context;)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v0

    .line 97
    invoke-virtual {v0, p1}, Landroid/support/v7/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v0

    .line 98
    invoke-virtual {v0, p2, p3}, Landroid/support/v7/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v0

    .line 99
    invoke-virtual {v0, p4, p5}, Landroid/support/v7/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v0

    .line 100
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v7/app/AlertDialog$Builder;->setCancelable(Z)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v0

    invoke-static {p6}, Lbr/com/itau/sdk/android/core_ui/c;->a(Ljava/util/concurrent/CountDownLatch;)Landroid/content/DialogInterface$OnDismissListener;

    move-result-object v1

    .line 101
    invoke-virtual {v0, v1}, Landroid/support/v7/app/AlertDialog$Builder;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v0

    .line 102
    invoke-virtual {v0}, Landroid/support/v7/app/AlertDialog$Builder;->show()Landroid/support/v7/app/AlertDialog;

    move-result-object v2

    .line 103
    invoke-static {v2}, Lbr/com/itau/sdk/android/core_ui/theme/b;->a(Landroid/support/v7/app/AlertDialog;)V

    .line 104
    return-void
.end method

.class final Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingFunctions$15;
.super Ljava/lang/Object;
.source "Easing.java"

# interfaces
.implements Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/EasingFunction;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingFunctions;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .registers 1

    .line 307
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getInterpolation(F)F
    .registers 6
    .param p1, "input"    # F

    .line 317
    const/high16 v0, 0x3f800000    # 1.0f

    cmpl-float v0, p1, v0

    if-nez v0, :cond_9

    const/high16 v0, 0x3f800000    # 1.0f

    goto :goto_18

    :cond_9
    const/high16 v0, 0x3f800000    # 1.0f

    add-float/2addr v0, p1

    const/high16 v1, -0x3ee00000    # -10.0f

    mul-float/2addr v0, v1

    float-to-double v0, v0

    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v0

    double-to-float v0, v0

    neg-float v0, v0

    :goto_18
    return v0
.end method

.class public final Lbr/com/concretesolutions/canarinho/DigitoPara$Builder;
.super Ljava/lang/Object;
.source "DigitoPara.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lbr/com/concretesolutions/canarinho/DigitoPara;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation


# instance fields
.field private complementar:Z

.field private modulo:I

.field private multiplicadores:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Ljava/lang/Integer;>;"
        }
    .end annotation
.end field

.field private somarIndividual:Z

.field private final substituicoes:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray<Ljava/lang/String;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 2

    .line 127
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 129
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lbr/com/concretesolutions/canarinho/DigitoPara$Builder;->multiplicadores:Ljava/util/List;

    .line 133
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lbr/com/concretesolutions/canarinho/DigitoPara$Builder;->substituicoes:Landroid/util/SparseArray;

    return-void
.end method

.method static synthetic access$000(Lbr/com/concretesolutions/canarinho/DigitoPara$Builder;)Ljava/util/List;
    .registers 2
    .param p0, "x0"    # Lbr/com/concretesolutions/canarinho/DigitoPara$Builder;

    .line 127
    iget-object v0, p0, Lbr/com/concretesolutions/canarinho/DigitoPara$Builder;->multiplicadores:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$100(Lbr/com/concretesolutions/canarinho/DigitoPara$Builder;)Z
    .registers 2
    .param p0, "x0"    # Lbr/com/concretesolutions/canarinho/DigitoPara$Builder;

    .line 127
    iget-boolean v0, p0, Lbr/com/concretesolutions/canarinho/DigitoPara$Builder;->complementar:Z

    return v0
.end method

.method static synthetic access$200(Lbr/com/concretesolutions/canarinho/DigitoPara$Builder;)I
    .registers 2
    .param p0, "x0"    # Lbr/com/concretesolutions/canarinho/DigitoPara$Builder;

    .line 127
    iget v0, p0, Lbr/com/concretesolutions/canarinho/DigitoPara$Builder;->modulo:I

    return v0
.end method

.method static synthetic access$300(Lbr/com/concretesolutions/canarinho/DigitoPara$Builder;)Z
    .registers 2
    .param p0, "x0"    # Lbr/com/concretesolutions/canarinho/DigitoPara$Builder;

    .line 127
    iget-boolean v0, p0, Lbr/com/concretesolutions/canarinho/DigitoPara$Builder;->somarIndividual:Z

    return v0
.end method

.method static synthetic access$400(Lbr/com/concretesolutions/canarinho/DigitoPara$Builder;)Landroid/util/SparseArray;
    .registers 2
    .param p0, "x0"    # Lbr/com/concretesolutions/canarinho/DigitoPara$Builder;

    .line 127
    iget-object v0, p0, Lbr/com/concretesolutions/canarinho/DigitoPara$Builder;->substituicoes:Landroid/util/SparseArray;

    return-object v0
.end method


# virtual methods
.method public final build()Lbr/com/concretesolutions/canarinho/DigitoPara;
    .registers 3

    .line 237
    iget-object v0, p0, Lbr/com/concretesolutions/canarinho/DigitoPara$Builder;->multiplicadores:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_e

    .line 238
    const/4 v0, 0x2

    const/16 v1, 0x9

    invoke-virtual {p0, v0, v1}, Lbr/com/concretesolutions/canarinho/DigitoPara$Builder;->comMultiplicadoresDeAte(II)Lbr/com/concretesolutions/canarinho/DigitoPara$Builder;

    .line 241
    :cond_e
    iget v0, p0, Lbr/com/concretesolutions/canarinho/DigitoPara$Builder;->modulo:I

    if-nez v0, :cond_17

    .line 242
    const/16 v0, 0xb

    invoke-virtual {p0, v0}, Lbr/com/concretesolutions/canarinho/DigitoPara$Builder;->mod(I)Lbr/com/concretesolutions/canarinho/DigitoPara$Builder;

    .line 245
    :cond_17
    new-instance v0, Lbr/com/concretesolutions/canarinho/DigitoPara;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lbr/com/concretesolutions/canarinho/DigitoPara;-><init>(Lbr/com/concretesolutions/canarinho/DigitoPara$Builder;Lbr/com/concretesolutions/canarinho/DigitoPara$1;)V

    return-object v0
.end method

.method public final varargs comMultiplicadores([Ljava/lang/Integer;)Lbr/com/concretesolutions/canarinho/DigitoPara$Builder;
    .registers 4
    .param p1, "multiplicadoresEmOrdem"    # [Ljava/lang/Integer;

    .line 220
    iget-object v0, p0, Lbr/com/concretesolutions/canarinho/DigitoPara$Builder;->multiplicadores:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 221
    iget-object v0, p0, Lbr/com/concretesolutions/canarinho/DigitoPara$Builder;->multiplicadores:Ljava/util/List;

    invoke-static {p1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 222
    return-object p0
.end method

.method public final comMultiplicadoresDeAte(II)Lbr/com/concretesolutions/canarinho/DigitoPara$Builder;
    .registers 6
    .param p1, "inicio"    # I
    .param p2, "fim"    # I

    .line 157
    iget-object v0, p0, Lbr/com/concretesolutions/canarinho/DigitoPara$Builder;->multiplicadores:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 159
    move v2, p1

    .local v2, "i":I
    :goto_6
    if-gt v2, p2, :cond_14

    .line 160
    iget-object v0, p0, Lbr/com/concretesolutions/canarinho/DigitoPara$Builder;->multiplicadores:Ljava/util/List;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 159
    add-int/lit8 v2, v2, 0x1

    goto :goto_6

    .line 163
    .end local v2    # "i":I
    :cond_14
    return-object p0
.end method

.method public final complementarAoModulo()Lbr/com/concretesolutions/canarinho/DigitoPara$Builder;
    .registers 2

    .line 189
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbr/com/concretesolutions/canarinho/DigitoPara$Builder;->complementar:Z

    .line 190
    return-object p0
.end method

.method public final mod(I)Lbr/com/concretesolutions/canarinho/DigitoPara$Builder;
    .registers 2
    .param p1, "modulo"    # I

    .line 141
    iput p1, p0, Lbr/com/concretesolutions/canarinho/DigitoPara$Builder;->modulo:I

    .line 142
    return-object p0
.end method

.method public final somandoIndividualmente()Lbr/com/concretesolutions/canarinho/DigitoPara$Builder;
    .registers 2

    .line 176
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbr/com/concretesolutions/canarinho/DigitoPara$Builder;->somarIndividual:Z

    .line 177
    return-object p0
.end method

.method public final varargs trocandoPorSeEncontrar(Ljava/lang/String;[Ljava/lang/Integer;)Lbr/com/concretesolutions/canarinho/DigitoPara$Builder;
    .registers 9
    .param p1, "substituto"    # Ljava/lang/String;
    .param p2, "i"    # [Ljava/lang/Integer;

    .line 202
    iget-object v0, p0, Lbr/com/concretesolutions/canarinho/DigitoPara$Builder;->substituicoes:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    .line 204
    move-object v2, p2

    array-length v3, v2

    const/4 v4, 0x0

    :goto_8
    if-ge v4, v3, :cond_18

    aget-object v5, v2, v4

    .line 205
    .local v5, "integer":Ljava/lang/Integer;
    iget-object v0, p0, Lbr/com/concretesolutions/canarinho/DigitoPara$Builder;->substituicoes:Landroid/util/SparseArray;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1, p1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 204
    .end local v5    # "integer":Ljava/lang/Integer;
    add-int/lit8 v4, v4, 0x1

    goto :goto_8

    .line 208
    :cond_18
    return-object p0
.end method

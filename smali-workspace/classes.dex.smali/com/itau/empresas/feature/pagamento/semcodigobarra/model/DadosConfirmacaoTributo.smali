.class public Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosConfirmacaoTributo;
.super Ljava/lang/Object;
.source "DadosConfirmacaoTributo.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private isViewChange:Z

.field private label:Ljava/lang/String;

.field private valor:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Z)V
    .registers 4
    .param p1, "label"    # Ljava/lang/String;
    .param p2, "valor"    # Ljava/lang/String;
    .param p3, "isViewChange"    # Z

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    iput-object p1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosConfirmacaoTributo;->label:Ljava/lang/String;

    .line 15
    iput-object p2, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosConfirmacaoTributo;->valor:Ljava/lang/String;

    .line 16
    iput-boolean p3, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosConfirmacaoTributo;->isViewChange:Z

    .line 17
    return-void
.end method


# virtual methods
.method public getLabel()Ljava/lang/String;
    .registers 2

    .line 20
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosConfirmacaoTributo;->label:Ljava/lang/String;

    return-object v0
.end method

.method public getValor()Ljava/lang/String;
    .registers 2

    .line 24
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosConfirmacaoTributo;->valor:Ljava/lang/String;

    return-object v0
.end method

.method public isViewChange()Z
    .registers 2

    .line 28
    iget-boolean v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosConfirmacaoTributo;->isViewChange:Z

    return v0
.end method

.class Lcom/wdullaer/materialdatetimepicker/time/TimePickerDialog$5;
.super Ljava/lang/Object;
.source "TimePickerDialog.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/wdullaer/materialdatetimepicker/time/TimePickerDialog;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/wdullaer/materialdatetimepicker/time/TimePickerDialog;


# direct methods
.method constructor <init>(Lcom/wdullaer/materialdatetimepicker/time/TimePickerDialog;)V
    .registers 2

    .line 335
    iput-object p1, p0, Lcom/wdullaer/materialdatetimepicker/time/TimePickerDialog$5;->this$0:Lcom/wdullaer/materialdatetimepicker/time/TimePickerDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .registers 4
    .param p1, "v"    # Landroid/view/View;

    .line 338
    iget-object v0, p0, Lcom/wdullaer/materialdatetimepicker/time/TimePickerDialog$5;->this$0:Lcom/wdullaer/materialdatetimepicker/time/TimePickerDialog;

    invoke-virtual {v0}, Lcom/wdullaer/materialdatetimepicker/time/TimePickerDialog;->tryVibrate()V

    .line 339
    iget-object v0, p0, Lcom/wdullaer/materialdatetimepicker/time/TimePickerDialog$5;->this$0:Lcom/wdullaer/materialdatetimepicker/time/TimePickerDialog;

    # getter for: Lcom/wdullaer/materialdatetimepicker/time/TimePickerDialog;->mTimePicker:Lcom/wdullaer/materialdatetimepicker/time/RadialPickerLayout;
    invoke-static {v0}, Lcom/wdullaer/materialdatetimepicker/time/TimePickerDialog;->access$600(Lcom/wdullaer/materialdatetimepicker/time/TimePickerDialog;)Lcom/wdullaer/materialdatetimepicker/time/RadialPickerLayout;

    move-result-object v0

    invoke-virtual {v0}, Lcom/wdullaer/materialdatetimepicker/time/RadialPickerLayout;->getIsCurrentlyAmOrPm()I

    move-result v1

    .line 340
    .local v1, "amOrPm":I
    if-nez v1, :cond_13

    .line 341
    const/4 v1, 0x1

    goto :goto_17

    .line 342
    :cond_13
    const/4 v0, 0x1

    if-ne v1, v0, :cond_17

    .line 343
    const/4 v1, 0x0

    .line 345
    :cond_17
    :goto_17
    iget-object v0, p0, Lcom/wdullaer/materialdatetimepicker/time/TimePickerDialog$5;->this$0:Lcom/wdullaer/materialdatetimepicker/time/TimePickerDialog;

    # invokes: Lcom/wdullaer/materialdatetimepicker/time/TimePickerDialog;->updateAmPmDisplay(I)V
    invoke-static {v0, v1}, Lcom/wdullaer/materialdatetimepicker/time/TimePickerDialog;->access$700(Lcom/wdullaer/materialdatetimepicker/time/TimePickerDialog;I)V

    .line 346
    iget-object v0, p0, Lcom/wdullaer/materialdatetimepicker/time/TimePickerDialog$5;->this$0:Lcom/wdullaer/materialdatetimepicker/time/TimePickerDialog;

    # getter for: Lcom/wdullaer/materialdatetimepicker/time/TimePickerDialog;->mTimePicker:Lcom/wdullaer/materialdatetimepicker/time/RadialPickerLayout;
    invoke-static {v0}, Lcom/wdullaer/materialdatetimepicker/time/TimePickerDialog;->access$600(Lcom/wdullaer/materialdatetimepicker/time/TimePickerDialog;)Lcom/wdullaer/materialdatetimepicker/time/RadialPickerLayout;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/wdullaer/materialdatetimepicker/time/RadialPickerLayout;->setAmOrPm(I)V

    .line 347
    return-void
.end method

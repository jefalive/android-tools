.class public final Lorg/joda/money/format/MoneyFormatter;
.super Ljava/lang/Object;
.source "MoneyFormatter.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final serialVersionUID:J = 0x8e2d7ed2L


# instance fields
.field private final locale:Ljava/util/Locale;

.field private final parsers:[Lorg/joda/money/format/MoneyParser;

.field private final printers:[Lorg/joda/money/format/MoneyPrinter;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .line 35
    const-class v0, Lorg/joda/money/format/MoneyFormatter;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_a

    const/4 v0, 0x1

    goto :goto_b

    :cond_a
    const/4 v0, 0x0

    :goto_b
    sput-boolean v0, Lorg/joda/money/format/MoneyFormatter;->$assertionsDisabled:Z

    return-void
.end method

.method constructor <init>(Ljava/util/Locale;[Lorg/joda/money/format/MoneyPrinter;[Lorg/joda/money/format/MoneyParser;)V
    .registers 6
    .param p1, "locale"    # Ljava/util/Locale;
    .param p2, "printers"    # [Lorg/joda/money/format/MoneyPrinter;
    .param p3, "parsers"    # [Lorg/joda/money/format/MoneyParser;

    .line 80
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 81
    sget-boolean v0, Lorg/joda/money/format/MoneyFormatter;->$assertionsDisabled:Z

    if-nez v0, :cond_f

    if-nez p1, :cond_f

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 82
    :cond_f
    sget-boolean v0, Lorg/joda/money/format/MoneyFormatter;->$assertionsDisabled:Z

    if-nez v0, :cond_1b

    if-nez p2, :cond_1b

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 83
    :cond_1b
    sget-boolean v0, Lorg/joda/money/format/MoneyFormatter;->$assertionsDisabled:Z

    if-nez v0, :cond_27

    if-nez p3, :cond_27

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 84
    :cond_27
    sget-boolean v0, Lorg/joda/money/format/MoneyFormatter;->$assertionsDisabled:Z

    if-nez v0, :cond_35

    array-length v0, p2

    array-length v1, p3

    if-eq v0, v1, :cond_35

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 85
    :cond_35
    iput-object p1, p0, Lorg/joda/money/format/MoneyFormatter;->locale:Ljava/util/Locale;

    .line 86
    iput-object p2, p0, Lorg/joda/money/format/MoneyFormatter;->printers:[Lorg/joda/money/format/MoneyPrinter;

    .line 87
    iput-object p3, p0, Lorg/joda/money/format/MoneyFormatter;->parsers:[Lorg/joda/money/format/MoneyParser;

    .line 88
    return-void
.end method

.method static checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V
    .registers 3
    .param p0, "object"    # Ljava/lang/Object;
    .param p1, "message"    # Ljava/lang/String;

    .line 64
    if-nez p0, :cond_8

    .line 65
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0, p1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 67
    :cond_8
    return-void
.end method


# virtual methods
.method appendTo(Lorg/joda/money/format/MoneyFormatterBuilder;)V
    .registers 5
    .param p1, "builder"    # Lorg/joda/money/format/MoneyFormatterBuilder;

    .line 97
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    iget-object v0, p0, Lorg/joda/money/format/MoneyFormatter;->printers:[Lorg/joda/money/format/MoneyPrinter;

    array-length v0, v0

    if-ge v2, v0, :cond_14

    .line 98
    iget-object v0, p0, Lorg/joda/money/format/MoneyFormatter;->printers:[Lorg/joda/money/format/MoneyPrinter;

    aget-object v0, v0, v2

    iget-object v1, p0, Lorg/joda/money/format/MoneyFormatter;->parsers:[Lorg/joda/money/format/MoneyParser;

    aget-object v1, v1, v2

    invoke-virtual {p1, v0, v1}, Lorg/joda/money/format/MoneyFormatterBuilder;->append(Lorg/joda/money/format/MoneyPrinter;Lorg/joda/money/format/MoneyParser;)Lorg/joda/money/format/MoneyFormatterBuilder;

    .line 97
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 100
    .end local v2    # "i":I
    :cond_14
    return-void
.end method

.method public getLocale()Ljava/util/Locale;
    .registers 2

    .line 109
    iget-object v0, p0, Lorg/joda/money/format/MoneyFormatter;->locale:Ljava/util/Locale;

    return-object v0
.end method

.method public isParser()Z
    .registers 3

    .line 148
    iget-object v0, p0, Lorg/joda/money/format/MoneyFormatter;->parsers:[Lorg/joda/money/format/MoneyParser;

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_f

    const/4 v0, 0x1

    goto :goto_10

    :cond_f
    const/4 v0, 0x0

    :goto_10
    return v0
.end method

.method public isPrinter()Z
    .registers 3

    .line 136
    iget-object v0, p0, Lorg/joda/money/format/MoneyFormatter;->printers:[Lorg/joda/money/format/MoneyPrinter;

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_f

    const/4 v0, 0x1

    goto :goto_10

    :cond_f
    const/4 v0, 0x0

    :goto_10
    return v0
.end method

.method public parse(Ljava/lang/CharSequence;I)Lorg/joda/money/format/MoneyParseContext;
    .registers 11
    .param p1, "text"    # Ljava/lang/CharSequence;
    .param p2, "startIndex"    # I

    .line 276
    const-string v0, "Text must not be null"

    invoke-static {p1, v0}, Lorg/joda/money/format/MoneyFormatter;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 277
    if-ltz p2, :cond_d

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-le p2, v0, :cond_26

    .line 278
    :cond_d
    new-instance v0, Ljava/lang/StringIndexOutOfBoundsException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid start index: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 280
    :cond_26
    invoke-virtual {p0}, Lorg/joda/money/format/MoneyFormatter;->isParser()Z

    move-result v0

    if-nez v0, :cond_34

    .line 281
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "MoneyFomatter has not been configured to be able to parse"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 283
    :cond_34
    new-instance v3, Lorg/joda/money/format/MoneyParseContext;

    iget-object v0, p0, Lorg/joda/money/format/MoneyFormatter;->locale:Ljava/util/Locale;

    invoke-direct {v3, v0, p1, p2}, Lorg/joda/money/format/MoneyParseContext;-><init>(Ljava/util/Locale;Ljava/lang/CharSequence;I)V

    .line 284
    .local v3, "context":Lorg/joda/money/format/MoneyParseContext;
    iget-object v4, p0, Lorg/joda/money/format/MoneyFormatter;->parsers:[Lorg/joda/money/format/MoneyParser;

    .local v4, "arr$":[Lorg/joda/money/format/MoneyParser;
    array-length v5, v4

    .local v5, "len$":I
    const/4 v6, 0x0

    .local v6, "i$":I
    :goto_3f
    if-ge v6, v5, :cond_50

    aget-object v7, v4, v6

    .line 285
    .local v7, "parser":Lorg/joda/money/format/MoneyParser;
    invoke-interface {v7, v3}, Lorg/joda/money/format/MoneyParser;->parse(Lorg/joda/money/format/MoneyParseContext;)V

    .line 286
    invoke-virtual {v3}, Lorg/joda/money/format/MoneyParseContext;->isError()Z

    move-result v0

    if-eqz v0, :cond_4d

    .line 287
    goto :goto_50

    .line 284
    .end local v7    # "parser":Lorg/joda/money/format/MoneyParser;
    :cond_4d
    add-int/lit8 v6, v6, 0x1

    goto :goto_3f

    .line 290
    .end local v4    # "arr$":[Lorg/joda/money/format/MoneyParser;
    .end local v5    # "len$":I
    .end local v6    # "i$":I
    :cond_50
    :goto_50
    return-object v3
.end method

.method public parseBigMoney(Ljava/lang/CharSequence;)Lorg/joda/money/BigMoney;
    .registers 7
    .param p1, "text"    # Ljava/lang/CharSequence;

    .line 228
    const-string v0, "Text must not be null"

    invoke-static {p1, v0}, Lorg/joda/money/format/MoneyFormatter;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 229
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lorg/joda/money/format/MoneyFormatter;->parse(Ljava/lang/CharSequence;I)Lorg/joda/money/format/MoneyParseContext;

    move-result-object v3

    .line 230
    .local v3, "result":Lorg/joda/money/format/MoneyParseContext;
    invoke-virtual {v3}, Lorg/joda/money/format/MoneyParseContext;->isError()Z

    move-result v0

    if-nez v0, :cond_1c

    invoke-virtual {v3}, Lorg/joda/money/format/MoneyParseContext;->isFullyParsed()Z

    move-result v0

    if-eqz v0, :cond_1c

    invoke-virtual {v3}, Lorg/joda/money/format/MoneyParseContext;->isComplete()Z

    move-result v0

    if-nez v0, :cond_ba

    .line 231
    :cond_1c
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    const/16 v1, 0x40

    if-le v0, v1, :cond_43

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v1, 0x0

    const/16 v2, 0x40

    invoke-interface {p1, v1, v2}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "..."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto :goto_47

    :cond_43
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    .line 232
    .local v4, "str":Ljava/lang/String;
    :goto_47
    invoke-virtual {v3}, Lorg/joda/money/format/MoneyParseContext;->isError()Z

    move-result v0

    if-eqz v0, :cond_74

    .line 233
    new-instance v0, Lorg/joda/money/format/MoneyFormatException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Text could not be parsed at index "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v3}, Lorg/joda/money/format/MoneyParseContext;->getErrorIndex()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/joda/money/format/MoneyFormatException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 234
    :cond_74
    invoke-virtual {v3}, Lorg/joda/money/format/MoneyParseContext;->isFullyParsed()Z

    move-result v0

    if-nez v0, :cond_a1

    .line 235
    new-instance v0, Lorg/joda/money/format/MoneyFormatException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unparsed text found at index "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v3}, Lorg/joda/money/format/MoneyParseContext;->getIndex()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/joda/money/format/MoneyFormatException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 237
    :cond_a1
    new-instance v0, Lorg/joda/money/format/MoneyFormatException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Parsing did not find both currency and amount: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/joda/money/format/MoneyFormatException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 240
    .end local v4    # "str":Ljava/lang/String;
    :cond_ba
    invoke-virtual {v3}, Lorg/joda/money/format/MoneyParseContext;->toBigMoney()Lorg/joda/money/BigMoney;

    move-result-object v0

    return-object v0
.end method

.method public parseMoney(Ljava/lang/CharSequence;)Lorg/joda/money/Money;
    .registers 3
    .param p1, "text"    # Ljava/lang/CharSequence;

    .line 258
    invoke-virtual {p0, p1}, Lorg/joda/money/format/MoneyFormatter;->parseBigMoney(Ljava/lang/CharSequence;)Lorg/joda/money/BigMoney;

    move-result-object v0

    invoke-virtual {v0}, Lorg/joda/money/BigMoney;->toMoney()Lorg/joda/money/Money;

    move-result-object v0

    return-object v0
.end method

.method public print(Lorg/joda/money/BigMoneyProvider;)Ljava/lang/String;
    .registers 4
    .param p1, "moneyProvider"    # Lorg/joda/money/BigMoneyProvider;

    .line 161
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 162
    .local v1, "buf":Ljava/lang/StringBuilder;
    invoke-virtual {p0, v1, p1}, Lorg/joda/money/format/MoneyFormatter;->print(Ljava/lang/Appendable;Lorg/joda/money/BigMoneyProvider;)V

    .line 163
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public print(Ljava/lang/Appendable;Lorg/joda/money/BigMoneyProvider;)V
    .registers 6
    .param p1, "appendable"    # Ljava/lang/Appendable;
    .param p2, "moneyProvider"    # Lorg/joda/money/BigMoneyProvider;

    .line 181
    :try_start_0
    invoke-virtual {p0, p1, p2}, Lorg/joda/money/format/MoneyFormatter;->printIO(Ljava/lang/Appendable;Lorg/joda/money/BigMoneyProvider;)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_3} :catch_4

    .line 184
    goto :goto_f

    .line 182
    :catch_4
    move-exception v2

    .line 183
    .local v2, "ex":Ljava/io/IOException;
    new-instance v0, Lorg/joda/money/format/MoneyFormatException;

    invoke-virtual {v2}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, v2}, Lorg/joda/money/format/MoneyFormatException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0

    .line 185
    .end local v2    # "ex":Ljava/io/IOException;
    :goto_f
    return-void
.end method

.method public printIO(Ljava/lang/Appendable;Lorg/joda/money/BigMoneyProvider;)V
    .registers 11
    .param p1, "appendable"    # Ljava/lang/Appendable;
    .param p2, "moneyProvider"    # Lorg/joda/money/BigMoneyProvider;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 202
    const-string v0, "BigMoneyProvider must not be null"

    invoke-static {p2, v0}, Lorg/joda/money/format/MoneyFormatter;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 203
    invoke-virtual {p0}, Lorg/joda/money/format/MoneyFormatter;->isPrinter()Z

    move-result v0

    if-nez v0, :cond_13

    .line 204
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "MoneyFomatter has not been configured to be able to print"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 207
    :cond_13
    invoke-static {p2}, Lorg/joda/money/BigMoney;->of(Lorg/joda/money/BigMoneyProvider;)Lorg/joda/money/BigMoney;

    move-result-object v2

    .line 208
    .local v2, "money":Lorg/joda/money/BigMoney;
    new-instance v3, Lorg/joda/money/format/MoneyPrintContext;

    iget-object v0, p0, Lorg/joda/money/format/MoneyFormatter;->locale:Ljava/util/Locale;

    invoke-direct {v3, v0}, Lorg/joda/money/format/MoneyPrintContext;-><init>(Ljava/util/Locale;)V

    .line 209
    .local v3, "context":Lorg/joda/money/format/MoneyPrintContext;
    iget-object v4, p0, Lorg/joda/money/format/MoneyFormatter;->printers:[Lorg/joda/money/format/MoneyPrinter;

    .local v4, "arr$":[Lorg/joda/money/format/MoneyPrinter;
    array-length v5, v4

    .local v5, "len$":I
    const/4 v6, 0x0

    .local v6, "i$":I
    :goto_22
    if-ge v6, v5, :cond_2c

    aget-object v7, v4, v6

    .line 210
    .local v7, "printer":Lorg/joda/money/format/MoneyPrinter;
    invoke-interface {v7, v3, p1, v2}, Lorg/joda/money/format/MoneyPrinter;->print(Lorg/joda/money/format/MoneyPrintContext;Ljava/lang/Appendable;Lorg/joda/money/BigMoney;)V

    .line 209
    .end local v7    # "printer":Lorg/joda/money/format/MoneyPrinter;
    add-int/lit8 v6, v6, 0x1

    goto :goto_22

    .line 212
    .end local v4    # "arr$":[Lorg/joda/money/format/MoneyPrinter;
    .end local v5    # "len$":I
    .end local v6    # "i$":I
    :cond_2c
    return-void
.end method

.method public toString()Ljava/lang/String;
    .registers 9

    .line 301
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 302
    .local v2, "buf1":Ljava/lang/StringBuilder;
    invoke-virtual {p0}, Lorg/joda/money/format/MoneyFormatter;->isPrinter()Z

    move-result v0

    if-eqz v0, :cond_1d

    .line 303
    iget-object v3, p0, Lorg/joda/money/format/MoneyFormatter;->printers:[Lorg/joda/money/format/MoneyPrinter;

    .local v3, "arr$":[Lorg/joda/money/format/MoneyPrinter;
    array-length v4, v3

    .local v4, "len$":I
    const/4 v5, 0x0

    .local v5, "i$":I
    :goto_f
    if-ge v5, v4, :cond_1d

    aget-object v6, v3, v5

    .line 304
    .local v6, "printer":Lorg/joda/money/format/MoneyPrinter;
    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 303
    .end local v6    # "printer":Lorg/joda/money/format/MoneyPrinter;
    add-int/lit8 v5, v5, 0x1

    goto :goto_f

    .line 307
    .end local v3    # "arr$":[Lorg/joda/money/format/MoneyPrinter;
    .end local v4    # "len$":I
    .end local v5    # "i$":I
    :cond_1d
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 308
    .local v3, "buf2":Ljava/lang/StringBuilder;
    invoke-virtual {p0}, Lorg/joda/money/format/MoneyFormatter;->isParser()Z

    move-result v0

    if-eqz v0, :cond_3a

    .line 309
    iget-object v4, p0, Lorg/joda/money/format/MoneyFormatter;->parsers:[Lorg/joda/money/format/MoneyParser;

    .local v4, "arr$":[Lorg/joda/money/format/MoneyParser;
    array-length v5, v4

    .local v5, "len$":I
    const/4 v6, 0x0

    .local v6, "i$":I
    :goto_2c
    if-ge v6, v5, :cond_3a

    aget-object v7, v4, v6

    .line 310
    .local v7, "parser":Lorg/joda/money/format/MoneyParser;
    invoke-virtual {v7}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 309
    .end local v7    # "parser":Lorg/joda/money/format/MoneyParser;
    add-int/lit8 v6, v6, 0x1

    goto :goto_2c

    .line 313
    .end local v4    # "arr$":[Lorg/joda/money/format/MoneyParser;
    .end local v5    # "len$":I
    .end local v6    # "i$":I
    :cond_3a
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 314
    .local v4, "str1":Ljava/lang/String;
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 315
    .local v5, "str2":Ljava/lang/String;
    invoke-virtual {p0}, Lorg/joda/money/format/MoneyFormatter;->isPrinter()Z

    move-result v0

    if-eqz v0, :cond_4f

    invoke-virtual {p0}, Lorg/joda/money/format/MoneyFormatter;->isParser()Z

    move-result v0

    if-nez v0, :cond_4f

    .line 316
    return-object v4

    .line 317
    :cond_4f
    invoke-virtual {p0}, Lorg/joda/money/format/MoneyFormatter;->isParser()Z

    move-result v0

    if-eqz v0, :cond_5c

    invoke-virtual {p0}, Lorg/joda/money/format/MoneyFormatter;->isPrinter()Z

    move-result v0

    if-nez v0, :cond_5c

    .line 318
    return-object v5

    .line 319
    :cond_5c
    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_63

    .line 320
    return-object v4

    .line 322
    :cond_63
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public withLocale(Ljava/util/Locale;)Lorg/joda/money/format/MoneyFormatter;
    .registers 5
    .param p1, "locale"    # Ljava/util/Locale;

    .line 122
    const-string v0, "Locale must not be null"

    invoke-static {p1, v0}, Lorg/joda/money/format/MoneyFormatter;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 123
    new-instance v0, Lorg/joda/money/format/MoneyFormatter;

    iget-object v1, p0, Lorg/joda/money/format/MoneyFormatter;->printers:[Lorg/joda/money/format/MoneyPrinter;

    iget-object v2, p0, Lorg/joda/money/format/MoneyFormatter;->parsers:[Lorg/joda/money/format/MoneyParser;

    invoke-direct {v0, p1, v1, v2}, Lorg/joda/money/format/MoneyFormatter;-><init>(Ljava/util/Locale;[Lorg/joda/money/format/MoneyPrinter;[Lorg/joda/money/format/MoneyParser;)V

    return-object v0
.end method

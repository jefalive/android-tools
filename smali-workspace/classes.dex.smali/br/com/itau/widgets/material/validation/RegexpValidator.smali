.class public Lbr/com/itau/widgets/material/validation/RegexpValidator;
.super Lbr/com/itau/widgets/material/validation/METValidator;
.source "RegexpValidator.java"


# instance fields
.field private pattern:Ljava/util/regex/Pattern;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .registers 4
    .param p1, "errorMessage"    # Ljava/lang/String;
    .param p2, "regex"    # Ljava/lang/String;

    .line 15
    invoke-direct {p0, p1}, Lbr/com/itau/widgets/material/validation/METValidator;-><init>(Ljava/lang/String;)V

    .line 16
    invoke-static {p2}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/widgets/material/validation/RegexpValidator;->pattern:Ljava/util/regex/Pattern;

    .line 17
    return-void
.end method


# virtual methods
.method public isValid(Ljava/lang/CharSequence;Z)Z
    .registers 4
    .param p1, "text"    # Ljava/lang/CharSequence;
    .param p2, "isEmpty"    # Z

    .line 21
    iget-object v0, p0, Lbr/com/itau/widgets/material/validation/RegexpValidator;->pattern:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    return v0
.end method

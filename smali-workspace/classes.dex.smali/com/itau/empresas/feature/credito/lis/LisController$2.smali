.class Lcom/itau/empresas/feature/credito/lis/LisController$2;
.super Ljava/lang/Object;
.source "LisController.java"

# interfaces
.implements Lbr/com/itau/sdk/android/core/type/BackendCall;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/itau/empresas/feature/credito/lis/LisController;->buscaOfertaChequeEspecial(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/itau/empresas/feature/credito/lis/LisController;

.field final synthetic val$codigoProduto:Ljava/lang/String;

.field final synthetic val$cpfRepresentante:Ljava/lang/String;

.field final synthetic val$tipoOperacao:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/itau/empresas/feature/credito/lis/LisController;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 5
    .param p1, "this$0"    # Lcom/itau/empresas/feature/credito/lis/LisController;

    .line 44
    iput-object p1, p0, Lcom/itau/empresas/feature/credito/lis/LisController$2;->this$0:Lcom/itau/empresas/feature/credito/lis/LisController;

    iput-object p2, p0, Lcom/itau/empresas/feature/credito/lis/LisController$2;->val$cpfRepresentante:Ljava/lang/String;

    iput-object p3, p0, Lcom/itau/empresas/feature/credito/lis/LisController$2;->val$codigoProduto:Ljava/lang/String;

    iput-object p4, p0, Lcom/itau/empresas/feature/credito/lis/LisController$2;->val$tipoOperacao:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public callBackend()V
    .registers 10

    .line 47
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/LisController$2;->this$0:Lcom/itau/empresas/feature/credito/lis/LisController;

    iget-object v0, v0, Lcom/itau/empresas/feature/credito/lis/LisController;->app:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getContaSelecionada()Lcom/itau/empresas/api/model/ContaOperadorVO;

    move-result-object v8

    .line 48
    .local v8, "contaSelecionada":Lcom/itau/empresas/api/model/ContaOperadorVO;
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/LisController$2;->this$0:Lcom/itau/empresas/feature/credito/lis/LisController;

    # invokes: Lcom/itau/empresas/feature/credito/lis/LisController;->api()Ljava/lang/Object;
    invoke-static {v0}, Lcom/itau/empresas/feature/credito/lis/LisController;->access$200(Lcom/itau/empresas/feature/credito/lis/LisController;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/api/Api;

    .line 49
    invoke-virtual {v8}, Lcom/itau/empresas/api/model/ContaOperadorVO;->getAgencia()Ljava/lang/String;

    move-result-object v1

    .line 50
    invoke-virtual {v8}, Lcom/itau/empresas/api/model/ContaOperadorVO;->getConta()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x7

    invoke-static {v3, v2}, Lcom/itau/empresas/ui/util/ViewUtils;->formataAdicionandoZeros(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 51
    invoke-virtual {v8}, Lcom/itau/empresas/api/model/ContaOperadorVO;->getDac()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/itau/empresas/feature/credito/lis/LisController$2;->val$cpfRepresentante:Ljava/lang/String;

    iget-object v5, p0, Lcom/itau/empresas/feature/credito/lis/LisController$2;->val$codigoProduto:Ljava/lang/String;

    const-string v6, "cheque_especial"

    iget-object v7, p0, Lcom/itau/empresas/feature/credito/lis/LisController$2;->val$tipoOperacao:Ljava/lang/String;

    .line 48
    invoke-interface/range {v0 .. v7}, Lcom/itau/empresas/api/Api;->buscaOfertaChequeEspecial(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/itau/empresas/feature/credito/lis/model/OfertaContratacaoLisVo;

    move-result-object v0

    # invokes: Lcom/itau/empresas/feature/credito/lis/LisController;->post(Ljava/lang/Object;)V
    invoke-static {v0}, Lcom/itau/empresas/feature/credito/lis/LisController;->access$300(Ljava/lang/Object;)V

    .line 54
    return-void
.end method

.class public Lcom/itau/empresas/feature/pagamento/comcodigobarra/model/DadosConfirmacaoComprovanteVO;
.super Ljava/lang/Object;
.source "DadosConfirmacaoComprovanteVO.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private autenticacao:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "autenticacao"
    .end annotation
.end field

.field private codigoComprovante:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "codigo_comprovante"
    .end annotation
.end field

.field private codigoControlePagamento:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "codigo_controle_pagamento"
    .end annotation
.end field

.field private dataAgendamento:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "data_agendamento"
    .end annotation
.end field

.field private dataPagamento:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "data_pagamento"
    .end annotation
.end field

.field private formaPagamento:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "forma_pagamento"
    .end annotation
.end field

.field private horaAgendamento:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "hora_agendamento"
    .end annotation
.end field

.field private horaPagamento:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "hora_pagamento"
    .end annotation
.end field

.field private identificacaoComprovante:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "identificacao_comprovante"
    .end annotation
.end field

.field private moeda:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "moeda"
    .end annotation
.end field

.field private valorPagamento:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "valor_pagamento"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 2

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    const-string v0, "BRL"

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/comcodigobarra/model/DadosConfirmacaoComprovanteVO;->moeda:Ljava/lang/String;

    return-void
.end method

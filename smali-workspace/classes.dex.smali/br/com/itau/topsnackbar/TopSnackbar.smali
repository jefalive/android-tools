.class public final Lbr/com/itau/topsnackbar/TopSnackbar;
.super Ljava/lang/Object;
.source "TopSnackbar.java"


# instance fields
.field final content:Landroid/widget/TextView;

.field duration:I

.field final parent:Landroid/view/ViewGroup;

.field final viewManager:Lbr/com/itau/topsnackbar/ViewManager;


# direct methods
.method private constructor <init>(Landroid/view/ViewGroup;Landroid/widget/TextView;Lbr/com/itau/topsnackbar/ViewManager;)V
    .registers 4
    .param p1, "parent"    # Landroid/view/ViewGroup;
    .param p2, "content"    # Landroid/widget/TextView;
    .param p3, "viewManager"    # Lbr/com/itau/topsnackbar/ViewManager;

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object p1, p0, Lbr/com/itau/topsnackbar/TopSnackbar;->parent:Landroid/view/ViewGroup;

    .line 33
    iput-object p2, p0, Lbr/com/itau/topsnackbar/TopSnackbar;->content:Landroid/widget/TextView;

    .line 34
    iput-object p3, p0, Lbr/com/itau/topsnackbar/TopSnackbar;->viewManager:Lbr/com/itau/topsnackbar/ViewManager;

    .line 35
    return-void
.end method

.method public static dismissPrevious()V
    .registers 2

    .line 75
    invoke-static {}, Lbr/com/itau/topsnackbar/TopSnackbarManager;->getInstance()Lbr/com/itau/topsnackbar/TopSnackbarManager;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lbr/com/itau/topsnackbar/TopSnackbarManager;->dismiss(Lbr/com/itau/topsnackbar/TopSnackbar;)V

    .line 76
    return-void
.end method

.method private static getString(Landroid/view/ViewGroup;I)Ljava/lang/String;
    .registers 3
    .param p0, "parent"    # Landroid/view/ViewGroup;
    .param p1, "text"    # I

    .line 43
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static inflateView(Landroid/view/ViewGroup;)Landroid/widget/TextView;
    .registers 5
    .param p0, "parent"    # Landroid/view/ViewGroup;

    .line 60
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lbr/com/itau/topsnackbar/R$layout;->item_snack:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Landroid/widget/TextView;

    .line 61
    .local v3, "result":Landroid/widget/TextView;
    sget v0, Lbr/com/itau/topsnackbar/R$id;->top_snackbar:I

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setId(I)V

    .line 62
    return-object v3
.end method

.method public static make(Landroid/view/ViewGroup;II)Lbr/com/itau/topsnackbar/TopSnackbar;
    .registers 4
    .param p0, "parent"    # Landroid/view/ViewGroup;
    .param p1, "text"    # I
    .param p2, "duration"    # I

    .line 39
    invoke-static {p0, p1}, Lbr/com/itau/topsnackbar/TopSnackbar;->getString(Landroid/view/ViewGroup;I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0, p2}, Lbr/com/itau/topsnackbar/TopSnackbar;->make(Landroid/view/ViewGroup;Ljava/lang/CharSequence;I)Lbr/com/itau/topsnackbar/TopSnackbar;

    move-result-object v0

    return-object v0
.end method

.method public static make(Landroid/view/ViewGroup;Ljava/lang/CharSequence;I)Lbr/com/itau/topsnackbar/TopSnackbar;
    .registers 9
    .param p0, "parent"    # Landroid/view/ViewGroup;
    .param p1, "text"    # Ljava/lang/CharSequence;
    .param p2, "duration"    # I

    .line 47
    const-string v0, "TopSnackbar"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "make() called with: parent = ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "], text = ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "], duration = ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/Throwable;

    invoke-direct {v2}, Ljava/lang/Throwable;-><init>()V

    invoke-static {v0, v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 48
    invoke-static {p0}, Lbr/com/itau/topsnackbar/ViewManagerFactory;->createManager(Landroid/view/ViewGroup;)Lbr/com/itau/topsnackbar/ViewManager;

    move-result-object v3

    .line 50
    .local v3, "viewManager":Lbr/com/itau/topsnackbar/ViewManager;
    invoke-static {p0}, Lbr/com/itau/topsnackbar/TopSnackbar;->inflateView(Landroid/view/ViewGroup;)Landroid/widget/TextView;

    move-result-object v4

    .line 51
    .local v4, "content":Landroid/widget/TextView;
    invoke-virtual {v4, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 53
    new-instance v5, Lbr/com/itau/topsnackbar/TopSnackbar;

    invoke-direct {v5, p0, v4, v3}, Lbr/com/itau/topsnackbar/TopSnackbar;-><init>(Landroid/view/ViewGroup;Landroid/widget/TextView;Lbr/com/itau/topsnackbar/ViewManager;)V

    .line 54
    .local v5, "topSnackbar":Lbr/com/itau/topsnackbar/TopSnackbar;
    iput p2, v5, Lbr/com/itau/topsnackbar/TopSnackbar;->duration:I

    .line 56
    return-object v5
.end method


# virtual methods
.method public dismiss()V
    .registers 2

    .line 71
    invoke-static {}, Lbr/com/itau/topsnackbar/TopSnackbarManager;->getInstance()Lbr/com/itau/topsnackbar/TopSnackbarManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lbr/com/itau/topsnackbar/TopSnackbarManager;->dismiss(Lbr/com/itau/topsnackbar/TopSnackbar;)V

    .line 72
    return-void
.end method

.method public show()Lbr/com/itau/topsnackbar/TopSnackbar;
    .registers 2

    .line 66
    invoke-static {}, Lbr/com/itau/topsnackbar/TopSnackbarManager;->getInstance()Lbr/com/itau/topsnackbar/TopSnackbarManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lbr/com/itau/topsnackbar/TopSnackbarManager;->show(Lbr/com/itau/topsnackbar/TopSnackbar;)V

    .line 67
    return-object p0
.end method

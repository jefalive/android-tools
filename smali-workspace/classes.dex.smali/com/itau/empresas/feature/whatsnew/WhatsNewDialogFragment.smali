.class public Lcom/itau/empresas/feature/whatsnew/WhatsNewDialogFragment;
.super Landroid/support/v4/app/DialogFragment;
.source "WhatsNewDialogFragment.java"

# interfaces
.implements Lcom/itau/empresas/ui/view/PageIndicator$Configuration;
.implements Landroid/support/v4/view/ViewPager$OnPageChangeListener;
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private botaoAnterior:Landroid/widget/Button;

.field private botaoFecharListener:Lcom/itau/empresas/feature/whatsnew/WhatsNewDialogFragmentListener;

.field private botaoProximo:Landroid/widget/Button;

.field private pageIndex:I

.field private pagerIndicator:Lcom/itau/empresas/ui/view/PageIndicator;

.field private viewPager:Landroid/support/v4/view/ViewPager;

.field private whatsNewAdapter:Lcom/itau/empresas/feature/whatsnew/WhatsNewAdapter;


# direct methods
.method public constructor <init>()V
    .registers 2

    .line 33
    invoke-direct {p0}, Landroid/support/v4/app/DialogFragment;-><init>()V

    .line 41
    const/4 v0, 0x0

    iput v0, p0, Lcom/itau/empresas/feature/whatsnew/WhatsNewDialogFragment;->pageIndex:I

    return-void
.end method

.method private alteraTextoDoBotaoProximo(Z)V
    .registers 4
    .param p1, "ultimoSlide"    # Z

    .line 211
    if-eqz p1, :cond_f

    .line 212
    iget-object v0, p0, Lcom/itau/empresas/feature/whatsnew/WhatsNewDialogFragment;->botaoProximo:Landroid/widget/Button;

    const v1, 0x7f0706eb

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/whatsnew/WhatsNewDialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1b

    .line 214
    :cond_f
    iget-object v0, p0, Lcom/itau/empresas/feature/whatsnew/WhatsNewDialogFragment;->botaoProximo:Landroid/widget/Button;

    const v1, 0x7f0706ec

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/whatsnew/WhatsNewDialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 216
    :goto_1b
    return-void
.end method

.method private atualizaPagerIndicator(I)V
    .registers 4
    .param p1, "pageIndex"    # I

    .line 153
    const/4 v1, 0x0

    .line 155
    .local v1, "primeiroIndice":I
    iput p1, p0, Lcom/itau/empresas/feature/whatsnew/WhatsNewDialogFragment;->pageIndex:I

    .line 157
    if-nez p1, :cond_9

    .line 158
    invoke-direct {p0}, Lcom/itau/empresas/feature/whatsnew/WhatsNewDialogFragment;->exibeApenasBotaoProximo()V

    goto :goto_c

    .line 160
    :cond_9
    invoke-direct {p0}, Lcom/itau/empresas/feature/whatsnew/WhatsNewDialogFragment;->exibeAmbosBotoes()V

    .line 163
    :goto_c
    invoke-direct {p0}, Lcom/itau/empresas/feature/whatsnew/WhatsNewDialogFragment;->estaNoUltimoSlide()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/itau/empresas/feature/whatsnew/WhatsNewDialogFragment;->alteraTextoDoBotaoProximo(Z)V

    .line 165
    iget-object v0, p0, Lcom/itau/empresas/feature/whatsnew/WhatsNewDialogFragment;->pagerIndicator:Lcom/itau/empresas/ui/view/PageIndicator;

    invoke-virtual {v0}, Lcom/itau/empresas/ui/view/PageIndicator;->refresh()V

    .line 166
    return-void
.end method

.method private configuraView(Landroid/view/View;)V
    .registers 4
    .param p1, "view"    # Landroid/view/View;

    .line 128
    const v0, 0x7f0e0487

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    iput-object v0, p0, Lcom/itau/empresas/feature/whatsnew/WhatsNewDialogFragment;->viewPager:Landroid/support/v4/view/ViewPager;

    .line 129
    const v0, 0x7f0e048a

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/ui/view/PageIndicator;

    iput-object v0, p0, Lcom/itau/empresas/feature/whatsnew/WhatsNewDialogFragment;->pagerIndicator:Lcom/itau/empresas/ui/view/PageIndicator;

    .line 130
    const v0, 0x7f0e048b

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/itau/empresas/feature/whatsnew/WhatsNewDialogFragment;->botaoProximo:Landroid/widget/Button;

    .line 131
    const v0, 0x7f0e0489

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/itau/empresas/feature/whatsnew/WhatsNewDialogFragment;->botaoAnterior:Landroid/widget/Button;

    .line 132
    const v0, 0x7f0e021e

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/itau/empresas/ui/view/TextViewIcon;

    .line 133
    .local v1, "botaoFechar":Lcom/itau/empresas/ui/view/TextViewIcon;
    iget-object v0, p0, Lcom/itau/empresas/feature/whatsnew/WhatsNewDialogFragment;->botaoProximo:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 134
    iget-object v0, p0, Lcom/itau/empresas/feature/whatsnew/WhatsNewDialogFragment;->botaoAnterior:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 135
    invoke-virtual {v1, p0}, Lcom/itau/empresas/ui/view/TextViewIcon;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 136
    return-void
.end method

.method private criaWhatsNewAdapter()Lcom/itau/empresas/feature/whatsnew/WhatsNewAdapter;
    .registers 4

    .line 146
    new-instance v0, Lcom/itau/empresas/feature/whatsnew/WhatsNewAdapter;

    invoke-virtual {p0}, Lcom/itau/empresas/feature/whatsnew/WhatsNewDialogFragment;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 147
    invoke-virtual {p0}, Lcom/itau/empresas/feature/whatsnew/WhatsNewDialogFragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/itau/empresas/feature/whatsnew/WhatsNewItemBuilder;->criaListaApartirDosAssets(Landroid/content/Context;)Ljava/util/List;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/itau/empresas/feature/whatsnew/WhatsNewAdapter;-><init>(Landroid/content/Context;Ljava/util/List;)V

    iput-object v0, p0, Lcom/itau/empresas/feature/whatsnew/WhatsNewDialogFragment;->whatsNewAdapter:Lcom/itau/empresas/feature/whatsnew/WhatsNewAdapter;

    .line 148
    iget-object v0, p0, Lcom/itau/empresas/feature/whatsnew/WhatsNewDialogFragment;->whatsNewAdapter:Lcom/itau/empresas/feature/whatsnew/WhatsNewAdapter;

    return-object v0
.end method

.method private estaNoUltimoSlide()Z
    .registers 4

    .line 206
    const/4 v2, 0x1

    .line 207
    .local v2, "numeroSubtracaoUltimoItem":I
    iget v0, p0, Lcom/itau/empresas/feature/whatsnew/WhatsNewDialogFragment;->pageIndex:I

    invoke-virtual {p0}, Lcom/itau/empresas/feature/whatsnew/WhatsNewDialogFragment;->count()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ne v0, v1, :cond_d

    const/4 v0, 0x1

    goto :goto_e

    :cond_d
    const/4 v0, 0x0

    :goto_e
    return v0
.end method

.method private executaNavegacao()V
    .registers 3

    .line 191
    iget-object v0, p0, Lcom/itau/empresas/feature/whatsnew/WhatsNewDialogFragment;->viewPager:Landroid/support/v4/view/ViewPager;

    iget v1, p0, Lcom/itau/empresas/feature/whatsnew/WhatsNewDialogFragment;->pageIndex:I

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 192
    iget v0, p0, Lcom/itau/empresas/feature/whatsnew/WhatsNewDialogFragment;->pageIndex:I

    invoke-direct {p0, v0}, Lcom/itau/empresas/feature/whatsnew/WhatsNewDialogFragment;->atualizaPagerIndicator(I)V

    .line 193
    return-void
.end method

.method private exibeAmbosBotoes()V
    .registers 1

    .line 196
    invoke-direct {p0}, Lcom/itau/empresas/feature/whatsnew/WhatsNewDialogFragment;->exibeBotaoAnterior()V

    .line 197
    invoke-direct {p0}, Lcom/itau/empresas/feature/whatsnew/WhatsNewDialogFragment;->exibeBotaoProximo()V

    .line 198
    return-void
.end method

.method private exibeApenasBotaoProximo()V
    .registers 1

    .line 201
    invoke-direct {p0}, Lcom/itau/empresas/feature/whatsnew/WhatsNewDialogFragment;->ocultaBotaoAnterior()V

    .line 202
    invoke-direct {p0}, Lcom/itau/empresas/feature/whatsnew/WhatsNewDialogFragment;->exibeBotaoProximo()V

    .line 203
    return-void
.end method

.method private exibeBotaoAnterior()V
    .registers 3

    .line 223
    iget-object v0, p0, Lcom/itau/empresas/feature/whatsnew/WhatsNewDialogFragment;->botaoAnterior:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 224
    return-void
.end method

.method private exibeBotaoProximo()V
    .registers 3

    .line 219
    iget-object v0, p0, Lcom/itau/empresas/feature/whatsnew/WhatsNewDialogFragment;->botaoProximo:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 220
    return-void
.end method

.method private incrementaDecrementaPageIndex(Z)V
    .registers 4
    .param p1, "incrementa"    # Z

    .line 183
    if-eqz p1, :cond_9

    .line 184
    iget v0, p0, Lcom/itau/empresas/feature/whatsnew/WhatsNewDialogFragment;->pageIndex:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/itau/empresas/feature/whatsnew/WhatsNewDialogFragment;->pageIndex:I

    goto :goto_f

    .line 186
    :cond_9
    iget v0, p0, Lcom/itau/empresas/feature/whatsnew/WhatsNewDialogFragment;->pageIndex:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/itau/empresas/feature/whatsnew/WhatsNewDialogFragment;->pageIndex:I

    .line 188
    :goto_f
    return-void
.end method

.method private navegaParaAnterior()V
    .registers 2

    .line 174
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/itau/empresas/feature/whatsnew/WhatsNewDialogFragment;->incrementaDecrementaPageIndex(Z)V

    .line 175
    invoke-direct {p0}, Lcom/itau/empresas/feature/whatsnew/WhatsNewDialogFragment;->executaNavegacao()V

    .line 176
    return-void
.end method

.method private navegaParaProximo()V
    .registers 2

    .line 169
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/itau/empresas/feature/whatsnew/WhatsNewDialogFragment;->incrementaDecrementaPageIndex(Z)V

    .line 170
    invoke-direct {p0}, Lcom/itau/empresas/feature/whatsnew/WhatsNewDialogFragment;->executaNavegacao()V

    .line 171
    return-void
.end method

.method private navegaParaTelaAnterior()V
    .registers 2

    .line 179
    iget-object v0, p0, Lcom/itau/empresas/feature/whatsnew/WhatsNewDialogFragment;->botaoFecharListener:Lcom/itau/empresas/feature/whatsnew/WhatsNewDialogFragmentListener;

    invoke-interface {v0}, Lcom/itau/empresas/feature/whatsnew/WhatsNewDialogFragmentListener;->clicouNoBotaoFechar()V

    .line 180
    return-void
.end method

.method public static newInstance()Lcom/itau/empresas/feature/whatsnew/WhatsNewDialogFragment;
    .registers 1

    .line 45
    new-instance v0, Lcom/itau/empresas/feature/whatsnew/WhatsNewDialogFragment;

    invoke-direct {v0}, Lcom/itau/empresas/feature/whatsnew/WhatsNewDialogFragment;-><init>()V

    return-object v0
.end method

.method private ocultaBotaoAnterior()V
    .registers 3

    .line 227
    iget-object v0, p0, Lcom/itau/empresas/feature/whatsnew/WhatsNewDialogFragment;->botaoAnterior:Landroid/widget/Button;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 228
    return-void
.end method

.method private preparaViewPager()V
    .registers 4

    .line 139
    iget-object v0, p0, Lcom/itau/empresas/feature/whatsnew/WhatsNewDialogFragment;->viewPager:Landroid/support/v4/view/ViewPager;

    invoke-direct {p0}, Lcom/itau/empresas/feature/whatsnew/WhatsNewDialogFragment;->criaWhatsNewAdapter()Lcom/itau/empresas/feature/whatsnew/WhatsNewAdapter;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 140
    iget-object v0, p0, Lcom/itau/empresas/feature/whatsnew/WhatsNewDialogFragment;->pagerIndicator:Lcom/itau/empresas/ui/view/PageIndicator;

    const v1, 0x7f0c013b

    const v2, 0x7f0c013c

    invoke-virtual {v0, p0, v1, v2}, Lcom/itau/empresas/ui/view/PageIndicator;->setConfiguration(Lcom/itau/empresas/ui/view/PageIndicator$Configuration;II)V

    .line 141
    iget-object v0, p0, Lcom/itau/empresas/feature/whatsnew/WhatsNewDialogFragment;->viewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, p0}, Landroid/support/v4/view/ViewPager;->addOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    .line 142
    iget-object v0, p0, Lcom/itau/empresas/feature/whatsnew/WhatsNewDialogFragment;->pagerIndicator:Lcom/itau/empresas/ui/view/PageIndicator;

    invoke-virtual {v0}, Lcom/itau/empresas/ui/view/PageIndicator;->refresh()V

    .line 143
    return-void
.end method


# virtual methods
.method public count()I
    .registers 2

    .line 76
    iget-object v0, p0, Lcom/itau/empresas/feature/whatsnew/WhatsNewDialogFragment;->whatsNewAdapter:Lcom/itau/empresas/feature/whatsnew/WhatsNewAdapter;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/whatsnew/WhatsNewAdapter;->getCount()I

    move-result v0

    return v0
.end method

.method public onClick(Landroid/view/View;)V
    .registers 4
    .param p1, "v"    # Landroid/view/View;

    .line 102
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    .line 104
    .local v1, "identificadorView":I
    const v0, 0x7f0e048b

    if-ne v1, v0, :cond_17

    .line 105
    invoke-direct {p0}, Lcom/itau/empresas/feature/whatsnew/WhatsNewDialogFragment;->estaNoUltimoSlide()Z

    move-result v0

    if-eqz v0, :cond_13

    .line 106
    invoke-direct {p0}, Lcom/itau/empresas/feature/whatsnew/WhatsNewDialogFragment;->navegaParaTelaAnterior()V

    goto :goto_28

    .line 108
    :cond_13
    invoke-direct {p0}, Lcom/itau/empresas/feature/whatsnew/WhatsNewDialogFragment;->navegaParaProximo()V

    goto :goto_28

    .line 110
    :cond_17
    const v0, 0x7f0e0489

    if-ne v1, v0, :cond_20

    .line 111
    invoke-direct {p0}, Lcom/itau/empresas/feature/whatsnew/WhatsNewDialogFragment;->navegaParaAnterior()V

    goto :goto_28

    .line 112
    :cond_20
    const v0, 0x7f0e021e

    if-ne v1, v0, :cond_28

    .line 113
    invoke-direct {p0}, Lcom/itau/empresas/feature/whatsnew/WhatsNewDialogFragment;->navegaParaTelaAnterior()V

    .line 115
    :cond_28
    :goto_28
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 50
    invoke-super {p0, p1}, Landroid/support/v4/app/DialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 51
    const/4 v0, 0x2

    const v1, 0x7f090122

    invoke-virtual {p0, v0, v1}, Lcom/itau/empresas/feature/whatsnew/WhatsNewDialogFragment;->setStyle(II)V

    .line 53
    iget-object v0, p0, Lcom/itau/empresas/feature/whatsnew/WhatsNewDialogFragment;->botaoFecharListener:Lcom/itau/empresas/feature/whatsnew/WhatsNewDialogFragmentListener;

    if-nez v0, :cond_1b

    .line 54
    new-instance v0, Ljava/lang/IllegalArgumentException;

    .line 55
    const v1, 0x7f07044f

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/whatsnew/WhatsNewDialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 57
    :cond_1b
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .registers 6
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .line 63
    const v0, 0x7f0300c4

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onPageScrollStateChanged(I)V
    .registers 2
    .param p1, "state"    # I

    .line 97
    return-void
.end method

.method public onPageScrolled(IFI)V
    .registers 4
    .param p1, "position"    # I
    .param p2, "positionOffset"    # F
    .param p3, "positionOffsetPixels"    # I

    .line 92
    return-void
.end method

.method public onPageSelected(I)V
    .registers 2
    .param p1, "position"    # I

    .line 86
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/whatsnew/WhatsNewDialogFragment;->atualizaPagerIndicator(I)V

    .line 87
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .registers 4
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;

    .line 68
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/DialogFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 69
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/whatsnew/WhatsNewDialogFragment;->configuraView(Landroid/view/View;)V

    .line 70
    invoke-direct {p0}, Lcom/itau/empresas/feature/whatsnew/WhatsNewDialogFragment;->preparaViewPager()V

    .line 71
    iget v0, p0, Lcom/itau/empresas/feature/whatsnew/WhatsNewDialogFragment;->pageIndex:I

    invoke-direct {p0, v0}, Lcom/itau/empresas/feature/whatsnew/WhatsNewDialogFragment;->atualizaPagerIndicator(I)V

    .line 72
    return-void
.end method

.method public selected(I)Z
    .registers 3
    .param p1, "i"    # I

    .line 81
    iget v0, p0, Lcom/itau/empresas/feature/whatsnew/WhatsNewDialogFragment;->pageIndex:I

    if-ne v0, p1, :cond_6

    const/4 v0, 0x1

    goto :goto_7

    :cond_6
    const/4 v0, 0x0

    :goto_7
    return v0
.end method

.method setBotaoFecharListener(Lcom/itau/empresas/feature/whatsnew/WhatsNewDialogFragmentListener;)V
    .registers 2
    .param p1, "botaoFecharListener"    # Lcom/itau/empresas/feature/whatsnew/WhatsNewDialogFragmentListener;

    .line 124
    iput-object p1, p0, Lcom/itau/empresas/feature/whatsnew/WhatsNewDialogFragment;->botaoFecharListener:Lcom/itau/empresas/feature/whatsnew/WhatsNewDialogFragmentListener;

    .line 125
    return-void
.end method

.class public Lcom/itau/empresas/feature/recarga/efetivacao/DadosRecargaView;
.super Landroid/widget/RelativeLayout;
.source "DadosRecargaView.java"


# instance fields
.field llRegiao:Landroid/widget/LinearLayout;

.field llTextoNome:Landroid/widget/LinearLayout;

.field textoCelular:Landroid/widget/TextView;

.field textoNome:Landroid/widget/TextView;

.field textoOperadora:Landroid/widget/TextView;

.field textoRegiao:Landroid/widget/TextView;

.field textoValor:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 2
    .param p1, "context"    # Landroid/content/Context;

    .line 40
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 41
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .line 44
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 45
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .line 48
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 49
    return-void
.end method


# virtual methods
.method public setCelular(Ljava/lang/String;Ljava/lang/String;)V
    .registers 5
    .param p1, "ddd"    # Ljava/lang/String;
    .param p2, "numero"    # Ljava/lang/String;

    .line 66
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/DadosRecargaView;->textoCelular:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/itau/empresas/feature/recarga/efetivacao/DadosRecargaView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, p1, p2}, Lcom/itau/empresas/ui/util/ViewUtils;->formataNumeroTelefonico(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 67
    return-void
.end method

.method public setNome(Ljava/lang/String;)V
    .registers 4
    .param p1, "nome"    # Ljava/lang/String;

    .line 58
    if-eqz p1, :cond_8

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_10

    .line 59
    :cond_8
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/DadosRecargaView;->llTextoNome:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_15

    .line 61
    :cond_10
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/DadosRecargaView;->textoNome:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 63
    :goto_15
    return-void
.end method

.method public setOperadora(Ljava/lang/String;)V
    .registers 3
    .param p1, "operadora"    # Ljava/lang/String;

    .line 74
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/DadosRecargaView;->textoOperadora:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 75
    return-void
.end method

.method public setRegiao(Ljava/lang/String;)V
    .registers 4
    .param p1, "regiao"    # Ljava/lang/String;

    .line 78
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/DadosRecargaView;->llRegiao:Landroid/widget/LinearLayout;

    if-eqz p1, :cond_6

    const/4 v1, 0x0

    goto :goto_7

    :cond_6
    const/4 v1, 0x4

    :goto_7
    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 79
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/DadosRecargaView;->textoRegiao:Landroid/widget/TextView;

    if-eqz p1, :cond_10

    move-object v1, p1

    goto :goto_12

    :cond_10
    const-string v1, ""

    :goto_12
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 80
    return-void
.end method

.method public setValor(Ljava/lang/String;)V
    .registers 4
    .param p1, "valor"    # Ljava/lang/String;

    .line 70
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/DadosRecargaView;->textoValor:Landroid/widget/TextView;

    const/4 v1, 0x1

    invoke-static {p1, v1}, Lcom/itau/empresas/ui/util/ValorMonetarioUtils;->formataValorDecimalParaMonetario(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 71
    return-void
.end method

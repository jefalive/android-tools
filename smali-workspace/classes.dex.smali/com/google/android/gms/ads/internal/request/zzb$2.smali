.class Lcom/google/android/gms/ads/internal/request/zzb$2;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/gms/ads/internal/request/zzb;->zzbr()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic zzHj:Lcom/google/android/gms/ads/internal/request/zzb;

.field final synthetic zzHk:Lcom/google/android/gms/internal/zzji;


# direct methods
.method constructor <init>(Lcom/google/android/gms/ads/internal/request/zzb;Lcom/google/android/gms/internal/zzji;)V
    .registers 3

    iput-object p1, p0, Lcom/google/android/gms/ads/internal/request/zzb$2;->zzHj:Lcom/google/android/gms/ads/internal/request/zzb;

    iput-object p2, p0, Lcom/google/android/gms/ads/internal/request/zzb$2;->zzHk:Lcom/google/android/gms/internal/zzji;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .registers 7

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/request/zzb$2;->zzHj:Lcom/google/android/gms/ads/internal/request/zzb;

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/request/zzb;->zza(Lcom/google/android/gms/ads/internal/request/zzb;)Ljava/lang/Object;

    move-result-object v4

    monitor-enter v4

    :try_start_7
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/request/zzb$2;->zzHj:Lcom/google/android/gms/ads/internal/request/zzb;

    iget-object v1, p0, Lcom/google/android/gms/ads/internal/request/zzb$2;->zzHj:Lcom/google/android/gms/ads/internal/request/zzb;

    iget-object v2, p0, Lcom/google/android/gms/ads/internal/request/zzb$2;->zzHj:Lcom/google/android/gms/ads/internal/request/zzb;

    invoke-static {v2}, Lcom/google/android/gms/ads/internal/request/zzb;->zzb(Lcom/google/android/gms/ads/internal/request/zzb;)Lcom/google/android/gms/ads/internal/request/AdRequestInfoParcel$zza;

    move-result-object v2

    iget-object v2, v2, Lcom/google/android/gms/ads/internal/request/AdRequestInfoParcel$zza;->zzrl:Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;

    iget-object v3, p0, Lcom/google/android/gms/ads/internal/request/zzb$2;->zzHk:Lcom/google/android/gms/internal/zzji;

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gms/ads/internal/request/zzb;->zza(Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;Lcom/google/android/gms/internal/zzji;)Lcom/google/android/gms/internal/zzit;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/gms/ads/internal/request/zzb;->zzHi:Lcom/google/android/gms/internal/zzit;

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/request/zzb$2;->zzHj:Lcom/google/android/gms/ads/internal/request/zzb;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/request/zzb;->zzHi:Lcom/google/android/gms/internal/zzit;

    if-nez v0, :cond_34

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/request/zzb$2;->zzHj:Lcom/google/android/gms/ads/internal/request/zzb;

    const-string v1, "Could not start the ad request service."

    const/4 v2, 0x0

    invoke-static {v0, v2, v1}, Lcom/google/android/gms/ads/internal/request/zzb;->zza(Lcom/google/android/gms/ads/internal/request/zzb;ILjava/lang/String;)V

    sget-object v0, Lcom/google/android/gms/internal/zzir;->zzMc:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/gms/ads/internal/request/zzb$2;->zzHj:Lcom/google/android/gms/ads/internal/request/zzb;

    invoke-static {v1}, Lcom/google/android/gms/ads/internal/request/zzb;->zzc(Lcom/google/android/gms/ads/internal/request/zzb;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V
    :try_end_34
    .catchall {:try_start_7 .. :try_end_34} :catchall_36

    :cond_34
    monitor-exit v4

    goto :goto_39

    :catchall_36
    move-exception v5

    monitor-exit v4

    throw v5

    :goto_39
    return-void
.end method

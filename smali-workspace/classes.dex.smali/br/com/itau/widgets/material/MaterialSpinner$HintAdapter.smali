.class Lbr/com/itau/widgets/material/MaterialSpinner$HintAdapter;
.super Landroid/widget/BaseAdapter;
.source "MaterialSpinner.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lbr/com/itau/widgets/material/MaterialSpinner;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "HintAdapter"
.end annotation


# static fields
.field private static final HINT_TYPE:I = -0x1


# instance fields
.field private mContext:Landroid/content/Context;

.field private mSpinnerAdapter:Landroid/widget/SpinnerAdapter;

.field final synthetic this$0:Lbr/com/itau/widgets/material/MaterialSpinner;


# direct methods
.method public constructor <init>(Lbr/com/itau/widgets/material/MaterialSpinner;Landroid/widget/SpinnerAdapter;Landroid/content/Context;)V
    .registers 4
    .param p2, "spinnerAdapter"    # Landroid/widget/SpinnerAdapter;
    .param p3, "context"    # Landroid/content/Context;

    .line 784
    iput-object p1, p0, Lbr/com/itau/widgets/material/MaterialSpinner$HintAdapter;->this$0:Lbr/com/itau/widgets/material/MaterialSpinner;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 785
    iput-object p2, p0, Lbr/com/itau/widgets/material/MaterialSpinner$HintAdapter;->mSpinnerAdapter:Landroid/widget/SpinnerAdapter;

    .line 786
    iput-object p3, p0, Lbr/com/itau/widgets/material/MaterialSpinner$HintAdapter;->mContext:Landroid/content/Context;

    .line 787
    return-void
.end method

.method private buildView(ILandroid/view/View;Landroid/view/ViewGroup;Z)Landroid/view/View;
    .registers 7
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;
    .param p4, "isDropDownView"    # Z

    .line 835
    invoke-virtual {p0, p1}, Lbr/com/itau/widgets/material/MaterialSpinner$HintAdapter;->getItemViewType(I)I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_c

    .line 836
    invoke-direct {p0, p3, p4}, Lbr/com/itau/widgets/material/MaterialSpinner$HintAdapter;->getHintView(Landroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0

    .line 840
    :cond_c
    if-eqz p2, :cond_2b

    .line 841
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_2a

    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Ljava/lang/Integer;

    if-eqz v0, :cond_2a

    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_2a

    goto :goto_2b

    :cond_2a
    const/4 p2, 0x0

    .line 843
    :cond_2b
    :goto_2b
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner$HintAdapter;->this$0:Lbr/com/itau/widgets/material/MaterialSpinner;

    # getter for: Lbr/com/itau/widgets/material/MaterialSpinner;->hint:Ljava/lang/CharSequence;
    invoke-static {v0}, Lbr/com/itau/widgets/material/MaterialSpinner;->access$000(Lbr/com/itau/widgets/material/MaterialSpinner;)Ljava/lang/CharSequence;

    move-result-object v0

    if-eqz v0, :cond_36

    add-int/lit8 v0, p1, -0x1

    goto :goto_37

    :cond_36
    move v0, p1

    :goto_37
    move p1, v0

    .line 844
    if-eqz p4, :cond_41

    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner$HintAdapter;->mSpinnerAdapter:Landroid/widget/SpinnerAdapter;

    invoke-interface {v0, p1, p2, p3}, Landroid/widget/SpinnerAdapter;->getDropDownView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_47

    :cond_41
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner$HintAdapter;->mSpinnerAdapter:Landroid/widget/SpinnerAdapter;

    invoke-interface {v0, p1, p2, p3}, Landroid/widget/SpinnerAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    :goto_47
    return-object v0
.end method

.method private getHintView(Landroid/view/ViewGroup;Z)Landroid/view/View;
    .registers 10
    .param p1, "parent"    # Landroid/view/ViewGroup;
    .param p2, "isDropDownView"    # Z

    .line 852
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner$HintAdapter;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v5

    .line 853
    .local v5, "inflater":Landroid/view/LayoutInflater;
    if-eqz p2, :cond_b

    sget v6, Lbr/com/itau/widgets/material/R$layout;->spinner_dropdown:I

    goto :goto_d

    :cond_b
    sget v6, Lbr/com/itau/widgets/material/R$layout;->spinner_item:I

    .line 854
    .local v6, "resid":I
    :goto_d
    const/4 v0, 0x0

    invoke-virtual {v5, v6, p1, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Landroid/widget/TextView;

    .line 856
    .local v4, "textView":Landroid/widget/TextView;
    if-nez p2, :cond_1e

    .line 857
    const/4 v0, 0x0

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v4, v0, v1, v2, v3}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 859
    :cond_1e
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner$HintAdapter;->this$0:Lbr/com/itau/widgets/material/MaterialSpinner;

    # getter for: Lbr/com/itau/widgets/material/MaterialSpinner;->hintTextSize:F
    invoke-static {v0}, Lbr/com/itau/widgets/material/MaterialSpinner;->access$1000(Lbr/com/itau/widgets/material/MaterialSpinner;)F

    move-result v0

    const/4 v1, 0x0

    invoke-virtual {v4, v1, v0}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 860
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner$HintAdapter;->this$0:Lbr/com/itau/widgets/material/MaterialSpinner;

    # getter for: Lbr/com/itau/widgets/material/MaterialSpinner;->hint:Ljava/lang/CharSequence;
    invoke-static {v0}, Lbr/com/itau/widgets/material/MaterialSpinner;->access$000(Lbr/com/itau/widgets/material/MaterialSpinner;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 861
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner$HintAdapter;->this$0:Lbr/com/itau/widgets/material/MaterialSpinner;

    invoke-virtual {v0}, Lbr/com/itau/widgets/material/MaterialSpinner;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_40

    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner$HintAdapter;->this$0:Lbr/com/itau/widgets/material/MaterialSpinner;

    # getter for: Lbr/com/itau/widgets/material/MaterialSpinner;->hintTextColor:I
    invoke-static {v0}, Lbr/com/itau/widgets/material/MaterialSpinner;->access$1100(Lbr/com/itau/widgets/material/MaterialSpinner;)I

    move-result v0

    goto :goto_46

    :cond_40
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner$HintAdapter;->this$0:Lbr/com/itau/widgets/material/MaterialSpinner;

    # getter for: Lbr/com/itau/widgets/material/MaterialSpinner;->disabledColor:I
    invoke-static {v0}, Lbr/com/itau/widgets/material/MaterialSpinner;->access$1200(Lbr/com/itau/widgets/material/MaterialSpinner;)I

    move-result v0

    :goto_46
    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 862
    const/4 v0, -0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setTag(Ljava/lang/Object;)V

    .line 864
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner$HintAdapter;->this$0:Lbr/com/itau/widgets/material/MaterialSpinner;

    # getter for: Lbr/com/itau/widgets/material/MaterialSpinner;->typeface:Landroid/graphics/Typeface;
    invoke-static {v0}, Lbr/com/itau/widgets/material/MaterialSpinner;->access$1300(Lbr/com/itau/widgets/material/MaterialSpinner;)Landroid/graphics/Typeface;

    move-result-object v0

    if-eqz v0, :cond_62

    .line 865
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner$HintAdapter;->this$0:Lbr/com/itau/widgets/material/MaterialSpinner;

    # getter for: Lbr/com/itau/widgets/material/MaterialSpinner;->typeface:Landroid/graphics/Typeface;
    invoke-static {v0}, Lbr/com/itau/widgets/material/MaterialSpinner;->access$1300(Lbr/com/itau/widgets/material/MaterialSpinner;)Landroid/graphics/Typeface;

    move-result-object v0

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 868
    :cond_62
    return-object v4
.end method


# virtual methods
.method public getCount()I
    .registers 3

    .line 807
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner$HintAdapter;->mSpinnerAdapter:Landroid/widget/SpinnerAdapter;

    invoke-interface {v0}, Landroid/widget/SpinnerAdapter;->getCount()I

    move-result v1

    .line 808
    .local v1, "count":I
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner$HintAdapter;->this$0:Lbr/com/itau/widgets/material/MaterialSpinner;

    # getter for: Lbr/com/itau/widgets/material/MaterialSpinner;->hint:Ljava/lang/CharSequence;
    invoke-static {v0}, Lbr/com/itau/widgets/material/MaterialSpinner;->access$000(Lbr/com/itau/widgets/material/MaterialSpinner;)Ljava/lang/CharSequence;

    move-result-object v0

    if-eqz v0, :cond_11

    add-int/lit8 v0, v1, 0x1

    goto :goto_12

    :cond_11
    move v0, v1

    :goto_12
    return v0
.end method

.method public getDropDownView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 5
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .line 830
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, p3, v0}, Lbr/com/itau/widgets/material/MaterialSpinner$HintAdapter;->buildView(ILandroid/view/View;Landroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .registers 3
    .param p1, "position"    # I

    .line 813
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner$HintAdapter;->this$0:Lbr/com/itau/widgets/material/MaterialSpinner;

    # getter for: Lbr/com/itau/widgets/material/MaterialSpinner;->hint:Ljava/lang/CharSequence;
    invoke-static {v0}, Lbr/com/itau/widgets/material/MaterialSpinner;->access$000(Lbr/com/itau/widgets/material/MaterialSpinner;)Ljava/lang/CharSequence;

    move-result-object v0

    if-eqz v0, :cond_b

    add-int/lit8 v0, p1, -0x1

    goto :goto_c

    :cond_b
    move v0, p1

    :goto_c
    move p1, v0

    .line 814
    const/4 v0, -0x1

    if-ne p1, v0, :cond_17

    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner$HintAdapter;->this$0:Lbr/com/itau/widgets/material/MaterialSpinner;

    # getter for: Lbr/com/itau/widgets/material/MaterialSpinner;->hint:Ljava/lang/CharSequence;
    invoke-static {v0}, Lbr/com/itau/widgets/material/MaterialSpinner;->access$000(Lbr/com/itau/widgets/material/MaterialSpinner;)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_1d

    :cond_17
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner$HintAdapter;->mSpinnerAdapter:Landroid/widget/SpinnerAdapter;

    invoke-interface {v0, p1}, Landroid/widget/SpinnerAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    :goto_1d
    return-object v0
.end method

.method public getItemId(I)J
    .registers 4
    .param p1, "position"    # I

    .line 819
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner$HintAdapter;->this$0:Lbr/com/itau/widgets/material/MaterialSpinner;

    # getter for: Lbr/com/itau/widgets/material/MaterialSpinner;->hint:Ljava/lang/CharSequence;
    invoke-static {v0}, Lbr/com/itau/widgets/material/MaterialSpinner;->access$000(Lbr/com/itau/widgets/material/MaterialSpinner;)Ljava/lang/CharSequence;

    move-result-object v0

    if-eqz v0, :cond_b

    add-int/lit8 v0, p1, -0x1

    goto :goto_c

    :cond_b
    move v0, p1

    :goto_c
    move p1, v0

    .line 820
    const/4 v0, -0x1

    if-ne p1, v0, :cond_13

    const-wide/16 v0, 0x0

    goto :goto_19

    :cond_13
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner$HintAdapter;->mSpinnerAdapter:Landroid/widget/SpinnerAdapter;

    invoke-interface {v0, p1}, Landroid/widget/SpinnerAdapter;->getItemId(I)J

    move-result-wide v0

    :goto_19
    return-wide v0
.end method

.method public getItemViewType(I)I
    .registers 3
    .param p1, "position"    # I

    .line 801
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner$HintAdapter;->this$0:Lbr/com/itau/widgets/material/MaterialSpinner;

    # getter for: Lbr/com/itau/widgets/material/MaterialSpinner;->hint:Ljava/lang/CharSequence;
    invoke-static {v0}, Lbr/com/itau/widgets/material/MaterialSpinner;->access$000(Lbr/com/itau/widgets/material/MaterialSpinner;)Ljava/lang/CharSequence;

    move-result-object v0

    if-eqz v0, :cond_b

    add-int/lit8 v0, p1, -0x1

    goto :goto_c

    :cond_b
    move v0, p1

    :goto_c
    move p1, v0

    .line 802
    const/4 v0, -0x1

    if-ne p1, v0, :cond_12

    const/4 v0, -0x1

    goto :goto_18

    :cond_12
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner$HintAdapter;->mSpinnerAdapter:Landroid/widget/SpinnerAdapter;

    invoke-interface {v0, p1}, Landroid/widget/SpinnerAdapter;->getItemViewType(I)I

    move-result v0

    :goto_18
    return v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 5
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .line 825
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lbr/com/itau/widgets/material/MaterialSpinner$HintAdapter;->buildView(ILandroid/view/View;Landroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public getViewTypeCount()I
    .registers 4

    .line 792
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_8

    .line 793
    const/4 v0, 0x1

    return v0

    .line 795
    :cond_8
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner$HintAdapter;->mSpinnerAdapter:Landroid/widget/SpinnerAdapter;

    invoke-interface {v0}, Landroid/widget/SpinnerAdapter;->getViewTypeCount()I

    move-result v2

    .line 796
    .local v2, "viewTypeCount":I
    return v2
.end method

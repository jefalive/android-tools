.class public Lbr/com/itau/widgets/graphicalfriedcake/model/ItemVO;
.super Ljava/lang/Object;
.source "ItemVO.java"


# instance fields
.field private color:Ljava/lang/String;

.field private count:I

.field private name:Ljava/lang/String;

.field private percentual:F

.field private value:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IF)V
    .registers 6
    .param p1, "color"    # Ljava/lang/String;
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "value"    # Ljava/lang/String;
    .param p4, "count"    # I
    .param p5, "percentual"    # F

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    iput-object p1, p0, Lbr/com/itau/widgets/graphicalfriedcake/model/ItemVO;->color:Ljava/lang/String;

    .line 14
    iput-object p2, p0, Lbr/com/itau/widgets/graphicalfriedcake/model/ItemVO;->name:Ljava/lang/String;

    .line 15
    iput-object p3, p0, Lbr/com/itau/widgets/graphicalfriedcake/model/ItemVO;->value:Ljava/lang/String;

    .line 16
    invoke-virtual {p0, p4}, Lbr/com/itau/widgets/graphicalfriedcake/model/ItemVO;->setCount(I)V

    .line 17
    iput p5, p0, Lbr/com/itau/widgets/graphicalfriedcake/model/ItemVO;->percentual:F

    .line 18
    return-void
.end method


# virtual methods
.method public getColor()Ljava/lang/String;
    .registers 2

    .line 45
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/model/ItemVO;->color:Ljava/lang/String;

    return-object v0
.end method

.method public getCount()I
    .registers 2

    .line 53
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/model/ItemVO;->count:I

    return v0
.end method

.method public getName()Ljava/lang/String;
    .registers 2

    .line 21
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/model/ItemVO;->name:Ljava/lang/String;

    return-object v0
.end method

.method public getPercentual()F
    .registers 2

    .line 37
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/model/ItemVO;->percentual:F

    return v0
.end method

.method public getValue()Ljava/lang/String;
    .registers 2

    .line 25
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/model/ItemVO;->value:Ljava/lang/String;

    return-object v0
.end method

.method public setColor(Ljava/lang/String;)V
    .registers 2
    .param p1, "color"    # Ljava/lang/String;

    .line 49
    iput-object p1, p0, Lbr/com/itau/widgets/graphicalfriedcake/model/ItemVO;->color:Ljava/lang/String;

    .line 50
    return-void
.end method

.method public setCount(I)V
    .registers 2
    .param p1, "count"    # I

    .line 57
    iput p1, p0, Lbr/com/itau/widgets/graphicalfriedcake/model/ItemVO;->count:I

    .line 58
    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .registers 2
    .param p1, "name"    # Ljava/lang/String;

    .line 29
    iput-object p1, p0, Lbr/com/itau/widgets/graphicalfriedcake/model/ItemVO;->name:Ljava/lang/String;

    .line 30
    return-void
.end method

.method public setPercentual(F)V
    .registers 2
    .param p1, "percentual"    # F

    .line 41
    iput p1, p0, Lbr/com/itau/widgets/graphicalfriedcake/model/ItemVO;->percentual:F

    .line 42
    return-void
.end method

.method public setValue(Ljava/lang/String;)V
    .registers 2
    .param p1, "value"    # Ljava/lang/String;

    .line 33
    iput-object p1, p0, Lbr/com/itau/widgets/graphicalfriedcake/model/ItemVO;->value:Ljava/lang/String;

    .line 34
    return-void
.end method

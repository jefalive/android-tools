.class synthetic Lbr/com/itau/sdk/android/core/endpoint/g;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1008
    name = null
.end annotation


# static fields
.field static final synthetic a:[I


# direct methods
.method static constructor <clinit>()V
    .registers 4

    .line 13
    invoke-static {}, Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;->values()[Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lbr/com/itau/sdk/android/core/endpoint/g;->a:[I

    :try_start_9
    sget-object v0, Lbr/com/itau/sdk/android/core/endpoint/g;->a:[I

    sget-object v1, Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;->NETWORK:Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;

    invoke-virtual {v1}, Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_14
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9 .. :try_end_14} :catch_15

    goto :goto_16

    :catch_15
    move-exception v3

    :goto_16
    :try_start_16
    sget-object v0, Lbr/com/itau/sdk/android/core/endpoint/g;->a:[I

    sget-object v1, Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;->CONVERSION:Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;

    invoke-virtual {v1}, Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_21
    .catch Ljava/lang/NoSuchFieldError; {:try_start_16 .. :try_end_21} :catch_22

    goto :goto_23

    :catch_22
    move-exception v3

    :goto_23
    :try_start_23
    sget-object v0, Lbr/com/itau/sdk/android/core/endpoint/g;->a:[I

    sget-object v1, Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;->HTTP:Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;

    invoke-virtual {v1}, Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2e
    .catch Ljava/lang/NoSuchFieldError; {:try_start_23 .. :try_end_2e} :catch_2f

    goto :goto_30

    :catch_2f
    move-exception v3

    :goto_30
    :try_start_30
    sget-object v0, Lbr/com/itau/sdk/android/core/endpoint/g;->a:[I

    sget-object v1, Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;->UNEXPECTED:Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;

    invoke-virtual {v1}, Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_3b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_30 .. :try_end_3b} :catch_3c

    goto :goto_3d

    :catch_3c
    move-exception v3

    :goto_3d
    :try_start_3d
    sget-object v0, Lbr/com/itau/sdk/android/core/endpoint/g;->a:[I

    sget-object v1, Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;->ROUTER:Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;

    invoke-virtual {v1}, Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_48
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3d .. :try_end_48} :catch_49

    goto :goto_4a

    :catch_49
    move-exception v3

    :goto_4a
    :try_start_4a
    sget-object v0, Lbr/com/itau/sdk/android/core/endpoint/g;->a:[I

    sget-object v1, Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;->TOKEN:Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;

    invoke-virtual {v1}, Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_55
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4a .. :try_end_55} :catch_56

    goto :goto_57

    :catch_56
    move-exception v3

    :goto_57
    :try_start_57
    sget-object v0, Lbr/com/itau/sdk/android/core/endpoint/g;->a:[I

    sget-object v1, Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;->SESSION:Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;

    invoke-virtual {v1}, Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_62
    .catch Ljava/lang/NoSuchFieldError; {:try_start_57 .. :try_end_62} :catch_63

    goto :goto_64

    :catch_63
    move-exception v3

    :goto_64
    :try_start_64
    sget-object v0, Lbr/com/itau/sdk/android/core/endpoint/g;->a:[I

    sget-object v1, Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;->SIMULTANEOUS_SESSION:Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;

    invoke-virtual {v1}, Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;->ordinal()I

    move-result v1

    const/16 v2, 0x8

    aput v2, v0, v1
    :try_end_70
    .catch Ljava/lang/NoSuchFieldError; {:try_start_64 .. :try_end_70} :catch_71

    goto :goto_72

    :catch_71
    move-exception v3

    :goto_72
    :try_start_72
    sget-object v0, Lbr/com/itau/sdk/android/core/endpoint/g;->a:[I

    sget-object v1, Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;->VERSION_CONTROL:Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;

    invoke-virtual {v1}, Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;->ordinal()I

    move-result v1

    const/16 v2, 0x9

    aput v2, v0, v1
    :try_end_7e
    .catch Ljava/lang/NoSuchFieldError; {:try_start_72 .. :try_end_7e} :catch_7f

    goto :goto_80

    :catch_7f
    move-exception v3

    :goto_80
    return-void
.end method

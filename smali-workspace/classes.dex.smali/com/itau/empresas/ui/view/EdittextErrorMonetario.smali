.class public Lcom/itau/empresas/ui/view/EdittextErrorMonetario;
.super Lcom/itau/empresas/ui/view/EditTextMonetario;
.source "EdittextErrorMonetario.java"


# instance fields
.field private hint:Lbr/com/itau/widgets/hintview/Hint$Builder;

.field private hintView:Lbr/com/itau/widgets/hintview/Hint;

.field private isErro:Z

.field private mensagemErro:Ljava/lang/String;

.field private mostraHint:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 2
    .param p1, "context"    # Landroid/content/Context;

    .line 33
    invoke-direct {p0, p1}, Lcom/itau/empresas/ui/view/EditTextMonetario;-><init>(Landroid/content/Context;)V

    .line 34
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .line 37
    invoke-direct {p0, p1, p2}, Lcom/itau/empresas/ui/view/EditTextMonetario;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 38
    .line 39
    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    sget-object v1, Lcom/itau/empresas/R$styleable;->EditTextCustomError:[I

    .line 40
    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, p2, v1, v2, v3}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v4

    .line 41
    .local v4, "typedArray":Landroid/content/res/TypedArray;
    const/4 v0, 0x1

    invoke-virtual {v4, v0}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/ui/view/EdittextErrorMonetario;->mensagemErro:Ljava/lang/String;

    .line 42
    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-virtual {v4, v0, v1}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    iput-boolean v0, p0, Lcom/itau/empresas/ui/view/EdittextErrorMonetario;->mostraHint:Z

    .line 44
    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/EdittextErrorMonetario;->getRootView()Landroid/view/View;

    move-result-object v5

    .line 45
    .local v5, "activityRootView":Landroid/view/View;
    invoke-virtual {v5}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    new-instance v1, Lcom/itau/empresas/ui/view/EdittextErrorMonetario$1;

    invoke-direct {v1, p0, v5}, Lcom/itau/empresas/ui/view/EdittextErrorMonetario$1;-><init>(Lcom/itau/empresas/ui/view/EdittextErrorMonetario;Landroid/view/View;)V

    .line 46
    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 57
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .line 60
    invoke-direct {p0, p1, p2, p3}, Lcom/itau/empresas/ui/view/EditTextMonetario;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 61
    return-void
.end method

.method static synthetic access$000(Lcom/itau/empresas/ui/view/EdittextErrorMonetario;)V
    .registers 1
    .param p0, "x0"    # Lcom/itau/empresas/ui/view/EdittextErrorMonetario;

    .line 23
    invoke-direct {p0}, Lcom/itau/empresas/ui/view/EdittextErrorMonetario;->removePopPupErro()V

    return-void
.end method

.method public static dpToPx(Landroid/content/Context;F)F
    .registers 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "valueInDp"    # F

    .line 68
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    .line 69
    .local v1, "metrics":Landroid/util/DisplayMetrics;
    const/4 v0, 0x1

    invoke-static {v0, p1, v1}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    return v0
.end method

.method private mostraCampoRequerido()V
    .registers 3

    .line 99
    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/EdittextErrorMonetario;->getParent()Landroid/view/ViewParent;

    .line 100
    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/EdittextErrorMonetario;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {p0, v0}, Lcom/itau/empresas/ui/view/EdittextErrorMonetario;->setaBackgroundErro(Landroid/view/ViewGroup;)V

    .line 101
    new-instance v0, Lbr/com/itau/widgets/hintview/Hint$Builder;

    const v1, 0x7f09011d

    invoke-direct {v0, p0, v1}, Lbr/com/itau/widgets/hintview/Hint$Builder;-><init>(Landroid/view/View;I)V

    iput-object v0, p0, Lcom/itau/empresas/ui/view/EdittextErrorMonetario;->hint:Lbr/com/itau/widgets/hintview/Hint$Builder;

    .line 102
    iget-object v0, p0, Lcom/itau/empresas/ui/view/EdittextErrorMonetario;->hint:Lbr/com/itau/widgets/hintview/Hint$Builder;

    iget-object v1, p0, Lcom/itau/empresas/ui/view/EdittextErrorMonetario;->mensagemErro:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/itau/empresas/ui/util/HintUtils;->montaHintErro(Lbr/com/itau/widgets/hintview/Hint$Builder;Ljava/lang/String;)Lbr/com/itau/widgets/hintview/Hint$Builder;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/ui/view/EdittextErrorMonetario;->hint:Lbr/com/itau/widgets/hintview/Hint$Builder;

    .line 103
    iget-object v0, p0, Lcom/itau/empresas/ui/view/EdittextErrorMonetario;->hint:Lbr/com/itau/widgets/hintview/Hint$Builder;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lbr/com/itau/widgets/hintview/Hint$Builder;->setCancelable(Z)Lbr/com/itau/widgets/hintview/Hint$Builder;

    .line 104
    iget-object v0, p0, Lcom/itau/empresas/ui/view/EdittextErrorMonetario;->hint:Lbr/com/itau/widgets/hintview/Hint$Builder;

    new-instance v1, Lcom/itau/empresas/ui/view/EdittextErrorMonetario$2;

    invoke-direct {v1, p0}, Lcom/itau/empresas/ui/view/EdittextErrorMonetario$2;-><init>(Lcom/itau/empresas/ui/view/EdittextErrorMonetario;)V

    invoke-virtual {v0, v1}, Lbr/com/itau/widgets/hintview/Hint$Builder;->setOnClickListener(Lbr/com/itau/widgets/hintview/OnClickListener;)Lbr/com/itau/widgets/hintview/Hint$Builder;

    .line 110
    iget-object v0, p0, Lcom/itau/empresas/ui/view/EdittextErrorMonetario;->hint:Lbr/com/itau/widgets/hintview/Hint$Builder;

    invoke-virtual {v0}, Lbr/com/itau/widgets/hintview/Hint$Builder;->show()Lbr/com/itau/widgets/hintview/Hint;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/ui/view/EdittextErrorMonetario;->hintView:Lbr/com/itau/widgets/hintview/Hint;

    .line 112
    return-void
.end method

.method private removePopPupErro()V
    .registers 2

    .line 147
    iget-object v0, p0, Lcom/itau/empresas/ui/view/EdittextErrorMonetario;->hintView:Lbr/com/itau/widgets/hintview/Hint;

    if-eqz v0, :cond_9

    .line 148
    iget-object v0, p0, Lcom/itau/empresas/ui/view/EdittextErrorMonetario;->hintView:Lbr/com/itau/widgets/hintview/Hint;

    invoke-virtual {v0}, Lbr/com/itau/widgets/hintview/Hint;->dismiss()V

    .line 150
    :cond_9
    return-void
.end method

.method private setaBackgroundDefault(Landroid/view/ViewGroup;)V
    .registers 4
    .param p1, "parent"    # Landroid/view/ViewGroup;

    .line 136
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-ge v0, v1, :cond_15

    .line 137
    .line 138
    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/EdittextErrorMonetario;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f020064

    invoke-static {v0, v1}, Landroid/support/v4/content/ContextCompat;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 137
    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_23

    .line 140
    .line 141
    :cond_15
    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/EdittextErrorMonetario;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f020064

    invoke-static {v0, v1}, Landroid/support/v4/content/ContextCompat;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 140
    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 143
    :goto_23
    return-void
.end method


# virtual methods
.method protected onConfigurationChanged(Landroid/content/res/Configuration;)V
    .registers 4
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .line 116
    invoke-super {p0, p1}, Lcom/itau/empresas/ui/view/EditTextMonetario;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 117
    iget v0, p1, Landroid/content/res/Configuration;->hardKeyboardHidden:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_c

    .line 118
    invoke-direct {p0}, Lcom/itau/empresas/ui/view/EdittextErrorMonetario;->removePopPupErro()V

    goto :goto_14

    .line 119
    :cond_c
    iget v0, p1, Landroid/content/res/Configuration;->hardKeyboardHidden:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_14

    .line 120
    invoke-direct {p0}, Lcom/itau/empresas/ui/view/EdittextErrorMonetario;->removePopPupErro()V

    .line 122
    :cond_14
    :goto_14
    return-void
.end method

.method protected onFocusChanged(ZILandroid/graphics/Rect;)V
    .registers 5
    .param p1, "focused"    # Z
    .param p2, "direction"    # I
    .param p3, "previouslyFocusedRect"    # Landroid/graphics/Rect;

    .line 78
    invoke-super {p0, p1, p2, p3}, Lcom/itau/empresas/ui/view/EditTextMonetario;->onFocusChanged(ZILandroid/graphics/Rect;)V

    .line 80
    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/EdittextErrorMonetario;->hasFocus()Z

    move-result v0

    if-eqz v0, :cond_1f

    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/EdittextErrorMonetario;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1f

    iget-boolean v0, p0, Lcom/itau/empresas/ui/view/EdittextErrorMonetario;->isErro:Z

    if-eqz v0, :cond_1f

    .line 81
    invoke-direct {p0}, Lcom/itau/empresas/ui/view/EdittextErrorMonetario;->mostraCampoRequerido()V

    goto :goto_4c

    .line 82
    :cond_1f
    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/EdittextErrorMonetario;->hasFocus()Z

    move-result v0

    if-nez v0, :cond_40

    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/EdittextErrorMonetario;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_40

    .line 83
    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/EdittextErrorMonetario;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {p0, v0}, Lcom/itau/empresas/ui/view/EdittextErrorMonetario;->setaBackgroundErro(Landroid/view/ViewGroup;)V

    .line 84
    invoke-direct {p0}, Lcom/itau/empresas/ui/view/EdittextErrorMonetario;->removePopPupErro()V

    goto :goto_4c

    .line 86
    :cond_40
    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/EdittextErrorMonetario;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-direct {p0, v0}, Lcom/itau/empresas/ui/view/EdittextErrorMonetario;->setaBackgroundDefault(Landroid/view/ViewGroup;)V

    .line 87
    invoke-direct {p0}, Lcom/itau/empresas/ui/view/EdittextErrorMonetario;->removePopPupErro()V

    .line 89
    :goto_4c
    return-void
.end method

.method protected onTextChanged(Ljava/lang/CharSequence;III)V
    .registers 5
    .param p1, "text"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "lengthBefore"    # I
    .param p4, "lengthAfter"    # I

    .line 93
    invoke-super {p0, p1, p2, p3, p4}, Lcom/itau/empresas/ui/view/EditTextMonetario;->onTextChanged(Ljava/lang/CharSequence;III)V

    .line 94
    invoke-direct {p0}, Lcom/itau/empresas/ui/view/EdittextErrorMonetario;->removePopPupErro()V

    .line 95
    return-void
.end method

.method public setErro(Z)V
    .registers 2
    .param p1, "erro"    # Z

    .line 64
    iput-boolean p1, p0, Lcom/itau/empresas/ui/view/EdittextErrorMonetario;->isErro:Z

    .line 65
    return-void
.end method

.method public setMostraHint(Z)V
    .registers 2
    .param p1, "mostraHint"    # Z

    .line 73
    iput-boolean p1, p0, Lcom/itau/empresas/ui/view/EdittextErrorMonetario;->mostraHint:Z

    .line 74
    return-void
.end method

.method public setaBackgroundErro(Landroid/view/ViewGroup;)V
    .registers 4
    .param p1, "viewGroup"    # Landroid/view/ViewGroup;

    .line 126
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-ge v0, v1, :cond_15

    .line 127
    .line 128
    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/EdittextErrorMonetario;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0200d5

    invoke-static {v0, v1}, Landroid/support/v4/content/ContextCompat;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 127
    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_23

    .line 130
    .line 131
    :cond_15
    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/EdittextErrorMonetario;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0200d5

    invoke-static {v0, v1}, Landroid/support/v4/content/ContextCompat;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 130
    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 133
    :goto_23
    return-void
.end method

.class public Lcom/itau/empresas/feature/itoken/ITokenNaoInstaladoDialogFragment;
.super Landroid/support/v4/app/DialogFragment;
.source "ITokenNaoInstaladoDialogFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/itau/empresas/feature/itoken/ITokenNaoInstaladoDialogFragment$Listener;
    }
.end annotation


# instance fields
.field private botaoContinuar:Landroid/widget/Button;

.field private botaoFechar:Lcom/itau/empresas/ui/view/TextViewIcon;

.field private listener:Lcom/itau/empresas/feature/itoken/ITokenNaoInstaladoDialogFragment$Listener;

.field private texto:Landroid/widget/TextView;

.field private titulo:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 18
    invoke-direct {p0}, Landroid/support/v4/app/DialogFragment;-><init>()V

    return-void
.end method

.method private aoClicarNoBotaoContinuar()V
    .registers 2

    .line 70
    iget-object v0, p0, Lcom/itau/empresas/feature/itoken/ITokenNaoInstaladoDialogFragment;->listener:Lcom/itau/empresas/feature/itoken/ITokenNaoInstaladoDialogFragment$Listener;

    if-eqz v0, :cond_9

    .line 71
    iget-object v0, p0, Lcom/itau/empresas/feature/itoken/ITokenNaoInstaladoDialogFragment;->listener:Lcom/itau/empresas/feature/itoken/ITokenNaoInstaladoDialogFragment$Listener;

    invoke-interface {v0}, Lcom/itau/empresas/feature/itoken/ITokenNaoInstaladoDialogFragment$Listener;->aoClicarEmContinuar()V

    .line 72
    :cond_9
    invoke-direct {p0}, Lcom/itau/empresas/feature/itoken/ITokenNaoInstaladoDialogFragment;->disparaEventoAnalytics()V

    .line 73
    invoke-virtual {p0}, Lcom/itau/empresas/feature/itoken/ITokenNaoInstaladoDialogFragment;->dismiss()V

    .line 74
    return-void
.end method

.method private aoClicarNoBotaoFechar()V
    .registers 1

    .line 66
    invoke-virtual {p0}, Lcom/itau/empresas/feature/itoken/ITokenNaoInstaladoDialogFragment;->dismiss()V

    .line 67
    return-void
.end method

.method private disparaEventoAnalytics()V
    .registers 5

    .line 77
    invoke-static {}, Lcom/itau/empresas/feature/analytics/AnalyticsHelper;->getInstance()Lcom/itau/empresas/feature/analytics/AnalyticsHelper;

    move-result-object v3

    .line 78
    .line 79
    .local v3, "analyticsHelper":Lcom/itau/empresas/feature/analytics/AnalyticsHelper;
    const v0, 0x7f0700e7

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/itoken/ITokenNaoInstaladoDialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 80
    const v1, 0x7f0700be

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/itoken/ITokenNaoInstaladoDialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "Instalar iToken"

    .line 78
    invoke-virtual {v3, v0, v1, v2}, Lcom/itau/empresas/feature/analytics/AnalyticsHelper;->eventoAdobeState(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 82
    .line 83
    const v0, 0x7f0700e7

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/itoken/ITokenNaoInstaladoDialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 84
    const v1, 0x7f0700ba

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/itoken/ITokenNaoInstaladoDialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "1"

    .line 82
    invoke-virtual {v3, v0, v1, v2}, Lcom/itau/empresas/feature/analytics/AnalyticsHelper;->eventoAdobeState(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 86
    return-void
.end method

.method private static findView(Landroid/view/View;I)Landroid/view/View;
    .registers 3
    .param p0, "root"    # Landroid/view/View;
    .param p1, "viewId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:Landroid/view/View;>(Landroid/view/View;I)TT;"
        }
    .end annotation

    .line 55
    invoke-virtual {p0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public static newInstace(Landroid/content/Context;)Lcom/itau/empresas/feature/itoken/ITokenNaoInstaladoDialogFragment;
    .registers 2
    .param p0, "context"    # Landroid/content/Context;

    .line 28
    new-instance v0, Lcom/itau/empresas/feature/itoken/ITokenNaoInstaladoDialogFragment;

    invoke-direct {v0}, Lcom/itau/empresas/feature/itoken/ITokenNaoInstaladoDialogFragment;-><init>()V

    return-object v0
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .registers 3
    .param p1, "v"    # Landroid/view/View;

    .line 59
    iget-object v0, p0, Lcom/itau/empresas/feature/itoken/ITokenNaoInstaladoDialogFragment;->botaoFechar:Lcom/itau/empresas/ui/view/TextViewIcon;

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 60
    invoke-direct {p0}, Lcom/itau/empresas/feature/itoken/ITokenNaoInstaladoDialogFragment;->aoClicarNoBotaoFechar()V

    goto :goto_f

    .line 62
    :cond_c
    invoke-direct {p0}, Lcom/itau/empresas/feature/itoken/ITokenNaoInstaladoDialogFragment;->aoClicarNoBotaoContinuar()V

    .line 63
    :goto_f
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 32
    invoke-super {p0, p1}, Landroid/support/v4/app/DialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 33
    const/4 v0, 0x2

    const v1, 0x7f090122

    invoke-virtual {p0, v0, v1}, Lcom/itau/empresas/feature/itoken/ITokenNaoInstaladoDialogFragment;->setStyle(II)V

    .line 34
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .registers 6
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .line 39
    const v0, 0x7f03009c

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .registers 4
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;

    .line 43
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/DialogFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 44
    const v0, 0x7f0e0404

    invoke-static {p1, v0}, Lcom/itau/empresas/feature/itoken/ITokenNaoInstaladoDialogFragment;->findView(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/itoken/ITokenNaoInstaladoDialogFragment;->titulo:Landroid/widget/TextView;

    .line 45
    const v0, 0x7f0e0405

    invoke-static {p1, v0}, Lcom/itau/empresas/feature/itoken/ITokenNaoInstaladoDialogFragment;->findView(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/itoken/ITokenNaoInstaladoDialogFragment;->texto:Landroid/widget/TextView;

    .line 46
    const v0, 0x7f0e021e

    invoke-static {p1, v0}, Lcom/itau/empresas/feature/itoken/ITokenNaoInstaladoDialogFragment;->findView(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/ui/view/TextViewIcon;

    iput-object v0, p0, Lcom/itau/empresas/feature/itoken/ITokenNaoInstaladoDialogFragment;->botaoFechar:Lcom/itau/empresas/ui/view/TextViewIcon;

    .line 47
    const v0, 0x7f0e018f

    invoke-static {p1, v0}, Lcom/itau/empresas/feature/itoken/ITokenNaoInstaladoDialogFragment;->findView(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/itau/empresas/feature/itoken/ITokenNaoInstaladoDialogFragment;->botaoContinuar:Landroid/widget/Button;

    .line 49
    iget-object v0, p0, Lcom/itau/empresas/feature/itoken/ITokenNaoInstaladoDialogFragment;->botaoFechar:Lcom/itau/empresas/ui/view/TextViewIcon;

    invoke-virtual {v0, p0}, Lcom/itau/empresas/ui/view/TextViewIcon;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 50
    iget-object v0, p0, Lcom/itau/empresas/feature/itoken/ITokenNaoInstaladoDialogFragment;->botaoContinuar:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 51
    return-void
.end method

.method public setListener(Lcom/itau/empresas/feature/itoken/ITokenNaoInstaladoDialogFragment$Listener;)V
    .registers 2
    .param p1, "listener"    # Lcom/itau/empresas/feature/itoken/ITokenNaoInstaladoDialogFragment$Listener;

    .line 89
    iput-object p1, p0, Lcom/itau/empresas/feature/itoken/ITokenNaoInstaladoDialogFragment;->listener:Lcom/itau/empresas/feature/itoken/ITokenNaoInstaladoDialogFragment$Listener;

    .line 90
    return-void
.end method

.class public Lcom/itau/empresas/ui/view/TopoItauView;
.super Landroid/widget/LinearLayout;
.source "TopoItauView.java"


# instance fields
.field private attrLogoCustom:Ljava/lang/String;

.field logoCustom:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 2
    .param p1, "context"    # Landroid/content/Context;

    .line 25
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 26
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .line 29
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 30
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .line 33
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 34
    invoke-direct {p0, p1, p2}, Lcom/itau/empresas/ui/view/TopoItauView;->init(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 35
    return-void
.end method

.method private init(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .line 38
    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    sget-object v1, Lcom/itau/empresas/R$styleable;->LogoItau:[I

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, p2, v1, v2, v3}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v4

    .line 41
    .local v4, "typedArray":Landroid/content/res/TypedArray;
    const/4 v0, 0x0

    invoke-virtual {v4, v0}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/ui/view/TopoItauView;->attrLogoCustom:Ljava/lang/String;

    .line 43
    return-void
.end method


# virtual methods
.method afterViews()V
    .registers 3

    .line 47
    iget-object v0, p0, Lcom/itau/empresas/ui/view/TopoItauView;->attrLogoCustom:Ljava/lang/String;

    if-eqz v0, :cond_19

    iget-object v0, p0, Lcom/itau/empresas/ui/view/TopoItauView;->attrLogoCustom:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_19

    .line 48
    iget-object v0, p0, Lcom/itau/empresas/ui/view/TopoItauView;->logoCustom:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 49
    iget-object v0, p0, Lcom/itau/empresas/ui/view/TopoItauView;->logoCustom:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/itau/empresas/ui/view/TopoItauView;->attrLogoCustom:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 51
    :cond_19
    return-void
.end method

.method public setLogoCustomContentDescription(Ljava/lang/String;)V
    .registers 3
    .param p1, "contentDescription"    # Ljava/lang/String;

    .line 59
    iget-object v0, p0, Lcom/itau/empresas/ui/view/TopoItauView;->logoCustom:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 60
    return-void
.end method

.method public setLogoCustomText(Ljava/lang/String;)V
    .registers 4
    .param p1, "text"    # Ljava/lang/String;

    .line 54
    iget-object v0, p0, Lcom/itau/empresas/ui/view/TopoItauView;->logoCustom:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 55
    iget-object v0, p0, Lcom/itau/empresas/ui/view/TopoItauView;->logoCustom:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 56
    return-void
.end method

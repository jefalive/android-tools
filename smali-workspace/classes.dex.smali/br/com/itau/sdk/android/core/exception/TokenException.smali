.class public Lbr/com/itau/sdk/android/core/exception/TokenException;
.super Ljava/lang/RuntimeException;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lbr/com/itau/sdk/android/core/exception/TokenException$Type;
    }
.end annotation


# instance fields
.field private final type:Lbr/com/itau/sdk/android/core/exception/TokenException$Type;


# direct methods
.method public constructor <init>(Lbr/com/itau/sdk/android/core/exception/TokenException$Type;Ljava/lang/String;Ljava/lang/Exception;)V
    .registers 4

    .line 32
    invoke-direct {p0, p2, p3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 33
    iput-object p1, p0, Lbr/com/itau/sdk/android/core/exception/TokenException;->type:Lbr/com/itau/sdk/android/core/exception/TokenException$Type;

    .line 34
    return-void
.end method


# virtual methods
.method public getType()Lbr/com/itau/sdk/android/core/exception/TokenException$Type;
    .registers 2

    .line 37
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/exception/TokenException;->type:Lbr/com/itau/sdk/android/core/exception/TokenException$Type;

    return-object v0
.end method

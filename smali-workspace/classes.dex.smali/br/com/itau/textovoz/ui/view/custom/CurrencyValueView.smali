.class public Lbr/com/itau/textovoz/ui/view/custom/CurrencyValueView;
.super Landroid/widget/LinearLayout;
.source "CurrencyValueView.java"


# instance fields
.field private currencyColor:I

.field private currencyFont:Landroid/graphics/Typeface;

.field private currencyText:Ljava/lang/String;

.field private currencyTextSize:F

.field currencyTextView:Landroid/widget/TextView;

.field private currencyTypeface:Ljava/lang/String;

.field private defaultCurrency:Z

.field signalTextView:Landroid/widget/TextView;

.field private valueColor:I

.field private valueFont:Landroid/graphics/Typeface;

.field private valueText:Ljava/lang/String;

.field private valueTextSize:F

.field valueTextView:Landroid/widget/TextView;

.field private valueTypeface:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 2
    .param p1, "context"    # Landroid/content/Context;

    .line 39
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 40
    invoke-direct {p0}, Lbr/com/itau/textovoz/ui/view/custom/CurrencyValueView;->init()V

    .line 41
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .line 44
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 45
    invoke-direct {p0}, Lbr/com/itau/textovoz/ui/view/custom/CurrencyValueView;->init()V

    .line 46
    invoke-direct {p0, p1, p2}, Lbr/com/itau/textovoz/ui/view/custom/CurrencyValueView;->getAttrs(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 47
    invoke-virtual {p0}, Lbr/com/itau/textovoz/ui/view/custom/CurrencyValueView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0}, Lbr/com/itau/textovoz/ui/view/custom/CurrencyValueView;->applyAttrs(Landroid/content/Context;)V

    .line 48
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .line 51
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 52
    invoke-direct {p0}, Lbr/com/itau/textovoz/ui/view/custom/CurrencyValueView;->init()V

    .line 53
    invoke-direct {p0, p1, p2}, Lbr/com/itau/textovoz/ui/view/custom/CurrencyValueView;->getAttrs(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 54
    invoke-virtual {p0}, Lbr/com/itau/textovoz/ui/view/custom/CurrencyValueView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0}, Lbr/com/itau/textovoz/ui/view/custom/CurrencyValueView;->applyAttrs(Landroid/content/Context;)V

    .line 55
    return-void
.end method

.method private applyAttrs(Landroid/content/Context;)V
    .registers 5
    .param p1, "context"    # Landroid/content/Context;

    .line 98
    invoke-virtual {p0}, Lbr/com/itau/textovoz/ui/view/custom/CurrencyValueView;->isInEditMode()Z

    move-result v0

    if-nez v0, :cond_34

    .line 99
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/view/custom/CurrencyValueView;->currencyTypeface:Ljava/lang/String;

    if-eqz v0, :cond_1d

    .line 100
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/view/custom/CurrencyValueView;->currencyTypeface:Ljava/lang/String;

    invoke-static {p1, v0}, Lbr/com/itau/textovoz/util/StringUtils;->getTypeface(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/textovoz/ui/view/custom/CurrencyValueView;->currencyFont:Landroid/graphics/Typeface;

    .line 101
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/view/custom/CurrencyValueView;->currencyFont:Landroid/graphics/Typeface;

    if-eqz v0, :cond_1d

    .line 102
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/view/custom/CurrencyValueView;->currencyTextView:Landroid/widget/TextView;

    iget-object v1, p0, Lbr/com/itau/textovoz/ui/view/custom/CurrencyValueView;->currencyFont:Landroid/graphics/Typeface;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 105
    :cond_1d
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/view/custom/CurrencyValueView;->valueTypeface:Ljava/lang/String;

    if-eqz v0, :cond_34

    .line 106
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/view/custom/CurrencyValueView;->valueTypeface:Ljava/lang/String;

    invoke-static {p1, v0}, Lbr/com/itau/textovoz/util/StringUtils;->getTypeface(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/textovoz/ui/view/custom/CurrencyValueView;->valueFont:Landroid/graphics/Typeface;

    .line 107
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/view/custom/CurrencyValueView;->valueFont:Landroid/graphics/Typeface;

    if-eqz v0, :cond_34

    .line 108
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/view/custom/CurrencyValueView;->valueTextView:Landroid/widget/TextView;

    iget-object v1, p0, Lbr/com/itau/textovoz/ui/view/custom/CurrencyValueView;->valueFont:Landroid/graphics/Typeface;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 113
    :cond_34
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/view/custom/CurrencyValueView;->currencyTextView:Landroid/widget/TextView;

    iget v1, p0, Lbr/com/itau/textovoz/ui/view/custom/CurrencyValueView;->currencyColor:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 114
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/view/custom/CurrencyValueView;->currencyTextView:Landroid/widget/TextView;

    iget v1, p0, Lbr/com/itau/textovoz/ui/view/custom/CurrencyValueView;->currencyTextSize:F

    const/4 v2, 0x0

    invoke-virtual {v0, v2, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 115
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/view/custom/CurrencyValueView;->valueTextView:Landroid/widget/TextView;

    iget v1, p0, Lbr/com/itau/textovoz/ui/view/custom/CurrencyValueView;->valueColor:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 116
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/view/custom/CurrencyValueView;->valueTextView:Landroid/widget/TextView;

    iget v1, p0, Lbr/com/itau/textovoz/ui/view/custom/CurrencyValueView;->valueTextSize:F

    const/4 v2, 0x0

    invoke-virtual {v0, v2, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 118
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/view/custom/CurrencyValueView;->signalTextView:Landroid/widget/TextView;

    iget v1, p0, Lbr/com/itau/textovoz/ui/view/custom/CurrencyValueView;->valueTextSize:F

    const/4 v2, 0x0

    invoke-virtual {v0, v2, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 120
    iget-boolean v0, p0, Lbr/com/itau/textovoz/ui/view/custom/CurrencyValueView;->defaultCurrency:Z

    if-eqz v0, :cond_65

    .line 121
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/view/custom/CurrencyValueView;->currencyTextView:Landroid/widget/TextView;

    iget-object v1, p0, Lbr/com/itau/textovoz/ui/view/custom/CurrencyValueView;->currencyText:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 124
    :cond_65
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/view/custom/CurrencyValueView;->valueTextView:Landroid/widget/TextView;

    iget-object v1, p0, Lbr/com/itau/textovoz/ui/view/custom/CurrencyValueView;->valueText:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 125
    return-void
.end method

.method private getAttrs(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .line 74
    sget-object v0, Lbr/com/itau/textovoz/R$styleable;->CurrencyValueView:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v3

    .line 75
    .local v3, "values":Landroid/content/res/TypedArray;
    sget v0, Lbr/com/itau/textovoz/R$styleable;->CurrencyValueView_cv_currencyColor:I

    sget v1, Lbr/com/itau/textovoz/R$color;->black_464646:I

    .line 76
    invoke-static {p1, v1}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v1

    .line 75
    invoke-virtual {v3, v0, v1}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    iput v0, p0, Lbr/com/itau/textovoz/ui/view/custom/CurrencyValueView;->currencyColor:I

    .line 77
    sget v0, Lbr/com/itau/textovoz/R$styleable;->CurrencyValueView_cv_valueColor:I

    sget v1, Lbr/com/itau/textovoz/R$color;->black_464646:I

    .line 78
    invoke-static {p1, v1}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v1

    .line 77
    invoke-virtual {v3, v0, v1}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    iput v0, p0, Lbr/com/itau/textovoz/ui/view/custom/CurrencyValueView;->valueColor:I

    .line 79
    sget v0, Lbr/com/itau/textovoz/R$styleable;->CurrencyValueView_cv_currencyTypeface:I

    invoke-virtual {v3, v0}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/textovoz/ui/view/custom/CurrencyValueView;->currencyTypeface:Ljava/lang/String;

    .line 80
    sget v0, Lbr/com/itau/textovoz/R$styleable;->CurrencyValueView_cv_valueTypeface:I

    invoke-virtual {v3, v0}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/textovoz/ui/view/custom/CurrencyValueView;->valueTypeface:Ljava/lang/String;

    .line 81
    sget v0, Lbr/com/itau/textovoz/R$styleable;->CurrencyValueView_cv_currencyTextSize:I

    .line 82
    invoke-virtual {p0}, Lbr/com/itau/textovoz/ui/view/custom/CurrencyValueView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lbr/com/itau/textovoz/R$dimen;->default_text_25_sp:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 81
    invoke-virtual {v3, v0, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Lbr/com/itau/textovoz/ui/view/custom/CurrencyValueView;->currencyTextSize:F

    .line 84
    sget v0, Lbr/com/itau/textovoz/R$styleable;->CurrencyValueView_cv_valueTextSize:I

    .line 85
    invoke-virtual {p0}, Lbr/com/itau/textovoz/ui/view/custom/CurrencyValueView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lbr/com/itau/textovoz/R$dimen;->default_text_25_sp:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 84
    invoke-virtual {v3, v0, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Lbr/com/itau/textovoz/ui/view/custom/CurrencyValueView;->valueTextSize:F

    .line 87
    sget v0, Lbr/com/itau/textovoz/R$styleable;->CurrencyValueView_cv_currencyText:I

    invoke-virtual {v3, v0}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_6b

    sget v0, Lbr/com/itau/textovoz/R$styleable;->CurrencyValueView_cv_currencyText:I

    .line 89
    invoke-virtual {v3, v0}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_75

    :cond_6b
    invoke-virtual {p0}, Lbr/com/itau/textovoz/ui/view/custom/CurrencyValueView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lbr/com/itau/textovoz/R$string;->currency:I

    .line 90
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_75
    iput-object v0, p0, Lbr/com/itau/textovoz/ui/view/custom/CurrencyValueView;->currencyText:Ljava/lang/String;

    .line 91
    sget v0, Lbr/com/itau/textovoz/R$styleable;->CurrencyValueView_cv_valueText:I

    invoke-virtual {v3, v0}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/textovoz/ui/view/custom/CurrencyValueView;->valueText:Ljava/lang/String;

    .line 92
    sget v0, Lbr/com/itau/textovoz/R$styleable;->CurrencyValueView_cv_show_default_currency_on_create:I

    const/4 v1, 0x0

    invoke-virtual {v3, v0, v1}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    iput-boolean v0, p0, Lbr/com/itau/textovoz/ui/view/custom/CurrencyValueView;->defaultCurrency:Z

    .line 94
    invoke-virtual {v3}, Landroid/content/res/TypedArray;->recycle()V

    .line 95
    return-void
.end method

.method private init()V
    .registers 3

    .line 66
    invoke-virtual {p0}, Lbr/com/itau/textovoz/ui/view/custom/CurrencyValueView;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lbr/com/itau/textovoz/R$layout;->view_currency_value:I

    invoke-static {v0, v1, p0}, Lbr/com/itau/textovoz/ui/view/custom/CurrencyValueView;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 68
    sget v0, Lbr/com/itau/textovoz/R$id;->text_view_currency_signal_currency:I

    invoke-virtual {p0, v0}, Lbr/com/itau/textovoz/ui/view/custom/CurrencyValueView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lbr/com/itau/textovoz/ui/view/custom/CurrencyValueView;->signalTextView:Landroid/widget/TextView;

    .line 69
    sget v0, Lbr/com/itau/textovoz/R$id;->text_view_currency_value_currency:I

    invoke-virtual {p0, v0}, Lbr/com/itau/textovoz/ui/view/custom/CurrencyValueView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lbr/com/itau/textovoz/ui/view/custom/CurrencyValueView;->currencyTextView:Landroid/widget/TextView;

    .line 70
    sget v0, Lbr/com/itau/textovoz/R$id;->text_view_currency_value_value:I

    invoke-virtual {p0, v0}, Lbr/com/itau/textovoz/ui/view/custom/CurrencyValueView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lbr/com/itau/textovoz/ui/view/custom/CurrencyValueView;->valueTextView:Landroid/widget/TextView;

    .line 71
    return-void
.end method

.method private prepareToNegativeNumber(Ljava/lang/String;)Ljava/lang/String;
    .registers 5
    .param p1, "valueText"    # Ljava/lang/String;

    .line 128
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_51

    const-string v0, "-"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_51

    .line 129
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/view/custom/CurrencyValueView;->signalTextView:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 130
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/view/custom/CurrencyValueView;->currencyTextView:Landroid/widget/TextView;

    iget-object v1, p0, Lbr/com/itau/textovoz/ui/view/custom/CurrencyValueView;->currencyText:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 132
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/view/custom/CurrencyValueView;->currencyTextView:Landroid/widget/TextView;

    invoke-virtual {p0}, Lbr/com/itau/textovoz/ui/view/custom/CurrencyValueView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lbr/com/itau/textovoz/R$color;->signal_negative_color:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 133
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/view/custom/CurrencyValueView;->signalTextView:Landroid/widget/TextView;

    invoke-virtual {p0}, Lbr/com/itau/textovoz/ui/view/custom/CurrencyValueView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lbr/com/itau/textovoz/R$color;->signal_negative_color:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 134
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/view/custom/CurrencyValueView;->valueTextView:Landroid/widget/TextView;

    invoke-virtual {p0}, Lbr/com/itau/textovoz/ui/view/custom/CurrencyValueView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lbr/com/itau/textovoz/R$color;->signal_negative_color:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 136
    const-string v0, "-"

    const-string v1, ""

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 138
    :cond_51
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/view/custom/CurrencyValueView;->currencyTextView:Landroid/widget/TextView;

    iget-object v1, p0, Lbr/com/itau/textovoz/ui/view/custom/CurrencyValueView;->currencyText:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 139
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/view/custom/CurrencyValueView;->signalTextView:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 140
    return-object p1
.end method


# virtual methods
.method public setCurrencyText(I)V
    .registers 3
    .param p1, "currencyText"    # I

    .line 149
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/view/custom/CurrencyValueView;->currencyTextView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    .line 150
    invoke-virtual {p0}, Lbr/com/itau/textovoz/ui/view/custom/CurrencyValueView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/textovoz/ui/view/custom/CurrencyValueView;->currencyText:Ljava/lang/String;

    .line 151
    return-void
.end method

.method public setCurrencyText(Ljava/lang/String;)V
    .registers 3
    .param p1, "currencyText"    # Ljava/lang/String;

    .line 144
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/view/custom/CurrencyValueView;->currencyTextView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 145
    iput-object p1, p0, Lbr/com/itau/textovoz/ui/view/custom/CurrencyValueView;->currencyText:Ljava/lang/String;

    .line 146
    return-void
.end method

.method public setValueText(I)V
    .registers 3
    .param p1, "valueText"    # I

    .line 161
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/view/custom/CurrencyValueView;->valueTextView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    .line 162
    return-void
.end method

.method public setValueText(Ljava/lang/String;)V
    .registers 4
    .param p1, "valueText"    # Ljava/lang/String;

    .line 154
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/view/custom/CurrencyValueView;->currencyText:Ljava/lang/String;

    if-eqz v0, :cond_b

    .line 155
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/view/custom/CurrencyValueView;->currencyTextView:Landroid/widget/TextView;

    iget-object v1, p0, Lbr/com/itau/textovoz/ui/view/custom/CurrencyValueView;->currencyText:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 157
    :cond_b
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/view/custom/CurrencyValueView;->valueTextView:Landroid/widget/TextView;

    invoke-direct {p0, p1}, Lbr/com/itau/textovoz/ui/view/custom/CurrencyValueView;->prepareToNegativeNumber(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 158
    return-void
.end method

.class public Lcom/itau/empresas/feature/home/customview/FormalizacaoRemotaWarningView;
.super Lcom/itau/empresas/ui/view/WarningView;
.source "FormalizacaoRemotaWarningView.java"


# instance fields
.field application:Lcom/itau/empresas/CustomApplication;

.field controller:Lcom/itau/empresas/controller/MenuUniversalController;

.field corLaranja:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 2
    .param p1, "context"    # Landroid/content/Context;

    .line 40
    invoke-direct {p0, p1}, Lcom/itau/empresas/ui/view/WarningView;-><init>(Landroid/content/Context;)V

    .line 41
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .line 44
    invoke-direct {p0, p1, p2}, Lcom/itau/empresas/ui/view/WarningView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 45
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .line 48
    invoke-direct {p0, p1, p2, p3}, Lcom/itau/empresas/ui/view/WarningView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 49
    return-void
.end method

.method private setMensagem(I)V
    .registers 4
    .param p1, "resString"    # I

    .line 80
    iget-object v0, p0, Lcom/itau/empresas/feature/home/customview/FormalizacaoRemotaWarningView;->imgWarning:Landroid/widget/ImageView;

    const v1, 0x7f020129

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 81
    iget-object v0, p0, Lcom/itau/empresas/feature/home/customview/FormalizacaoRemotaWarningView;->llViewWarning:Landroid/widget/LinearLayout;

    iget v1, p0, Lcom/itau/empresas/feature/home/customview/FormalizacaoRemotaWarningView;->corLaranja:I

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundColor(I)V

    .line 82
    iget-object v0, p0, Lcom/itau/empresas/feature/home/customview/FormalizacaoRemotaWarningView;->textoWarning:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    .line 83
    return-void
.end method


# virtual methods
.method afterViews()V
    .registers 2

    .line 59
    const v0, 0x7f07056c

    invoke-direct {p0, v0}, Lcom/itau/empresas/feature/home/customview/FormalizacaoRemotaWarningView;->setMensagem(I)V

    .line 60
    new-instance v0, Lcom/itau/empresas/feature/home/customview/FormalizacaoRemotaWarningView$1;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/home/customview/FormalizacaoRemotaWarningView$1;-><init>(Lcom/itau/empresas/feature/home/customview/FormalizacaoRemotaWarningView;)V

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/home/customview/FormalizacaoRemotaWarningView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 72
    return-void
.end method

.method protected onAttachedToWindow()V
    .registers 1

    .line 91
    invoke-static {p0}, Lcom/itau/empresas/ui/util/EventBusUtils;->registraEventBus(Landroid/view/View;)V

    .line 92
    invoke-super {p0}, Lcom/itau/empresas/ui/view/WarningView;->onAttachedToWindow()V

    .line 93
    return-void
.end method

.method protected onDetachedFromWindow()V
    .registers 2

    .line 100
    invoke-static {}, Lde/greenrobot/event/EventBus;->getDefault()Lde/greenrobot/event/EventBus;

    move-result-object v0

    invoke-virtual {v0, p0}, Lde/greenrobot/event/EventBus;->isRegistered(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 101
    invoke-static {}, Lde/greenrobot/event/EventBus;->getDefault()Lde/greenrobot/event/EventBus;

    move-result-object v0

    invoke-virtual {v0, p0}, Lde/greenrobot/event/EventBus;->unregister(Ljava/lang/Object;)V

    .line 103
    :cond_11
    invoke-super {p0}, Lcom/itau/empresas/ui/view/WarningView;->onDetachedFromWindow()V

    .line 104
    return-void
.end method

.method public onEventMainThread(Lcom/itau/empresas/api/model/MenuUniversalVO;)V
    .registers 10
    .param p1, "menuUniversalVO"    # Lcom/itau/empresas/api/model/MenuUniversalVO;

    .line 115
    const-string v0, "POSSUI_ACESSO_AUTORIZANTE"

    invoke-virtual {p1, v0}, Lcom/itau/empresas/api/model/MenuUniversalVO;->getDado(Ljava/lang/String;)Lcom/itau/empresas/api/model/DadoMenuUniversalVO;

    move-result-object v6

    .line 116
    .local v6, "dAutorizante":Lcom/itau/empresas/api/model/DadoMenuUniversalVO;
    const-string v0, "EXISTE_FORMALIZACAO_REMOTA"

    .line 117
    invoke-virtual {p1, v0}, Lcom/itau/empresas/api/model/MenuUniversalVO;->getDado(Ljava/lang/String;)Lcom/itau/empresas/api/model/DadoMenuUniversalVO;

    move-result-object v7

    .line 119
    .local v7, "dFormalizacao":Lcom/itau/empresas/api/model/DadoMenuUniversalVO;
    if-eqz v6, :cond_3b

    if-eqz v7, :cond_3b

    invoke-virtual {v6}, Lcom/itau/empresas/api/model/DadoMenuUniversalVO;->toBoolean()Z

    move-result v0

    if-eqz v0, :cond_3b

    .line 120
    invoke-virtual {v7}, Lcom/itau/empresas/api/model/DadoMenuUniversalVO;->toBoolean()Z

    move-result v0

    if-eqz v0, :cond_3b

    .line 121
    iget-object v0, p0, Lcom/itau/empresas/feature/home/customview/FormalizacaoRemotaWarningView;->application:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getEventoTracker()Lcom/itau/empresas/ui/util/analytics/TrackerEvento;

    move-result-object v0

    invoke-virtual {p0}, Lcom/itau/empresas/feature/home/customview/FormalizacaoRemotaWarningView;->getContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, Lcom/itau/empresas/ui/util/analytics/EventoAnalyticsRes;

    const v3, 0x7f0702de

    const v4, 0x7f0702aa

    const v5, 0x7f070307

    invoke-direct {v2, v3, v4, v5}, Lcom/itau/empresas/ui/util/analytics/EventoAnalyticsRes;-><init>(III)V

    invoke-virtual {v0, v1, v2}, Lcom/itau/empresas/ui/util/analytics/TrackerEvento;->enviarHitEvento(Landroid/content/Context;Lcom/itau/empresas/ui/util/analytics/EventoAnalyticsRes;)V

    .line 125
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/home/customview/FormalizacaoRemotaWarningView;->setVisibility(I)V

    .line 127
    :cond_3b
    return-void
.end method

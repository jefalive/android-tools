.class public final enum Lcom/itau/empresas/ui/util/MascaraValorMonetario$Moeda;
.super Ljava/lang/Enum;
.source "MascaraValorMonetario.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/itau/empresas/ui/util/MascaraValorMonetario;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Moeda"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<Lcom/itau/empresas/ui/util/MascaraValorMonetario$Moeda;>;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/itau/empresas/ui/util/MascaraValorMonetario$Moeda;

.field public static final enum DOLAR:Lcom/itau/empresas/ui/util/MascaraValorMonetario$Moeda;

.field public static final enum REAL:Lcom/itau/empresas/ui/util/MascaraValorMonetario$Moeda;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .line 14
    new-instance v0, Lcom/itau/empresas/ui/util/MascaraValorMonetario$Moeda;

    const-string v1, "REAL"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/itau/empresas/ui/util/MascaraValorMonetario$Moeda;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/itau/empresas/ui/util/MascaraValorMonetario$Moeda;->REAL:Lcom/itau/empresas/ui/util/MascaraValorMonetario$Moeda;

    new-instance v0, Lcom/itau/empresas/ui/util/MascaraValorMonetario$Moeda;

    const-string v1, "DOLAR"

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lcom/itau/empresas/ui/util/MascaraValorMonetario$Moeda;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/itau/empresas/ui/util/MascaraValorMonetario$Moeda;->DOLAR:Lcom/itau/empresas/ui/util/MascaraValorMonetario$Moeda;

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/itau/empresas/ui/util/MascaraValorMonetario$Moeda;

    sget-object v1, Lcom/itau/empresas/ui/util/MascaraValorMonetario$Moeda;->REAL:Lcom/itau/empresas/ui/util/MascaraValorMonetario$Moeda;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sget-object v1, Lcom/itau/empresas/ui/util/MascaraValorMonetario$Moeda;->DOLAR:Lcom/itau/empresas/ui/util/MascaraValorMonetario$Moeda;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    sput-object v0, Lcom/itau/empresas/ui/util/MascaraValorMonetario$Moeda;->$VALUES:[Lcom/itau/empresas/ui/util/MascaraValorMonetario$Moeda;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 14
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/itau/empresas/ui/util/MascaraValorMonetario$Moeda;
    .registers 2
    .param p0, "name"    # Ljava/lang/String;

    .line 14
    const-class v0, Lcom/itau/empresas/ui/util/MascaraValorMonetario$Moeda;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/ui/util/MascaraValorMonetario$Moeda;

    return-object v0
.end method

.method public static values()[Lcom/itau/empresas/ui/util/MascaraValorMonetario$Moeda;
    .registers 1

    .line 14
    sget-object v0, Lcom/itau/empresas/ui/util/MascaraValorMonetario$Moeda;->$VALUES:[Lcom/itau/empresas/ui/util/MascaraValorMonetario$Moeda;

    invoke-virtual {v0}, [Lcom/itau/empresas/ui/util/MascaraValorMonetario$Moeda;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/itau/empresas/ui/util/MascaraValorMonetario$Moeda;

    return-object v0
.end method

.class public Lcom/itau/empresas/api/model/UtilizacaoVO;
.super Ljava/lang/Object;
.source "UtilizacaoVO.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private dataJurosAcumulados:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "data_juros_acumulados "
    .end annotation
.end field

.field private diasUtilizados:Ljava/lang/Integer;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "dias_utilizados"
    .end annotation
.end field

.field private jurosAcumulados:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "juros_acumulados"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getDataJurosAcumulados()Ljava/lang/String;
    .registers 2

    .line 27
    iget-object v0, p0, Lcom/itau/empresas/api/model/UtilizacaoVO;->dataJurosAcumulados:Ljava/lang/String;

    return-object v0
.end method

.method public getDiasUtilizados()Ljava/lang/Integer;
    .registers 2

    .line 19
    iget-object v0, p0, Lcom/itau/empresas/api/model/UtilizacaoVO;->diasUtilizados:Ljava/lang/Integer;

    return-object v0
.end method

.method public getJurosAcumulados()Ljava/lang/String;
    .registers 2

    .line 23
    iget-object v0, p0, Lcom/itau/empresas/api/model/UtilizacaoVO;->jurosAcumulados:Ljava/lang/String;

    return-object v0
.end method

.class Lcom/adobe/mobile/MobileConfig$9;
.super Ljava/lang/Object;
.source "MobileConfig.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/adobe/mobile/MobileConfig;->downloadRemoteConfigs()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/adobe/mobile/MobileConfig;


# direct methods
.method constructor <init>(Lcom/adobe/mobile/MobileConfig;)V
    .registers 2
    .param p1, "this$0"    # Lcom/adobe/mobile/MobileConfig;

    .line 694
    iput-object p1, p0, Lcom/adobe/mobile/MobileConfig$9;->this$0:Lcom/adobe/mobile/MobileConfig;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .registers 7

    .line 698
    new-instance v4, Ljava/util/concurrent/FutureTask;

    new-instance v0, Lcom/adobe/mobile/MobileConfig$9$1;

    invoke-direct {v0, p0}, Lcom/adobe/mobile/MobileConfig$9$1;-><init>(Lcom/adobe/mobile/MobileConfig$9;)V

    invoke-direct {v4, v0}, Ljava/util/concurrent/FutureTask;-><init>(Ljava/util/concurrent/Callable;)V

    .line 704
    .local v4, "f":Ljava/util/concurrent/FutureTask;, "Ljava/util/concurrent/FutureTask<Ljava/lang/Void;>;"
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getMessagesExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    invoke-interface {v0, v4}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 706
    :try_start_11
    invoke-virtual {v4}, Ljava/util/concurrent/FutureTask;->get()Ljava/lang/Object;
    :try_end_14
    .catch Ljava/lang/Exception; {:try_start_11 .. :try_end_14} :catch_15

    .line 709
    goto :goto_25

    .line 707
    :catch_15
    move-exception v5

    .line 708
    .local v5, "e":Ljava/lang/Exception;
    const-string v0, "Data Callback - Error waiting for callbacks being loaded (%s)"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v5}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logErrorFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 710
    .end local v5    # "e":Ljava/lang/Exception;
    :goto_25
    return-void
.end method

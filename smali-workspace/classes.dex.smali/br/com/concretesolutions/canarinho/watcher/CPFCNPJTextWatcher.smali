.class public Lbr/com/concretesolutions/canarinho/watcher/CPFCNPJTextWatcher;
.super Lbr/com/concretesolutions/canarinho/watcher/BaseCanarinhoTextWatcher;
.source "CPFCNPJTextWatcher.java"


# static fields
.field private static final CNPJ:[C

.field private static final CPF:[C

.field private static final FILTRO_CPF_CNPJ:[Landroid/text/InputFilter;


# instance fields
.field private final resultadoParcial:Lbr/com/concretesolutions/canarinho/validator/Validador$ResultadoParcial;

.field private final validador:Lbr/com/concretesolutions/canarinho/validator/Validador;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .line 19
    const-string v0, "###.###.###-##"

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    sput-object v0, Lbr/com/concretesolutions/canarinho/watcher/CPFCNPJTextWatcher;->CPF:[C

    .line 20
    const-string v0, "##.###.###/####-##"

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    sput-object v0, Lbr/com/concretesolutions/canarinho/watcher/CPFCNPJTextWatcher;->CNPJ:[C

    .line 21
    const/4 v0, 0x1

    new-array v0, v0, [Landroid/text/InputFilter;

    new-instance v1, Landroid/text/InputFilter$LengthFilter;

    sget-object v2, Lbr/com/concretesolutions/canarinho/watcher/CPFCNPJTextWatcher;->CNPJ:[C

    array-length v2, v2

    invoke-direct {v1, v2}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sput-object v0, Lbr/com/concretesolutions/canarinho/watcher/CPFCNPJTextWatcher;->FILTRO_CPF_CNPJ:[Landroid/text/InputFilter;

    return-void
.end method

.method public constructor <init>()V
    .registers 2

    .line 29
    invoke-direct {p0}, Lbr/com/concretesolutions/canarinho/watcher/BaseCanarinhoTextWatcher;-><init>()V

    .line 23
    invoke-static {}, Lbr/com/concretesolutions/canarinho/validator/ValidadorCPFCNPJ;->getInstance()Lbr/com/concretesolutions/canarinho/validator/ValidadorCPFCNPJ;

    move-result-object v0

    iput-object v0, p0, Lbr/com/concretesolutions/canarinho/watcher/CPFCNPJTextWatcher;->validador:Lbr/com/concretesolutions/canarinho/validator/Validador;

    .line 24
    new-instance v0, Lbr/com/concretesolutions/canarinho/validator/Validador$ResultadoParcial;

    invoke-direct {v0}, Lbr/com/concretesolutions/canarinho/validator/Validador$ResultadoParcial;-><init>()V

    iput-object v0, p0, Lbr/com/concretesolutions/canarinho/watcher/CPFCNPJTextWatcher;->resultadoParcial:Lbr/com/concretesolutions/canarinho/validator/Validador$ResultadoParcial;

    .line 30
    return-void
.end method

.method private ehCpf(Landroid/text/Editable;)Z
    .registers 4
    .param p1, "e"    # Landroid/text/Editable;

    .line 57
    sget-object v0, Lbr/com/concretesolutions/canarinho/formatador/Formatador$Padroes;->PADRAO_SOMENTE_NUMEROS:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/util/regex/Matcher;->replaceAll(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v1, 0xc

    if-ge v0, v1, :cond_16

    const/4 v0, 0x1

    goto :goto_17

    :cond_16
    const/4 v0, 0x0

    :goto_17
    return v0
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .registers 6
    .param p1, "s"    # Landroid/text/Editable;

    .line 44
    invoke-virtual {p0}, Lbr/com/concretesolutions/canarinho/watcher/CPFCNPJTextWatcher;->isMudancaInterna()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 45
    return-void

    .line 48
    :cond_7
    sget-object v0, Lbr/com/concretesolutions/canarinho/watcher/CPFCNPJTextWatcher;->FILTRO_CPF_CNPJ:[Landroid/text/InputFilter;

    invoke-interface {p1, v0}, Landroid/text/Editable;->setFilters([Landroid/text/InputFilter;)V

    .line 50
    invoke-direct {p0, p1}, Lbr/com/concretesolutions/canarinho/watcher/CPFCNPJTextWatcher;->ehCpf(Landroid/text/Editable;)Z

    move-result v0

    if-eqz v0, :cond_15

    sget-object v2, Lbr/com/concretesolutions/canarinho/watcher/CPFCNPJTextWatcher;->CPF:[C

    goto :goto_17

    :cond_15
    sget-object v2, Lbr/com/concretesolutions/canarinho/watcher/CPFCNPJTextWatcher;->CNPJ:[C

    .line 51
    .local v2, "mascara":[C
    :goto_17
    invoke-virtual {p0, p1, v2}, Lbr/com/concretesolutions/canarinho/watcher/CPFCNPJTextWatcher;->trataAdicaoRemocaoDeCaracter(Landroid/text/Editable;[C)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 52
    .local v3, "builder":Ljava/lang/StringBuilder;
    iget-object v0, p0, Lbr/com/concretesolutions/canarinho/watcher/CPFCNPJTextWatcher;->validador:Lbr/com/concretesolutions/canarinho/validator/Validador;

    iget-object v1, p0, Lbr/com/concretesolutions/canarinho/watcher/CPFCNPJTextWatcher;->resultadoParcial:Lbr/com/concretesolutions/canarinho/validator/Validador$ResultadoParcial;

    invoke-virtual {p0, v0, v1, p1, v3}, Lbr/com/concretesolutions/canarinho/watcher/CPFCNPJTextWatcher;->atualizaTexto(Lbr/com/concretesolutions/canarinho/validator/Validador;Lbr/com/concretesolutions/canarinho/validator/Validador$ResultadoParcial;Landroid/text/Editable;Ljava/lang/StringBuilder;)V

    .line 53
    return-void
.end method

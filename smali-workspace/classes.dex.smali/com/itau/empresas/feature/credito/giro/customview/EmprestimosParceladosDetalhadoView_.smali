.class public final Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView_;
.super Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;
.source "EmprestimosParceladosDetalhadoView_.java"

# interfaces
.implements Lorg/androidannotations/api/view/HasViews;
.implements Lorg/androidannotations/api/view/OnViewChangedListener;


# instance fields
.field private alreadyInflated_:Z

.field private final onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .param p1, "context"    # Landroid/content/Context;

    .line 40
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;-><init>(Landroid/content/Context;)V

    .line 36
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView_;->alreadyInflated_:Z

    .line 37
    new-instance v0, Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-direct {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 41
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView_;->init_()V

    .line 42
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .line 45
    invoke-direct {p0, p1, p2}, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 36
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView_;->alreadyInflated_:Z

    .line 37
    new-instance v0, Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-direct {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 46
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView_;->init_()V

    .line 47
    return-void
.end method

.method private init_()V
    .registers 3

    .line 72
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-static {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    move-result-object v1

    .line 73
    .local v1, "previousNotifier":Lorg/androidannotations/api/view/OnViewChangedNotifier;
    invoke-static {p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->registerOnViewChangedListener(Lorg/androidannotations/api/view/OnViewChangedListener;)V

    .line 74
    invoke-static {}, Lcom/itau/empresas/CustomApplication_;->getInstance()Lcom/itau/empresas/CustomApplication;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView_;->application:Lcom/itau/empresas/CustomApplication;

    .line 75
    invoke-static {v1}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 76
    return-void
.end method


# virtual methods
.method public onFinishInflate()V
    .registers 3

    .line 63
    iget-boolean v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView_;->alreadyInflated_:Z

    if-nez v0, :cond_16

    .line 64
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView_;->alreadyInflated_:Z

    .line 65
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView_;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f030167

    invoke-static {v0, v1, p0}, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView_;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 66
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 68
    :cond_16
    invoke-super {p0}, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->onFinishInflate()V

    .line 69
    return-void
.end method

.method public onViewChanged(Lorg/androidannotations/api/view/HasViews;)V
    .registers 5
    .param p1, "hasViews"    # Lorg/androidannotations/api/view/HasViews;

    .line 86
    const v0, 0x7f0e0627

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView_;->botaoContratarCreditoDetalhado:Landroid/widget/Button;

    .line 87
    const v0, 0x7f0e0626

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView_;->botaoCreditoDetalhadoCompartilhar:Landroid/widget/Button;

    .line 88
    const v0, 0x7f0e022b

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView_;->botaoSimular:Landroid/widget/Button;

    .line 89
    const v0, 0x7f0e0622

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView_;->llCondicoesGeraisBotao:Landroid/widget/LinearLayout;

    .line 90
    const v0, 0x7f0e061d

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView_;->llViewSeguro:Landroid/widget/LinearLayout;

    .line 91
    const v0, 0x7f0e060f

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView_;->llEmprestimosParceladosDetalhadosViewConfirmacao:Landroid/widget/LinearLayout;

    .line 92
    const v0, 0x7f0e0610

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView_;->llViewEmprestimosParceladosDetalhadosValorEmprestimo:Landroid/widget/LinearLayout;

    .line 93
    const v0, 0x7f0e060c

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView_;->llEmprestimosParceladosPersonalizarSimulacaoResultado:Landroid/widget/LinearLayout;

    .line 94
    const v0, 0x7f0e060d

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView_;->llEmprestimosParceladosDetalhadosViewComprovanteLogo:Landroid/widget/LinearLayout;

    .line 95
    const v0, 0x7f0e060b

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ScrollView;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView_;->svEmprestimosParceladosSimulacao:Landroid/widget/ScrollView;

    .line 96
    const v0, 0x7f0e0611

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView_;->textoCreditoSimulacaoValorCredito:Landroid/widget/TextView;

    .line 97
    const v0, 0x7f0e0612

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView_;->textoCreditoSimulacaoTaxaValorCredito:Landroid/widget/TextView;

    .line 98
    const v0, 0x7f0e0613

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView_;->textoCreditoSimulacaoValorTotalFinanciado:Landroid/widget/TextView;

    .line 99
    const v0, 0x7f0e0614

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView_;->textoCreditoSimulacaoTaxaValorTotalFinanciado:Landroid/widget/TextView;

    .line 100
    const v0, 0x7f0e0616

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView_;->textoCreditoSimulacaoParcelas:Landroid/widget/TextView;

    .line 101
    const v0, 0x7f0e0617

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView_;->textoCreditoSimulacaoTipoCapitalizacao:Landroid/widget/TextView;

    .line 102
    const v0, 0x7f0e0618

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView_;->textoCreditoSimulacaoVencimentoPrimeiraParcela:Landroid/widget/TextView;

    .line 103
    const v0, 0x7f0e0619

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView_;->textoCreditoSimulacaoTaxaJuros:Landroid/widget/TextView;

    .line 104
    const v0, 0x7f0e061a

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView_;->textoCreditoSimulacaoCustoEfetivoTotal:Landroid/widget/TextView;

    .line 105
    const v0, 0x7f0e061b

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView_;->textoCreditoSimulacaoValorIof:Landroid/widget/TextView;

    .line 106
    const v0, 0x7f0e061c

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView_;->textoCreditoSimulacaoTaxaIof:Landroid/widget/TextView;

    .line 107
    const v0, 0x7f0e061e

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView_;->textoCreditoSimulacaoValorSeguro:Landroid/widget/TextView;

    .line 108
    const v0, 0x7f0e061f

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView_;->textoCreditoSimulacaoTaxaSeguro:Landroid/widget/TextView;

    .line 109
    const v0, 0x7f0e0620

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView_;->textoCreditoSimulacaoValorTarifaContratacao:Landroid/widget/TextView;

    .line 110
    const v0, 0x7f0e0621

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView_;->textoCreditoSimulacaoTaxaTarifaContratacao:Landroid/widget/TextView;

    .line 111
    const v0, 0x7f0e0615

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView_;->textoCreditoDetalhadoParcelaComValoresSeguro:Landroid/widget/TextView;

    .line 112
    const v0, 0x7f0e0623

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView_;->textoAoConfirmarVoceDeclara:Landroid/widget/TextView;

    .line 113
    const v0, 0x7f0e0167

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView_;->textoCreditoEfetuadoEm:Landroid/widget/TextView;

    .line 114
    const v0, 0x7f0e0624

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 116
    .local v2, "view_ll_credito_detalhado_botao_condicoes_gerais":Landroid/view/View;
    if-eqz v2, :cond_145

    .line 117
    new-instance v0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView_$1;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView_$1;-><init>(Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView_;)V

    invoke-virtual {v2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 126
    :cond_145
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView_;->botaoSimular:Landroid/widget/Button;

    if-eqz v0, :cond_153

    .line 127
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView_;->botaoSimular:Landroid/widget/Button;

    new-instance v1, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView_$2;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView_$2;-><init>(Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView_;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 136
    :cond_153
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView_;->botaoContratarCreditoDetalhado:Landroid/widget/Button;

    if-eqz v0, :cond_161

    .line 137
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView_;->botaoContratarCreditoDetalhado:Landroid/widget/Button;

    new-instance v1, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView_$3;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView_$3;-><init>(Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView_;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 146
    :cond_161
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView_;->botaoCreditoDetalhadoCompartilhar:Landroid/widget/Button;

    if-eqz v0, :cond_16f

    .line 147
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView_;->botaoCreditoDetalhadoCompartilhar:Landroid/widget/Button;

    new-instance v1, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView_$4;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView_$4;-><init>(Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView_;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 156
    :cond_16f
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView_;->aoInicalizar()V

    .line 157
    return-void
.end method

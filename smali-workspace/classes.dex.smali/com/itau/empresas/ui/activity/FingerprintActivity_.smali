.class public final Lcom/itau/empresas/ui/activity/FingerprintActivity_;
.super Lcom/itau/empresas/ui/activity/FingerprintActivity;
.source "FingerprintActivity_.java"

# interfaces
.implements Lorg/androidannotations/api/view/HasViews;
.implements Lorg/androidannotations/api/view/OnViewChangedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/itau/empresas/ui/activity/FingerprintActivity_$IntentBuilder_;
    }
.end annotation


# instance fields
.field private final onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;


# direct methods
.method public constructor <init>()V
    .registers 2

    .line 32
    invoke-direct {p0}, Lcom/itau/empresas/ui/activity/FingerprintActivity;-><init>()V

    .line 36
    new-instance v0, Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-direct {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/ui/activity/FingerprintActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    return-void
.end method

.method static synthetic access$001(Lcom/itau/empresas/ui/activity/FingerprintActivity_;)V
    .registers 1
    .param p0, "x0"    # Lcom/itau/empresas/ui/activity/FingerprintActivity_;

    .line 32
    invoke-super {p0}, Lcom/itau/empresas/ui/activity/FingerprintActivity;->mostraDialogoDeProgresso()V

    return-void
.end method

.method static synthetic access$101(Lcom/itau/empresas/ui/activity/FingerprintActivity_;)V
    .registers 1
    .param p0, "x0"    # Lcom/itau/empresas/ui/activity/FingerprintActivity_;

    .line 32
    invoke-super {p0}, Lcom/itau/empresas/ui/activity/FingerprintActivity;->escondeDialogoDeProgresso()V

    return-void
.end method

.method private init_(Landroid/os/Bundle;)V
    .registers 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 50
    new-instance v0, Lcom/itau/empresas/Preferences_;

    invoke-direct {v0, p0}, Lcom/itau/empresas/Preferences_;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/itau/empresas/ui/activity/FingerprintActivity_;->prefs:Lcom/itau/empresas/Preferences_;

    .line 51
    invoke-static {p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->registerOnViewChangedListener(Lorg/androidannotations/api/view/OnViewChangedListener;)V

    .line 52
    invoke-static {}, Lcom/itau/empresas/CustomApplication_;->getInstance()Lcom/itau/empresas/CustomApplication;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/ui/activity/FingerprintActivity_;->application:Lcom/itau/empresas/CustomApplication;

    .line 53
    invoke-direct {p0}, Lcom/itau/empresas/ui/activity/FingerprintActivity_;->injectExtras_()V

    .line 54
    invoke-virtual {p0}, Lcom/itau/empresas/ui/activity/FingerprintActivity_;->afterInject()V

    .line 55
    return-void
.end method

.method private injectExtras_()V
    .registers 3

    .line 119
    invoke-virtual {p0}, Lcom/itau/empresas/ui/activity/FingerprintActivity_;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 120
    .local v1, "extras_":Landroid/os/Bundle;
    if-eqz v1, :cond_2a

    .line 121
    const-string v0, "status"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 122
    const-string v0, "status"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/ui/activity/FingerprintActivity_;->status:Ljava/lang/String;

    .line 124
    :cond_1a
    const-string v0, "isRedirecionaConfiguracoes"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2a

    .line 125
    const-string v0, "isRedirecionaConfiguracoes"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/itau/empresas/ui/activity/FingerprintActivity_;->isRedirecionaConfiguracoes:Z

    .line 128
    :cond_2a
    return-void
.end method


# virtual methods
.method public escondeDialogoDeProgresso()V
    .registers 5

    .line 153
    const-string v0, ""

    new-instance v1, Lcom/itau/empresas/ui/activity/FingerprintActivity_$4;

    invoke-direct {v1, p0}, Lcom/itau/empresas/ui/activity/FingerprintActivity_$4;-><init>(Lcom/itau/empresas/ui/activity/FingerprintActivity_;)V

    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3}, Lorg/androidannotations/api/UiThreadExecutor;->runTask(Ljava/lang/String;Ljava/lang/Runnable;J)V

    .line 161
    return-void
.end method

.method public mostraDialogoDeProgresso()V
    .registers 5
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "CommitTransaction"
        }
    .end annotation

    .line 141
    const-string v0, ""

    new-instance v1, Lcom/itau/empresas/ui/activity/FingerprintActivity_$3;

    invoke-direct {v1, p0}, Lcom/itau/empresas/ui/activity/FingerprintActivity_$3;-><init>(Lcom/itau/empresas/ui/activity/FingerprintActivity_;)V

    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3}, Lorg/androidannotations/api/UiThreadExecutor;->runTask(Ljava/lang/String;Ljava/lang/Runnable;J)V

    .line 149
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 42
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/FingerprintActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-static {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    move-result-object v1

    .line 43
    .local v1, "previousNotifier":Lorg/androidannotations/api/view/OnViewChangedNotifier;
    invoke-direct {p0, p1}, Lcom/itau/empresas/ui/activity/FingerprintActivity_;->init_(Landroid/os/Bundle;)V

    .line 44
    invoke-super {p0, p1}, Lcom/itau/empresas/ui/activity/FingerprintActivity;->onCreate(Landroid/os/Bundle;)V

    .line 45
    invoke-static {v1}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 46
    const v0, 0x7f030037

    invoke-virtual {p0, v0}, Lcom/itau/empresas/ui/activity/FingerprintActivity_;->setContentView(I)V

    .line 47
    return-void
.end method

.method public onViewChanged(Lorg/androidannotations/api/view/HasViews;)V
    .registers 5
    .param p1, "hasViews"    # Lorg/androidannotations/api/view/HasViews;

    .line 89
    const v0, 0x7f0e018d

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/ui/activity/FingerprintActivity_;->textoTermosDeUso:Landroid/widget/TextView;

    .line 90
    const v0, 0x7f0e018b

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/ui/activity/FingerprintActivity_;->textoTituloFingerprint:Landroid/widget/TextView;

    .line 91
    const v0, 0x7f0e018c

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/ui/activity/FingerprintActivity_;->textoFingerprint:Landroid/widget/TextView;

    .line 92
    const v0, 0x7f0e018e

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 93
    .local v1, "view_botao_agora_nao":Landroid/view/View;
    const v0, 0x7f0e018f

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 95
    .local v2, "view_botao_continuar":Landroid/view/View;
    if-eqz v1, :cond_39

    .line 96
    new-instance v0, Lcom/itau/empresas/ui/activity/FingerprintActivity_$1;

    invoke-direct {v0, p0}, Lcom/itau/empresas/ui/activity/FingerprintActivity_$1;-><init>(Lcom/itau/empresas/ui/activity/FingerprintActivity_;)V

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 105
    :cond_39
    if-eqz v2, :cond_43

    .line 106
    new-instance v0, Lcom/itau/empresas/ui/activity/FingerprintActivity_$2;

    invoke-direct {v0, p0}, Lcom/itau/empresas/ui/activity/FingerprintActivity_$2;-><init>(Lcom/itau/empresas/ui/activity/FingerprintActivity_;)V

    invoke-virtual {v2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 115
    :cond_43
    invoke-virtual {p0}, Lcom/itau/empresas/ui/activity/FingerprintActivity_;->carregaDadosTela()V

    .line 116
    return-void
.end method

.method public setContentView(I)V
    .registers 3
    .param p1, "layoutResID"    # I

    .line 59
    invoke-super {p0, p1}, Lcom/itau/empresas/ui/activity/FingerprintActivity;->setContentView(I)V

    .line 60
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/FingerprintActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 61
    return-void
.end method

.method public setContentView(Landroid/view/View;)V
    .registers 3
    .param p1, "view"    # Landroid/view/View;

    .line 71
    invoke-super {p0, p1}, Lcom/itau/empresas/ui/activity/FingerprintActivity;->setContentView(Landroid/view/View;)V

    .line 72
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/FingerprintActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 73
    return-void
.end method

.method public setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .registers 4
    .param p1, "view"    # Landroid/view/View;
    .param p2, "params"    # Landroid/view/ViewGroup$LayoutParams;

    .line 65
    invoke-super {p0, p1, p2}, Lcom/itau/empresas/ui/activity/FingerprintActivity;->setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 66
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/FingerprintActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 67
    return-void
.end method

.method public setIntent(Landroid/content/Intent;)V
    .registers 2
    .param p1, "newIntent"    # Landroid/content/Intent;

    .line 132
    invoke-super {p0, p1}, Lcom/itau/empresas/ui/activity/FingerprintActivity;->setIntent(Landroid/content/Intent;)V

    .line 133
    invoke-direct {p0}, Lcom/itau/empresas/ui/activity/FingerprintActivity_;->injectExtras_()V

    .line 134
    return-void
.end method

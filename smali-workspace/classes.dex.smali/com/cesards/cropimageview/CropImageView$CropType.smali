.class public final enum Lcom/cesards/cropimageview/CropImageView$CropType;
.super Ljava/lang/Enum;
.source "CropImageView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/cesards/cropimageview/CropImageView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "CropType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<Lcom/cesards/cropimageview/CropImageView$CropType;>;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/cesards/cropimageview/CropImageView$CropType;

.field public static final enum CENTER_BOTTOM:Lcom/cesards/cropimageview/CropImageView$CropType;

.field public static final enum CENTER_TOP:Lcom/cesards/cropimageview/CropImageView$CropType;

.field public static final enum LEFT_BOTTOM:Lcom/cesards/cropimageview/CropImageView$CropType;

.field public static final enum LEFT_CENTER:Lcom/cesards/cropimageview/CropImageView$CropType;

.field public static final enum LEFT_TOP:Lcom/cesards/cropimageview/CropImageView$CropType;

.field public static final enum NONE:Lcom/cesards/cropimageview/CropImageView$CropType;

.field public static final enum RIGHT_BOTTOM:Lcom/cesards/cropimageview/CropImageView$CropType;

.field public static final enum RIGHT_CENTER:Lcom/cesards/cropimageview/CropImageView$CropType;

.field public static final enum RIGHT_TOP:Lcom/cesards/cropimageview/CropImageView$CropType;

.field private static final codeLookup:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<Ljava/lang/Integer;Lcom/cesards/cropimageview/CropImageView$CropType;>;"
        }
    .end annotation
.end field


# instance fields
.field final cropType:I


# direct methods
.method static constructor <clinit>()V
    .registers 8

    .line 178
    new-instance v0, Lcom/cesards/cropimageview/CropImageView$CropType;

    const-string v1, "NONE"

    const/4 v2, 0x0

    const/4 v3, -0x1

    invoke-direct {v0, v1, v2, v3}, Lcom/cesards/cropimageview/CropImageView$CropType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/cesards/cropimageview/CropImageView$CropType;->NONE:Lcom/cesards/cropimageview/CropImageView$CropType;

    .line 179
    new-instance v0, Lcom/cesards/cropimageview/CropImageView$CropType;

    const-string v1, "LEFT_TOP"

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/cesards/cropimageview/CropImageView$CropType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/cesards/cropimageview/CropImageView$CropType;->LEFT_TOP:Lcom/cesards/cropimageview/CropImageView$CropType;

    .line 180
    new-instance v0, Lcom/cesards/cropimageview/CropImageView$CropType;

    const-string v1, "LEFT_CENTER"

    const/4 v2, 0x2

    const/4 v3, 0x1

    invoke-direct {v0, v1, v2, v3}, Lcom/cesards/cropimageview/CropImageView$CropType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/cesards/cropimageview/CropImageView$CropType;->LEFT_CENTER:Lcom/cesards/cropimageview/CropImageView$CropType;

    .line 181
    new-instance v0, Lcom/cesards/cropimageview/CropImageView$CropType;

    const-string v1, "LEFT_BOTTOM"

    const/4 v2, 0x3

    const/4 v3, 0x2

    invoke-direct {v0, v1, v2, v3}, Lcom/cesards/cropimageview/CropImageView$CropType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/cesards/cropimageview/CropImageView$CropType;->LEFT_BOTTOM:Lcom/cesards/cropimageview/CropImageView$CropType;

    .line 182
    new-instance v0, Lcom/cesards/cropimageview/CropImageView$CropType;

    const-string v1, "RIGHT_TOP"

    const/4 v2, 0x4

    const/4 v3, 0x3

    invoke-direct {v0, v1, v2, v3}, Lcom/cesards/cropimageview/CropImageView$CropType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/cesards/cropimageview/CropImageView$CropType;->RIGHT_TOP:Lcom/cesards/cropimageview/CropImageView$CropType;

    .line 183
    new-instance v0, Lcom/cesards/cropimageview/CropImageView$CropType;

    const-string v1, "RIGHT_CENTER"

    const/4 v2, 0x5

    const/4 v3, 0x4

    invoke-direct {v0, v1, v2, v3}, Lcom/cesards/cropimageview/CropImageView$CropType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/cesards/cropimageview/CropImageView$CropType;->RIGHT_CENTER:Lcom/cesards/cropimageview/CropImageView$CropType;

    .line 184
    new-instance v0, Lcom/cesards/cropimageview/CropImageView$CropType;

    const-string v1, "RIGHT_BOTTOM"

    const/4 v2, 0x6

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, Lcom/cesards/cropimageview/CropImageView$CropType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/cesards/cropimageview/CropImageView$CropType;->RIGHT_BOTTOM:Lcom/cesards/cropimageview/CropImageView$CropType;

    .line 185
    new-instance v0, Lcom/cesards/cropimageview/CropImageView$CropType;

    const-string v1, "CENTER_TOP"

    const/4 v2, 0x7

    const/4 v3, 0x6

    invoke-direct {v0, v1, v2, v3}, Lcom/cesards/cropimageview/CropImageView$CropType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/cesards/cropimageview/CropImageView$CropType;->CENTER_TOP:Lcom/cesards/cropimageview/CropImageView$CropType;

    .line 186
    new-instance v0, Lcom/cesards/cropimageview/CropImageView$CropType;

    const-string v1, "CENTER_BOTTOM"

    const/16 v2, 0x8

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, Lcom/cesards/cropimageview/CropImageView$CropType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/cesards/cropimageview/CropImageView$CropType;->CENTER_BOTTOM:Lcom/cesards/cropimageview/CropImageView$CropType;

    .line 177
    const/16 v0, 0x9

    new-array v0, v0, [Lcom/cesards/cropimageview/CropImageView$CropType;

    sget-object v1, Lcom/cesards/cropimageview/CropImageView$CropType;->NONE:Lcom/cesards/cropimageview/CropImageView$CropType;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sget-object v1, Lcom/cesards/cropimageview/CropImageView$CropType;->LEFT_TOP:Lcom/cesards/cropimageview/CropImageView$CropType;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    sget-object v1, Lcom/cesards/cropimageview/CropImageView$CropType;->LEFT_CENTER:Lcom/cesards/cropimageview/CropImageView$CropType;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    sget-object v1, Lcom/cesards/cropimageview/CropImageView$CropType;->LEFT_BOTTOM:Lcom/cesards/cropimageview/CropImageView$CropType;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    sget-object v1, Lcom/cesards/cropimageview/CropImageView$CropType;->RIGHT_TOP:Lcom/cesards/cropimageview/CropImageView$CropType;

    const/4 v2, 0x4

    aput-object v1, v0, v2

    sget-object v1, Lcom/cesards/cropimageview/CropImageView$CropType;->RIGHT_CENTER:Lcom/cesards/cropimageview/CropImageView$CropType;

    const/4 v2, 0x5

    aput-object v1, v0, v2

    sget-object v1, Lcom/cesards/cropimageview/CropImageView$CropType;->RIGHT_BOTTOM:Lcom/cesards/cropimageview/CropImageView$CropType;

    const/4 v2, 0x6

    aput-object v1, v0, v2

    sget-object v1, Lcom/cesards/cropimageview/CropImageView$CropType;->CENTER_TOP:Lcom/cesards/cropimageview/CropImageView$CropType;

    const/4 v2, 0x7

    aput-object v1, v0, v2

    sget-object v1, Lcom/cesards/cropimageview/CropImageView$CropType;->CENTER_BOTTOM:Lcom/cesards/cropimageview/CropImageView$CropType;

    const/16 v2, 0x8

    aput-object v1, v0, v2

    sput-object v0, Lcom/cesards/cropimageview/CropImageView$CropType;->$VALUES:[Lcom/cesards/cropimageview/CropImageView$CropType;

    .line 191
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/cesards/cropimageview/CropImageView$CropType;->codeLookup:Ljava/util/Map;

    .line 194
    invoke-static {}, Lcom/cesards/cropimageview/CropImageView$CropType;->values()[Lcom/cesards/cropimageview/CropImageView$CropType;

    move-result-object v4

    array-length v5, v4

    const/4 v6, 0x0

    :goto_a5
    if-ge v6, v5, :cond_b9

    aget-object v7, v4, v6

    .line 195
    .local v7, "ft":Lcom/cesards/cropimageview/CropImageView$CropType;
    sget-object v0, Lcom/cesards/cropimageview/CropImageView$CropType;->codeLookup:Ljava/util/Map;

    invoke-virtual {v7}, Lcom/cesards/cropimageview/CropImageView$CropType;->getCrop()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 194
    .end local v7    # "ft":Lcom/cesards/cropimageview/CropImageView$CropType;
    add-int/lit8 v6, v6, 0x1

    goto :goto_a5

    .line 197
    :cond_b9
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .registers 4
    .param p3, "ct"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 199
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 200
    iput p3, p0, Lcom/cesards/cropimageview/CropImageView$CropType;->cropType:I

    .line 201
    return-void
.end method

.method public static get(I)Lcom/cesards/cropimageview/CropImageView$CropType;
    .registers 3
    .param p0, "number"    # I

    .line 208
    sget-object v0, Lcom/cesards/cropimageview/CropImageView$CropType;->codeLookup:Ljava/util/Map;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cesards/cropimageview/CropImageView$CropType;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/cesards/cropimageview/CropImageView$CropType;
    .registers 2
    .param p0, "name"    # Ljava/lang/String;

    .line 177
    const-class v0, Lcom/cesards/cropimageview/CropImageView$CropType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/cesards/cropimageview/CropImageView$CropType;

    return-object v0
.end method

.method public static values()[Lcom/cesards/cropimageview/CropImageView$CropType;
    .registers 1

    .line 177
    sget-object v0, Lcom/cesards/cropimageview/CropImageView$CropType;->$VALUES:[Lcom/cesards/cropimageview/CropImageView$CropType;

    invoke-virtual {v0}, [Lcom/cesards/cropimageview/CropImageView$CropType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/cesards/cropimageview/CropImageView$CropType;

    return-object v0
.end method


# virtual methods
.method public getCrop()I
    .registers 2

    .line 204
    iget v0, p0, Lcom/cesards/cropimageview/CropImageView$CropType;->cropType:I

    return v0
.end method

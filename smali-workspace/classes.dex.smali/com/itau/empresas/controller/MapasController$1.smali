.class Lcom/itau/empresas/controller/MapasController$1;
.super Ljava/lang/Object;
.source "MapasController.java"

# interfaces
.implements Lbr/com/itau/sdk/android/core/type/BackendCall;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/itau/empresas/controller/MapasController;->buscaAgencias(Lcom/itau/empresas/api/model/FiltroAgenciaVO;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/itau/empresas/controller/MapasController;

.field final synthetic val$filtro:Lcom/itau/empresas/api/model/FiltroAgenciaVO;


# direct methods
.method constructor <init>(Lcom/itau/empresas/controller/MapasController;Lcom/itau/empresas/api/model/FiltroAgenciaVO;)V
    .registers 3
    .param p1, "this$0"    # Lcom/itau/empresas/controller/MapasController;

    .line 22
    iput-object p1, p0, Lcom/itau/empresas/controller/MapasController$1;->this$0:Lcom/itau/empresas/controller/MapasController;

    iput-object p2, p0, Lcom/itau/empresas/controller/MapasController$1;->val$filtro:Lcom/itau/empresas/api/model/FiltroAgenciaVO;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public callBackend()V
    .registers 5

    .line 25
    new-instance v2, Lcom/itau/empresas/Evento$EventoListaMapas;

    invoke-direct {v2}, Lcom/itau/empresas/Evento$EventoListaMapas;-><init>()V

    .line 28
    .local v2, "eventoRetorno":Lcom/itau/empresas/Evento$EventoListaMapas;
    const-wide/16 v0, 0x1388

    :try_start_7
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V

    .line 29
    iget-object v0, p0, Lcom/itau/empresas/controller/MapasController$1;->this$0:Lcom/itau/empresas/controller/MapasController;

    invoke-virtual {v0}, Lcom/itau/empresas/controller/MapasController;->api()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/api/Api;

    iget-object v1, p0, Lcom/itau/empresas/controller/MapasController$1;->val$filtro:Lcom/itau/empresas/api/model/FiltroAgenciaVO;

    invoke-interface {v0, v1}, Lcom/itau/empresas/api/Api;->buscaAgencias(Lcom/itau/empresas/api/model/FiltroAgenciaVO;)Ljava/lang/String;

    move-result-object v3

    .line 30
    .local v3, "resposta":Ljava/lang/String;
    iget-object v0, p0, Lcom/itau/empresas/controller/MapasController$1;->this$0:Lcom/itau/empresas/controller/MapasController;

    # invokes: Lcom/itau/empresas/controller/MapasController;->getReposta(Ljava/lang/String;)Ljava/util/List;
    invoke-static {v0, v3}, Lcom/itau/empresas/controller/MapasController;->access$000(Lcom/itau/empresas/controller/MapasController;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/itau/empresas/Evento$EventoListaMapas;->setAgencias(Ljava/util/List;)V

    .line 32
    invoke-static {}, Lcom/itau/empresas/Evento$EventoListaMapas;->getAgencias()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_2b

    .line 33
    invoke-static {v2}, Lcom/itau/empresas/controller/BaseController;->post(Ljava/lang/Object;)V

    goto :goto_33

    .line 35
    :cond_2b
    new-instance v0, Lcom/itau/empresas/Evento$ErroInesperadoApi;

    invoke-direct {v0}, Lcom/itau/empresas/Evento$ErroInesperadoApi;-><init>()V

    invoke-static {v0}, Lcom/itau/empresas/controller/BaseController;->post(Ljava/lang/Object;)V
    :try_end_33
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_33} :catch_34

    .line 41
    .end local v3    # "resposta":Ljava/lang/String;
    :goto_33
    goto :goto_40

    .line 38
    :catch_34
    move-exception v3

    .line 39
    .local v3, "e":Ljava/lang/Exception;
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V

    .line 40
    new-instance v0, Lcom/itau/empresas/Evento$ErroInesperadoApi;

    invoke-direct {v0}, Lcom/itau/empresas/Evento$ErroInesperadoApi;-><init>()V

    invoke-static {v0}, Lcom/itau/empresas/controller/BaseController;->post(Ljava/lang/Object;)V

    .line 42
    .end local v3    # "e":Ljava/lang/Exception;
    :goto_40
    return-void
.end method

.class public Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;
.super Ljava/lang/Object;
.source "EventoAnalytics.java"


# instance fields
.field private acao:Ljava/lang/String;

.field private categoria:Ljava/lang/String;

.field private label:Ljava/lang/String;

.field private valor:Ljava/lang/Long;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .registers 4
    .param p1, "categoria"    # Ljava/lang/String;
    .param p2, "acao"    # Ljava/lang/String;

    .line 11
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 12
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 5
    .param p1, "categoria"    # Ljava/lang/String;
    .param p2, "acao"    # Ljava/lang/String;
    .param p3, "label"    # Ljava/lang/String;

    .line 15
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    .line 16
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V
    .registers 5
    .param p1, "categoria"    # Ljava/lang/String;
    .param p2, "acao"    # Ljava/lang/String;
    .param p3, "label"    # Ljava/lang/String;
    .param p4, "valor"    # Ljava/lang/Long;

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-object p1, p0, Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;->categoria:Ljava/lang/String;

    .line 20
    iput-object p2, p0, Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;->acao:Ljava/lang/String;

    .line 21
    iput-object p3, p0, Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;->label:Ljava/lang/String;

    .line 22
    iput-object p4, p0, Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;->valor:Ljava/lang/Long;

    .line 23
    return-void
.end method

.method private formatarGA(Ljava/lang/String;)Ljava/lang/String;
    .registers 5
    .param p1, "s"    # Ljava/lang/String;

    .line 57
    if-nez p1, :cond_4

    .line 58
    const/4 v0, 0x0

    return-object v0

    .line 60
    :cond_4
    sget-object v0, Ljava/text/Normalizer$Form;->NFD:Ljava/text/Normalizer$Form;

    invoke-static {p1, v0}, Ljava/text/Normalizer;->normalize(Ljava/lang/CharSequence;Ljava/text/Normalizer$Form;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "[\\p{InCombiningDiacriticalMarks}]"

    const-string v2, ""

    .line 61
    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 62
    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, " "

    const-string v2, "-"

    .line 63
    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 60
    return-object v0
.end method


# virtual methods
.method public ajustarFormatacao()Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;
    .registers 2

    .line 42
    iget-object v0, p0, Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;->categoria:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;->formatarGA(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;->categoria:Ljava/lang/String;

    .line 43
    iget-object v0, p0, Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;->acao:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;->formatarGA(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;->acao:Ljava/lang/String;

    .line 44
    iget-object v0, p0, Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;->label:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;->formatarGA(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;->label:Ljava/lang/String;

    .line 46
    return-object p0
.end method

.method public getAcao()Ljava/lang/String;
    .registers 2

    .line 30
    iget-object v0, p0, Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;->acao:Ljava/lang/String;

    return-object v0
.end method

.method public getCategoria()Ljava/lang/String;
    .registers 2

    .line 26
    iget-object v0, p0, Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;->categoria:Ljava/lang/String;

    return-object v0
.end method

.method public getLabel()Ljava/lang/String;
    .registers 2

    .line 34
    iget-object v0, p0, Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;->label:Ljava/lang/String;

    return-object v0
.end method

.method public getValor()Ljava/lang/Long;
    .registers 2

    .line 38
    iget-object v0, p0, Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;->valor:Ljava/lang/Long;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    .line 68
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "EventoGA{categoria=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;->categoria:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", acao=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;->acao:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", label=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;->label:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", valor=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;->valor:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

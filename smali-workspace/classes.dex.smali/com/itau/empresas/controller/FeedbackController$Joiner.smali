.class final Lcom/itau/empresas/controller/FeedbackController$Joiner;
.super Ljava/lang/Object;
.source "FeedbackController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/itau/empresas/controller/FeedbackController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "Joiner"
.end annotation


# instance fields
.field private separator:Ljava/lang/String;


# direct methods
.method private constructor <init>(Ljava/lang/String;)V
    .registers 2
    .param p1, "separator"    # Ljava/lang/String;

    .line 115
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 116
    iput-object p1, p0, Lcom/itau/empresas/controller/FeedbackController$Joiner;->separator:Ljava/lang/String;

    .line 117
    return-void
.end method

.method static on(Ljava/lang/Character;)Lcom/itau/empresas/controller/FeedbackController$Joiner;
    .registers 3
    .param p0, "separator"    # Ljava/lang/Character;

    .line 120
    new-instance v0, Lcom/itau/empresas/controller/FeedbackController$Joiner;

    invoke-virtual {p0}, Ljava/lang/Character;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/itau/empresas/controller/FeedbackController$Joiner;-><init>(Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method join([Ljava/lang/String;)Ljava/lang/String;
    .registers 6
    .param p1, "list"    # [Ljava/lang/String;

    .line 128
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 129
    .local v2, "builder":Ljava/lang/StringBuilder;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_6
    array-length v0, p1

    if-ge v3, v0, :cond_1b

    .line 130
    aget-object v0, p1, v3

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 131
    array-length v0, p1

    add-int/lit8 v0, v0, -0x1

    if-eq v3, v0, :cond_18

    .line 132
    iget-object v0, p0, Lcom/itau/empresas/controller/FeedbackController$Joiner;->separator:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 129
    :cond_18
    add-int/lit8 v3, v3, 0x1

    goto :goto_6

    .line 135
    .end local v3    # "i":I
    :cond_1b
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

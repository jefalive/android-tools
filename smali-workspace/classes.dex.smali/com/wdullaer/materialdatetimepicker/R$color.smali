.class public final Lcom/wdullaer/materialdatetimepicker/R$color;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/wdullaer/materialdatetimepicker/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "color"
.end annotation


# static fields
.field public static final mdtp_accent_color:I = 0x7f0c00e1

.field public static final mdtp_accent_color_dark:I = 0x7f0c00e2

.field public static final mdtp_accent_color_focused:I = 0x7f0c00e3

.field public static final mdtp_ampm_text_color:I = 0x7f0c00e4

.field public static final mdtp_background_color:I = 0x7f0c00e5

.field public static final mdtp_button_color:I = 0x7f0c00e6

.field public static final mdtp_button_selected:I = 0x7f0c00e7

.field public static final mdtp_calendar_header:I = 0x7f0c00e8

.field public static final mdtp_calendar_selected_date_text:I = 0x7f0c00e9

.field public static final mdtp_circle_background:I = 0x7f0c00ea

.field public static final mdtp_circle_background_dark_theme:I = 0x7f0c00eb

.field public static final mdtp_circle_color:I = 0x7f0c00ec

.field public static final mdtp_dark_gray:I = 0x7f0c00ed

.field public static final mdtp_date_picker_month_day:I = 0x7f0c00ee

.field public static final mdtp_date_picker_month_day_dark_theme:I = 0x7f0c00ef

.field public static final mdtp_date_picker_selector:I = 0x7f0c0186

.field public static final mdtp_date_picker_text_disabled:I = 0x7f0c00f0

.field public static final mdtp_date_picker_text_disabled_dark_theme:I = 0x7f0c00f1

.field public static final mdtp_date_picker_text_highlighted:I = 0x7f0c00f2

.field public static final mdtp_date_picker_text_highlighted_dark_theme:I = 0x7f0c00f3

.field public static final mdtp_date_picker_text_normal:I = 0x7f0c00f4

.field public static final mdtp_date_picker_text_normal_dark_theme:I = 0x7f0c00f5

.field public static final mdtp_date_picker_view_animator:I = 0x7f0c00f6

.field public static final mdtp_date_picker_view_animator_dark_theme:I = 0x7f0c00f7

.field public static final mdtp_date_picker_year_selector:I = 0x7f0c0187

.field public static final mdtp_done_disabled_dark:I = 0x7f0c00f8

.field public static final mdtp_done_text_color:I = 0x7f0c0188

.field public static final mdtp_done_text_color_dark:I = 0x7f0c0189

.field public static final mdtp_done_text_color_dark_disabled:I = 0x7f0c00f9

.field public static final mdtp_done_text_color_dark_normal:I = 0x7f0c00fa

.field public static final mdtp_done_text_color_disabled:I = 0x7f0c00fb

.field public static final mdtp_done_text_color_normal:I = 0x7f0c00fc

.field public static final mdtp_light_gray:I = 0x7f0c00fd

.field public static final mdtp_line_background:I = 0x7f0c00fe

.field public static final mdtp_line_dark:I = 0x7f0c00ff

.field public static final mdtp_neutral_pressed:I = 0x7f0c0100

.field public static final mdtp_numbers_text_color:I = 0x7f0c0101

.field public static final mdtp_red:I = 0x7f0c0102

.field public static final mdtp_red_focused:I = 0x7f0c0103

.field public static final mdtp_transparent_black:I = 0x7f0c0104

.field public static final mdtp_white:I = 0x7f0c0105


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.class public Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity;
.super Lcom/itau/empresas/ui/activity/BaseActivity;
.source "PagamentosComCodigoBarraActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/itau/empresas/ui/util/SnackbarUtils$Gerenciador;


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field app:Lcom/itau/empresas/CustomApplication;

.field botaoLerArquivo:Landroid/widget/ImageButton;

.field botaoPagamentosContinuar:Landroid/widget/Button;

.field campoCodigoBarras:Lbr/com/itau/widgets/material/MaterialEditText;

.field private fechouHintComClique:Z

.field private hintUtils:Lcom/itau/empresas/ui/util/HintUtils;

.field progress:Landroid/app/ProgressDialog;

.field root:Landroid/widget/RelativeLayout;

.field toolbar:Landroid/support/v7/widget/Toolbar;

.field private topSnackbar:Lcom/itau/empresas/ui/util/SnackbarUtils$Gerenciador;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .line 77
    const-class v0, Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .registers 1

    .line 74
    invoke-direct {p0}, Lcom/itau/empresas/ui/activity/BaseActivity;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity;)Z
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity;

    .line 74
    iget-boolean v0, p0, Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity;->fechouHintComClique:Z

    return v0
.end method

.method static synthetic access$002(Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity;Z)Z
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity;
    .param p1, "x1"    # Z

    .line 74
    iput-boolean p1, p0, Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity;->fechouHintComClique:Z

    return p1
.end method

.method static synthetic access$100(Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity;)Lcom/itau/empresas/ui/util/HintUtils;
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity;

    .line 74
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity;->hintUtils:Lcom/itau/empresas/ui/util/HintUtils;

    return-object v0
.end method

.method static synthetic access$200(Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity;Ljava/lang/String;Lbr/com/itau/widgets/material/MaterialEditText;)V
    .registers 3
    .param p0, "x0"    # Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Lbr/com/itau/widgets/material/MaterialEditText;

    .line 74
    invoke-direct {p0, p1, p2}, Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity;->campoInvalido(Ljava/lang/String;Lbr/com/itau/widgets/material/MaterialEditText;)V

    return-void
.end method

.method static synthetic access$300(Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity;Lbr/com/itau/widgets/material/MaterialEditText;)V
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity;
    .param p1, "x1"    # Lbr/com/itau/widgets/material/MaterialEditText;

    .line 74
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity;->campoParcialmenteInvalido(Lbr/com/itau/widgets/material/MaterialEditText;)V

    return-void
.end method

.method static synthetic access$400(Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity;Lbr/com/itau/widgets/material/MaterialEditText;)V
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity;
    .param p1, "x1"    # Lbr/com/itau/widgets/material/MaterialEditText;

    .line 74
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity;->campoTotalmenteValido(Lbr/com/itau/widgets/material/MaterialEditText;)V

    return-void
.end method

.method static synthetic access$500(Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity;Landroid/net/Uri;Landroid/os/Handler;)V
    .registers 3
    .param p0, "x0"    # Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity;
    .param p1, "x1"    # Landroid/net/Uri;
    .param p2, "x2"    # Landroid/os/Handler;

    .line 74
    invoke-direct {p0, p1, p2}, Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity;->preencheBoleto(Landroid/net/Uri;Landroid/os/Handler;)V

    return-void
.end method

.method private adicionaHint(Landroid/view/View;)V
    .registers 5
    .param p1, "view"    # Landroid/view/View;

    .line 198
    new-instance v2, Lbr/com/itau/widgets/hintview/Hint$Builder;

    const v0, 0x7f09011b

    invoke-direct {v2, p1, v0}, Lbr/com/itau/widgets/hintview/Hint$Builder;-><init>(Landroid/view/View;I)V

    .line 199
    .line 200
    .local v2, "builder":Lbr/com/itau/widgets/hintview/Hint$Builder;
    const v0, 0x7f070358

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 199
    const/16 v1, 0x50

    invoke-static {v2, v0, v1}, Lcom/itau/empresas/ui/util/HintUtils;->montaHintAlerta(Lbr/com/itau/widgets/hintview/Hint$Builder;Ljava/lang/String;I)Lbr/com/itau/widgets/hintview/Hint$Builder;

    move-result-object v2

    .line 202
    new-instance v0, Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity$1;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity$1;-><init>(Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity;)V

    invoke-virtual {v2, v0}, Lbr/com/itau/widgets/hintview/Hint$Builder;->setOnClickListener(Lbr/com/itau/widgets/hintview/OnClickListener;)Lbr/com/itau/widgets/hintview/Hint$Builder;

    .line 211
    new-instance v0, Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity$2;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity$2;-><init>(Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity;)V

    invoke-virtual {v2, v0}, Lbr/com/itau/widgets/hintview/Hint$Builder;->setOnDismissListener(Lbr/com/itau/widgets/hintview/OnDismissListener;)Lbr/com/itau/widgets/hintview/Hint$Builder;

    .line 230
    invoke-virtual {v2}, Lbr/com/itau/widgets/hintview/Hint$Builder;->show()Lbr/com/itau/widgets/hintview/Hint;

    .line 231
    return-void
.end method

.method private campoInvalido(Ljava/lang/String;Lbr/com/itau/widgets/material/MaterialEditText;)V
    .registers 5
    .param p1, "mensagem"    # Ljava/lang/String;
    .param p2, "campo"    # Lbr/com/itau/widgets/material/MaterialEditText;

    .line 340
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity;->botaoPagamentosContinuar:Landroid/widget/Button;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 341
    if-eqz p1, :cond_15

    .line 342
    invoke-virtual {p2, p1}, Lbr/com/itau/widgets/material/MaterialEditText;->setError(Ljava/lang/CharSequence;)V

    .line 343
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity;->campoCodigoBarras:Lbr/com/itau/widgets/material/MaterialEditText;

    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbr/com/itau/widgets/material/MaterialEditText;->setError(Ljava/lang/CharSequence;)V

    .line 345
    :cond_15
    return-void
.end method

.method private campoParcialmenteInvalido(Lbr/com/itau/widgets/material/MaterialEditText;)V
    .registers 4
    .param p1, "campo"    # Lbr/com/itau/widgets/material/MaterialEditText;

    .line 335
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lbr/com/itau/widgets/material/MaterialEditText;->setError(Ljava/lang/CharSequence;)V

    .line 336
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity;->botaoPagamentosContinuar:Landroid/widget/Button;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 337
    return-void
.end method

.method private campoTotalmenteValido(Lbr/com/itau/widgets/material/MaterialEditText;)V
    .registers 4
    .param p1, "campo"    # Lbr/com/itau/widgets/material/MaterialEditText;

    .line 329
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lbr/com/itau/widgets/material/MaterialEditText;->setError(Ljava/lang/CharSequence;)V

    .line 330
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity;->botaoPagamentosContinuar:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 331
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity;->botaoPagamentosContinuar:Landroid/widget/Button;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 332
    return-void
.end method

.method private constroiToolbar()V
    .registers 3

    .line 111
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity;->toolbar:Landroid/support/v7/widget/Toolbar;

    invoke-static {v0, p0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom;->Builder(Landroid/support/v7/widget/Toolbar;Landroid/support/v7/app/AppCompatActivity;)Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    move-result-object v0

    .line 112
    const v1, 0x7f0706b6

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->comTitulo(I)Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    move-result-object v0

    .line 113
    invoke-virtual {v0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->comClickListenerFinish()Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    move-result-object v0

    .line 114
    invoke-virtual {v0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->build()Lcom/itau/empresas/ui/util/listener/ToolbarCustom;

    .line 115
    return-void
.end method

.method private disparaEventoAnalytics(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 5
    .param p1, "nome"    # Ljava/lang/String;
    .param p2, "secao"    # Ljava/lang/String;
    .param p3, "valor"    # Ljava/lang/String;

    .line 423
    invoke-static {}, Lcom/itau/empresas/feature/analytics/AnalyticsHelper;->getInstance()Lcom/itau/empresas/feature/analytics/AnalyticsHelper;

    move-result-object v0

    .line 424
    .local v0, "analyticsHelper":Lcom/itau/empresas/feature/analytics/AnalyticsHelper;
    invoke-virtual {v0, p1, p2, p3}, Lcom/itau/empresas/feature/analytics/AnalyticsHelper;->eventoAdobeState(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 425
    return-void
.end method

.method private exibeLeitorBoleto()V
    .registers 4

    .line 248
    invoke-virtual {p0}, Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity;->temPermissaoPagamento()Z

    move-result v0

    if-nez v0, :cond_7

    .line 249
    return-void

    .line 252
    :cond_7
    const v0, 0x7f0702d3

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 253
    const v1, 0x7f070311

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 252
    invoke-virtual {p0, v0, v1}, Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity;->analyticsHit(Ljava/lang/String;Ljava/lang/String;)V

    .line 255
    const v0, 0x7f0700fd

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 256
    const v1, 0x7f0700d1

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 257
    const v2, 0x7f0700c3

    invoke-virtual {p0, v2}, Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 255
    invoke-direct {p0, v0, v1, v2}, Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity;->disparaEventoAnalytics(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 259
    invoke-static {p0}, Lcom/itau/empresas/ui/activity/LeitorBoletoActivity_;->intent(Landroid/content/Context;)Lcom/itau/empresas/ui/activity/LeitorBoletoActivity_$IntentBuilder_;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/ui/activity/LeitorBoletoActivity_$IntentBuilder_;->get()Landroid/content/Intent;

    move-result-object v0

    const v1, 0xc0de

    invoke-virtual {p0, v0, v1}, Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 261
    return-void
.end method

.method private geraParaCampo(Lbr/com/itau/widgets/material/MaterialEditText;)Lbr/com/concretesolutions/canarinho/watcher/evento/EventoDeValidacao;
    .registers 3
    .param p1, "campo"    # Lbr/com/itau/widgets/material/MaterialEditText;

    .line 310
    new-instance v0, Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity$4;

    invoke-direct {v0, p0, p1}, Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity$4;-><init>(Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity;Lbr/com/itau/widgets/material/MaterialEditText;)V

    return-object v0
.end method

.method public static intent(Landroid/content/Context;)Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity_$IntentBuilder_;
    .registers 2
    .param p0, "context"    # Landroid/content/Context;

    .line 419
    new-instance v0, Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity_$IntentBuilder_;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity_$IntentBuilder_;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method private preencheBoleto(Landroid/net/Uri;Landroid/os/Handler;)V
    .registers 7
    .param p1, "file"    # Landroid/net/Uri;
    .param p2, "handler"    # Landroid/os/Handler;

    .line 378
    :try_start_0
    invoke-static {p0, p1}, Lcom/itau/empresas/BoletoUtils;->lerBoletoPdf(Landroid/content/Context;Landroid/net/Uri;)Lcom/itau/empresas/BoletoUtils$Boleto;

    move-result-object v3

    .line 379
    .local v3, "boleto":Lcom/itau/empresas/BoletoUtils$Boleto;
    new-instance v0, Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity$6;

    invoke-direct {v0, p0, v3}, Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity$6;-><init>(Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity;Lcom/itau/empresas/BoletoUtils$Boleto;)V

    invoke-virtual {p2, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_c} :catch_d

    .line 402
    .end local v3    # "boleto":Lcom/itau/empresas/BoletoUtils$Boleto;
    goto :goto_2f

    .line 396
    :catch_d
    move-exception v3

    .line 397
    .local v3, "e":Ljava/io/IOException;
    sget-object v0, Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity;->TAG:Ljava/lang/String;

    const-string v1, "Erro ao abrir PDF"

    invoke-static {v0, v1, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 398
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity;->progress:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 399
    const v0, 0x7f0700fb

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 400
    const v1, 0x7f0700bb

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 401
    invoke-virtual {v3}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v2

    .line 399
    invoke-direct {p0, v0, v1, v2}, Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity;->disparaEventoAnalytics(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 403
    .end local v3    # "e":Ljava/io/IOException;
    :goto_2f
    return-void
.end method


# virtual methods
.method public abrirFileExplorer()V
    .registers 3

    .line 176
    const-string v0, "application/pdf"

    invoke-static {p0, v0}, Lcom/itau/empresas/ui/util/ViewUtils;->abrirFileExplorerIntent(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 177
    .local v1, "i":Landroid/content/Intent;
    if-eqz v1, :cond_e

    .line 178
    const/16 v0, 0x400

    invoke-virtual {p0, v1, v0}, Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_11

    .line 180
    :cond_e
    invoke-virtual {p0}, Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity;->dismissSnackbar()V

    .line 181
    :goto_11
    return-void
.end method

.method aoClicarContinuar()V
    .registers 3

    .line 265
    invoke-virtual {p0}, Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity;->temPermissaoPagamento()Z

    move-result v0

    if-nez v0, :cond_7

    .line 266
    return-void

    .line 269
    :cond_7
    const v0, 0x7f0702d3

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 270
    const v1, 0x7f070302

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 269
    invoke-virtual {p0, v0, v1}, Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity;->analyticsHit(Ljava/lang/String;Ljava/lang/String;)V

    .line 271
    invoke-static {p0}, Lcom/itau/empresas/ui/activity/PagamentosWebViewActivity;->intent(Landroid/content/Context;)Lcom/itau/empresas/ui/activity/PagamentosWebViewActivity_$IntentBuilder_;

    move-result-object v0

    const-string v1, "NET_PAG_UNIF_PLUS"

    .line 272
    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/activity/PagamentosWebViewActivity_$IntentBuilder_;->chaveMobile(Ljava/lang/String;)Lcom/itau/empresas/ui/activity/PagamentosWebViewActivity_$IntentBuilder_;

    move-result-object v0

    .line 273
    const v1, 0x7f0706b6

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/activity/PagamentosWebViewActivity_$IntentBuilder_;->nomeMenu(Ljava/lang/String;)Lcom/itau/empresas/ui/activity/PagamentosWebViewActivity_$IntentBuilder_;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity;->campoCodigoBarras:Lbr/com/itau/widgets/material/MaterialEditText;

    .line 274
    invoke-virtual {v1}, Lbr/com/itau/widgets/material/MaterialEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/activity/PagamentosWebViewActivity_$IntentBuilder_;->codigoBarra(Ljava/lang/String;)Lcom/itau/empresas/ui/activity/PagamentosWebViewActivity_$IntentBuilder_;

    move-result-object v0

    .line 275
    invoke-virtual {v0}, Lcom/itau/empresas/ui/activity/PagamentosWebViewActivity_$IntentBuilder_;->start()Lorg/androidannotations/api/builder/PostActivityStarter;

    .line 276
    return-void
.end method

.method aoClicarLeitorBoleto()V
    .registers 3

    .line 236
    invoke-static {}, Lcom/itau/empresas/ui/util/PermissoesUtil;->getPermissionShowCamera()[Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/itau/empresas/ui/util/PermissoesUtil;->hasSelfPermissions(Landroid/content/Context;[Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 237
    invoke-direct {p0}, Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity;->exibeLeitorBoleto()V

    goto :goto_1c

    .line 239
    :cond_e
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x17

    if-lt v0, v1, :cond_1c

    .line 240
    invoke-static {}, Lcom/itau/empresas/ui/util/PermissoesUtil;->getPermissionShowCamera()[Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity;->requestPermissions([Ljava/lang/String;I)V

    .line 245
    :cond_1c
    :goto_1c
    return-void
.end method

.method aoClicarLerDoArquivo()V
    .registers 3

    .line 165
    invoke-virtual {p0}, Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity;->temPermissaoPagamento()Z

    move-result v0

    if-nez v0, :cond_7

    .line 166
    return-void

    .line 169
    :cond_7
    const v0, 0x7f0702d3

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 170
    const v1, 0x7f070312

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 169
    invoke-virtual {p0, v0, v1}, Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity;->analyticsHit(Ljava/lang/String;Ljava/lang/String;)V

    .line 172
    invoke-virtual {p0}, Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity;->abrirFileExplorer()V

    .line 173
    return-void
.end method

.method carregandoElementosDeTela()V
    .registers 4

    .line 99
    new-instance v0, Lcom/itau/empresas/ui/util/SnackbarUtils$GerenciadorDelegate;

    iget-object v1, p0, Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity;->root:Landroid/widget/RelativeLayout;

    invoke-direct {v0, v1}, Lcom/itau/empresas/ui/util/SnackbarUtils$GerenciadorDelegate;-><init>(Landroid/view/ViewGroup;)V

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity;->topSnackbar:Lcom/itau/empresas/ui/util/SnackbarUtils$Gerenciador;

    .line 100
    invoke-direct {p0}, Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity;->constroiToolbar()V

    .line 101
    invoke-virtual {p0}, Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity;->iniciaComponentes()V

    .line 103
    const v0, 0x7f0700fb

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 104
    const v1, 0x7f0700d1

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 105
    const v2, 0x7f0700c3

    invoke-virtual {p0, v2}, Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 103
    invoke-direct {p0, v0, v1, v2}, Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity;->disparaEventoAnalytics(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 107
    invoke-virtual {p0}, Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity;->aoClicarLeitorBoleto()V

    .line 108
    return-void
.end method

.method public dismissSnackbar()V
    .registers 2

    .line 411
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity;->topSnackbar:Lcom/itau/empresas/ui/util/SnackbarUtils$Gerenciador;

    invoke-interface {v0}, Lcom/itau/empresas/ui/util/SnackbarUtils$Gerenciador;->dismissSnackbar()V

    .line 412
    return-void
.end method

.method public iniciaComponentes()V
    .registers 5

    .line 184
    new-instance v3, Lcom/itau/empresas/ui/util/listener/TextWatcherWrapper;

    new-instance v0, Lbr/com/concretesolutions/canarinho/watcher/BoletoBancarioTextWatcher;

    iget-object v1, p0, Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity;->campoCodigoBarras:Lbr/com/itau/widgets/material/MaterialEditText;

    .line 185
    invoke-direct {p0, v1}, Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity;->geraParaCampo(Lbr/com/itau/widgets/material/MaterialEditText;)Lbr/com/concretesolutions/canarinho/watcher/evento/EventoDeValidacao;

    move-result-object v1

    invoke-direct {v0, v1}, Lbr/com/concretesolutions/canarinho/watcher/BoletoBancarioTextWatcher;-><init>(Lbr/com/concretesolutions/canarinho/watcher/evento/EventoDeValidacao;)V

    invoke-direct {v3, v0}, Lcom/itau/empresas/ui/util/listener/TextWatcherWrapper;-><init>(Landroid/text/TextWatcher;)V

    .line 186
    .local v3, "textWatcher":Landroid/text/TextWatcher;
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity;->campoCodigoBarras:Lbr/com/itau/widgets/material/MaterialEditText;

    invoke-virtual {v0, v3}, Lbr/com/itau/widgets/material/MaterialEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 188
    new-instance v0, Lcom/itau/empresas/ui/util/HintUtils;

    new-instance v1, Lbr/com/itau/security/securestorage/SecureStorage;

    const-string v2, "PREMP"

    invoke-direct {v1, p0, v2}, Lbr/com/itau/security/securestorage/SecureStorage;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    invoke-direct {v0, v1}, Lcom/itau/empresas/ui/util/HintUtils;-><init>(Landroid/content/SharedPreferences;)V

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity;->hintUtils:Lcom/itau/empresas/ui/util/HintUtils;

    .line 190
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity;->hintUtils:Lcom/itau/empresas/ui/util/HintUtils;

    const-string v1, "CHVPAG-ID"

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/HintUtils;->exibeHint(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_32

    .line 191
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity;->botaoLerArquivo:Landroid/widget/ImageButton;

    invoke-direct {p0, v0}, Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity;->adicionaHint(Landroid/view/View;)V

    .line 194
    :cond_32
    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .registers 9
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .line 119
    const/16 v0, 0x400

    if-ne p1, v0, :cond_f

    .line 120
    const/4 v0, -0x1

    if-ne p2, v0, :cond_4c

    .line 122
    invoke-virtual {p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v4

    .line 123
    .local v4, "uri":Landroid/net/Uri;
    invoke-virtual {p0, v4}, Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity;->preencherBoletoDoPdfHelper(Landroid/net/Uri;)V

    .line 124
    .end local v4    # "uri":Landroid/net/Uri;
    goto :goto_4c

    .line 125
    :cond_f
    const v0, 0xc0de

    if-ne p1, v0, :cond_4c

    .line 126
    .line 127
    invoke-static {p1, p2, p3}, Lcom/google/zxing/integration/android/IntentIntegrator;->parseActivityResult(IILandroid/content/Intent;)Lcom/google/zxing/integration/android/IntentResult;

    move-result-object v4

    .line 128
    .local v4, "result":Lcom/google/zxing/integration/android/IntentResult;
    if-nez v4, :cond_1b

    .line 129
    return-void

    .line 132
    :cond_1b
    invoke-virtual {v4}, Lcom/google/zxing/integration/android/IntentResult;->getContents()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_4c

    .line 134
    invoke-virtual {v4}, Lcom/google/zxing/integration/android/IntentResult;->getContents()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v1, 0x2c

    if-eq v0, v1, :cond_37

    .line 135
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity;->campoCodigoBarras:Lbr/com/itau/widgets/material/MaterialEditText;

    invoke-virtual {v4}, Lcom/google/zxing/integration/android/IntentResult;->getContents()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbr/com/itau/widgets/material/MaterialEditText;->setText(Ljava/lang/CharSequence;)V

    goto :goto_4c

    .line 137
    :cond_37
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity;->campoCodigoBarras:Lbr/com/itau/widgets/material/MaterialEditText;

    sget-object v1, Lbr/com/concretesolutions/canarinho/formatador/Formatador;->BOLETO:Lbr/com/concretesolutions/canarinho/formatador/Formatador;

    sget-object v2, Lbr/com/concretesolutions/canarinho/formatador/Formatador;->LINHA_DIGITAVEL:Lbr/com/concretesolutions/canarinho/formatador/Formatador;

    .line 139
    invoke-virtual {v4}, Lcom/google/zxing/integration/android/IntentResult;->getContents()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lbr/com/concretesolutions/canarinho/formatador/Formatador;->formata(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 138
    invoke-interface {v1, v2}, Lbr/com/concretesolutions/canarinho/formatador/Formatador;->formata(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 137
    invoke-virtual {v0, v1}, Lbr/com/itau/widgets/material/MaterialEditText;->setText(Ljava/lang/CharSequence;)V

    .line 143
    .end local v4    # "result":Lcom/google/zxing/integration/android/IntentResult;
    :cond_4c
    :goto_4c
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .registers 2
    .param p1, "v"    # Landroid/view/View;

    .line 407
    invoke-virtual {p0}, Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity;->onBackPressed()V

    .line 408
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .registers 3
    .param p1, "menu"    # Landroid/view/Menu;

    .line 152
    invoke-static {p0, p1}, Lcom/itau/empresas/ui/util/PoyntUtils;->onCreateOptionsMenu(Landroid/app/Activity;Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method public onEventMainThread(Lcom/itau/empresas/Evento$PermissaoCamera;)V
    .registers 2
    .param p1, "permissao"    # Lcom/itau/empresas/Evento$PermissaoCamera;

    .line 280
    invoke-direct {p0}, Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity;->exibeLeitorBoleto()V

    .line 281
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .registers 3
    .param p1, "item"    # Landroid/view/MenuItem;

    .line 157
    invoke-static {p0, p1}, Lcom/itau/empresas/ui/util/PoyntUtils;->onOptionsItemSelected(Lcom/itau/empresas/ui/activity/BaseActivity;Landroid/view/MenuItem;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 158
    const/4 v0, 0x1

    return v0

    .line 160
    :cond_8
    invoke-super {p0, p1}, Lcom/itau/empresas/ui/activity/BaseActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0
.end method

.method public opkeysConsumidas()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Ljava/lang/String;>;"
        }
    .end annotation

    .line 147
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public preencherBoletoDoPdfHelper(Landroid/net/Uri;)V
    .registers 7
    .param p1, "filePath"    # Landroid/net/Uri;

    .line 354
    new-instance v3, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {v3, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 355
    .local v3, "handler":Landroid/os/Handler;
    move-object v4, p1

    .line 357
    .local v4, "file":Landroid/net/Uri;
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity;->progress:Landroid/app/ProgressDialog;

    .line 358
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity;->progress:Landroid/app/ProgressDialog;

    invoke-virtual {p0}, Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0704ab

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 359
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity;->progress:Landroid/app/ProgressDialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    .line 360
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity;->progress:Landroid/app/ProgressDialog;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 361
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity;->progress:Landroid/app/ProgressDialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setProgress(I)V

    .line 362
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity;->progress:Landroid/app/ProgressDialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 363
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity;->progress:Landroid/app/ProgressDialog;

    .line 365
    const v1, 0x7f0201c7

    invoke-static {p0, v1}, Landroid/support/v4/content/ContextCompat;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 364
    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setIndeterminateDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 366
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity;->progress:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 368
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity$5;

    invoke-direct {v1, p0, v4, v3}, Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity$5;-><init>(Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity;Landroid/net/Uri;Landroid/os/Handler;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 373
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 374
    return-void
.end method

.method public showSnackbar(Ljava/lang/CharSequence;I)V
    .registers 4
    .param p1, "texto"    # Ljava/lang/CharSequence;
    .param p2, "duracao"    # I

    .line 415
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity;->topSnackbar:Lcom/itau/empresas/ui/util/SnackbarUtils$Gerenciador;

    invoke-interface {v0, p1, p2}, Lcom/itau/empresas/ui/util/SnackbarUtils$Gerenciador;->showSnackbar(Ljava/lang/CharSequence;I)V

    .line 416
    return-void
.end method

.method public temPermissaoPagamento()Z
    .registers 4

    .line 284
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity;->app:Lcom/itau/empresas/CustomApplication;

    sget-object v1, Lcom/itau/empresas/CustomApplication$Exibicao;->EXIBICAO_WIDGET_LEITOR_CODIGO_BARRAS:Lcom/itau/empresas/CustomApplication$Exibicao;

    .line 285
    invoke-virtual {v0, v1}, Lcom/itau/empresas/CustomApplication;->verificarPermissao(Lcom/itau/empresas/CustomApplication$Exibicao;)Z

    move-result v0

    if-nez v0, :cond_45

    .line 287
    const v0, 0x7f0702d3

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 288
    const v1, 0x7f07031e

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 287
    invoke-virtual {p0, v0, v1}, Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity;->analyticsHit(Ljava/lang/String;Ljava/lang/String;)V

    .line 289
    new-instance v0, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;

    invoke-direct {v0, p0}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget-object v1, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Estados;->UM_BOTAO:Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Estados;

    .line 291
    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;->estado(Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Estados;)Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;

    move-result-object v0

    .line 292
    const v1, 0x7f070727

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;->mensagem(I)Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;

    move-result-object v0

    .line 293
    const v1, 0x7f07048b

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;->textoNeutro(I)Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;

    move-result-object v0

    .line 294
    invoke-virtual {v0}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;->build()Lcom/itau/empresas/ui/dialog/CustomAlertDialog;

    move-result-object v2

    .line 295
    .local v2, "dialog":Lcom/itau/empresas/ui/dialog/CustomAlertDialog;
    new-instance v0, Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity$3;

    invoke-direct {v0, p0, v2}, Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity$3;-><init>(Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity;Lcom/itau/empresas/ui/dialog/CustomAlertDialog;)V

    invoke-virtual {v2, v0}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog;->setNeutroListener(Landroid/view/View$OnClickListener;)V

    .line 302
    invoke-virtual {v2, p0}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog;->show(Landroid/support/v7/app/AppCompatActivity;)Lcom/itau/empresas/ui/dialog/BaseDialog;

    .line 303
    const/4 v0, 0x0

    return v0

    .line 306
    .end local v2    # "dialog":Lcom/itau/empresas/ui/dialog/CustomAlertDialog;
    :cond_45
    const/4 v0, 0x1

    return v0
.end method

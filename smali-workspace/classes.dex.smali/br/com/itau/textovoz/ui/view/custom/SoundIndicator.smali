.class public Lbr/com/itau/textovoz/ui/view/custom/SoundIndicator;
.super Landroid/view/View;
.source "SoundIndicator.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private circlePaint:Landroid/graphics/Paint;

.field private circlePosition:Landroid/graphics/Point;

.field private maxRadius:F

.field private minRadius:F

.field private minRadiusCircle:F

.field private soundLevel:F


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .line 15
    const-class v0, Lbr/com/itau/textovoz/ui/view/custom/SoundIndicator;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lbr/com/itau/textovoz/ui/view/custom/SoundIndicator;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .line 24
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 19
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lbr/com/itau/textovoz/ui/view/custom/SoundIndicator;->soundLevel:F

    .line 20
    const/high16 v0, 0x42200000    # 40.0f

    iput v0, p0, Lbr/com/itau/textovoz/ui/view/custom/SoundIndicator;->minRadius:F

    .line 21
    const/high16 v0, 0x41f00000    # 30.0f

    iput v0, p0, Lbr/com/itau/textovoz/ui/view/custom/SoundIndicator;->minRadiusCircle:F

    .line 25
    invoke-direct {p0}, Lbr/com/itau/textovoz/ui/view/custom/SoundIndicator;->init()V

    .line 26
    return-void
.end method

.method private init()V
    .registers 3

    .line 29
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lbr/com/itau/textovoz/ui/view/custom/SoundIndicator;->circlePaint:Landroid/graphics/Paint;

    .line 30
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/view/custom/SoundIndicator;->circlePaint:Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 31
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/view/custom/SoundIndicator;->circlePaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 32
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/view/custom/SoundIndicator;->circlePaint:Landroid/graphics/Paint;

    const-string v1, "#66F5F5F5"

    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 33
    return-void
.end method


# virtual methods
.method public clear()V
    .registers 2

    .line 64
    const/high16 v0, 0x41f00000    # 30.0f

    iput v0, p0, Lbr/com/itau/textovoz/ui/view/custom/SoundIndicator;->minRadius:F

    .line 65
    const/high16 v0, 0x42200000    # 40.0f

    iput v0, p0, Lbr/com/itau/textovoz/ui/view/custom/SoundIndicator;->minRadiusCircle:F

    .line 66
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lbr/com/itau/textovoz/ui/view/custom/SoundIndicator;->soundLevel:F

    .line 67
    invoke-virtual {p0}, Lbr/com/itau/textovoz/ui/view/custom/SoundIndicator;->invalidate()V

    .line 68
    return-void
.end method

.method public dipToPixels(F)F
    .registers 4
    .param p1, "dipValue"    # F

    .line 85
    invoke-virtual {p0}, Lbr/com/itau/textovoz/ui/view/custom/SoundIndicator;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    .line 86
    .local v1, "metrics":Landroid/util/DisplayMetrics;
    const/4 v0, 0x1

    invoke-static {v0, p1, v1}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    return v0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .registers 8
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .line 45
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 52
    iget v0, p0, Lbr/com/itau/textovoz/ui/view/custom/SoundIndicator;->soundLevel:F

    const/high16 v1, 0x3f800000    # 1.0f

    cmpl-float v0, v0, v1

    if-nez v0, :cond_21

    .line 53
    const/high16 v4, 0x41f00000    # 30.0f

    .line 54
    .local v4, "circleRadius":F
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/view/custom/SoundIndicator;->circlePosition:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->x:I

    int-to-float v0, v0

    iget-object v1, p0, Lbr/com/itau/textovoz/ui/view/custom/SoundIndicator;->circlePosition:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->y:I

    int-to-float v1, v1

    invoke-virtual {p0, v4}, Lbr/com/itau/textovoz/ui/view/custom/SoundIndicator;->dipToPixels(F)F

    move-result v2

    iget-object v3, p0, Lbr/com/itau/textovoz/ui/view/custom/SoundIndicator;->circlePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    goto :goto_53

    .line 55
    .end local v4    # "circleRadius":F
    :cond_21
    iget v0, p0, Lbr/com/itau/textovoz/ui/view/custom/SoundIndicator;->soundLevel:F

    const/high16 v1, 0x40000000    # 2.0f

    cmpl-float v0, v0, v1

    if-nez v0, :cond_53

    .line 56
    const/high16 v4, 0x41f00000    # 30.0f

    .line 57
    .local v4, "circleRadius":F
    const/high16 v5, 0x42180000    # 38.0f

    .line 58
    .local v5, "circleRadiusCircle":F
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/view/custom/SoundIndicator;->circlePosition:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->x:I

    int-to-float v0, v0

    iget-object v1, p0, Lbr/com/itau/textovoz/ui/view/custom/SoundIndicator;->circlePosition:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->y:I

    int-to-float v1, v1

    invoke-virtual {p0, v4}, Lbr/com/itau/textovoz/ui/view/custom/SoundIndicator;->dipToPixels(F)F

    move-result v2

    iget-object v3, p0, Lbr/com/itau/textovoz/ui/view/custom/SoundIndicator;->circlePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 59
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/view/custom/SoundIndicator;->circlePosition:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->x:I

    int-to-float v0, v0

    iget-object v1, p0, Lbr/com/itau/textovoz/ui/view/custom/SoundIndicator;->circlePosition:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->y:I

    int-to-float v1, v1

    invoke-virtual {p0, v5}, Lbr/com/itau/textovoz/ui/view/custom/SoundIndicator;->dipToPixels(F)F

    move-result v2

    iget-object v3, p0, Lbr/com/itau/textovoz/ui/view/custom/SoundIndicator;->circlePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 61
    .end local v4    # "circleRadius":F
    .end local v5    # "circleRadiusCircle":F
    :cond_53
    :goto_53
    return-void
.end method

.method public onSizeChanged(IIII)V
    .registers 8
    .param p1, "w"    # I
    .param p2, "h"    # I
    .param p3, "oldw"    # I
    .param p4, "oldh"    # I

    .line 37
    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/View;->onSizeChanged(IIII)V

    .line 38
    sget-object v0, Lbr/com/itau/textovoz/ui/view/custom/SoundIndicator;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onSizeChanged: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 39
    new-instance v0, Landroid/graphics/Point;

    div-int/lit8 v1, p1, 0x2

    div-int/lit8 v2, p2, 0x2

    invoke-direct {v0, v1, v2}, Landroid/graphics/Point;-><init>(II)V

    iput-object v0, p0, Lbr/com/itau/textovoz/ui/view/custom/SoundIndicator;->circlePosition:Landroid/graphics/Point;

    .line 40
    int-to-float v0, p1

    int-to-float v1, p2

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr v0, v1

    iput v0, p0, Lbr/com/itau/textovoz/ui/view/custom/SoundIndicator;->maxRadius:F

    .line 41
    return-void
.end method

.method public setSoundLevel(F)V
    .registers 3
    .param p1, "level"    # F

    .line 73
    const/high16 v0, 0x40800000    # 4.0f

    cmpl-float v0, p1, v0

    if-ltz v0, :cond_11

    const/high16 v0, 0x41000000    # 8.0f

    cmpg-float v0, p1, v0

    if-gtz v0, :cond_11

    .line 74
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lbr/com/itau/textovoz/ui/view/custom/SoundIndicator;->soundLevel:F

    goto :goto_20

    .line 75
    :cond_11
    const/high16 v0, 0x41200000    # 10.0f

    cmpl-float v0, p1, v0

    if-ltz v0, :cond_1c

    .line 76
    const/high16 v0, 0x40000000    # 2.0f

    iput v0, p0, Lbr/com/itau/textovoz/ui/view/custom/SoundIndicator;->soundLevel:F

    goto :goto_20

    .line 78
    :cond_1c
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lbr/com/itau/textovoz/ui/view/custom/SoundIndicator;->soundLevel:F

    .line 81
    :goto_20
    invoke-virtual {p0}, Lbr/com/itau/textovoz/ui/view/custom/SoundIndicator;->invalidate()V

    .line 82
    return-void
.end method

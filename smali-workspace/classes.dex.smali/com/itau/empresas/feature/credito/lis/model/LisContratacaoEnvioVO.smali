.class public Lcom/itau/empresas/feature/credito/lis/model/LisContratacaoEnvioVO;
.super Ljava/lang/Object;
.source "LisContratacaoEnvioVO.java"


# instance fields
.field private final agencia:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "agencia"
    .end annotation
.end field

.field private final agenciaContratual:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "agencia_contratual"
    .end annotation
.end field

.field private final conta:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "conta"
    .end annotation
.end field

.field private final contaContratual:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "conta_contratual"
    .end annotation
.end field

.field private final cpfDevedorSolidario:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "cpf_devedor_solidario"
    .end annotation
.end field

.field private final diaPagamento:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "dia_pagamento"
    .end annotation
.end field

.field private final digitoConta:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "digito_conta"
    .end annotation
.end field

.field private final digitoContaContratual:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "digito_conta_contratual"
    .end annotation
.end field

.field private final limiteContratado:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "limite_contratado"
    .end annotation
.end field

.field private final nomeDevedorSolidario:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "nome_devedor_solidario"
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 9
    .param p1, "limiteContratado"    # Ljava/lang/String;
    .param p2, "diaPagamento"    # Ljava/lang/String;
    .param p3, "agencia"    # Ljava/lang/String;
    .param p4, "conta"    # Ljava/lang/String;
    .param p5, "digitoConta"    # Ljava/lang/String;
    .param p6, "cpfDevedorSolidario"    # Ljava/lang/String;
    .param p7, "nomeDevedorSolidario"    # Ljava/lang/String;

    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    iput-object p1, p0, Lcom/itau/empresas/feature/credito/lis/model/LisContratacaoEnvioVO;->limiteContratado:Ljava/lang/String;

    .line 41
    iput-object p2, p0, Lcom/itau/empresas/feature/credito/lis/model/LisContratacaoEnvioVO;->diaPagamento:Ljava/lang/String;

    .line 42
    iput-object p3, p0, Lcom/itau/empresas/feature/credito/lis/model/LisContratacaoEnvioVO;->agencia:Ljava/lang/String;

    .line 43
    iput-object p4, p0, Lcom/itau/empresas/feature/credito/lis/model/LisContratacaoEnvioVO;->conta:Ljava/lang/String;

    .line 44
    iput-object p5, p0, Lcom/itau/empresas/feature/credito/lis/model/LisContratacaoEnvioVO;->digitoConta:Ljava/lang/String;

    .line 45
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/model/LisContratacaoEnvioVO;->agencia:Ljava/lang/String;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/model/LisContratacaoEnvioVO;->agenciaContratual:Ljava/lang/String;

    .line 46
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/model/LisContratacaoEnvioVO;->conta:Ljava/lang/String;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/model/LisContratacaoEnvioVO;->contaContratual:Ljava/lang/String;

    .line 47
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/model/LisContratacaoEnvioVO;->digitoConta:Ljava/lang/String;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/model/LisContratacaoEnvioVO;->digitoContaContratual:Ljava/lang/String;

    .line 48
    iput-object p6, p0, Lcom/itau/empresas/feature/credito/lis/model/LisContratacaoEnvioVO;->cpfDevedorSolidario:Ljava/lang/String;

    .line 49
    iput-object p7, p0, Lcom/itau/empresas/feature/credito/lis/model/LisContratacaoEnvioVO;->nomeDevedorSolidario:Ljava/lang/String;

    .line 50
    return-void
.end method

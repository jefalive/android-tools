.class public Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment;
.super Lcom/itau/empresas/ui/fragment/BaseFragment;
.source "AlterarSenhaFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment$RegexValidatorWithLength;
    }
.end annotation


# instance fields
.field botaoContinuar:Landroid/widget/Button;

.field campoCodigoOperador:Lbr/com/itau/widgets/material/MaterialEditText;

.field campoConfirmarNovaSenha:Lbr/com/itau/widgets/material/MaterialEditText;

.field campoNovaSenha:Lbr/com/itau/widgets/material/MaterialEditText;

.field campoSenhaAtual:Lbr/com/itau/widgets/material/MaterialEditText;

.field primeiroAcesso:Ljava/lang/Boolean;

.field trocaObrigatoria:Ljava/lang/Boolean;

.field trocaSenhaController:Lcom/itau/empresas/controller/TrocaSenhaController;

.field viewEspacoSemCampoSenhaAtual:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .registers 2

    .line 47
    invoke-direct {p0}, Lcom/itau/empresas/ui/fragment/BaseFragment;-><init>()V

    .line 62
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment;->trocaObrigatoria:Ljava/lang/Boolean;

    .line 63
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment;->primeiroAcesso:Ljava/lang/Boolean;

    return-void
.end method

.method private evento(Ljava/lang/String;)V
    .registers 4
    .param p1, "acao"    # Ljava/lang/String;

    .line 280
    invoke-virtual {p0}, Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    .line 281
    .local v1, "activity":Landroid/app/Activity;
    if-eqz v1, :cond_10

    instance-of v0, v1, Lcom/itau/empresas/ui/activity/GenericaActivity;

    if-eqz v0, :cond_10

    .line 282
    move-object v0, v1

    check-cast v0, Lcom/itau/empresas/ui/activity/GenericaActivity;

    invoke-virtual {v0, p1}, Lcom/itau/empresas/ui/activity/GenericaActivity;->analyticsHit(Ljava/lang/String;)V

    .line 284
    :cond_10
    return-void
.end method

.method private habilitarBotao()V
    .registers 6

    .line 205
    iget-object v0, p0, Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment;->campoNovaSenha:Lbr/com/itau/widgets/material/MaterialEditText;

    invoke-virtual {v0}, Lbr/com/itau/widgets/material/MaterialEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    .line 206
    .local v3, "novaSenha":Ljava/lang/String;
    iget-object v0, p0, Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment;->campoConfirmarNovaSenha:Lbr/com/itau/widgets/material/MaterialEditText;

    invoke-virtual {v0}, Lbr/com/itau/widgets/material/MaterialEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    .line 207
    .local v4, "confirmacao":Ljava/lang/String;
    iget-object v0, p0, Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment;->primeiroAcesso:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_39

    .line 208
    iget-object v0, p0, Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment;->botaoContinuar:Landroid/widget/Button;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v2, 0x6

    if-lt v1, v2, :cond_34

    .line 209
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v2, 0x6

    if-lt v1, v2, :cond_34

    invoke-virtual {p0}, Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment;->isValido()Z

    move-result v1

    if-eqz v1, :cond_34

    const/4 v1, 0x1

    goto :goto_35

    :cond_34
    const/4 v1, 0x0

    .line 208
    :goto_35
    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_66

    .line 211
    :cond_39
    iget-object v0, p0, Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment;->botaoContinuar:Landroid/widget/Button;

    iget-object v1, p0, Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment;->campoSenhaAtual:Lbr/com/itau/widgets/material/MaterialEditText;

    invoke-virtual {v1}, Lbr/com/itau/widgets/material/MaterialEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v2, 0x6

    if-lt v1, v2, :cond_62

    .line 212
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v2, 0x6

    if-lt v1, v2, :cond_62

    .line 213
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v2, 0x6

    if-lt v1, v2, :cond_62

    invoke-virtual {p0}, Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment;->isValido()Z

    move-result v1

    if-eqz v1, :cond_62

    const/4 v1, 0x1

    goto :goto_63

    :cond_62
    const/4 v1, 0x0

    .line 211
    :goto_63
    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 215
    :goto_66
    return-void
.end method

.method private static transformeEmPassword(Landroid/widget/EditText;)V
    .registers 2
    .param p0, "campo"    # Landroid/widget/EditText;

    .line 124
    new-instance v0, Lcom/itau/empresas/ui/util/ViewUtils$CustomPasswordTransformationMethod;

    invoke-direct {v0}, Lcom/itau/empresas/ui/util/ViewUtils$CustomPasswordTransformationMethod;-><init>()V

    invoke-virtual {p0, v0}, Landroid/widget/EditText;->setTransformationMethod(Landroid/text/method/TransformationMethod;)V

    .line 125
    return-void
.end method


# virtual methods
.method protected aoAlterarConfirmarNovaSenha()V
    .registers 2

    .line 200
    iget-object v0, p0, Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment;->campoConfirmarNovaSenha:Lbr/com/itau/widgets/material/MaterialEditText;

    invoke-virtual {v0}, Lbr/com/itau/widgets/material/MaterialEditText;->validate()Z

    .line 201
    invoke-direct {p0}, Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment;->habilitarBotao()V

    .line 202
    return-void
.end method

.method protected aoAlterarNovaSenha()V
    .registers 2

    .line 194
    iget-object v0, p0, Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment;->campoNovaSenha:Lbr/com/itau/widgets/material/MaterialEditText;

    invoke-virtual {v0}, Lbr/com/itau/widgets/material/MaterialEditText;->validate()Z

    .line 195
    invoke-direct {p0}, Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment;->habilitarBotao()V

    .line 196
    return-void
.end method

.method protected aoAlterarSenhaAtual()V
    .registers 2

    .line 188
    iget-object v0, p0, Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment;->campoSenhaAtual:Lbr/com/itau/widgets/material/MaterialEditText;

    invoke-virtual {v0}, Lbr/com/itau/widgets/material/MaterialEditText;->validate()Z

    .line 189
    invoke-direct {p0}, Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment;->habilitarBotao()V

    .line 190
    return-void
.end method

.method protected aoIniciarActivity()V
    .registers 7

    .line 73
    const v0, 0x7f0704fa

    invoke-virtual {p0, v0}, Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment;->setTitle(I)V

    .line 74
    iget-object v0, p0, Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment;->campoCodigoOperador:Lbr/com/itau/widgets/material/MaterialEditText;

    iget-object v1, p0, Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment;->application:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v1}, Lcom/itau/empresas/CustomApplication;->getCodigoOperador()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbr/com/itau/widgets/material/MaterialEditText;->setText(Ljava/lang/CharSequence;)V

    .line 76
    iget-object v0, p0, Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment;->primeiroAcesso:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_27

    .line 77
    iget-object v0, p0, Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment;->campoSenhaAtual:Lbr/com/itau/widgets/material/MaterialEditText;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lbr/com/itau/widgets/material/MaterialEditText;->setVisibility(I)V

    .line 78
    iget-object v0, p0, Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment;->viewEspacoSemCampoSenhaAtual:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_48

    .line 80
    :cond_27
    iget-object v0, p0, Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment;->viewEspacoSemCampoSenhaAtual:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 81
    iget-object v0, p0, Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment;->campoSenhaAtual:Lbr/com/itau/widgets/material/MaterialEditText;

    invoke-static {v0}, Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment;->transformeEmPassword(Landroid/widget/EditText;)V

    .line 82
    iget-object v0, p0, Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment;->campoSenhaAtual:Lbr/com/itau/widgets/material/MaterialEditText;

    new-instance v1, Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment$RegexValidatorWithLength;

    iget-object v2, p0, Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment;->campoSenhaAtual:Lbr/com/itau/widgets/material/MaterialEditText;

    .line 83
    const v3, 0x7f070173

    invoke-virtual {p0, v3}, Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    const-string v4, "^(\\d{6,8})$"

    invoke-direct {v1, v2, v3, v4}, Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment$RegexValidatorWithLength;-><init>(Lbr/com/itau/widgets/material/MaterialEditText;Ljava/lang/String;Ljava/lang/String;)V

    .line 82
    invoke-virtual {v0, v1}, Lbr/com/itau/widgets/material/MaterialEditText;->addValidator(Lbr/com/itau/widgets/material/validation/METValidator;)Lbr/com/itau/widgets/material/MaterialEditText;

    .line 86
    :goto_48
    iget-object v0, p0, Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment;->campoNovaSenha:Lbr/com/itau/widgets/material/MaterialEditText;

    invoke-static {v0}, Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment;->transformeEmPassword(Landroid/widget/EditText;)V

    .line 87
    iget-object v0, p0, Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment;->campoConfirmarNovaSenha:Lbr/com/itau/widgets/material/MaterialEditText;

    invoke-static {v0}, Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment;->transformeEmPassword(Landroid/widget/EditText;)V

    .line 89
    iget-object v0, p0, Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment;->campoNovaSenha:Lbr/com/itau/widgets/material/MaterialEditText;

    new-instance v1, Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment$RegexValidatorWithLength;

    iget-object v2, p0, Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment;->campoNovaSenha:Lbr/com/itau/widgets/material/MaterialEditText;

    .line 91
    const v3, 0x7f07016f

    invoke-virtual {p0, v3}, Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    const-string v4, "^\\d{6}$"

    invoke-direct {v1, v2, v3, v4}, Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment$RegexValidatorWithLength;-><init>(Lbr/com/itau/widgets/material/MaterialEditText;Ljava/lang/String;Ljava/lang/String;)V

    .line 89
    invoke-virtual {v0, v1}, Lbr/com/itau/widgets/material/MaterialEditText;->addValidator(Lbr/com/itau/widgets/material/validation/METValidator;)Lbr/com/itau/widgets/material/MaterialEditText;

    .line 93
    iget-object v0, p0, Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment;->campoConfirmarNovaSenha:Lbr/com/itau/widgets/material/MaterialEditText;

    new-instance v1, Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment$1;

    .line 95
    const v2, 0x7f070167

    invoke-virtual {p0, v2}, Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment$1;-><init>(Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment;Ljava/lang/String;)V

    .line 94
    invoke-virtual {v0, v1}, Lbr/com/itau/widgets/material/MaterialEditText;->addValidator(Lbr/com/itau/widgets/material/validation/METValidator;)Lbr/com/itau/widgets/material/MaterialEditText;

    .line 104
    invoke-virtual {p0}, Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Landroid/view/inputmethod/InputMethodManager;

    .line 106
    .local v5, "imm":Landroid/view/inputmethod/InputMethodManager;
    iget-object v0, p0, Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment;->primeiroAcesso:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_9c

    .line 107
    iget-object v0, p0, Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment;->campoSenhaAtual:Lbr/com/itau/widgets/material/MaterialEditText;

    invoke-virtual {v0}, Lbr/com/itau/widgets/material/MaterialEditText;->clearFocus()V

    .line 108
    iget-object v0, p0, Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment;->campoSenhaAtual:Lbr/com/itau/widgets/material/MaterialEditText;

    invoke-virtual {v0}, Lbr/com/itau/widgets/material/MaterialEditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v5, v0, v1}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 110
    :cond_9c
    iget-object v0, p0, Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment;->campoNovaSenha:Lbr/com/itau/widgets/material/MaterialEditText;

    invoke-virtual {v0}, Lbr/com/itau/widgets/material/MaterialEditText;->clearFocus()V

    .line 111
    iget-object v0, p0, Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment;->campoNovaSenha:Lbr/com/itau/widgets/material/MaterialEditText;

    invoke-virtual {v0}, Lbr/com/itau/widgets/material/MaterialEditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v5, v0, v1}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 112
    iget-object v0, p0, Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment;->campoConfirmarNovaSenha:Lbr/com/itau/widgets/material/MaterialEditText;

    invoke-virtual {v0}, Lbr/com/itau/widgets/material/MaterialEditText;->clearFocus()V

    .line 113
    iget-object v0, p0, Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment;->campoConfirmarNovaSenha:Lbr/com/itau/widgets/material/MaterialEditText;

    invoke-virtual {v0}, Lbr/com/itau/widgets/material/MaterialEditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v5, v0, v1}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 115
    iget-object v0, p0, Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment;->botaoContinuar:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 117
    const v0, 0x7f070101

    invoke-virtual {p0, v0}, Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 118
    const v1, 0x7f0700cf

    invoke-virtual {p0, v1}, Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 119
    const v2, 0x7f0700c3

    invoke-virtual {p0, v2}, Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 117
    invoke-virtual {p0, v0, v1, v2}, Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment;->eventoAdobe(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 120
    return-void
.end method

.method protected continuar()V
    .registers 4

    .line 129
    invoke-virtual {p0}, Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment;->isValido()Z

    move-result v0

    if-nez v0, :cond_7

    .line 130
    return-void

    .line 133
    :cond_7
    new-instance v2, Lcom/itau/empresas/api/model/CadastroSenhaEletronicaVO;

    invoke-direct {v2}, Lcom/itau/empresas/api/model/CadastroSenhaEletronicaVO;-><init>()V

    .line 134
    .local v2, "senhaEletronicaVO":Lcom/itau/empresas/api/model/CadastroSenhaEletronicaVO;
    iget-object v0, p0, Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment;->primeiroAcesso:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 135
    const-string v0, ""

    invoke-virtual {v2, v0}, Lcom/itau/empresas/api/model/CadastroSenhaEletronicaVO;->setSenhaExpirada(Ljava/lang/String;)V

    goto :goto_27

    .line 137
    :cond_1a
    iget-object v0, p0, Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment;->campoSenhaAtual:Lbr/com/itau/widgets/material/MaterialEditText;

    invoke-virtual {v0}, Lbr/com/itau/widgets/material/MaterialEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/itau/empresas/api/model/CadastroSenhaEletronicaVO;->setSenhaExpirada(Ljava/lang/String;)V

    .line 139
    :goto_27
    iget-object v0, p0, Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment;->campoNovaSenha:Lbr/com/itau/widgets/material/MaterialEditText;

    invoke-virtual {v0}, Lbr/com/itau/widgets/material/MaterialEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/itau/empresas/api/model/CadastroSenhaEletronicaVO;->setNovaSenha(Ljava/lang/String;)V

    .line 140
    iget-object v0, p0, Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment;->campoConfirmarNovaSenha:Lbr/com/itau/widgets/material/MaterialEditText;

    invoke-virtual {v0}, Lbr/com/itau/widgets/material/MaterialEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/itau/empresas/api/model/CadastroSenhaEletronicaVO;->setNovaSenhaRepetida(Ljava/lang/String;)V

    .line 143
    const-string v0, ""

    invoke-virtual {v2, v0}, Lcom/itau/empresas/api/model/CadastroSenhaEletronicaVO;->setCnpj(Ljava/lang/String;)V

    .line 145
    iget-object v0, p0, Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment;->trocaObrigatoria:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_56

    iget-object v0, p0, Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment;->primeiroAcesso:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_67

    .line 146
    :cond_56
    const-string v0, "E"

    invoke-virtual {v2, v0}, Lcom/itau/empresas/api/model/CadastroSenhaEletronicaVO;->setTipoAcesso(Ljava/lang/String;)V

    .line 147
    iget-object v0, p0, Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment;->trocaSenhaController:Lcom/itau/empresas/controller/TrocaSenhaController;

    iget-object v1, p0, Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment;->application:Lcom/itau/empresas/CustomApplication;

    .line 148
    invoke-virtual {v1}, Lcom/itau/empresas/CustomApplication;->getCodigoOperador()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v2}, Lcom/itau/empresas/controller/TrocaSenhaController;->trocarSenhaObrigatoria(Ljava/lang/String;Lcom/itau/empresas/api/model/CadastroSenhaEletronicaVO;)V

    goto :goto_77

    .line 150
    :cond_67
    const-string v0, "T"

    invoke-virtual {v2, v0}, Lcom/itau/empresas/api/model/CadastroSenhaEletronicaVO;->setTipoAcesso(Ljava/lang/String;)V

    .line 151
    iget-object v0, p0, Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment;->trocaSenhaController:Lcom/itau/empresas/controller/TrocaSenhaController;

    iget-object v1, p0, Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment;->application:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v1}, Lcom/itau/empresas/CustomApplication;->getCodigoOperador()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v2}, Lcom/itau/empresas/controller/TrocaSenhaController;->cadastroSenha(Ljava/lang/String;Lcom/itau/empresas/api/model/CadastroSenhaEletronicaVO;)V

    .line 154
    :goto_77
    invoke-virtual {p0}, Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment;->ocultaMensagemErro()V

    .line 155
    invoke-virtual {p0}, Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment;->mostraDialogoDeProgresso()V

    .line 156
    return-void
.end method

.method public exibeMensagemErro(Ljava/lang/String;)V
    .registers 4
    .param p1, "msg"    # Ljava/lang/String;

    .line 265
    invoke-virtual {p0}, Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, p1, v1}, Lcom/itau/empresas/ui/util/SnackbarUtils;->tentaMostrarSnackbarNo(Ljava/lang/Object;Ljava/lang/CharSequence;I)V

    .line 266
    return-void
.end method

.method protected isValido()Z
    .registers 5

    .line 219
    iget-object v0, p0, Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment;->campoNovaSenha:Lbr/com/itau/widgets/material/MaterialEditText;

    invoke-virtual {v0}, Lbr/com/itau/widgets/material/MaterialEditText;->validate()Z

    move-result v1

    .line 220
    .local v1, "novaSenha":Z
    iget-object v0, p0, Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment;->campoConfirmarNovaSenha:Lbr/com/itau/widgets/material/MaterialEditText;

    invoke-virtual {v0}, Lbr/com/itau/widgets/material/MaterialEditText;->validate()Z

    move-result v2

    .line 223
    .local v2, "confirmarNovaSenha":Z
    iget-object v0, p0, Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment;->primeiroAcesso:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 224
    if-eqz v1, :cond_1a

    if-eqz v2, :cond_1a

    const/4 v0, 0x1

    goto :goto_1b

    :cond_1a
    const/4 v0, 0x0

    :goto_1b
    return v0

    .line 226
    :cond_1c
    iget-object v0, p0, Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment;->campoSenhaAtual:Lbr/com/itau/widgets/material/MaterialEditText;

    invoke-virtual {v0}, Lbr/com/itau/widgets/material/MaterialEditText;->validate()Z

    move-result v3

    .line 227
    .local v3, "senhaAtual":Z
    if-eqz v3, :cond_2a

    if-eqz v1, :cond_2a

    if-eqz v2, :cond_2a

    const/4 v0, 0x1

    goto :goto_2b

    :cond_2a
    const/4 v0, 0x0

    :goto_2b
    return v0
.end method

.method public ocultaMensagemErro()V
    .registers 2

    .line 269
    invoke-virtual {p0}, Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/itau/empresas/ui/util/SnackbarUtils;->tentaDispensarSnackbarNo(Ljava/lang/Object;)V

    .line 270
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 67
    invoke-virtual {p0}, Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/itau/empresas/ui/util/X;->y(Landroid/content/Context;)V

    .line 68
    invoke-super {p0, p1}, Lcom/itau/empresas/ui/fragment/BaseFragment;->onCreate(Landroid/os/Bundle;)V

    .line 69
    return-void
.end method

.method public onEventMainThread(Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent$Conversion;)V
    .registers 3
    .param p1, "e"    # Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent$Conversion;

    .line 232
    invoke-static {p1, p0}, Lcom/itau/empresas/api/ApiHelper;->devoTratarEvento(Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;Lcom/itau/empresas/api/ApiConsumidor;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 233
    return-void

    .line 235
    :cond_7
    invoke-virtual {p0}, Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment;->escondeDialogoDeProgresso()V

    .line 236
    const v0, 0x7f0701f2

    invoke-virtual {p0, v0}, Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment;->exibeMensagemErro(Ljava/lang/String;)V

    .line 237
    return-void
.end method

.method public onEventMainThread(Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;)V
    .registers 8
    .param p1, "e"    # Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;

    .line 241
    invoke-static {p1, p0}, Lcom/itau/empresas/api/ApiHelper;->devoTratarEvento(Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;Lcom/itau/empresas/api/ApiConsumidor;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 242
    return-void

    .line 245
    :cond_7
    invoke-virtual {p0}, Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment;->escondeDialogoDeProgresso()V

    .line 246
    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;->getException()Lbr/com/itau/sdk/android/core/exception/BackendException;

    move-result-object v2

    .line 247
    .local v2, "exception":Lbr/com/itau/sdk/android/core/exception/BackendException;
    invoke-virtual {v2}, Lbr/com/itau/sdk/android/core/exception/BackendException;->getError()Lbr/com/itau/sdk/android/core/model/ErrorDTO;

    move-result-object v3

    .line 248
    .local v3, "errorDTO":Lbr/com/itau/sdk/android/core/model/ErrorDTO;
    if-eqz v3, :cond_73

    .line 249
    invoke-virtual {v3}, Lbr/com/itau/sdk/android/core/model/ErrorDTO;->getCodigo()Ljava/lang/String;

    move-result-object v4

    const/4 v5, -0x1

    invoke-virtual {v4}, Ljava/lang/String;->hashCode()I

    move-result v0

    sparse-switch v0, :sswitch_data_74

    goto :goto_34

    :sswitch_21
    const-string v0, "07"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_34

    const/4 v5, 0x0

    goto :goto_34

    :sswitch_2b
    const-string v0, "03"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_34

    const/4 v5, 0x1

    :cond_34
    :goto_34
    sparse-switch v5, :sswitch_data_7e

    goto :goto_68

    .line 251
    :sswitch_38
    const v0, 0x7f0702ce

    invoke-virtual {p0, v0}, Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment;->evento(Ljava/lang/String;)V

    .line 252
    iget-object v0, p0, Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment;->campoSenhaAtual:Lbr/com/itau/widgets/material/MaterialEditText;

    invoke-virtual {v3}, Lbr/com/itau/sdk/android/core/model/ErrorDTO;->getMensagem()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbr/com/itau/widgets/material/MaterialEditText;->setError(Ljava/lang/CharSequence;)V

    .line 253
    goto :goto_73

    .line 255
    :sswitch_50
    const v0, 0x7f0702cd

    invoke-virtual {p0, v0}, Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment;->evento(Ljava/lang/String;)V

    .line 256
    iget-object v0, p0, Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment;->campoNovaSenha:Lbr/com/itau/widgets/material/MaterialEditText;

    invoke-virtual {v3}, Lbr/com/itau/sdk/android/core/model/ErrorDTO;->getMensagem()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbr/com/itau/widgets/material/MaterialEditText;->setError(Ljava/lang/CharSequence;)V

    .line 257
    goto :goto_73

    .line 259
    :goto_68
    invoke-virtual {v2}, Lbr/com/itau/sdk/android/core/exception/BackendException;->getError()Lbr/com/itau/sdk/android/core/model/ErrorDTO;

    move-result-object v0

    invoke-virtual {v0}, Lbr/com/itau/sdk/android/core/model/ErrorDTO;->getMensagem()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment;->exibeMensagemErro(Ljava/lang/String;)V

    .line 262
    :cond_73
    :goto_73
    return-void

    :sswitch_data_74
    .sparse-switch
        0x603 -> :sswitch_2b
        0x607 -> :sswitch_21
    .end sparse-switch

    :sswitch_data_7e
    .sparse-switch
        0x0 -> :sswitch_38
        0x1 -> :sswitch_50
    .end sparse-switch
.end method

.method public onEventMainThread(Lcom/itau/empresas/api/model/ResultadoCadastroSenhaEletronicaVO;)V
    .registers 6
    .param p1, "resultadoVO"    # Lcom/itau/empresas/api/model/ResultadoCadastroSenhaEletronicaVO;

    .line 160
    const v0, 0x7f0702cf

    invoke-virtual {p0, v0}, Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment;->evento(Ljava/lang/String;)V

    .line 161
    invoke-virtual {p0}, Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment;->baseActivity()Lcom/itau/empresas/ui/activity/BaseActivity;

    move-result-object v2

    .line 162
    .local v2, "activity":Lcom/itau/empresas/ui/activity/BaseActivity;
    invoke-virtual {p0}, Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment;->escondeDialogoDeProgresso()V

    .line 164
    new-instance v0, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;

    invoke-direct {v0, v2}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget-object v1, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Estados;->UM_BOTAO:Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Estados;

    .line 165
    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;->estado(Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Estados;)Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;

    move-result-object v0

    .line 166
    const v1, 0x7f07066c

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;->mensagem(I)Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;

    move-result-object v0

    .line 167
    const v1, 0x7f07048b

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;->textoNeutro(I)Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;->build()Lcom/itau/empresas/ui/dialog/CustomAlertDialog;

    move-result-object v3

    .line 169
    .local v3, "dialog":Lcom/itau/empresas/ui/dialog/CustomAlertDialog;
    new-instance v0, Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment$2;

    invoke-direct {v0, p0, v3}, Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment$2;-><init>(Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment;Lcom/itau/empresas/ui/dialog/CustomAlertDialog;)V

    invoke-virtual {v3, v0}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog;->setNeutroListener(Landroid/view/View$OnClickListener;)V

    .line 183
    invoke-virtual {v3, v2}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog;->show(Landroid/support/v7/app/AppCompatActivity;)Lcom/itau/empresas/ui/dialog/BaseDialog;

    .line 184
    return-void
.end method

.method public opkeysConsumidas()Ljava/util/List;
    .registers 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Ljava/lang/String;>;"
        }
    .end annotation

    .line 288
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "trocarSenhaObrigatoria"

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const-string v1, "operadorSenha"

    const/4 v2, 0x1

    aput-object v1, v0, v2

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public setTitle(I)V
    .registers 3
    .param p1, "res"    # I

    .line 273
    invoke-virtual {p0}, Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 274
    .local v0, "activity":Landroid/app/Activity;
    if-eqz v0, :cond_9

    .line 275
    invoke-virtual {v0, p1}, Landroid/app/Activity;->setTitle(I)V

    .line 277
    :cond_9
    return-void
.end method

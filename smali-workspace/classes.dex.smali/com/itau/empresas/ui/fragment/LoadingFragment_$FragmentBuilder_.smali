.class public Lcom/itau/empresas/ui/fragment/LoadingFragment_$FragmentBuilder_;
.super Lorg/androidannotations/api/builder/FragmentBuilder;
.source "LoadingFragment_.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/itau/empresas/ui/fragment/LoadingFragment_;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "FragmentBuilder_"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/androidannotations/api/builder/FragmentBuilder<Lcom/itau/empresas/ui/fragment/LoadingFragment_$FragmentBuilder_;Lcom/itau/empresas/ui/fragment/LoadingFragment;>;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 72
    invoke-direct {p0}, Lorg/androidannotations/api/builder/FragmentBuilder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/itau/empresas/ui/fragment/LoadingFragment;
    .registers 3

    .line 78
    new-instance v1, Lcom/itau/empresas/ui/fragment/LoadingFragment_;

    invoke-direct {v1}, Lcom/itau/empresas/ui/fragment/LoadingFragment_;-><init>()V

    .line 79
    .local v1, "fragment_":Lcom/itau/empresas/ui/fragment/LoadingFragment_;
    iget-object v0, p0, Lcom/itau/empresas/ui/fragment/LoadingFragment_$FragmentBuilder_;->args:Landroid/os/Bundle;

    invoke-virtual {v1, v0}, Lcom/itau/empresas/ui/fragment/LoadingFragment_;->setArguments(Landroid/os/Bundle;)V

    .line 80
    return-object v1
.end method

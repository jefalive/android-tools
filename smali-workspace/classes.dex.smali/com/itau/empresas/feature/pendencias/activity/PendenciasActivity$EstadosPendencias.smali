.class public final enum Lcom/itau/empresas/feature/pendencias/activity/PendenciasActivity$EstadosPendencias;
.super Ljava/lang/Enum;
.source "PendenciasActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/itau/empresas/feature/pendencias/activity/PendenciasActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "EstadosPendencias"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<Lcom/itau/empresas/feature/pendencias/activity/PendenciasActivity$EstadosPendencias;>;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/itau/empresas/feature/pendencias/activity/PendenciasActivity$EstadosPendencias;

.field public static final enum NAO_POSSUI_PENDENCIAS:Lcom/itau/empresas/feature/pendencias/activity/PendenciasActivity$EstadosPendencias;

.field public static final enum POSSUI_PENDENCIAS:Lcom/itau/empresas/feature/pendencias/activity/PendenciasActivity$EstadosPendencias;

.field public static final enum SEM_CONEXAO:Lcom/itau/empresas/feature/pendencias/activity/PendenciasActivity$EstadosPendencias;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .line 154
    new-instance v0, Lcom/itau/empresas/feature/pendencias/activity/PendenciasActivity$EstadosPendencias;

    const-string v1, "POSSUI_PENDENCIAS"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/itau/empresas/feature/pendencias/activity/PendenciasActivity$EstadosPendencias;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/itau/empresas/feature/pendencias/activity/PendenciasActivity$EstadosPendencias;->POSSUI_PENDENCIAS:Lcom/itau/empresas/feature/pendencias/activity/PendenciasActivity$EstadosPendencias;

    .line 155
    new-instance v0, Lcom/itau/empresas/feature/pendencias/activity/PendenciasActivity$EstadosPendencias;

    const-string v1, "NAO_POSSUI_PENDENCIAS"

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lcom/itau/empresas/feature/pendencias/activity/PendenciasActivity$EstadosPendencias;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/itau/empresas/feature/pendencias/activity/PendenciasActivity$EstadosPendencias;->NAO_POSSUI_PENDENCIAS:Lcom/itau/empresas/feature/pendencias/activity/PendenciasActivity$EstadosPendencias;

    .line 156
    new-instance v0, Lcom/itau/empresas/feature/pendencias/activity/PendenciasActivity$EstadosPendencias;

    const-string v1, "SEM_CONEXAO"

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2}, Lcom/itau/empresas/feature/pendencias/activity/PendenciasActivity$EstadosPendencias;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/itau/empresas/feature/pendencias/activity/PendenciasActivity$EstadosPendencias;->SEM_CONEXAO:Lcom/itau/empresas/feature/pendencias/activity/PendenciasActivity$EstadosPendencias;

    .line 153
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/itau/empresas/feature/pendencias/activity/PendenciasActivity$EstadosPendencias;

    sget-object v1, Lcom/itau/empresas/feature/pendencias/activity/PendenciasActivity$EstadosPendencias;->POSSUI_PENDENCIAS:Lcom/itau/empresas/feature/pendencias/activity/PendenciasActivity$EstadosPendencias;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sget-object v1, Lcom/itau/empresas/feature/pendencias/activity/PendenciasActivity$EstadosPendencias;->NAO_POSSUI_PENDENCIAS:Lcom/itau/empresas/feature/pendencias/activity/PendenciasActivity$EstadosPendencias;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    sget-object v1, Lcom/itau/empresas/feature/pendencias/activity/PendenciasActivity$EstadosPendencias;->SEM_CONEXAO:Lcom/itau/empresas/feature/pendencias/activity/PendenciasActivity$EstadosPendencias;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    sput-object v0, Lcom/itau/empresas/feature/pendencias/activity/PendenciasActivity$EstadosPendencias;->$VALUES:[Lcom/itau/empresas/feature/pendencias/activity/PendenciasActivity$EstadosPendencias;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 153
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/itau/empresas/feature/pendencias/activity/PendenciasActivity$EstadosPendencias;
    .registers 2
    .param p0, "name"    # Ljava/lang/String;

    .line 153
    const-class v0, Lcom/itau/empresas/feature/pendencias/activity/PendenciasActivity$EstadosPendencias;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/feature/pendencias/activity/PendenciasActivity$EstadosPendencias;

    return-object v0
.end method

.method public static values()[Lcom/itau/empresas/feature/pendencias/activity/PendenciasActivity$EstadosPendencias;
    .registers 1

    .line 153
    sget-object v0, Lcom/itau/empresas/feature/pendencias/activity/PendenciasActivity$EstadosPendencias;->$VALUES:[Lcom/itau/empresas/feature/pendencias/activity/PendenciasActivity$EstadosPendencias;

    invoke-virtual {v0}, [Lcom/itau/empresas/feature/pendencias/activity/PendenciasActivity$EstadosPendencias;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/itau/empresas/feature/pendencias/activity/PendenciasActivity$EstadosPendencias;

    return-object v0
.end method

.class public Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;
.super Lbr/com/itau/widgets/graphicalfriedcake/charts/PieRadarChartBase;
.source "RadarChart.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lbr/com/itau/widgets/graphicalfriedcake/charts/PieRadarChartBase<Lbr/com/itau/widgets/graphicalfriedcake/charts/data/RadarData;>;"
    }
.end annotation


# instance fields
.field private mDrawWeb:Z

.field private mInnerWebLineWidth:F

.field private mWebAlpha:I

.field private mWebColor:I

.field private mWebColorInner:I

.field private mWebLineWidth:F

.field private mXAxis:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis;

.field protected mXAxisRenderer:Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/XAxisRendererRadarChart;

.field private mYAxis:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;

.field protected mYAxisRenderer:Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRendererRadarChart;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 5
    .param p1, "context"    # Landroid/content/Context;

    .line 56
    invoke-direct {p0, p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieRadarChartBase;-><init>(Landroid/content/Context;)V

    .line 29
    const/high16 v0, 0x40200000    # 2.5f

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->mWebLineWidth:F

    .line 32
    const/high16 v0, 0x3fc00000    # 1.5f

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->mInnerWebLineWidth:F

    .line 35
    const/16 v0, 0x7a

    const/16 v1, 0x7a

    const/16 v2, 0x7a

    invoke-static {v0, v1, v2}, Landroid/graphics/Color;->rgb(III)I

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->mWebColor:I

    .line 38
    const/16 v0, 0x7a

    const/16 v1, 0x7a

    const/16 v2, 0x7a

    invoke-static {v0, v1, v2}, Landroid/graphics/Color;->rgb(III)I

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->mWebColorInner:I

    .line 41
    const/16 v0, 0x96

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->mWebAlpha:I

    .line 44
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->mDrawWeb:Z

    .line 57
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .line 60
    invoke-direct {p0, p1, p2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieRadarChartBase;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 29
    const/high16 v0, 0x40200000    # 2.5f

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->mWebLineWidth:F

    .line 32
    const/high16 v0, 0x3fc00000    # 1.5f

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->mInnerWebLineWidth:F

    .line 35
    const/16 v0, 0x7a

    const/16 v1, 0x7a

    const/16 v2, 0x7a

    invoke-static {v0, v1, v2}, Landroid/graphics/Color;->rgb(III)I

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->mWebColor:I

    .line 38
    const/16 v0, 0x7a

    const/16 v1, 0x7a

    const/16 v2, 0x7a

    invoke-static {v0, v1, v2}, Landroid/graphics/Color;->rgb(III)I

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->mWebColorInner:I

    .line 41
    const/16 v0, 0x96

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->mWebAlpha:I

    .line 44
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->mDrawWeb:Z

    .line 61
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .line 64
    invoke-direct {p0, p1, p2, p3}, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieRadarChartBase;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 29
    const/high16 v0, 0x40200000    # 2.5f

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->mWebLineWidth:F

    .line 32
    const/high16 v0, 0x3fc00000    # 1.5f

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->mInnerWebLineWidth:F

    .line 35
    const/16 v0, 0x7a

    const/16 v1, 0x7a

    const/16 v2, 0x7a

    invoke-static {v0, v1, v2}, Landroid/graphics/Color;->rgb(III)I

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->mWebColor:I

    .line 38
    const/16 v0, 0x7a

    const/16 v1, 0x7a

    const/16 v2, 0x7a

    invoke-static {v0, v1, v2}, Landroid/graphics/Color;->rgb(III)I

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->mWebColorInner:I

    .line 41
    const/16 v0, 0x96

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->mWebAlpha:I

    .line 44
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->mDrawWeb:Z

    .line 65
    return-void
.end method


# virtual methods
.method protected calcMinMax()V
    .registers 9

    .line 85
    invoke-super {p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieRadarChartBase;->calcMinMax()V

    .line 87
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->mData:Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;

    check-cast v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/RadarData;

    sget-object v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$AxisDependency;->LEFT:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$AxisDependency;

    invoke-virtual {v0, v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/RadarData;->getYMin(Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$AxisDependency;)F

    move-result v3

    .line 88
    .local v3, "minLeft":F
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->mData:Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;

    check-cast v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/RadarData;

    sget-object v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$AxisDependency;->LEFT:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$AxisDependency;

    invoke-virtual {v0, v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/RadarData;->getYMax(Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$AxisDependency;)F

    move-result v4

    .line 90
    .local v4, "maxLeft":F
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->mData:Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;

    check-cast v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/RadarData;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/RadarData;->getXVals()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    int-to-float v0, v0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->mXChartMax:F

    .line 91
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->mXChartMax:F

    iget v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->mXChartMin:F

    sub-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->mDeltaX:F

    .line 93
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->mYAxis:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->isStartAtZeroEnabled()Z

    move-result v0

    if-eqz v0, :cond_3d

    const/4 v0, 0x0

    goto :goto_3e

    :cond_3d
    move v0, v3

    :goto_3e
    sub-float v0, v4, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v5

    .line 95
    .local v5, "leftRange":F
    const/high16 v0, 0x42c80000    # 100.0f

    div-float v0, v5, v0

    iget-object v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->mYAxis:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;

    invoke-virtual {v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->getSpaceTop()F

    move-result v1

    mul-float v6, v0, v1

    .line 96
    .local v6, "topSpaceLeft":F
    const/high16 v0, 0x42c80000    # 100.0f

    div-float v0, v5, v0

    iget-object v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->mYAxis:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;

    invoke-virtual {v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->getSpaceBottom()F

    move-result v1

    mul-float v7, v0, v1

    .line 98
    .local v7, "bottomSpaceLeft":F
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->mData:Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;

    check-cast v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/RadarData;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/RadarData;->getXVals()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    int-to-float v0, v0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->mXChartMax:F

    .line 99
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->mXChartMax:F

    iget v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->mXChartMin:F

    sub-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->mDeltaX:F

    .line 101
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->mYAxis:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;

    iget-object v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->mYAxis:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;

    invoke-virtual {v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->getAxisMaxValue()F

    move-result v1

    invoke-static {v1}, Ljava/lang/Float;->isNaN(F)Z

    move-result v1

    if-nez v1, :cond_8d

    iget-object v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->mYAxis:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;

    .line 102
    invoke-virtual {v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->getAxisMaxValue()F

    move-result v1

    goto :goto_8f

    :cond_8d
    add-float v1, v4, v6

    :goto_8f
    iput v1, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->mAxisMaximum:F

    .line 103
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->mYAxis:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;

    iget-object v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->mYAxis:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;

    invoke-virtual {v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->getAxisMinValue()F

    move-result v1

    invoke-static {v1}, Ljava/lang/Float;->isNaN(F)Z

    move-result v1

    if-nez v1, :cond_a6

    iget-object v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->mYAxis:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;

    .line 104
    invoke-virtual {v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->getAxisMinValue()F

    move-result v1

    goto :goto_a8

    :cond_a6
    sub-float v1, v3, v7

    :goto_a8
    iput v1, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->mAxisMinimum:F

    .line 107
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->mYAxis:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->isStartAtZeroEnabled()Z

    move-result v0

    if-eqz v0, :cond_b7

    .line 108
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->mYAxis:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;

    const/4 v1, 0x0

    iput v1, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->mAxisMinimum:F

    .line 110
    :cond_b7
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->mYAxis:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;

    iget-object v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->mYAxis:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;

    iget v1, v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->mAxisMaximum:F

    iget-object v2, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->mYAxis:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;

    iget v2, v2, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->mAxisMinimum:F

    sub-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    iput v1, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->mAxisRange:F

    .line 111
    return-void
.end method

.method public getFactor()F
    .registers 5

    .line 184
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->mViewPortHandler:Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->getContentRect()Landroid/graphics/RectF;

    move-result-object v3

    .line 185
    .local v3, "content":Landroid/graphics/RectF;
    invoke-virtual {v3}, Landroid/graphics/RectF;->width()F

    move-result v0

    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr v0, v1

    invoke-virtual {v3}, Landroid/graphics/RectF;->height()F

    move-result v1

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    iget-object v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->mYAxis:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;

    iget v1, v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->mAxisRange:F

    div-float/2addr v0, v1

    return v0
.end method

.method public getIndexForAngle(F)I
    .registers 7
    .param p1, "angle"    # F

    .line 202
    invoke-virtual {p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->getRotationAngle()F

    move-result v0

    sub-float v0, p1, v0

    invoke-static {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Utils;->getNormalizedAngle(F)F

    move-result v2

    .line 204
    .local v2, "a":F
    invoke-virtual {p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->getSliceAngle()F

    move-result v3

    .line 206
    .local v3, "sliceangle":F
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_f
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->mData:Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;

    check-cast v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/RadarData;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/RadarData;->getXValCount()I

    move-result v0

    if-ge v4, v0, :cond_2a

    .line 207
    add-int/lit8 v0, v4, 0x1

    int-to-float v0, v0

    mul-float/2addr v0, v3

    const/high16 v1, 0x40000000    # 2.0f

    div-float v1, v3, v1

    sub-float/2addr v0, v1

    cmpl-float v0, v0, v2

    if-lez v0, :cond_27

    .line 208
    return v4

    .line 206
    :cond_27
    add-int/lit8 v4, v4, 0x1

    goto :goto_f

    .line 211
    .end local v4    # "i":I
    :cond_2a
    const/4 v0, 0x0

    return v0
.end method

.method protected getMarkerPosition(Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;Lbr/com/itau/widgets/graphicalfriedcake/charts/highlight/Highlight;)[F
    .registers 14
    .param p1, "e"    # Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;
    .param p2, "highlight"    # Lbr/com/itau/widgets/graphicalfriedcake/charts/highlight/Highlight;

    .line 116
    invoke-virtual {p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->getSliceAngle()F

    move-result v0

    invoke-virtual {p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;->getXIndex()I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v0, v1

    invoke-virtual {p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->getRotationAngle()F

    move-result v1

    add-float v7, v0, v1

    .line 117
    .local v7, "angle":F
    invoke-virtual {p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;->getVal()F

    move-result v0

    invoke-virtual {p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->getFactor()F

    move-result v1

    mul-float v8, v0, v1

    .line 118
    .local v8, "val":F
    invoke-virtual {p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->getCenterOffsets()Landroid/graphics/PointF;

    move-result-object v9

    .line 120
    .local v9, "c":Landroid/graphics/PointF;
    new-instance v10, Landroid/graphics/PointF;

    iget v0, v9, Landroid/graphics/PointF;->x:F

    float-to-double v0, v0

    float-to-double v2, v8

    float-to-double v4, v7

    invoke-static {v4, v5}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Math;->cos(D)D

    move-result-wide v4

    mul-double/2addr v2, v4

    add-double/2addr v0, v2

    double-to-float v0, v0

    iget v1, v9, Landroid/graphics/PointF;->y:F

    float-to-double v1, v1

    float-to-double v3, v8

    float-to-double v5, v7

    .line 121
    invoke-static {v5, v6}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Math;->sin(D)D

    move-result-wide v5

    mul-double/2addr v3, v5

    add-double/2addr v1, v3

    double-to-float v1, v1

    invoke-direct {v10, v0, v1}, Landroid/graphics/PointF;-><init>(FF)V

    .line 123
    .local v10, "p":Landroid/graphics/PointF;
    const/4 v0, 0x2

    new-array v0, v0, [F

    iget v1, v10, Landroid/graphics/PointF;->x:F

    const/4 v2, 0x0

    aput v1, v0, v2

    iget v1, v10, Landroid/graphics/PointF;->y:F

    const/4 v2, 0x1

    aput v1, v0, v2

    return-object v0
.end method

.method public getRadius()F
    .registers 5

    .line 331
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->mViewPortHandler:Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->getContentRect()Landroid/graphics/RectF;

    move-result-object v3

    .line 332
    .local v3, "content":Landroid/graphics/RectF;
    invoke-virtual {v3}, Landroid/graphics/RectF;->width()F

    move-result v0

    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr v0, v1

    invoke-virtual {v3}, Landroid/graphics/RectF;->height()F

    move-result v1

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    return v0
.end method

.method protected getRequiredBaseOffset()F
    .registers 2

    .line 326
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->mXAxis:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_e

    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->mXAxis:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis;

    iget v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis;->mLabelWidth:I

    int-to-float v0, v0

    goto :goto_14

    :cond_e
    const/high16 v0, 0x41200000    # 10.0f

    invoke-static {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Utils;->convertDpToPixel(F)F

    move-result v0

    :goto_14
    return v0
.end method

.method protected getRequiredBottomOffset()F
    .registers 3

    .line 321
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->mLegendRenderer:Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/LegendRenderer;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/LegendRenderer;->getLabelPaint()Landroid/graphics/Paint;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Paint;->getTextSize()F

    move-result v0

    const/high16 v1, 0x40800000    # 4.0f

    mul-float/2addr v0, v1

    return v0
.end method

.method public getSliceAngle()F
    .registers 3

    .line 195
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->mData:Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;

    check-cast v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/RadarData;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/RadarData;->getXValCount()I

    move-result v0

    int-to-float v0, v0

    const/high16 v1, 0x43b40000    # 360.0f

    div-float v0, v1, v0

    return v0
.end method

.method public getWebAlpha()I
    .registers 2

    .line 276
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->mWebAlpha:I

    return v0
.end method

.method public getWebColor()I
    .registers 2

    .line 291
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->mWebColor:I

    return v0
.end method

.method public getWebColorInner()I
    .registers 2

    .line 306
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->mWebColorInner:I

    return v0
.end method

.method public getWebLineWidth()F
    .registers 2

    .line 243
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->mWebLineWidth:F

    return v0
.end method

.method public getWebLineWidthInner()F
    .registers 2

    .line 257
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->mInnerWebLineWidth:F

    return v0
.end method

.method public getXAxis()Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis;
    .registers 2

    .line 230
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->mXAxis:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis;

    return-object v0
.end method

.method public getYAxis()Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;
    .registers 2

    .line 220
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->mYAxis:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;

    return-object v0
.end method

.method public getYChartMax()F
    .registers 2

    .line 339
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->mYAxis:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;

    iget v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->mAxisMaximum:F

    return v0
.end method

.method public getYChartMin()F
    .registers 2

    .line 346
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->mYAxis:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;

    iget v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->mAxisMinimum:F

    return v0
.end method

.method public getYRange()F
    .registers 2

    .line 355
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->mYAxis:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;

    iget v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->mAxisRange:F

    return v0
.end method

.method protected init()V
    .registers 4

    .line 69
    invoke-super {p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieRadarChartBase;->init()V

    .line 71
    new-instance v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;

    sget-object v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$AxisDependency;->LEFT:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$AxisDependency;

    invoke-direct {v0, v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;-><init>(Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$AxisDependency;)V

    iput-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->mYAxis:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;

    .line 72
    new-instance v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis;

    invoke-direct {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis;-><init>()V

    iput-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->mXAxis:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis;

    .line 73
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->mXAxis:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis;->setSpaceBetweenLabels(I)V

    .line 75
    const/high16 v0, 0x3fc00000    # 1.5f

    invoke-static {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Utils;->convertDpToPixel(F)F

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->mWebLineWidth:F

    .line 76
    const/high16 v0, 0x3f400000    # 0.75f

    invoke-static {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Utils;->convertDpToPixel(F)F

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->mInnerWebLineWidth:F

    .line 78
    new-instance v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/RadarChartRenderer;

    iget-object v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->mAnimator:Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/ChartAnimator;

    iget-object v2, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->mViewPortHandler:Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;

    invoke-direct {v0, p0, v1, v2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/RadarChartRenderer;-><init>(Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/ChartAnimator;Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;)V

    iput-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->mRenderer:Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/DataRenderer;

    .line 79
    new-instance v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRendererRadarChart;

    iget-object v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->mViewPortHandler:Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;

    iget-object v2, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->mYAxis:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;

    invoke-direct {v0, v1, v2, p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRendererRadarChart;-><init>(Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;)V

    iput-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->mYAxisRenderer:Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRendererRadarChart;

    .line 80
    new-instance v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/XAxisRendererRadarChart;

    iget-object v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->mViewPortHandler:Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;

    iget-object v2, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->mXAxis:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis;

    invoke-direct {v0, v1, v2, p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/XAxisRendererRadarChart;-><init>(Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis;Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;)V

    iput-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->mXAxisRenderer:Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/XAxisRendererRadarChart;

    .line 81
    return-void
.end method

.method public notifyDataSetChanged()V
    .registers 4

    .line 130
    iget-boolean v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->mDataNotSet:Z

    if-eqz v0, :cond_5

    .line 131
    return-void

    .line 133
    :cond_5
    invoke-virtual {p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->calcMinMax()V

    .line 135
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->mYAxis:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->needsDefaultFormatter()Z

    move-result v0

    if-eqz v0, :cond_17

    .line 136
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->mYAxis:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;

    iget-object v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->mDefaultFormatter:Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ValueFormatter;

    invoke-virtual {v0, v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->setValueFormatter(Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ValueFormatter;)V

    .line 139
    :cond_17
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->mYAxisRenderer:Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRendererRadarChart;

    iget-object v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->mYAxis:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;

    iget v1, v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->mAxisMinimum:F

    iget-object v2, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->mYAxis:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;

    iget v2, v2, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->mAxisMaximum:F

    invoke-virtual {v0, v1, v2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRendererRadarChart;->computeAxis(FF)V

    .line 140
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->mXAxisRenderer:Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/XAxisRendererRadarChart;

    iget-object v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->mData:Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;

    check-cast v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/RadarData;

    invoke-virtual {v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/RadarData;->getXValAverageLength()F

    move-result v1

    iget-object v2, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->mData:Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;

    check-cast v2, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/RadarData;

    invoke-virtual {v2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/RadarData;->getXVals()Ljava/util/List;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/XAxisRendererRadarChart;->computeAxis(FLjava/util/List;)V

    .line 142
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->mLegend:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;

    if-eqz v0, :cond_4c

    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->mLegend:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->isLegendCustom()Z

    move-result v0

    if-nez v0, :cond_4c

    .line 143
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->mLegendRenderer:Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/LegendRenderer;

    iget-object v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->mData:Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;

    invoke-virtual {v0, v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/LegendRenderer;->computeLegend(Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;)V

    .line 145
    :cond_4c
    invoke-virtual {p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->calculateOffsets()V

    .line 146
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .registers 4
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .line 150
    invoke-super {p0, p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieRadarChartBase;->onDraw(Landroid/graphics/Canvas;)V

    .line 152
    iget-boolean v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->mDataNotSet:Z

    if-eqz v0, :cond_8

    .line 153
    return-void

    .line 155
    :cond_8
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->mXAxisRenderer:Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/XAxisRendererRadarChart;

    invoke-virtual {v0, p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/XAxisRendererRadarChart;->renderAxisLabels(Landroid/graphics/Canvas;)V

    .line 157
    iget-boolean v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->mDrawWeb:Z

    if-eqz v0, :cond_16

    .line 158
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->mRenderer:Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/DataRenderer;

    invoke-virtual {v0, p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/DataRenderer;->drawExtras(Landroid/graphics/Canvas;)V

    .line 160
    :cond_16
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->mYAxisRenderer:Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRendererRadarChart;

    invoke-virtual {v0, p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRendererRadarChart;->renderLimitLines(Landroid/graphics/Canvas;)V

    .line 162
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->mRenderer:Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/DataRenderer;

    invoke-virtual {v0, p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/DataRenderer;->drawData(Landroid/graphics/Canvas;)V

    .line 164
    invoke-virtual {p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->valuesToHighlight()Z

    move-result v0

    if-eqz v0, :cond_2d

    .line 165
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->mRenderer:Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/DataRenderer;

    iget-object v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->mIndicesToHightlight:[Lbr/com/itau/widgets/graphicalfriedcake/charts/highlight/Highlight;

    invoke-virtual {v0, p1, v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/DataRenderer;->drawHighlighted(Landroid/graphics/Canvas;[Lbr/com/itau/widgets/graphicalfriedcake/charts/highlight/Highlight;)V

    .line 167
    :cond_2d
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->mYAxisRenderer:Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRendererRadarChart;

    invoke-virtual {v0, p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRendererRadarChart;->renderAxisLabels(Landroid/graphics/Canvas;)V

    .line 169
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->mRenderer:Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/DataRenderer;

    invoke-virtual {v0, p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/DataRenderer;->drawValues(Landroid/graphics/Canvas;)V

    .line 171
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->mLegendRenderer:Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/LegendRenderer;

    invoke-virtual {v0, p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/LegendRenderer;->renderLegend(Landroid/graphics/Canvas;)V

    .line 173
    invoke-virtual {p0, p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->drawDescription(Landroid/graphics/Canvas;)V

    .line 175
    invoke-virtual {p0, p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->drawMarkers(Landroid/graphics/Canvas;)V

    .line 176
    return-void
.end method

.method public setDrawWeb(Z)V
    .registers 2
    .param p1, "enabled"    # Z

    .line 316
    iput-boolean p1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->mDrawWeb:Z

    .line 317
    return-void
.end method

.method public setWebAlpha(I)V
    .registers 2
    .param p1, "alpha"    # I

    .line 267
    iput p1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->mWebAlpha:I

    .line 268
    return-void
.end method

.method public setWebColor(I)V
    .registers 2
    .param p1, "color"    # I

    .line 287
    iput p1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->mWebColor:I

    .line 288
    return-void
.end method

.method public setWebColorInner(I)V
    .registers 2
    .param p1, "color"    # I

    .line 302
    iput p1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->mWebColorInner:I

    .line 303
    return-void
.end method

.method public setWebLineWidth(F)V
    .registers 3
    .param p1, "width"    # F

    .line 239
    invoke-static {p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Utils;->convertDpToPixel(F)F

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->mWebLineWidth:F

    .line 240
    return-void
.end method

.method public setWebLineWidthInner(F)V
    .registers 3
    .param p1, "width"    # F

    .line 253
    invoke-static {p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Utils;->convertDpToPixel(F)F

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->mInnerWebLineWidth:F

    .line 254
    return-void
.end method

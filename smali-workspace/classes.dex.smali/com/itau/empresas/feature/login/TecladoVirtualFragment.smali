.class public Lcom/itau/empresas/feature/login/TecladoVirtualFragment;
.super Landroid/support/v4/app/Fragment;
.source "TecladoVirtualFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/itau/empresas/feature/login/TecladoVirtualFragment$TeclaSelecionadaClickListener;
    }
.end annotation


# instance fields
.field private teclaSelecionadaListener:Lcom/itau/empresas/feature/login/TecladoVirtualFragment$TeclaSelecionadaClickListener;

.field private teclas:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Lcom/itau/empresas/feature/login/model/TecladoVirtualVO$Tecla;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 19
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/itau/empresas/feature/login/TecladoVirtualFragment;)Lcom/itau/empresas/feature/login/TecladoVirtualFragment$TeclaSelecionadaClickListener;
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/feature/login/TecladoVirtualFragment;

    .line 19
    iget-object v0, p0, Lcom/itau/empresas/feature/login/TecladoVirtualFragment;->teclaSelecionadaListener:Lcom/itau/empresas/feature/login/TecladoVirtualFragment$TeclaSelecionadaClickListener;

    return-object v0
.end method

.method private associaTeclaAoBotao(Landroid/widget/Button;Lcom/itau/empresas/feature/login/model/TecladoVirtualVO$Tecla;)V
    .registers 8
    .param p1, "botaoTeclado"    # Landroid/widget/Button;
    .param p2, "tecla"    # Lcom/itau/empresas/feature/login/model/TecladoVirtualVO$Tecla;

    .line 77
    if-eqz p2, :cond_32

    .line 78
    invoke-virtual {p2}, Lcom/itau/empresas/feature/login/model/TecladoVirtualVO$Tecla;->getDuplaDeDigitos()Ljava/lang/String;

    move-result-object v4

    .line 79
    .line 80
    .local v4, "digitos":Ljava/lang/String;
    const v0, 0x7f07028f

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/login/TecladoVirtualFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {v4, v2}, Ljava/lang/String;->charAt(I)C

    move-result v2

    invoke-static {v2}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    .line 81
    const/4 v2, 0x1

    invoke-virtual {v4, v2}, Ljava/lang/String;->charAt(I)C

    move-result v2

    invoke-static {v2}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v2

    const/4 v3, 0x1

    aput-object v2, v1, v3

    .line 80
    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 79
    invoke-virtual {p1, v0}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 82
    invoke-virtual {p1, p2}, Landroid/widget/Button;->setTag(Ljava/lang/Object;)V

    .line 85
    .end local v4    # "digitos":Ljava/lang/String;
    :cond_32
    if-eqz p2, :cond_3f

    .line 86
    invoke-virtual {p2}, Lcom/itau/empresas/feature/login/model/TecladoVirtualVO$Tecla;->getCodigo()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/itau/empresas/feature/login/TecladoVirtualFragment;->clickTeclado(Ljava/lang/String;)Landroid/view/View$OnClickListener;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 89
    :cond_3f
    return-void
.end method

.method private carregarTeclas(Landroid/view/View;)V
    .registers 6
    .param p1, "view"    # Landroid/view/View;

    .line 57
    iget-object v0, p0, Lcom/itau/empresas/feature/login/TecladoVirtualFragment;->teclas:Ljava/util/List;

    if-eqz v0, :cond_7f

    .line 58
    const v0, 0x7f0e047b

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iget-object v1, p0, Lcom/itau/empresas/feature/login/TecladoVirtualFragment;->teclas:Ljava/util/List;

    .line 59
    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/itau/empresas/feature/login/model/TecladoVirtualVO$Tecla;

    .line 58
    invoke-direct {p0, v0, v1}, Lcom/itau/empresas/feature/login/TecladoVirtualFragment;->associaTeclaAoBotao(Landroid/widget/Button;Lcom/itau/empresas/feature/login/model/TecladoVirtualVO$Tecla;)V

    .line 60
    const v0, 0x7f0e047f

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iget-object v1, p0, Lcom/itau/empresas/feature/login/TecladoVirtualFragment;->teclas:Ljava/util/List;

    .line 61
    const/4 v2, 0x1

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/itau/empresas/feature/login/model/TecladoVirtualVO$Tecla;

    .line 60
    invoke-direct {p0, v0, v1}, Lcom/itau/empresas/feature/login/TecladoVirtualFragment;->associaTeclaAoBotao(Landroid/widget/Button;Lcom/itau/empresas/feature/login/model/TecladoVirtualVO$Tecla;)V

    .line 62
    const v0, 0x7f0e047c

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iget-object v1, p0, Lcom/itau/empresas/feature/login/TecladoVirtualFragment;->teclas:Ljava/util/List;

    .line 63
    const/4 v2, 0x2

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/itau/empresas/feature/login/model/TecladoVirtualVO$Tecla;

    .line 62
    invoke-direct {p0, v0, v1}, Lcom/itau/empresas/feature/login/TecladoVirtualFragment;->associaTeclaAoBotao(Landroid/widget/Button;Lcom/itau/empresas/feature/login/model/TecladoVirtualVO$Tecla;)V

    .line 64
    const v0, 0x7f0e047d

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iget-object v1, p0, Lcom/itau/empresas/feature/login/TecladoVirtualFragment;->teclas:Ljava/util/List;

    .line 65
    const/4 v2, 0x3

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/itau/empresas/feature/login/model/TecladoVirtualVO$Tecla;

    .line 64
    invoke-direct {p0, v0, v1}, Lcom/itau/empresas/feature/login/TecladoVirtualFragment;->associaTeclaAoBotao(Landroid/widget/Button;Lcom/itau/empresas/feature/login/model/TecladoVirtualVO$Tecla;)V

    .line 66
    const v0, 0x7f0e047e

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iget-object v1, p0, Lcom/itau/empresas/feature/login/TecladoVirtualFragment;->teclas:Ljava/util/List;

    .line 67
    const/4 v2, 0x4

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/itau/empresas/feature/login/model/TecladoVirtualVO$Tecla;

    .line 66
    invoke-direct {p0, v0, v1}, Lcom/itau/empresas/feature/login/TecladoVirtualFragment;->associaTeclaAoBotao(Landroid/widget/Button;Lcom/itau/empresas/feature/login/model/TecladoVirtualVO$Tecla;)V

    .line 69
    .line 70
    const v0, 0x7f0e0480

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Landroid/widget/ImageButton;

    .line 71
    .local v3, "viewById":Landroid/widget/ImageButton;
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/itau/empresas/feature/login/TecladoVirtualFragment;->clickTeclado(Ljava/lang/String;)Landroid/view/View$OnClickListener;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 73
    .end local v3    # "viewById":Landroid/widget/ImageButton;
    :cond_7f
    return-void
.end method

.method private clickTeclado(Ljava/lang/String;)Landroid/view/View$OnClickListener;
    .registers 3
    .param p1, "codigo"    # Ljava/lang/String;

    .line 92
    new-instance v0, Lcom/itau/empresas/feature/login/TecladoVirtualFragment$1;

    invoke-direct {v0, p0, p1}, Lcom/itau/empresas/feature/login/TecladoVirtualFragment$1;-><init>(Lcom/itau/empresas/feature/login/TecladoVirtualFragment;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public onAttach(Landroid/content/Context;)V
    .registers 4
    .param p1, "context"    # Landroid/content/Context;

    .line 37
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onAttach(Landroid/content/Context;)V

    .line 38
    iget-object v0, p0, Lcom/itau/empresas/feature/login/TecladoVirtualFragment;->teclaSelecionadaListener:Lcom/itau/empresas/feature/login/TecladoVirtualFragment$TeclaSelecionadaClickListener;

    if-nez v0, :cond_10

    instance-of v0, p1, Lcom/itau/empresas/feature/login/TecladoVirtualFragment$TeclaSelecionadaClickListener;

    if-eqz v0, :cond_10

    .line 39
    move-object v0, p1

    check-cast v0, Lcom/itau/empresas/feature/login/TecladoVirtualFragment$TeclaSelecionadaClickListener;

    iput-object v0, p0, Lcom/itau/empresas/feature/login/TecladoVirtualFragment;->teclaSelecionadaListener:Lcom/itau/empresas/feature/login/TecladoVirtualFragment$TeclaSelecionadaClickListener;

    .line 40
    :cond_10
    iget-object v0, p0, Lcom/itau/empresas/feature/login/TecladoVirtualFragment;->teclaSelecionadaListener:Lcom/itau/empresas/feature/login/TecladoVirtualFragment$TeclaSelecionadaClickListener;

    if-nez v0, :cond_21

    .line 41
    new-instance v0, Ljava/lang/IllegalArgumentException;

    .line 42
    const v1, 0x7f070450

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/login/TecladoVirtualFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 43
    :cond_21
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 47
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 48
    invoke-virtual {p0}, Lcom/itau/empresas/feature/login/TecladoVirtualFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_16

    .line 49
    invoke-virtual {p0}, Lcom/itau/empresas/feature/login/TecladoVirtualFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "TECLADO_ARG"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/login/TecladoVirtualFragment;->teclas:Ljava/util/List;

    goto :goto_23

    .line 51
    :cond_16
    new-instance v0, Ljava/lang/IllegalArgumentException;

    .line 52
    const v1, 0x7f07044f

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/login/TecladoVirtualFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 54
    :goto_23
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .registers 7
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .line 29
    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    .line 30
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    const v1, 0x7f0300c1

    invoke-virtual {p1, v1, p2, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 31
    .local v2, "view":Landroid/view/View;
    invoke-direct {p0, v2}, Lcom/itau/empresas/feature/login/TecladoVirtualFragment;->carregarTeclas(Landroid/view/View;)V

    .line 32
    return-object v2
.end method

.method public setTeclaSelecionadaListener(Lcom/itau/empresas/feature/login/TecladoVirtualFragment$TeclaSelecionadaClickListener;)V
    .registers 2
    .param p1, "teclaSelecionadaListener"    # Lcom/itau/empresas/feature/login/TecladoVirtualFragment$TeclaSelecionadaClickListener;

    .line 105
    iput-object p1, p0, Lcom/itau/empresas/feature/login/TecladoVirtualFragment;->teclaSelecionadaListener:Lcom/itau/empresas/feature/login/TecladoVirtualFragment$TeclaSelecionadaClickListener;

    .line 106
    return-void
.end method

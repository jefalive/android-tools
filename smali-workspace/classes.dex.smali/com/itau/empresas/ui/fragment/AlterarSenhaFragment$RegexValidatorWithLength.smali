.class final Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment$RegexValidatorWithLength;
.super Lbr/com/itau/widgets/material/validation/METValidator;
.source "AlterarSenhaFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "RegexValidatorWithLength"
.end annotation


# instance fields
.field private campo:Lbr/com/itau/widgets/material/MaterialEditText;

.field private pattern:Ljava/util/regex/Pattern;

.field private stringFormat:Ljava/lang/String;


# direct methods
.method constructor <init>(Lbr/com/itau/widgets/material/MaterialEditText;Ljava/lang/String;Ljava/lang/String;)V
    .registers 7
    .param p1, "campo"    # Lbr/com/itau/widgets/material/MaterialEditText;
    .param p2, "stringFormat"    # Ljava/lang/String;
    .param p3, "regex"    # Ljava/lang/String;

    .line 299
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    invoke-static {p2, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lbr/com/itau/widgets/material/validation/METValidator;-><init>(Ljava/lang/String;)V

    .line 300
    iput-object p1, p0, Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment$RegexValidatorWithLength;->campo:Lbr/com/itau/widgets/material/MaterialEditText;

    .line 301
    iput-object p2, p0, Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment$RegexValidatorWithLength;->stringFormat:Ljava/lang/String;

    .line 302
    invoke-static {p3}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment$RegexValidatorWithLength;->pattern:Ljava/util/regex/Pattern;

    .line 303
    return-void
.end method


# virtual methods
.method public getErrorMessage()Ljava/lang/String;
    .registers 5

    .line 308
    iget-object v0, p0, Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment$RegexValidatorWithLength;->stringFormat:Ljava/lang/String;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment$RegexValidatorWithLength;->campo:Lbr/com/itau/widgets/material/MaterialEditText;

    invoke-virtual {v2}, Lbr/com/itau/widgets/material/MaterialEditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-interface {v2}, Landroid/text/Editable;->length()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public isValid(Ljava/lang/CharSequence;Z)Z
    .registers 4
    .param p1, "text"    # Ljava/lang/CharSequence;
    .param p2, "isEmpty"    # Z

    .line 312
    iget-object v0, p0, Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment$RegexValidatorWithLength;->pattern:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    return v0
.end method

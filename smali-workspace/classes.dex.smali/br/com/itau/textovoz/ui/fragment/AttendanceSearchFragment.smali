.class public Lbr/com/itau/textovoz/ui/fragment/AttendanceSearchFragment;
.super Landroid/support/v4/app/Fragment;
.source "AttendanceSearchFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lbr/com/itau/textovoz/ui/fragment/AttendanceSearchFragment$AttendanceListener;
    }
.end annotation


# instance fields
.field private attendanceCardView:Landroid/support/v7/widget/CardView;

.field private listener:Lbr/com/itau/textovoz/ui/fragment/AttendanceSearchFragment$AttendanceListener;


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 17
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lbr/com/itau/textovoz/ui/fragment/AttendanceSearchFragment;)Lbr/com/itau/textovoz/ui/fragment/AttendanceSearchFragment$AttendanceListener;
    .registers 2
    .param p0, "x0"    # Lbr/com/itau/textovoz/ui/fragment/AttendanceSearchFragment;

    .line 17
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/fragment/AttendanceSearchFragment;->listener:Lbr/com/itau/textovoz/ui/fragment/AttendanceSearchFragment$AttendanceListener;

    return-object v0
.end method


# virtual methods
.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .registers 7
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .line 33
    sget v0, Lbr/com/itau/textovoz/R$layout;->fragment_search_attendance:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 35
    .local v2, "v":Landroid/view/View;
    sget v0, Lbr/com/itau/textovoz/R$id;->card_view_need_help_tel:I

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/CardView;

    iput-object v0, p0, Lbr/com/itau/textovoz/ui/fragment/AttendanceSearchFragment;->attendanceCardView:Landroid/support/v7/widget/CardView;

    .line 36
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/fragment/AttendanceSearchFragment;->attendanceCardView:Landroid/support/v7/widget/CardView;

    new-instance v1, Lbr/com/itau/textovoz/ui/fragment/AttendanceSearchFragment$1;

    invoke-direct {v1, p0}, Lbr/com/itau/textovoz/ui/fragment/AttendanceSearchFragment$1;-><init>(Lbr/com/itau/textovoz/ui/fragment/AttendanceSearchFragment;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/CardView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 45
    return-object v2
.end method

.method public setAttendanceListener(Lbr/com/itau/textovoz/ui/fragment/AttendanceSearchFragment$AttendanceListener;)V
    .registers 2
    .param p1, "listener"    # Lbr/com/itau/textovoz/ui/fragment/AttendanceSearchFragment$AttendanceListener;

    .line 27
    iput-object p1, p0, Lbr/com/itau/textovoz/ui/fragment/AttendanceSearchFragment;->listener:Lbr/com/itau/textovoz/ui/fragment/AttendanceSearchFragment$AttendanceListener;

    .line 28
    return-void
.end method

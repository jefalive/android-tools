.class public Lcom/itau/animacao/loading/LoadingView;
.super Landroid/view/View;
.source "LoadingView.java"


# instance fields
.field private angle:F

.field private corIndicador:I

.field private corPreenchimento:I

.field private paint:Landroid/graphics/Paint;

.field private paintInner:Landroid/graphics/Paint;

.field private rect:Landroid/graphics/RectF;

.field private startAngle:F

.field private strokeWidth:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .line 32
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 33
    sget v0, Lcom/itau/animacao/R$style;->LoadingView:I

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2, v1, v0}, Lcom/itau/animacao/loading/LoadingView;->init(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    .line 34
    return-void
.end method

.method private desenhaCirculo()V
    .registers 3

    .line 48
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/itau/animacao/loading/LoadingView;->paint:Landroid/graphics/Paint;

    .line 49
    iget-object v0, p0, Lcom/itau/animacao/loading/LoadingView;->paint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 50
    iget-object v0, p0, Lcom/itau/animacao/loading/LoadingView;->paint:Landroid/graphics/Paint;

    iget v1, p0, Lcom/itau/animacao/loading/LoadingView;->strokeWidth:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 51
    iget-object v0, p0, Lcom/itau/animacao/loading/LoadingView;->paint:Landroid/graphics/Paint;

    iget v1, p0, Lcom/itau/animacao/loading/LoadingView;->corIndicador:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 52
    iget-object v0, p0, Lcom/itau/animacao/loading/LoadingView;->paint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Cap;->ROUND:Landroid/graphics/Paint$Cap;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    .line 54
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/itau/animacao/loading/LoadingView;->paintInner:Landroid/graphics/Paint;

    .line 55
    iget-object v0, p0, Lcom/itau/animacao/loading/LoadingView;->paintInner:Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 56
    iget-object v0, p0, Lcom/itau/animacao/loading/LoadingView;->paintInner:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 57
    iget-object v0, p0, Lcom/itau/animacao/loading/LoadingView;->paintInner:Landroid/graphics/Paint;

    iget v1, p0, Lcom/itau/animacao/loading/LoadingView;->corPreenchimento:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 58
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/itau/animacao/loading/LoadingView;->rect:Landroid/graphics/RectF;

    .line 59
    const/4 v0, 0x0

    iput v0, p0, Lcom/itau/animacao/loading/LoadingView;->angle:F

    .line 60
    return-void
.end method

.method private init(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .registers 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I
    .param p4, "defStyleRes"    # I

    .line 38
    sget-object v0, Lcom/itau/animacao/R$styleable;->LoadingView:[I

    .line 39
    invoke-virtual {p1, p2, v0, p3, p4}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v2

    .line 40
    .local v2, "typedArray":Landroid/content/res/TypedArray;
    sget v0, Lcom/itau/animacao/R$styleable;->LoadingView_strokeWidth:I

    const/16 v1, 0xc

    invoke-virtual {v2, v0, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    iput v0, p0, Lcom/itau/animacao/loading/LoadingView;->strokeWidth:I

    .line 41
    sget v0, Lcom/itau/animacao/R$styleable;->LoadingView_corIndicador:I

    const/4 v1, -0x1

    invoke-virtual {v2, v0, v1}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    iput v0, p0, Lcom/itau/animacao/loading/LoadingView;->corIndicador:I

    .line 42
    sget v0, Lcom/itau/animacao/R$styleable;->LoadingView_corPreenchimento:I

    .line 43
    const/high16 v1, -0x1000000

    invoke-virtual {v2, v0, v1}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    iput v0, p0, Lcom/itau/animacao/loading/LoadingView;->corPreenchimento:I

    .line 44
    invoke-direct {p0}, Lcom/itau/animacao/loading/LoadingView;->desenhaCirculo()V

    .line 45
    return-void
.end method

.method private updateRect(Landroid/graphics/Canvas;)V
    .registers 10
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .line 72
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v3

    .line 73
    .local v3, "height":I
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v4

    .line 74
    .local v4, "width":I
    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v0

    iget v1, p0, Lcom/itau/animacao/loading/LoadingView;->strokeWidth:I

    mul-int/lit8 v1, v1, 0x2

    sub-int/2addr v0, v1

    int-to-float v5, v0

    .line 75
    .local v5, "size":F
    div-int/lit8 v0, v4, 0x2

    int-to-float v0, v0

    const/high16 v1, 0x40000000    # 2.0f

    div-float v1, v5, v1

    sub-float/2addr v0, v1

    iget v1, p0, Lcom/itau/animacao/loading/LoadingView;->strokeWidth:I

    int-to-float v1, v1

    add-float v6, v0, v1

    .line 76
    .local v6, "x":F
    div-int/lit8 v0, v3, 0x2

    int-to-float v0, v0

    const/high16 v1, 0x40000000    # 2.0f

    div-float v1, v5, v1

    sub-float/2addr v0, v1

    iget v1, p0, Lcom/itau/animacao/loading/LoadingView;->strokeWidth:I

    int-to-float v1, v1

    add-float v7, v0, v1

    .line 78
    .local v7, "y":F
    iget-object v0, p0, Lcom/itau/animacao/loading/LoadingView;->rect:Landroid/graphics/RectF;

    invoke-virtual {v0, v6, v7, v5, v5}, Landroid/graphics/RectF;->set(FFFF)V

    .line 79
    return-void
.end method


# virtual methods
.method public getAngle()F
    .registers 2

    .line 87
    iget v0, p0, Lcom/itau/animacao/loading/LoadingView;->angle:F

    return v0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .registers 8
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .line 64
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 65
    iget-object v0, p0, Lcom/itau/animacao/loading/LoadingView;->rect:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_e

    .line 66
    invoke-direct {p0, p1}, Lcom/itau/animacao/loading/LoadingView;->updateRect(Landroid/graphics/Canvas;)V

    .line 67
    :cond_e
    iget-object v0, p0, Lcom/itau/animacao/loading/LoadingView;->rect:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->centerX()F

    move-result v0

    iget-object v1, p0, Lcom/itau/animacao/loading/LoadingView;->rect:Landroid/graphics/RectF;

    invoke-virtual {v1}, Landroid/graphics/RectF;->centerY()F

    move-result v1

    iget-object v2, p0, Lcom/itau/animacao/loading/LoadingView;->paintInner:Landroid/graphics/Paint;

    const/high16 v3, 0x43870000    # 270.0f

    invoke-virtual {p1, v0, v1, v3, v2}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 68
    move-object v0, p1

    iget-object v1, p0, Lcom/itau/animacao/loading/LoadingView;->rect:Landroid/graphics/RectF;

    iget v2, p0, Lcom/itau/animacao/loading/LoadingView;->startAngle:F

    const/high16 v3, -0x3d4c0000    # -90.0f

    add-float/2addr v2, v3

    iget v3, p0, Lcom/itau/animacao/loading/LoadingView;->angle:F

    iget-object v5, p0, Lcom/itau/animacao/loading/LoadingView;->paint:Landroid/graphics/Paint;

    const/4 v4, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    .line 69
    return-void
.end method

.method protected onMeasure(II)V
    .registers 3
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .line 83
    invoke-super {p0, p1, p2}, Landroid/view/View;->onMeasure(II)V

    .line 84
    return-void
.end method

.method public setAngle(F)V
    .registers 2
    .param p1, "angle"    # F

    .line 91
    iput p1, p0, Lcom/itau/animacao/loading/LoadingView;->angle:F

    .line 92
    return-void
.end method

.method public setStartAngle(F)V
    .registers 2
    .param p1, "startAngle"    # F

    .line 95
    iput p1, p0, Lcom/itau/animacao/loading/LoadingView;->startAngle:F

    .line 96
    return-void
.end method

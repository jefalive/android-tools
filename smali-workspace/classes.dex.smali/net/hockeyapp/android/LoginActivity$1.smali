.class Lnet/hockeyapp/android/LoginActivity$1;
.super Landroid/os/Handler;
.source "LoginActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lnet/hockeyapp/android/LoginActivity;->initLoginHandler()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/hockeyapp/android/LoginActivity;


# direct methods
.method constructor <init>(Lnet/hockeyapp/android/LoginActivity;)V
    .registers 2
    .param p1, "this$0"    # Lnet/hockeyapp/android/LoginActivity;

    .line 117
    iput-object p1, p0, Lnet/hockeyapp/android/LoginActivity$1;->this$0:Lnet/hockeyapp/android/LoginActivity;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 7
    .param p1, "msg"    # Landroid/os/Message;

    .line 120
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v3

    .line 121
    .local v3, "bundle":Landroid/os/Bundle;
    const-string v0, "success"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v4

    .line 123
    .local v4, "success":Z
    if-eqz v4, :cond_1b

    .line 124
    iget-object v0, p0, Lnet/hockeyapp/android/LoginActivity$1;->this$0:Lnet/hockeyapp/android/LoginActivity;

    invoke-virtual {v0}, Lnet/hockeyapp/android/LoginActivity;->finish()V

    .line 126
    sget-object v0, Lnet/hockeyapp/android/LoginManager;->listener:Lnet/hockeyapp/android/LoginManagerListener;

    if-eqz v0, :cond_28

    .line 127
    sget-object v0, Lnet/hockeyapp/android/LoginManager;->listener:Lnet/hockeyapp/android/LoginManagerListener;

    invoke-virtual {v0}, Lnet/hockeyapp/android/LoginManagerListener;->onSuccess()V

    goto :goto_28

    .line 131
    :cond_1b
    iget-object v0, p0, Lnet/hockeyapp/android/LoginActivity$1;->this$0:Lnet/hockeyapp/android/LoginActivity;

    const-string v1, "Login failed. Check your credentials."

    const/16 v2, 0x7d0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 133
    :cond_28
    :goto_28
    return-void
.end method

.class Lbr/com/itau/widgets/material/MaterialEditText$2;
.super Ljava/lang/Object;
.source "MaterialEditText.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lbr/com/itau/widgets/material/MaterialEditText;->initFloatingLabel()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lbr/com/itau/widgets/material/MaterialEditText;


# direct methods
.method constructor <init>(Lbr/com/itau/widgets/material/MaterialEditText;)V
    .registers 2

    .line 923
    iput-object p1, p0, Lbr/com/itau/widgets/material/MaterialEditText$2;->this$0:Lbr/com/itau/widgets/material/MaterialEditText;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .registers 4
    .param p1, "s"    # Landroid/text/Editable;

    .line 934
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialEditText$2;->this$0:Lbr/com/itau/widgets/material/MaterialEditText;

    # getter for: Lbr/com/itau/widgets/material/MaterialEditText;->floatingLabelEnabled:Z
    invoke-static {v0}, Lbr/com/itau/widgets/material/MaterialEditText;->access$200(Lbr/com/itau/widgets/material/MaterialEditText;)Z

    move-result v0

    if-eqz v0, :cond_3d

    .line 935
    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v0

    if-nez v0, :cond_26

    .line 936
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialEditText$2;->this$0:Lbr/com/itau/widgets/material/MaterialEditText;

    # getter for: Lbr/com/itau/widgets/material/MaterialEditText;->floatingLabelShown:Z
    invoke-static {v0}, Lbr/com/itau/widgets/material/MaterialEditText;->access$300(Lbr/com/itau/widgets/material/MaterialEditText;)Z

    move-result v0

    if-eqz v0, :cond_3d

    .line 937
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialEditText$2;->this$0:Lbr/com/itau/widgets/material/MaterialEditText;

    const/4 v1, 0x0

    # setter for: Lbr/com/itau/widgets/material/MaterialEditText;->floatingLabelShown:Z
    invoke-static {v0, v1}, Lbr/com/itau/widgets/material/MaterialEditText;->access$302(Lbr/com/itau/widgets/material/MaterialEditText;Z)Z

    .line 938
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialEditText$2;->this$0:Lbr/com/itau/widgets/material/MaterialEditText;

    # invokes: Lbr/com/itau/widgets/material/MaterialEditText;->getLabelAnimator()Lcom/nineoldandroids/animation/ObjectAnimator;
    invoke-static {v0}, Lbr/com/itau/widgets/material/MaterialEditText;->access$400(Lbr/com/itau/widgets/material/MaterialEditText;)Lcom/nineoldandroids/animation/ObjectAnimator;

    move-result-object v0

    invoke-virtual {v0}, Lcom/nineoldandroids/animation/ObjectAnimator;->reverse()V

    goto :goto_3d

    .line 940
    :cond_26
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialEditText$2;->this$0:Lbr/com/itau/widgets/material/MaterialEditText;

    # getter for: Lbr/com/itau/widgets/material/MaterialEditText;->floatingLabelShown:Z
    invoke-static {v0}, Lbr/com/itau/widgets/material/MaterialEditText;->access$300(Lbr/com/itau/widgets/material/MaterialEditText;)Z

    move-result v0

    if-nez v0, :cond_3d

    .line 941
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialEditText$2;->this$0:Lbr/com/itau/widgets/material/MaterialEditText;

    const/4 v1, 0x1

    # setter for: Lbr/com/itau/widgets/material/MaterialEditText;->floatingLabelShown:Z
    invoke-static {v0, v1}, Lbr/com/itau/widgets/material/MaterialEditText;->access$302(Lbr/com/itau/widgets/material/MaterialEditText;Z)Z

    .line 942
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialEditText$2;->this$0:Lbr/com/itau/widgets/material/MaterialEditText;

    # invokes: Lbr/com/itau/widgets/material/MaterialEditText;->getLabelAnimator()Lcom/nineoldandroids/animation/ObjectAnimator;
    invoke-static {v0}, Lbr/com/itau/widgets/material/MaterialEditText;->access$400(Lbr/com/itau/widgets/material/MaterialEditText;)Lcom/nineoldandroids/animation/ObjectAnimator;

    move-result-object v0

    invoke-virtual {v0}, Lcom/nineoldandroids/animation/ObjectAnimator;->start()V

    .line 945
    :cond_3d
    :goto_3d
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .registers 5
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "count"    # I
    .param p4, "after"    # I

    .line 926
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .registers 5
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "before"    # I
    .param p4, "count"    # I

    .line 930
    return-void
.end method

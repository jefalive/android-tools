.class public Lcom/google/android/gms/internal/zzci;
.super Lcom/google/android/gms/ads/formats/NativeAd$Image;


# annotations
.annotation runtime Lcom/google/android/gms/internal/zzhb;
.end annotation


# instance fields
.field private final mDrawable:Landroid/graphics/drawable/Drawable;

.field private final mUri:Landroid/net/Uri;

.field private final zzxV:D

.field private final zzyL:Lcom/google/android/gms/internal/zzch;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/internal/zzch;)V
    .registers 9

    invoke-direct {p0}, Lcom/google/android/gms/ads/formats/NativeAd$Image;-><init>()V

    iput-object p1, p0, Lcom/google/android/gms/internal/zzci;->zzyL:Lcom/google/android/gms/internal/zzch;

    const/4 v2, 0x0

    :try_start_6
    iget-object v0, p0, Lcom/google/android/gms/internal/zzci;->zzyL:Lcom/google/android/gms/internal/zzch;

    invoke-interface {v0}, Lcom/google/android/gms/internal/zzch;->zzdJ()Lcom/google/android/gms/dynamic/zzd;

    move-result-object v3

    if-eqz v3, :cond_15

    invoke-static {v3}, Lcom/google/android/gms/dynamic/zze;->zzp(Lcom/google/android/gms/dynamic/zzd;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/Drawable;
    :try_end_14
    .catch Landroid/os/RemoteException; {:try_start_6 .. :try_end_14} :catch_16

    move-object v2, v0

    :cond_15
    goto :goto_1c

    :catch_16
    move-exception v3

    const-string v0, "Failed to get drawable."

    invoke-static {v0, v3}, Lcom/google/android/gms/ads/internal/util/client/zzb;->zzb(Ljava/lang/String;Ljava/lang/Throwable;)V

    :goto_1c
    iput-object v2, p0, Lcom/google/android/gms/internal/zzci;->mDrawable:Landroid/graphics/drawable/Drawable;

    const/4 v3, 0x0

    :try_start_1f
    iget-object v0, p0, Lcom/google/android/gms/internal/zzci;->zzyL:Lcom/google/android/gms/internal/zzch;

    invoke-interface {v0}, Lcom/google/android/gms/internal/zzch;->getUri()Landroid/net/Uri;
    :try_end_24
    .catch Landroid/os/RemoteException; {:try_start_1f .. :try_end_24} :catch_27

    move-result-object v0

    move-object v3, v0

    goto :goto_2d

    :catch_27
    move-exception v4

    const-string v0, "Failed to get uri."

    invoke-static {v0, v4}, Lcom/google/android/gms/ads/internal/util/client/zzb;->zzb(Ljava/lang/String;Ljava/lang/Throwable;)V

    :goto_2d
    iput-object v3, p0, Lcom/google/android/gms/internal/zzci;->mUri:Landroid/net/Uri;

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    :try_start_31
    iget-object v0, p0, Lcom/google/android/gms/internal/zzci;->zzyL:Lcom/google/android/gms/internal/zzch;

    invoke-interface {v0}, Lcom/google/android/gms/internal/zzch;->getScale()D
    :try_end_36
    .catch Landroid/os/RemoteException; {:try_start_31 .. :try_end_36} :catch_39

    move-result-wide v0

    move-wide v4, v0

    goto :goto_3f

    :catch_39
    move-exception v6

    const-string v0, "Failed to get scale."

    invoke-static {v0, v6}, Lcom/google/android/gms/ads/internal/util/client/zzb;->zzb(Ljava/lang/String;Ljava/lang/Throwable;)V

    :goto_3f
    iput-wide v4, p0, Lcom/google/android/gms/internal/zzci;->zzxV:D

    return-void
.end method


# virtual methods
.method public getDrawable()Landroid/graphics/drawable/Drawable;
    .registers 2

    iget-object v0, p0, Lcom/google/android/gms/internal/zzci;->mDrawable:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public getScale()D
    .registers 3

    iget-wide v0, p0, Lcom/google/android/gms/internal/zzci;->zzxV:D

    return-wide v0
.end method

.method public getUri()Landroid/net/Uri;
    .registers 2

    iget-object v0, p0, Lcom/google/android/gms/internal/zzci;->mUri:Landroid/net/Uri;

    return-object v0
.end method

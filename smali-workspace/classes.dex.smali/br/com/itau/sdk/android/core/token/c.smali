.class public final Lbr/com/itau/sdk/android/core/token/c;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lbr/com/itau/sdk/android/core/token/c$a;
    }
.end annotation


# static fields
.field private static final b:[B

.field private static c:I


# instance fields
.field private a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    const/16 v0, 0x23

    new-array v0, v0, [B

    fill-array-data v0, :array_e

    sput-object v0, Lbr/com/itau/sdk/android/core/token/c;->b:[B

    const/16 v0, 0x70

    sput v0, Lbr/com/itau/sdk/android/core/token/c;->c:I

    return-void

    :array_e
    .array-data 1
        0x54t
        0x12t
        0x44t
        -0x51t
        -0x6t
        -0x5t
        -0x7t
        0x8t
        -0x1ct
        -0x7t
        0x5t
        -0x7t
        0x5t
        0x13t
        -0xbt
        0x3t
        0x7t
        -0xat
        -0xct
        -0x1ft
        0x1dt
        0xct
        -0xct
        0x1t
        0x6t
        -0xct
        0x10t
        -0x1ft
        0x1at
        -0x5t
        -0x7t
        0x8t
        -0x2et
        0x2et
        -0x1t
    .end array-data
.end method

.method private constructor <init>()V
    .registers 1

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    return-void
.end method

.method synthetic constructor <init>(Lbr/com/itau/sdk/android/core/token/i;)V
    .registers 2

    .line 17
    invoke-direct {p0}, Lbr/com/itau/sdk/android/core/token/c;-><init>()V

    return-void
.end method

.method private static a(III)Ljava/lang/String;
    .registers 9

    const/4 v5, 0x0

    new-instance v0, Ljava/lang/String;

    add-int/lit8 p2, p2, 0x4

    mul-int/lit8 p1, p1, 0x5

    add-int/lit8 p1, p1, 0x3

    sget-object v4, Lbr/com/itau/sdk/android/core/token/c;->b:[B

    rsub-int/lit8 p0, p0, 0x74

    new-array v1, p1, [B

    add-int/lit8 p1, p1, -0x1

    if-nez v4, :cond_18

    move v2, p1

    move v3, p0

    :goto_15
    add-int/2addr v2, v3

    add-int/lit8 p0, v2, 0x1

    :cond_18
    add-int/lit8 p2, p2, 0x1

    int-to-byte v2, p0

    aput-byte v2, v1, v5

    if-ne v5, p1, :cond_24

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Ljava/lang/String;-><init>([BI)V

    return-object v0

    :cond_24
    add-int/lit8 v5, v5, 0x1

    move v2, p0

    aget-byte v3, v4, p2

    goto :goto_15
.end method

.method private synthetic a(Lbr/com/itau/sdk/android/core/model/TokenRequestDTO;)V
    .registers 3

    .line 27
    invoke-direct {p0}, Lbr/com/itau/sdk/android/core/token/c;->f()Lbr/com/itau/sdk/android/core/token/b;

    move-result-object v0

    invoke-interface {v0, p1}, Lbr/com/itau/sdk/android/core/token/b;->a(Lbr/com/itau/sdk/android/core/model/TokenRequestDTO;)Lbr/com/itau/sdk/android/core/SDKCustomUiController$EnvioSMSResponseDTO;

    move-result-object v0

    invoke-direct {p0, v0}, Lbr/com/itau/sdk/android/core/token/c;->a(Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic a(Lbr/com/itau/sdk/android/core/token/c;Lbr/com/itau/sdk/android/core/model/TokenRequestDTO;)V
    .registers 2

    invoke-direct {p0, p1}, Lbr/com/itau/sdk/android/core/token/c;->a(Lbr/com/itau/sdk/android/core/model/TokenRequestDTO;)V

    return-void
.end method

.method static synthetic a(Lbr/com/itau/sdk/android/core/token/c;Ljava/lang/String;)V
    .registers 2

    invoke-direct {p0, p1}, Lbr/com/itau/sdk/android/core/token/c;->e(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lbr/com/itau/sdk/android/core/token/c;Ljava/lang/String;Ljava/lang/String;)V
    .registers 3

    invoke-direct {p0, p1, p2}, Lbr/com/itau/sdk/android/core/token/c;->g(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private a(Ljava/lang/Object;)V
    .registers 3

    .line 113
    invoke-static {}, Lde/greenrobot/event/EventBus;->getDefault()Lde/greenrobot/event/EventBus;

    move-result-object v0

    invoke-virtual {v0, p1}, Lde/greenrobot/event/EventBus;->post(Ljava/lang/Object;)V

    .line 114
    return-void
.end method

.method static synthetic b(Lbr/com/itau/sdk/android/core/token/c;Ljava/lang/String;Ljava/lang/String;)V
    .registers 3

    invoke-direct {p0, p1, p2}, Lbr/com/itau/sdk/android/core/token/c;->f(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic c(Lbr/com/itau/sdk/android/core/token/c;Ljava/lang/String;Ljava/lang/String;)V
    .registers 3

    invoke-direct {p0, p1, p2}, Lbr/com/itau/sdk/android/core/token/c;->e(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public static d()Lbr/com/itau/sdk/android/core/token/c;
    .registers 1

    .line 121
    invoke-static {}, Lbr/com/itau/sdk/android/core/token/c$a;->a()Lbr/com/itau/sdk/android/core/token/c;

    move-result-object v0

    return-object v0
.end method

.method private e()Lbr/com/itau/sdk/android/core/model/TokenAppRequestDTO;
    .registers 3

    .line 105
    new-instance v0, Lbr/com/itau/sdk/android/core/model/TokenAppRequestDTO;

    invoke-static {}, Lbr/com/itau/sdk/android/core/BackendClient;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lbr/com/itau/security/token/til/Token;->generateDeviceID(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lbr/com/itau/sdk/android/core/model/TokenAppRequestDTO;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method private synthetic e(Ljava/lang/String;)V
    .registers 4

    .line 101
    invoke-direct {p0}, Lbr/com/itau/sdk/android/core/token/c;->f()Lbr/com/itau/sdk/android/core/token/b;

    move-result-object v0

    new-instance v1, Lbr/com/itau/sdk/android/core/model/PasswordDTO;

    invoke-direct {v1, p1}, Lbr/com/itau/sdk/android/core/model/PasswordDTO;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v1}, Lbr/com/itau/sdk/android/core/token/b;->a(Lbr/com/itau/sdk/android/core/model/PasswordDTO;)Lbr/com/itau/sdk/android/core/SDKCustomUiController$ValidatePasswordResponseDTO;

    move-result-object v0

    invoke-direct {p0, v0}, Lbr/com/itau/sdk/android/core/token/c;->a(Ljava/lang/Object;)V

    return-void
.end method

.method private synthetic e(Ljava/lang/String;Ljava/lang/String;)V
    .registers 5

    .line 53
    invoke-direct {p0}, Lbr/com/itau/sdk/android/core/token/c;->f()Lbr/com/itau/sdk/android/core/token/b;

    move-result-object v0

    new-instance v1, Lbr/com/itau/sdk/android/core/model/TokenRequestDTO;

    invoke-direct {v1, p1, p2}, Lbr/com/itau/sdk/android/core/model/TokenRequestDTO;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Lbr/com/itau/sdk/android/core/token/b;->d(Lbr/com/itau/sdk/android/core/model/TokenRequestDTO;)Lbr/com/itau/sdk/android/core/SDKCustomUiController$ValidacaoFisicoResponseDTO;

    move-result-object v0

    invoke-direct {p0, v0}, Lbr/com/itau/sdk/android/core/token/c;->a(Ljava/lang/Object;)V

    return-void
.end method

.method private f()Lbr/com/itau/sdk/android/core/token/b;
    .registers 2

    .line 109
    const-class v0, Lbr/com/itau/sdk/android/core/token/b;

    invoke-static {v0}, Lbr/com/itau/sdk/android/core/BackendClient;->api(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbr/com/itau/sdk/android/core/token/b;

    return-object v0
.end method

.method private synthetic f(Ljava/lang/String;Ljava/lang/String;)V
    .registers 4

    .line 37
    invoke-virtual {p0, p1, p2}, Lbr/com/itau/sdk/android/core/token/c;->c(Ljava/lang/String;Ljava/lang/String;)Lbr/com/itau/sdk/android/core/SDKCustomUiController$ValidacaoAppResponseDTO;

    move-result-object v0

    .line 38
    invoke-direct {p0, v0}, Lbr/com/itau/sdk/android/core/token/c;->a(Ljava/lang/Object;)V

    .line 39
    return-void
.end method

.method private synthetic g(Ljava/lang/String;Ljava/lang/String;)V
    .registers 5

    .line 32
    invoke-direct {p0}, Lbr/com/itau/sdk/android/core/token/c;->f()Lbr/com/itau/sdk/android/core/token/b;

    move-result-object v0

    new-instance v1, Lbr/com/itau/sdk/android/core/model/TokenRequestDTO;

    invoke-direct {v1, p1, p2}, Lbr/com/itau/sdk/android/core/model/TokenRequestDTO;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Lbr/com/itau/sdk/android/core/token/b;->b(Lbr/com/itau/sdk/android/core/model/TokenRequestDTO;)Lbr/com/itau/sdk/android/core/SDKCustomUiController$ValidacaoSMSResponseDTO;

    move-result-object v0

    invoke-direct {p0, v0}, Lbr/com/itau/sdk/android/core/token/c;->a(Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;)Lbr/com/itau/sdk/android/core/SDKCustomUiController$ValidacaoAppResponseDTO;
    .registers 3

    .line 43
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lbr/com/itau/sdk/android/core/token/c;->c(Ljava/lang/String;Ljava/lang/String;)Lbr/com/itau/sdk/android/core/SDKCustomUiController$ValidacaoAppResponseDTO;

    move-result-object v0

    return-object v0
.end method

.method public a()Lcom/google/gson/JsonElement;
    .registers 3

    .line 57
    invoke-direct {p0}, Lbr/com/itau/sdk/android/core/token/c;->f()Lbr/com/itau/sdk/android/core/token/b;

    move-result-object v0

    invoke-direct {p0}, Lbr/com/itau/sdk/android/core/token/c;->e()Lbr/com/itau/sdk/android/core/model/TokenAppRequestDTO;

    move-result-object v1

    invoke-interface {v0, v1}, Lbr/com/itau/sdk/android/core/token/b;->a(Lbr/com/itau/sdk/android/core/model/TokenAppRequestDTO;)Lcom/google/gson/JsonElement;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .registers 5

    .line 31
    invoke-static {}, Lbr/com/itau/sdk/android/core/BackendClient;->getInstance()Lbr/com/itau/sdk/android/core/BackendClient;

    move-result-object v0

    invoke-static {p0, p1, p2}, Lbr/com/itau/sdk/android/core/token/e;->a(Lbr/com/itau/sdk/android/core/token/c;Ljava/lang/String;Ljava/lang/String;)Lbr/com/itau/sdk/android/core/type/TokenCall;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbr/com/itau/sdk/android/core/BackendClient;->call(Lbr/com/itau/sdk/android/core/type/TokenCall;)V

    .line 33
    return-void
.end method

.method public a(Z)V
    .registers 5

    .line 25
    new-instance v2, Lbr/com/itau/sdk/android/core/model/TokenRequestDTO;

    invoke-direct {v2}, Lbr/com/itau/sdk/android/core/model/TokenRequestDTO;-><init>()V

    .line 26
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v2, v0}, Lbr/com/itau/sdk/android/core/model/TokenRequestDTO;->setModoDesbloqueio(Ljava/lang/Boolean;)V

    .line 27
    invoke-static {}, Lbr/com/itau/sdk/android/core/BackendClient;->getInstance()Lbr/com/itau/sdk/android/core/BackendClient;

    move-result-object v0

    invoke-static {p0, v2}, Lbr/com/itau/sdk/android/core/token/d;->a(Lbr/com/itau/sdk/android/core/token/c;Lbr/com/itau/sdk/android/core/model/TokenRequestDTO;)Lbr/com/itau/sdk/android/core/type/TokenCall;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbr/com/itau/sdk/android/core/BackendClient;->call(Lbr/com/itau/sdk/android/core/type/TokenCall;)V

    .line 28
    return-void
.end method

.method public b()Lcom/google/gson/JsonElement;
    .registers 6

    .line 61
    const/4 v0, 0x0

    iput-object v0, p0, Lbr/com/itau/sdk/android/core/token/c;->a:Ljava/lang/String;

    .line 63
    invoke-direct {p0}, Lbr/com/itau/sdk/android/core/token/c;->f()Lbr/com/itau/sdk/android/core/token/b;

    move-result-object v0

    invoke-direct {p0}, Lbr/com/itau/sdk/android/core/token/c;->e()Lbr/com/itau/sdk/android/core/model/TokenAppRequestDTO;

    move-result-object v1

    invoke-interface {v0, v1}, Lbr/com/itau/sdk/android/core/token/b;->b(Lbr/com/itau/sdk/android/core/model/TokenAppRequestDTO;)Lcom/google/gson/JsonElement;

    .line 65
    const/4 v4, 0x0

    .line 66
    sget-object v0, Lbr/com/itau/sdk/android/core/token/c;->b:[B

    const/16 v1, 0x17

    aget-byte v0, v0, v1

    add-int/lit8 v1, v0, -0x1

    sget-object v2, Lbr/com/itau/sdk/android/core/token/c;->b:[B

    const/16 v3, 0x18

    aget-byte v2, v2, v3

    invoke-static {v0, v1, v2}, Lbr/com/itau/sdk/android/core/token/c;->a(III)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lbr/com/itau/sdk/android/core/token/c;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_41

    .line 67
    sget-object v0, Lbr/com/itau/sdk/android/core/token/c;->b:[B

    const/16 v1, 0x17

    aget-byte v0, v0, v1

    add-int/lit8 v0, v0, -0x1

    sget-object v1, Lbr/com/itau/sdk/android/core/token/c;->b:[B

    const/16 v2, 0x17

    aget-byte v1, v1, v2

    sget-object v2, Lbr/com/itau/sdk/android/core/token/c;->b:[B

    const/16 v3, 0x22

    aget-byte v2, v2, v3

    invoke-static {v0, v1, v2}, Lbr/com/itau/sdk/android/core/token/c;->a(III)Ljava/lang/String;

    move-result-object v4

    .line 70
    :cond_41
    invoke-direct {p0}, Lbr/com/itau/sdk/android/core/token/c;->f()Lbr/com/itau/sdk/android/core/token/b;

    move-result-object v0

    invoke-direct {p0}, Lbr/com/itau/sdk/android/core/token/c;->e()Lbr/com/itau/sdk/android/core/model/TokenAppRequestDTO;

    move-result-object v1

    invoke-interface {v0, v4, v1}, Lbr/com/itau/sdk/android/core/token/b;->d(Ljava/lang/String;Lbr/com/itau/sdk/android/core/model/TokenAppRequestDTO;)Lcom/google/gson/JsonElement;

    move-result-object v0

    return-object v0
.end method

.method public b(Ljava/lang/String;)Lcom/google/gson/JsonElement;
    .registers 7

    .line 74
    const/4 v0, 0x0

    iput-object v0, p0, Lbr/com/itau/sdk/android/core/token/c;->a:Ljava/lang/String;

    .line 76
    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_15

    .line 77
    invoke-direct {p0}, Lbr/com/itau/sdk/android/core/token/c;->f()Lbr/com/itau/sdk/android/core/token/b;

    move-result-object v0

    invoke-direct {p0}, Lbr/com/itau/sdk/android/core/token/c;->e()Lbr/com/itau/sdk/android/core/model/TokenAppRequestDTO;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Lbr/com/itau/sdk/android/core/token/b;->a(Ljava/lang/String;Lbr/com/itau/sdk/android/core/model/TokenAppRequestDTO;)Lcom/google/gson/JsonElement;

    goto :goto_20

    .line 79
    :cond_15
    invoke-direct {p0}, Lbr/com/itau/sdk/android/core/token/c;->f()Lbr/com/itau/sdk/android/core/token/b;

    move-result-object v0

    invoke-direct {p0}, Lbr/com/itau/sdk/android/core/token/c;->e()Lbr/com/itau/sdk/android/core/model/TokenAppRequestDTO;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Lbr/com/itau/sdk/android/core/token/b;->b(Ljava/lang/String;Lbr/com/itau/sdk/android/core/model/TokenAppRequestDTO;)Lcom/google/gson/JsonElement;

    .line 82
    :goto_20
    const/4 v4, 0x0

    .line 83
    sget-object v0, Lbr/com/itau/sdk/android/core/token/c;->b:[B

    const/16 v1, 0x17

    aget-byte v0, v0, v1

    add-int/lit8 v1, v0, -0x1

    sget-object v2, Lbr/com/itau/sdk/android/core/token/c;->b:[B

    const/16 v3, 0x18

    aget-byte v2, v2, v3

    invoke-static {v0, v1, v2}, Lbr/com/itau/sdk/android/core/token/c;->a(III)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lbr/com/itau/sdk/android/core/token/c;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_53

    .line 84
    sget-object v0, Lbr/com/itau/sdk/android/core/token/c;->b:[B

    const/16 v1, 0x17

    aget-byte v0, v0, v1

    add-int/lit8 v0, v0, -0x1

    sget-object v1, Lbr/com/itau/sdk/android/core/token/c;->b:[B

    const/16 v2, 0x17

    aget-byte v1, v1, v2

    sget-object v2, Lbr/com/itau/sdk/android/core/token/c;->b:[B

    const/16 v3, 0x22

    aget-byte v2, v2, v3

    invoke-static {v0, v1, v2}, Lbr/com/itau/sdk/android/core/token/c;->a(III)Ljava/lang/String;

    move-result-object v4

    .line 87
    :cond_53
    invoke-direct {p0}, Lbr/com/itau/sdk/android/core/token/c;->f()Lbr/com/itau/sdk/android/core/token/b;

    move-result-object v0

    invoke-direct {p0}, Lbr/com/itau/sdk/android/core/token/c;->e()Lbr/com/itau/sdk/android/core/model/TokenAppRequestDTO;

    move-result-object v1

    invoke-interface {v0, v4, v1}, Lbr/com/itau/sdk/android/core/token/b;->c(Ljava/lang/String;Lbr/com/itau/sdk/android/core/model/TokenAppRequestDTO;)Lcom/google/gson/JsonElement;

    move-result-object v0

    return-object v0
.end method

.method public b(Ljava/lang/String;Ljava/lang/String;)V
    .registers 5

    .line 36
    invoke-static {}, Lbr/com/itau/sdk/android/core/BackendClient;->getInstance()Lbr/com/itau/sdk/android/core/BackendClient;

    move-result-object v0

    invoke-static {p0, p1, p2}, Lbr/com/itau/sdk/android/core/token/f;->a(Lbr/com/itau/sdk/android/core/token/c;Ljava/lang/String;Ljava/lang/String;)Lbr/com/itau/sdk/android/core/type/TokenCall;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbr/com/itau/sdk/android/core/BackendClient;->call(Lbr/com/itau/sdk/android/core/type/TokenCall;)V

    .line 40
    return-void
.end method

.method public c(Ljava/lang/String;Ljava/lang/String;)Lbr/com/itau/sdk/android/core/SDKCustomUiController$ValidacaoAppResponseDTO;
    .registers 5

    .line 48
    invoke-direct {p0}, Lbr/com/itau/sdk/android/core/token/c;->f()Lbr/com/itau/sdk/android/core/token/b;

    move-result-object v0

    new-instance v1, Lbr/com/itau/sdk/android/core/model/TokenRequestDTO;

    invoke-direct {v1, p1, p2}, Lbr/com/itau/sdk/android/core/model/TokenRequestDTO;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Lbr/com/itau/sdk/android/core/token/b;->c(Lbr/com/itau/sdk/android/core/model/TokenRequestDTO;)Lbr/com/itau/sdk/android/core/SDKCustomUiController$ValidacaoAppResponseDTO;

    move-result-object v0

    return-object v0
.end method

.method public c()Lcom/google/gson/JsonElement;
    .registers 6

    .line 91
    invoke-direct {p0}, Lbr/com/itau/sdk/android/core/token/c;->e()Lbr/com/itau/sdk/android/core/model/TokenAppRequestDTO;

    move-result-object v4

    .line 93
    invoke-static {}, Lbr/com/itau/sdk/android/core/BackendClient;->getFlowManager()Lbr/com/itau/sdk/android/core/FlowManager;

    move-result-object v0

    sget-object v1, Lbr/com/itau/sdk/android/core/token/c;->b:[B

    const/4 v2, 0x7

    aget-byte v1, v1, v2

    const/16 v2, 0x15

    const/4 v3, 0x4

    invoke-static {v2, v3, v1}, Lbr/com/itau/sdk/android/core/token/c;->a(III)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbr/com/itau/sdk/android/core/FlowManager;->d(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_21

    .line 94
    invoke-direct {p0}, Lbr/com/itau/sdk/android/core/token/c;->f()Lbr/com/itau/sdk/android/core/token/b;

    move-result-object v0

    invoke-interface {v0, v4}, Lbr/com/itau/sdk/android/core/token/b;->c(Lbr/com/itau/sdk/android/core/model/TokenAppRequestDTO;)Lcom/google/gson/JsonElement;

    .line 96
    :cond_21
    invoke-direct {p0}, Lbr/com/itau/sdk/android/core/token/c;->f()Lbr/com/itau/sdk/android/core/token/b;

    move-result-object v0

    invoke-interface {v0, v4}, Lbr/com/itau/sdk/android/core/token/b;->d(Lbr/com/itau/sdk/android/core/model/TokenAppRequestDTO;)Lcom/google/gson/JsonElement;

    move-result-object v0

    return-object v0
.end method

.method public c(Ljava/lang/String;)V
    .registers 4

    .line 100
    invoke-static {}, Lbr/com/itau/sdk/android/core/BackendClient;->getInstance()Lbr/com/itau/sdk/android/core/BackendClient;

    move-result-object v0

    invoke-static {p0, p1}, Lbr/com/itau/sdk/android/core/token/h;->a(Lbr/com/itau/sdk/android/core/token/c;Ljava/lang/String;)Lbr/com/itau/sdk/android/core/type/TokenCall;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbr/com/itau/sdk/android/core/BackendClient;->call(Lbr/com/itau/sdk/android/core/type/TokenCall;)V

    .line 102
    return-void
.end method

.method public d(Ljava/lang/String;)V
    .registers 2

    .line 117
    iput-object p1, p0, Lbr/com/itau/sdk/android/core/token/c;->a:Ljava/lang/String;

    .line 118
    return-void
.end method

.method public d(Ljava/lang/String;Ljava/lang/String;)V
    .registers 5

    .line 52
    invoke-static {}, Lbr/com/itau/sdk/android/core/BackendClient;->getInstance()Lbr/com/itau/sdk/android/core/BackendClient;

    move-result-object v0

    invoke-static {p0, p1, p2}, Lbr/com/itau/sdk/android/core/token/g;->a(Lbr/com/itau/sdk/android/core/token/c;Ljava/lang/String;Ljava/lang/String;)Lbr/com/itau/sdk/android/core/type/TokenCall;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbr/com/itau/sdk/android/core/BackendClient;->call(Lbr/com/itau/sdk/android/core/type/TokenCall;)V

    .line 54
    return-void
.end method

.class public final Lcom/itau/empresas/PreferenciaUtils_;
.super Lcom/itau/empresas/PreferenciaUtils;
.source "PreferenciaUtils_.java"


# static fields
.field private static instance_:Lcom/itau/empresas/PreferenciaUtils_;


# instance fields
.field private context_:Landroid/content/Context;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .registers 2
    .param p1, "context"    # Landroid/content/Context;

    .line 20
    invoke-direct {p0, p1}, Lcom/itau/empresas/PreferenciaUtils;-><init>(Landroid/content/Context;)V

    .line 21
    iput-object p1, p0, Lcom/itau/empresas/PreferenciaUtils_;->context_:Landroid/content/Context;

    .line 22
    return-void
.end method

.method public static getInstance_(Landroid/content/Context;)Lcom/itau/empresas/PreferenciaUtils_;
    .registers 4
    .param p0, "context"    # Landroid/content/Context;

    .line 25
    sget-object v0, Lcom/itau/empresas/PreferenciaUtils_;->instance_:Lcom/itau/empresas/PreferenciaUtils_;

    if-nez v0, :cond_1c

    .line 26
    const/4 v0, 0x0

    invoke-static {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    move-result-object v2

    .line 27
    .local v2, "previousNotifier":Lorg/androidannotations/api/view/OnViewChangedNotifier;
    new-instance v0, Lcom/itau/empresas/PreferenciaUtils_;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/itau/empresas/PreferenciaUtils_;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/itau/empresas/PreferenciaUtils_;->instance_:Lcom/itau/empresas/PreferenciaUtils_;

    .line 28
    sget-object v0, Lcom/itau/empresas/PreferenciaUtils_;->instance_:Lcom/itau/empresas/PreferenciaUtils_;

    invoke-direct {v0}, Lcom/itau/empresas/PreferenciaUtils_;->init_()V

    .line 29
    invoke-static {v2}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 31
    .end local v2    # "previousNotifier":Lorg/androidannotations/api/view/OnViewChangedNotifier;
    :cond_1c
    sget-object v0, Lcom/itau/empresas/PreferenciaUtils_;->instance_:Lcom/itau/empresas/PreferenciaUtils_;

    return-object v0
.end method

.method private init_()V
    .registers 1

    .line 35
    return-void
.end method

.class public Lcom/google/android/gms/internal/zzjj;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/internal/zzji;


# annotations
.annotation runtime Lcom/google/android/gms/internal/zzhb;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/internal/zzjj$zza;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:Ljava/lang/Object;>Ljava/lang/Object;Lcom/google/android/gms/internal/zzji<TT;>;"
    }
.end annotation


# instance fields
.field protected zzBc:I

.field protected final zzNq:Ljava/util/concurrent/BlockingQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/BlockingQueue<Lcom/google/android/gms/internal/zzjj<TT;>.zza;>;"
        }
    .end annotation
.end field

.field protected zzNr:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field private final zzpV:Ljava/lang/Object;


# direct methods
.method public constructor <init>()V
    .registers 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/internal/zzjj;->zzpV:Ljava/lang/Object;

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/internal/zzjj;->zzBc:I

    new-instance v0, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v0}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/internal/zzjj;->zzNq:Ljava/util/concurrent/BlockingQueue;

    return-void
.end method


# virtual methods
.method public getStatus()I
    .registers 2

    iget v0, p0, Lcom/google/android/gms/internal/zzjj;->zzBc:I

    return v0
.end method

.method public reject()V
    .registers 6

    iget-object v1, p0, Lcom/google/android/gms/internal/zzjj;->zzpV:Ljava/lang/Object;

    monitor-enter v1

    :try_start_3
    iget v0, p0, Lcom/google/android/gms/internal/zzjj;->zzBc:I

    if-eqz v0, :cond_d

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    :cond_d
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/internal/zzjj;->zzBc:I

    iget-object v0, p0, Lcom/google/android/gms/internal/zzjj;->zzNq:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v0}, Ljava/util/concurrent/BlockingQueue;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_16
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_29

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/google/android/gms/internal/zzjj$zza;

    iget-object v0, v3, Lcom/google/android/gms/internal/zzjj$zza;->zzNt:Lcom/google/android/gms/internal/zzji$zza;

    invoke-interface {v0}, Lcom/google/android/gms/internal/zzji$zza;->run()V

    goto :goto_16

    :cond_29
    iget-object v0, p0, Lcom/google/android/gms/internal/zzjj;->zzNq:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v0}, Ljava/util/concurrent/BlockingQueue;->clear()V
    :try_end_2e
    .catchall {:try_start_3 .. :try_end_2e} :catchall_30

    monitor-exit v1

    goto :goto_33

    :catchall_30
    move-exception v4

    monitor-exit v1

    throw v4

    :goto_33
    return-void
.end method

.method public zza(Lcom/google/android/gms/internal/zzji$zzc;Lcom/google/android/gms/internal/zzji$zza;)V
    .registers 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Lcom/google/android/gms/internal/zzji$zzc<TT;>;Lcom/google/android/gms/internal/zzji$zza;)V"
        }
    .end annotation

    iget-object v2, p0, Lcom/google/android/gms/internal/zzjj;->zzpV:Ljava/lang/Object;

    monitor-enter v2

    :try_start_3
    iget v0, p0, Lcom/google/android/gms/internal/zzjj;->zzBc:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_e

    iget-object v0, p0, Lcom/google/android/gms/internal/zzjj;->zzNr:Ljava/lang/Object;

    invoke-interface {p1, v0}, Lcom/google/android/gms/internal/zzji$zzc;->zze(Ljava/lang/Object;)V

    goto :goto_25

    :cond_e
    iget v0, p0, Lcom/google/android/gms/internal/zzjj;->zzBc:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_17

    invoke-interface {p2}, Lcom/google/android/gms/internal/zzji$zza;->run()V

    goto :goto_25

    :cond_17
    iget v0, p0, Lcom/google/android/gms/internal/zzjj;->zzBc:I

    if-nez v0, :cond_25

    iget-object v0, p0, Lcom/google/android/gms/internal/zzjj;->zzNq:Ljava/util/concurrent/BlockingQueue;

    new-instance v1, Lcom/google/android/gms/internal/zzjj$zza;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/gms/internal/zzjj$zza;-><init>(Lcom/google/android/gms/internal/zzjj;Lcom/google/android/gms/internal/zzji$zzc;Lcom/google/android/gms/internal/zzji$zza;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/BlockingQueue;->add(Ljava/lang/Object;)Z
    :try_end_25
    .catchall {:try_start_3 .. :try_end_25} :catchall_27

    :cond_25
    :goto_25
    monitor-exit v2

    goto :goto_2a

    :catchall_27
    move-exception v3

    monitor-exit v2

    throw v3

    :goto_2a
    return-void
.end method

.method public zzh(Ljava/lang/Object;)V
    .registers 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    iget-object v1, p0, Lcom/google/android/gms/internal/zzjj;->zzpV:Ljava/lang/Object;

    monitor-enter v1

    :try_start_3
    iget v0, p0, Lcom/google/android/gms/internal/zzjj;->zzBc:I

    if-eqz v0, :cond_d

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    :cond_d
    iput-object p1, p0, Lcom/google/android/gms/internal/zzjj;->zzNr:Ljava/lang/Object;

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/internal/zzjj;->zzBc:I

    iget-object v0, p0, Lcom/google/android/gms/internal/zzjj;->zzNq:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v0}, Ljava/util/concurrent/BlockingQueue;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_18
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2b

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/google/android/gms/internal/zzjj$zza;

    iget-object v0, v3, Lcom/google/android/gms/internal/zzjj$zza;->zzNs:Lcom/google/android/gms/internal/zzji$zzc;

    invoke-interface {v0, p1}, Lcom/google/android/gms/internal/zzji$zzc;->zze(Ljava/lang/Object;)V

    goto :goto_18

    :cond_2b
    iget-object v0, p0, Lcom/google/android/gms/internal/zzjj;->zzNq:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v0}, Ljava/util/concurrent/BlockingQueue;->clear()V
    :try_end_30
    .catchall {:try_start_3 .. :try_end_30} :catchall_32

    monitor-exit v1

    goto :goto_35

    :catchall_32
    move-exception v4

    monitor-exit v1

    throw v4

    :goto_35
    return-void
.end method

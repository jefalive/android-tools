.class public Lcom/itau/empresas/controller/SessaoController;
.super Lcom/itau/empresas/controller/BaseController;
.source "SessaoController.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/itau/empresas/controller/BaseController<Lcom/itau/empresas/api/Api;>;"
    }
.end annotation


# instance fields
.field app:Lcom/itau/empresas/CustomApplication;

.field menuController:Lcom/itau/empresas/feature/login/controller/MenuController;


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 19
    invoke-direct {p0}, Lcom/itau/empresas/controller/BaseController;-><init>()V

    return-void
.end method

.method public static executarLogout()V
    .registers 1

    .line 43
    new-instance v0, Lcom/itau/empresas/controller/SessaoController$2;

    invoke-direct {v0}, Lcom/itau/empresas/controller/SessaoController$2;-><init>()V

    invoke-static {v0}, Lbr/com/itau/sdk/android/core/BackendClient;->callApi(Lbr/com/itau/sdk/android/core/type/BackendCall;)V

    .line 49
    return-void
.end method


# virtual methods
.method public trocarConta(Lcom/itau/empresas/api/model/ContaOperadorVO;)V
    .registers 3
    .param p1, "conta"    # Lcom/itau/empresas/api/model/ContaOperadorVO;

    .line 26
    new-instance v0, Lcom/itau/empresas/controller/SessaoController$1;

    invoke-direct {v0, p0, p1}, Lcom/itau/empresas/controller/SessaoController$1;-><init>(Lcom/itau/empresas/controller/SessaoController;Lcom/itau/empresas/api/model/ContaOperadorVO;)V

    invoke-static {v0}, Lbr/com/itau/sdk/android/core/BackendClient;->callApi(Lbr/com/itau/sdk/android/core/type/BackendCall;)V

    .line 40
    return-void
.end method

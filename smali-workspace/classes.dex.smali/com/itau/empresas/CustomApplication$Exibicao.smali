.class public final enum Lcom/itau/empresas/CustomApplication$Exibicao;
.super Ljava/lang/Enum;
.source "CustomApplication.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/itau/empresas/CustomApplication;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Exibicao"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<Lcom/itau/empresas/CustomApplication$Exibicao;>;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/itau/empresas/CustomApplication$Exibicao;

.field public static final enum EXIBICAO_BANNER_EXTRATO:Lcom/itau/empresas/CustomApplication$Exibicao;

.field public static final enum EXIBICAO_BANNER_GIRO_EXTRATO:Lcom/itau/empresas/CustomApplication$Exibicao;

.field public static final enum EXIBICAO_CARRINHO:Lcom/itau/empresas/CustomApplication$Exibicao;

.field public static final enum EXIBICAO_PEDAGIO_AUTORIZAR_PEX:Lcom/itau/empresas/CustomApplication$Exibicao;

.field public static final enum EXIBICAO_PEDAGIO_AUTORIZAR_SISPAG:Lcom/itau/empresas/CustomApplication$Exibicao;

.field public static final enum EXIBICAO_PEDAGIO_TRANSFERENCIA_DOC:Lcom/itau/empresas/CustomApplication$Exibicao;

.field public static final enum EXIBICAO_PEDAGIO_TRANSFERENCIA_TED:Lcom/itau/empresas/CustomApplication$Exibicao;

.field public static final enum EXIBICAO_PEDAGIO_TRANSFERENCIA_TEF:Lcom/itau/empresas/CustomApplication$Exibicao;

.field public static final enum EXIBICAO_RECARGA_AUTORIZAR:Lcom/itau/empresas/CustomApplication$Exibicao;

.field public static final enum EXIBICAO_SINO_PENDENCIAS:Lcom/itau/empresas/CustomApplication$Exibicao;

.field public static final enum EXIBICAO_TOGGLE_ACAOFIMANO:Lcom/itau/empresas/CustomApplication$Exibicao;

.field public static final enum EXIBICAO_TOGGLE_FINGERPRINT:Lcom/itau/empresas/CustomApplication$Exibicao;

.field public static final enum EXIBICAO_WIDGET_BUSCA:Lcom/itau/empresas/CustomApplication$Exibicao;

.field public static final enum EXIBICAO_WIDGET_COBRANCA:Lcom/itau/empresas/CustomApplication$Exibicao;

.field public static final enum EXIBICAO_WIDGET_COBRANCAFRANCESINHA:Lcom/itau/empresas/CustomApplication$Exibicao;

.field public static final enum EXIBICAO_WIDGET_CONTA_CORRENTE:Lcom/itau/empresas/CustomApplication$Exibicao;

.field public static final enum EXIBICAO_WIDGET_LEITOR_CODIGO_BARRAS:Lcom/itau/empresas/CustomApplication$Exibicao;

.field public static final enum EXIBICAO_WIDGET_SALDO:Lcom/itau/empresas/CustomApplication$Exibicao;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .line 330
    new-instance v0, Lcom/itau/empresas/CustomApplication$Exibicao;

    const-string v1, "EXIBICAO_RECARGA_AUTORIZAR"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/itau/empresas/CustomApplication$Exibicao;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/itau/empresas/CustomApplication$Exibicao;->EXIBICAO_RECARGA_AUTORIZAR:Lcom/itau/empresas/CustomApplication$Exibicao;

    .line 334
    new-instance v0, Lcom/itau/empresas/CustomApplication$Exibicao;

    const-string v1, "EXIBICAO_PEDAGIO_TRANSFERENCIA_TEF"

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lcom/itau/empresas/CustomApplication$Exibicao;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/itau/empresas/CustomApplication$Exibicao;->EXIBICAO_PEDAGIO_TRANSFERENCIA_TEF:Lcom/itau/empresas/CustomApplication$Exibicao;

    .line 335
    new-instance v0, Lcom/itau/empresas/CustomApplication$Exibicao;

    const-string v1, "EXIBICAO_PEDAGIO_TRANSFERENCIA_DOC"

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2}, Lcom/itau/empresas/CustomApplication$Exibicao;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/itau/empresas/CustomApplication$Exibicao;->EXIBICAO_PEDAGIO_TRANSFERENCIA_DOC:Lcom/itau/empresas/CustomApplication$Exibicao;

    .line 336
    new-instance v0, Lcom/itau/empresas/CustomApplication$Exibicao;

    const-string v1, "EXIBICAO_PEDAGIO_TRANSFERENCIA_TED"

    const/4 v2, 0x3

    invoke-direct {v0, v1, v2}, Lcom/itau/empresas/CustomApplication$Exibicao;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/itau/empresas/CustomApplication$Exibicao;->EXIBICAO_PEDAGIO_TRANSFERENCIA_TED:Lcom/itau/empresas/CustomApplication$Exibicao;

    .line 339
    new-instance v0, Lcom/itau/empresas/CustomApplication$Exibicao;

    const-string v1, "EXIBICAO_PEDAGIO_AUTORIZAR_PEX"

    const/4 v2, 0x4

    invoke-direct {v0, v1, v2}, Lcom/itau/empresas/CustomApplication$Exibicao;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/itau/empresas/CustomApplication$Exibicao;->EXIBICAO_PEDAGIO_AUTORIZAR_PEX:Lcom/itau/empresas/CustomApplication$Exibicao;

    .line 340
    new-instance v0, Lcom/itau/empresas/CustomApplication$Exibicao;

    const-string v1, "EXIBICAO_PEDAGIO_AUTORIZAR_SISPAG"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/itau/empresas/CustomApplication$Exibicao;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/itau/empresas/CustomApplication$Exibicao;->EXIBICAO_PEDAGIO_AUTORIZAR_SISPAG:Lcom/itau/empresas/CustomApplication$Exibicao;

    .line 343
    new-instance v0, Lcom/itau/empresas/CustomApplication$Exibicao;

    const-string v1, "EXIBICAO_WIDGET_SALDO"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/itau/empresas/CustomApplication$Exibicao;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/itau/empresas/CustomApplication$Exibicao;->EXIBICAO_WIDGET_SALDO:Lcom/itau/empresas/CustomApplication$Exibicao;

    .line 344
    new-instance v0, Lcom/itau/empresas/CustomApplication$Exibicao;

    const-string v1, "EXIBICAO_WIDGET_CONTA_CORRENTE"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/itau/empresas/CustomApplication$Exibicao;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/itau/empresas/CustomApplication$Exibicao;->EXIBICAO_WIDGET_CONTA_CORRENTE:Lcom/itau/empresas/CustomApplication$Exibicao;

    .line 347
    new-instance v0, Lcom/itau/empresas/CustomApplication$Exibicao;

    const-string v1, "EXIBICAO_WIDGET_LEITOR_CODIGO_BARRAS"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/itau/empresas/CustomApplication$Exibicao;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/itau/empresas/CustomApplication$Exibicao;->EXIBICAO_WIDGET_LEITOR_CODIGO_BARRAS:Lcom/itau/empresas/CustomApplication$Exibicao;

    .line 350
    new-instance v0, Lcom/itau/empresas/CustomApplication$Exibicao;

    const-string v1, "EXIBICAO_WIDGET_COBRANCA"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/itau/empresas/CustomApplication$Exibicao;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/itau/empresas/CustomApplication$Exibicao;->EXIBICAO_WIDGET_COBRANCA:Lcom/itau/empresas/CustomApplication$Exibicao;

    .line 351
    new-instance v0, Lcom/itau/empresas/CustomApplication$Exibicao;

    const-string v1, "EXIBICAO_WIDGET_COBRANCAFRANCESINHA"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/itau/empresas/CustomApplication$Exibicao;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/itau/empresas/CustomApplication$Exibicao;->EXIBICAO_WIDGET_COBRANCAFRANCESINHA:Lcom/itau/empresas/CustomApplication$Exibicao;

    .line 354
    new-instance v0, Lcom/itau/empresas/CustomApplication$Exibicao;

    const-string v1, "EXIBICAO_BANNER_EXTRATO"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/itau/empresas/CustomApplication$Exibicao;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/itau/empresas/CustomApplication$Exibicao;->EXIBICAO_BANNER_EXTRATO:Lcom/itau/empresas/CustomApplication$Exibicao;

    .line 357
    new-instance v0, Lcom/itau/empresas/CustomApplication$Exibicao;

    const-string v1, "EXIBICAO_WIDGET_BUSCA"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/itau/empresas/CustomApplication$Exibicao;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/itau/empresas/CustomApplication$Exibicao;->EXIBICAO_WIDGET_BUSCA:Lcom/itau/empresas/CustomApplication$Exibicao;

    .line 359
    new-instance v0, Lcom/itau/empresas/CustomApplication$Exibicao;

    const-string v1, "EXIBICAO_TOGGLE_ACAOFIMANO"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/itau/empresas/CustomApplication$Exibicao;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/itau/empresas/CustomApplication$Exibicao;->EXIBICAO_TOGGLE_ACAOFIMANO:Lcom/itau/empresas/CustomApplication$Exibicao;

    .line 360
    new-instance v0, Lcom/itau/empresas/CustomApplication$Exibicao;

    const-string v1, "EXIBICAO_TOGGLE_FINGERPRINT"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/itau/empresas/CustomApplication$Exibicao;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/itau/empresas/CustomApplication$Exibicao;->EXIBICAO_TOGGLE_FINGERPRINT:Lcom/itau/empresas/CustomApplication$Exibicao;

    .line 362
    new-instance v0, Lcom/itau/empresas/CustomApplication$Exibicao;

    const-string v1, "EXIBICAO_CARRINHO"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lcom/itau/empresas/CustomApplication$Exibicao;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/itau/empresas/CustomApplication$Exibicao;->EXIBICAO_CARRINHO:Lcom/itau/empresas/CustomApplication$Exibicao;

    .line 365
    new-instance v0, Lcom/itau/empresas/CustomApplication$Exibicao;

    const-string v1, "EXIBICAO_BANNER_GIRO_EXTRATO"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, Lcom/itau/empresas/CustomApplication$Exibicao;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/itau/empresas/CustomApplication$Exibicao;->EXIBICAO_BANNER_GIRO_EXTRATO:Lcom/itau/empresas/CustomApplication$Exibicao;

    .line 368
    new-instance v0, Lcom/itau/empresas/CustomApplication$Exibicao;

    const-string v1, "EXIBICAO_SINO_PENDENCIAS"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, Lcom/itau/empresas/CustomApplication$Exibicao;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/itau/empresas/CustomApplication$Exibicao;->EXIBICAO_SINO_PENDENCIAS:Lcom/itau/empresas/CustomApplication$Exibicao;

    .line 328
    const/16 v0, 0x12

    new-array v0, v0, [Lcom/itau/empresas/CustomApplication$Exibicao;

    sget-object v1, Lcom/itau/empresas/CustomApplication$Exibicao;->EXIBICAO_RECARGA_AUTORIZAR:Lcom/itau/empresas/CustomApplication$Exibicao;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sget-object v1, Lcom/itau/empresas/CustomApplication$Exibicao;->EXIBICAO_PEDAGIO_TRANSFERENCIA_TEF:Lcom/itau/empresas/CustomApplication$Exibicao;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    sget-object v1, Lcom/itau/empresas/CustomApplication$Exibicao;->EXIBICAO_PEDAGIO_TRANSFERENCIA_DOC:Lcom/itau/empresas/CustomApplication$Exibicao;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    sget-object v1, Lcom/itau/empresas/CustomApplication$Exibicao;->EXIBICAO_PEDAGIO_TRANSFERENCIA_TED:Lcom/itau/empresas/CustomApplication$Exibicao;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    sget-object v1, Lcom/itau/empresas/CustomApplication$Exibicao;->EXIBICAO_PEDAGIO_AUTORIZAR_PEX:Lcom/itau/empresas/CustomApplication$Exibicao;

    const/4 v2, 0x4

    aput-object v1, v0, v2

    sget-object v1, Lcom/itau/empresas/CustomApplication$Exibicao;->EXIBICAO_PEDAGIO_AUTORIZAR_SISPAG:Lcom/itau/empresas/CustomApplication$Exibicao;

    const/4 v2, 0x5

    aput-object v1, v0, v2

    sget-object v1, Lcom/itau/empresas/CustomApplication$Exibicao;->EXIBICAO_WIDGET_SALDO:Lcom/itau/empresas/CustomApplication$Exibicao;

    const/4 v2, 0x6

    aput-object v1, v0, v2

    sget-object v1, Lcom/itau/empresas/CustomApplication$Exibicao;->EXIBICAO_WIDGET_CONTA_CORRENTE:Lcom/itau/empresas/CustomApplication$Exibicao;

    const/4 v2, 0x7

    aput-object v1, v0, v2

    sget-object v1, Lcom/itau/empresas/CustomApplication$Exibicao;->EXIBICAO_WIDGET_LEITOR_CODIGO_BARRAS:Lcom/itau/empresas/CustomApplication$Exibicao;

    const/16 v2, 0x8

    aput-object v1, v0, v2

    sget-object v1, Lcom/itau/empresas/CustomApplication$Exibicao;->EXIBICAO_WIDGET_COBRANCA:Lcom/itau/empresas/CustomApplication$Exibicao;

    const/16 v2, 0x9

    aput-object v1, v0, v2

    sget-object v1, Lcom/itau/empresas/CustomApplication$Exibicao;->EXIBICAO_WIDGET_COBRANCAFRANCESINHA:Lcom/itau/empresas/CustomApplication$Exibicao;

    const/16 v2, 0xa

    aput-object v1, v0, v2

    sget-object v1, Lcom/itau/empresas/CustomApplication$Exibicao;->EXIBICAO_BANNER_EXTRATO:Lcom/itau/empresas/CustomApplication$Exibicao;

    const/16 v2, 0xb

    aput-object v1, v0, v2

    sget-object v1, Lcom/itau/empresas/CustomApplication$Exibicao;->EXIBICAO_WIDGET_BUSCA:Lcom/itau/empresas/CustomApplication$Exibicao;

    const/16 v2, 0xc

    aput-object v1, v0, v2

    sget-object v1, Lcom/itau/empresas/CustomApplication$Exibicao;->EXIBICAO_TOGGLE_ACAOFIMANO:Lcom/itau/empresas/CustomApplication$Exibicao;

    const/16 v2, 0xd

    aput-object v1, v0, v2

    sget-object v1, Lcom/itau/empresas/CustomApplication$Exibicao;->EXIBICAO_TOGGLE_FINGERPRINT:Lcom/itau/empresas/CustomApplication$Exibicao;

    const/16 v2, 0xe

    aput-object v1, v0, v2

    sget-object v1, Lcom/itau/empresas/CustomApplication$Exibicao;->EXIBICAO_CARRINHO:Lcom/itau/empresas/CustomApplication$Exibicao;

    const/16 v2, 0xf

    aput-object v1, v0, v2

    sget-object v1, Lcom/itau/empresas/CustomApplication$Exibicao;->EXIBICAO_BANNER_GIRO_EXTRATO:Lcom/itau/empresas/CustomApplication$Exibicao;

    const/16 v2, 0x10

    aput-object v1, v0, v2

    sget-object v1, Lcom/itau/empresas/CustomApplication$Exibicao;->EXIBICAO_SINO_PENDENCIAS:Lcom/itau/empresas/CustomApplication$Exibicao;

    const/16 v2, 0x11

    aput-object v1, v0, v2

    sput-object v0, Lcom/itau/empresas/CustomApplication$Exibicao;->$VALUES:[Lcom/itau/empresas/CustomApplication$Exibicao;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 328
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/itau/empresas/CustomApplication$Exibicao;
    .registers 2
    .param p0, "name"    # Ljava/lang/String;

    .line 328
    const-class v0, Lcom/itau/empresas/CustomApplication$Exibicao;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/CustomApplication$Exibicao;

    return-object v0
.end method

.method public static values()[Lcom/itau/empresas/CustomApplication$Exibicao;
    .registers 1

    .line 328
    sget-object v0, Lcom/itau/empresas/CustomApplication$Exibicao;->$VALUES:[Lcom/itau/empresas/CustomApplication$Exibicao;

    invoke-virtual {v0}, [Lcom/itau/empresas/CustomApplication$Exibicao;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/itau/empresas/CustomApplication$Exibicao;

    return-object v0
.end method

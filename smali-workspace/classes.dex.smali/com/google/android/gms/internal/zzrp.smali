.class public Lcom/google/android/gms/internal/zzrp;
.super Ljava/lang/Object;


# static fields
.field private static DEBUG:Z

.field private static TAG:Ljava/lang/String;

.field private static zzbhl:Ljava/lang/String;


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final zzanQ:Ljava/lang/String;

.field private final zzbhm:Landroid/os/PowerManager$WakeLock;

.field private zzbhn:Landroid/os/WorkSource;

.field private final zzbho:I

.field private final zzbhp:Ljava/lang/String;

.field private zzbhq:Z

.field private zzbhr:I

.field private zzbhs:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    const-string v0, "WakeLock"

    sput-object v0, Lcom/google/android/gms/internal/zzrp;->TAG:Ljava/lang/String;

    const-string v0, "*gcore*:"

    sput-object v0, Lcom/google/android/gms/internal/zzrp;->zzbhl:Ljava/lang/String;

    const/4 v0, 0x0

    sput-boolean v0, Lcom/google/android/gms/internal/zzrp;->DEBUG:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ILjava/lang/String;)V
    .registers 10

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    if-nez p1, :cond_8

    const/4 v5, 0x0

    goto :goto_c

    :cond_8
    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v5

    :goto_c
    const/4 v4, 0x0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/internal/zzrp;-><init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 10
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "UnwrappedWakeLock"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/internal/zzrp;->zzbhq:Z

    const-string v0, "Wake lock name can NOT be empty"

    invoke-static {p3, v0}, Lcom/google/android/gms/common/internal/zzx;->zzh(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    iput p2, p0, Lcom/google/android/gms/internal/zzrp;->zzbho:I

    iput-object p4, p0, Lcom/google/android/gms/internal/zzrp;->zzbhp:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/internal/zzrp;->mContext:Landroid/content/Context;

    invoke-static {p5}, Lcom/google/android/gms/internal/zzni;->zzcV(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_35

    const-string v0, "com.google.android.gms"

    if-eq v0, p5, :cond_35

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/google/android/gms/internal/zzrp;->zzbhl:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/internal/zzrp;->zzanQ:Ljava/lang/String;

    goto :goto_37

    :cond_35
    iput-object p3, p0, Lcom/google/android/gms/internal/zzrp;->zzanQ:Ljava/lang/String;

    :goto_37
    const-string v0, "power"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    invoke-virtual {v0, p2, p3}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/internal/zzrp;->zzbhm:Landroid/os/PowerManager$WakeLock;

    iget-object v0, p0, Lcom/google/android/gms/internal/zzrp;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/internal/zznj;->zzaA(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_96

    invoke-static {p5}, Lcom/google/android/gms/internal/zzni;->zzcV(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8a

    sget-boolean v0, Lcom/google/android/gms/common/internal/zzd;->zzakE:Z

    if-eqz v0, :cond_85

    invoke-static {}, Lcom/google/android/gms/internal/zzlz;->isInitialized()Z

    move-result v0

    if-eqz v0, :cond_85

    sget-object v0, Lcom/google/android/gms/internal/zzrp;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "callingPackage is not supposed to be empty for wakelock "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/internal/zzrp;->zzanQ:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "!"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/IllegalArgumentException;

    invoke-direct {v2}, Ljava/lang/IllegalArgumentException;-><init>()V

    invoke-static {v0, v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const-string v3, "com.google.android.gms"

    goto :goto_8b

    :cond_85
    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    goto :goto_8b

    :cond_8a
    move-object v3, p5

    :goto_8b
    invoke-static {p1, v3}, Lcom/google/android/gms/internal/zznj;->zzl(Landroid/content/Context;Ljava/lang/String;)Landroid/os/WorkSource;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/internal/zzrp;->zzbhn:Landroid/os/WorkSource;

    iget-object v0, p0, Lcom/google/android/gms/internal/zzrp;->zzbhn:Landroid/os/WorkSource;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/internal/zzrp;->zzc(Landroid/os/WorkSource;)V

    :cond_96
    return-void
.end method

.method private zzfJ(Ljava/lang/String;)V
    .registers 14

    invoke-direct {p0, p1}, Lcom/google/android/gms/internal/zzrp;->zzfK(Ljava/lang/String;)Z

    move-result v8

    invoke-direct {p0, p1, v8}, Lcom/google/android/gms/internal/zzrp;->zzn(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v9

    sget-boolean v0, Lcom/google/android/gms/internal/zzrp;->DEBUG:Z

    if-eqz v0, :cond_68

    sget-object v0, Lcom/google/android/gms/internal/zzrp;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Release:\n mWakeLockName: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/internal/zzrp;->zzanQ:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n mSecondaryName: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/internal/zzrp;->zzbhp:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\nmReferenceCounted: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/google/android/gms/internal/zzrp;->zzbhq:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\nreason: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n mOpenEventCount"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/gms/internal/zzrp;->zzbhs:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\nuseWithReason: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\ntrackingName: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_68
    move-object v10, p0

    monitor-enter v10

    :try_start_6a
    iget-boolean v0, p0, Lcom/google/android/gms/internal/zzrp;->zzbhq:Z

    if-eqz v0, :cond_78

    iget v0, p0, Lcom/google/android/gms/internal/zzrp;->zzbhr:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/android/gms/internal/zzrp;->zzbhr:I

    if-eqz v0, :cond_81

    if-nez v8, :cond_81

    :cond_78
    iget-boolean v0, p0, Lcom/google/android/gms/internal/zzrp;->zzbhq:Z

    if-nez v0, :cond_a3

    iget v0, p0, Lcom/google/android/gms/internal/zzrp;->zzbhs:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_a3

    :cond_81
    invoke-static {}, Lcom/google/android/gms/common/stats/zzi;->zzrZ()Lcom/google/android/gms/common/stats/zzi;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/internal/zzrp;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/gms/internal/zzrp;->zzbhm:Landroid/os/PowerManager$WakeLock;

    invoke-static {v2, v9}, Lcom/google/android/gms/common/stats/zzg;->zza(Landroid/os/PowerManager$WakeLock;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iget-object v4, p0, Lcom/google/android/gms/internal/zzrp;->zzanQ:Ljava/lang/String;

    move-object v5, v9

    iget v6, p0, Lcom/google/android/gms/internal/zzrp;->zzbho:I

    iget-object v3, p0, Lcom/google/android/gms/internal/zzrp;->zzbhn:Landroid/os/WorkSource;

    invoke-static {v3}, Lcom/google/android/gms/internal/zznj;->zzb(Landroid/os/WorkSource;)Ljava/util/List;

    move-result-object v7

    const/16 v3, 0x8

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/gms/common/stats/zzi;->zza(Landroid/content/Context;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;ILjava/util/List;)V

    iget v0, p0, Lcom/google/android/gms/internal/zzrp;->zzbhs:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/android/gms/internal/zzrp;->zzbhs:I
    :try_end_a3
    .catchall {:try_start_6a .. :try_end_a3} :catchall_a5

    :cond_a3
    monitor-exit v10

    goto :goto_a8

    :catchall_a5
    move-exception v11

    monitor-exit v10

    throw v11

    :goto_a8
    return-void
.end method

.method private zzfK(Ljava/lang/String;)Z
    .registers 3

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_10

    iget-object v0, p0, Lcom/google/android/gms/internal/zzrp;->zzbhp:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_10

    const/4 v0, 0x1

    goto :goto_11

    :cond_10
    const/4 v0, 0x0

    :goto_11
    return v0
.end method

.method private zzj(Ljava/lang/String;J)V
    .registers 18

    invoke-direct {p0, p1}, Lcom/google/android/gms/internal/zzrp;->zzfK(Ljava/lang/String;)Z

    move-result v10

    invoke-direct {p0, p1, v10}, Lcom/google/android/gms/internal/zzrp;->zzn(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v11

    sget-boolean v0, Lcom/google/android/gms/internal/zzrp;->DEBUG:Z

    if-eqz v0, :cond_74

    sget-object v0, Lcom/google/android/gms/internal/zzrp;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Acquire:\n mWakeLockName: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/internal/zzrp;->zzanQ:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n mSecondaryName: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/internal/zzrp;->zzbhp:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\nmReferenceCounted: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/google/android/gms/internal/zzrp;->zzbhq:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\nreason: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\nmOpenEventCount"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/gms/internal/zzrp;->zzbhs:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\nuseWithReason: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\ntrackingName: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\ntimeout: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-wide/from16 v2, p2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_74
    move-object v12, p0

    monitor-enter v12

    :try_start_76
    iget-boolean v0, p0, Lcom/google/android/gms/internal/zzrp;->zzbhq:Z

    if-eqz v0, :cond_84

    iget v0, p0, Lcom/google/android/gms/internal/zzrp;->zzbhr:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/google/android/gms/internal/zzrp;->zzbhr:I

    if-eqz v0, :cond_8c

    if-nez v10, :cond_8c

    :cond_84
    iget-boolean v0, p0, Lcom/google/android/gms/internal/zzrp;->zzbhq:Z

    if-nez v0, :cond_af

    iget v0, p0, Lcom/google/android/gms/internal/zzrp;->zzbhs:I

    if-nez v0, :cond_af

    :cond_8c
    invoke-static {}, Lcom/google/android/gms/common/stats/zzi;->zzrZ()Lcom/google/android/gms/common/stats/zzi;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/internal/zzrp;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/gms/internal/zzrp;->zzbhm:Landroid/os/PowerManager$WakeLock;

    invoke-static {v2, v11}, Lcom/google/android/gms/common/stats/zzg;->zza(Landroid/os/PowerManager$WakeLock;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iget-object v4, p0, Lcom/google/android/gms/internal/zzrp;->zzanQ:Ljava/lang/String;

    move-object v5, v11

    iget v6, p0, Lcom/google/android/gms/internal/zzrp;->zzbho:I

    iget-object v3, p0, Lcom/google/android/gms/internal/zzrp;->zzbhn:Landroid/os/WorkSource;

    invoke-static {v3}, Lcom/google/android/gms/internal/zznj;->zzb(Landroid/os/WorkSource;)Ljava/util/List;

    move-result-object v7

    move-wide/from16 v8, p2

    const/4 v3, 0x7

    invoke-virtual/range {v0 .. v9}, Lcom/google/android/gms/common/stats/zzi;->zza(Landroid/content/Context;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;ILjava/util/List;J)V

    iget v0, p0, Lcom/google/android/gms/internal/zzrp;->zzbhs:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/gms/internal/zzrp;->zzbhs:I
    :try_end_af
    .catchall {:try_start_76 .. :try_end_af} :catchall_b1

    :cond_af
    monitor-exit v12

    goto :goto_b4

    :catchall_b1
    move-exception v13

    monitor-exit v12

    throw v13

    :goto_b4
    return-void
.end method

.method private zzn(Ljava/lang/String;Z)Ljava/lang/String;
    .registers 4

    iget-boolean v0, p0, Lcom/google/android/gms/internal/zzrp;->zzbhq:Z

    if-eqz v0, :cond_b

    if-eqz p2, :cond_8

    move-object v0, p1

    goto :goto_d

    :cond_8
    iget-object v0, p0, Lcom/google/android/gms/internal/zzrp;->zzbhp:Ljava/lang/String;

    goto :goto_d

    :cond_b
    iget-object v0, p0, Lcom/google/android/gms/internal/zzrp;->zzbhp:Ljava/lang/String;

    :goto_d
    return-object v0
.end method


# virtual methods
.method public acquire(J)V
    .registers 6
    .param p1, "timeout"    # J

    invoke-static {}, Lcom/google/android/gms/internal/zzne;->zzsg()Z

    move-result v0

    if-nez v0, :cond_24

    iget-boolean v0, p0, Lcom/google/android/gms/internal/zzrp;->zzbhq:Z

    if-eqz v0, :cond_24

    sget-object v0, Lcom/google/android/gms/internal/zzrp;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Do not acquire with timeout on reference counted WakeLocks before ICS. wakelock: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/internal/zzrp;->zzanQ:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    :cond_24
    const/4 v0, 0x0

    invoke-direct {p0, v0, p1, p2}, Lcom/google/android/gms/internal/zzrp;->zzj(Ljava/lang/String;J)V

    iget-object v0, p0, Lcom/google/android/gms/internal/zzrp;->zzbhm:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0, p1, p2}, Landroid/os/PowerManager$WakeLock;->acquire(J)V

    return-void
.end method

.method public isHeld()Z
    .registers 2

    iget-object v0, p0, Lcom/google/android/gms/internal/zzrp;->zzbhm:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    return v0
.end method

.method public release()V
    .registers 2

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/internal/zzrp;->zzfJ(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/internal/zzrp;->zzbhm:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    return-void
.end method

.method public setReferenceCounted(Z)V
    .registers 3
    .param p1, "value"    # Z

    iget-object v0, p0, Lcom/google/android/gms/internal/zzrp;->zzbhm:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0, p1}, Landroid/os/PowerManager$WakeLock;->setReferenceCounted(Z)V

    iput-boolean p1, p0, Lcom/google/android/gms/internal/zzrp;->zzbhq:Z

    return-void
.end method

.method public zzc(Landroid/os/WorkSource;)V
    .registers 4

    iget-object v0, p0, Lcom/google/android/gms/internal/zzrp;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/internal/zznj;->zzaA(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1d

    if-eqz p1, :cond_1d

    iget-object v0, p0, Lcom/google/android/gms/internal/zzrp;->zzbhn:Landroid/os/WorkSource;

    if-eqz v0, :cond_14

    iget-object v0, p0, Lcom/google/android/gms/internal/zzrp;->zzbhn:Landroid/os/WorkSource;

    invoke-virtual {v0, p1}, Landroid/os/WorkSource;->add(Landroid/os/WorkSource;)Z

    goto :goto_16

    :cond_14
    iput-object p1, p0, Lcom/google/android/gms/internal/zzrp;->zzbhn:Landroid/os/WorkSource;

    :goto_16
    iget-object v0, p0, Lcom/google/android/gms/internal/zzrp;->zzbhm:Landroid/os/PowerManager$WakeLock;

    iget-object v1, p0, Lcom/google/android/gms/internal/zzrp;->zzbhn:Landroid/os/WorkSource;

    invoke-virtual {v0, v1}, Landroid/os/PowerManager$WakeLock;->setWorkSource(Landroid/os/WorkSource;)V

    :cond_1d
    return-void
.end method

.class Lbr/com/itau/widgets/hintview/Hint$7;
.super Ljava/lang/Object;
.source "Hint.java"

# interfaces
.implements Landroid/widget/PopupWindow$OnDismissListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lbr/com/itau/widgets/hintview/Hint;-><init>(Lbr/com/itau/widgets/hintview/Hint$Builder;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lbr/com/itau/widgets/hintview/Hint;


# direct methods
.method constructor <init>(Lbr/com/itau/widgets/hintview/Hint;)V
    .registers 2
    .param p1, "this$0"    # Lbr/com/itau/widgets/hintview/Hint;

    .line 172
    iput-object p1, p0, Lbr/com/itau/widgets/hintview/Hint$7;->this$0:Lbr/com/itau/widgets/hintview/Hint;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDismiss()V
    .registers 3

    .line 175
    iget-object v0, p0, Lbr/com/itau/widgets/hintview/Hint$7;->this$0:Lbr/com/itau/widgets/hintview/Hint;

    # getter for: Lbr/com/itau/widgets/hintview/Hint;->mAnchorView:Landroid/view/View;
    invoke-static {v0}, Lbr/com/itau/widgets/hintview/Hint;->access$500(Lbr/com/itau/widgets/hintview/Hint;)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lbr/com/itau/widgets/hintview/Hint$7;->this$0:Lbr/com/itau/widgets/hintview/Hint;

    # getter for: Lbr/com/itau/widgets/hintview/Hint;->mOnAttachStateChangeListener:Landroid/view/View$OnAttachStateChangeListener;
    invoke-static {v1}, Lbr/com/itau/widgets/hintview/Hint;->access$2200(Lbr/com/itau/widgets/hintview/Hint;)Landroid/view/View$OnAttachStateChangeListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->removeOnAttachStateChangeListener(Landroid/view/View$OnAttachStateChangeListener;)V

    .line 177
    iget-object v0, p0, Lbr/com/itau/widgets/hintview/Hint$7;->this$0:Lbr/com/itau/widgets/hintview/Hint;

    # getter for: Lbr/com/itau/widgets/hintview/Hint;->mOnDismissListener:Lbr/com/itau/widgets/hintview/OnDismissListener;
    invoke-static {v0}, Lbr/com/itau/widgets/hintview/Hint;->access$2300(Lbr/com/itau/widgets/hintview/Hint;)Lbr/com/itau/widgets/hintview/OnDismissListener;

    move-result-object v0

    if-eqz v0, :cond_20

    .line 178
    iget-object v0, p0, Lbr/com/itau/widgets/hintview/Hint$7;->this$0:Lbr/com/itau/widgets/hintview/Hint;

    # getter for: Lbr/com/itau/widgets/hintview/Hint;->mOnDismissListener:Lbr/com/itau/widgets/hintview/OnDismissListener;
    invoke-static {v0}, Lbr/com/itau/widgets/hintview/Hint;->access$2300(Lbr/com/itau/widgets/hintview/Hint;)Lbr/com/itau/widgets/hintview/OnDismissListener;

    move-result-object v0

    invoke-interface {v0}, Lbr/com/itau/widgets/hintview/OnDismissListener;->onDismiss()V

    .line 180
    :cond_20
    return-void
.end method

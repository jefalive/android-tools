.class public final Lcom/itau/empresas/feature/login/LoginNavegacao;
.super Ljava/lang/Object;
.source "LoginNavegacao.java"


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static redirecionaPara(Landroid/content/Context;ILcom/itau/empresas/CustomApplication;)V
    .registers 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "navegacao"    # I
    .param p2, "application"    # Lcom/itau/empresas/CustomApplication;

    .line 41
    packed-switch p1, :pswitch_data_58

    goto/16 :goto_50

    .line 43
    :pswitch_5
    invoke-static {p0}, Lcom/itau/empresas/feature/home/HomeLogadaActivity;->intent(Landroid/content/Context;)Lcom/itau/empresas/feature/home/HomeLogadaActivity_$IntentBuilder_;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/feature/home/HomeLogadaActivity_$IntentBuilder_;->start()Lorg/androidannotations/api/builder/PostActivityStarter;

    .line 44
    goto :goto_57

    .line 46
    :pswitch_d
    invoke-static {p0}, Lcom/itau/empresas/ui/activity/FingerprintActivity;->intent(Landroid/content/Context;)Lcom/itau/empresas/ui/activity/FingerprintActivity_$IntentBuilder_;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/ui/activity/FingerprintActivity_$IntentBuilder_;->start()Lorg/androidannotations/api/builder/PostActivityStarter;

    .line 47
    invoke-virtual {p2}, Lcom/itau/empresas/CustomApplication;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "FP"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 48
    goto :goto_57

    .line 50
    :pswitch_27
    invoke-static {p0}, Lcom/itau/empresas/ui/activity/FingerprintActivity;->intent(Landroid/content/Context;)Lcom/itau/empresas/ui/activity/FingerprintActivity_$IntentBuilder_;

    move-result-object v0

    const-string v1, "status"

    const-string v2, "HABILITADO_OUTRO_DISPOSITIVO"

    invoke-virtual {v0, v1, v2}, Lcom/itau/empresas/ui/activity/FingerprintActivity_$IntentBuilder_;->extra(Ljava/lang/String;Ljava/lang/String;)Lorg/androidannotations/api/builder/IntentBuilder;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/ui/activity/FingerprintActivity_$IntentBuilder_;

    .line 51
    invoke-virtual {v0}, Lcom/itau/empresas/ui/activity/FingerprintActivity_$IntentBuilder_;->start()Lorg/androidannotations/api/builder/PostActivityStarter;

    .line 52
    goto :goto_57

    .line 54
    :pswitch_39
    invoke-static {p0}, Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity;->intent(Landroid/content/Context;)Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity_$IntentBuilder_;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity_$IntentBuilder_;->start()Lorg/androidannotations/api/builder/PostActivityStarter;

    .line 55
    goto :goto_57

    .line 57
    :pswitch_41
    invoke-static {p0}, Lcom/itau/empresas/feature/home/HomeLogadaActivity;->intent(Landroid/content/Context;)Lcom/itau/empresas/feature/home/HomeLogadaActivity_$IntentBuilder_;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/feature/home/HomeLogadaActivity_$IntentBuilder_;->start()Lorg/androidannotations/api/builder/PostActivityStarter;

    .line 58
    invoke-static {p0}, Lcom/itau/empresas/feature/itoken/ITokenActivity;->intent(Landroid/content/Context;)Lcom/itau/empresas/feature/itoken/ITokenActivity_$IntentBuilder_;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/feature/itoken/ITokenActivity_$IntentBuilder_;->start()Lorg/androidannotations/api/builder/PostActivityStarter;

    .line 59
    goto :goto_57

    .line 64
    :goto_50
    :pswitch_50
    invoke-static {p0}, Lcom/itau/empresas/feature/home/HomeLogadaActivity;->intent(Landroid/content/Context;)Lcom/itau/empresas/feature/home/HomeLogadaActivity_$IntentBuilder_;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/feature/home/HomeLogadaActivity_$IntentBuilder_;->start()Lorg/androidannotations/api/builder/PostActivityStarter;

    .line 68
    :goto_57
    return-void

    :pswitch_data_58
    .packed-switch 0x1
        :pswitch_5
        :pswitch_27
        :pswitch_d
        :pswitch_39
        :pswitch_41
        :pswitch_50
        :pswitch_50
        :pswitch_50
    .end packed-switch
.end method

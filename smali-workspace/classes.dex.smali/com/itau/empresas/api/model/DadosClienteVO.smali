.class public Lcom/itau/empresas/api/model/DadosClienteVO;
.super Ljava/lang/Object;
.source "DadosClienteVO.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private agencia:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "agencia"
    .end annotation
.end field

.field private conta:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "conta"
    .end annotation
.end field

.field private cpf:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "cpf"
    .end annotation
.end field

.field private cpfCnpjSemFormatacao:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "cpf_cnpj"
    .end annotation
.end field

.field private digitoVerificadorDaConta:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "digito_verificador_conta"
    .end annotation
.end field

.field private nomeCliente:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "nome_cliente"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getAgencia()Ljava/lang/String;
    .registers 2

    .line 29
    iget-object v0, p0, Lcom/itau/empresas/api/model/DadosClienteVO;->agencia:Ljava/lang/String;

    return-object v0
.end method

.method public getConta()Ljava/lang/String;
    .registers 2

    .line 38
    iget-object v0, p0, Lcom/itau/empresas/api/model/DadosClienteVO;->conta:Ljava/lang/String;

    return-object v0
.end method

.method public getDigitoVerificadorDaConta()Ljava/lang/String;
    .registers 2

    .line 46
    iget-object v0, p0, Lcom/itau/empresas/api/model/DadosClienteVO;->digitoVerificadorDaConta:Ljava/lang/String;

    return-object v0
.end method

.method public setAgencia(Ljava/lang/String;)V
    .registers 2
    .param p1, "agencia"    # Ljava/lang/String;

    .line 33
    iput-object p1, p0, Lcom/itau/empresas/api/model/DadosClienteVO;->agencia:Ljava/lang/String;

    .line 34
    return-void
.end method

.method public setConta(Ljava/lang/String;)V
    .registers 2
    .param p1, "conta"    # Ljava/lang/String;

    .line 42
    iput-object p1, p0, Lcom/itau/empresas/api/model/DadosClienteVO;->conta:Ljava/lang/String;

    .line 43
    return-void
.end method

.method public setDigitoVerificadorDaConta(Ljava/lang/String;)V
    .registers 2
    .param p1, "digitoVerificadorDaConta"    # Ljava/lang/String;

    .line 50
    iput-object p1, p0, Lcom/itau/empresas/api/model/DadosClienteVO;->digitoVerificadorDaConta:Ljava/lang/String;

    .line 51
    return-void
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    .line 67
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "DadosClienteVO{agencia=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/api/model/DadosClienteVO;->agencia:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", conta=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/api/model/DadosClienteVO;->conta:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", digitoVerificadorDaConta=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/api/model/DadosClienteVO;->digitoVerificadorDaConta:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", cpfCnpjSemFormatacao=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/api/model/DadosClienteVO;->cpfCnpjSemFormatacao:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", cpf=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/api/model/DadosClienteVO;->cpf:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", nomeCliente=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/api/model/DadosClienteVO;->nomeCliente:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

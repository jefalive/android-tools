.class public Lbr/com/itau/sdk/android/core/util/TypefaceSpan;
.super Landroid/text/style/MetricAffectingSpan;
.source "SourceFile"


# static fields
.field private static final $:[B

.field private static $$:I


# instance fields
.field private final typeface:Landroid/graphics/Typeface;


# direct methods
.method private static $(III)Ljava/lang/String;
    .registers 9

    sget-object v5, Lbr/com/itau/sdk/android/core/util/TypefaceSpan;->$:[B

    new-instance v0, Ljava/lang/String;

    const/4 v4, 0x0

    add-int/lit8 p1, p1, 0x4

    mul-int/lit8 p0, p0, 0x2

    add-int/lit8 p0, p0, 0x10

    mul-int/lit8 p2, p2, 0x4

    rsub-int/lit8 p2, p2, 0x74

    new-array v1, p0, [B

    if-nez v5, :cond_18

    move v2, p1

    move v3, p0

    :goto_15
    add-int/2addr v2, v3

    add-int/lit8 p2, v2, -0x2

    :cond_18
    add-int/lit8 p1, p1, 0x1

    move v2, v4

    add-int/lit8 v4, v4, 0x1

    int-to-byte v3, p2

    aput-byte v3, v1, v2

    if-ne v4, p0, :cond_27

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Ljava/lang/String;-><init>([BI)V

    return-object v0

    :cond_27
    move v2, p2

    aget-byte v3, v5, p1

    goto :goto_15
.end method

.method static constructor <clinit>()V
    .registers 3

    const/16 v0, 0x13

    new-array v0, v0, [B

    fill-array-data v0, :array_e

    sput-object v0, Lbr/com/itau/sdk/android/core/util/TypefaceSpan;->$:[B

    const/16 v0, 0x6f

    sput v0, Lbr/com/itau/sdk/android/core/util/TypefaceSpan;->$$:I

    return-void

    :array_e
    .array-data 1
        0x24t
        -0x4bt
        0x2at
        -0x29t
        0x7t
        -0x7t
        -0x9t
        0x3t
        -0x3t
        0x4t
        0x4t
        -0x43t
        0x4bt
        0xct
        -0x51t
        0x50t
        0x9t
        -0x7t
        0x2t
    .end array-data
.end method

.method public constructor <init>(Landroid/graphics/Typeface;)V
    .registers 6

    .line 11
    invoke-direct {p0}, Landroid/text/style/MetricAffectingSpan;-><init>()V

    .line 12
    if-nez p1, :cond_12

    .line 13
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const/4 v1, 0x0

    const/4 v2, -0x1

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Lbr/com/itau/sdk/android/core/util/TypefaceSpan;->$(III)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 16
    :cond_12
    iput-object p1, p0, Lbr/com/itau/sdk/android/core/util/TypefaceSpan;->typeface:Landroid/graphics/Typeface;

    .line 17
    return-void
.end method

.method private apply(Landroid/graphics/Paint;)V
    .registers 7

    .line 35
    invoke-virtual {p1}, Landroid/graphics/Paint;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v2

    .line 36
    if-eqz v2, :cond_b

    invoke-virtual {v2}, Landroid/graphics/Typeface;->getStyle()I

    move-result v3

    goto :goto_c

    :cond_b
    const/4 v3, 0x0

    .line 37
    :goto_c
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/util/TypefaceSpan;->typeface:Landroid/graphics/Typeface;

    invoke-virtual {v0}, Landroid/graphics/Typeface;->getStyle()I

    move-result v0

    xor-int/lit8 v0, v0, -0x1

    and-int v4, v3, v0

    .line 39
    and-int/lit8 v0, v4, 0x1

    if-eqz v0, :cond_1e

    .line 40
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setFakeBoldText(Z)V

    .line 43
    :cond_1e
    and-int/lit8 v0, v4, 0x2

    if-eqz v0, :cond_27

    .line 44
    const/high16 v0, -0x41800000    # -0.25f

    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setTextSkewX(F)V

    .line 47
    :cond_27
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/util/TypefaceSpan;->typeface:Landroid/graphics/Typeface;

    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 48
    return-void
.end method


# virtual methods
.method public updateDrawState(Landroid/text/TextPaint;)V
    .registers 2

    .line 26
    invoke-direct {p0, p1}, Lbr/com/itau/sdk/android/core/util/TypefaceSpan;->apply(Landroid/graphics/Paint;)V

    .line 27
    return-void
.end method

.method public updateMeasureState(Landroid/text/TextPaint;)V
    .registers 2

    .line 31
    invoke-direct {p0, p1}, Lbr/com/itau/sdk/android/core/util/TypefaceSpan;->apply(Landroid/graphics/Paint;)V

    .line 32
    return-void
.end method

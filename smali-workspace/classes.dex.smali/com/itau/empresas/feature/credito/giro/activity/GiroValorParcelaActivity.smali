.class public Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity;
.super Lcom/itau/empresas/ui/activity/BaseActivity;
.source "GiroValorParcelaActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity$Exibicao;
    }
.end annotation


# instance fields
.field campoEntrada:Lbr/com/itau/widgets/material/MaterialEditText;

.field exibicao:Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity$Exibicao;

.field ofertaEspecialAceita:Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;

.field parcelaMaxima:J

.field parcelaMinima:J

.field produtosParceladosSimulacaoPropostaSaidaVO:Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoPropostaSaidaVO;

.field temSeguro:Z

.field textoAuxiliar:Landroid/widget/TextView;

.field textoIndicador:Landroid/widget/TextView;

.field textoPergunta:Landroid/widget/TextView;

.field toolBar:Landroid/support/v7/widget/Toolbar;

.field valorMaximo:F

.field valorMinimo:F


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 38
    invoke-direct {p0}, Lcom/itau/empresas/ui/activity/BaseActivity;-><init>()V

    return-void
.end method

.method private constroiToolbar()V
    .registers 3

    .line 210
    iget-boolean v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity;->temSeguro:Z

    if-eqz v0, :cond_8

    .line 211
    const v1, 0x7f070699

    .local v1, "id":I
    goto :goto_b

    .line 214
    .end local v1    # "id":I
    :cond_8
    const v1, 0x7f07069a

    .line 217
    .local v1, "id":I
    :goto_b
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity;->toolBar:Landroid/support/v7/widget/Toolbar;

    invoke-static {v0, p0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom;->Builder(Landroid/support/v7/widget/Toolbar;Landroid/support/v7/app/AppCompatActivity;)Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    move-result-object v0

    .line 219
    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->comTitulo(I)Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    move-result-object v0

    .line 220
    invoke-virtual {v0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->comClickListenerFinish()Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    move-result-object v0

    .line 221
    invoke-virtual {v0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->build()Lcom/itau/empresas/ui/util/listener/ToolbarCustom;

    .line 222
    return-void
.end method

.method public static intent(Landroid/content/Context;)Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity_$IntentBuilder_;
    .registers 2
    .param p0, "context"    # Landroid/content/Context;

    .line 244
    new-instance v0, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity_$IntentBuilder_;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity_$IntentBuilder_;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method private navegarAteSelecaoDeData(F)V
    .registers 7
    .param p1, "valorDigitado"    # F

    .line 166
    float-to-double v0, p1

    invoke-static {v0, v1}, Ljava/math/BigDecimal;->valueOf(D)Ljava/math/BigDecimal;

    move-result-object v3

    .line 167
    .local v3, "valorBigDecimal":Ljava/math/BigDecimal;
    invoke-static {p1}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v4

    .line 170
    .local v4, "valorComoString":Ljava/lang/String;
    const-string v0, ".0"

    invoke-virtual {v4, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 172
    new-instance v0, Ljava/math/BigDecimal;

    invoke-virtual {v3}, Ljava/math/BigDecimal;->longValue()J

    move-result-wide v1

    invoke-direct {v0, v1, v2}, Ljava/math/BigDecimal;-><init>(J)V

    move-object v3, v0

    goto :goto_23

    .line 176
    :cond_1c
    sget-object v0, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    const/4 v1, 0x2

    invoke-virtual {v3, v1, v0}, Ljava/math/BigDecimal;->setScale(ILjava/math/RoundingMode;)Ljava/math/BigDecimal;

    move-result-object v3

    .line 179
    :goto_23
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity;->ofertaEspecialAceita:Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;

    invoke-virtual {v0, v3}, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;->setValorEmprestimo(Ljava/math/BigDecimal;)V

    .line 181
    invoke-static {p0}, Lcom/itau/empresas/feature/credito/giro/activity/GiroDataActivity;->intent(Landroid/content/Context;)Lcom/itau/empresas/feature/credito/giro/activity/GiroDataActivity_$IntentBuilder_;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity;->ofertaEspecialAceita:Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;

    .line 182
    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/credito/giro/activity/GiroDataActivity_$IntentBuilder_;->ofertaEspecialAceita(Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;)Lcom/itau/empresas/feature/credito/giro/activity/GiroDataActivity_$IntentBuilder_;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity;->produtosParceladosSimulacaoPropostaSaidaVO:Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoPropostaSaidaVO;

    .line 183
    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/credito/giro/activity/GiroDataActivity_$IntentBuilder_;->simulacaoPropostaSaidaVO(Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoPropostaSaidaVO;)Lcom/itau/empresas/feature/credito/giro/activity/GiroDataActivity_$IntentBuilder_;

    move-result-object v0

    .line 184
    invoke-virtual {v0}, Lcom/itau/empresas/feature/credito/giro/activity/GiroDataActivity_$IntentBuilder_;->start()Lorg/androidannotations/api/builder/PostActivityStarter;

    .line 185
    return-void
.end method

.method private sinalizarEntradaInvalida()V
    .registers 4

    .line 226
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity;->textoAuxiliar:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c0150

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 227
    return-void
.end method

.method private voltarNavegacaoComResultadoDaParcela()V
    .registers 7

    .line 189
    new-instance v4, Landroid/content/Intent;

    invoke-direct {v4}, Landroid/content/Intent;-><init>()V

    .line 191
    .local v4, "intent":Landroid/content/Intent;
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity;->campoEntrada:Lbr/com/itau/widgets/material/MaterialEditText;

    invoke-virtual {v0}, Lbr/com/itau/widgets/material/MaterialEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    .line 193
    .local v5, "valorDigitado":I
    iget-wide v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity;->parcelaMinima:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_2d

    int-to-long v0, v5

    iget-wide v2, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity;->parcelaMinima:J

    cmp-long v0, v0, v2

    if-ltz v0, :cond_29

    int-to-long v0, v5

    iget-wide v2, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity;->parcelaMaxima:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_2d

    .line 196
    :cond_29
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity;->sinalizarEntradaInvalida()V

    .line 197
    return-void

    .line 200
    :cond_2d
    const-string v0, "parcela"

    invoke-virtual {v4, v0, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 202
    const/4 v0, -0x1

    invoke-virtual {p0, v0, v4}, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity;->setResult(ILandroid/content/Intent;)V

    .line 204
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity;->finish()V

    .line 206
    return-void
.end method


# virtual methods
.method aoCarregarTela()V
    .registers 9

    .line 81
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity;->constroiToolbar()V

    .line 83
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity;->exibicao:Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity$Exibicao;

    if-eqz v0, :cond_d2

    .line 88
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity;->campoEntrada:Lbr/com/itau/widgets/material/MaterialEditText;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lbr/com/itau/widgets/material/MaterialEditText;->setBottomTextSize(I)V

    .line 90
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity;->exibicao:Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity$Exibicao;

    sget-object v1, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity$Exibicao;->PARCELA:Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity$Exibicao;

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity$Exibicao;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4c

    .line 92
    const v0, 0x7f070632

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 93
    .local v4, "pergunta":Ljava/lang/String;
    const v0, 0x7f070563

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 95
    .local v5, "indicador":Ljava/lang/String;
    const v0, 0x7f070559

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    iget-wide v2, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity;->parcelaMinima:J

    .line 96
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    iget-wide v2, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity;->parcelaMaxima:J

    .line 97
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    const/4 v3, 0x1

    aput-object v2, v1, v3

    .line 95
    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    .line 99
    .local v6, "auxiliar":Ljava/lang/String;
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity;->campoEntrada:Lbr/com/itau/widgets/material/MaterialEditText;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lbr/com/itau/widgets/material/MaterialEditText;->setInputType(I)V

    goto :goto_8c

    .line 104
    .end local v4    # "pergunta":Ljava/lang/String;
    .end local v5    # "indicador":Ljava/lang/String;
    .end local v6    # "auxiliar":Ljava/lang/String;
    :cond_4c
    const v0, 0x7f070643

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 105
    .local v4, "pergunta":Ljava/lang/String;
    const v0, 0x7f070564

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 106
    .local v5, "indicador":Ljava/lang/String;
    const v0, 0x7f07055a

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    iget v2, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity;->valorMinimo:F

    .line 107
    const/4 v3, 0x1

    invoke-static {v2, v3}, Lcom/itau/empresas/ui/util/ValorMonetarioUtils;->formataValorDecimalParaMonetario(FZ)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    iget v2, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity;->valorMaximo:F

    .line 108
    const/4 v3, 0x1

    invoke-static {v2, v3}, Lcom/itau/empresas/ui/util/ValorMonetarioUtils;->formataValorDecimalParaMonetario(FZ)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    aput-object v2, v1, v3

    .line 106
    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    .line 110
    .local v6, "auxiliar":Ljava/lang/String;
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity;->campoEntrada:Lbr/com/itau/widgets/material/MaterialEditText;

    const-string v1, "[R$,.]"

    sget-object v2, Lcom/itau/empresas/ui/util/MascaraValorMonetario$Moeda;->REAL:Lcom/itau/empresas/ui/util/MascaraValorMonetario$Moeda;

    .line 111
    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, Lcom/itau/empresas/ui/util/MascaraValorMonetario;->insere(Landroid/widget/EditText;Ljava/lang/String;Lcom/itau/empresas/ui/util/MascaraValorMonetario$Moeda;Lcom/itau/empresas/ui/util/INotificarAlteracaoView;)Landroid/text/TextWatcher;

    move-result-object v7

    .line 113
    .local v7, "mascara":Landroid/text/TextWatcher;
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity;->campoEntrada:Lbr/com/itau/widgets/material/MaterialEditText;

    invoke-virtual {v0, v7}, Lbr/com/itau/widgets/material/MaterialEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 117
    .end local v7    # "mascara":Landroid/text/TextWatcher;
    :goto_8c
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity;->campoEntrada:Lbr/com/itau/widgets/material/MaterialEditText;

    const/4 v1, 0x1

    new-array v1, v1, [Landroid/text/InputFilter;

    iget-object v2, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity;->exibicao:Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity$Exibicao;

    sget-object v3, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity$Exibicao;->PARCELA:Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity$Exibicao;

    .line 119
    invoke-virtual {v2, v3}, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity$Exibicao;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_a2

    new-instance v2, Landroid/text/InputFilter$LengthFilter;

    const/4 v3, 0x2

    invoke-direct {v2, v3}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    goto :goto_a9

    :cond_a2
    new-instance v2, Landroid/text/InputFilter$LengthFilter;

    const/16 v3, 0xf

    invoke-direct {v2, v3}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    :goto_a9
    const/4 v3, 0x0

    aput-object v2, v1, v3

    .line 117
    invoke-virtual {v0, v1}, Lbr/com/itau/widgets/material/MaterialEditText;->setFilters([Landroid/text/InputFilter;)V

    .line 124
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity;->textoPergunta:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 125
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity;->textoIndicador:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 126
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity;->textoAuxiliar:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 128
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity;->campoEntrada:Lbr/com/itau/widgets/material/MaterialEditText;

    invoke-virtual {v0}, Lbr/com/itau/widgets/material/MaterialEditText;->requestFocus()Z

    .line 129
    const-string v0, "input_method"

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Landroid/view/inputmethod/InputMethodManager;

    .line 131
    .local v7, "keyboard":Landroid/view/inputmethod/InputMethodManager;
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity;->campoEntrada:Lbr/com/itau/widgets/material/MaterialEditText;

    const/4 v1, 0x0

    invoke-virtual {v7, v0, v1}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    .line 133
    .end local v4    # "pergunta":Ljava/lang/String;
    .end local v5    # "indicador":Ljava/lang/String;
    .end local v6    # "auxiliar":Ljava/lang/String;
    .end local v7    # "keyboard":Landroid/view/inputmethod/InputMethodManager;
    :cond_d2
    return-void
.end method

.method aoClicarNoBotaoContinuar()V
    .registers 4

    .line 137
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity;->campoEntrada:Lbr/com/itau/widgets/material/MaterialEditText;

    invoke-virtual {v0}, Lbr/com/itau/widgets/material/MaterialEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 138
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity;->sinalizarEntradaInvalida()V

    .line 139
    return-void

    .line 142
    :cond_14
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity;->exibicao:Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity$Exibicao;

    sget-object v1, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity$Exibicao;->PARCELA:Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity$Exibicao;

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity$Exibicao;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_22

    .line 144
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity;->voltarNavegacaoComResultadoDaParcela()V

    goto :goto_4d

    .line 149
    :cond_22
    const-string v0, "[R$,.]"

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity;->campoEntrada:Lbr/com/itau/widgets/material/MaterialEditText;

    .line 151
    invoke-virtual {v1}, Lbr/com/itau/widgets/material/MaterialEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 150
    invoke-static {v0, v1}, Lcom/itau/empresas/ui/util/MascaraValorMonetario;->removeMascara(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 149
    invoke-static {v0}, Lcom/itau/empresas/ui/util/MascaraValorMonetario;->formataStringValorMonetarioParaDecimal(Ljava/lang/String;)Ljava/math/BigDecimal;

    move-result-object v0

    .line 151
    invoke-virtual {v0}, Ljava/math/BigDecimal;->floatValue()F

    move-result v2

    .line 153
    .local v2, "valorDigitado":F
    iget v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity;->valorMinimo:F

    cmpg-float v0, v2, v0

    if-ltz v0, :cond_46

    iget v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity;->valorMaximo:F

    cmpl-float v0, v2, v0

    if-lez v0, :cond_4a

    .line 155
    :cond_46
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity;->sinalizarEntradaInvalida()V

    .line 156
    return-void

    .line 159
    :cond_4a
    invoke-direct {p0, v2}, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity;->navegarAteSelecaoDeData(F)V

    .line 162
    .end local v2    # "valorDigitado":F
    :goto_4d
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .registers 2
    .param p1, "v"    # Landroid/view/View;

    .line 230
    invoke-static {p0}, Lcom/itau/empresas/ui/util/TecladoUtils;->esconderTeclado(Landroid/app/Activity;)V

    .line 231
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity;->onBackPressed()V

    .line 232
    return-void
.end method

.method public opkeysConsumidas()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Ljava/lang/String;>;"
        }
    .end annotation

    .line 240
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

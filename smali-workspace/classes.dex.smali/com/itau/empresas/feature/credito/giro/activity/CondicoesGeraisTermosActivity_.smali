.class public final Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisTermosActivity_;
.super Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisTermosActivity;
.source "CondicoesGeraisTermosActivity_.java"

# interfaces
.implements Lorg/androidannotations/api/view/HasViews;
.implements Lorg/androidannotations/api/view/OnViewChangedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisTermosActivity_$IntentBuilder_;
    }
.end annotation


# instance fields
.field private final onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;


# direct methods
.method public constructor <init>()V
    .registers 2

    .line 32
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisTermosActivity;-><init>()V

    .line 36
    new-instance v0, Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-direct {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisTermosActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    return-void
.end method

.method static synthetic access$001(Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisTermosActivity_;)V
    .registers 1
    .param p0, "x0"    # Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisTermosActivity_;

    .line 32
    invoke-super {p0}, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisTermosActivity;->mostraDialogoDeProgresso()V

    return-void
.end method

.method static synthetic access$101(Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisTermosActivity_;)V
    .registers 1
    .param p0, "x0"    # Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisTermosActivity_;

    .line 32
    invoke-super {p0}, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisTermosActivity;->escondeDialogoDeProgresso()V

    return-void
.end method

.method private init_(Landroid/os/Bundle;)V
    .registers 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 49
    new-instance v0, Lcom/itau/empresas/Preferences_;

    invoke-direct {v0, p0}, Lcom/itau/empresas/Preferences_;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisTermosActivity_;->prefs:Lcom/itau/empresas/Preferences_;

    .line 50
    invoke-static {p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->registerOnViewChangedListener(Lorg/androidannotations/api/view/OnViewChangedListener;)V

    .line 51
    invoke-static {}, Lcom/itau/empresas/CustomApplication_;->getInstance()Lcom/itau/empresas/CustomApplication;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisTermosActivity_;->application:Lcom/itau/empresas/CustomApplication;

    .line 52
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisTermosActivity_;->injectExtras_()V

    .line 53
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisTermosActivity_;->afterInject()V

    .line 54
    return-void
.end method

.method private injectExtras_()V
    .registers 3

    .line 94
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisTermosActivity_;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 95
    .local v1, "extras_":Landroid/os/Bundle;
    if-eqz v1, :cond_1a

    .line 96
    const-string v0, "termo"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 97
    const-string v0, "termo"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisTermosActivity_;->termo:Ljava/lang/String;

    .line 100
    :cond_1a
    return-void
.end method


# virtual methods
.method public escondeDialogoDeProgresso()V
    .registers 5

    .line 125
    const-string v0, ""

    new-instance v1, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisTermosActivity_$2;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisTermosActivity_$2;-><init>(Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisTermosActivity_;)V

    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3}, Lorg/androidannotations/api/UiThreadExecutor;->runTask(Ljava/lang/String;Ljava/lang/Runnable;J)V

    .line 133
    return-void
.end method

.method public mostraDialogoDeProgresso()V
    .registers 5
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "CommitTransaction"
        }
    .end annotation

    .line 113
    const-string v0, ""

    new-instance v1, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisTermosActivity_$1;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisTermosActivity_$1;-><init>(Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisTermosActivity_;)V

    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3}, Lorg/androidannotations/api/UiThreadExecutor;->runTask(Ljava/lang/String;Ljava/lang/Runnable;J)V

    .line 121
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 41
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisTermosActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-static {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    move-result-object v1

    .line 42
    .local v1, "previousNotifier":Lorg/androidannotations/api/view/OnViewChangedNotifier;
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisTermosActivity_;->init_(Landroid/os/Bundle;)V

    .line 43
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisTermosActivity;->onCreate(Landroid/os/Bundle;)V

    .line 44
    invoke-static {v1}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 45
    const v0, 0x7f030028

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisTermosActivity_;->setContentView(I)V

    .line 46
    return-void
.end method

.method public onViewChanged(Lorg/androidannotations/api/view/HasViews;)V
    .registers 3
    .param p1, "hasViews"    # Lorg/androidannotations/api/view/HasViews;

    .line 88
    const v0, 0x7f0e0117

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/webkit/WebView;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisTermosActivity_;->condicoesGeraisTermosWebview:Landroid/webkit/WebView;

    .line 89
    const v0, 0x7f0e00b6

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/Toolbar;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisTermosActivity_;->toolBar:Landroid/support/v7/widget/Toolbar;

    .line 90
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisTermosActivity_;->aoCarregarElementosDeTela()V

    .line 91
    return-void
.end method

.method public setContentView(I)V
    .registers 3
    .param p1, "layoutResID"    # I

    .line 58
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisTermosActivity;->setContentView(I)V

    .line 59
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisTermosActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 60
    return-void
.end method

.method public setContentView(Landroid/view/View;)V
    .registers 3
    .param p1, "view"    # Landroid/view/View;

    .line 70
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisTermosActivity;->setContentView(Landroid/view/View;)V

    .line 71
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisTermosActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 72
    return-void
.end method

.method public setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .registers 4
    .param p1, "view"    # Landroid/view/View;
    .param p2, "params"    # Landroid/view/ViewGroup$LayoutParams;

    .line 64
    invoke-super {p0, p1, p2}, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisTermosActivity;->setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 65
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisTermosActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 66
    return-void
.end method

.method public setIntent(Landroid/content/Intent;)V
    .registers 2
    .param p1, "newIntent"    # Landroid/content/Intent;

    .line 104
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisTermosActivity;->setIntent(Landroid/content/Intent;)V

    .line 105
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisTermosActivity_;->injectExtras_()V

    .line 106
    return-void
.end method

.class public final Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaActivity_;
.super Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaActivity;
.source "LisConsultaActivity_.java"

# interfaces
.implements Lorg/androidannotations/api/view/HasViews;
.implements Lorg/androidannotations/api/view/OnViewChangedListener;


# instance fields
.field private final onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;


# direct methods
.method public constructor <init>()V
    .registers 2

    .line 36
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaActivity;-><init>()V

    .line 40
    new-instance v0, Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-direct {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    return-void
.end method

.method static synthetic access$001(Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaActivity_;)V
    .registers 1
    .param p0, "x0"    # Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaActivity_;

    .line 36
    invoke-super {p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaActivity;->mostraDialogoDeProgresso()V

    return-void
.end method

.method static synthetic access$101(Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaActivity_;)V
    .registers 1
    .param p0, "x0"    # Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaActivity_;

    .line 36
    invoke-super {p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaActivity;->escondeDialogoDeProgresso()V

    return-void
.end method

.method private init_(Landroid/os/Bundle;)V
    .registers 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 52
    new-instance v0, Lcom/itau/empresas/Preferences_;

    invoke-direct {v0, p0}, Lcom/itau/empresas/Preferences_;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaActivity_;->prefs:Lcom/itau/empresas/Preferences_;

    .line 53
    invoke-static {p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->registerOnViewChangedListener(Lorg/androidannotations/api/view/OnViewChangedListener;)V

    .line 54
    invoke-static {}, Lcom/itau/empresas/CustomApplication_;->getInstance()Lcom/itau/empresas/CustomApplication;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaActivity_;->application:Lcom/itau/empresas/CustomApplication;

    .line 55
    invoke-static {p0}, Lcom/itau/empresas/feature/credito/lis/LisController_;->getInstance_(Landroid/content/Context;)Lcom/itau/empresas/feature/credito/lis/LisController_;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaActivity_;->controller:Lcom/itau/empresas/feature/credito/lis/LisController;

    .line 56
    invoke-static {p0}, Lcom/itau/empresas/controller/CreditoProdutosController_;->getInstance_(Landroid/content/Context;)Lcom/itau/empresas/controller/CreditoProdutosController_;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaActivity_;->creditoProdutosController:Lcom/itau/empresas/controller/CreditoProdutosController;

    .line 57
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaActivity_;->afterInject()V

    .line 58
    return-void
.end method


# virtual methods
.method public escondeDialogoDeProgresso()V
    .registers 5

    .line 131
    const-string v0, ""

    new-instance v1, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaActivity_$3;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaActivity_$3;-><init>(Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaActivity_;)V

    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3}, Lorg/androidannotations/api/UiThreadExecutor;->runTask(Ljava/lang/String;Ljava/lang/Runnable;J)V

    .line 139
    return-void
.end method

.method public mostraDialogoDeProgresso()V
    .registers 5
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "CommitTransaction"
        }
    .end annotation

    .line 119
    const-string v0, ""

    new-instance v1, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaActivity_$2;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaActivity_$2;-><init>(Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaActivity_;)V

    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3}, Lorg/androidannotations/api/UiThreadExecutor;->runTask(Ljava/lang/String;Ljava/lang/Runnable;J)V

    .line 127
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 44
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-static {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    move-result-object v1

    .line 45
    .local v1, "previousNotifier":Lorg/androidannotations/api/view/OnViewChangedNotifier;
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaActivity_;->init_(Landroid/os/Bundle;)V

    .line 46
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaActivity;->onCreate(Landroid/os/Bundle;)V

    .line 47
    invoke-static {v1}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 48
    const v0, 0x7f030047

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaActivity_;->setContentView(I)V

    .line 49
    return-void
.end method

.method public onViewChanged(Lorg/androidannotations/api/view/HasViews;)V
    .registers 4
    .param p1, "hasViews"    # Lorg/androidannotations/api/view/HasViews;

    .line 92
    const v0, 0x7f0e01f2

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaActivity_;->mainLayout:Landroid/widget/RelativeLayout;

    .line 93
    const v0, 0x7f0e01f5

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaActivity_;->tituloComprovante:Landroid/widget/TextView;

    .line 94
    const v0, 0x7f0e01e6

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaActivity_;->textoValorLimite:Landroid/widget/TextView;

    .line 95
    const v0, 0x7f0e01f9

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaActivity_;->textoDiaPagamento:Landroid/widget/TextView;

    .line 96
    const v0, 0x7f0e00b6

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/Toolbar;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaActivity_;->toolBar:Landroid/support/v7/widget/Toolbar;

    .line 97
    const v0, 0x7f0e01f3

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaActivity_;->emptyStateView:Landroid/view/ViewGroup;

    .line 98
    const v0, 0x7f0e01eb

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaActivity_;->cardView:Landroid/view/View;

    .line 99
    const v0, 0x7f0e01e8

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 101
    .local v1, "view_botao_detalhe":Landroid/view/View;
    if-eqz v1, :cond_5c

    .line 102
    new-instance v0, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaActivity_$1;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaActivity_$1;-><init>(Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaActivity_;)V

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 111
    :cond_5c
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaActivity_;->aoCarregarTela()V

    .line 112
    return-void
.end method

.method public setContentView(I)V
    .registers 3
    .param p1, "layoutResID"    # I

    .line 62
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaActivity;->setContentView(I)V

    .line 63
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 64
    return-void
.end method

.method public setContentView(Landroid/view/View;)V
    .registers 3
    .param p1, "view"    # Landroid/view/View;

    .line 74
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaActivity;->setContentView(Landroid/view/View;)V

    .line 75
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 76
    return-void
.end method

.method public setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .registers 4
    .param p1, "view"    # Landroid/view/View;
    .param p2, "params"    # Landroid/view/ViewGroup$LayoutParams;

    .line 68
    invoke-super {p0, p1, p2}, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaActivity;->setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 69
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 70
    return-void
.end method

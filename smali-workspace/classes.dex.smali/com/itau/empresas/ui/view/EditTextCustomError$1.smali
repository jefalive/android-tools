.class Lcom/itau/empresas/ui/view/EditTextCustomError$1;
.super Ljava/lang/Object;
.source "EditTextCustomError.java"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/itau/empresas/ui/view/EditTextCustomError;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/itau/empresas/ui/view/EditTextCustomError;

.field final synthetic val$activityRootView:Landroid/view/View;


# direct methods
.method constructor <init>(Lcom/itau/empresas/ui/view/EditTextCustomError;Landroid/view/View;)V
    .registers 3
    .param p1, "this$0"    # Lcom/itau/empresas/ui/view/EditTextCustomError;

    .line 45
    iput-object p1, p0, Lcom/itau/empresas/ui/view/EditTextCustomError$1;->this$0:Lcom/itau/empresas/ui/view/EditTextCustomError;

    iput-object p2, p0, Lcom/itau/empresas/ui/view/EditTextCustomError$1;->val$activityRootView:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onGlobalLayout()V
    .registers 5

    .line 48
    iget-object v0, p0, Lcom/itau/empresas/ui/view/EditTextCustomError$1;->val$activityRootView:Landroid/view/View;

    .line 49
    invoke-virtual {v0}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    iget-object v1, p0, Lcom/itau/empresas/ui/view/EditTextCustomError$1;->val$activityRootView:Landroid/view/View;

    .line 50
    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v1

    sub-int v3, v0, v1

    .line 51
    .local v3, "heightDiff":I
    int-to-float v0, v3

    iget-object v1, p0, Lcom/itau/empresas/ui/view/EditTextCustomError$1;->this$0:Lcom/itau/empresas/ui/view/EditTextCustomError;

    invoke-virtual {v1}, Lcom/itau/empresas/ui/view/EditTextCustomError;->getContext()Landroid/content/Context;

    move-result-object v1

    const/high16 v2, 0x43480000    # 200.0f

    invoke-static {v1, v2}, Lcom/itau/empresas/ui/view/EditTextCustomError;->dpToPx(Landroid/content/Context;F)F

    move-result v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_28

    .line 52
    iget-object v0, p0, Lcom/itau/empresas/ui/view/EditTextCustomError$1;->this$0:Lcom/itau/empresas/ui/view/EditTextCustomError;

    # invokes: Lcom/itau/empresas/ui/view/EditTextCustomError;->removePopPupErro()V
    invoke-static {v0}, Lcom/itau/empresas/ui/view/EditTextCustomError;->access$000(Lcom/itau/empresas/ui/view/EditTextCustomError;)V

    .line 54
    :cond_28
    return-void
.end method

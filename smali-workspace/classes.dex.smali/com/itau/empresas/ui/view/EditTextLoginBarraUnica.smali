.class public Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;
.super Landroid/widget/LinearLayout;
.source "EditTextLoginBarraUnica.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica$BarraUnicaListener;
    }
.end annotation


# instance fields
.field private final DELAY:J

.field campoCodigoOperador:Landroid/widget/EditText;

.field campoConta:Landroid/widget/EditText;

.field private executando:Z

.field final hintAgencia:Ljava/lang/String;

.field final hintConta:Ljava/lang/String;

.field final hintContinue:Ljava/lang/String;

.field labelAgencia:Landroid/widget/TextView;

.field labelConta:Landroid/widget/TextView;

.field private listener:Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica$BarraUnicaListener;

.field private mudarHint:Z

.field private timer:Ljava/util/Timer;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 4
    .param p1, "context"    # Landroid/content/Context;

    .line 56
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 45
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->timer:Ljava/util/Timer;

    .line 46
    const-wide/16 v0, 0xbb8

    iput-wide v0, p0, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->DELAY:J

    .line 47
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->mudarHint:Z

    .line 48
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->executando:Z

    .line 50
    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f070352

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->hintAgencia:Ljava/lang/String;

    .line 51
    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f070355

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->hintConta:Ljava/lang/String;

    .line 52
    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f070538

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->hintContinue:Ljava/lang/String;

    .line 57
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .line 60
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 45
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->timer:Ljava/util/Timer;

    .line 46
    const-wide/16 v0, 0xbb8

    iput-wide v0, p0, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->DELAY:J

    .line 47
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->mudarHint:Z

    .line 48
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->executando:Z

    .line 50
    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f070352

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->hintAgencia:Ljava/lang/String;

    .line 51
    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f070355

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->hintConta:Ljava/lang/String;

    .line 52
    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f070538

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->hintContinue:Ljava/lang/String;

    .line 61
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .line 64
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 45
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->timer:Ljava/util/Timer;

    .line 46
    const-wide/16 v0, 0xbb8

    iput-wide v0, p0, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->DELAY:J

    .line 47
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->mudarHint:Z

    .line 48
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->executando:Z

    .line 50
    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f070352

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->hintAgencia:Ljava/lang/String;

    .line 51
    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f070355

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->hintConta:Ljava/lang/String;

    .line 52
    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f070538

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->hintContinue:Ljava/lang/String;

    .line 65
    return-void
.end method

.method static synthetic access$000(Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;)Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica$BarraUnicaListener;
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;

    .line 34
    iget-object v0, p0, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->listener:Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica$BarraUnicaListener;

    return-object v0
.end method


# virtual methods
.method alteraCampo()V
    .registers 6

    .line 271
    iget-boolean v0, p0, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->executando:Z

    if-nez v0, :cond_1aa

    .line 273
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->executando:Z

    .line 275
    iget-object v0, p0, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->timer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 276
    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->getConteudo()Ljava/lang/String;

    move-result-object v3

    .line 278
    .local v3, "conteudo":Ljava/lang/String;
    iget-object v0, p0, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->campoCodigoOperador:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->length()I

    move-result v0

    if-lez v0, :cond_14f

    .line 279
    iget-object v0, p0, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->labelAgencia:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 280
    iget-object v0, p0, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->labelConta:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 282
    iget-object v0, p0, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->labelAgencia:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 283
    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f070353

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_51

    .line 284
    iget-object v0, p0, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->labelAgencia:Landroid/widget/TextView;

    .line 285
    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 286
    const v2, 0x7f070354

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 285
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1aa

    .line 287
    :cond_51
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v1, 0xa

    if-ne v0, v1, :cond_c3

    .line 288
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->mudarHint:Z

    .line 289
    iget-object v0, p0, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->timer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 290
    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/ui/activity/BaseActivity;

    iget-boolean v1, p0, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->mudarHint:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/activity/BaseActivity;->publicaEvento(Ljava/lang/Object;)V

    .line 291
    iget-object v0, p0, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->labelAgencia:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->hintAgencia:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 292
    iget-object v0, p0, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->campoCodigoOperador:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->hintAgencia:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 293
    iget-object v0, p0, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->labelConta:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->hintConta:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 295
    const/4 v0, 0x1

    new-array v4, v0, [Landroid/text/InputFilter;

    .line 296
    .local v4, "fArray":[Landroid/text/InputFilter;
    new-instance v0, Landroid/text/InputFilter$LengthFilter;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    const/4 v1, 0x0

    aput-object v0, v4, v1

    .line 297
    iget-object v0, p0, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->campoCodigoOperador:Landroid/widget/EditText;

    invoke-virtual {v0, v4}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 298
    iget-object v0, p0, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->campoConta:Landroid/widget/EditText;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setVisibility(I)V

    .line 299
    iget-object v0, p0, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->campoCodigoOperador:Landroid/widget/EditText;

    const/4 v1, 0x0

    const/4 v2, 0x4

    invoke-virtual {v3, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 300
    iget-object v0, p0, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->campoConta:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    .line 301
    iget-object v0, p0, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->campoConta:Landroid/widget/EditText;

    const/4 v1, 0x4

    invoke-virtual {v3, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 302
    iget-object v0, p0, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->campoConta:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->campoConta:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setSelection(I)V

    .line 303
    .end local v4    # "fArray":[Landroid/text/InputFilter;
    goto/16 :goto_1aa

    :cond_c3
    iget-object v0, p0, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->campoConta:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->length()I

    move-result v0

    if-lez v0, :cond_123

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v1, 0xa

    if-ge v0, v1, :cond_123

    .line 304
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->mudarHint:Z

    .line 305
    iget-object v0, p0, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->labelAgencia:Landroid/widget/TextView;

    .line 306
    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 307
    const v2, 0x7f070354

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 306
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 308
    iget-object v0, p0, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->campoCodigoOperador:Landroid/widget/EditText;

    .line 309
    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f070354

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 308
    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 311
    const/4 v0, 0x1

    new-array v4, v0, [Landroid/text/InputFilter;

    .line 312
    .local v4, "fArray":[Landroid/text/InputFilter;
    new-instance v0, Landroid/text/InputFilter$LengthFilter;

    const/16 v1, 0xa

    invoke-direct {v0, v1}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    const/4 v1, 0x0

    aput-object v0, v4, v1

    .line 313
    iget-object v0, p0, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->campoCodigoOperador:Landroid/widget/EditText;

    invoke-virtual {v0, v4}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 314
    iget-object v0, p0, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->campoCodigoOperador:Landroid/widget/EditText;

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 315
    iget-object v0, p0, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->campoConta:Landroid/widget/EditText;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setVisibility(I)V

    .line 316
    iget-object v0, p0, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->campoConta:Landroid/widget/EditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 317
    iget-object v0, p0, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->labelConta:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 318
    .end local v4    # "fArray":[Landroid/text/InputFilter;
    goto/16 :goto_1aa

    :cond_123
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v1, 0xa

    if-ge v0, v1, :cond_1aa

    .line 319
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->mudarHint:Z

    .line 320
    iget-object v0, p0, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->labelAgencia:Landroid/widget/TextView;

    .line 321
    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 322
    const v2, 0x7f070354

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 321
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 323
    iget-object v0, p0, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->campoCodigoOperador:Landroid/widget/EditText;

    .line 324
    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f070354

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 323
    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_1aa

    .line 328
    :cond_14f
    iget-object v0, p0, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->labelAgencia:Landroid/widget/TextView;

    .line 329
    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f070354

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 328
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 330
    iget-object v0, p0, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->campoCodigoOperador:Landroid/widget/EditText;

    .line 331
    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f070354

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 330
    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 332
    iget-object v0, p0, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->labelAgencia:Landroid/widget/TextView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 333
    iget-object v0, p0, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->campoConta:Landroid/widget/EditText;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setVisibility(I)V

    .line 334
    iget-object v0, p0, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->campoConta:Landroid/widget/EditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 335
    iget-object v0, p0, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->labelConta:Landroid/widget/TextView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 336
    iget-object v0, p0, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->labelConta:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 338
    const/4 v0, 0x1

    new-array v4, v0, [Landroid/text/InputFilter;

    .line 339
    .local v4, "fArray":[Landroid/text/InputFilter;
    new-instance v0, Landroid/text/InputFilter$LengthFilter;

    const/16 v1, 0xa

    invoke-direct {v0, v1}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    const/4 v1, 0x0

    aput-object v0, v4, v1

    .line 340
    iget-object v0, p0, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->campoCodigoOperador:Landroid/widget/EditText;

    invoke-virtual {v0, v4}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 342
    iget-object v0, p0, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->listener:Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica$BarraUnicaListener;

    if-eqz v0, :cond_1aa

    .line 343
    iget-object v0, p0, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->listener:Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica$BarraUnicaListener;

    invoke-interface {v0}, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica$BarraUnicaListener;->alterouConteudo()V

    .line 348
    .end local v3    # "conteudo":Ljava/lang/String;
    .end local v4    # "fArray":[Landroid/text/InputFilter;
    :cond_1aa
    :goto_1aa
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->executando:Z

    .line 349
    return-void
.end method

.method alteraConta()V
    .registers 7

    .line 354
    iget-object v0, p0, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->campoConta:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->length()I

    move-result v0

    const/4 v1, 0x6

    if-ge v0, v1, :cond_7d

    iget-object v0, p0, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->campoCodigoOperador:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->length()I

    move-result v0

    if-lez v0, :cond_7d

    .line 355
    iget-object v0, p0, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->labelAgencia:Landroid/widget/TextView;

    .line 356
    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f070354

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 355
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 357
    iget-object v0, p0, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->campoCodigoOperador:Landroid/widget/EditText;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->campoCodigoOperador:Landroid/widget/EditText;

    .line 358
    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->campoConta:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "-"

    const-string v4, ""

    .line 359
    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 357
    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 360
    iget-object v0, p0, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->campoConta:Landroid/widget/EditText;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setVisibility(I)V

    .line 361
    iget-object v0, p0, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->labelConta:Landroid/widget/TextView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 363
    const/4 v0, 0x1

    new-array v5, v0, [Landroid/text/InputFilter;

    .line 364
    .local v5, "fArray":[Landroid/text/InputFilter;
    new-instance v0, Landroid/text/InputFilter$LengthFilter;

    const/16 v1, 0xa

    invoke-direct {v0, v1}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    const/4 v1, 0x0

    aput-object v0, v5, v1

    .line 365
    iget-object v0, p0, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->campoCodigoOperador:Landroid/widget/EditText;

    invoke-virtual {v0, v5}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 366
    iget-object v0, p0, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->campoCodigoOperador:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    .line 367
    iget-object v0, p0, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->campoCodigoOperador:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->campoCodigoOperador:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setSelection(I)V

    .line 370
    .end local v5    # "fArray":[Landroid/text/InputFilter;
    :cond_7d
    return-void
.end method

.method aposCarregar()V
    .registers 4

    .line 76
    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->isInEditMode()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 77
    return-void

    .line 80
    :cond_7
    iget-object v0, p0, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->campoCodigoOperador:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->clearFocus()V

    .line 82
    iget-object v0, p0, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->labelAgencia:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f070354

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 83
    iget-object v0, p0, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->labelAgencia:Landroid/widget/TextView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 84
    iget-object v0, p0, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->labelConta:Landroid/widget/TextView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 85
    iget-object v0, p0, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->campoConta:Landroid/widget/EditText;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setVisibility(I)V

    .line 86
    iget-object v0, p0, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->campoConta:Landroid/widget/EditText;

    const-string v1, "#####-#"

    iget-object v2, p0, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->campoConta:Landroid/widget/EditText;

    .line 87
    invoke-static {v1, v2}, Lcom/itau/empresas/ui/util/MascaraEditText;->insere(Ljava/lang/String;Landroid/widget/EditText;)Landroid/text/TextWatcher;

    move-result-object v1

    .line 86
    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 89
    iget-object v0, p0, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->campoCodigoOperador:Landroid/widget/EditText;

    new-instance v1, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica$1;

    invoke-direct {v1, p0}, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica$1;-><init>(Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 100
    iget-object v0, p0, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->campoConta:Landroid/widget/EditText;

    new-instance v1, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica$2;

    invoke-direct {v1, p0}, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica$2;-><init>(Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 111
    iget-object v0, p0, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->campoCodigoOperador:Landroid/widget/EditText;

    new-instance v1, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica$3;

    invoke-direct {v1, p0}, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica$3;-><init>(Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 126
    iget-object v0, p0, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->campoCodigoOperador:Landroid/widget/EditText;

    new-instance v1, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica$4;

    invoke-direct {v1, p0}, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica$4;-><init>(Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setCustomSelectionActionModeCallback(Landroid/view/ActionMode$Callback;)V

    .line 145
    iget-object v0, p0, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->campoConta:Landroid/widget/EditText;

    new-instance v1, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica$5;

    invoke-direct {v1, p0}, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica$5;-><init>(Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setCustomSelectionActionModeCallback(Landroid/view/ActionMode$Callback;)V

    .line 165
    iget-object v0, p0, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->campoConta:Landroid/widget/EditText;

    new-instance v1, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica$6;

    invoke-direct {v1, p0}, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica$6;-><init>(Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 184
    iget-object v0, p0, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->campoCodigoOperador:Landroid/widget/EditText;

    new-instance v1, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica$7;

    invoke-direct {v1, p0}, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica$7;-><init>(Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 204
    iget-object v0, p0, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->campoConta:Landroid/widget/EditText;

    new-instance v1, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica$8;

    invoke-direct {v1, p0}, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica$8;-><init>(Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 222
    return-void
.end method

.method public clearFocus()V
    .registers 2

    .line 382
    iget-object v0, p0, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->campoConta:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->clearFocus()V

    .line 383
    iget-object v0, p0, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->campoCodigoOperador:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->clearFocus()V

    .line 384
    return-void
.end method

.method public getAgencia()Ljava/lang/String;
    .registers 4

    .line 239
    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->getConteudo()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x4

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getConta()Ljava/lang/String;
    .registers 4

    .line 243
    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->getConteudo()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x4

    const/16 v2, 0x9

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getConteudo()Ljava/lang/String;
    .registers 5

    .line 373
    iget-object v0, p0, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->campoConta:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getVisibility()I

    move-result v0

    if-nez v0, :cond_43

    iget-object v0, p0, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->campoCodigoOperador:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_43

    .line 374
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->campoCodigoOperador:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->campoConta:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "-"

    const-string v3, ""

    .line 375
    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 374
    return-object v0

    .line 377
    :cond_43
    iget-object v0, p0, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->campoCodigoOperador:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getDac()Ljava/lang/String;
    .registers 3

    .line 247
    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->getConteudo()Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0x9

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getOperador()Ljava/lang/String;
    .registers 2

    .line 235
    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->getConteudo()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public isValido()Z
    .registers 2

    .line 256
    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->isValidoOperador()Z

    move-result v0

    if-nez v0, :cond_c

    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->isValidoAgenciaConta()Z

    move-result v0

    if-eqz v0, :cond_e

    :cond_c
    const/4 v0, 0x1

    goto :goto_f

    :cond_e
    const/4 v0, 0x0

    :goto_f
    return v0
.end method

.method public isValidoAgenciaConta()Z
    .registers 3

    .line 264
    iget-object v0, p0, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->campoCodigoOperador:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->length()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_17

    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->getConteudo()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v1, 0xa

    if-ne v0, v1, :cond_17

    const/4 v0, 0x1

    goto :goto_18

    :cond_17
    const/4 v0, 0x0

    :goto_18
    return v0
.end method

.method public isValidoOperador()Z
    .registers 3

    .line 260
    iget-object v0, p0, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->campoCodigoOperador:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->length()I

    move-result v0

    const/16 v1, 0x9

    if-ne v0, v1, :cond_c

    const/4 v0, 0x1

    goto :goto_d

    :cond_c
    const/4 v0, 0x0

    :goto_d
    return v0
.end method

.method public setBarraUnicaListener(Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica$BarraUnicaListener;)V
    .registers 2
    .param p1, "listener"    # Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica$BarraUnicaListener;

    .line 395
    iput-object p1, p0, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->listener:Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica$BarraUnicaListener;

    .line 396
    return-void
.end method

.method public setTexto(Ljava/lang/String;)V
    .registers 3
    .param p1, "preferencia"    # Ljava/lang/String;

    .line 387
    iget-object v0, p0, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->campoCodigoOperador:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 388
    return-void
.end method

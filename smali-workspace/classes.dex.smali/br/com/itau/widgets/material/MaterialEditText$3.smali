.class Lbr/com/itau/widgets/material/MaterialEditText$3;
.super Ljava/lang/Object;
.source "MaterialEditText.java"

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lbr/com/itau/widgets/material/MaterialEditText;->initFloatingLabel()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lbr/com/itau/widgets/material/MaterialEditText;


# direct methods
.method constructor <init>(Lbr/com/itau/widgets/material/MaterialEditText;)V
    .registers 2

    .line 948
    iput-object p1, p0, Lbr/com/itau/widgets/material/MaterialEditText$3;->this$0:Lbr/com/itau/widgets/material/MaterialEditText;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFocusChange(Landroid/view/View;Z)V
    .registers 4
    .param p1, "v"    # Landroid/view/View;
    .param p2, "hasFocus"    # Z

    .line 951
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialEditText$3;->this$0:Lbr/com/itau/widgets/material/MaterialEditText;

    # getter for: Lbr/com/itau/widgets/material/MaterialEditText;->floatingLabelEnabled:Z
    invoke-static {v0}, Lbr/com/itau/widgets/material/MaterialEditText;->access$200(Lbr/com/itau/widgets/material/MaterialEditText;)Z

    move-result v0

    if-eqz v0, :cond_25

    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialEditText$3;->this$0:Lbr/com/itau/widgets/material/MaterialEditText;

    # getter for: Lbr/com/itau/widgets/material/MaterialEditText;->highlightFloatingLabel:Z
    invoke-static {v0}, Lbr/com/itau/widgets/material/MaterialEditText;->access$500(Lbr/com/itau/widgets/material/MaterialEditText;)Z

    move-result v0

    if-eqz v0, :cond_25

    .line 952
    if-eqz p2, :cond_1c

    .line 953
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialEditText$3;->this$0:Lbr/com/itau/widgets/material/MaterialEditText;

    # invokes: Lbr/com/itau/widgets/material/MaterialEditText;->getLabelFocusAnimator()Lcom/nineoldandroids/animation/ObjectAnimator;
    invoke-static {v0}, Lbr/com/itau/widgets/material/MaterialEditText;->access$600(Lbr/com/itau/widgets/material/MaterialEditText;)Lcom/nineoldandroids/animation/ObjectAnimator;

    move-result-object v0

    invoke-virtual {v0}, Lcom/nineoldandroids/animation/ObjectAnimator;->start()V

    goto :goto_25

    .line 955
    :cond_1c
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialEditText$3;->this$0:Lbr/com/itau/widgets/material/MaterialEditText;

    # invokes: Lbr/com/itau/widgets/material/MaterialEditText;->getLabelFocusAnimator()Lcom/nineoldandroids/animation/ObjectAnimator;
    invoke-static {v0}, Lbr/com/itau/widgets/material/MaterialEditText;->access$600(Lbr/com/itau/widgets/material/MaterialEditText;)Lcom/nineoldandroids/animation/ObjectAnimator;

    move-result-object v0

    invoke-virtual {v0}, Lcom/nineoldandroids/animation/ObjectAnimator;->reverse()V

    .line 958
    :cond_25
    :goto_25
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialEditText$3;->this$0:Lbr/com/itau/widgets/material/MaterialEditText;

    # getter for: Lbr/com/itau/widgets/material/MaterialEditText;->validateOnFocusLost:Z
    invoke-static {v0}, Lbr/com/itau/widgets/material/MaterialEditText;->access$700(Lbr/com/itau/widgets/material/MaterialEditText;)Z

    move-result v0

    if-eqz v0, :cond_34

    if-nez p2, :cond_34

    .line 959
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialEditText$3;->this$0:Lbr/com/itau/widgets/material/MaterialEditText;

    invoke-virtual {v0}, Lbr/com/itau/widgets/material/MaterialEditText;->validate()Z

    .line 961
    :cond_34
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialEditText$3;->this$0:Lbr/com/itau/widgets/material/MaterialEditText;

    iget-object v0, v0, Lbr/com/itau/widgets/material/MaterialEditText;->outerFocusChangeListener:Landroid/view/View$OnFocusChangeListener;

    if-eqz v0, :cond_41

    .line 962
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialEditText$3;->this$0:Lbr/com/itau/widgets/material/MaterialEditText;

    iget-object v0, v0, Lbr/com/itau/widgets/material/MaterialEditText;->outerFocusChangeListener:Landroid/view/View$OnFocusChangeListener;

    invoke-interface {v0, p1, p2}, Landroid/view/View$OnFocusChangeListener;->onFocusChange(Landroid/view/View;Z)V

    .line 964
    :cond_41
    return-void
.end method

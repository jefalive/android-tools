.class public Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoPropostaSaidaVO;
.super Ljava/lang/Object;
.source "ProdutosParceladosSimulacaoPropostaSaidaVO.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private agencia:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "agencia"
    .end annotation
.end field

.field private conta:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "conta"
    .end annotation
.end field

.field private dadosSeguro:Lcom/itau/empresas/feature/credito/giro/model/SeguroProdutosParceladoSaidaVO;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "dados_seguro"
    .end annotation
.end field

.field private dataPrimeiroPagamento:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "data_primeiro_pagamento"
    .end annotation
.end field

.field private datasPrimeiroPagamento:Ljava/util/ArrayList;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "datas_primeiro_pagamento"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<Lcom/itau/empresas/api/model/DataPrimeiroPagamentoVO;>;"
        }
    .end annotation
.end field

.field private moeda:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "moeda"
    .end annotation
.end field

.field private parcelasEmprestimoVO:Ljava/util/ArrayList;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "quantidade_parcelas"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<Lcom/itau/empresas/feature/credito/giro/model/ParcelasEmprestimoVO;>;"
        }
    .end annotation
.end field

.field private prazoMaximoContratacao:J
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "prazo_maximo_contratacao"
    .end annotation
.end field

.field private prazoMinimoContratacao:J
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "prazo_minimo_contratacao"
    .end annotation
.end field

.field private quantidadeMaximaDiasPrimeiroPagamento:J
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "quantidade_maxima_dias_primeiro_pagamento"
    .end annotation
.end field

.field private quantidadeMinimaDiasPrimeiroPagamento:J
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "quantidade_minima_dias_primeiro_pagamento"
    .end annotation
.end field

.field private taxaJurosMensal:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "taxa_juros_mensal"
    .end annotation
.end field

.field private valorEmprestimo:J
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "valor_emprestimo"
    .end annotation
.end field

.field private valorMaximoContratacao:F
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "valor_maximo_contratacao"
    .end annotation
.end field

.field private valorMinimoContratacao:F
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "valor_minimo_contratacao"
    .end annotation
.end field

.field private valoresEmprestimos:Ljava/util/ArrayList;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "valores_emprestimos"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<Lcom/itau/empresas/feature/credito/giro/model/ValoresEmprestimoVO;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getDadosSeguro()Lcom/itau/empresas/feature/credito/giro/model/SeguroProdutosParceladoSaidaVO;
    .registers 2

    .line 106
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoPropostaSaidaVO;->dadosSeguro:Lcom/itau/empresas/feature/credito/giro/model/SeguroProdutosParceladoSaidaVO;

    return-object v0
.end method

.method public getDataPrimeiroPagamento()Ljava/lang/String;
    .registers 2

    .line 182
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoPropostaSaidaVO;->dataPrimeiroPagamento:Ljava/lang/String;

    return-object v0
.end method

.method public getDatasPrimeiroPagamento()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Lcom/itau/empresas/api/model/DataPrimeiroPagamentoVO;>;"
        }
    .end annotation

    .line 172
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoPropostaSaidaVO;->datasPrimeiroPagamento:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getParcelasEmprestimoVO()Ljava/util/ArrayList;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/ArrayList<Lcom/itau/empresas/feature/credito/giro/model/ParcelasEmprestimoVO;>;"
        }
    .end annotation

    .line 111
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoPropostaSaidaVO;->parcelasEmprestimoVO:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getPrazoMaximoContratacao()J
    .registers 3

    .line 157
    iget-wide v0, p0, Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoPropostaSaidaVO;->prazoMaximoContratacao:J

    return-wide v0
.end method

.method public getPrazoMinimoContratacao()J
    .registers 3

    .line 153
    iget-wide v0, p0, Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoPropostaSaidaVO;->prazoMinimoContratacao:J

    return-wide v0
.end method

.method public getQuantidadeMaximaDiasPrimeiroPagamento()J
    .registers 3

    .line 92
    iget-wide v0, p0, Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoPropostaSaidaVO;->quantidadeMaximaDiasPrimeiroPagamento:J

    return-wide v0
.end method

.method public getQuantidadeMinimaDiasPrimeiroPagamento()J
    .registers 3

    .line 96
    iget-wide v0, p0, Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoPropostaSaidaVO;->quantidadeMinimaDiasPrimeiroPagamento:J

    return-wide v0
.end method

.method public getValorMaximoContratacao()F
    .registers 2

    .line 88
    iget v0, p0, Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoPropostaSaidaVO;->valorMaximoContratacao:F

    return v0
.end method

.method public getValorMinimoContratacao()F
    .registers 2

    .line 84
    iget v0, p0, Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoPropostaSaidaVO;->valorMinimoContratacao:F

    return v0
.end method

.method public getValoresEmprestimos()Ljava/util/ArrayList;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/ArrayList<Lcom/itau/empresas/feature/credito/giro/model/ValoresEmprestimoVO;>;"
        }
    .end annotation

    .line 101
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoPropostaSaidaVO;->valoresEmprestimos:Ljava/util/ArrayList;

    return-object v0
.end method

.method public setDataPrimeiroPagamento(Ljava/lang/String;)V
    .registers 2
    .param p1, "dataPrimeiroPagamento"    # Ljava/lang/String;

    .line 177
    iput-object p1, p0, Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoPropostaSaidaVO;->dataPrimeiroPagamento:Ljava/lang/String;

    .line 178
    return-void
.end method

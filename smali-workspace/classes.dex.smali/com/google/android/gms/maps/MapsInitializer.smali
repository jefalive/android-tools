.class public final Lcom/google/android/gms/maps/MapsInitializer;
.super Ljava/lang/Object;


# static fields
.field private static zznY:Z


# direct methods
.method static constructor <clinit>()V
    .registers 1

    const/4 v0, 0x0

    sput-boolean v0, Lcom/google/android/gms/maps/MapsInitializer;->zznY:Z

    return-void
.end method

.method public static declared-synchronized initialize(Landroid/content/Context;)I
    .registers 5
    .param p0, "context"    # Landroid/content/Context;

    const-class v3, Lcom/google/android/gms/maps/MapsInitializer;

    monitor-enter v3

    const-string v0, "Context is null"

    :try_start_5
    invoke-static {p0, v0}, Lcom/google/android/gms/common/internal/zzx;->zzb(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-boolean v0, Lcom/google/android/gms/maps/MapsInitializer;->zznY:Z
    :try_end_a
    .catchall {:try_start_5 .. :try_end_a} :catchall_22

    if-eqz v0, :cond_f

    monitor-exit v3

    const/4 v0, 0x0

    return v0

    :cond_f
    :try_start_f
    invoke-static {p0}, Lcom/google/android/gms/maps/internal/zzad;->zzaO(Landroid/content/Context;)Lcom/google/android/gms/maps/internal/zzc;
    :try_end_12
    .catch Lcom/google/android/gms/common/GooglePlayServicesNotAvailableException; {:try_start_f .. :try_end_12} :catch_14
    .catchall {:try_start_f .. :try_end_12} :catchall_22

    move-result-object v1

    goto :goto_19

    :catch_14
    move-exception v2

    :try_start_15
    iget v0, v2, Lcom/google/android/gms/common/GooglePlayServicesNotAvailableException;->errorCode:I

    monitor-exit v3

    return v0

    :goto_19
    invoke-static {v1}, Lcom/google/android/gms/maps/MapsInitializer;->zza(Lcom/google/android/gms/maps/internal/zzc;)V

    const/4 v0, 0x1

    sput-boolean v0, Lcom/google/android/gms/maps/MapsInitializer;->zznY:Z
    :try_end_1f
    .catchall {:try_start_15 .. :try_end_1f} :catchall_22

    monitor-exit v3

    const/4 v0, 0x0

    return v0

    :catchall_22
    move-exception p0

    monitor-exit v3

    throw p0
.end method

.method public static zza(Lcom/google/android/gms/maps/internal/zzc;)V
    .registers 3

    :try_start_0
    invoke-interface {p0}, Lcom/google/android/gms/maps/internal/zzc;->zzAe()Lcom/google/android/gms/maps/internal/ICameraUpdateFactoryDelegate;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/maps/CameraUpdateFactory;->zza(Lcom/google/android/gms/maps/internal/ICameraUpdateFactoryDelegate;)V

    invoke-interface {p0}, Lcom/google/android/gms/maps/internal/zzc;->zzAf()Lcom/google/android/gms/maps/model/internal/zza;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/maps/model/BitmapDescriptorFactory;->zza(Lcom/google/android/gms/maps/model/internal/zza;)V
    :try_end_e
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_e} :catch_f

    goto :goto_16

    :catch_f
    move-exception v1

    new-instance v0, Lcom/google/android/gms/maps/model/RuntimeRemoteException;

    invoke-direct {v0, v1}, Lcom/google/android/gms/maps/model/RuntimeRemoteException;-><init>(Landroid/os/RemoteException;)V

    throw v0

    :goto_16
    return-void
.end method

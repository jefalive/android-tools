.class public Lcom/itau/empresas/feature/credito/lis/model/LisContratacaoRespostaVO;
.super Ljava/lang/Object;
.source "LisContratacaoRespostaVO.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private final agencia:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "agencia"
    .end annotation
.end field

.field private final codigoProduto:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "codigo_produto"
    .end annotation
.end field

.field private final codigoTipoComprovante:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "codigo_tipo_comprovante"
    .end annotation
.end field

.field private final codigoVersaoComprovante:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "codigo_versao_comprovante"
    .end annotation
.end field

.field private final conta:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "conta"
    .end annotation
.end field

.field private final dataHoraContratacao:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "data_hora_contratacao"
    .end annotation
.end field

.field private final dataVencimentoOperacao:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "data_vencimento_operacao"
    .end annotation
.end field

.field private final descricaoProduto:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "descricao_produto"
    .end annotation
.end field

.field private final digitoConta:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "digito_conta"
    .end annotation
.end field

.field private final moeda:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "moeda"
    .end annotation
.end field

.field private final valorLimite:Ljava/math/BigDecimal;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "valor_limite"
    .end annotation
.end field


# virtual methods
.method public getCodigoProduto()Ljava/lang/String;
    .registers 2

    .line 78
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/model/LisContratacaoRespostaVO;->codigoProduto:Ljava/lang/String;

    return-object v0
.end method

.method public getCodigoTipoComprovante()Ljava/lang/String;
    .registers 2

    .line 98
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/model/LisContratacaoRespostaVO;->codigoTipoComprovante:Ljava/lang/String;

    return-object v0
.end method

.method public getCodigoVersaoComprovante()Ljava/lang/String;
    .registers 2

    .line 102
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/model/LisContratacaoRespostaVO;->codigoVersaoComprovante:Ljava/lang/String;

    return-object v0
.end method

.method public getDataHoraContratacao()Ljava/lang/String;
    .registers 2

    .line 94
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/model/LisContratacaoRespostaVO;->dataHoraContratacao:Ljava/lang/String;

    return-object v0
.end method

.method public getDataVencimentoOperacao()Ljava/lang/String;
    .registers 2

    .line 90
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/model/LisContratacaoRespostaVO;->dataVencimentoOperacao:Ljava/lang/String;

    return-object v0
.end method

.method public getDescricaoProduto()Ljava/lang/String;
    .registers 2

    .line 82
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/model/LisContratacaoRespostaVO;->descricaoProduto:Ljava/lang/String;

    return-object v0
.end method

.method public getValorLimite()Ljava/math/BigDecimal;
    .registers 2

    .line 86
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/model/LisContratacaoRespostaVO;->valorLimite:Ljava/math/BigDecimal;

    return-object v0
.end method

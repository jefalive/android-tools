.class public Lcom/itau/empresas/feature/extrato/util/ExtratoUtil;
.super Ljava/lang/Object;
.source "ExtratoUtil.java"


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static formatarDataPorExtenso(Ljava/lang/String;)Ljava/lang/String;
    .registers 9
    .param p0, "dataLancamento"    # Ljava/lang/String;

    .line 27
    const-string v4, "%d %s %s"

    .line 29
    .local v4, "formato":Ljava/lang/String;
    invoke-static {p0}, Lcom/itau/empresas/ui/util/DataEHoraUtils;->getDia(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v5

    .line 30
    .local v5, "dia":I
    invoke-static {p0}, Lcom/itau/empresas/ui/util/DataEHoraUtils;->getMesPorExtenso(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 31
    .local v6, "mesExtenso":Ljava/lang/String;
    invoke-static {p0}, Lcom/itau/empresas/ui/util/DataEHoraUtils;->getAno(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v7

    .line 33
    .local v7, "ano":I
    const-string v0, "%d %s %s"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const/4 v2, 0x0

    const/4 v3, 0x3

    invoke-virtual {v6, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    aput-object v2, v1, v3

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x2

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getCorItens(Landroid/content/Context;DZ)I
    .registers 8
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "valor"    # D
    .param p3, "saldoDia"    # Z

    .line 75
    const-wide/16 v0, 0x0

    cmpg-double v0, p1, v0

    if-gez v0, :cond_8

    const/4 v2, 0x1

    goto :goto_9

    :cond_8
    const/4 v2, 0x0

    .line 78
    .local v2, "negativo":Z
    :goto_9
    if-eqz p3, :cond_15

    if-eqz v2, :cond_15

    .line 79
    const v0, 0x7f0c0151

    invoke-static {p0, v0}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v3

    .local v3, "cor":I
    goto :goto_26

    .line 80
    .end local v3    # "cor":I
    :cond_15
    if-eqz v2, :cond_1f

    .line 81
    const v0, 0x7f0c0149

    invoke-static {p0, v0}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v3

    .local v3, "cor":I
    goto :goto_26

    .line 83
    .end local v3    # "cor":I
    :cond_1f
    const v0, 0x7f0c014c

    invoke-static {p0, v0}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v3

    .line 86
    .local v3, "cor":I
    :goto_26
    return v3
.end method

.method public static obterOrdenacao(Lcom/itau/empresas/CustomApplication;)I
    .registers 4
    .param p0, "application"    # Lcom/itau/empresas/CustomApplication;

    .line 70
    invoke-virtual {p0}, Lcom/itau/empresas/CustomApplication;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 71
    invoke-virtual {p0}, Lcom/itau/empresas/CustomApplication;->getCodigoOperador()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_ORDEMEXTRATO"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 70
    return v0
.end method

.method public static ordenar(Ljava/util/List;I)V
    .registers 3
    .param p0, "listaExtrato"    # Ljava/util/List;
    .param p1, "ordem"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<Lcom/itau/empresas/api/model/LancamentoVO;>;I)V"
        }
    .end annotation

    .line 55
    new-instance v0, Lcom/itau/empresas/feature/extrato/util/ExtratoUtil$1;

    invoke-direct {v0, p1}, Lcom/itau/empresas/feature/extrato/util/ExtratoUtil$1;-><init>(I)V

    invoke-static {p0, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 67
    return-void
.end method

.class public final Lcom/itau/empresas/ui/view/ItemAgenciaMapaView_;
.super Lcom/itau/empresas/ui/view/ItemAgenciaMapaView;
.source "ItemAgenciaMapaView_.java"

# interfaces
.implements Lorg/androidannotations/api/view/HasViews;
.implements Lorg/androidannotations/api/view/OnViewChangedListener;


# instance fields
.field private alreadyInflated_:Z

.field private final onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .param p1, "context"    # Landroid/content/Context;

    .line 36
    invoke-direct {p0, p1}, Lcom/itau/empresas/ui/view/ItemAgenciaMapaView;-><init>(Landroid/content/Context;)V

    .line 32
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/itau/empresas/ui/view/ItemAgenciaMapaView_;->alreadyInflated_:Z

    .line 33
    new-instance v0, Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-direct {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/ui/view/ItemAgenciaMapaView_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 37
    invoke-direct {p0}, Lcom/itau/empresas/ui/view/ItemAgenciaMapaView_;->init_()V

    .line 38
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .line 41
    invoke-direct {p0, p1, p2}, Lcom/itau/empresas/ui/view/ItemAgenciaMapaView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 32
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/itau/empresas/ui/view/ItemAgenciaMapaView_;->alreadyInflated_:Z

    .line 33
    new-instance v0, Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-direct {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/ui/view/ItemAgenciaMapaView_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 42
    invoke-direct {p0}, Lcom/itau/empresas/ui/view/ItemAgenciaMapaView_;->init_()V

    .line 43
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .line 46
    invoke-direct {p0, p1, p2, p3}, Lcom/itau/empresas/ui/view/ItemAgenciaMapaView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 32
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/itau/empresas/ui/view/ItemAgenciaMapaView_;->alreadyInflated_:Z

    .line 33
    new-instance v0, Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-direct {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/ui/view/ItemAgenciaMapaView_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 47
    invoke-direct {p0}, Lcom/itau/empresas/ui/view/ItemAgenciaMapaView_;->init_()V

    .line 48
    return-void
.end method

.method public static build(Landroid/content/Context;)Lcom/itau/empresas/ui/view/ItemAgenciaMapaView;
    .registers 2
    .param p0, "context"    # Landroid/content/Context;

    .line 56
    new-instance v0, Lcom/itau/empresas/ui/view/ItemAgenciaMapaView_;

    invoke-direct {v0, p0}, Lcom/itau/empresas/ui/view/ItemAgenciaMapaView_;-><init>(Landroid/content/Context;)V

    .line 57
    .local v0, "instance":Lcom/itau/empresas/ui/view/ItemAgenciaMapaView_;
    invoke-virtual {v0}, Lcom/itau/empresas/ui/view/ItemAgenciaMapaView_;->onFinishInflate()V

    .line 58
    return-object v0
.end method

.method private init_()V
    .registers 3

    .line 78
    iget-object v0, p0, Lcom/itau/empresas/ui/view/ItemAgenciaMapaView_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-static {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    move-result-object v1

    .line 79
    .local v1, "previousNotifier":Lorg/androidannotations/api/view/OnViewChangedNotifier;
    invoke-static {p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->registerOnViewChangedListener(Lorg/androidannotations/api/view/OnViewChangedListener;)V

    .line 80
    invoke-static {v1}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 81
    return-void
.end method


# virtual methods
.method public onFinishInflate()V
    .registers 3

    .line 69
    iget-boolean v0, p0, Lcom/itau/empresas/ui/view/ItemAgenciaMapaView_;->alreadyInflated_:Z

    if-nez v0, :cond_16

    .line 70
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/itau/empresas/ui/view/ItemAgenciaMapaView_;->alreadyInflated_:Z

    .line 71
    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/ItemAgenciaMapaView_;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0300f1

    invoke-static {v0, v1, p0}, Lcom/itau/empresas/ui/view/ItemAgenciaMapaView_;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 72
    iget-object v0, p0, Lcom/itau/empresas/ui/view/ItemAgenciaMapaView_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 74
    :cond_16
    invoke-super {p0}, Lcom/itau/empresas/ui/view/ItemAgenciaMapaView;->onFinishInflate()V

    .line 75
    return-void
.end method

.method public onViewChanged(Lorg/androidannotations/api/view/HasViews;)V
    .registers 3
    .param p1, "hasViews"    # Lorg/androidannotations/api/view/HasViews;

    .line 103
    const v0, 0x7f0e0441

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/ui/view/ItemAgenciaMapaView_;->textoNomeAgencia:Landroid/widget/TextView;

    .line 104
    const v0, 0x7f0e0442

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/ui/view/ItemAgenciaMapaView_;->textoEnderecoAgencia:Landroid/widget/TextView;

    .line 105
    const v0, 0x7f0e0443

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/ui/view/ItemAgenciaMapaView_;->textoBairroAgencia:Landroid/widget/TextView;

    .line 106
    const v0, 0x7f0e0444

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/ui/view/ItemAgenciaMapaView_;->textoTelefone:Landroid/widget/TextView;

    .line 107
    const v0, 0x7f0e0445

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/ui/view/ItemAgenciaMapaView_;->textoHorarioAtendimento:Landroid/widget/TextView;

    .line 108
    const v0, 0x7f0e0446

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/ui/view/ItemAgenciaMapaView_;->textoServicos:Landroid/widget/TextView;

    .line 109
    const v0, 0x7f0e044a

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/ui/view/ItemAgenciaMapaView_;->textoAtendimentoEmpresas:Landroid/widget/TextView;

    .line 110
    const v0, 0x7f0e0448

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/itau/empresas/ui/view/ItemAgenciaMapaView_;->imgCaixaExclusivo:Landroid/widget/ImageView;

    .line 111
    const v0, 0x7f0e0449

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/itau/empresas/ui/view/ItemAgenciaMapaView_;->imgHorarioDiferente:Landroid/widget/ImageView;

    .line 112
    const v0, 0x7f0e0522

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/itau/empresas/ui/view/ItemAgenciaMapaView_;->llDetalhesAgencia:Landroid/widget/LinearLayout;

    .line 113
    const v0, 0x7f0e0521

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/itau/empresas/ui/view/ItemAgenciaMapaView_;->llItemAgenciaMapa:Landroid/widget/LinearLayout;

    .line 114
    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/ItemAgenciaMapaView_;->carregaView()V

    .line 115
    return-void
.end method

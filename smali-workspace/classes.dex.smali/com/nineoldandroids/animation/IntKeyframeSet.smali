.class Lcom/nineoldandroids/animation/IntKeyframeSet;
.super Lcom/nineoldandroids/animation/KeyframeSet;
.source "IntKeyframeSet.java"


# instance fields
.field private deltaValue:I

.field private firstTime:Z

.field private firstValue:I

.field private lastValue:I


# direct methods
.method public varargs constructor <init>([Lcom/nineoldandroids/animation/Keyframe$IntKeyframe;)V
    .registers 3
    .param p1, "keyframes"    # [Lcom/nineoldandroids/animation/Keyframe$IntKeyframe;

    .line 42
    invoke-direct {p0, p1}, Lcom/nineoldandroids/animation/KeyframeSet;-><init>([Lcom/nineoldandroids/animation/Keyframe;)V

    .line 39
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/nineoldandroids/animation/IntKeyframeSet;->firstTime:Z

    .line 43
    return-void
.end method


# virtual methods
.method public clone()Lcom/nineoldandroids/animation/IntKeyframeSet;
    .registers 6

    .line 52
    iget-object v1, p0, Lcom/nineoldandroids/animation/IntKeyframeSet;->mKeyframes:Ljava/util/ArrayList;

    .line 53
    .local v1, "keyframes":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/nineoldandroids/animation/Keyframe;>;"
    iget-object v0, p0, Lcom/nineoldandroids/animation/IntKeyframeSet;->mKeyframes:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 54
    .local v2, "numKeyframes":I
    new-array v3, v2, [Lcom/nineoldandroids/animation/Keyframe$IntKeyframe;

    .line 55
    .local v3, "newKeyframes":[Lcom/nineoldandroids/animation/Keyframe$IntKeyframe;
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_b
    if-ge v4, v2, :cond_1e

    .line 56
    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/nineoldandroids/animation/Keyframe;

    invoke-virtual {v0}, Lcom/nineoldandroids/animation/Keyframe;->clone()Lcom/nineoldandroids/animation/Keyframe;

    move-result-object v0

    check-cast v0, Lcom/nineoldandroids/animation/Keyframe$IntKeyframe;

    aput-object v0, v3, v4

    .line 55
    add-int/lit8 v4, v4, 0x1

    goto :goto_b

    .line 58
    .end local v4    # "i":I
    :cond_1e
    new-instance v4, Lcom/nineoldandroids/animation/IntKeyframeSet;

    invoke-direct {v4, v3}, Lcom/nineoldandroids/animation/IntKeyframeSet;-><init>([Lcom/nineoldandroids/animation/Keyframe$IntKeyframe;)V

    .line 59
    .local v4, "newSet":Lcom/nineoldandroids/animation/IntKeyframeSet;
    return-object v4
.end method

.method public bridge synthetic clone()Lcom/nineoldandroids/animation/KeyframeSet;
    .registers 2

    .line 35
    invoke-virtual {p0}, Lcom/nineoldandroids/animation/IntKeyframeSet;->clone()Lcom/nineoldandroids/animation/IntKeyframeSet;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .line 35
    invoke-virtual {p0}, Lcom/nineoldandroids/animation/IntKeyframeSet;->clone()Lcom/nineoldandroids/animation/IntKeyframeSet;

    move-result-object v0

    return-object v0
.end method

.method public getIntValue(F)I
    .registers 13
    .param p1, "fraction"    # F

    .line 63
    iget v0, p0, Lcom/nineoldandroids/animation/IntKeyframeSet;->mNumKeyframes:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_61

    .line 64
    iget-boolean v0, p0, Lcom/nineoldandroids/animation/IntKeyframeSet;->firstTime:Z

    if-eqz v0, :cond_31

    .line 65
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/nineoldandroids/animation/IntKeyframeSet;->firstTime:Z

    .line 66
    iget-object v0, p0, Lcom/nineoldandroids/animation/IntKeyframeSet;->mKeyframes:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/nineoldandroids/animation/Keyframe$IntKeyframe;

    invoke-virtual {v0}, Lcom/nineoldandroids/animation/Keyframe$IntKeyframe;->getIntValue()I

    move-result v0

    iput v0, p0, Lcom/nineoldandroids/animation/IntKeyframeSet;->firstValue:I

    .line 67
    iget-object v0, p0, Lcom/nineoldandroids/animation/IntKeyframeSet;->mKeyframes:Ljava/util/ArrayList;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/nineoldandroids/animation/Keyframe$IntKeyframe;

    invoke-virtual {v0}, Lcom/nineoldandroids/animation/Keyframe$IntKeyframe;->getIntValue()I

    move-result v0

    iput v0, p0, Lcom/nineoldandroids/animation/IntKeyframeSet;->lastValue:I

    .line 68
    iget v0, p0, Lcom/nineoldandroids/animation/IntKeyframeSet;->lastValue:I

    iget v1, p0, Lcom/nineoldandroids/animation/IntKeyframeSet;->firstValue:I

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/nineoldandroids/animation/IntKeyframeSet;->deltaValue:I

    .line 70
    :cond_31
    iget-object v0, p0, Lcom/nineoldandroids/animation/IntKeyframeSet;->mInterpolator:Landroid/view/animation/Interpolator;

    if-eqz v0, :cond_3b

    .line 71
    iget-object v0, p0, Lcom/nineoldandroids/animation/IntKeyframeSet;->mInterpolator:Landroid/view/animation/Interpolator;

    invoke-interface {v0, p1}, Landroid/view/animation/Interpolator;->getInterpolation(F)F

    move-result p1

    .line 73
    :cond_3b
    iget-object v0, p0, Lcom/nineoldandroids/animation/IntKeyframeSet;->mEvaluator:Lcom/nineoldandroids/animation/TypeEvaluator;

    if-nez v0, :cond_48

    .line 74
    iget v0, p0, Lcom/nineoldandroids/animation/IntKeyframeSet;->firstValue:I

    iget v1, p0, Lcom/nineoldandroids/animation/IntKeyframeSet;->deltaValue:I

    int-to-float v1, v1

    mul-float/2addr v1, p1

    float-to-int v1, v1

    add-int/2addr v0, v1

    return v0

    .line 76
    :cond_48
    iget-object v0, p0, Lcom/nineoldandroids/animation/IntKeyframeSet;->mEvaluator:Lcom/nineoldandroids/animation/TypeEvaluator;

    iget v1, p0, Lcom/nineoldandroids/animation/IntKeyframeSet;->firstValue:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iget v2, p0, Lcom/nineoldandroids/animation/IntKeyframeSet;->lastValue:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, p1, v1, v2}, Lcom/nineoldandroids/animation/TypeEvaluator;->evaluate(FLjava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    move-result v0

    return v0

    .line 79
    :cond_61
    const/4 v0, 0x0

    cmpg-float v0, p1, v0

    if-gtz v0, :cond_ba

    .line 80
    iget-object v0, p0, Lcom/nineoldandroids/animation/IntKeyframeSet;->mKeyframes:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/nineoldandroids/animation/Keyframe$IntKeyframe;

    .line 81
    .local v3, "prevKeyframe":Lcom/nineoldandroids/animation/Keyframe$IntKeyframe;
    iget-object v0, p0, Lcom/nineoldandroids/animation/IntKeyframeSet;->mKeyframes:Ljava/util/ArrayList;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/nineoldandroids/animation/Keyframe$IntKeyframe;

    .line 82
    .local v4, "nextKeyframe":Lcom/nineoldandroids/animation/Keyframe$IntKeyframe;
    invoke-virtual {v3}, Lcom/nineoldandroids/animation/Keyframe$IntKeyframe;->getIntValue()I

    move-result v5

    .line 83
    .local v5, "prevValue":I
    invoke-virtual {v4}, Lcom/nineoldandroids/animation/Keyframe$IntKeyframe;->getIntValue()I

    move-result v6

    .line 84
    .local v6, "nextValue":I
    invoke-virtual {v3}, Lcom/nineoldandroids/animation/Keyframe$IntKeyframe;->getFraction()F

    move-result v7

    .line 85
    .local v7, "prevFraction":F
    invoke-virtual {v4}, Lcom/nineoldandroids/animation/Keyframe$IntKeyframe;->getFraction()F

    move-result v8

    .line 86
    .local v8, "nextFraction":F
    invoke-virtual {v4}, Lcom/nineoldandroids/animation/Keyframe$IntKeyframe;->getInterpolator()Landroid/view/animation/Interpolator;

    move-result-object v9

    .line 87
    .local v9, "interpolator":Landroid/view/animation/Interpolator;
    if-eqz v9, :cond_94

    .line 88
    invoke-interface {v9, p1}, Landroid/view/animation/Interpolator;->getInterpolation(F)F

    move-result p1

    .line 90
    :cond_94
    sub-float v0, p1, v7

    sub-float v1, v8, v7

    div-float v10, v0, v1

    .line 91
    .local v10, "intervalFraction":F
    iget-object v0, p0, Lcom/nineoldandroids/animation/IntKeyframeSet;->mEvaluator:Lcom/nineoldandroids/animation/TypeEvaluator;

    if-nez v0, :cond_a5

    sub-int v0, v6, v5

    int-to-float v0, v0

    mul-float/2addr v0, v10

    float-to-int v0, v0

    add-int/2addr v0, v5

    goto :goto_b9

    :cond_a5
    iget-object v0, p0, Lcom/nineoldandroids/animation/IntKeyframeSet;->mEvaluator:Lcom/nineoldandroids/animation/TypeEvaluator;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v10, v1, v2}, Lcom/nineoldandroids/animation/TypeEvaluator;->evaluate(FLjava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    move-result v0

    :goto_b9
    return v0

    .line 95
    .end local v3    # "prevKeyframe":Lcom/nineoldandroids/animation/Keyframe$IntKeyframe;
    .end local v4    # "nextKeyframe":Lcom/nineoldandroids/animation/Keyframe$IntKeyframe;
    .end local v5    # "prevValue":I
    .end local v6    # "nextValue":I
    .end local v7    # "prevFraction":F
    .end local v8    # "nextFraction":F
    .end local v9    # "interpolator":Landroid/view/animation/Interpolator;
    .end local v10    # "intervalFraction":F
    :cond_ba
    const/high16 v0, 0x3f800000    # 1.0f

    cmpl-float v0, p1, v0

    if-ltz v0, :cond_11a

    .line 96
    iget-object v0, p0, Lcom/nineoldandroids/animation/IntKeyframeSet;->mKeyframes:Ljava/util/ArrayList;

    iget v1, p0, Lcom/nineoldandroids/animation/IntKeyframeSet;->mNumKeyframes:I

    add-int/lit8 v1, v1, -0x2

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/nineoldandroids/animation/Keyframe$IntKeyframe;

    .line 97
    .local v3, "prevKeyframe":Lcom/nineoldandroids/animation/Keyframe$IntKeyframe;
    iget-object v0, p0, Lcom/nineoldandroids/animation/IntKeyframeSet;->mKeyframes:Ljava/util/ArrayList;

    iget v1, p0, Lcom/nineoldandroids/animation/IntKeyframeSet;->mNumKeyframes:I

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/nineoldandroids/animation/Keyframe$IntKeyframe;

    .line 98
    .local v4, "nextKeyframe":Lcom/nineoldandroids/animation/Keyframe$IntKeyframe;
    invoke-virtual {v3}, Lcom/nineoldandroids/animation/Keyframe$IntKeyframe;->getIntValue()I

    move-result v5

    .line 99
    .local v5, "prevValue":I
    invoke-virtual {v4}, Lcom/nineoldandroids/animation/Keyframe$IntKeyframe;->getIntValue()I

    move-result v6

    .line 100
    .local v6, "nextValue":I
    invoke-virtual {v3}, Lcom/nineoldandroids/animation/Keyframe$IntKeyframe;->getFraction()F

    move-result v7

    .line 101
    .local v7, "prevFraction":F
    invoke-virtual {v4}, Lcom/nineoldandroids/animation/Keyframe$IntKeyframe;->getFraction()F

    move-result v8

    .line 102
    .local v8, "nextFraction":F
    invoke-virtual {v4}, Lcom/nineoldandroids/animation/Keyframe$IntKeyframe;->getInterpolator()Landroid/view/animation/Interpolator;

    move-result-object v9

    .line 103
    .local v9, "interpolator":Landroid/view/animation/Interpolator;
    if-eqz v9, :cond_f4

    .line 104
    invoke-interface {v9, p1}, Landroid/view/animation/Interpolator;->getInterpolation(F)F

    move-result p1

    .line 106
    :cond_f4
    sub-float v0, p1, v7

    sub-float v1, v8, v7

    div-float v10, v0, v1

    .line 107
    .local v10, "intervalFraction":F
    iget-object v0, p0, Lcom/nineoldandroids/animation/IntKeyframeSet;->mEvaluator:Lcom/nineoldandroids/animation/TypeEvaluator;

    if-nez v0, :cond_105

    sub-int v0, v6, v5

    int-to-float v0, v0

    mul-float/2addr v0, v10

    float-to-int v0, v0

    add-int/2addr v0, v5

    goto :goto_119

    :cond_105
    iget-object v0, p0, Lcom/nineoldandroids/animation/IntKeyframeSet;->mEvaluator:Lcom/nineoldandroids/animation/TypeEvaluator;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v10, v1, v2}, Lcom/nineoldandroids/animation/TypeEvaluator;->evaluate(FLjava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    move-result v0

    :goto_119
    return v0

    .line 111
    .end local v3    # "prevKeyframe":Lcom/nineoldandroids/animation/Keyframe$IntKeyframe;
    .end local v4    # "nextKeyframe":Lcom/nineoldandroids/animation/Keyframe$IntKeyframe;
    .end local v5    # "prevValue":I
    .end local v6    # "nextValue":I
    .end local v7    # "prevFraction":F
    .end local v8    # "nextFraction":F
    .end local v9    # "interpolator":Landroid/view/animation/Interpolator;
    .end local v10    # "intervalFraction":F
    :cond_11a
    iget-object v0, p0, Lcom/nineoldandroids/animation/IntKeyframeSet;->mKeyframes:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/nineoldandroids/animation/Keyframe$IntKeyframe;

    .line 112
    .local v3, "prevKeyframe":Lcom/nineoldandroids/animation/Keyframe$IntKeyframe;
    const/4 v4, 0x1

    .local v4, "i":I
    :goto_125
    iget v0, p0, Lcom/nineoldandroids/animation/IntKeyframeSet;->mNumKeyframes:I

    if-ge v4, v0, :cond_182

    .line 113
    iget-object v0, p0, Lcom/nineoldandroids/animation/IntKeyframeSet;->mKeyframes:Ljava/util/ArrayList;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/nineoldandroids/animation/Keyframe$IntKeyframe;

    .line 114
    .local v5, "nextKeyframe":Lcom/nineoldandroids/animation/Keyframe$IntKeyframe;
    invoke-virtual {v5}, Lcom/nineoldandroids/animation/Keyframe$IntKeyframe;->getFraction()F

    move-result v0

    cmpg-float v0, p1, v0

    if-gez v0, :cond_17d

    .line 115
    invoke-virtual {v5}, Lcom/nineoldandroids/animation/Keyframe$IntKeyframe;->getInterpolator()Landroid/view/animation/Interpolator;

    move-result-object v6

    .line 116
    .local v6, "interpolator":Landroid/view/animation/Interpolator;
    if-eqz v6, :cond_144

    .line 117
    invoke-interface {v6, p1}, Landroid/view/animation/Interpolator;->getInterpolation(F)F

    move-result p1

    .line 119
    :cond_144
    invoke-virtual {v3}, Lcom/nineoldandroids/animation/Keyframe$IntKeyframe;->getFraction()F

    move-result v0

    sub-float v0, p1, v0

    invoke-virtual {v5}, Lcom/nineoldandroids/animation/Keyframe$IntKeyframe;->getFraction()F

    move-result v1

    invoke-virtual {v3}, Lcom/nineoldandroids/animation/Keyframe$IntKeyframe;->getFraction()F

    move-result v2

    sub-float/2addr v1, v2

    div-float v7, v0, v1

    .line 121
    .local v7, "intervalFraction":F
    invoke-virtual {v3}, Lcom/nineoldandroids/animation/Keyframe$IntKeyframe;->getIntValue()I

    move-result v8

    .line 122
    .local v8, "prevValue":I
    invoke-virtual {v5}, Lcom/nineoldandroids/animation/Keyframe$IntKeyframe;->getIntValue()I

    move-result v9

    .line 123
    .local v9, "nextValue":I
    iget-object v0, p0, Lcom/nineoldandroids/animation/IntKeyframeSet;->mEvaluator:Lcom/nineoldandroids/animation/TypeEvaluator;

    if-nez v0, :cond_168

    sub-int v0, v9, v8

    int-to-float v0, v0

    mul-float/2addr v0, v7

    float-to-int v0, v0

    add-int/2addr v0, v8

    goto :goto_17c

    :cond_168
    iget-object v0, p0, Lcom/nineoldandroids/animation/IntKeyframeSet;->mEvaluator:Lcom/nineoldandroids/animation/TypeEvaluator;

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v7, v1, v2}, Lcom/nineoldandroids/animation/TypeEvaluator;->evaluate(FLjava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    move-result v0

    :goto_17c
    return v0

    .line 128
    .end local v6    # "interpolator":Landroid/view/animation/Interpolator;
    .end local v7    # "intervalFraction":F
    .end local v8    # "prevValue":I
    .end local v9    # "nextValue":I
    :cond_17d
    move-object v3, v5

    .line 112
    .end local v5    # "nextKeyframe":Lcom/nineoldandroids/animation/Keyframe$IntKeyframe;
    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_125

    .line 131
    .end local v4    # "i":I
    :cond_182
    iget-object v0, p0, Lcom/nineoldandroids/animation/IntKeyframeSet;->mKeyframes:Ljava/util/ArrayList;

    iget v1, p0, Lcom/nineoldandroids/animation/IntKeyframeSet;->mNumKeyframes:I

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/nineoldandroids/animation/Keyframe;

    invoke-virtual {v0}, Lcom/nineoldandroids/animation/Keyframe;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    move-result v0

    return v0
.end method

.method public getValue(F)Ljava/lang/Object;
    .registers 3
    .param p1, "fraction"    # F

    .line 47
    invoke-virtual {p0, p1}, Lcom/nineoldandroids/animation/IntKeyframeSet;->getIntValue(F)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

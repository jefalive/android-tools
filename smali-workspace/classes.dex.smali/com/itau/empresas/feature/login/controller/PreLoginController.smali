.class public Lcom/itau/empresas/feature/login/controller/PreLoginController;
.super Lcom/itau/empresas/controller/BaseController;
.source "PreLoginController.java"

# interfaces
.implements Lbr/com/itau/sdk/android/core/endpoint/ErrorHandler;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/itau/empresas/controller/BaseController<Lcom/itau/empresas/feature/login/PreLoginAPI;>;Lbr/com/itau/sdk/android/core/endpoint/ErrorHandler;"
    }
.end annotation


# static fields
.field private static controller:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<Lcom/itau/empresas/feature/login/controller/PreLoginController;>;"
        }
    .end annotation
.end field


# instance fields
.field private api:Lcom/itau/empresas/feature/login/PreLoginAPI;

.field private listener:Lcom/itau/empresas/feature/login/strategy/PreLoginControllerListener;


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 22
    invoke-direct {p0}, Lcom/itau/empresas/controller/BaseController;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/itau/empresas/feature/login/controller/PreLoginController;)Lcom/itau/empresas/feature/login/strategy/PreLoginControllerListener;
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/feature/login/controller/PreLoginController;

    .line 22
    iget-object v0, p0, Lcom/itau/empresas/feature/login/controller/PreLoginController;->listener:Lcom/itau/empresas/feature/login/strategy/PreLoginControllerListener;

    return-object v0
.end method

.method static synthetic access$100()Z
    .registers 1

    .line 22
    invoke-static {}, Lcom/itau/empresas/feature/login/controller/PreLoginController;->enviaVerificacaoParaChaveDeAcesso()Z

    move-result v0

    return v0
.end method

.method private static enviaVerificacaoParaChaveDeAcesso()Z
    .registers 1

    .line 66
    const/4 v0, 0x1

    return v0
.end method

.method public static getInstance()Lcom/itau/empresas/feature/login/controller/PreLoginController;
    .registers 2

    .line 89
    sget-object v0, Lcom/itau/empresas/feature/login/controller/PreLoginController;->controller:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_c

    sget-object v0, Lcom/itau/empresas/feature/login/controller/PreLoginController;->controller:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_18

    .line 90
    :cond_c
    new-instance v0, Ljava/lang/ref/WeakReference;

    new-instance v1, Lcom/itau/empresas/feature/login/controller/PreLoginController;

    invoke-direct {v1}, Lcom/itau/empresas/feature/login/controller/PreLoginController;-><init>()V

    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    sput-object v0, Lcom/itau/empresas/feature/login/controller/PreLoginController;->controller:Ljava/lang/ref/WeakReference;

    .line 92
    :cond_18
    sget-object v0, Lcom/itau/empresas/feature/login/controller/PreLoginController;->controller:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/feature/login/controller/PreLoginController;

    return-object v0
.end method


# virtual methods
.method protected api()Lcom/itau/empresas/feature/login/PreLoginAPI;
    .registers 2

    .line 77
    iget-object v0, p0, Lcom/itau/empresas/feature/login/controller/PreLoginController;->api:Lcom/itau/empresas/feature/login/PreLoginAPI;

    if-nez v0, :cond_e

    .line 78
    const-class v0, Lcom/itau/empresas/feature/login/PreLoginAPI;

    invoke-static {v0, p0}, Lbr/com/itau/sdk/android/core/BackendClient;->api(Ljava/lang/Class;Lbr/com/itau/sdk/android/core/endpoint/ErrorHandler;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/feature/login/PreLoginAPI;

    iput-object v0, p0, Lcom/itau/empresas/feature/login/controller/PreLoginController;->api:Lcom/itau/empresas/feature/login/PreLoginAPI;

    .line 80
    :cond_e
    iget-object v0, p0, Lcom/itau/empresas/feature/login/controller/PreLoginController;->api:Lcom/itau/empresas/feature/login/PreLoginAPI;

    return-object v0
.end method

.method protected bridge synthetic api()Ljava/lang/Object;
    .registers 2

    .line 22
    invoke-virtual {p0}, Lcom/itau/empresas/feature/login/controller/PreLoginController;->api()Lcom/itau/empresas/feature/login/PreLoginAPI;

    move-result-object v0

    return-object v0
.end method

.method public buscaOperador(Ljava/lang/String;)V
    .registers 3
    .param p1, "codigoOperador"    # Ljava/lang/String;

    .line 52
    new-instance v0, Lcom/itau/empresas/feature/login/controller/PreLoginController$2;

    invoke-direct {v0, p0, p1}, Lcom/itau/empresas/feature/login/controller/PreLoginController$2;-><init>(Lcom/itau/empresas/feature/login/controller/PreLoginController;Ljava/lang/String;)V

    invoke-static {v0}, Lbr/com/itau/sdk/android/core/BackendClient;->callApi(Lbr/com/itau/sdk/android/core/type/BackendCall;)V

    .line 63
    return-void
.end method

.method public buscaOperador(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 15
    .param p1, "agencia"    # Ljava/lang/String;
    .param p2, "conta"    # Ljava/lang/String;
    .param p3, "dac"    # Ljava/lang/String;
    .param p4, "documento"    # Ljava/lang/String;
    .param p5, "tipoDocumento"    # Ljava/lang/String;
    .param p6, "codigoOperador"    # Ljava/lang/String;

    .line 34
    new-instance v0, Lcom/itau/empresas/feature/login/controller/PreLoginController$1;

    move-object v1, p0

    move-object v2, p5

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/itau/empresas/feature/login/controller/PreLoginController$1;-><init>(Lcom/itau/empresas/feature/login/controller/PreLoginController;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Lbr/com/itau/sdk/android/core/BackendClient;->callApi(Lbr/com/itau/sdk/android/core/type/BackendCall;)V

    .line 49
    return-void
.end method

.method public handleError(Lbr/com/itau/sdk/android/core/exception/BackendException;)Ljava/lang/Throwable;
    .registers 3
    .param p1, "e"    # Lbr/com/itau/sdk/android/core/exception/BackendException;

    .line 71
    iget-object v0, p0, Lcom/itau/empresas/feature/login/controller/PreLoginController;->listener:Lcom/itau/empresas/feature/login/strategy/PreLoginControllerListener;

    invoke-interface {v0, p1}, Lcom/itau/empresas/feature/login/strategy/PreLoginControllerListener;->backendException(Lbr/com/itau/sdk/android/core/exception/BackendException;)V

    .line 72
    return-object p1
.end method

.method public setListener(Lcom/itau/empresas/feature/login/strategy/PreLoginControllerListener;)V
    .registers 5
    .param p1, "listener"    # Lcom/itau/empresas/feature/login/strategy/PreLoginControllerListener;

    .line 84
    const-string v0, "PreLoginController"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setListener() called with: listener = ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 85
    iput-object p1, p0, Lcom/itau/empresas/feature/login/controller/PreLoginController;->listener:Lcom/itau/empresas/feature/login/strategy/PreLoginControllerListener;

    .line 86
    return-void
.end method

.class public Lcom/itau/empresas/feature/login/adapter/OperadoresAdapter;
.super Landroid/support/v7/widget/RecyclerView$Adapter;
.source "OperadoresAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/itau/empresas/feature/login/adapter/OperadoresAdapter$ListenerItemSelecionado;,
        Lcom/itau/empresas/feature/login/adapter/OperadoresAdapter$ViewHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/support/v7/widget/RecyclerView$Adapter<Lcom/itau/empresas/feature/login/adapter/OperadoresAdapter$ViewHolder;>;"
    }
.end annotation


# instance fields
.field private indexSelecionado:I

.field private itemSselecionado:Landroid/widget/RadioButton;

.field private listaOperadores:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Lcom/itau/empresas/api/model/OperadorVO;>;"
        }
    .end annotation
.end field

.field private listenerItemSelecionado:Lcom/itau/empresas/feature/login/adapter/OperadoresAdapter$ListenerItemSelecionado;


# direct methods
.method public constructor <init>(Ljava/util/List;Lcom/itau/empresas/feature/login/adapter/OperadoresAdapter$ListenerItemSelecionado;)V
    .registers 4
    .param p1, "operadorVOArrayList"    # Ljava/util/List;
    .param p2, "listenerItemSelecionado"    # Lcom/itau/empresas/feature/login/adapter/OperadoresAdapter$ListenerItemSelecionado;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<Lcom/itau/empresas/api/model/OperadorVO;>;Lcom/itau/empresas/feature/login/adapter/OperadoresAdapter$ListenerItemSelecionado;)V"
        }
    .end annotation

    .line 26
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView$Adapter;-><init>()V

    .line 20
    const/4 v0, -0x1

    iput v0, p0, Lcom/itau/empresas/feature/login/adapter/OperadoresAdapter;->indexSelecionado:I

    .line 27
    iput-object p1, p0, Lcom/itau/empresas/feature/login/adapter/OperadoresAdapter;->listaOperadores:Ljava/util/List;

    .line 28
    iput-object p2, p0, Lcom/itau/empresas/feature/login/adapter/OperadoresAdapter;->listenerItemSelecionado:Lcom/itau/empresas/feature/login/adapter/OperadoresAdapter$ListenerItemSelecionado;

    .line 29
    return-void
.end method

.method static synthetic access$000(Lcom/itau/empresas/feature/login/adapter/OperadoresAdapter;)Lcom/itau/empresas/feature/login/adapter/OperadoresAdapter$ListenerItemSelecionado;
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/feature/login/adapter/OperadoresAdapter;

    .line 15
    iget-object v0, p0, Lcom/itau/empresas/feature/login/adapter/OperadoresAdapter;->listenerItemSelecionado:Lcom/itau/empresas/feature/login/adapter/OperadoresAdapter$ListenerItemSelecionado;

    return-object v0
.end method

.method static synthetic access$100(Lcom/itau/empresas/feature/login/adapter/OperadoresAdapter;)I
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/feature/login/adapter/OperadoresAdapter;

    .line 15
    iget v0, p0, Lcom/itau/empresas/feature/login/adapter/OperadoresAdapter;->indexSelecionado:I

    return v0
.end method

.method static synthetic access$102(Lcom/itau/empresas/feature/login/adapter/OperadoresAdapter;I)I
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/feature/login/adapter/OperadoresAdapter;
    .param p1, "x1"    # I

    .line 15
    iput p1, p0, Lcom/itau/empresas/feature/login/adapter/OperadoresAdapter;->indexSelecionado:I

    return p1
.end method

.method static synthetic access$200(Lcom/itau/empresas/feature/login/adapter/OperadoresAdapter;)Landroid/widget/RadioButton;
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/feature/login/adapter/OperadoresAdapter;

    .line 15
    iget-object v0, p0, Lcom/itau/empresas/feature/login/adapter/OperadoresAdapter;->itemSselecionado:Landroid/widget/RadioButton;

    return-object v0
.end method

.method static synthetic access$202(Lcom/itau/empresas/feature/login/adapter/OperadoresAdapter;Landroid/widget/RadioButton;)Landroid/widget/RadioButton;
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/feature/login/adapter/OperadoresAdapter;
    .param p1, "x1"    # Landroid/widget/RadioButton;

    .line 15
    iput-object p1, p0, Lcom/itau/empresas/feature/login/adapter/OperadoresAdapter;->itemSselecionado:Landroid/widget/RadioButton;

    return-object p1
.end method


# virtual methods
.method public getItemCount()I
    .registers 2

    .line 74
    iget-object v0, p0, Lcom/itau/empresas/feature/login/adapter/OperadoresAdapter;->listaOperadores:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public bridge synthetic onBindViewHolder(Landroid/support/v7/widget/RecyclerView$ViewHolder;I)V
    .registers 4

    .line 15
    move-object v0, p1

    check-cast v0, Lcom/itau/empresas/feature/login/adapter/OperadoresAdapter$ViewHolder;

    invoke-virtual {p0, v0, p2}, Lcom/itau/empresas/feature/login/adapter/OperadoresAdapter;->onBindViewHolder(Lcom/itau/empresas/feature/login/adapter/OperadoresAdapter$ViewHolder;I)V

    return-void
.end method

.method public onBindViewHolder(Lcom/itau/empresas/feature/login/adapter/OperadoresAdapter$ViewHolder;I)V
    .registers 7
    .param p1, "holder"    # Lcom/itau/empresas/feature/login/adapter/OperadoresAdapter$ViewHolder;
    .param p2, "position"    # I

    .line 41
    iget-object v0, p0, Lcom/itau/empresas/feature/login/adapter/OperadoresAdapter;->listaOperadores:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/itau/empresas/api/model/OperadorVO;

    .line 42
    .local v2, "operador":Lcom/itau/empresas/api/model/OperadorVO;
    move v3, p2

    .line 43
    .local v3, "posicaoSelecionado":I
    if-nez p2, :cond_1c

    .line 44
    iget-object v0, p1, Lcom/itau/empresas/feature/login/adapter/OperadoresAdapter$ViewHolder;->radioButton:Landroid/widget/RadioButton;

    invoke-virtual {v2}, Lcom/itau/empresas/api/model/OperadorVO;->getCodigoOperador()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setText(Ljava/lang/CharSequence;)V

    .line 45
    iget-object v0, p1, Lcom/itau/empresas/feature/login/adapter/OperadoresAdapter$ViewHolder;->divider:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2c

    .line 47
    :cond_1c
    iget-object v0, p1, Lcom/itau/empresas/feature/login/adapter/OperadoresAdapter$ViewHolder;->radioButton:Landroid/widget/RadioButton;

    invoke-virtual {v2}, Lcom/itau/empresas/api/model/OperadorVO;->getCodigoOperador()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setText(Ljava/lang/CharSequence;)V

    .line 48
    iget-object v0, p1, Lcom/itau/empresas/feature/login/adapter/OperadoresAdapter$ViewHolder;->divider:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 50
    :goto_2c
    iget-object v0, p1, Lcom/itau/empresas/feature/login/adapter/OperadoresAdapter$ViewHolder;->radioButton:Landroid/widget/RadioButton;

    new-instance v1, Lcom/itau/empresas/feature/login/adapter/OperadoresAdapter$1;

    invoke-direct {v1, p0, v2, v3}, Lcom/itau/empresas/feature/login/adapter/OperadoresAdapter$1;-><init>(Lcom/itau/empresas/feature/login/adapter/OperadoresAdapter;Lcom/itau/empresas/api/model/OperadorVO;I)V

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 62
    iget v0, p0, Lcom/itau/empresas/feature/login/adapter/OperadoresAdapter;->indexSelecionado:I

    if-eq v0, p2, :cond_41

    .line 63
    iget-object v0, p1, Lcom/itau/empresas/feature/login/adapter/OperadoresAdapter$ViewHolder;->radioButton:Landroid/widget/RadioButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setChecked(Z)V

    goto :goto_55

    .line 65
    :cond_41
    iget-object v0, p1, Lcom/itau/empresas/feature/login/adapter/OperadoresAdapter$ViewHolder;->radioButton:Landroid/widget/RadioButton;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 66
    iget-object v0, p0, Lcom/itau/empresas/feature/login/adapter/OperadoresAdapter;->itemSselecionado:Landroid/widget/RadioButton;

    if-eqz v0, :cond_55

    iget-object v0, p1, Lcom/itau/empresas/feature/login/adapter/OperadoresAdapter$ViewHolder;->radioButton:Landroid/widget/RadioButton;

    iget-object v1, p0, Lcom/itau/empresas/feature/login/adapter/OperadoresAdapter;->itemSselecionado:Landroid/widget/RadioButton;

    if-eq v0, v1, :cond_55

    .line 67
    iget-object v0, p1, Lcom/itau/empresas/feature/login/adapter/OperadoresAdapter$ViewHolder;->radioButton:Landroid/widget/RadioButton;

    iput-object v0, p0, Lcom/itau/empresas/feature/login/adapter/OperadoresAdapter;->itemSselecionado:Landroid/widget/RadioButton;

    .line 70
    :cond_55
    :goto_55
    return-void
.end method

.method public bridge synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .registers 4

    .line 15
    invoke-virtual {p0, p1, p2}, Lcom/itau/empresas/feature/login/adapter/OperadoresAdapter;->onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/itau/empresas/feature/login/adapter/OperadoresAdapter$ViewHolder;

    move-result-object v0

    return-object v0
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/itau/empresas/feature/login/adapter/OperadoresAdapter$ViewHolder;
    .registers 7
    .param p1, "parent"    # Landroid/view/ViewGroup;
    .param p2, "viewType"    # I

    .line 34
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 35
    const v1, 0x7f030108

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    .line 36
    .local v3, "v":Landroid/view/View;
    new-instance v0, Lcom/itau/empresas/feature/login/adapter/OperadoresAdapter$ViewHolder;

    invoke-direct {v0, v3}, Lcom/itau/empresas/feature/login/adapter/OperadoresAdapter$ViewHolder;-><init>(Landroid/view/View;)V

    return-object v0
.end method

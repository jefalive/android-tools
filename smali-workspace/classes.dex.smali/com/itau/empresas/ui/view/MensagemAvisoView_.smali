.class public final Lcom/itau/empresas/ui/view/MensagemAvisoView_;
.super Lcom/itau/empresas/ui/view/MensagemAvisoView;
.source "MensagemAvisoView_.java"

# interfaces
.implements Lorg/androidannotations/api/view/HasViews;
.implements Lorg/androidannotations/api/view/OnViewChangedListener;


# instance fields
.field private alreadyInflated_:Z

.field private final onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .param p1, "context"    # Landroid/content/Context;

    .line 36
    invoke-direct {p0, p1}, Lcom/itau/empresas/ui/view/MensagemAvisoView;-><init>(Landroid/content/Context;)V

    .line 32
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/itau/empresas/ui/view/MensagemAvisoView_;->alreadyInflated_:Z

    .line 33
    new-instance v0, Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-direct {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/ui/view/MensagemAvisoView_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 37
    invoke-direct {p0}, Lcom/itau/empresas/ui/view/MensagemAvisoView_;->init_()V

    .line 38
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .line 41
    invoke-direct {p0, p1, p2}, Lcom/itau/empresas/ui/view/MensagemAvisoView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 32
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/itau/empresas/ui/view/MensagemAvisoView_;->alreadyInflated_:Z

    .line 33
    new-instance v0, Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-direct {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/ui/view/MensagemAvisoView_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 42
    invoke-direct {p0}, Lcom/itau/empresas/ui/view/MensagemAvisoView_;->init_()V

    .line 43
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .line 46
    invoke-direct {p0, p1, p2, p3}, Lcom/itau/empresas/ui/view/MensagemAvisoView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 32
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/itau/empresas/ui/view/MensagemAvisoView_;->alreadyInflated_:Z

    .line 33
    new-instance v0, Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-direct {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/ui/view/MensagemAvisoView_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 47
    invoke-direct {p0}, Lcom/itau/empresas/ui/view/MensagemAvisoView_;->init_()V

    .line 48
    return-void
.end method

.method private init_()V
    .registers 4

    .line 78
    iget-object v0, p0, Lcom/itau/empresas/ui/view/MensagemAvisoView_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-static {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    move-result-object v2

    .line 79
    .local v2, "previousNotifier":Lorg/androidannotations/api/view/OnViewChangedNotifier;
    invoke-static {p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->registerOnViewChangedListener(Lorg/androidannotations/api/view/OnViewChangedListener;)V

    .line 80
    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/MensagemAvisoView_;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0c014d

    invoke-static {v0, v1}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v0

    iput v0, p0, Lcom/itau/empresas/ui/view/MensagemAvisoView_;->corVerde:I

    .line 81
    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/MensagemAvisoView_;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0c0044

    invoke-static {v0, v1}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v0

    iput v0, p0, Lcom/itau/empresas/ui/view/MensagemAvisoView_;->corCinza:I

    .line 82
    invoke-static {v2}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 83
    return-void
.end method


# virtual methods
.method public onFinishInflate()V
    .registers 3

    .line 69
    iget-boolean v0, p0, Lcom/itau/empresas/ui/view/MensagemAvisoView_;->alreadyInflated_:Z

    if-nez v0, :cond_16

    .line 70
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/itau/empresas/ui/view/MensagemAvisoView_;->alreadyInflated_:Z

    .line 71
    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/MensagemAvisoView_;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f03017f

    invoke-static {v0, v1, p0}, Lcom/itau/empresas/ui/view/MensagemAvisoView_;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 72
    iget-object v0, p0, Lcom/itau/empresas/ui/view/MensagemAvisoView_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 74
    :cond_16
    invoke-super {p0}, Lcom/itau/empresas/ui/view/MensagemAvisoView;->onFinishInflate()V

    .line 75
    return-void
.end method

.method public onViewChanged(Lorg/androidannotations/api/view/HasViews;)V
    .registers 3
    .param p1, "hasViews"    # Lorg/androidannotations/api/view/HasViews;

    .line 105
    const v0, 0x7f0e0680

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/itau/empresas/ui/view/MensagemAvisoView_;->ll_view_mensagem_aviso:Landroid/widget/LinearLayout;

    .line 106
    const v0, 0x7f0e019d

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/ui/view/MensagemAvisoView_;->texto_aviso:Landroid/widget/TextView;

    .line 107
    return-void
.end method

.class public final Lbr/com/itau/security/token/model/OTPModel;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final serialNumber:Ljava/lang/String;

.field private final timeInterval:J

.field private final timeRemaining:J

.field private final token:Ljava/lang/String;

.field private final weakToken:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJ)V
    .registers 8

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    iput-object p1, p0, Lbr/com/itau/security/token/model/OTPModel;->token:Ljava/lang/String;

    .line 13
    iput-object p2, p0, Lbr/com/itau/security/token/model/OTPModel;->weakToken:Ljava/lang/String;

    .line 14
    iput-wide p4, p0, Lbr/com/itau/security/token/model/OTPModel;->timeRemaining:J

    .line 15
    iput-wide p6, p0, Lbr/com/itau/security/token/model/OTPModel;->timeInterval:J

    .line 16
    iput-object p3, p0, Lbr/com/itau/security/token/model/OTPModel;->serialNumber:Ljava/lang/String;

    .line 17
    return-void
.end method


# virtual methods
.method protected final clone()Ljava/lang/Object;
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .line 21
    new-instance v0, Ljava/lang/CloneNotSupportedException;

    invoke-direct {v0}, Ljava/lang/CloneNotSupportedException;-><init>()V

    throw v0
.end method

.method public final serialNumber()Ljava/lang/String;
    .registers 2

    .line 41
    iget-object v0, p0, Lbr/com/itau/security/token/model/OTPModel;->serialNumber:Ljava/lang/String;

    return-object v0
.end method

.method public final timeInterval()J
    .registers 3

    .line 37
    iget-wide v0, p0, Lbr/com/itau/security/token/model/OTPModel;->timeInterval:J

    return-wide v0
.end method

.method public final timeRemaining()J
    .registers 3

    .line 33
    iget-wide v0, p0, Lbr/com/itau/security/token/model/OTPModel;->timeRemaining:J

    return-wide v0
.end method

.method public final token()Ljava/lang/String;
    .registers 2

    .line 25
    iget-object v0, p0, Lbr/com/itau/security/token/model/OTPModel;->token:Ljava/lang/String;

    return-object v0
.end method

.method public final weakToken()Ljava/lang/String;
    .registers 2

    .line 29
    iget-object v0, p0, Lbr/com/itau/security/token/model/OTPModel;->weakToken:Ljava/lang/String;

    return-object v0
.end method

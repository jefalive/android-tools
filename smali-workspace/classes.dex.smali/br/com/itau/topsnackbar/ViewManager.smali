.class abstract Lbr/com/itau/topsnackbar/ViewManager;
.super Ljava/lang/Object;
.source "ViewManager.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<VIEWGROUP:Landroid/view/ViewGroup;>Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field protected final parent:Landroid/view/ViewGroup;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TVIEWGROUP;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Landroid/view/ViewGroup;)V
    .registers 2
    .param p1, "parent"    # Landroid/view/ViewGroup;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TVIEWGROUP;)V"
        }
    .end annotation

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    iput-object p1, p0, Lbr/com/itau/topsnackbar/ViewManager;->parent:Landroid/view/ViewGroup;

    .line 11
    return-void
.end method


# virtual methods
.method abstract addContent(Landroid/view/View;)V
.end method

.method abstract removeContent(Landroid/view/View;)V
.end method

.class public abstract Lcom/google/zxing/b/b;
.super Ljava/lang/Object;


# direct methods
.method public constructor <init>()V
    .registers 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method protected static a([I[IF)F
    .registers 14

    array-length v2, p0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    :goto_4
    if-ge v5, v2, :cond_f

    aget v0, p0, v5

    add-int/2addr v3, v0

    aget v0, p1, v5

    add-int/2addr v4, v0

    add-int/lit8 v5, v5, 0x1

    goto :goto_4

    :cond_f
    if-ge v3, v4, :cond_14

    const/high16 v0, 0x7f800000    # Float.POSITIVE_INFINITY

    return v0

    :cond_14
    int-to-float v0, v3

    int-to-float v1, v4

    div-float v5, v0, v1

    mul-float/2addr p2, v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    :goto_1b
    if-ge v7, v2, :cond_3b

    aget v8, p0, v7

    aget v0, p1, v7

    int-to-float v0, v0

    mul-float v9, v0, v5

    int-to-float v0, v8

    cmpl-float v0, v0, v9

    if-lez v0, :cond_2d

    int-to-float v0, v8

    sub-float v10, v0, v9

    goto :goto_30

    :cond_2d
    int-to-float v0, v8

    sub-float v10, v9, v0

    :goto_30
    cmpl-float v0, v10, p2

    if-lez v0, :cond_37

    const/high16 v0, 0x7f800000    # Float.POSITIVE_INFINITY

    return v0

    :cond_37
    add-float/2addr v6, v10

    add-int/lit8 v7, v7, 0x1

    goto :goto_1b

    :cond_3b
    int-to-float v0, v3

    div-float v0, v6, v0

    return v0
.end method

.method protected static a(Lcom/google/zxing/a/a;I[I)V
    .registers 10

    array-length v2, p2

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-static {p2, v0, v2, v1}, Ljava/util/Arrays;->fill([IIII)V

    invoke-virtual {p0}, Lcom/google/zxing/a/a;->a()I

    move-result v3

    if-lt p1, v3, :cond_11

    invoke-static {}, Lcom/google/zxing/h;->a()Lcom/google/zxing/h;

    move-result-object v0

    throw v0

    :cond_11
    invoke-virtual {p0, p1}, Lcom/google/zxing/a/a;->a(I)Z

    move-result v0

    if-nez v0, :cond_19

    const/4 v4, 0x1

    goto :goto_1a

    :cond_19
    const/4 v4, 0x0

    :goto_1a
    const/4 v5, 0x0

    move v6, p1

    :goto_1c
    if-ge v6, v3, :cond_3c

    invoke-virtual {p0, v6}, Lcom/google/zxing/a/a;->a(I)Z

    move-result v0

    xor-int/2addr v0, v4

    if-eqz v0, :cond_2c

    aget v0, p2, v5

    add-int/lit8 v0, v0, 0x1

    aput v0, p2, v5

    goto :goto_39

    :cond_2c
    add-int/lit8 v5, v5, 0x1

    if-ne v5, v2, :cond_31

    goto :goto_3c

    :cond_31
    const/4 v0, 0x1

    aput v0, p2, v5

    if-nez v4, :cond_38

    const/4 v4, 0x1

    goto :goto_39

    :cond_38
    const/4 v4, 0x0

    :goto_39
    add-int/lit8 v6, v6, 0x1

    goto :goto_1c

    :cond_3c
    :goto_3c
    if-eq v5, v2, :cond_49

    add-int/lit8 v0, v2, -0x1

    if-ne v5, v0, :cond_44

    if-eq v6, v3, :cond_49

    :cond_44
    invoke-static {}, Lcom/google/zxing/h;->a()Lcom/google/zxing/h;

    move-result-object v0

    throw v0

    :cond_49
    return-void
.end method

.method private b(Lcom/google/zxing/c;Ljava/util/Map;)Lcom/google/zxing/k;
    .registers 20

    invoke-virtual/range {p1 .. p1}, Lcom/google/zxing/c;->a()I

    move-result v3

    invoke-virtual/range {p1 .. p1}, Lcom/google/zxing/c;->b()I

    move-result v4

    new-instance v5, Lcom/google/zxing/a/a;

    invoke-direct {v5, v3}, Lcom/google/zxing/a/a;-><init>(I)V

    shr-int/lit8 v6, v4, 0x1

    if-eqz p2, :cond_1d

    sget-object v0, Lcom/google/zxing/e;->d:Lcom/google/zxing/e;

    move-object/from16 v1, p2

    invoke-interface {v1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1d

    const/4 v7, 0x1

    goto :goto_1e

    :cond_1d
    const/4 v7, 0x0

    :goto_1e
    if-eqz v7, :cond_23

    const/16 v0, 0x8

    goto :goto_24

    :cond_23
    const/4 v0, 0x5

    :goto_24
    shr-int v0, v4, v0

    const/4 v1, 0x1

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v8

    if-eqz v7, :cond_2f

    move v9, v4

    goto :goto_31

    :cond_2f
    const/16 v9, 0xf

    :goto_31
    const/4 v10, 0x0

    :goto_32
    if-ge v10, v9, :cond_dd

    add-int/lit8 v0, v10, 0x1

    div-int/lit8 v11, v0, 0x2

    and-int/lit8 v0, v10, 0x1

    if-nez v0, :cond_3e

    const/4 v12, 0x1

    goto :goto_3f

    :cond_3e
    const/4 v12, 0x0

    :goto_3f
    if-eqz v12, :cond_43

    move v0, v11

    goto :goto_44

    :cond_43
    neg-int v0, v11

    :goto_44
    mul-int/2addr v0, v8

    add-int v13, v6, v0

    if-ltz v13, :cond_dd

    if-lt v13, v4, :cond_4d

    goto/16 :goto_dd

    :cond_4d
    move-object/from16 v0, p1

    :try_start_4f
    invoke-virtual {v0, v13, v5}, Lcom/google/zxing/c;->a(ILcom/google/zxing/a/a;)Lcom/google/zxing/a/a;
    :try_end_52
    .catch Lcom/google/zxing/h; {:try_start_4f .. :try_end_52} :catch_55

    move-result-object v0

    move-object v5, v0

    goto :goto_58

    :catch_55
    move-exception v14

    goto/16 :goto_d9

    :goto_58
    const/4 v14, 0x0

    :goto_59
    const/4 v0, 0x2

    if-ge v14, v0, :cond_d9

    const/4 v0, 0x1

    if-ne v14, v0, :cond_81

    invoke-virtual {v5}, Lcom/google/zxing/a/a;->c()V

    if-eqz p2, :cond_81

    sget-object v0, Lcom/google/zxing/e;->j:Lcom/google/zxing/e;

    move-object/from16 v1, p2

    invoke-interface {v1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_81

    new-instance v15, Ljava/util/EnumMap;

    const-class v0, Lcom/google/zxing/e;

    invoke-direct {v15, v0}, Ljava/util/EnumMap;-><init>(Ljava/lang/Class;)V

    move-object/from16 v0, p2

    invoke-interface {v15, v0}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    sget-object v0, Lcom/google/zxing/e;->j:Lcom/google/zxing/e;

    invoke-interface {v15, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-object/from16 p2, v15

    :cond_81
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    :try_start_85
    invoke-virtual {v0, v13, v5, v1}, Lcom/google/zxing/b/b;->a(ILcom/google/zxing/a/a;Ljava/util/Map;)Lcom/google/zxing/k;

    move-result-object v15

    const/4 v0, 0x1

    if-ne v14, v0, :cond_d3

    sget-object v0, Lcom/google/zxing/l;->b:Lcom/google/zxing/l;

    const/16 v1, 0xb4

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v15, v0, v1}, Lcom/google/zxing/k;->a(Lcom/google/zxing/l;Ljava/lang/Object;)V

    invoke-virtual {v15}, Lcom/google/zxing/k;->b()[Lcom/google/zxing/m;

    move-result-object v16

    if-eqz v16, :cond_d3

    new-instance v0, Lcom/google/zxing/m;

    int-to-float v1, v3

    const/4 v2, 0x0

    aget-object v2, v16, v2

    invoke-virtual {v2}, Lcom/google/zxing/m;->a()F

    move-result v2

    sub-float/2addr v1, v2

    const/high16 v2, 0x3f800000    # 1.0f

    sub-float/2addr v1, v2

    const/4 v2, 0x0

    aget-object v2, v16, v2

    invoke-virtual {v2}, Lcom/google/zxing/m;->b()F

    move-result v2

    invoke-direct {v0, v1, v2}, Lcom/google/zxing/m;-><init>(FF)V

    const/4 v1, 0x0

    aput-object v0, v16, v1

    new-instance v0, Lcom/google/zxing/m;

    int-to-float v1, v3

    const/4 v2, 0x1

    aget-object v2, v16, v2

    invoke-virtual {v2}, Lcom/google/zxing/m;->a()F

    move-result v2

    sub-float/2addr v1, v2

    const/high16 v2, 0x3f800000    # 1.0f

    sub-float/2addr v1, v2

    const/4 v2, 0x1

    aget-object v2, v16, v2

    invoke-virtual {v2}, Lcom/google/zxing/m;->b()F

    move-result v2

    invoke-direct {v0, v1, v2}, Lcom/google/zxing/m;-><init>(FF)V

    const/4 v1, 0x1

    aput-object v0, v16, v1
    :try_end_d3
    .catch Lcom/google/zxing/j; {:try_start_85 .. :try_end_d3} :catch_d4

    :cond_d3
    return-object v15

    :catch_d4
    move-exception v15

    add-int/lit8 v14, v14, 0x1

    goto/16 :goto_59

    :cond_d9
    :goto_d9
    add-int/lit8 v10, v10, 0x1

    goto/16 :goto_32

    :cond_dd
    :goto_dd
    invoke-static {}, Lcom/google/zxing/h;->a()Lcom/google/zxing/h;

    move-result-object v0

    throw v0
.end method


# virtual methods
.method public abstract a(ILcom/google/zxing/a/a;Ljava/util/Map;)Lcom/google/zxing/k;
.end method

.method public a(Lcom/google/zxing/c;Ljava/util/Map;)Lcom/google/zxing/k;
    .registers 15

    :try_start_0
    invoke-direct {p0, p1, p2}, Lcom/google/zxing/b/b;->b(Lcom/google/zxing/c;Ljava/util/Map;)Lcom/google/zxing/k;
    :try_end_3
    .catch Lcom/google/zxing/h; {:try_start_0 .. :try_end_3} :catch_5

    move-result-object v0

    return-object v0

    :catch_5
    move-exception v3

    if-eqz p2, :cond_12

    sget-object v0, Lcom/google/zxing/e;->d:Lcom/google/zxing/e;

    invoke-interface {p2, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_12

    const/4 v4, 0x1

    goto :goto_13

    :cond_12
    const/4 v4, 0x0

    :goto_13
    if-eqz v4, :cond_76

    invoke-virtual {p1}, Lcom/google/zxing/c;->d()Z

    move-result v0

    if-eqz v0, :cond_76

    invoke-virtual {p1}, Lcom/google/zxing/c;->e()Lcom/google/zxing/c;

    move-result-object v5

    invoke-direct {p0, v5, p2}, Lcom/google/zxing/b/b;->b(Lcom/google/zxing/c;Ljava/util/Map;)Lcom/google/zxing/k;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/zxing/k;->c()Ljava/util/Map;

    move-result-object v7

    const/16 v8, 0x10e

    if-eqz v7, :cond_43

    sget-object v0, Lcom/google/zxing/l;->b:Lcom/google/zxing/l;

    invoke-interface {v7, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_43

    sget-object v0, Lcom/google/zxing/l;->b:Lcom/google/zxing/l;

    invoke-interface {v7, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    add-int/lit16 v0, v0, 0x10e

    rem-int/lit16 v8, v0, 0x168

    :cond_43
    sget-object v0, Lcom/google/zxing/l;->b:Lcom/google/zxing/l;

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v6, v0, v1}, Lcom/google/zxing/k;->a(Lcom/google/zxing/l;Ljava/lang/Object;)V

    invoke-virtual {v6}, Lcom/google/zxing/k;->b()[Lcom/google/zxing/m;

    move-result-object v9

    if-eqz v9, :cond_75

    invoke-virtual {v5}, Lcom/google/zxing/c;->b()I

    move-result v10

    const/4 v11, 0x0

    :goto_57
    array-length v0, v9

    if-ge v11, v0, :cond_75

    new-instance v0, Lcom/google/zxing/m;

    int-to-float v1, v10

    aget-object v2, v9, v11

    invoke-virtual {v2}, Lcom/google/zxing/m;->b()F

    move-result v2

    sub-float/2addr v1, v2

    const/high16 v2, 0x3f800000    # 1.0f

    sub-float/2addr v1, v2

    aget-object v2, v9, v11

    invoke-virtual {v2}, Lcom/google/zxing/m;->a()F

    move-result v2

    invoke-direct {v0, v1, v2}, Lcom/google/zxing/m;-><init>(FF)V

    aput-object v0, v9, v11

    add-int/lit8 v11, v11, 0x1

    goto :goto_57

    :cond_75
    return-object v6

    :cond_76
    throw v3
.end method

.method public a()V
    .registers 1

    return-void
.end method

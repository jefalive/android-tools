.class public Lbr/com/itau/sdk/android/core/token/TokenWaiter;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final c:Ljava/util/concurrent/CountDownLatch;

.field private d:I


# direct methods
.method public constructor <init>()V
    .registers 3

    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, Lbr/com/itau/sdk/android/core/token/TokenWaiter;->c:Ljava/util/concurrent/CountDownLatch;

    .line 12
    const/4 v0, 0x0

    iput v0, p0, Lbr/com/itau/sdk/android/core/token/TokenWaiter;->d:I

    return-void
.end method


# virtual methods
.method public cancelWaiting()V
    .registers 2

    .line 22
    const/4 v0, -0x1

    iput v0, p0, Lbr/com/itau/sdk/android/core/token/TokenWaiter;->d:I

    .line 23
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/token/TokenWaiter;->c:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 24
    return-void
.end method

.method public startWaiting(ILjava/util/concurrent/TimeUnit;)V
    .registers 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .line 15
    invoke-virtual {p0}, Lbr/com/itau/sdk/android/core/token/TokenWaiter;->wasCanceled()Z

    move-result v0

    if-nez v0, :cond_e

    .line 16
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/token/TokenWaiter;->c:Ljava/util/concurrent/CountDownLatch;

    add-int/lit8 v1, p1, 0x1

    int-to-long v1, v1

    invoke-virtual {v0, v1, v2, p2}, Ljava/util/concurrent/CountDownLatch;->await(JLjava/util/concurrent/TimeUnit;)Z

    .line 18
    :cond_e
    return-void
.end method

.method public wasCanceled()Z
    .registers 3

    .line 27
    iget v0, p0, Lbr/com/itau/sdk/android/core/token/TokenWaiter;->d:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_7

    const/4 v0, 0x1

    goto :goto_8

    :cond_7
    const/4 v0, 0x0

    :goto_8
    return v0
.end method

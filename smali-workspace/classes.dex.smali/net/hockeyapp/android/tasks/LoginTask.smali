.class public Lnet/hockeyapp/android/tasks/LoginTask;
.super Landroid/os/AsyncTask;
.source "LoginTask.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<Ljava/lang/Void;Ljava/lang/Void;Ljava/lang/Boolean;>;"
    }
.end annotation


# instance fields
.field private context:Landroid/content/Context;

.field private handler:Landroid/os/Handler;

.field private final mode:I

.field private final params:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
        }
    .end annotation
.end field

.field private progressDialog:Landroid/app/ProgressDialog;

.field private showProgressDialog:Z

.field private final urlString:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/os/Handler;Ljava/lang/String;ILjava/util/Map;)V
    .registers 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "handler"    # Landroid/os/Handler;
    .param p3, "urlString"    # Ljava/lang/String;
    .param p4, "mode"    # I
    .param p5, "params"    # Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Landroid/content/Context;Landroid/os/Handler;Ljava/lang/String;ILjava/util/Map<Ljava/lang/String;Ljava/lang/String;>;)V"
        }
    .end annotation

    .line 93
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 94
    iput-object p1, p0, Lnet/hockeyapp/android/tasks/LoginTask;->context:Landroid/content/Context;

    .line 95
    iput-object p2, p0, Lnet/hockeyapp/android/tasks/LoginTask;->handler:Landroid/os/Handler;

    .line 96
    iput-object p3, p0, Lnet/hockeyapp/android/tasks/LoginTask;->urlString:Ljava/lang/String;

    .line 97
    iput p4, p0, Lnet/hockeyapp/android/tasks/LoginTask;->mode:I

    .line 98
    iput-object p5, p0, Lnet/hockeyapp/android/tasks/LoginTask;->params:Ljava/util/Map;

    .line 99
    const/4 v0, 0x1

    iput-boolean v0, p0, Lnet/hockeyapp/android/tasks/LoginTask;->showProgressDialog:Z

    .line 101
    if-eqz p1, :cond_15

    .line 102
    invoke-static {p1}, Lnet/hockeyapp/android/Constants;->loadFromContext(Landroid/content/Context;)V

    .line 104
    :cond_15
    return-void
.end method

.method private handleResponse(Ljava/lang/String;)Z
    .registers 9
    .param p1, "responseStr"    # Ljava/lang/String;

    .line 221
    iget-object v0, p0, Lnet/hockeyapp/android/tasks/LoginTask;->context:Landroid/content/Context;

    const-string v1, "net.hockeyapp.android.login"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v3

    .line 224
    .local v3, "prefs":Landroid/content/SharedPreferences;
    :try_start_9
    new-instance v4, Lorg/json/JSONObject;

    invoke-direct {v4, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 225
    .local v4, "response":Lorg/json/JSONObject;
    const-string v0, "status"

    invoke-virtual {v4, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 227
    .local v5, "status":Ljava/lang/String;
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z
    :try_end_17
    .catch Lorg/json/JSONException; {:try_start_9 .. :try_end_17} :catch_b5

    move-result v0

    if-eqz v0, :cond_1c

    .line 228
    const/4 v0, 0x0

    return v0

    .line 231
    :cond_1c
    :try_start_1c
    iget v0, p0, Lnet/hockeyapp/android/tasks/LoginTask;->mode:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_46

    .line 232
    const-string v0, "identified"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b3

    .line 233
    const-string v0, "iuid"

    invoke-virtual {v4, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 234
    .local v6, "iuid":Ljava/lang/String;
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_44

    .line 235
    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "iuid"

    invoke-interface {v0, v1, v6}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-static {v0}, Lnet/hockeyapp/android/utils/PrefsUtil;->applyChanges(Landroid/content/SharedPreferences$Editor;)V
    :try_end_42
    .catch Lorg/json/JSONException; {:try_start_1c .. :try_end_42} :catch_b5

    .line 236
    const/4 v0, 0x1

    return v0

    .line 238
    .end local v6    # "iuid":Ljava/lang/String;
    :cond_44
    goto/16 :goto_b3

    .line 240
    :cond_46
    :try_start_46
    iget v0, p0, Lnet/hockeyapp/android/tasks/LoginTask;->mode:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_6f

    .line 241
    const-string v0, "authorized"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b3

    .line 242
    const-string v0, "auid"

    invoke-virtual {v4, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 243
    .local v6, "auid":Ljava/lang/String;
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_6e

    .line 244
    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "auid"

    invoke-interface {v0, v1, v6}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-static {v0}, Lnet/hockeyapp/android/utils/PrefsUtil;->applyChanges(Landroid/content/SharedPreferences$Editor;)V
    :try_end_6c
    .catch Lorg/json/JSONException; {:try_start_46 .. :try_end_6c} :catch_b5

    .line 245
    const/4 v0, 0x1

    return v0

    .line 247
    .end local v6    # "auid":Ljava/lang/String;
    :cond_6e
    goto :goto_b3

    .line 249
    :cond_6f
    :try_start_6f
    iget v0, p0, Lnet/hockeyapp/android/tasks/LoginTask;->mode:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_92

    .line 250
    const-string v0, "validated"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_79
    .catch Lorg/json/JSONException; {:try_start_6f .. :try_end_79} :catch_b5

    move-result v0

    if-eqz v0, :cond_7e

    .line 251
    const/4 v0, 0x1

    return v0

    .line 254
    :cond_7e
    :try_start_7e
    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "iuid"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "auid"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-static {v0}, Lnet/hockeyapp/android/utils/PrefsUtil;->applyChanges(Landroid/content/SharedPreferences$Editor;)V

    goto :goto_b3

    .line 258
    :cond_92
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Login mode "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lnet/hockeyapp/android/tasks/LoginTask;->mode:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " not supported."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_b3
    .catch Lorg/json/JSONException; {:try_start_7e .. :try_end_b3} :catch_b5

    .line 261
    :cond_b3
    :goto_b3
    const/4 v0, 0x0

    return v0

    .line 263
    .end local v4    # "response":Lorg/json/JSONObject;
    .end local v5    # "status":Ljava/lang/String;
    :catch_b5
    move-exception v4

    .line 264
    .local v4, "e":Lorg/json/JSONException;
    invoke-virtual {v4}, Lorg/json/JSONException;->printStackTrace()V

    .line 265
    const/4 v0, 0x0

    return v0
.end method

.method private makeRequest(ILjava/util/Map;)Lorg/apache/http/client/methods/HttpUriRequest;
    .registers 10
    .param p1, "mode"    # I
    .param p2, "params"    # Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ILjava/util/Map<Ljava/lang/String;Ljava/lang/String;>;)Lorg/apache/http/client/methods/HttpUriRequest;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/UnsupportedEncodingException;
        }
    .end annotation

    .line 182
    const/4 v0, 0x1

    if-ne p1, v0, :cond_49

    .line 183
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 185
    .local v3, "nameValuePairs":Ljava/util/List;, "Ljava/util/List<Lorg/apache/http/NameValuePair;>;"
    invoke-interface {p2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_10
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_32

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Ljava/util/Map$Entry;

    .line 186
    .local v5, "param":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    invoke-interface {v5}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v5}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 187
    .end local v5    # "param":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v5
    goto :goto_10

    .line 189
    :cond_32
    new-instance v4, Lorg/apache/http/client/entity/UrlEncodedFormEntity;

    const-string v0, "UTF-8"

    invoke-direct {v4, v3, v0}, Lorg/apache/http/client/entity/UrlEncodedFormEntity;-><init>(Ljava/util/List;Ljava/lang/String;)V

    .line 190
    .local v4, "form":Lorg/apache/http/client/entity/UrlEncodedFormEntity;
    const-string v0, "UTF-8"

    invoke-virtual {v4, v0}, Lorg/apache/http/client/entity/UrlEncodedFormEntity;->setContentEncoding(Ljava/lang/String;)V

    .line 191
    new-instance v5, Lorg/apache/http/client/methods/HttpPost;

    iget-object v0, p0, Lnet/hockeyapp/android/tasks/LoginTask;->urlString:Ljava/lang/String;

    invoke-direct {v5, v0}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/lang/String;)V

    .line 192
    .local v5, "httpPost":Lorg/apache/http/client/methods/HttpPost;
    invoke-virtual {v5, v4}, Lorg/apache/http/client/methods/HttpPost;->setEntity(Lorg/apache/http/HttpEntity;)V

    .line 194
    return-object v5

    .line 196
    .end local v3    # "nameValuePairs":Ljava/util/List;, "Ljava/util/List<Lorg/apache/http/NameValuePair;>;"
    .end local v3
    .end local v4    # "form":Lorg/apache/http/client/entity/UrlEncodedFormEntity;
    .end local v5    # "httpPost":Lorg/apache/http/client/methods/HttpPost;
    :cond_49
    const/4 v0, 0x2

    if-ne p1, v0, :cond_9e

    .line 197
    const-string v0, "email"

    invoke-interface {p2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Ljava/lang/String;

    .line 198
    .local v3, "email":Ljava/lang/String;
    const-string v0, "password"

    invoke-interface {p2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Ljava/lang/String;

    .line 199
    .local v4, "password":Ljava/lang/String;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Basic "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 200
    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    .line 199
    const/4 v2, 0x2

    invoke-static {v1, v2}, Lnet/hockeyapp/android/utils/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 202
    .local v5, "authStr":Ljava/lang/String;
    new-instance v6, Lorg/apache/http/client/methods/HttpPost;

    iget-object v0, p0, Lnet/hockeyapp/android/tasks/LoginTask;->urlString:Ljava/lang/String;

    invoke-direct {v6, v0}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/lang/String;)V

    .line 203
    .local v6, "httpPost":Lorg/apache/http/client/methods/HttpPost;
    const-string v0, "Authorization"

    invoke-virtual {v6, v0, v5}, Lorg/apache/http/client/methods/HttpPost;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 205
    return-object v6

    .line 207
    .end local v3    # "email":Ljava/lang/String;
    .end local v4    # "password":Ljava/lang/String;
    .end local v5    # "authStr":Ljava/lang/String;
    .end local v6    # "httpPost":Lorg/apache/http/client/methods/HttpPost;
    :cond_9e
    const/4 v0, 0x3

    if-ne p1, v0, :cond_dc

    .line 208
    const-string v0, "type"

    invoke-interface {p2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Ljava/lang/String;

    .line 209
    .local v3, "type":Ljava/lang/String;
    const-string v0, "id"

    invoke-interface {p2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Ljava/lang/String;

    .line 210
    .local v4, "id":Ljava/lang/String;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lnet/hockeyapp/android/tasks/LoginTask;->urlString:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "?"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 212
    .local v5, "paramUrl":Ljava/lang/String;
    new-instance v6, Lorg/apache/http/client/methods/HttpGet;

    invoke-direct {v6, v5}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    .line 213
    .local v6, "httpGet":Lorg/apache/http/client/methods/HttpGet;
    return-object v6

    .line 216
    .end local v3    # "type":Ljava/lang/String;
    .end local v4    # "id":Ljava/lang/String;
    .end local v5    # "paramUrl":Ljava/lang/String;
    .end local v6    # "httpGet":Lorg/apache/http/client/methods/HttpGet;
    :cond_dc
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Login mode "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " not supported."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public attach(Landroid/content/Context;Landroid/os/Handler;)V
    .registers 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "handler"    # Landroid/os/Handler;

    .line 111
    iput-object p1, p0, Lnet/hockeyapp/android/tasks/LoginTask;->context:Landroid/content/Context;

    .line 112
    iput-object p2, p0, Lnet/hockeyapp/android/tasks/LoginTask;->handler:Landroid/os/Handler;

    .line 113
    return-void
.end method

.method public detach()V
    .registers 2

    .line 116
    const/4 v0, 0x0

    iput-object v0, p0, Lnet/hockeyapp/android/tasks/LoginTask;->context:Landroid/content/Context;

    .line 117
    const/4 v0, 0x0

    iput-object v0, p0, Lnet/hockeyapp/android/tasks/LoginTask;->handler:Landroid/os/Handler;

    .line 118
    const/4 v0, 0x0

    iput-object v0, p0, Lnet/hockeyapp/android/tasks/LoginTask;->progressDialog:Landroid/app/ProgressDialog;

    .line 119
    return-void
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;
    .registers 9
    .param p1, "args"    # [Ljava/lang/Void;

    .line 130
    invoke-static {}, Lnet/hockeyapp/android/utils/ConnectionManager;->getInstance()Lnet/hockeyapp/android/utils/ConnectionManager;

    move-result-object v0

    invoke-virtual {v0}, Lnet/hockeyapp/android/utils/ConnectionManager;->getHttpClient()Lorg/apache/http/client/HttpClient;

    move-result-object v2

    .line 133
    .local v2, "httpClient":Lorg/apache/http/client/HttpClient;
    :try_start_8
    iget v0, p0, Lnet/hockeyapp/android/tasks/LoginTask;->mode:I

    iget-object v1, p0, Lnet/hockeyapp/android/tasks/LoginTask;->params:Ljava/util/Map;

    invoke-direct {p0, v0, v1}, Lnet/hockeyapp/android/tasks/LoginTask;->makeRequest(ILjava/util/Map;)Lorg/apache/http/client/methods/HttpUriRequest;

    move-result-object v3

    .line 134
    .local v3, "httpUriRequest":Lorg/apache/http/client/methods/HttpUriRequest;
    invoke-interface {v2, v3}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v4

    .line 136
    .local v4, "response":Lorg/apache/http/HttpResponse;
    if-eqz v4, :cond_2d

    .line 137
    invoke-interface {v4}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v5

    .line 138
    .local v5, "resEntity":Lorg/apache/http/HttpEntity;
    invoke-static {v5}, Lorg/apache/http/util/EntityUtils;->toString(Lorg/apache/http/HttpEntity;)Ljava/lang/String;

    move-result-object v6

    .line 141
    .local v6, "responseStr":Ljava/lang/String;
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2d

    .line 142
    invoke-direct {p0, v6}, Lnet/hockeyapp/android/tasks/LoginTask;->handleResponse(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;
    :try_end_2b
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_8 .. :try_end_2b} :catch_2e
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_8 .. :try_end_2b} :catch_33
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_2b} :catch_38

    move-result-object v0

    return-object v0

    .line 154
    .end local v3    # "httpUriRequest":Lorg/apache/http/client/methods/HttpUriRequest;
    .end local v4    # "response":Lorg/apache/http/HttpResponse;
    .end local v5    # "resEntity":Lorg/apache/http/HttpEntity;
    .end local v6    # "responseStr":Ljava/lang/String;
    :cond_2d
    goto :goto_3c

    .line 146
    :catch_2e
    move-exception v3

    .line 147
    .local v3, "e":Ljava/io/UnsupportedEncodingException;
    invoke-virtual {v3}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    .line 154
    .end local v3    # "e":Ljava/io/UnsupportedEncodingException;
    goto :goto_3c

    .line 149
    :catch_33
    move-exception v3

    .line 150
    .local v3, "e":Lorg/apache/http/client/ClientProtocolException;
    invoke-virtual {v3}, Lorg/apache/http/client/ClientProtocolException;->printStackTrace()V

    .line 154
    .end local v3    # "e":Lorg/apache/http/client/ClientProtocolException;
    goto :goto_3c

    .line 152
    :catch_38
    move-exception v3

    .line 153
    .local v3, "e":Ljava/io/IOException;
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V

    .line 156
    .end local v3    # "e":Ljava/io/IOException;
    :goto_3c
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3

    .line 71
    move-object v0, p1

    check-cast v0, [Ljava/lang/Void;

    invoke-virtual {p0, v0}, Lnet/hockeyapp/android/tasks/LoginTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Ljava/lang/Boolean;)V
    .registers 6
    .param p1, "success"    # Ljava/lang/Boolean;

    .line 161
    iget-object v0, p0, Lnet/hockeyapp/android/tasks/LoginTask;->progressDialog:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_e

    .line 163
    :try_start_4
    iget-object v0, p0, Lnet/hockeyapp/android/tasks/LoginTask;->progressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_9} :catch_a

    .line 167
    goto :goto_e

    .line 165
    :catch_a
    move-exception v2

    .line 166
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    .line 171
    .end local v2    # "e":Ljava/lang/Exception;
    :cond_e
    :goto_e
    iget-object v0, p0, Lnet/hockeyapp/android/tasks/LoginTask;->handler:Landroid/os/Handler;

    if-eqz v0, :cond_2d

    .line 172
    new-instance v2, Landroid/os/Message;

    invoke-direct {v2}, Landroid/os/Message;-><init>()V

    .line 173
    .local v2, "msg":Landroid/os/Message;
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 174
    .local v3, "bundle":Landroid/os/Bundle;
    const-string v0, "success"

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v3, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 176
    invoke-virtual {v2, v3}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 177
    iget-object v0, p0, Lnet/hockeyapp/android/tasks/LoginTask;->handler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 179
    .end local v2    # "msg":Landroid/os/Message;
    .end local v3    # "bundle":Landroid/os/Bundle;
    :cond_2d
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .registers 3

    .line 71
    move-object v0, p1

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {p0, v0}, Lnet/hockeyapp/android/tasks/LoginTask;->onPostExecute(Ljava/lang/Boolean;)V

    return-void
.end method

.method protected onPreExecute()V
    .registers 6

    .line 123
    iget-object v0, p0, Lnet/hockeyapp/android/tasks/LoginTask;->progressDialog:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_c

    iget-object v0, p0, Lnet/hockeyapp/android/tasks/LoginTask;->progressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_1e

    :cond_c
    iget-boolean v0, p0, Lnet/hockeyapp/android/tasks/LoginTask;->showProgressDialog:Z

    if-eqz v0, :cond_1e

    .line 124
    iget-object v0, p0, Lnet/hockeyapp/android/tasks/LoginTask;->context:Landroid/content/Context;

    const-string v1, ""

    const-string v2, "Please wait..."

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-static {v0, v1, v2, v3, v4}, Landroid/app/ProgressDialog;->show(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ZZ)Landroid/app/ProgressDialog;

    move-result-object v0

    iput-object v0, p0, Lnet/hockeyapp/android/tasks/LoginTask;->progressDialog:Landroid/app/ProgressDialog;

    .line 126
    :cond_1e
    return-void
.end method

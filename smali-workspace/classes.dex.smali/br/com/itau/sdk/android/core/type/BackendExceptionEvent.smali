.class public Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent$VersionControl;,
        Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent$SimultaneousSession;,
        Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent$Session;,
        Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent$Token;,
        Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent$Router;,
        Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent$Unexpected;,
        Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent$Network;,
        Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent$HTTP;,
        Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent$Conversion;
    }
.end annotation


# instance fields
.field private final exception:Lbr/com/itau/sdk/android/core/exception/BackendException;


# direct methods
.method public constructor <init>(Lbr/com/itau/sdk/android/core/exception/BackendException;)V
    .registers 2

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    iput-object p1, p0, Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;->exception:Lbr/com/itau/sdk/android/core/exception/BackendException;

    .line 12
    return-void
.end method


# virtual methods
.method public getException()Lbr/com/itau/sdk/android/core/exception/BackendException;
    .registers 2

    .line 15
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;->exception:Lbr/com/itau/sdk/android/core/exception/BackendException;

    return-object v0
.end method

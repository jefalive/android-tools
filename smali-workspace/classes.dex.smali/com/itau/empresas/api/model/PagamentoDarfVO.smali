.class public Lcom/itau/empresas/api/model/PagamentoDarfVO;
.super Ljava/lang/Object;
.source "PagamentoDarfVO.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private cnpjEmpresa:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "cnpj_empresa"
    .end annotation
.end field

.field private codigoOperador:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "codigo_operador"
    .end annotation
.end field

.field private codigoReceita:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "codigo_receita"
    .end annotation
.end field

.field private cpfContribuinte:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "cpf_contibuinte"
    .end annotation
.end field

.field private dataApuracao:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "data_apauracao"
    .end annotation
.end field

.field private dataPagamento:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "data_pagamento"
    .end annotation
.end field

.field private dataVencimento:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "data_vencimento"
    .end annotation
.end field

.field private duplicidade:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "duplicidade"
    .end annotation
.end field

.field private idConta:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "id_conta"
    .end annotation
.end field

.field private informativoPagador:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "informativo_pagador"
    .end annotation
.end field

.field private nomeContribuinte:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "nome_contibuinte"
    .end annotation
.end field

.field private nomeEmpresa:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "nome_empresa"
    .end annotation
.end field

.field private numeroReferencia:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "numero_referencia"
    .end annotation
.end field

.field private percentual:F
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "percentual"
    .end annotation
.end field

.field private referenciaEmpresa:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "referencia_empresa"
    .end annotation
.end field

.field private simples:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "simples"
    .end annotation
.end field

.field private valorJuros:D
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "valor_juros"
    .end annotation
.end field

.field private valorMultas:D
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "valor_multas"
    .end annotation
.end field

.field private valorPrincipal:D
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "valor_principal"
    .end annotation
.end field

.field private valorReceita:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "valor_receita"
    .end annotation
.end field

.field private valorTotal:D
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "valorTotal"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

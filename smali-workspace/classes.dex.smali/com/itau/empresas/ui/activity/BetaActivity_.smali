.class public final Lcom/itau/empresas/ui/activity/BetaActivity_;
.super Lcom/itau/empresas/ui/activity/BetaActivity;
.source "BetaActivity_.java"

# interfaces
.implements Lorg/androidannotations/api/view/HasViews;
.implements Lorg/androidannotations/api/view/OnViewChangedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/itau/empresas/ui/activity/BetaActivity_$IntentBuilder_;
    }
.end annotation


# instance fields
.field private final onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;


# direct methods
.method public constructor <init>()V
    .registers 2

    .line 32
    invoke-direct {p0}, Lcom/itau/empresas/ui/activity/BetaActivity;-><init>()V

    .line 36
    new-instance v0, Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-direct {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/ui/activity/BetaActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    return-void
.end method

.method static synthetic access$001(Lcom/itau/empresas/ui/activity/BetaActivity_;)V
    .registers 1
    .param p0, "x0"    # Lcom/itau/empresas/ui/activity/BetaActivity_;

    .line 32
    invoke-super {p0}, Lcom/itau/empresas/ui/activity/BetaActivity;->mostraDialogoDeProgresso()V

    return-void
.end method

.method static synthetic access$101(Lcom/itau/empresas/ui/activity/BetaActivity_;)V
    .registers 1
    .param p0, "x0"    # Lcom/itau/empresas/ui/activity/BetaActivity_;

    .line 32
    invoke-super {p0}, Lcom/itau/empresas/ui/activity/BetaActivity;->escondeDialogoDeProgresso()V

    return-void
.end method

.method private init_(Landroid/os/Bundle;)V
    .registers 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 49
    new-instance v0, Lcom/itau/empresas/Preferences_;

    invoke-direct {v0, p0}, Lcom/itau/empresas/Preferences_;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/itau/empresas/ui/activity/BetaActivity_;->prefs:Lcom/itau/empresas/Preferences_;

    .line 50
    invoke-static {p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->registerOnViewChangedListener(Lorg/androidannotations/api/view/OnViewChangedListener;)V

    .line 51
    invoke-static {}, Lcom/itau/empresas/CustomApplication_;->getInstance()Lcom/itau/empresas/CustomApplication;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/ui/activity/BetaActivity_;->application:Lcom/itau/empresas/CustomApplication;

    .line 52
    invoke-static {}, Lcom/itau/empresas/CustomApplication_;->getInstance()Lcom/itau/empresas/CustomApplication;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/ui/activity/BetaActivity_;->application:Lcom/itau/empresas/CustomApplication;

    .line 53
    invoke-direct {p0}, Lcom/itau/empresas/ui/activity/BetaActivity_;->injectExtras_()V

    .line 54
    invoke-virtual {p0}, Lcom/itau/empresas/ui/activity/BetaActivity_;->afterInject()V

    .line 55
    return-void
.end method

.method private injectExtras_()V
    .registers 3

    .line 127
    invoke-virtual {p0}, Lcom/itau/empresas/ui/activity/BetaActivity_;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 128
    .local v1, "extras_":Landroid/os/Bundle;
    if-eqz v1, :cond_1a

    .line 129
    const-string v0, "isLogado"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 130
    const-string v0, "isLogado"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/itau/empresas/ui/activity/BetaActivity_;->isLogado:Z

    .line 133
    :cond_1a
    return-void
.end method


# virtual methods
.method public escondeDialogoDeProgresso()V
    .registers 5

    .line 158
    const-string v0, ""

    new-instance v1, Lcom/itau/empresas/ui/activity/BetaActivity_$5;

    invoke-direct {v1, p0}, Lcom/itau/empresas/ui/activity/BetaActivity_$5;-><init>(Lcom/itau/empresas/ui/activity/BetaActivity_;)V

    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3}, Lorg/androidannotations/api/UiThreadExecutor;->runTask(Ljava/lang/String;Ljava/lang/Runnable;J)V

    .line 166
    return-void
.end method

.method public mostraDialogoDeProgresso()V
    .registers 5
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "CommitTransaction"
        }
    .end annotation

    .line 146
    const-string v0, ""

    new-instance v1, Lcom/itau/empresas/ui/activity/BetaActivity_$4;

    invoke-direct {v1, p0}, Lcom/itau/empresas/ui/activity/BetaActivity_$4;-><init>(Lcom/itau/empresas/ui/activity/BetaActivity_;)V

    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3}, Lorg/androidannotations/api/UiThreadExecutor;->runTask(Ljava/lang/String;Ljava/lang/Runnable;J)V

    .line 154
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 41
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/BetaActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-static {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    move-result-object v1

    .line 42
    .local v1, "previousNotifier":Lorg/androidannotations/api/view/OnViewChangedNotifier;
    invoke-direct {p0, p1}, Lcom/itau/empresas/ui/activity/BetaActivity_;->init_(Landroid/os/Bundle;)V

    .line 43
    invoke-super {p0, p1}, Lcom/itau/empresas/ui/activity/BetaActivity;->onCreate(Landroid/os/Bundle;)V

    .line 44
    invoke-static {v1}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 45
    const v0, 0x7f030023

    invoke-virtual {p0, v0}, Lcom/itau/empresas/ui/activity/BetaActivity_;->setContentView(I)V

    .line 46
    return-void
.end method

.method public onViewChanged(Lorg/androidannotations/api/view/HasViews;)V
    .registers 6
    .param p1, "hasViews"    # Lorg/androidannotations/api/view/HasViews;

    .line 89
    const v0, 0x7f0e00e2

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/itau/empresas/ui/activity/BetaActivity_;->botaoFaleSobreApp:Landroid/widget/Button;

    .line 90
    const v0, 0x7f0e00e1

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 91
    .local v2, "view_botao_entrar_beta":Landroid/view/View;
    const v0, 0x7f0e00e3

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 93
    .local v3, "view_botao_sair_beta":Landroid/view/View;
    if-eqz v2, :cond_23

    .line 94
    new-instance v0, Lcom/itau/empresas/ui/activity/BetaActivity_$1;

    invoke-direct {v0, p0}, Lcom/itau/empresas/ui/activity/BetaActivity_$1;-><init>(Lcom/itau/empresas/ui/activity/BetaActivity_;)V

    invoke-virtual {v2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 103
    :cond_23
    if-eqz v3, :cond_2d

    .line 104
    new-instance v0, Lcom/itau/empresas/ui/activity/BetaActivity_$2;

    invoke-direct {v0, p0}, Lcom/itau/empresas/ui/activity/BetaActivity_$2;-><init>(Lcom/itau/empresas/ui/activity/BetaActivity_;)V

    invoke-virtual {v3, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 113
    :cond_2d
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/BetaActivity_;->botaoFaleSobreApp:Landroid/widget/Button;

    if-eqz v0, :cond_3b

    .line 114
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/BetaActivity_;->botaoFaleSobreApp:Landroid/widget/Button;

    new-instance v1, Lcom/itau/empresas/ui/activity/BetaActivity_$3;

    invoke-direct {v1, p0}, Lcom/itau/empresas/ui/activity/BetaActivity_$3;-><init>(Lcom/itau/empresas/ui/activity/BetaActivity_;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 123
    :cond_3b
    invoke-virtual {p0}, Lcom/itau/empresas/ui/activity/BetaActivity_;->validarElementos()V

    .line 124
    return-void
.end method

.method public setContentView(I)V
    .registers 3
    .param p1, "layoutResID"    # I

    .line 59
    invoke-super {p0, p1}, Lcom/itau/empresas/ui/activity/BetaActivity;->setContentView(I)V

    .line 60
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/BetaActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 61
    return-void
.end method

.method public setContentView(Landroid/view/View;)V
    .registers 3
    .param p1, "view"    # Landroid/view/View;

    .line 71
    invoke-super {p0, p1}, Lcom/itau/empresas/ui/activity/BetaActivity;->setContentView(Landroid/view/View;)V

    .line 72
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/BetaActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 73
    return-void
.end method

.method public setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .registers 4
    .param p1, "view"    # Landroid/view/View;
    .param p2, "params"    # Landroid/view/ViewGroup$LayoutParams;

    .line 65
    invoke-super {p0, p1, p2}, Lcom/itau/empresas/ui/activity/BetaActivity;->setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 66
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/BetaActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 67
    return-void
.end method

.method public setIntent(Landroid/content/Intent;)V
    .registers 2
    .param p1, "newIntent"    # Landroid/content/Intent;

    .line 137
    invoke-super {p0, p1}, Lcom/itau/empresas/ui/activity/BetaActivity;->setIntent(Landroid/content/Intent;)V

    .line 138
    invoke-direct {p0}, Lcom/itau/empresas/ui/activity/BetaActivity_;->injectExtras_()V

    .line 139
    return-void
.end method

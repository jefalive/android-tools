.class final Lcom/itau/empresas/ui/util/AnimationUtils$6;
.super Landroid/view/animation/Animation;
.source "AnimationUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/itau/empresas/ui/util/AnimationUtils;->expandAnimation(Landroid/view/View;ILandroid/view/animation/Animation$AnimationListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$targetHeight:I

.field final synthetic val$view:Landroid/view/View;


# direct methods
.method constructor <init>(Landroid/view/View;I)V
    .registers 3

    .line 142
    iput-object p1, p0, Lcom/itau/empresas/ui/util/AnimationUtils$6;->val$view:Landroid/view/View;

    iput p2, p0, Lcom/itau/empresas/ui/util/AnimationUtils$6;->val$targetHeight:I

    invoke-direct {p0}, Landroid/view/animation/Animation;-><init>()V

    return-void
.end method


# virtual methods
.method protected applyTransformation(FLandroid/view/animation/Transformation;)V
    .registers 5
    .param p1, "interpolatedTime"    # F
    .param p2, "t"    # Landroid/view/animation/Transformation;

    .line 145
    iget-object v0, p0, Lcom/itau/empresas/ui/util/AnimationUtils$6;->val$view:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    cmpl-float v1, p1, v1

    if-nez v1, :cond_e

    const/4 v1, -0x2

    goto :goto_13

    :cond_e
    iget v1, p0, Lcom/itau/empresas/ui/util/AnimationUtils$6;->val$targetHeight:I

    int-to-float v1, v1

    mul-float/2addr v1, p1

    float-to-int v1, v1

    :goto_13
    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 148
    iget-object v0, p0, Lcom/itau/empresas/ui/util/AnimationUtils$6;->val$view:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->requestLayout()V

    .line 149
    return-void
.end method

.method public willChangeBounds()Z
    .registers 2

    .line 153
    const/4 v0, 0x1

    return v0
.end method

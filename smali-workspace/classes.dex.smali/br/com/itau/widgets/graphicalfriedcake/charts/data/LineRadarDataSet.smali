.class public abstract Lbr/com/itau/widgets/graphicalfriedcake/charts/data/LineRadarDataSet;
.super Lbr/com/itau/widgets/graphicalfriedcake/charts/data/LineScatterCandleRadarDataSet;
.source "LineRadarDataSet.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;>Lbr/com/itau/widgets/graphicalfriedcake/charts/data/LineScatterCandleRadarDataSet<TT;>;"
    }
.end annotation


# instance fields
.field private mDrawFilled:Z

.field private mFillAlpha:I

.field private mFillColor:I

.field private mLineWidth:F


# direct methods
.method public constructor <init>(Ljava/util/List;Ljava/lang/String;)V
    .registers 6
    .param p1, "yVals"    # Ljava/util/List;
    .param p2, "label"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<TT;>;Ljava/lang/String;)V"
        }
    .end annotation

    .line 37
    invoke-direct {p0, p1, p2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/LineScatterCandleRadarDataSet;-><init>(Ljava/util/List;Ljava/lang/String;)V

    .line 20
    const/16 v0, 0x8c

    const/16 v1, 0xea

    const/16 v2, 0xff

    invoke-static {v0, v1, v2}, Landroid/graphics/Color;->rgb(III)I

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/LineRadarDataSet;->mFillColor:I

    .line 24
    const/16 v0, 0x55

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/LineRadarDataSet;->mFillAlpha:I

    .line 28
    const/high16 v0, 0x40200000    # 2.5f

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/LineRadarDataSet;->mLineWidth:F

    .line 32
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/LineRadarDataSet;->mDrawFilled:Z

    .line 38
    return-void
.end method


# virtual methods
.method public getFillAlpha()I
    .registers 2

    .line 68
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/LineRadarDataSet;->mFillAlpha:I

    return v0
.end method

.method public getFillColor()I
    .registers 2

    .line 47
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/LineRadarDataSet;->mFillColor:I

    return v0
.end method

.method public getLineWidth()F
    .registers 2

    .line 106
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/LineRadarDataSet;->mLineWidth:F

    return v0
.end method

.method public isDrawFilledEnabled()Z
    .registers 2

    .line 128
    iget-boolean v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/LineRadarDataSet;->mDrawFilled:Z

    return v0
.end method

.method public setDrawFilled(Z)V
    .registers 2
    .param p1, "filled"    # Z

    .line 118
    iput-boolean p1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/LineRadarDataSet;->mDrawFilled:Z

    .line 119
    return-void
.end method

.method public setFillAlpha(I)V
    .registers 2
    .param p1, "alpha"    # I

    .line 79
    iput p1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/LineRadarDataSet;->mFillAlpha:I

    .line 80
    return-void
.end method

.method public setFillColor(I)V
    .registers 2
    .param p1, "color"    # I

    .line 57
    iput p1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/LineRadarDataSet;->mFillColor:I

    .line 58
    return-void
.end method

.method public setLineWidth(F)V
    .registers 3
    .param p1, "width"    # F

    .line 92
    const v0, 0x3e4ccccd    # 0.2f

    cmpg-float v0, p1, v0

    if-gez v0, :cond_a

    .line 93
    const p1, 0x3e4ccccd    # 0.2f

    .line 94
    :cond_a
    const/high16 v0, 0x41200000    # 10.0f

    cmpl-float v0, p1, v0

    if-lez v0, :cond_12

    .line 95
    const/high16 p1, 0x41200000    # 10.0f

    .line 96
    :cond_12
    invoke-static {p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Utils;->convertDpToPixel(F)F

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/LineRadarDataSet;->mLineWidth:F

    .line 97
    return-void
.end method

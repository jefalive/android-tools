.class Lcom/itau/empresas/feature/home/adapter/HomeAdapter$ViewHolderCardSimples;
.super Landroid/support/v7/widget/RecyclerView$ViewHolder;
.source "HomeAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/itau/empresas/feature/home/adapter/HomeAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "ViewHolderCardSimples"
.end annotation


# instance fields
.field final parentView:Lcom/itau/empresas/feature/home/view/ICardSimplesView;


# direct methods
.method private constructor <init>(Lcom/itau/empresas/feature/home/view/ICardSimplesView;)V
    .registers 3
    .param p1, "itemView"    # Lcom/itau/empresas/feature/home/view/ICardSimplesView;

    .line 82
    invoke-direct {p0, p1}, Landroid/support/v7/widget/RecyclerView$ViewHolder;-><init>(Landroid/view/View;)V

    .line 83
    iput-object p1, p0, Lcom/itau/empresas/feature/home/adapter/HomeAdapter$ViewHolderCardSimples;->parentView:Lcom/itau/empresas/feature/home/view/ICardSimplesView;

    .line 84
    iget-object v0, p0, Lcom/itau/empresas/feature/home/adapter/HomeAdapter$ViewHolderCardSimples;->parentView:Lcom/itau/empresas/feature/home/view/ICardSimplesView;

    invoke-virtual {v0, p0}, Lcom/itau/empresas/feature/home/view/ICardSimplesView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 85
    return-void
.end method

.method synthetic constructor <init>(Lcom/itau/empresas/feature/home/view/ICardSimplesView;Lcom/itau/empresas/feature/home/adapter/HomeAdapter$1;)V
    .registers 3
    .param p1, "x0"    # Lcom/itau/empresas/feature/home/view/ICardSimplesView;
    .param p2, "x1"    # Lcom/itau/empresas/feature/home/adapter/HomeAdapter$1;

    .line 77
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/home/adapter/HomeAdapter$ViewHolderCardSimples;-><init>(Lcom/itau/empresas/feature/home/view/ICardSimplesView;)V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .registers 4
    .param p1, "view"    # Landroid/view/View;

    .line 89
    invoke-static {}, Lcom/itau/empresas/feature/feedback/FeedbackDialog_;->builder()Lcom/itau/empresas/feature/feedback/FeedbackDialog_$FragmentBuilder_;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/feedback/FeedbackDialog_$FragmentBuilder_;->feedbackEspontaneo(Ljava/lang/Boolean;)Lcom/itau/empresas/feature/feedback/FeedbackDialog_$FragmentBuilder_;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/feature/feedback/FeedbackDialog_$FragmentBuilder_;->build()Lcom/itau/empresas/feature/feedback/FeedbackDialog;

    move-result-object v0

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    check-cast v1, Lcom/itau/empresas/ui/activity/BaseActivity;

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/feedback/FeedbackDialog;->show(Landroid/support/v7/app/AppCompatActivity;)Lcom/itau/empresas/ui/dialog/BaseDialog;

    .line 90
    return-void
.end method

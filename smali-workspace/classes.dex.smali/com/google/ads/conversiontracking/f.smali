.class public Lcom/google/ads/conversiontracking/f;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/ads/conversiontracking/f$a;
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Lcom/google/ads/conversiontracking/f$a;

.field private final c:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .registers 5

    .line 44
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "CREATE TABLE IF NOT EXISTS %s ( %s INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, %s TEXT NOT NULL, %s TEXT, %s INTEGER, %s INTEGER, %s TEXT, %s INTEGER, %s INTEGER,%s INTEGER);"

    const/16 v2, 0xa

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "conversiontracking"

    const/4 v4, 0x0

    aput-object v3, v2, v4

    const-string v3, "conversion_ping_id"

    const/4 v4, 0x1

    aput-object v3, v2, v4

    const-string v3, "string_url"

    const/4 v4, 0x2

    aput-object v3, v2, v4

    const-string v3, "preference_key"

    const/4 v4, 0x3

    aput-object v3, v2, v4

    const-string v3, "is_repeatable"

    const/4 v4, 0x4

    aput-object v3, v2, v4

    const-string v3, "parameter_is_null"

    const/4 v4, 0x5

    aput-object v3, v2, v4

    const-string v3, "preference_name"

    const/4 v4, 0x6

    aput-object v3, v2, v4

    const-string v3, "record_time"

    const/4 v4, 0x7

    aput-object v3, v2, v4

    const-string v3, "retry_count"

    const/16 v4, 0x8

    aput-object v3, v2, v4

    const-string v3, "last_retry_time"

    const/16 v4, 0x9

    aput-object v3, v2, v4

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/ads/conversiontracking/f;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 4

    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 68
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/ads/conversiontracking/f;->c:Ljava/lang/Object;

    .line 71
    new-instance v0, Lcom/google/ads/conversiontracking/f$a;

    const-string v1, "google_conversion_tracking.db"

    invoke-direct {v0, p0, p1, v1}, Lcom/google/ads/conversiontracking/f$a;-><init>(Lcom/google/ads/conversiontracking/f;Landroid/content/Context;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/ads/conversiontracking/f;->b:Lcom/google/ads/conversiontracking/f$a;

    .line 72
    return-void
.end method

.method static synthetic e()Ljava/lang/String;
    .registers 1

    .line 19
    sget-object v0, Lcom/google/ads/conversiontracking/f;->a:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public a()Landroid/database/sqlite/SQLiteDatabase;
    .registers 5

    .line 100
    const/4 v2, 0x0

    .line 102
    :try_start_1
    iget-object v0, p0, Lcom/google/ads/conversiontracking/f;->b:Lcom/google/ads/conversiontracking/f$a;

    invoke-virtual {v0}, Lcom/google/ads/conversiontracking/f$a;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;
    :try_end_6
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1 .. :try_end_6} :catch_8

    move-result-object v2

    .line 106
    goto :goto_12

    .line 103
    :catch_8
    move-exception v3

    .line 104
    const-string v0, "GoogleConversionReporter"

    const-string v1, "Error opening writable conversion tracking database"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 105
    const/4 v0, 0x0

    return-object v0

    .line 107
    :goto_12
    return-object v2
.end method

.method public a(Landroid/database/Cursor;)Lcom/google/ads/conversiontracking/d;
    .registers 15

    .line 302
    if-nez p1, :cond_4

    .line 303
    const/4 v0, 0x0

    return-object v0

    .line 310
    :cond_4
    const/4 v0, 0x7

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v11

    .line 311
    const/4 v0, 0x1

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 312
    if-lez v11, :cond_2a

    .line 313
    invoke-static {v12}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 314
    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "retry"

    .line 315
    invoke-static {v11}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    .line 316
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    .line 317
    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v12

    .line 320
    :cond_2a
    new-instance v0, Lcom/google/ads/conversiontracking/d;

    .line 321
    const/4 v1, 0x0

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    .line 323
    const/4 v3, 0x2

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 324
    const/4 v3, 0x3

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    if-lez v3, :cond_3f

    const/4 v5, 0x1

    goto :goto_40

    :cond_3f
    const/4 v5, 0x0

    .line 325
    :goto_40
    const/4 v3, 0x4

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    if-lez v3, :cond_49

    const/4 v6, 0x1

    goto :goto_4a

    :cond_49
    const/4 v6, 0x0

    .line 326
    :goto_4a
    const/4 v3, 0x5

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 327
    const/4 v3, 0x6

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    move-object v3, v12

    move v10, v11

    invoke-direct/range {v0 .. v10}, Lcom/google/ads/conversiontracking/d;-><init>(JLjava/lang/String;Ljava/lang/String;ZZLjava/lang/String;JI)V

    return-object v0
.end method

.method public a(J)Ljava/util/List;
    .registers 19
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)Ljava/util/List<Lcom/google/ads/conversiontracking/d;>;"
        }
    .end annotation

    .line 115
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/ads/conversiontracking/f;->c:Ljava/lang/Object;

    monitor-enter v9

    .line 116
    :try_start_5
    new-instance v10, Ljava/util/LinkedList;

    invoke-direct {v10}, Ljava/util/LinkedList;-><init>()V
    :try_end_a
    .catchall {:try_start_5 .. :try_end_a} :catchall_79

    .line 117
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-gtz v0, :cond_12

    .line 118
    monitor-exit v9

    return-object v10

    .line 120
    :cond_12
    :try_start_12
    invoke-virtual/range {p0 .. p0}, Lcom/google/ads/conversiontracking/f;->a()Landroid/database/sqlite/SQLiteDatabase;
    :try_end_15
    .catchall {:try_start_12 .. :try_end_15} :catchall_79

    move-result-object v11

    .line 121
    if-nez v11, :cond_1a

    .line 122
    monitor-exit v9

    return-object v10

    .line 124
    :cond_1a
    const/4 v12, 0x0

    .line 126
    const-string v13, "last_retry_time ASC"

    .line 127
    move-object v0, v11

    const-string v1, "conversiontracking"

    move-object v7, v13

    .line 134
    :try_start_21
    invoke-static/range {p1 .. p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v8

    .line 127
    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    move-object v12, v0

    .line 135
    invoke-interface {v12}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_44

    .line 137
    :cond_35
    move-object/from16 v0, p0

    invoke-virtual {v0, v12}, Lcom/google/ads/conversiontracking/f;->a(Landroid/database/Cursor;)Lcom/google/ads/conversiontracking/d;

    move-result-object v0

    invoke-interface {v10, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 138
    invoke-interface {v12}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_41
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_21 .. :try_end_41} :catch_4a
    .catchall {:try_start_21 .. :try_end_41} :catchall_70

    move-result v0

    if-nez v0, :cond_35

    .line 143
    :cond_44
    if-eqz v12, :cond_77

    .line 144
    :try_start_46
    invoke-interface {v12}, Landroid/database/Cursor;->close()V
    :try_end_49
    .catchall {:try_start_46 .. :try_end_49} :catchall_79

    goto :goto_77

    .line 140
    :catch_4a
    move-exception v13

    .line 141
    const-string v0, "GoogleConversionReporter"

    const-string v2, "Error extracing ping Info: "

    :try_start_4f
    invoke-virtual {v13}, Landroid/database/sqlite/SQLiteException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_62

    invoke-virtual {v2, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto :goto_67

    :cond_62
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    :goto_67
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_6a
    .catchall {:try_start_4f .. :try_end_6a} :catchall_70

    .line 143
    if-eqz v12, :cond_77

    .line 144
    :try_start_6c
    invoke-interface {v12}, Landroid/database/Cursor;->close()V
    :try_end_6f
    .catchall {:try_start_6c .. :try_end_6f} :catchall_79

    goto :goto_77

    .line 143
    :catchall_70
    move-exception v14

    if-eqz v12, :cond_76

    .line 144
    :try_start_73
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    :cond_76
    throw v14
    :try_end_77
    .catchall {:try_start_73 .. :try_end_77} :catchall_79

    .line 147
    :cond_77
    :goto_77
    monitor-exit v9

    return-object v10

    .line 148
    :catchall_79
    move-exception v15

    monitor-exit v9

    throw v15
.end method

.method public a(Lcom/google/ads/conversiontracking/d;)V
    .registers 11

    .line 79
    if-nez p1, :cond_3

    .line 80
    return-void

    .line 82
    :cond_3
    iget-object v5, p0, Lcom/google/ads/conversiontracking/f;->c:Ljava/lang/Object;

    monitor-enter v5

    .line 83
    :try_start_6
    invoke-virtual {p0}, Lcom/google/ads/conversiontracking/f;->a()Landroid/database/sqlite/SQLiteDatabase;
    :try_end_9
    .catchall {:try_start_6 .. :try_end_9} :catchall_2f

    move-result-object v6

    .line 84
    if-nez v6, :cond_e

    .line 85
    monitor-exit v5

    return-void

    .line 87
    :cond_e
    :try_start_e
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "%s = %d"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "conversion_ping_id"

    const/4 v4, 0x0

    aput-object v3, v2, v4

    iget-wide v3, p1, Lcom/google/ads/conversiontracking/d;->h:J

    .line 91
    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    const/4 v4, 0x1

    aput-object v3, v2, v4

    .line 87
    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    .line 92
    const-string v0, "conversiontracking"

    const/4 v1, 0x0

    invoke-virtual {v6, v0, v7, v1}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_2d
    .catchall {:try_start_e .. :try_end_2d} :catchall_2f

    .line 93
    monitor-exit v5

    goto :goto_32

    :catchall_2f
    move-exception v8

    monitor-exit v5

    throw v8

    .line 94
    :goto_32
    return-void
.end method

.method public b()V
    .registers 12

    .line 189
    iget-object v7, p0, Lcom/google/ads/conversiontracking/f;->c:Ljava/lang/Object;

    monitor-enter v7

    .line 190
    :try_start_3
    invoke-virtual {p0}, Lcom/google/ads/conversiontracking/f;->a()Landroid/database/sqlite/SQLiteDatabase;
    :try_end_6
    .catchall {:try_start_3 .. :try_end_6} :catchall_45

    move-result-object v8

    .line 191
    if-nez v8, :cond_b

    .line 192
    monitor-exit v7

    return-void

    .line 194
    :cond_b
    :try_start_b
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "(%s > %d) or (%s < %d and %s > 0)"

    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "retry_count"

    const/4 v4, 0x0

    aput-object v3, v2, v4

    .line 198
    const-wide/16 v3, 0x2328

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    const/4 v4, 0x1

    aput-object v3, v2, v4

    const-string v3, "record_time"

    const/4 v4, 0x2

    aput-object v3, v2, v4

    .line 200
    invoke-static {}, Lcom/google/ads/conversiontracking/g;->a()J

    move-result-wide v3

    const-wide/32 v5, 0x2932e00

    sub-long/2addr v3, v5

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    const/4 v4, 0x3

    aput-object v3, v2, v4

    const-string v3, "retry_count"

    const/4 v4, 0x4

    aput-object v3, v2, v4

    .line 194
    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    .line 202
    const-string v0, "conversiontracking"

    const/4 v1, 0x0

    invoke-virtual {v8, v0, v9, v1}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_43
    .catchall {:try_start_b .. :try_end_43} :catchall_45

    .line 203
    monitor-exit v7

    goto :goto_48

    :catchall_45
    move-exception v10

    monitor-exit v7

    throw v10

    .line 204
    :goto_48
    return-void
.end method

.method public b(Lcom/google/ads/conversiontracking/d;)V
    .registers 10

    .line 157
    if-nez p1, :cond_3

    .line 158
    return-void

    .line 160
    :cond_3
    iget-object v4, p0, Lcom/google/ads/conversiontracking/f;->c:Ljava/lang/Object;

    monitor-enter v4

    .line 161
    :try_start_6
    invoke-virtual {p0}, Lcom/google/ads/conversiontracking/f;->a()Landroid/database/sqlite/SQLiteDatabase;
    :try_end_9
    .catchall {:try_start_6 .. :try_end_9} :catchall_84

    move-result-object v5

    .line 162
    if-nez v5, :cond_e

    .line 163
    monitor-exit v4

    return-void

    .line 165
    :cond_e
    :try_start_e
    new-instance v6, Landroid/content/ContentValues;

    invoke-direct {v6}, Landroid/content/ContentValues;-><init>()V

    .line 166
    const-string v0, "string_url"

    iget-object v1, p1, Lcom/google/ads/conversiontracking/d;->g:Ljava/lang/String;

    invoke-virtual {v6, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 167
    const-string v0, "preference_key"

    iget-object v1, p1, Lcom/google/ads/conversiontracking/d;->f:Ljava/lang/String;

    invoke-virtual {v6, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 168
    const-string v0, "is_repeatable"

    iget-boolean v1, p1, Lcom/google/ads/conversiontracking/d;->b:Z

    if-eqz v1, :cond_29

    const/4 v1, 0x1

    goto :goto_2a

    :cond_29
    const/4 v1, 0x0

    :goto_2a
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v6, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 169
    const-string v0, "parameter_is_null"

    iget-boolean v1, p1, Lcom/google/ads/conversiontracking/d;->a:Z

    if-eqz v1, :cond_39

    const/4 v1, 0x1

    goto :goto_3a

    :cond_39
    const/4 v1, 0x0

    :goto_3a
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v6, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 170
    const-string v0, "preference_name"

    iget-object v1, p1, Lcom/google/ads/conversiontracking/d;->e:Ljava/lang/String;

    invoke-virtual {v6, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 171
    const-string v0, "record_time"

    iget-wide v1, p1, Lcom/google/ads/conversiontracking/d;->d:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v6, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 172
    const-string v0, "retry_count"

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v6, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 173
    const-string v0, "last_retry_time"

    iget-wide v1, p1, Lcom/google/ads/conversiontracking/d;->d:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v6, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 174
    const-string v0, "conversiontracking"

    const/4 v1, 0x0

    invoke-virtual {v5, v0, v1, v6}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v0

    iput-wide v0, p1, Lcom/google/ads/conversiontracking/d;->h:J

    .line 175
    invoke-virtual {p0}, Lcom/google/ads/conversiontracking/f;->b()V

    .line 176
    invoke-virtual {p0}, Lcom/google/ads/conversiontracking/f;->c()I

    move-result v0

    int-to-long v0, v0

    const-wide/16 v2, 0x4e20

    cmp-long v0, v0, v2

    if-lez v0, :cond_82

    .line 177
    invoke-virtual {p0}, Lcom/google/ads/conversiontracking/f;->d()V
    :try_end_82
    .catchall {:try_start_e .. :try_end_82} :catchall_84

    .line 179
    :cond_82
    monitor-exit v4

    goto :goto_87

    :catchall_84
    move-exception v7

    monitor-exit v4

    throw v7

    .line 180
    :goto_87
    return-void
.end method

.method public c()I
    .registers 11

    .line 246
    iget-object v4, p0, Lcom/google/ads/conversiontracking/f;->c:Ljava/lang/Object;

    monitor-enter v4

    .line 247
    :try_start_3
    invoke-virtual {p0}, Lcom/google/ads/conversiontracking/f;->a()Landroid/database/sqlite/SQLiteDatabase;
    :try_end_6
    .catchall {:try_start_3 .. :try_end_6} :catchall_5d

    move-result-object v5

    .line 248
    if-nez v5, :cond_c

    .line 249
    monitor-exit v4

    const/4 v0, 0x0

    return v0

    .line 251
    :cond_c
    const/4 v6, 0x0

    .line 253
    const-string v0, "select count(*) from conversiontracking"

    const/4 v1, 0x0

    :try_start_10
    invoke-virtual {v5, v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    move-object v6, v0

    .line 254
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_27

    .line 255
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_1f
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_10 .. :try_end_1f} :catch_2d
    .catchall {:try_start_10 .. :try_end_1f} :catchall_53

    move-result v7

    .line 260
    if-eqz v6, :cond_25

    .line 261
    :try_start_22
    invoke-interface {v6}, Landroid/database/Cursor;->close()V
    :try_end_25
    .catchall {:try_start_22 .. :try_end_25} :catchall_5d

    :cond_25
    monitor-exit v4

    return v7

    .line 260
    :cond_27
    if-eqz v6, :cond_5a

    .line 261
    :try_start_29
    invoke-interface {v6}, Landroid/database/Cursor;->close()V
    :try_end_2c
    .catchall {:try_start_29 .. :try_end_2c} :catchall_5d

    goto :goto_5a

    .line 257
    :catch_2d
    move-exception v7

    .line 258
    const-string v0, "GoogleConversionReporter"

    const-string v2, "Error getting record count"

    :try_start_32
    invoke-virtual {v7}, Landroid/database/sqlite/SQLiteException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_45

    invoke-virtual {v2, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto :goto_4a

    :cond_45
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    :goto_4a
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4d
    .catchall {:try_start_32 .. :try_end_4d} :catchall_53

    .line 260
    if-eqz v6, :cond_5a

    .line 261
    :try_start_4f
    invoke-interface {v6}, Landroid/database/Cursor;->close()V
    :try_end_52
    .catchall {:try_start_4f .. :try_end_52} :catchall_5d

    goto :goto_5a

    .line 260
    :catchall_53
    move-exception v8

    if-eqz v6, :cond_59

    .line 261
    :try_start_56
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_59
    throw v8
    :try_end_5a
    .catchall {:try_start_56 .. :try_end_5a} :catchall_5d

    .line 264
    :cond_5a
    :goto_5a
    monitor-exit v4

    const/4 v0, 0x0

    return v0

    .line 265
    :catchall_5d
    move-exception v9

    monitor-exit v4

    throw v9
.end method

.method public c(Lcom/google/ads/conversiontracking/d;)V
    .registers 12

    .line 225
    if-nez p1, :cond_3

    .line 226
    return-void

    .line 228
    :cond_3
    iget-object v5, p0, Lcom/google/ads/conversiontracking/f;->c:Ljava/lang/Object;

    monitor-enter v5

    .line 229
    :try_start_6
    invoke-virtual {p0}, Lcom/google/ads/conversiontracking/f;->a()Landroid/database/sqlite/SQLiteDatabase;
    :try_end_9
    .catchall {:try_start_6 .. :try_end_9} :catchall_51

    move-result-object v6

    .line 230
    if-nez v6, :cond_e

    .line 231
    monitor-exit v5

    return-void

    .line 233
    :cond_e
    :try_start_e
    new-instance v7, Landroid/content/ContentValues;

    invoke-direct {v7}, Landroid/content/ContentValues;-><init>()V

    .line 234
    const-string v0, "last_retry_time"

    invoke-static {}, Lcom/google/ads/conversiontracking/g;->a()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v7, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 235
    const-string v0, "retry_count"

    iget v1, p1, Lcom/google/ads/conversiontracking/d;->c:I

    add-int/lit8 v1, v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v7, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 236
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "%s = %d"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "conversion_ping_id"

    const/4 v4, 0x0

    aput-object v3, v2, v4

    iget-wide v3, p1, Lcom/google/ads/conversiontracking/d;->h:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    const/4 v4, 0x1

    aput-object v3, v2, v4

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    .line 237
    const-string v0, "conversiontracking"

    const/4 v1, 0x0

    invoke-virtual {v6, v0, v7, v8, v1}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 238
    invoke-virtual {p0}, Lcom/google/ads/conversiontracking/f;->b()V
    :try_end_4f
    .catchall {:try_start_e .. :try_end_4f} :catchall_51

    .line 239
    monitor-exit v5

    goto :goto_54

    :catchall_51
    move-exception v9

    monitor-exit v5

    throw v9

    .line 240
    :goto_54
    return-void
.end method

.method public d()V
    .registers 16

    .line 272
    iget-object v9, p0, Lcom/google/ads/conversiontracking/f;->c:Ljava/lang/Object;

    monitor-enter v9

    .line 273
    :try_start_3
    invoke-virtual {p0}, Lcom/google/ads/conversiontracking/f;->a()Landroid/database/sqlite/SQLiteDatabase;
    :try_end_6
    .catchall {:try_start_3 .. :try_end_6} :catchall_62

    move-result-object v10

    .line 274
    if-nez v10, :cond_b

    .line 275
    monitor-exit v9

    return-void

    .line 277
    :cond_b
    const/4 v11, 0x0

    .line 279
    const-string v12, "record_time ASC"

    .line 280
    move-object v0, v10

    const-string v1, "conversiontracking"

    move-object v7, v12

    const-string v8, "1"

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    :try_start_19
    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    move-object v11, v0

    .line 288
    if-eqz v11, :cond_2d

    invoke-interface {v11}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2d

    .line 289
    invoke-virtual {p0, v11}, Lcom/google/ads/conversiontracking/f;->a(Landroid/database/Cursor;)Lcom/google/ads/conversiontracking/d;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/ads/conversiontracking/f;->a(Lcom/google/ads/conversiontracking/d;)V
    :try_end_2d
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_19 .. :try_end_2d} :catch_33
    .catchall {:try_start_19 .. :try_end_2d} :catchall_59

    .line 294
    :cond_2d
    if-eqz v11, :cond_60

    .line 295
    :try_start_2f
    invoke-interface {v11}, Landroid/database/Cursor;->close()V
    :try_end_32
    .catchall {:try_start_2f .. :try_end_32} :catchall_62

    goto :goto_60

    .line 291
    :catch_33
    move-exception v12

    .line 292
    const-string v0, "GoogleConversionReporter"

    const-string v2, "Error remove oldest record"

    :try_start_38
    invoke-virtual {v12}, Landroid/database/sqlite/SQLiteException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_4b

    invoke-virtual {v2, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto :goto_50

    :cond_4b
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    :goto_50
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_53
    .catchall {:try_start_38 .. :try_end_53} :catchall_59

    .line 294
    if-eqz v11, :cond_60

    .line 295
    :try_start_55
    invoke-interface {v11}, Landroid/database/Cursor;->close()V
    :try_end_58
    .catchall {:try_start_55 .. :try_end_58} :catchall_62

    goto :goto_60

    .line 294
    :catchall_59
    move-exception v13

    if-eqz v11, :cond_5f

    .line 295
    :try_start_5c
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    :cond_5f
    throw v13
    :try_end_60
    .catchall {:try_start_5c .. :try_end_60} :catchall_62

    .line 298
    :cond_60
    :goto_60
    monitor-exit v9

    goto :goto_65

    :catchall_62
    move-exception v14

    monitor-exit v9

    throw v14

    .line 299
    :goto_65
    return-void
.end method

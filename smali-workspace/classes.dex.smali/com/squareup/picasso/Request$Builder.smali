.class public final Lcom/squareup/picasso/Request$Builder;
.super Ljava/lang/Object;
.source "Request.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/picasso/Request;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation


# instance fields
.field private centerCrop:Z

.field private centerInside:Z

.field private config:Landroid/graphics/Bitmap$Config;

.field private hasRotationPivot:Z

.field private onlyScaleDown:Z

.field private priority:Lcom/squareup/picasso/Picasso$Priority;

.field private resourceId:I

.field private rotationDegrees:F

.field private rotationPivotX:F

.field private rotationPivotY:F

.field private stableKey:Ljava/lang/String;

.field private targetHeight:I

.field private targetWidth:I

.field private transformations:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Lcom/squareup/picasso/Transformation;>;"
        }
    .end annotation
.end field

.field private uri:Landroid/net/Uri;


# direct methods
.method constructor <init>(Landroid/net/Uri;ILandroid/graphics/Bitmap$Config;)V
    .registers 4
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "resourceId"    # I
    .param p3, "bitmapConfig"    # Landroid/graphics/Bitmap$Config;

    .line 218
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 219
    iput-object p1, p0, Lcom/squareup/picasso/Request$Builder;->uri:Landroid/net/Uri;

    .line 220
    iput p2, p0, Lcom/squareup/picasso/Request$Builder;->resourceId:I

    .line 221
    iput-object p3, p0, Lcom/squareup/picasso/Request$Builder;->config:Landroid/graphics/Bitmap$Config;

    .line 222
    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/picasso/Request;
    .registers 18

    .line 454
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/squareup/picasso/Request$Builder;->centerInside:Z

    if-eqz v0, :cond_14

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/squareup/picasso/Request$Builder;->centerCrop:Z

    if-eqz v0, :cond_14

    .line 455
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Center crop and center inside can not be used together."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 457
    :cond_14
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/squareup/picasso/Request$Builder;->centerCrop:Z

    if-eqz v0, :cond_2e

    move-object/from16 v0, p0

    iget v0, v0, Lcom/squareup/picasso/Request$Builder;->targetWidth:I

    if-nez v0, :cond_2e

    move-object/from16 v0, p0

    iget v0, v0, Lcom/squareup/picasso/Request$Builder;->targetHeight:I

    if-nez v0, :cond_2e

    .line 458
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Center crop requires calling resize with positive width and height."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 461
    :cond_2e
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/squareup/picasso/Request$Builder;->centerInside:Z

    if-eqz v0, :cond_48

    move-object/from16 v0, p0

    iget v0, v0, Lcom/squareup/picasso/Request$Builder;->targetWidth:I

    if-nez v0, :cond_48

    move-object/from16 v0, p0

    iget v0, v0, Lcom/squareup/picasso/Request$Builder;->targetHeight:I

    if-nez v0, :cond_48

    .line 462
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Center inside requires calling resize with positive width and height."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 465
    :cond_48
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/squareup/picasso/Request$Builder;->priority:Lcom/squareup/picasso/Picasso$Priority;

    if-nez v0, :cond_54

    .line 466
    sget-object v0, Lcom/squareup/picasso/Picasso$Priority;->NORMAL:Lcom/squareup/picasso/Picasso$Priority;

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/squareup/picasso/Request$Builder;->priority:Lcom/squareup/picasso/Picasso$Priority;

    .line 468
    :cond_54
    new-instance v0, Lcom/squareup/picasso/Request;

    move-object/from16 v1, p0

    iget-object v1, v1, Lcom/squareup/picasso/Request$Builder;->uri:Landroid/net/Uri;

    move-object/from16 v2, p0

    iget v2, v2, Lcom/squareup/picasso/Request$Builder;->resourceId:I

    move-object/from16 v3, p0

    iget-object v3, v3, Lcom/squareup/picasso/Request$Builder;->stableKey:Ljava/lang/String;

    move-object/from16 v4, p0

    iget-object v4, v4, Lcom/squareup/picasso/Request$Builder;->transformations:Ljava/util/List;

    move-object/from16 v5, p0

    iget v5, v5, Lcom/squareup/picasso/Request$Builder;->targetWidth:I

    move-object/from16 v6, p0

    iget v6, v6, Lcom/squareup/picasso/Request$Builder;->targetHeight:I

    move-object/from16 v7, p0

    iget-boolean v7, v7, Lcom/squareup/picasso/Request$Builder;->centerCrop:Z

    move-object/from16 v8, p0

    iget-boolean v8, v8, Lcom/squareup/picasso/Request$Builder;->centerInside:Z

    move-object/from16 v9, p0

    iget-boolean v9, v9, Lcom/squareup/picasso/Request$Builder;->onlyScaleDown:Z

    move-object/from16 v10, p0

    iget v10, v10, Lcom/squareup/picasso/Request$Builder;->rotationDegrees:F

    move-object/from16 v11, p0

    iget v11, v11, Lcom/squareup/picasso/Request$Builder;->rotationPivotX:F

    move-object/from16 v12, p0

    iget v12, v12, Lcom/squareup/picasso/Request$Builder;->rotationPivotY:F

    move-object/from16 v13, p0

    iget-boolean v13, v13, Lcom/squareup/picasso/Request$Builder;->hasRotationPivot:Z

    move-object/from16 v14, p0

    iget-object v14, v14, Lcom/squareup/picasso/Request$Builder;->config:Landroid/graphics/Bitmap$Config;

    move-object/from16 v15, p0

    iget-object v15, v15, Lcom/squareup/picasso/Request$Builder;->priority:Lcom/squareup/picasso/Picasso$Priority;

    const/16 v16, 0x0

    invoke-direct/range {v0 .. v16}, Lcom/squareup/picasso/Request;-><init>(Landroid/net/Uri;ILjava/lang/String;Ljava/util/List;IIZZZFFFZLandroid/graphics/Bitmap$Config;Lcom/squareup/picasso/Picasso$Priority;Lcom/squareup/picasso/Request$1;)V

    return-object v0
.end method

.method hasImage()Z
    .registers 2

    .line 245
    iget-object v0, p0, Lcom/squareup/picasso/Request$Builder;->uri:Landroid/net/Uri;

    if-nez v0, :cond_8

    iget v0, p0, Lcom/squareup/picasso/Request$Builder;->resourceId:I

    if-eqz v0, :cond_a

    :cond_8
    const/4 v0, 0x1

    goto :goto_b

    :cond_a
    const/4 v0, 0x0

    :goto_b
    return v0
.end method

.method hasSize()Z
    .registers 2

    .line 249
    iget v0, p0, Lcom/squareup/picasso/Request$Builder;->targetWidth:I

    if-nez v0, :cond_8

    iget v0, p0, Lcom/squareup/picasso/Request$Builder;->targetHeight:I

    if-eqz v0, :cond_a

    :cond_8
    const/4 v0, 0x1

    goto :goto_b

    :cond_a
    const/4 v0, 0x0

    :goto_b
    return v0
.end method

.method public resize(II)Lcom/squareup/picasso/Request$Builder;
    .registers 5
    .param p1, "targetWidth"    # I
    .param p2, "targetHeight"    # I

    .line 298
    if-gez p1, :cond_a

    .line 299
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Width must be positive number or 0."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 301
    :cond_a
    if-gez p2, :cond_14

    .line 302
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Height must be positive number or 0."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 304
    :cond_14
    if-nez p2, :cond_20

    if-nez p1, :cond_20

    .line 305
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "At least one dimension has to be positive number."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 307
    :cond_20
    iput p1, p0, Lcom/squareup/picasso/Request$Builder;->targetWidth:I

    .line 308
    iput p2, p0, Lcom/squareup/picasso/Request$Builder;->targetHeight:I

    .line 309
    return-object p0
.end method

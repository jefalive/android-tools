.class final Lcom/itau/empresas/ui/view/wraper/LinearLayoutWrapper$ListViewAdapterObserver;
.super Landroid/database/DataSetObserver;
.source "LinearLayoutWrapper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/itau/empresas/ui/view/wraper/LinearLayoutWrapper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ListViewAdapterObserver"
.end annotation


# instance fields
.field private final listener:Lcom/itau/empresas/ui/view/wraper/LinearLayoutWrapper$OnDataSetChanged;


# direct methods
.method constructor <init>(Lcom/itau/empresas/ui/view/wraper/LinearLayoutWrapper$OnDataSetChanged;)V
    .registers 2
    .param p1, "listener"    # Lcom/itau/empresas/ui/view/wraper/LinearLayoutWrapper$OnDataSetChanged;

    .line 207
    invoke-direct {p0}, Landroid/database/DataSetObserver;-><init>()V

    .line 208
    iput-object p1, p0, Lcom/itau/empresas/ui/view/wraper/LinearLayoutWrapper$ListViewAdapterObserver;->listener:Lcom/itau/empresas/ui/view/wraper/LinearLayoutWrapper$OnDataSetChanged;

    .line 209
    return-void
.end method


# virtual methods
.method public onChanged()V
    .registers 2

    .line 213
    iget-object v0, p0, Lcom/itau/empresas/ui/view/wraper/LinearLayoutWrapper$ListViewAdapterObserver;->listener:Lcom/itau/empresas/ui/view/wraper/LinearLayoutWrapper$OnDataSetChanged;

    invoke-interface {v0}, Lcom/itau/empresas/ui/view/wraper/LinearLayoutWrapper$OnDataSetChanged;->onChanged()V

    .line 214
    return-void
.end method

.class public Lcom/itau/empresas/feature/credito/produtos/adapter/ProdutosMultilimiteListAdapter;
.super Landroid/widget/BaseAdapter;
.source "ProdutosMultilimiteListAdapter.java"


# instance fields
.field context:Landroid/content/Context;

.field private produtos:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Lcom/itau/empresas/feature/credito/produtos/model/DetalhesMultilimiteProdutoVO;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 20
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public getCount()I
    .registers 2

    .line 34
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/adapter/ProdutosMultilimiteListAdapter;->produtos:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Lcom/itau/empresas/feature/credito/produtos/model/DetalhesMultilimiteProdutoVO;
    .registers 3
    .param p1, "position"    # I

    .line 39
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/adapter/ProdutosMultilimiteListAdapter;->produtos:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/feature/credito/produtos/model/DetalhesMultilimiteProdutoVO;

    return-object v0
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .registers 3

    .line 19
    invoke-virtual {p0, p1}, Lcom/itau/empresas/feature/credito/produtos/adapter/ProdutosMultilimiteListAdapter;->getItem(I)Lcom/itau/empresas/feature/credito/produtos/model/DetalhesMultilimiteProdutoVO;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .registers 4
    .param p1, "position"    # I

    .line 44
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 8
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;
    .annotation build Landroid/annotation/TargetApi;
        value = 0x13
    .end annotation

    .line 50
    invoke-virtual {p0, p1}, Lcom/itau/empresas/feature/credito/produtos/adapter/ProdutosMultilimiteListAdapter;->getItem(I)Lcom/itau/empresas/feature/credito/produtos/model/DetalhesMultilimiteProdutoVO;

    move-result-object v1

    .line 51
    .local v1, "produto":Lcom/itau/empresas/feature/credito/produtos/model/DetalhesMultilimiteProdutoVO;
    move-object v2, p2

    .line 53
    .local v2, "v":Landroid/view/View;
    if-nez v2, :cond_d

    .line 54
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/adapter/ProdutosMultilimiteListAdapter;->context:Landroid/content/Context;

    invoke-static {v0}, Lcom/itau/empresas/feature/credito/produtos/view/ProdutoMultilimiteView_;->build(Landroid/content/Context;)Lcom/itau/empresas/feature/credito/produtos/view/ProdutoMultilimiteView;

    move-result-object v2

    .line 57
    :cond_d
    move-object v3, v2

    check-cast v3, Lcom/itau/empresas/feature/credito/produtos/view/ProdutoMultilimiteView;

    .line 59
    .local v3, "view":Lcom/itau/empresas/feature/credito/produtos/view/ProdutoMultilimiteView;
    invoke-virtual {v3, v1}, Lcom/itau/empresas/feature/credito/produtos/view/ProdutoMultilimiteView;->bind(Lcom/itau/empresas/feature/credito/produtos/model/DetalhesMultilimiteProdutoVO;)V

    .line 61
    return-object v3
.end method

.method public setData(Ljava/util/List;)V
    .registers 2
    .param p1, "produtos"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<Lcom/itau/empresas/feature/credito/produtos/model/DetalhesMultilimiteProdutoVO;>;)V"
        }
    .end annotation

    .line 28
    iput-object p1, p0, Lcom/itau/empresas/feature/credito/produtos/adapter/ProdutosMultilimiteListAdapter;->produtos:Ljava/util/List;

    .line 29
    return-void
.end method

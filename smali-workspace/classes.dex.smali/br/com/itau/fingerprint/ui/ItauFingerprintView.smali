.class public Lbr/com/itau/fingerprint/ui/ItauFingerprintView;
.super Landroid/widget/RelativeLayout;
.source "ItauFingerprintView.java"


# static fields
.field private static final transient synthetic $jacocoData:[Z


# instance fields
.field private icon:Landroid/widget/ImageView;

.field private status:Landroid/widget/TextView;


# direct methods
.method private static synthetic $jacocoInit()[Z
    .registers 4

    sget-object v0, Lbr/com/itau/fingerprint/ui/ItauFingerprintView;->$jacocoData:[Z

    if-nez v0, :cond_13

    const-string v0, "br/com/itau/fingerprint/ui/ItauFingerprintView"

    const-wide v1, -0x173da12309312a2fL    # -4.291306762714624E196

    const/16 v3, 0x13

    invoke-static {v1, v2, v0, v3}, Lorg/jacoco/agent/rt/internal_b0d6a23/Offline;->getProbes(JLjava/lang/String;I)[Z

    move-result-object v0

    sput-object v0, Lbr/com/itau/fingerprint/ui/ItauFingerprintView;->$jacocoData:[Z

    :cond_13
    return-object v0
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 5

    invoke-static {}, Lbr/com/itau/fingerprint/ui/ItauFingerprintView;->$jacocoInit()[Z

    move-result-object v2

    .line 20
    .local p0, "this":Lbr/com/itau/fingerprint/ui/ItauFingerprintView;
    .local p1, "context":Landroid/content/Context;
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x1

    const/4 v1, 0x0

    aput-boolean v0, v2, v1

    .line 21
    invoke-direct {p0}, Lbr/com/itau/fingerprint/ui/ItauFingerprintView;->init()V

    .line 22
    const/4 v0, 0x1

    const/4 v1, 0x1

    aput-boolean v0, v2, v1

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 6

    invoke-static {}, Lbr/com/itau/fingerprint/ui/ItauFingerprintView;->$jacocoInit()[Z

    move-result-object v2

    .line 25
    .local p0, "this":Lbr/com/itau/fingerprint/ui/ItauFingerprintView;
    .local p1, "context":Landroid/content/Context;
    .local p2, "attrs":Landroid/util/AttributeSet;
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v0, 0x1

    const/4 v1, 0x2

    aput-boolean v0, v2, v1

    .line 26
    invoke-direct {p0}, Lbr/com/itau/fingerprint/ui/ItauFingerprintView;->init()V

    .line 27
    const/4 v0, 0x1

    const/4 v1, 0x3

    aput-boolean v0, v2, v1

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 7

    invoke-static {}, Lbr/com/itau/fingerprint/ui/ItauFingerprintView;->$jacocoInit()[Z

    move-result-object v2

    .line 30
    .local p0, "this":Lbr/com/itau/fingerprint/ui/ItauFingerprintView;
    .local p1, "context":Landroid/content/Context;
    .local p2, "attrs":Landroid/util/AttributeSet;
    .local p3, "defStyleAttr":I
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 v0, 0x1

    const/4 v1, 0x4

    aput-boolean v0, v2, v1

    .line 31
    invoke-direct {p0}, Lbr/com/itau/fingerprint/ui/ItauFingerprintView;->init()V

    .line 32
    const/4 v0, 0x1

    const/4 v1, 0x5

    aput-boolean v0, v2, v1

    return-void
.end method

.method private init()V
    .registers 4

    invoke-static {}, Lbr/com/itau/fingerprint/ui/ItauFingerprintView;->$jacocoInit()[Z

    move-result-object v2

    .line 35
    .local p0, "this":Lbr/com/itau/fingerprint/ui/ItauFingerprintView;
    invoke-virtual {p0}, Lbr/com/itau/fingerprint/ui/ItauFingerprintView;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lbr/com/itau/fingerprintui/R$layout;->itausdkcore_fingerprint_dialog_content:I

    invoke-static {v0, v1, p0}, Lbr/com/itau/fingerprint/ui/ItauFingerprintView;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    const/4 v0, 0x1

    const/4 v1, 0x6

    aput-boolean v0, v2, v1

    .line 36
    sget v0, Lbr/com/itau/fingerprintui/R$id;->fingerprint_icon:I

    invoke-virtual {p0, v0}, Lbr/com/itau/fingerprint/ui/ItauFingerprintView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lbr/com/itau/fingerprint/ui/ItauFingerprintView;->icon:Landroid/widget/ImageView;

    const/4 v0, 0x1

    const/4 v1, 0x7

    aput-boolean v0, v2, v1

    .line 37
    sget v0, Lbr/com/itau/fingerprintui/R$id;->fingerprint_status:I

    invoke-virtual {p0, v0}, Lbr/com/itau/fingerprint/ui/ItauFingerprintView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lbr/com/itau/fingerprint/ui/ItauFingerprintView;->status:Landroid/widget/TextView;

    .line 38
    const/4 v0, 0x1

    const/16 v1, 0x8

    aput-boolean v0, v2, v1

    return-void
.end method


# virtual methods
.method public setFailed()V
    .registers 6

    invoke-static {}, Lbr/com/itau/fingerprint/ui/ItauFingerprintView;->$jacocoInit()[Z

    move-result-object v4

    .line 41
    .local p0, "this":Lbr/com/itau/fingerprint/ui/ItauFingerprintView;
    iget-object v0, p0, Lbr/com/itau/fingerprint/ui/ItauFingerprintView;->icon:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lbr/com/itau/fingerprint/ui/ItauFingerprintView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lbr/com/itau/fingerprintui/R$drawable;->itausdkcore_ic_fingerprint_error:I

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Landroid/support/graphics/drawable/VectorDrawableCompat;->create(Landroid/content/res/Resources;ILandroid/content/res/Resources$Theme;)Landroid/support/graphics/drawable/VectorDrawableCompat;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    const/4 v0, 0x1

    const/16 v1, 0x9

    aput-boolean v0, v4, v1

    .line 43
    iget-object v0, p0, Lbr/com/itau/fingerprint/ui/ItauFingerprintView;->status:Landroid/widget/TextView;

    sget v1, Lbr/com/itau/fingerprintui/R$string;->fingerprint_not_recognized:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    const/4 v0, 0x1

    const/16 v1, 0xa

    aput-boolean v0, v4, v1

    .line 44
    iget-object v0, p0, Lbr/com/itau/fingerprint/ui/ItauFingerprintView;->status:Landroid/widget/TextView;

    invoke-virtual {p0}, Lbr/com/itau/fingerprint/ui/ItauFingerprintView;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lbr/com/itau/fingerprintui/R$color;->warning_color:I

    invoke-static {v1, v2}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 45
    const/4 v0, 0x1

    const/16 v1, 0xb

    aput-boolean v0, v4, v1

    return-void
.end method

.method public setHelpMessage(Ljava/lang/String;)V
    .registers 5

    invoke-static {}, Lbr/com/itau/fingerprint/ui/ItauFingerprintView;->$jacocoInit()[Z

    move-result-object v2

    .line 55
    .local p0, "this":Lbr/com/itau/fingerprint/ui/ItauFingerprintView;
    .local p1, "message":Ljava/lang/String;
    iget-object v0, p0, Lbr/com/itau/fingerprint/ui/ItauFingerprintView;->status:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 56
    const/4 v0, 0x1

    const/16 v1, 0xf

    aput-boolean v0, v2, v1

    return-void
.end method

.method public setSuccess()V
    .registers 6

    invoke-static {}, Lbr/com/itau/fingerprint/ui/ItauFingerprintView;->$jacocoInit()[Z

    move-result-object v4

    .line 48
    .local p0, "this":Lbr/com/itau/fingerprint/ui/ItauFingerprintView;
    iget-object v0, p0, Lbr/com/itau/fingerprint/ui/ItauFingerprintView;->icon:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lbr/com/itau/fingerprint/ui/ItauFingerprintView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lbr/com/itau/fingerprintui/R$drawable;->itausdkcore_ic_fingerprint_success:I

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Landroid/support/graphics/drawable/VectorDrawableCompat;->create(Landroid/content/res/Resources;ILandroid/content/res/Resources$Theme;)Landroid/support/graphics/drawable/VectorDrawableCompat;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    const/4 v0, 0x1

    const/16 v1, 0xc

    aput-boolean v0, v4, v1

    .line 50
    iget-object v0, p0, Lbr/com/itau/fingerprint/ui/ItauFingerprintView;->status:Landroid/widget/TextView;

    sget v1, Lbr/com/itau/fingerprintui/R$string;->fingerprint_success:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    const/4 v0, 0x1

    const/16 v1, 0xd

    aput-boolean v0, v4, v1

    .line 51
    iget-object v0, p0, Lbr/com/itau/fingerprint/ui/ItauFingerprintView;->status:Landroid/widget/TextView;

    invoke-virtual {p0}, Lbr/com/itau/fingerprint/ui/ItauFingerprintView;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lbr/com/itau/fingerprintui/R$color;->success_color:I

    invoke-static {v1, v2}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 52
    const/4 v0, 0x1

    const/16 v1, 0xe

    aput-boolean v0, v4, v1

    return-void
.end method

.class final Lbr/com/itau/topsnackbar/LinearLayoutManager;
.super Lbr/com/itau/topsnackbar/ViewManager;
.source "LinearLayoutManager.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lbr/com/itau/topsnackbar/ViewManager<Landroid/widget/LinearLayout;>;"
    }
.end annotation


# instance fields
.field private final position:I


# direct methods
.method constructor <init>(Landroid/widget/LinearLayout;)V
    .registers 3
    .param p1, "parent"    # Landroid/widget/LinearLayout;

    .line 12
    invoke-direct {p0, p1}, Lbr/com/itau/topsnackbar/ViewManager;-><init>(Landroid/view/ViewGroup;)V

    .line 13
    invoke-static {p1}, Lbr/com/itau/topsnackbar/LinearLayoutManager;->containsToolbar(Landroid/view/ViewGroup;)Z

    move-result v0

    if-eqz v0, :cond_b

    const/4 v0, 0x1

    goto :goto_c

    :cond_b
    const/4 v0, 0x0

    :goto_c
    iput v0, p0, Lbr/com/itau/topsnackbar/LinearLayoutManager;->position:I

    .line 14
    return-void
.end method

.method private static containsToolbar(Landroid/view/ViewGroup;)Z
    .registers 6
    .param p0, "parent"    # Landroid/view/ViewGroup;

    .line 26
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    .line 27
    .local v1, "childCount":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_5
    if-ge v2, v1, :cond_20

    .line 28
    invoke-virtual {p0, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 29
    .local v3, "view":Landroid/view/View;
    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    .line 30
    .local v4, "simpleName":Ljava/lang/String;
    const-string v0, "Toolbar"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1d

    .line 31
    const/4 v0, 0x1

    return v0

    .line 27
    .end local v3    # "view":Landroid/view/View;
    .end local v4    # "simpleName":Ljava/lang/String;
    :cond_1d
    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    .line 33
    .end local v2    # "i":I
    :cond_20
    const/4 v0, 0x0

    return v0
.end method


# virtual methods
.method public addContent(Landroid/view/View;)V
    .registers 4
    .param p1, "content"    # Landroid/view/View;

    .line 17
    iget-object v0, p0, Lbr/com/itau/topsnackbar/LinearLayoutManager;->parent:Landroid/view/ViewGroup;

    check-cast v0, Landroid/widget/LinearLayout;

    iget v1, p0, Lbr/com/itau/topsnackbar/LinearLayoutManager;->position:I

    invoke-virtual {v0, p1, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;I)V

    .line 18
    return-void
.end method

.method public removeContent(Landroid/view/View;)V
    .registers 3
    .param p1, "content"    # Landroid/view/View;

    .line 21
    iget-object v0, p0, Lbr/com/itau/topsnackbar/LinearLayoutManager;->parent:Landroid/view/ViewGroup;

    check-cast v0, Landroid/widget/LinearLayout;

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->removeView(Landroid/view/View;)V

    .line 22
    return-void
.end method

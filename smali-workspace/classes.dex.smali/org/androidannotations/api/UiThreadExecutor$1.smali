.class final Lorg/androidannotations/api/UiThreadExecutor$1;
.super Landroid/os/Handler;
.source "UiThreadExecutor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/androidannotations/api/UiThreadExecutor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>(Landroid/os/Looper;)V
    .registers 2
    .param p1, "x0"    # Landroid/os/Looper;

    .line 32
    invoke-direct {p0, p1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 4
    .param p1, "msg"    # Landroid/os/Message;

    .line 35
    invoke-virtual {p1}, Landroid/os/Message;->getCallback()Ljava/lang/Runnable;

    move-result-object v1

    .line 36
    .local v1, "callback":Ljava/lang/Runnable;
    if-eqz v1, :cond_11

    .line 37
    invoke-interface {v1}, Ljava/lang/Runnable;->run()V

    .line 38
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lorg/androidannotations/api/UiThreadExecutor$Token;

    # invokes: Lorg/androidannotations/api/UiThreadExecutor;->decrementToken(Lorg/androidannotations/api/UiThreadExecutor$Token;)V
    invoke-static {v0}, Lorg/androidannotations/api/UiThreadExecutor;->access$000(Lorg/androidannotations/api/UiThreadExecutor$Token;)V

    goto :goto_14

    .line 40
    :cond_11
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 42
    :goto_14
    return-void
.end method

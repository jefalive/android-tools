.class public Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosSimulacaoActivity;
.super Lcom/itau/empresas/ui/activity/BaseActivity;
.source "EmprestimosParceladosSimulacaoActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field devedorSolidario:Z

.field emprestimosParceladosSimulacaoController:Lcom/itau/empresas/feature/credito/giro/controller/EmprestimosParceladosSimulacaoController;

.field exibirSimulacao:Z

.field fluxoPersonalizado:Z

.field private ofertaEspecialAceitaNovaRequisicao:Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;

.field ofertaEspecialAceitaVO:Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;

.field root:Landroid/widget/LinearLayout;

.field simulacaoPropostaController:Lcom/itau/empresas/feature/credito/giro/controller/EmprestimosParceladosSimulacaoPropostaController;

.field simulacaoSaidaVO:Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoSaidaVO;

.field private temSeguro:Z

.field toolBar:Landroid/support/v7/widget/Toolbar;

.field private topSnackbar:Lbr/com/itau/topsnackbar/TopSnackbar;

.field viewCreditoDetalhado:Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 37
    invoke-direct {p0}, Lcom/itau/empresas/ui/activity/BaseActivity;-><init>()V

    return-void
.end method

.method private buscaSimulacaoProposta()V
    .registers 4

    .line 185
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosSimulacaoActivity;->topSnackbar:Lbr/com/itau/topsnackbar/TopSnackbar;

    if-eqz v0, :cond_9

    .line 186
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosSimulacaoActivity;->topSnackbar:Lbr/com/itau/topsnackbar/TopSnackbar;

    invoke-virtual {v0}, Lbr/com/itau/topsnackbar/TopSnackbar;->dismiss()V

    .line 188
    :cond_9
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosSimulacaoActivity;->mostraDialogoDeProgresso()V

    .line 191
    :try_start_c
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosSimulacaoActivity;->ofertaEspecialAceitaVO:Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;

    .line 192
    invoke-virtual {v0}, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosSimulacaoActivity;->ofertaEspecialAceitaNovaRequisicao:Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;

    .line 194
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosSimulacaoActivity;->ofertaEspecialAceitaNovaRequisicao:Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosSimulacaoActivity;->ofertaEspecialAceitaVO:Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;

    .line 195
    invoke-virtual {v1}, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;->getPrazoMinimoContratacao()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 194
    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;->setPrazoMinimoContratacao(Ljava/lang/String;)V

    .line 196
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosSimulacaoActivity;->ofertaEspecialAceitaNovaRequisicao:Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosSimulacaoActivity;->ofertaEspecialAceitaVO:Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;

    .line 197
    invoke-virtual {v1}, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;->getPrazoMaximoContratacao()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 196
    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;->setPrazoMaximoContratacao(Ljava/lang/String;)V

    .line 198
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosSimulacaoActivity;->ofertaEspecialAceitaNovaRequisicao:Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;->setValorEmprestimo(Ljava/math/BigDecimal;)V

    .line 199
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosSimulacaoActivity;->ofertaEspecialAceitaNovaRequisicao:Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;->setDataPrimeiroPagamento(Ljava/lang/String;)V

    .line 201
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosSimulacaoActivity;->simulacaoPropostaController:Lcom/itau/empresas/feature/credito/giro/controller/EmprestimosParceladosSimulacaoPropostaController;

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosSimulacaoActivity;->ofertaEspecialAceitaNovaRequisicao:Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;

    .line 202
    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/credito/giro/controller/EmprestimosParceladosSimulacaoPropostaController;->consultarSimulacaoProposta(Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;)V
    :try_end_47
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_c .. :try_end_47} :catch_48

    .line 205
    goto :goto_58

    .line 203
    :catch_48
    move-exception v2

    .line 204
    .local v2, "e":Ljava/lang/CloneNotSupportedException;
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2}, Ljava/lang/CloneNotSupportedException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v2}, Lbr/com/itau/textovoz/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 206
    .end local v2    # "e":Ljava/lang/CloneNotSupportedException;
    :goto_58
    return-void
.end method

.method private constroiToolbar()V
    .registers 3

    .line 84
    iget-boolean v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosSimulacaoActivity;->temSeguro:Z

    if-eqz v0, :cond_8

    .line 85
    const v1, 0x7f070699

    .local v1, "id":I
    goto :goto_b

    .line 88
    .end local v1    # "id":I
    :cond_8
    const v1, 0x7f07069a

    .line 91
    .local v1, "id":I
    :goto_b
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosSimulacaoActivity;->toolBar:Landroid/support/v7/widget/Toolbar;

    invoke-static {v0, p0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom;->Builder(Landroid/support/v7/widget/Toolbar;Landroid/support/v7/app/AppCompatActivity;)Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    move-result-object v0

    .line 93
    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->comTitulo(I)Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    move-result-object v0

    .line 94
    invoke-virtual {v0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->comClickListenerFinish()Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    move-result-object v0

    .line 95
    invoke-virtual {v0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->build()Lcom/itau/empresas/ui/util/listener/ToolbarCustom;

    .line 96
    return-void
.end method

.method public static intent(Landroid/content/Context;)Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosSimulacaoActivity_$IntentBuilder_;
    .registers 2
    .param p0, "context"    # Landroid/content/Context;

    .line 249
    new-instance v0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosSimulacaoActivity_$IntentBuilder_;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosSimulacaoActivity_$IntentBuilder_;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method private validarRespostaDaSimulacaoProposta(Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoPropostaSaidaVO;)Z
    .registers 6
    .param p1, "propostaSaidaVO"    # Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoPropostaSaidaVO;

    .line 229
    const/4 v3, 0x1

    .line 231
    .local v3, "valido":Z
    invoke-virtual {p1}, Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoPropostaSaidaVO;->getValoresEmprestimos()Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_11

    .line 232
    invoke-virtual {p1}, Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoPropostaSaidaVO;->getValoresEmprestimos()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_22

    .line 234
    :cond_11
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosSimulacaoActivity;->root:Landroid/widget/LinearLayout;

    const v1, 0x7f070622

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lbr/com/itau/topsnackbar/TopSnackbar;->make(Landroid/view/ViewGroup;II)Lbr/com/itau/topsnackbar/TopSnackbar;

    move-result-object v0

    .line 236
    invoke-virtual {v0}, Lbr/com/itau/topsnackbar/TopSnackbar;->show()Lbr/com/itau/topsnackbar/TopSnackbar;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosSimulacaoActivity;->topSnackbar:Lbr/com/itau/topsnackbar/TopSnackbar;

    .line 238
    const/4 v3, 0x0

    .line 241
    :cond_22
    return v3
.end method


# virtual methods
.method aoCarregarElementosDeTela()V
    .registers 9

    .line 60
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosSimulacaoActivity;->topSnackbar:Lbr/com/itau/topsnackbar/TopSnackbar;

    if-eqz v0, :cond_9

    .line 61
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosSimulacaoActivity;->topSnackbar:Lbr/com/itau/topsnackbar/TopSnackbar;

    invoke-virtual {v0}, Lbr/com/itau/topsnackbar/TopSnackbar;->dismiss()V

    .line 63
    :cond_9
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosSimulacaoActivity;->simulacaoSaidaVO:Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoSaidaVO;

    if-eqz v0, :cond_3a

    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosSimulacaoActivity;->ofertaEspecialAceitaVO:Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;

    if-eqz v0, :cond_3a

    .line 65
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosSimulacaoActivity;->simulacaoSaidaVO:Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoSaidaVO;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoSaidaVO;->getDadosSeguro()Lcom/itau/empresas/feature/credito/giro/model/SeguroProdutosParceladoSaidaVO;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/feature/credito/giro/model/SeguroProdutosParceladoSaidaVO;->getIndicadorSeguro()Z

    move-result v0

    iput-boolean v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosSimulacaoActivity;->temSeguro:Z

    .line 67
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosSimulacaoActivity;->constroiToolbar()V

    .line 69
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosSimulacaoActivity;->ofertaEspecialAceitaVO:Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;->getDevedorSolidario()Z

    move-result v0

    iput-boolean v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosSimulacaoActivity;->devedorSolidario:Z

    .line 71
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosSimulacaoActivity;->viewCreditoDetalhado:Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosSimulacaoActivity;->simulacaoSaidaVO:Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoSaidaVO;

    sget-object v2, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView$EnumTipoExibicao;->SIMULACAO_OFERTA_ESPECIAL:Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView$EnumTipoExibicao;

    iget-boolean v3, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosSimulacaoActivity;->temSeguro:Z

    iget-boolean v4, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosSimulacaoActivity;->devedorSolidario:Z

    iget-boolean v6, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosSimulacaoActivity;->fluxoPersonalizado:Z

    iget-boolean v7, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosSimulacaoActivity;->exibirSimulacao:Z

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v7}, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->bind(Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoSaidaVO;Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView$EnumTipoExibicao;ZZLcom/itau/empresas/feature/credito/giro/model/ResultadoEfetivacaoEmprestimosParceladosVO;ZZ)V

    .line 80
    :cond_3a
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .registers 2
    .param p1, "v"    # Landroid/view/View;

    .line 245
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosSimulacaoActivity;->onBackPressed()V

    .line 246
    return-void
.end method

.method public onEventMainThread(Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent$Conversion;)V
    .registers 5
    .param p1, "e"    # Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent$Conversion;

    .line 110
    invoke-static {p1, p0}, Lcom/itau/empresas/api/ApiHelper;->devoTratarEvento(Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;Lcom/itau/empresas/api/ApiConsumidor;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 111
    return-void

    .line 114
    :cond_7
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosSimulacaoActivity;->escondeDialogoDeProgresso()V

    .line 116
    const-string v0, "ItauEmpresas"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Erro na Chamanda: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent$Conversion;->getException()Lbr/com/itau/sdk/android/core/exception/BackendException;

    move-result-object v2

    invoke-virtual {v2}, Lbr/com/itau/sdk/android/core/exception/BackendException;->getOpKey()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent$Conversion;->getException()Lbr/com/itau/sdk/android/core/exception/BackendException;

    move-result-object v2

    invoke-static {v0, v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 117
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosSimulacaoActivity;->root:Landroid/widget/LinearLayout;

    const v1, 0x7f0701f2

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lbr/com/itau/topsnackbar/TopSnackbar;->make(Landroid/view/ViewGroup;II)Lbr/com/itau/topsnackbar/TopSnackbar;

    move-result-object v0

    .line 118
    invoke-virtual {v0}, Lbr/com/itau/topsnackbar/TopSnackbar;->show()Lbr/com/itau/topsnackbar/TopSnackbar;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosSimulacaoActivity;->topSnackbar:Lbr/com/itau/topsnackbar/TopSnackbar;

    .line 119
    return-void
.end method

.method public onEventMainThread(Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;)V
    .registers 5
    .param p1, "e"    # Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;

    .line 128
    invoke-static {p1, p0}, Lcom/itau/empresas/api/ApiHelper;->devoTratarEvento(Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;Lcom/itau/empresas/api/ApiConsumidor;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 129
    return-void

    .line 132
    :cond_7
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosSimulacaoActivity;->escondeDialogoDeProgresso()V

    .line 134
    const-string v0, "ItauEmpresas"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Erro na Chamanda: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;->getException()Lbr/com/itau/sdk/android/core/exception/BackendException;

    move-result-object v2

    invoke-virtual {v2}, Lbr/com/itau/sdk/android/core/exception/BackendException;->getOpKey()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;->getException()Lbr/com/itau/sdk/android/core/exception/BackendException;

    move-result-object v2

    invoke-static {v0, v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 135
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosSimulacaoActivity;->root:Landroid/widget/LinearLayout;

    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;->getException()Lbr/com/itau/sdk/android/core/exception/BackendException;

    move-result-object v1

    invoke-virtual {v1}, Lbr/com/itau/sdk/android/core/exception/BackendException;->getError()Lbr/com/itau/sdk/android/core/model/ErrorDTO;

    move-result-object v1

    invoke-virtual {v1}, Lbr/com/itau/sdk/android/core/model/ErrorDTO;->getMensagem()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lbr/com/itau/topsnackbar/TopSnackbar;->make(Landroid/view/ViewGroup;Ljava/lang/CharSequence;I)Lbr/com/itau/topsnackbar/TopSnackbar;

    move-result-object v0

    .line 136
    invoke-virtual {v0}, Lbr/com/itau/topsnackbar/TopSnackbar;->show()Lbr/com/itau/topsnackbar/TopSnackbar;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosSimulacaoActivity;->topSnackbar:Lbr/com/itau/topsnackbar/TopSnackbar;

    .line 137
    return-void
.end method

.method public onEventMainThread(Lcom/itau/empresas/Evento$EventoGiroEfetivacao;)V
    .registers 4
    .param p1, "eventoGiroEfetivacao"    # Lcom/itau/empresas/Evento$EventoGiroEfetivacao;

    .line 145
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosSimulacaoActivity;->topSnackbar:Lbr/com/itau/topsnackbar/TopSnackbar;

    if-eqz v0, :cond_9

    .line 146
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosSimulacaoActivity;->topSnackbar:Lbr/com/itau/topsnackbar/TopSnackbar;

    invoke-virtual {v0}, Lbr/com/itau/topsnackbar/TopSnackbar;->dismiss()V

    .line 148
    :cond_9
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosSimulacaoActivity;->mostraDialogoDeProgresso()V

    .line 150
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosSimulacaoActivity;->emprestimosParceladosSimulacaoController:Lcom/itau/empresas/feature/credito/giro/controller/EmprestimosParceladosSimulacaoController;

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosSimulacaoActivity;->simulacaoSaidaVO:Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoSaidaVO;

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/credito/giro/controller/EmprestimosParceladosSimulacaoController;->contrataEmprestimoSimulado(Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoSaidaVO;)V

    .line 152
    return-void
.end method

.method public onEventMainThread(Lcom/itau/empresas/Evento$EventoTrocaTipoSimulacao;)V
    .registers 3
    .param p1, "eventoGiroEfetivacao"    # Lcom/itau/empresas/Evento$EventoTrocaTipoSimulacao;

    .line 171
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosSimulacaoActivity;->topSnackbar:Lbr/com/itau/topsnackbar/TopSnackbar;

    if-eqz v0, :cond_9

    .line 172
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosSimulacaoActivity;->topSnackbar:Lbr/com/itau/topsnackbar/TopSnackbar;

    invoke-virtual {v0}, Lbr/com/itau/topsnackbar/TopSnackbar;->dismiss()V

    .line 174
    :cond_9
    invoke-virtual {p1}, Lcom/itau/empresas/Evento$EventoTrocaTipoSimulacao;->getPersonalizado()Z

    move-result v0

    if-eqz v0, :cond_13

    .line 176
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosSimulacaoActivity;->onBackPressed()V

    goto :goto_16

    .line 180
    :cond_13
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosSimulacaoActivity;->buscaSimulacaoProposta()V

    .line 182
    :goto_16
    return-void
.end method

.method public onEventMainThread(Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoPropostaSaidaVO;)V
    .registers 5
    .param p1, "propostaSaidaVO"    # Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoPropostaSaidaVO;

    .line 214
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosSimulacaoActivity;->escondeDialogoDeProgresso()V

    .line 216
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosSimulacaoActivity;->validarRespostaDaSimulacaoProposta(Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoPropostaSaidaVO;)Z

    move-result v2

    .line 218
    .local v2, "retornoValido":Z
    if-eqz v2, :cond_1a

    .line 220
    invoke-static {p0}, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorActivity;->intent(Landroid/content/Context;)Lcom/itau/empresas/feature/credito/giro/activity/GiroValorActivity_$IntentBuilder_;

    move-result-object v0

    .line 221
    invoke-virtual {v0, p1}, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorActivity_$IntentBuilder_;->produtosParceladosSimulacaoPropostaSaidaVO(Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoPropostaSaidaVO;)Lcom/itau/empresas/feature/credito/giro/activity/GiroValorActivity_$IntentBuilder_;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosSimulacaoActivity;->ofertaEspecialAceitaNovaRequisicao:Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;

    .line 222
    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorActivity_$IntentBuilder_;->ofertaEspecialAceita(Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;)Lcom/itau/empresas/feature/credito/giro/activity/GiroValorActivity_$IntentBuilder_;

    move-result-object v0

    .line 223
    invoke-virtual {v0}, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorActivity_$IntentBuilder_;->start()Lorg/androidannotations/api/builder/PostActivityStarter;

    .line 225
    :cond_1a
    return-void
.end method

.method public onEventMainThread(Lcom/itau/empresas/feature/credito/giro/model/ResultadoEfetivacaoEmprestimosParceladosVO;)V
    .registers 4
    .param p1, "efetivacao"    # Lcom/itau/empresas/feature/credito/giro/model/ResultadoEfetivacaoEmprestimosParceladosVO;

    .line 155
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosSimulacaoActivity;->escondeDialogoDeProgresso()V

    .line 157
    .line 158
    invoke-static {p0}, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosEfetivacaoActivity;->intent(Landroid/content/Context;)Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosEfetivacaoActivity_$IntentBuilder_;

    move-result-object v0

    .line 159
    invoke-virtual {v0, p1}, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosEfetivacaoActivity_$IntentBuilder_;->efetivacaoVO(Lcom/itau/empresas/feature/credito/giro/model/ResultadoEfetivacaoEmprestimosParceladosVO;)Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosEfetivacaoActivity_$IntentBuilder_;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosSimulacaoActivity;->simulacaoSaidaVO:Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoSaidaVO;

    .line 160
    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosEfetivacaoActivity_$IntentBuilder_;->simulacaoVO(Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoSaidaVO;)Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosEfetivacaoActivity_$IntentBuilder_;

    move-result-object v0

    iget-boolean v1, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosSimulacaoActivity;->temSeguro:Z

    .line 161
    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosEfetivacaoActivity_$IntentBuilder_;->temSeguro(Z)Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosEfetivacaoActivity_$IntentBuilder_;

    move-result-object v0

    iget-boolean v1, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosSimulacaoActivity;->devedorSolidario:Z

    .line 162
    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosEfetivacaoActivity_$IntentBuilder_;->devedorSolidario(Z)Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosEfetivacaoActivity_$IntentBuilder_;

    move-result-object v0

    .line 163
    invoke-virtual {v0}, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosEfetivacaoActivity_$IntentBuilder_;->start()Lorg/androidannotations/api/builder/PostActivityStarter;

    .line 164
    return-void
.end method

.method public opkeysConsumidas()Ljava/util/List;
    .registers 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Ljava/lang/String;>;"
        }
    .end annotation

    .line 99
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "creditoParceladoEfetivar"

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const-string v1, "creditoParceladoPersonalizadoSimular"

    const/4 v2, 0x1

    aput-object v1, v0, v2

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.class public final Lcom/itau/empresas/feature/credito/giro/activity/GiroValorActivity_;
.super Lcom/itau/empresas/feature/credito/giro/activity/GiroValorActivity;
.source "GiroValorActivity_.java"

# interfaces
.implements Lorg/androidannotations/api/view/HasViews;
.implements Lorg/androidannotations/api/view/OnViewChangedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/itau/empresas/feature/credito/giro/activity/GiroValorActivity_$IntentBuilder_;
    }
.end annotation


# instance fields
.field private final onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;


# direct methods
.method public constructor <init>()V
    .registers 2

    .line 39
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorActivity;-><init>()V

    .line 43
    new-instance v0, Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-direct {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    return-void
.end method

.method static synthetic access$001(Lcom/itau/empresas/feature/credito/giro/activity/GiroValorActivity_;)V
    .registers 1
    .param p0, "x0"    # Lcom/itau/empresas/feature/credito/giro/activity/GiroValorActivity_;

    .line 39
    invoke-super {p0}, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorActivity;->mostraDialogoDeProgresso()V

    return-void
.end method

.method static synthetic access$101(Lcom/itau/empresas/feature/credito/giro/activity/GiroValorActivity_;)V
    .registers 1
    .param p0, "x0"    # Lcom/itau/empresas/feature/credito/giro/activity/GiroValorActivity_;

    .line 39
    invoke-super {p0}, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorActivity;->escondeDialogoDeProgresso()V

    return-void
.end method

.method private init_(Landroid/os/Bundle;)V
    .registers 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 57
    new-instance v0, Lcom/itau/empresas/Preferences_;

    invoke-direct {v0, p0}, Lcom/itau/empresas/Preferences_;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorActivity_;->prefs:Lcom/itau/empresas/Preferences_;

    .line 58
    invoke-static {p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->registerOnViewChangedListener(Lorg/androidannotations/api/view/OnViewChangedListener;)V

    .line 59
    invoke-static {}, Lcom/itau/empresas/CustomApplication_;->getInstance()Lcom/itau/empresas/CustomApplication;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorActivity_;->application:Lcom/itau/empresas/CustomApplication;

    .line 60
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorActivity_;->injectExtras_()V

    .line 61
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorActivity_;->afterInject()V

    .line 62
    return-void
.end method

.method private injectExtras_()V
    .registers 3

    .line 116
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorActivity_;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 117
    .local v1, "extras_":Landroid/os/Bundle;
    if-eqz v1, :cond_2e

    .line 118
    const-string v0, "produtosParceladosSimulacaoPropostaSaidaVO"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 119
    const-string v0, "produtosParceladosSimulacaoPropostaSaidaVO"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoPropostaSaidaVO;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorActivity_;->produtosParceladosSimulacaoPropostaSaidaVO:Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoPropostaSaidaVO;

    .line 121
    :cond_1c
    const-string v0, "ofertaEspecialAceita"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2e

    .line 122
    const-string v0, "ofertaEspecialAceita"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorActivity_;->ofertaEspecialAceita:Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;

    .line 125
    :cond_2e
    return-void
.end method


# virtual methods
.method public escondeDialogoDeProgresso()V
    .registers 5

    .line 150
    const-string v0, ""

    new-instance v1, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorActivity_$3;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorActivity_$3;-><init>(Lcom/itau/empresas/feature/credito/giro/activity/GiroValorActivity_;)V

    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3}, Lorg/androidannotations/api/UiThreadExecutor;->runTask(Ljava/lang/String;Ljava/lang/Runnable;J)V

    .line 158
    return-void
.end method

.method public mostraDialogoDeProgresso()V
    .registers 5
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "CommitTransaction"
        }
    .end annotation

    .line 138
    const-string v0, ""

    new-instance v1, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorActivity_$2;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorActivity_$2;-><init>(Lcom/itau/empresas/feature/credito/giro/activity/GiroValorActivity_;)V

    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3}, Lorg/androidannotations/api/UiThreadExecutor;->runTask(Ljava/lang/String;Ljava/lang/Runnable;J)V

    .line 146
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 49
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-static {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    move-result-object v1

    .line 50
    .local v1, "previousNotifier":Lorg/androidannotations/api/view/OnViewChangedNotifier;
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorActivity_;->init_(Landroid/os/Bundle;)V

    .line 51
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorActivity;->onCreate(Landroid/os/Bundle;)V

    .line 52
    invoke-static {v1}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 53
    const v0, 0x7f03003b

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorActivity_;->setContentView(I)V

    .line 54
    return-void
.end method

.method public onViewChanged(Lorg/androidannotations/api/view/HasViews;)V
    .registers 4
    .param p1, "hasViews"    # Lorg/androidannotations/api/view/HasViews;

    .line 96
    const v0, 0x7f0e01ad

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorActivity_;->root:Landroid/widget/RelativeLayout;

    .line 97
    const v0, 0x7f0e01b1

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorActivity_;->campoValorDisponivel:Landroid/widget/TextView;

    .line 98
    const v0, 0x7f0e00b6

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/Toolbar;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorActivity_;->toolBar:Landroid/support/v7/widget/Toolbar;

    .line 99
    const v0, 0x7f0e018f

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorActivity_;->botaoContinuar:Landroid/widget/Button;

    .line 100
    const v0, 0x7f0e01b2

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/feature/credito/giro/customview/GiroPersonalizadoValoresView;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorActivity_;->giroPersonalizadoValoresView:Lcom/itau/empresas/feature/credito/giro/customview/GiroPersonalizadoValoresView;

    .line 101
    const v0, 0x7f0e01b3

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorActivity_;->textoValorAuxiliar:Landroid/widget/TextView;

    .line 102
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorActivity_;->botaoContinuar:Landroid/widget/Button;

    if-eqz v0, :cond_50

    .line 103
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorActivity_;->botaoContinuar:Landroid/widget/Button;

    new-instance v1, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorActivity_$1;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorActivity_$1;-><init>(Lcom/itau/empresas/feature/credito/giro/activity/GiroValorActivity_;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 112
    :cond_50
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorActivity_;->aoCarregarTela()V

    .line 113
    return-void
.end method

.method public setContentView(I)V
    .registers 3
    .param p1, "layoutResID"    # I

    .line 66
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorActivity;->setContentView(I)V

    .line 67
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 68
    return-void
.end method

.method public setContentView(Landroid/view/View;)V
    .registers 3
    .param p1, "view"    # Landroid/view/View;

    .line 78
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorActivity;->setContentView(Landroid/view/View;)V

    .line 79
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 80
    return-void
.end method

.method public setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .registers 4
    .param p1, "view"    # Landroid/view/View;
    .param p2, "params"    # Landroid/view/ViewGroup$LayoutParams;

    .line 72
    invoke-super {p0, p1, p2}, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorActivity;->setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 73
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 74
    return-void
.end method

.method public setIntent(Landroid/content/Intent;)V
    .registers 2
    .param p1, "newIntent"    # Landroid/content/Intent;

    .line 129
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorActivity;->setIntent(Landroid/content/Intent;)V

    .line 130
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorActivity_;->injectExtras_()V

    .line 131
    return-void
.end method

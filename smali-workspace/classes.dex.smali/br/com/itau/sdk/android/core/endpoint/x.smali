.class synthetic Lbr/com/itau/sdk/android/core/endpoint/x;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1008
    name = null
.end annotation


# static fields
.field static final synthetic a:[I


# direct methods
.method static constructor <clinit>()V
    .registers 4

    .line 60
    invoke-static {}, Lbr/com/itau/sdk/android/core/model/VersionControlType;->values()[Lbr/com/itau/sdk/android/core/model/VersionControlType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lbr/com/itau/sdk/android/core/endpoint/x;->a:[I

    :try_start_9
    sget-object v0, Lbr/com/itau/sdk/android/core/endpoint/x;->a:[I

    sget-object v1, Lbr/com/itau/sdk/android/core/model/VersionControlType;->UNAVAILABLE:Lbr/com/itau/sdk/android/core/model/VersionControlType;

    invoke-virtual {v1}, Lbr/com/itau/sdk/android/core/model/VersionControlType;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_14
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9 .. :try_end_14} :catch_15

    goto :goto_16

    :catch_15
    move-exception v3

    :goto_16
    :try_start_16
    sget-object v0, Lbr/com/itau/sdk/android/core/endpoint/x;->a:[I

    sget-object v1, Lbr/com/itau/sdk/android/core/model/VersionControlType;->REQUIRED:Lbr/com/itau/sdk/android/core/model/VersionControlType;

    invoke-virtual {v1}, Lbr/com/itau/sdk/android/core/model/VersionControlType;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_21
    .catch Ljava/lang/NoSuchFieldError; {:try_start_16 .. :try_end_21} :catch_22

    goto :goto_23

    :catch_22
    move-exception v3

    :goto_23
    :try_start_23
    sget-object v0, Lbr/com/itau/sdk/android/core/endpoint/x;->a:[I

    sget-object v1, Lbr/com/itau/sdk/android/core/model/VersionControlType;->OPTIONAL:Lbr/com/itau/sdk/android/core/model/VersionControlType;

    invoke-virtual {v1}, Lbr/com/itau/sdk/android/core/model/VersionControlType;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2e
    .catch Ljava/lang/NoSuchFieldError; {:try_start_23 .. :try_end_2e} :catch_2f

    goto :goto_30

    :catch_2f
    move-exception v3

    :goto_30
    return-void
.end method

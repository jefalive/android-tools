.class Lcom/itau/empresas/feature/comprovantes/controller/ComprovantesController$2;
.super Ljava/lang/Object;
.source "ComprovantesController.java"

# interfaces
.implements Lbr/com/itau/sdk/android/core/type/BackendCall;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/itau/empresas/feature/comprovantes/controller/ComprovantesController;->enviarComprovante(Ljava/lang/String;Lcom/itau/empresas/api/model/EnvioComprovanteVO;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/itau/empresas/feature/comprovantes/controller/ComprovantesController;

.field final synthetic val$envioComprovanteVO:Lcom/itau/empresas/api/model/EnvioComprovanteVO;

.field final synthetic val$idComprovante:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/itau/empresas/feature/comprovantes/controller/ComprovantesController;Ljava/lang/String;Lcom/itau/empresas/api/model/EnvioComprovanteVO;)V
    .registers 4
    .param p1, "this$0"    # Lcom/itau/empresas/feature/comprovantes/controller/ComprovantesController;

    .line 41
    iput-object p1, p0, Lcom/itau/empresas/feature/comprovantes/controller/ComprovantesController$2;->this$0:Lcom/itau/empresas/feature/comprovantes/controller/ComprovantesController;

    iput-object p2, p0, Lcom/itau/empresas/feature/comprovantes/controller/ComprovantesController$2;->val$idComprovante:Ljava/lang/String;

    iput-object p3, p0, Lcom/itau/empresas/feature/comprovantes/controller/ComprovantesController$2;->val$envioComprovanteVO:Lcom/itau/empresas/api/model/EnvioComprovanteVO;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public callBackend()V
    .registers 4

    .line 44
    iget-object v0, p0, Lcom/itau/empresas/feature/comprovantes/controller/ComprovantesController$2;->this$0:Lcom/itau/empresas/feature/comprovantes/controller/ComprovantesController;

    # invokes: Lcom/itau/empresas/feature/comprovantes/controller/ComprovantesController;->api()Ljava/lang/Object;
    invoke-static {v0}, Lcom/itau/empresas/feature/comprovantes/controller/ComprovantesController;->access$200(Lcom/itau/empresas/feature/comprovantes/controller/ComprovantesController;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/api/Api;

    iget-object v1, p0, Lcom/itau/empresas/feature/comprovantes/controller/ComprovantesController$2;->val$idComprovante:Ljava/lang/String;

    iget-object v2, p0, Lcom/itau/empresas/feature/comprovantes/controller/ComprovantesController$2;->val$envioComprovanteVO:Lcom/itau/empresas/api/model/EnvioComprovanteVO;

    invoke-interface {v0, v1, v2}, Lcom/itau/empresas/api/Api;->enviarComprovantePorEmail(Ljava/lang/String;Lcom/itau/empresas/api/model/EnvioComprovanteVO;)Lcom/itau/empresas/api/model/ResultadoComprovanteEnviadoVO;

    move-result-object v0

    # invokes: Lcom/itau/empresas/feature/comprovantes/controller/ComprovantesController;->post(Ljava/lang/Object;)V
    invoke-static {v0}, Lcom/itau/empresas/feature/comprovantes/controller/ComprovantesController;->access$300(Ljava/lang/Object;)V

    .line 45
    return-void
.end method

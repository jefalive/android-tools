.class public final Lcom/itau/empresas/ui/activity/LeitorBoletoActivity_;
.super Lcom/itau/empresas/ui/activity/LeitorBoletoActivity;
.source "LeitorBoletoActivity_.java"

# interfaces
.implements Lorg/androidannotations/api/view/HasViews;
.implements Lorg/androidannotations/api/view/OnViewChangedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/itau/empresas/ui/activity/LeitorBoletoActivity_$IntentBuilder_;
    }
.end annotation


# instance fields
.field private final onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;


# direct methods
.method public constructor <init>()V
    .registers 2

    .line 26
    invoke-direct {p0}, Lcom/itau/empresas/ui/activity/LeitorBoletoActivity;-><init>()V

    .line 30
    new-instance v0, Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-direct {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/ui/activity/LeitorBoletoActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    return-void
.end method

.method private init_(Landroid/os/Bundle;)V
    .registers 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 42
    invoke-static {p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->registerOnViewChangedListener(Lorg/androidannotations/api/view/OnViewChangedListener;)V

    .line 43
    return-void
.end method

.method public static intent(Landroid/content/Context;)Lcom/itau/empresas/ui/activity/LeitorBoletoActivity_$IntentBuilder_;
    .registers 2
    .param p0, "context"    # Landroid/content/Context;

    .line 64
    new-instance v0, Lcom/itau/empresas/ui/activity/LeitorBoletoActivity_$IntentBuilder_;

    invoke-direct {v0, p0}, Lcom/itau/empresas/ui/activity/LeitorBoletoActivity_$IntentBuilder_;-><init>(Landroid/content/Context;)V

    return-object v0
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .registers 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 34
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/LeitorBoletoActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-static {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    move-result-object v1

    .line 35
    .local v1, "previousNotifier":Lorg/androidannotations/api/view/OnViewChangedNotifier;
    invoke-direct {p0, p1}, Lcom/itau/empresas/ui/activity/LeitorBoletoActivity_;->init_(Landroid/os/Bundle;)V

    .line 36
    invoke-super {p0, p1}, Lcom/itau/empresas/ui/activity/LeitorBoletoActivity;->onCreate(Landroid/os/Bundle;)V

    .line 37
    invoke-static {v1}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 38
    const v0, 0x7f030043

    invoke-virtual {p0, v0}, Lcom/itau/empresas/ui/activity/LeitorBoletoActivity_;->setContentView(I)V

    .line 39
    return-void
.end method

.method public onViewChanged(Lorg/androidannotations/api/view/HasViews;)V
    .registers 4
    .param p1, "hasViews"    # Lorg/androidannotations/api/view/HasViews;

    .line 77
    const v0, 0x7f0e01e1

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 79
    .local v1, "view_button_barcode_confirm_input":Landroid/view/View;
    if-eqz v1, :cond_11

    .line 80
    new-instance v0, Lcom/itau/empresas/ui/activity/LeitorBoletoActivity_$1;

    invoke-direct {v0, p0}, Lcom/itau/empresas/ui/activity/LeitorBoletoActivity_$1;-><init>(Lcom/itau/empresas/ui/activity/LeitorBoletoActivity_;)V

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 89
    :cond_11
    return-void
.end method

.method public setContentView(I)V
    .registers 3
    .param p1, "layoutResID"    # I

    .line 47
    invoke-super {p0, p1}, Lcom/itau/empresas/ui/activity/LeitorBoletoActivity;->setContentView(I)V

    .line 48
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/LeitorBoletoActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 49
    return-void
.end method

.method public setContentView(Landroid/view/View;)V
    .registers 3
    .param p1, "view"    # Landroid/view/View;

    .line 59
    invoke-super {p0, p1}, Lcom/itau/empresas/ui/activity/LeitorBoletoActivity;->setContentView(Landroid/view/View;)V

    .line 60
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/LeitorBoletoActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 61
    return-void
.end method

.method public setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .registers 4
    .param p1, "view"    # Landroid/view/View;
    .param p2, "params"    # Landroid/view/ViewGroup$LayoutParams;

    .line 53
    invoke-super {p0, p1, p2}, Lcom/itau/empresas/ui/activity/LeitorBoletoActivity;->setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 54
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/LeitorBoletoActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 55
    return-void
.end method

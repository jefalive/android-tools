.class public Lcom/itau/empresas/ui/view/WidgetIrParaView;
.super Landroid/widget/RelativeLayout;
.source "WidgetIrParaView.java"


# instance fields
.field private final context:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 2
    .param p1, "context"    # Landroid/content/Context;

    .line 19
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 20
    iput-object p1, p0, Lcom/itau/empresas/ui/view/WidgetIrParaView;->context:Landroid/content/Context;

    .line 21
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .line 24
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 25
    iput-object p1, p0, Lcom/itau/empresas/ui/view/WidgetIrParaView;->context:Landroid/content/Context;

    .line 26
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .line 29
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 30
    iput-object p1, p0, Lcom/itau/empresas/ui/view/WidgetIrParaView;->context:Landroid/content/Context;

    .line 31
    return-void
.end method


# virtual methods
.method clickExtrato()V
    .registers 4

    .line 42
    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/WidgetIrParaView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/itau/empresas/feature/home/HomeLogadaActivity;->intent(Landroid/content/Context;)Lcom/itau/empresas/feature/home/HomeLogadaActivity_$IntentBuilder_;

    move-result-object v0

    const-string v1, "EXTRA_EXIBIR_EXTRATO"

    .line 43
    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/itau/empresas/feature/home/HomeLogadaActivity_$IntentBuilder_;->extra(Ljava/lang/String;Z)Lorg/androidannotations/api/builder/IntentBuilder;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/feature/home/HomeLogadaActivity_$IntentBuilder_;

    .line 44
    invoke-virtual {v0}, Lcom/itau/empresas/feature/home/HomeLogadaActivity_$IntentBuilder_;->start()Lorg/androidannotations/api/builder/PostActivityStarter;

    .line 45
    return-void
.end method

.method clickHome()V
    .registers 2

    .line 35
    iget-object v0, p0, Lcom/itau/empresas/ui/view/WidgetIrParaView;->context:Landroid/content/Context;

    .line 36
    invoke-static {v0}, Lcom/itau/empresas/feature/home/HomeLogadaActivity;->intent(Landroid/content/Context;)Lcom/itau/empresas/feature/home/HomeLogadaActivity_$IntentBuilder_;

    move-result-object v0

    .line 37
    invoke-virtual {v0}, Lcom/itau/empresas/feature/home/HomeLogadaActivity_$IntentBuilder_;->start()Lorg/androidannotations/api/builder/PostActivityStarter;

    .line 38
    return-void
.end method

.class public final enum Lcom/google/zxing/e;
.super Ljava/lang/Enum;


# static fields
.field public static final enum a:Lcom/google/zxing/e;

.field public static final enum b:Lcom/google/zxing/e;

.field public static final enum c:Lcom/google/zxing/e;

.field public static final enum d:Lcom/google/zxing/e;

.field public static final enum e:Lcom/google/zxing/e;

.field public static final enum f:Lcom/google/zxing/e;

.field public static final enum g:Lcom/google/zxing/e;

.field public static final enum h:Lcom/google/zxing/e;

.field public static final enum i:Lcom/google/zxing/e;

.field public static final enum j:Lcom/google/zxing/e;

.field public static final enum k:Lcom/google/zxing/e;

.field private static final synthetic m:[Lcom/google/zxing/e;


# instance fields
.field private final l:Ljava/lang/Class;


# direct methods
.method static constructor <clinit>()V
    .registers 4

    new-instance v0, Lcom/google/zxing/e;

    const-string v1, "OTHER"

    const-class v2, Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-direct {v0, v1, v3, v2}, Lcom/google/zxing/e;-><init>(Ljava/lang/String;ILjava/lang/Class;)V

    sput-object v0, Lcom/google/zxing/e;->a:Lcom/google/zxing/e;

    new-instance v0, Lcom/google/zxing/e;

    const-string v1, "PURE_BARCODE"

    const-class v2, Ljava/lang/Void;

    const/4 v3, 0x1

    invoke-direct {v0, v1, v3, v2}, Lcom/google/zxing/e;-><init>(Ljava/lang/String;ILjava/lang/Class;)V

    sput-object v0, Lcom/google/zxing/e;->b:Lcom/google/zxing/e;

    new-instance v0, Lcom/google/zxing/e;

    const-string v1, "POSSIBLE_FORMATS"

    const-class v2, Ljava/util/List;

    const/4 v3, 0x2

    invoke-direct {v0, v1, v3, v2}, Lcom/google/zxing/e;-><init>(Ljava/lang/String;ILjava/lang/Class;)V

    sput-object v0, Lcom/google/zxing/e;->c:Lcom/google/zxing/e;

    new-instance v0, Lcom/google/zxing/e;

    const-string v1, "TRY_HARDER"

    const-class v2, Ljava/lang/Void;

    const/4 v3, 0x3

    invoke-direct {v0, v1, v3, v2}, Lcom/google/zxing/e;-><init>(Ljava/lang/String;ILjava/lang/Class;)V

    sput-object v0, Lcom/google/zxing/e;->d:Lcom/google/zxing/e;

    new-instance v0, Lcom/google/zxing/e;

    const-string v1, "CHARACTER_SET"

    const-class v2, Ljava/lang/String;

    const/4 v3, 0x4

    invoke-direct {v0, v1, v3, v2}, Lcom/google/zxing/e;-><init>(Ljava/lang/String;ILjava/lang/Class;)V

    sput-object v0, Lcom/google/zxing/e;->e:Lcom/google/zxing/e;

    new-instance v0, Lcom/google/zxing/e;

    const-string v1, "ALLOWED_LENGTHS"

    const-class v2, [I

    const/4 v3, 0x5

    invoke-direct {v0, v1, v3, v2}, Lcom/google/zxing/e;-><init>(Ljava/lang/String;ILjava/lang/Class;)V

    sput-object v0, Lcom/google/zxing/e;->f:Lcom/google/zxing/e;

    new-instance v0, Lcom/google/zxing/e;

    const-string v1, "ASSUME_CODE_39_CHECK_DIGIT"

    const-class v2, Ljava/lang/Void;

    const/4 v3, 0x6

    invoke-direct {v0, v1, v3, v2}, Lcom/google/zxing/e;-><init>(Ljava/lang/String;ILjava/lang/Class;)V

    sput-object v0, Lcom/google/zxing/e;->g:Lcom/google/zxing/e;

    new-instance v0, Lcom/google/zxing/e;

    const-string v1, "ASSUME_GS1"

    const-class v2, Ljava/lang/Void;

    const/4 v3, 0x7

    invoke-direct {v0, v1, v3, v2}, Lcom/google/zxing/e;-><init>(Ljava/lang/String;ILjava/lang/Class;)V

    sput-object v0, Lcom/google/zxing/e;->h:Lcom/google/zxing/e;

    new-instance v0, Lcom/google/zxing/e;

    const-string v1, "RETURN_CODABAR_START_END"

    const-class v2, Ljava/lang/Void;

    const/16 v3, 0x8

    invoke-direct {v0, v1, v3, v2}, Lcom/google/zxing/e;-><init>(Ljava/lang/String;ILjava/lang/Class;)V

    sput-object v0, Lcom/google/zxing/e;->i:Lcom/google/zxing/e;

    new-instance v0, Lcom/google/zxing/e;

    const-string v1, "NEED_RESULT_POINT_CALLBACK"

    const-class v2, Lcom/google/zxing/n;

    const/16 v3, 0x9

    invoke-direct {v0, v1, v3, v2}, Lcom/google/zxing/e;-><init>(Ljava/lang/String;ILjava/lang/Class;)V

    sput-object v0, Lcom/google/zxing/e;->j:Lcom/google/zxing/e;

    new-instance v0, Lcom/google/zxing/e;

    const-string v1, "ALLOWED_EAN_EXTENSIONS"

    const-class v2, [I

    const/16 v3, 0xa

    invoke-direct {v0, v1, v3, v2}, Lcom/google/zxing/e;-><init>(Ljava/lang/String;ILjava/lang/Class;)V

    sput-object v0, Lcom/google/zxing/e;->k:Lcom/google/zxing/e;

    const/16 v0, 0xb

    new-array v0, v0, [Lcom/google/zxing/e;

    sget-object v1, Lcom/google/zxing/e;->a:Lcom/google/zxing/e;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/zxing/e;->b:Lcom/google/zxing/e;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/zxing/e;->c:Lcom/google/zxing/e;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/zxing/e;->d:Lcom/google/zxing/e;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/zxing/e;->e:Lcom/google/zxing/e;

    const/4 v2, 0x4

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/zxing/e;->f:Lcom/google/zxing/e;

    const/4 v2, 0x5

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/zxing/e;->g:Lcom/google/zxing/e;

    const/4 v2, 0x6

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/zxing/e;->h:Lcom/google/zxing/e;

    const/4 v2, 0x7

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/zxing/e;->i:Lcom/google/zxing/e;

    const/16 v2, 0x8

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/zxing/e;->j:Lcom/google/zxing/e;

    const/16 v2, 0x9

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/zxing/e;->k:Lcom/google/zxing/e;

    const/16 v2, 0xa

    aput-object v1, v0, v2

    sput-object v0, Lcom/google/zxing/e;->m:[Lcom/google/zxing/e;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/Class;)V
    .registers 4

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lcom/google/zxing/e;->l:Ljava/lang/Class;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/zxing/e;
    .registers 2

    const-class v0, Lcom/google/zxing/e;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/zxing/e;

    return-object v0
.end method

.method public static values()[Lcom/google/zxing/e;
    .registers 1

    sget-object v0, Lcom/google/zxing/e;->m:[Lcom/google/zxing/e;

    invoke-virtual {v0}, [Lcom/google/zxing/e;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/zxing/e;

    return-object v0
.end method


# virtual methods
.method public a()Ljava/lang/Class;
    .registers 2

    iget-object v0, p0, Lcom/google/zxing/e;->l:Ljava/lang/Class;

    return-object v0
.end method

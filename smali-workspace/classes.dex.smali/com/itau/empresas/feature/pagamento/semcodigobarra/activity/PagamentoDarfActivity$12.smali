.class Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity$12;
.super Ljava/lang/Object;
.source "PagamentoDarfActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->mostraDialogDuplicidade()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;

.field final synthetic val$dialog:Lcom/itau/empresas/ui/dialog/CustomAlertDialog;


# direct methods
.method constructor <init>(Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;Lcom/itau/empresas/ui/dialog/CustomAlertDialog;)V
    .registers 3
    .param p1, "this$0"    # Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;

    .line 859
    iput-object p1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity$12;->this$0:Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;

    iput-object p2, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity$12;->val$dialog:Lcom/itau/empresas/ui/dialog/CustomAlertDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .registers 4
    .param p1, "view"    # Landroid/view/View;

    .line 862
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity$12;->this$0:Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;

    const/4 v1, 0x1

    # invokes: Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->incluiPagamentoDARF(Z)V
    invoke-static {v0, v1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->access$1500(Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;Z)V

    .line 863
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity$12;->this$0:Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;

    iget-object v0, v0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->pagamentoDarfController:Lcom/itau/empresas/controller/PagamentoDarfController;

    invoke-virtual {v0}, Lcom/itau/empresas/controller/PagamentoDarfController;->incluiComDuplicidade()V

    .line 864
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity$12;->val$dialog:Lcom/itau/empresas/ui/dialog/CustomAlertDialog;

    invoke-virtual {v0}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog;->dismiss()V

    .line 865
    return-void
.end method

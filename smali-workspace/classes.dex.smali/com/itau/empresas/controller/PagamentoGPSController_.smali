.class public final Lcom/itau/empresas/controller/PagamentoGPSController_;
.super Lcom/itau/empresas/controller/PagamentoGPSController;
.source "PagamentoGPSController_.java"


# instance fields
.field private context_:Landroid/content/Context;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .registers 2
    .param p1, "context"    # Landroid/content/Context;

    .line 19
    invoke-direct {p0}, Lcom/itau/empresas/controller/PagamentoGPSController;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/itau/empresas/controller/PagamentoGPSController_;->context_:Landroid/content/Context;

    .line 21
    invoke-direct {p0}, Lcom/itau/empresas/controller/PagamentoGPSController_;->init_()V

    .line 22
    return-void
.end method

.method public static getInstance_(Landroid/content/Context;)Lcom/itau/empresas/controller/PagamentoGPSController_;
    .registers 2
    .param p0, "context"    # Landroid/content/Context;

    .line 25
    new-instance v0, Lcom/itau/empresas/controller/PagamentoGPSController_;

    invoke-direct {v0, p0}, Lcom/itau/empresas/controller/PagamentoGPSController_;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method private init_()V
    .registers 2

    .line 29
    invoke-static {}, Lcom/itau/empresas/CustomApplication_;->getInstance()Lcom/itau/empresas/CustomApplication;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/controller/PagamentoGPSController_;->app:Lcom/itau/empresas/CustomApplication;

    .line 30
    iget-object v0, p0, Lcom/itau/empresas/controller/PagamentoGPSController_;->context_:Landroid/content/Context;

    invoke-static {v0}, Lcom/itau/empresas/ui/util/validacao/ValidaEditText_;->getInstance_(Landroid/content/Context;)Lcom/itau/empresas/ui/util/validacao/ValidaEditText_;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/controller/PagamentoGPSController_;->validaEditText:Lcom/itau/empresas/ui/util/validacao/ValidaEditText;

    .line 31
    return-void
.end method

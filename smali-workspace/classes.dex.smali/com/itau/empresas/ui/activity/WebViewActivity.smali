.class public Lcom/itau/empresas/ui/activity/WebViewActivity;
.super Lcom/itau/empresas/ui/activity/BaseActivity;
.source "WebViewActivity.java"

# interfaces
.implements Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment$Listener;
.implements Landroid/view/View$OnClickListener;


# instance fields
.field base:Ljava/lang/String;

.field chaveMobile:Ljava/lang/String;

.field contexto:Landroid/content/Context;

.field link:Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment$OnLocaStorageWebviewChangeListener;

.field private loadingFragment:Lcom/itau/empresas/ui/fragment/LoadingFragment;

.field nomeMenu:Ljava/lang/String;

.field private tag:Ljava/lang/String;

.field toolBar:Landroid/support/v7/widget/Toolbar;

.field webviewfragment:Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;


# direct methods
.method public constructor <init>()V
    .registers 2

    .line 51
    invoke-direct {p0}, Lcom/itau/empresas/ui/activity/BaseActivity;-><init>()V

    .line 71
    new-instance v0, Lcom/itau/empresas/ui/activity/WebViewActivity$1;

    invoke-direct {v0, p0}, Lcom/itau/empresas/ui/activity/WebViewActivity$1;-><init>(Lcom/itau/empresas/ui/activity/WebViewActivity;)V

    iput-object v0, p0, Lcom/itau/empresas/ui/activity/WebViewActivity;->link:Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment$OnLocaStorageWebviewChangeListener;

    .line 79
    const-string v0, "loading"

    iput-object v0, p0, Lcom/itau/empresas/ui/activity/WebViewActivity;->tag:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$000(Lcom/itau/empresas/ui/activity/WebViewActivity;Ljava/lang/String;)V
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/ui/activity/WebViewActivity;
    .param p1, "x1"    # Ljava/lang/String;

    .line 51
    invoke-direct {p0, p1}, Lcom/itau/empresas/ui/activity/WebViewActivity;->compartilharComprovanteListener(Ljava/lang/String;)V

    return-void
.end method

.method private apresentaDialogoAtualizarWebView(Ljava/lang/String;)V
    .registers 5
    .param p1, "pacote"    # Ljava/lang/String;

    .line 236
    new-instance v0, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;

    invoke-direct {v0, p0}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget-object v1, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Estados;->DOIS_BOTOES:Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Estados;

    .line 237
    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;->estado(Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Estados;)Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;

    move-result-object v0

    const-string v1, "Identificamos que a fun\u00e7\u00e3o WebView do seu sistema Android est\u00e1 desatualizada.\nPara melhor experi\u00eancia no app Ita\u00fa Empresas recomendamos a sua atualiza\u00e7\u00e3o na Google Play."

    .line 238
    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;->mensagem(Ljava/lang/String;)Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;

    move-result-object v0

    .line 242
    const v1, 0x7f070175

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;->textoNegativo(I)Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;

    move-result-object v0

    .line 243
    const v1, 0x7f070376

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;->textoPositivo(I)Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;

    move-result-object v0

    .line 244
    invoke-virtual {v0}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;->build()Lcom/itau/empresas/ui/dialog/CustomAlertDialog;

    move-result-object v2

    .line 246
    .local v2, "dialog":Lcom/itau/empresas/ui/dialog/CustomAlertDialog;
    new-instance v0, Lcom/itau/empresas/ui/activity/WebViewActivity$2;

    invoke-direct {v0, p0, p1, v2}, Lcom/itau/empresas/ui/activity/WebViewActivity$2;-><init>(Lcom/itau/empresas/ui/activity/WebViewActivity;Ljava/lang/String;Lcom/itau/empresas/ui/dialog/CustomAlertDialog;)V

    invoke-virtual {v2, v0}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog;->setPositivoListener(Landroid/view/View$OnClickListener;)V

    .line 263
    new-instance v0, Lcom/itau/empresas/ui/activity/WebViewActivity$3;

    invoke-direct {v0, p0, v2}, Lcom/itau/empresas/ui/activity/WebViewActivity$3;-><init>(Lcom/itau/empresas/ui/activity/WebViewActivity;Lcom/itau/empresas/ui/dialog/CustomAlertDialog;)V

    invoke-virtual {v2, v0}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog;->setNegativoListener(Landroid/view/View$OnClickListener;)V

    .line 269
    invoke-virtual {v2, p0}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog;->show(Landroid/support/v7/app/AppCompatActivity;)Lcom/itau/empresas/ui/dialog/BaseDialog;

    .line 270
    return-void
.end method

.method private compartilhaPrintTelaComprovante(Landroid/graphics/Bitmap;)V
    .registers 5
    .param p1, "btm"    # Landroid/graphics/Bitmap;

    .line 304
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/WebViewActivity;->contexto:Landroid/content/Context;

    invoke-static {v0}, Lbr/com/itau/sdk/android/core/webview/LocalStorageWebview;->getInstance(Landroid/content/Context;)Lbr/com/itau/sdk/android/core/webview/LocalStorageWebview;

    move-result-object v0

    const-string v1, "compartilhar"

    invoke-virtual {v0, v1}, Lbr/com/itau/sdk/android/core/webview/LocalStorageWebview;->remove(Ljava/lang/String;)V

    .line 305
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/itau/empresas/ui/activity/WebViewActivity;->setBase(Ljava/lang/String;)V

    .line 307
    const-string v0, "compartilhar_comprovante"

    .line 308
    :try_start_11
    invoke-static {p0, p1, v0}, Lcom/itau/empresas/ui/util/BitmapUtils;->salvarPrintWebView(Landroid/content/Context;Landroid/graphics/Bitmap;Ljava/lang/String;)Ljava/io/File;

    move-result-object v2

    .line 309
    .local v2, "file":Ljava/io/File;
    invoke-static {p0, v2}, Lcom/itau/empresas/ui/util/CompartilhamentoUtils;->compartilharArquivoImagem(Landroid/app/Activity;Ljava/io/File;)V
    :try_end_18
    .catch Ljava/lang/Exception; {:try_start_11 .. :try_end_18} :catch_19

    .line 314
    .end local v2    # "file":Ljava/io/File;
    goto :goto_30

    .line 310
    :catch_19
    move-exception v2

    .line 311
    .local v2, "e":Ljava/lang/Exception;
    const v0, 0x7f07046e

    invoke-virtual {p0, v0}, Lcom/itau/empresas/ui/activity/WebViewActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/itau/empresas/ui/activity/WebViewActivity;->exibeAvisoDeErro(Ljava/lang/String;)V

    .line 312
    const-string v0, "webview"

    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 313
    invoke-static {v2}, Lcom/crashlytics/android/Crashlytics;->logException(Ljava/lang/Throwable;)V

    .line 315
    .end local v2    # "e":Ljava/lang/Exception;
    :goto_30
    return-void
.end method

.method private compartilharComprovanteListener(Ljava/lang/String;)V
    .registers 4
    .param p1, "base"    # Ljava/lang/String;

    .line 278
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/WebViewActivity;->contexto:Landroid/content/Context;

    invoke-static {v0}, Lbr/com/itau/sdk/android/core/webview/LocalStorageWebview;->getInstance(Landroid/content/Context;)Lbr/com/itau/sdk/android/core/webview/LocalStorageWebview;

    move-result-object v0

    const-string v1, "compartilhar"

    invoke-virtual {v0, v1}, Lbr/com/itau/sdk/android/core/webview/LocalStorageWebview;->findItem(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3b

    .line 279
    .line 280
    invoke-static {}, Lcom/itau/empresas/ui/util/PermissoesUtil;->getPermissionWriteExternalStorage()[Ljava/lang/String;

    move-result-object v0

    .line 279
    invoke-static {p0, v0}, Lcom/itau/empresas/ui/util/PermissoesUtil;->hasSelfPermissions(Landroid/content/Context;[Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_34

    .line 281
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x17

    if-lt v0, v1, :cond_3b

    .line 282
    invoke-virtual {p0, p1}, Lcom/itau/empresas/ui/activity/WebViewActivity;->setBase(Ljava/lang/String;)V

    .line 283
    invoke-static {}, Lcom/itau/empresas/ui/util/PermissoesUtil;->getPermissionWriteExternalStorage()[Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/itau/empresas/ui/activity/WebViewActivity;->requestPermissions([Ljava/lang/String;I)V

    .line 285
    const/4 v0, 0x0

    new-array v0, v0, [Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/itau/empresas/ui/activity/WebViewActivity;->registraView([Landroid/view/View;)V

    goto :goto_3b

    .line 288
    :cond_34
    invoke-static {p1}, Lcom/itau/empresas/ui/util/BitmapUtils;->getBitmapFromBase64(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/itau/empresas/ui/activity/WebViewActivity;->compartilhaPrintTelaComprovante(Landroid/graphics/Bitmap;)V

    .line 291
    :cond_3b
    :goto_3b
    return-void
.end method

.method private constroiToolbar()V
    .registers 3

    .line 341
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/WebViewActivity;->toolBar:Landroid/support/v7/widget/Toolbar;

    invoke-static {v0, p0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom;->Builder(Landroid/support/v7/widget/Toolbar;Landroid/support/v7/app/AppCompatActivity;)Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/ui/activity/WebViewActivity;->nomeMenu:Ljava/lang/String;

    .line 342
    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->comTitulo(Ljava/lang/String;)Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    move-result-object v0

    .line 343
    invoke-virtual {v0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->comClickListenerFinish()Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    move-result-object v0

    .line 344
    invoke-virtual {v0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->build()Lcom/itau/empresas/ui/util/listener/ToolbarCustom;

    .line 345
    return-void
.end method

.method private exibeAvisoDeErro(Ljava/lang/String;)V
    .registers 5
    .param p1, "message"    # Ljava/lang/String;

    .line 318
    new-instance v2, Lbr/com/itau/widgets/hintview/Hint$Builder;

    iget-object v0, p0, Lcom/itau/empresas/ui/activity/WebViewActivity;->webviewfragment:Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;

    invoke-virtual {v0}, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->getView()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f09011b

    invoke-direct {v2, v0, v1}, Lbr/com/itau/widgets/hintview/Hint$Builder;-><init>(Landroid/view/View;I)V

    .line 319
    .local v2, "hint":Lbr/com/itau/widgets/hintview/Hint$Builder;
    const/16 v0, 0x30

    invoke-static {v2, p1, v0}, Lcom/itau/empresas/ui/util/HintUtils;->montaHintAlerta(Lbr/com/itau/widgets/hintview/Hint$Builder;Ljava/lang/String;I)Lbr/com/itau/widgets/hintview/Hint$Builder;

    move-result-object v2

    .line 323
    new-instance v0, Lcom/itau/empresas/ui/activity/WebViewActivity$4;

    invoke-direct {v0, p0}, Lcom/itau/empresas/ui/activity/WebViewActivity$4;-><init>(Lcom/itau/empresas/ui/activity/WebViewActivity;)V

    invoke-virtual {v2, v0}, Lbr/com/itau/widgets/hintview/Hint$Builder;->setOnClickListener(Lbr/com/itau/widgets/hintview/OnClickListener;)Lbr/com/itau/widgets/hintview/Hint$Builder;

    .line 329
    invoke-virtual {v2}, Lbr/com/itau/widgets/hintview/Hint$Builder;->show()Lbr/com/itau/widgets/hintview/Hint;

    .line 330
    return-void
.end method

.method public static iniciarWebViewActivity(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .registers 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "chaveMobile"    # Ljava/lang/String;
    .param p2, "titulo"    # Ljava/lang/String;

    .line 85
    invoke-static {p0}, Lcom/itau/empresas/ui/activity/WebViewActivity_;->intent(Landroid/content/Context;)Lcom/itau/empresas/ui/activity/WebViewActivity_$IntentBuilder_;

    move-result-object v0

    .line 86
    invoke-virtual {v0, p1}, Lcom/itau/empresas/ui/activity/WebViewActivity_$IntentBuilder_;->chaveMobile(Ljava/lang/String;)Lcom/itau/empresas/ui/activity/WebViewActivity_$IntentBuilder_;

    move-result-object v0

    .line 87
    invoke-virtual {v0, p2}, Lcom/itau/empresas/ui/activity/WebViewActivity_$IntentBuilder_;->nomeMenu(Ljava/lang/String;)Lcom/itau/empresas/ui/activity/WebViewActivity_$IntentBuilder_;

    move-result-object v0

    .line 88
    invoke-virtual {v0}, Lcom/itau/empresas/ui/activity/WebViewActivity_$IntentBuilder_;->start()Lorg/androidannotations/api/builder/PostActivityStarter;

    .line 89
    return-void
.end method

.method private verificaVersaoWebView()V
    .registers 9

    .line 211
    const/4 v3, 0x0

    .line 212
    .local v3, "packageInfo":Landroid/content/pm/PackageInfo;
    const-string v4, "com.google.android.webview"

    .line 214
    .local v4, "pacote":Ljava/lang/String;
    :try_start_3
    invoke-virtual {p0}, Lcom/itau/empresas/ui/activity/WebViewActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v1, "com.google.android.webview"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v3

    .line 215
    iget-object v5, v3, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    .line 216
    .local v5, "versao":Ljava/lang/String;
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/WebViewActivity;->application:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "WBV"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v6

    .line 217
    .local v6, "qtdAcesso":I
    if-eqz v5, :cond_6b

    .line 218
    const-string v0, " "

    invoke-virtual {v5, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    aget-object v0, v0, v1

    const-string v1, "."

    const-string v2, ";"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    const-string v1, ";"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    aget-object v7, v0, v1

    .line 219
    .local v7, "codigoVersao":Ljava/lang/String;
    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    const/16 v1, 0x36

    if-ge v0, v1, :cond_6a

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_6a

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x18

    if-ge v0, v1, :cond_6a

    const/4 v0, 0x3

    if-ge v6, v0, :cond_6a

    .line 221
    const-string v0, "com.google.android.webview"

    invoke-direct {p0, v0}, Lcom/itau/empresas/ui/activity/WebViewActivity;->apresentaDialogoAtualizarWebView(Ljava/lang/String;)V

    .line 222
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/WebViewActivity;->application:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "WBV"

    add-int/lit8 v2, v6, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 224
    .end local v7    # "codigoVersao":Ljava/lang/String;
    :cond_6a
    goto :goto_85

    .line 225
    :cond_6b
    const-string v0, "com.google.android.webview"

    invoke-direct {p0, v0}, Lcom/itau/empresas/ui/activity/WebViewActivity;->apresentaDialogoAtualizarWebView(Ljava/lang/String;)V

    .line 226
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/WebViewActivity;->application:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "WBV"

    add-int/lit8 v2, v6, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V
    :try_end_85
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_3 .. :try_end_85} :catch_86

    .line 232
    .end local v5    # "versao":Ljava/lang/String;
    .end local v6    # "qtdAcesso":I
    :goto_85
    goto :goto_93

    .line 229
    :catch_86
    move-exception v5

    .line 230
    .local v5, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v0, "webview"

    invoke-virtual {v5}, Landroid/content/pm/PackageManager$NameNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 231
    invoke-virtual {v5}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    .line 233
    .end local v5    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :goto_93
    return-void
.end method


# virtual methods
.method aoCarregarWebView()V
    .registers 3

    .line 93
    iput-object p0, p0, Lcom/itau/empresas/ui/activity/WebViewActivity;->contexto:Landroid/content/Context;

    .line 94
    invoke-direct {p0}, Lcom/itau/empresas/ui/activity/WebViewActivity;->verificaVersaoWebView()V

    .line 95
    invoke-direct {p0}, Lcom/itau/empresas/ui/activity/WebViewActivity;->constroiToolbar()V

    .line 96
    invoke-virtual {p0}, Lcom/itau/empresas/ui/activity/WebViewActivity;->configuraParametrosWebView()V

    .line 98
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/WebViewActivity;->webviewfragment:Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;

    .line 99
    invoke-virtual {v0, p0}, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->withListener(Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment$Listener;)Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/ui/activity/WebViewActivity;->chaveMobile:Ljava/lang/String;

    .line 100
    invoke-virtual {v0, v1}, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->init(Ljava/lang/String;)Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;

    .line 102
    invoke-virtual {p0}, Lcom/itau/empresas/ui/activity/WebViewActivity;->mostraLoading()V

    .line 103
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/WebViewActivity;->webviewfragment:Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;

    iget-object v1, p0, Lcom/itau/empresas/ui/activity/WebViewActivity;->link:Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment$OnLocaStorageWebviewChangeListener;

    invoke-virtual {v0, v1}, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->registerOnLocalstoragewebviewChangeListener(Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment$OnLocaStorageWebviewChangeListener;)Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;

    .line 104
    return-void
.end method

.method protected configuraParametrosWebView()V
    .registers 4

    .line 168
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/WebViewActivity;->webviewfragment:Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;

    const-string v1, "isKony"

    const-string v2, "S"

    .line 169
    invoke-virtual {v0, v1, v2}, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->addParameter(Ljava/lang/String;Ljava/lang/String;)Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;

    move-result-object v0

    const-string v1, "versaoAppKony"

    const-string v2, "4.0"

    .line 170
    invoke-virtual {v0, v1, v2}, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->addParameter(Ljava/lang/String;Ljava/lang/String;)Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;

    .line 171
    return-void
.end method

.method public escondeLoading()V
    .registers 5

    .line 197
    :try_start_0
    invoke-virtual {p0}, Lcom/itau/empresas/ui/activity/WebViewActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->popBackStack()V

    .line 198
    invoke-virtual {p0}, Lcom/itau/empresas/ui/activity/WebViewActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/ui/activity/WebViewActivity;->tag:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    if-eqz v0, :cond_2c

    .line 199
    invoke-virtual {p0}, Lcom/itau/empresas/ui/activity/WebViewActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    .line 200
    invoke-virtual {p0}, Lcom/itau/empresas/ui/activity/WebViewActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    iget-object v2, p0, Lcom/itau/empresas/ui/activity/WebViewActivity;->tag:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentTransaction;->remove(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    .line 201
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 204
    :cond_2c
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/itau/empresas/ui/activity/WebViewActivity;->loadingFragment:Lcom/itau/empresas/ui/fragment/LoadingFragment;
    :try_end_2f
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_2f} :catch_30

    .line 207
    goto :goto_3a

    .line 205
    :catch_30
    move-exception v3

    .line 206
    .local v3, "ignored":Ljava/lang/Exception;
    const-string v0, "webview"

    invoke-virtual {v3}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 208
    .end local v3    # "ignored":Ljava/lang/Exception;
    :goto_3a
    return-void
.end method

.method public getBase()Ljava/lang/String;
    .registers 2

    .line 333
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/WebViewActivity;->base:Ljava/lang/String;

    return-object v0
.end method

.method public mostraLoading()V
    .registers 6

    .line 179
    :try_start_0
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/WebViewActivity;->loadingFragment:Lcom/itau/empresas/ui/fragment/LoadingFragment;

    if-nez v0, :cond_24

    .line 180
    invoke-static {}, Lcom/itau/empresas/ui/fragment/LoadingFragment_;->builder()Lcom/itau/empresas/ui/fragment/LoadingFragment_$FragmentBuilder_;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/ui/fragment/LoadingFragment_$FragmentBuilder_;->build()Lcom/itau/empresas/ui/fragment/LoadingFragment;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/ui/activity/WebViewActivity;->loadingFragment:Lcom/itau/empresas/ui/fragment/LoadingFragment;

    .line 181
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/WebViewActivity;->loadingFragment:Lcom/itau/empresas/ui/fragment/LoadingFragment;

    if-eqz v0, :cond_24

    .line 182
    .line 183
    invoke-virtual {p0}, Lcom/itau/empresas/ui/activity/WebViewActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/ui/activity/WebViewActivity;->loadingFragment:Lcom/itau/empresas/ui/fragment/LoadingFragment;

    iget-object v2, p0, Lcom/itau/empresas/ui/activity/WebViewActivity;->tag:Ljava/lang/String;

    .line 182
    const v3, 0x1020002

    invoke-static {v0, v3, v1, v2}, Lcom/itau/empresas/ui/util/FragmentUtils;->attachFragment(Landroid/support/v4/app/FragmentTransaction;ILcom/itau/empresas/ui/fragment/BaseFragment;Ljava/lang/String;)V
    :try_end_24
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_24} :catch_25

    .line 192
    :cond_24
    goto :goto_2f

    .line 190
    :catch_25
    move-exception v4

    .line 191
    .local v4, "ignored":Ljava/lang/Exception;
    const-string v0, "webview"

    invoke-virtual {v4}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 193
    .end local v4    # "ignored":Ljava/lang/Exception;
    :goto_2f
    return-void
.end method

.method public onBackPressed()V
    .registers 1

    .line 108
    invoke-virtual {p0}, Lcom/itau/empresas/ui/activity/WebViewActivity;->finish()V

    .line 109
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .registers 2
    .param p1, "v"    # Landroid/view/View;

    .line 274
    invoke-virtual {p0}, Lcom/itau/empresas/ui/activity/WebViewActivity;->onBackPressed()V

    .line 275
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .registers 3
    .param p1, "menu"    # Landroid/view/Menu;

    .line 156
    invoke-static {p0, p1}, Lcom/itau/empresas/ui/util/PoyntUtils;->onCreateOptionsMenu(Landroid/app/Activity;Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .registers 3
    .param p1, "item"    # Landroid/view/MenuItem;

    .line 161
    invoke-static {p0, p1}, Lcom/itau/empresas/ui/util/PoyntUtils;->onOptionsItemSelected(Lcom/itau/empresas/ui/activity/BaseActivity;Landroid/view/MenuItem;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 162
    const/4 v0, 0x1

    return v0

    .line 164
    :cond_8
    invoke-super {p0, p1}, Lcom/itau/empresas/ui/activity/BaseActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0
.end method

.method public onPageError()V
    .registers 1

    .line 135
    invoke-virtual {p0}, Lcom/itau/empresas/ui/activity/WebViewActivity;->escondeLoading()V

    .line 136
    return-void
.end method

.method public onPageFinished()V
    .registers 1

    .line 130
    invoke-virtual {p0}, Lcom/itau/empresas/ui/activity/WebViewActivity;->escondeLoading()V

    .line 131
    return-void
.end method

.method public onPageStarted()V
    .registers 1

    .line 125
    invoke-virtual {p0}, Lcom/itau/empresas/ui/activity/WebViewActivity;->mostraLoading()V

    .line 126
    return-void
.end method

.method protected onPause()V
    .registers 1

    .line 113
    invoke-super {p0}, Lcom/itau/empresas/ui/activity/BaseActivity;->onPause()V

    .line 114
    invoke-virtual {p0}, Lcom/itau/empresas/ui/activity/WebViewActivity;->escondeLoading()V

    .line 115
    return-void
.end method

.method public onRequestPermissionsResult(I[Ljava/lang/String;[I)V
    .registers 5
    .param p1, "requestCode"    # I
    .param p2, "permissions"    # [Ljava/lang/String;
    .param p3, "grantResults"    # [I

    .line 296
    invoke-super {p0, p1, p2, p3}, Lcom/itau/empresas/ui/activity/BaseActivity;->onRequestPermissionsResult(I[Ljava/lang/String;[I)V

    .line 297
    const/4 v0, 0x4

    if-ne p1, v0, :cond_1b

    .line 298
    invoke-static {}, Lcom/itau/empresas/ui/util/PermissoesUtil;->getPermissionWriteExternalStorage()[Ljava/lang/String;

    move-result-object v0

    .line 297
    invoke-static {p0, v0}, Lcom/itau/empresas/ui/util/PermissoesUtil;->hasSelfPermissions(Landroid/content/Context;[Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1b

    .line 299
    invoke-virtual {p0}, Lcom/itau/empresas/ui/activity/WebViewActivity;->getBase()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/itau/empresas/ui/util/BitmapUtils;->getBitmapFromBase64(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/itau/empresas/ui/activity/WebViewActivity;->compartilhaPrintTelaComprovante(Landroid/graphics/Bitmap;)V

    .line 301
    :cond_1b
    return-void
.end method

.method protected onResume()V
    .registers 1

    .line 140
    invoke-super {p0}, Lcom/itau/empresas/ui/activity/BaseActivity;->onResume()V

    .line 141
    invoke-virtual {p0}, Lcom/itau/empresas/ui/activity/WebViewActivity;->escondeLoading()V

    .line 142
    return-void
.end method

.method protected onStop()V
    .registers 1

    .line 119
    invoke-super {p0}, Lcom/itau/empresas/ui/activity/BaseActivity;->onStop()V

    .line 120
    invoke-virtual {p0}, Lcom/itau/empresas/ui/activity/WebViewActivity;->escondeLoading()V

    .line 121
    return-void
.end method

.method public opkeysConsumidas()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Ljava/lang/String;>;"
        }
    .end annotation

    .line 151
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public pageTitle(Ljava/lang/String;)V
    .registers 2
    .param p1, "s"    # Ljava/lang/String;

    .line 147
    return-void
.end method

.method public setBase(Ljava/lang/String;)V
    .registers 2
    .param p1, "base"    # Ljava/lang/String;

    .line 337
    iput-object p1, p0, Lcom/itau/empresas/ui/activity/WebViewActivity;->base:Ljava/lang/String;

    .line 338
    return-void
.end method

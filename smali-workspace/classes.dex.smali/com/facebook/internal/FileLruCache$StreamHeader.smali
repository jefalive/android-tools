.class final Lcom/facebook/internal/FileLruCache$StreamHeader;
.super Ljava/lang/Object;
.source "FileLruCache.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/facebook/internal/FileLruCache;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "StreamHeader"
.end annotation


# direct methods
.method static readHeader(Ljava/io/InputStream;)Lorg/json/JSONObject;
    .registers 13
    .param p0, "stream"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 418
    invoke-virtual {p0}, Ljava/io/InputStream;->read()I

    move-result v4

    .line 419
    .local v4, "version":I
    if-eqz v4, :cond_8

    .line 420
    const/4 v0, 0x0

    return-object v0

    .line 423
    :cond_8
    const/4 v5, 0x0

    .line 424
    .local v5, "headerSize":I
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_a
    const/4 v0, 0x3

    if-ge v6, v0, :cond_27

    .line 425
    invoke-virtual {p0}, Ljava/io/InputStream;->read()I

    move-result v7

    .line 426
    .local v7, "b":I
    const/4 v0, -0x1

    if-ne v7, v0, :cond_1f

    .line 427
    sget-object v0, Lcom/facebook/LoggingBehavior;->CACHE:Lcom/facebook/LoggingBehavior;

    sget-object v1, Lcom/facebook/internal/FileLruCache;->TAG:Ljava/lang/String;

    const-string v2, "readHeader: stream.read returned -1 while reading header size"

    invoke-static {v0, v1, v2}, Lcom/facebook/internal/Logger;->log(Lcom/facebook/LoggingBehavior;Ljava/lang/String;Ljava/lang/String;)V

    .line 429
    const/4 v0, 0x0

    return-object v0

    .line 431
    :cond_1f
    shl-int/lit8 v5, v5, 0x8

    .line 432
    and-int/lit16 v0, v7, 0xff

    add-int/2addr v5, v0

    .line 424
    .end local v7    # "b":I
    add-int/lit8 v6, v6, 0x1

    goto :goto_a

    .line 435
    .end local v6    # "i":I
    :cond_27
    new-array v6, v5, [B

    .line 436
    .local v6, "headerBytes":[B
    const/4 v7, 0x0

    .line 437
    .local v7, "count":I
    :goto_2a
    array-length v0, v6

    if-ge v7, v0, :cond_63

    .line 438
    array-length v0, v6

    sub-int/2addr v0, v7

    invoke-virtual {p0, v6, v7, v0}, Ljava/io/InputStream;->read([BII)I

    move-result v8

    .line 439
    .local v8, "readCount":I
    const/4 v0, 0x1

    if-ge v8, v0, :cond_61

    .line 440
    sget-object v0, Lcom/facebook/LoggingBehavior;->CACHE:Lcom/facebook/LoggingBehavior;

    sget-object v1, Lcom/facebook/internal/FileLruCache;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "readHeader: stream.read stopped at "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " when expected "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    array-length v3, v6

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/facebook/internal/Logger;->log(Lcom/facebook/LoggingBehavior;Ljava/lang/String;Ljava/lang/String;)V

    .line 445
    const/4 v0, 0x0

    return-object v0

    .line 447
    :cond_61
    add-int/2addr v7, v8

    .line 448
    .end local v8    # "readCount":I
    goto :goto_2a

    .line 450
    :cond_63
    new-instance v8, Ljava/lang/String;

    invoke-direct {v8, v6}, Ljava/lang/String;-><init>([B)V

    .line 451
    .local v8, "headerString":Ljava/lang/String;
    const/4 v9, 0x0

    .line 452
    .local v9, "header":Lorg/json/JSONObject;
    new-instance v10, Lorg/json/JSONTokener;

    invoke-direct {v10, v8}, Lorg/json/JSONTokener;-><init>(Ljava/lang/String;)V

    .line 454
    .local v10, "tokener":Lorg/json/JSONTokener;
    :try_start_6e
    invoke-virtual {v10}, Lorg/json/JSONTokener;->nextValue()Ljava/lang/Object;

    move-result-object v11

    .line 455
    .local v11, "parsed":Ljava/lang/Object;
    instance-of v0, v11, Lorg/json/JSONObject;

    if-nez v0, :cond_9a

    .line 456
    sget-object v0, Lcom/facebook/LoggingBehavior;->CACHE:Lcom/facebook/LoggingBehavior;

    sget-object v1, Lcom/facebook/internal/FileLruCache;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "readHeader: expected JSONObject, got "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v11}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/facebook/internal/Logger;->log(Lcom/facebook/LoggingBehavior;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_98
    .catch Lorg/json/JSONException; {:try_start_6e .. :try_end_98} :catch_9e

    .line 461
    const/4 v0, 0x0

    return-object v0

    .line 463
    :cond_9a
    move-object v9, v11

    :try_start_9b
    check-cast v9, Lorg/json/JSONObject;
    :try_end_9d
    .catch Lorg/json/JSONException; {:try_start_9b .. :try_end_9d} :catch_9e

    .line 466
    .end local v11    # "parsed":Ljava/lang/Object;
    goto :goto_a9

    .line 464
    :catch_9e
    move-exception v11

    .line 465
    .local v11, "e":Lorg/json/JSONException;
    new-instance v0, Ljava/io/IOException;

    invoke-virtual {v11}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 468
    .end local v11    # "e":Lorg/json/JSONException;
    :goto_a9
    return-object v9
.end method

.method static writeHeader(Ljava/io/OutputStream;Lorg/json/JSONObject;)V
    .registers 6
    .param p0, "stream"    # Ljava/io/OutputStream;
    .param p1, "header"    # Lorg/json/JSONObject;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 405
    invoke-virtual {p1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v2

    .line 406
    .local v2, "headerString":Ljava/lang/String;
    invoke-virtual {v2}, Ljava/lang/String;->getBytes()[B

    move-result-object v3

    .line 409
    .local v3, "headerBytes":[B
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Ljava/io/OutputStream;->write(I)V

    .line 410
    array-length v0, v3

    shr-int/lit8 v0, v0, 0x10

    and-int/lit16 v0, v0, 0xff

    invoke-virtual {p0, v0}, Ljava/io/OutputStream;->write(I)V

    .line 411
    array-length v0, v3

    shr-int/lit8 v0, v0, 0x8

    and-int/lit16 v0, v0, 0xff

    invoke-virtual {p0, v0}, Ljava/io/OutputStream;->write(I)V

    .line 412
    array-length v0, v3

    shr-int/lit8 v0, v0, 0x0

    and-int/lit16 v0, v0, 0xff

    invoke-virtual {p0, v0}, Ljava/io/OutputStream;->write(I)V

    .line 414
    invoke-virtual {p0, v3}, Ljava/io/OutputStream;->write([B)V

    .line 415
    return-void
.end method

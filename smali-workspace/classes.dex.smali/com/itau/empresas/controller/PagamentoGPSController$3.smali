.class Lcom/itau/empresas/controller/PagamentoGPSController$3;
.super Ljava/lang/Object;
.source "PagamentoGPSController.java"

# interfaces
.implements Lbr/com/itau/sdk/android/core/type/BackendCall;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/itau/empresas/controller/PagamentoGPSController;->inclusaoGPS(Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/GpsRequestVO;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/itau/empresas/controller/PagamentoGPSController;

.field final synthetic val$gpsRequestVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/GpsRequestVO;


# direct methods
.method constructor <init>(Lcom/itau/empresas/controller/PagamentoGPSController;Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/GpsRequestVO;)V
    .registers 3
    .param p1, "this$0"    # Lcom/itau/empresas/controller/PagamentoGPSController;

    .line 66
    iput-object p1, p0, Lcom/itau/empresas/controller/PagamentoGPSController$3;->this$0:Lcom/itau/empresas/controller/PagamentoGPSController;

    iput-object p2, p0, Lcom/itau/empresas/controller/PagamentoGPSController$3;->val$gpsRequestVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/GpsRequestVO;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public callBackend()V
    .registers 3

    .line 69
    new-instance v1, Lcom/itau/empresas/api/model/DadosGpsRequest;

    invoke-direct {v1}, Lcom/itau/empresas/api/model/DadosGpsRequest;-><init>()V

    .line 70
    .local v1, "dadosGpsRequest":Lcom/itau/empresas/api/model/DadosGpsRequest;
    iget-object v0, p0, Lcom/itau/empresas/controller/PagamentoGPSController$3;->val$gpsRequestVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/GpsRequestVO;

    invoke-virtual {v1, v0}, Lcom/itau/empresas/api/model/DadosGpsRequest;->setGpsRequestVO(Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/GpsRequestVO;)V

    .line 71
    iget-object v0, p0, Lcom/itau/empresas/controller/PagamentoGPSController$3;->this$0:Lcom/itau/empresas/controller/PagamentoGPSController;

    invoke-virtual {v0}, Lcom/itau/empresas/controller/PagamentoGPSController;->api()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/api/Api;

    invoke-interface {v0, v1}, Lcom/itau/empresas/api/Api;->simularInclusaoGPS(Lcom/itau/empresas/api/model/DadosGpsRequest;)Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/InclusaoTributoResponseVO;

    .line 72
    iget-object v0, p0, Lcom/itau/empresas/controller/PagamentoGPSController$3;->this$0:Lcom/itau/empresas/controller/PagamentoGPSController;

    invoke-virtual {v0}, Lcom/itau/empresas/controller/PagamentoGPSController;->api()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/api/Api;

    invoke-interface {v0}, Lcom/itau/empresas/api/Api;->incluiGps()Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/InclusaoTributoResponseVO;

    move-result-object v0

    invoke-static {v0}, Lcom/itau/empresas/controller/BaseController;->post(Ljava/lang/Object;)V

    .line 73
    return-void
.end method

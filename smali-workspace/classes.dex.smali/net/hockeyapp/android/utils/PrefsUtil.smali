.class public Lnet/hockeyapp/android/utils/PrefsUtil;
.super Ljava/lang/Object;
.source "PrefsUtil.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lnet/hockeyapp/android/utils/PrefsUtil$1;,
        Lnet/hockeyapp/android/utils/PrefsUtil$PrefsUtilHolder;
    }
.end annotation


# instance fields
.field private feedbackTokenPrefs:Landroid/content/SharedPreferences;

.field private feedbackTokenPrefsEditor:Landroid/content/SharedPreferences$Editor;

.field private nameEmailSubjectPrefs:Landroid/content/SharedPreferences;

.field private nameEmailSubjectPrefsEditor:Landroid/content/SharedPreferences$Editor;


# direct methods
.method private constructor <init>()V
    .registers 1

    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    return-void
.end method

.method synthetic constructor <init>(Lnet/hockeyapp/android/utils/PrefsUtil$1;)V
    .registers 2
    .param p1, "x0"    # Lnet/hockeyapp/android/utils/PrefsUtil$1;

    .line 41
    invoke-direct {p0}, Lnet/hockeyapp/android/utils/PrefsUtil;-><init>()V

    return-void
.end method

.method public static applyChanges(Landroid/content/SharedPreferences$Editor;)V
    .registers 2
    .param p0, "editor"    # Landroid/content/SharedPreferences$Editor;

    .line 155
    invoke-static {}, Lnet/hockeyapp/android/utils/PrefsUtil;->applySupported()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_e

    .line 156
    invoke-interface {p0}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto :goto_11

    .line 159
    :cond_e
    invoke-interface {p0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 161
    :goto_11
    return-void
.end method

.method public static applySupported()Ljava/lang/Boolean;
    .registers 3

    .line 170
    :try_start_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x9

    if-lt v0, v1, :cond_8

    const/4 v0, 0x1

    goto :goto_9

    :cond_8
    const/4 v0, 0x0

    :goto_9
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;
    :try_end_c
    .catch Ljava/lang/NoClassDefFoundError; {:try_start_0 .. :try_end_c} :catch_e

    move-result-object v0

    return-object v0

    .line 172
    :catch_e
    move-exception v2

    .line 173
    .local v2, "e":Ljava/lang/NoClassDefFoundError;
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public static getInstance()Lnet/hockeyapp/android/utils/PrefsUtil;
    .registers 1

    .line 65
    sget-object v0, Lnet/hockeyapp/android/utils/PrefsUtil$PrefsUtilHolder;->INSTANCE:Lnet/hockeyapp/android/utils/PrefsUtil;

    return-object v0
.end method


# virtual methods
.method public getFeedbackTokenFromPrefs(Landroid/content/Context;)Ljava/lang/String;
    .registers 5
    .param p1, "context"    # Landroid/content/Context;

    .line 92
    if-nez p1, :cond_4

    .line 93
    const/4 v0, 0x0

    return-object v0

    .line 96
    :cond_4
    const-string v0, "net.hockeyapp.android.prefs_feedback_token"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lnet/hockeyapp/android/utils/PrefsUtil;->feedbackTokenPrefs:Landroid/content/SharedPreferences;

    .line 97
    iget-object v0, p0, Lnet/hockeyapp/android/utils/PrefsUtil;->feedbackTokenPrefs:Landroid/content/SharedPreferences;

    if-nez v0, :cond_13

    .line 98
    const/4 v0, 0x0

    return-object v0

    .line 101
    :cond_13
    iget-object v0, p0, Lnet/hockeyapp/android/utils/PrefsUtil;->feedbackTokenPrefs:Landroid/content/SharedPreferences;

    const-string v1, "net.hockeyapp.android.prefs_key_feedback_token"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getNameEmailFromPrefs(Landroid/content/Context;)Ljava/lang/String;
    .registers 5
    .param p1, "context"    # Landroid/content/Context;

    .line 136
    if-nez p1, :cond_4

    .line 137
    const/4 v0, 0x0

    return-object v0

    .line 140
    :cond_4
    const-string v0, "net.hockeyapp.android.prefs_name_email"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lnet/hockeyapp/android/utils/PrefsUtil;->nameEmailSubjectPrefs:Landroid/content/SharedPreferences;

    .line 141
    iget-object v0, p0, Lnet/hockeyapp/android/utils/PrefsUtil;->nameEmailSubjectPrefs:Landroid/content/SharedPreferences;

    if-nez v0, :cond_13

    .line 142
    const/4 v0, 0x0

    return-object v0

    .line 145
    :cond_13
    iget-object v0, p0, Lnet/hockeyapp/android/utils/PrefsUtil;->nameEmailSubjectPrefs:Landroid/content/SharedPreferences;

    const-string v1, "net.hockeyapp.android.prefs_key_name_email"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public saveFeedbackTokenToPrefs(Landroid/content/Context;Ljava/lang/String;)V
    .registers 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "token"    # Ljava/lang/String;

    .line 75
    if-eqz p1, :cond_23

    .line 76
    const-string v0, "net.hockeyapp.android.prefs_feedback_token"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lnet/hockeyapp/android/utils/PrefsUtil;->feedbackTokenPrefs:Landroid/content/SharedPreferences;

    .line 77
    iget-object v0, p0, Lnet/hockeyapp/android/utils/PrefsUtil;->feedbackTokenPrefs:Landroid/content/SharedPreferences;

    if-eqz v0, :cond_23

    .line 78
    iget-object v0, p0, Lnet/hockeyapp/android/utils/PrefsUtil;->feedbackTokenPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    iput-object v0, p0, Lnet/hockeyapp/android/utils/PrefsUtil;->feedbackTokenPrefsEditor:Landroid/content/SharedPreferences$Editor;

    .line 79
    iget-object v0, p0, Lnet/hockeyapp/android/utils/PrefsUtil;->feedbackTokenPrefsEditor:Landroid/content/SharedPreferences$Editor;

    const-string v1, "net.hockeyapp.android.prefs_key_feedback_token"

    invoke-interface {v0, v1, p2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 80
    iget-object v0, p0, Lnet/hockeyapp/android/utils/PrefsUtil;->feedbackTokenPrefsEditor:Landroid/content/SharedPreferences$Editor;

    invoke-static {v0}, Lnet/hockeyapp/android/utils/PrefsUtil;->applyChanges(Landroid/content/SharedPreferences$Editor;)V

    .line 83
    :cond_23
    return-void
.end method

.method public saveNameEmailSubjectToPrefs(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 10
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "email"    # Ljava/lang/String;
    .param p4, "subject"    # Ljava/lang/String;

    .line 113
    if-eqz p1, :cond_44

    .line 114
    const-string v0, "net.hockeyapp.android.prefs_name_email"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lnet/hockeyapp/android/utils/PrefsUtil;->nameEmailSubjectPrefs:Landroid/content/SharedPreferences;

    .line 115
    iget-object v0, p0, Lnet/hockeyapp/android/utils/PrefsUtil;->nameEmailSubjectPrefs:Landroid/content/SharedPreferences;

    if-eqz v0, :cond_44

    .line 116
    iget-object v0, p0, Lnet/hockeyapp/android/utils/PrefsUtil;->nameEmailSubjectPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    iput-object v0, p0, Lnet/hockeyapp/android/utils/PrefsUtil;->nameEmailSubjectPrefsEditor:Landroid/content/SharedPreferences$Editor;

    .line 117
    if-eqz p2, :cond_1d

    if-eqz p3, :cond_1d

    if-nez p4, :cond_26

    .line 118
    :cond_1d
    iget-object v0, p0, Lnet/hockeyapp/android/utils/PrefsUtil;->nameEmailSubjectPrefsEditor:Landroid/content/SharedPreferences$Editor;

    const-string v1, "net.hockeyapp.android.prefs_key_name_email"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    goto :goto_3f

    .line 120
    :cond_26
    iget-object v0, p0, Lnet/hockeyapp/android/utils/PrefsUtil;->nameEmailSubjectPrefsEditor:Landroid/content/SharedPreferences$Editor;

    const-string v1, "net.hockeyapp.android.prefs_key_name_email"

    const-string v2, "%s|%s|%s"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p2, v3, v4

    const/4 v4, 0x1

    aput-object p3, v3, v4

    const/4 v4, 0x2

    aput-object p4, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 124
    :goto_3f
    iget-object v0, p0, Lnet/hockeyapp/android/utils/PrefsUtil;->nameEmailSubjectPrefsEditor:Landroid/content/SharedPreferences$Editor;

    invoke-static {v0}, Lnet/hockeyapp/android/utils/PrefsUtil;->applyChanges(Landroid/content/SharedPreferences$Editor;)V

    .line 127
    :cond_44
    return-void
.end method

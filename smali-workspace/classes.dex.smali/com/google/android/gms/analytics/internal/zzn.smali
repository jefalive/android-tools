.class public Lcom/google/android/gms/analytics/internal/zzn;
.super Lcom/google/android/gms/analytics/internal/zzd;


# instance fields
.field private volatile zzPO:Ljava/lang/String;

.field private zzRr:Ljava/util/concurrent/Future;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/Future<Ljava/lang/String;>;"
        }
    .end annotation
.end field


# direct methods
.method protected constructor <init>(Lcom/google/android/gms/analytics/internal/zzf;)V
    .registers 2

    invoke-direct {p0, p1}, Lcom/google/android/gms/analytics/internal/zzd;-><init>(Lcom/google/android/gms/analytics/internal/zzf;)V

    return-void
.end method

.method static synthetic zza(Lcom/google/android/gms/analytics/internal/zzn;)Ljava/lang/String;
    .registers 2

    invoke-direct {p0}, Lcom/google/android/gms/analytics/internal/zzn;->zzkn()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private zzh(Landroid/content/Context;Ljava/lang/String;)Z
    .registers 11

    invoke-static {p2}, Lcom/google/android/gms/common/internal/zzx;->zzcM(Ljava/lang/String;)Ljava/lang/String;

    const-string v0, "ClientId should be saved from worker thread"

    invoke-static {v0}, Lcom/google/android/gms/common/internal/zzx;->zzcE(Ljava/lang/String;)V

    const/4 v2, 0x0

    const-string v0, "Storing clientId"

    :try_start_b
    invoke-virtual {p0, v0, p2}, Lcom/google/android/gms/analytics/internal/zzn;->zza(Ljava/lang/String;Ljava/lang/Object;)V

    const-string v0, "gaClientId"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->openFileOutput(Ljava/lang/String;I)Ljava/io/FileOutputStream;

    move-result-object v0

    move-object v2, v0

    invoke-virtual {p2}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/io/FileOutputStream;->write([B)V
    :try_end_1d
    .catch Ljava/io/FileNotFoundException; {:try_start_b .. :try_end_1d} :catch_2b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_1d} :catch_3f
    .catchall {:try_start_b .. :try_end_1d} :catchall_53

    const/4 v3, 0x1

    if-eqz v2, :cond_2a

    :try_start_20
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_23
    .catch Ljava/io/IOException; {:try_start_20 .. :try_end_23} :catch_24

    goto :goto_2a

    :catch_24
    move-exception v4

    const-string v0, "Failed to close clientId writing stream"

    invoke-virtual {p0, v0, v4}, Lcom/google/android/gms/analytics/internal/zzn;->zze(Ljava/lang/String;Ljava/lang/Object;)V

    :cond_2a
    :goto_2a
    return v3

    :catch_2b
    move-exception v3

    const-string v0, "Error creating clientId file"

    :try_start_2e
    invoke-virtual {p0, v0, v3}, Lcom/google/android/gms/analytics/internal/zzn;->zze(Ljava/lang/String;Ljava/lang/Object;)V
    :try_end_31
    .catchall {:try_start_2e .. :try_end_31} :catchall_53

    const/4 v4, 0x0

    if-eqz v2, :cond_3e

    :try_start_34
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_37
    .catch Ljava/io/IOException; {:try_start_34 .. :try_end_37} :catch_38

    goto :goto_3e

    :catch_38
    move-exception v5

    const-string v0, "Failed to close clientId writing stream"

    invoke-virtual {p0, v0, v5}, Lcom/google/android/gms/analytics/internal/zzn;->zze(Ljava/lang/String;Ljava/lang/Object;)V

    :cond_3e
    :goto_3e
    return v4

    :catch_3f
    move-exception v3

    const-string v0, "Error writing to clientId file"

    :try_start_42
    invoke-virtual {p0, v0, v3}, Lcom/google/android/gms/analytics/internal/zzn;->zze(Ljava/lang/String;Ljava/lang/Object;)V
    :try_end_45
    .catchall {:try_start_42 .. :try_end_45} :catchall_53

    const/4 v4, 0x0

    if-eqz v2, :cond_52

    :try_start_48
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_4b
    .catch Ljava/io/IOException; {:try_start_48 .. :try_end_4b} :catch_4c

    goto :goto_52

    :catch_4c
    move-exception v5

    const-string v0, "Failed to close clientId writing stream"

    invoke-virtual {p0, v0, v5}, Lcom/google/android/gms/analytics/internal/zzn;->zze(Ljava/lang/String;Ljava/lang/Object;)V

    :cond_52
    :goto_52
    return v4

    :catchall_53
    move-exception v6

    if-eqz v2, :cond_60

    :try_start_56
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_59
    .catch Ljava/io/IOException; {:try_start_56 .. :try_end_59} :catch_5a

    goto :goto_60

    :catch_5a
    move-exception v7

    const-string v0, "Failed to close clientId writing stream"

    invoke-virtual {p0, v0, v7}, Lcom/google/android/gms/analytics/internal/zzn;->zze(Ljava/lang/String;Ljava/lang/Object;)V

    :cond_60
    :goto_60
    throw v6
.end method

.method private zzkn()Ljava/lang/String;
    .registers 4

    invoke-virtual {p0}, Lcom/google/android/gms/analytics/internal/zzn;->zzko()Ljava/lang/String;

    move-result-object v1

    :try_start_4
    invoke-virtual {p0}, Lcom/google/android/gms/analytics/internal/zzn;->zzjo()Lcom/google/android/gms/measurement/zzg;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/zzg;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/analytics/internal/zzn;->zzh(Landroid/content/Context;Ljava/lang/String;)Z
    :try_end_f
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_f} :catch_16

    move-result v0

    if-nez v0, :cond_15

    const-string v0, "0"

    return-object v0

    :cond_15
    goto :goto_1f

    :catch_16
    move-exception v2

    const-string v0, "Error saving clientId file"

    invoke-virtual {p0, v0, v2}, Lcom/google/android/gms/analytics/internal/zzn;->zze(Ljava/lang/String;Ljava/lang/Object;)V

    const-string v0, "0"

    return-object v0

    :goto_1f
    return-object v1
.end method


# virtual methods
.method protected zzac(Landroid/content/Context;)Ljava/lang/String;
    .registers 12

    const-string v0, "ClientId should be loaded from worker thread"

    invoke-static {v0}, Lcom/google/android/gms/common/internal/zzx;->zzcE(Ljava/lang/String;)V

    const/4 v2, 0x0

    const-string v0, "gaClientId"

    :try_start_8
    invoke-virtual {p1, v0}, Landroid/content/Context;->openFileInput(Ljava/lang/String;)Ljava/io/FileInputStream;

    move-result-object v0

    move-object v2, v0

    const/16 v0, 0x24

    new-array v3, v0, [B

    array-length v0, v3

    const/4 v1, 0x0

    invoke-virtual {v2, v3, v1, v0}, Ljava/io/FileInputStream;->read([BII)I

    move-result v4

    invoke-virtual {v2}, Ljava/io/FileInputStream;->available()I

    move-result v0

    if-lez v0, :cond_38

    const-string v0, "clientId file seems corrupted, deleting it."

    invoke-virtual {p0, v0}, Lcom/google/android/gms/analytics/internal/zzn;->zzbg(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V

    const-string v0, "gaClientId"

    invoke-virtual {p1, v0}, Landroid/content/Context;->deleteFile(Ljava/lang/String;)Z
    :try_end_2a
    .catch Ljava/io/FileNotFoundException; {:try_start_8 .. :try_end_2a} :catch_73
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_2a} :catch_82
    .catchall {:try_start_8 .. :try_end_2a} :catchall_9b

    const/4 v5, 0x0

    if-eqz v2, :cond_37

    :try_start_2d
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_30
    .catch Ljava/io/IOException; {:try_start_2d .. :try_end_30} :catch_31

    goto :goto_37

    :catch_31
    move-exception v6

    const-string v0, "Failed to close client id reading stream"

    invoke-virtual {p0, v0, v6}, Lcom/google/android/gms/analytics/internal/zzn;->zze(Ljava/lang/String;Ljava/lang/Object;)V

    :cond_37
    :goto_37
    return-object v5

    :cond_38
    const/16 v0, 0xe

    if-ge v4, v0, :cond_57

    const-string v0, "clientId file is empty, deleting it."

    :try_start_3e
    invoke-virtual {p0, v0}, Lcom/google/android/gms/analytics/internal/zzn;->zzbg(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V

    const-string v0, "gaClientId"

    invoke-virtual {p1, v0}, Landroid/content/Context;->deleteFile(Ljava/lang/String;)Z
    :try_end_49
    .catch Ljava/io/FileNotFoundException; {:try_start_3e .. :try_end_49} :catch_73
    .catch Ljava/io/IOException; {:try_start_3e .. :try_end_49} :catch_82
    .catchall {:try_start_3e .. :try_end_49} :catchall_9b

    const/4 v5, 0x0

    if-eqz v2, :cond_56

    :try_start_4c
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_4f
    .catch Ljava/io/IOException; {:try_start_4c .. :try_end_4f} :catch_50

    goto :goto_56

    :catch_50
    move-exception v6

    const-string v0, "Failed to close client id reading stream"

    invoke-virtual {p0, v0, v6}, Lcom/google/android/gms/analytics/internal/zzn;->zze(Ljava/lang/String;Ljava/lang/Object;)V

    :cond_56
    :goto_56
    return-object v5

    :cond_57
    :try_start_57
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V

    new-instance v5, Ljava/lang/String;

    const/4 v0, 0x0

    invoke-direct {v5, v3, v0, v4}, Ljava/lang/String;-><init>([BII)V

    const-string v0, "Read client id from disk"

    invoke-virtual {p0, v0, v5}, Lcom/google/android/gms/analytics/internal/zzn;->zza(Ljava/lang/String;Ljava/lang/Object;)V
    :try_end_65
    .catch Ljava/io/FileNotFoundException; {:try_start_57 .. :try_end_65} :catch_73
    .catch Ljava/io/IOException; {:try_start_57 .. :try_end_65} :catch_82
    .catchall {:try_start_57 .. :try_end_65} :catchall_9b

    move-object v6, v5

    if-eqz v2, :cond_72

    :try_start_68
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_6b
    .catch Ljava/io/IOException; {:try_start_68 .. :try_end_6b} :catch_6c

    goto :goto_72

    :catch_6c
    move-exception v7

    const-string v0, "Failed to close client id reading stream"

    invoke-virtual {p0, v0, v7}, Lcom/google/android/gms/analytics/internal/zzn;->zze(Ljava/lang/String;Ljava/lang/Object;)V

    :cond_72
    :goto_72
    return-object v6

    :catch_73
    move-exception v3

    const/4 v4, 0x0

    if-eqz v2, :cond_81

    :try_start_77
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_7a
    .catch Ljava/io/IOException; {:try_start_77 .. :try_end_7a} :catch_7b

    goto :goto_81

    :catch_7b
    move-exception v5

    const-string v0, "Failed to close client id reading stream"

    invoke-virtual {p0, v0, v5}, Lcom/google/android/gms/analytics/internal/zzn;->zze(Ljava/lang/String;Ljava/lang/Object;)V

    :cond_81
    :goto_81
    return-object v4

    :catch_82
    move-exception v3

    const-string v0, "Error reading client id file, deleting it"

    :try_start_85
    invoke-virtual {p0, v0, v3}, Lcom/google/android/gms/analytics/internal/zzn;->zze(Ljava/lang/String;Ljava/lang/Object;)V

    const-string v0, "gaClientId"

    invoke-virtual {p1, v0}, Landroid/content/Context;->deleteFile(Ljava/lang/String;)Z
    :try_end_8d
    .catchall {:try_start_85 .. :try_end_8d} :catchall_9b

    const/4 v4, 0x0

    if-eqz v2, :cond_9a

    :try_start_90
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_93
    .catch Ljava/io/IOException; {:try_start_90 .. :try_end_93} :catch_94

    goto :goto_9a

    :catch_94
    move-exception v5

    const-string v0, "Failed to close client id reading stream"

    invoke-virtual {p0, v0, v5}, Lcom/google/android/gms/analytics/internal/zzn;->zze(Ljava/lang/String;Ljava/lang/Object;)V

    :cond_9a
    :goto_9a
    return-object v4

    :catchall_9b
    move-exception v8

    if-eqz v2, :cond_a8

    :try_start_9e
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_a1
    .catch Ljava/io/IOException; {:try_start_9e .. :try_end_a1} :catch_a2

    goto :goto_a8

    :catch_a2
    move-exception v9

    const-string v0, "Failed to close client id reading stream"

    invoke-virtual {p0, v0, v9}, Lcom/google/android/gms/analytics/internal/zzn;->zze(Ljava/lang/String;Ljava/lang/Object;)V

    :cond_a8
    :goto_a8
    throw v8
.end method

.method protected zziJ()V
    .registers 1

    return-void
.end method

.method public zzkk()Ljava/lang/String;
    .registers 6

    invoke-virtual {p0}, Lcom/google/android/gms/analytics/internal/zzn;->zzjv()V

    move-object v2, p0

    monitor-enter v2

    :try_start_5
    iget-object v0, p0, Lcom/google/android/gms/analytics/internal/zzn;->zzPO:Ljava/lang/String;

    if-nez v0, :cond_18

    invoke-virtual {p0}, Lcom/google/android/gms/analytics/internal/zzn;->zzjo()Lcom/google/android/gms/measurement/zzg;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/analytics/internal/zzn$1;

    invoke-direct {v1, p0}, Lcom/google/android/gms/analytics/internal/zzn$1;-><init>(Lcom/google/android/gms/analytics/internal/zzn;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/measurement/zzg;->zzc(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/Future;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/analytics/internal/zzn;->zzRr:Ljava/util/concurrent/Future;

    :cond_18
    iget-object v0, p0, Lcom/google/android/gms/analytics/internal/zzn;->zzRr:Ljava/util/concurrent/Future;
    :try_end_1a
    .catchall {:try_start_5 .. :try_end_1a} :catchall_52

    if-eqz v0, :cond_4e

    :try_start_1c
    iget-object v0, p0, Lcom/google/android/gms/analytics/internal/zzn;->zzRr:Ljava/util/concurrent/Future;

    invoke-interface {v0}, Ljava/util/concurrent/Future;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/analytics/internal/zzn;->zzPO:Ljava/lang/String;
    :try_end_26
    .catch Ljava/lang/InterruptedException; {:try_start_1c .. :try_end_26} :catch_27
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_1c .. :try_end_26} :catch_32
    .catchall {:try_start_1c .. :try_end_26} :catchall_52

    goto :goto_3c

    :catch_27
    move-exception v3

    const-string v0, "ClientId loading or generation was interrupted"

    :try_start_2a
    invoke-virtual {p0, v0, v3}, Lcom/google/android/gms/analytics/internal/zzn;->zzd(Ljava/lang/String;Ljava/lang/Object;)V

    const-string v0, "0"

    iput-object v0, p0, Lcom/google/android/gms/analytics/internal/zzn;->zzPO:Ljava/lang/String;

    goto :goto_3c

    :catch_32
    move-exception v3

    const-string v0, "Failed to load or generate client id"

    invoke-virtual {p0, v0, v3}, Lcom/google/android/gms/analytics/internal/zzn;->zze(Ljava/lang/String;Ljava/lang/Object;)V

    const-string v0, "0"

    iput-object v0, p0, Lcom/google/android/gms/analytics/internal/zzn;->zzPO:Ljava/lang/String;

    :goto_3c
    iget-object v0, p0, Lcom/google/android/gms/analytics/internal/zzn;->zzPO:Ljava/lang/String;

    if-nez v0, :cond_44

    const-string v0, "0"

    iput-object v0, p0, Lcom/google/android/gms/analytics/internal/zzn;->zzPO:Ljava/lang/String;

    :cond_44
    const-string v0, "Loaded clientId"

    iget-object v1, p0, Lcom/google/android/gms/analytics/internal/zzn;->zzPO:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/analytics/internal/zzn;->zza(Ljava/lang/String;Ljava/lang/Object;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/analytics/internal/zzn;->zzRr:Ljava/util/concurrent/Future;

    :cond_4e
    iget-object v0, p0, Lcom/google/android/gms/analytics/internal/zzn;->zzPO:Ljava/lang/String;
    :try_end_50
    .catchall {:try_start_2a .. :try_end_50} :catchall_52

    monitor-exit v2

    return-object v0

    :catchall_52
    move-exception v4

    monitor-exit v2

    throw v4
.end method

.method zzkl()Ljava/lang/String;
    .registers 5

    move-object v2, p0

    monitor-enter v2

    const/4 v0, 0x0

    :try_start_3
    iput-object v0, p0, Lcom/google/android/gms/analytics/internal/zzn;->zzPO:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/gms/analytics/internal/zzn;->zzjo()Lcom/google/android/gms/measurement/zzg;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/analytics/internal/zzn$2;

    invoke-direct {v1, p0}, Lcom/google/android/gms/analytics/internal/zzn$2;-><init>(Lcom/google/android/gms/analytics/internal/zzn;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/measurement/zzg;->zzc(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/Future;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/analytics/internal/zzn;->zzRr:Ljava/util/concurrent/Future;
    :try_end_14
    .catchall {:try_start_3 .. :try_end_14} :catchall_16

    monitor-exit v2

    goto :goto_19

    :catchall_16
    move-exception v3

    monitor-exit v2

    throw v3

    :goto_19
    invoke-virtual {p0}, Lcom/google/android/gms/analytics/internal/zzn;->zzkk()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method zzkm()Ljava/lang/String;
    .registers 3

    invoke-virtual {p0}, Lcom/google/android/gms/analytics/internal/zzn;->zzjo()Lcom/google/android/gms/measurement/zzg;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/zzg;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/analytics/internal/zzn;->zzac(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_12

    invoke-direct {p0}, Lcom/google/android/gms/analytics/internal/zzn;->zzkn()Ljava/lang/String;

    move-result-object v1

    :cond_12
    return-object v1
.end method

.method protected zzko()Ljava/lang/String;
    .registers 2

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.class public final Lbr/com/itau/sdk/android/core/endpoint/b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lbr/com/itau/sdk/android/core/endpoint/e;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:Ljava/lang/Object;>Ljava/lang/Object;Lbr/com/itau/sdk/android/core/endpoint/e<TT;>;"
    }
.end annotation


# static fields
.field private static final f:[B

.field private static g:I


# instance fields
.field private final a:Lokhttp3/Response;

.field private final b:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field private final c:Lbr/com/itau/sdk/android/core/endpoint/http/RouterResponse;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbr/com/itau/sdk/android/core/endpoint/http/RouterResponse<TT;>;"
        }
    .end annotation
.end field

.field private final d:Lokhttp3/ResponseBody;

.field private e:Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    const/16 v0, 0x28

    new-array v0, v0, [B

    fill-array-data v0, :array_e

    sput-object v0, Lbr/com/itau/sdk/android/core/endpoint/b;->f:[B

    const/16 v0, 0x56

    sput v0, Lbr/com/itau/sdk/android/core/endpoint/b;->g:I

    return-void

    :array_e
    .array-data 1
        0x25t
        0x57t
        -0x4at
        0x71t
        0x11t
        -0x16t
        0x25t
        -0x13t
        -0xet
        0x3t
        0x1t
        0x1t
        -0x5t
        0xet
        0x45t
        -0x1dt
        0x0t
        0x1dt
        -0x4et
        -0x7t
        0x9t
        0x0t
        -0x29t
        -0x3t
        0x1t
        0x9t
        0x45t
        -0x4et
        -0x1t
        -0x5t
        0x54t
        -0x53t
        -0x2t
        0x5t
        0x0t
        0x1t
        -0x3t
        -0x2t
        0xft
        0x1t
    .end array-data
.end method

.method public constructor <init>(Lokhttp3/Response;Lbr/com/itau/sdk/android/core/endpoint/http/RouterResponse;Lokhttp3/ResponseBody;)V
    .registers 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Lokhttp3/Response;Lbr/com/itau/sdk/android/core/endpoint/http/RouterResponse<TT;>;Lokhttp3/ResponseBody;)V"
        }
    .end annotation

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    sget-object v0, Lbr/com/itau/sdk/android/core/endpoint/b;->f:[B

    const/16 v1, 0x10

    aget-byte v0, v0, v1

    sget-object v1, Lbr/com/itau/sdk/android/core/endpoint/b;->f:[B

    const/16 v2, 0xa

    aget-byte v1, v1, v2

    sget-object v2, Lbr/com/itau/sdk/android/core/endpoint/b;->f:[B

    const/16 v3, 0x10

    aget-byte v2, v2, v3

    invoke-static {v0, v1, v2}, Lbr/com/itau/sdk/android/core/endpoint/b;->a(III)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lbr/com/itau/sdk/android/core/endpoint/aa;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lokhttp3/Response;

    iput-object v0, p0, Lbr/com/itau/sdk/android/core/endpoint/b;->a:Lokhttp3/Response;

    .line 33
    invoke-virtual {p2}, Lbr/com/itau/sdk/android/core/endpoint/http/RouterResponse;->getData()Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/sdk/android/core/endpoint/b;->b:Ljava/lang/Object;

    .line 34
    iput-object p3, p0, Lbr/com/itau/sdk/android/core/endpoint/b;->d:Lokhttp3/ResponseBody;

    .line 35
    iput-object p2, p0, Lbr/com/itau/sdk/android/core/endpoint/b;->c:Lbr/com/itau/sdk/android/core/endpoint/http/RouterResponse;

    .line 36
    return-void
.end method

.method public constructor <init>(Lokhttp3/Response;Ljava/lang/Object;Lokhttp3/ResponseBody;)V
    .registers 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Lokhttp3/Response;TT;Lokhttp3/ResponseBody;)V"
        }
    .end annotation

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    sget-object v0, Lbr/com/itau/sdk/android/core/endpoint/b;->f:[B

    const/16 v1, 0x10

    aget-byte v0, v0, v1

    sget-object v1, Lbr/com/itau/sdk/android/core/endpoint/b;->f:[B

    const/16 v2, 0xa

    aget-byte v1, v1, v2

    sget-object v2, Lbr/com/itau/sdk/android/core/endpoint/b;->f:[B

    const/16 v3, 0x10

    aget-byte v2, v2, v3

    invoke-static {v0, v1, v2}, Lbr/com/itau/sdk/android/core/endpoint/b;->a(III)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lbr/com/itau/sdk/android/core/endpoint/aa;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lokhttp3/Response;

    iput-object v0, p0, Lbr/com/itau/sdk/android/core/endpoint/b;->a:Lokhttp3/Response;

    .line 26
    iput-object p2, p0, Lbr/com/itau/sdk/android/core/endpoint/b;->b:Ljava/lang/Object;

    .line 27
    iput-object p3, p0, Lbr/com/itau/sdk/android/core/endpoint/b;->d:Lokhttp3/ResponseBody;

    .line 28
    const/4 v0, 0x0

    iput-object v0, p0, Lbr/com/itau/sdk/android/core/endpoint/b;->c:Lbr/com/itau/sdk/android/core/endpoint/http/RouterResponse;

    .line 29
    return-void
.end method

.method private static a(III)Ljava/lang/String;
    .registers 9

    mul-int/lit8 p0, p0, 0x2f

    rsub-int/lit8 p0, p0, 0x72

    sget-object v5, Lbr/com/itau/sdk/android/core/endpoint/b;->f:[B

    mul-int/lit8 p2, p2, 0x4

    add-int/lit8 p2, p2, 0x13

    mul-int/lit8 p1, p1, 0x12

    rsub-int/lit8 p1, p1, 0x15

    const/4 v4, 0x0

    new-instance v0, Ljava/lang/String;

    new-array v1, p2, [B

    if-nez v5, :cond_1a

    move v2, p1

    move v3, p2

    :goto_17
    neg-int v3, v3

    add-int p0, v2, v3

    :cond_1a
    move v2, v4

    add-int/lit8 p1, p1, 0x1

    add-int/lit8 v4, v4, 0x1

    int-to-byte v3, p0

    aput-byte v3, v1, v2

    if-ne v4, p2, :cond_29

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Ljava/lang/String;-><init>([BI)V

    return-object v0

    :cond_29
    move v2, p0

    aget-byte v3, v5, p1

    goto :goto_17
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .registers 2

    .line 54
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/endpoint/b;->a:Lokhttp3/Response;

    invoke-virtual {v0}, Lokhttp3/Response;->message()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/Integer;)V
    .registers 2

    .line 45
    iput-object p1, p0, Lbr/com/itau/sdk/android/core/endpoint/b;->e:Ljava/lang/Integer;

    .line 46
    return-void
.end method

.method public b()Z
    .registers 2

    .line 58
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/endpoint/b;->c:Lbr/com/itau/sdk/android/core/endpoint/http/RouterResponse;

    if-eqz v0, :cond_6

    const/4 v0, 0x1

    goto :goto_7

    :cond_6
    const/4 v0, 0x0

    :goto_7
    return v0
.end method

.method public c()I
    .registers 2

    .line 68
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/endpoint/b;->e:Ljava/lang/Integer;

    if-eqz v0, :cond_b

    .line 69
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/endpoint/b;->e:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0

    .line 71
    :cond_b
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/endpoint/b;->a:Lokhttp3/Response;

    invoke-virtual {v0}, Lokhttp3/Response;->code()I

    move-result v0

    return v0
.end method

.method public clone()Ljava/lang/Object;
    .registers 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .line 41
    new-instance v0, Ljava/lang/CloneNotSupportedException;

    sget-object v1, Lbr/com/itau/sdk/android/core/endpoint/b;->f:[B

    const/16 v2, 0xa

    aget-byte v1, v1, v2

    sget-object v2, Lbr/com/itau/sdk/android/core/endpoint/b;->f:[B

    const/16 v3, 0x10

    aget-byte v2, v2, v3

    invoke-static {v1, v2, v2}, Lbr/com/itau/sdk/android/core/endpoint/b;->a(III)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/CloneNotSupportedException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public d()Ljava/lang/Object;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .line 80
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/endpoint/b;->b:Ljava/lang/Object;

    return-object v0
.end method

.method public e()Lbr/com/itau/sdk/android/core/endpoint/http/RouterResponse;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Lbr/com/itau/sdk/android/core/endpoint/http/RouterResponse<TT;>;"
        }
    .end annotation

    .line 89
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/endpoint/b;->c:Lbr/com/itau/sdk/android/core/endpoint/http/RouterResponse;

    return-object v0
.end method

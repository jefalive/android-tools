.class public final Lcom/google/zxing/c;
.super Ljava/lang/Object;


# instance fields
.field private final a:Lcom/google/zxing/b;

.field private b:Lcom/google/zxing/a/b;


# direct methods
.method public constructor <init>(Lcom/google/zxing/b;)V
    .registers 4

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-nez p1, :cond_d

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Binarizer must be non-null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_d
    iput-object p1, p0, Lcom/google/zxing/c;->a:Lcom/google/zxing/b;

    return-void
.end method


# virtual methods
.method public a()I
    .registers 2

    iget-object v0, p0, Lcom/google/zxing/c;->a:Lcom/google/zxing/b;

    invoke-virtual {v0}, Lcom/google/zxing/b;->c()I

    move-result v0

    return v0
.end method

.method public a(ILcom/google/zxing/a/a;)Lcom/google/zxing/a/a;
    .registers 4

    iget-object v0, p0, Lcom/google/zxing/c;->a:Lcom/google/zxing/b;

    invoke-virtual {v0, p1, p2}, Lcom/google/zxing/b;->a(ILcom/google/zxing/a/a;)Lcom/google/zxing/a/a;

    move-result-object v0

    return-object v0
.end method

.method public b()I
    .registers 2

    iget-object v0, p0, Lcom/google/zxing/c;->a:Lcom/google/zxing/b;

    invoke-virtual {v0}, Lcom/google/zxing/b;->d()I

    move-result v0

    return v0
.end method

.method public c()Lcom/google/zxing/a/b;
    .registers 2

    iget-object v0, p0, Lcom/google/zxing/c;->b:Lcom/google/zxing/a/b;

    if-nez v0, :cond_c

    iget-object v0, p0, Lcom/google/zxing/c;->a:Lcom/google/zxing/b;

    invoke-virtual {v0}, Lcom/google/zxing/b;->b()Lcom/google/zxing/a/b;

    move-result-object v0

    iput-object v0, p0, Lcom/google/zxing/c;->b:Lcom/google/zxing/a/b;

    :cond_c
    iget-object v0, p0, Lcom/google/zxing/c;->b:Lcom/google/zxing/a/b;

    return-object v0
.end method

.method public d()Z
    .registers 2

    iget-object v0, p0, Lcom/google/zxing/c;->a:Lcom/google/zxing/b;

    invoke-virtual {v0}, Lcom/google/zxing/b;->a()Lcom/google/zxing/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/zxing/g;->d()Z

    move-result v0

    return v0
.end method

.method public e()Lcom/google/zxing/c;
    .registers 4

    iget-object v0, p0, Lcom/google/zxing/c;->a:Lcom/google/zxing/b;

    invoke-virtual {v0}, Lcom/google/zxing/b;->a()Lcom/google/zxing/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/zxing/g;->e()Lcom/google/zxing/g;

    move-result-object v2

    new-instance v0, Lcom/google/zxing/c;

    iget-object v1, p0, Lcom/google/zxing/c;->a:Lcom/google/zxing/b;

    invoke-virtual {v1, v2}, Lcom/google/zxing/b;->a(Lcom/google/zxing/g;)Lcom/google/zxing/b;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/zxing/c;-><init>(Lcom/google/zxing/b;)V

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    :try_start_0
    invoke-virtual {p0}, Lcom/google/zxing/c;->c()Lcom/google/zxing/a/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/zxing/a/b;->toString()Ljava/lang/String;
    :try_end_7
    .catch Lcom/google/zxing/h; {:try_start_0 .. :try_end_7} :catch_9

    move-result-object v0

    return-object v0

    :catch_9
    move-exception v1

    const-string v0, ""

    return-object v0
.end method

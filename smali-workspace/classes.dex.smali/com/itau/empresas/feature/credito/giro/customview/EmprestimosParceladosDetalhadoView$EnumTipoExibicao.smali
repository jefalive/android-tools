.class public final enum Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView$EnumTipoExibicao;
.super Ljava/lang/Enum;
.source "EmprestimosParceladosDetalhadoView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "EnumTipoExibicao"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView$EnumTipoExibicao;>;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView$EnumTipoExibicao;

.field public static final enum SIMULACAO_COMPROVANTE_EFETIVACAO:Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView$EnumTipoExibicao;

.field public static final enum SIMULACAO_OFERTA_ESPECIAL:Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView$EnumTipoExibicao;

.field public static final enum SIMULACAO_PERSONALIZADA:Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView$EnumTipoExibicao;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .line 150
    new-instance v0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView$EnumTipoExibicao;

    const-string v1, "SIMULACAO_OFERTA_ESPECIAL"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView$EnumTipoExibicao;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView$EnumTipoExibicao;->SIMULACAO_OFERTA_ESPECIAL:Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView$EnumTipoExibicao;

    .line 151
    new-instance v0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView$EnumTipoExibicao;

    const-string v1, "SIMULACAO_COMPROVANTE_EFETIVACAO"

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView$EnumTipoExibicao;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView$EnumTipoExibicao;->SIMULACAO_COMPROVANTE_EFETIVACAO:Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView$EnumTipoExibicao;

    .line 152
    new-instance v0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView$EnumTipoExibicao;

    const-string v1, "SIMULACAO_PERSONALIZADA"

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2}, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView$EnumTipoExibicao;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView$EnumTipoExibicao;->SIMULACAO_PERSONALIZADA:Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView$EnumTipoExibicao;

    .line 149
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView$EnumTipoExibicao;

    sget-object v1, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView$EnumTipoExibicao;->SIMULACAO_OFERTA_ESPECIAL:Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView$EnumTipoExibicao;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sget-object v1, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView$EnumTipoExibicao;->SIMULACAO_COMPROVANTE_EFETIVACAO:Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView$EnumTipoExibicao;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    sget-object v1, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView$EnumTipoExibicao;->SIMULACAO_PERSONALIZADA:Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView$EnumTipoExibicao;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    sput-object v0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView$EnumTipoExibicao;->$VALUES:[Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView$EnumTipoExibicao;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 149
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView$EnumTipoExibicao;
    .registers 2
    .param p0, "name"    # Ljava/lang/String;

    .line 149
    const-class v0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView$EnumTipoExibicao;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView$EnumTipoExibicao;

    return-object v0
.end method

.method public static values()[Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView$EnumTipoExibicao;
    .registers 1

    .line 149
    sget-object v0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView$EnumTipoExibicao;->$VALUES:[Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView$EnumTipoExibicao;

    invoke-virtual {v0}, [Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView$EnumTipoExibicao;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView$EnumTipoExibicao;

    return-object v0
.end method

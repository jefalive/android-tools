.class public interface abstract Lbr/com/itau/sdk/android/core/endpoint/ErrorHandler;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final DEFAULT:Lbr/com/itau/sdk/android/core/endpoint/ErrorHandler;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .line 18
    new-instance v0, Lbr/com/itau/sdk/android/core/DefaultEventedErrorHandler;

    invoke-direct {v0}, Lbr/com/itau/sdk/android/core/DefaultEventedErrorHandler;-><init>()V

    sput-object v0, Lbr/com/itau/sdk/android/core/endpoint/ErrorHandler;->DEFAULT:Lbr/com/itau/sdk/android/core/endpoint/ErrorHandler;

    return-void
.end method


# virtual methods
.method public abstract handleError(Lbr/com/itau/sdk/android/core/exception/BackendException;)Ljava/lang/Throwable;
.end method

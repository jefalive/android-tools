.class public Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity;
.super Lcom/itau/empresas/ui/activity/BaseActivity;
.source "LisConsultaDetalheActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity$TipoDeLis;
    }
.end annotation


# instance fields
.field comporvanteDetalhe:Landroid/widget/ScrollView;

.field comprovante:Lcom/itau/empresas/api/model/ComprovanteServiceVO;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/itau/empresas/api/model/ComprovanteServiceVO<Lcom/itau/empresas/api/model/CampoComprovanteLisConsultaVO;>;"
        }
    .end annotation
.end field

.field comprovanteDetalheLayoutPrincipal:Landroid/widget/LinearLayout;

.field comprovanteHeader:Landroid/widget/LinearLayout;

.field comprovanteValores:Landroid/widget/LinearLayout;

.field descricaoProduto:Landroid/widget/TextView;

.field lisAdicionalAtencao:Landroid/widget/TextView;

.field lisConsultaDetalhadaRespostaVO:Lcom/itau/empresas/feature/credito/lis/model/LisConsultaDetalhadaRespostaVO;

.field lisRecevivelAtencao:Landroid/widget/TextView;

.field rlLisConsultaDetalheHeader:Landroid/view/View;

.field textoDadoscontroleDescricao:Landroid/widget/TextView;

.field tipoComprovante:Ljava/lang/String;

.field private tipoDeLis:Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity$TipoDeLis;

.field toolbar:Landroid/support/v7/widget/Toolbar;

.field warning:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 44
    invoke-direct {p0}, Lcom/itau/empresas/ui/activity/BaseActivity;-><init>()V

    return-void
.end method

.method private compartilhaComprovante()V
    .registers 5

    .line 316
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity;->comporvanteDetalhe:Landroid/widget/ScrollView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity;->comprovanteDetalheLayoutPrincipal:Landroid/widget/LinearLayout;

    invoke-static {v0, v1}, Lcom/itau/empresas/ui/util/BitmapUtils;->getBitmapFromView(Landroid/view/View;Landroid/view/View;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 318
    .local v2, "comprovanteBitmap":Landroid/graphics/Bitmap;
    invoke-static {p0, v2}, Lcom/itau/empresas/ui/util/BitmapUtils;->getUriBitmapTemporario(Landroid/content/Context;Landroid/graphics/Bitmap;)Landroid/net/Uri;

    move-result-object v3

    .line 319
    .line 321
    .local v3, "bitmapUri":Landroid/net/Uri;
    const v0, 0x7f0705dc

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 320
    invoke-static {p0, v3, v0}, Lcom/itau/empresas/ui/util/CompartilhamentoUtils;->compartilharArquivoImagemPorUri(Landroid/app/Activity;Landroid/net/Uri;Ljava/lang/String;)V

    .line 322
    return-void
.end method

.method private configuraAcessibilidade()V
    .registers 6

    .line 349
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity;->lisConsultaDetalhadaRespostaVO:Lcom/itau/empresas/feature/credito/lis/model/LisConsultaDetalhadaRespostaVO;

    invoke-virtual {v1}, Lcom/itau/empresas/feature/credito/lis/model/LisConsultaDetalhadaRespostaVO;->getDescricaoProduto()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity;->lisConsultaDetalhadaRespostaVO:Lcom/itau/empresas/feature/credito/lis/model/LisConsultaDetalhadaRespostaVO;

    .line 352
    invoke-virtual {v2}, Lcom/itau/empresas/feature/credito/lis/model/LisConsultaDetalhadaRespostaVO;->getValorLimiteContratado()F

    move-result v2

    sget-object v3, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    .line 353
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    .line 351
    invoke-static {v2, v3}, Lcom/itau/empresas/ui/util/ValorMonetarioUtils;->formataValorDecimalParaMonetario(FZ)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    iget-object v2, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity;->lisConsultaDetalhadaRespostaVO:Lcom/itau/empresas/feature/credito/lis/model/LisConsultaDetalhadaRespostaVO;

    .line 354
    invoke-virtual {v2}, Lcom/itau/empresas/feature/credito/lis/model/LisConsultaDetalhadaRespostaVO;->getDiaPagamentoEncargo()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    aput-object v2, v1, v3

    .line 349
    const v2, 0x7f07040f

    invoke-virtual {p0, v2, v1}, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 355
    .local v4, "header":Ljava/lang/String;
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity;->rlLisConsultaDetalheHeader:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 357
    return-void
.end method

.method private configuraToolbar()V
    .registers 3

    .line 325
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity;->toolbar:Landroid/support/v7/widget/Toolbar;

    invoke-static {v0, p0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom;->Builder(Landroid/support/v7/widget/Toolbar;Landroid/support/v7/app/AppCompatActivity;)Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    move-result-object v0

    .line 326
    const v1, 0x7f070618

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->comTitulo(I)Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    move-result-object v0

    .line 327
    invoke-virtual {v0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->comClickListenerFinish()Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    move-result-object v0

    .line 328
    invoke-virtual {v0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->build()Lcom/itau/empresas/ui/util/listener/ToolbarCustom;

    .line 329
    return-void
.end method

.method private getHeader()Ljava/util/List;
    .registers 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Lcom/itau/empresas/ui/view/comprovante/ComprovanteLabelWrapper;>;"
        }
    .end annotation

    .line 187
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity;->tipoDeLis:Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity$TipoDeLis;

    if-eqz v0, :cond_31

    .line 188
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity;->tipoDeLis:Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity$TipoDeLis;

    sget-object v1, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity$TipoDeLis;->LIS:Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity$TipoDeLis;

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity$TipoDeLis;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 189
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity;->vincularHeader()Ljava/util/List;

    move-result-object v0

    return-object v0

    .line 190
    :cond_13
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity;->tipoDeLis:Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity$TipoDeLis;

    sget-object v1, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity$TipoDeLis;->LIS_RECEBIVEL:Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity$TipoDeLis;

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity$TipoDeLis;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_22

    .line 191
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity;->vincularHeaderRecebiveis()Ljava/util/List;

    move-result-object v0

    return-object v0

    .line 192
    :cond_22
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity;->tipoDeLis:Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity$TipoDeLis;

    sget-object v1, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity$TipoDeLis;->LIS_ADICIONAL:Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity$TipoDeLis;

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity$TipoDeLis;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_31

    .line 193
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity;->vincularHeaderAdicional()Ljava/util/List;

    move-result-object v0

    return-object v0

    .line 197
    :cond_31
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    return-object v0
.end method

.method private inicializaMaquinaDeEstado()Lcom/itau/empresas/ui/util/MaquinaDeEstado;
    .registers 7

    .line 151
    new-instance v0, Lcom/itau/empresas/ui/util/MaquinaDeEstado;

    invoke-direct {v0}, Lcom/itau/empresas/ui/util/MaquinaDeEstado;-><init>()V

    sget-object v1, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity$TipoDeLis;->LIS:Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity$TipoDeLis;

    new-instance v2, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;

    invoke-direct {v2}, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;-><init>()V

    const/4 v3, 0x2

    new-array v3, v3, [Landroid/view/View;

    iget-object v4, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity;->lisAdicionalAtencao:Landroid/widget/TextView;

    const/4 v5, 0x0

    aput-object v4, v3, v5

    iget-object v4, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity;->lisRecevivelAtencao:Landroid/widget/TextView;

    const/4 v5, 0x1

    aput-object v4, v3, v5

    .line 154
    invoke-virtual {v2, v3}, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;->gone([Landroid/view/View;)Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;

    move-result-object v2

    .line 155
    invoke-virtual {v2}, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;->build()Lcom/itau/empresas/ui/util/maquinaestado/Estado;

    move-result-object v2

    .line 152
    invoke-virtual {v0, v1, v2}, Lcom/itau/empresas/ui/util/MaquinaDeEstado;->novoEstado(Lcom/itau/empresas/ui/util/MaquinaDeEstado$Chave;Lcom/itau/empresas/ui/util/maquinaestado/Estado;)Lcom/itau/empresas/ui/util/MaquinaDeEstado;

    move-result-object v0

    sget-object v1, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity$TipoDeLis;->LIS_RECEBIVEL:Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity$TipoDeLis;

    new-instance v2, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;

    invoke-direct {v2}, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;-><init>()V

    const/4 v3, 0x1

    new-array v3, v3, [Landroid/view/View;

    iget-object v4, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity;->lisRecevivelAtencao:Landroid/widget/TextView;

    const/4 v5, 0x0

    aput-object v4, v3, v5

    .line 158
    invoke-virtual {v2, v3}, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;->visible([Landroid/view/View;)Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Landroid/view/View;

    iget-object v4, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity;->lisAdicionalAtencao:Landroid/widget/TextView;

    const/4 v5, 0x0

    aput-object v4, v3, v5

    .line 159
    invoke-virtual {v2, v3}, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;->gone([Landroid/view/View;)Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;

    move-result-object v2

    .line 160
    invoke-virtual {v2}, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;->build()Lcom/itau/empresas/ui/util/maquinaestado/Estado;

    move-result-object v2

    .line 156
    invoke-virtual {v0, v1, v2}, Lcom/itau/empresas/ui/util/MaquinaDeEstado;->novoEstado(Lcom/itau/empresas/ui/util/MaquinaDeEstado$Chave;Lcom/itau/empresas/ui/util/maquinaestado/Estado;)Lcom/itau/empresas/ui/util/MaquinaDeEstado;

    move-result-object v0

    sget-object v1, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity$TipoDeLis;->LIS_ADICIONAL:Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity$TipoDeLis;

    new-instance v2, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;

    invoke-direct {v2}, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;-><init>()V

    const/4 v3, 0x1

    new-array v3, v3, [Landroid/view/View;

    iget-object v4, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity;->lisAdicionalAtencao:Landroid/widget/TextView;

    const/4 v5, 0x0

    aput-object v4, v3, v5

    .line 163
    invoke-virtual {v2, v3}, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;->visible([Landroid/view/View;)Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Landroid/view/View;

    iget-object v4, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity;->lisRecevivelAtencao:Landroid/widget/TextView;

    const/4 v5, 0x0

    aput-object v4, v3, v5

    .line 164
    invoke-virtual {v2, v3}, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;->gone([Landroid/view/View;)Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;

    move-result-object v2

    .line 165
    invoke-virtual {v2}, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;->build()Lcom/itau/empresas/ui/util/maquinaestado/Estado;

    move-result-object v2

    .line 161
    invoke-virtual {v0, v1, v2}, Lcom/itau/empresas/ui/util/MaquinaDeEstado;->novoEstado(Lcom/itau/empresas/ui/util/MaquinaDeEstado$Chave;Lcom/itau/empresas/ui/util/maquinaestado/Estado;)Lcom/itau/empresas/ui/util/MaquinaDeEstado;

    move-result-object v0

    .line 151
    return-object v0
.end method

.method public static intent(Landroid/content/Context;)Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity_$IntentBuilder_;
    .registers 2
    .param p0, "context"    # Landroid/content/Context;

    .line 360
    new-instance v0, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity_$IntentBuilder_;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity_$IntentBuilder_;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method private setTipoDeLis()V
    .registers 4

    .line 170
    iget-object v1, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity;->tipoComprovante:Ljava/lang/String;

    const/4 v2, -0x1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v0

    packed-switch v0, :pswitch_data_3c

    goto :goto_28

    :pswitch_b
    const-string v0, "L01"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_28

    const/4 v2, 0x0

    goto :goto_28

    :pswitch_15
    const-string v0, "L02"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_28

    const/4 v2, 0x1

    goto :goto_28

    :pswitch_1f
    const-string v0, "L03"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_28

    const/4 v2, 0x2

    :cond_28
    :goto_28
    packed-switch v2, :pswitch_data_46

    goto :goto_3a

    .line 172
    :pswitch_2c
    sget-object v0, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity$TipoDeLis;->LIS:Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity$TipoDeLis;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity;->tipoDeLis:Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity$TipoDeLis;

    .line 173
    goto :goto_3a

    .line 175
    :pswitch_31
    sget-object v0, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity$TipoDeLis;->LIS_RECEBIVEL:Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity$TipoDeLis;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity;->tipoDeLis:Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity$TipoDeLis;

    .line 176
    goto :goto_3a

    .line 178
    :pswitch_36
    sget-object v0, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity$TipoDeLis;->LIS_ADICIONAL:Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity$TipoDeLis;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity;->tipoDeLis:Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity$TipoDeLis;

    .line 179
    .line 183
    :goto_3a
    return-void

    nop

    :pswitch_data_3c
    .packed-switch 0x1234d
        :pswitch_b
        :pswitch_15
        :pswitch_1f
    .end packed-switch

    :pswitch_data_46
    .packed-switch 0x0
        :pswitch_2c
        :pswitch_31
        :pswitch_36
    .end packed-switch
.end method

.method private validaWarning()V
    .registers 6

    .line 136
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity;->lisConsultaDetalhadaRespostaVO:Lcom/itau/empresas/feature/credito/lis/model/LisConsultaDetalhadaRespostaVO;

    .line 137
    invoke-virtual {v0}, Lcom/itau/empresas/feature/credito/lis/model/LisConsultaDetalhadaRespostaVO;->getDataVencimentoOperacao()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/itau/empresas/ui/util/DataEHoraUtils;->parse(Ljava/lang/String;)Lorg/threeten/bp/LocalDate;

    move-result-object v2

    .line 138
    .local v2, "dataMaxima":Lorg/threeten/bp/LocalDate;
    const-wide/16 v0, -0xb

    invoke-virtual {v2, v0, v1}, Lorg/threeten/bp/LocalDate;->plusDays(J)Lorg/threeten/bp/LocalDate;

    move-result-object v3

    .line 139
    .local v3, "dataMinima":Lorg/threeten/bp/LocalDate;
    const-wide/16 v0, 0x1

    invoke-virtual {v2, v0, v1}, Lorg/threeten/bp/LocalDate;->plusDays(J)Lorg/threeten/bp/LocalDate;

    move-result-object v2

    .line 141
    invoke-static {}, Lorg/threeten/bp/LocalDate;->now()Lorg/threeten/bp/LocalDate;

    move-result-object v4

    .line 143
    .local v4, "dataHoje":Lorg/threeten/bp/LocalDate;
    invoke-virtual {v4, v2}, Lorg/threeten/bp/LocalDate;->isBefore(Lorg/threeten/bp/chrono/ChronoLocalDate;)Z

    move-result v0

    if-eqz v0, :cond_2d

    invoke-virtual {v4, v3}, Lorg/threeten/bp/LocalDate;->isAfter(Lorg/threeten/bp/chrono/ChronoLocalDate;)Z

    move-result v0

    if-eqz v0, :cond_2d

    .line 144
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity;->warning:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_34

    .line 146
    :cond_2d
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity;->warning:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 148
    :goto_34
    return-void
.end method

.method private vincularHeader()Ljava/util/List;
    .registers 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Lcom/itau/empresas/ui/view/comprovante/ComprovanteLabelWrapper;>;"
        }
    .end annotation

    .line 202
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 204
    .line 205
    .local v3, "list":Ljava/util/List;, "Ljava/util/List<Lcom/itau/empresas/ui/view/comprovante/ComprovanteLabelWrapper;>;"
    const v0, 0x7f0705eb

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity;->lisConsultaDetalhadaRespostaVO:Lcom/itau/empresas/feature/credito/lis/model/LisConsultaDetalhadaRespostaVO;

    .line 207
    invoke-virtual {v1}, Lcom/itau/empresas/feature/credito/lis/model/LisConsultaDetalhadaRespostaVO;->getValorLimiteContratado()F

    move-result v1

    .line 206
    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/itau/empresas/ui/util/ValorMonetarioUtils;->formataValorDecimalParaMonetario(FZ)Ljava/lang/String;

    move-result-object v1

    .line 205
    invoke-static {v0, v1}, Lcom/itau/empresas/ui/view/comprovante/ComprovanteLabelWrapperImpl;->load(Ljava/lang/String;Ljava/lang/String;)Lcom/itau/empresas/ui/view/comprovante/ComprovanteLabelWrapper;

    move-result-object v0

    .line 204
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 208
    .line 209
    const v0, 0x7f0705e2

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity;->lisConsultaDetalhadaRespostaVO:Lcom/itau/empresas/feature/credito/lis/model/LisConsultaDetalhadaRespostaVO;

    .line 210
    invoke-virtual {v1}, Lcom/itau/empresas/feature/credito/lis/model/LisConsultaDetalhadaRespostaVO;->getDiaPagamentoEncargo()Ljava/lang/String;

    move-result-object v1

    .line 209
    invoke-static {v0, v1}, Lcom/itau/empresas/ui/view/comprovante/ComprovanteLabelWrapperImpl;->load(Ljava/lang/String;Ljava/lang/String;)Lcom/itau/empresas/ui/view/comprovante/ComprovanteLabelWrapper;

    move-result-object v0

    .line 208
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 212
    return-object v3
.end method

.method private vincularHeaderAdicional()Ljava/util/List;
    .registers 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Lcom/itau/empresas/ui/view/comprovante/ComprovanteLabelWrapper;>;"
        }
    .end annotation

    .line 244
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 246
    .line 247
    .local v3, "list":Ljava/util/List;, "Ljava/util/List<Lcom/itau/empresas/ui/view/comprovante/ComprovanteLabelWrapper;>;"
    const v0, 0x7f0705e4

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity;->lisConsultaDetalhadaRespostaVO:Lcom/itau/empresas/feature/credito/lis/model/LisConsultaDetalhadaRespostaVO;

    .line 249
    invoke-virtual {v1}, Lcom/itau/empresas/feature/credito/lis/model/LisConsultaDetalhadaRespostaVO;->getValorLimiteContratado()F

    move-result v1

    .line 248
    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/itau/empresas/ui/util/ValorMonetarioUtils;->formataValorDecimalParaMonetario(FZ)Ljava/lang/String;

    move-result-object v1

    .line 247
    invoke-static {v0, v1}, Lcom/itau/empresas/ui/view/comprovante/ComprovanteLabelWrapperImpl;->load(Ljava/lang/String;Ljava/lang/String;)Lcom/itau/empresas/ui/view/comprovante/ComprovanteLabelWrapper;

    move-result-object v0

    .line 246
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 251
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity;->lisConsultaDetalhadaRespostaVO:Lcom/itau/empresas/feature/credito/lis/model/LisConsultaDetalhadaRespostaVO;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/credito/lis/model/LisConsultaDetalhadaRespostaVO;->getValorLimiteAdicional()Ljava/lang/Float;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Float;->compareTo(Ljava/lang/Float;)I

    move-result v0

    if-nez v0, :cond_44

    .line 253
    .line 254
    const v0, 0x7f0705e5

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity;->lisConsultaDetalhadaRespostaVO:Lcom/itau/empresas/feature/credito/lis/model/LisConsultaDetalhadaRespostaVO;

    .line 255
    invoke-virtual {v1}, Lcom/itau/empresas/feature/credito/lis/model/LisConsultaDetalhadaRespostaVO;->getTextoValorLimiteAdicional()Ljava/lang/String;

    move-result-object v1

    .line 254
    invoke-static {v0, v1}, Lcom/itau/empresas/ui/view/comprovante/ComprovanteLabelWrapperImpl;->load(Ljava/lang/String;Ljava/lang/String;)Lcom/itau/empresas/ui/view/comprovante/ComprovanteLabelWrapper;

    move-result-object v0

    .line 253
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_61

    .line 259
    .line 260
    :cond_44
    const v0, 0x7f0705e5

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity;->lisConsultaDetalhadaRespostaVO:Lcom/itau/empresas/feature/credito/lis/model/LisConsultaDetalhadaRespostaVO;

    .line 262
    invoke-virtual {v1}, Lcom/itau/empresas/feature/credito/lis/model/LisConsultaDetalhadaRespostaVO;->getValorLimiteAdicional()Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    .line 261
    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/itau/empresas/ui/util/ValorMonetarioUtils;->formataValorDecimalParaMonetario(FZ)Ljava/lang/String;

    move-result-object v1

    .line 260
    invoke-static {v0, v1}, Lcom/itau/empresas/ui/view/comprovante/ComprovanteLabelWrapperImpl;->load(Ljava/lang/String;Ljava/lang/String;)Lcom/itau/empresas/ui/view/comprovante/ComprovanteLabelWrapper;

    move-result-object v0

    .line 259
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 266
    .line 267
    :goto_61
    const v0, 0x7f0705e2

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity;->lisConsultaDetalhadaRespostaVO:Lcom/itau/empresas/feature/credito/lis/model/LisConsultaDetalhadaRespostaVO;

    .line 268
    invoke-virtual {v1}, Lcom/itau/empresas/feature/credito/lis/model/LisConsultaDetalhadaRespostaVO;->getDiaPagamentoEncargo()Ljava/lang/String;

    move-result-object v1

    .line 267
    invoke-static {v0, v1}, Lcom/itau/empresas/ui/view/comprovante/ComprovanteLabelWrapperImpl;->load(Ljava/lang/String;Ljava/lang/String;)Lcom/itau/empresas/ui/view/comprovante/ComprovanteLabelWrapper;

    move-result-object v0

    .line 266
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 270
    return-object v3
.end method

.method private vincularHeaderRecebiveis()Ljava/util/List;
    .registers 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Lcom/itau/empresas/ui/view/comprovante/ComprovanteLabelWrapper;>;"
        }
    .end annotation

    .line 217
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 219
    .line 220
    .local v3, "list":Ljava/util/List;, "Ljava/util/List<Lcom/itau/empresas/ui/view/comprovante/ComprovanteLabelWrapper;>;"
    const v0, 0x7f0705de

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity;->lisConsultaDetalhadaRespostaVO:Lcom/itau/empresas/feature/credito/lis/model/LisConsultaDetalhadaRespostaVO;

    .line 221
    invoke-virtual {v1}, Lcom/itau/empresas/feature/credito/lis/model/LisConsultaDetalhadaRespostaVO;->getContaVinculada()Ljava/lang/String;

    move-result-object v1

    .line 220
    invoke-static {v0, v1}, Lcom/itau/empresas/ui/view/comprovante/ComprovanteLabelWrapperImpl;->load(Ljava/lang/String;Ljava/lang/String;)Lcom/itau/empresas/ui/view/comprovante/ComprovanteLabelWrapper;

    move-result-object v0

    .line 219
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 222
    .line 223
    const v0, 0x7f0705e4

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity;->lisConsultaDetalhadaRespostaVO:Lcom/itau/empresas/feature/credito/lis/model/LisConsultaDetalhadaRespostaVO;

    .line 225
    invoke-virtual {v1}, Lcom/itau/empresas/feature/credito/lis/model/LisConsultaDetalhadaRespostaVO;->getValorLimiteContratado()F

    move-result v1

    .line 224
    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/itau/empresas/ui/util/ValorMonetarioUtils;->formataValorDecimalParaMonetario(FZ)Ljava/lang/String;

    move-result-object v1

    .line 223
    invoke-static {v0, v1}, Lcom/itau/empresas/ui/view/comprovante/ComprovanteLabelWrapperImpl;->load(Ljava/lang/String;Ljava/lang/String;)Lcom/itau/empresas/ui/view/comprovante/ComprovanteLabelWrapper;

    move-result-object v0

    .line 222
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 226
    .line 227
    const v0, 0x7f0705e6

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity;->lisConsultaDetalhadaRespostaVO:Lcom/itau/empresas/feature/credito/lis/model/LisConsultaDetalhadaRespostaVO;

    .line 229
    invoke-virtual {v1}, Lcom/itau/empresas/feature/credito/lis/model/LisConsultaDetalhadaRespostaVO;->getValorLimiteContratadoRecebivel()F

    move-result v1

    .line 228
    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/itau/empresas/ui/util/ValorMonetarioUtils;->formataValorDecimalParaMonetario(FZ)Ljava/lang/String;

    move-result-object v1

    .line 227
    invoke-static {v0, v1}, Lcom/itau/empresas/ui/view/comprovante/ComprovanteLabelWrapperImpl;->load(Ljava/lang/String;Ljava/lang/String;)Lcom/itau/empresas/ui/view/comprovante/ComprovanteLabelWrapper;

    move-result-object v0

    .line 226
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 231
    .line 232
    const v0, 0x7f0705e8

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity;->lisConsultaDetalhadaRespostaVO:Lcom/itau/empresas/feature/credito/lis/model/LisConsultaDetalhadaRespostaVO;

    .line 234
    invoke-virtual {v1}, Lcom/itau/empresas/feature/credito/lis/model/LisConsultaDetalhadaRespostaVO;->getValorLimiteRecebivel()F

    move-result v1

    .line 233
    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/itau/empresas/ui/util/ValorMonetarioUtils;->formataValorDecimalParaMonetario(FZ)Ljava/lang/String;

    move-result-object v1

    .line 232
    invoke-static {v0, v1}, Lcom/itau/empresas/ui/view/comprovante/ComprovanteLabelWrapperImpl;->load(Ljava/lang/String;Ljava/lang/String;)Lcom/itau/empresas/ui/view/comprovante/ComprovanteLabelWrapper;

    move-result-object v0

    .line 231
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 235
    .line 236
    const v0, 0x7f0705e2

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity;->lisConsultaDetalhadaRespostaVO:Lcom/itau/empresas/feature/credito/lis/model/LisConsultaDetalhadaRespostaVO;

    .line 237
    invoke-virtual {v1}, Lcom/itau/empresas/feature/credito/lis/model/LisConsultaDetalhadaRespostaVO;->getDiaPagamentoEncargo()Ljava/lang/String;

    move-result-object v1

    .line 236
    invoke-static {v0, v1}, Lcom/itau/empresas/ui/view/comprovante/ComprovanteLabelWrapperImpl;->load(Ljava/lang/String;Ljava/lang/String;)Lcom/itau/empresas/ui/view/comprovante/ComprovanteLabelWrapper;

    move-result-object v0

    .line 235
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 239
    return-object v3
.end method

.method private vincularItem()Ljava/util/List;
    .registers 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Lcom/itau/empresas/ui/view/comprovante/ComprovanteLabelWrapper;>;"
        }
    .end annotation

    .line 275
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 277
    .line 278
    .local v5, "list":Ljava/util/List;, "Ljava/util/List<Lcom/itau/empresas/ui/view/comprovante/ComprovanteLabelWrapper;>;"
    const v0, 0x7f0705ec

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity;->lisConsultaDetalhadaRespostaVO:Lcom/itau/empresas/feature/credito/lis/model/LisConsultaDetalhadaRespostaVO;

    .line 280
    invoke-virtual {v1}, Lcom/itau/empresas/feature/credito/lis/model/LisConsultaDetalhadaRespostaVO;->getValorTarifaContratacao()F

    move-result v1

    .line 279
    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/itau/empresas/ui/util/ValorMonetarioUtils;->formataValorDecimalParaMonetario(FZ)Ljava/lang/String;

    move-result-object v1

    .line 278
    invoke-static {v0, v1}, Lcom/itau/empresas/ui/view/comprovante/ComprovanteLabelWrapperImpl;->load(Ljava/lang/String;Ljava/lang/String;)Lcom/itau/empresas/ui/view/comprovante/ComprovanteLabelWrapper;

    move-result-object v0

    .line 277
    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 281
    .line 282
    const v0, 0x7f0705ea

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity;->lisConsultaDetalhadaRespostaVO:Lcom/itau/empresas/feature/credito/lis/model/LisConsultaDetalhadaRespostaVO;

    .line 284
    invoke-virtual {v1}, Lcom/itau/empresas/feature/credito/lis/model/LisConsultaDetalhadaRespostaVO;->getValorIof()F

    move-result v1

    .line 283
    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/itau/empresas/ui/util/ValorMonetarioUtils;->formataValorDecimalParaMonetario(FZ)Ljava/lang/String;

    move-result-object v1

    .line 282
    invoke-static {v0, v1}, Lcom/itau/empresas/ui/view/comprovante/ComprovanteLabelWrapperImpl;->load(Ljava/lang/String;Ljava/lang/String;)Lcom/itau/empresas/ui/view/comprovante/ComprovanteLabelWrapper;

    move-result-object v0

    .line 281
    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 285
    .line 286
    const v0, 0x7f0705ed

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity;->lisConsultaDetalhadaRespostaVO:Lcom/itau/empresas/feature/credito/lis/model/LisConsultaDetalhadaRespostaVO;

    .line 288
    invoke-virtual {v1}, Lcom/itau/empresas/feature/credito/lis/model/LisConsultaDetalhadaRespostaVO;->getValorTotalOperacao()F

    move-result v1

    .line 287
    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/itau/empresas/ui/util/ValorMonetarioUtils;->formataValorDecimalParaMonetario(FZ)Ljava/lang/String;

    move-result-object v1

    .line 286
    invoke-static {v0, v1}, Lcom/itau/empresas/ui/view/comprovante/ComprovanteLabelWrapperImpl;->load(Ljava/lang/String;Ljava/lang/String;)Lcom/itau/empresas/ui/view/comprovante/ComprovanteLabelWrapper;

    move-result-object v0

    .line 285
    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 289
    .line 290
    const v0, 0x7f0705e9

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 291
    const v1, 0x7f0705da

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity;->lisConsultaDetalhadaRespostaVO:Lcom/itau/empresas/feature/credito/lis/model/LisConsultaDetalhadaRespostaVO;

    .line 293
    invoke-virtual {v3}, Lcom/itau/empresas/feature/credito/lis/model/LisConsultaDetalhadaRespostaVO;->getTaxaJurosMes()F

    move-result v3

    .line 292
    invoke-static {v3}, Lcom/itau/empresas/ui/util/PorcentagemUtils;->formataPorcentagemSimples(F)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    aput-object v3, v2, v4

    iget-object v3, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity;->lisConsultaDetalhadaRespostaVO:Lcom/itau/empresas/feature/credito/lis/model/LisConsultaDetalhadaRespostaVO;

    .line 295
    invoke-virtual {v3}, Lcom/itau/empresas/feature/credito/lis/model/LisConsultaDetalhadaRespostaVO;->getTaxaJurosAno()F

    move-result v3

    .line 294
    invoke-static {v3}, Lcom/itau/empresas/ui/util/PorcentagemUtils;->formataPorcentagemSimples(F)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    aput-object v3, v2, v4

    .line 291
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 290
    invoke-static {v0, v1}, Lcom/itau/empresas/ui/view/comprovante/ComprovanteLabelWrapperImpl;->load(Ljava/lang/String;Ljava/lang/String;)Lcom/itau/empresas/ui/view/comprovante/ComprovanteLabelWrapper;

    move-result-object v0

    .line 289
    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 296
    .line 297
    const v0, 0x7f0705db

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 298
    const v1, 0x7f0705da

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity;->lisConsultaDetalhadaRespostaVO:Lcom/itau/empresas/feature/credito/lis/model/LisConsultaDetalhadaRespostaVO;

    .line 300
    invoke-virtual {v3}, Lcom/itau/empresas/feature/credito/lis/model/LisConsultaDetalhadaRespostaVO;->getTaxaCetMes()F

    move-result v3

    .line 299
    invoke-static {v3}, Lcom/itau/empresas/ui/util/PorcentagemUtils;->formataPorcentagemSimples(F)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    aput-object v3, v2, v4

    iget-object v3, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity;->lisConsultaDetalhadaRespostaVO:Lcom/itau/empresas/feature/credito/lis/model/LisConsultaDetalhadaRespostaVO;

    .line 302
    invoke-virtual {v3}, Lcom/itau/empresas/feature/credito/lis/model/LisConsultaDetalhadaRespostaVO;->getTaxaCetAno()F

    move-result v3

    .line 301
    invoke-static {v3}, Lcom/itau/empresas/ui/util/PorcentagemUtils;->formataPorcentagemSimples(F)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    aput-object v3, v2, v4

    .line 298
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 297
    invoke-static {v0, v1}, Lcom/itau/empresas/ui/view/comprovante/ComprovanteLabelWrapperImpl;->load(Ljava/lang/String;Ljava/lang/String;)Lcom/itau/empresas/ui/view/comprovante/ComprovanteLabelWrapper;

    move-result-object v0

    .line 296
    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 303
    .line 304
    const v0, 0x7f0705e7

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity;->lisConsultaDetalhadaRespostaVO:Lcom/itau/empresas/feature/credito/lis/model/LisConsultaDetalhadaRespostaVO;

    .line 305
    invoke-virtual {v1}, Lcom/itau/empresas/feature/credito/lis/model/LisConsultaDetalhadaRespostaVO;->getPeriodicidadeCapitalizacao()Ljava/lang/String;

    move-result-object v1

    .line 306
    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    .line 304
    invoke-static {v0, v1}, Lcom/itau/empresas/ui/view/comprovante/ComprovanteLabelWrapperImpl;->load(Ljava/lang/String;Ljava/lang/String;)Lcom/itau/empresas/ui/view/comprovante/ComprovanteLabelWrapper;

    move-result-object v0

    .line 303
    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 307
    .line 308
    const v0, 0x7f0705e1

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity;->lisConsultaDetalhadaRespostaVO:Lcom/itau/empresas/feature/credito/lis/model/LisConsultaDetalhadaRespostaVO;

    .line 310
    invoke-virtual {v1}, Lcom/itau/empresas/feature/credito/lis/model/LisConsultaDetalhadaRespostaVO;->getDataVencimentoOperacao()Ljava/lang/String;

    move-result-object v1

    .line 309
    invoke-static {v1}, Lcom/itau/empresas/ui/util/DataEHoraUtils;->formataDataComBarra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 308
    invoke-static {v0, v1}, Lcom/itau/empresas/ui/view/comprovante/ComprovanteLabelWrapperImpl;->load(Ljava/lang/String;Ljava/lang/String;)Lcom/itau/empresas/ui/view/comprovante/ComprovanteLabelWrapper;

    move-result-object v0

    .line 307
    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 312
    return-object v5
.end method


# virtual methods
.method aoCarregarTela()V
    .registers 8

    .line 92
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity;->configuraToolbar()V

    .line 94
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity;->setTipoDeLis()V

    .line 96
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity;->inicializaMaquinaDeEstado()Lcom/itau/empresas/ui/util/MaquinaDeEstado;

    move-result-object v5

    .line 98
    .local v5, "maquinaDeEstado":Lcom/itau/empresas/ui/util/MaquinaDeEstado;
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity;->validaWarning()V

    .line 101
    .line 102
    :try_start_d
    invoke-static {p0}, Lcom/itau/empresas/ui/view/comprovante/GeradoritensComprovanteBuilder;->builder(Landroid/content/Context;)Lcom/itau/empresas/ui/view/comprovante/GeradoritensComprovanteBuilder;

    move-result-object v0

    .line 103
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity;->getHeader()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/view/comprovante/GeradoritensComprovanteBuilder;->itensComprovanteList(Ljava/util/List;)Lcom/itau/empresas/ui/view/comprovante/GeradoritensComprovanteBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity;->comprovanteHeader:Landroid/widget/LinearLayout;

    .line 104
    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/view/comprovante/GeradoritensComprovanteBuilder;->targetLayout(Landroid/widget/LinearLayout;)Lcom/itau/empresas/ui/view/comprovante/GeradoritensComprovanteBuilder;

    move-result-object v0

    .line 105
    invoke-virtual {v0}, Lcom/itau/empresas/ui/view/comprovante/GeradoritensComprovanteBuilder;->build()V

    .line 107
    .line 108
    invoke-static {p0}, Lcom/itau/empresas/ui/view/comprovante/GeradoritensComprovanteBuilder;->builder(Landroid/content/Context;)Lcom/itau/empresas/ui/view/comprovante/GeradoritensComprovanteBuilder;

    move-result-object v0

    .line 109
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity;->vincularItem()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/view/comprovante/GeradoritensComprovanteBuilder;->itensComprovanteList(Ljava/util/List;)Lcom/itau/empresas/ui/view/comprovante/GeradoritensComprovanteBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity;->comprovanteValores:Landroid/widget/LinearLayout;

    .line 110
    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/view/comprovante/GeradoritensComprovanteBuilder;->targetLayout(Landroid/widget/LinearLayout;)Lcom/itau/empresas/ui/view/comprovante/GeradoritensComprovanteBuilder;

    move-result-object v0

    .line 111
    invoke-virtual {v0}, Lcom/itau/empresas/ui/view/comprovante/GeradoritensComprovanteBuilder;->build()V
    :try_end_37
    .catch Ljava/lang/IllegalAccessException; {:try_start_d .. :try_end_37} :catch_38

    .line 115
    goto :goto_42

    .line 113
    :catch_38
    move-exception v6

    .line 114
    .local v6, "e":Ljava/lang/IllegalAccessException;
    const-string v0, "LisConsultaDetalhe"

    invoke-virtual {v6}, Ljava/lang/IllegalAccessException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v6}, Lbr/com/itau/textovoz/util/Logger;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 117
    .end local v6    # "e":Ljava/lang/IllegalAccessException;
    :goto_42
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity;->descricaoProduto:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity;->lisConsultaDetalhadaRespostaVO:Lcom/itau/empresas/feature/credito/lis/model/LisConsultaDetalhadaRespostaVO;

    .line 118
    invoke-virtual {v1}, Lcom/itau/empresas/feature/credito/lis/model/LisConsultaDetalhadaRespostaVO;->getDescricaoProduto()Ljava/lang/String;

    move-result-object v1

    .line 117
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 120
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity;->textoDadoscontroleDescricao:Landroid/widget/TextView;

    .line 121
    const v1, 0x7f0705e3

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    .line 122
    invoke-static {}, Lcom/itau/empresas/ui/util/DataEHoraUtils;->getDataAtualString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/itau/empresas/ui/util/DataEHoraUtils;->formataDataComBarra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    aput-object v3, v2, v4

    .line 123
    invoke-static {}, Lcom/itau/empresas/ui/util/DataEHoraUtils;->getHoraAtual()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/itau/empresas/ui/util/DataEHoraUtils;->formataHorario(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    aput-object v3, v2, v4

    .line 121
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 120
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 125
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity;->tipoDeLis:Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity$TipoDeLis;

    invoke-virtual {v5, v0}, Lcom/itau/empresas/ui/util/MaquinaDeEstado;->mudaParaEstado(Lcom/itau/empresas/ui/util/MaquinaDeEstado$Chave;)Lcom/itau/empresas/ui/util/MaquinaDeEstado$Chave;

    .line 127
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity;->configuraAcessibilidade()V

    .line 128
    return-void
.end method

.method compartilhar(Landroid/view/View;)V
    .registers 3
    .param p1, "v"    # Landroid/view/View;

    .line 338
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 339
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity;->compartilhaComprovante()V

    .line 340
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 341
    return-void
.end method

.method public hitAnalytics()Ljava/lang/String;
    .registers 2

    .line 86
    const v0, 0x7f070333

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public onClick(Landroid/view/View;)V
    .registers 2
    .param p1, "v"    # Landroid/view/View;

    .line 345
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisConsultaDetalheActivity;->onBackPressed()V

    .line 346
    return-void
.end method

.method public opkeysConsumidas()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Ljava/lang/String;>;"
        }
    .end annotation

    .line 333
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    return-object v0
.end method

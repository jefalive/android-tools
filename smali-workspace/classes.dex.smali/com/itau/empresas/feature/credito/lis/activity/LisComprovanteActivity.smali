.class public Lcom/itau/empresas/feature/credito/lis/activity/LisComprovanteActivity;
.super Lcom/itau/empresas/ui/activity/BaseActivity;
.source "LisComprovanteActivity.java"


# instance fields
.field contratacaoRespostaVO:Lcom/itau/empresas/feature/credito/lis/model/LisContratacaoRespostaVO;

.field lisAdicional:Z

.field lisController:Lcom/itau/empresas/feature/credito/lis/LisController;

.field root:Landroid/widget/LinearLayout;

.field textoContratacaoSucesso:Landroid/widget/TextView;

.field textoLisComprovanteProduto:Landroid/widget/TextView;

.field textoLisDataVencimento:Landroid/widget/TextView;

.field textoLisValorLimite:Landroid/widget/TextView;

.field toolBar:Landroid/support/v7/widget/Toolbar;


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 37
    invoke-direct {p0}, Lcom/itau/empresas/ui/activity/BaseActivity;-><init>()V

    return-void
.end method

.method private configuraLayout()V
    .registers 8

    .line 110
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisComprovanteActivity;->textoContratacaoSucesso:Landroid/widget/TextView;

    .line 112
    const v1, 0x7f0c014d

    invoke-static {p0, v1}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v1

    sget-object v2, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    .line 113
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    const v3, 0x7f0705b2

    invoke-virtual {p0, v3}, Lcom/itau/empresas/feature/credito/lis/activity/LisComprovanteActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const-string v5, "com sucesso ;)"

    const/4 v6, 0x0

    aput-object v5, v4, v6

    .line 112
    invoke-static {v1, v2, v3, v4}, Lcom/itau/empresas/ui/util/ViewUtils;->destacaStringAlteraCor(IZLjava/lang/String;[Ljava/lang/String;)Landroid/text/Spannable;

    move-result-object v1

    .line 110
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 116
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisComprovanteActivity;->textoLisComprovanteProduto:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisComprovanteActivity;->contratacaoRespostaVO:Lcom/itau/empresas/feature/credito/lis/model/LisContratacaoRespostaVO;

    invoke-virtual {v1}, Lcom/itau/empresas/feature/credito/lis/model/LisContratacaoRespostaVO;->getDescricaoProduto()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 117
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisComprovanteActivity;->textoLisValorLimite:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisComprovanteActivity;->contratacaoRespostaVO:Lcom/itau/empresas/feature/credito/lis/model/LisContratacaoRespostaVO;

    .line 119
    invoke-virtual {v1}, Lcom/itau/empresas/feature/credito/lis/model/LisContratacaoRespostaVO;->getValorLimite()Ljava/math/BigDecimal;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/itau/empresas/ui/util/ValorMonetarioUtils;->formataValorDecimalParaMonetario(Ljava/math/BigDecimal;Z)Ljava/lang/String;

    move-result-object v1

    .line 117
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 121
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisComprovanteActivity;->textoLisDataVencimento:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisComprovanteActivity;->contratacaoRespostaVO:Lcom/itau/empresas/feature/credito/lis/model/LisContratacaoRespostaVO;

    .line 123
    invoke-virtual {v1}, Lcom/itau/empresas/feature/credito/lis/model/LisContratacaoRespostaVO;->getDataVencimentoOperacao()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/itau/empresas/ui/util/DataEHoraUtils;->formataDataComBarra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 121
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 124
    return-void
.end method

.method private constroiIdComprovante()Ljava/lang/String;
    .registers 3

    .line 128
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisComprovanteActivity;->contratacaoRespostaVO:Lcom/itau/empresas/feature/credito/lis/model/LisContratacaoRespostaVO;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/credito/lis/model/LisContratacaoRespostaVO;->getDataHoraContratacao()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisComprovanteActivity;->contratacaoRespostaVO:Lcom/itau/empresas/feature/credito/lis/model/LisContratacaoRespostaVO;

    .line 129
    invoke-virtual {v1}, Lcom/itau/empresas/feature/credito/lis/model/LisContratacaoRespostaVO;->getCodigoTipoComprovante()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisComprovanteActivity;->contratacaoRespostaVO:Lcom/itau/empresas/feature/credito/lis/model/LisContratacaoRespostaVO;

    .line 130
    invoke-virtual {v1}, Lcom/itau/empresas/feature/credito/lis/model/LisContratacaoRespostaVO;->getCodigoVersaoComprovante()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 128
    return-object v0
.end method

.method private constroiToolbar()V
    .registers 3

    .line 102
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisComprovanteActivity;->toolBar:Landroid/support/v7/widget/Toolbar;

    invoke-static {v0, p0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom;->Builder(Landroid/support/v7/widget/Toolbar;Landroid/support/v7/app/AppCompatActivity;)Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    move-result-object v0

    .line 104
    const v1, 0x7f07040b

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->comTitulo(I)Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    move-result-object v0

    .line 105
    invoke-virtual {v0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->comClickListenerFinish()Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    move-result-object v0

    .line 106
    invoke-virtual {v0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->build()Lcom/itau/empresas/ui/util/listener/ToolbarCustom;

    .line 107
    return-void
.end method

.method public static intent(Landroid/content/Context;)Lcom/itau/empresas/feature/credito/lis/activity/LisComprovanteActivity_$IntentBuilder_;
    .registers 2
    .param p0, "context"    # Landroid/content/Context;

    .line 134
    new-instance v0, Lcom/itau/empresas/feature/credito/lis/activity/LisComprovanteActivity_$IntentBuilder_;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisComprovanteActivity_$IntentBuilder_;-><init>(Landroid/content/Context;)V

    return-object v0
.end method


# virtual methods
.method aoCarregarTela()V
    .registers 1

    .line 73
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisComprovanteActivity;->configuraLayout()V

    .line 74
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisComprovanteActivity;->constroiToolbar()V

    .line 75
    return-void
.end method

.method public hitAnalytics()Ljava/lang/String;
    .registers 2

    .line 56
    const v0, 0x7f070332

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/credito/lis/activity/LisComprovanteActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public onBackPressed()V
    .registers 2

    .line 66
    .line 67
    invoke-static {p0}, Lcom/itau/empresas/feature/home/HomeLogadaActivity;->intent(Landroid/content/Context;)Lcom/itau/empresas/feature/home/HomeLogadaActivity_$IntentBuilder_;

    move-result-object v0

    .line 68
    invoke-virtual {v0}, Lcom/itau/empresas/feature/home/HomeLogadaActivity_$IntentBuilder_;->start()Lorg/androidannotations/api/builder/PostActivityStarter;

    .line 69
    return-void
.end method

.method public onEventMainThread(Lcom/itau/empresas/api/model/ComprovanteServiceVO;)V
    .registers 5
    .param p1, "comprovanteIdentificadorVO"    # Lcom/itau/empresas/api/model/ComprovanteServiceVO;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Lcom/itau/empresas/api/model/ComprovanteServiceVO<Lcom/itau/empresas/api/model/CampoComprovanteLisConsultaVO;>;)V"
        }
    .end annotation

    .line 86
    if-eqz p1, :cond_2b

    .line 87
    invoke-virtual {p1}, Lcom/itau/empresas/api/model/ComprovanteServiceVO;->getComprovantes()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_2b

    .line 88
    invoke-virtual {p1}, Lcom/itau/empresas/api/model/ComprovanteServiceVO;->getComprovantes()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2b

    .line 89
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisComprovanteActivity;->escondeDialogoDeProgresso()V

    .line 90
    .line 91
    invoke-static {p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisComprovanteDetalheActivity;->intent(Landroid/content/Context;)Lcom/itau/empresas/feature/credito/lis/activity/LisComprovanteDetalheActivity_$IntentBuilder_;

    move-result-object v0

    .line 92
    invoke-virtual {v0, p1}, Lcom/itau/empresas/feature/credito/lis/activity/LisComprovanteDetalheActivity_$IntentBuilder_;->comprovante(Lcom/itau/empresas/api/model/ComprovanteServiceVO;)Lcom/itau/empresas/feature/credito/lis/activity/LisComprovanteDetalheActivity_$IntentBuilder_;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisComprovanteActivity;->contratacaoRespostaVO:Lcom/itau/empresas/feature/credito/lis/model/LisContratacaoRespostaVO;

    .line 93
    invoke-virtual {v1}, Lcom/itau/empresas/feature/credito/lis/model/LisContratacaoRespostaVO;->getCodigoProduto()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/credito/lis/activity/LisComprovanteDetalheActivity_$IntentBuilder_;->tipoComprovante(Ljava/lang/String;)Lcom/itau/empresas/feature/credito/lis/activity/LisComprovanteDetalheActivity_$IntentBuilder_;

    move-result-object v0

    .line 94
    invoke-virtual {v0}, Lcom/itau/empresas/feature/credito/lis/activity/LisComprovanteDetalheActivity_$IntentBuilder_;->start()Lorg/androidannotations/api/builder/PostActivityStarter;

    goto :goto_37

    .line 96
    :cond_2b
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisComprovanteActivity;->root:Landroid/widget/LinearLayout;

    const-string v1, "Sem comprovantes dispon\u00edveis"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lbr/com/itau/topsnackbar/TopSnackbar;->make(Landroid/view/ViewGroup;Ljava/lang/CharSequence;I)Lbr/com/itau/topsnackbar/TopSnackbar;

    move-result-object v0

    .line 97
    invoke-virtual {v0}, Lbr/com/itau/topsnackbar/TopSnackbar;->show()Lbr/com/itau/topsnackbar/TopSnackbar;

    .line 99
    :goto_37
    return-void
.end method

.method public opkeysConsumidas()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Ljava/lang/String;>;"
        }
    .end annotation

    .line 61
    const-string v0, "comprovanteId"

    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method verDetalhes()V
    .registers 3

    .line 79
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisComprovanteActivity;->mostraDialogoDeProgresso()V

    .line 80
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisComprovanteActivity;->lisController:Lcom/itau/empresas/feature/credito/lis/LisController;

    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisComprovanteActivity;->constroiIdComprovante()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/credito/lis/LisController;->consultaContratadaComprovante(Ljava/lang/String;)V

    .line 81
    return-void
.end method

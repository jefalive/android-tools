.class public Lcom/itau/empresas/api/model/FrancesinhaVO;
.super Ljava/lang/Object;
.source "FrancesinhaVO.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/itau/empresas/api/model/FrancesinhaVO$DadosPeriodo;,
        Lcom/itau/empresas/api/model/FrancesinhaVO$Periodo;,
        Lcom/itau/empresas/api/model/FrancesinhaVO$ValorEQuantidade;,
        Lcom/itau/empresas/api/model/FrancesinhaVO$AVencer;,
        Lcom/itau/empresas/api/model/FrancesinhaVO$CobrancComRegistro;,
        Lcom/itau/empresas/api/model/FrancesinhaVO$ExtratoResumido;
    }
.end annotation


# instance fields
.field private extratosResumidos:[Lcom/itau/empresas/api/model/FrancesinhaVO$ExtratoResumido;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "extratos_resumidos"
    .end annotation
.end field

.field private totalValorDisponivel:Ljava/lang/Float;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "total_valor_disponivel"
    .end annotation
.end field

.field private totalValorNaoDisponivel:Ljava/lang/Float;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "total_valor_nao_disponivel"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getExtratosResumidos()[Lcom/itau/empresas/api/model/FrancesinhaVO$ExtratoResumido;
    .registers 2

    .line 19
    iget-object v0, p0, Lcom/itau/empresas/api/model/FrancesinhaVO;->extratosResumidos:[Lcom/itau/empresas/api/model/FrancesinhaVO$ExtratoResumido;

    return-object v0
.end method

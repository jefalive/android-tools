.class public final Lbr/com/concretesolutions/canarinho/formatador/FormatadorCPFCNPJ;
.super Ljava/lang/Object;
.source "FormatadorCPFCNPJ.java"

# interfaces
.implements Lbr/com/concretesolutions/canarinho/formatador/Formatador;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lbr/com/concretesolutions/canarinho/formatador/FormatadorCPFCNPJ$SingletonHolder;
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .registers 1

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    return-void
.end method

.method synthetic constructor <init>(Lbr/com/concretesolutions/canarinho/formatador/FormatadorCPFCNPJ$1;)V
    .registers 2
    .param p1, "x0"    # Lbr/com/concretesolutions/canarinho/formatador/FormatadorCPFCNPJ$1;

    .line 8
    invoke-direct {p0}, Lbr/com/concretesolutions/canarinho/formatador/FormatadorCPFCNPJ;-><init>()V

    return-void
.end method

.method private ehCpf(Ljava/lang/String;)Z
    .registers 5
    .param p1, "value"    # Ljava/lang/String;

    .line 58
    if-nez p1, :cond_a

    .line 59
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Valor n\u00e3o pode ser nulo"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 62
    :cond_a
    sget-object v0, Lbr/com/concretesolutions/canarinho/formatador/Formatador$Padroes;->PADRAO_SOMENTE_NUMEROS:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/util/regex/Matcher;->replaceAll(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 63
    .local v2, "desformatado":Ljava/lang/String;
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v1, 0xc

    if-ge v0, v1, :cond_20

    const/4 v0, 0x1

    goto :goto_21

    :cond_20
    const/4 v0, 0x0

    :goto_21
    return v0
.end method

.method static getInstance()Lbr/com/concretesolutions/canarinho/formatador/FormatadorCPFCNPJ;
    .registers 1

    .line 14
    # getter for: Lbr/com/concretesolutions/canarinho/formatador/FormatadorCPFCNPJ$SingletonHolder;->INSTANCE:Lbr/com/concretesolutions/canarinho/formatador/FormatadorCPFCNPJ;
    invoke-static {}, Lbr/com/concretesolutions/canarinho/formatador/FormatadorCPFCNPJ$SingletonHolder;->access$000()Lbr/com/concretesolutions/canarinho/formatador/FormatadorCPFCNPJ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public desformata(Ljava/lang/String;)Ljava/lang/String;
    .registers 3
    .param p1, "value"    # Ljava/lang/String;

    .line 28
    invoke-direct {p0, p1}, Lbr/com/concretesolutions/canarinho/formatador/FormatadorCPFCNPJ;->ehCpf(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 29
    sget-object v0, Lbr/com/concretesolutions/canarinho/formatador/Formatador;->CPF:Lbr/com/concretesolutions/canarinho/formatador/Formatador;

    invoke-interface {v0, p1}, Lbr/com/concretesolutions/canarinho/formatador/Formatador;->desformata(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 32
    :cond_d
    sget-object v0, Lbr/com/concretesolutions/canarinho/formatador/Formatador;->CNPJ:Lbr/com/concretesolutions/canarinho/formatador/Formatador;

    invoke-interface {v0, p1}, Lbr/com/concretesolutions/canarinho/formatador/Formatador;->desformata(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public estaFormatado(Ljava/lang/String;)Z
    .registers 3
    .param p1, "value"    # Ljava/lang/String;

    .line 37
    invoke-direct {p0, p1}, Lbr/com/concretesolutions/canarinho/formatador/FormatadorCPFCNPJ;->ehCpf(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 38
    sget-object v0, Lbr/com/concretesolutions/canarinho/formatador/Formatador;->CPF:Lbr/com/concretesolutions/canarinho/formatador/Formatador;

    invoke-interface {v0, p1}, Lbr/com/concretesolutions/canarinho/formatador/Formatador;->estaFormatado(Ljava/lang/String;)Z

    move-result v0

    return v0

    .line 41
    :cond_d
    sget-object v0, Lbr/com/concretesolutions/canarinho/formatador/Formatador;->CNPJ:Lbr/com/concretesolutions/canarinho/formatador/Formatador;

    invoke-interface {v0, p1}, Lbr/com/concretesolutions/canarinho/formatador/Formatador;->estaFormatado(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public formata(Ljava/lang/String;)Ljava/lang/String;
    .registers 3
    .param p1, "value"    # Ljava/lang/String;

    .line 19
    invoke-direct {p0, p1}, Lbr/com/concretesolutions/canarinho/formatador/FormatadorCPFCNPJ;->ehCpf(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 20
    sget-object v0, Lbr/com/concretesolutions/canarinho/formatador/Formatador;->CPF:Lbr/com/concretesolutions/canarinho/formatador/Formatador;

    invoke-interface {v0, p1}, Lbr/com/concretesolutions/canarinho/formatador/Formatador;->formata(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 23
    :cond_d
    sget-object v0, Lbr/com/concretesolutions/canarinho/formatador/Formatador;->CNPJ:Lbr/com/concretesolutions/canarinho/formatador/Formatador;

    invoke-interface {v0, p1}, Lbr/com/concretesolutions/canarinho/formatador/Formatador;->formata(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public podeSerFormatado(Ljava/lang/String;)Z
    .registers 3
    .param p1, "value"    # Ljava/lang/String;

    .line 46
    if-nez p1, :cond_4

    .line 47
    const/4 v0, 0x0

    return v0

    .line 50
    :cond_4
    invoke-direct {p0, p1}, Lbr/com/concretesolutions/canarinho/formatador/FormatadorCPFCNPJ;->ehCpf(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 51
    sget-object v0, Lbr/com/concretesolutions/canarinho/formatador/Formatador;->CPF:Lbr/com/concretesolutions/canarinho/formatador/Formatador;

    invoke-interface {v0, p1}, Lbr/com/concretesolutions/canarinho/formatador/Formatador;->podeSerFormatado(Ljava/lang/String;)Z

    move-result v0

    return v0

    .line 54
    :cond_11
    sget-object v0, Lbr/com/concretesolutions/canarinho/formatador/Formatador;->CNPJ:Lbr/com/concretesolutions/canarinho/formatador/Formatador;

    invoke-interface {v0, p1}, Lbr/com/concretesolutions/canarinho/formatador/Formatador;->podeSerFormatado(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.class public Lcom/itau/empresas/feature/feedback/FeedbackDialogBeta;
.super Lcom/itau/empresas/feature/feedback/FeedbackDialog;
.source "FeedbackDialogBeta.java"


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 14
    invoke-direct {p0}, Lcom/itau/empresas/feature/feedback/FeedbackDialog;-><init>()V

    return-void
.end method

.method private chamaIntentLojaBetaActivity(Landroid/content/Context;)V
    .registers 4
    .param p1, "context"    # Landroid/content/Context;

    .line 62
    new-instance v1, Landroid/content/Intent;

    const-string v0, "android.intent.action.VIEW"

    invoke-direct {v1, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 63
    .local v1, "i":Landroid/content/Intent;
    const-string v0, "http://play.google.com/apps/testing/com.itau.empresas/"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 64
    invoke-virtual {p1, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 65
    return-void
.end method


# virtual methods
.method protected agoraNaoEventoGA()V
    .registers 2

    .line 30
    invoke-super {p0}, Lcom/itau/empresas/feature/feedback/FeedbackDialog;->agoraNaoEventoGA()V

    .line 33
    invoke-virtual {p0}, Lcom/itau/empresas/feature/feedback/FeedbackDialogBeta;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/itau/empresas/feature/feedback/FeedbackDialogBeta;->chamaIntentLojaBetaActivity(Landroid/content/Context;)V

    .line 34
    return-void
.end method

.method public aoInicializar()V
    .registers 3

    .line 19
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/feedback/FeedbackDialogBeta;->setBeta(Ljava/lang/Boolean;)V

    .line 20
    invoke-super {p0}, Lcom/itau/empresas/feature/feedback/FeedbackDialog;->aoInicializar()V

    .line 22
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/feedback/FeedbackDialogBeta;->setFeedbackEspontaneoFlag(Ljava/lang/Boolean;)V

    .line 23
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/feedback/FeedbackDialogBeta;->setCancelable(Z)V

    .line 24
    iget-object v0, p0, Lcom/itau/empresas/feature/feedback/FeedbackDialogBeta;->textoFeedbackMensagem:Landroid/widget/TextView;

    const v1, 0x7f070113

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 25
    iget-object v0, p0, Lcom/itau/empresas/feature/feedback/FeedbackDialogBeta;->botaoAvaliar:Landroid/widget/Button;

    const v1, 0x7f070146

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    .line 26
    return-void
.end method

.method protected enviarAvaliacao()V
    .registers 10

    .line 45
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/feedback/FeedbackDialogBeta;->validaDadosObrigatorios(I)Z

    move-result v0

    if-eqz v0, :cond_44

    .line 46
    iget-object v0, p0, Lcom/itau/empresas/feature/feedback/FeedbackDialogBeta;->spinnerCategoria:Lbr/com/itau/widgets/material/MaterialSpinner;

    invoke-virtual {v0}, Lbr/com/itau/widgets/material/MaterialSpinner;->getSelectedItemPosition()I

    move-result v6

    .line 47
    .local v6, "selectedIndex":I
    const/4 v7, 0x0

    .line 50
    .local v7, "codigoCategoria":I
    if-eqz v6, :cond_1d

    .line 51
    iget-object v0, p0, Lcom/itau/empresas/feature/feedback/FeedbackDialogBeta;->spinnerCategoria:Lbr/com/itau/widgets/material/MaterialSpinner;

    invoke-virtual {v0, v6}, Lbr/com/itau/widgets/material/MaterialSpinner;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/itau/empresas/feature/feedback/FeedbackDialog$Categoria;

    .line 53
    .local v8, "categoria":Lcom/itau/empresas/feature/feedback/FeedbackDialog$Categoria;
    invoke-virtual {v8}, Lcom/itau/empresas/feature/feedback/FeedbackDialog$Categoria;->getCodigo()I

    move-result v7

    .line 56
    .end local v8    # "categoria":Lcom/itau/empresas/feature/feedback/FeedbackDialog$Categoria;
    :cond_1d
    iget-object v0, p0, Lcom/itau/empresas/feature/feedback/FeedbackDialogBeta;->controller:Lcom/itau/empresas/controller/FeedbackController;

    invoke-virtual {p0}, Lcom/itau/empresas/feature/feedback/FeedbackDialogBeta;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    move v2, v7

    iget-object v3, p0, Lcom/itau/empresas/feature/feedback/FeedbackDialogBeta;->campoEmail:Lbr/com/itau/widgets/material/MaterialEditText;

    .line 57
    invoke-virtual {v3}, Lbr/com/itau/widgets/material/MaterialEditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    iget-object v3, p0, Lcom/itau/empresas/feature/feedback/FeedbackDialogBeta;->campoComentario:Lbr/com/itau/widgets/material/MaterialEditText;

    invoke-virtual {v3}, Lbr/com/itau/widgets/material/MaterialEditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    .line 56
    const/4 v3, 0x0

    invoke-virtual/range {v0 .. v5}, Lcom/itau/empresas/controller/FeedbackController;->enviaFeedback(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    .line 59
    .end local v6    # "selectedIndex":I
    .end local v7    # "codigoCategoria":I
    :cond_44
    return-void
.end method

.method protected ok()V
    .registers 3

    .line 38
    const v0, 0x7f0702ac

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/feedback/FeedbackDialogBeta;->getString(I)Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f0702af

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/feedback/FeedbackDialogBeta;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/itau/empresas/feature/feedback/FeedbackDialogBeta;->analyticsHit(Ljava/lang/String;Ljava/lang/String;)V

    .line 39
    invoke-virtual {p0}, Lcom/itau/empresas/feature/feedback/FeedbackDialogBeta;->dismiss()V

    .line 40
    invoke-virtual {p0}, Lcom/itau/empresas/feature/feedback/FeedbackDialogBeta;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/itau/empresas/feature/feedback/FeedbackDialogBeta;->chamaIntentLojaBetaActivity(Landroid/content/Context;)V

    .line 41
    return-void
.end method

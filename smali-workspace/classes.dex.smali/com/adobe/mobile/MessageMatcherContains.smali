.class Lcom/adobe/mobile/MessageMatcherContains;
.super Lcom/adobe/mobile/MessageMatcher;
.source "MessageMatcherContains.java"


# direct methods
.method constructor <init>()V
    .registers 1

    .line 23
    invoke-direct {p0}, Lcom/adobe/mobile/MessageMatcher;-><init>()V

    return-void
.end method


# virtual methods
.method protected matches(Ljava/lang/Object;)Z
    .registers 9
    .param p1, "value"    # Ljava/lang/Object;

    .line 26
    instance-of v2, p1, Ljava/lang/String;

    .line 27
    .local v2, "valueIsString":Z
    instance-of v3, p1, Ljava/lang/Number;

    .line 28
    .local v3, "valueIsNumber":Z
    if-nez v2, :cond_a

    if-nez v3, :cond_a

    .line 29
    const/4 v0, 0x0

    return v0

    .line 32
    :cond_a
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    .line 33
    .local v4, "valueToSearchFor":Ljava/lang/String;
    iget-object v0, p0, Lcom/adobe/mobile/MessageMatcherContains;->values:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_14
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3c

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    .line 34
    .local v6, "v":Ljava/lang/Object;
    instance-of v0, v6, Ljava/lang/String;

    if-eqz v0, :cond_3b

    .line 35
    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/regex/Pattern;->quote(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x2

    invoke-static {v0, v1}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;I)Ljava/util/regex/Pattern;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->find()Z

    move-result v0

    if-eqz v0, :cond_3b

    .line 36
    const/4 v0, 0x1

    return v0

    .line 38
    .end local v6    # "v":Ljava/lang/Object;
    :cond_3b
    goto :goto_14

    .line 40
    :cond_3c
    const/4 v0, 0x0

    return v0
.end method

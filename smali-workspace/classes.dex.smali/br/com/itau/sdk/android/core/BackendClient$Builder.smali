.class public final Lbr/com/itau/sdk/android/core/BackendClient$Builder;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lbr/com/itau/sdk/android/core/BackendClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation


# static fields
.field private static final n:[B

.field private static o:I


# instance fields
.field private final a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<Landroid/content/Context;>;"
        }
    .end annotation
.end field

.field private final b:Lbr/com/itau/sdk/android/core/ExtraConfiguration;

.field private c:Z

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/String;

.field private j:Ljava/lang/String;

.field private k:Ljava/lang/String;

.field private l:J

.field private m:Ljava/util/concurrent/TimeUnit;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    const/16 v0, 0x93

    new-array v0, v0, [B

    fill-array-data v0, :array_e

    sput-object v0, Lbr/com/itau/sdk/android/core/BackendClient$Builder;->n:[B

    const/16 v0, 0xb5

    sput v0, Lbr/com/itau/sdk/android/core/BackendClient$Builder;->o:I

    return-void

    :array_e
    .array-data 1
        0x22t
        -0x18t
        0x7t
        0x23t
        -0x22t
        -0x11t
        0xbt
        -0xdt
        0x15t
        -0x13t
        0xbt
        -0x6t
        0x1t
        0x4et
        -0x4bt
        0x6t
        -0x14t
        0x59t
        -0x43t
        0x2t
        -0xdt
        0x0t
        -0x1t
        -0x5t
        0x54t
        -0x42t
        -0x3t
        0x45t
        -0x4et
        -0x7t
        0x9t
        0x0t
        0x4ct
        -0x4ft
        -0x3t
        0x52t
        -0x45t
        -0x8t
        -0x3t
        -0x4t
        -0x5t
        -0x1dt
        0x6t
        -0x2t
        -0x9t
        -0x5t
        0xet
        0x45t
        -0x50t
        0xft
        -0x13t
        0xct
        0x48t
        -0x43t
        0x2t
        -0xdt
        0x0t
        -0x1t
        -0x5t
        0x54t
        -0x42t
        -0x3t
        0x45t
        -0x4et
        -0x7t
        0x9t
        0x0t
        0x4ct
        -0x4ft
        -0x3t
        0x52t
        -0x45t
        -0x8t
        -0x3t
        -0x4t
        -0x5t
        -0x15t
        -0x4t
        0x8t
        -0xat
        -0x6t
        0x1t
        0x54t
        -0x43t
        0x2t
        -0xdt
        0x0t
        -0x1t
        -0x5t
        0x54t
        -0x42t
        -0x3t
        0x45t
        -0x5at
        0x15t
        -0xdt
        0x3t
        -0x15t
        -0x4t
        0x8t
        -0xat
        -0x6t
        0x1t
        0x54t
        -0x43t
        0x2t
        -0xdt
        0x0t
        -0x1t
        -0x5t
        0x54t
        -0x42t
        -0x3t
        0x45t
        -0x5at
        0x15t
        -0xdt
        0x3t
        0x4ft
        -0x41t
        -0xdt
        0xat
        0x44t
        -0x54t
        0xbt
        -0x4t
        0x8t
        0x45t
        -0x35t
        -0x19t
        0x5t
        -0xbt
        0x54t
        -0x43t
        0x2t
        -0xdt
        0x0t
        -0x1t
        -0x5t
        0x54t
        -0x42t
        -0x3t
        0x45t
        -0x4et
        -0x7t
        0x9t
        0x0t
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 5

    .line 427
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 407
    new-instance v0, Lbr/com/itau/sdk/android/core/ExtraConfiguration;

    invoke-direct {v0, p0}, Lbr/com/itau/sdk/android/core/ExtraConfiguration;-><init>(Lbr/com/itau/sdk/android/core/BackendClient$Builder;)V

    iput-object v0, p0, Lbr/com/itau/sdk/android/core/BackendClient$Builder;->b:Lbr/com/itau/sdk/android/core/ExtraConfiguration;

    .line 409
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbr/com/itau/sdk/android/core/BackendClient$Builder;->c:Z

    .line 428
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lbr/com/itau/sdk/android/core/BackendClient$Builder;->a:Ljava/lang/ref/WeakReference;

    .line 429
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v1, 0x1e

    invoke-virtual {p0, v1, v2, v0}, Lbr/com/itau/sdk/android/core/BackendClient$Builder;->withTimeout(JLjava/util/concurrent/TimeUnit;)Lbr/com/itau/sdk/android/core/BackendClient$Builder;

    .line 430
    return-void
.end method

.method private static a(III)Ljava/lang/String;
    .registers 9

    add-int/lit8 p0, p0, 0x41

    new-instance v0, Ljava/lang/String;

    rsub-int/lit8 p2, p2, 0x33

    const/4 v5, 0x0

    add-int/lit8 p1, p1, 0x4

    sget-object v4, Lbr/com/itau/sdk/android/core/BackendClient$Builder;->n:[B

    new-array v1, p2, [B

    if-nez v4, :cond_16

    move v2, p0

    move v3, p2

    :goto_11
    neg-int v3, v3

    add-int/lit8 p1, p1, 0x1

    add-int p0, v2, v3

    :cond_16
    move v2, v5

    add-int/lit8 v5, v5, 0x1

    int-to-byte v3, p0

    aput-byte v3, v1, v2

    if-ne v5, p2, :cond_23

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Ljava/lang/String;-><init>([BI)V

    return-object v0

    :cond_23
    move v2, p0

    aget-byte v3, v4, p1

    goto :goto_11
.end method

.method static synthetic a(Lbr/com/itau/sdk/android/core/BackendClient$Builder;)Ljava/lang/String;
    .registers 2

    .line 404
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/BackendClient$Builder;->h:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic b(Lbr/com/itau/sdk/android/core/BackendClient$Builder;)Ljava/lang/String;
    .registers 2

    .line 404
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/BackendClient$Builder;->g:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic c(Lbr/com/itau/sdk/android/core/BackendClient$Builder;)Ljava/lang/String;
    .registers 2

    .line 404
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/BackendClient$Builder;->f:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic d(Lbr/com/itau/sdk/android/core/BackendClient$Builder;)Ljava/lang/String;
    .registers 2

    .line 404
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/BackendClient$Builder;->e:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic e(Lbr/com/itau/sdk/android/core/BackendClient$Builder;)Ljava/lang/String;
    .registers 2

    .line 404
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/BackendClient$Builder;->d:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic f(Lbr/com/itau/sdk/android/core/BackendClient$Builder;)Ljava/lang/ref/WeakReference;
    .registers 2

    .line 404
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/BackendClient$Builder;->a:Ljava/lang/ref/WeakReference;

    return-object v0
.end method

.method static synthetic g(Lbr/com/itau/sdk/android/core/BackendClient$Builder;)Ljava/lang/String;
    .registers 2

    .line 404
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/BackendClient$Builder;->i:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic h(Lbr/com/itau/sdk/android/core/BackendClient$Builder;)Z
    .registers 2

    .line 404
    iget-boolean v0, p0, Lbr/com/itau/sdk/android/core/BackendClient$Builder;->c:Z

    return v0
.end method

.method static synthetic i(Lbr/com/itau/sdk/android/core/BackendClient$Builder;)J
    .registers 3

    .line 404
    iget-wide v0, p0, Lbr/com/itau/sdk/android/core/BackendClient$Builder;->l:J

    return-wide v0
.end method

.method static synthetic j(Lbr/com/itau/sdk/android/core/BackendClient$Builder;)Ljava/util/concurrent/TimeUnit;
    .registers 2

    .line 404
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/BackendClient$Builder;->m:Ljava/util/concurrent/TimeUnit;

    return-object v0
.end method

.method static synthetic k(Lbr/com/itau/sdk/android/core/BackendClient$Builder;)Ljava/lang/String;
    .registers 2

    .line 404
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/BackendClient$Builder;->j:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic l(Lbr/com/itau/sdk/android/core/BackendClient$Builder;)Ljava/lang/String;
    .registers 2

    .line 404
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/BackendClient$Builder;->k:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public extraConfiguration()Lbr/com/itau/sdk/android/core/ExtraConfiguration;
    .registers 2

    .line 488
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/BackendClient$Builder;->b:Lbr/com/itau/sdk/android/core/ExtraConfiguration;

    return-object v0
.end method

.method public init()Lbr/com/itau/sdk/android/core/BackendClient;
    .registers 4

    .line 497
    invoke-static {}, Lbr/com/itau/sdk/android/core/BackendClient;->a()Ljava/util/concurrent/atomic/AtomicBoolean;

    move-result-object v0

    invoke-static {}, Lbr/com/itau/sdk/android/core/BackendClient;->b()Lbr/com/itau/sdk/android/core/BackendClient;

    move-result-object v1

    invoke-static {v0, v1}, Lbr/com/itau/sdk/android/core/i;->a(Ljava/util/concurrent/atomic/AtomicBoolean;Lbr/com/itau/sdk/android/core/BackendClient;)V

    .line 498
    invoke-static {}, Lbr/com/itau/security/commons/PRNGFixes;->apply()V

    .line 499
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/BackendClient$Builder;->b:Lbr/com/itau/sdk/android/core/ExtraConfiguration;

    invoke-virtual {v0}, Lbr/com/itau/sdk/android/core/ExtraConfiguration;->isUseCryptoPayload()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lbr/com/itau/sdk/android/core/BackendClient$Builder;->c:Z

    .line 500
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/BackendClient$Builder;->b:Lbr/com/itau/sdk/android/core/ExtraConfiguration;

    invoke-virtual {v0}, Lbr/com/itau/sdk/android/core/ExtraConfiguration;->getPublicUrl()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/sdk/android/core/BackendClient$Builder;->e:Ljava/lang/String;

    .line 501
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/BackendClient$Builder;->b:Lbr/com/itau/sdk/android/core/ExtraConfiguration;

    invoke-virtual {v0}, Lbr/com/itau/sdk/android/core/ExtraConfiguration;->getRouterUrl()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/sdk/android/core/BackendClient$Builder;->d:Ljava/lang/String;

    .line 502
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/BackendClient$Builder;->b:Lbr/com/itau/sdk/android/core/ExtraConfiguration;

    invoke-virtual {v0}, Lbr/com/itau/sdk/android/core/ExtraConfiguration;->getMiddlewareUrl()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/sdk/android/core/BackendClient$Builder;->f:Ljava/lang/String;

    .line 503
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/BackendClient$Builder;->b:Lbr/com/itau/sdk/android/core/ExtraConfiguration;

    invoke-virtual {v0}, Lbr/com/itau/sdk/android/core/ExtraConfiguration;->getUniversalUrl()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/sdk/android/core/BackendClient$Builder;->g:Ljava/lang/String;

    .line 504
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/BackendClient$Builder;->b:Lbr/com/itau/sdk/android/core/ExtraConfiguration;

    invoke-virtual {v0}, Lbr/com/itau/sdk/android/core/ExtraConfiguration;->getUsabilidadeUrl()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/sdk/android/core/BackendClient$Builder;->h:Ljava/lang/String;

    .line 505
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/BackendClient$Builder;->b:Lbr/com/itau/sdk/android/core/ExtraConfiguration;

    invoke-virtual {v0}, Lbr/com/itau/sdk/android/core/ExtraConfiguration;->getUserAgent()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/sdk/android/core/BackendClient$Builder;->i:Ljava/lang/String;

    .line 506
    new-instance v0, Lbr/com/itau/sdk/android/core/BackendClient;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lbr/com/itau/sdk/android/core/BackendClient;-><init>(Lbr/com/itau/sdk/android/core/BackendClient$Builder;Lbr/com/itau/sdk/android/core/b;)V

    invoke-static {v0}, Lbr/com/itau/sdk/android/core/BackendClient;->a(Lbr/com/itau/sdk/android/core/BackendClient;)Lbr/com/itau/sdk/android/core/BackendClient;

    .line 507
    invoke-static {}, Lbr/com/itau/sdk/android/core/BackendClient;->b()Lbr/com/itau/sdk/android/core/BackendClient;

    move-result-object v0

    invoke-static {v0}, Lbr/com/itau/sdk/android/core/i;->a(Lbr/com/itau/sdk/android/core/BackendClient;)V

    .line 508
    invoke-static {}, Lbr/com/itau/sdk/android/core/BackendClient;->a()Ljava/util/concurrent/atomic/AtomicBoolean;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    .line 509
    invoke-static {}, Lbr/com/itau/sdk/android/core/BackendClient;->b()Lbr/com/itau/sdk/android/core/BackendClient;

    move-result-object v0

    invoke-static {v0}, Lbr/com/itau/sdk/android/core/BackendClient;->b(Lbr/com/itau/sdk/android/core/BackendClient;)Lbr/com/itau/sdk/android/core/BackendClient$a;

    move-result-object v0

    invoke-static {v0}, Lbr/com/itau/sdk/android/core/BackendClient$a;->h(Lbr/com/itau/sdk/android/core/BackendClient$a;)Lde/greenrobot/event/EventBus;

    move-result-object v0

    invoke-static {}, Lbr/com/itau/sdk/android/core/BackendClient;->b()Lbr/com/itau/sdk/android/core/BackendClient;

    move-result-object v1

    const/high16 v2, -0x80000000

    invoke-virtual {v0, v1, v2}, Lde/greenrobot/event/EventBus;->register(Ljava/lang/Object;I)V

    .line 510
    invoke-static {}, Lbr/com/itau/sdk/android/core/BackendClient;->b()Lbr/com/itau/sdk/android/core/BackendClient;

    move-result-object v0

    invoke-static {v0}, Lbr/com/itau/sdk/android/core/BackendClient;->b(Lbr/com/itau/sdk/android/core/BackendClient;)Lbr/com/itau/sdk/android/core/BackendClient$a;

    move-result-object v0

    invoke-static {v0}, Lbr/com/itau/sdk/android/core/BackendClient$a;->j(Lbr/com/itau/sdk/android/core/BackendClient$a;)Lde/greenrobot/event/EventBus;

    move-result-object v0

    invoke-static {}, Lbr/com/itau/sdk/android/core/BackendClient;->b()Lbr/com/itau/sdk/android/core/BackendClient;

    move-result-object v1

    const/high16 v2, -0x80000000

    invoke-virtual {v0, v1, v2}, Lde/greenrobot/event/EventBus;->register(Ljava/lang/Object;I)V

    .line 511
    invoke-static {}, Lbr/com/itau/sdk/android/core/BackendClient;->b()Lbr/com/itau/sdk/android/core/BackendClient;

    move-result-object v0

    return-object v0
.end method

.method public withActivationKey(Ljava/lang/String;)Lbr/com/itau/sdk/android/core/BackendClient$Builder;
    .registers 6

    .line 434
    const-string v0, ""

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1e

    .line 435
    new-instance v0, Ljava/lang/IllegalArgumentException;

    sget-object v1, Lbr/com/itau/sdk/android/core/BackendClient$Builder;->n:[B

    const/16 v2, 0x15

    aget-byte v1, v1, v2

    sget-object v2, Lbr/com/itau/sdk/android/core/BackendClient$Builder;->n:[B

    const/4 v3, 0x7

    aget-byte v2, v2, v3

    neg-int v2, v2

    invoke-static {v1, v1, v2}, Lbr/com/itau/sdk/android/core/BackendClient$Builder;->a(III)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 437
    :cond_1e
    iput-object p1, p0, Lbr/com/itau/sdk/android/core/BackendClient$Builder;->j:Ljava/lang/String;

    .line 438
    return-object p0
.end method

.method public withLicensePathFromAssets(Ljava/lang/String;)Lbr/com/itau/sdk/android/core/BackendClient$Builder;
    .registers 7

    .line 449
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1f

    .line 450
    new-instance v0, Ljava/lang/IllegalArgumentException;

    sget-object v1, Lbr/com/itau/sdk/android/core/BackendClient$Builder;->n:[B

    const/4 v2, 0x6

    aget-byte v1, v1, v2

    sget v2, Lbr/com/itau/sdk/android/core/BackendClient$Builder;->o:I

    and-int/lit8 v2, v2, 0x6f

    sget-object v3, Lbr/com/itau/sdk/android/core/BackendClient$Builder;->n:[B

    const/16 v4, 0x31

    aget-byte v3, v3, v4

    invoke-static {v1, v2, v3}, Lbr/com/itau/sdk/android/core/BackendClient$Builder;->a(III)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 452
    :cond_1f
    iput-object p1, p0, Lbr/com/itau/sdk/android/core/BackendClient$Builder;->k:Ljava/lang/String;

    .line 453
    return-object p0
.end method

.method public withTimeout(J)Lbr/com/itau/sdk/android/core/BackendClient$Builder;
    .registers 8

    .line 464
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-nez v0, :cond_24

    .line 465
    new-instance v0, Ljava/lang/IllegalArgumentException;

    sget-object v1, Lbr/com/itau/sdk/android/core/BackendClient$Builder;->n:[B

    const/16 v2, 0x9

    aget-byte v1, v1, v2

    neg-int v1, v1

    sget-object v2, Lbr/com/itau/sdk/android/core/BackendClient$Builder;->n:[B

    const/16 v3, 0x34

    aget-byte v2, v2, v3

    sget-object v3, Lbr/com/itau/sdk/android/core/BackendClient$Builder;->n:[B

    const/16 v4, 0x29

    aget-byte v3, v3, v4

    neg-int v3, v3

    invoke-static {v1, v2, v3}, Lbr/com/itau/sdk/android/core/BackendClient$Builder;->a(III)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 467
    :cond_24
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {p0, p1, p2, v0}, Lbr/com/itau/sdk/android/core/BackendClient$Builder;->withTimeout(JLjava/util/concurrent/TimeUnit;)Lbr/com/itau/sdk/android/core/BackendClient$Builder;

    move-result-object v0

    return-object v0
.end method

.method public withTimeout(JLjava/util/concurrent/TimeUnit;)Lbr/com/itau/sdk/android/core/BackendClient$Builder;
    .registers 8

    .line 479
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-eqz v0, :cond_8

    if-nez p3, :cond_21

    .line 480
    :cond_8
    new-instance v0, Ljava/lang/IllegalArgumentException;

    sget-object v1, Lbr/com/itau/sdk/android/core/BackendClient$Builder;->n:[B

    const/16 v2, 0x9

    aget-byte v1, v1, v2

    neg-int v1, v1

    sget-object v2, Lbr/com/itau/sdk/android/core/BackendClient$Builder;->n:[B

    const/16 v3, 0x15

    aget-byte v2, v2, v3

    const/16 v3, 0x5d

    invoke-static {v1, v3, v2}, Lbr/com/itau/sdk/android/core/BackendClient$Builder;->a(III)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 482
    :cond_21
    iput-wide p1, p0, Lbr/com/itau/sdk/android/core/BackendClient$Builder;->l:J

    .line 483
    iput-object p3, p0, Lbr/com/itau/sdk/android/core/BackendClient$Builder;->m:Ljava/util/concurrent/TimeUnit;

    .line 484
    return-object p0
.end method

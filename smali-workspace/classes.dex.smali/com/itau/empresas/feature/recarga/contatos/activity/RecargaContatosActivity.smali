.class public Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaContatosActivity;
.super Lcom/itau/empresas/ui/activity/BaseActivity;
.source "RecargaContatosActivity.java"

# interfaces
.implements Lcom/itau/empresas/ui/util/SnackbarUtils$Gerenciador;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaContatosActivity$SectionsPagerAdapter;
    }
.end annotation


# instance fields
.field adapter:Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaContatosActivity$SectionsPagerAdapter;

.field private menu:Landroid/view/Menu;

.field private recargaCadastroContatoFragment:Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;

.field private recargaListaContatoFragment:Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment;

.field root:Landroid/widget/LinearLayout;

.field tabContato:Landroid/support/design/widget/TabLayout;

.field toolBar:Landroid/support/v7/widget/Toolbar;

.field private topSnackbar:Lcom/itau/empresas/ui/util/SnackbarUtils$Gerenciador;

.field viewpagerContato:Lcom/itau/empresas/ui/util/CustomViewPager;


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 49
    invoke-direct {p0}, Lcom/itau/empresas/ui/activity/BaseActivity;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaContatosActivity;)Landroid/view/Menu;
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaContatosActivity;

    .line 49
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaContatosActivity;->menu:Landroid/view/Menu;

    return-object v0
.end method

.method static synthetic access$100(Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaContatosActivity;)Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment;
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaContatosActivity;

    .line 49
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaContatosActivity;->recargaListaContatoFragment:Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment;

    return-object v0
.end method

.method static synthetic access$102(Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaContatosActivity;Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment;)Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment;
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaContatosActivity;
    .param p1, "x1"    # Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment;

    .line 49
    iput-object p1, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaContatosActivity;->recargaListaContatoFragment:Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaListaContatoFragment;

    return-object p1
.end method

.method static synthetic access$200(Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaContatosActivity;)Lcom/itau/empresas/CustomApplication;
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaContatosActivity;

    .line 49
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaContatosActivity;->application:Lcom/itau/empresas/CustomApplication;

    return-object v0
.end method

.method static synthetic access$300(Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaContatosActivity;)Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaContatosActivity;

    .line 49
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaContatosActivity;->recargaCadastroContatoFragment:Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;

    return-object v0
.end method

.method static synthetic access$302(Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaContatosActivity;Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;)Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaContatosActivity;
    .param p1, "x1"    # Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;

    .line 49
    iput-object p1, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaContatosActivity;->recargaCadastroContatoFragment:Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;

    return-object p1
.end method

.method private constroiToolbar()V
    .registers 3

    .line 131
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaContatosActivity;->toolBar:Landroid/support/v7/widget/Toolbar;

    invoke-static {v0, p0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom;->Builder(Landroid/support/v7/widget/Toolbar;Landroid/support/v7/app/AppCompatActivity;)Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    move-result-object v0

    .line 132
    const v1, 0x7f0706b8

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->comTitulo(I)Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    move-result-object v0

    .line 133
    invoke-virtual {v0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->comClickListenerFinish()Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    move-result-object v0

    .line 134
    invoke-virtual {v0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->build()Lcom/itau/empresas/ui/util/listener/ToolbarCustom;

    .line 135
    return-void
.end method

.method private disparaEventoAnalytics()V
    .registers 5

    .line 109
    invoke-static {}, Lcom/itau/empresas/feature/analytics/AnalyticsHelper;->getInstance()Lcom/itau/empresas/feature/analytics/AnalyticsHelper;

    move-result-object v3

    .line 110
    .line 111
    .local v3, "analyticsHelper":Lcom/itau/empresas/feature/analytics/AnalyticsHelper;
    const v0, 0x7f0700ee

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaContatosActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 112
    const v1, 0x7f0700c9

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaContatosActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 113
    const v2, 0x7f0700c3

    invoke-virtual {p0, v2}, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaContatosActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 110
    invoke-virtual {v3, v0, v1, v2}, Lcom/itau/empresas/feature/analytics/AnalyticsHelper;->eventoAdobeState(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 115
    return-void
.end method

.method public static intent(Landroid/content/Context;)Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaContatosActivity_$IntentBuilder_;
    .registers 2
    .param p0, "context"    # Landroid/content/Context;

    .line 255
    new-instance v0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaContatosActivity_$IntentBuilder_;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaContatosActivity_$IntentBuilder_;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method private pesquisarContatos()Landroid/support/v7/widget/SearchView$OnQueryTextListener;
    .registers 2

    .line 230
    new-instance v0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaContatosActivity$3;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaContatosActivity$3;-><init>(Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaContatosActivity;)V

    return-object v0
.end method


# virtual methods
.method carregaElementosDaTela()V
    .registers 3

    .line 71
    new-instance v0, Lcom/itau/empresas/ui/util/SnackbarUtils$GerenciadorDelegate;

    iget-object v1, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaContatosActivity;->root:Landroid/widget/LinearLayout;

    invoke-direct {v0, v1}, Lcom/itau/empresas/ui/util/SnackbarUtils$GerenciadorDelegate;-><init>(Landroid/view/ViewGroup;)V

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaContatosActivity;->topSnackbar:Lcom/itau/empresas/ui/util/SnackbarUtils$Gerenciador;

    .line 72
    invoke-direct {p0}, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaContatosActivity;->constroiToolbar()V

    .line 74
    new-instance v0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaContatosActivity$SectionsPagerAdapter;

    invoke-virtual {p0}, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaContatosActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaContatosActivity$SectionsPagerAdapter;-><init>(Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaContatosActivity;Landroid/support/v4/app/FragmentManager;)V

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaContatosActivity;->adapter:Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaContatosActivity$SectionsPagerAdapter;

    .line 75
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaContatosActivity;->viewpagerContato:Lcom/itau/empresas/ui/util/CustomViewPager;

    iget-object v1, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaContatosActivity;->adapter:Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaContatosActivity$SectionsPagerAdapter;

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/CustomViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 76
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaContatosActivity;->viewpagerContato:Lcom/itau/empresas/ui/util/CustomViewPager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/CustomViewPager;->setPagingEnabled(Z)V

    .line 77
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaContatosActivity;->tabContato:Landroid/support/design/widget/TabLayout;

    iget-object v1, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaContatosActivity;->viewpagerContato:Lcom/itau/empresas/ui/util/CustomViewPager;

    invoke-virtual {v0, v1}, Landroid/support/design/widget/TabLayout;->setupWithViewPager(Landroid/support/v4/view/ViewPager;)V

    .line 79
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaContatosActivity;->viewpagerContato:Lcom/itau/empresas/ui/util/CustomViewPager;

    new-instance v1, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaContatosActivity$1;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaContatosActivity$1;-><init>(Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaContatosActivity;)V

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/CustomViewPager;->addOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    .line 93
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaContatosActivity;->tabContato:Landroid/support/design/widget/TabLayout;

    new-instance v1, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaContatosActivity$2;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaContatosActivity$2;-><init>(Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaContatosActivity;)V

    invoke-virtual {v0, v1}, Landroid/support/design/widget/TabLayout;->setOnTabSelectedListener(Landroid/support/design/widget/TabLayout$OnTabSelectedListener;)V

    .line 105
    invoke-direct {p0}, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaContatosActivity;->disparaEventoAnalytics()V

    .line 106
    return-void
.end method

.method public dismissSnackbar()V
    .registers 2

    .line 142
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaContatosActivity;->topSnackbar:Lcom/itau/empresas/ui/util/SnackbarUtils$Gerenciador;

    invoke-interface {v0}, Lcom/itau/empresas/ui/util/SnackbarUtils$Gerenciador;->dismissSnackbar()V

    .line 143
    return-void
.end method

.method public escondePesquisa()V
    .registers 3

    .line 138
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaContatosActivity;->menu:Landroid/view/Menu;

    const v1, 0x7f0e06c6

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 139
    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .registers 8
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .line 182
    if-nez p1, :cond_24

    const/4 v0, -0x1

    if-ne p2, v0, :cond_24

    if-eqz p3, :cond_24

    const-string v0, "contato"

    .line 183
    invoke-virtual {p3, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_24

    .line 184
    const-string v0, "contato"

    .line 185
    invoke-virtual {p3, v0}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;

    .line 186
    .local v3, "contatoRecargaVO":Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaContatosActivity;->viewpagerContato:Lcom/itau/empresas/ui/util/CustomViewPager;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/itau/empresas/ui/util/CustomViewPager;->setCurrentItem(IZ)V

    .line 187
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaContatosActivity;->recargaCadastroContatoFragment:Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;

    invoke-virtual {v0, v3}, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->setContatoParaRecarga(Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;)V

    .line 189
    .end local v3    # "contatoRecargaVO":Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;
    :cond_24
    invoke-super {p0, p1, p2, p3}, Lcom/itau/empresas/ui/activity/BaseActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 190
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .registers 7
    .param p1, "menu"    # Landroid/view/Menu;

    .line 215
    invoke-virtual {p0}, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaContatosActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f0f0005

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 216
    iput-object p1, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaContatosActivity;->menu:Landroid/view/Menu;

    .line 218
    const v0, 0x7f0e06c5

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    .line 219
    .local v2, "itemMenuFiltro":Landroid/view/MenuItem;
    const/4 v0, 0x0

    invoke-interface {v2, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 221
    const v0, 0x7f0e06c6

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v3

    .line 222
    .local v3, "itemSVSaldoExtrato":Landroid/view/MenuItem;
    invoke-static {v3}, Landroid/support/v4/view/MenuItemCompat;->getActionView(Landroid/view/MenuItem;)Landroid/view/View;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Landroid/support/v7/widget/SearchView;

    .line 223
    .local v4, "searchView":Landroid/support/v7/widget/SearchView;
    const v0, 0x7f070635

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaContatosActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Landroid/support/v7/widget/SearchView;->setQueryHint(Ljava/lang/CharSequence;)V

    .line 224
    invoke-direct {p0}, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaContatosActivity;->pesquisarContatos()Landroid/support/v7/widget/SearchView$OnQueryTextListener;

    move-result-object v0

    invoke-virtual {v4, v0}, Landroid/support/v7/widget/SearchView;->setOnQueryTextListener(Landroid/support/v7/widget/SearchView$OnQueryTextListener;)V

    .line 226
    const/4 v0, 0x1

    return v0
.end method

.method public onEventMainThread(Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent$Conversion;)V
    .registers 4
    .param p1, "e"    # Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent$Conversion;

    .line 193
    invoke-static {p1, p0}, Lcom/itau/empresas/api/ApiHelper;->devoTratarEvento(Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;Lcom/itau/empresas/api/ApiConsumidor;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 194
    return-void

    .line 197
    :cond_7
    invoke-virtual {p0}, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaContatosActivity;->escondeDialogoDeProgresso()V

    .line 198
    const v0, 0x7f0701f2

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaContatosActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaContatosActivity;->showSnackbar(Ljava/lang/CharSequence;I)V

    .line 200
    return-void
.end method

.method public onEventMainThread(Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;)V
    .registers 5
    .param p1, "e"    # Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;

    .line 204
    invoke-static {p1, p0}, Lcom/itau/empresas/api/ApiHelper;->devoTratarEvento(Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;Lcom/itau/empresas/api/ApiConsumidor;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 205
    return-void

    .line 207
    :cond_7
    invoke-virtual {p0}, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaContatosActivity;->escondeDialogoDeProgresso()V

    .line 208
    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;->getException()Lbr/com/itau/sdk/android/core/exception/BackendException;

    move-result-object v0

    invoke-virtual {v0}, Lbr/com/itau/sdk/android/core/exception/BackendException;->getError()Lbr/com/itau/sdk/android/core/model/ErrorDTO;

    move-result-object v2

    .line 209
    .local v2, "error":Lbr/com/itau/sdk/android/core/model/ErrorDTO;
    if-eqz v2, :cond_1c

    .line 210
    invoke-virtual {v2}, Lbr/com/itau/sdk/android/core/model/ErrorDTO;->getMensagem()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaContatosActivity;->showSnackbar(Ljava/lang/CharSequence;I)V

    .line 211
    :cond_1c
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .registers 4
    .param p1, "item"    # Landroid/view/MenuItem;

    .line 247
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x7f0e06cf

    if-ne v0, v1, :cond_10

    .line 248
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaContatosActivity;->application:Lcom/itau/empresas/CustomApplication;

    invoke-static {v0, p0}, Lcom/itau/empresas/ui/util/ViewUtils;->aoClicarBotaoSairMenu(Lcom/itau/empresas/CustomApplication;Lcom/itau/empresas/ui/activity/BaseActivity;)V

    .line 249
    const/4 v0, 0x1

    return v0

    .line 251
    :cond_10
    invoke-super {p0, p1}, Lcom/itau/empresas/ui/activity/BaseActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0
.end method

.method public opkeysConsumidas()Ljava/util/List;
    .registers 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Ljava/lang/String;>;"
        }
    .end annotation

    .line 119
    const/16 v0, 0x9

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "listaOperadoras"

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const-string v1, "contatosRecarga"

    const/4 v2, 0x1

    aput-object v1, v0, v2

    const-string v1, "cadastrarContatoRecarga"

    const/4 v2, 0x2

    aput-object v1, v0, v2

    const-string v1, "alterarContatoRecarga"

    const/4 v2, 0x3

    aput-object v1, v0, v2

    const-string v1, "deletarContatoRecarga"

    const/4 v2, 0x4

    aput-object v1, v0, v2

    const-string v1, "valoresOperadora"

    const/4 v2, 0x5

    aput-object v1, v0, v2

    const-string v1, "simularRecarga"

    const/4 v2, 0x6

    aput-object v1, v0, v2

    const-string v1, "exibicao_recarga_autorizar"

    const/4 v2, 0x7

    aput-object v1, v0, v2

    const-string v1, "efetivarRecarga"

    const/16 v2, 0x8

    aput-object v1, v0, v2

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public showSnackbar(Ljava/lang/CharSequence;I)V
    .registers 4
    .param p1, "texto"    # Ljava/lang/CharSequence;
    .param p2, "duracao"    # I

    .line 146
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaContatosActivity;->topSnackbar:Lcom/itau/empresas/ui/util/SnackbarUtils$Gerenciador;

    invoke-interface {v0, p1, p2}, Lcom/itau/empresas/ui/util/SnackbarUtils$Gerenciador;->showSnackbar(Ljava/lang/CharSequence;I)V

    .line 147
    return-void
.end method

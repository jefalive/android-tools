.class final Lcom/adobe/mobile/Lifecycle;
.super Ljava/lang/Object;
.source "Lifecycle.java"


# static fields
.field private static final _contextDataMutex:Ljava/lang/Object;

.field private static final _lifecycleContextData:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
        }
    .end annotation
.end field

.field private static final _lifecycleContextDataLowercase:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
        }
    .end annotation
.end field

.field private static final _lowercaseContextDataMutex:Ljava/lang/Object;

.field private static final _previousSessionlifecycleContextData:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
        }
    .end annotation
.end field

.field protected static volatile lifecycleHasRun:Z

.field protected static sessionStartTime:J


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .line 45
    const-wide/16 v0, 0x0

    sput-wide v0, Lcom/adobe/mobile/Lifecycle;->sessionStartTime:J

    .line 57
    const/4 v0, 0x0

    sput-boolean v0, Lcom/adobe/mobile/Lifecycle;->lifecycleHasRun:Z

    .line 58
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/adobe/mobile/Lifecycle;->_lifecycleContextData:Ljava/util/HashMap;

    .line 59
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/adobe/mobile/Lifecycle;->_lifecycleContextDataLowercase:Ljava/util/HashMap;

    .line 60
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/adobe/mobile/Lifecycle;->_previousSessionlifecycleContextData:Ljava/util/HashMap;

    .line 228
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/adobe/mobile/Lifecycle;->_contextDataMutex:Ljava/lang/Object;

    .line 268
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/adobe/mobile/Lifecycle;->_lowercaseContextDataMutex:Ljava/lang/Object;

    return-void
.end method

.method constructor <init>()V
    .registers 1

    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static addInstallData(Ljava/util/Map;J)V
    .registers 9
    .param p0, "cdata"    # Ljava/util/Map;
    .param p1, "date"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;J)V"
        }
    .end annotation

    .line 471
    new-instance v4, Ljava/text/SimpleDateFormat;

    const-string v0, "M/d/yyyy"

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v4, v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 472
    .local v4, "dayMonthYearFormat":Ljava/text/DateFormat;
    const-string v0, "a.InstallDate"

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/text/DateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p0, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 473
    const-string v0, "a.InstallEvent"

    const-string v1, "InstallEvent"

    invoke-interface {p0, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 474
    const-string v0, "a.DailyEngUserEvent"

    const-string v1, "DailyEngUserEvent"

    invoke-interface {p0, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 475
    const-string v0, "a.MonthlyEngUserEvent"

    const-string v1, "MonthlyEngUserEvent"

    invoke-interface {p0, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 480
    :try_start_2b
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "ADMS_Referrer_ContextData_Json_String"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_43

    .line 481
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "utm_campaign"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5c

    .line 483
    :cond_43
    invoke-static {}, Lcom/adobe/mobile/Lifecycle;->getReferrerDataFromSharedPreferences()Ljava/util/Map;

    move-result-object v5

    .line 484
    .local v5, "referrerData":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    if-eqz v5, :cond_5b

    invoke-interface {v5}, Ljava/util/Map;->size()I

    move-result v0

    if-ltz v0, :cond_5b

    .line 485
    invoke-interface {p0, v5}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 486
    invoke-static {}, Lcom/adobe/mobile/MobileConfig;->getInstance()Lcom/adobe/mobile/MobileConfig;

    move-result-object v0

    sget-object v1, Lcom/adobe/mobile/Config$MobileDataEvent;->MOBILE_EVENT_ACQUISITION_INSTALL:Lcom/adobe/mobile/Config$MobileDataEvent;

    invoke-virtual {v0, v1, v5}, Lcom/adobe/mobile/MobileConfig;->invokeAdobeDataCallback(Lcom/adobe/mobile/Config$MobileDataEvent;Ljava/util/Map;)V

    .line 489
    .end local v5    # "referrerData":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    .end local v5
    :cond_5b
    goto :goto_77

    :cond_5c
    invoke-static {}, Lcom/adobe/mobile/MobileConfig;->getInstance()Lcom/adobe/mobile/MobileConfig;

    move-result-object v0

    invoke-virtual {v0}, Lcom/adobe/mobile/MobileConfig;->mobileReferrerConfigured()Z

    move-result v0

    if-eqz v0, :cond_77

    invoke-static {}, Lcom/adobe/mobile/MobileConfig;->getInstance()Lcom/adobe/mobile/MobileConfig;

    move-result-object v0

    invoke-virtual {v0}, Lcom/adobe/mobile/MobileConfig;->getReferrerTimeout()I

    move-result v0

    if-lez v0, :cond_77

    .line 491
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/adobe/mobile/ReferrerHandler;->setReferrerProcessed(Z)V

    .line 492
    invoke-static {}, Lcom/adobe/mobile/Messages;->block3rdPartyCallbacksQueueForReferrer()V

    .line 495
    :cond_77
    :goto_77
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getSharedPreferencesEditor()Landroid/content/SharedPreferences$Editor;

    move-result-object v5

    .line 497
    .local v5, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v0, "ADMS_InstallDate"

    invoke-interface {v5, v0, p1, p2}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 498
    invoke-interface {v5}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_83
    .catch Lcom/adobe/mobile/StaticMethods$NullContextException; {:try_start_2b .. :try_end_83} :catch_84

    .line 501
    .end local v5    # "editor":Landroid/content/SharedPreferences$Editor;
    goto :goto_94

    .line 499
    :catch_84
    move-exception v5

    .line 500
    .local v5, "e":Lcom/adobe/mobile/StaticMethods$NullContextException;
    const-string v0, "Lifecycle - Error setting install data (%s)."

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v5}, Lcom/adobe/mobile/StaticMethods$NullContextException;->getMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logErrorFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 502
    .end local v5    # "e":Lcom/adobe/mobile/StaticMethods$NullContextException;
    :goto_94
    return-void
.end method

.method private static addLifecycleGenericData(Ljava/util/Map;J)V
    .registers 13
    .param p0, "cdata"    # Ljava/util/Map;
    .param p1, "date"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;J)V"
        }
    .end annotation

    .line 531
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getDefaultData()Ljava/util/HashMap;

    move-result-object v0

    invoke-interface {p0, v0}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 532
    const-string v0, "a.LaunchEvent"

    const-string v1, "LaunchEvent"

    invoke-interface {p0, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 533
    const-string v0, "a.OSVersion"

    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getOperatingSystem()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p0, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 535
    new-instance v4, Ljava/text/SimpleDateFormat;

    const-string v0, "H"

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v4, v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 536
    .local v4, "hourOfDayDateFormat":Ljava/text/DateFormat;
    const-string v0, "a.HourOfDay"

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/text/DateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p0, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 538
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v5

    .line 539
    .local v5, "cal":Ljava/util/Calendar;
    invoke-virtual {v5, p1, p2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 540
    const/4 v0, 0x7

    invoke-virtual {v5, v0}, Ljava/util/Calendar;->get(I)I

    move-result v6

    .line 541
    .local v6, "dow":I
    const-string v0, "a.DayOfWeek"

    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p0, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 543
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getAdvertisingIdentifier()Ljava/lang/String;

    move-result-object v7

    .line 544
    .local v7, "adid":Ljava/lang/String;
    if-eqz v7, :cond_4d

    .line 545
    const-string v0, "a.adid"

    invoke-interface {p0, v0, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 549
    :cond_4d
    :try_start_4d
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getSharedPreferencesEditor()Landroid/content/SharedPreferences$Editor;

    move-result-object v8

    .line 550
    .local v8, "editor":Landroid/content/SharedPreferences$Editor;
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "ADMS_Launches"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    add-int/lit8 v9, v0, 0x1

    .line 551
    .local v9, "launches":I
    const-string v0, "a.Launches"

    invoke-static {v9}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p0, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 553
    const-string v0, "ADMS_Launches"

    invoke-interface {v8, v0, v9}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 554
    const-string v0, "ADMS_LastDateUsed"

    invoke-interface {v8, v0, p1, p2}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 555
    invoke-interface {v8}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_74
    .catch Lcom/adobe/mobile/StaticMethods$NullContextException; {:try_start_4d .. :try_end_74} :catch_75

    .line 558
    .end local v8    # "editor":Landroid/content/SharedPreferences$Editor;
    .end local v9    # "launches":I
    goto :goto_85

    .line 556
    :catch_75
    move-exception v8

    .line 557
    .local v8, "e":Lcom/adobe/mobile/StaticMethods$NullContextException;
    const-string v0, "Lifecycle - Error adding generic data (%s)."

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v8}, Lcom/adobe/mobile/StaticMethods$NullContextException;->getMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logErrorFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 559
    .end local v8    # "e":Lcom/adobe/mobile/StaticMethods$NullContextException;
    :goto_85
    return-void
.end method

.method private static addNonInstallData(Ljava/util/Map;J)V
    .registers 22
    .param p0, "cdata"    # Ljava/util/Map;
    .param p1, "date"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;J)V"
        }
    .end annotation

    .line 565
    :try_start_0
    new-instance v4, Ljava/text/SimpleDateFormat;

    const-string v0, "yyyy/M/d"

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v4, v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 567
    .local v4, "dailyFormatter":Ljava/text/DateFormat;
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "ADMS_LastDateUsed"

    const-wide/16 v2, 0x0

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v5

    .line 568
    .local v5, "lastUseMillis":J
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0, v5, v6}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v4, v0}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v7

    .line 569
    .local v7, "lastUsedDateDaily":Ljava/lang/String;
    invoke-static/range {p1 .. p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/text/DateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    .line 571
    .local v8, "currentDateDaily":Ljava/lang/String;
    invoke-virtual {v8, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_35

    .line 572
    const-string v0, "a.DailyEngUserEvent"

    const-string v1, "DailyEngUserEvent"

    move-object/from16 v2, p0

    invoke-interface {v2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 576
    :cond_35
    new-instance v9, Ljava/text/SimpleDateFormat;

    const-string v0, "yyyy/M"

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v9, v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 577
    .local v9, "monthlyFormatter":Ljava/text/DateFormat;
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0, v5, v6}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v9, v0}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v10

    .line 578
    .local v10, "lastUsedDateMonthly":Ljava/lang/String;
    invoke-static/range {p1 .. p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v9, v0}, Ljava/text/DateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    .line 579
    .local v11, "currentDateMonthly":Ljava/lang/String;
    invoke-virtual {v11, v10}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5e

    .line 580
    const-string v0, "a.MonthlyEngUserEvent"

    const-string v1, "MonthlyEngUserEvent"

    move-object/from16 v2, p0

    invoke-interface {v2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 584
    :cond_5e
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "ADMS_InstallDate"

    const-wide/16 v2, 0x0

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v12

    .line 585
    .local v12, "installMillis":J
    const-string v0, "a.DaysSinceFirstUse"

    move-wide/from16 v1, p1

    invoke-static {v12, v13, v1, v2}, Lcom/adobe/mobile/Lifecycle;->calculateDaysSince(JJ)Ljava/lang/String;

    move-result-object v1

    move-object/from16 v2, p0

    invoke-interface {v2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 588
    const-string v0, "a.DaysSinceLastUse"

    move-wide/from16 v1, p1

    invoke-static {v5, v6, v1, v2}, Lcom/adobe/mobile/Lifecycle;->calculateDaysSince(JJ)Ljava/lang/String;

    move-result-object v1

    move-object/from16 v2, p0

    invoke-interface {v2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 591
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "ADMS_SuccessfulClose"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_141

    .line 592
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getSharedPreferencesEditor()Landroid/content/SharedPreferences$Editor;

    move-result-object v14

    .line 594
    .local v14, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v0, "ADMS_PauseDate"

    invoke-interface {v14, v0}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 595
    const-string v0, "ADMS_SessionStart"

    invoke-interface {v14, v0}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 596
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getTimeSince1970()J

    move-result-wide v0

    sput-wide v0, Lcom/adobe/mobile/Lifecycle;->sessionStartTime:J

    .line 597
    invoke-interface {v14}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 600
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "ADBLastKnownTimestampKey"

    const-wide/16 v2, 0x0

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v15

    .line 602
    .local v15, "lastKnownTimestamp":J
    const-wide/16 v0, 0x0

    cmp-long v0, v15, v0

    if-lez v0, :cond_131

    invoke-static {}, Lcom/adobe/mobile/MobileConfig;->getInstance()Lcom/adobe/mobile/MobileConfig;

    move-result-object v0

    invoke-virtual {v0}, Lcom/adobe/mobile/MobileConfig;->mobileUsingAnalytics()Z

    move-result v0

    if-eqz v0, :cond_131

    invoke-static {}, Lcom/adobe/mobile/MobileConfig;->getInstance()Lcom/adobe/mobile/MobileConfig;

    move-result-object v0

    invoke-virtual {v0}, Lcom/adobe/mobile/MobileConfig;->getOfflineTrackingEnabled()Z

    move-result v0

    if-eqz v0, :cond_131

    invoke-static {}, Lcom/adobe/mobile/MobileConfig;->getInstance()Lcom/adobe/mobile/MobileConfig;

    move-result-object v0

    invoke-virtual {v0}, Lcom/adobe/mobile/MobileConfig;->getBackdateSessionInfoEnabled()Z
    :try_end_d5
    .catch Lcom/adobe/mobile/StaticMethods$NullContextException; {:try_start_0 .. :try_end_d5} :catch_142

    move-result v0

    if-eqz v0, :cond_131

    .line 604
    :try_start_d8
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v17

    .line 605
    .local v17, "defaults":Landroid/content/SharedPreferences;
    new-instance v18, Ljava/util/HashMap;

    invoke-direct/range {v18 .. v18}, Ljava/util/HashMap;-><init>()V

    .line 606
    .local v18, "crashCData":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    const-string v0, "a.CrashEvent"

    const-string v1, "CrashEvent"

    move-object/from16 v2, v18

    invoke-virtual {v2, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 607
    const-string v0, "a.OSVersion"

    const-string v1, "ADOBEMOBILE_STOREDDEFAULTS_OS"

    const-string v2, ""

    move-object/from16 v3, v17

    invoke-interface {v3, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    move-object/from16 v2, v18

    invoke-virtual {v2, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 608
    const-string v0, "a.AppID"

    const-string v1, "ADOBEMOBILE_STOREDDEFAULTS_APPID"

    const-string v2, ""

    move-object/from16 v3, v17

    invoke-interface {v3, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    move-object/from16 v2, v18

    invoke-virtual {v2, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 611
    const-string v0, "Crash"

    const-wide/16 v1, 0x1

    add-long/2addr v1, v15

    move-object/from16 v3, v18

    invoke-static {v0, v3, v1, v2}, Lcom/adobe/mobile/AnalyticsTrackInternal;->trackInternal(Ljava/lang/String;Ljava/util/Map;J)V

    .line 614
    sget-object v0, Lcom/adobe/mobile/Lifecycle;->_lifecycleContextData:Ljava/util/HashMap;

    const-string v1, "a.CrashEvent"

    const-string v2, "CrashEvent"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_11f
    .catch Lcom/adobe/mobile/StaticMethods$NullContextException; {:try_start_d8 .. :try_end_11f} :catch_120

    .line 618
    .end local v17    # "defaults":Landroid/content/SharedPreferences;
    .end local v18    # "crashCData":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    .end local v18
    goto :goto_13a

    .line 616
    :catch_120
    move-exception v17

    .line 617
    .local v17, "ex":Lcom/adobe/mobile/StaticMethods$NullContextException;
    const-string v0, "Config - Unable to get crash data for backdated hit (%s)"

    const/4 v1, 0x1

    :try_start_124
    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual/range {v17 .. v17}, Lcom/adobe/mobile/StaticMethods$NullContextException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logWarningFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 618
    .end local v17    # "ex":Lcom/adobe/mobile/StaticMethods$NullContextException;
    goto :goto_13a

    .line 620
    :cond_131
    const-string v0, "a.CrashEvent"

    const-string v1, "CrashEvent"

    move-object/from16 v2, p0

    invoke-interface {v2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 624
    :goto_13a
    invoke-static {}, Lcom/adobe/mobile/AnalyticsTrackTimedAction;->sharedInstance()Lcom/adobe/mobile/AnalyticsTrackTimedAction;

    move-result-object v0

    invoke-virtual {v0}, Lcom/adobe/mobile/AnalyticsTrackTimedAction;->trackTimedActionUpdateActionsClearAdjustedStartTime()V
    :try_end_141
    .catch Lcom/adobe/mobile/StaticMethods$NullContextException; {:try_start_124 .. :try_end_141} :catch_142

    .line 628
    .end local v4    # "dailyFormatter":Ljava/text/DateFormat;
    .end local v5    # "lastUseMillis":J
    .end local v7    # "lastUsedDateDaily":Ljava/lang/String;
    .end local v8    # "currentDateDaily":Ljava/lang/String;
    .end local v9    # "monthlyFormatter":Ljava/text/DateFormat;
    .end local v10    # "lastUsedDateMonthly":Ljava/lang/String;
    .end local v11    # "currentDateMonthly":Ljava/lang/String;
    .end local v12    # "installMillis":J
    .end local v14    # "editor":Landroid/content/SharedPreferences$Editor;
    .end local v15    # "lastKnownTimestamp":J
    :cond_141
    goto :goto_152

    .line 626
    :catch_142
    move-exception v4

    .line 627
    .local v4, "e":Lcom/adobe/mobile/StaticMethods$NullContextException;
    const-string v0, "Lifecycle - Error setting non install data (%s)."

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v4}, Lcom/adobe/mobile/StaticMethods$NullContextException;->getMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logErrorFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 629
    .end local v4    # "e":Lcom/adobe/mobile/StaticMethods$NullContextException;
    :goto_152
    return-void
.end method

.method private static addPersistedLifecycleToMap(Ljava/util/Map;)V
    .registers 7
    .param p0, "data"    # Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;)V"
        }
    .end annotation

    .line 335
    :try_start_0
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "ADMS_LifecycleData"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 337
    .local v4, "lifecycleJSONString":Ljava/lang/String;
    if-eqz v4, :cond_1f

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_1f

    .line 338
    new-instance v5, Lorg/json/JSONObject;

    invoke-direct {v5, v4}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 339
    .local v5, "lifecycleData":Lorg/json/JSONObject;
    invoke-static {v5}, Lcom/adobe/mobile/StaticMethods;->mapFromJson(Lorg/json/JSONObject;)Ljava/util/HashMap;

    move-result-object v0

    invoke-interface {p0, v0}, Ljava/util/Map;->putAll(Ljava/util/Map;)V
    :try_end_1f
    .catch Lcom/adobe/mobile/StaticMethods$NullContextException; {:try_start_0 .. :try_end_1f} :catch_20
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_1f} :catch_31

    .line 345
    .end local v4    # "lifecycleJSONString":Ljava/lang/String;
    .end local v5    # "lifecycleData":Lorg/json/JSONObject;
    :cond_1f
    goto :goto_41

    .line 341
    :catch_20
    move-exception v4

    .line 342
    .local v4, "e":Lcom/adobe/mobile/StaticMethods$NullContextException;
    const-string v0, "Lifecycle - Issue loading persisted lifecycle data"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v4}, Lcom/adobe/mobile/StaticMethods$NullContextException;->getMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logErrorFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 345
    .end local v4    # "e":Lcom/adobe/mobile/StaticMethods$NullContextException;
    goto :goto_41

    .line 343
    :catch_31
    move-exception v4

    .line 344
    .local v4, "ex":Lorg/json/JSONException;
    const-string v0, "Lifecycle - Issue loading persisted lifecycle data (%s)"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v4}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logWarningFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 346
    .end local v4    # "ex":Lorg/json/JSONException;
    :goto_41
    return-void
.end method

.method private static addSessionLengthData(Ljava/util/Map;)V
    .registers 19
    .param p0, "cdata"    # Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;)V"
        }
    .end annotation

    .line 634
    :try_start_0
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "ADMS_PauseDate"

    const-wide/16 v2, 0x0

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/adobe/mobile/Lifecycle;->msToSec(J)J

    move-result-wide v4

    .line 635
    .local v4, "pauseTime":J
    invoke-static {}, Lcom/adobe/mobile/MobileConfig;->getInstance()Lcom/adobe/mobile/MobileConfig;

    move-result-object v0

    invoke-virtual {v0}, Lcom/adobe/mobile/MobileConfig;->getLifecycleTimeout()I

    move-result v0

    int-to-long v6, v0

    .line 636
    .local v6, "sessionTimeout":J
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getTimeSince1970()J
    :try_end_1c
    .catch Lcom/adobe/mobile/StaticMethods$NullContextException; {:try_start_0 .. :try_end_1c} :catch_fa

    move-result-wide v0

    sub-long v8, v0, v4

    .line 639
    .local v8, "timeSincePause":J
    cmp-long v0, v8, v6

    if-gez v0, :cond_24

    .line 640
    return-void

    .line 643
    :cond_24
    :try_start_24
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "ADMS_SessionStart"

    const-wide/16 v2, 0x0

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/adobe/mobile/Lifecycle;->msToSec(J)J

    move-result-wide v10

    .line 644
    .local v10, "lastSessionStartTime":J
    sub-long v12, v4, v10

    .line 645
    .local v12, "lastSessionTime":J
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getTimeSince1970()J

    move-result-wide v0

    sput-wide v0, Lcom/adobe/mobile/Lifecycle;->sessionStartTime:J

    .line 648
    const-wide/16 v0, 0x0

    cmp-long v0, v12, v0

    if-lez v0, :cond_e2

    const-wide/32 v0, 0x93a80

    cmp-long v0, v12, v0

    if-gez v0, :cond_e2

    .line 651
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "ADBLastKnownTimestampKey"

    const-wide/16 v2, 0x0

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v14

    .line 654
    .local v14, "lastKnownTimestamp":J
    const-wide/16 v0, 0x0

    cmp-long v0, v14, v0

    if-lez v0, :cond_d6

    .line 655
    invoke-static {}, Lcom/adobe/mobile/MobileConfig;->getInstance()Lcom/adobe/mobile/MobileConfig;

    move-result-object v0

    invoke-virtual {v0}, Lcom/adobe/mobile/MobileConfig;->mobileUsingAnalytics()Z

    move-result v0

    if-eqz v0, :cond_d6

    .line 656
    invoke-static {}, Lcom/adobe/mobile/MobileConfig;->getInstance()Lcom/adobe/mobile/MobileConfig;

    move-result-object v0

    invoke-virtual {v0}, Lcom/adobe/mobile/MobileConfig;->getOfflineTrackingEnabled()Z

    move-result v0

    if-eqz v0, :cond_d6

    .line 657
    invoke-static {}, Lcom/adobe/mobile/MobileConfig;->getInstance()Lcom/adobe/mobile/MobileConfig;

    move-result-object v0

    invoke-virtual {v0}, Lcom/adobe/mobile/MobileConfig;->getBackdateSessionInfoEnabled()Z
    :try_end_76
    .catch Lcom/adobe/mobile/StaticMethods$NullContextException; {:try_start_24 .. :try_end_76} :catch_fa

    move-result v0

    if-eqz v0, :cond_d6

    .line 660
    :try_start_79
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v16

    .line 661
    .local v16, "defaults":Landroid/content/SharedPreferences;
    new-instance v17, Ljava/util/HashMap;

    invoke-direct/range {v17 .. v17}, Ljava/util/HashMap;-><init>()V

    .line 662
    .local v17, "sessionCData":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    const-string v0, "a.PrevSessionLength"

    invoke-static {v12, v13}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    move-object/from16 v2, v17

    invoke-virtual {v2, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 663
    const-string v0, "a.OSVersion"

    const-string v1, "ADOBEMOBILE_STOREDDEFAULTS_OS"

    const-string v2, ""

    move-object/from16 v3, v16

    invoke-interface {v3, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    move-object/from16 v2, v17

    invoke-virtual {v2, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 664
    const-string v0, "a.AppID"

    const-string v1, "ADOBEMOBILE_STOREDDEFAULTS_APPID"

    const-string v2, ""

    move-object/from16 v3, v16

    invoke-interface {v3, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    move-object/from16 v2, v17

    invoke-virtual {v2, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 667
    const-string v0, "SessionInfo"

    const-wide/16 v1, 0x1

    add-long/2addr v1, v14

    move-object/from16 v3, v17

    invoke-static {v0, v3, v1, v2}, Lcom/adobe/mobile/AnalyticsTrackInternal;->trackInternal(Ljava/lang/String;Ljava/util/Map;J)V

    .line 670
    sget-object v0, Lcom/adobe/mobile/Lifecycle;->_lifecycleContextData:Ljava/util/HashMap;

    const-string v1, "a.PrevSessionLength"

    invoke-static {v12, v13}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_c4
    .catch Lcom/adobe/mobile/StaticMethods$NullContextException; {:try_start_79 .. :try_end_c4} :catch_c5

    .line 674
    .end local v16    # "defaults":Landroid/content/SharedPreferences;
    .end local v17    # "sessionCData":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    .end local v17
    goto :goto_e1

    .line 672
    :catch_c5
    move-exception v16

    .line 673
    .local v16, "ex":Lcom/adobe/mobile/StaticMethods$NullContextException;
    const-string v0, "Config - Unable to get session data for backdated hit (%s)"

    const/4 v1, 0x1

    :try_start_c9
    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual/range {v16 .. v16}, Lcom/adobe/mobile/StaticMethods$NullContextException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logWarningFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 674
    .end local v16    # "ex":Lcom/adobe/mobile/StaticMethods$NullContextException;
    goto :goto_e1

    .line 676
    :cond_d6
    const-string v0, "a.PrevSessionLength"

    invoke-static {v12, v13}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    move-object/from16 v2, p0

    invoke-interface {v2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 679
    .end local v14    # "lastKnownTimestamp":J
    :goto_e1
    goto :goto_ed

    .line 680
    :cond_e2
    const-string v0, "a.ignoredSessionLength"

    invoke-static {v12, v13}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    move-object/from16 v2, p0

    invoke-interface {v2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 683
    :goto_ed
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getSharedPreferencesEditor()Landroid/content/SharedPreferences$Editor;

    move-result-object v14

    .line 685
    .local v14, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v0, "ADMS_SessionStart"

    invoke-interface {v14, v0}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 686
    invoke-interface {v14}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_f9
    .catch Lcom/adobe/mobile/StaticMethods$NullContextException; {:try_start_c9 .. :try_end_f9} :catch_fa

    .line 689
    .end local v4    # "pauseTime":J
    .end local v6    # "sessionTimeout":J
    .end local v8    # "timeSincePause":J
    .end local v10    # "lastSessionStartTime":J
    .end local v12    # "lastSessionTime":J
    .end local v14    # "editor":Landroid/content/SharedPreferences$Editor;
    goto :goto_10a

    .line 687
    :catch_fa
    move-exception v4

    .line 688
    .local v4, "e":Lcom/adobe/mobile/StaticMethods$NullContextException;
    const-string v0, "Lifecycle - Error adding session length data (%s)."

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v4}, Lcom/adobe/mobile/StaticMethods$NullContextException;->getMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logErrorFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 690
    .end local v4    # "e":Lcom/adobe/mobile/StaticMethods$NullContextException;
    :goto_10a
    return-void
.end method

.method private static addUpgradeData(Ljava/util/Map;J)V
    .registers 11
    .param p0, "cdata"    # Ljava/util/Map;
    .param p1, "date"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;J)V"
        }
    .end annotation

    .line 506
    :try_start_0
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getSharedPreferencesEditor()Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    .line 508
    .local v4, "editor":Landroid/content/SharedPreferences$Editor;
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "ADMS_UpgradeDate"

    const-wide/16 v2, 0x0

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v5

    .line 510
    .local v5, "upgradeDate":J
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getApplicationVersion()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v1

    const-string v2, "ADMS_LastVersion"

    const-string v3, ""

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_39

    .line 511
    const-string v0, "a.UpgradeEvent"

    const-string v1, "UpgradeEvent"

    invoke-interface {p0, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 512
    const-string v0, "ADMS_UpgradeDate"

    invoke-interface {v4, v0, p1, p2}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 513
    const-string v0, "ADMS_LaunchesAfterUpgrade"

    const/4 v1, 0x0

    invoke-interface {v4, v0, v1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    goto :goto_48

    .line 514
    :cond_39
    const-wide/16 v0, 0x0

    cmp-long v0, v5, v0

    if-lez v0, :cond_48

    .line 515
    const-string v0, "a.DaysSinceLastUpgrade"

    invoke-static {v5, v6, p1, p2}, Lcom/adobe/mobile/Lifecycle;->calculateDaysSince(JJ)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p0, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 518
    :cond_48
    :goto_48
    const-wide/16 v0, 0x0

    cmp-long v0, v5, v0

    if-lez v0, :cond_78

    .line 519
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "ADMS_LaunchesAfterUpgrade"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    add-int/lit8 v7, v0, 0x1

    .line 520
    .local v7, "newLaunchesAfterUpgradeCount":I
    const-string v0, "a.LaunchesSinceUpgrade"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p0, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 521
    const-string v0, "ADMS_LaunchesAfterUpgrade"

    invoke-interface {v4, v0, v7}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 524
    .end local v7    # "newLaunchesAfterUpgradeCount":I
    :cond_78
    invoke-interface {v4}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_7b
    .catch Lcom/adobe/mobile/StaticMethods$NullContextException; {:try_start_0 .. :try_end_7b} :catch_7c

    .line 527
    .end local v4    # "editor":Landroid/content/SharedPreferences$Editor;
    .end local v5    # "upgradeDate":J
    goto :goto_8c

    .line 525
    :catch_7c
    move-exception v4

    .line 526
    .local v4, "e":Lcom/adobe/mobile/StaticMethods$NullContextException;
    const-string v0, "Lifecycle - Error setting upgrade data (%s)."

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v4}, Lcom/adobe/mobile/StaticMethods$NullContextException;->getMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logErrorFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 528
    .end local v4    # "e":Lcom/adobe/mobile/StaticMethods$NullContextException;
    :goto_8c
    return-void
.end method

.method private static calculateDaysSince(JJ)Ljava/lang/String;
    .registers 8
    .param p0, "since"    # J
    .param p2, "to"    # J

    .line 694
    sub-long v0, p2, p0

    const-wide/32 v2, 0x5265c00

    div-long/2addr v0, v2

    long-to-int v0, v0

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static checkForAdobeClickThrough(Landroid/app/Activity;Z)V
    .registers 11
    .param p0, "activity"    # Landroid/app/Activity;
    .param p1, "isNewSession"    # Z

    .line 703
    if-nez p0, :cond_3

    return-void

    .line 706
    :cond_3
    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    .line 707
    .local v2, "intent":Landroid/content/Intent;
    if-nez v2, :cond_a

    return-void

    .line 710
    :cond_a
    const/4 v3, 0x0

    .line 711
    .local v3, "action":Ljava/lang/String;
    const-string v0, "adb_m_id"

    invoke-virtual {v2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 712
    .local v4, "pushMessageID":Ljava/lang/String;
    const-string v0, "adb_m_l_id"

    invoke-virtual {v2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 715
    .local v5, "messageID":Ljava/lang/String;
    invoke-virtual {v2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v6

    .line 717
    .local v6, "uri":Landroid/net/Uri;
    invoke-static {v6}, Lcom/adobe/mobile/Lifecycle;->getAdobeDeepLinkQueryParameters(Landroid/net/Uri;)Ljava/util/Map;

    move-result-object v7

    .line 718
    .local v7, "deepLinkData":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    new-instance v8, Ljava/util/HashMap;

    invoke-direct {v8}, Ljava/util/HashMap;-><init>()V

    .line 721
    .local v8, "cData":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    if-nez p1, :cond_30

    if-eqz v7, :cond_30

    .line 722
    const-string v3, "AdobeLink"

    .line 723
    invoke-virtual {v8, v7}, Ljava/util/HashMap;->putAll(Ljava/util/Map;)V

    .line 724
    invoke-static {v8}, Lcom/adobe/mobile/Lifecycle;->updateContextData(Ljava/util/Map;)V

    .line 728
    :cond_30
    if-eqz v4, :cond_43

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_43

    .line 729
    const-string v0, "a.push.payloadId"

    invoke-virtual {v8, v0, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 730
    const-string v3, "PushMessage"

    .line 731
    invoke-static {v8}, Lcom/adobe/mobile/Lifecycle;->updateContextData(Ljava/util/Map;)V

    goto :goto_5f

    .line 732
    :cond_43
    if-eqz v5, :cond_5f

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_5f

    .line 733
    const-string v0, "a.message.id"

    invoke-virtual {v8, v0, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 734
    const-string v0, "a.message.clicked"

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v8, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 735
    const-string v3, "In-App Message"

    .line 736
    invoke-static {v8}, Lcom/adobe/mobile/Lifecycle;->updateContextData(Ljava/util/Map;)V

    .line 741
    :cond_5f
    :goto_5f
    if-eqz v3, :cond_72

    invoke-static {}, Lcom/adobe/mobile/MobileConfig;->getInstance()Lcom/adobe/mobile/MobileConfig;

    move-result-object v0

    invoke-virtual {v0}, Lcom/adobe/mobile/MobileConfig;->mobileUsingAnalytics()Z

    move-result v0

    if-eqz v0, :cond_72

    .line 742
    .line 744
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getTimeSince1970()J

    move-result-wide v0

    .line 742
    invoke-static {v3, v8, v0, v1}, Lcom/adobe/mobile/AnalyticsTrackInternal;->trackInternal(Ljava/lang/String;Ljava/util/Map;J)V

    .line 746
    :cond_72
    return-void
.end method

.method private static checkForAdobeLinkData(Landroid/app/Activity;)Ljava/util/Map;
    .registers 5
    .param p0, "activity"    # Landroid/app/Activity;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Landroid/app/Activity;)Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
        }
    .end annotation

    .line 750
    if-nez p0, :cond_4

    const/4 v0, 0x0

    return-object v0

    .line 754
    :cond_4
    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 755
    .local v1, "intent":Landroid/content/Intent;
    if-nez v1, :cond_c

    const/4 v0, 0x0

    return-object v0

    .line 758
    :cond_c
    invoke-virtual {v1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v2

    .line 759
    .local v2, "uri":Landroid/net/Uri;
    if-nez v2, :cond_14

    const/4 v0, 0x0

    return-object v0

    .line 761
    :cond_14
    invoke-static {v2}, Lcom/adobe/mobile/Lifecycle;->getAdobeDeepLinkQueryParameters(Landroid/net/Uri;)Ljava/util/Map;

    move-result-object v3

    .line 763
    .local v3, "queryParameters":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    return-object v3
.end method

.method private static checkReferrerDataForLaunch()V
    .registers 3

    .line 388
    invoke-static {}, Lcom/adobe/mobile/Lifecycle;->getReferrerDataFromSharedPreferences()Ljava/util/Map;

    move-result-object v2

    .line 389
    .local v2, "referrerData":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    if-eqz v2, :cond_c

    invoke-interface {v2}, Ljava/util/Map;->size()I

    move-result v0

    if-nez v0, :cond_d

    .line 390
    :cond_c
    return-void

    .line 392
    :cond_d
    invoke-static {v2}, Lcom/adobe/mobile/Lifecycle;->updateContextData(Ljava/util/Map;)V

    .line 393
    invoke-static {}, Lcom/adobe/mobile/MobileConfig;->getInstance()Lcom/adobe/mobile/MobileConfig;

    move-result-object v0

    sget-object v1, Lcom/adobe/mobile/Config$MobileDataEvent;->MOBILE_EVENT_ACQUISITION_LAUNCH:Lcom/adobe/mobile/Config$MobileDataEvent;

    invoke-virtual {v0, v1, v2}, Lcom/adobe/mobile/MobileConfig;->invokeAdobeDataCallback(Lcom/adobe/mobile/Config$MobileDataEvent;Ljava/util/Map;)V

    .line 394
    return-void
.end method

.method private static clearContextDataLowercase()V
    .registers 3

    .line 286
    sget-object v1, Lcom/adobe/mobile/Lifecycle;->_lowercaseContextDataMutex:Ljava/lang/Object;

    monitor-enter v1

    .line 287
    :try_start_3
    sget-object v0, Lcom/adobe/mobile/Lifecycle;->_lifecycleContextDataLowercase:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V
    :try_end_8
    .catchall {:try_start_3 .. :try_end_8} :catchall_a

    .line 288
    monitor-exit v1

    goto :goto_d

    :catchall_a
    move-exception v2

    monitor-exit v1

    throw v2

    .line 289
    :goto_d
    return-void
.end method

.method private static generateLifecycleToBeSaved(Ljava/util/Map;)V
    .registers 7
    .param p0, "data"    # Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;)V"
        }
    .end annotation

    .line 349
    if-eqz p0, :cond_8

    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3, p0}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    goto :goto_d

    :cond_8
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 352
    .local v3, "mutableData":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    :goto_d
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getDefaultData()Ljava/util/HashMap;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/util/HashMap;->putAll(Ljava/util/Map;)V

    .line 353
    const-string v0, "a.locale"

    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getDefaultAcceptLanguage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 356
    const-string v0, "a.ltv.amount"

    invoke-static {}, Lcom/adobe/mobile/AnalyticsTrackLifetimeValueIncrease;->getLifetimeValue()Ljava/math/BigDecimal;

    move-result-object v1

    invoke-virtual {v3, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 358
    sget-object v0, Lcom/adobe/mobile/Lifecycle;->_lifecycleContextData:Ljava/util/HashMap;

    invoke-virtual {v0, v3}, Ljava/util/HashMap;->putAll(Ljava/util/Map;)V

    .line 361
    invoke-static {}, Lcom/adobe/mobile/Lifecycle;->clearContextDataLowercase()V

    .line 362
    sget-object v0, Lcom/adobe/mobile/Lifecycle;->_lifecycleContextData:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_38
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_59

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Ljava/util/Map$Entry;

    .line 363
    .local v5, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Object;>;"
    sget-object v0, Lcom/adobe/mobile/Lifecycle;->_lifecycleContextDataLowercase:Ljava/util/HashMap;

    invoke-interface {v5}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v5}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 364
    .end local v5    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Object;>;"
    .end local v5
    goto :goto_38

    .line 365
    :cond_59
    return-void
.end method

.method private static getAdobeDeepLinkQueryParameters(Landroid/net/Uri;)Ljava/util/Map;
    .registers 13
    .param p0, "uri"    # Landroid/net/Uri;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Landroid/net/Uri;)Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
        }
    .end annotation

    .line 768
    if-nez p0, :cond_4

    .line 769
    const/4 v0, 0x0

    return-object v0

    .line 772
    :cond_4
    invoke-virtual {p0}, Landroid/net/Uri;->getQuery()Ljava/lang/String;

    move-result-object v4

    .line 775
    .local v4, "query":Ljava/lang/String;
    if-eqz v4, :cond_18

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_18

    const-string v0, "a.deeplink.id="

    invoke-virtual {v4, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1a

    .line 776
    :cond_18
    const/4 v0, 0x0

    return-object v0

    .line 779
    :cond_1a
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    .line 780
    .local v5, "parameters":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    const-string v0, "&"

    invoke-virtual {v4, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    .line 782
    .local v6, "paramArray":[Ljava/lang/String;
    move-object v7, v6

    array-length v8, v7

    const/4 v9, 0x0

    :goto_28
    if-ge v9, v8, :cond_68

    aget-object v10, v7, v9

    .line 784
    .local v10, "currentParam":Ljava/lang/String;
    if-eqz v10, :cond_65

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v0

    if-gtz v0, :cond_35

    .line 785
    goto :goto_65

    .line 788
    :cond_35
    const-string v0, "="

    const/4 v1, 0x2

    invoke-virtual {v10, v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v11

    .line 794
    .local v11, "currentParamArray":[Ljava/lang/String;
    array-length v0, v11

    const/4 v1, 0x1

    if-eq v0, v1, :cond_4d

    array-length v0, v11

    const/4 v1, 0x2

    if-ne v0, v1, :cond_5c

    const/4 v0, 0x1

    aget-object v0, v11, v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_5c

    .line 795
    :cond_4d
    const-string v0, "Deep Link - Skipping an invalid variable on the URI query (%s)."

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aget-object v2, v11, v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logWarningFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 796
    goto :goto_65

    .line 800
    :cond_5c
    const/4 v0, 0x0

    aget-object v0, v11, v0

    const/4 v1, 0x1

    aget-object v1, v11, v1

    invoke-interface {v5, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 782
    .end local v10    # "currentParam":Ljava/lang/String;
    .end local v11    # "currentParamArray":[Ljava/lang/String;
    :cond_65
    :goto_65
    add-int/lit8 v9, v9, 0x1

    goto :goto_28

    .line 803
    :cond_68
    return-object v5
.end method

.method protected static getContextDataLowercase()Ljava/util/Map;
    .registers 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
        }
    .end annotation

    .line 271
    sget-object v3, Lcom/adobe/mobile/Lifecycle;->_lowercaseContextDataMutex:Ljava/lang/Object;

    monitor-enter v3

    .line 272
    :try_start_3
    sget-object v0, Lcom/adobe/mobile/Lifecycle;->_lifecycleContextDataLowercase:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v0

    if-gtz v0, :cond_3c

    .line 274
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    .line 275
    .local v4, "previousSessionlifecycleContextData":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    invoke-static {v4}, Lcom/adobe/mobile/Lifecycle;->addPersistedLifecycleToMap(Ljava/util/Map;)V

    .line 276
    invoke-virtual {v4}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1b
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3c

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Ljava/util/Map$Entry;

    .line 277
    .local v6, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Object;>;"
    sget-object v0, Lcom/adobe/mobile/Lifecycle;->_lifecycleContextDataLowercase:Ljava/util/HashMap;

    invoke-interface {v6}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v6}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 278
    .end local v6    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Object;>;"
    .end local v6
    goto :goto_1b

    .line 281
    .end local v4    # "previousSessionlifecycleContextData":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    .end local v4
    :cond_3c
    sget-object v0, Lcom/adobe/mobile/Lifecycle;->_lifecycleContextDataLowercase:Ljava/util/HashMap;
    :try_end_3e
    .catchall {:try_start_3 .. :try_end_3e} :catchall_40

    monitor-exit v3

    return-object v0

    .line 282
    :catchall_40
    move-exception v7

    monitor-exit v3

    throw v7
.end method

.method private static getReferrerDataFromSharedPreferences()Ljava/util/Map;
    .registers 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
        }
    .end annotation

    .line 399
    :try_start_0
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "ADMS_Referrer_ContextData_Json_String"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4d

    .line 400
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    .line 403
    .local v4, "referrerData":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "ADMS_Referrer_ContextData_Json_String"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 407
    .local v5, "referrerDataString":Ljava/lang/String;
    invoke-static {v5}, Lcom/adobe/mobile/ReferrerHandler;->processReferrerDataFromV3Server(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 408
    invoke-static {v5}, Lcom/adobe/mobile/ReferrerHandler;->processV3ResponseAndReturnContextData(Ljava/lang/String;)Ljava/util/HashMap;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/util/HashMap;->putAll(Ljava/util/Map;)V

    .line 409
    invoke-virtual {v4}, Ljava/util/HashMap;->size()I

    move-result v0

    if-lez v0, :cond_35

    .line 411
    invoke-static {v5}, Lcom/adobe/mobile/ReferrerHandler;->processV3ResponseAndReturnAdobeData(Ljava/lang/String;)Ljava/util/HashMap;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/util/HashMap;->putAll(Ljava/util/Map;)V

    goto :goto_4c

    .line 415
    :cond_35
    invoke-static {v5}, Lcom/adobe/mobile/ReferrerHandler;->parseGoogleReferrerFields(Ljava/lang/String;)Ljava/util/HashMap;

    move-result-object v6

    .line 416
    .local v6, "googleReferrerData":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    const-string v0, "a.referrer.campaign.name"

    invoke-virtual {v6, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4c

    const-string v0, "a.referrer.campaign.source"

    .line 417
    invoke-virtual {v6, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4c

    .line 418
    invoke-virtual {v4, v6}, Ljava/util/HashMap;->putAll(Ljava/util/Map;)V
    :try_end_4c
    .catch Lcom/adobe/mobile/StaticMethods$NullContextException; {:try_start_0 .. :try_end_4c} :catch_105

    .line 421
    .end local v6    # "googleReferrerData":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    .end local v6
    :cond_4c
    :goto_4c
    return-object v4

    .line 422
    .end local v4    # "referrerData":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    .end local v4
    .end local v5    # "referrerDataString":Ljava/lang/String;
    :cond_4d
    :try_start_4d
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "utm_campaign"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_104

    .line 426
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "utm_source"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 427
    .local v4, "source":Ljava/lang/String;
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "utm_medium"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 428
    .local v5, "medium":Ljava/lang/String;
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "utm_term"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 429
    .local v6, "term":Ljava/lang/String;
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "utm_content"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 430
    .local v7, "content":Ljava/lang/String;
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "utm_campaign"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 431
    .local v8, "campaign":Ljava/lang/String;
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "trackingcode"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 433
    .local v9, "trackingcode":Ljava/lang/String;
    if-eqz v4, :cond_104

    if-eqz v8, :cond_104

    .line 434
    new-instance v10, Ljava/util/HashMap;

    invoke-direct {v10}, Ljava/util/HashMap;-><init>()V

    .line 436
    .local v10, "referrerContextData":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    const-string v0, "a.referrer.campaign.source"

    invoke-virtual {v10, v0, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 437
    const-string v0, "a.referrer.campaign.medium"

    invoke-virtual {v10, v0, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 438
    const-string v0, "a.referrer.campaign.term"

    invoke-virtual {v10, v0, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 439
    const-string v0, "a.referrer.campaign.content"

    invoke-virtual {v10, v0, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 440
    const-string v0, "a.referrer.campaign.name"

    invoke-virtual {v10, v0, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 441
    const-string v0, "a.referrer.campaign.trackingcode"

    invoke-virtual {v10, v0, v9}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_c2
    .catch Lcom/adobe/mobile/StaticMethods$NullContextException; {:try_start_4d .. :try_end_c2} :catch_105

    .line 445
    :try_start_c2
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getSharedPreferencesEditor()Landroid/content/SharedPreferences$Editor;

    move-result-object v11

    .line 447
    .local v11, "editor":Landroid/content/SharedPreferences$Editor;
    new-instance v12, Lorg/json/JSONObject;

    invoke-direct {v12}, Lorg/json/JSONObject;-><init>()V

    .line 448
    .local v12, "googleReferrerObject":Lorg/json/JSONObject;
    const-string v0, "googleReferrerData"

    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, v10}, Lorg/json/JSONObject;-><init>(Ljava/util/Map;)V

    invoke-virtual {v12, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 449
    const-string v0, "ADMS_Referrer_ContextData_Json_String"

    invoke-virtual {v12}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v11, v0, v1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 450
    invoke-interface {v11}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_e1
    .catch Lcom/adobe/mobile/StaticMethods$NullContextException; {:try_start_c2 .. :try_end_e1} :catch_e2
    .catch Lorg/json/JSONException; {:try_start_c2 .. :try_end_e1} :catch_f3

    .line 455
    .end local v11    # "editor":Landroid/content/SharedPreferences$Editor;
    .end local v12    # "googleReferrerObject":Lorg/json/JSONObject;
    goto :goto_103

    .line 451
    :catch_e2
    move-exception v11

    .line 452
    .local v11, "e":Lcom/adobe/mobile/StaticMethods$NullContextException;
    const-string v0, "Analytics - Error persisting referrer data (%s)"

    const/4 v1, 0x1

    :try_start_e6
    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v11}, Lcom/adobe/mobile/StaticMethods$NullContextException;->getMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logErrorFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 455
    .end local v11    # "e":Lcom/adobe/mobile/StaticMethods$NullContextException;
    goto :goto_103

    .line 453
    :catch_f3
    move-exception v11

    .line 454
    .local v11, "e":Lorg/json/JSONException;
    const-string v0, "Analytics - Error persisting referrer data (%s)"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v11}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logErrorFormat(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_103
    .catch Lcom/adobe/mobile/StaticMethods$NullContextException; {:try_start_e6 .. :try_end_103} :catch_105

    .line 458
    .end local v11    # "e":Lorg/json/JSONException;
    :goto_103
    return-object v10

    .line 464
    .end local v4    # "source":Ljava/lang/String;
    .end local v5    # "medium":Ljava/lang/String;
    .end local v6    # "term":Ljava/lang/String;
    .end local v7    # "content":Ljava/lang/String;
    .end local v8    # "campaign":Ljava/lang/String;
    .end local v9    # "trackingcode":Ljava/lang/String;
    .end local v10    # "referrerContextData":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    .end local v10
    :cond_104
    goto :goto_115

    .line 462
    :catch_105
    move-exception v4

    .line 463
    .local v4, "e":Lcom/adobe/mobile/StaticMethods$NullContextException;
    const-string v0, "Lifecycle - Error pulling persisted Acquisition data (%s)"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v4}, Lcom/adobe/mobile/StaticMethods$NullContextException;->getMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logErrorFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 466
    .end local v4    # "e":Lcom/adobe/mobile/StaticMethods$NullContextException;
    :goto_115
    const/4 v0, 0x0

    return-object v0
.end method

.method private static msToSec(J)J
    .registers 4
    .param p0, "milliSeconds"    # J

    .line 807
    const-wide/16 v0, 0x3e8

    div-long v0, p0, v0

    return-wide v0
.end method

.method private static persistLifecycleContextData()V
    .registers 6

    .line 218
    :try_start_0
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getSharedPreferencesEditor()Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    .line 219
    .local v4, "editor":Landroid/content/SharedPreferences$Editor;
    new-instance v5, Lorg/json/JSONObject;

    sget-object v0, Lcom/adobe/mobile/Lifecycle;->_lifecycleContextData:Ljava/util/HashMap;

    invoke-direct {v5, v0}, Lorg/json/JSONObject;-><init>(Ljava/util/Map;)V

    .line 220
    .local v5, "lifecycleJSON":Lorg/json/JSONObject;
    const-string v0, "ADMS_LifecycleData"

    invoke-virtual {v5}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v4, v0, v1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 221
    invoke-interface {v4}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_17
    .catch Lcom/adobe/mobile/StaticMethods$NullContextException; {:try_start_0 .. :try_end_17} :catch_18

    .line 225
    .end local v4    # "editor":Landroid/content/SharedPreferences$Editor;
    .end local v5    # "lifecycleJSON":Lorg/json/JSONObject;
    goto :goto_28

    .line 223
    :catch_18
    move-exception v4

    .line 224
    .local v4, "e":Lcom/adobe/mobile/StaticMethods$NullContextException;
    const-string v0, "Lifecycle - Error persisting lifecycle data (%s)"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v4}, Lcom/adobe/mobile/StaticMethods$NullContextException;->getMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logWarningFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 226
    .end local v4    # "e":Lcom/adobe/mobile/StaticMethods$NullContextException;
    :goto_28
    return-void
.end method

.method private static resetLifecycleFlags(J)V
    .registers 7
    .param p0, "date"    # J

    .line 369
    :try_start_0
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getSharedPreferencesEditor()Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    .line 371
    .local v4, "editor":Landroid/content/SharedPreferences$Editor;
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "ADMS_SessionStart"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1b

    .line 372
    const-string v0, "ADMS_SessionStart"

    invoke-interface {v4, v0, p0, p1}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 375
    const-wide/16 v0, 0x3e8

    div-long v0, p0, v0

    sput-wide v0, Lcom/adobe/mobile/Lifecycle;->sessionStartTime:J

    .line 378
    :cond_1b
    const-string v0, "ADMS_LastVersion"

    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getApplicationVersion()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v4, v0, v1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 379
    const-string v0, "ADMS_SuccessfulClose"

    const/4 v1, 0x0

    invoke-interface {v4, v0, v1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 380
    const-string v0, "ADMS_PauseDate"

    invoke-interface {v4, v0}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 381
    invoke-interface {v4}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_32
    .catch Lcom/adobe/mobile/StaticMethods$NullContextException; {:try_start_0 .. :try_end_32} :catch_33

    .line 384
    .end local v4    # "editor":Landroid/content/SharedPreferences$Editor;
    goto :goto_43

    .line 382
    :catch_33
    move-exception v4

    .line 383
    .local v4, "e":Lcom/adobe/mobile/StaticMethods$NullContextException;
    const-string v0, "Lifecycle - Error resetting lifecycle flags (%s)."

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v4}, Lcom/adobe/mobile/StaticMethods$NullContextException;->getMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logErrorFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 385
    .end local v4    # "e":Lcom/adobe/mobile/StaticMethods$NullContextException;
    :goto_43
    return-void
.end method

.method private static secToMs(J)J
    .registers 4
    .param p0, "seconds"    # J

    .line 811
    const-wide/16 v0, 0x3e8

    mul-long/2addr v0, p0

    return-wide v0
.end method

.method protected static start(Landroid/app/Activity;Ljava/util/Map;)V
    .registers 19
    .param p0, "activity"    # Landroid/app/Activity;
    .param p1, "data"    # Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Landroid/app/Activity;Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;)V"
        }
    .end annotation

    .line 66
    sget-boolean v0, Lcom/adobe/mobile/Lifecycle;->lifecycleHasRun:Z

    if-eqz v0, :cond_5

    .line 67
    return-void

    .line 70
    :cond_5
    const/4 v0, 0x1

    sput-boolean v0, Lcom/adobe/mobile/Lifecycle;->lifecycleHasRun:Z

    .line 74
    :try_start_8
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getSharedPreferences()Landroid/content/SharedPreferences;
    :try_end_b
    .catch Lcom/adobe/mobile/StaticMethods$NullContextException; {:try_start_8 .. :try_end_b} :catch_d

    move-result-object v5

    .line 78
    .local v5, "userDefaults":Landroid/content/SharedPreferences;
    goto :goto_1e

    .line 75
    .end local v5    # "userDefaults":Landroid/content/SharedPreferences;
    :catch_d
    move-exception v6

    .line 76
    .local v6, "e":Lcom/adobe/mobile/StaticMethods$NullContextException;
    const-string v0, "Lifecycle - Error starting lifecycle (%s)."

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v6}, Lcom/adobe/mobile/StaticMethods$NullContextException;->getMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logErrorFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 77
    return-void

    .line 82
    .local v5, "userDefaults":Landroid/content/SharedPreferences;
    .end local v6    # "e":Lcom/adobe/mobile/StaticMethods$NullContextException;
    :goto_1e
    const/4 v6, 0x0

    .line 84
    .local v6, "currentActivity":Landroid/app/Activity;
    :try_start_1f
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getCurrentActivity()Landroid/app/Activity;
    :try_end_22
    .catch Lcom/adobe/mobile/StaticMethods$NullActivityException; {:try_start_1f .. :try_end_22} :catch_25

    move-result-object v0

    move-object v6, v0

    .line 88
    goto :goto_26

    .line 86
    :catch_25
    move-exception v7

    .line 89
    :goto_26
    if-eqz v6, :cond_46

    if-eqz p0, :cond_46

    .line 90
    invoke-virtual {v6}, Landroid/app/Activity;->getComponentName()Landroid/content/ComponentName;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ComponentName;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual/range {p0 .. p0}, Landroid/app/Activity;->getComponentName()Landroid/content/ComponentName;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/ComponentName;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_46

    .line 91
    const/4 v0, 0x0

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/adobe/mobile/Messages;->checkForInAppMessage(Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;)V

    .line 95
    :cond_46
    invoke-static/range {p0 .. p0}, Lcom/adobe/mobile/StaticMethods;->setCurrentActivity(Landroid/app/Activity;)V

    .line 97
    invoke-static {}, Lcom/adobe/mobile/MobileConfig;->getInstance()Lcom/adobe/mobile/MobileConfig;

    move-result-object v7

    .line 98
    .local v7, "mobileConfig":Lcom/adobe/mobile/MobileConfig;
    const-string v0, "ADMS_PauseDate"

    const-wide/16 v1, 0x0

    invoke-interface {v5, v0, v1, v2}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/adobe/mobile/Lifecycle;->msToSec(J)J

    move-result-wide v8

    .line 99
    .local v8, "lastPauseTime":J
    invoke-virtual {v7}, Lcom/adobe/mobile/MobileConfig;->getLifecycleTimeout()I

    move-result v10

    .line 101
    .local v10, "sessionTimeout":I
    const/4 v11, 0x1

    .line 104
    .local v11, "isNewSession":Z
    const-wide/16 v0, 0x0

    cmp-long v0, v8, v0

    if-lez v0, :cond_be

    .line 105
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getTimeSince1970()J

    move-result-wide v0

    sub-long v12, v0, v8

    .line 106
    .local v12, "pausedTime":J
    const-string v0, "ADMS_SessionStart"

    const-wide/16 v1, 0x0

    invoke-interface {v5, v0, v1, v2}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/adobe/mobile/Lifecycle;->msToSec(J)J

    move-result-wide v14

    .line 108
    .local v14, "originalStartTime":J
    sput-wide v14, Lcom/adobe/mobile/Lifecycle;->sessionStartTime:J

    .line 109
    invoke-static {}, Lcom/adobe/mobile/AnalyticsTrackTimedAction;->sharedInstance()Lcom/adobe/mobile/AnalyticsTrackTimedAction;

    move-result-object v0

    invoke-virtual {v0, v12, v13}, Lcom/adobe/mobile/AnalyticsTrackTimedAction;->trackTimedActionUpdateAdjustedStartTime(J)V

    .line 111
    int-to-long v0, v10

    cmp-long v0, v12, v0

    if-gez v0, :cond_be

    const-wide/16 v0, 0x0

    cmp-long v0, v14, v0

    if-lez v0, :cond_be

    .line 115
    :try_start_8a
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getSharedPreferencesEditor()Landroid/content/SharedPreferences$Editor;

    move-result-object v16

    .line 116
    .local v16, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v0, "ADMS_SessionStart"

    add-long v1, v14, v12

    invoke-static {v1, v2}, Lcom/adobe/mobile/Lifecycle;->secToMs(J)J

    move-result-wide v1

    move-object/from16 v3, v16

    invoke-interface {v3, v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 117
    invoke-interface/range {v16 .. v16}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_9e
    .catch Lcom/adobe/mobile/StaticMethods$NullContextException; {:try_start_8a .. :try_end_9e} :catch_9f

    .line 120
    .end local v16    # "editor":Landroid/content/SharedPreferences$Editor;
    goto :goto_af

    .line 118
    :catch_9f
    move-exception v16

    .line 119
    .local v16, "e":Lcom/adobe/mobile/StaticMethods$NullContextException;
    const-string v0, "Lifecycle - Error while updating start time (%s)."

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual/range {v16 .. v16}, Lcom/adobe/mobile/StaticMethods$NullContextException;->getMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logErrorFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 122
    .end local v16    # "e":Lcom/adobe/mobile/StaticMethods$NullContextException;
    :goto_af
    const-string v0, "ADMS_SessionStart"

    const-wide/16 v1, 0x0

    invoke-interface {v5, v0, v1, v2}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/adobe/mobile/Lifecycle;->msToSec(J)J

    move-result-wide v0

    sput-wide v0, Lcom/adobe/mobile/Lifecycle;->sessionStartTime:J

    .line 123
    const/4 v11, 0x0

    .line 127
    .end local v12    # "pausedTime":J
    .end local v14    # "originalStartTime":J
    :cond_be
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getTimeSince1970()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/adobe/mobile/Lifecycle;->secToMs(J)J

    move-result-wide v12

    .line 130
    .local v12, "date":J
    if-eqz v11, :cond_138

    .line 132
    invoke-static {}, Lcom/adobe/mobile/VisitorIDService;->sharedInstance()Lcom/adobe/mobile/VisitorIDService;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x1

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/adobe/mobile/VisitorIDService;->idSync(Ljava/util/Map;Ljava/util/Map;Lcom/adobe/mobile/VisitorID$VisitorIDAuthenticationState;Z)V

    .line 135
    invoke-virtual {v7}, Lcom/adobe/mobile/MobileConfig;->downloadRemoteConfigs()V

    .line 138
    sget-object v0, Lcom/adobe/mobile/Lifecycle;->_lifecycleContextData:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 140
    invoke-static {}, Lcom/adobe/mobile/Lifecycle;->clearContextDataLowercase()V

    .line 142
    if-eqz p1, :cond_e8

    new-instance v14, Ljava/util/HashMap;

    move-object/from16 v0, p1

    invoke-direct {v14, v0}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    goto :goto_ed

    :cond_e8
    new-instance v14, Ljava/util/HashMap;

    invoke-direct {v14}, Ljava/util/HashMap;-><init>()V

    .line 145
    .local v14, "contextData":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    :goto_ed
    invoke-static/range {p0 .. p0}, Lcom/adobe/mobile/Lifecycle;->checkForAdobeLinkData(Landroid/app/Activity;)Ljava/util/Map;

    move-result-object v15

    .line 146
    .local v15, "clickThroughData":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    if-eqz v15, :cond_f6

    .line 147
    invoke-virtual {v14, v15}, Ljava/util/HashMap;->putAll(Ljava/util/Map;)V

    .line 151
    :cond_f6
    const-string v0, "ADMS_InstallDate"

    invoke-interface {v5, v0}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_102

    .line 152
    invoke-static {v14, v12, v13}, Lcom/adobe/mobile/Lifecycle;->addInstallData(Ljava/util/Map;J)V

    goto :goto_10e

    .line 154
    :cond_102
    invoke-static {v14, v12, v13}, Lcom/adobe/mobile/Lifecycle;->addNonInstallData(Ljava/util/Map;J)V

    .line 155
    invoke-static {v14, v12, v13}, Lcom/adobe/mobile/Lifecycle;->addUpgradeData(Ljava/util/Map;J)V

    .line 156
    invoke-static {v14}, Lcom/adobe/mobile/Lifecycle;->addSessionLengthData(Ljava/util/Map;)V

    .line 158
    invoke-static {}, Lcom/adobe/mobile/Lifecycle;->checkReferrerDataForLaunch()V

    .line 162
    :goto_10e
    invoke-static {v14, v12, v13}, Lcom/adobe/mobile/Lifecycle;->addLifecycleGenericData(Ljava/util/Map;J)V

    .line 163
    invoke-static {v14}, Lcom/adobe/mobile/Lifecycle;->generateLifecycleToBeSaved(Ljava/util/Map;)V

    .line 170
    invoke-static {}, Lcom/adobe/mobile/Lifecycle;->persistLifecycleContextData()V

    .line 171
    invoke-static {}, Lcom/adobe/mobile/MobileConfig;->getInstance()Lcom/adobe/mobile/MobileConfig;

    move-result-object v0

    sget-object v1, Lcom/adobe/mobile/Config$MobileDataEvent;->MOBILE_EVENT_LIFECYCLE:Lcom/adobe/mobile/Config$MobileDataEvent;

    invoke-virtual {v0, v1, v14}, Lcom/adobe/mobile/MobileConfig;->invokeAdobeDataCallback(Lcom/adobe/mobile/Config$MobileDataEvent;Ljava/util/Map;)V

    .line 172
    const-string v0, "Lifecycle"

    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getTimeSince1970()J

    move-result-wide v1

    const-wide/16 v3, 0x1

    sub-long/2addr v1, v3

    invoke-static {v0, v14, v1, v2}, Lcom/adobe/mobile/AnalyticsTrackInternal;->trackInternal(Ljava/lang/String;Ljava/util/Map;J)V

    .line 175
    invoke-virtual {v7}, Lcom/adobe/mobile/MobileConfig;->getAamAnalyticsForwardingEnabled()Z

    move-result v0

    if-nez v0, :cond_138

    .line 176
    sget-object v0, Lcom/adobe/mobile/Lifecycle;->_lifecycleContextData:Ljava/util/HashMap;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/adobe/mobile/AudienceManagerWorker;->SubmitSignal(Ljava/util/Map;Lcom/adobe/mobile/AudienceManager$AudienceManagerCallback;)V

    .line 182
    .end local v14    # "contextData":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    .end local v14
    .end local v15    # "clickThroughData":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    .end local v15
    :cond_138
    move-object/from16 v0, p0

    invoke-static {v0, v11}, Lcom/adobe/mobile/Lifecycle;->checkForAdobeClickThrough(Landroid/app/Activity;Z)V

    .line 184
    invoke-static {v12, v13}, Lcom/adobe/mobile/Lifecycle;->resetLifecycleFlags(J)V

    .line 185
    return-void
.end method

.method protected static stop()V
    .registers 6

    .line 189
    const/4 v0, 0x0

    sput-boolean v0, Lcom/adobe/mobile/Lifecycle;->lifecycleHasRun:Z

    .line 191
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getTimeSince1970()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {v0}, Lcom/adobe/mobile/StaticMethods;->updateLastKnownTimestamp(Ljava/lang/Long;)V

    .line 194
    :try_start_e
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getSharedPreferencesEditor()Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    .line 195
    .local v4, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v0, "ADMS_SuccessfulClose"

    const/4 v1, 0x1

    invoke-interface {v4, v0, v1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 196
    const-string v0, "ADMS_PauseDate"

    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getTimeSince1970()J

    move-result-wide v1

    invoke-static {v1, v2}, Lcom/adobe/mobile/Lifecycle;->secToMs(J)J

    move-result-wide v1

    invoke-interface {v4, v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 197
    invoke-interface {v4}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_28
    .catch Lcom/adobe/mobile/StaticMethods$NullContextException; {:try_start_e .. :try_end_28} :catch_29

    .line 200
    .end local v4    # "editor":Landroid/content/SharedPreferences$Editor;
    goto :goto_39

    .line 198
    :catch_29
    move-exception v4

    .line 199
    .local v4, "e":Lcom/adobe/mobile/StaticMethods$NullContextException;
    const-string v0, "Lifecycle - Error updating lifecycle pause data (%s)"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v4}, Lcom/adobe/mobile/StaticMethods$NullContextException;->getMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logErrorFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 205
    .end local v4    # "e":Lcom/adobe/mobile/StaticMethods$NullContextException;
    :goto_39
    :try_start_39
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getCurrentActivity()Landroid/app/Activity;
    :try_end_3c
    .catch Lcom/adobe/mobile/StaticMethods$NullActivityException; {:try_start_39 .. :try_end_3c} :catch_3e

    move-result-object v4

    .line 209
    .local v4, "currentActivity":Landroid/app/Activity;
    goto :goto_40

    .line 207
    .end local v4    # "currentActivity":Landroid/app/Activity;
    :catch_3e
    move-exception v5

    .line 208
    .local v5, "ex":Lcom/adobe/mobile/StaticMethods$NullActivityException;
    return-void

    .line 211
    .local v4, "currentActivity":Landroid/app/Activity;
    .end local v5    # "ex":Lcom/adobe/mobile/StaticMethods$NullActivityException;
    :goto_40
    invoke-virtual {v4}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_49

    .line 212
    invoke-static {}, Lcom/adobe/mobile/Messages;->resetAllInAppMessages()V

    .line 214
    :cond_49
    return-void
.end method

.method protected static updateContextData(Ljava/util/Map;)V
    .registers 8
    .param p0, "data"    # Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;)V"
        }
    .end annotation

    .line 247
    sget-object v3, Lcom/adobe/mobile/Lifecycle;->_contextDataMutex:Ljava/lang/Object;

    monitor-enter v3

    .line 248
    :try_start_3
    sget-object v0, Lcom/adobe/mobile/Lifecycle;->_lifecycleContextData:Ljava/util/HashMap;

    invoke-virtual {v0, p0}, Ljava/util/HashMap;->putAll(Ljava/util/Map;)V
    :try_end_8
    .catchall {:try_start_3 .. :try_end_8} :catchall_a

    .line 249
    monitor-exit v3

    goto :goto_d

    :catchall_a
    move-exception v4

    monitor-exit v3

    throw v4

    .line 251
    :goto_d
    sget-object v3, Lcom/adobe/mobile/Lifecycle;->_lowercaseContextDataMutex:Ljava/lang/Object;

    monitor-enter v3

    .line 252
    :try_start_10
    invoke-interface {p0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_18
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_39

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Ljava/util/Map$Entry;

    .line 253
    .local v5, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Object;>;"
    sget-object v0, Lcom/adobe/mobile/Lifecycle;->_lifecycleContextDataLowercase:Ljava/util/HashMap;

    invoke-interface {v5}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v5}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_38
    .catchall {:try_start_10 .. :try_end_38} :catchall_3b

    .line 254
    .end local v5    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Object;>;"
    .end local v5
    goto :goto_18

    .line 255
    :cond_39
    monitor-exit v3

    goto :goto_3e

    :catchall_3b
    move-exception v6

    monitor-exit v3

    throw v6

    .line 256
    :goto_3e
    return-void
.end method

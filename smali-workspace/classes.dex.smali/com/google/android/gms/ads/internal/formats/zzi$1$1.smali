.class Lcom/google/android/gms/ads/internal/formats/zzi$1$1;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/internal/zzjq$zza;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/gms/ads/internal/formats/zzi$1;->zza(Lcom/google/android/gms/internal/zzjp;Ljava/util/Map;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic zzyy:Ljava/util/Map;

.field final synthetic zzyz:Lcom/google/android/gms/ads/internal/formats/zzi$1;


# direct methods
.method constructor <init>(Lcom/google/android/gms/ads/internal/formats/zzi$1;Ljava/util/Map;)V
    .registers 3

    iput-object p1, p0, Lcom/google/android/gms/ads/internal/formats/zzi$1$1;->zzyz:Lcom/google/android/gms/ads/internal/formats/zzi$1;

    iput-object p2, p0, Lcom/google/android/gms/ads/internal/formats/zzi$1$1;->zzyy:Ljava/util/Map;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public zza(Lcom/google/android/gms/internal/zzjp;Z)V
    .registers 8

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/formats/zzi$1$1;->zzyz:Lcom/google/android/gms/ads/internal/formats/zzi$1;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/formats/zzi$1;->zzyx:Lcom/google/android/gms/ads/internal/formats/zzi;

    iget-object v1, p0, Lcom/google/android/gms/ads/internal/formats/zzi$1$1;->zzyy:Ljava/util/Map;

    const-string v2, "id"

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/gms/ads/internal/formats/zzi;->zza(Lcom/google/android/gms/ads/internal/formats/zzi;Ljava/lang/String;)Ljava/lang/String;

    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3}, Lorg/json/JSONObject;-><init>()V

    const-string v0, "messageType"

    const-string v1, "htmlLoaded"

    :try_start_1a
    invoke-virtual {v3, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "id"

    iget-object v1, p0, Lcom/google/android/gms/ads/internal/formats/zzi$1$1;->zzyz:Lcom/google/android/gms/ads/internal/formats/zzi$1;

    iget-object v1, v1, Lcom/google/android/gms/ads/internal/formats/zzi$1;->zzyx:Lcom/google/android/gms/ads/internal/formats/zzi;

    invoke-static {v1}, Lcom/google/android/gms/ads/internal/formats/zzi;->zza(Lcom/google/android/gms/ads/internal/formats/zzi;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/formats/zzi$1$1;->zzyz:Lcom/google/android/gms/ads/internal/formats/zzi$1;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/formats/zzi$1;->zzyx:Lcom/google/android/gms/ads/internal/formats/zzi;

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/formats/zzi;->zzb(Lcom/google/android/gms/ads/internal/formats/zzi;)Lcom/google/android/gms/internal/zzed;

    move-result-object v0

    const-string v1, "sendMessageToNativeJs"

    invoke-interface {v0, v1, v3}, Lcom/google/android/gms/internal/zzed;->zzb(Ljava/lang/String;Lorg/json/JSONObject;)V
    :try_end_37
    .catch Lorg/json/JSONException; {:try_start_1a .. :try_end_37} :catch_38

    goto :goto_3e

    :catch_38
    move-exception v4

    const-string v0, "Unable to dispatch sendMessageToNativeJsevent"

    invoke-static {v0, v4}, Lcom/google/android/gms/internal/zzin;->zzb(Ljava/lang/String;Ljava/lang/Throwable;)V

    :goto_3e
    return-void
.end method

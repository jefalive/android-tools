.class public final Lbr/com/itau/sdk/android/core/model/ResponseDTO;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private data:Lcom/google/gson/JsonElement;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "data"
    .end annotation
.end field

.field private flow:Ljava/util/List;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "flow"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Lbr/com/itau/sdk/android/core/model/FlowDTO;>;"
        }
    .end annotation
.end field

.field private header:Ljava/util/List;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "header"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Lbr/com/itau/sdk/android/core/model/KeyValue;>;"
        }
    .end annotation
.end field

.field private sdkData:Lbr/com/itau/sdk/android/core/model/SdkDataDTO;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "sdkData"
    .end annotation
.end field

.field private securityData:Lbr/com/itau/sdk/android/core/model/SecurityDataDTO;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "securityData"
    .end annotation
.end field

.field private statusCode:I
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "statusCode"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getData()Lcom/google/gson/JsonElement;
    .registers 2

    .line 27
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/model/ResponseDTO;->data:Lcom/google/gson/JsonElement;

    return-object v0
.end method

.method public getFlow()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Lbr/com/itau/sdk/android/core/model/FlowDTO;>;"
        }
    .end annotation

    .line 35
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/model/ResponseDTO;->flow:Ljava/util/List;

    return-object v0
.end method

.method public getHeader()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Lbr/com/itau/sdk/android/core/model/KeyValue;>;"
        }
    .end annotation

    .line 43
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/model/ResponseDTO;->header:Ljava/util/List;

    return-object v0
.end method

.method public getSdkData()Lbr/com/itau/sdk/android/core/model/SdkDataDTO;
    .registers 2

    .line 47
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/model/ResponseDTO;->sdkData:Lbr/com/itau/sdk/android/core/model/SdkDataDTO;

    return-object v0
.end method

.method public getSecurityData()Lbr/com/itau/sdk/android/core/model/SecurityDataDTO;
    .registers 2

    .line 39
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/model/ResponseDTO;->securityData:Lbr/com/itau/sdk/android/core/model/SecurityDataDTO;

    return-object v0
.end method

.method public getStatusCode()I
    .registers 2

    .line 31
    iget v0, p0, Lbr/com/itau/sdk/android/core/model/ResponseDTO;->statusCode:I

    return v0
.end method

.class Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity$5;
.super Ljava/lang/Object;
.source "FaqSearchActivity.java"

# interfaces
.implements Landroid/widget/ExpandableListView$OnGroupExpandListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;->showChannels(Ljava/util/List;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;


# direct methods
.method constructor <init>(Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;)V
    .registers 2
    .param p1, "this$0"    # Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;

    .line 210
    iput-object p1, p0, Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity$5;->this$0:Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onGroupExpand(I)V
    .registers 4
    .param p1, "groupPosition"    # I

    .line 213
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity$5;->this$0:Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;

    # getter for: Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;->listChannels:Landroid/widget/ExpandableListView;
    invoke-static {v0}, Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;->access$600(Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;)Landroid/widget/ExpandableListView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ExpandableListView;->getChildCount()I

    move-result v0

    if-ge v1, v0, :cond_1e

    .line 214
    const/4 v0, -0x1

    if-eq p1, v0, :cond_1b

    if-eq p1, v1, :cond_1b

    .line 215
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity$5;->this$0:Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;

    # getter for: Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;->listChannels:Landroid/widget/ExpandableListView;
    invoke-static {v0}, Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;->access$600(Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;)Landroid/widget/ExpandableListView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/ExpandableListView;->collapseGroup(I)Z

    .line 213
    :cond_1b
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 218
    .end local v1    # "i":I
    :cond_1e
    return-void
.end method

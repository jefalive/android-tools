.class public final Lbr/com/itau/sdk/android/core/model/TokenRequestDTO;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private modoDesbloqueio:Ljava/lang/Boolean;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "modoDesbloqueio"
    .end annotation
.end field

.field private nextToken:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "nextToken"
    .end annotation
.end field

.field private token:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "token"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .registers 3

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lbr/com/itau/sdk/android/core/model/TokenRequestDTO;->token:Ljava/lang/String;

    .line 21
    iput-object p2, p0, Lbr/com/itau/sdk/android/core/model/TokenRequestDTO;->nextToken:Ljava/lang/String;

    .line 22
    return-void
.end method


# virtual methods
.method public getModoDesbloqueio()Ljava/lang/Boolean;
    .registers 2

    .line 41
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/model/TokenRequestDTO;->modoDesbloqueio:Ljava/lang/Boolean;

    return-object v0
.end method

.method public getNextToken()Ljava/lang/String;
    .registers 2

    .line 33
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/model/TokenRequestDTO;->nextToken:Ljava/lang/String;

    return-object v0
.end method

.method public getToken()Ljava/lang/String;
    .registers 2

    .line 25
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/model/TokenRequestDTO;->token:Ljava/lang/String;

    return-object v0
.end method

.method public setModoDesbloqueio(Ljava/lang/Boolean;)V
    .registers 2

    .line 45
    iput-object p1, p0, Lbr/com/itau/sdk/android/core/model/TokenRequestDTO;->modoDesbloqueio:Ljava/lang/Boolean;

    .line 46
    return-void
.end method

.method public setNextToken(Ljava/lang/String;)V
    .registers 2

    .line 37
    iput-object p1, p0, Lbr/com/itau/sdk/android/core/model/TokenRequestDTO;->nextToken:Ljava/lang/String;

    .line 38
    return-void
.end method

.method public setToken(Ljava/lang/String;)V
    .registers 2

    .line 29
    iput-object p1, p0, Lbr/com/itau/sdk/android/core/model/TokenRequestDTO;->token:Ljava/lang/String;

    .line 30
    return-void
.end method

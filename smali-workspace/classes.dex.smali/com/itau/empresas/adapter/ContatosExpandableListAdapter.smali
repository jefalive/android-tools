.class public Lcom/itau/empresas/adapter/ContatosExpandableListAdapter;
.super Landroid/widget/BaseExpandableListAdapter;
.source "ContatosExpandableListAdapter.java"


# instance fields
.field private context:Landroid/content/Context;

.field private listaDadosCabecalho:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Ljava/lang/String;>;"
        }
    .end annotation
.end field

.field private listaDadosItem:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<Ljava/lang/String;Ljava/util/List<Lcom/itau/empresas/ui/widgets/ContatosChildItem;>;>;"
        }
    .end annotation
.end field

.field private listaDescricaoContatos:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Ljava/lang/String;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/List;Ljava/util/HashMap;Ljava/util/List;)V
    .registers 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "listaDadosCabecalho"    # Ljava/util/List;
    .param p3, "listaDadosItem"    # Ljava/util/HashMap;
    .param p4, "listaDescricaoContatos"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Landroid/content/Context;Ljava/util/List<Ljava/lang/String;>;Ljava/util/HashMap<Ljava/lang/String;Ljava/util/List<Lcom/itau/empresas/ui/widgets/ContatosChildItem;>;>;Ljava/util/List<Ljava/lang/String;>;)V"
        }
    .end annotation

    .line 28
    invoke-direct {p0}, Landroid/widget/BaseExpandableListAdapter;-><init>()V

    .line 29
    iput-object p1, p0, Lcom/itau/empresas/adapter/ContatosExpandableListAdapter;->context:Landroid/content/Context;

    .line 30
    iput-object p2, p0, Lcom/itau/empresas/adapter/ContatosExpandableListAdapter;->listaDadosCabecalho:Ljava/util/List;

    .line 31
    iput-object p3, p0, Lcom/itau/empresas/adapter/ContatosExpandableListAdapter;->listaDadosItem:Ljava/util/HashMap;

    .line 32
    iput-object p4, p0, Lcom/itau/empresas/adapter/ContatosExpandableListAdapter;->listaDescricaoContatos:Ljava/util/List;

    .line 33
    return-void
.end method


# virtual methods
.method public getChild(II)Lcom/itau/empresas/ui/widgets/ContatosChildItem;
    .registers 5
    .param p1, "groupPosition"    # I
    .param p2, "childPosititon"    # I

    .line 37
    iget-object v0, p0, Lcom/itau/empresas/adapter/ContatosExpandableListAdapter;->listaDadosItem:Ljava/util/HashMap;

    iget-object v1, p0, Lcom/itau/empresas/adapter/ContatosExpandableListAdapter;->listaDadosCabecalho:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 38
    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/ui/widgets/ContatosChildItem;

    .line 37
    return-object v0
.end method

.method public bridge synthetic getChild(II)Ljava/lang/Object;
    .registers 4

    .line 19
    invoke-virtual {p0, p1, p2}, Lcom/itau/empresas/adapter/ContatosExpandableListAdapter;->getChild(II)Lcom/itau/empresas/ui/widgets/ContatosChildItem;

    move-result-object v0

    return-object v0
.end method

.method public getChildId(II)J
    .registers 5
    .param p1, "groupPosition"    # I
    .param p2, "childPosition"    # I

    .line 43
    int-to-long v0, p2

    return-wide v0
.end method

.method public getChildView(IIZLandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 18
    .param p1, "groupPosition"    # I
    .param p2, "childPosition"    # I
    .param p3, "isLastChild"    # Z
    .param p4, "convertView"    # Landroid/view/View;
    .param p5, "parent"    # Landroid/view/ViewGroup;

    .line 50
    invoke-virtual {p0, p1, p2}, Lcom/itau/empresas/adapter/ContatosExpandableListAdapter;->getChild(II)Lcom/itau/empresas/ui/widgets/ContatosChildItem;

    move-result-object v3

    .line 52
    .local v3, "subItems":Lcom/itau/empresas/ui/widgets/ContatosChildItem;
    if-nez p4, :cond_19

    .line 53
    iget-object v0, p0, Lcom/itau/empresas/adapter/ContatosExpandableListAdapter;->context:Landroid/content/Context;

    const-string v1, "layout_inflater"

    .line 54
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Landroid/view/LayoutInflater;

    .line 55
    .local v4, "infalInflater":Landroid/view/LayoutInflater;
    const v0, 0x7f0300fa

    const/4 v1, 0x0

    invoke-virtual {v4, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p4

    .line 58
    .line 59
    .end local v4    # "infalInflater":Landroid/view/LayoutInflater;
    :cond_19
    move-object/from16 v0, p4

    const v1, 0x7f0e0535

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Landroid/widget/TextView;

    .line 60
    .local v4, "tituloItemTelefones":Landroid/widget/TextView;
    invoke-virtual {v3}, Lcom/itau/empresas/ui/widgets/ContatosChildItem;->getNumeroTelefone()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 61
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/itau/empresas/adapter/ContatosExpandableListAdapter;->context:Landroid/content/Context;

    .line 62
    const v2, 0x7f0700b4

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 63
    invoke-virtual {v3}, Lcom/itau/empresas/ui/widgets/ContatosChildItem;->getNumeroTelefone()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/adapter/ContatosExpandableListAdapter;->context:Landroid/content/Context;

    .line 64
    const v2, 0x7f0700b6

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 62
    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 66
    .line 67
    move-object/from16 v0, p4

    const v1, 0x7f0e0536

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Landroid/widget/TextView;

    .line 68
    .local v5, "subTitulosTelefones":Landroid/widget/TextView;
    invoke-virtual {v3}, Lcom/itau/empresas/ui/widgets/ContatosChildItem;->getInformacoes()Ljava/lang/String;

    move-result-object v6

    .line 69
    .local v6, "informacoes":Ljava/lang/String;
    if-eqz v6, :cond_72

    invoke-virtual {v6}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_78

    .line 70
    :cond_72
    const/16 v0, 0x8

    invoke-virtual {v5, v0}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_7f

    .line 72
    :cond_78
    const/4 v0, 0x0

    invoke-virtual {v5, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 73
    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 76
    .line 77
    :goto_7f
    move-object/from16 v0, p4

    const v1, 0x7f0e0445

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Landroid/widget/TextView;

    .line 78
    .local v7, "horarioDeAtendimento":Landroid/widget/TextView;
    invoke-virtual {v3}, Lcom/itau/empresas/ui/widgets/ContatosChildItem;->getHorarioDeAtendimento()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 79
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/itau/empresas/adapter/ContatosExpandableListAdapter;->context:Landroid/content/Context;

    .line 81
    const v2, 0x7f0700a8

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 82
    invoke-virtual {v3}, Lcom/itau/empresas/ui/widgets/ContatosChildItem;->getHorarioDeAtendimento()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 80
    invoke-virtual {v7, v0}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 84
    move-object/from16 v0, p4

    const v1, 0x7f0e0538

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    .line 85
    .local v8, "linha":Landroid/view/View;
    move-object/from16 v0, p4

    const v1, 0x7f0e0539

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    .line 86
    .line 87
    .local v9, "linhaFinal":Landroid/view/View;
    move-object/from16 v0, p4

    const v1, 0x7f0e0534

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Landroid/widget/RelativeLayout;

    .line 88
    .line 89
    .local v10, "rlItemListaContatos":Landroid/widget/RelativeLayout;
    move-object/from16 v0, p4

    const v1, 0x7f0e0537

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v11, v0

    check-cast v11, Landroid/widget/LinearLayout;

    .line 91
    .local v11, "llHorarioAtendimento":Landroid/widget/LinearLayout;
    if-eqz p3, :cond_f2

    .line 92
    const/16 v0, 0x8

    invoke-virtual {v10, v0}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 93
    const/4 v0, 0x0

    invoke-virtual {v11, v0}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 94
    const/16 v0, 0x8

    invoke-virtual {v8, v0}, Landroid/view/View;->setVisibility(I)V

    .line 95
    const/4 v0, 0x0

    invoke-virtual {v9, v0}, Landroid/view/View;->setVisibility(I)V

    goto :goto_104

    .line 97
    :cond_f2
    const/4 v0, 0x0

    invoke-virtual {v10, v0}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 98
    const/16 v0, 0x8

    invoke-virtual {v11, v0}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 99
    const/4 v0, 0x0

    invoke-virtual {v8, v0}, Landroid/view/View;->setVisibility(I)V

    .line 100
    const/16 v0, 0x8

    invoke-virtual {v9, v0}, Landroid/view/View;->setVisibility(I)V

    .line 103
    :goto_104
    return-object p4
.end method

.method public getChildrenCount(I)I
    .registers 4
    .param p1, "groupPosition"    # I

    .line 108
    iget-object v0, p0, Lcom/itau/empresas/adapter/ContatosExpandableListAdapter;->listaDadosItem:Ljava/util/HashMap;

    iget-object v1, p0, Lcom/itau/empresas/adapter/ContatosExpandableListAdapter;->listaDadosCabecalho:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getGroup(I)Ljava/lang/Object;
    .registers 3
    .param p1, "groupPosition"    # I

    .line 113
    iget-object v0, p0, Lcom/itau/empresas/adapter/ContatosExpandableListAdapter;->listaDadosCabecalho:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getGroupCount()I
    .registers 2

    .line 118
    iget-object v0, p0, Lcom/itau/empresas/adapter/ContatosExpandableListAdapter;->listaDadosCabecalho:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getGroupId(I)J
    .registers 4
    .param p1, "groupPosition"    # I

    .line 123
    int-to-long v0, p1

    return-wide v0
.end method

.method public getGroupView(IZLandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 12
    .param p1, "groupPosition"    # I
    .param p2, "isExpanded"    # Z
    .param p3, "convertView"    # Landroid/view/View;
    .param p4, "parent"    # Landroid/view/ViewGroup;

    .line 129
    invoke-virtual {p0, p1}, Lcom/itau/empresas/adapter/ContatosExpandableListAdapter;->getGroup(I)Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Ljava/lang/String;

    .line 130
    .local v2, "titulo":Ljava/lang/String;
    iget-object v0, p0, Lcom/itau/empresas/adapter/ContatosExpandableListAdapter;->listaDescricaoContatos:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Ljava/lang/String;

    .line 132
    .local v3, "descricao":Ljava/lang/String;
    if-nez p3, :cond_25

    .line 133
    iget-object v0, p0, Lcom/itau/empresas/adapter/ContatosExpandableListAdapter;->context:Landroid/content/Context;

    const-string v1, "layout_inflater"

    .line 134
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Landroid/view/LayoutInflater;

    .line 135
    .local v4, "infalInflater":Landroid/view/LayoutInflater;
    const v0, 0x7f0300fb

    const/4 v1, 0x0

    invoke-virtual {v4, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p3

    .line 138
    .end local v4    # "infalInflater":Landroid/view/LayoutInflater;
    :cond_25
    const v0, 0x7f0e053c

    invoke-virtual {p3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Landroid/widget/ImageView;

    .line 139
    .local v4, "groupIndicator":Landroid/widget/ImageView;
    const v0, 0x7f0e053d

    invoke-virtual {p3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Landroid/widget/TextView;

    .line 140
    .local v5, "tituloContato":Landroid/widget/TextView;
    const v0, 0x7f0e053e

    invoke-virtual {p3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Landroid/widget/TextView;

    .line 141
    .local v6, "descricaoContato":Landroid/widget/TextView;
    if-eqz p2, :cond_49

    const v0, 0x7f020162

    goto :goto_4c

    :cond_49
    const v0, 0x7f020160

    :goto_4c
    invoke-virtual {v4, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 144
    invoke-virtual {v5, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 145
    invoke-virtual {v6, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 146
    iget-object v0, p0, Lcom/itau/empresas/adapter/ContatosExpandableListAdapter;->context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 147
    const v1, 0x7f07009a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 146
    invoke-virtual {v4, v0}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 149
    return-object p3
.end method

.method public hasStableIds()Z
    .registers 2

    .line 154
    const/4 v0, 0x0

    return v0
.end method

.method public isChildSelectable(II)Z
    .registers 4
    .param p1, "groupPosition"    # I
    .param p2, "childPosition"    # I

    .line 159
    const/4 v0, 0x1

    return v0
.end method

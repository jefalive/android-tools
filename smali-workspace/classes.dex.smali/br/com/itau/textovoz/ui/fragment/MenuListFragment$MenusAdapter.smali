.class Lbr/com/itau/textovoz/ui/fragment/MenuListFragment$MenusAdapter;
.super Landroid/support/v7/widget/RecyclerView$Adapter;
.source "MenuListFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lbr/com/itau/textovoz/ui/fragment/MenuListFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MenusAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/support/v7/widget/RecyclerView$Adapter<Lbr/com/itau/textovoz/ui/fragment/MenuListFragment$MenuHolder;>;"
    }
.end annotation


# instance fields
.field menus:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<Lbr/com/itau/textovoz/model/Menu;>;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lbr/com/itau/textovoz/ui/fragment/MenuListFragment;


# direct methods
.method constructor <init>(Lbr/com/itau/textovoz/ui/fragment/MenuListFragment;Ljava/util/ArrayList;)V
    .registers 3
    .param p2, "menus"    # Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/ArrayList<Lbr/com/itau/textovoz/model/Menu;>;)V"
        }
    .end annotation

    .line 130
    iput-object p1, p0, Lbr/com/itau/textovoz/ui/fragment/MenuListFragment$MenusAdapter;->this$0:Lbr/com/itau/textovoz/ui/fragment/MenuListFragment;

    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView$Adapter;-><init>()V

    .line 131
    iput-object p2, p0, Lbr/com/itau/textovoz/ui/fragment/MenuListFragment$MenusAdapter;->menus:Ljava/util/ArrayList;

    .line 132
    return-void
.end method


# virtual methods
.method public getItemCount()I
    .registers 2

    .line 151
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/fragment/MenuListFragment$MenusAdapter;->menus:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public bridge synthetic onBindViewHolder(Landroid/support/v7/widget/RecyclerView$ViewHolder;I)V
    .registers 4

    .line 127
    move-object v0, p1

    check-cast v0, Lbr/com/itau/textovoz/ui/fragment/MenuListFragment$MenuHolder;

    invoke-virtual {p0, v0, p2}, Lbr/com/itau/textovoz/ui/fragment/MenuListFragment$MenusAdapter;->onBindViewHolder(Lbr/com/itau/textovoz/ui/fragment/MenuListFragment$MenuHolder;I)V

    return-void
.end method

.method public onBindViewHolder(Lbr/com/itau/textovoz/ui/fragment/MenuListFragment$MenuHolder;I)V
    .registers 5
    .param p1, "holder"    # Lbr/com/itau/textovoz/ui/fragment/MenuListFragment$MenuHolder;
    .param p2, "position"    # I

    .line 144
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/fragment/MenuListFragment$MenusAdapter;->menus:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lbr/com/itau/textovoz/model/Menu;

    .line 146
    .local v1, "m":Lbr/com/itau/textovoz/model/Menu;
    invoke-virtual {p1, v1}, Lbr/com/itau/textovoz/ui/fragment/MenuListFragment$MenuHolder;->bindMenu(Lbr/com/itau/textovoz/model/Menu;)V

    .line 147
    return-void
.end method

.method public bridge synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .registers 4

    .line 127
    invoke-virtual {p0, p1, p2}, Lbr/com/itau/textovoz/ui/fragment/MenuListFragment$MenusAdapter;->onCreateViewHolder(Landroid/view/ViewGroup;I)Lbr/com/itau/textovoz/ui/fragment/MenuListFragment$MenuHolder;

    move-result-object v0

    return-object v0
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Lbr/com/itau/textovoz/ui/fragment/MenuListFragment$MenuHolder;
    .registers 7
    .param p1, "parent"    # Landroid/view/ViewGroup;
    .param p2, "viewType"    # I

    .line 136
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/fragment/MenuListFragment$MenusAdapter;->this$0:Lbr/com/itau/textovoz/ui/fragment/MenuListFragment;

    invoke-virtual {v0}, Lbr/com/itau/textovoz/ui/fragment/MenuListFragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    .line 137
    .local v2, "inflater":Landroid/view/LayoutInflater;
    sget v0, Lbr/com/itau/textovoz/R$layout;->search_details_row:I

    const/4 v1, 0x0

    invoke-virtual {v2, v0, p1, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    .line 139
    .local v3, "v":Landroid/view/View;
    new-instance v0, Lbr/com/itau/textovoz/ui/fragment/MenuListFragment$MenuHolder;

    iget-object v1, p0, Lbr/com/itau/textovoz/ui/fragment/MenuListFragment$MenusAdapter;->this$0:Lbr/com/itau/textovoz/ui/fragment/MenuListFragment;

    invoke-direct {v0, v1, v3}, Lbr/com/itau/textovoz/ui/fragment/MenuListFragment$MenuHolder;-><init>(Lbr/com/itau/textovoz/ui/fragment/MenuListFragment;Landroid/view/View;)V

    return-object v0
.end method

.class public Lcom/itau/empresas/feature/credito/produtos/adapter/LimitesProdutosListAdapter;
.super Landroid/widget/BaseAdapter;
.source "LimitesProdutosListAdapter.java"


# instance fields
.field private context:Landroid/content/Context;

.field private itens:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Lcom/itau/empresas/feature/credito/produtos/model/ProdutoChildItem;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 21
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public getCount()I
    .registers 2

    .line 40
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/adapter/LimitesProdutosListAdapter;->itens:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Lcom/itau/empresas/feature/credito/produtos/model/ProdutoChildItem;
    .registers 3
    .param p1, "position"    # I

    .line 45
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/adapter/LimitesProdutosListAdapter;->itens:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/feature/credito/produtos/model/ProdutoChildItem;

    return-object v0
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .registers 3

    .line 20
    invoke-virtual {p0, p1}, Lcom/itau/empresas/feature/credito/produtos/adapter/LimitesProdutosListAdapter;->getItem(I)Lcom/itau/empresas/feature/credito/produtos/model/ProdutoChildItem;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .registers 4
    .param p1, "position"    # I

    .line 50
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 12
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;
    .annotation build Landroid/annotation/TargetApi;
        value = 0x13
    .end annotation

    .line 56
    invoke-virtual {p0, p1}, Lcom/itau/empresas/feature/credito/produtos/adapter/LimitesProdutosListAdapter;->getItem(I)Lcom/itau/empresas/feature/credito/produtos/model/ProdutoChildItem;

    move-result-object v2

    .line 57
    .local v2, "item":Lcom/itau/empresas/feature/credito/produtos/model/ProdutoChildItem;
    move-object v3, p2

    .line 58
    .local v3, "view":Landroid/view/View;
    if-nez v3, :cond_d

    .line 59
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/adapter/LimitesProdutosListAdapter;->context:Landroid/content/Context;

    invoke-static {v0}, Lcom/itau/empresas/feature/credito/produtos/view/ProdutosLimitesContratadosItemView_;->build(Landroid/content/Context;)Lcom/itau/empresas/feature/credito/produtos/view/ProdutosLimitesContratadosItemView;

    move-result-object v3

    .line 62
    :cond_d
    move-object v0, v3

    check-cast v0, Lcom/itau/empresas/feature/credito/produtos/view/ProdutosLimitesContratadosItemView;

    iget-object v4, v0, Lcom/itau/empresas/feature/credito/produtos/view/ProdutosLimitesContratadosItemView;->textoTipo:Landroid/widget/TextView;

    .line 63
    .local v4, "textoTipo":Landroid/widget/TextView;
    move-object v0, v3

    check-cast v0, Lcom/itau/empresas/feature/credito/produtos/view/ProdutosLimitesContratadosItemView;

    iget-object v5, v0, Lcom/itau/empresas/feature/credito/produtos/view/ProdutosLimitesContratadosItemView;->textoValor:Landroid/widget/TextView;

    .line 65
    .local v5, "textoValor":Landroid/widget/TextView;
    invoke-virtual {v2}, Lcom/itau/empresas/feature/credito/produtos/model/ProdutoChildItem;->getDescricao()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 66
    invoke-virtual {v2}, Lcom/itau/empresas/feature/credito/produtos/model/ProdutoChildItem;->getValor()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_46

    invoke-virtual {v2}, Lcom/itau/empresas/feature/credito/produtos/model/ProdutoChildItem;->getValor()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_46

    .line 67
    .line 68
    invoke-virtual {v2}, Lcom/itau/empresas/feature/credito/produtos/model/ProdutoChildItem;->getValor()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/itau/empresas/ui/util/ValorMonetarioUtils;->formataValorDecimalParaMonetario(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v6

    .line 69
    .line 70
    .local v6, "valorFormatado":Ljava/lang/String;
    invoke-virtual {v2}, Lcom/itau/empresas/feature/credito/produtos/model/ProdutoChildItem;->getValor()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/itau/empresas/ui/util/ValorMonetarioUtils;->formataValorDecimalParaMonetario(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v7

    .line 71
    .local v7, "valorFormatadoAcessibilidade":Ljava/lang/String;
    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 72
    invoke-virtual {v5, v7}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 75
    .end local v6    # "valorFormatado":Ljava/lang/String;
    .end local v7    # "valorFormatadoAcessibilidade":Ljava/lang/String;
    :cond_46
    return-object v3
.end method

.method public setContext(Landroid/content/Context;)V
    .registers 2
    .param p1, "context"    # Landroid/content/Context;

    .line 35
    iput-object p1, p0, Lcom/itau/empresas/feature/credito/produtos/adapter/LimitesProdutosListAdapter;->context:Landroid/content/Context;

    .line 36
    return-void
.end method

.method public setData(Ljava/util/List;)V
    .registers 2
    .param p1, "itens"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<Lcom/itau/empresas/feature/credito/produtos/model/ProdutoChildItem;>;)V"
        }
    .end annotation

    .line 27
    iput-object p1, p0, Lcom/itau/empresas/feature/credito/produtos/adapter/LimitesProdutosListAdapter;->itens:Ljava/util/List;

    .line 28
    return-void
.end method

.class public final enum Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity$Acao;
.super Ljava/lang/Enum;
.source "TransferenciaRecebidasDetalheActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Acao"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity$Acao;>;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity$Acao;

.field public static final enum COMPARTILHAR:Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity$Acao;

.field public static final enum SALVAR:Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity$Acao;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .line 196
    new-instance v0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity$Acao;

    const-string v1, "SALVAR"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity$Acao;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity$Acao;->SALVAR:Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity$Acao;

    new-instance v0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity$Acao;

    const-string v1, "COMPARTILHAR"

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity$Acao;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity$Acao;->COMPARTILHAR:Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity$Acao;

    .line 195
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity$Acao;

    sget-object v1, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity$Acao;->SALVAR:Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity$Acao;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sget-object v1, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity$Acao;->COMPARTILHAR:Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity$Acao;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    sput-object v0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity$Acao;->$VALUES:[Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity$Acao;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 195
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity$Acao;
    .registers 2
    .param p0, "name"    # Ljava/lang/String;

    .line 195
    const-class v0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity$Acao;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity$Acao;

    return-object v0
.end method

.method public static values()[Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity$Acao;
    .registers 1

    .line 195
    sget-object v0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity$Acao;->$VALUES:[Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity$Acao;

    invoke-virtual {v0}, [Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity$Acao;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity$Acao;

    return-object v0
.end method

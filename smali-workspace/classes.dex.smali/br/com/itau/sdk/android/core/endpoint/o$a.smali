.class public final Lbr/com/itau/sdk/android/core/endpoint/o$a;
.super Lokio/ForwardingSink;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lbr/com/itau/sdk/android/core/endpoint/o;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x14
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Lbr/com/itau/sdk/android/core/endpoint/o;

.field private b:J


# direct methods
.method public constructor <init>(Lbr/com/itau/sdk/android/core/endpoint/o;Lokio/Sink;)V
    .registers 5

    .line 52
    iput-object p1, p0, Lbr/com/itau/sdk/android/core/endpoint/o$a;->a:Lbr/com/itau/sdk/android/core/endpoint/o;

    .line 53
    invoke-direct {p0, p2}, Lokio/ForwardingSink;-><init>(Lokio/Sink;)V

    .line 50
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lbr/com/itau/sdk/android/core/endpoint/o$a;->b:J

    .line 54
    return-void
.end method


# virtual methods
.method public write(Lokio/Buffer;J)V
    .registers 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 58
    invoke-super {p0, p1, p2, p3}, Lokio/ForwardingSink;->write(Lokio/Buffer;J)V

    .line 60
    iget-wide v0, p0, Lbr/com/itau/sdk/android/core/endpoint/o$a;->b:J

    add-long/2addr v0, p2

    iput-wide v0, p0, Lbr/com/itau/sdk/android/core/endpoint/o$a;->b:J

    .line 61
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/endpoint/o$a;->a:Lbr/com/itau/sdk/android/core/endpoint/o;

    iget-object v0, v0, Lbr/com/itau/sdk/android/core/endpoint/o;->b:Lbr/com/itau/sdk/android/core/SDKProgressListener;

    iget-wide v1, p0, Lbr/com/itau/sdk/android/core/endpoint/o$a;->b:J

    iget-object v3, p0, Lbr/com/itau/sdk/android/core/endpoint/o$a;->a:Lbr/com/itau/sdk/android/core/endpoint/o;

    invoke-virtual {v3}, Lbr/com/itau/sdk/android/core/endpoint/o;->contentLength()J

    move-result-wide v3

    invoke-interface {v0, v1, v2, v3, v4}, Lbr/com/itau/sdk/android/core/SDKProgressListener;->onRequestProgress(JJ)V

    .line 62
    return-void
.end method

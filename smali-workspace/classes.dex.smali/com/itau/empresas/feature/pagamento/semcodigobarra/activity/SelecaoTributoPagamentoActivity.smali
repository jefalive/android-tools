.class public Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/SelecaoTributoPagamentoActivity;
.super Lcom/itau/empresas/ui/activity/BaseActivity;
.source "SelecaoTributoPagamentoActivity.java"


# instance fields
.field adapter:Lcom/itau/empresas/feature/pagamento/semcodigobarra/adapter/TributosAdapter;

.field app:Lcom/itau/empresas/CustomApplication;

.field itemTributosList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/ItemTributos;>;"
        }
    .end annotation
.end field

.field listaTributos:Landroid/widget/ListView;

.field toolBar:Landroid/support/v7/widget/Toolbar;


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 29
    invoke-direct {p0}, Lcom/itau/empresas/ui/activity/BaseActivity;-><init>()V

    return-void
.end method

.method private buildAdapter()V
    .registers 3

    .line 70
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/SelecaoTributoPagamentoActivity;->itemTributosList:Ljava/util/ArrayList;

    if-nez v0, :cond_7

    .line 71
    invoke-direct {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/SelecaoTributoPagamentoActivity;->montaListaItensExibicao()V

    .line 73
    :cond_7
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/SelecaoTributoPagamentoActivity;->adapter:Lcom/itau/empresas/feature/pagamento/semcodigobarra/adapter/TributosAdapter;

    iget-object v1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/SelecaoTributoPagamentoActivity;->itemTributosList:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/adapter/TributosAdapter;->setListaItensTributos(Ljava/util/List;)V

    .line 74
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/SelecaoTributoPagamentoActivity;->listaTributos:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/SelecaoTributoPagamentoActivity;->adapter:Lcom/itau/empresas/feature/pagamento/semcodigobarra/adapter/TributosAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 75
    return-void
.end method

.method private constroiToolbar()V
    .registers 3

    .line 63
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/SelecaoTributoPagamentoActivity;->toolBar:Landroid/support/v7/widget/Toolbar;

    invoke-static {v0, p0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom;->Builder(Landroid/support/v7/widget/Toolbar;Landroid/support/v7/app/AppCompatActivity;)Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    move-result-object v0

    .line 64
    const v1, 0x7f0706b7

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->comTitulo(I)Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    move-result-object v0

    .line 65
    invoke-virtual {v0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->comClickListenerFinish()Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    move-result-object v0

    .line 66
    invoke-virtual {v0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->build()Lcom/itau/empresas/ui/util/listener/ToolbarCustom;

    .line 67
    return-void
.end method

.method public static intent(Landroid/content/Context;)Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/SelecaoTributoPagamentoActivity_$IntentBuilder_;
    .registers 2
    .param p0, "context"    # Landroid/content/Context;

    .line 95
    new-instance v0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/SelecaoTributoPagamentoActivity_$IntentBuilder_;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/SelecaoTributoPagamentoActivity_$IntentBuilder_;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method private montaListaItensExibicao()V
    .registers 7

    .line 79
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/SelecaoTributoPagamentoActivity;->itemTributosList:Ljava/util/ArrayList;

    .line 80
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/SelecaoTributoPagamentoActivity;->app:Lcom/itau/empresas/CustomApplication;

    if-eqz v0, :cond_6d

    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/SelecaoTributoPagamentoActivity;->app:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getPermissaoAutorizante()Lcom/itau/empresas/api/model/MenuVO;

    move-result-object v0

    if-eqz v0, :cond_6d

    .line 81
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/SelecaoTributoPagamentoActivity;->app:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getPermissaoAutorizante()Lcom/itau/empresas/api/model/MenuVO;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/api/model/MenuVO;->getFilhos()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_21
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6d

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/itau/empresas/api/model/MenuVO;

    .line 82
    .local v5, "menuVO":Lcom/itau/empresas/api/model/MenuVO;
    invoke-virtual {v5}, Lcom/itau/empresas/api/model/MenuVO;->getNome()Ljava/lang/String;

    move-result-object v0

    const-string v1, "exibicao_pagamento_darf"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4d

    .line 83
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/SelecaoTributoPagamentoActivity;->itemTributosList:Ljava/util/ArrayList;

    new-instance v1, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/ItemTributos;

    const v2, 0x7f0706a2

    invoke-virtual {p0, v2}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/SelecaoTributoPagamentoActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-class v3, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity_;

    invoke-direct {v1, v2, v3}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/ItemTributos;-><init>(Ljava/lang/String;Ljava/lang/Class;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 86
    :cond_4d
    invoke-virtual {v5}, Lcom/itau/empresas/api/model/MenuVO;->getNome()Ljava/lang/String;

    move-result-object v0

    const-string v1, "exibicao_pagamento_gps"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6c

    .line 87
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/SelecaoTributoPagamentoActivity;->itemTributosList:Ljava/util/ArrayList;

    new-instance v1, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/ItemTributos;

    const v2, 0x7f070345

    invoke-virtual {p0, v2}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/SelecaoTributoPagamentoActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-class v3, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy_;

    invoke-direct {v1, v2, v3}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/ItemTributos;-><init>(Ljava/lang/String;Ljava/lang/Class;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 90
    .end local v5    # "menuVO":Lcom/itau/empresas/api/model/MenuVO;
    :cond_6c
    goto :goto_21

    .line 92
    :cond_6d
    return-void
.end method


# virtual methods
.method abreActivitySelecionada(I)V
    .registers 4
    .param p1, "position"    # I

    .line 59
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/SelecaoTributoPagamentoActivity;->itemTributosList:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/ItemTributos;

    invoke-virtual {v1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/ItemTributos;->getActivity()Ljava/lang/Class;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/SelecaoTributoPagamentoActivity;->startActivity(Landroid/content/Intent;)V

    .line 60
    return-void
.end method

.method carregaElemetosTela()V
    .registers 1

    .line 53
    invoke-direct {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/SelecaoTributoPagamentoActivity;->constroiToolbar()V

    .line 54
    invoke-direct {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/SelecaoTributoPagamentoActivity;->buildAdapter()V

    .line 55
    return-void
.end method

.method public opkeysConsumidas()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Ljava/lang/String;>;"
        }
    .end annotation

    .line 48
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

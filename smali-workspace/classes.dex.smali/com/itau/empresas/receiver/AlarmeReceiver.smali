.class public Lcom/itau/empresas/receiver/AlarmeReceiver;
.super Landroid/content/BroadcastReceiver;
.source "AlarmeReceiver.java"


# instance fields
.field app:Lcom/itau/empresas/CustomApplication;


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 21
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method private calculaTempoRestante(J)J
    .registers 8
    .param p1, "datetime"    # J

    .line 134
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    .line 136
    .local v2, "dataAtual":Ljava/util/Calendar;
    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v3

    .line 137
    .local v3, "atual":J
    sub-long v0, v3, p1

    return-wide v0
.end method

.method private podeLancarNotificacaoToken()Z
    .registers 2

    .line 141
    invoke-static {}, Lbr/com/itau/sdk/android/core/BackendClient;->getOfflineOTP()Lbr/com/itau/security/token/model/OTPModel;

    move-result-object v0

    if-nez v0, :cond_8

    const/4 v0, 0x1

    goto :goto_9

    :cond_8
    const/4 v0, 0x0

    :goto_9
    return v0
.end method

.method private reagendarAlarmeExtrato(Landroid/content/Context;)V
    .registers 14
    .param p1, "context"    # Landroid/content/Context;

    .line 166
    const-string v0, "extrato_key"

    iget-object v1, p0, Lcom/itau/empresas/receiver/AlarmeReceiver;->app:Lcom/itau/empresas/CustomApplication;

    .line 167
    invoke-static {v0, v1}, Lcom/itau/empresas/AlarmeUtils;->buscarDatetimeAlarme(Ljava/lang/String;Lcom/itau/empresas/CustomApplication;)J

    move-result-wide v6

    .line 168
    .local v6, "datetimeExtrato":J
    invoke-direct {p0, v6, v7}, Lcom/itau/empresas/receiver/AlarmeReceiver;->calculaTempoRestante(J)J

    move-result-wide v8

    .line 171
    .local v8, "tempoRestanteExtrato":J
    const-wide/16 v0, 0x0

    cmp-long v0, v8, v0

    if-lez v0, :cond_14

    .line 172
    move-wide v10, v8

    .local v10, "time":J
    goto :goto_17

    .line 174
    .end local v10    # "time":J
    :cond_14
    const-wide/32 v10, 0x337f9800

    .line 176
    .local v10, "time":J
    :goto_17
    move-object v0, p1

    move-wide v4, v10

    const/4 v1, 0x2

    const-wide/32 v2, 0x337f9800

    invoke-static/range {v0 .. v5}, Lcom/itau/empresas/AlarmeUtils;->agendarAlarmeComRepeticao(Landroid/content/Context;IJJ)V

    .line 178
    const-string v0, "extrato_key"

    iget-object v1, p0, Lcom/itau/empresas/receiver/AlarmeReceiver;->app:Lcom/itau/empresas/CustomApplication;

    invoke-static {v0, v10, v11, v1}, Lcom/itau/empresas/AlarmeUtils;->salvarDatetimeAlarme(Ljava/lang/String;JLcom/itau/empresas/CustomApplication;)V

    .line 179
    return-void
.end method

.method private reagendarAlarmeLogin(Landroid/content/Context;)V
    .registers 10
    .param p1, "context"    # Landroid/content/Context;

    .line 183
    const-string v0, "login_key"

    iget-object v1, p0, Lcom/itau/empresas/receiver/AlarmeReceiver;->app:Lcom/itau/empresas/CustomApplication;

    invoke-static {v0, v1}, Lcom/itau/empresas/AlarmeUtils;->buscarDatetimeAlarme(Ljava/lang/String;Lcom/itau/empresas/CustomApplication;)J

    move-result-wide v2

    .line 184
    .local v2, "datetimeLogin":J
    invoke-direct {p0, v2, v3}, Lcom/itau/empresas/receiver/AlarmeReceiver;->calculaTempoRestante(J)J

    move-result-wide v4

    .line 187
    .local v4, "tempoRestanteLogin":J
    const-wide/16 v0, 0x0

    cmp-long v0, v2, v0

    if-nez v0, :cond_13

    .line 188
    return-void

    .line 191
    :cond_13
    const-wide/16 v0, 0x0

    cmp-long v0, v4, v0

    if-lez v0, :cond_1b

    .line 192
    move-wide v6, v4

    .local v6, "time":J
    goto :goto_1e

    .line 194
    .end local v6    # "time":J
    :cond_1b
    const-wide/32 v6, 0xa4cb800

    .line 196
    .local v6, "time":J
    :goto_1e
    const/4 v0, 0x3

    invoke-static {p1, v0, v6, v7}, Lcom/itau/empresas/AlarmeUtils;->agendarAlarmeUnico(Landroid/content/Context;IJ)V

    .line 197
    const-string v0, "login_key"

    iget-object v1, p0, Lcom/itau/empresas/receiver/AlarmeReceiver;->app:Lcom/itau/empresas/CustomApplication;

    invoke-static {v0, v6, v7, v1}, Lcom/itau/empresas/AlarmeUtils;->salvarDatetimeAlarme(Ljava/lang/String;JLcom/itau/empresas/CustomApplication;)V

    .line 198
    return-void
.end method

.method private reagendarAlarmeToken(Landroid/content/Context;)V
    .registers 10
    .param p1, "context"    # Landroid/content/Context;

    .line 145
    const-string v0, "instalar_token_key"

    iget-object v1, p0, Lcom/itau/empresas/receiver/AlarmeReceiver;->app:Lcom/itau/empresas/CustomApplication;

    .line 146
    invoke-static {v0, v1}, Lcom/itau/empresas/AlarmeUtils;->buscarDatetimeAlarme(Ljava/lang/String;Lcom/itau/empresas/CustomApplication;)J

    move-result-wide v2

    .line 147
    .local v2, "datetimeInstalarToken":J
    invoke-direct {p0, v2, v3}, Lcom/itau/empresas/receiver/AlarmeReceiver;->calculaTempoRestante(J)J

    move-result-wide v4

    .line 150
    .local v4, "tempoRestanteToken":J
    const-wide/16 v0, 0x0

    cmp-long v0, v2, v0

    if-eqz v0, :cond_18

    invoke-direct {p0}, Lcom/itau/empresas/receiver/AlarmeReceiver;->podeLancarNotificacaoToken()Z

    move-result v0

    if-nez v0, :cond_19

    .line 151
    :cond_18
    return-void

    .line 154
    :cond_19
    const-wide/16 v0, 0x0

    cmp-long v0, v4, v0

    if-lez v0, :cond_21

    .line 155
    move-wide v6, v4

    .local v6, "time":J
    goto :goto_24

    .line 157
    .end local v6    # "time":J
    :cond_21
    const-wide/32 v6, 0xf731400

    .line 160
    .local v6, "time":J
    :goto_24
    const/4 v0, 0x1

    invoke-static {p1, v0, v6, v7}, Lcom/itau/empresas/AlarmeUtils;->agendarAlarmeUnico(Landroid/content/Context;IJ)V

    .line 161
    const-string v0, "instalar_token_key"

    iget-object v1, p0, Lcom/itau/empresas/receiver/AlarmeReceiver;->app:Lcom/itau/empresas/CustomApplication;

    invoke-static {v0, v6, v7, v1}, Lcom/itau/empresas/AlarmeUtils;->salvarDatetimeAlarme(Ljava/lang/String;JLcom/itau/empresas/CustomApplication;)V

    .line 162
    return-void
.end method

.method private reagendarAlarmes(Landroid/content/Context;)V
    .registers 2
    .param p1, "context"    # Landroid/content/Context;

    .line 127
    invoke-direct {p0, p1}, Lcom/itau/empresas/receiver/AlarmeReceiver;->reagendarAlarmeToken(Landroid/content/Context;)V

    .line 128
    invoke-direct {p0, p1}, Lcom/itau/empresas/receiver/AlarmeReceiver;->reagendarAlarmeExtrato(Landroid/content/Context;)V

    .line 129
    invoke-direct {p0, p1}, Lcom/itau/empresas/receiver/AlarmeReceiver;->reagendarAlarmeLogin(Landroid/content/Context;)V

    .line 130
    invoke-static {p1}, Lcom/itau/empresas/NotificacaoUtils;->configurarNotificacaoFimAno(Landroid/content/Context;)V

    .line 131
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 11
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .line 29
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v4

    .line 31
    .local v4, "extras":Landroid/os/Bundle;
    if-eqz v4, :cond_10d

    .line 34
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1d

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.intent.action.BOOT_COMPLETED"

    .line 35
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1d

    .line 36
    invoke-direct {p0, p1}, Lcom/itau/empresas/receiver/AlarmeReceiver;->reagendarAlarmes(Landroid/content/Context;)V

    goto/16 :goto_10d

    .line 39
    :cond_1d
    const-string v0, "alarmeNotification"

    const/4 v1, -0x1

    invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v5

    .line 41
    .local v5, "alarmeExtra":I
    packed-switch v5, :pswitch_data_10e

    goto/16 :goto_10d

    .line 43
    :pswitch_29
    const-string v0, "instalar_token_key"

    iget-object v1, p0, Lcom/itau/empresas/receiver/AlarmeReceiver;->app:Lcom/itau/empresas/CustomApplication;

    .line 44
    invoke-static {v0, v1}, Lcom/itau/empresas/AlarmeUtils;->buscarDatetimeAlarme(Ljava/lang/String;Lcom/itau/empresas/CustomApplication;)J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_10d

    .line 46
    .line 48
    const v0, 0x7f070439

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/receiver/AlarmeReceiver;->app:Lcom/itau/empresas/CustomApplication;

    .line 46
    const/4 v2, 0x1

    invoke-static {p1, v2, v0, v1}, Lcom/itau/empresas/NotificacaoUtils;->notificacaoSimples(Landroid/content/Context;ILjava/lang/String;Lcom/itau/empresas/CustomApplication;)V

    .line 51
    const-string v0, "instalar_token_key"

    iget-object v1, p0, Lcom/itau/empresas/receiver/AlarmeReceiver;->app:Lcom/itau/empresas/CustomApplication;

    invoke-static {v0, v1}, Lcom/itau/empresas/AlarmeUtils;->zerarDatetimeAlarme(Ljava/lang/String;Lcom/itau/empresas/CustomApplication;)V

    goto/16 :goto_10d

    .line 57
    .line 59
    :pswitch_4d
    const v0, 0x7f070438

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/receiver/AlarmeReceiver;->app:Lcom/itau/empresas/CustomApplication;

    .line 58
    const/4 v2, 0x2

    invoke-static {p1, v2, v0, v1}, Lcom/itau/empresas/NotificacaoUtils;->notificacaoSimples(Landroid/content/Context;ILjava/lang/String;Lcom/itau/empresas/CustomApplication;)V

    .line 63
    const-string v0, "extrato_key"

    iget-object v1, p0, Lcom/itau/empresas/receiver/AlarmeReceiver;->app:Lcom/itau/empresas/CustomApplication;

    const-wide/32 v2, 0x337f9800

    invoke-static {v0, v2, v3, v1}, Lcom/itau/empresas/AlarmeUtils;->salvarDatetimeAlarme(Ljava/lang/String;JLcom/itau/empresas/CustomApplication;)V

    .line 65
    goto/16 :goto_10d

    .line 68
    :pswitch_66
    const-string v0, "login_key"

    iget-object v1, p0, Lcom/itau/empresas/receiver/AlarmeReceiver;->app:Lcom/itau/empresas/CustomApplication;

    invoke-static {v0, v1}, Lcom/itau/empresas/AlarmeUtils;->buscarDatetimeAlarme(Ljava/lang/String;Lcom/itau/empresas/CustomApplication;)J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_10d

    .line 70
    .line 72
    const v0, 0x7f070437

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/receiver/AlarmeReceiver;->app:Lcom/itau/empresas/CustomApplication;

    .line 71
    const/4 v2, 0x3

    invoke-static {p1, v2, v0, v1}, Lcom/itau/empresas/NotificacaoUtils;->notificacaoSimples(Landroid/content/Context;ILjava/lang/String;Lcom/itau/empresas/CustomApplication;)V

    .line 75
    const-string v0, "login_key"

    iget-object v1, p0, Lcom/itau/empresas/receiver/AlarmeReceiver;->app:Lcom/itau/empresas/CustomApplication;

    invoke-static {v0, v1}, Lcom/itau/empresas/AlarmeUtils;->zerarDatetimeAlarme(Ljava/lang/String;Lcom/itau/empresas/CustomApplication;)V

    goto/16 :goto_10d

    .line 81
    :pswitch_8a
    const-string v6, "\ud83c\udf84"

    .line 83
    .local v6, "emojiUnicodeNatal":Ljava/lang/String;
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x17

    if-lt v0, v1, :cond_b7

    .line 84
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 87
    const v1, 0x7f07043a

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/receiver/AlarmeReceiver;->app:Lcom/itau/empresas/CustomApplication;

    .line 85
    const/4 v2, 0x4

    invoke-static {p1, v2, v0, v1}, Lcom/itau/empresas/NotificacaoUtils;->notificacaoSimples(Landroid/content/Context;ILjava/lang/String;Lcom/itau/empresas/CustomApplication;)V

    goto :goto_c4

    .line 90
    .line 92
    :cond_b7
    const v0, 0x7f07043a

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/receiver/AlarmeReceiver;->app:Lcom/itau/empresas/CustomApplication;

    .line 91
    const/4 v2, 0x4

    invoke-static {p1, v2, v0, v1}, Lcom/itau/empresas/NotificacaoUtils;->notificacaoSimples(Landroid/content/Context;ILjava/lang/String;Lcom/itau/empresas/CustomApplication;)V

    .line 96
    :goto_c4
    const-string v0, "natal_key"

    iget-object v1, p0, Lcom/itau/empresas/receiver/AlarmeReceiver;->app:Lcom/itau/empresas/CustomApplication;

    invoke-static {v0, v1}, Lcom/itau/empresas/AlarmeUtils;->zerarDatetimeAlarme(Ljava/lang/String;Lcom/itau/empresas/CustomApplication;)V

    .line 97
    goto :goto_10d

    .line 101
    .end local v6    # "emojiUnicodeNatal":Ljava/lang/String;
    :pswitch_cc
    const-string v7, "\ud83c\udf89"

    .line 103
    .local v7, "emojiUnicodeAnoNovo":Ljava/lang/String;
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x17

    if-lt v0, v1, :cond_f9

    .line 104
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 107
    const v1, 0x7f070436

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/receiver/AlarmeReceiver;->app:Lcom/itau/empresas/CustomApplication;

    .line 105
    const/4 v2, 0x5

    invoke-static {p1, v2, v0, v1}, Lcom/itau/empresas/NotificacaoUtils;->notificacaoSimples(Landroid/content/Context;ILjava/lang/String;Lcom/itau/empresas/CustomApplication;)V

    goto :goto_106

    .line 111
    .line 113
    :cond_f9
    const v0, 0x7f070436

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/receiver/AlarmeReceiver;->app:Lcom/itau/empresas/CustomApplication;

    .line 112
    const/4 v2, 0x5

    invoke-static {p1, v2, v0, v1}, Lcom/itau/empresas/NotificacaoUtils;->notificacaoSimples(Landroid/content/Context;ILjava/lang/String;Lcom/itau/empresas/CustomApplication;)V

    .line 117
    :goto_106
    const-string v0, "ano_novo_key"

    iget-object v1, p0, Lcom/itau/empresas/receiver/AlarmeReceiver;->app:Lcom/itau/empresas/CustomApplication;

    invoke-static {v0, v1}, Lcom/itau/empresas/AlarmeUtils;->zerarDatetimeAlarme(Ljava/lang/String;Lcom/itau/empresas/CustomApplication;)V

    .line 122
    .end local v5    # "alarmeExtra":I
    .end local v7    # "emojiUnicodeAnoNovo":Ljava/lang/String;
    :cond_10d
    :goto_10d
    return-void

    :pswitch_data_10e
    .packed-switch 0x1
        :pswitch_29
        :pswitch_4d
        :pswitch_66
        :pswitch_8a
        :pswitch_cc
    .end packed-switch
.end method

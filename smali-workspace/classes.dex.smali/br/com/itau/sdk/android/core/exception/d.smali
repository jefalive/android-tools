.class Lbr/com/itau/sdk/android/core/exception/d;
.super Lokio/ForwardingSource;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lbr/com/itau/sdk/android/core/exception/c;


# direct methods
.method constructor <init>(Lbr/com/itau/sdk/android/core/exception/c;Lokio/Source;)V
    .registers 3

    .line 37
    iput-object p1, p0, Lbr/com/itau/sdk/android/core/exception/d;->a:Lbr/com/itau/sdk/android/core/exception/c;

    invoke-direct {p0, p2}, Lokio/ForwardingSource;-><init>(Lokio/Source;)V

    return-void
.end method


# virtual methods
.method public read(Lokio/Buffer;J)J
    .registers 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 41
    :try_start_0
    invoke-super {p0, p1, p2, p3}, Lokio/ForwardingSource;->read(Lokio/Buffer;J)J
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_3} :catch_5

    move-result-wide v0

    return-wide v0

    .line 42
    :catch_5
    move-exception v2

    .line 43
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/exception/d;->a:Lbr/com/itau/sdk/android/core/exception/c;

    invoke-static {v0, v2}, Lbr/com/itau/sdk/android/core/exception/c;->a(Lbr/com/itau/sdk/android/core/exception/c;Ljava/io/IOException;)Ljava/io/IOException;

    .line 44
    throw v2
.end method

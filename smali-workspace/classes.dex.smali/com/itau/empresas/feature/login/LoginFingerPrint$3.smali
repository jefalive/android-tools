.class Lcom/itau/empresas/feature/login/LoginFingerPrint$3;
.super Ljava/lang/Object;
.source "LoginFingerPrint.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/itau/empresas/feature/login/LoginFingerPrint;->exibeMensagemErroFingerPrint(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/itau/empresas/feature/login/LoginFingerPrint;

.field final synthetic val$dialog:Lcom/itau/empresas/ui/dialog/CustomAlertDialog;


# direct methods
.method constructor <init>(Lcom/itau/empresas/feature/login/LoginFingerPrint;Lcom/itau/empresas/ui/dialog/CustomAlertDialog;)V
    .registers 3
    .param p1, "this$0"    # Lcom/itau/empresas/feature/login/LoginFingerPrint;

    .line 128
    iput-object p1, p0, Lcom/itau/empresas/feature/login/LoginFingerPrint$3;->this$0:Lcom/itau/empresas/feature/login/LoginFingerPrint;

    iput-object p2, p0, Lcom/itau/empresas/feature/login/LoginFingerPrint$3;->val$dialog:Lcom/itau/empresas/ui/dialog/CustomAlertDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .registers 4
    .param p1, "view"    # Landroid/view/View;

    .line 131
    iget-object v0, p0, Lcom/itau/empresas/feature/login/LoginFingerPrint$3;->val$dialog:Lcom/itau/empresas/ui/dialog/CustomAlertDialog;

    invoke-virtual {v0}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog;->dismiss()V

    .line 132
    iget-object v0, p0, Lcom/itau/empresas/feature/login/LoginFingerPrint$3;->this$0:Lcom/itau/empresas/feature/login/LoginFingerPrint;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/login/LoginFingerPrint;->setLogarComTeclado(Z)V

    .line 133
    iget-object v0, p0, Lcom/itau/empresas/feature/login/LoginFingerPrint$3;->this$0:Lcom/itau/empresas/feature/login/LoginFingerPrint;

    # getter for: Lcom/itau/empresas/feature/login/LoginFingerPrint;->loginActivity:Lcom/itau/empresas/feature/login/LoginActivity;
    invoke-static {v0}, Lcom/itau/empresas/feature/login/LoginFingerPrint;->access$100(Lcom/itau/empresas/feature/login/LoginFingerPrint;)Lcom/itau/empresas/feature/login/LoginActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/feature/login/LoginActivity;->escondeDialogoDeProgresso()V

    .line 134
    iget-object v0, p0, Lcom/itau/empresas/feature/login/LoginFingerPrint$3;->this$0:Lcom/itau/empresas/feature/login/LoginFingerPrint;

    # getter for: Lcom/itau/empresas/feature/login/LoginFingerPrint;->loginActivity:Lcom/itau/empresas/feature/login/LoginActivity;
    invoke-static {v0}, Lcom/itau/empresas/feature/login/LoginFingerPrint;->access$100(Lcom/itau/empresas/feature/login/LoginFingerPrint;)Lcom/itau/empresas/feature/login/LoginActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/feature/login/LoginActivity;->chamaAPITeclado()V

    .line 135
    iget-object v0, p0, Lcom/itau/empresas/feature/login/LoginFingerPrint$3;->this$0:Lcom/itau/empresas/feature/login/LoginFingerPrint;

    # getter for: Lcom/itau/empresas/feature/login/LoginFingerPrint;->loginActivity:Lcom/itau/empresas/feature/login/LoginActivity;
    invoke-static {v0}, Lcom/itau/empresas/feature/login/LoginFingerPrint;->access$100(Lcom/itau/empresas/feature/login/LoginFingerPrint;)Lcom/itau/empresas/feature/login/LoginActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/feature/login/LoginActivity;->mostraDialogoDeProgresso()V

    .line 136
    return-void
.end method

.class public Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/GpsRequestVO;
.super Ljava/lang/Object;
.source "GpsRequestVO.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private agencia:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "agencia"
    .end annotation
.end field

.field private anoCompetencia:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "ano_competencia"
    .end annotation
.end field

.field private cnpj:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "cnpj"
    .end annotation
.end field

.field private codigoOperador:Ljava/lang/Integer;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "codigo_operador"
    .end annotation
.end field

.field private codigoPagamento:Ljava/lang/Integer;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "codigo_pagamento"
    .end annotation
.end field

.field private confirmaInclusao:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "confirma_inclusao"
    .end annotation
.end field

.field private conta:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "conta"
    .end annotation
.end field

.field private dataPagamento:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "data_pagamento"
    .end annotation
.end field

.field private digitoVerificadorConta:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "digito_verificador_conta"
    .end annotation
.end field

.field private duplicidade:Ljava/lang/Boolean;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "duplicidade"
    .end annotation
.end field

.field private identificadorInclusao:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "identificador_inclusao"
    .end annotation
.end field

.field private informacoesComplementares:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "informacoes_complementares"
    .end annotation
.end field

.field private mesCompetencia:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "mes_competencia"
    .end annotation
.end field

.field private nomeContribuinte:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "nome_contribuinte"
    .end annotation
.end field

.field private nomeEmpresa:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "nome_empresa"
    .end annotation
.end field

.field private referenciaEmpresa:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "referencia_empresa"
    .end annotation
.end field

.field private tipoUsuario:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "tipo_usuario"
    .end annotation
.end field

.field private valorArrecadado:Ljava/math/BigDecimal;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "valor_arrecadado"
    .end annotation
.end field

.field private valorAtualizacaoMonetaria:Ljava/math/BigDecimal;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "valor_atualizacao_monetaria"
    .end annotation
.end field

.field private valorINSS:Ljava/math/BigDecimal;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "valor_INSS"
    .end annotation
.end field

.field private valorOutrasEntidades:Ljava/math/BigDecimal;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "valor_outras_entidades"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getAnoCompetencia()Ljava/lang/String;
    .registers 2

    .line 153
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/GpsRequestVO;->anoCompetencia:Ljava/lang/String;

    return-object v0
.end method

.method public getCnpj()Ljava/lang/String;
    .registers 2

    .line 73
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/GpsRequestVO;->cnpj:Ljava/lang/String;

    return-object v0
.end method

.method public getCodigoPagamento()Ljava/lang/Integer;
    .registers 2

    .line 137
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/GpsRequestVO;->codigoPagamento:Ljava/lang/Integer;

    return-object v0
.end method

.method public getDataPagamento()Ljava/lang/String;
    .registers 2

    .line 176
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/GpsRequestVO;->dataPagamento:Ljava/lang/String;

    return-object v0
.end method

.method public getIdentificadorInclusao()Ljava/lang/String;
    .registers 2

    .line 161
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/GpsRequestVO;->identificadorInclusao:Ljava/lang/String;

    return-object v0
.end method

.method public getInformacoesComplementares()Ljava/lang/String;
    .registers 2

    .line 129
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/GpsRequestVO;->informacoesComplementares:Ljava/lang/String;

    return-object v0
.end method

.method public getMesCompetencia()Ljava/lang/String;
    .registers 2

    .line 145
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/GpsRequestVO;->mesCompetencia:Ljava/lang/String;

    return-object v0
.end method

.method public getNomeContribuinte()Ljava/lang/String;
    .registers 2

    .line 121
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/GpsRequestVO;->nomeContribuinte:Ljava/lang/String;

    return-object v0
.end method

.method public getReferenciaEmpresa()Ljava/lang/String;
    .registers 2

    .line 216
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/GpsRequestVO;->referenciaEmpresa:Ljava/lang/String;

    return-object v0
.end method

.method public getValorArrecadado()Ljava/math/BigDecimal;
    .registers 2

    .line 208
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/GpsRequestVO;->valorArrecadado:Ljava/math/BigDecimal;

    return-object v0
.end method

.method public getValorAtualizacaoMonetaria()Ljava/math/BigDecimal;
    .registers 2

    .line 200
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/GpsRequestVO;->valorAtualizacaoMonetaria:Ljava/math/BigDecimal;

    return-object v0
.end method

.method public getValorINSS()Ljava/math/BigDecimal;
    .registers 2

    .line 184
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/GpsRequestVO;->valorINSS:Ljava/math/BigDecimal;

    return-object v0
.end method

.method public getValorOutrasEntidades()Ljava/math/BigDecimal;
    .registers 2

    .line 192
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/GpsRequestVO;->valorOutrasEntidades:Ljava/math/BigDecimal;

    return-object v0
.end method

.method public setAgencia(Ljava/lang/String;)V
    .registers 2
    .param p1, "agencia"    # Ljava/lang/String;

    .line 101
    iput-object p1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/GpsRequestVO;->agencia:Ljava/lang/String;

    .line 102
    return-void
.end method

.method public setAnoCompetencia(Ljava/lang/String;)V
    .registers 2
    .param p1, "anoCompetencia"    # Ljava/lang/String;

    .line 157
    iput-object p1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/GpsRequestVO;->anoCompetencia:Ljava/lang/String;

    .line 158
    return-void
.end method

.method public setCnpj(Ljava/lang/String;)V
    .registers 2
    .param p1, "cnpj"    # Ljava/lang/String;

    .line 77
    iput-object p1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/GpsRequestVO;->cnpj:Ljava/lang/String;

    .line 78
    return-void
.end method

.method public setCodigoOperador(Ljava/lang/Integer;)V
    .registers 2
    .param p1, "codigoOperador"    # Ljava/lang/Integer;

    .line 69
    iput-object p1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/GpsRequestVO;->codigoOperador:Ljava/lang/Integer;

    .line 70
    return-void
.end method

.method public setCodigoPagamento(Ljava/lang/Integer;)V
    .registers 2
    .param p1, "codigoPagamento"    # Ljava/lang/Integer;

    .line 141
    iput-object p1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/GpsRequestVO;->codigoPagamento:Ljava/lang/Integer;

    .line 142
    return-void
.end method

.method public setConta(Ljava/lang/String;)V
    .registers 2
    .param p1, "conta"    # Ljava/lang/String;

    .line 109
    iput-object p1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/GpsRequestVO;->conta:Ljava/lang/String;

    .line 110
    return-void
.end method

.method public setDataPagamento(Ljava/lang/String;)V
    .registers 2
    .param p1, "dataPagamento"    # Ljava/lang/String;

    .line 180
    iput-object p1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/GpsRequestVO;->dataPagamento:Ljava/lang/String;

    .line 181
    return-void
.end method

.method public setDigitoVerificadorConta(Ljava/lang/String;)V
    .registers 2
    .param p1, "digitoVerificadorConta"    # Ljava/lang/String;

    .line 117
    iput-object p1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/GpsRequestVO;->digitoVerificadorConta:Ljava/lang/String;

    .line 118
    return-void
.end method

.method public setDuplicidade(Ljava/lang/Boolean;)V
    .registers 2
    .param p1, "duplicidade"    # Ljava/lang/Boolean;

    .line 61
    iput-object p1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/GpsRequestVO;->duplicidade:Ljava/lang/Boolean;

    .line 62
    return-void
.end method

.method public setIdentificadorInclusao(Ljava/lang/String;)V
    .registers 7
    .param p1, "identificadorInclusao"    # Ljava/lang/String;

    .line 165
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    rsub-int/lit8 v2, v0, 0xe

    .line 166
    .local v2, "diff":I
    const-string v3, ""

    .line 167
    .local v3, "novoIdentificaor":Ljava/lang/String;
    const/16 v0, 0xe

    if-ge v2, v0, :cond_18

    .line 168
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_d
    if-ge v4, v2, :cond_18

    .line 169
    const-string v0, "0"

    invoke-virtual {v3, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 168
    add-int/lit8 v4, v4, 0x1

    goto :goto_d

    .line 172
    .end local v4    # "i":I
    :cond_18
    invoke-virtual {v3, p1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/GpsRequestVO;->identificadorInclusao:Ljava/lang/String;

    .line 173
    return-void
.end method

.method public setInformacoesComplementares(Ljava/lang/String;)V
    .registers 2
    .param p1, "informacoesComplementares"    # Ljava/lang/String;

    .line 133
    iput-object p1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/GpsRequestVO;->informacoesComplementares:Ljava/lang/String;

    .line 134
    return-void
.end method

.method public setMesCompetencia(Ljava/lang/String;)V
    .registers 2
    .param p1, "mesCompetencia"    # Ljava/lang/String;

    .line 149
    iput-object p1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/GpsRequestVO;->mesCompetencia:Ljava/lang/String;

    .line 150
    return-void
.end method

.method public setNomeContribuinte(Ljava/lang/String;)V
    .registers 2
    .param p1, "nomeContribuinte"    # Ljava/lang/String;

    .line 125
    iput-object p1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/GpsRequestVO;->nomeContribuinte:Ljava/lang/String;

    .line 126
    return-void
.end method

.method public setNomeEmpresa(Ljava/lang/String;)V
    .registers 2
    .param p1, "nomeEmpresa"    # Ljava/lang/String;

    .line 85
    iput-object p1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/GpsRequestVO;->nomeEmpresa:Ljava/lang/String;

    .line 86
    return-void
.end method

.method public setReferenciaEmpresa(Ljava/lang/String;)V
    .registers 2
    .param p1, "referenciaEmpresa"    # Ljava/lang/String;

    .line 220
    iput-object p1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/GpsRequestVO;->referenciaEmpresa:Ljava/lang/String;

    .line 221
    return-void
.end method

.method public setTipoUsuario(Ljava/lang/String;)V
    .registers 2
    .param p1, "tipoUsuario"    # Ljava/lang/String;

    .line 228
    iput-object p1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/GpsRequestVO;->tipoUsuario:Ljava/lang/String;

    .line 229
    return-void
.end method

.method public setValorArrecadado(Ljava/math/BigDecimal;)V
    .registers 2
    .param p1, "valorArrecadado"    # Ljava/math/BigDecimal;

    .line 212
    iput-object p1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/GpsRequestVO;->valorArrecadado:Ljava/math/BigDecimal;

    .line 213
    return-void
.end method

.method public setValorAtualizacaoMonetaria(Ljava/math/BigDecimal;)V
    .registers 2
    .param p1, "valorAtualizacaoMonetaria"    # Ljava/math/BigDecimal;

    .line 204
    iput-object p1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/GpsRequestVO;->valorAtualizacaoMonetaria:Ljava/math/BigDecimal;

    .line 205
    return-void
.end method

.method public setValorINSS(Ljava/math/BigDecimal;)V
    .registers 2
    .param p1, "valorINSS"    # Ljava/math/BigDecimal;

    .line 188
    iput-object p1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/GpsRequestVO;->valorINSS:Ljava/math/BigDecimal;

    .line 189
    return-void
.end method

.method public setValorOutrasEntidades(Ljava/math/BigDecimal;)V
    .registers 2
    .param p1, "valorOutrasEntidades"    # Ljava/math/BigDecimal;

    .line 196
    iput-object p1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/GpsRequestVO;->valorOutrasEntidades:Ljava/math/BigDecimal;

    .line 197
    return-void
.end method

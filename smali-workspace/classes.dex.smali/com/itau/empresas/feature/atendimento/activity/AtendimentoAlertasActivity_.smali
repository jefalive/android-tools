.class public final Lcom/itau/empresas/feature/atendimento/activity/AtendimentoAlertasActivity_;
.super Lcom/itau/empresas/feature/atendimento/activity/AtendimentoAlertasActivity;
.source "AtendimentoAlertasActivity_.java"

# interfaces
.implements Lorg/androidannotations/api/view/HasViews;
.implements Lorg/androidannotations/api/view/OnViewChangedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/itau/empresas/feature/atendimento/activity/AtendimentoAlertasActivity_$IntentBuilder_;
    }
.end annotation


# instance fields
.field private final onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;


# direct methods
.method public constructor <init>()V
    .registers 2

    .line 32
    invoke-direct {p0}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoAlertasActivity;-><init>()V

    .line 36
    new-instance v0, Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-direct {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoAlertasActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    return-void
.end method

.method static synthetic access$001(Lcom/itau/empresas/feature/atendimento/activity/AtendimentoAlertasActivity_;)V
    .registers 1
    .param p0, "x0"    # Lcom/itau/empresas/feature/atendimento/activity/AtendimentoAlertasActivity_;

    .line 32
    invoke-super {p0}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoAlertasActivity;->mostraDialogoDeProgresso()V

    return-void
.end method

.method static synthetic access$101(Lcom/itau/empresas/feature/atendimento/activity/AtendimentoAlertasActivity_;)V
    .registers 1
    .param p0, "x0"    # Lcom/itau/empresas/feature/atendimento/activity/AtendimentoAlertasActivity_;

    .line 32
    invoke-super {p0}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoAlertasActivity;->escondeDialogoDeProgresso()V

    return-void
.end method

.method private init_(Landroid/os/Bundle;)V
    .registers 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 48
    new-instance v0, Lcom/itau/empresas/Preferences_;

    invoke-direct {v0, p0}, Lcom/itau/empresas/Preferences_;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoAlertasActivity_;->prefs:Lcom/itau/empresas/Preferences_;

    .line 49
    invoke-static {p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->registerOnViewChangedListener(Lorg/androidannotations/api/view/OnViewChangedListener;)V

    .line 50
    invoke-static {}, Lcom/itau/empresas/CustomApplication_;->getInstance()Lcom/itau/empresas/CustomApplication;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoAlertasActivity_;->application:Lcom/itau/empresas/CustomApplication;

    .line 51
    invoke-virtual {p0}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoAlertasActivity_;->afterInject()V

    .line 52
    return-void
.end method


# virtual methods
.method public escondeDialogoDeProgresso()V
    .registers 5

    .line 132
    const-string v0, ""

    new-instance v1, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoAlertasActivity_$4;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoAlertasActivity_$4;-><init>(Lcom/itau/empresas/feature/atendimento/activity/AtendimentoAlertasActivity_;)V

    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3}, Lorg/androidannotations/api/UiThreadExecutor;->runTask(Ljava/lang/String;Ljava/lang/Runnable;J)V

    .line 140
    return-void
.end method

.method public mostraDialogoDeProgresso()V
    .registers 5
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "CommitTransaction"
        }
    .end annotation

    .line 120
    const-string v0, ""

    new-instance v1, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoAlertasActivity_$3;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoAlertasActivity_$3;-><init>(Lcom/itau/empresas/feature/atendimento/activity/AtendimentoAlertasActivity_;)V

    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3}, Lorg/androidannotations/api/UiThreadExecutor;->runTask(Ljava/lang/String;Ljava/lang/Runnable;J)V

    .line 128
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 40
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoAlertasActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-static {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    move-result-object v1

    .line 41
    .local v1, "previousNotifier":Lorg/androidannotations/api/view/OnViewChangedNotifier;
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoAlertasActivity_;->init_(Landroid/os/Bundle;)V

    .line 42
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoAlertasActivity;->onCreate(Landroid/os/Bundle;)V

    .line 43
    invoke-static {v1}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 44
    const v0, 0x7f03001e

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoAlertasActivity_;->setContentView(I)V

    .line 45
    return-void
.end method

.method public onViewChanged(Lorg/androidannotations/api/view/HasViews;)V
    .registers 5
    .param p1, "hasViews"    # Lorg/androidannotations/api/view/HasViews;

    .line 86
    const v0, 0x7f0e00c8

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoAlertasActivity_;->root:Landroid/widget/LinearLayout;

    .line 87
    const v0, 0x7f0e00cb

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoAlertasActivity_;->textoDetalheAlerta:Landroid/widget/TextView;

    .line 88
    const v0, 0x7f0e00c9

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoAlertasActivity_;->textoTitulo:Landroid/widget/TextView;

    .line 89
    const v0, 0x7f0e00cc

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 90
    .local v1, "view_texto_ok":Landroid/view/View;
    const v0, 0x7f0e00ca

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 92
    .local v2, "view_img_fechar":Landroid/view/View;
    if-eqz v1, :cond_39

    .line 93
    new-instance v0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoAlertasActivity_$1;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoAlertasActivity_$1;-><init>(Lcom/itau/empresas/feature/atendimento/activity/AtendimentoAlertasActivity_;)V

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 102
    :cond_39
    if-eqz v2, :cond_43

    .line 103
    new-instance v0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoAlertasActivity_$2;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoAlertasActivity_$2;-><init>(Lcom/itau/empresas/feature/atendimento/activity/AtendimentoAlertasActivity_;)V

    invoke-virtual {v2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 112
    :cond_43
    invoke-virtual {p0}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoAlertasActivity_;->carregaElementosDeTela()V

    .line 113
    return-void
.end method

.method public setContentView(I)V
    .registers 3
    .param p1, "layoutResID"    # I

    .line 56
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoAlertasActivity;->setContentView(I)V

    .line 57
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoAlertasActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 58
    return-void
.end method

.method public setContentView(Landroid/view/View;)V
    .registers 3
    .param p1, "view"    # Landroid/view/View;

    .line 68
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoAlertasActivity;->setContentView(Landroid/view/View;)V

    .line 69
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoAlertasActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 70
    return-void
.end method

.method public setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .registers 4
    .param p1, "view"    # Landroid/view/View;
    .param p2, "params"    # Landroid/view/ViewGroup$LayoutParams;

    .line 62
    invoke-super {p0, p1, p2}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoAlertasActivity;->setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 63
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoAlertasActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 64
    return-void
.end method

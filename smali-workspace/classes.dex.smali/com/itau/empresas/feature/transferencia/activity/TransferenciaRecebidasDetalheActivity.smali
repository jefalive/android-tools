.class public Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity;
.super Lcom/itau/empresas/ui/activity/BaseActivity;
.source "TransferenciaRecebidasDetalheActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity$Acao;
    }
.end annotation


# instance fields
.field acao:Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity$Acao;

.field botaoCompartilhar:Landroid/widget/Button;

.field botaoSalvar:Landroid/widget/Button;

.field campoTipoTransferencia:Landroid/widget/TextView;

.field campoValorTranferencia:Landroid/widget/TextView;

.field llTransferenciaRecebidasDetalhes:Landroid/view/View;

.field scrollView:Landroid/widget/ScrollView;

.field textoCpfCnpj:Landroid/widget/TextView;

.field textoData:Landroid/widget/TextView;

.field textoRemetente:Landroid/widget/TextView;

.field toolBar:Landroid/support/v7/widget/Toolbar;

.field transferenciaRecebidasVO:Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasVO;

.field warning:Lcom/itau/empresas/ui/view/WarningView;


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 44
    invoke-direct {p0}, Lcom/itau/empresas/ui/activity/BaseActivity;-><init>()V

    return-void
.end method

.method private constroiToolbar()V
    .registers 3

    .line 188
    iget-object v0, p0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity;->toolBar:Landroid/support/v7/widget/Toolbar;

    invoke-static {v0, p0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom;->Builder(Landroid/support/v7/widget/Toolbar;Landroid/support/v7/app/AppCompatActivity;)Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    move-result-object v0

    .line 190
    const v1, 0x7f0706bf

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->comTitulo(I)Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    move-result-object v0

    .line 191
    invoke-virtual {v0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->comClickListenerFinish()Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    move-result-object v0

    .line 192
    invoke-virtual {v0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->build()Lcom/itau/empresas/ui/util/listener/ToolbarCustom;

    .line 193
    return-void
.end method

.method private criarFotoComprovante()Ljava/io/File;
    .registers 5

    .line 169
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 170
    .local v2, "views":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    iget-object v0, p0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity;->toolBar:Landroid/support/v7/widget/Toolbar;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 171
    iget-object v0, p0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity;->warning:Lcom/itau/empresas/ui/view/WarningView;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 172
    iget-object v0, p0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity;->scrollView:Landroid/widget/ScrollView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 174
    const-string v0, "comprovante"

    :try_start_1b
    invoke-static {p0, v2, v0}, Lcom/itau/empresas/ui/util/BitmapUtils;->salvarViewsPrint(Landroid/content/Context;Ljava/util/List;Ljava/lang/String;)Ljava/io/File;
    :try_end_1e
    .catch Ljava/io/IOException; {:try_start_1b .. :try_end_1e} :catch_20

    move-result-object v0

    return-object v0

    .line 175
    :catch_20
    move-exception v3

    .line 176
    .local v3, "e":Ljava/io/IOException;
    const-class v0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    .line 177
    invoke-virtual {v3}, Ljava/io/IOException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v1

    .line 176
    invoke-static {v0, v1, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 178
    const/4 v0, 0x0

    return-object v0
.end method

.method private disparaEventoAnalytics()V
    .registers 5

    .line 116
    invoke-static {}, Lcom/itau/empresas/feature/analytics/AnalyticsHelper;->getInstance()Lcom/itau/empresas/feature/analytics/AnalyticsHelper;

    move-result-object v3

    .line 117
    .line 118
    .local v3, "analyticsHelper":Lcom/itau/empresas/feature/analytics/AnalyticsHelper;
    const v0, 0x7f0700ff

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 119
    const v1, 0x7f0700d2

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 120
    const v2, 0x7f0700c3

    invoke-virtual {p0, v2}, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 117
    invoke-virtual {v3, v0, v1, v2}, Lcom/itau/empresas/feature/analytics/AnalyticsHelper;->eventoAdobeState(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 122
    return-void
.end method

.method public static intent(Landroid/content/Context;)Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity_$IntentBuilder_;
    .registers 2
    .param p0, "context"    # Landroid/content/Context;

    .line 200
    new-instance v0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity_$IntentBuilder_;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity_$IntentBuilder_;-><init>(Landroid/content/Context;)V

    return-object v0
.end method


# virtual methods
.method aoClicarEmCompartilhar()V
    .registers 4

    .line 145
    const v0, 0x7f0702a1

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity;->analyticsHit(Ljava/lang/String;)V

    .line 147
    invoke-static {}, Lcom/itau/empresas/ui/util/PermissoesUtil;->getPermissionWriteExternalStorage()[Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/itau/empresas/ui/util/PermissoesUtil;->hasSelfPermissions(Landroid/content/Context;[Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1e

    .line 149
    invoke-direct {p0}, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity;->criarFotoComprovante()Ljava/io/File;

    move-result-object v2

    .line 150
    .local v2, "f":Ljava/io/File;
    if-eqz v2, :cond_1d

    .line 151
    invoke-static {p0, v2}, Lcom/itau/empresas/ui/util/CompartilhamentoUtils;->compartilharArquivoImagem(Landroid/app/Activity;Ljava/io/File;)V

    .line 153
    .end local v2    # "f":Ljava/io/File;
    :cond_1d
    goto :goto_30

    .line 154
    :cond_1e
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x17

    if-lt v0, v1, :cond_30

    .line 155
    invoke-static {}, Lcom/itau/empresas/ui/util/PermissoesUtil;->getPermissionWriteExternalStorage()[Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity;->requestPermissions([Ljava/lang/String;I)V

    .line 157
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity;->registraView([Landroid/view/View;)V

    .line 160
    :cond_30
    :goto_30
    return-void
.end method

.method aoClicarEmSalvar()V
    .registers 4

    .line 126
    const v0, 0x7f0702c2

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity;->analyticsHit(Ljava/lang/String;)V

    .line 128
    invoke-static {}, Lcom/itau/empresas/ui/util/PermissoesUtil;->getPermissionWriteExternalStorage()[Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/itau/empresas/ui/util/PermissoesUtil;->hasSelfPermissions(Landroid/content/Context;[Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1e

    .line 130
    invoke-direct {p0}, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity;->criarFotoComprovante()Ljava/io/File;

    move-result-object v2

    .line 131
    .local v2, "f":Ljava/io/File;
    if-eqz v2, :cond_1d

    .line 132
    invoke-static {p0, v2}, Lcom/itau/empresas/ui/util/BitmapUtils;->abrirArquivoImagem(Landroid/content/Context;Ljava/io/File;)V

    .line 134
    .end local v2    # "f":Ljava/io/File;
    :cond_1d
    goto :goto_30

    .line 135
    :cond_1e
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x17

    if-lt v0, v1, :cond_30

    .line 136
    invoke-static {}, Lcom/itau/empresas/ui/util/PermissoesUtil;->getPermissionWriteExternalStorage()[Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity;->requestPermissions([Ljava/lang/String;I)V

    .line 138
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity;->registraView([Landroid/view/View;)V

    .line 141
    :cond_30
    :goto_30
    return-void
.end method

.method carregaElementosDaTela()V
    .registers 6

    .line 63
    invoke-direct {p0}, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity;->constroiToolbar()V

    .line 65
    iget-object v0, p0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity;->transferenciaRecebidasVO:Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasVO;

    if-eqz v0, :cond_a0

    .line 67
    iget-object v0, p0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity;->campoTipoTransferencia:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "valor do "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity;->transferenciaRecebidasVO:Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasVO;

    .line 68
    invoke-virtual {v2}, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasVO;->getTipoPagamento()Ljava/lang/String;

    move-result-object v2

    .line 69
    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 68
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 70
    iget-object v0, p0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity;->campoValorTranferencia:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity;->transferenciaRecebidasVO:Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasVO;

    .line 72
    invoke-virtual {v1}, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasVO;->getValorPagamento()Ljava/lang/String;

    move-result-object v1

    .line 71
    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/itau/empresas/ui/util/ValorMonetarioUtils;->formataValorDecimalParaMonetario(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v1

    .line 70
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 74
    iget-object v0, p0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity;->textoRemetente:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity;->transferenciaRecebidasVO:Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasVO;

    invoke-virtual {v1}, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasVO;->getRemetente()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 75
    sget-object v0, Lbr/com/concretesolutions/canarinho/formatador/FormatadorCPFCNPJ;->CPF_CNPJ:Lbr/com/concretesolutions/canarinho/formatador/Formatador;

    iget-object v1, p0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity;->transferenciaRecebidasVO:Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasVO;

    .line 76
    invoke-virtual {v1}, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasVO;->getCpfCnpj()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lbr/com/concretesolutions/canarinho/formatador/Formatador;->podeSerFormatado(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_63

    .line 77
    iget-object v0, p0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity;->textoCpfCnpj:Landroid/widget/TextView;

    sget-object v1, Lbr/com/concretesolutions/canarinho/formatador/FormatadorCPFCNPJ;->CPF_CNPJ:Lbr/com/concretesolutions/canarinho/formatador/Formatador;

    iget-object v2, p0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity;->transferenciaRecebidasVO:Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasVO;

    .line 79
    invoke-virtual {v2}, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasVO;->getCpfCnpj()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lbr/com/concretesolutions/canarinho/formatador/Formatador;->formata(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 77
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 81
    :cond_63
    iget-object v0, p0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity;->textoData:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity;->transferenciaRecebidasVO:Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasVO;

    .line 83
    invoke-virtual {v1}, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasVO;->getDataRecebimento()Ljava/lang/String;

    move-result-object v1

    .line 82
    invoke-static {v1}, Lcom/itau/empresas/ui/util/DataEHoraUtils;->formataDataComBarra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 81
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 85
    iget-object v0, p0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity;->warning:Lcom/itau/empresas/ui/view/WarningView;

    sget-object v1, Lcom/itau/empresas/ui/view/WarningView$Estado;->SUCESSO:Lcom/itau/empresas/ui/view/WarningView$Estado;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity;->transferenciaRecebidasVO:Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasVO;

    .line 88
    invoke-virtual {v3}, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasVO;->getDataRecebimento()Ljava/lang/String;

    move-result-object v3

    .line 87
    invoke-static {v3}, Lcom/itau/empresas/ui/util/DataEHoraUtils;->formataDataComBarra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    aput-object v3, v2, v4

    .line 86
    const v3, 0x7f07067e

    invoke-virtual {p0, v3, v2}, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 85
    invoke-virtual {v0, v1, v2}, Lcom/itau/empresas/ui/view/WarningView;->setMensagem(Lcom/itau/empresas/ui/view/WarningView$Estado;Ljava/lang/String;)V

    .line 89
    iget-object v0, p0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity;->warning:Lcom/itau/empresas/ui/view/WarningView;

    invoke-virtual {p0}, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c00cd

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/view/WarningView;->setBackgroundColor(I)V

    .line 93
    :cond_a0
    iget-object v0, p0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity;->llTransferenciaRecebidasDetalhes:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    new-instance v1, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity$1;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity$1;-><init>(Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 112
    invoke-direct {p0}, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity;->disparaEventoAnalytics()V

    .line 113
    return-void
.end method

.method public opkeysConsumidas()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Ljava/lang/String;>;"
        }
    .end annotation

    .line 184
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

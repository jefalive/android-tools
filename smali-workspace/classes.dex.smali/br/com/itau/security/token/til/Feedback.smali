.class public final Lbr/com/itau/security/token/til/Feedback;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:[B

.field private static b:I


# direct methods
.method static constructor <clinit>()V
    .registers 3

    const/16 v0, 0x40

    new-array v0, v0, [B

    fill-array-data v0, :array_e

    sput-object v0, Lbr/com/itau/security/token/til/Feedback;->a:[B

    const/16 v0, 0x10

    sput v0, Lbr/com/itau/security/token/til/Feedback;->b:I

    return-void

    :array_e
    .array-data 1
        0x7et
        -0x57t
        0x72t
        0x53t
        0x24t
        0x52t
        0x23t
        0x4t
        0x3t
        0x2t
        0x3t
        0x6t
        0xct
        -0x47t
        0x51t
        -0x4t
        0x12t
        0x4t
        -0xet
        0xat
        0x2t
        -0x41t
        0x4dt
        0xet
        -0x4ft
        0x52t
        0x5t
        0x9t
        -0x50t
        0x54t
        0x6t
        0x1t
        0x5t
        -0x7t
        0x11t
        -0x2t
        0x11t
        -0x55t
        0x4at
        0xdt
        0x7t
        -0x1t
        -0x8t
        0x17t
        0x4t
        -0xbt
        0x3t
        -0x40t
        0x53t
        0x7t
        -0x4et
        0x4dt
        0xet
        -0x4ft
        0x52t
        0xbt
        -0x5t
        0x4t
        -0x39t
        0x3at
        0xct
        0x7t
        0x8t
        0x9t
    .end array-data
.end method

.method public constructor <init>()V
    .registers 1

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(III)Ljava/lang/String;
    .registers 9

    sget-object v4, Lbr/com/itau/security/token/til/Feedback;->a:[B

    add-int/lit8 p0, p0, 0x4

    mul-int/lit8 p2, p2, 0x39

    add-int/lit8 p2, p2, 0x2

    mul-int/lit8 p1, p1, 0xb

    rsub-int/lit8 p1, p1, 0x5c

    new-instance v0, Ljava/lang/String;

    const/4 v5, 0x0

    new-array v1, p2, [B

    add-int/lit8 p2, p2, -0x1

    if-nez v4, :cond_1c

    move v2, p1

    move v3, p2

    :goto_17
    add-int/2addr v2, v3

    add-int/lit8 p1, v2, -0x4

    add-int/lit8 p0, p0, 0x1

    :cond_1c
    int-to-byte v2, p1

    aput-byte v2, v1, v5

    if-ne v5, p2, :cond_26

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Ljava/lang/String;-><init>([BI)V

    return-object v0

    :cond_26
    move v2, p1

    add-int/lit8 v5, v5, 0x1

    aget-byte v3, v4, p0

    goto :goto_17
.end method

.method public static fillMessage(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .registers 9

    .line 16
    if-eqz p1, :cond_44

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_44

    sget-object v0, Lbr/com/itau/security/token/til/Feedback;->a:[B

    const/16 v1, 0x1f

    aget-byte v0, v0, v1

    add-int/lit8 v0, v0, -0x1

    int-to-byte v0, v0

    int-to-byte v1, v0

    int-to-byte v2, v1

    invoke-static {v0, v1, v2}, Lbr/com/itau/security/token/til/Feedback;->a(III)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    .line 17
    invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    array-length v0, v0

    const/4 v1, 0x5

    if-lt v0, v1, :cond_44

    sget-object v0, Lbr/com/itau/security/token/til/Feedback;->a:[B

    const/16 v1, 0x1f

    aget-byte v0, v0, v1

    int-to-byte v0, v0

    sget-object v1, Lbr/com/itau/security/token/til/Feedback;->a:[B

    const/16 v2, 0x1a

    aget-byte v1, v1, v2

    int-to-byte v1, v1

    add-int/lit8 v2, v1, -0x5

    int-to-byte v2, v2

    invoke-static {v0, v1, v2}, Lbr/com/itau/security/token/til/Feedback;->a(III)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    .line 18
    invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    array-length v0, v0

    const/4 v1, 0x4

    if-eq v0, v1, :cond_61

    .line 19
    :cond_44
    new-instance v0, Lbr/com/itau/security/commons/exception/CommonSecurityException;

    sget-object v1, Lbr/com/itau/security/token/til/Feedback;->a:[B

    const/16 v2, 0x9

    aget-byte v1, v1, v2

    int-to-byte v1, v1

    int-to-byte v2, v1

    sget-object v3, Lbr/com/itau/security/token/til/Feedback;->a:[B

    const/16 v4, 0x1f

    aget-byte v3, v3, v4

    int-to-byte v3, v3

    invoke-static {v1, v2, v3}, Lbr/com/itau/security/token/til/Feedback;->a(III)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lbr/com/itau/security/commons/exception/CommonSecurityException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 21
    :cond_61
    const-string v5, ""

    .line 22
    invoke-static {p0}, Lti/d;->b(Landroid/content/Context;)Ljava/util/ArrayList;

    move-result-object v6

    .line 24
    invoke-interface {v6}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_80

    .line 25
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    sget v1, Lti/c;->a:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    invoke-static {v0}, Lbr/com/itau/security/commons/binary/ByteUtils;->to([B)Ljava/lang/String;

    move-result-object v5

    .line 27
    :cond_80
    invoke-static {p0}, Lbr/com/itau/security/did/DID;->getDID(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lbr/com/itau/security/commons/binary/ByteUtils;->from(Ljava/lang/String;)[B

    move-result-object v0

    invoke-static {v0}, Lbr/com/itau/security/commons/Crypto;->sha1([B)[B

    move-result-object v0

    invoke-static {v0}, Lbr/com/itau/security/commons/binary/ByteUtils;->bytes2hexStr([B)Ljava/lang/String;

    move-result-object p0

    .line 28
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    const/4 v1, 0x1

    aput-object p0, v0, v1

    const/4 v1, 0x2

    aput-object v5, v0, v1

    invoke-static {p1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

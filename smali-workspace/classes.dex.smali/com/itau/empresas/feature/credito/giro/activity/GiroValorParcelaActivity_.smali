.class public final Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity_;
.super Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity;
.source "GiroValorParcelaActivity_.java"

# interfaces
.implements Lorg/androidannotations/api/view/HasViews;
.implements Lorg/androidannotations/api/view/OnViewChangedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity_$IntentBuilder_;
    }
.end annotation


# instance fields
.field private final onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;


# direct methods
.method public constructor <init>()V
    .registers 2

    .line 38
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity;-><init>()V

    .line 42
    new-instance v0, Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-direct {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    return-void
.end method

.method static synthetic access$001(Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity_;)V
    .registers 1
    .param p0, "x0"    # Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity_;

    .line 38
    invoke-super {p0}, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity;->mostraDialogoDeProgresso()V

    return-void
.end method

.method static synthetic access$101(Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity_;)V
    .registers 1
    .param p0, "x0"    # Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity_;

    .line 38
    invoke-super {p0}, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity;->escondeDialogoDeProgresso()V

    return-void
.end method

.method private init_(Landroid/os/Bundle;)V
    .registers 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 62
    new-instance v0, Lcom/itau/empresas/Preferences_;

    invoke-direct {v0, p0}, Lcom/itau/empresas/Preferences_;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity_;->prefs:Lcom/itau/empresas/Preferences_;

    .line 63
    invoke-static {p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->registerOnViewChangedListener(Lorg/androidannotations/api/view/OnViewChangedListener;)V

    .line 64
    invoke-static {}, Lcom/itau/empresas/CustomApplication_;->getInstance()Lcom/itau/empresas/CustomApplication;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity_;->application:Lcom/itau/empresas/CustomApplication;

    .line 65
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity_;->injectExtras_()V

    .line 66
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity_;->afterInject()V

    .line 67
    return-void
.end method

.method private injectExtras_()V
    .registers 4

    .line 122
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity_;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    .line 123
    .local v2, "extras_":Landroid/os/Bundle;
    if-eqz v2, :cond_90

    .line 124
    const-string v0, "exibicao"

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 125
    const-string v0, "exibicao"

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity$Exibicao;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity_;->exibicao:Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity$Exibicao;

    .line 127
    :cond_1c
    const-string v0, "temSeguro"

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2c

    .line 128
    const-string v0, "temSeguro"

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity_;->temSeguro:Z

    .line 130
    :cond_2c
    const-string v0, "valorMinimo"

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3c

    .line 131
    const-string v0, "valorMinimo"

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->getFloat(Ljava/lang/String;)F

    move-result v0

    iput v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity_;->valorMinimo:F

    .line 133
    :cond_3c
    const-string v0, "valorMaximo"

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4c

    .line 134
    const-string v0, "valorMaximo"

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->getFloat(Ljava/lang/String;)F

    move-result v0

    iput v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity_;->valorMaximo:F

    .line 136
    :cond_4c
    const-string v0, "parcelaMinima"

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5c

    .line 137
    const-string v0, "parcelaMinima"

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity_;->parcelaMinima:J

    .line 139
    :cond_5c
    const-string v0, "parcelaMaxima"

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6c

    .line 140
    const-string v0, "parcelaMaxima"

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity_;->parcelaMaxima:J

    .line 142
    :cond_6c
    const-string v0, "produtosParceladosSimulacaoPropostaSaidaVO"

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7e

    .line 143
    const-string v0, "produtosParceladosSimulacaoPropostaSaidaVO"

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoPropostaSaidaVO;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity_;->produtosParceladosSimulacaoPropostaSaidaVO:Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoPropostaSaidaVO;

    .line 145
    :cond_7e
    const-string v0, "ofertaEspecialAceita"

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_90

    .line 146
    const-string v0, "ofertaEspecialAceita"

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity_;->ofertaEspecialAceita:Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;

    .line 149
    :cond_90
    return-void
.end method


# virtual methods
.method public escondeDialogoDeProgresso()V
    .registers 5

    .line 174
    const-string v0, ""

    new-instance v1, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity_$3;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity_$3;-><init>(Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity_;)V

    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3}, Lorg/androidannotations/api/UiThreadExecutor;->runTask(Ljava/lang/String;Ljava/lang/Runnable;J)V

    .line 182
    return-void
.end method

.method public mostraDialogoDeProgresso()V
    .registers 5
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "CommitTransaction"
        }
    .end annotation

    .line 162
    const-string v0, ""

    new-instance v1, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity_$2;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity_$2;-><init>(Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity_;)V

    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3}, Lorg/androidannotations/api/UiThreadExecutor;->runTask(Ljava/lang/String;Ljava/lang/Runnable;J)V

    .line 170
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 54
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-static {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    move-result-object v1

    .line 55
    .local v1, "previousNotifier":Lorg/androidannotations/api/view/OnViewChangedNotifier;
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity_;->init_(Landroid/os/Bundle;)V

    .line 56
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity;->onCreate(Landroid/os/Bundle;)V

    .line 57
    invoke-static {v1}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 58
    const v0, 0x7f03003c

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity_;->setContentView(I)V

    .line 59
    return-void
.end method

.method public onViewChanged(Lorg/androidannotations/api/view/HasViews;)V
    .registers 4
    .param p1, "hasViews"    # Lorg/androidannotations/api/view/HasViews;

    .line 101
    const v0, 0x7f0e01b4

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity_;->textoPergunta:Landroid/widget/TextView;

    .line 102
    const v0, 0x7f0e01b5

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity_;->textoIndicador:Landroid/widget/TextView;

    .line 103
    const v0, 0x7f0e01b7

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity_;->textoAuxiliar:Landroid/widget/TextView;

    .line 104
    const v0, 0x7f0e01b6

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lbr/com/itau/widgets/material/MaterialEditText;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity_;->campoEntrada:Lbr/com/itau/widgets/material/MaterialEditText;

    .line 105
    const v0, 0x7f0e00b6

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/Toolbar;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity_;->toolBar:Landroid/support/v7/widget/Toolbar;

    .line 106
    const v0, 0x7f0e018f

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 108
    .local v1, "view_botao_continuar":Landroid/view/View;
    if-eqz v1, :cond_48

    .line 109
    new-instance v0, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity_$1;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity_$1;-><init>(Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity_;)V

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 118
    :cond_48
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity_;->aoCarregarTela()V

    .line 119
    return-void
.end method

.method public setContentView(I)V
    .registers 3
    .param p1, "layoutResID"    # I

    .line 71
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity;->setContentView(I)V

    .line 72
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 73
    return-void
.end method

.method public setContentView(Landroid/view/View;)V
    .registers 3
    .param p1, "view"    # Landroid/view/View;

    .line 83
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity;->setContentView(Landroid/view/View;)V

    .line 84
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 85
    return-void
.end method

.method public setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .registers 4
    .param p1, "view"    # Landroid/view/View;
    .param p2, "params"    # Landroid/view/ViewGroup$LayoutParams;

    .line 77
    invoke-super {p0, p1, p2}, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity;->setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 78
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 79
    return-void
.end method

.method public setIntent(Landroid/content/Intent;)V
    .registers 2
    .param p1, "newIntent"    # Landroid/content/Intent;

    .line 153
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity;->setIntent(Landroid/content/Intent;)V

    .line 154
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity_;->injectExtras_()V

    .line 155
    return-void
.end method
